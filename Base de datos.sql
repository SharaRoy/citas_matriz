-- Pendiente
ALTER TABLE `documentacion_ordenes` ADD `tipo_vita` INT NULL DEFAULT '1' AFTER `folio_externo`;

--
CREATE TABLE `comentarios_cierre_orden` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `id_cita` int(11) NULL DEFAULT NULL,
 `fecha_registro` datetime NULL DEFAULT NULL,
 `id_usuario` int(11) NULL DEFAULT NULL,
 `titulo` varchar(50) NULL DEFAULT NULL,
 `comentario` text NULL DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- /. Adicionales para garantias 
ALTER TABLE `presupuesto_multipunto` ADD `firma_gerente_garantia` VARCHAR(100) NULL DEFAULT NULL AFTER `prioridad`, ADD `firma_jefetaller_garantia` VARCHAR(100) NULL DEFAULT NULL AFTER `firma_gerente_garantia`;
ALTER TABLE `presupuesto_multipunto` ADD `fecha_firma_gerente` DATETIME NULL DEFAULT NULL AFTER `fecha_pedido_dms`, ADD `fecha_firma_jefetaller` DATETIME NULL DEFAULT NULL AFTER `fecha_firma_gerente`;

ALTER TABLE `presupuesto_multipunto` ADD `nombre_gerente` VARCHAR(100) NULL DEFAULT NULL AFTER `firma_gerente_garantia`;
ALTER TABLE `presupuesto_multipunto` ADD `nombre_jefetaller` VARCHAR(100) NULL DEFAULT NULL AFTER `firma_jefetaller_garantia`;

ALTER TABLE `presupuesto_multipunto` ADD `prefijo` VARCHAR(10) NULL DEFAULT NULL AFTER `identificador`, 
ADD `sufijo` VARCHAR(15) NULL DEFAULT NULL AFTER `prefijo`, 
ADD `basico` VARCHAR(15) NULL DEFAULT NULL AFTER `sufijo`;

ALTER TABLE `presupuesto_multipunto` ADD `nombre_gerente_req` VARCHAR(100) NULL DEFAULT NULL AFTER `nombre_jefetaller`, 
ADD `firma_gerente_req` VARCHAR(100) NULL DEFAULT NULL AFTER `nombre_gerente_req`, 
ADD `parte_remplazada_req` VARCHAR(50) NULL DEFAULT NULL AFTER `firma_gerente_req`, 
ADD `asc_servicio_req` VARCHAR(50) NULL DEFAULT NULL AFTER `parte_remplazada_req`;

ALTER TABLE `presupuesto_multipunto` ADD `fecha_firma_greq` DATETIME NULL DEFAULT NULL AFTER `fecha_firma_jefetaller`;

ALTER TABLE `presupuesto_multipunto` ADD `id_garantia` INT NULL DEFAULT NULL AFTER `id_cita`;
ALTER TABLE `presupuesto_multipunto` ADD `alta_garantia` INT NULL DEFAULT NULL AFTER `identificador`;
ALTER TABLE `presupuesto_multipunto` ADD `fecha_registro_garantia` DATETIME NULL DEFAULT NULL AFTER `fecha_registro`;
ALTER TABLE `presupuesto_registro` ADD `anexa_garantias` INT NULL DEFAULT NULL AFTER `identificador`;
ALTER TABLE `presupuesto_multipunto` ADD `ref` VARCHAR(25) NULL DEFAULT NULL AFTER `alta_garantia`;
ALTER TABLE `presupuesto_multipunto` ADD `fecha_precesa_altag` DATETIME NULL DEFAULT NULL AFTER `fecha_psni`;
ALTER TABLE `presupuesto_multipunto` ADD `fecha_entrega` DATETIME NULL DEFAULT NULL AFTER `fecha_precesa_altag`;

ALTER TABLE `presupuesto_multipunto` ADD `firma_ventanilla_entrega` VARCHAR(90) NULL DEFAULT NULL AFTER `estado_entrega`;
ALTER TABLE `presupuesto_multipunto` ADD `fecha_firma_entrega` DATETIME NULL DEFAULT NULL COMMENT 'firma de ventanilla' AFTER `fecha_firma_greq`;
ALTER TABLE `presupuesto_registro` ADD `fecha_entrega_garantia` DATETIME NULL DEFAULT NULL AFTER `termina_requisicion_garantias`;
ALTER TABLE `historial_requisicion` ADD `tipo_archivo` INT NOT NULL DEFAULT '1' COMMENT ' 1 - Requisicion, 2- Recibo de piezas' AFTER `id`;

ALTER TABLE `carta_renuncia` CHANGE `beneficios` `beneficios` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `presupuesto_multipunto` CHANGE `identificador` `identificador` CHAR(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'M';
ALTER TABLE `presupuesto_registro` CHANGE `identificador` `identificador` CHAR(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'M';

ALTER TABLE `presupuesto_multipunto` ADD `id_tipo_orden` INT NULL DEFAULT NULL AFTER `identificador`;
ALTER TABLE `presupuesto_multipunto` ADD `costo_aseguradora` FLOAT NULL DEFAULT NULL AFTER `costo_pieza`;
ALTER TABLE `presupuesto_multipunto` ADD `rep_adicional` INT NULL DEFAULT NULL AFTER `costo_aseguradora`;
ALTER TABLE `presupuesto_registro` ADD `id_tipo_orden` INT NULL DEFAULT NULL AFTER `identificador`;

CREATE TABLE `historial_requisicion` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`id_cita` int(11) DEFAULT NULL,
	`url_archivo` varchar(100) DEFAULT NULL,
	`fecha_creacion` datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `archivos_poliza_garantias` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`id_cita` int(11) DEFAULT NULL,
	`url_archivo` varchar(100) DEFAULT NULL,
	`id_usuario` int(11) DEFAULT NULL,
	`usuario` varchar(80) DEFAULT NULL,
	`fecha_creacion` datetime DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `documentacion_ordenes` ADD `archivo_poliza` INT NULL DEFAULT NULL AFTER `servicio_valet`;
ALTER TABLE `archivos_poliza_garantias` ADD `activo` INT NULL DEFAULT '1' AFTER `usuario`;

 CREATE TABLE `refaciones_psni` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `id_cita` int(11) DEFAULT NULL,
 `id_registro` int(11) DEFAULT NULL,
 `id_refaccion` int(11) DEFAULT NULL,
 `tipo` int(11) DEFAULT NULL COMMENT '1 - publico, 2 -garantias',
 `cantidad` float DEFAULT NULL,
 `descripcion` tinytext,
 `clave_pieza` varchar(40) DEFAULT NULL,
 `existencia` varchar(8) DEFAULT NULL,
 `activo` int(11) DEFAULT NULL,
 `pedido` int(11) DEFAULT NULL,
 `fecha_pedido` datetime DEFAULT NULL,
 `fecha_alta` datetime DEFAULT NULL,
 `fecha_edicion` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `refacciones_cliente` ADD `tiempo_inst_tecnico` VARCHAR(30) NULL DEFAULT NULL AFTER `archivos`, 
ADD `tiempo_inst_cleinte` VARCHAR(30) NULL DEFAULT NULL AFTER `tiempo_inst_tecnico`;

ALTER TABLE `refacciones_garantias` ADD `tiempo_inst_tecnico` VARCHAR(30) NULL DEFAULT NULL AFTER `backorder`, 
ADD `tiempo_inst_cleinte` VARCHAR(30) NULL DEFAULT NULL AFTER `tiempo_inst_tecnico`; 

ALTER TABLE `estatus_refacciones_historico` ADD `id_registro` INT NULL DEFAULT NULL AFTER `id_cita`;
ALTER TABLE `estatus_cliente_historico` ADD `id_registro` INT NULL DEFAULT NULL AFTER `id_cita`;

ALTER TABLE `presupuesto_registro` ADD `tecnico_nombre`  VARCHAR(100) NULL DEFAULT NULL AFTER `firma_requisicion_garantias`;
ALTER TABLE `presupuesto_registro` ADD `garantias_nombre`  VARCHAR(100) NULL DEFAULT NULL AFTER `tecnico_nombre`;
ALTER TABLE `articulos_orden` ADD `folio_asociado`  VARCHAR(20) NULL DEFAULT NULL AFTER `fecha_detencion`;
ALTER TABLE `articulos_orden` ADD `n_reparacion_garantia`  INT NULL DEFAULT NULL AFTER `folio_asociado`;


CREATE TABLE `registro_historial_soporte` ( 
    `id` INT NOT NULL AUTO_INCREMENT ,  
    `id_cita` INT NULL DEFAULT NULL ,  
    `id_login` INT NULL DEFAULT NULL ,  
    `usuario_login` VARCHAR(40) NULL DEFAULT NULL ,  
    `usuario_edita` VARCHAR(40) NULL DEFAULT NULL ,  
    `motivo_edicion` TINYTEXT NULL DEFAULT NULL ,  
    `formulario` VARCHAR(50) NULL DEFAULT NULL ,  
    `respaldo_edicion` TINYTEXT NULL DEFAULT NULL ,  
    `fecha_registro` DATETIME NULL DEFAULT NULL ,    
    PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `registro_evidencia_soporte` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `id_cita` int(11) DEFAULT NULL,
 `ref_historico` int(11) DEFAULT NULL,
 `id_login` int(11) DEFAULT NULL,
 `usuario_edita` varchar(50) DEFAULT NULL,
 `formulario` varchar(30) DEFAULT NULL,
 `url` varchar(100) DEFAULT NULL,
 `fecha_registro` datetime DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;