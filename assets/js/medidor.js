  //Creamos la barra para mostrar
  var nivel_gas = document.getElementById("nivel_gasolina");
  var ctx_gas = nivel_gas.getContext("2d");
  ctx_gas.beginPath();
  ctx_gas.strokeStyle = "#999191";
  ctx_gas.lineWidth = 35;
  ctx_gas.arc(125,125,100,0,(Math.PI/180)*180,true);
  ctx_gas.stroke();
  ctx_gas.closePath();

  //Inicializar progress
  $("#medidor").css('background', '#ffffff');
  $("#medidor").css('height', '10px');


  $("#medidor").slider({
      value:$('#nivelGasolina').val(),
      slide: function(event, ui) {
          // console.log(ui.value);
          var nvoGrado = 0;
          var pivote = 0;
          var nvoPivote = 0;
          var nvoNivel = 0;

          $('#nivelGasolina').val(ui.value);

          //Cambiamos los colores del medidor
          if((ui.value == 0) || (ui.value < 20)){
              //Obetenemos nuevos angulos
              nvoGrado = (ui.value*1.8)+180;
              pivote = (Math.PI/180)*180;
              nvoNivel = (Math.PI/180)*nvoGrado;

              //Pintamos el circulo
              ctx_gas.beginPath();
              ctx_gas.strokeStyle = "#ff0000";
              ctx_gas.lineWidth = 35;
              ctx_gas.arc(125,125,100,pivote,nvoNivel,false);
              ctx_gas.stroke();
              ctx_gas.closePath();

              //Si retrocedio limpiamos la barra
              ctx_gas.beginPath();
              ctx_gas.strokeStyle = "#999191";
              ctx_gas.lineWidth = 35;
              ctx_gas.arc(125,125,100,(Math.PI/180)*210,(Math.PI/180)*320,false);
              ctx_gas.stroke();
              ctx_gas.closePath();
          }else if ((ui.value == 20) || (ui.value < 80)) {
              //Obetenemos nuevos angulos
              nvoGrado = (ui.value*1.8)+180;
              pivote = (Math.PI/180)*210;
              nvoNivel = (Math.PI/180)*nvoGrado;

              //Pintamos el circulo
              ctx_gas.beginPath();
              ctx_gas.strokeStyle = "#ffff00";
              ctx_gas.lineWidth = 35;
              ctx_gas.arc(125,125,100,pivote,nvoNivel,false);
              ctx_gas.stroke();
              ctx_gas.closePath();
              // console.log(ui.value);
              //Si retrocedio limpiamos la barra
              ctx_gas.beginPath();
              ctx_gas.strokeStyle = "#999191";
              ctx_gas.lineWidth = 35;
              ctx_gas.arc(125,125,100,(Math.PI/180)*320,(Math.PI/180)*360,false);
              ctx_gas.stroke();
              ctx_gas.closePath();

              //Pintamos el circulo
              ctx_gas.beginPath();
              ctx_gas.strokeStyle = "#ff0000";
              ctx_gas.lineWidth = 35;
              ctx_gas.arc(125,125,100,(Math.PI/180)*180,(Math.PI/180)*210,false);
              ctx_gas.stroke();
              ctx_gas.closePath();

          }else if(ui.value >= 80) {
              //Obetenemos nuevos angulos
              nvoGrado = (ui.value*1.8)+180;
              // nvoPivote = 180+80;
              pivote = (Math.PI/180)*320;
              nvoNivel = (Math.PI/180)*nvoGrado;

              //Pintamos el circulo
              ctx_gas.beginPath();
              ctx_gas.strokeStyle = "#00ff00";
              ctx_gas.lineWidth = 35;
              ctx_gas.arc(125,125,100,pivote,nvoNivel,false);
              ctx_gas.stroke();
              ctx_gas.closePath();
          }
      }
  });
