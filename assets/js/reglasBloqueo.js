$(document).ready(function() {
    //Comprobamos quien entra al formulario y que tipo de proceso llevara
    var panelOrigen = $("#panelOrigen").val();

    //Si es una edicion y no es desde el panel de administracion
    if ((panelOrigen != "ADMIN") && ($("#cargaFormulario").val() == "2")) {
        //Bloqueamos las firmas
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        $(".cuadroFirma").attr("disabled", true);
        $(".cuadroFirma").attr("data-toggle", false);
        // $("#noOrden").attr("disabled", false);
        $("#orderService").attr("disabled", false);

    }


    $("#superUsuarioName").attr("disabled", false);
    $("#superUsuarioPass").attr("disabled", false);
    $("#superUsuarioA").attr("disabled", false);
    $("#superUsuarioA").attr("data-toggle", "modal");

    /*if ($("#completoFormulario").val() == "1") {
        console.log("entro formulario completo");
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        $("a").attr("disabled", true);
        $("a").attr("data-toggle", false);

        $("#superUsuarioA").css("cursor","no-drop");

        $("#permisoProv").attr("disabled", false);
        $("#permisoProv").attr("data-toggle", "modal");
        $("#idCitaVideo").attr("disabled", false);
        $("#comentarioVideo").attr("disabled", false);
        $("#uploadfilesVideo").attr("disabled", false);
        $("input[name='tipoEnvioEvidencia']").attr("disabled", false);
    }*/

    $("#permisoProv").attr("disabled", false);
    $("#permisoProv").attr("data-toggle", "modal");
    $("#idCitaVideo").attr("disabled", false);
    $("#comentarioVideo").attr("disabled", false);
    $("#uploadfilesVideo").attr("disabled", false);
    $("input[name='tipoEnvioEvidencia']").attr("disabled", false);

    console.log("aqui");
});

$('#busqueda_tabla').keyup(function () {
    toSearch();
});


//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}
