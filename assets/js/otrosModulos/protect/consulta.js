$(document).ready(function() {
    if ($("input[name='id_orden']").length) {
        if ($("input[name='id_orden']").val() != "0") {
            $("input").attr("disabled", true);
            $("input[name='id_orden']").attr("disabled", false);
            $("#regreso").attr("disabled", false);
            $("#enviarOrdenForm").css('display','none');
            $(".cuadroFirma").css('display','none');
        }
    }
});

/*$("input[name='autorizacion_ausencia']").on('change', function() {
    if ($("input[name='autorizacion_ausencia']").is(':checked') ) {
        $(".firmaCliente").attr('hidden',true);
    }else{
        $(".firmaCliente").attr('hidden',false);
    }
});*/


//Recuperamos los datos del prellenado
function datos_llenado(serie){
    var base = $("#basePeticion").val();
    var orden = $("#orden").val();
    if (orden != "") {
        $("#cargaIcono").css("display","inline-block");

        $.ajax({
            url: base+"otros_Modulos/Renuncia_Protect/consulta_datos",
            method: 'post',
            data: {
                orden: orden,
            },
            success:function(resp){
                //console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    var respuesta = resp.split("=");
                    console.log(respuesta);
                    //Si existe el registro, recargamos con la informacion
                    if (respuesta[0] != "ALTA") {
                        //Redireccionamos al formulario de alta
                        location.href = base+"CartaRenuncia/"+respuesta[1];

                    //De lo contrario, precargamos los datos de llenado
                    }else{
                        //Validamos la existencia de la orden parte 1
                        if (respuesta[(respuesta.length - 1)] == "1") {
                            var orden_campo = $("input[name='id_orden']").val();
                            if ((orden_campo == "0")||(orden_campo == "")) {
                                $("input[name='serie']").val(respuesta[1]);
                                $("input[name='folio_externo']").val(respuesta[2]);
                                $("input[name='modelo']").val(respuesta[3]);
                                $("input[name='placas']").val(respuesta[4]);
                                $("input[name='uen']").val(respuesta[5]);
                                $("input[name='tecnico']").val(respuesta[6]);
                                $("input[name='nombre_cliente']").val(respuesta[7]);
                                $("input[name='nombre_asesor']").val(respuesta[8]);

                                //$("#autorizacion").attr('hidden',false);

                                //Si existe, marcamos los check previos
                                /*if (respuesta[9] != "0") {
                                    $("#autorizacion_ausencia").prop('checked', true);
                                    $("#motivo_"+respuesta[10]).prop('checked', true);

                                    $("#autorizacion_ausencia").attr('disabled',true);
                                    $(".mt").attr('disabled',true);
                                    $(".firmaCliente").attr('hidden',true);
                                } */

                                $("#formulario_error").text("");
                            }else{
                                location.href = base+"CartaRenuncia/0";
                                $("#formulario_error").text("");
                            }
                        }else{
                            $("#formulario_error").text("NO SE HA ABIERTO UNA ORDEN DE SERVICIO CON ESE NO. DE CITA");
                        }
                    }
                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                    //$("#formulario_error").text(resp);
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
            error:function(error){
                console.log(error);
                $("#cargaIcono").css("display","none");
                $("#formulario_error").text("SE PRODUJO UN ERROR EN EL ENVIO");
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
}

//Filtramos resultados de la busqueda
$("#btnBusqueda").on('click',function () {
    var campo = $('#busqueda_gral').val();

    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");

    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    if (campo != "") {
        //Validamos si existe la informacion
        var url = $("#basePeticion").val();
        try{
            $.ajax({
                url: url+"otros_Modulos/Renuncia_Protect/buscar_cartarenuncia",
                method: 'post',
                data: {
                    campo: campo,
                },
                success:function(resp){
                    console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        //identificamos la tabla a afectar
                        var tabla = $(".campos_buscar");
                        tabla.empty();

                        //Verificamos que exista un resultado
                        if (resp.length > 8) {
                            //Fraccionamos la respuesta en renglones
                            var campos = resp.split("|");
                            //console.log(campos);

                            //Recorremos los "renglones" obtenidos para partir los campos
                            var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                            for (var i = 0; i < regitro; i++) {
                                var celdas = campos[i].split("=");

                                var ver = "<a href='"+url+"CartaRenuncia_PDF/"+celdas[9]+"' class='btn btn-primary' class='btn btn-default' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
                                                "VER"+
                                            "</a>";

                                var envio = "";
                                if (celdas[11] == "0") {
                                    envio = "<a onclick='enviar_correo("+celdas[9]+")' class='btn btn-success' data-target='#AlertaModal' data-toggle='modal' style='font-size: 11px;color:white;'>"+
                                                "Enviar"+
                                            "</a>"+
                                            "<input type='hidden' id='rcb_"+celdas[9]+"' value='"+celdas[5]+"|1'>";
                                }else{
                                    envio = "<a onclick='enviar_correo("+celdas[9]+")' class='btn btn-warning' data-target='#AlertaModal' data-toggle='modal' style='font-size: 11px;color:white;'>"+
                                                "Reenviar"+
                                            "</a>"+
                                            "<input type='hidden' id='rcb_"+celdas[9]+"' value='"+celdas[5]+"|2'>";
                                }

                                //Creamos las celdas en la tabla
                                tabla.append("<tr style='font-size:11px;'>"+
                                    //Indice
                                    "<td align='center' style='vertical-align: middle;background-color:"+celdas[12]+";vertical-align: middle;'>"+
                                        (i+1)+
                                    "</td>"+
                                    //No ORDEN
                                    "<td align='center' style='vertical-align: middle;'>"+celdas[0]+"</td>"+
                                    //FOLIO
                                    //"<td align='center' style='vertical-align: middle;'>"+celdas[1]+"</td>"+
                                    //SERIE
                                    "<td align='center' style='vertical-align: middle;'>"+celdas[2]+"</td>"+
                                    //PLACAS
                                    "<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
                                    //UNIDAD
                                    "<td align='center' style='vertical-align: middle;'>"+celdas[4]+"</td>"+
                                    //CLIENTE
                                    "<td align='center' style='vertical-align: middle;'>"+celdas[5]+"</td>"+
                                    //ASESOR
                                    "<td align='center' style='vertical-align: middle;'>"+celdas[6]+"</td>"+
                                    //TÉCNICO
                                    "<td align='center' style='vertical-align: middle;'>"+celdas[7]+"</td>"+
                                    //FECHA REGISTRO
                                    "<td align='center' style='vertical-align: middle;'>"+celdas[8]+"</td>"+
                                    //ACCIONES
                                    "<td align='center' style='vertical-align: middle;'>"+
                                        ver+""+
                                    "</td>"+
                                    "<td align='center' style='vertical-align: middle;'>"+
                                        envio+""+
                                    "</td>"+
                                    "<td align='center' style='width:10%;vertical-align: middle;'>"+
                                        "<a href='"+url+"assets/ford_protect_plan_2019.pdf' class='btn btn-warning' target='_blank' style='font-size: 10px; color:white;'>FORD Protect</a>"+
                                    "</td>"+
                                "</tr>"); 
                            }
                        }else{
                            tabla.append("<tr style='font-size:14px;'>"+
                                "<td align='center' colspan='12'>No se encontraron resultados</td>"+
                            "</tr>");
                        }
                        
                    }else{
                        tabla.append("<tr style='font-size:14px;'>"+
                            "<td align='center' colspan='12'>No se encontraron resultados</td>"+
                        "</tr>");
                    }

                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    $(".campos_buscar").css("background-color","white");
                    $(".campos_buscar").css("color","black");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    $(".campos_buscar").css("background-color","white");
                    $(".campos_buscar").css("color","black");
                //Cierre del error
                }
            //Cierre del ajax
            });
        } catch (e) {
            console.log(e);
        } finally {
            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        }
    }
});

function enviar_correo(orden) {
    var info = $("#rcb_"+orden).val();
    var datos = info.split("|");

    $("#orden").val(orden);
    $("#envio").val(datos[1]);

    if (datos[1] == "1") {
        $("#respuesta_envio").text("¿Estás seguro de que quieres enviar el correo al cliente "+datos[0]+" de la orden #"+orden+" ?");
        $("#AlertaModalCuerpo").css("background-color","#c7ecc7");
        $("#AlertaModalCuerpo").css("border-color","#4cae4c");
    }else{
        $("#respuesta_envio").text("¿Estás seguro de que quieres reenviar el correo al cliente "+datos[0]+" de la orden #"+orden+" ?")
        $("#AlertaModalCuerpo").css("background-color","#f0ad4e");
        $("#AlertaModalCuerpo").css("border-color","#eea236");
    }
}

$("#enviar_email").on('click',function () {
    var base = $("#basePeticion").val();
    var orden = $("#orden").val();
    var envio = $("#envio").val();

    if (orden != "") {
        $(".cargaIcono").css("display","inline-block");

        $.ajax({
            url: base+"otros_Modulos/Renuncia_Protect/envio_email",
            method: 'post',
            data: {
                orden: orden,
                envio: envio,
            },
            success:function(resp){
                console.log(resp);

                if (resp.indexOf("handler           </p>")<1) {
                    var respuesta = resp.split("|");
                    $(".cargaIcono").css("display","none");
                    if (respuesta[0] == "OK") {
                        $(".cargaIcono").css("display","none");
                        location.reload();
                    }else{
                        $("#errorEnvioCotizacion").text(respuesta[0]+". Se envia a: "+respuesta[1]+" ; "+respuesta[2]);
                    }
                }else{
                    $("#errorEnvioCotizacion").text("Ocurrio un problema al enviar la documentación");
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});