//Se envia el formulario
$("#enviarOrdenForm").on('click', function (e){
    console.log("Se envia formulario");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#formulario").css("background-color","#ddd");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        $('input').attr('disabled',false);
        $('textarea').attr('disabled',false);

        var paqueteCotizacion = new FormData(document.getElementById('formulario'));
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"otros_Modulos/Renuncia_Protect/guardar_formulario",
            type: 'post',
            contentType: false,
            data: paqueteCotizacion,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("_");
                    if (respuesta[0] == "OK") {
                        location.href = base+"CartaRenuncia/"+respuesta[1];
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $("#formulario").css("background-color","white");
            //Cierre de success
            },
              error:function(error){
                  console.log(error);
                  //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                  $(".cargaIcono").css("display","none");
                  //Regresamos los colores de la tabla a la normalidad
                  $("#formulario").css("background-color","white");
                  //location.href = base+"Diagnostico/"+orden;
                  
              }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $(".cargaIcono").css("display","none");
        //Regresamos los colores de la tabla a la normalidad
        $("#formulario").css("background-color","white");
    }
});

function validar() {
    var retorno = true;

    if ($("input[name='autorizacion_ausencia']").is(':checked') ) {
        var campo_2 = $("#formulario input[name='motivo']:radio").is(':checked');
        if (!campo_2) {
            var error_2 = $("#error_autorizacion_ausencia");
            error_2.empty();
            error_2.append('<label class="form-text text-danger">Campo requerido</label>');
            retorno = false;
        }else{
            var error_2 = $("#error_autorizacion_ausencia");
            error_2.empty();    
        }
    }else{
        var error_2 = $("#error_autorizacion_ausencia");
        error_2.empty();  
    }

    var tipo_formulario = $("input[name='envio_formulario']").val();
    if (tipo_formulario == "1") {
        if (!$("input[name='autorizacion_ausencia']").is(':checked') ) {
            var campo_15 = $("input[name='ruta_firma_cliente']").val();
            if (campo_15 == "") {
                var error_15 = $("#ruta_firma_cliente_error");
                error_15.empty();
                $("#ruta_firma_cliente_error").css("display","inline-block");
                error_15.append("<br>");
                error_15.append('<label class="form-text text-danger">Falta firma del cliente</label>');
                retorno = false;
            }else{
                var error_15 = $("#ruta_firma_cliente_error");
                error_15.empty();
            }
        }else{
            var error_15 = $("#ruta_firma_cliente_error");
            error_15.empty();
        }
    }

    var campo_1 = $("input[name='serie']").val();
    if (campo_1 == "") {
        var error_1 = $("#error_vin");
        error_1.empty();
        $("#error_vin").css("display","inline-block");
        error_1.append("<br>");
        error_1.append('<label class="form-text text-danger">Campo incompleto</label>');
        retorno = false;
    }else{
        var error_1 = $("#error_vin");
        error_1.empty();
    }

    var campo_12 = $("input[name='ruta_firma_asesor']").val();
    if (campo_12 == "") {
        var error_12 = $("#ruta_firma_asesor_error");
        error_12.empty();
        $("#ruta_firma_asesor_error").css("display","inline-block");
        error_12.append("<br>");
        error_12.append('<label class="form-text text-danger">Falta firma del asesor</label>');
        retorno = false;
    }else{
        var error_12 = $("#ruta_firma_asesor_error");
        error_12.empty();
    }

    var campo_13 = $("input[name='nombre_asesor']").val();
    if (campo_13 == "") {
        var error_13 = $("#nombre_asesor_error");
        error_13.empty();
        $("#nombre_asesor_error").css("display","inline-block");
        error_13.append("<br>");
        error_13.append('<label class="form-text text-danger">Falta nombre del asesor</label>');
        retorno = false;
    }else{
        var error_13 = $("#nombre_asesor_error");
        error_13.empty();
    }

    var campo_14 = $("input[name='nombre_cliente']").val();
    if (campo_14 == "") {
        var error_14 = $("#nombre_cliente_error");
        error_14.empty();
        $("#nombre_cliente_error").css("display","inline-block");
        error_14.append("<br>");
        error_14.append('<label class="form-text text-danger">Falta nombre del cliente</label>');
        retorno = false;
    }else{
        var error_14 = $("#nombre_cliente_error");
        error_14.empty();
    }

    var check = $(".caracterirtica:checked").length; 
    if (check == 0) {
        $("#error_caracterirtica").text("Faltan opciones de indicar");
        retorno = false;
    }else{
        $("#error_caracterirtica").text("");
    }

    return retorno;
}


// ------------------------------------------------------------------------------
//  -----------------------------------------------------------------------------
$("#envio_diag").on('click', function (e){
    $("#cargaIcono").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault();

    var validaciones = validar2();

    if(validaciones){
        $('input').attr('disabled',false);
        $('textarea').attr('disabled',false);
        
        var paqueteDatos = new FormData(document.getElementById('formulario_cliente'));
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"otros_Modulos/Renuncia_Protect/guardar_firma", 
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("=");
                    if (respuesta[0] == "OK") {
                        location.reload();
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                    //$("#formulario_error").text(resp);
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                $("#formulario_error").text("SE PRODUJO UN ERROR EN EL ENVIO");
                //location.href = base+"index.php/inventario/index/"+orden;
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
        $("#formulario_error").text("FALTAN CAMPOS POR COMPLETAR");
    }
});

function validar2() {
    var retorno = true;

    var firma_2 = $("input[name='firma_cliente_Img']").val();
    if (firma_2 == "") {
        var firma_2 = $("#error_firma_2");
        firma_2.empty();
        firma_2.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var firma_2 = $("#error_firma_2");
        firma_2.empty();
    }

    return retorno;
}
