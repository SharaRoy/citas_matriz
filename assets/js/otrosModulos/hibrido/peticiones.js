//Peticiones ajax para validar numero de orden
function revisarOrden(idOrden) {
    //Recuperamos el numero de orden
    var orden = $("#idOrden").val();

    //Verificamos que no sea un campo vacio
    if (orden != "") {
        var url = $("#basePeticion").val();
        
        $.ajax({
            url: url+"otros_Modulos/InspeccionBS/datosVerifica",
            method: 'post',
            data: {
                orden: orden,
            },
            success:function(resp){
                // console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
                    //Identificamos la respuesta
                    //Si existe un registro con ese No. de Orden
                    if (resp == "EXISTE") {
                        $("#idOrdenAlerta").text("Ya existe un registro con ese ID.");
                    }else if (resp == "S/I") {
                        $("#idOrdenAlerta").text("No existe ningun proyecto con este ID.");
                    }else{
                        $("#idOrdenAlerta").text("");
                        var campos = resp.split("|");
                        console.log(campos);
                        if (campos.length > 4) {
                            $(".nombreCliente").val(campos[0]);
                            $("#correoCliente").val(campos[1]);
                            $("#telefonoCliente").val(campos[2]);
                            // $("#vin").val(campos[]);
                            $("#placas").val(campos[3]);
                            $("#serieVehiculo").val(campos[4]);
                            // $("#torreVehiculo").val(campos[]);
                            // $("#tipoMotor").val(campos[]);
                            // $("#kilometraje").val(campos[]);
                            // Transmision
                            // if (campos[] == "") {
                                // $("#tipoTrans_1").attr('checked','checked');
                            // }else {
                                // $("#tipoTrans_2").attr('checked','checked');
                            // }
                            $("#asesor").val(campos[5]);
                            $("#colorVehiculo").val(campos[6]);
                        }
                    }

                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }

}
