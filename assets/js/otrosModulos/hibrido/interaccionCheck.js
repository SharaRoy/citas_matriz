//Inteaccion para grupos de checks de las ordenes de servicio
//Interiores
$('.llaveroCheck').change(function(){
    if($(this).is(':checked')){
        $('.llaveroCheck').not(this).prop('checked', false);
    }
});

$('.SeguroRinesCheck').change(function(){
    if($(this).is(':checked')){
        $('.SeguroRinesCheck').not(this).prop('checked', false);
    }
});

$('.IndicadorGasolinaCheck').change(function(){
    if($(this).is(':checked')){
        $('.IndicadorGasolinaCheck').not(this).prop('checked', false);
    }
});

$('.IndicadorMantenimentoCheck').change(function(){
    if($(this).is(':checked')){
        $('.IndicadorMantenimentoCheck').not(this).prop('checked', false);
    }
});

$('.SistemaABSCheck').change(function(){
    if($(this).is(':checked')){
        $('.SistemaABSCheck').not(this).prop('checked', false);
    }
});

$('.IndicadorFrenosCheck').change(function(){
    if($(this).is(':checked')){
        $('.IndicadorFrenosCheck').not(this).prop('checked', false);
    }
});

$('.IndicadorBolsaAireCheck').change(function(){
    if($(this).is(':checked')){
        $('.IndicadorBolsaAireCheck').not(this).prop('checked', false);
    }
});

$('.IndicadorTPMSCheck').change(function(){
    if($(this).is(':checked')){
        $('.IndicadorTPMSCheck').not(this).prop('checked', false);
    }
});

$('.IndicadorBateriaCheck').change(function(){
    if($(this).is(':checked')){
        $('.IndicadorBateriaCheck').not(this).prop('checked', false);
    }
});

$('.IndicadorDeFallaCheck').change(function(){
    if($(this).is(':checked')){
        $('.IndicadorDeFallaCheck').not(this).prop('checked', false);
    }
});

$('.RociadoresCheck').change(function(){
    if($(this).is(':checked')){
        $('.RociadoresCheck').not(this).prop('checked', false);
    }
});

$('.ClaxonCheck').change(function(){
    if($(this).is(':checked')){
        $('.ClaxonCheck').not(this).prop('checked', false);
    }
});

$('.LucesDelanterasCheck').change(function(){
    if($(this).is(':checked')){
        $('.LucesDelanterasCheck').not(this).prop('checked', false);
    }
});

$('.LucesTraserasCheck').change(function(){
    if($(this).is(':checked')){
        $('.LucesTraserasCheck').not(this).prop('checked', false);
    }
});

$('.LucesStopCheck').change(function(){
    if($(this).is(':checked')){
        $('.LucesStopCheck').not(this).prop('checked', false);
    }
});

$('.CaratulasCheck').change(function(){
    if($(this).is(':checked')){
        $('.CaratulasCheck').not(this).prop('checked', false);
    }
});

$('.PantallasCheck').change(function(){
    if($(this).is(':checked')){
        $('.PantallasCheck').not(this).prop('checked', false);
    }
});

$('.AACheck').change(function(){
    if($(this).is(':checked')){
        $('.AACheck').not(this).prop('checked', false);
    }
});

$('.EncendedorCheck').change(function(){
    if($(this).is(':checked')){
        $('.EncendedorCheck').not(this).prop('checked', false);
    }
});

$('.VidriosCheck').change(function(){
    if($(this).is(':checked')){
        $('.VidriosCheck').not(this).prop('checked', false);
    }
});

$('.EspejosCheck').change(function(){
    if($(this).is(':checked')){
        $('.EspejosCheck').not(this).prop('checked', false);
    }
});

$('.SegurosEléctricosCheck').change(function(){
    if($(this).is(':checked')){
        $('.SegurosEléctricosCheck').not(this).prop('checked', false);
    }
});

$('.CDCheck').change(function(){
    if($(this).is(':checked')){
        $('.CDCheck').not(this).prop('checked', false);
    }
});

$('.VestidurasCheck').change(function(){
    if($(this).is(':checked')){
        $('.VestidurasCheck').not(this).prop('checked', false);
    }
});

$('.TapetesCheck').change(function(){
    if($(this).is(':checked')){
        $('.TapetesCheck').not(this).prop('checked', false);
    }
});

$('.OasisCheck').change(function(){
    if($(this).is(':checked')){
        $('.OasisCheck').not(this).prop('checked', false);
    }
});

//Cajuela
$('.HerramientaCheck').change(function(){
    if($(this).is(':checked')){
        $('.HerramientaCheck').not(this).prop('checked', false);
    }
});

$('.eLlaveCheck').change(function(){
    if($(this).is(':checked')){
        $('.eLlaveCheck').not(this).prop('checked', false);
    }
});

$('.ReflejantesCheck').change(function(){
    if($(this).is(':checked')){
        $('.ReflejantesCheck').not(this).prop('checked', false);
    }
});

$('.CablesCheck').change(function(){
    if($(this).is(':checked')){
        $('.CablesCheck').not(this).prop('checked', false);
    }
});

$('.ExtintorCheck').change(function(){
    if($(this).is(':checked')){
        $('.ExtintorCheck').not(this).prop('checked', false);
    }
});

$('.LlantaRefaccionCheck').change(function(){
    if($(this).is(':checked')){
        $('.LlantaRefaccionCheck').not(this).prop('checked', false);
    }
});

//Exteriores
$('.TaponesRuedaCheck').change(function(){
    if($(this).is(':checked')){
        $('.TaponesRuedaCheck').not(this).prop('checked', false);
    }
});

$('.GotasCheck').change(function(){
    if($(this).is(':checked')){
        $('.GotasCheck').not(this).prop('checked', false);
    }
});

$('.AntenaCheck').change(function(){
    if($(this).is(':checked')){
        $('.AntenaCheck').not(this).prop('checked', false);
    }
});

$('.TaponGasolinaCheck').change(function(){
    if($(this).is(':checked')){
        $('.TaponGasolinaCheck').not(this).prop('checked', false);
    }
});

//Checks de la tabla inferior
$('.inferior_1').change(function(){
    if($(this).is(':checked')){
        $('.inferior_1').not(this).prop('checked', false);
    }
});

$('.inferior_2').change(function(){
    if($(this).is(':checked')){
        $('.inferior_2').not(this).prop('checked', false);
    }
});

$('.inferior_3').change(function(){
    if($(this).is(':checked')){
        $('.inferior_3').not(this).prop('checked', false);
    }
});

$('.inferior_4').change(function(){
    if($(this).is(':checked')){
        $('.inferior_4').not(this).prop('checked', false);
    }
});

$('.inferior_5').change(function(){
    if($(this).is(':checked')){
        $('.inferior_5').not(this).prop('checked', false);
    }
});

$('.inferior_6').change(function(){
    if($(this).is(':checked')){
        $('.inferior_6').not(this).prop('checked', false);
    }
});

$('.inferior_7').change(function(){
    if($(this).is(':checked')){
        $('.inferior_7').not(this).prop('checked', false);
    }
});

//Interacciones checks para sistema de frenos
$('.ruedaT1').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT1').not(this).prop('checked', false);
    }
});

$('.ruedaT2').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT2').not(this).prop('checked', false);
    }
});

$('.ruedaT3').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT3').not(this).prop('checked', false);
    }
});

$('.ruedaT4').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT4').not(this).prop('checked', false);
    }
});

$('.ruedaT5').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT5').not(this).prop('checked', false);
    }
});

$('.ruedaT6').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT6').not(this).prop('checked', false);
    }
});

$('.ruedaT7').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT7').not(this).prop('checked', false);
    }
});

$('.ruedaT8').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT8').not(this).prop('checked', false);
    }
});

$('.ruedaT9').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT9').not(this).prop('checked', false);
    }
});

$('.ruedaT10').change(function(){
    if($(this).is(':checked')){
        $('.ruedaT10').not(this).prop('checked', false);
    }
});

//Interacciones checks cofre
$('.cofre_1').change(function(){
    if($(this).is(':checked')){
        $('.cofre_1').not(this).prop('checked', false);
    }
});

$('.cofre_2').change(function(){
    if($(this).is(':checked')){
        $('.cofre_2').not(this).prop('checked', false);
    }
});

$('.cofre_3').change(function(){
    if($(this).is(':checked')){
        $('.cofre_3').not(this).prop('checked', false);
    }
});

$('.cofre_4').change(function(){
    if($(this).is(':checked')){
        $('.cofre_4').not(this).prop('checked', false);
    }
});

$('.cofre_5').change(function(){
    if($(this).is(':checked')){
        $('.cofre_5').not(this).prop('checked', false);
    }
});
