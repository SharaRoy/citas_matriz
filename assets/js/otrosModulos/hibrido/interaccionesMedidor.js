// Configuracion general
$('.centradoBarra').slider();
$("#medidor_1").css('background', '#ffffff');
$("#medidor_1").css('height', '11px');


if (($("#respuesta").val() == "")&&($("#cargaFormulario").val() == "1")) {
    $('.centradoBarra').slider('disable');
} else if (($("#formulario").val() == "Revisión")&&($("#cargaFormulario").val() == "1")) {
    $('.centradoBarra').slider('disable');
} else {
    $('.centradoBarra').slider('enable');
}

//Condiciones ambientales
$("#medidor_1").slider({
    range: "min",
    animate: true,
    min: -10,
    max: 60,
    value:( $("#tempAmbiente").val() != "" ) ? parseInt($("#tempAmbiente").val()) : 0,
    slide: function(event, ui) {
       // console.log(ui.value);
        $('#tempAmbiente').val(ui.value);
    }
});

$("#medidor_2").empty().slider({
    range: "min",
    animate: true,
    min: 1,
    max: 100,
    value:( $("#humedadVal").val() != "" ) ? parseInt($("#humedadVal").val()) : 1,
    slide: function(event, ui) {
        if ((ui.value >= 1)&&(ui.value <= 20)) {
            $('#humedad').val("Seco");
        }else if ((ui.value >20)&&(ui.value <= 40)) {
            $('#humedad').val("Húmedo");
        }else if ((ui.value > 40)&&(ui.value <= 60)) {
            $('#humedad').val("Mojado");
        }else if ((ui.value > 80)&&(ui.value <= 80)) {
            $('#humedad').val("Lluvia");
        }else {
            $('#humedad').val("Hielo");
        }
        $('#humedadVal').val(ui.value);
    }
});

$("#medidor_3").empty().slider({
    range: "min",
    animate: true,
    min: 1,
    max: 120,
    value:( $("#vientoVal").val() != "" ) ? parseInt($("#vientoVal").val()) : 1,
    slide: function(event, ui) {
       // console.log(ui.value);
        if ((ui.value >= 0)&&(ui.value < 40)) {
            $('#viento').val("Ligero");
        }else if ((ui.value >= 40)&&(ui.value <= 80)) {
            $('#viento').val("Medio");
        }else {
            $('#viento').val("Fuerte");
        }
        $('#vientoVal').val(ui.value);
    }
});

//Condiciones operativas
$("#medidor_4").empty().slider({
    range: "min",
    animate: true,
    min: 0,
    max: 260,
    value:( $("#frenado").val() != "" ) ? parseInt($("#frenado").val()) : 1,
    slide: function(event, ui) {
       // console.log(ui.value);
        $('#frenado').val(ui.value);
    }
});

$("#medidor_5").empty().slider({
    range: "min",
    animate: true,
    min: 1,
    max: 70,
    value:( $("#cambioVal").val() != "" ) ? parseInt($("#cambioVal").val()) : 1,
    slide: function(event, ui) {
       // console.log(ui.value);
        if ((ui.value >= 0)&&(ui.value < 10)) {
            $('#cambio').val("R");
        }else if ((ui.value >= 10)&&(ui.value <= 20)) {
            $('#cambio').val("1");
        }else if ((ui.value > 20)&&(ui.value <= 30)) {
            $('#cambio').val("2");
        }else if ((ui.value > 30)&&(ui.value <= 40)) {
            $('#cambio').val("3");
        }else if ((ui.value > 40)&&(ui.value <= 50)) {
            $('#cambio').val("4");
        }else if ((ui.value > 50)&&(ui.value <= 60)) {
            $('#cambio').val("5");
        }else {
            $('#cambio').val("6");
        }
        $('#cambioVal').val(ui.value);
    }
});

$("#medidor_5_A").empty().slider({
    range: "min",
    animate: true,
    min: 1,
    max: 100,
    value:( $("#cambio_AVal").val() != "" ) ? parseInt($("#cambio_AVal").val()) : 1,
    slide: function(event, ui) {
       // console.log(ui.value);
        if ((ui.value >= 0)&&(ui.value < 20)) {
            $('#cambio_A').val("P");
        }else if ((ui.value > 20)&&(ui.value <= 40)) {
            $('#cambio_A').val("R");
        }else if ((ui.value > 40)&&(ui.value <= 60)) {
            $('#cambio_A').val("N");
        }else if ((ui.value > 60)&&(ui.value <= 80)) {
            $('#cambio_A').val("D");
        }else {
            $('#cambio_A').val("L");
        }
        $('#cambio_AVal').val(ui.value);
    }
});

$("#medidor_6").empty().slider({
    range: "min",
    animate: true,
    min: 0,
    max: 80,
    value:( $("#rpmVal").val() != "" ) ? parseInt($("#rpmVal").val()) : 1,
    slide: function(event, ui) {
        var sobra = ui.value/10;
        var resto = parseInt(sobra)+1;

        $('#rpm').val(resto);
        $('#rpmVal').val(ui.value);
    }
});

$("#medidor_7").empty().slider({
    range: "min",
    animate: true,
    min: 1,
    max: 100,
    value:( $("#carga").val() != "" ) ? parseInt($("#carga").val()) : 1,
    slide: function(event, ui) {
       // console.log(ui.value);
        $('#carga').val(ui.value);
    }
});

$("#medidor_8").empty().slider({
    range: "min",
    animate: true,
    min: 1,
    max: 90,
    value:( $("#pasajeros_2Val").val() != "" ) ? parseInt($("#pasajeros_2Val").val()) : 1,
    slide: function(event, ui) {
        // console.log(ui.value);
        var sobra = ui.value/10;
        var resto = parseInt(sobra)+1;
        $('#pasajeros_2').val(""+resto);
        $('#pasajeros_2Val').val(ui.value);
    }
});

$("#medidor_8_A").empty().slider({
    range: "min",
    animate: true,
    min: 20,
    max: 100,
    value:( $("#cajuela_2").val() != "" ) ? parseInt($("#cajuela_2").val()) : 25,
    slide: function(event, ui) {
        // console.log(ui.value);

        $('#cajuela_2').val(""+ui.value);
    }
});

//Condiciones de camino
$("#medidor_9").empty().slider({
    range: "min",
    animate: true,
    min: 0,
    max: 100,
    value:( $("#estructuraVal").val() != "" ) ? parseInt($("#estructuraVal").val()) : 1,
    slide: function(event, ui) {
        // console.log(ui.value);
        if ((ui.value >= 0)&&(ui.value < 20)) {
            $('#estructura').val("Plano");
        }else if ((ui.value > 20)&&(ui.value <= 40)) {
            $('#estructura').val("Vado");
        }else if ((ui.value > 40)&&(ui.value <= 60)) {
            $('#estructura').val("Tope");
        }else if ((ui.value > 60)&&(ui.value <= 80)) {
            $('#estructura').val("Baches");
        }else {
            $('#estructura').val("Vibradores");
        }
        $('#estructuraVal').val(ui.value);
    }
});

$("#medidor_10").empty().slider({
    range: "min",
    animate: true,
    min: 0,
    max: 80,
    value:( $("#caminoVal").val() != "" ) ? parseInt($("#caminoVal").val()) : 1,
    slide: function(event, ui) {
       // console.log(ui.value);
        if ((ui.value >= 0)&&(ui.value <=20)) {
            $('#camino').val("Recto");
        }else if ((ui.value > 20)&&(ui.value <= 40)) {
            $('#camino').val("Curva ligera");
        }else if ((ui.value > 40)&&(ui.value <= 60)) {
            $('#camino').val("Curva cerrada");
        }else {
            $('#camino').val("Sinuoso");
        }
        $('#caminoVal').val(ui.value);
    }
});

$("#medidor_11").empty().slider({
    range: "min",
    animate: true,
    min: 0,
    max: 80,
    value:( $("#pendienteVal").val() != "" ) ? parseInt($("#pendienteVal").val()) : 1,
    slide: function(event, ui) {
       // console.log(ui.value);
       if ((ui.value >= 0)&&(ui.value <20)) {
           $('#pendiente').val("Recto");
       }else if ((ui.value > 20)&&(ui.value <= 40)) {
           $('#pendiente').val("Pendiente ligera 10°");
       }else if ((ui.value > 40)&&(ui.value <= 60)) {
           $('#pendiente').val("Pendiente media");
       }else {
           $('#pendiente').val("Montaña");
       }
       $('#pendienteVal').val(ui.value);
    }
});

$("#medidor_12").empty().slider({
    range: "min",
    animate: true,
    min: 1,
    max: 120,
    //values: [ 35, 200 ],
    value:( $("#superficieVal").val() != "" ) ? parseInt($("#superficieVal").val()) : 1,
    slide: function(event, ui) {
        // console.log(ui.value);
        if ((ui.value >= 1)&&(ui.value <= 23)) {
           $('#superficie').val("Pavimento");
        }else if ((ui.value >23)&&(ui.value <= 47)) {
           $('#superficie').val("Terracería");
        }else if ((ui.value > 47)&&(ui.value <= 70)) {
           $('#superficie').val("Empedrado");
        }else if ((ui.value > 70)&&(ui.value <= 95)) {
           $('#superficie').val("Adoquín");
        }else {
           $('#superficie').val("Fango");
        }
        $('#superficieVal').val(ui.value);
    }
});


$(".ui-widget-header").css('background-color','darkblue');
