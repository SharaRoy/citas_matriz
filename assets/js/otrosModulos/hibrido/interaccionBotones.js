$(document).ready(function() {
    //Sección de variables para audio
    var mic;
    var recorder;
    var soundFile;
    // presionar el ratón cambiará el estado de grabar, a parar y a reproducir
    var state = 0;

    if (($("#respuesta").val() == "")&&($("#cargaFormulario").val() == "1")) {
        console.log("entro");
        var estadoFormulario = false;
        $('input').attr('disabled',true);
        $('select').attr('disabled',true);
        $('textarea').attr('disabled',true);
        $('a').attr('disabled',true);
        // $('.centradoBarra').slider('disable');
        $(".cuadroFirma").attr("data-toggle", false);

    }else {
        liberar();
        $("#iniciar").attr('disabled','disabled');
        $("#terminar").attr('disabled','disabled');
    }

    if ($("#cargaFormulario").val() == "2") {
        $('#idOrden').attr('disabled',true);
    }

    $('#uploadfilesVideo').attr('disabled',false);
});

var mediaRecorder;
var audioChunks;
var actionButton = document.getElementById('iniciar');

var recipiente = document.getElementById('blockAudio');
var audioBlob;
var audioUrl;
var audio;

$("#iniciar").on('click',function(){
    //Intentamos inicializar el audio
    try {
        // var getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
        navigator.mediaDevices.getUserMedia({ audio: true })
            .then(stream => {
                mediaRecorder = new MediaRecorder(stream);
                mediaRecorder.start();

                audioChunks = [];

                mediaRecorder.addEventListener("dataavailable", event => {
                    audioChunks.push(event.data);
                });
            })
            // .then(() => new Promise(resolve => mediaRecorder.onloadedmetadata = resolve))
            .catch(e => console.log(e));

        actionButton.disabled = true;
    } catch (e) {
        console.log("No se pudo inicializsr el audio");
        console.log(e);
    } finally {
        liberar();
        $('#uploadfilesVideo').attr('disabled',true);
        $("#enviarOrdenForm").attr('disabled',true);
    }

});

function liberar(){
    $('input').attr('disabled',false);
    $('textarea').attr('disabled',false);
    $('a').attr('disabled',false);
    $('.centradoBarra').slider('enable');
    $('select').attr('disabled',false);

    //Si el formulario es de alta, permitimos la edicion de los diagramas
    if ($("#cargaFormulario").val() == "1") {
        $("#diagramaFallas").css("display","block");
        $("#diagramaTemp").css("display","none");

        $("#canvas_3").css("display","block");
        $("#diagramaTempDanio").css("display","none");
    }

    $(".cuadroFirma").attr("disabled", false);
    $(".cuadroFirma").attr("data-toggle", "modal");

}

$("#terminar").on('click',function(){
    try {
        mediaRecorder.stop();

        mediaRecorder.addEventListener("stop", () => {
            audioBlob = new Blob(audioChunks, { 'type' : 'audio/ogg; codecs=opus' });
            audioUrl = URL.createObjectURL(audioBlob);
            audio = new Audio(audioUrl);
            // audio.play();

            // console.log(audioBlob);
            // console.log(audioUrl);
            // console.log(audio);

            input(audioBlob);
          });
    } catch (e) {
        console.log("Error generando el audio");
        console.log(e);
    } finally {
        $("#enviarOrdenForm").attr('disabled',false);
        $("#terminar").attr('disabled',true);
    }
});

function input(audioBlob){
		// console.log(direccion);
		// recipiente.value = direccion;
		// $("#blockAudio").val(direccion);

		var reader = new FileReader();
		reader.readAsDataURL(audioBlob);
		reader.onloadend = function() {
			 base64data = reader.result;
			 console.log(base64data.length);
			 fraccion(base64data);
			 // recipiente.value = base64data;
			 // $("#blockAudio").val(base64data);
		}
}

function fraccion(base64data){
		// var contenedor = $("#contenedorAudio");
    var limite = base64data.length;
		var partes = limite/1000000;

		var inputs = partes.toFixed(0);
		console.log(inputs);

		var indiceIncio = 0;
		var indiceFin = 1000000
    var contador = 1;

		if (inputs>0) {
				for (var i = 1; i <= inputs; i++) {
						// contenedor.append("<input type='hidden' name='blockAudio[]' value='"+base64data.substring(indiceIncio, indiceFin)+"'>")
            $("#blockAudio_"+i).val(base64data.substring(indiceIncio, indiceFin));
						// console.log(base64data.substring(indiceIncio, indiceFin).length);
						indiceIncio += 1000000;
						indiceFin += 1000000;
            contador++;
				}
				// contenedor.append("<input type='hidden' name='blockAudio[]' value='"+base64data.substring(indiceIncio, limite)+"'>")
        $("#blockAudio_"+contador).val(base64data.substring(indiceIncio, limite));
				// console.log(base64data.substring(indiceIncio, limite).length);
		}else {
				// contenedor.append("<input type='hidden' name='blockAudio[]' value='"+base64data.substring(indiceIncio, limite)+"'>")
        $("#blockAudio_1").val(base64data.substring(indiceIncio, limite));
				// console.log(base64data.substring(indiceIncio, limite).length);
		}
}

//Nombre del cliente
function duplicado (){
    var escritura = $('#contacto').val();
    $("#otroClienteN").val(escritura);
}
