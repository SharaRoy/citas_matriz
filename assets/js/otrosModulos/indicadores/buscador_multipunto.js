//Limpiamos la busqueda y recuperamos la lista original
$("#limpiarBusqueda").on('click',function(){
    $("#fecha_inicio").val("");
    $("#fecha_fin").val("");
    location.reload();
});

//Condicionamos las busquedas por fecha 
$('#fecha_inicio').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_fin").attr('min',fecha_inicio);
    //console.log(fecha_inicio)
});

$('#fecha_fin').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_inicio").attr('max',fecha_fin);
    //console.log(fecha_fin)
});


$('#busqueda').on('click',function(){
    console.log("Buscando por fechas");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_campo").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var url = $("#basePeticion").val().trim();
    
    $("#errorEnvioCotizacion").text("");

    $.ajax({
        url: url+"multipunto/IndicadoresProactivo/buscador_multipunto",
        method: 'post',
        data: {
            fecha_inicio: fecha_inicio,
            fecha_fin: fecha_fin,
            campo:campo,
        },
        success:function(resp){
            // console.log(resp.length);
            //console.log(resp);

            //identificamos la tabla a afectar
            var tabla = $(".campos_buscar");
            tabla.empty();

            if ((resp.indexOf("handler			</p>") < 1)&&(resp.length > 10)) {
                //Fraccionamos la respuesta en renglones
                var campos = resp.split("|");

                //Recorremos los "renglones" obtenidos para partir los campos
                var registro = ( campos.length > 1) ? campos.length -1 : campos.length;

                for (var i = 0; i < registro; i++) {
                    var celdas = campos[i].split("="); 
                    //console.log(celdas);
                    if (celdas.length>13) {
                        //Creamos las celdas en la tabla
                        var camp1 = (celdas[9] == 'V') ? '#c7ecc7' : ((celdas[9] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp2 = (celdas[10] == 'V') ? '#c7ecc7' : ((celdas[10] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp3 = (celdas[11] == 'V') ? '#c7ecc7' : ((celdas[11] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp4 = (celdas[12] == 'V') ? '#c7ecc7' : ((celdas[12] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp5 = (celdas[13] == 'V') ? '#c7ecc7' : ((celdas[13] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp6 = (celdas[14] == 'V') ? '#c7ecc7' : ((celdas[14] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp7 = (celdas[15] == 'V') ? '#c7ecc7' : ((celdas[15] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp8 = (celdas[16] == 'V') ? '#c7ecc7' : ((celdas[16] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp9 = (celdas[17] == 'V') ? '#c7ecc7' : ((celdas[17] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp10 = (celdas[18] == 'V') ? '#c7ecc7' : ((celdas[18] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp11 = (celdas[19] == 'V') ? '#c7ecc7' : ((celdas[19] == 'A') ? '#f5ff51' : '#f5acaa');

                        var campA = (celdas[20] == 'V') ? '#c7ecc7' : ((celdas[20] == 'A') ? '#f5ff51' : '#f5acaa');
                        var campB = (celdas[21] == 'V') ? '#c7ecc7' : ((celdas[21] == 'A') ? '#f5ff51' : '#f5acaa');
                        var campC = (celdas[22] == 'V') ? '#c7ecc7' : ((celdas[19] == 'A') ? '#f5ff51' : '#f5acaa');
                        var campD = (celdas[23] == 'V') ? '#c7ecc7' : ((celdas[20] == 'A') ? '#f5ff51' : '#f5acaa');

                        //Campos de pieza cambiadas
                        var indicador1 = (celdas[24] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador2 = (celdas[25] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador3 = (celdas[26] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador4 = (celdas[27] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador5 = (celdas[28] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador6 = (celdas[29] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador7 = (celdas[30] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador8 = (celdas[31] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador9 = (celdas[32] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador10 = (celdas[33] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador11 = (celdas[34] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador12 = (celdas[35] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador13 = (celdas[36] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador14 = (celdas[37] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador15 = (celdas[38] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';

                        var base_cita = $("#baseCitas").val();
                        var urlCita = 'parent.window.location="'+base_cita+'citas/agendar_cita/'+celdas[0]+'/0/0/0/1"';
                        
                        tabla.append("<tr style=''>"+
                            "<td align='center' style='font-size: 9px;font-weight: initial;vertical-align:middle;background-color: white;'>"+
                                celdas[0].toUpperCase()+
                            "</td>"+
                            "<td align='center' style='font-size: 9px;font-weight: initial;vertical-align:middle;background-color: white;'>"+
                                celdas[1].toUpperCase()+
                            "</td>"+
                            "<td align='center' style='font-size: 9px;font-weight: initial;vertical-align:middle;background-color: white;'>"+
                                celdas[2].toUpperCase()+
                            "</td>"+
                            "<td align='center' style='font-size: 9px;font-weight: initial;vertical-align:middle;background-color: white;'>"+
                                celdas[3].toUpperCase()+
                            "</td>"+
                            "<td align='center' style='font-size: 9px;font-weight: initial;vertical-align:middle;background-color: white;'>"+
                                celdas[4].toUpperCase()+
                            "</td>"+
                            "<td align='center' style='font-size: 9px;font-weight: initial;vertical-align:middle;background-color: white;'>"+
                                celdas[5].toUpperCase()+
                            "</td>"+
                            "<td align='center' style='font-size: 9px;font-weight: initial;vertical-align:middle;background-color: white;'>"+
                                celdas[6].toUpperCase()+
                            "</td>"+
                            "<td align='center' style='font-weight: initial;padding:0px;vertical-align:middle;background-color: white;'>"+
                                celdas[7].toUpperCase()+
                            "</td>"+

                            // Indicadores con color
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp1+";'>"+indicador1+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp2+";'>"+indicador2+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp3+";'>"+indicador3+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp4+";'>"+indicador4+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp5+";'>"+indicador5+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp6+";'>"+indicador6+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp7+";'>"+indicador7+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp8+";'>"+indicador8+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp9+";'>"+indicador9+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp10+";'>"+indicador10+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp11+";'>"+indicador11+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+campA+";'>"+indicador12+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+campB+";'>"+indicador13+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+campC+";'>"+indicador14+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+campD+";'>"+indicador15+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-weight: initial;font-size: 8px;'>"+
                                "<a href='"+url+'Multipunto_PDF/'+celdas[39]+"' class='btn btn-info' target='_blank' style='font-size: 11px;'>PDF</a>"+
                            "</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;background-color:white;'>"+
                                "<a href='javascript: void(0);' title='Agregar Cita' onclick='"+urlCita+"' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-plus'></i>"+
                                "</a>"+
                            "</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;background-color:white;'>"+
                                "<a class='addComentarioBtn' onclick='btnAddClick("+celdas[8]+")' title='Agregar Comentario' data-id='"+celdas[8]+"' data-target='#addComentario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-comments'></i>"+
                                "</a>"+
                            "</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;background-color:white;'>"+
                                "<a class='historialCometarioBtn' onclick='btnHistorialClick("+celdas[8]+")' title='Historial comentarios' data-id='"+celdas[8]+"' data-target='#historialCometario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-info'></i>"+
                                "</a>"+
                            "</td>"+
                        "</tr>");
                    }

                }

                //Limpiamos el input
                // $("#busqueda_tabla").val("");

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#campos_buscar").css("color","black");
            }else{
                tabla.append("<tr style='font-size:13px;'>"+
                    "<td align='center' colspan='17' style='width:100%;'>No se encontraron registros de la busqueda</td>"+
                    "</td>"+
                "</tr>");

                // console.log(resp);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#campos_buscar").css("color","black");
                $("#campos_buscar").css("background-color","white");

            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            $("#campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Modales para los comentarios (agregar)
function btnAddClick(idMagic){
    // var idMagic = $(this).data("id");
    var hoy = new Date();
    var hora = hoy.getHours()+":"+hoy.getMinutes()
    console.log("Id de Magic: "+idMagic);
    $("#idMagigAdd").val(idMagic);
    $("#titulo").val("");
    $("#HoraComentario").val(hora);
    $("#comentario").val("");
}

//Enviamos el comentario por ajax
$("#btnEnviarComentario").on('click',function(){
    //Recuperamos los valores para enviar
    var idHistorial = $("#idMagigAdd").val();
    var titulo = $("#titulo").val();
    var hora = $("#HoraComentario").val();
    var fecha = $("#fechaComentario").val();
    var comentario = $("#comentario").val();

    //Hacemos la peticion ajax de la busqueda
    var url = $("#basePeticion").val();
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");

    $.ajax({
        url: url+"multipunto/ProactivoAnalisisHistorial/comentarioMagic", 
        method: 'post',
        data: {
            idHistorial : idHistorial,
            titulo : titulo,
            hora : hora,
            fecha : fecha,
            comentario : comentario,
        },
        success:function(resp){
            console.log(resp);

            if (resp.indexOf("handler			</p>") < 1) {
                if (resp == "OK") {
                    $('#OkResult').fadeIn(1000);
                    $('#OkResult').fadeOut(2000);
                }else {
                    $('errorResult').fadeIn(1000);
                    $('errorResult').fadeOut(2000);
                }
            }
            $(".cargaIcono").css("display","none");
            limpiar();
        //Cierre de success
        },
        error:function(error){
            console.log(error);
            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Limpiamos modal de comentario
function limpiar(){
    var hoy = new Date();
    var hora = hoy.getHours()+":"+hoy.getMinutes();
    $("#titulo").val("");
    $("#HoraComentario").val(hora);
    $("#comentario").val("");
};

//Modales para los comentarios (historial)
// $(".historialCometarioBtn").on('click',function(){
function btnHistorialClick(idHistorial){
    // var idMagic = $(this).data("id");
    console.log("Id de Magic Historia: "+idHistorial);
    $("#idMagigHistorial").val(idHistorial);

    //Enviamos el id de magic para recuperar los comentarios
    //Hacemos la peticion ajax de la busqueda
    var url = $("#basePeticion").val();

    $.ajax({
        url: url+"multipunto/ProactivoAnalisisHistorial/loadHistoryMagic",
        method: 'post',
        data: {
            idHistorial : idHistorial,
        },
        success:function(resp){
            // console.log(resp);

            //identificamos la tabla a afectar
            var tabla = $("#tablaComnetario");
            tabla.empty();

            if ((resp.indexOf("handler			</p>") < 1)&&(resp.length > 10)) {
                //Fraccionamos la respuesta en renglones
                var campos = resp.split("|");

                //Recorremos los "renglones" obtenidos para partir los campos
                var registro = ( campos.length > 1) ? campos.length -1 : campos.length;

                for (var i = 0; i < registro; i++) {
                    var celdas = campos[i].split("_");
                    // console.log(celdas);
                    if (celdas.length>2) {
                        tabla.append("<tr style='font-size:13px;'>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 25%;font-family: Arial;'>"+celdas[0].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 50%;font-family: Arial;'>"+celdas[1].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 25%;font-family: Arial;'>"+celdas[2].toUpperCase()+"</td>"+
                        "</tr>");
                    }

                }
            }else{
                tabla.append("<tr style='font-size:13px;'>"+
                    "<td align='center' colspan='3' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 100%;font-family: Arial;'>No se encontraron registros de la busqueda</td>"+
                    "</td>"+
                "</tr>");
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}