//Alta/Edicion de refacciones
//Liberamos los campos cuando se envie la cotizacion
$("#btn_envio").on('click', function (e){
  //Si se hizo bien la conexión, mostramos el simbolo de carga
  $(".cargaIcono").css("display","inline-block");
  //Aparentamos la carga de la tabla
  $("#servicio_valet").css("background-color","#ddd");

  // Evitamos que salte el enlace.
  e.preventDefault(); 

  var validarReglas = validar_formulario();

  //Si el formulario procedio correctamente
  if (validarReglas == 1) {
      //Si las validaciones estan correctas, desbloqueamos los campos para el envio
      $("input").attr('disabled',false);
      $("textarea").attr('disabled',false);
      //$("select").attr('disabled',false);

      $("#servicio_valet").css("background-color","#eee");

      /* Creamos un nuevo objeto FormData. Este sustituye al atributo enctype = "multipart/form-data" que, tradicionalmente, se incluía en los formularios (y que aún se incluye, cuando son enviados 
      desde HTML. */
      var paqueteEnvio = new FormData(document.getElementById('servicio_valet'));
      //paqueteEnvio.append('aceptarCotizacion', $(this).prop('value')); 
      //console.log(paqueteEnvio);

      /* Se envia el paquete de datos por ajax. */
      var base = $("#basePeticion").val().trim();

      $.ajax({
          url: base+"otros_Modulos/Servicio_Valet/recepcion_formulario",
          type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
          contentType: false,
          data: paqueteEnvio, // Al atributo data se le asigna el objeto FormData.
          processData: false,
          cache: false,
          success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
              var respuestas = resp.split("_");
              //Si el registro se guardo correctamente, redireccionamos a la vista de no edicion
                if (respuestas[0] == "OK") {
                  //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                  $(".cargaIcono").css("display","none");
                  //Regresamos los colores de la tabla a la normalidad
                  $("#servicio_valet").css("background-color","white");
                  //location.href = base+"MPM_VWRevicionMP1/"+respuestas[1];
                  location.href = base+"Panel_Asesor/1";

                //De lo contrario, mandamos un mensaje
                } else {
                  //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                  $(".cargaIcono").css("display","none");
                  //Regresamos los colores de la tabla a la normalidad
                  $("#servicio_valet").css("background-color","white");
                  $("#errorEnvio").text(respuestas[0])
                }
            }else{
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $(".cargaIcono").css("display","none");
              //Regresamos los colores de la tabla a la normalidad
              $("#servicio_valet").css("background-color","white");
              $("#errorEnvio").text("ERROR AL ENVIAR LA INFORMACIÓN");
            }
      
          //Cierre de success
          },
          error:function(error){
              console.log(error);
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $(".cargaIcono").css("display","none");
              //Regresamos los colores de la tabla a la normalidad
              $("#servicio_valet").css("background-color","white");
              $("#errorEnvio").text("ERROR 02 AL ENVIAR LA INFORMACIÓN");
          }
       });
  //De lo contrario regresamos los errores
  }else{
      //Regresamos los colores de la tabla a la normalidad
      $("#servicio_valet").css("background-color","white");
      //Si ya se termino de cargar la tabla, ocultamos el icono de carga
      $(".cargaIcono").css("display","none");
  }
});


function validar_formulario() {
    $validar = 1;
    //Validamos los campos del formulario

    if ($("#marca").val() == "") {
        $("#error_marca").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_marca").text("");
    }

    //Validamos los campos del formulario
    if ($("#modelo").val() == "") {
        $("#error_modelo").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_modelo").text("");
    }

    //Validamos los campos del formulario
    if ($("#color").val() == "") {
        $("#error_color").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_color").text("");
    }

    //Validamos los campos del formulario
    if ($("#serie").val() == "") {
        $("#error_serie").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_serie").text("");
    }

    if ($("#placas").val() == "") {
        $("#error_placas").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_placas").text("");
    }

    if ($("#aseguradora").val() == "") {
        $("#error_aseguradora").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_aseguradora").text("");
    }

    if ($("#poliza").val() == "") {
        $("#error_poliza").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_poliza").text("");
    }

    if ($("#contratante").val() == "") {
        $("#error_contratante").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_contratante").text("");
    }

    if ($("#fecha_vencimiento").val() == "") {
        $("#error_fecha_vencimiento").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_fecha_vencimiento").text("");
    }


    if (!validar_email( $("#correo").val() )) {
        $("#error_correo").text("Campo requerido o incorrecto");
        $validar = 0;
    }else{
        $("#error_correo").text("");
    }

    if ($("#whatsp").val() == "") {
        $("#error_whatsp").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_whatsp").text("");
    }

    if (($("#rutaFirmaConsumidor").val() == "")||($("#nombre_clienteB").val() == "")) {
        $("#error_nombrecliente").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_nombrecliente").text("");
    }

    return $validar;
}

function validar_email( email ) 
{
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}