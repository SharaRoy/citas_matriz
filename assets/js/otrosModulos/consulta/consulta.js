function cliente_nombre(argument) {
	var nom = $("#nombre_cliente").val();
	$("#nombre_clienteB").val(nom);

}

//Validamos la existencia del formulario
function revision_valet() {
    var cita_ingresada = $('#id_cita').val();
    var cita_actual = $('#cita_Act').val();

    if (cita_ingresada != cita_actual) {
        //Validamos si existe la informacion
        var url = $("#basePeticion").val();
        try{
            $.ajax({
                url: url+"otros_Modulos/Servicio_Valet/revision_registro",
                method: 'post',
                data: {
                    cita_ingresada: cita_ingresada,
                },
                success:function(resp){
                    //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {

                        if (resp != "0") {
                            location.href = url+"Servicio_valet_PDF/"+resp;
                        }else{
                        	//rellenado();
                        }
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        } catch (e) {
            console.log(e);
        } finally {

        }
    }
}

//Cargamos los datos a visualizar en la cotización
function rellenado() {
	var serie = $("#serie").val();
	var placas = $("#placas").val();

    var url = $("#basePeticion").val();
    $.ajax({
        url: url+"otros_Modulos/Servicio_Valet/carga_datos",
        method: 'post',
        data: {
            serie: serie,
            placas: placas,
        },
        success:function(resp){
            console.log(resp.length);
            if (resp.indexOf("handler			</p>")<1) {

                var campos = resp.split("=");
                //console.log(campos);
                if (campos.length > 2) {

                	//verificamos que no se hayan rellenado ya los datos
                	if ($("#nombre_cliente").val() == "") {
                		$("#nombre_cliente").val(campos[0]);
	                    $("#nombre_clienteB").val(campos[0]);

	                    $("#marca").val(campos[1]);
	                    $("#modelo").val(campos[2]);
	                    
	                    $("#serie").val(campos[3]);
	                    $("#placas").val(campos[4]);
	                    $("#color").val(campos[5]);

	                    $("#correo").val(campos[6]);
	                    $("#whatsp").val(campos[8]);
                	}
                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

//Filtramos resultados de la busqueda
$("#btnBusqueda").on('click',function () {
	var campo = $('#busqueda_gral').val();

	//Si se hizo bien la conexión, mostramos el simbolo de carga
	$(".cargaIcono").css("display","inline-block");

	//Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    if (campo != "") {
        //Validamos si existe la informacion
        var url = $("#basePeticion").val();
        try{
            $.ajax({
                url: url+"otros_Modulos/Servicio_Valet/buscar_registro",
                method: 'post',
                data: {
                    campo: campo,
                },
                success:function(resp){
                    //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                    	//identificamos la tabla a afectar
		                var tabla = $(".campos_buscar");
		                tabla.empty();

		                //Verificamos que exista un resultado
		                if (resp.length > 8) {
		                    //Fraccionamos la respuesta en renglones
		                    var campos = resp.split("|");
		                    //console.log(campos);

		                    //Recorremos los "renglones" obtenidos para partir los campos
		                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
		                    for (var i = 0; i < regitro; i++) {
		                        var celdas = campos[i].split("="); 
		                        //console.log(celdas);
		                        
		                        var asignar = "<a class='btn btn-primary' onclick='asignar_orden("+celdas[0]+")' style='color:white;' data-id='"+celdas[0]+"' data-target='#asignar'  data-toggle='modal'>"+
                                                "Asignar No. de Orden"+
                                            "</a>";

                                //Creamos las celdas en la tabla
		                        tabla.append("<tr style='font-size:11px;'>"+
		                            //Indice
		                            "<td align='center' style='vertical-align: middle;'>"+(i+1)+"</td>"+
		                            //CLIENTE
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[1]+"</td>"+
		                            //FECHA SOLICITUD
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[2]+"</td>"+
		                            //PLACAS
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
		                            //SERIE
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[4]+"</td>"+
		                            //MODELO
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[5]+"</td>"+
		                            //ACCIONES
		                            "<td align='center' style='vertical-align: middle;'>"+
		                                asignar+""+
		                            "</td>"+
		                        "</tr>");
		                    }
		                }else{
		                	tabla.append("<tr style='font-size:14px;'>"+
		                        "<td align='center' colspan='7'>No se encontraron resultados</td>"+
		                    "</tr>");
		                }
                        
                    }else{
                    	tabla.append("<tr style='font-size:14px;'>"+
	                        "<td align='center' colspan='7'>No se encontraron resultados</td>"+
	                    "</tr>");
                    }

                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
		            $(".cargaIcono").css("display","none");
		            //Regresamos los colores de la tabla a la normalidad
		            $(".campos_buscar").css("background-color","white");
		            $(".campos_buscar").css("color","black");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
		            $(".cargaIcono").css("display","none");
		            //Regresamos los colores de la tabla a la normalidad
		            $(".campos_buscar").css("background-color","white");
		            $(".campos_buscar").css("color","black");
                //Cierre del error
                }
            //Cierre del ajax
            });
        } catch (e) {
            console.log(e);
        } finally {
        	//Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        }
    }
});

//Filtramos resultados de la busqueda
$("#btnBusqueda_asig").on('click',function () {
	var campo = $('#busqueda_gral').val();

	//Si se hizo bien la conexión, mostramos el simbolo de carga
	$(".cargaIcono").css("display","inline-block");

	//Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    if (campo != "") {
        //Validamos si existe la informacion
        var url = $("#basePeticion").val();
        try{
            $.ajax({
                url: url+"otros_Modulos/Servicio_Valet/buscar_registro_2",
                method: 'post',
                data: {
                    campo: campo,
                },
                success:function(resp){
                    //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                    	//identificamos la tabla a afectar
		                var tabla = $(".campos_buscar");
		                tabla.empty();

		                //Verificamos que exista un resultado
		                if (resp.length > 8) {
		                    //Fraccionamos la respuesta en renglones
		                    var campos = resp.split("~");
		                    //console.log(campos);

		                    //Recorremos los "renglones" obtenidos para partir los campos
		                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
		                    for (var i = 0; i < regitro; i++) {
		                        var celdas = campos[i].split("="); 
		                        //console.log(celdas);
		                        //
		                        var archivos = celdas[7].split("|");
	                            var conteo = 1;
	                            var btnArchivos = "";
	                            for (var j = 0; j < archivos.length; j++) {
	                                if (archivos[j] != "") {
	                                    btnArchivos += "<a href='"+url+""+archivos[j]+"' class='btn btn-primary' target='_blank' style='font-size: 9px;margin-top:5px;'>"+
	                                        "Doc.("+conteo+")"+
	                                    "</a>";
	                                    conteo++;
	                                }
	                            }

	                            if (conteo == 1) {
	                            	btnArchivos = "<button type='button' class='btn btn-default' style='font-size: 10px;'>SIN <br> ARCHIVO</button>";
	                            }

		                        var ver = "<a href='"+url+"Servicio_valet_PDF/"+celdas[8]+"'  class='btn btn-default' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
	                                        "VER"+
	                                    "</a>";

                                //Creamos las celdas en la tabla
		                        tabla.append("<tr style='font-size:11px;'>"+
		                            //Indice
		                            "<td align='center' style='vertical-align: middle;'>"+(i+1)+"</td>"+
		                            //ID ORDEN
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[6]+"</td>"+
		                            //CLIENTE
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[1]+"</td>"+
		                            //FECHA SOLICITUD
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[2]+"</td>"+
		                            //PLACAS
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
		                            //SERIE
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[4]+"</td>"+
		                            //MODELO
		                            "<td align='center' style='vertical-align: middle;'>"+celdas[5]+"</td>"+
		                            //ACCIONES
		                            "<td align='center' style='vertical-align: middle;'>"+
		                                ver+""+
		                            "</td>"+
		                            "<td align='center' style='vertical-align: middle;'>"+
		                                btnArchivos+""+
		                            "</td>"+
		                        "</tr>");
		                    }
		                }else{
		                	tabla.append("<tr style='font-size:14px;'>"+
		                        "<td align='center' colspan='9'>No se encontraron resultados</td>"+
		                    "</tr>");
		                }
                        
                    }else{
                    	tabla.append("<tr style='font-size:14px;'>"+
	                        "<td align='center' colspan='9'>No se encontraron resultados</td>"+
	                    "</tr>");
                    }

                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
		            $(".cargaIcono").css("display","none");
		            //Regresamos los colores de la tabla a la normalidad
		            $(".campos_buscar").css("background-color","white");
		            $(".campos_buscar").css("color","black");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
		            $(".cargaIcono").css("display","none");
		            //Regresamos los colores de la tabla a la normalidad
		            $(".campos_buscar").css("background-color","white");
		            $(".campos_buscar").css("color","black");
                //Cierre del error
                }
            //Cierre del ajax
            });
        } catch (e) {
            console.log(e);
        } finally {
        	//Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        }
    }
});

//consultar informacion para asignar un registro
function asignar_orden(registro) {
	var url = $("#basePeticion").val();
	$("#id_registro").val(registro);

    $.ajax({
        url: url+"otros_Modulos/Servicio_Valet/consulta_datos",
        method: 'post',
        data: {
            registro: registro,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler			</p>")<1) {

                var campos = resp.split("=");
                //console.log(campos);
                if (campos.length > 2) {
                	//verificamos que no se hayan rellenado ya los datos
            		$("#cliente_txt").val(campos[0]);
                    $("#fecha").val(campos[1]);
                    $("#modelo_txt").val(campos[2]);
                    $("#placas_txt").val(campos[3]);
                    $("#serie_txt").val(campos[4]);
                }
            }

        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

//Asignamos el registro a la orden
$("#asignar_registro").on('click',function () {
	var url = $("#basePeticion").val();
	var registro = $("#id_registro").val();
	var id_cita = $("#id_cita").val();

	//Si se hizo bien la conexión, mostramos el simbolo de carga
	$(".cargaIcono").css("display","inline-block");

	$.ajax({
        url: url+"otros_Modulos/Servicio_Valet/asignar_orden",
        method: 'post',
        data: {
            registro: registro,
            id_cita: id_cita,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler			</p>")<1) {
            	var result = resp.split("_");

            	if (result[0] == "OK") {
            		$("#cerrar").css("display","inline-block");
        			$("#asignar_registro").css("display", "none");
        			$("#cancelar").css("display", "none");

            		$("#resultad_envio").css('color','darkgreen');
            	}else{
            		$("#resultad_envio").css('color','red');
            	}
                
                $("#resultad_envio").text(result[1]);

            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre del error
        }
    //Cierre del ajax
    });
});