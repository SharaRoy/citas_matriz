function precarga() {
    var idOrden = $("#ordenForm").val().trim();
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"otros_Modulos/Clientes_Refacciones/loadDataForm",
        method: 'post',
        data: {
            idOrden: idOrden,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {

                var campos = resp.split("|"); 
                // console.log(campos);
                if (campos.length > 3) {
                    $("#inputTecnico").val(campos[0]);
                    $("#inputAsesor").val(campos[1]);
                    $("#inputModelo").val(campos[2]);
                    $("#inputCliente").val(campos[3]);
                    $("#inputTelefono").val(campos[4]);
                    $("#inputSerie").val(campos[5]);
                    $("#inputFechaPromesa").val(campos[6]);
                    $("#inputFechaRecibe").val(campos[7]);

                    $("#inputTelefono_2").val(campos[8]);
                }

                //Si es el formulario de clientes, jalamos refacciones echazadas
                if (($("#formulario").val() == "clientes") && ($("#cargaFormulario").val() == "1")) {
                    cargar_presupuesto();
                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
      //Cierre del ajax
    });
}

// ---------------------- Cargamos refacciones rechazadas de un presupuesto (si hay) ----------------------------------- //
function cargar_presupuesto() {
    var id_cita = $("#ordenForm").val().trim();
    var base = $("#basePeticion").val().trim(); 

    $.ajax({
        url: base+"otros_Modulos/Clientes_Refacciones/cargar_datos_presupuesto",
        method: 'post',
        data: {
            id_cita: id_cita,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //Recuperamos el indice de renglones actual
                var limite_act = $("#maxRenglon").val();
                var renglon = limite_act - 1;
                var contenedor = $("#contenedor_Refacciones");

                //Verificamos si es el primer renglon o si es un consecutivo
                if (renglon == 0) {
                    contenedor.empty();
                }

                var campos = resp.split("|");
                //var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                for (var i = 0; i < campos.length; i++) {
                    var celdas = campos[i].split("=");
                    if (celdas.length > 2) {
                        renglon++;
                        limite_act++;
                        //console.log(celdas);

                        //Validamos que el renglon no este vacio
                        var id_refaccion = celdas[0].trim();
                        var cantidad = celdas[1].trim();
                        var descripcion = ((celdas[2] != "") ? celdas[2].trim() : "Sin informaciÓn");
                        var num_part = ((celdas[3] != "") ? celdas[3].trim() : ".");
                        //Creamos un nuevo renglon
                        contenedor.append(
                            "<br>"+
                            "<div class='row' id='renglon_"+renglon+"'>"+
                                "<div class='col-md-2'>"+
                                    "<input type='number' step='0.01' min='0.1' name='cantidad_"+renglon+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='"+cantidad+"'>"+
                                "</div>"+
                                "<div class='col-md-3'>"+
                                    "<input type='text' name='numParte_"+renglon+"' id='numParte_"+renglon+"' class='form-control' value='"+num_part+"'>"+
                                    "<input type='hidden' name='id_refaccion_"+renglon+"' id='id_refaccion_"+renglon+"' class='form-control' value='"+id_refaccion+"'>"+
                                "</div>"+
                                "<div class='col-md-6'>"+
                                    "<textarea  class='form-control' name='descripcionCorta_"+renglon+"' id='descripcionCorta_"+renglon+"' rows='2'>"+descripcion+"</textarea>"+
                                "</div>"+
                                //"<div class='col-md-2' align='right'></div>"+
                                "<div class='col-md-1' align='right'>"+
                                "<input type='hidden' name='' id='sub_indice_"+renglon+"' class='form-control' value='"+renglon+"'>"+
                                    "<a class='btn btn-danger' style='color:white; width: 1cm;' onclick='quitarRef_cliente("+renglon+")'>"+
                                        "<i class='fa fa-minus'></i>"+
                                    "</a>"+
                                "</div>"+
                            "</div>"
                        );

                        //limite_act++;
                        //renglon++;
                        //Aumentamos el indice del renglon
                        $("#maxRenglon").val(limite_act);
                    }
                }

                limite_act++;
                renglon++;

                //Creamos un nuevo renglon vacio
                contenedor.append(
                    "<br>"+
                    "<div class='row' id='renglon_"+renglon+"'>"+
                        "<div class='col-md-2'>"+
                            "<input type='number' step='0.01' min='1' name='cantidad_"+renglon+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='1'>"+
                        "</div>"+
                        "<div class='col-md-3'>"+
                            "<input type='text' name='numParte_"+renglon+"' id='numParte_"+renglon+"' class='form-control' value=''>"+
                            "<input type='hidden' name='id_refaccion_"+renglon+"' id='id_refaccion_"+renglon+"' class='form-control' value='0'>"+
                        "</div>"+
                        "<div class='col-md-6'>"+
                            "<textarea  class='form-control' name='descripcionCorta_"+renglon+"' id='descripcionCorta_"+renglon+"' rows='2'></textarea>"+
                        "</div>"+
                        //"<div class='col-md-2' align='right'></div>"+
                        "<div class='col-md-1' align='right'>"+
                        "<input type='hidden' name='' id='sub_indice_"+renglon+"' class='form-control' value='"+renglon+"'>"+
                            "<a class='btn btn-danger' style='color:white; width: 1cm;' onclick='quitarRef_cliente("+renglon+")'>"+
                                "<i class='fa fa-minus'></i>"+
                            "</a>"+
                        "</div>"+
                    "</div>"
                );
                
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
      //Cierre del ajax
    });
}

$('#busqueda_tabla').keyup(function () {
    toSearch();
});


//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}


//Condicionamos las busquedas por fecha 
$('#fecha_inicio').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_fin").attr('min',fecha_inicio);
    //console.log(fecha_inicio)
});

$('#fecha_fin').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_inicio").attr('max',fecha_fin);
    //console.log(fecha_fin)
});


$('#btnBusqueda').on('click',function(){ 
    console.log("Buscando spni clientes");
    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"otros_Modulos/Clientes_Refacciones/filtroPorFechas", 
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length >5) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? campos.length -1 : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("=");
                        console.log(celdas);
                        var fondo = celdas[10];
                        var color = celdas[11];
                        var base_2 = $("#basePeticion").val().trim();
                        var editar = '<input type="button" class="btn btn-info" style="color:white;" onclick="location.href='+"'"+base_2+"ClienteRF_Editar/"+celdas[8]+"'"+';" value="Editar">';
                        var base_cita = $("#baseCitas").val();

                        var urlCita = '"'+base_cita+'citas/agendar_cita/0/0/0/'+celdas[0]+'"';
                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:12px;'>"+
                            //Indice
                            "<td align='center' style='color:"+color+"; background-color:"+fondo+";'>"+(i+1)+"</td>"+
                            //Orden
                            "<td align='center' style=''>"+celdas[0]+"</td>"+
                            //Folio
                            "<td align='center' style=''>"+celdas[1]+"</td>"+
                            //Serie
                            "<td align='center' style=''>"+celdas[2]+"</td>"+
                            //Asesor
                            "<td align='center' style=''>"+celdas[4]+"</td>"+
                            //Tecnico
                            "<td align='center' style=''>"+celdas[5]+"</td>"+
                            //unidad_taller
                            "<td align='center' style=''>"+celdas[3]+"</td>"+
                            //Telefono cliente
                            "<td align='center' style=''>"+celdas[12]+"</td>"+
                            //Fecha recibe
                            "<td align='center' style=''>"+celdas[7]+"</td>"+
                            //ESTADO REFACCIONES
                            "<td align='center' style='vertical-align: middle;width:15%;"+celdas[13]+"'>"+
                                ""+celdas[14]+""
                            +"</td>"+
                            //Editar
                            "<td align='center' style='width:10%;'>"+
                                ""+editar+""+
                            "</td>"+
                            //Nueva cita
                            "<td align='center' style='width:10%;'>"+
                                "<a href='javascript: void(0);' title='Agregar Cita' onclick='parent.window.location="+urlCita+";' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-plus'></i>"+
                                "</a>"+
                            "</td>"+
                            //Comentario
                            "<td align='center' style='width:10%;'>"+
                                "<a class='addComentarioBtn' onclick='btnAddClick("+celdas[9]+")' title='Agregar Comentario' data-id='"+celdas[9]+"' data-target='#addComentario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-comments'></i>"+
                                "</a>"+
                            "</td>"+
                            //Historial comentario
                            "<td align='center' style='width:10%;'>"+
                                "<a class='historialCometarioBtn' onclick='btnHistorialClick("+celdas[9]+")' title='Historial comentarios' data-id='"+celdas[9]+"' data-target='#historialCometario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-info'></i>"+
                                "</a>"+
                            "</td>"+
                            //Quitar
                            "<td align='center' style='width:15%;'>"+
                                "<a class='eliminar' onclick='btnDeleteClick_RC("+celdas[8]+")' title='Eliminar Registro' data-id='"+celdas[8]+"' data-target='#eliminar' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-trash'></i>"+
                                "</a>"+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
});


$('#btnBusqueda_hg').on('click',function(){
    console.log("Buscando por fechas garantias");
    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"otros_Modulos/Historial_garantias/filtroPorFechas",
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length >5) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? campos.length -1 : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("=");
                        console.log(celdas);
                        var fondo = celdas[10];
                        var color = celdas[11];
                        var base_2 = $("#basePeticion").val().trim();
                        var editar = '<input type="button" class="btn btn-info" style="color:white;" onclick="location.href='+"'"+base_2+"HG_Editar/"+celdas[8]+"'"+';" value="Editar">';

                        var base_cita = $("#baseCitas").val();
                        var urlCita = '"'+base_cita+'citas/agendar_cita/0/0/0/'+celdas[0]+'"';
                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:12px;'>"+
                            //Indice
                            "<td align='center' style='color:"+color+"; background-color:"+fondo+";'>"+(i+1)+"</td>"+
                            //orden
                            "<td align='center' style=''>"+celdas[0]+"</td>"+
                            //folio
                            "<td align='center' style=''>"+celdas[1]+"</td>"+
                            //serie
                            "<td align='center' style=''>"+celdas[13]+"</td>"+
                            //cliente
                            "<td align='center' style=''>"+celdas[2]+"</td>"+
                            //asesor
                            "<td align='center' style=''>"+celdas[3]+"</td>"+
                            //tecnico
                            "<td align='center' style=''>"+celdas[4]+"</td>"+
                            //tel_cliente
                            "<td align='center' style=''>"+celdas[5]+"</td>"+
                            //fecha_recibe
                            "<td align='center' style=''>"+celdas[6]+"</td>"+
                            //fecha_promesa
                            "<td align='center' style=''>"+celdas[7]+"</td>"+
                            //Editar
                            "<td align='center' style='width:10%;'>"+
                                ""+editar+""+
                            "</td>"+
                            //Nueva cita
                            "<td align='center' style='width:10%;'>"+
                                "<a href='javascript: void(0);' title='Agregar Cita' onclick='parent.window.location="+urlCita+";' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-plus'></i>"+
                                "</a>"+
                            "</td>"+
                            //Comentario
                            "<td align='center' style='width:10%;'>"+
                                "<a class='addComentarioBtn' onclick='btnAddClick("+celdas[9]+")' title='Agregar Comentario' data-id='"+celdas[9]+"' data-target='#addComentario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-comments'></i>"+
                                "</a>"+
                            "</td>"+
                            //Historial comentario
                            "<td align='center' style='width:10%;'>"+
                                "<a class='historialCometarioBtn' onclick='btnHistorialClick("+celdas[9]+")' title='Historial comentarios' data-id='"+celdas[9]+"' data-target='#historialCometario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-info'></i>"+
                                "</a>"+
                            "</td>"+
                            //Quitar
                            "<td align='center' style='width:15%;'>"+
                                "<a class='eliminar' onclick='btnDeleteClick("+celdas[8]+")' title='Eliminar Registro' data-id='"+celdas[8]+"' data-target='#eliminar' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-trash'></i>"+
                                "</a>"+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Agregar renglones de refacciones
function agregarRef(x) {
    //Recuperamos el indice de renglones actual
    var limite_act = $("#maxRenglon").val();

    var renglon = limite_act - 1;
    
    //Validamos que el renglon no este vacio
    var num_part = $("#numParte_"+renglon).val().trim();
    var descripcion = $("#descripcionCorta_"+renglon).val().trim();

    //Si los campos estan llenos, creamos el nuevo renglon
    if (descripcion != "") {
        //identificamos el espacio a afectar
        var contenedor = $("#contenedor_Refacciones");
        //contenedor.empty();
        
        //Creamos un nuevo renglon
        contenedor.append(
            "<br>"+
            "<div class='row' id='renglon_"+limite_act+"'>"+
                "<div class='col-md-2'>"+
                    //"<label for='' style='font-size: 13px;font-weight: inherit;'>Cantidad:</label>"+
                    //"<br>"+
                    "<input type='number' step='0.01' min='1' name='cantidad_"+limite_act+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='1'>"+
                    "<input type='hidden' name='id_refaccion_"+limite_act+"' id='id_refaccion_"+limite_act+"' value='0'>"+
                    "<input type='hidden' name='id_index_"+limite_act+"' id='id_index_"+limite_act+"' value='0'>"+
                "</div>"+
                "<div class='col-md-3'>"+
                    //"<label for='' style='font-size: 13px;font-weight: inherit;'>Número de parte:</label>"+
                    //"<br>"+
                    "<input type='text' name='numParte_"+limite_act+"' id='numParte_"+limite_act+"' class='form-control' value=''>"+
                "</div>"+
                "<div class='col-md-5'>"+
                    //"<label for='' style='font-size: 13px;font-weight: inherit;'>Descripción Corta:</label>"+
                    //"<br>"+
                    "<textarea  class='form-control' name='descripcionCorta_"+limite_act+"' id='descripcionCorta_"+limite_act+"' rows='2'></textarea>"+
                "</div>"+
                //"<div class='col-md-2' align='right'></div>"+
            "</div>"
        );

        limite_act++;
        //Aumentamos el indice del renglon
        $("#maxRenglon").val(limite_act);
    
    //Si falta algun campo por llenar, mandamos el cursor a ese campo
    }else{
        if (num_part == "") {
            $("#numParte_"+renglon).focus();
        }else{
            $("#descripcionCorta_"+renglon).focus();
        }
    }
}

//Quitar reglon de refacciones
function quitarRef(renglon) {
    //Recuperamos el indice de renglones actual
    var limite_act = $("#maxRenglon").val();

    var renglon = limite_act - 1;


    //Validamos que no sea el primer renglon
    if (renglon > 0) {
        var fila = $("#renglon_"+renglon);
        fila.remove();

        //Decrementamos el indice para los renglones
        limite_act--;
        $("#maxRenglon").val(limite_act);
    }
}

//Para buscar e imprimir los cambios de estatus del historial
$('#historiaComentarios').on('click',function(){
    var id_cita = $("#historiaComentarios").data("orden");
    //console.log(id_cita);

    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"otros_Modulos/Historial_garantias/historial_cambios",
        method: 'post',
        data: {
            id_cita : id_cita,
        },
        success:function(resp){
            console.log("");
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $("#tablaComnetario");
                tabla.empty();

                var renglones = resp.split("|");

                //Recorremos los "renglones" obtenidos para partir los renglones
                var regitro = ( renglones.length > 1) ? (renglones.length -1) : renglones.length;
                for (var i = 0; i < regitro; i++) {
                    var campos = renglones[i].split("=");
                    //console.log(campos);         
                    
                    //Creamos las campos en la tabla
                    tabla.append("<tr style='font-size:15px;border-bottom:1px solid blue;'>"+
                        "<td align='center' style='vertical-align: middle;'>"+campos[0]+"</td>"+
                        "<td align='center' style='vertical-align: middle;'>"+campos[1]+"</td>"+
                        "<td align='center' style='color:"+campos[5]+"; background-color:"+campos[4]+";vertical-align: middle;'>"+campos[2]+"</td>"+
                        "<td align='center' style='vertical-align: middle;'>"+campos[3]+"</td>"+
                "</tr>");
                }
            }
        
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Para buscar e imprimir los cambios de estatus del historial
$('#historiaComentariosCliente').on('click',function(){
    var id_cita = $("#historiaComentariosCliente").data("orden");
    //console.log(id_cita);

    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"otros_Modulos/Clientes_Refacciones/historial_cambios",
        method: 'post',
        data: {
            id_cita : id_cita,
        },
        success:function(resp){
            console.log("");
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $("#tablaComnetario");
                tabla.empty();

                var renglones = resp.split("|");

                //Recorremos los "renglones" obtenidos para partir los renglones
                var regitro = ( renglones.length > 1) ? (renglones.length -1) : renglones.length;
                for (var i = 0; i < regitro; i++) {
                    var campos = renglones[i].split("=");
                    //console.log(campos);         
                    
                    //Creamos las campos en la tabla
                    tabla.append("<tr style='font-size:15px;border-bottom:1px solid blue;'>"+
                        "<td align='center' style='vertical-align: middle;'>"+campos[0]+"</td>"+
                        "<td align='center' style='vertical-align: middle;'>"+campos[1]+"</td>"+
                        "<td align='center' style='color:"+campos[5]+"; background-color:"+campos[4]+";vertical-align: middle;'>"+campos[2]+"</td>"+
                        "<td align='center' style='vertical-align: middle;'>"+campos[3]+"</td>"+
                "</tr>");
                }
            }
        
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
});


//Para el alta de refacciones de cleintes
//Agregar renglones de refacciones
function agregarRef_cliente(x) {
    //Recuperamos el indice de renglones actual
    var limite_act = $("#maxRenglon").val();

    var renglon = limite_act - 1;
    
    //Validamos que el renglon no este vacio
    var num_part = $("#numParte_"+limite_act).val();
    var descripcion = $("#descripcionCorta_"+limite_act).val();

    //Si los campos estan llenos, creamos el nuevo renglon
    if ((descripcion != "")&&(num_part != "")) {
        limite_act++;
        //identificamos el espacio a afectar
        var contenedor = $("#contenedor_Refacciones");
        //contenedor.empty();
        
        //Creamos un nuevo renglon
        contenedor.append(
            "<br>"+
            "<div class='row' id='renglon_"+limite_act+"'>"+
                "<div class='col-md-2'>"+
                    "<input type='number' step='0.01' min='1' name='cantidad_"+limite_act+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='1'>"+
                "</div>"+
                "<div class='col-md-3'>"+
                    "<input type='text' name='numParte_"+limite_act+"' id='numParte_"+limite_act+"' class='form-control' value=''>"+
                    "<input type='hidden' name='id_refaccion_"+limite_act+"' id='id_refaccion_"+limite_act+"' class='form-control' value='0'>"+
                    "<input type='hidden' name='id_index_"+limite_act+"' id='id_index_"+limite_act+"' value='0'>"+
                "</div>"+
                "<div class='col-md-6'>"+
                    "<textarea  class='form-control' name='descripcionCorta_"+limite_act+"' id='descripcionCorta_"+limite_act+"' rows='2'></textarea>"+
                "</div>"+
                //"<div class='col-md-2' align='right'></div>"+
                "<div class='col-md-1' align='right'>"+
                "<input type='hidden' name='' id='sub_indice_"+limite_act+"' class='form-control' value='"+limite_act+"'>"+
                    "<a class='btn btn-danger' style='color:white; width: 1cm;' onclick='quitarRef_cliente("+limite_act+")'>"+
                        "<i class='fa fa-minus'></i>"+
                    "</a>"+
                "</div>"+
            "</div>"
        );

        //limite_act++;
        //Aumentamos el indice del renglon
        $("#maxRenglon").val(limite_act);
    
    //Si falta algun campo por llenar, mandamos el cursor a ese campo
    }else{
        if (num_part == "") {
            $("#numParte_"+limite_act).focus();
        }else{
            $("#descripcionCorta_"+limite_act).focus();
        }
    }
}

//Quitar reglon de refacciones
/*function quitarRef_cliente(renglon) {
    //Recuperamos el indice de renglones actual
    //var limite_act = $("#maxRenglon").val();
    //var renglon = limite_act - 1;

    //Validamos que no sea el primer renglon
    if (renglon > 0) {
        var fila = $("#renglon_"+renglon);
        fila.remove();

        //Decrementamos el indice para los renglones
        //limite_act--;
        //$("#maxRenglon").val(limite_act);
    //De lo contrario solo limpiamos los campos
    }else{
        $("#cantidad_"+renglon).val("1");
        $("#numParte_"+renglon).val("");
        $("#id_refaccion_"+renglon).val("0");
        $("#descripcionCorta_"+renglon).val("");
        $("#maxRenglon").val("1");
    }
}*/

function quitarRef_cliente(renglon) {
    if ($("#cargaFormulario").val() == "1") {
        if (renglon > 1) {
            var fila = $("#renglon_"+renglon);
            fila.remove();

            var max_limite = $("#maxRenglon").val();
            if (max_limite > 1) {
                max_limite--;
                $("#maxRenglon").val(max_limite);
            }else{
                $("#maxRenglon").val("1");
            }

        //De lo contrario solo limpiamos los campos
        }else{
            var max_limite = $("#maxRenglon").val();
            $("#cantidad_"+renglon).val("1");
            $("#numParte_"+renglon).val("");
            $("#id_refaccion_"+renglon).val("0");
            $("#descripcionCorta_"+renglon).val("");

            if (max_limite > 1) {
                max_limite--;
                $("#maxRenglon").val(max_limite);
            }else{
                $("#maxRenglon").val("1");
            }
            
        }
    }else{
        $("#renglon_eliminar").val(renglon);
    }
}

$('#btn_envio_refaccion').on('click',function(){
    var renglon = $("#renglon_eliminar").val();
    console.log(renglon);
    //Validamos que no sea el primer renglon
    if (renglon > 1) {
        //Si el renglon esta vacio o no se ha guardado previamente, lo eliminamos
        if (($("#descripcionCorta_"+renglon).val() == "")||($("#id_index_"+renglon).val() == "0")) {
            var fila = $("#renglon_"+renglon);
            fila.remove();
        }else{
            $("#renglon_"+renglon).css("display","none");
            $("#id_refaccion_"+renglon).val("1A");
        }

    //De lo contrario solo limpiamos los campos
    }else{
        $("#cantidad_"+renglon).val("1");
        $("#numParte_"+renglon).val("");
        $("#id_refaccion_"+renglon).val("0");
        $("#descripcionCorta_"+renglon).val("");
        $("#maxRenglon").val("1");
    }
});