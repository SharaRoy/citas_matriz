function btnDeleteClick(idRegistro){    
    //console.log("Id de Registro: "+idRegistro);
    $("#idRegistro").val(idRegistro);

    //Solicitamos e imprimimos la informacion en el modal
    var url = $("#basePeticion").val();

    $.ajax({
        url: url+"otros_Modulos/Historial_garantias/cargaInfo",
        method: 'post',
        data: {
            idRegistro : idRegistro,
        },
        success:function(resp){
            //console.log(resp);

            if (resp.indexOf("handler           </p>") < 1) {
                var campos = resp.split("|");
                //console.log(campos);

                $("#datosCabecera").text("Orden #"+campos[0]+" Folio: "+campos[1]);
                $("#datosEstatus").text("Estatus: "+campos[2]);
                $("#datosEstatus").css('background-color',campos[3]);
                $("#datosEstatus").css('color',campos[4]);
                
                var cantidades = campos[5].split("_");
                var descripcion = campos[6].split("_");
                var contenido = "";

                for (var i = cantidades.length - 1; i >= 0; i--) {
                    if (descripcion[i] != "") {
                        contenido += "C. "+cantidades[i]+" - Descripcion: "+descripcion[i]+'\n';
                    }                    
                }

                $("#refacciones").val(contenido);
                
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

//Enviamos el comentario por ajax
$("#btnEliminar").on('click',function(){
    //Recuperamos los valores para enviar
    var motivo = $("#motivoBaja").val();
    var idRegistro = $("#idRegistro").val();

    //Verificamos que se haya escrito el motivo de baja
    if (motivo.length > 4) {
        //Hacemos la peticion ajax de la busqueda
        var url = $("#basePeticion").val();

        $(".cargaIcono").css("display","inline-block");
        $("#errorEnvioCotizacion").text("");

        $.ajax({
            url: url+"otros_Modulos/Historial_garantias/guardarBaja",
            method: 'post',
            data: {
                motivo : motivo,
                idRegistro : idRegistro,
            },
            success:function(resp){
                //console.log(resp);
                console.log("");

                if (resp.indexOf("handler           </p>") < 1) {
                    if (resp == "OK") {
                        $("#datosCabecera").text("");
                        $("#datosEstatus").text("");
                        $("#datosEstatus").css('background-color','white');
                        $("#datosEstatus").css('color','black');
                        $("#refacciones").val("");

                        $('#OkResult2').fadeIn(1000);
                        $('#OkResult2').fadeOut(2000);

                        location.reload()
                    }else {
                        $('errorResult2').fadeIn(1000);
                        $('errorResult2').fadeOut(2000);
                    }
                }

                $(".cargaIcono").css("display","none");
                $("#mensajeError").text("");
                
            //Cierre de success
            },
            error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#mensajeError").text("Error al intentar enviar la solicitud");
            //Cierre del error
            }
        //Cierre del ajax
        });
    } else {
        $("#mensajeError").text("Falta especificar motivo de baja");
    }
});

//Modal para eliminar registro de lista de clientes (REFACCIONES)
function btnDeleteClick_RC(idRegistro){    
    //console.log("Id de Registro: "+idRegistro);
    $("#idRegistro").val(idRegistro);

    //Solicitamos e imprimimos la informacion en el modal
    var url = $("#basePeticion").val();

    $.ajax({
        url: url+"otros_Modulos/Clientes_Refacciones/cargaInfo",
        method: 'post',
        data: {
            idRegistro : idRegistro,
        },
        success:function(resp){
            //console.log(resp);

            if (resp.indexOf("handler           </p>") < 1) {
                var campos = resp.split("|");
                //console.log(campos);

                $("#datosCabecera").text("Orden #"+campos[0]+" Folio: "+campos[1]);
                $("#datosEstatus").text("Estatus: "+campos[2]);
                $("#datosEstatus").css('background-color',campos[3]);
                $("#datosEstatus").css('color',campos[4]);
                
                var cantidades = campos[5].split("_");
                var descripcion = campos[6].split("_");
                var contenido = "";

                for (var i = cantidades.length - 1; i >= 0; i--) {
                    if (descripcion[i] != "") {
                        contenido += "C. "+cantidades[i]+" - Descripcion: "+descripcion[i]+'\n';
                    }                    
                }

                $("#refacciones").val(contenido);
                
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

//Enviamos el comentario por ajax
$("#btnEliminar_RC").on('click',function(){
    //Recuperamos los valores para enviar
    var motivo = $("#motivoBaja").val();
    var idRegistro = $("#idRegistro").val();

    //Verificamos que se haya escrito el motivo de baja
    if (motivo.length > 4) {
        //Hacemos la peticion ajax de la busqueda
        var url = $("#basePeticion").val();

        $(".cargaIcono").css("display","inline-block");
        $("#errorEnvioCotizacion").text("");

        $.ajax({
            url: url+"otros_Modulos/Clientes_Refacciones/guardarBaja",
            method: 'post',
            data: {
                motivo : motivo,
                idRegistro : idRegistro,
            },
            success:function(resp){
                //console.log(resp);

                if (resp.indexOf("handler           </p>") < 1) {
                    if (resp == "OK") {
                        $("#datosCabecera").text("");
                        $("#datosEstatus").text("");
                        $("#datosEstatus").css('background-color','white');
                        $("#datosEstatus").css('color','black');
                        $("#refacciones").val("");

                        $('#OkResult2').fadeIn(1000);
                        $('#OkResult2').fadeOut(2000);

                        location.reload()
                    }else {
                        $('errorResult2').fadeIn(1000);
                        $('errorResult2').fadeOut(2000);
                    }
                }

                $(".cargaIcono").css("display","none");
                $("#mensajeError").text("");
                
            //Cierre de success
            },
            error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#mensajeError").text("Error al intentar enviar la solicitud");
            //Cierre del error
            }
        //Cierre del ajax
        });
    } else {
        $("#mensajeError").text("Falta especificar motivo de baja");
    }
    
});