//Alta de FORMULARIO para enviar por ajax
//Liberamos los campos cuando se envie la cotizacion
$("#guardar_formulario").on('click', function (e){
    console.log("Se envia formulario");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    //$("#historial_clientes_refacciones").css("background-color","#ddd");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    /* Creamos un nuevo objeto FormData. Este sustituye al atributo enctype = "multipart/form-data" que, tradicionalmente, se incluía en los formularios (y que aún se incluye, cuando son enviados 
	desde HTML. */
	var paqueteInformacion = new FormData(document.getElementById('historial_clientes_refacciones'));
	paqueteInformacion.append('guardar_formulario', $(this).prop('value')); 

	/* Se envia el paquete de datos por ajax. */
	var base = $("#basePeticion").val().trim();


    var validarReglas = validar_formulario();

    console.log(validarReglas);

    //Si el formulario procedio correctamente
    if (validarReglas == 1) {
        $.ajax({
            url: base+"otros_Modulos/Clientes_Refacciones/guardar_formulario",
            type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
            contentType: false,
            data: paqueteInformacion, // Al atributo data se le asigna el objeto FormData.
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                	var respuestas = resp.split("_");
                    //Si el registro se guardo correctamente, redireccionamos a la vista de no edicion
                    if (respuestas[0] == "OK") {
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        //Regresamos los colores de la tabla a la normalidad
                        //$("#historial_clientes_refacciones").css("background-color","white");
                        location.href = base+"ClienteRF_Lista/4";

                  	//De lo contrario, mandamos un mensaje
                  	} else {
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        //Regresamos los colores de la tabla a la normalidad
                        //$("#historial_clientes_refacciones").css("background-color","white");
                  		$("#errorEnvio").text(respuestas[0]);
                  	}
                }else{
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    //$("#historial_clientes_refacciones").css("background-color","white");
                    $("#errorEnvio").text("ERROR AL ENVIAR EL FORMULARIO");
                }
    		
    		//Cierre de success
            },
                error:function(error){
                    console.log(error);
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    //$("#historial_clientes_refacciones").css("background-color","white");
                    $("#errorEnvio").text("ERROR 02 AL ENVIAR EL FORMULARIO");
                }
    	}); 
    //De lo contrario regresamos los errores
    }else{
        //Regresamos los colores de la tabla a la normalidad
        //$("#historial_clientes_refacciones").css("background-color","white");
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $(".cargaIcono").css("display","none");
    }
});


function validar_formulario() {
    $validar = 1;

    //Validamos los campos del formulario
    if ($("input[name='orden']").val() == "") {
        $("#error_orden").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_orden").text("");
    }

    if ($("input[name='fecha_folio']").val() == "") {
        $("#error_fecha_folio").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_fecha_folio").text("");
    }

    if ($("input[name='serie']").val() == "") {
        $("#error_serie").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_serie").text("");
    }

    if ($("input[name='tecnico']").val() == "") {
        $("#error_tecnico").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_tecnico").text("");
    }

    if ($("#asesor").val() == "") {
        $("#error_asesor").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_asesor").text("");
    }

    if ($("input[name='modeloVehiculo']").val() == "") {
        $("#error_modeloVehiculo").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_modeloVehiculo").text("");
    }

    if ($("input[name='unidadTaller']").val() == "") {
        $("#error_unidadTaller").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_unidadTaller").text("");
    }

    if ($("input[name='folio']").val() == "") {
        $("#error_folio").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_folio").text("");
    }

    if ($("#estadoProceso").val() == "") {
        $("#error_estadoProceso").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_estadoProceso").text("");
    }

    /*if ($("input[name='cantidad_0']").val() == "") {
        $("#error_cantidad_0").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_cantidad_0").text("");
    }

    if ($("input[name='descripcionCorta_0']").val() == "") {
        $("#error_descripcionCorta_0").text("Campo requerido");
        $validar = 0;
    }else{
        $("#error_descripcionCorta_0").text("");
    }*/

    if (parseInt($("input[name='maxRenglon']").val()) <= 1) {
        $("#error_refaccion").text("No se ingreso ninguna refacción");
        $validar = 0;
    }else{
        $("#error_refaccion").text("");
    }

    return $validar;
}