$('#btnBusqueda_pGraficas').on('click',function(){
    console.log("Buscando presupuestos para graficas");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#imprimir_busqueda").css("background-color","#ddd");
    $("#imprimir_busqueda").css("color","#6f5e5e");
    
    var campo = $("#busqueda_campo").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"otros_Modulos/Graficas/busquedaGrafica",
        method: 'post',
        data: {
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $("#imprimir_busqueda");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("=");
                        //console.log(celdas);

                        var ver_grafica = "<a href='"+base+"Graficas_Cotizacion/"+celdas[12]+"' class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>"+
                                    "VER<br>PROCESO"+
                                "</a>"; 

                        var apruebaCliente = "";
                        if (celdas[8] != "") {
                            if (celdas[8] == 'Si') {
                                apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                            } else if(celdas[8] == 'Val'){
                                apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                            } else{
                                apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                            }
                        } else{
                            apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                        }

                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:12px;'>"+
                            // ORDEN
                            "<td align='center' style='vertical-align: middle;'>"+celdas[0]+"</td>"+
                            // ORDEN INTELISIS 
                            "<td align='center' style='vertical-align: middle;'>"+celdas[1]+"</td>"+
                            // ASESOR 
                            "<td align='center' style='vertical-align: middle;'>"+celdas[2]+"</td>"+
                            // TÉCNICO 
                            "<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
                            // MODELO  
                            "<td align='center' style='vertical-align: middle;'>"+celdas[4]+"</td>"+
                            // PLACAS  
                            "<td align='center' style='vertical-align: middle;'>"+celdas[5]+"</td>"+
                            // PROCESO  
                            "<td align='center' style='vertical-align: middle;'>"+celdas[6]+"</td>"+
                            // ULTIMA ACTUALIZACIÓN 
                            "<td align='center' style='vertical-align: middle;'>"+celdas[7]+"</td>"+ 
                            //APRUEBA CLIENTE
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+apruebaCliente+"</td>"+
                            //ESTADO REFACCIONES
                            "<td align='center' style='vertical-align: middle;width:15%;"+celdas[10]+"'>"+
                                ""+celdas[11]+""
                            +"</td>"+
                            //VER PRESUPUESTO
                            "<td align='center' style='vertical-align: middle;'>"+ver_grafica+"</td>"+
                        "</tr>");
                    }

                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $("#imprimir_busqueda").css("background-color","white");
            $("#imprimir_busqueda").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $("#imprimir_busqueda").css("background-color","white");
            $("#imprimir_busqueda").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});