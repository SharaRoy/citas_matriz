$(document).ready(function() {
    if ( document.getElementById( "idOrden" )) {
        //Recuperamos el id de la orden para hacer la grafica
        datos_grafica();

        //Recuperamos los valores de retrasado
        datos_grafica_retrasado();
    }
    

    function datos_grafica(){
        var idOrden = $("#idOrden").val();

        //Si existe el numero de orden, consultamos la información
        if ((idOrden != "")&&(idOrden != 0)) {
            $(".cargaIcono").css("display","inline-block");
            //Recuperamos la base del proyecto
            var url = $("#basePeticion").val().trim();
            $.ajax({
                url: url+"otros_Modulos/Graficas/cargaDatosGrafica",
                method: 'post',
                data: {
                    idOrden: idOrden,
                },
                success:function(resp){
                    //console.log(resp);
                    //Si la respuesta no es un error
                    if (resp.indexOf("handler           </p>")<1) {
                        //Limpiamos los posibles mensajes de error
                        $("#txtError").text("");

                        //Recuperamos los fragmentos de informacion
                        var campos = resp.split("|");
                        //console.log(campos);

                        if (campos.length > 3) {
                            google.charts.load('current', {'packages':['corechart']});
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                var data = new google.visualization.DataTable();
                                data.addColumn('string', 'Base');
                                data.addColumn('datetime', 'PROCESO');
                                //data.addColumn('string', 'tiempo');

                                var contenedor = [];

                                //Fecha de registro  campos[0]                                    
                                var tiempo = campos[1].split(" ");
                                var fecha = tiempo[0].split("-");
                                var hora = tiempo[1].split(":");
                                var tiempo_1 = new Date(parseInt(fecha[0]), (parseInt(fecha[1])-1) , parseInt(fecha[2]) , parseInt(hora[0]) , parseInt(hora[1]));

                                contenedor.push(["REGISTRO",tiempo_1]);

                                //Fecha de termino en ventanilla
                                if (campos[2] != "NA") {
                                    var tiempo_01 = campos[3].split(" ");
                                    var fecha_1 = tiempo_01[0].split("-");
                                    var hora_1 = tiempo_01[1].split(":");
                                    var tiempo_A = new Date(parseInt(fecha_1[0]), (parseInt(fecha_1[1])-1) , parseInt(fecha_1[2]) , parseInt(hora_1[0]) , parseInt(hora_1[1]));

                                    contenedor.push(["VENTANILLA",tiempo_A]);
                                }
                                
                                //Fecha de termino en JEFE DE TALLER
                                if (campos[4] != "NA") {
                                    var tiempo_2 = campos[5].split(" ");
                                    var fecha_2 = tiempo_2[0].split("-");
                                    var hora_2 = tiempo_2[1].split(":");
                                    var tiempo_B = new Date(parseInt(fecha_2[0]), (parseInt(fecha_2[1])-1) , parseInt(fecha_2[2]) , parseInt(hora_2[0]) , parseInt(hora_2[1]));

                                    contenedor.push(["JEFE DE TALLER",tiempo_B]);
                                }

                                //Fecha de termino en revision de garantias
                                if (campos[6] != "NA") {
                                    var tiempo_3 = campos[7].split(" ");
                                    var fecha_3 = tiempo_3[0].split("-");
                                    var hora_3 = tiempo_3[1].split(":");
                                    var tiempo_C = new Date(parseInt(fecha_3[0]), (parseInt(fecha_3[1])-1) , parseInt(fecha_3[2]) , parseInt(hora_3[0]) , parseInt(hora_3[1]));

                                    contenedor.push(["GARANTÍAS",tiempo_C]);
                                }

                                //Fecha de firma del asesor
                                if (campos[8] != "NA") {
                                    var tiempo_4 = campos[9].split(" ");
                                    var fecha_4 = tiempo_4[0].split("-");
                                    var hora_4 = tiempo_4[1].split(":");
                                    var tiempo_D = new Date(parseInt(fecha_4[0]), (parseInt(fecha_4[1])-1) , parseInt(fecha_4[2]) , parseInt(hora_4[0]) , parseInt(hora_4[1]));

                                    contenedor.push(["ASESOR",tiempo_D]);
                                }

                                //Fecha en que el cliente/asesor aceptan/rechazan la cotizacion
                                if (campos[10] != "NA") {
                                    var tiempo_5 = campos[11].split(" ");
                                    var fecha_5 = tiempo_5[0].split("-");
                                    var hora_5 = tiempo_5[1].split(":");
                                    var tiempo_E = new Date(parseInt(fecha_5[0]), (parseInt(fecha_5[1])-1) , parseInt(fecha_5[2]) , parseInt(hora_5[0]) , parseInt(hora_5[1]));

                                    contenedor.push(["ASESOR / CLIENTE",tiempo_E]);
                                }

                                //Fecha en que se solicitan las refacciones en ventanilla
                                if (campos[12] != "NA") {
                                    var tiempo_5A = campos[13].split(" ");
                                    var fecha_5A = tiempo_5A[0].split("-");
                                    var hora_5A = tiempo_5A[1].split(":");
                                    var tiempo_EA = new Date(parseInt(fecha_5A[0]), (parseInt(fecha_5A[1])-1) , parseInt(fecha_5A[2]) , parseInt(hora_5A[0]) , parseInt(hora_5A[1]));

                                    contenedor.push(["REFACCIONES SOLICITADAS",tiempo_EA]);
                                }

                                //Fecha en que se reciben las refacciones en ventanilla
                                if (campos[14] != "NA") {
                                    var tiempo_5B = campos[15].split(" ");
                                    var fecha_5B = tiempo_5B[0].split("-");
                                    var hora_5B = tiempo_5B[1].split(":");
                                    var tiempo_EB = new Date(parseInt(fecha_5B[0]), (parseInt(fecha_5B[1])-1) , parseInt(fecha_5B[2]) , parseInt(hora_5B[0]) , parseInt(hora_5B[1]));

                                    contenedor.push(["REFACCIONES RECIBIDAS",tiempo_EB]);
                                }

                                //Fecha en que se entrgan las refacciones en ventanilla
                                if (campos[16] != "NA") {
                                    var tiempo_5C = campos[17].split(" ");
                                    var fecha_5C = tiempo_5C[0].split("-");
                                    var hora_5C = tiempo_5C[1].split(":");
                                    var tiempo_EC = new Date(parseInt(fecha_5C[0]), (parseInt(fecha_5C[1])-1) , parseInt(fecha_5C[2]) , parseInt(hora_5C[0]) , parseInt(hora_5C[1]));

                                    contenedor.push(["REFACCIONES ENTREGADAS",tiempo_EC]);
                                }

                                //Ultima actualizacion
                                if (campos[18] != "NA") {
                                    var tiempo_6 = campos[19].split(" ");
                                    var fecha_6 = tiempo_6[0].split("-");
                                    var hora_6 = tiempo_6[1].split(":");
                                    var tiempo_F = new Date(parseInt(fecha_6[0]), (parseInt(fecha_6[1])-1) , parseInt(fecha_6[2]) , parseInt(hora_6[0]) , parseInt(hora_6[1]));

                                    contenedor.push(["ULTIMA ACTUALIZACIÓN",tiempo_F]);
                                }

                                //console.log(contenedor);
                                data.addRows(contenedor);
  
                                var options = {
                                    legend: {position: 'top', maxLines: 3},
                                    hAxis: {
                                        viewWindow: {
                                            min: tiempo_1,
                                            max: tiempo_F
                                        },
                                      /*gridlines: {
                                        count: -1,
                                        units: {
                                          days: {format: ['MMM dd']},
                                          hours: {format: ['HH:mm', 'ha']},
                                        }
                                      },
                                      minorGridlines: {
                                        units: {
                                          hours: {format: ['hh:mm:ss a', 'ha']},
                                          minutes: {format: ['HH:mm a Z', ':mm']}
                                        }
                                      }*/
                                    }

                                };

                                var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                                
                                chart.draw(data, options);
                            }
                        }else{
                            $("#txtError").text("Sin información que mostrar.");    
                        }
                    //Si la respuesta es un error
                    }else{
                        $("#txtError").text("No se cargo correctamente la información.");
                    }

                    $("#grafica_1").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $("#txtError").text("No se cargo correctamente la información...");
                    $("#grafica_1").css("display","none");
                //Cierre del error
                }
            //Cierre del ajax
            });
        }else{
            $("#txtError").text("No. de Orden incorrecto.");
            $("#grafica_1").css("display","none");
        }
    }

    function datos_grafica_retrasado(){
        var idOrden = $("#idOrden").val();

        //Si existe el numero de orden, consultamos la información
        if ((idOrden != "")&&(idOrden != 0)) {
            $(".cargaIcono").css("display","inline-block");
            //Recuperamos la base del proyecto
            var url = $("#basePeticion").val().trim();
            $.ajax({
                url: url+"otros_Modulos/Graficas/cargaDatosGraficaRetrasados",
                method: 'post',
                data: {
                    idOrden: idOrden,
                },
                success:function(resp){
                    //console.log(resp);
                    //Si la respuesta no es un error
                    if (resp.indexOf("handler           </p>")<1) {
                        //Limpiamos los posibles mensajes de error
                        $("#txtError").text("");

                        //Recuperamos los fragmentos de informacion
                        var campos = resp.split("|");
                        //console.log(campos);

                        if (campos.length > 3) {
                            google.charts.load('current', {'packages':['corechart']});
                            google.charts.setOnLoadCallback(drawChart);

                            function drawChart() {
                                var data = new google.visualization.DataTable();
                                data.addColumn('string', 'PROCESO');
                                data.addColumn('number', 'TRANSCURRIDO (MIN)');
                                data.addColumn('number', 'RETRADADO (MIN)');

                                var contenedor = [];

                                contenedor.push(["REGISTRO",0,0]);

                                //Fecha de termino en ventanilla
                                if (campos[0] != "NA") {
                                    contenedor.push(["VENTANILLA",0,parseInt(campos[1])]);
                                }
                                
                                //Fecha de termino en JEFE DE TALLER
                                if (campos[2] != "NA") {
                                    contenedor.push(["JEFE DE TALLER",0,parseInt(campos[3])]);
                                }

                                //Fecha de termino en revision de garantias
                                if (campos[4] != "NA") {
                                    contenedor.push(["GARANTÍAS",0,parseInt(campos[5])]);
                                }

                                //Fecha de firma del asesor
                                if (campos[6] != "NA") {
                                    contenedor.push(["ASESOR",0,parseInt(campos[7])]);
                                }

                                //Fecha en que el cliente/asesor aceptan/rechazan la cotizacion
                                if (campos[8] != "NA") {
                                    contenedor.push(["ASESOR / CLIENTE",0,parseInt(campos[9])]);
                                }

                                //Fecha en que se solicitan las refacciones en ventanilla
                                if (campos[10] != "NA") {
                                    contenedor.push(["REFACCIONES SOLICITADAS",0,parseInt(campos[11])]);
                                }

                                //Fecha en que se reciben las refacciones en ventanilla
                                if (campos[12] != "NA") {
                                    contenedor.push(["REFACCIONES RECIBIDAS",0,parseInt(campos[13])]);
                                }

                                //Fecha en que se entrgan las refacciones en ventanilla
                                if (campos[14] != "NA") {
                                    contenedor.push(["REFACCIONES ENTREGADAS",0,parseInt(campos[15])]);
                                }

                                //console.log(contenedor);
                                data.addRows(contenedor);
  
                                var options = {
                                    isStacked: true,
                                    height: 400,
                                    legend: {position: 'top', maxLines: 3},
                                    hAxis: {
                                        title: 'PASO DEL PROCESO'
                                        
                                    },
                                    vAxis: {
                                        title: 'HORAS TRANSCURRIDAS',
                                        minValue: 0
                                    }

                                };

                                var chart = new google.visualization.AreaChart(document.getElementById('chart_div_2'));
                                
                                chart.draw(data, options);
                            }
                        }else{
                            $("#txtError").text("Sin información que mostrar.");    
                        }
                    //Si la respuesta es un error
                    }else{
                        $("#txtError").text("No se cargo correctamente la información.");
                    }

                    $("#grafica_2").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $("#txtError").text("No se cargo correctamente la información...");
                    $("#grafica_2").css("display","none");
                //Cierre del error
                }
            //Cierre del ajax
            });
        }else{
            $("#txtError").text("No. de Orden incorrecto.");
            $("#grafica_2").css("display","none");
        }
    }

    function formato(texto){
        return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
    }
    
});