//Condicionamos las busquedas por fecha 
$('#fecha_inicio').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_fin").attr('min',fecha_inicio);
    //console.log(fecha_inicio)
});

$('#fecha_fin').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_inicio").attr('max',fecha_fin);
    //console.log(fecha_fin)
});

$('#btn_buscar').on('click',function(){
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#cuadro_filtro").css("background-color","#ddd");
    $("#cuadro_filtro").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_ini = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var tecnico = $("#tecnico").val();
    var tipo_orden = $("#tipo_orden").val();
    var tipo_operacion = $("#tipo_operacion").val();

    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"conexion/Estadisticas_Indicadores/recuperar_totales", 
        method: 'post',
        data: {
            fecha_ini : fecha_ini,
            fecha_fin : fecha_fin,
            tecnico : tecnico,
            tipo_orden : tipo_orden,
            tipo_operacion : tipo_operacion
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                if (tipo_orden == "1") {
                    $("#titulo_busqueda").text("VENTA MANO DE OBRA : MATENIMIENTO");
                }else if (tipo_orden == "2") {
                    $("#titulo_busqueda").text("VENTA MANO DE OBRA : GARANTÍAS");
                }else if (tipo_orden == "3") {
                    $("#titulo_busqueda").text("VENTA MANO DE OBRA : TALLER");
                }else if (tipo_orden == "4") {
                    $("#titulo_busqueda").text("VENTA MANO DE OBRA : INTERNA");
                }else{
                    $("#titulo_busqueda").text("VENTA MANO DE OBRA");
                }
                
                if (resp.length < 2) {
                    $("#Alerta_busqueda").css("color","red");
                    $("#Alerta_busqueda").css("font-weight","bold");
                    $("#Alerta_busqueda").text("No se encontraron resultados");
                }else{
                    $("#Alerta_busqueda").text("");
                    //Recuperamos los registros
                    var registros = resp.split("|");
                    //Recorremos los "renglones" obtenidos para partir los registros
                    var regitro = ( registros.length > 1) ? (registros.length -1) : registros.length;
                    //console.log(regitro);

                    //Verificamos si son mas de 1 registro
                    if (regitro == 1) {
                        //Inicializamos los valores de la grafica
                        google.charts.load('current', {packages: ['corechart', 'bar']});

                        google.charts.setOnLoadCallback(drawChart_T1);
                        google.charts.setOnLoadCallback(drawChart_T2);
                    }else{
                        //Inicializamos los valores de la grafica
                        google.charts.load('current', {'packages':['corechart']});

                        google.charts.setOnLoadCallback(drawChart_T1);
                        google.charts.setOnLoadCallback(drawChart_T2);
                    }

                    //console.log(registros);

                    function drawChart_T1() {
                        var data = new google.visualization.DataTable();
                        
                        data.addColumn('string', 'Fecha');
                        data.addColumn('number', 'Venta Total');
                        //data.addColumn('string', { role: 'style' });
                        //data.addColumn('string', { role: 'annotation' } );

                        for (var i = 0; i < regitro; i++) {
                            var celdas = registros[i].split("=");
                            //console.log(celdas);

                            //Creamos los componentes para impresion
                            var fecha = celdas[0];
                            var total_horas = celdas[1];
                            var costo_mo = celdas[2];
                            var total_venta = celdas[3];

                            data.addRows([ [ fecha , parseFloat(total_venta)] ]);
                            //dataT2.addRows([ [fecha,total_horas] ]);
                        }

                        var options = {
                            //title: 'VENTA TOTAL DE MANO DE OBRA',
                            isStacked: true,
                            height: 300,
                            legend: {position: 'top', maxLines: 3},
                            hAxis: {
                                title: 'FECHA'
                                
                            },
                            vAxis: {
                                title: 'VENTA TOTAL DE MANO DE OBRA',
                                minValue: 0
                            }

                            //hAxis: {title: 'FECHA', titleTextStyle: {color: 'Black'}},
                            //vAxis: {minValue: 0}
                        };

                        //Verificamos si son mas de 1 registro
                        if (regitro == 1) {
                            var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
                        }else{
                            //var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));    
                            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                        }
                        
                        chart.draw(data, options);
                    }

                    function drawChart_T2() {
                        var dataT2 = new google.visualization.DataTable();
                        
                        dataT2.addColumn('string', 'Fecha');
                        dataT2.addColumn('number', 'Total de Horas(MO)');
                        //data.addColumn('string', { role: 'style' });
                        //data.addColumn('string', { role: 'annotation' } );

                        for (var i = 0; i < regitro; i++) {
                            var celdas = registros[i].split("=");
                            //console.log(celdas);

                            //Creamos los componentes para impresion
                            var fecha = celdas[0];
                            var total_horas = celdas[1];
                            var costo_mo = celdas[2];
                            var total_venta = celdas[3];

                            dataT2.addRows([ [ fecha , parseFloat(celdas)] ]);
                        }

                        var optionsT2 = {
                            //title: 'VENTA TOTAL DE MANO DE OBRA',
                            isStacked: true,
                            height: 300,
                            legend: {position: 'top', maxLines: 3},
                            hAxis: {
                                title: 'FECHA'
                                
                            },
                            vAxis: {
                                title: 'TOTAL HORAS MANO DE OBRA',
                                minValue: 0
                            }

                            //hAxis: {title: 'FECHA', titleTextStyle: {color: 'Black'}},
                            //vAxis: {minValue: 0}
                        };

                        if (regitro == 1) {
                            var chart = new google.visualization.BarChart(document.getElementById('chart_div_2'));
                        }else{
                            //var chart = new google.visualization.AreaChart(document.getElementById('chart_div_2'));    
                            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_2'));
                        }
                        chart.draw(dataT2, optionsT2);
                    }
                }
                
            }else{
                $("#Alerta_busqueda").text("Erro al realizar la busqueda");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $("#cuadro_filtro").css("background-color","white");
            $("#cuadro_filtro").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $("#cuadro_filtro").css("background-color","white");
            $("#cuadro_filtro").css("color","black");

            $("#Alerta_busqueda").text("Erro al realizar la busqueda");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

$('#btn_buscar_garantiaV1').on('click',function(){
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#cuadro_filtro").css("background-color","#ddd");
    $("#cuadro_filtro").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_ini = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var tecnico = $("#tecnico").val();
    var estatus = $("#estatus").val();

    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"conexion/Estadisticas_Indicadores/conteo_garantias", 
        method: 'post',
        data: {
            fecha_ini : fecha_ini,
            fecha_fin : fecha_fin,
            tecnico : tecnico,
            estatus : estatus,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                var texto = $("#estatus").find('option:selected').text();
                $("#titulo_busqueda_garantia").text("ESTATUS GARANTÍA: "+texto.toUpperCase());
                
                if (resp.length < 2) {
                    $("#Alerta_busqueda").css("color","red");
                    $("#Alerta_busqueda").css("font-weight","bold");
                    $("#Alerta_busqueda").text("No se encontraron resultados");
                }else{
                    $("#Alerta_busqueda").text("");
                    //Recuperamos los registros
                    var registros = resp.split("|");
                    //Recorremos los "renglones" obtenidos para partir los registros
                    var regitro = ( registros.length > 1) ? (registros.length -1) : registros.length;
                    //console.log(regitro);

                    //Verificamos si son mas de 1 registro
                    if (regitro == 1) {
                        //Inicializamos los valores de la grafica
                        google.charts.load('current', {packages: ['corechart', 'bar']});

                        google.charts.setOnLoadCallback(drawChart_T1);
                    }else{
                        //Inicializamos los valores de la grafica
                        google.charts.load('current', {'packages':['corechart']});

                        google.charts.setOnLoadCallback(drawChart_T1);
                    }

                    //console.log(registros);

                    function drawChart_T1() {
                        var data = new google.visualization.DataTable();
                        
                        data.addColumn('string', 'Fecha');
                        data.addColumn('number', 'Ordenes');
                        //data.addColumn('string', { role: 'style' });
                        //data.addColumn('string', { role: 'annotation' } );

                        for (var i = 0; i < regitro; i++) {
                            var celdas = registros[i].split("=");
                            //console.log(celdas);

                            //Creamos los componentes para impresion
                            var fecha = celdas[0];
                            var conteo_por_dia = celdas[1];

                            data.addRows([ [ fecha , parseInt(conteo_por_dia)] ]);
                            //dataT2.addRows([ [fecha,total_horas] ]);
                        }

                        var options = {
                            //title: 'VENTA TOTAL DE MANO DE OBRA',
                            isStacked: true,
                            height: 300,
                            legend: {position: 'top', maxLines: 3},
                            hAxis: {
                                title: 'FECHA'
                                
                            },
                            vAxis: {
                                title: 'ORDENES',
                                minValue: 0
                            }

                            //hAxis: {title: 'FECHA', titleTextStyle: {color: 'Black'}},
                            //vAxis: {minValue: 0}
                        };

                        //Verificamos si son mas de 1 registro
                        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                        //var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));    
                        
                        chart.draw(data, options);
                    }
                }
                
            }else{
                $("#Alerta_busqueda").text("Erro al realizar la busqueda");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $("#cuadro_filtro").css("background-color","white");
            $("#cuadro_filtro").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $("#cuadro_filtro").css("background-color","white");
            $("#cuadro_filtro").css("color","black");

            $("#Alerta_busqueda").text("Erro al realizar la busqueda");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

$('#btn_buscar_garantiaV2').on('click',function(){
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#cuadro_filtro").css("background-color","#ddd");
    $("#cuadro_filtro").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_ini = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var tecnico = $("#tecnico").val();
    var estatus = $("#estatus").val();

    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"conexion/Estadisticas_Indicadores/monto_garantias", 
        method: 'post',
        data: {
            fecha_ini : fecha_ini,
            fecha_fin : fecha_fin,
            tecnico : tecnico,
            estatus : estatus,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                var texto = $("#estatus").find('option:selected').text();
                $("#titulo_busqueda_garantia").text("ESTATUS GARANTÍA: "+texto.toUpperCase());
                
                if (resp.length < 2) {
                    $("#Alerta_busqueda").css("color","red");
                    $("#Alerta_busqueda").css("font-weight","bold");
                    $("#Alerta_busqueda").text("No se encontraron resultados");
                }else{
                    $("#Alerta_busqueda").text("");
                    //Recuperamos los registros
                    var registros = resp.split("|");
                    //Recorremos los "renglones" obtenidos para partir los registros
                    var regitro = ( registros.length > 1) ? (registros.length -1) : registros.length;
                    //console.log(regitro);
                    google.charts.load('current', {packages: ['corechart', 'bar']});
                    google.charts.setOnLoadCallback(drawChart_T1);

                    //console.log(registros);

                    function drawChart_T1() {
                        var data = new google.visualization.DataTable();
                        
                        data.addColumn('string', 'Fecha');
                        data.addColumn('number', 'Ordenes');
                        //data.addColumn('string', { role: 'style' });
                        //data.addColumn('string', { role: 'annotation' } );

                        for (var i = 0; i < regitro; i++) {
                            var celdas = registros[i].split("=");
                            //console.log(celdas);

                            //Creamos los componentes para impresion
                            var fecha = celdas[0];
                            var conteo_por_dia = celdas[1];

                            data.addRows([ [ fecha , parseInt(conteo_por_dia)] ]);
                            //dataT2.addRows([ [fecha,total_horas] ]);
                        }

                        var options = {
                            //title: 'VENTA TOTAL DE MANO DE OBRA',
                            isStacked: true,
                            height: 300,
                            legend: {position: 'top', maxLines: 3},
                            hAxis: {
                                title: 'FECHA'
                                
                            },
                            vAxis: {
                                title: 'ORDENES',
                                minValue: 0
                            }

                            //hAxis: {title: 'FECHA', titleTextStyle: {color: 'Black'}},
                            //vAxis: {minValue: 0}
                        };

                        //Verificamos si son mas de 1 registro
                        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                        //var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));    
                        
                        chart.draw(data, options);
                    }
                }
                
            }else{
                $("#Alerta_busqueda").text("Erro al realizar la busqueda");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $("#cuadro_filtro").css("background-color","white");
            $("#cuadro_filtro").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $("#cuadro_filtro").css("background-color","white");
            $("#cuadro_filtro").css("color","black");

            $("#Alerta_busqueda").text("Erro al realizar la busqueda");
        //Cierre del error
        }
    //Cierre del ajax
    });
});