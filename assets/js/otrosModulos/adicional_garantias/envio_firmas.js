//Enviamos las firmas
function guardar_firma_gerente(renglon) {
    var orden = $('#orden_servicio').val();

    if ((orden != "")||($('#rutaGerente_'+renglon).val() != "")) {
    	$("#envio_error").text("");
    	$("#cargaIcono").css("display","inline-block");
        //Validamos si existe la informacion
        var paqueteDatos = new FormData();
        paqueteDatos.append('orden', $('#orden_servicio').prop('value'));
        paqueteDatos.append('firma', $('#rutaGerente_'+renglon).prop('value'));
        paqueteDatos.append('encargado', $('#gerente_'+renglon).prop('value'));
        paqueteDatos.append('renglon', renglon);
        
        var base = $("#basePeticion").val();
        
        try{
            $.ajax({
		        url: base+"api/Operaciones_Garantias/firma_gerente", 
		        type: 'post',
		        contentType: false,
		        data: paqueteDatos,
		        processData: false,
		        cache: false,
		        success:function(resp){
		            console.log(resp);
		            if (resp.indexOf("handler         </p>")<1) {
		                var respuesta = resp.split("=");
		                if (respuesta[0] == "OK") {
		                    $("#envio_error").text("OK OK");
		                    $("#fg_"+renglon).css("display","none");
		                    $("#bg_"+renglon).removeClass('btn-warning');
		                    $("#bg_"+renglon).addClass('btn-success');

		                    $("#firma_g_"+renglon).val("1");

		                    if (($("#firma_g_"+renglon).val() == "1")&&($("#firma_j_"+renglon).val() == "1")) {
		                    	$("#renglon_"+renglon).css("background-color","#96ff78");
		                    }
		                } else {
		                    $("#envio_error").text(respuesta[0]);
		                }

		            }else{
		                $("#envio_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
		                //$("#formulario_error").text(resp);
		            }
		            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
		            $("#cargaIcono").css("display","none");
		            //Regresamos los colores de la tabla a la normalidad
		            //$("#formulario").css("background-color","white");
		        //Cierre de success
		        },
		          error:function(error){
		            console.log(error);
		            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
		            $("#cargaIcono").css("display","none");
		            //Regresamos los colores de la tabla a la normalidad
		            //$("#formulario").css("background-color","white");
		            $("#envio_error").text("SE PRODUJO UN ERROR EN EL ENVIO");
		            //location.href = base+"index.php/inventario/index/"+orden;
		        }
		    });
        } catch (e) {
            console.log(e);
            $("#cargaIcono").css("display","none");
            $("#envio_error").text("SE PRODUJO UN ERROR");
        } finally {
        	$("#envio_error").text("OK");
        }
    }else{
    	$("#envio_error").text("FALTA INFORMACIÓN");
    }
}

function guardar_firma_JDT(renglon) {
    var orden = $('#orden_servicio').val();

    if ((orden != "")||($('#rutaJDT_'+renglon).val() != "")) {
    	$("#envio_error").text("");
    	$("#cargaIcono").css("display","inline-block");
        //Validamos si existe la informacion
        var paqueteDatos = new FormData();
        paqueteDatos.append('orden', $('#orden_servicio').prop('value'));
        paqueteDatos.append('firma', $('#rutaJDT_'+renglon).prop('value'));
        paqueteDatos.append('encargado', $('#jefetaller_'+renglon).prop('value'));
        paqueteDatos.append('renglon', renglon);

        var base = $("#basePeticion").val();
        
        try{
            $.ajax({
		        url: base+"api/Operaciones_Garantias/firma_jefetaller", 
		        type: 'post',
		        contentType: false,
		        data: paqueteDatos,
		        processData: false,
		        cache: false,
		        success:function(resp){
		            console.log(resp);
		            if (resp.indexOf("handler         </p>")<1) {
		                var respuesta = resp.split("=");
		                if (respuesta[0] == "OK") {
		                    $("#envio_error").text("OK OK");
		                    $("#fj_"+renglon).css("display","none");
		                    $("#bj_"+renglon).removeClass('btn-warning');
		                    $("#bj_"+renglon).addClass('btn-success');

		                    $("#firma_j_"+renglon).val("1");

		                    if (($("#firma_g_"+renglon).val() == "1")&&($("#firma_j_"+renglon).val() == "1")) {
		                    	$("#renglon_"+renglon).css("background-color","#96ff78");
		                    }
		                } else {
		                    $("#envio_error").text(respuesta[0]);
		                }

		            }else{
		                $("#envio_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
		                //$("#formulario_error").text(resp);
		            }
		            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
		            $("#cargaIcono").css("display","none");
		            //Regresamos los colores de la tabla a la normalidad
		            //$("#formulario").css("background-color","white");
		        //Cierre de success
		        },
		          error:function(error){
		            console.log(error);
		            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
		            $("#cargaIcono").css("display","none");
		            //Regresamos los colores de la tabla a la normalidad
		            //$("#formulario").css("background-color","white");
		            $("#envio_error").text("SE PRODUJO UN ERROR EN EL ENVIO");
		            //location.href = base+"index.php/inventario/index/"+orden;
		        }
		    });
        } catch (e) {
            console.log(e);
            $("#cargaIcono").css("display","none");
            $("#envio_error").text("SE PRODUJO UN ERROR");
        } finally {
        	$("#envio_error").text("OK");
        }
    }else{
    	$("#envio_error").text("FALTA INFORMACIÓN");
    }
}