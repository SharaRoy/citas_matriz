
$(".asignar").on('click',function() {
    //Recuperamos la fila que se editara
    var renglon = $(this).data("id");
    // console.log(renglon);
    //Recuperamos los valores para mostrar ala usuario
    var id = $("#id_"+renglon).val();
    var folio = $("#folio_"+renglon).val();
    var fecha = $("#fecha_"+renglon).val();
    var vehiculo = $("#vehiculo_"+renglon).val();
    var cliente = $("#cliente_"+renglon).val();
    var asesor = $("#asesor_"+renglon).val();
    var hora = $("#hora_"+renglon).val();

    //Imprmimimos el texto en el modal
    $("#infoRegistroScita1").text("Folio: "+folio+".");
    $("#infoRegistroScita2").text("Vehículo: "+vehiculo+".");
    $("#infoRegistroScita3").text("Cliente: "+cliente+".");
    $("#infoRegistroScita4").text("Asesor: "+asesor+".");
    $("#infoRegistroScita5").text("Fecha de registro: "+fecha+".");
    $("#idRegistro").val(id);
    $("#folioTem").val(folio);
    $("#horaRegistro").val(hora);
    $("#fechReg").val(fecha);

});

$('#busqueda_tabla').keyup(function () {
    toSearch();
});


//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}
