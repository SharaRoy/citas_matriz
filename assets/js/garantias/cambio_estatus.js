function informacion_visual(renglon) {
    var base = $("#basePeticion").val();

    var informacion = $("#info_garantia_"+renglon).val();
    var info = informacion.split("_");
    $("#cita_estatus").text(info[0]);
    $("#titulo_estatus").text(info[1]);
    $("#id_renglon").val(renglon);

    $("#id_cita").val(info[0]);

    console.log(informacion);
}

//Enviamos el cambio de estatus
$('#btn_actualizar_estatus').on('click',function(){
    //console.log("Actualizando estatus");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");

    //Recuperamos las fechas a consultar
    var estatus = $("#estatus").val();
    var id_cita = $("#id_cita").val();
    var usuario = $("#usuario").val();
    var comentario = $("#comentario").val();

    var renglon = $("#id_renglon").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"garantias/Operaciones/actulizaEstatus",
        method: 'post',
        data: {
            estatus : estatus,
            id_cita : id_cita,
            usuario : usuario,
            comentario : comentario,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //Verificamos que exista un resultado
                if (resp == "OK") {
                	$(".infoTituo").css('display','none');
                	$("#finalizado").text("Estatus actualizado");

                	var texto = $("#estatus").find('option:selected').text();
                	$("#estatusLB_"+renglon).text(texto);
                }else{
                    console.log("Error al enviar la actualización");
                }
            }else{
                console.log("Error al enviar la actualización");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre del error
        }
    //Cierre del ajax
    });
});	