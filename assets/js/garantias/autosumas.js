//Autosumas tabla de garantias

//Fila de partes
function autosuma_partes(fila){
    //Verificamos que no quede vacio el campo
    if ($("#partes_"+fila).val() == "") {
        $("#partes_"+fila).val("0");
    }else {
        //Recuperamos los valores de la fila
        var partes = parseFloat($("#partes_"+fila).val());
        var misc = parseFloat($("#misc_"+fila).val());
        var obra = parseFloat($("#obra_"+fila).val());
        // var iva = parseFloat($("#iva_"+fila).val());
        var cliente = parseFloat($("#cliente_"+fila).val());
        var distribuidor = parseFloat($("#distribuidor_"+fila).val());

        // var totalVertical = parseFloat($("#TotalPartes").val());
        // var totalHorizontal = parseFloat($("#TotalPartes").val());

        //Operación para suma vertical
        var sumatoriaVertical = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaVertical += parseFloat($("#partes_"+(i+1)).val());
        }

        //Operación para sumatoria horizontal
        var sumatoriaHoriz_1 = partes + obra + misc;
        var sumatoriaHoriz_iva = sumatoriaHoriz_1 * 0.16;
        var sumatoriaHoriz_2 = sumatoriaHoriz_1 + sumatoriaHoriz_iva;
        var sumatoriaHoriz_3 = sumatoriaHoriz_2 - cliente - distribuidor;

        //Sumatoria en vertical
        $("#TotalPartes").val(sumatoriaVertical);
        $("#labelTotalPartes").text(sumatoriaVertical.toFixed(2));

        //Sumatoria en horizontal
        $("#total_"+fila).val(sumatoriaHoriz_3);
        $("#labelTotal_"+fila).text(sumatoriaHoriz_3.toFixed(2));

        //Operación para suma vertical de la fila de totales
        var sumatoriaFinal = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaFinal += parseFloat($("#total_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalReparacion").text(sumatoriaFinal.toFixed(2));
        $("#TotalReparacion").val(sumatoriaFinal);

        //Actualización Iva
        $("#labelIva_"+fila).text(sumatoriaHoriz_iva.toFixed(2));
        $("#iva_"+fila).val(sumatoriaHoriz_iva);

        //Operación para suma vertical de la fila del iva
        var sumatoriaIVA = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaIVA += parseFloat($("#iva_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalIva").text(sumatoriaIVA.toFixed(2));
        $("#TotalIva").val(sumatoriaIVA);

    }

    //Agregamos los valores a los contenedores finales
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+fila).val();
    var campo_2 = $("#reparacion_"+fila).val();
    var campo_3 = $("#autoriz_n1_"+fila).val();
    var campo_4 = $("#autoriz_n2_"+fila).val();
    var campo_5 = $("#partes_"+fila).val();
    var campo_6 = $("#obra_"+fila).val();
    var campo_7 = $("#misc_"+fila).val();
    var campo_8 = $("#iva_"+fila).val();
    var campo_9 = $("#cliente_"+fila).val();
    var campo_10 = $("#distribuidor_"+fila).val();
    var campo_11 = $("#total_"+fila).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+fila).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}

//Mano de obra
function autosuma_MObra(fila){
    if ($("#obra_"+fila).val() == "") {
        $("#obra_"+fila).val("0");
    }else {
        //Recuperamos los valores de la fila
        var partes = parseFloat($("#partes_"+fila).val());
        var misc = parseFloat($("#misc_"+fila).val());
        var obra = parseFloat($("#obra_"+fila).val());
        // var iva = parseFloat($("#iva_"+fila).val());
        var cliente = parseFloat($("#cliente_"+fila).val());
        var distribuidor = parseFloat($("#distribuidor_"+fila).val());

        // var totalVertical = parseFloat($("#TotalPartes").val());
        // var totalHorizontal = parseFloat($("#TotalPartes").val());

        //Operación para suma vertical
        var sumatoriaVertical = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaVertical += parseFloat($("#obra_"+(i+1)).val());
        }

        //Operación para sumatoria horizontal
        var sumatoriaHoriz_1 = partes + obra + misc;
        var sumatoriaHoriz_iva = sumatoriaHoriz_1 * 0.16;
        var sumatoriaHoriz_2 = sumatoriaHoriz_1 + sumatoriaHoriz_iva;
        var sumatoriaHoriz_3 = sumatoriaHoriz_2 - cliente - distribuidor;

        //Sumatoria en vertical
        $("#TotalObra").val(sumatoriaVertical);
        $("#labelTotalObra").text(sumatoriaVertical.toFixed(2));

        //Sumatoria en horizontal
        $("#total_"+fila).val(sumatoriaHoriz_3);
        $("#labelTotal_"+fila).text(sumatoriaHoriz_3.toFixed(2));

        //Operación para suma vertical de la fila de totales
        var sumatoriaFinal = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaFinal += parseFloat($("#total_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalReparacion").text(sumatoriaFinal.toFixed(2));
        $("#TotalReparacion").val(sumatoriaFinal);

        //Actualización Iva
        $("#labelIva_"+fila).text(sumatoriaHoriz_iva.toFixed(2));
        $("#iva_"+fila).val(sumatoriaHoriz_iva);

        //Operación para suma vertical de la fila del iva
        var sumatoriaIVA = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaIVA += parseFloat($("#iva_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalIva").text(sumatoriaIVA.toFixed(2));
        $("#TotalIva").val(sumatoriaIVA);
    }

    //Agregamos los valores a los contenedores finales
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+fila).val();
    var campo_2 = $("#reparacion_"+fila).val();
    var campo_3 = $("#autoriz_n1_"+fila).val();
    var campo_4 = $("#autoriz_n2_"+fila).val();
    var campo_5 = $("#partes_"+fila).val();
    var campo_6 = $("#obra_"+fila).val();
    var campo_7 = $("#misc_"+fila).val();
    var campo_8 = $("#iva_"+fila).val();
    var campo_9 = $("#cliente_"+fila).val();
    var campo_10 = $("#distribuidor_"+fila).val();
    var campo_11 = $("#total_"+fila).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+fila).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}

//MISC
function autosuma_misc(fila){
    if ($("#misc_"+fila).val() == "") {
        $("#misc_"+fila).val("0");
    }else {
        //Recuperamos los valores de la fila
        var partes = parseFloat($("#partes_"+fila).val());
        var misc = parseFloat($("#misc_"+fila).val());
        var obra = parseFloat($("#obra_"+fila).val());
        // var iva = parseFloat($("#iva_"+fila).val());
        var cliente = parseFloat($("#cliente_"+fila).val());
        var distribuidor = parseFloat($("#distribuidor_"+fila).val());

        // var totalVertical = parseFloat($("#TotalPartes").val());
        // var totalHorizontal = parseFloat($("#TotalPartes").val());

        //Operación para suma vertical
        var sumatoriaVertical = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaVertical += parseFloat($("#misc_"+(i+1)).val());
        }

        //Operación para sumatoria horizontal
        var sumatoriaHoriz_1 = partes + obra + misc;
        var sumatoriaHoriz_iva = sumatoriaHoriz_1 * 0.16;
        var sumatoriaHoriz_2 = sumatoriaHoriz_1 + sumatoriaHoriz_iva;
        var sumatoriaHoriz_3 = sumatoriaHoriz_2 - cliente - distribuidor;

        //Sumatoria en vertical
        $("#TotalMisc").val(sumatoriaVertical);
        $("#labelTotalMisc").text(sumatoriaVertical.toFixed(2));

        //Sumatoria en horizontal
        $("#total_"+fila).val(sumatoriaHoriz_3);
        $("#labelTotal_"+fila).text(sumatoriaHoriz_3.toFixed(2));

        //Operación para suma vertical de la fila de totales
        var sumatoriaFinal = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaFinal += parseFloat($("#total_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalReparacion").text(sumatoriaFinal.toFixed(2));
        $("#TotalReparacion").val(sumatoriaFinal);

        //Actualización Iva
        $("#labelIva_"+fila).text(sumatoriaHoriz_iva.toFixed(2));
        $("#iva_"+fila).val(sumatoriaHoriz_iva);

        //Operación para suma vertical de la fila del iva
        var sumatoriaIVA = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaIVA += parseFloat($("#iva_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalIva").text(sumatoriaIVA.toFixed(2));
        $("#TotalIva").val(sumatoriaIVA);
    }

    //Agregamos los valores a los contenedores finales
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+fila).val();
    var campo_2 = $("#reparacion_"+fila).val();
    var campo_3 = $("#autoriz_n1_"+fila).val();
    var campo_4 = $("#autoriz_n2_"+fila).val();
    var campo_5 = $("#partes_"+fila).val();
    var campo_6 = $("#obra_"+fila).val();
    var campo_7 = $("#misc_"+fila).val();
    var campo_8 = $("#iva_"+fila).val();
    var campo_9 = $("#cliente_"+fila).val();
    var campo_10 = $("#distribuidor_"+fila).val();
    var campo_11 = $("#total_"+fila).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+fila).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}

//Cliente
function autosuma_cliente(fila){
    if ($("#cliente_"+fila).val() == "") {
        $("#cliente_"+fila).val("0");
    }else {
        //Recuperamos los valores de la fila
        var partes = parseFloat($("#partes_"+fila).val());
        var misc = parseFloat($("#misc_"+fila).val());
        var obra = parseFloat($("#obra_"+fila).val());
        // var iva = parseFloat($("#iva_"+fila).val());
        var cliente = parseFloat($("#cliente_"+fila).val());
        var distribuidor = parseFloat($("#distribuidor_"+fila).val());

        // var totalVertical = parseFloat($("#TotalPartes").val());
        // var totalHorizontal = parseFloat($("#TotalPartes").val());

        //Operación para suma vertical
        var sumatoriaVertical = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaVertical += parseFloat($("#cliente_"+(i+1)).val());
        }

        //Operación para sumatoria horizontal
        var sumatoriaHoriz_1 = partes + obra + misc;
        var sumatoriaHoriz_iva = sumatoriaHoriz_1 * 0.16;
        var sumatoriaHoriz_2 = sumatoriaHoriz_1 + sumatoriaHoriz_iva;
        var sumatoriaHoriz_3 = sumatoriaHoriz_2 - cliente - distribuidor;

        //Sumatoria en vertical
        $("#TotalCliente").val(sumatoriaVertical);
        $("#labelTotalCliente").text(sumatoriaVertical.toFixed(2));

        //Sumatoria en horizontal
        $("#total_"+fila).val(sumatoriaHoriz_3);
        $("#labelTotal_"+fila).text(sumatoriaHoriz_3.toFixed(2));

        //Operación para suma vertical de la fila de totales
        var sumatoriaFinal = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaFinal += parseFloat($("#total_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalReparacion").text(sumatoriaFinal.toFixed(2));
        $("#TotalReparacion").val(sumatoriaFinal);

        //Actualización Iva
        $("#labelIva_"+fila).text(sumatoriaHoriz_iva.toFixed(2));
        $("#iva_"+fila).val(sumatoriaHoriz_iva);

        //Operación para suma vertical de la fila del iva
        var sumatoriaIVA = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaIVA += parseFloat($("#iva_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalIva").text(sumatoriaIVA.toFixed(2));
        $("#TotalIva").val(sumatoriaIVA);
    }

    //Agregamos los valores a los contenedores finales
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+fila).val();
    var campo_2 = $("#reparacion_"+fila).val();
    var campo_3 = $("#autoriz_n1_"+fila).val();
    var campo_4 = $("#autoriz_n2_"+fila).val();
    var campo_5 = $("#partes_"+fila).val();
    var campo_6 = $("#obra_"+fila).val();
    var campo_7 = $("#misc_"+fila).val();
    var campo_8 = $("#iva_"+fila).val();
    var campo_9 = $("#cliente_"+fila).val();
    var campo_10 = $("#distribuidor_"+fila).val();
    var campo_11 = $("#total_"+fila).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+fila).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}

//Distribuidor
function autosuma_distribuidor(fila){
    if ($("#distribuidor_"+fila).val() == "") {
        $("#distribuidor_"+fila).val("0");
    }else {
        //Recuperamos los valores de la fila
        var partes = parseFloat($("#partes_"+fila).val());
        var misc = parseFloat($("#misc_"+fila).val());
        var obra = parseFloat($("#obra_"+fila).val());
        // var iva = parseFloat($("#iva_"+fila).val());
        var cliente = parseFloat($("#cliente_"+fila).val());
        var distribuidor = parseFloat($("#distribuidor_"+fila).val());

        // var totalVertical = parseFloat($("#TotalPartes").val());
        // var totalHorizontal = parseFloat($("#TotalPartes").val());

        //Operación para suma vertical
        var sumatoriaVertical = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaVertical += parseFloat($("#distribuidor_"+(i+1)).val());
        }

        //Operación para sumatoria horizontal
        var sumatoriaHoriz_1 = partes + obra + misc;
        var sumatoriaHoriz_iva = sumatoriaHoriz_1 * 0.16;
        var sumatoriaHoriz_2 = sumatoriaHoriz_1 + sumatoriaHoriz_iva;
        var sumatoriaHoriz_3 = sumatoriaHoriz_2 - cliente - distribuidor;

        //Sumatoria en vertical
        $("#TotalDistribuidor").val(sumatoriaVertical);
        $("#labelTotalDistribuidor").text(sumatoriaVertical.toFixed(2));

        //Sumatoria en horizontal
        $("#total_"+fila).val(sumatoriaHoriz_3);
        $("#labelTotal_"+fila).text(sumatoriaHoriz_3.toFixed(2));

        //Operación para suma vertical de la fila de totales
        var sumatoriaFinal = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaFinal += parseFloat($("#total_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalReparacion").text(sumatoriaFinal.toFixed(2));
        $("#TotalReparacion").val(sumatoriaFinal);

        //Actualización Iva
        $("#labelIva_"+fila).text(sumatoriaHoriz_iva.toFixed(2));
        $("#iva_"+fila).val(sumatoriaHoriz_iva);

        //Operación para suma vertical de la fila del iva
        var sumatoriaIVA = 0;
        for (var i = 0; i < fila; i++) {
            sumatoriaIVA += parseFloat($("#iva_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalIva").text(sumatoriaIVA.toFixed(2));
        $("#TotalIva").val(sumatoriaIVA);
    }

    //Agregamos los valores a los contenedores finales
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+fila).val();
    var campo_2 = $("#reparacion_"+fila).val();
    var campo_3 = $("#autoriz_n1_"+fila).val();
    var campo_4 = $("#autoriz_n2_"+fila).val();
    var campo_5 = $("#partes_"+fila).val();
    var campo_6 = $("#obra_"+fila).val();
    var campo_7 = $("#misc_"+fila).val();
    var campo_8 = $("#iva_"+fila).val();
    var campo_9 = $("#cliente_"+fila).val();
    var campo_10 = $("#distribuidor_"+fila).val();
    var campo_11 = $("#total_"+fila).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+fila).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}
