function verificaGarantias(){
    console.log("Llenado garantias");
    //Recuperamos el numero de orden
    var orden = $("#noOrdenServicio").val().trim();
    var base = $("#basePeticion").val();

    //Verificamos si existe una garantia para esa orden
    if (orden != "") {
        $.ajax({
            url: base+"garantias/Operaciones/loadDataForm",
            method: 'post',
            data: {
                idOrden: orden,
            },
            success:function(resp){
                // console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
                    //Fraccionamos los datos para darlos de alta
                    var campos = resp.split("_");
                    // console.log(campos);
                    //Si la garantia existe redireccionamos a la carga
                    if (campos[0] == "EXISTE") {
                        //Redireccionamos
                        location.href = base+"Garantia/"+orden+"/";
                    //Si no existe la garantia, cargamos los datos
                    }else {
                          $("#idAsesor").val(campos[1]);
                          $("#placas").val(campos[2]);
                          $("#anio").val(campos[3]);
                          $("#tipo").val(campos[4]);
                          $("#telefono").val(campos[5]);
                          $("#nombreProp").val(campos[6]);
                          $("#clienteVisita").val(campos[7]);
                          $("#direccion").val(campos[8]);
                          $("#estado").val(campos[9]);
                          $("#cPostal").val(campos[10]);
                          //Serie del vehiculo
                          if (campos[11].length > 5) {
                              for (var i = 0; i< campos[11].length; i++) {
                                  $("#idVehiculo_"+(i+1)).val(campos[11].charAt(i));
                              }
                          }

                          $("#noTorre").val(campos[12]);

                          //Kilimetraje del vehiculo recibido
                          if (campos[13].length >2) {
                              var casilla = 6;
                              var contenido = [];
                              for (var i = (campos[13].length-1); i >= 0 ; i--) {
                                  $("#kmRecibe_"+casilla).val(campos[13].charAt(i));
                                  casilla--;
                              }
                          }

                          $("#tecniconame").val(campos[14]);

                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
}
