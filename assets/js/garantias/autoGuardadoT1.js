function garantiaT1(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+indice).val();
    var campo_2 = $("#reparacion_"+indice).val();
    var campo_3 = $("#autoriz_n1_"+indice).val();
    var campo_4 = $("#autoriz_n2_"+indice).val();
    var campo_5 = $("#partes_"+indice).val();
    var campo_6 = $("#obra_"+indice).val();
    var campo_7 = $("#misc_"+indice).val();
    var campo_8 = $("#iva_"+indice).val();
    var campo_9 = $("#cliente_"+indice).val();
    var campo_10 = $("#distribuidor_"+indice).val();
    var campo_11 = $("#total_"+indice).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}

function reparacionT1(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+indice).val();
    var campo_2 = $("#reparacion_"+indice).val();
    var campo_3 = $("#autoriz_n1_"+indice).val();
    var campo_4 = $("#autoriz_n2_"+indice).val();
    var campo_5 = $("#partes_"+indice).val();
    var campo_6 = $("#obra_"+indice).val();
    var campo_7 = $("#misc_"+indice).val();
    var campo_8 = $("#iva_"+indice).val();
    var campo_9 = $("#cliente_"+indice).val();
    var campo_10 = $("#distribuidor_"+indice).val();
    var campo_11 = $("#total_"+indice).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}

function autoriz_n1T1(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+indice).val();
    var campo_2 = $("#reparacion_"+indice).val();
    var campo_3 = $("#autoriz_n1_"+indice).val();
    var campo_4 = $("#autoriz_n2_"+indice).val();
    var campo_5 = $("#partes_"+indice).val();
    var campo_6 = $("#obra_"+indice).val();
    var campo_7 = $("#misc_"+indice).val();
    var campo_8 = $("#iva_"+indice).val();
    var campo_9 = $("#cliente_"+indice).val();
    var campo_10 = $("#distribuidor_"+indice).val();
    var campo_11 = $("#total_"+indice).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}

function autoriz_n2T1(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#garantia_"+indice).val();
    var campo_2 = $("#reparacion_"+indice).val();
    var campo_3 = $("#autoriz_n1_"+indice).val();
    var campo_4 = $("#autoriz_n2_"+indice).val();
    var campo_5 = $("#partes_"+indice).val();
    var campo_6 = $("#obra_"+indice).val();
    var campo_7 = $("#misc_"+indice).val();
    var campo_8 = $("#iva_"+indice).val();
    var campo_9 = $("#cliente_"+indice).val();
    var campo_10 = $("#distribuidor_"+indice).val();
    var campo_11 = $("#total_"+indice).val();

    //vaciamos los valores en el contenedor
    //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
    $("#contenedor_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11);
}

$('#busqueda_tabla').keyup(function () {
    toSearch();
});


//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}
