//Designamos la tabla de la vista
var tabla = $("#cuerpoProblema");

$("#agregarT1").on('click',function(){
    //Recuperamos el indice del renglon
    var indice = $("#indice").val();

    //Recuperamos la informacion de los campos
    var campo_1 = $("#cometario_a_"+indice).val();
    var campo_2 = $("#ref_a_"+indice).val();
    var campo_3 = $("#clave_a_"+indice).val();
    var campo_4 = $("#fecha_a_"+indice).val();
    var campo_5 = $("#mecanico_"+indice).val();
    var campo_6 = $("#costo_"+indice).val();
    var campo_7 = $("#tinicio_a_"+indice).val();
    var campo_8 = $("#cometario_b_"+indice).val();
    var campo_9 = $("#ref_b_"+indice).val();
    var campo_10 = $("#clave_b_"+indice).val();
    var campo_11 = $("#fecha_b_"+indice).val();
    var campo_12 = $("#tinicio_b_"+indice).val();

    //Validamos que no todos los campos esten vacios
    if ((campo_1 != "") && (campo_2 != "") && (campo_3 != "") && (campo_4 != "") && (campo_7 != "")) {
        //Bloqueamos los campos del renglon terminado
        $("#cometario_a_"+indice).prop('disabled',true);
        $("#ref_a_"+indice).prop('disabled',true);
        $("#clave_a_"+indice).prop('disabled',true);
        $("#fecha_a_"+indice).prop('disabled',true);
        $("#mecanico_"+indice).prop('disabled',true);
        $("#costo_"+indice).prop('disabled',true);
        $("#tinicio_a_"+indice).prop('disabled',true);
        $("#cometario_b_"+indice).prop('disabled',true);
        $("#ref_b_"+indice).prop('disabled',true);
        $("#clave_b_"+indice).prop('disabled',true);
        $("#fecha_b_"+indice).prop('disabled',true);
        $("#tinicio_b_"+indice).prop('disabled',true);

        //Grabamos la informacion en el contenedor
        $("#renglon_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12);

        //Aumentamos el indice para los renglones
        indice++;
        $("#indice").val(indice);

        //Creamos la fecha y hora para imprimir
        var hoy = new Date();
        if (hoy.getDate() <10) {
            var dia = "0"+hoy.getDate();
        } else {
            var dia = hoy.getDate();
        }

        if ((hoy.getMonth() +1) < 10) {
            var mes = "0"+(hoy.getMonth() +1);
        }else {
            var mes = (hoy.getMonth() +1);
        }

        var fecha = hoy.getFullYear()+"-"+mes+"-"+dia;
        var hora = hoy.getHours()+":"+hoy.getMinutes()

        tabla.append("<tr id='fila_a_"+indice+"'>"+
            "<td style='width:30%;border:1px solid;' align='center'>"+
                "<input type='text' class='input_field' id='cometario_a_"+indice+"' onblur='comentario_A("+indice+")'  value='' style='width:100%;'>"+
            "</td>"+
            "<td style='width:10%;border:1px solid;' align='center'>"+
                "<input type='text' class='input_field' id='ref_a_"+indice+"' onblur='ref_A("+indice+")'  value='' style='width:100%;'>"+
            "</td>"+
            "<td  style='width:10%;border:1px solid;' align='center'>"+
                "<input type='text' class='input_field' id='clave_a_"+indice+"' onblur='clave_A("+indice+")'  value='' style='width:100%;'>"+
            "</td>"+
            "<td align='center' style='width:10%;border:1px solid;'>"+
                "<input type='date' class='input_field' id='fecha_a_"+indice+"' onblur='retorno_A("+indice+")' value='"+fecha+"' max='"+fecha+"' min='"+fecha+"' style='width:100%;'>"+
            "</td>"+
            "<td align='center' style='width:10%;border:1px solid;' rowspan='2'>"+
                "<input type='text' class='input_field' id='mecanico_"+indice+"' onblur='mecanico("+indice+")'  value='' style='width:80%;'>"+
            "</td>"+
            "<td align='center' style='width:10%;border:1px solid;' rowspan='2'>"+
                "<input type='text' class='input_field' id='costo_"+indice+"' onblur='costo("+indice+")'  value='' style='width:80%;'>"+
            "</td>"+
            "<td align='left' style='width:15%;border:1px solid;'>"+
                "INICIO <input type='text' class='input_field' id='tinicio_a_"+indice+"' onblur='tInicio("+indice+")' value='"+hora+"' max='"+hora+"' min='"+hora+"' style='width:60%;'>"+
            "</td>"+
        "</tr>");

        tabla.append("<tr id='fila_b_"+indice+"'>"+
            "<td style='width:30%;border:1px solid;' align='center'>"+
                "<input type='text' class='input_field' id='cometario_b_"+indice+"' onblur='comentario_B("+indice+")'  value='' style='width:100%;'>"+
            "</td>"+
            "<td style='width:10%;border:1px solid;' align='center'>"+
                "<input type='text' class='input_field' id='ref_b_"+indice+"' onblur='ref_B("+indice+")'  value='' style='width:100%;'>"+
            "</td>"+
            "<td  style='width:10%;border:1px solid;' align='center'>"+
                "<input type='text' class='input_field' id='clave_b_"+indice+"' onblur='clave_B("+indice+")'  value='' style='width:100%;'>"+
            "</td>"+
            "<td align='center' style='width:10%;border:1px solid;'>"+
                "<input type='date' class='input_field' id='fecha_b_"+indice+"' onblur='retorno_B("+indice+")'  value='"+fecha+"' max='"+fecha+"' min='"+fecha+"' style='width:100%;'>"+
            "</td>"+
            "<td align='left' style='width:15%;border:1px solid;'>"+
                "TERMINACIÓN  <input type='text' class='input_field' id='tinicio_b_"+indice+"' onblur='tFinal("+indice+")'  value='"+hora+"' max='"+hora+"' min='"+hora+"' style='width:1.5cm;'>"+
                "<input type='hidden' id='renglon_"+indice+"' name='contenedor[]' value=''>"+
            "</td>"+
        "</tr>"
      );


    //De lo contrario regresamos el mando al campo faltante
    } else {
        if (campo_1 == "") {
            $("#cometario_a_"+indice).focus();
        }else if (campo_2 == "") {
            $("#ref_a_"+indice).focus();
        }else if (campo_3 == "") {
            $("#clave_a_"+indice).focus();
        }else if (campo_4 == "") {
            $("#fecha_a_"+indice).focus();
        }else {
            $("#tinicio_a_"+indice).focus();
        }
    }

});


//Boton en borrar un renglon
//Eliminamos la ultima fila ingresada (control de materiales)
$("#eliminarT1").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indice").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        var fila = $("#fila_a_"+indice);
        var fila_2 = $("#fila_b_"+indice);
        fila.remove();
        fila_2.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indice").val(indice);

        //Restablecemos los valores default del reglon anterior
        $("#cometario_a_"+indice).val("");
        $("#ref_a_"+indice).val("");
        $("#clave_a_"+indice).val("");
        $("#fecha_a_"+indice).val();
        $("#mecanico_"+indice).val("");
        $("#costo_"+indice).val("");
        $("#tinicio_a_"+indice).val();
        $("#cometario_b_"+indice).val("");
        $("#ref_b_"+indice).val("");
        $("#clave_b_"+indice).val("");
        $("#fecha_b_"+indice).val();
        $("#tinicio_b_"+indice).val();

        //Desbloqueamos el renglon para la edición
        $("#cometario_a_"+indice).prop('disabled',false);
        $("#ref_a_"+indice).prop('disabled',false);
        $("#clave_a_"+indice).prop('disabled',false);
        $("#fecha_a_"+indice).prop('disabled',false);
        $("#mecanico_"+indice).prop('disabled',false);
        $("#costo_"+indice).prop('disabled',false);
        $("#tinicio_a_"+indice).prop('disabled',false);
        $("#cometario_b_"+indice).prop('disabled',false);
        $("#ref_b_"+indice).prop('disabled',false);
        $("#clave_b_"+indice).prop('disabled',false);
        $("#fecha_b_"+indice).prop('disabled',false);
        $("#tinicio_b_"+indice).prop('disabled',false);
    }
});
