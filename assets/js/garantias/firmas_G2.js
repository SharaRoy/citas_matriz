$(document).ready(function() {
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 500) && (ancho < 1024)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.18;
        var nvo_ancho = ancho * 0.50;
        console.log("Tablet");
    }else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.25;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        // console.log(destino);

        if (destino == "Firma1") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirma1').val(data);
            $("#firma1").attr("src",data);
        }else {
          //Designamos el destino de la firma en la vista
          $('#rutaFirma2').val(data);
          $("#firma2").attr("src",data);
        }

        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
        var destino = $('#destinoFirma').val();
        // console.log(destino);

        if (destino == "Firma1") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirma1').val("");
            $("#firma1").attr("src","");
        }else { //Cliente
            //Designamos el destino de la firma en la vista
            $('#rutaFirma2').val("");
            $("#firma2").attr("src","");
        }

    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });

});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    //Pasamos el valor al formulario local
    $('#destinoFirma').val(firma);

    $("#firmaDigitalLabel").text("FIRMA");

});
