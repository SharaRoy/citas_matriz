var tabla_1 = $("#tabla_garantia");

//Tabla de control de materiales
$("#agregarT1").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTGarantia").val();

    //Recuperamos los campos de la fila default
    var campo_1 = $("#garantia_"+indice).val();
    var campo_2 = $("#reparacion_"+indice).val();
    var campo_3 = $("#autoriz_n1_"+indice).val();
    var campo_4 = $("#autoriz_n2_"+indice).val();
    var campo_5 = $("#partes_"+indice).val();
    var campo_6 = $("#obra_"+indice).val();
    var campo_7 = $("#iva_"+indice).val();
    var campo_8 = $("#cliente_"+indice).val();
    var campo_9 = $("#distribuidor_"+indice).val();
    var campo_10 = $("#total_"+indice).val();
    var campo_11 = $("#misc_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ((campo_1 != "")&&(campo_2 != "")&&(campo_3 != "")&&(campo_4 != "")&&(campo_5 != "")&&(campo_6 != "")){
        //Evitamos la edicion de los  elementos
        $("#garantia_"+indice).prop('disabled',true);
        $("#reparacion_"+indice).prop('disabled',true);
        $("#autoriz_n1_"+indice).prop('disabled',true);
        $("#autoriz_n2_"+indice).prop('disabled',true);
        $("#partes_"+indice).prop('disabled',true);
        $("#misc_"+indice).prop('disabled',true);
        $("#obra_"+indice).prop('disabled',true);
        // $("#iva_"+indice).prop('disabled',true);
        $("#cliente_"+indice).prop('disabled',true);
        $("#distribuidor_"+indice).prop('disabled',true);
        // $("#total_"+indice).prop('disabled','disabled');

        //    Tipo de garantia / daños / autoriz 1 / autoriz 2 / partes $ / obra $ / misc $ / iva $ / cliente $ / distribuidor $ / total $
        $("#contenedor_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_11+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10);

        //Aumentamos el indice para los renglones
        indice++;
        $("#indiceTGarantia").val(indice);

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla_1.append("<tr id='fila_reparacion_"+indice+"' style='border: 1px solid #340f7b;'>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "1"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='garantia_"+indice+"' onblur='garantiaT1("+indice+")' value='' style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='reparacion_"+indice+"' onblur='reparacionT1("+indice+")' value='' style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='autoriz_n1_"+indice+"' onblur='autoriz_n1T1("+indice+")' value='' style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='autoriz_n2_"+indice+"' onblur='autoriz_n2T1("+indice+")' value='' style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='partes_"+indice+"' onblur='autosuma_partes("+indice+")' value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='obra_"+indice+"' onblur='autosuma_MObra("+indice+")' value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='misc_"+indice+"' onblur='autosuma_misc("+indice+")' value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<label id='labelIva_"+indice+"'>0</label>"+
                "<input type='hidden' class='input_field' id='iva_"+indice+"' value='0'>"+
                // "<input type='text' class='input_field' id='iva_"+indice+"' onblur='autosuma_iva("+indice+")' value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='cliente_"+indice+"' onblur='autosuma_cliente("+indice+")' value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='distribuidor_"+indice+"' onblur='autosuma_distribuidor("+indice+")' value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<label id='labelTotal_"+indice+"'>0</label>"+
                "<input type='hidden' id='total_"+indice+"' value='0'>"+
                "<input type='hidden' id='contenedor_"+indice+"' name='contenedor[]' value=''>"+
            "</td>"+
        "</tr>");
    }else {
        //Regresamos el control al campo que falta rellenar
        if (campo_1 == "") {
            $("#garantia_"+indice).focus();
        }else if (campo_2 == "") {
            $("#reparacion_"+indice).focus();
        }else if (campo_3 == "") {
            $("#autoriz_n1_"+indice).focus();
        }else if (campo_4 == "") {
            $("#autoriz_n2_"+indice).focus();
        }else if (campo_5 == "") {
            $("#partes_"+indice).focus();
        }else{
            $("#obra_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada (control de materiales)
$("#eliminarT1").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTGarantia").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        var fila = $("#fila_reparacion_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceTGarantia").val(indice);

        //Restablecemos los valores default del reglon anterior
        $("#garantia_"+indice).val("");
        $("#reparacion_"+indice).val("");
        $("#autoriz_n1_"+indice).val("");
        $("#autoriz_n2_"+indice).val("");
        $("#partes_"+indice).val(0);
        $("#obra_"+indice).val(0);
        $("#iva_"+indice).val(0);
        $("#misc_"+indice).val(0);
        $("#cliente_"+indice).val(0);
        $("#distribuidor_"+indice).val(0);
        $("#total_"+indice).val(0);
        $("#contenedor_"+indice).val("");

        //Desbloqueamos el renglon para la edición
        $("#garantia_"+indice).prop('disabled',false);
        $("#reparacion_"+indice).prop('disabled',false);
        $("#autoriz_n1_"+indice).prop('disabled',false);
        $("#autoriz_n2_"+indice).prop('disabled',false);
        $("#partes_"+indice).prop('disabled',false);
        $("#obra_"+indice).prop('disabled',false);
        $("#misc_"+indice).prop('disabled',false);
        $("#cliente_"+indice).prop('disabled',false);
        $("#distribuidor_"+indice).prop('disabled',false);

        //Recalculamos los totales
        //Recuperamos los valores de la fila
        var partes = parseFloat($("#partes_"+indice).val());
        var misc = parseFloat($("#misc_"+indice).val());
        var obra = parseFloat($("#obra_"+indice).val());
        // var iva = parseFloat($("#iva_"+indice).val());
        var cliente = parseFloat($("#cliente_"+indice).val());
        var distribuidor = parseFloat($("#distribuidor_"+indice).val());

        //Sacamos el nuevo total de cada fila
        var sumatoriaP1 = 0;
        for (var i = 0; i < indice; i++) {
            sumatoriaP1 += parseFloat($("#partes_"+(i+1)).val());
        }
        $("#TotalPartes").val(sumatoriaP1);
        $("#labelTotalPartes").text(sumatoriaP1.toFixed(2));

        var sumatoriaP2 = 0;
        for (var i = 0; i < indice; i++) {
            sumatoriaP2 += parseFloat($("#obra_"+(i+1)).val());
        }
        $("#TotalObra").val(sumatoriaP2);
        $("#labelTotalObra").text(sumatoriaP2.toFixed(2));

        var sumatoriaP3 = 0;
        for (var i = 0; i < indice; i++) {
            sumatoriaP3 += parseFloat($("#misc_"+(i+1)).val());
        }
        $("#TotalMisc").val(sumatoriaP3);
        $("#labelTotalMisc").text(sumatoriaP3.toFixed(2));

        var sumatoriaP4 = 0;
        for (var i = 0; i < indice; i++) {
            sumatoriaP4 += parseFloat($("#cliente_"+(i+1)).val());
        }
        $("#TotalCliente").val(sumatoriaP4);
        $("#labelTotalCliente").text(sumatoriaP4.toFixed(2));

        var sumatoriaP5 = 0;
        for (var i = 0; i < indice; i++) {
            sumatoriaP5 += parseFloat($("#distribuidor_"+(i+1)).val());
        }
        $("#TotalDistribuidor").val(sumatoriaP5);
        $("#labelTotalDistribuidor").text(sumatoriaP5.toFixed(2));

        //Operación para sumatoria horizontal
        var sumatoriaHoriz_1 = partes + obra + misc;
        var sumatoriaHoriz_iva = sumatoriaHoriz_1 * 0.16;
        var sumatoriaHoriz_2 = sumatoriaHoriz_1 + sumatoriaHoriz_iva;
        var sumatoriaHoriz_3 = sumatoriaHoriz_2 - cliente - distribuidor;

        //Sumatoria en horizontal
        $("#total_"+indice).val(sumatoriaHoriz_3);
        $("#labelTotal_"+indice).text(sumatoriaHoriz_3.toFixed(2));

        //Operación para suma vertical de la indice de totales
        var sumatoriaFinal = 0;
        for (var i = 0; i < indice; i++) {
            sumatoriaFinal += parseFloat($("#total_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalReparacion").text(sumatoriaFinal.toFixed(2));
        $("#TotalReparacion").val(sumatoriaFinal);

        //Actualización Iva
        $("#labelIva_"+indice).text(sumatoriaHoriz_iva.toFixed(2));
        $("#iva_"+indice).val(sumatoriaHoriz_iva);

        //Operación para suma vertical de la indice del iva
        var sumatoriaIVA = 0;
        for (var i = 0; i < indice; i++) {
            sumatoriaIVA += parseFloat($("#iva_"+(i+1)).val());
        }

        //Sumatoria final
        $("#labelTotalIva").text(sumatoriaIVA.toFixed(2));
        $("#TotalIva").val(sumatoriaIVA);
    }
});

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

var tabla_2 = $("#tabla_partes");

//Tabla de control de materiales
$("#agregarT2").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indice_tabla_partes").val();

    //Recuperamos los campos de la fila default
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ((campo_1 != "")&&(campo_2 != "")&&(campo_3 != "")&&(campo_4 != "")&&(campo_5 != "")&&(campo_6 != "")){
        //Evitamos la edicion de los  elementos
        $("#prefijo_"+indice).prop('disabled',true);
        $("#basico_"+indice).prop('disabled',true);
        $("#sufijo_"+indice).prop('disabled',true);
        $("#nombre_"+indice).prop('disabled',true);
        $("#cantidad_"+indice).prop('disabled',true);
        $("#precio_"+indice).prop('disabled',true);
        $("#importePartes_"+indice).prop('disabled',true);
        $("#clave_"+indice).prop('disabled',true);
        $("#operacion_"+indice).prop('disabled',true);
        $("#tiempo_"+indice).prop('disabled',true);
        $("#mecanico_"+indice).prop('disabled',true);
        $("#importeObra_"+indice).prop('disabled',true);
        $("#codigo_"+indice).prop('disabled',true);
        $("#descripcion_"+indice).prop('disabled',true);

        //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
        $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);

        //Aumentamos el indice para los renglones
        indice++;
        $("#indice_tabla_partes").val(indice);

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla_2.append("<tr id='fila_partes_"+indice+"' style='border: 1px solid #340f7b;'>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                indice+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='prefijo_"+indice+"' value='' onblur='prefijoT2("+indice+")' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='basico_"+indice+"' value='' onblur='basicoT2("+indice+")' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='sufijo_"+indice+"' value='' onblur='sufijoT2("+indice+")' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='nombre_"+indice+"' value='' onblur='nombreT2("+indice+")' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='cantidad_"+indice+"' value='0' onblur='cantidadT2("+indice+")' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='precio_"+indice+"' value='0' onblur='precioT2("+indice+")' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='importePartes_"+indice+"' value='0' onblur='importePartesT2("+indice+")' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='clave_"+indice+"' value='' onblur='claveT2("+indice+")' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='operacion_"+indice+"' value='' onblur='operacionT2("+indice+")' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='tiempo_"+indice+"' value='0' onblur='tiempoT2("+indice+")' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='importeObra_"+indice+"' value='0' onblur='importeObraT2("+indice+")' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='mecanico_"+indice+"' value='' onblur='mecanicoT2("+indice+")' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='descripcion_"+indice+"' value='' onblur='descripcionT2("+indice+")' style='width:100%;'>"+
            "</td>"+
            "<td style='border-right: 1px solid #340f7b;' align='center'>"+
                "<input type='text' class='input_field' id='codigo_"+indice+"' value='' onblur='codigoT2("+indice+")' style='width:100%;'>"+
                "<input type='hidden' name='contenido[]' id='contenido_"+indice+"' value=''>"+
            "</td>"+
        "</tr>");
    }else {
        //Regresamos el control al campo que falta rellenar
        if (campo_1 == "") {
            $("#prefijo_"+indice).focus();
        }else if (campo_2 == "") {
            $("#basico_"+indice).focus();
        }else if (campo_3 == "") {
            $("#sufijo_"+indice).focus();
        }else if (campo_4 == "") {
            $("#nombre_"+indice).focus();
        }else if (campo_5 == "") {
            $("#cantidad_"+indice).focus();
        }else{
            $("#precio_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada (control de materiales)
$("#eliminarT2").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indice_tabla_partes").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        var fila = $("#fila_partes_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indice_tabla_partes").val(indice);

        //Restablecemos los valores default del reglon anterior
        $("#prefijo_"+indice).val("");
        $("#basico_"+indice).val("");
        $("#sufijo_"+indice).val("");
        $("#nombre_"+indice).val("");
        $("#cantidad_"+indice).val("0");
        $("#precio_"+indice).val("0");
        $("#importePartes_"+indice).val("0");
        $("#clave_"+indice).val("");
        $("#operacion_"+indice).val("");
        $("#tiempo_"+indice).val("0");
        $("#mecanico_"+indice).val("");
        $("#importeObra_"+indice).val("0");
        $("#codigo_"+indice).val("");
        $("#contenido_"+indice).val("");
        $("#descripcion_"+indice).val("");

        //Desbloqueamos el renglon para la edición
        $("#prefijo_"+indice).prop('disabled',false);
        $("#basico_"+indice).prop('disabled',false);
        $("#sufijo_"+indice).prop('disabled',false);
        $("#nombre_"+indice).prop('disabled',false);
        $("#cantidad_"+indice).prop('disabled',false);
        $("#precio_"+indice).prop('disabled',false);
        $("#importePartes_"+indice).prop('disabled',false);
        $("#clave_"+indice).prop('disabled',false);
        $("#operacion_"+indice).prop('disabled',false);
        $("#tiempo_"+indice).prop('disabled',false);
        $("#mecanico_"+indice).prop('disabled',false);
        $("#importeObra_"+indice).prop('disabled',false);
        $("#codigo_"+indice).prop('disabled',false);
        $("#descripcion_"+indice).prop('disabled',false);

    }
});
