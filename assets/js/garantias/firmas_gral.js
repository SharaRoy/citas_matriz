$(document).ready(function() {
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 500) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.50;
        console.log("Tablet");
    }else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        // console.log(destino);

        if (destino == "Cliente") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaCliente').val(data);
            $("#firmaImgCliente").attr("src",data);
        }else if (destino == "Gerente") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaGerente').val(data);
            $("#firmaImgGerente").attr("src",data);
        }else if (destino == "Tecnico") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaTecnico').val(data);
            $("#firmaFirmaTecnico").attr("src",data);
        }else if (destino == "Extra") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaProv').val(data);
            $("#firmaFirmaProv").attr("src",data);
        }else if (destino == "Firma1") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirma1').val(data);
            $("#firma1").attr("src",data);
        }else if (destino == "Firma2") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirma2').val(data);
            $("#firma2").attr("src",data);
        }

        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
        var destino = $('#destinoFirma').val();
        // console.log(destino);

        if (destino == "Cliente") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaCliente').val("");
            $("#firmaImgCliente").attr("src","");
        }else if (destino == "Gerente") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaGerente').val("");
            $("#firmaImgGerente").attr("src","");
        }else if (destino == "Tecnico") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaTecnico').val("");
            $("#firmaFirmaTecnico").attr("src","");
        }else if (destino == "Extra") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaProv').val("");
            $("#firmaFirmaProv").attr("src","");
        }else if (destino == "Firma1") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaTecnico').val("");
            $("#firmaFirmaTecnico").attr("src","");
        }else if (destino == "Firma2") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaProv').val("");
            $("#firmaFirmaProv").attr("src","");
        }

    });

});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    //Pasamos el valor al formulario local
    $('#destinoFirma').val(firma);

    if (firma == "Cliente") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL CLIENTE");
    }else if (firma == "Gerente") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL GERENTE DE SERVICIO");
    }else if (firma == "Tecnico") {
        //Designamos el destino de la firma en la vista
        $("#firmaDigitalLabel").text("FIRMA DEL TÉCNICO");
    }else if (firma == "Extra") {
        //Designamos el destino de la firma en la vista
        $("#firmaDigitalLabel").text("FIRMA EXTRA");
    }else if (firma == "Firma1") {
        //Designamos el destino de la firma en la vista
        $("#firmaDigitalLabel").text("FIRMA RERVISOR 1");
    }else if (firma == "Firma2") {
        //Designamos el destino de la firma en la vista
        $("#firmaDigitalLabel").text("FIRMA REVISOR 2");
    }

});
