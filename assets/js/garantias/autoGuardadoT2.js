function prefijoT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function basicoT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function sufijoT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function nombreT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function cantidadT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function precioT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function importePartesT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function claveT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function operacionT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function tiempoT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function importeObraT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function mecanicoT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function descripcionT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}

function codigoT2(indice){
    //Recuperamos los campos de la fila
    var campo_1 = $("#prefijo_"+indice).val();
    var campo_2 = $("#basico_"+indice).val();
    var campo_3 = $("#sufijo_"+indice).val();
    var campo_4 = $("#nombre_"+indice).val();
    var campo_5 = $("#cantidad_"+indice).val();
    var campo_6 = $("#precio_"+indice).val();
    var campo_7 = $("#importePartes_"+indice).val();
    var campo_8 = $("#clave_"+indice).val();
    var campo_9 = $("#operacion_"+indice).val();
    var campo_10 = $("#tiempo_"+indice).val();
    var campo_11 = $("#importeObra_"+indice).val();
    var campo_12 = $("#mecanico_"+indice).val();
    var campo_13 = $("#descripcion_"+indice).val();
    var campo_14 = $("#codigo_"+indice).val();

    //    prefijo / basico / sufijo / nombre / cantidad / precio / importe partes / clave / operacion / tiempo / importe obra / mecanico clave / descripcion / codigo
    $("#contenido_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8+"_"+campo_9+"_"+campo_10+"_"+campo_11+"_"+campo_12+"_"+campo_13+"_"+campo_14);
}
