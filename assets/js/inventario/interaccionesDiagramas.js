//Interacciones de dibujo (daños)
//Cargamos la imagen para daños
var canvas_3 = document.getElementById('canvas_3');
var ctx_3 = canvas_3.getContext("2d");
var background = new Image();

if ($("#danosMarcas").val() == "") {
    background.src = $("#direccionFondo").val();
} else {
    background.src = $("#danosMarcas").val();
}

background.onload = function(){
    ctx_3.drawImage(background,0,0,420,400);
}

$("#canvas_3").click(function(e){
    //Indicamos que ya se hizo al menos un señalamiento
    $("#validaClick_1").val("1");
    getPosition(e);
});

function getPosition(event){
    var rect = canvas_3.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    //Recuperamos el tipo de marca que se pondra
    var marca = $("input[name='marcasRadio']:checked").val();
    switch(marca)
    {
        case 'hit':
            draw(ctx_3, x, y, 8);
        break;
        case 'broken':
            drawX(ctx_3,x, y, 3);
        break;
        case 'scratch':
            drawZ(ctx_3,x, y, 5);
        break;
        default:
        break;
    }
}

//Funciones de dibujo para diagrama de daños
function draw(ctx,x,y,size) {
    ctx.fillStyle = "#ffffff"
    ctx.strokeStyle = "#ff0000"
    ctx.beginPath()
    ctx.arc(x, y, size, 0, Math.PI*2, true)
    ctx.closePath()
    ctx.fill()
    ctx.stroke()
}

function drawX(ctx,x,y,size){
     ctx.beginPath();
     ctx.strokeStyle = "#00ff00";
     ctx.moveTo(x - 15, y - 15);
     ctx.lineTo(x + 15, y + 15);

     ctx.moveTo(x + 15, y - 15);
     ctx.lineTo(x - 15, y + 15);
     ctx.stroke();
}

function drawZ(ctx,x,y,size){
     // ctx.font = "15px Arial";
     ctx.fillStyle = "#0000ff";
     ctx.font = "20px Arial";
     ctx.fillText("#", x, y);
}

//Orden de servicio descarga de imagenes
if ($("#cargaFormulario").val() == "1") {
    var button = document.getElementById('btn-download');
    button.addEventListener('click', function (e) {
        var dataURL = canvas_3.toDataURL('image/png');
        button.href = dataURL;
    });
}

function draw(ctx,x,y,size) {
    ctx.fillStyle = "#ffffff"
    ctx.strokeStyle = "#ff0000"
    ctx.beginPath()
    ctx.arc(x, y, size, 0, Math.PI*2, true)
    ctx.closePath()
    ctx.fill()
    ctx.stroke()
}

//Presionamos el boton de enviado
$("#enviarOrdenForm").on('click',function(){
    if ($("#cargaFormulario").val() == "1") {
        var dataURL = canvas_3.toDataURL('image/png');
        $("#danosMarcas").val(dataURL);
    }
    $('input').attr('disabled',false);
    document.forms[0].submit();
});

//Presionamos el boton para resetear la imagen (diagrama)
$("#resetoeDiagrama").on('click',function(){
    background.src = $("#direccionFondo").val();
});
