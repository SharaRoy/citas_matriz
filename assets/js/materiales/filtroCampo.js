//Condicionamos las busquedas por fecha 
/*$('#fecha_inicio').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_fin").attr('min',fecha_inicio);
    //console.log(fecha_inicio)
});*/

/*$('#fecha_fin').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_inicio").attr('max',fecha_fin);
    //console.log(fecha_fin)
});*/

//Busqueda en cotizaciones creadas para modificar (Tecnico)
$('#btnBusqueda_fechas').on('click',function(){
    console.log("Buscando por id Orden");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    //var fecha_inicio = $("#fecha_inicio").val();
    //var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_campo").val();
    var indicador = $("#indicador").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"multipunto/Cotizaciones/filtroCotizaciones",
        method: 'post',
        data: {
            indicador : indicador,
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        //console.log(celdas);

                        var indicador = celdas[0];
                        var base_2 = $("#basePeticion").val().trim();                       

                        //Validamos la la aprovacion del cliente
                        var apruebaCliente = '';
                        if (celdas[7] != "") {
                            if (celdas[7] == 'Si') {
                                apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                            } else if(celdas[7] == 'Val'){
                                apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                            } else{
                                apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                            }
                        } else{
                            apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                        }

                        if (indicador == "ACT") {
                            var edicion = "href='"+base_2+"Cotizacion_Actualiza/"+celdas[1]+"'";
                        }         
                        
                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:12px;'>"+
                            //Indice
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                            //NO. ORDEN
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[1]+"</td>"+
                            //NO. INTELISIS
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[16]+"</td>"+
                            //FECHA REGISTRO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[2]+"</td>"+
                            //VEHÍCULO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[3]+"</td>"+
                            //PLACAS
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[4]+"</td>"+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[5]+"</td>"+
                            //TÉCNICO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[6]+"</td>"+
                            //APRUEBA CLIENTE
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                ""+apruebaCliente+""
                            +"</td>"+
                            //ACCIONES
                            //Editar cotizacion
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                ""+"<a class='btn btn-warning' style='color:white;' "+edicion+">Editar</a>"+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Busquedas del asesor
$('#btnBusqueda_fechas_coti').on('click',function(){
    console.log("Buscando por numero de orden");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    //var fecha_inicio = $("#fecha_inicio").val();
    //var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_campo_coti").val();
    var indicador = $("#indicador").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"multipunto/Cotizaciones/filtroCotizaciones",
        method: 'post',
        data: {
            indicador : indicador,
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    var rol = $("#rolBusqueda").val().trim();

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        //console.log(celdas);

                        var indicador = celdas[0];
                        var base_2 = $("#basePeticion").val().trim();

                        //Validamos la la aprovacion del cliente
                        var apruebaCliente = '';
                        if (celdas[7] != "") {
                            if (celdas[7] == 'Si') {
                                apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                            } else if(celdas[7] == 'Val'){
                                apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                            } else{
                                apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                            }
                        } else{
                            apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                        }

                        var estadoRefaccion = "";
                        var estadoRefaccionColor = ""

                        if (celdas[7] == "No") {
                            estadoRefaccionColor = "background-color:red;color:white;";
                        } else {
                            if (celdas[14] == "0") {
                                estadoRefaccionColor = "background-color:#f5acaa;";
                                estadoRefaccion = "SIN SOLICITAR";
                            } else if (celdas[14] == "1") {
                                estadoRefaccionColor = "background-color:#f5ff51;";
                                estadoRefaccion = "SOLICIATADAS";
                            } else if (celdas[14] == "2") {
                                estadoRefaccionColor = "background-color:#c7ecc7;";
                                estadoRefaccion = "ENTREGADAS";
                            } else {
                                estadoRefaccionColor = "background-color:#5bc0de;";
                                estadoRefaccion = "RECIBIDAS";
                            }
                        }

                        var acciones = "";
                        //Si ya firmo el asesor y aun no se afecta por el cliente
                        if ((celdas[7] == "")&&(celdas[8] == "SI")) {
                            acciones = "<a href='"+base_2+"Cotizacion_Cliente/"+celdas[1]+"' class='btn btn-primary'>"+
                                            "Aprobar/Rechazar"+
                                        "</a>";
                        } else {
                            acciones = "<a href='"+base_2+"Multipunto_Cotizacion/"+celdas[1]+"' class='btn btn-info'>"+
                                            "VER"+
                                        "</a>";
                        }

                        if (indicador == "ACT") {
                            var edicion = "href='"+base_2+"Cotizacion_Actualiza/"+celdas[1]+"'";
                        }         
                        
                        console.log(rol);
                        if ((rol == "ASE")&&(celdas[13] == "1")) {
                             //Creamos las celdas en la tabla
                            tabla.append("<tr style='font-size:12px;'>"+
                                //Indice
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                                //NO. ORDEN
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[1]+"</td>"+
                                //FECHA REGISTRO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[2]+"</td>"+
                                //VEHÍCULO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[3]+"</td>"+
                                //PLACAS
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[4]+"</td>"+
                                //ASESOR
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[5]+"</td>"+
                                //TÉCNICO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[6]+"</td>"+
                                //FIRMA ASESOR
                                "<td align='center' style='vertical-align: middle;width:8%;'>"+celdas[8]+"</td>"+
                                //APRUEBA CLIENTE
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                    ""+apruebaCliente+""
                                +"</td>"+
                                //EDO. REFACCIONES
                                "<td align='center' style='vertical-align: middle;width:15%;"+estadoRefaccionColor+"'>"+
                                    ""+estadoRefaccion+""
                                +"</td>"+
                                //ACCIONES
                                //Editar cotizacion
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                    ""+acciones+""+
                                "</td>"+
                            "</tr>");
                        } else {
                             //Creamos las celdas en la tabla
                            tabla.append("<tr style='font-size:12px;'>"+
                                //Indice
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                                //NO. ORDEN
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[1]+"</td>"+
                                //FECHA REGISTRO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[2]+"</td>"+
                                //VEHÍCULO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[3]+"</td>"+
                                //PLACAS
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[4]+"</td>"+
                                //ASESOR
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[5]+"</td>"+
                                //TÉCNICO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[6]+"</td>"+
                                //FIRMA ASESOR
                                "<td align='center' style='vertical-align: middle;width:8%;'>"+celdas[8]+"</td>"+
                                //APRUEBA CLIENTE
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                    ""+apruebaCliente+""
                                +"</td>"+
                                //EDO. REFACCIONES
                                "<td align='center' style='vertical-align: middle;width:15%;"+estadoRefaccionColor+"'>"+
                                    ""+estadoRefaccion+""
                                +"</td>"+
                                //ACCIONES
                                //Editar cotizacion
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                    ""+acciones+""+
                                "</td>"+
                            "</tr>");
                        }
                       
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Busquedas del asesor
$('#btnBusqueda_fechas_jdt').on('click',function(){
    console.log("Buscando por numero de orden jdt");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    //var fecha_inicio = $("#fecha_inicio").val();
    //var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_jdt_coti").val();
    var indicador = $("#indicador").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"multipunto/Cotizaciones/filtroCotizaciones",
        method: 'post',
        data: {
            indicador : indicador,
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        //console.log(celdas);

                        var indicador = celdas[0];
                        var base_2 = $("#basePeticion").val().trim();                       

                        //Validamos la la aprovacion del cliente
                        var apruebaCliente = '';
                        if (celdas[7] != "") {
                            if (celdas[7] == 'Si') {
                                apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                            } else if(celdas[7] == 'Val'){
                                apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                            } else{
                                apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                            }
                        } else{
                            apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                        }

                        var acciones = "";
                        //Si ya firmo el asesor y aun no se afecta por el cliente
                        if (celdas[13] == "1") {
                            acciones = "<a href='"+base_2+"Cotizacion_Revisa_JDT/"+celdas[1]+"' class='btn btn-primary'>"+
                                            "VER"+
                                        "</a>";
                        } else {
                            acciones = "<a href='"+base_2+"Cotizacion_Revisa_JDT/"+celdas[1]+"' class='btn btn-info'>"+
                                            "EDITAR"+
                                        "</a>";
                        }

                        if (indicador == "ACT") {
                            var edicion = "href='"+base_2+"Cotizacion_Actualiza/"+celdas[1]+"'";
                        }         
                        
                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:12px;'>"+
                            //Indice
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                            //NO. ORDEN
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[1]+"</td>"+
                            //FECHA REGISTRO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[2]+"</td>"+
                            //VEHÍCULO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[3]+"</td>"+
                            //PLACAS
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[4]+"</td>"+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[5]+"</td>"+
                            //TÉCNICO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[6]+"</td>"+
                            //APRUEBA CLIENTE
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                ""+apruebaCliente+""
                            +"</td>"+
                            //ACCIONES
                            //Editar cotizacion
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                ""+acciones+""+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

function addAudio(renglon) {
    var base = $("#basePeticion").val();
    var orden = $("#audio_"+renglon).data("orden");
    var audio = $("#audio_"+renglon).data("value");

    //Imprimimos en el modal la informacion
    $("#tituloAudio").text("Evidencia de audio No. de Orden: "+orden);
    $("#reproductor").attr("src",base+""+audio);
}
