var tabla_2 = $("#tabla_gasolina");
var tabla_3 = $("#tabla_taller");

//Tabla de gasolina y lubricantes
$("#agregarGL").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceGasolina").val();

    //Recuperamos los campos de la fila default
    var campo_1 = $("#oc_"+indice).val();
    var campo_2 = $("#gasolina_"+indice).val();
    var campo_3 = $("#costo_"+indice).val();

    if ((campo_2 != "")&&(campo_3 != 0)) {
        //Evitamos la edicion de los  elementos
        $("#oc_"+indice).prop('disabled','disabled');
        $("#gasolina_"+indice).prop('disabled','disabled');
        $("#costo_"+indice).prop('disabled','disabled');

        //sacamos el total de la venta
        var totalRenglon = parseFloat(campo_1) * parseFloat(campo_3);
        var totalAnterior = parseFloat($("#totalesGasolinaGral").val());
        var totalGral = totalAnterior + totalRenglon;

        $("#venta_"+indice).text("$ "+totalRenglon.toFixed(2));
        $("#totalRenglon_"+indice).val(totalRenglon);

        $("#totalesGasolinaLabel").text("$ "+totalGral.toFixed(2));
        $("#totalesGasolinaGral").val(totalGral);

        //Vaciamos los valores del renglon en un input
        //                            O.C / Gasolina / costo / venta
        $("#ventaValor_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+totalRenglon);

        indice++;
        $("#indiceGasolina").val(indice);

        tabla_2.append("<tr id='fila_Gasolina_"+indice+"'>"+
            "<td align='center'>"+
                "<input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_"+indice+"' value='1' style='width:1cm;'>"+
            "</td>"+
            "<td style='border-left:1px solid #337ab7;width:50%;'>"+
                "<input type='text' class='input_field' id='gasolina_"+indice+"' value='' style='width:100%;'>"+
            "</td>"+
            "<td align='center' style='border-left:1px solid #337ab7;'>"+
                "<input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_"+indice+"' value='0' style='width:2cm;'>"+
            "</td>"+
            "<td align='center' colspan='3' style='border-left:1px solid #337ab7;'>"+
                "<label id='venta_"+indice+"'>$ 0</label>"+
                "<input type='hidden' class='input_field' id='ventaValor_"+indice+"' name='costos[]'>"+
                "<input type='hidden' id='totalRenglon_"+indice+"' value='0'>"+
            "</td>"+
        "</tr>");
    }else {
        //Regresamos el control al campo que falta rellenar
        if (campo_1 == "") {
            $("#oc_"+indice).focus();
        }else if (campo_2 == "") {
            $("#gasolina_"+indice).focus();
        }else if (campo_3 == "") {
            $("#costo_"+indice).focus();
        }
    }

});

//Eliminamos la ultima fila ingresada (gasolina y lubricantes)
$("#eliminarGL").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceGasolina").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        //sacamos el total de la venta
        var ultimoTotal = parseFloat($("#totalRenglon_"+(indice-1)).val());
        var totalAnterior = parseFloat($("#totalesGasolinaGral").val());
        var totalGral = totalAnterior - ultimoTotal;

        $("#totalesGasolinaLabel").text("$ "+totalGral.toFixed(2));
        $("#totalesGasolinaGral").val(totalGral);

        var fila = $("#fila_Gasolina_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceGasolina").val(indice);

        //Restablecemos los valores default del reglon anterior
        $("#oc_"+indice).val("1");
        $("#gasolina_"+indice).val("");
        $("#costo_"+indice).val("0");
        $("#venta_"+indice).text("$ 0");
        $("#totalRenglon_"+indice).val("0");
        $("#ventaValor_"+indice).val("");

        //Desbloqueamos el renglon para la edición
        $("#oc_"+indice).prop('disabled',false);
        $("#gasolina_"+indice).prop('disabled',false);
        $("#costo_"+indice).prop('disabled',false);

    }
});

//Tabla de trabajo fuera del taller
$("#agregarFDT").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTaller").val();

    //Recuperamos los campos de la fila default
    var campo_1 = $("#oc_taller_"+indice).val();
    var campo_2 = $("#taller_"+indice).val();
    var campo_3 = $("#costo_taller_"+indice).val();

    if ((campo_2 != "")&&(campo_3 != 0)) {
        //Evitamos la edicion de los  elementos
        $("#oc_taller_"+indice).prop('disabled','disabled');
        $("#taller_"+indice).prop('disabled','disabled');
        $("#costo_taller_"+indice).prop('disabled','disabled');

        //sacamos el total de la venta
        var totalRenglon = parseFloat(campo_1) * parseFloat(campo_3);
        var totalAnterior = parseFloat($("#totalesTaller").val());
        var totalGral = totalAnterior + totalRenglon;

        $("#venta_taller_"+indice).text("$ "+totalRenglon.toFixed(2));
        $("#totalRenglon_taller_"+indice).val(totalRenglon);

        $("#totalesTallerLabel").text("$ "+totalGral.toFixed(2));
        $("#totalesTaller").val(totalGral);

        //Vaciamos los valores del renglon en un input
        //                            O.C / trabajo fuera del taller / costo / venta
        $("#ventaValor_taller_"+indice).val(campo_1+"_"+campo_2+"_"+campo_3+"_"+totalRenglon);

        indice++;
        $("#indiceTaller").val(indice);

        tabla_3.append("<tr id='fila_taller_"+indice+"'>"+
            "<td align='center'>"+
                "<input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_taller_"+indice+"' value='1' style='width:1cm;'>"+
            "</td>"+
            "<td style='border-left:1px solid #337ab7;width:50%;'>"+
                "<input type='text' class='input_field' id='taller_"+indice+"' value='' style='width:100%;'>"+
            "</td>"+
            "<td align='center' style='border-left:1px solid #337ab7;'>"+
                "<input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_taller_"+indice+"' value='0' style='width:2cm;'>"+
            "</td>"+
            "<td align='center' colspan='3' style='border-left:1px solid #337ab7;'>"+
                "<label id='venta_taller_"+indice+"'>$ 0</label>"+
                "<input type='hidden' class='input_field' id='ventaValor_taller_"+indice+"' name='venta_taller[]'>"+
                "<input type='hidden' id='totalRenglon_taller_"+indice+"' value='0'>"+
            "</td>"+
        "</tr>");
    }else {
        //Regresamos el control al campo que falta rellenar
        if (campo_1 == "") {
            $("#oc_taller_"+indice).focus();
        }else if (campo_2 == "") {
            $("#taller_"+indice).focus();
        }else if (campo_3 == "") {
            $("#costo_taller_"+indice).focus();
        }
    }

});

//Eliminamos la ultima fila ingresada (trabajo fuer del taller)
$("#eliminarFDT").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTaller").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        //sacamos el total de la venta
        var ultimoTotal = parseFloat($("#totalRenglon_taller_"+(indice-1)).val());
        var totalAnterior = parseFloat($("#totalesTaller").val());
        var totalGral = totalAnterior - ultimoTotal;

        $("#totalesTallerLabel").text("$ "+totalGral.toFixed(2));
        $("#totalesTaller").val(totalGral);

        var fila = $("#fila_taller_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceTaller").val(indice);

        //Restablecemos los valores default del reglon anterior
        $("#oc_taller_"+indice).val("1");
        $("#taller_"+indice).val("");
        $("#costo_taller_"+indice).val("0");
        $("#venta_taller_"+indice).text("$ 0");
        $("#totalRenglon_taller_"+indice).val("0");
        $("#ventaValor_taller_"+indice).val("");

        //Desbloqueamos el renglon para la edición
        $("#oc_taller_"+indice).prop('disabled',false);
        $("#taller_"+indice).prop('disabled',false);
        $("#costo_taller_"+indice).prop('disabled',false);

    }
});
