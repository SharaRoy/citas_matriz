var tabla = $("#cuerpoMateriales");
var editorExt = $("#tipoRegistro").val();

//Validamos si es un asesor para que pueda firmar
if (editorExt == "Tecnico") {
    $("#AsesorCostos").attr("disabled", true);
    $("#AsesorCostos").attr("data-toggle", false);
}

//Tabla de control de materiales
$("#agregarCM").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Recuperamos los campos de la fila default
    // var campo_1 = $("#req_"+indice).val();
    var campo_2 = $("#cantidad_"+indice).val();
    var campo_3 = $("#descripcion_"+indice).val();
    var campo_4 = $("#costoCM_"+indice).val();
    var campo_5 = $("#horas_"+indice).val();
    var campo_6 = $("#totalReng_"+indice).val();
    var campo_8 = $("#pieza_"+indice).val();
    // var campo_7 = $("#autorizo_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ((campo_2 != "")&&(campo_3 != "")&&(campo_4 != "")){
        //Evitamos la edicion de los  elementos
        // $("#req_"+indice).prop('disabled','disabled');
        $("#cantidad_"+indice).prop('disabled','disabled');
        $("#descripcion_"+indice).prop('disabled','disabled');
        $("#costoCM_"+indice).prop('disabled','disabled');
        $("#horas_"+indice).prop('disabled','disabled');
        $("#totalReng_"+indice).prop('disabled','disabled');
        $("#pieza_"+indice).prop('disabled','disabled');
        // $("#autorizo_"+indice).prop('disabled','disabled');

        //Mostramos el boton para eliminar fila
        $("#eliminarCM").prop('disabled','disabled');

        //Recuperamos el valor del check
        if( $("#autorizo_"+indice).is(':checked') ) {
            var campo_7 = "SI";
        }else {
            var campo_7 = "NO";
        }

        //Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza
        $("#valoresCM_"+indice).val(campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6+"_"+campo_7+"_"+campo_8);

        //Aumentamos el indice para los renglones
        indice++;
        $("#indiceTablaMateria").val(indice);

        //confirmamos el tipo de formulario
        if ($("#cargaFormulario").val() == "1") {
            var prp = "disabled";
        }else {
            var prp = "";
        }

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla.append("<tr id='fila_"+indice+"'>"+
            "<td align='center' style='border-top:1px solid #337ab7;'>"+
                "<input type='text' class='input_field' id='cantidad_"+indice+"' onblur='cantidadCollet("+indice+")' value='1' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:90%;'>"+
            "</td>"+
            "<td style='border-left:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='text' class='input_field' id='descripcion_"+indice+"' onblur='descripcionCollect("+indice+")' style='width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border-left:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='text' class='input_field' id='pieza_"+indice+"' onblur='piezaCollet("+indice+")' value='' style='width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border-left:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "$<input type='text' class='input_field' id='costoCM_"+indice+"' onblur='costoCollet("+indice+")' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border-left:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='text' class='input_field' id='horas_"+indice+"' onblur='horasCollet("+indice+")' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border-left:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "$<input type='text' id='totalReng_"+indice+"' class='input_field' onblur='autosuma("+indice+")' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
                "<input type='hidden' id='totalOperacion_"+indice+"' value='0'>"+
            "</td>"+
            "<td style='border-left:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet("+indice+")' value='"+indice+"' id='autorizo_"+indice+"'>"+
                "<input type='hidden' id='valoresCM_"+indice+"' name='registros[]'>"+
            "</td>"+
        "</tr>");

        if (editorExt == "Tecnico") {
            $("#totalReng_"+indice).prop('disabled','disabled');
            $("#autorizo_"+indice).prop('disabled','disabled');
        }
    }else {
        //Regresamos el control al campo que falta rellenar
        if (campo_2 == "") {
            $("#cantidad_"+indice).focus();
        }else if (campo_3 == "") {
            $("#descripcion_"+indice).focus();
        }else  {
            $("#costoCM_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada (control de materiales)
$("#eliminarCM").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        var fila = $("#fila_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceTablaMateria").val(indice);

        //Restablecemos los valores default del reglon anterior
        $("#cantidad_"+indice).val("1");
        $("#descripcion_"+indice).val("");
        $("#costoCM_"+indice).val("0");
        $("#horas_"+indice).val("0");

        //Desbloqueamos el renglon para la edición
        $("#cantidad_"+indice).prop('disabled',false);
        $("#descripcion_"+indice).prop('disabled',false);
        $("#costoCM_"+indice).prop('disabled',false);
        $("#horas_"+indice).prop('disabled',false);
        $("#pieza_"+indice).prop('disabled',false);
    }
});
