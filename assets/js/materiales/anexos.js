$("#aceptar").on('click',function() {
    $("#AlertaModalCuerpo").css("background-color","#c7ecc7");
    $("#AlertaModalCuerpo").css("border-color","#4cae4c");

    $("#tituloForm").text("¿Está seguro de aceptar toda la cotización? NO podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");
    $("#desicionCoti").val("Si");
});

$("#rechazar").on('click',function() {
    $("#AlertaModalCuerpo").css("background-color","#f5acaa");
    $("#AlertaModalCuerpo").css("border-color","#d43f3a");

    $("#tituloForm").text("¿Está seguro de rechazar toda la cotización? NO  podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");
    $("#desicionCoti").val("No");
});

$("#terminarCotizacion").on('click',function() {
    $("#modoGuardado").val("Actualizar");

    // Evitamos la basura en cotizaciones anteriores
    var conteoTotal = 0;
    var renglonesCuenta = $("#indiceTablaMateria").val();

    //Recuperamos los renglones que no son fueron aprovados por el jefe de taller
    var pza_previa = $("#refAutorizadas").val();
    var pzas_individuales = pza_previa.split("_");

    for (var i = 1; i <= renglonesCuenta; i++) {
        //Comprobamos que ya haya sido revisado por el jefe de taller
        if (pza_previa != "") {
            //Si ya fue revisado por el jefe de taller, validamos que el renglon deba contarse
            if (pzas_individuales.indexOf(""+i) > -1) {
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var cantidadPza = parseFloat($("#cantidad_"+i).val());
                var costoPza = parseFloat($("#costoCM_"+i).val());
                var horasMo = parseFloat($("#horas_"+i).val());
                var costoMo = 860; //parseFloat($("#totalReng_"+i).val());
                var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                //console.log(totalRenglon);
                conteoTotal += totalRenglon;
            }
        }else{
            //Como alternativa, se puede hacer la cuenta completa por renglon
            var cantidadPza = parseFloat($("#cantidad_"+i).val());
            var costoPza = parseFloat($("#costoCM_"+i).val());
            var horasMo = parseFloat($("#horas_"+i).val());
            var costoMo = 860; //parseFloat($("#totalReng_"+i).val());
            var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
            //console.log(totalRenglon);
            conteoTotal += totalRenglon;
        }
    }

     var anticipo = parseFloat($("#anticipoMaterial").val());
     var iva = conteoTotal * 0.16;
     var totalV1 = conteoTotal + iva;
     var total = totalV1 - anticipo;

     //Vaciamos los nuevos resultados
     $("#subTotalMaterial").val(parseFloat(conteoTotal));
     $("#ivaMaterial").val(parseFloat(iva));
     $("#totalMaterial").val(parseFloat(total));

     $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
     $("#ivaMaterialLabel").text(iva.toFixed(2));
     $("#totalMaterialLabel").text(total.toFixed(2));
     $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

     document.getElementById('formularioPresupuesto').submit();
});

$("#terminarCotizacion2").on('click',function() {
    $("#modoGuardado").val("Terminar");

    // Evitamos la basura en cotizaciones anteriores
    var conteoTotal = 0;
    var renglonesCuenta = $("#indiceTablaMateria").val();

    //Recuperamos los renglones que no son fueron aprovados por el jefe de taller
    var pza_previa = $("#refAutorizadas").val();
    var pzas_individuales = pza_previa.split("_");

    for (var i = 1; i <= renglonesCuenta; i++) {
        //Comprobamos que ya haya sido revisado por el jefe de taller
        if (pza_previa != "") {
            //Si ya fue revisado por el jefe de taller, validamos que el renglon deba contarse
            if (pzas_individuales.indexOf(""+i) > -1) {
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var cantidadPza = parseFloat($("#cantidad_"+i).val());
                var costoPza = parseFloat($("#costoCM_"+i).val());
                var horasMo = parseFloat($("#horas_"+i).val());
                var costoMo = 860; //parseFloat($("#totalReng_"+i).val());
                var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                //console.log(totalRenglon);
                conteoTotal += totalRenglon;
            }
        }else{
            //Como alternativa, se puede hacer la cuenta completa por renglon
            var cantidadPza = parseFloat($("#cantidad_"+i).val());
            var costoPza = parseFloat($("#costoCM_"+i).val());
            var horasMo = parseFloat($("#horas_"+i).val());
            var costoMo = 860; //parseFloat($("#totalReng_"+i).val());
            var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
            //console.log(totalRenglon);
            conteoTotal += totalRenglon;
        }
    }

     var anticipo = parseFloat($("#anticipoMaterial").val());
     var iva = conteoTotal * 0.16;
     var totalV1 = conteoTotal + iva;
     var total = totalV1 - anticipo;

     //Vaciamos los nuevos resultados
     $("#subTotalMaterial").val(parseFloat(conteoTotal));
     $("#ivaMaterial").val(parseFloat(iva));
     $("#totalMaterial").val(parseFloat(total));

     $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
     $("#ivaMaterialLabel").text(iva.toFixed(2));
     $("#totalMaterialLabel").text(total.toFixed(2));
     $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

     document.getElementById('formularioPresupuesto').submit();
});

$("#aceptarCotizacion").on('click',function() {
    // Evitamos la basura en cotizaciones anteriores
    var conteoTotal = 0;
    var renglonesCuenta = $("#indiceTablaMateria").val();

    //Recuperamos los renglones que no son fueron aprovados por el jefe de taller
    var pza_previa = $("#refAutorizadas").val();
    var pzas_individuales = pza_previa.split("_");

    for (var i = 1; i <= renglonesCuenta; i++) {
        //Comprobamos que ya haya sido revisado por el jefe de taller
        if (pza_previa != "") {
            //Si ya fue revisado por el jefe de taller, validamos que el renglon deba contarse
            if (pzas_individuales.indexOf(""+i) > -1) {
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var cantidadPza = parseFloat($("#cantidad_"+i).val());
                var costoPza = parseFloat($("#costoCM_"+i).val());
                var horasMo = parseFloat($("#horas_"+i).val());
                var costoMo = 860;//parseFloat($("#totalReng_"+i).val());
                var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                //console.log(totalRenglon);
                conteoTotal += totalRenglon;
            }
        }else{
            //Como alternativa, se puede hacer la cuenta completa por renglon
            var cantidadPza = parseFloat($("#cantidad_"+i).val());
            var costoPza = parseFloat($("#costoCM_"+i).val());
            var horasMo = parseFloat($("#horas_"+i).val());
            var costoMo = 860;//parseFloat($("#totalReng_"+i).val());
            var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
            //console.log(totalRenglon);
            conteoTotal += totalRenglon;
        }
    }

     var anticipo = parseFloat($("#anticipoMaterial").val());
     var iva = conteoTotal * 0.16;
     var totalV1 = conteoTotal + iva;
     var total = totalV1 - anticipo;

     //Vaciamos los nuevos resultados
     $("#subTotalMaterial").val(parseFloat(conteoTotal));
     $("#ivaMaterial").val(parseFloat(iva));
     $("#totalMaterial").val(parseFloat(total));

     $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
     $("#ivaMaterialLabel").text(iva.toFixed(2));
     $("#totalMaterialLabel").text(total.toFixed(2));
     $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

     document.getElementById('formularioPresupuesto').submit();
});

//Envio de formulario
$("#envioFormulario").on('click',function() {
    $("input").attr("disabled", false);
});

//Mostrar botones para aceptar cotizacion completa o por renglones
$("#seleccionarTodo").on('click',function() {
    $(".multipleEleccion").attr("hidden","hidden");
    $(".multipleEleccionBtn").attr("hidden",false);
    $(".eleccionBtn").attr("hidden","hidden");

    //Recuperamos valores originales
    var subTotalMaterial = parseFloat($("#subTotalMaterial_rest").val());
    var canceladoMaterial = parseFloat($("#canceladoMaterial_rest").val());
    var ivaMaterial = parseFloat($("#ivaMaterial_rest").val());
    var totalMaterial = parseFloat($("#totalMaterial_rest").val());

    //Vaciamos los valores originales
    
    //Imprimimos el subtotal
    $("#subTotalMaterial").val(subTotalMaterial);
    $("#subTotalMaterialLabel").text(subTotalMaterial.toFixed(2));
    //Imprimimos los nuevos valores
    $("#canceladoMaterial").val(canceladoMaterial);
    $("#canceladoMaterialLabel").text(canceladoMaterial.toFixed(2));
    //Imprimimos el iva
    $("#ivaMaterial").val(ivaMaterial);
    $("#ivaMaterialLabel").text(ivaMaterial.toFixed(2));
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(totalMaterial);
    $("#totalMaterialLabel").text(totalMaterial.toFixed(2));
    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text((subTotalMaterial+ivaMaterial).toFixed(2));

    //Oculatamos el boton de guardar
    $("#envioFormulario").attr("hidden",true);

    //Limpiamos los colores de la tabla
    //Recuperamos el indice de la tabla
    var indice = $("#indiceTablaMateria").val();
    //Recorremos
    for (var i = 1; i <= indice; i++) {
        //Revisamos cada renglon
        var campos = $("#valoresCM_"+i).val().split("_");
        //console.log(campos);
        $("#fila_indicador_"+i).css("background-color","white");
        //Verficamos que ya tenga una leleccion
        if (campos.length > 9) {
            $("#valoresCM_"+i).val(campos[0]+"_"+campos[1]+"_"+campos[2]+"_"+campos[3]+"_"+campos[4]+"_"+campos[5]+"_"+campos[6]+"_"+campos[7]+"_"+campos[8]);
        }
    }
});

$("#seleccionarFilas").on('click',function() {
    $(".multipleEleccion").attr("hidden",false);
    $(".eleccionBtn").attr("hidden",false); 
    $(".multipleEleccionBtn").attr("hidden","hidden");

    //Colocamos en "0" la operacion para los calculos
    //Imprimimos el subtotal
    $("#subTotalMaterial").val(0);
    $("#subTotalMaterialLabel").text("0");
    //Imprimimos los nuevos valores
    $("#canceladoMaterial").val(0);
    $("#canceladoMaterialLabel").text("0");
    //Imprimimos el iva
    $("#ivaMaterial").val(0);
    $("#ivaMaterialLabel").text("0");
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(0);
    $("#totalMaterialLabel").text("0");
    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text("0");
});

/*//Aceptar/Rechazar cotizacion por partes
function aceptarItem(renglon){
    //Marcamos la fila para mostrar la decision tomada por el cliente
    $("#fila_indicador_"+renglon).css("background-color","#c7ecc7");
    //Recuperamos el renglon e imprimimos la decision
    var contenidoFila = $("#valoresCM_"+renglon).val();
    //Separamos los valores para validar si existe modificacion previa
    var campos = contenidoFila.split("_");
    //console.log(campos);
    //Si son mas de 7 campos , ya se habia tomado una decision
    if (campos.length > 9) {
        console.log("aceptada");
        $("#valoresCM_"+renglon).val(campos[0]+"_"+campos[1]+"_"+campos[2]+"_"+campos[3]+"_"+campos[4]+"_"+campos[5]+"_"+campos[6]+"_"+campos[7]+"_"+campos[8]+"_"+"Aceptada");

        //Si es rechazada hacemos la resta del precio rechazado
        var montoCancelado = $("#canceladoMaterial").val();

        //Volvemos a calcular el precio a cancelar
        var pieza = parseFloat(campos[0]) * parseFloat(campos[2]);
        var mo = parseFloat(campos[3]) * parseFloat(campos[4]);
        var renglonTotal =  pieza + mo;

        //Calculamos el valor a cancelar
        var nvoMontoCancelar = parseFloat(montoCancelado) - renglonTotal;

        //var nvoMontoCancelar = parseFloat(montoCancelado) - parseFloat(campos[7]);
        if ((nvoMontoCancelar >= 0)&&(campos[9] != "Aceptada")) {
            //Imprimimos los nuevos valores
            $("#canceladoMaterial").val(nvoMontoCancelar);
            $("#canceladoMaterialLabel").text(nvoMontoCancelar.toFixed(2));

            //Recalculamos los totales
            var subTotal = parseFloat($("#subTotalMaterial").val());
            var anticipo = parseFloat($("#anticipoMaterial").val());
            var nvoSubTotal = subTotal + renglonTotal;
            var iva = nvoSubTotal * 0.16;

            //Imprimimos el subtotal
            $("#subTotalMaterial").val(nvoSubTotal);
            $("#subTotalMaterialLabel").text(nvoSubTotal.toFixed(2));

            //Imprimimos el iva
            $("#ivaMaterial").val(iva);
            $("#ivaMaterialLabel").text(iva.toFixed(2));

            var totalV1 = nvoSubTotal * 1.16;
            var total = totalV1 - anticipo;
            //Imprimimos el total  menos el anticipo
            $("#totalMaterial").val(total);
            $("#totalMaterialLabel").text(total.toFixed(2));

            //Imprimimos el total con iva (presupuesto original)
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));
        }

    //De lo contrario, se afecta el renglon por primera vez
    }else {
        $("#valoresCM_"+renglon).val(contenidoFila+"_"+"Aceptada");
        console.log("aceptada nvo");

        //Comenzamos la sumatoria del presupuesto
        //Si es rechazada hacemos la resta del precio rechazado
        var montoCancelado = $("#canceladoMaterial").val();

        //Volvemos a calcular el precio a cancelar
        var pieza = parseFloat(campos[0]) * parseFloat(campos[2]);
        var mo = parseFloat(campos[3]) * parseFloat(campos[4]);
        var renglonTotal =  pieza + mo;

        //Recalculamos los totales
        var subTotal = parseFloat($("#subTotalMaterial").val());
        var anticipo = parseFloat($("#anticipoMaterial").val());
        var nvoSubTotal = subTotal + renglonTotal;
        var iva = nvoSubTotal * 0.16;

        //Imprimimos el subtotal
        $("#subTotalMaterial").val(nvoSubTotal);
        $("#subTotalMaterialLabel").text(nvoSubTotal.toFixed(2));

        //Imprimimos el iva
        $("#ivaMaterial").val(iva);
        $("#ivaMaterialLabel").text(iva.toFixed(2));

        var totalV1 = nvoSubTotal * 1.16;
        var total = totalV1 - anticipo;
        //Imprimimos el total  menos el anticipo
        $("#totalMaterial").val(total);
        $("#totalMaterialLabel").text(total.toFixed(2));

        //Imprimimos el total con iva (presupuesto original)
        $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

        /* //Aumentamos la bandera para validar
        var filasAfectadas = $("#bandera").val();
        var limite = $("#limite").val();

        filasAfectadas++;
        $("#bandera").val(filasAfectadas);

        if (filasAfectadas==limite) {
            $("input").attr("disabled", false);
            $("#envioFormulario").attr("hidden",false);
        }* /

        //Aumentamos la bandera para validar
        var filasAfectadas = $("#bandera").val();
        var limite = $("#limite").val();

        filasAfectadas++;
        $("#bandera").val(filasAfectadas);

        //if (filasAfectadas==limite) {
        if (filasAfectadas >= 1) {
            $("input").attr("disabled", false);
            $("#envioFormulario").attr("hidden",false);
        }
    }

    $("input").attr("disabled", false);
    $("#envioFormulario").attr("hidden",false);

    //Bloqueamos la opcion ya elegida
    $("#interruptorA_"+renglon).attr("hidden","hidden");
    $("#interruptorB_"+renglon).attr("hidden",false);
}

function rechazarItem(renglon){
    //Marcamos la fila para mostrar la decision tomada por el cliente
    $("#fila_indicador_"+renglon).css("background-color","#f5acaa");
    //Recuperamos el renglon e imprimimos la decision
    var contenidoFila = $("#valoresCM_"+renglon).val();
    //Separamos los valores para validar si existe modificacion previa
    var campos = contenidoFila.split("_");
    // console.log(campos);

    //Si son mas de 7 campos , ya se habia tomado una decision
    if (campos.length > 9) {
        console.log("rechazada");
        $("#valoresCM_"+renglon).val(campos[0]+"_"+campos[1]+"_"+campos[2]+"_"+campos[3]+"_"+campos[4]+"_"+campos[5]+"_"+campos[6]+"_"+campos[7]+"_"+campos[8]+"_"+"Rechazada");

        //Si es rechazada hacemos la resta del precio rechazado
        var montoCancelado = $("#canceladoMaterial").val();

        //Volvemos a calcular el precio a cancelar
        var pieza = parseFloat(campos[0]) * parseFloat(campos[2]);
        var mo = parseFloat(campos[3]) * parseFloat(campos[4]);
        var renglonTotal =  pieza + mo;

        //Calculamos el valor a cancelar
        var nvoMontoCancelar = parseFloat(montoCancelado) + renglonTotal;

        //var nvoMontoCancelar = parseFloat(montoCancelado) - parseFloat(campos[7]);
        if ((nvoMontoCancelar >= 0)&&(campos[9] != "Rechazada")) {
            //Imprimimos los nuevos valores
            $("#canceladoMaterial").val(nvoMontoCancelar);
            $("#canceladoMaterialLabel").text(nvoMontoCancelar.toFixed(2));

            //Recalculamos los totales
            var subTotal = parseFloat($("#subTotalMaterial").val());
            var anticipo = parseFloat($("#anticipoMaterial").val());
            var nvoSubTotal = subTotal - renglonTotal;
            var iva = nvoSubTotal * 0.16;

            //Imprimimos el subtotal
            $("#subTotalMaterial").val(nvoSubTotal);
            $("#subTotalMaterialLabel").text(nvoSubTotal.toFixed(2));

            //Imprimimos el iva
            $("#ivaMaterial").val(iva);
            $("#ivaMaterialLabel").text(iva.toFixed(2));

            var totalV1 = nvoSubTotal * 1.16;
            var total = totalV1 - anticipo;
            //Imprimimos el total  menos el anticipo
            $("#totalMaterial").val(total);
            $("#totalMaterialLabel").text(total.toFixed(2));

            //Imprimimos el total con iva (presupuesto original)
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));
        }

    //De lo contrario, se afecta el renglon por primera vez
    }else {
        $("#valoresCM_"+renglon).val(contenidoFila+"_"+"Rechazada");
        console.log("rechazada nvo");

        //Si es rechazada hacemos la resta del precio rechazado
        var montoCancelado = $("#canceladoMaterial").val();
        var anticipo = parseFloat($("#anticipoMaterial").val());

        //Volvemos a calcular el precio a cancelar
        var pieza = parseFloat(campos[0]) * parseFloat(campos[2]);
        var mo = parseFloat(campos[3]) * parseFloat(campos[4]);
        var renglonTotal =  pieza + mo;

        //Calculamos el valor a cancelar
        var nvoMontoCancelar = parseFloat(montoCancelado) + renglonTotal;
        
        //Imprimimos los nuevos valores
        $("#canceladoMaterialLabel").text(nvoMontoCancelar.toFixed(2));
        $("#canceladoMaterial").val(nvoMontoCancelar);

        //Aumentamos la bandera para validar
        var filasAfectadas = $("#bandera").val();
        var limite = $("#limite").val();

        filasAfectadas++;
        $("#bandera").val(filasAfectadas);

        //if (filasAfectadas==limite) {
        if (filasAfectadas >= 1) {
            $("input").attr("disabled", false);
            $("#envioFormulario").attr("hidden",false);
        }
    }

    $("input").attr("disabled", false);
    $("#envioFormulario").attr("hidden",false);
    
    //Bloqueamos la opcion ya elegida
    $("#interruptorA_"+renglon).attr("hidden",false);
    $("#interruptorB_"+renglon).attr("hidden","hidden");
} */

//Nuevo proceso de sumatoria
//Aceptar/Rechazar cotizacion por partes
function aceptarItem(renglon){
    //Marcamos la fila para mostrar la decision tomada por el cliente
    $("#fila_indicador_"+renglon).css("background-color","#c7ecc7");
    //Recuperamos el renglon e imprimimos la decision
    var contenidoFila = $("#valoresCM_"+renglon).val();
    //Separamos los valores para validar si existe modificacion previa
    var campos = contenidoFila.split("_");
    //console.log(campos);
    
    //Inicializamos variables se operacion
    var aceptado = 0;
    var cancelado = 0;

    //Si son mas de 7 campos , ya se habia tomado una decision
    if (campos.length > 9) {
        $("#valoresCM_"+renglon).val(campos[0]+"_"+campos[1]+"_"+campos[2]+"_"+campos[3]+"_"+campos[4]+"_"+campos[5]+"_"+campos[6]+"_"+campos[7]+"_"+campos[8]+"_"+"Aceptada");

        //Hacemos nuevamente la sumatoria
        //Recuperamos el indice de la tabla
        var indice = $("#indiceTablaMateria").val();
        //Recorremos
        for (var i = 1; i <= indice; i++) {
            //Revisamos cada renglon
            var campos = $("#valoresCM_"+i).val().split("_");
            //console.log(campos);

            //Verficamos que ya tenga una leleccion
            if (campos.length > 9) {
                var pieza = parseFloat(campos[0]) * parseFloat(campos[2]);
                var mo = parseFloat(campos[3]) * 860 ; //parseFloat(campos[4]);
                var renglonTotal =  pieza + mo;

                //Identificamos donde se realizara la operacion
                if (campos[9] == "Aceptada") {
                    aceptado += renglonTotal;
                } else {
                    cancelado += renglonTotal;
                }
            }
        }

    }else{
        $("#valoresCM_"+renglon).val(contenidoFila+"_"+"Aceptada");

        //Hacemos nuevamente la sumatoria
        //Recuperamos el indice de la tabla
        var indice = $("#indiceTablaMateria").val();
        //Recorremos
        for (var i = 1; i <= indice; i++) {
            //Revisamos cada renglon
            var campos = $("#valoresCM_"+i).val().split("_");
            //console.log(campos);

            //Verficamos que ya tenga una leleccion
            if (campos.length > 9) {
                var pieza = parseFloat(campos[0]) * parseFloat(campos[2]);
                var mo = parseFloat(campos[3]) * 860 ; //parseFloat(campos[4]);
                var renglonTotal =  pieza + mo;

                //Identificamos donde se realizara la operacion
                if (campos[9] == "Aceptada") {
                    aceptado += renglonTotal;
                } else {
                    cancelado += renglonTotal;
                }
            }
        }
    }

    //Recalculamos los totales
    var iva = aceptado * 0.16;

    //Imprimimos el subtotal
    $("#subTotalMaterial").val(aceptado);
    $("#subTotalMaterialLabel").text(aceptado.toFixed(2));

    //Imprimimos el iva
    $("#ivaMaterial").val(iva);
    $("#ivaMaterialLabel").text(iva.toFixed(2));

    var anticipo = parseFloat($("#anticipoMaterial").val());

    var totalV1 = aceptado * 1.16;
    var total = totalV1 - anticipo;
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(total);
    $("#totalMaterialLabel").text(total.toFixed(2));

    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

    //Imprimimos los nuevos valores
    $("#canceladoMaterial").val(cancelado);
    $("#canceladoMaterialLabel").text(cancelado.toFixed(2));

    //Bloqueamos la opcion ya elegida
    $("#interruptorA_"+renglon).attr("hidden","hidden");
    $("#interruptorB_"+renglon).attr("hidden",false);

    //Desbloqueamos el boton de enviar
    $("input").attr("disabled", false);
    $("#envioFormulario").attr("hidden",false);
}

function rechazarItem(renglon){
    //Marcamos la fila para mostrar la decision tomada por el cliente
    $("#fila_indicador_"+renglon).css("background-color","#f5acaa");
    //Recuperamos el renglon e imprimimos la decision
    var contenidoFila = $("#valoresCM_"+renglon).val();
    //Separamos los valores para validar si existe modificacion previa
    var campos = contenidoFila.split("_");
    
    //Inicializamos variables se operacion
    var aceptado = 0;
    var cancelado = 0;
    //Si son mas de 7 campos , ya se habia tomado una decision
    if (campos.length > 9) {
        $("#valoresCM_"+renglon).val(campos[0]+"_"+campos[1]+"_"+campos[2]+"_"+campos[3]+"_"+"860"+"_"+campos[5]+"_"+campos[6]+"_"+campos[7]+"_"+campos[8]+"_"+"Rechazada");

        //Hacemos nuevamente la sumatoria
        //Recuperamos el indice de la tabla
        var indice = $("#indiceTablaMateria").val();
        //Recorremos
        for (var i = 1; i <= indice; i++) {
            //Revisamos cada renglon
            var campos = $("#valoresCM_"+i).val().split("_");
            //console.log(campos);

            //Verficamos que ya tenga una leleccion
            if (campos.length > 9) {
                var pieza = parseFloat(campos[0]) * parseFloat(campos[2]);
                var mo = parseFloat(campos[3]) * 860 ; //parseFloat(campos[4]);
                var renglonTotal =  pieza + mo;

                //Identificamos donde se realizara la operacion
                if (campos[9] == "Aceptada") {
                    aceptado += renglonTotal;
                } else {
                    cancelado += renglonTotal;
                }
            }
        }
    }else{
        $("#valoresCM_"+renglon).val(contenidoFila+"_"+"Rechazada");

        //Hacemos nuevamente la sumatoria
        //Recuperamos el indice de la tabla
        var indice = $("#indiceTablaMateria").val();
        //Recorremos
        for (var i = 1; i <= indice; i++) {
            //Revisamos cada renglon
            var campos = $("#valoresCM_"+i).val().split("_");
            //console.log(campos);

            //Verficamos que ya tenga una leleccion
            if (campos.length > 9) {
                var pieza = parseFloat(campos[0]) * parseFloat(campos[2]);
                var mo = parseFloat(campos[3]) * 860 ; //parseFloat(campos[4]);
                var renglonTotal =  pieza + mo;

                //Identificamos donde se realizara la operacion
                if (campos[9] == "Aceptada") {
                    aceptado += renglonTotal;
                } else {
                    cancelado += renglonTotal;
                }
            }
        }
    }

    //Recalculamos los totales
    var iva = aceptado * 0.16;

    //Imprimimos el subtotal
    $("#subTotalMaterial").val(aceptado);
    $("#subTotalMaterialLabel").text(aceptado.toFixed(2));

    //Imprimimos el iva
    $("#ivaMaterial").val(iva);
    $("#ivaMaterialLabel").text(iva.toFixed(2));

    var anticipo = parseFloat($("#anticipoMaterial").val());

    var totalV1 = aceptado * 1.16;
    var total = totalV1 - anticipo;
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(total);
    $("#totalMaterialLabel").text(total.toFixed(2));

    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

    //Imprimimos los nuevos valores
    $("#canceladoMaterial").val(cancelado);
    $("#canceladoMaterialLabel").text(cancelado.toFixed(2));

    $("#interruptorA_"+renglon).attr("hidden",false);
    $("#interruptorB_"+renglon).attr("hidden","hidden");
}
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////

// Buscador dinamico para la tabla
$('#busqueda_tabla').keyup(function () {
    toSearch();
});

//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}

//Envio de video en la orden de servicio
$("#enviarCotizacionApro").on('click',function(){
    var paqueteDeDatos = new FormData();
    paqueteDeDatos.append('eleccion', $('#desicionCoti').prop('value'));
    paqueteDeDatos.append('idOrden', $('#idOrden').prop('value'));
    enviarCofirmacion(paqueteDeDatos);
});

//Funcion envio formulario para aceptar cotizacion
function enviarCofirmacion(paqueteDeDatos){
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");
    $("#cancelarCoti").attr("disabled", true);
    $("#enviarCotizacionApro").attr("disabled", true);
    var url = $("#basePeticion").val();
    $.ajax({
        url: url+"multipunto/Cotizaciones/getAjaxForm",
        type: 'post',
        contentType: false,
        data: paqueteDeDatos,
        processData: false,
        cache: false,
        success:function(resp){
          $(".cargaIcono").css("display","none");
          $("#errorEnvioCotizacion").text("");
          console.log(resp);
          if (resp == "OK") {
              $("#tituloForm").text("Cotización actualizada correctamente");
              $("#aceptar").css("display","none");

              $("#enviarCotizacionApro").attr("data-dismiss", "modal");
              location.reload();
          } else {
              $("#tituloForm").text("Error al actualizar, por favor intenta de nuevo");
              $("#cancelarCoti").attr("disabled", false);
              $("#enviarCotizacionApro").attr("disabled", false);
          }
        //Cierre de success
        },
        error:function(error){
            $(".cargaIcono").css("display","none");
            $("#errorEnvioCotizacion").text("Error al enviar la cotización, intente nuevamente");
            $("#cancelarCoti").attr("disabled", false);
            $("#enviarCotizacionApro").attr("disabled", false);
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}
