//Verificamos que no se quede el numero orden vacio
// function validarEstado(x){
//     if ($("#idOrdenTemp").val() == "" ) {
//         $("#aceptarCotizacion").attr("disabled", true);
//         $("#terminarCotizacion").attr("disabled", true);
//     } else {
//         $("#aceptarCotizacion").attr("disabled", false);
//         $("#terminarCotizacion").attr("disabled", false);
//     }
// }

//Si la vista es la de refacciones, desbloqueamos los campos
if (($("#modoVistaFormulario").val() == "4") && ($("#formularioHoja").val() == "CotizacionMultipunto") && ($("#envioRefacciones").val() == "0")) {
    $("input").attr("disabled", false);
    $(".blockCampo").attr("disabled", true);
    $("textarea").attr("disabled", false);

    $(".refaccionesBox").attr("disabled", false);
    $(".refaccionesBox").attr("data-toggle", "modal");

    //Para anticipos
    $("#passAnticipo").attr("disabled", false);
    $("#usuarioAnticipo").attr("disabled", false);
    $("#cantidadAnticipo").attr("disabled", false);
    $("#comentarioAnticipo").attr("disabled", false);
}

//Si ya firmo el asesor, se bloquea todo (para asesor y refacciones)
if (($("#rutaFirmaAsesorCostos").val() != "")&&($("#rolVista").val() != "lista")&&($("#modoVistaFormulario").val() != "8")&&($("#modoVistaFormulario").val() != "5A")&&($("#modoVistaFormulario").val() != "3A")) {
    console.log("Bloqueo general");
    //bloqueamos las celdas
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
    //ocultamos los botonoes
    $("#aceptarCotizacion").css("display", 'none');
    $("#terminarCotizacion").css("display", 'none');
    $("#terminarCotizacion2").css("display", 'none');
    $(".refaccionesBox").css("display", 'none');
    $("#regresarBtn").css("display", 'inline-table');
    //Ocultamos los indicadores de la table
    $("#agregarCM").css("display", 'none');
    $("#eliminarCM").css("display", 'none');
    //Negamos las busquedas
    $(".refaccionesBox").attr("disabled", true);
    $(".refaccionesBox").attr("data-toggle", false);
}

// //Si aun no ha firmado el asesor (y la cotizacion no se a finalizado)
// if (($("#rutaFirmaAsesorCostos").val() == "") && ($("#formularioHoja").val() == "CotizacionMultipunto")) {
//     $("#regresarBtn").css("display", 'none');
//     $("#AsesorCostos").css("display", 'none');
// }

//Si ya se envio esta cotizacion a un asesor por parte de refacciones, se blouea todo
if ((($("#envioRefacciones").val() == "1") || ($("#envioRefacciones").val() == "2")) && ($("#modoVistaFormulario").val() == "4")) {
      console.log("Bloqueo para refacciones");
      $("input").attr("disabled", true);
      $("textarea").attr("disabled", true);
      $("#AsesorCostos").css("display", 'none');
      $("#terminarCotizacion").css("display", 'none');
          $("#terminarCotizacion2").css("display", 'none');
          $(".refaccionesBox").css("display", 'none');
      $("#aceptarCotizacion").css("display", 'none');
      $("#agregarCM").css("display", 'none');
      $("#eliminarCM").css("display", 'none');
      $("#regresarBtn").css("display", 'inline-table');

      //Negamos las busquedas
      $(".refaccionesBox").attr("disabled", true);
      $(".refaccionesBox").attr("data-toggle", false);

      //Para anticipos
      $("#passAnticipo").attr("disabled", false);
      $("#usuarioAnticipo").attr("disabled", false);
      $("#cantidadAnticipo").attr("disabled", false);
      $("#comentarioAnticipo").attr("disabled", false);
}

//Si no lo ha revisado el de refacciones
if (($("#envioRefacciones").val() == "0")&&($("#modoVistaFormulario").val() == "2")&&($("#modoVistaFormulario").val() != "8")) {
      console.log("Bloqueo para asesor");
      $("input").attr("disabled", true);
      $("textarea").attr("disabled", true);
      $("#AsesorCostos").css("display", 'none');
      $("#terminarCotizacion").css("display", 'none');
          $("#terminarCotizacion2").css("display", 'none');
          $(".refaccionesBox").css("display", 'none');
      $("#aceptarCotizacion").css("display", 'none');
      $("#agregarCM").css("display", 'none');
      $("#eliminarCM").css("display", 'none');
      $("#regresarBtn").css("display", 'inline-table');
}

//Si ya se envio por parte de refacciones y no ha firmado el asesor
if (($("#envioRefacciones").val() == "1")&&($("#modoVistaFormulario").val() == "2")&&($("#rutaFirmaAsesorCostos").val() == "")) {
      console.log("Desbloqueo para asesor");
      $("input").attr("disabled", false);
      $(".blockCampo").attr("disabled", true);
      $("textarea").attr("disabled", false);
      // $("#AsesorCostos").css("display", 'none');
      // $("#terminarCotizacion").css("display", 'none');
      //     $("#terminarCotizacion2").css("display", 'none');
      //     $(".refaccionesBox").css("display", 'none');
      // $("#agregarCM").css("display", 'none');
      // $("#eliminarCM").css("display", 'none');
      $("#aceptarCotizacion").css("display", 'inline-table');
      $("#regresarBtn").css("display", 'none');
      $("#asesorNombre").attr("disabled", true);
      $("#Asesor_firma").attr("disabled", true);
      $("#Asesor_firma").attr("data-toggle", false);
} 

//Si ya se envio por parte de refacciones y no ha firmado el asesor
if ( ($("#envioRefacciones").val() == "0") && ($("#modoVistaFormulario").val() == "2") && ( ($("#usuarioActivo").val() == "")||($("#usuarioActivo").val() == "mylsaqtr")) ) {
      console.log("Desbloqueo para jefe de refacciones");
      $("input").attr("disabled", false);
      $(".blockCampo").attr("disabled", true);
      $("textarea").attr("disabled", false);
      // $("#AsesorCostos").css("display", 'none');
      // $("#terminarCotizacion").css("display", 'none');
      //     $("#terminarCotizacion2").css("display", 'none');
              // $(".refaccionesBox").css("display", 'none');
      // $("#agregarCM").css("display", 'none');
      // $("#eliminarCM").css("display", 'none');
      $("#aceptarCotizacion").css("display", 'inline-table');
      $("#regresarBtn").css("display", 'none');
}

//Si fue enviado al asesor directamente, bloqueamos al de refacciones
if (($("#envioRefacciones").val() == "2")&&($("#modoVistaFormulario").val() != "5A")&&($("#envioJDT").val() != "1")) {
  console.log("Enviado directo al jefe de taller");
    // $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
    var renglonesCuenta = $("#indiceTablaMateria").val();
    for (var i = 1; i <= renglonesCuenta; i++) {
        $("#cantidad_"+i).attr("disabled", true);
        $("#costoCM_"+i).attr("disabled", true);
        $("#horas_"+i).attr("disabled", true);
        $("#totalReng_"+i).attr("disabled", true);
        $("#apuntaSi_"+i).attr("disabled", true);
        $("#apuntaNo_"+i).attr("disabled", true);
        $("#apuntaPlanta_"+i).attr("disabled", true);
    }

    $(".aceptarCotizacion").attr("disabled", false);
    $(".autorizaCheck").attr("disabled", false);
}


//Si aun no ha firmado el asesor  y es un tecnico el que quiere editar
//if (($("#envioRefacciones").val() == "0")&&($("#modoVistaFormulario").val() == "2")&&($("#dirige").val() != "TEC")) {
if (($("#modoVistaFormulario").val() == "2")&&($("#dirige").val() == "TEC")) {
      console.log("Bloqueo para asesor");
      $("input").attr("disabled", false);
      $(".blockCampo").attr("disabled", true);
      $("textarea").attr("disabled", false);
      $("#AsesorCostos").css("display", 'none');
      $("#terminarCotizacion").css("display", 'none');
          $("#terminarCotizacion2").css("display", 'none');
          $(".refaccionesBox").css("display", 'none');
      $("#aceptarCotizacion").css("display", 'none');
      $("#agregarCM").css("display", 'none');
      $("#eliminarCM").css("display", 'none');
      //$("#regresarBtn").css("display", 'inline-table');
}


//Brincar validaciones  (Validaciones extras)
//Si ya firmo el asesor, se bloquea todo (para asesor y refacciones)
if ($("#modoVistaFormulario").val() == "8") {
    console.log("Desbloqueo de reglas para actualizacion libre");
    //bloqueamos las celdas
    $("input").attr("disabled", false);
    $(".blockCampo").attr("disabled", true);
    $("textarea").attr("disabled", false);
    //ocultamos los botonoes
    $("#aceptarCotizacion").css("display", 'inline-table');
    $("#terminarCotizacion").css("display", 'inline-table');
    $("#terminarCotizacion2").css("display", 'inline-table');
    $(".refaccionesBox").css("display", 'inline-table');
    //$("#regresarBtn").css("display", 'inline-table');
    //Ocultamos los indicadores de la table
    $("#agregarCM").css("display", 'inline-table');
    $("#eliminarCM").css("display", 'inline-table');
}

//Si se entra a la vista de actualizar de garantias y ya se envio la cotizacion
if (($("#envioGarantia").val() == "1")&&($("#modoVistaFormulario").val() == "3A")) {
    console.log("Bloqueo para garantias si ya ha afectado la cotización");
    //bloqueamos las celdas
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
    //ocultamos los botonoes
    $("#aceptarCotizacion").css("display", 'none');
    $("#terminarCotizacion").css("display", 'none');
    $("#terminarCotizacion2").css("display", 'none');
    $(".refaccionesBox").css("display", 'none');
    $("#AsesorCostos").css("display", 'none');
    //$("#regresarBtn").css("display", 'inline-table');
    //Ocultamos los indicadores de la table
    //$("#agregarCM").css("display", 'none');
    //$("#eliminarCM").css("display", 'none');
}

//Si es la vista de jefe de taller y aun no ha enviado la cotizacion
if (($("#envioJDT").val() == "1")&&($("#modoVistaFormulario").val() == "5A")) {
    console.log("Bloqueo para garantias si ya ha afectado la cotización");
    //bloqueamos las celdas
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
    //ocultamos los botonoes
    $("#aceptarCotizacion").css("display", 'none');
    $("#terminarCotizacion").css("display", 'none');
    $("#terminarCotizacion2").css("display", 'none');
    $(".refaccionesBox").css("display", 'none');
    $("#AsesorCostos").css("display", 'none');
    //$("#regresarBtn").css("display", 'inline-table');
    //Ocultamos los indicadores de la table
    //$("#agregarCM").css("display", 'none');
    //$("#eliminarCM").css("display", 'none');
}else if (($("#envioJDT").val() == "0")&&($("#modoVistaFormulario").val() == "5A")) {
    //Negamos la firma hasta confirmar las refacciones
    //$("#Coti_JDT_firma").attr("disabled", true);
    //$("#Coti_JDT_firma").attr("data-toggle", false);
}