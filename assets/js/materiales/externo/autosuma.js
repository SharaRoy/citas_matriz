// Funciones de autoguardado de renglon
function  descripcionCollect(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

}

function  piezaCollet(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

}

function autorizaColet(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

}

function existeCollet(indice){

    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

}

$('.autorizaCheck').on('click', function() {
    var indice = $(this).val();

    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
});


//Funciones con operaciones y autoguardado
function cantidadCollet(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

    //Verificamos que no se haya dejado el espacio vacio
    if ($("#cantidad_"+indice).val() == "") {
        $("#cantidad_"+indice).val("0");
    }else {
        //Recuperamos los valores del renglon
         var cantidad = parseFloat($("#cantidad_"+indice).val());
         var costoPieza = parseFloat($("#costoCM_"+indice).val());
         var costoHora = parseFloat($("#totalReng_"+indice).val());
         var horasTrabajo = parseFloat($("#horas_"+indice).val());

         //Recuperamos los valores finales (subtotal) y evitamos error de formato
         var limpieza = $("#subTotalMaterial").val().replace(",","");
         var subGral = parseFloat(limpieza);

         //Sacamos los nuevos valores
         var op_0 = cantidad * costoPieza;
         var op_1 = horasTrabajo * costoHora;
         var op_2 = op_1 + op_0;
         // console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
         //Actualizamos el valor del renglon
         $("#totalOperacion_"+indice).val(op_2);

         // Evitamos la basura en cotizaciones anteriores
         var conteoTotal = 0;
         var renglonesCuenta = $("#indiceTablaMateria").val();
         for (var i = 1; i <= renglonesCuenta; i++) {
             var cantidadPza = parseFloat($("#cantidad_"+i).val());
             var costoPza = parseFloat($("#costoCM_"+i).val());
             var horasMo = parseFloat($("#horas_"+i).val());
             var costoMo = parseFloat($("#totalReng_"+i).val());
             var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
             // console.log(totalRenglon);
             conteoTotal += totalRenglon;
         }
         // console.log("conteo: "+conteoTotal);

         var anticipo = parseFloat($("#anticipoMaterial").val());
         var iva = conteoTotal * 0.16;
         var totalV1 = conteoTotal + iva;
         var total = totalV1 - anticipo;
         // var total = conteoTotal / 1.16;
         // var iva = conteoTotal - total;
         // console.log("iva: "+iva);
         // console.log("total: "+total);

         //Vaciamos los nuevos resultados
         $("#subTotalMaterial").val(parseFloat(conteoTotal));
         $("#ivaMaterial").val(parseFloat(iva));
         $("#totalMaterial").val(parseFloat(total));

         $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
         $("#ivaMaterialLabel").text(iva.toFixed(2));
         $("#totalMaterialLabel").text(total.toFixed(2));
         $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

         //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
         $("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+costoHora+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);
    }

}

function  costoCollet(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

    //Verificamos que no se haya dejado el espacio vacio
    if ($("#costoCM_"+indice).val() == "") {
        $("#costoCM_"+indice).val("0");

        //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
        $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
    }else {
        //Recuperamos los valores del renglon
        var cantidad = parseFloat($("#cantidad_"+indice).val());
        var costoPieza = parseFloat($("#costoCM_"+indice).val());
        var costoHora = parseFloat($("#totalReng_"+indice).val());
        var horasTrabajo = parseFloat($("#horas_"+indice).val());

        //Recuperamos los valores finales (subtotal) y evitamos error de formato
        var limpieza = $("#subTotalMaterial").val().replace(",","");
        var subGral = parseFloat(limpieza);

        //Sacamos los nuevos valores
        var op_0 = cantidad * costoPieza;
        var op_1 = horasTrabajo * costoHora;
        var op_2 = op_1 + op_0;
        // console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
        //Actualizamos el valor del renglon
        $("#totalOperacion_"+indice).val(op_2);

        // Evitamos la basura en cotizaciones anteriores
        var conteoTotal = 0;
        var renglonesCuenta = $("#indiceTablaMateria").val();
        for (var i = 1; i <= renglonesCuenta; i++) {
            var cantidadPza = parseFloat($("#cantidad_"+i).val());
            var costoPza = parseFloat($("#costoCM_"+i).val());
            var horasMo = parseFloat($("#horas_"+i).val());
            var costoMo = parseFloat($("#totalReng_"+i).val());
            var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
            // console.log(totalRenglon);
            conteoTotal += totalRenglon;
        }
        // console.log("conteo: "+conteoTotal);

        var anticipo = parseFloat($("#anticipoMaterial").val());
        var iva = conteoTotal * 0.16;
        var totalV1 = conteoTotal + iva;
        var total = totalV1 - anticipo;
        // var total = conteoTotal / 1.16;
        // var iva = conteoTotal - total;
        // console.log("iva: "+iva);
        // console.log("total: "+total);

        //Vaciamos los nuevos resultados
        $("#subTotalMaterial").val(parseFloat(conteoTotal));
        $("#ivaMaterial").val(parseFloat(iva));
        $("#totalMaterial").val(parseFloat(total));

        $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
        $("#ivaMaterialLabel").text(iva.toFixed(2));
        $("#totalMaterialLabel").text(total.toFixed(2));
        $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

        //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
        $("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+costoHora+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);
    }
}

function  horasCollet(indice){
    //Recuperamos los valores de toda la fila y los guardamos provisionalmente
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Verificamos que no se haya dejado el espacio vacio
    if ($("#horas_"+indice).val() == "") {
        $("#horas_"+indice).val("0");

        //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
        $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
    }else {
        //Verificamos si se cambio el valor del campo
        var valores = $("#valoresCM_"+indice).val();
        var campos = valores.split("_");
        var horasTrabajo = parseFloat($("#horas_"+indice).val());
        //console.log(campos);
        if (horasTrabajo != campos[3]) {
            //Recuperamos los valores del renglon
            var cantidad = parseFloat($("#cantidad_"+indice).val());
            var costoPieza = parseFloat($("#costoCM_"+indice).val());
            var costoHora = parseFloat($("#totalReng_"+indice).val());

            var cantiAnterior = parseFloat($("#totalOperacion_"+indice).val());

            //Recuperamos los valores finales (subtotal) y evitamos error de formato
            var limpieza = $("#subTotalMaterial").val().replace(",","");
            var subGral = parseFloat(limpieza);

            //Sacamos los nuevos valores
            var op_0 = cantidad * costoPieza;
            var op_1 = horasTrabajo * costoHora;
            var op_2 = op_1 + op_0;
            // console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
            //Actualizamos el valor del renglon
            $("#totalOperacion_"+indice).val(op_2);

            // Evitamos la basura en cotizaciones anteriores
            var conteoTotal = 0;
            var renglonesCuenta = $("#indiceTablaMateria").val();
            for (var i = 1; i <= renglonesCuenta; i++) {
                // if (($("#totalOperacion_"+i).val() != "0")&&($("#horas_"+i).val() != "0")) {
                  // conteoTotal += parseFloat($("#totalOperacion_"+i).val());
                  //Como alternativa, se puede hacer la cuenta completa por renglon
                  var cantidadPza = parseFloat($("#cantidad_"+i).val());
                  var costoPza = parseFloat($("#costoCM_"+i).val());
                  var horasMo = parseFloat($("#horas_"+i).val());
                  var costoMo = parseFloat($("#totalReng_"+i).val());
                  var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                  // console.log(totalRenglon);
                  conteoTotal += totalRenglon;
                // }
            }
            // console.log("conteo: "+conteoTotal);

            var anticipo = parseFloat($("#anticipoMaterial").val());
            var iva = conteoTotal * 0.16;
            var totalV1 = conteoTotal + iva;
            var total = totalV1 - anticipo;
            // var total = conteoTotal / 1.16;
            // var iva = conteoTotal - total;
            // console.log("iva: "+iva);
            // console.log("total: "+total);

            //Vaciamos los nuevos resultados
            $("#subTotalMaterial").val(parseFloat(conteoTotal));
            $("#ivaMaterial").val(parseFloat(iva));
            $("#totalMaterial").val(parseFloat(total));

            $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#totalMaterialLabel").text(total.toFixed(2));
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

            //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
            $("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+costoHora+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);
        }else{
            //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
            $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
            console.log("No se modifico el campo");
        }
    }
}

//Nvo procedimiento de la funcion autosuma
function autosuma(indice){

    //Verificamos que no se haya dejado el espacio vacio
    if ($("#totalReng_"+indice).val() == "") {
        $("#totalReng_"+indice).val("0");
    }else {
        //Verificamos si se cambio el valor del campo
        var valores = $("#valoresCM_"+indice).val();
        var campos = valores.split("_");
        var costoHora = parseFloat($("#totalReng_"+indice).val());
        //console.log(campos);
        if (costoHora != campos[4]) {
            //Recuperamos los valores del renglon
            var cantidad = parseFloat($("#cantidad_"+indice).val());
            var costoPieza = parseFloat($("#costoCM_"+indice).val());
            var horasTrabajo = parseFloat($("#horas_"+indice).val());

            var cantiAnterior = parseFloat($("#totalOperacion_"+indice).val());

            //Recuperamos los valores finales (subtotal) y evitamos error de formato
            var limpieza = $("#subTotalMaterial").val().replace(",","");
            var subGral = parseFloat(limpieza);

            //Sacamos los nuevos valores
            var op_0 = cantidad * costoPieza;
            var op_1 = horasTrabajo * costoHora;
            var op_2 = op_1 + op_0;
            // console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
            //Actualizamos el valor del renglon
            $("#totalOperacion_"+indice).val(op_2);

            // Evitamos la basura en cotizaciones anteriores
            var conteoTotal = 0;
            var renglonesCuenta = $("#indiceTablaMateria").val();
            for (var i = 1; i <= renglonesCuenta; i++) {
                // if (($("#totalOperacion_"+i).val() != "0")&&($("#horas_"+i).val() != "0")) {
                    // conteoTotal += parseFloat($("#totalOperacion_"+i).val());
                    //Como alternativa, se puede hacer la cuenta completa por renglon
                    var cantidadPza = parseFloat($("#cantidad_"+i).val());
                    var costoPza = parseFloat($("#costoCM_"+i).val());
                    var horasMo = parseFloat($("#horas_"+i).val());
                    var costoMo = parseFloat($("#totalReng_"+i).val());
                    var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                    // console.log(totalRenglon);
                    conteoTotal += totalRenglon;
                // }
            }
            // console.log("conteo: "+conteoTotal);
            var anticipo = parseFloat($("#anticipoMaterial").val());
            var iva = conteoTotal * 0.16;
            var totalV1 = conteoTotal + iva;
            var total = totalV1 - anticipo;
            // var total = conteoTotal / 1.16;
            // var iva = conteoTotal - total;
            // console.log("iva: "+iva);
            // console.log("total: "+total);

            //Vaciamos los nuevos resultados
            $("#subTotalMaterial").val(parseFloat(conteoTotal));
            $("#ivaMaterial").val(parseFloat(iva));
            $("#totalMaterial").val(parseFloat(total));

            $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#totalMaterialLabel").text(total.toFixed(2));
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

            //Actualizamos los valores del renglon
            var campos_2 = $("#descripcion_"+indice).val();
            var campos_7 = $("#pieza_"+indice).val();

            if($("#autorizo_"+indice).is(':checked') ){
                var campos_6 = "SI";
            } else {
                var campos_6 = "NO";
            }

            if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
                var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
            }else {
                var campos_9 = "";
            }

            //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
            $("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+costoHora+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);
        }else{
            console.log("No se modifico el campo");
        }
    }
}
