if ($("#idOrdenTempCotiza").length > 0) {
    var serie = $("#idOrdenTempCotiza").val().trim();

    if ((serie != "")&&(serie != " ")&&(serie != "0")) {
        cargarDatosCoti(serie);
    }
}

//Verificamos que no se quede el numero orden vacio
function validarEstado(x){
    if ($("#idOrdenTempCotiza").val() == "" ) {
        $("#aceptarCotizacion").attr("disabled", true);
        $("#terminarCotizacion").attr("disabled", true);
    } else {
        $("#aceptarCotizacion").attr("disabled", false);
        $("#terminarCotizacion").attr("disabled", false);

        //Hacemos la peticion nuevamente para recuperar los datos
        cargarDatosCoti($("#idOrdenTempCotiza").val());
    }
}

//Cargamos los datos a visualizar en la cotización
function cargarDatosCoti(idOrden) {
    var url = $("#basePeticion").val();
    $.ajax({
        url: url+"multipunto/Multipunto/cargaDatos",
        method: 'post',
        data: {
            idOrden: idOrden,
        },
        success:function(resp){
            // console.log(resp);
            console.log("Cotizacion: "+resp);
            if (resp.indexOf("handler			</p>")<1) {

                var campos = resp.split("_");
                // console.log(campos);
                if (campos.length > 2) {
                    $("#noSerieText").val(campos[0]);
                    $("#txtmodelo").val(campos[1]);
                    $("#placas").val(campos[2]);
                    $("#uen").val(campos[3]);
                    $("#tecnico").val(campos[4]);
                    $("#asesors").val(campos[5]);
                    $("#nombreCamapanias").text("Campaña(s): "+campos[6]);
                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

//Proceso para refacciones (autorizar anticipo)
//Validamos credenciales del usuario
$("#btnCredencialAnticipo").on('click',function() {
    $("#AnticipoError").text("");
    //Recuperamos las credenciales
    var usuario = $("#usuarioAnticipo").val();
    var pass = $("#passAnticipo").val();

    //Verificamos los campos
    if ((usuario != "") && (pass != "")) {
        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/CotizacionesExternas/usuarioAnticipo",
            method: 'post',
            data: {
                user: usuario,
                pass: pass
            },
            success:function(resp){
                // console.log(resp);
                // console.log("Autorizacion anticipo: "+resp);
                if (resp.indexOf("handler			</p>")<1) {
                    if (resp == "OK") {
                        //Si las claves con correctas mostramos opcion de anticipo
                        $("#addAnticipo").css('display','block');
                        $("#credencialesAnticipo").css('display','none');
                    }else {
                        $("#AnticipoError").text(resp+".");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else {
        $("#AnticipoError").text("INFORMACIÓN INCOMPLETA.");
    }
});

//Guardamos el anticipo de la cotizacion
$("#btnAddAnticipo").on('click',function() {
    $("#AnticipoError").text("");
    //Recuperamos los valores
    var cantidad = $("#cantidadAnticipo").val();
    var comentario = $("#comentarioAnticipo").val();
    var idCoti = $("#idCotizacion").val();

    //Verificamos los campos
    if ((cantidad != "") && (idCoti != "")) {
        //Hacesmos la cuenta existente
        // Evitamos la basura en cotizaciones anteriores
        var conteoTotal = 0;
        var renglonesCuenta = $("#indiceTablaMateria").val();
        for (var i = 1; i <= renglonesCuenta; i++) {
            if (($("#totalOperacion_"+i).val() != "0")&&($("#horas_"+i).val() != "0")) {
                // conteoTotal += parseFloat($("#totalOperacion_"+i).val());
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var cantidadPza = parseFloat($("#cantidad_"+i).val());
                var costoPza = parseFloat($("#costoCM_"+i).val());
                var horasMo = parseFloat($("#horas_"+i).val());
                var costoMo = parseFloat($("#totalReng_"+i).val());
                var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                // console.log(totalRenglon);
                conteoTotal += totalRenglon;
            }
        }
        // console.log("conteo: "+conteoTotal);

        var iva = conteoTotal * 0.16;
        var total = conteoTotal + iva;
        var totalAnticipo = total - cantidad;

        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/CotizacionesExternas/agregarAnticipo",
            method: 'post',
            data: {
                cantidad: cantidad,
                comentario: comentario,
                idCoti: idCoti,
                total: totalAnticipo
            },
            success:function(resp){
                console.log(resp);
                // console.log("Autorizacion anticipo: "+resp);
                if (resp.indexOf("handler			</p>")<1) {
                    if (resp == "OK") {
                        //Si se guardo correctamente la información

                        $("#credencialesAnticipo").css('display','block');
                        $("#addAnticipo").css('display','none');

                        $("#credencialesAnticipo").css('display','block');
                        $("#addAnticipo").css('display','none');
                        $("#passAnticipo").attr("disabled", true);
                        $("#usuarioAnticipo").attr("disabled", true);
                        $("#btnCredencialAnticipo").css('display','none');

                        //Bajamos el anticipo a la tabla
                        $("#totalMaterial").val(parseFloat(totalAnticipo));
                        $("#totalMaterialLabel").text(totalAnticipo.toFixed(2));

                        $("#anticipoMaterial").val(parseFloat(cantidad));
                        $("#anticipoMaterialLabel").text(parseFloat(cantidad).toFixed(2));

                        $("#presupuestoMaterialLabel").text(total.toFixed(2));

                        $("#AnticipoError").css("color","green");
                        $("#AnticipoError").text("ANTICIPO GUARDADO.");
                        $("#anticipoNota").text(comentario);
                    }else {
                        $("#AnticipoError").text(resp+".");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else {
        $("#AnticipoError").text("INFORMACIÓN INCOMPLETA.");
    }
});
