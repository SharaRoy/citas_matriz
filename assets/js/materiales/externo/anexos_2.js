//Verificamos el tipo de filtro que se utilizara para
$("input[name='filtros']").on('change',function(){
		var filtro = $("input[name='filtros']:checked").val();

		if (filtro == "descripcion") {
				$(".busqueda_clave").attr("disabled",true);
				$("#descripcionRefaccion").attr("disabled",false);

				var url = $("#basePeticion").val();
				var renglon = $("#indiceOperacion").val();
				var valor_descripcion = $("#descripcion_"+renglon).val();

				$.ajax({
						url: url+"multipunto/CotizacionesExternas/filtrosDescripcion",
						method: 'post',
						data: {
								valor_1: valor_descripcion,
								dato_1: "descripcion",
						},
						success:function(resp){
								// console.log(resp);
								if (resp.indexOf("handler			</p>")<1) {
										var campos = resp.split("_");
										var select = $("#descripcionRefaccion");
										select.empty();
										// select.append("<option value=''>Selecciona...</option>");
										var regitro = ( campos.length > 1) ? campos.length -1 : campos.length;
										for (var i = 0; i < regitro; i++) {
												var texto = ( campos[i] != "") ? campos[i].trim() : '-';
												select.append("<option value='"+campos[i]+"'>"+texto+"</option>");
										}
								}
						//Cierre de success
						},
						error:function(error){
								console.log(error);
						//Cierre del error
						}
				//Cierre del ajax
				});
		}else {
				$("#descripcionRefaccion").attr("disabled",true);
				$(".busqueda_clave").attr("disabled",false);
		}
});

//Conprobamos que la cantidad seleccionada no sea 0
$("#cantidadRefacciones").on('change', function() {
		if ($("#cantidadRefacciones").val() < 1) {
        $("#bajaOperacion").attr('disabled',true);
    }else{
        $("#bajaOperacion").attr('disabled',false);
    }
});

//Agregamos el indice del renglon a editar
$(".refaccionesBox").on('click',function(){
    var renglon = $(this).data("id");
		// console.log("renglon->"+renglon);
    $("#indiceOperacion").val(renglon);
});

//Interacciones de los  filtros
$("#grupoRefaccion").change(function(){
    var frac_1 = $("#grupoRefaccion").val().trim();
    var frac = frac_1;
    // console.log("cadena: "+frac);
    if (frac != "") {
        //Limpiamos los campos
        limpiarCampor();

        $("#prefijoRefaccion").empty();
        $("#prefijoRefaccion").append("<option value=''>Selecciona...</option>");
        $("#basicoRefaccion").empty();
        $("#basicoRefaccion").append("<option value=''>Selecciona...</option>");
        $("#sufijoRefaccion").empty();
        $("#sufijoRefaccion").append("<option value=''>Selecciona...</option>");

        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/CotizacionesExternas/filtrosRef",
            method: 'post',
            data: {
                valor_1: frac,
                valor_2: frac,
                dato_1: "prefijo",
                dato_2: "gpo",
            },
            success:function(resp){
                // console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
                    var campos = resp.split("|");
                    var select = $("#prefijoRefaccion");
                    select.empty();
                    select.append("<option value=''>Selecciona...</option>");
                    var regitro = ( campos.length > 1) ? campos.length -1 : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var texto = ( campos[i] != "") ? campos[i].trim() : '-';
                        select.append("<option value='"+campos[i]+"'>"+texto+"</option>");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

$("#prefijoRefaccion").change(function(){
    var frac_1 = $("#grupoRefaccion").val().trim();
    var frac_2 = $("#prefijoRefaccion").val().trim();
    var frac = frac_1+""+frac_2;
    // console.log("cadena: "+frac);
    if (frac != "") {
        //Limpiamos los campos
        limpiarCampor();

        $("#basicoRefaccion").empty();
        $("#basicoRefaccion").append("<option value=''>Selecciona...</option>");
        $("#sufijoRefaccion").empty();
        $("#sufijoRefaccion").append("<option value=''>Selecciona...</option>");
        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/CotizacionesExternas/filtrosRef",
            method: 'post',
            data: {
                valor_1: frac,      //Valor de la cadena principal
                valor_2: frac_2,      //Valor del filtro anterior
                dato_1: "basico",   //Columan a buscar
                dato_2: "prefijo",  //Columna del filtro anterior
            },
            success:function(resp){
                // console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
                    var campos = resp.split("|");
                    var select = $("#basicoRefaccion");
                    select.empty();
                    select.append("<option value=''>Selecciona...</option>");
                    var regitro = ( campos.length > 1) ? campos.length -1 : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var texto = ( campos[i] != "") ? campos[i].trim() : '-';
                        select.append("<option value='"+campos[i]+"'>"+texto+"</option>");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

$("#basicoRefaccion").change(function(){
    var frac_1 = $("#grupoRefaccion").val();
    var frac_2 = $("#prefijoRefaccion").val();
    var frac_3 = $("#basicoRefaccion").val();

    var frac = frac_1.trim()+frac_2.trim()+frac_3.trim();
    // console.log("cadena: "+frac);
    if (frac != "") {
        //Limpiamos los campos
        limpiarCampor();

        $("#sufijoRefaccion").empty();
        $("#sufijoRefaccion").append("<option value=''>Selecciona...</option>");

        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/CotizacionesExternas/filtrosRef",
            method: 'post',
            data: {
                valor_1: frac,      //Valor de la cadena principal
                valor_2: frac_3,      //Valor del filtro anterior
                dato_1: "sufijo",   //Columan a buscar
                dato_2: "basico",   //Columna del filtro anterior
            },
            success:function(resp){
                // console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
                    var campos = resp.split("|");
                    var select = $("#sufijoRefaccion");
                    select.empty();
                    select.append("<option value=''>Selecciona...</option>");
                    var regitro = ( campos.length > 1) ? campos.length -1 : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var texto = ( campos[i] != "") ? campos[i].trim() : '-';
                        select.append("<option value='"+campos[i]+"'>"+texto+"</option>");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

$("#sufijoRefaccion").change(function(){
    var frac_1 = $("#grupoRefaccion").val();
    var frac_2 = $("#prefijoRefaccion").val();
    var frac_3 = $("#basicoRefaccion").val();
    var frac_4 = $("#sufijoRefaccion").val();
    var frac = frac_1.trim()+frac_2.trim()+frac_3.trim()+frac_4.trim();
    // console.log("cadena: "+frac);
    if (frac != "") {
        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/CotizacionesExternas/filtrosRefFinal",
            method: 'post',
            data: {
                dato: frac,
								pivote: "clave_comprimida",
            },
            success:function(resp){
                // console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
										$("#descripcionRefacciones").attr("disabled",true);
										$("#fueRefacciones").attr("disabled",true);
										$("#claRefacciones").attr("disabled",true);
										$("#precioRefacciones").attr("disabled",true);
										$("#cantidadRefacciones").attr("disabled",true);
										$("#existeRefacciones").attr("disabled",true);
										$("#ubicacionRefacciones").attr("disabled",true);

                    var campos = resp.split("|");
                    // $("#descripcionRefacciones").val(campos[0]);
										var select = $("#descripcionRefaccion");
										select.empty();
										select.append("<option value='"+campos[0]+"'>"+campos[0]+"</option>");
                    $("#fueRefacciones").val(campos[1]);
                    $("#claRefacciones").val(campos[2]);
                    $("#precioRefacciones").val(campos[3]);
										$("#cantidadRefacciones").val(campos[4]);
                    // $("#cantidadRefacciones").attr({
                    //    "max" : campos[4],
                    //    "min" : 1
                    // });

                    if (campos[4] > "0") {
                        $("#existeRefacciones").val("Si");
                    }else {
                        $("#existeRefacciones").val("No");
                    }

                    $("#ubicacionRefacciones").val(campos[5]);
                    $("#claveRefaccion").val(frac);
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

$("#descripcionRefaccion").change(function(){
		var url = $("#basePeticion").val();
		var renglon = $("#indiceOperacion").val();
		var valor_descripcion = $("#descripcionRefaccion").val();
		var clave_1 = valor_descripcion.split(")");
		var clave_limpia = clave_1[0].substring(1, clave_1[0].length);

    if (clave_limpia != "") {
        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/CotizacionesExternas/filtrosRefFinal",
            method: 'post',
            data: {
                dato: clave_limpia,
								pivote: "clave_comprimida",
            },
            success:function(resp){
                // console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
										$("#descripcionRefacciones").attr("disabled",true);
										$("#fueRefacciones").attr("disabled",true);
										$("#claRefacciones").attr("disabled",true);
										$("#precioRefacciones").attr("disabled",true);
										$("#cantidadRefacciones").attr("disabled",true);
										$("#existeRefacciones").attr("disabled",true);
										$("#ubicacionRefacciones").attr("disabled",true);

                    var campos = resp.split("|");
										// console.log(campos);
                    // $("#descripcionRefacciones").val(campos[0]);
										$("#descripcionRefaccion").empty();
										$("#descripcionRefaccion").append("<option value='"+campos[0]+"'>"+campos[0]+"</option>");
                    $("#fueRefacciones").val(campos[1]);
                    $("#claRefacciones").val(campos[2]);
                    $("#precioRefacciones").val(campos[3]);
										$("#cantidadRefacciones").val(campos[4]);

										document.getElementById("grupoRefaccion").value = campos[6];
										$("#prefijoRefaccion").empty();
										$("#prefijoRefaccion").append("<option value='"+campos[7]+"'>"+campos[7]+"</option>");
										$("#basicoRefaccion").empty();
										$("#basicoRefaccion").append("<option value='"+campos[8]+"'>"+campos[8]+"</option>");
										$("#sufijoRefaccion").empty();
										$("#sufijoRefaccion").append("<option value='"+campos[9]+"'>"+campos[9]+"</option>");
                    // $("#cantidadRefacciones").attr({
                    //    "max" : campos[4],
                    //    "min" : 1
                    // });

                    if (campos[4] > "0") {
                        $("#existeRefacciones").val("Si");
                    }else {
                        $("#existeRefacciones").val("No");
                    }

                    $("#ubicacionRefacciones").val(campos[5]);
                    $("#claveRefaccion").val(clave_limpia);
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

//Cuando precionamos el boton de aceptar
$("#bajaOperacion").on('click',function(){
    var renglon = $("#indiceOperacion").val();
    // console.log("renglon regreso->"+renglon);

    //Recuperamos los datos necesarios
    // var descripcion = $("#descripcionRefacciones").val();
    var precio = $("#precioRefacciones").val();
    // var cantidad = $("#cantidadRefacciones").val();
    var existe = $("#existeRefacciones").val();
    var clave = $("#claveRefaccion").val();

    //Enviamos los datos a la tabla
    // $("#cantidad_"+renglon).val(cantidad);
    // $("#descripcion_"+renglon).val(descripcion);
    $("#costoCM_"+renglon).val(precio);
    $("#pieza_"+renglon).val(clave);

		if (existe == "Si") {
        $("#apuntaSi_"+renglon).attr('checked','checked');
    }else {
        $("#apuntaNo_"+renglon).attr('checked','checked');
    }

    //Limpiamos los campos
    limpiarCampor();

    $("#basicoRefaccion").empty();
    $("#basicoRefaccion").append("<option value=''>Selecciona...</option>");
    $("#sufijoRefaccion").empty();
    $("#sufijoRefaccion").append("<option value=''>Selecciona...</option>");
    document.getElementById("grupoRefaccion").value = '';

		//Pasamos el control al precio ingreado
		document.getElementById("costoCM_"+renglon).focus();

		//Actualizamos el renglon para el guardado
		//Recuperamos los valores de toda la fila
		var campos_1 = $("#cantidad_"+renglon).val();
		var campos_2 = $("#descripcion_"+renglon).val();
		var campos_3 = $("#costoCM_"+renglon).val();
		var campos_4 = $("#horas_"+renglon).val();
		var campos_5 = $("#totalReng_"+renglon).val();
		var campos_7 = $("#pieza_"+renglon).val();
		var campos_8 = $("#totalOperacion_"+renglon).val();

		if($("#autorizo_"+renglon).is(':checked') ){
				var campos_6 = "SI";
		} else {
				var campos_6 = "NO";
		}

		if ($("input[name='existe_"+renglon+"']:radio").is(':checked')) {
				var campos_9 = $("input[name='existe_"+renglon+"']:checked").val();
		}else {
				var campos_9 = "";
		}

		//Hacemos la operacion previa de la refacciones
		//Recuperamos los valores del renglon
		var cantidad = parseFloat($("#cantidad_"+indice).val());
		var costoPieza = parseFloat($("#costoCM_"+indice).val());
		var costoHora = parseFloat($("#totalReng_"+indice).val());
		var horasTrabajo = parseFloat($("#horas_"+indice).val());

		//Recuperamos los valores finales (subtotal) y evitamos error de formato
		var limpieza = $("#subTotalMaterial").val().replace(",","");
		var subGral = parseFloat(limpieza);

		//Sacamos los nuevos valores
		var op_0 = cantidad * costoPieza;
		var op_1 = horasTrabajo * costoHora;
		var op_2 = op_1 + op_0;
		console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
		//Actualizamos el valor del renglon
		$("#totalOperacion_"+indice).val(op_2);

		// Evitamos la basura en cotizaciones anteriores
		var conteoTotal = 0;
		var renglonesCuenta = $("#indiceTablaMateria").val();
		for (var i = 1; i <= renglonesCuenta; i++) {
				var cantidadPza = parseFloat($("#cantidad_"+i).val());
				var costoPza = parseFloat($("#costoCM_"+i).val());
				var horasMo = parseFloat($("#horas_"+i).val());
				var costoMo = parseFloat($("#totalReng_"+i).val());
				var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
				// console.log(totalRenglon);
				conteoTotal += totalRenglon;
		}
		// console.log("conteo: "+conteoTotal);

		var anticipo = parseFloat($("#anticipoMaterial").val());
		var iva = conteoTotal * 0.16;
		var totalV1 = conteoTotal + iva;
		var total = totalV1 - anticipo;
		// var total = conteoTotal / 1.16;
		// var iva = conteoTotal - total;
		// console.log("iva: "+iva);
		// console.log("total: "+total);

		//Vaciamos los nuevos resultados
		$("#subTotalMaterial").val(parseFloat(conteoTotal));
		$("#ivaMaterial").val(parseFloat(iva));
		$("#totalMaterial").val(parseFloat(total));

		$("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
		$("#ivaMaterialLabel").text(iva.toFixed(2));
		$("#totalMaterialLabel").text(total.toFixed(2));

		//Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
		$("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+costoHora+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);

});

//Cuando precionamos el boton de cancelar
$("#bajaOperacion").on('click',function(){
    //Limpiamos los campos
    limpiarCampor();
		$("#indiceOperacion").val("");
    $("#prefijoRefaccion").empty();
    $("#prefijoRefaccion").append("<option value=''>Selecciona...</option>");
    $("#basicoRefaccion").empty();
    $("#basicoRefaccion").append("<option value=''>Selecciona...</option>");
    $("#sufijoRefaccion").empty();
    $("#sufijoRefaccion").append("<option value=''>Selecciona...</option>");
    document.getElementById("grupoRefaccion").value = '';

});

function limpiarCampor(){
    //Limpiamos los campos
    $("#descripcionRefacciones").val("");
    $("#fueRefacciones").val("");
    $("#claRefacciones").val("");
    $("#precioRefacciones").val("0");
    $("#cantidadRefacciones").val("1");
    $("#existeRefacciones").val("");
    $("#ubicacionRefacciones").val("");
}
