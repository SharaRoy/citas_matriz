$("#aceptar").on('click',function() {
    $("#AlertaModalCuerpo").css("background-color","#c7ecc7");
    $("#AlertaModalCuerpo").css("border-color","#4cae4c");

    $("#tituloForm").text("¿Está seguro de aceptar toda la cotización? NO podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");
    $("#desicionCoti").val("Si");
});

$("#rechazar").on('click',function() {
    $("#AlertaModalCuerpo").css("background-color","#f5acaa");
    $("#AlertaModalCuerpo").css("border-color","#d43f3a");

    $("#tituloForm").text("¿Está seguro de rechazar toda la cotización? NO  podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");
    $("#desicionCoti").val("No");
});

$("#terminarCotizacion").on('click',function() {
    $("#modoGuardado").val("Actualizar");
});

$("#terminarCotizacion2").on('click',function() {
    $("#modoGuardado").val("Terminar");
});

//Envio de formulario
$("#envioFormulario").on('click',function() {
    $("input").attr("disabled", false);
});

//Mostrar botones para aceptar cotizacion completa o por renglones
$("#seleccionarTodo").on('click',function() {
    $(".multipleEleccion").attr("hidden","hidden");
    $(".multipleEleccionBtn").attr("hidden",false);
    $(".eleccionBtn").attr("hidden","hidden");
});

$("#seleccionarFilas").on('click',function() {
    $(".multipleEleccion").attr("hidden",false);
    $(".eleccionBtn").attr("hidden",false);
    $(".multipleEleccionBtn").attr("hidden","hidden");
});

//Aceptar/Rechazar cotizacion por partes
function aceptarItem(renglon){
    //Marcamos la fila para mostrar la decision tomada por el cliente
    $("#fila_indicador_"+renglon).css("background-color","#c7ecc7");
    //Recuperamos el renglon e imprimimos la decision
    var contenidoFila = $("#valoresCM_"+renglon).val();
    //Separamos los valores para validar si existe modificacion previa
    var campos = contenidoFila.split("_");
    console.log(campos);
    //Si son mas de 7 campos , ya se habia tomado una decision
    if (campos.length > 9) {
        $("#valoresCM_"+renglon).val(campos[0]+"_"+campos[1]+"_"+campos[2]+"_"+campos[3]+"_"+campos[4]+"_"+campos[5]+"_"+campos[6]+"_"+campos[7]+"_"+campos[8]+"_"+"Aceptada");

        //Si es rechazada hacemos la resta del precio rechazado
        var montoCancelado = $("#canceladoMaterial").val();
        var nvoMontoCancelar = parseFloat(montoCancelado) - parseFloat(campos[7]);
        if (nvoMontoCancelar >= 0) {
            //Imprimimos los nuevos valores
            $("#canceladoMaterial").val(nvoMontoCancelar);
            $("#canceladoMaterialLabel").text(nvoMontoCancelar);

            //Recalculamos los totales
            var subTotal = parseFloat($("#subTotalMaterial").val());
            var anticipo = parseFloat($("#anticipoMaterial").val());
            var nvoSubTotal = subTotal - nvoMontoCancelar;
            var iva = nvoSubTotal * 0.16;

            //Imprimimos el iva
            $("#ivaMaterial").val(iva);
            $("#ivaMaterialLabel").text(iva.toFixed(2));

            var totalV1 = nvoSubTotal * 1.16;
            var total = totalV1 - anticipo;
            //Imprimimos el total  menos el anticipo
            $("#totalMaterial").val(total);
            $("#totalMaterialLabel").text(total.toFixed(2));

            //Imprimimos el total con iva (presupuesto original)
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));
        }

    //De lo contrario, se afecta el renglon por primera vez
    }else {
        $("#valoresCM_"+renglon).val(contenidoFila+"_"+"Aceptada");

        //Aumentamos la bandera para validar
        var filasAfectadas = $("#bandera").val();
        var limite = $("#limite").val();

        filasAfectadas++;
        $("#bandera").val(filasAfectadas);

        if (filasAfectadas==limite) {
            $("input").attr("disabled", false);
            $("#envioFormulario").attr("hidden",false);
        }
    }

    //Bloqueamos la opcion ya elegida
    $("#interruptorA_"+renglon).attr("hidden","hidden");
    $("#interruptorB_"+renglon).attr("hidden",false);
}

function rechazarItem(renglon){
    //Marcamos la fila para mostrar la decision tomada por el cliente
    $("#fila_indicador_"+renglon).css("background-color","#f5acaa");
    //Recuperamos el renglon e imprimimos la decision
    var contenidoFila = $("#valoresCM_"+renglon).val();
    //Separamos los valores para validar si existe modificacion previa
    var campos = contenidoFila.split("_");
    // console.log(campos);
    //Si son mas de 7 campos , ya se habia tomado una decision
    if (campos.length > 9) {
        $("#valoresCM_"+renglon).val(campos[0]+"_"+campos[1]+"_"+campos[2]+"_"+campos[3]+"_"+campos[4]+"_"+campos[5]+"_"+campos[6]+"_"+campos[7]+"_"+campos[8]+"_"+"Rechazada");
    //De lo contrario, se afecta el renglon por primera vez
    }else {
        $("#valoresCM_"+renglon).val(contenidoFila+"_"+"Rechazada");

        //Aumentamos la bandera para validar
        var filasAfectadas = $("#bandera").val();
        var limite = $("#limite").val();

        filasAfectadas++;
        $("#bandera").val(filasAfectadas);

        if (filasAfectadas==limite) {
            $("input").attr("disabled", false);
            $("#envioFormulario").attr("hidden",false);
        }
    }

    //Si es rechazada hacemos la resta del precio rechazado
    var montoCancelado = $("#canceladoMaterial").val();
    var anticipo = parseFloat($("#anticipoMaterial").val());
    var nvoMontoCancelar = parseFloat(montoCancelado) + parseFloat(campos[7]);
    //Imprimimos los nuevos valores
    $("#canceladoMaterialLabel").text(nvoMontoCancelar);
    $("#canceladoMaterial").val(nvoMontoCancelar);

    //Recalculamos los totales
    var subTotal = parseFloat($("#subTotalMaterial").val());

    var nvoSubTotal = subTotal - nvoMontoCancelar;
    var iva = nvoSubTotal * 0.16;
    //Imprimimos el iva
    $("#ivaMaterial").val(iva);
    $("#ivaMaterialLabel").text(iva.toFixed(2));

    var totalV1 = nvoSubTotal * 1.16;
    var total = totalV1 - anticipo;
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(total);
    $("#totalMaterialLabel").text(total.toFixed(2));

    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));
    
    //Bloqueamos la opcion ya elegida
    $("#interruptorA_"+renglon).attr("hidden",false);
    $("#interruptorB_"+renglon).attr("hidden","hidden");
}

// Buscador dinamico para la tabla
$('#busqueda_tabla').keyup(function () {
    toSearch();
});

//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}

//Envio de video en la orden de servicio
$("#enviarCotizacionApro").on('click',function(){
    var paqueteDeDatos = new FormData();
    paqueteDeDatos.append('eleccion', $('#desicionCoti').prop('value'));
    paqueteDeDatos.append('idOrden', $('#idOrden').prop('value'));
    enviarCofirmacion(paqueteDeDatos);
});

//Funcion envio formulario para aceptar cotizacion
function enviarCofirmacion(paqueteDeDatos){
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");
    $("#cancelarCoti").attr("disabled", true);
    $("#enviarCotizacionApro").attr("disabled", true);
    var url = $("#basePeticion").val();
    $.ajax({
        url: url+"multipunto/CotizacionesExternas/getAjaxForm",
        type: 'post',
        contentType: false,
        data: paqueteDeDatos,
        processData: false,
        cache: false,
        success:function(resp){
          $(".cargaIcono").css("display","none");
          $("#errorEnvioCotizacion").text("");
          console.log(resp);
          if (resp == "OK") {
              $("#tituloForm").text("Cotización actualizada correctamente");
              $("#aceptar").css("display","none");

              $("#enviarCotizacionApro").attr("data-dismiss", "modal");
              location.reload();
          } else {
              $("#tituloForm").text("Error al actualizar, por favor intenta de nuevo");
              $("#cancelarCoti").attr("disabled", false);
              $("#enviarCotizacionApro").attr("disabled", false);
          }
        //Cierre de success
        },
        error:function(error){
            $(".cargaIcono").css("display","none");
            $("#errorEnvioCotizacion").text("Error al enviar la cotización, intente nuevamente");
            $("#cancelarCoti").attr("disabled", false);
            $("#enviarCotizacionApro").attr("disabled", false);
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}
