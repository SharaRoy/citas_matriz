var tabla = $("#cuerpoMateriales");
var editorExt = $("#tipoRegistro").val();

//Validamos si es un asesor para que pueda firmar
if (editorExt == "Tecnico") {
    $("#AsesorCostos").attr("disabled", true);
    $("#AsesorCostos").attr("data-toggle", false);
}

//Tabla de control de materiales
$("#agregarCM").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Recuperamos los campos obligatorios
    var condi_1 = $("#cantidad_"+indice).val();
    var condi_2 = $("#descripcion_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ( (condi_1 != "") && (condi_2 != "") ){
        $(".aceptarCotizacion").attr("disabled",false);
        // console.log("Paso validacion");
        //Evitamos la edicion de los  elementos
        $("#cantidad_"+indice).attr('disabled','disabled');
        $("#descripcion_"+indice).attr('disabled','disabled');
        $("#costoCM_"+indice).attr('disabled','disabled');
        $("#horas_"+indice).attr('disabled','disabled');
        $("#totalReng_"+indice).attr('disabled','disabled');
        $("#pieza_"+indice).attr('disabled','disabled');
        $("input[name='existe_"+indice+"']").attr('disabled','disabled');

        //Bloqueamos el boton de buscar anterior
        $("#refaccionBusqueda_"+indice).attr("disabled", true);
        $("#refaccionBusqueda_"+indice).attr("data-toggle", false);

        //Mostramos el boton para eliminar fila
        $("#eliminarCM").attr('disabled','disabled');

        //Recuperamos los valores de toda la fila
        var campos_1 = $("#cantidad_"+indice).val();
        var campos_2 = $("#descripcion_"+indice).val();
        var campos_3 = $("#costoCM_"+indice).val();
        var campos_4 = $("#horas_"+indice).val();
        var campos_5 = $("#totalReng_"+indice).val();
        var campos_7 = $("#pieza_"+indice).val();
        var campos_8 = $("#totalOperacion_"+indice).val();

        if($("#autorizo_"+indice).is(':checked') ){
            var campos_6 = "SI";
        } else {
            var campos_6 = "NO";
        }

        if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
            var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
        }else {
            var campos_9 = "";
        }

        //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
        $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

        //Aumentamos el indice para los renglones
        indice++;
        $("#indiceTablaMateria").val(indice);

        //confirmamos el tipo de formulario
        if ($("#cargaFormulario").val() == "1") {
            var prp = "disabled";
        }else {
            var prp = "";
        }

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla.append("<tr id='fila_"+indice+"'>"+
            "<td align='center' style='border:1px solid #337ab7;'>"+
                "<input type='number' step='any' min='1' class='input_field' id='cantidad_"+indice+"' onblur='cantidadCollet("+indice+")' value='1' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:90%;color:black;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='3' class='input_field_lg' id='descripcion_"+indice+"' onblur='descripcionCollect("+indice+")' style='width:90%;text-align:left;color:black;'></textarea>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='1' class='input_field_lg' id='pieza_"+indice+"' onblur='piezaCollet("+indice+")' style='width:90%;text-align:left;color:black;'></textarea>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' id='apuntaSi_"+indice+"' class='input_field' onclick='existeCollet("+indice+")' name='existe_"+indice+"' value='SI' id='existe_"+indice+"' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' id='apuntaNo_"+indice+"' class='input_field' onclick='existeCollet("+indice+")' name='existe_"+indice+"' value='NO' id='existe_"+indice+"' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' id='apuntaPlanta_"+indice+"' class='input_field' onclick='existeCollet("+indice+")' name='existe_"+indice+"' value='PLANTA' id='existe_"+indice+"' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "$<input type='number' step='any' min='0' class='input_field' id='costoCM_"+indice+"' onblur='costoCollet("+indice+")' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='color:black;width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='number' step='any' min='0' class='input_field' id='horas_"+indice+"' onblur='horasCollet("+indice+")' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='color:black;width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "$<input type='number' step='any' min='0' id='totalReng_"+indice+"' class='input_field' onblur='autosuma("+indice+")' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='color:black;width:90%;text-align:left;' disabled>"+
                "<input type='hidden' id='totalOperacion_"+indice+"' value='0'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                //"<input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet("+indice+")' value='"+indice+"' id='autorizo_"+indice+"'>"+
                "<input type='checkbox' class='input_field' name='pzaGarantia[]' value='"+indice+"' style='transform: scale(1.5);'>"+
                "<input type='hidden' id='valoresCM_"+indice+"' name='registros[]'>"+
            "</td>"+
            /*"<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<a id='refaccionBusqueda_"+indice+"'class='refaccionesBox btn btn-warning' data-value='"+indice+"' data-target='#refacciones' data-toggle='modal' style='color:white;'>"+
                    "<i class='fa fa-search'></i>"+
                "</a>"+
            "</td>"+*/
        "</tr>");

        /*if (editorExt == "Tecnico") {
            $("#totalReng_"+indice).attr('disabled','disabled');
            $("#autorizo_"+indice).attr('disabled','disabled');
        }*/
    }else {
        //Regresamos el control al campo que falta rellenar
        if (condi_1 == "") {
            $("#cantidad_"+indice).focus();
        }else{
            $("#descripcion_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada (control de materiales)
$("#eliminarCM").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        //Recuperamos los valores finales (subtotal) y evitamos error de formato
        var limpieza = $("#subTotalMaterial").val().replace(",","");

        //Verificamos que no se haya hecho ningun calculo en ese renglon
        if ($("#totalOperacion_"+indice).val() != "0") {
            var renglon = parseFloat($("#totalOperacion_"+indice).val());
            var subGral = parseFloat(limpieza) - renglon;
        }else{

            var subGral = parseFloat(limpieza);
        }

        var fila = $("#fila_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceTablaMateria").val(indice);

        if (indice == 1) {
            $(".aceptarCotizacion").attr("disabled",true);
        }

        //Restamos el valor de esa fila antes de reiniciar los valores
        var cantiAnterior = parseFloat($("#totalOperacion_"+indice).val());

        //Restamos el valor anterior del renglon a sub-total actual
        var nvoSubTotal = subGral - cantiAnterior;
        // console.log("renglon - subtotal = NvoTotal");
        // console.log(subGral+" - "+cantiAnterior+" = "+nvoSubTotal);

        // Evitamos la basura en cotizaciones anteriores
        var conteoTotal = 0;
        var renglonesCuenta = $("#indiceTablaMateria").val();
        for (var i = 1; i <= renglonesCuenta; i++) {
            if (($("#totalOperacion_"+i).val() != "0")&&($("#horas_"+i).val() != "0")) {
                // conteoTotal += parseFloat($("#totalOperacion_"+i).val());
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var cantidadPza = parseFloat($("#cantidad_"+i).val());
                var costoPza = parseFloat($("#costoCM_"+i).val());
                var horasMo = parseFloat($("#horas_"+i).val());
                var costoMo = parseFloat($("#totalReng_"+i).val());
                var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                // console.log(totalRenglon);
                conteoTotal += totalRenglon;
            }
        }
        // console.log("conteo: "+conteoTotal);

        var anticipo = parseFloat($("#anticipoMaterial").val());
        var iva = conteoTotal * 0.16;
        var totalV1 = conteoTotal + iva;
        var total = totalV1 - anticipo;
        // var total = conteoTotal / 1.16;
        // var iva = conteoTotal - total;
        // console.log("iva: "+iva);
        // console.log("total: "+total);

        //Vaciamos los nuevos resultados
        $("#subTotalMaterial").val(parseFloat(conteoTotal));
        $("#ivaMaterial").val(parseFloat(iva));
        $("#totalMaterial").val(parseFloat(total));

        $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
        $("#ivaMaterialLabel").text(iva.toFixed(2));
        $("#totalMaterialLabel").text(total.toFixed(2));

        //Restablecemos los valores default del reglon anterior
        $("#cantidad_"+indice).val("1");
        $("#descripcion_"+indice).val("");
        $("#pieza_"+indice).val("");
        $("#costoCM_"+indice).val("0");
        $("#horas_"+indice).val("0");
        $("#totalOperacion_"+indice).val(0)
        $("#totalReng_"+indice).val("0");

        //Bloqueamos el boton de buscar anterior
        $("#refaccionBusqueda_"+indice).attr("disabled", false);
        $("#refaccionBusqueda_"+indice).attr("data-toggle", "modal");

        $("input[name='existe_"+indice+"']:radio").attr('disabled',false);

        //Desbloqueamos el renglon para la edición
        $("#cantidad_"+indice).attr('disabled',false);
        $("#descripcion_"+indice).attr('disabled',false);
        $("#costoCM_"+indice).attr('disabled',false);
        $("#horas_"+indice).attr('disabled',false);
        $("#pieza_"+indice).attr('disabled',false);
        $("input[name='existe_"+indice+"']").attr('disabled',false);
        $("#totalReng_"+indice).attr('disabled',false);
    }else {
        $(".aceptarCotizacion").attr("disabled",true);
    }
});
