$(document).ready(function() {
    $("#busquedaClaveBtn").attr("disabled",false);
    $("#claveBuscar").attr("disabled",false);

    $("#busquedaDescripBtn").attr("disabled",true);
    $("#descipBuscar").attr("disabled",true);

    $("#claveRefaccion").attr("disabled",true);
    $("#descripcionRefacciones").attr("disabled",true);
    $("#precioRefacciones").attr("disabled",true);
    $("#existenciaRefacciones").attr("disabled",true);
    $("#tipoBuscar").attr("disabled",true);
});

//Agregamos el indice del renglon a editar
$(".refaccionesBox").on('click',function(){
    var renglon = $(this).data("id");
        // console.log("renglon->"+renglon);
    $("#indiceOperacion").val(renglon);

    //Enviamos el valor de la descripcion al cuadro de busqueda
    var descripcion = $("#descripcion_"+renglon).val();
    $("#descipBuscar").val(descripcion);
});

//Verificamos el tipo de filtro que se utilizara para
$("input[name='filtros']").on('change',function(){
    var filtro = $("input[name='filtros']:checked").val();

    if (filtro == "descripcion") {
        $("#busquedaClaveBtn").attr("disabled",true);
        $("#claveBuscar").attr("disabled",true);

        $("#busquedaDescripBtn").attr("disabled",false);
        $("#descipBuscar").attr("disabled",false);
    }else {
        $("#busquedaClaveBtn").attr("disabled",false);
        $("#claveBuscar").attr("disabled",false);

        $("#busquedaDescripBtn").attr("disabled",true);
        $("#descipBuscar").attr("disabled",true);
    }
});

//Buscar por clave
$("#busquedaClaveBtn").on('click',function(){
    var valor = $("#claveBuscar").val();
    var base = $("#basePeticion").val();
    if (valor != "") {
        $(".cargaIcono").css("display","inline-block");
        $("#alertaConexion").text("Conectando con intelisi...");

        $.ajax({
                url: base+"formularios/otrosFormularios/Refacciones/filtrosDescripcion",
                method: 'post',
                data: {
                        valor_1: valor,
                        dato_1: "clave",
                },
                success:function(resp){
                    // console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        //Dividimos los registros
                        var filas = resp.split("|");
                        //Vaciamos el select donde se almacenan los resultados
                        var select = $("#resultadosVaciado");
                        select.empty();
                        // select.append("<option value=''>Selecciona...</option>");

                        //Verificamos que se hayan encontrado resultados
                        if (filas.length > 0) {
                            var regitro = ( filas.length > 1) ? filas.length -1 : filas.length;
                            for (var i = 0; i < regitro; i++) {
                                var campos = filas[i].split("_");
                                // console.log(campos);
                                if ((i == 0) && (campos.length < 2)) {
                                    select.append("<option value='-'>Sin Coincidencias...</option>");
                                    break;
                                }
                                select.append("<option value='"+campos[0]+"'>["+campos[0]+"]  "+campos[1]+"</option>");
                            }
                        }else {
                            select.append("<option value='-'>Sin Coincidencias...</option>");
                        }
                    }else {
                        console.log("Error");
                    }
                $(".cargaIcono").css("display","none");
                $("#alertaConexion").text("");
                //Cierre de success
                },
                error:function(error){
                        console.log(error);
                $(".cargaIcono").css("display","none");
                $("#alertaConexion").text("");
                //Cierre del error
                }
        //Cierre del ajax
        });
    }
});

//Buscar por descripción
$("#busquedaDescripBtn").on('click',function(){
    var valor = $("#descipBuscar").val();
    var base = $("#basePeticion").val();

    if (valor != "") {
        $(".cargaIcono").css("display","inline-block");
        $("#alertaConexion").text("Conectando con intelisi...");
        $.ajax({
                url: base+"formularios/otrosFormularios/Refacciones/filtrosDescripcion",
                method: 'post',
                data: {
                    valor_1: valor,
                    dato_1: "descripcion",
                },
                success:function(resp){
                    // console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        //Dividimos los registros
                        var filas = resp.split("|");
                        //Vaciamos el select donde se almacenan los resultados
                        var select = $("#resultadosVaciado");
                        select.empty();
                        // select.append("<option value=''>Selecciona...</option>");

                        //Verificamos que se hayan encontrado resultados
                        if (filas.length > 0) {
                            var regitro = ( filas.length > 1) ? filas.length -1 : filas.length;
                            for (var i = 0; i < regitro; i++) {
                                var campos = filas[i].split("_");
                                // console.log(campos);
                                if ((i == 0) && (campos.length < 2)) {
                                    select.append("<option value='-'>Sin Coincidencias...</option>");
                                    break;
                                }

                                select.append("<option value='"+campos[0]+"'>["+campos[0]+"]  "+campos[1]+"</option>");
                            }
                        }else {
                            select.append("<option value='-'>Sin Coincidencias...</option>");
                        }
                    }else {
                        console.log("Error");
                    }

                    $(".cargaIcono").css("display","none");
                    $("#alertaConexion").text("");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $(".cargaIcono").css("display","none");
                    $("#alertaConexion").text("");
                //Cierre del error
                }
        //Cierre del ajax
        });
    }
});

//Seleccionar un item para ver informacion
$("#resultadosVaciado").change(function(){
    var valor = $("#resultadosVaciado").val();
    var base = $("#basePeticion").val();

    if ((valor != "")&&(valor != "-")) {
        $(".cargaIcono").css("display","inline-block");
        $("#alertaConexion").text("Conectando con intelisi...");
        $.ajax({
            url: base+"formularios/otrosFormularios/Refacciones/filtrosDescripcion",
            method: 'post',
            data: {
                valor_1: valor,
                dato_1: "clave",
            },
            success:function(resp){
                // console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    //Dividimos los registros
                    var filas = resp.split("|");

                    //Verificamos que se hayan encontrado resultados
                    if (filas.length > 0) {
                        var regitro = ( filas.length > 1) ? filas.length -1 : filas.length;
                        for (var i = 0; i < regitro; i++) {
                            var campos = filas[i].split("_");
                            //Buscamos el que concuerde exactamente con la clave a buscar
                            if (campos[0] == valor) {
                                // console.log(campos);
                                $("#claveRefaccion").val(campos[0]);
                                $("#descripcionRefacciones").val(campos[1]);
                                $("#precioRefacciones").val(campos[3]);
                                $("#existenciaRefacciones").val(campos[4]);

                                // var tipo = (campos[5] == "MO") ? "Mano de Obra" : "Refacción";
                                if (campos[2] == "Servicio") {
                                    var tipo = "Mano de Obra";
                                }else if (campos[2] == "Normal") {
                                    var tipo = "Refacción";
                                }else {
                                    var tipo = campos[2];
                                }

                                $("#tipoBuscar").val(tipo);

                                //Verificamos que exista el articulo
                                if (campos[4] > "0") {
                                    //Comprobamos si la cantidad solicitada se cubre con la existencia real
                                    // var renglon = $("#indiceOperacion").val();
                                    // var cantidadSolicitada = $("#cantidad_"+renglon).val();
                                    // if (parseInt(cantidadSolicitada) >= parseInt(campos[4])) {
                                    //     $("#existeRefacciones").val("Si");
                                    // }else {
                                    //     $("#existeRefacciones").val("No");
                                    // }
                                    $("#existeRefacciones").val("Si");
                                }else if (campos[5] == "MO") {
                                    $("#existeRefacciones").val("Si");
                                }else {
                                    $("#existeRefacciones").val("No");
                                }
                            }
                        }
                    }else {
                        console.log("Error");
                        $("#claveRefaccion").val("");
                        $("#descripcionRefacciones").val("");
                        $("#precioRefacciones").val("");
                        $("#existenciaRefacciones").val("");
                        $("#tipoBuscar").val("");
                    }
                }else {
                    console.log("Error");
                    $("#claveRefaccion").val("");
                    $("#descripcionRefacciones").val("");
                    $("#precioRefacciones").val("");
                    $("#existenciaRefacciones").val("");
                    $("#tipoBuscar").val("");
                }
                $(".cargaIcono").css("display","none");
                $("#alertaConexion").text("");
            //Cierre de success
            },
            error:function(error){
                console.log(error);
                $(".cargaIcono").css("display","none");
                $("#alertaConexion").text("");
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

//Cuando precionamos el boton de aceptar
$("#bajaOperacion").on('click',function(){
    var renglon = $("#indiceOperacion").val();

    //Recuperamos los datos necesarios
    var precio = $("#precioRefacciones").val();
    var existe = $("#existeRefacciones").val();
    var clave = $("#claveRefaccion").val();
    var descripcion = $("#descripcionRefacciones").val();

    //Enviamos los datos a la tabla
    $("#costoCM_"+renglon).val(precio);
    $("#pieza_"+renglon).val(clave);
    $("#descripcion_"+renglon).val(descripcion);

    if (existe == "Si") {
        $("#apuntaSi_"+renglon).attr('checked','checked');
    }else {
        $("#apuntaNo_"+renglon).attr('checked','checked');
    }

    //Recuperamos los valores de toda la fila para actualizarla
    /*var campos_1 = $("#cantidad_"+renglon).val();
    var campos_2 = $("#descripcion_"+renglon).val();
    var campos_3 = $("#costoCM_"+renglon).val();
    var campos_4 = $("#horas_"+renglon).val();
    var campos_5 = $("#totalReng_"+renglon).val();
    var campos_7 = $("#pieza_"+renglon).val();
    var campos_8 = $("#totalOperacion_"+renglon).val();

    if($("#autorizo_"+renglon).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+renglon+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+renglon+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+renglon).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
    */
   
    //Pasamos el control al precio ingreado
    document.getElementById("costoCM_"+renglon).focus();

    //Si existe la tabla de garantias, bajamos al siguien renglon
    //Duplicamos los valores en la segunda tabla
    if ($("#renglonGarantia_"+renglon)) {
        var renglon_2 = $("#renglonGarantia_"+renglon).val();
        $("#tb2_costoCM_"+renglon_2).val(clave);
        $("#tb2_descripcion_"+renglon_2).val(descripcion);
    } 

    //Limpiamos los campos
    limpiarCampor();
});

//Boton cerrar
$("#cerrarOperacion").on('click',function(){
    //Limpiamos los campos
    limpiarCampor();
});

function limpiarCampor(){
    //Limpiamos los campos
    $("#claveBuscar").val("");
    $("#descipBuscar").val("");
    $("#claveRefaccion").val("");
    $("#tipoBuscar").val("");
    $("#descripcionRefacciones").val("");
    $("#precioRefacciones").val("");
    $("#existenciaRefacciones").val("");

    //Limpiamos el select de busqueda
    var select = $("#resultadosVaciado");
    select.empty();
    select.append("<option value=''>Selecciona...</option>");
}
