//Efecto de pasar sobre un input
$("input").mouseover(function() {
    this.focus();
});

$("textarea").mouseover(function() {
    this.focus();
});

// Funciones de autoguardado de renglon
function  descripcionCollect(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

}

function  piezaCollet(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

    //Duplicamos los valores en la segunda tabla
    var renglon_2 = $("#renglonGarantia_"+indice).val();
    $("#tb2_pieza_"+renglon_2).val(campos_7);

}

function autorizaColet(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

}

function existeCollet(indice){

    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

}

$('.autorizaCheck').on('click', function() {
    var indice = $(this).val();

    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
});


//Funciones con operaciones y autoguardado
function cantidadCollet(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

    //Verificamos que no se haya dejado el espacio vacio
    if ($("#cantidad_"+indice).val() == "") {
        $("#cantidad_"+indice).val("0");
    }else {
        //Recuperamos los valores del renglon
         var cantidad = parseFloat($("#cantidad_"+indice).val());
         var costoPieza = parseFloat($("#costoCM_"+indice).val());
         var costoHora = 860;//parseFloat($("#totalReng_"+indice).val());
         var horasTrabajo = parseFloat($("#horas_"+indice).val());

         //Recuperamos los valores finales (subtotal) y evitamos error de formato
         var limpieza = $("#subTotalMaterial").val().replace(",","");
         var subGral = parseFloat(limpieza);

         //Sacamos los nuevos valores
         var op_0 = cantidad * costoPieza;
         var op_1 = horasTrabajo * costoHora;
         var op_2 = op_1 + op_0;
         // console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
         //Actualizamos el valor del renglon
         $("#totalOperacion_"+indice).val(op_2);

        // Evitamos la basura en cotizaciones anteriores
        var conteoTotal = 0;
        var renglonesCuenta = $("#indiceTablaMateria").val();

        //Recuperamos los renglones que no son fueron aprovados por el jefe de taller
        var pza_previa = $("#refAutorizadas").val();
        var pzas_individuales = pza_previa.split("_");

        for (var i = 1; i <= renglonesCuenta; i++) {
            //Comprobamos que ya haya sido revisado por el jefe de taller
            if (pza_previa != "") {
                //Si ya fue revisado por el jefe de taller, validamos que el renglon deba contarse
                if (pzas_individuales.indexOf(""+i) > -1) {
                    //Como alternativa, se puede hacer la cuenta completa por renglon
                    var cantidadPza = parseFloat($("#cantidad_"+i).val());
                    var costoPza = parseFloat($("#costoCM_"+i).val());
                    var horasMo = parseFloat($("#horas_"+i).val());
                    var costoMo = 860 ; //parseFloat($("#totalReng_"+i).val());
                    var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                    //console.log(totalRenglon);
                    conteoTotal += totalRenglon;
                }
            }else{
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var cantidadPza = parseFloat($("#cantidad_"+i).val());
                var costoPza = parseFloat($("#costoCM_"+i).val());
                var horasMo = parseFloat($("#horas_"+i).val());
                var costoMo = 860 ; //parseFloat($("#totalReng_"+i).val());
                var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                //console.log(totalRenglon);
                conteoTotal += totalRenglon;
            }
        }

         var anticipo = parseFloat($("#anticipoMaterial").val());
         var iva = conteoTotal * 0.16;
         var totalV1 = conteoTotal + iva;
         var total = totalV1 - anticipo;
         // var total = conteoTotal / 1.16;
         // var iva = conteoTotal - total;
         // console.log("iva: "+iva);
         // console.log("total: "+total);

         //Vaciamos los nuevos resultados
         $("#subTotalMaterial").val(parseFloat(conteoTotal));
         $("#ivaMaterial").val(parseFloat(iva));
         $("#totalMaterial").val(parseFloat(total));

         $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
         $("#ivaMaterialLabel").text(iva.toFixed(2));
         $("#totalMaterialLabel").text(total.toFixed(2));
         $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

         //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
         $("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);
    }

}

function  costoCollet(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
    $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);

    //Verificamos que no se haya dejado el espacio vacio
    if ($("#costoCM_"+indice).val() == "") {
        $("#costoCM_"+indice).val("0");

        //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
        $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
    }else {
        //Recuperamos los valores del renglon
        var cantidad = parseFloat($("#cantidad_"+indice).val());
        var costoPieza = parseFloat($("#costoCM_"+indice).val());
        var costoHora = 860; //parseFloat($("#totalReng_"+indice).val());
        var horasTrabajo = parseFloat($("#horas_"+indice).val());

        //Recuperamos los valores finales (subtotal) y evitamos error de formato
        var limpieza = $("#subTotalMaterial").val().replace(",","");
        var subGral = parseFloat(limpieza);

        //Sacamos los nuevos valores
        var op_0 = cantidad * costoPieza;
        var op_1 = horasTrabajo * costoHora;
        var op_2 = op_1 + op_0;
        // console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
        //Actualizamos el valor del renglon
        $("#totalOperacion_"+indice).val(op_2);

        // Evitamos la basura en cotizaciones anteriores
        var conteoTotal = 0;
        var renglonesCuenta = $("#indiceTablaMateria").val();

        //Recuperamos los renglones que no son fueron aprovados por el jefe de taller
        var pza_previa = $("#refAutorizadas").val();
        var pzas_individuales = pza_previa.split("_");

        for (var i = 1; i <= renglonesCuenta; i++) {
            //Comprobamos que ya haya sido revisado por el jefe de taller
            if (pza_previa != "") {
                //Si ya fue revisado por el jefe de taller, validamos que el renglon deba contarse
                if (pzas_individuales.indexOf(""+i) > -1) {
                    //Como alternativa, se puede hacer la cuenta completa por renglon
                    var cantidadPza = parseFloat($("#cantidad_"+i).val());
                    var costoPza = parseFloat($("#costoCM_"+i).val());
                    var horasMo = parseFloat($("#horas_"+i).val());
                    var costoMo = 860; //parseFloat($("#totalReng_"+i).val());
                    var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                    //console.log(totalRenglon);
                    conteoTotal += totalRenglon;
                }
            }else{
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var cantidadPza = parseFloat($("#cantidad_"+i).val());
                var costoPza = parseFloat($("#costoCM_"+i).val());
                var horasMo = parseFloat($("#horas_"+i).val());
                var costoMo = 860; //parseFloat($("#totalReng_"+i).val());
                var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                //console.log(totalRenglon);
                conteoTotal += totalRenglon;
            }
        }
        // console.log("conteo: "+conteoTotal);

        var anticipo = parseFloat($("#anticipoMaterial").val());
        var iva = conteoTotal * 0.16;
        var totalV1 = conteoTotal + iva;
        var total = totalV1 - anticipo;
        // var total = conteoTotal / 1.16;
        // var iva = conteoTotal - total;
        // console.log("iva: "+iva);
        // console.log("total: "+total);

        //Vaciamos los nuevos resultados
        $("#subTotalMaterial").val(parseFloat(conteoTotal));
        $("#ivaMaterial").val(parseFloat(iva));
        $("#totalMaterial").val(parseFloat(total));

        $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
        $("#ivaMaterialLabel").text(iva.toFixed(2));
        $("#totalMaterialLabel").text(total.toFixed(2));
        $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

        //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
        $("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);
    }

    //Duplicamos los valores en la segunda tabla
    //var renglon_2 = $("#renglonGarantia_"+indice).val();
    //$("#tb2_costoCM_"+renglon_2).val(campos_7);
}

function  horasCollet(indice){
    //Recuperamos los valores de toda la fila y los guardamos provisionalmente
    var campos_1 = $("#cantidad_"+indice).val();
    var campos_2 = $("#descripcion_"+indice).val();
    var campos_3 = $("#costoCM_"+indice).val();
    var campos_4 = $("#horas_"+indice).val();
    var campos_5 = $("#totalReng_"+indice).val();
    var campos_7 = $("#pieza_"+indice).val();
    var campos_8 = $("#totalOperacion_"+indice).val();

    if($("#autorizo_"+indice).is(':checked') ){
        var campos_6 = "SI";
    } else {
        var campos_6 = "NO";
    }

    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
    }else {
        var campos_9 = "";
    }

    //Verificamos que no se haya dejado el espacio vacio
    if ($("#horas_"+indice).val() == "") {
        $("#horas_"+indice).val("0");

        //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
        $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
    }else {
        //Verificamos si se cambio el valor del campo
        var valores = $("#valoresCM_"+indice).val();
        var campos = valores.split("_");
        var horasTrabajo = parseFloat($("#horas_"+indice).val());
        //console.log(campos);
        if (horasTrabajo != campos[3]) {
            //Recuperamos los valores del renglon
            var cantidad = parseFloat($("#cantidad_"+indice).val());
            var costoPieza = parseFloat($("#costoCM_"+indice).val());
            var costoHora = 860; //parseFloat($("#totalReng_"+indice).val());

            var cantiAnterior = parseFloat($("#totalOperacion_"+indice).val());

            //Recuperamos los valores finales (subtotal) y evitamos error de formato
            var limpieza = $("#subTotalMaterial").val().replace(",","");
            var subGral = parseFloat(limpieza);

            //Sacamos los nuevos valores
            var op_0 = cantidad * costoPieza;
            var op_1 = horasTrabajo * costoHora;
            var op_2 = op_1 + op_0;
            // console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
            //Actualizamos el valor del renglon
            $("#totalOperacion_"+indice).val(op_2);

            // Evitamos la basura en cotizaciones anteriores
            var conteoTotal = 0;
            var renglonesCuenta = $("#indiceTablaMateria").val();
            
            //Recuperamos los renglones que no son fueron aprovados por el jefe de taller
            var pza_previa = $("#refAutorizadas").val();
            var pzas_individuales = pza_previa.split("_");

            for (var i = 1; i <= renglonesCuenta; i++) {
                //Comprobamos que ya haya sido revisado por el jefe de taller
                if (pza_previa != "") {
                    //Si ya fue revisado por el jefe de taller, validamos que el renglon deba contarse
                    if (pzas_individuales.indexOf(""+i) > -1) {
                        //Como alternativa, se puede hacer la cuenta completa por renglon
                        var cantidadPza = parseFloat($("#cantidad_"+i).val());
                        var costoPza = parseFloat($("#costoCM_"+i).val());
                        var horasMo = parseFloat($("#horas_"+i).val());
                        var costoMo = 860; //parseFloat($("#totalReng_"+i).val());
                        var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                        //console.log(totalRenglon);
                        conteoTotal += totalRenglon;
                    }
                }else{
                    //Como alternativa, se puede hacer la cuenta completa por renglon
                    var cantidadPza = parseFloat($("#cantidad_"+i).val());
                    var costoPza = parseFloat($("#costoCM_"+i).val());
                    var horasMo = parseFloat($("#horas_"+i).val());
                    var costoMo = 860; //parseFloat($("#totalReng_"+i).val());
                    var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                    //console.log(totalRenglon);
                    conteoTotal += totalRenglon;
                }
            }
            // console.log("conteo: "+conteoTotal);

            var anticipo = parseFloat($("#anticipoMaterial").val());
            var iva = conteoTotal * 0.16;
            var totalV1 = conteoTotal + iva;
            var total = totalV1 - anticipo;
            // var total = conteoTotal / 1.16;
            // var iva = conteoTotal - total;
            // console.log("iva: "+iva);
            // console.log("total: "+total);

            //Vaciamos los nuevos resultados
            $("#subTotalMaterial").val(parseFloat(conteoTotal));
            $("#ivaMaterial").val(parseFloat(iva));
            $("#totalMaterial").val(parseFloat(total));

            $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#totalMaterialLabel").text(total.toFixed(2));
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

            //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
            $("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);
        }else{
            //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
            $("#valoresCM_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+campos_8+"_"+campos_9);
            console.log("No se modifico el campo");
        }
    }
}

//Nvo procedimiento de la funcion autosuma
function autosuma(indice){

    //Verificamos que no se haya dejado el espacio vacio
    if ($("#totalReng_"+indice).val() == "") {
        $("#totalReng_"+indice).val("0");
    }else {
        //Verificamos si se cambio el valor del campo
        var valores = $("#valoresCM_"+indice).val();
        var campos = valores.split("_");
        var costoHora = 860; //parseFloat($("#totalReng_"+indice).val());
        //console.log(campos);
        if (costoHora != campos[4]) {
            //Recuperamos los valores del renglon
            var cantidad = parseFloat($("#cantidad_"+indice).val());
            var costoPieza = parseFloat($("#costoCM_"+indice).val());
            var horasTrabajo = parseFloat($("#horas_"+indice).val());

            var cantiAnterior = parseFloat($("#totalOperacion_"+indice).val());

            //Recuperamos los valores finales (subtotal) y evitamos error de formato
            var limpieza = $("#subTotalMaterial").val().replace(",","");
            var subGral = parseFloat(limpieza);

            //Sacamos los nuevos valores
            var op_0 = cantidad * costoPieza;
            var op_1 = horasTrabajo * costoHora;
            var op_2 = op_1 + op_0;
            //console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);
            //Actualizamos el valor del renglon
            $("#totalOperacion_"+indice).val(op_2);

            // Evitamos la basura en cotizaciones anteriores
            var conteoTotal = 0;
            var renglonesCuenta = $("#indiceTablaMateria").val();

            //Recuperamos los renglones que no son fueron aprovados por el jefe de taller
            var pza_previa = $("#refAutorizadas").val();
            var pzas_individuales = pza_previa.split("_");

            for (var i = 1; i <= renglonesCuenta; i++) {
                //Comprobamos que ya haya sido revisado por el jefe de taller
                if (pza_previa != "") {
                    //Si ya fue revisado por el jefe de taller, validamos que el renglon deba contarse
                    if (pzas_individuales.indexOf(""+i) > -1) {
                        //Como alternativa, se puede hacer la cuenta completa por renglon
                        var cantidadPza = parseFloat($("#cantidad_"+i).val());
                        var costoPza = parseFloat($("#costoCM_"+i).val());
                        var horasMo = parseFloat($("#horas_"+i).val());
                        var costoMo = 860 ; //parseFloat($("#totalReng_"+i).val());
                        var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                        //console.log(totalRenglon);
                        conteoTotal += totalRenglon;
                    }
                }else{
                    //Como alternativa, se puede hacer la cuenta completa por renglon
                    var cantidadPza = parseFloat($("#cantidad_"+i).val());
                    var costoPza = parseFloat($("#costoCM_"+i).val());
                    var horasMo = parseFloat($("#horas_"+i).val());
                    var costoMo = 860 ; //parseFloat($("#totalReng_"+i).val());
                    var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                    //console.log(totalRenglon);
                    conteoTotal += totalRenglon;
                }
            }
            console.log("conteo: "+conteoTotal);
            var anticipo = parseFloat($("#anticipoMaterial").val());
            var iva = conteoTotal * 0.16;
            var totalV1 = conteoTotal + iva;
            var total = totalV1 - anticipo;
            // var total = conteoTotal / 1.16;
            // var iva = conteoTotal - total;
            // console.log("iva: "+iva);
             console.log("total: "+total);

            //Vaciamos los nuevos resultados
            $("#subTotalMaterial").val(parseFloat(conteoTotal));
            $("#ivaMaterial").val(parseFloat(iva));
            $("#totalMaterial").val(parseFloat(total));

            $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#totalMaterialLabel").text(total.toFixed(2));
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

            //Actualizamos los valores del renglon
            var campos_2 = $("#descripcion_"+indice).val();
            var campos_7 = $("#pieza_"+indice).val();

            if($("#autorizo_"+indice).is(':checked') ){
                var campos_6 = "SI";
            } else {
                var campos_6 = "NO";
            }

            if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
                var campos_9 = $("input[name='existe_"+indice+"']:checked").val();
            }else {
                var campos_9 = "";
            }

            //Armamos el contenedor // Cantidad / Descripcion / Costo / Horas / Total / Autoriza / numero de pieza / existe pieza en inventario
            $("#valoresCM_"+indice).val(cantidad+"_"+campos_2+"_"+costoPieza+"_"+horasTrabajo+"_"+'860'+"_"+campos_6+"_"+campos_7+"_"+op_2+"_"+campos_9);
        }else{
            console.log("No se modifico el campo");
        }
    }
}


//Procedimiento para las refacciones que no seran aprovadas por el jefe de refacciones
function autorizaCollet(indice){
    //Recuperamos la opcion marcada
    if ($("input[name='autorizaJDT_"+indice+"']:radio").is(':checked')) {
        var decision = $("input[name='autorizaJDT_"+indice+"']:checked").val();
    }else {
        var decision = "";
    }

    //Verificamos si ya existe esa pieza en el almacen
    var pza_previa = $("#refAutorizadas").val();
    var pzas_individuales = pza_previa.split("_");

    var pza_previa_rez = $("#refRechazadas").val();
    var pzas_individuales_rez = pza_previa_rez.split("_");

    //Comprobamos la decision tomada
    if (decision == "SI") {
        //Si ya se habia marcado con "SI"
        if (pzas_individuales.indexOf(""+indice) >= 0) {
            console.log("Click");
        //Si ya se habia marcado con "NO"
        }else if(pzas_individuales_rez.indexOf(""+indice) >= 0){
            //Restauramos los valores del renglon en la cotización
            var cantidad = parseFloat($("#cantidad_"+indice).val());
            var costoPieza = parseFloat($("#costoCM_"+indice).val());
            var costoHora = 860; //parseFloat($("#totalReng_"+indice).val());
            var horasTrabajo = parseFloat($("#horas_"+indice).val());

            //Recuperamos los valores finales (subtotal) y evitamos error de formato
            var limpieza = $("#subTotalMaterial").val().replace(",","");
            var subGral = parseFloat(limpieza);

            //Sacamos los nuevos valores
            var op_0 = cantidad * costoPieza;
            var op_1 = horasTrabajo * costoHora;
            var op_2 = op_1 + op_0;
            //console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);

            var conteoTotal = subGral + op_2;
            //console.log("Nuevo subtotal: "+conteoTotal);

            var anticipo = parseFloat($("#anticipoMaterial").val());
            var iva = conteoTotal * 0.16;
            //console.log("Nuevo iva: "+iva);

            var totalV1 = conteoTotal + iva;
            var total = totalV1 - anticipo;
            //console.log("Nuevo total: "+total);  

            //Vaciamos los nuevos resultados
            $("#subTotalMaterial").val(parseFloat(conteoTotal));
            $("#ivaMaterial").val(parseFloat(iva));
            $("#totalMaterial").val(parseFloat(total));

            $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#totalMaterialLabel").text(total.toFixed(2));
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2)); 

            //Sacamos el indice del array
            var cadena = "";
            for (var i = pzas_individuales_rez.length - 1; i >= 0; i--) {
                var temp = pzas_individuales_rez[i];
                if ((temp != "")&&(temp != indice)) {
                    cadena = cadena+""+temp+"_";
                }
            }
            $("#refRechazadas").val(cadena);

            //  Introducimos el valor a la cadena correspondinte
            $("#refAutorizadas").val(pza_previa+"_"+indice);

        //Guardamos el renglon por primera vez
        }else{
            $("#refAutorizadas").val(pza_previa+"_"+indice);
        }
    //Si la respuesta fue  "NO", verificamos el proceso
    }else{
        //Si ya se habia marcado con "NO"
        if (pzas_individuales_rez.indexOf(""+indice) >= 0) {
            console.log("Click");
        //Si ya se habia marcado con "SI"
        }else if(pzas_individuales.indexOf(""+indice) >= 0){
            //Restamos los valores del renglon en la cotización
            var cantidad = parseFloat($("#cantidad_"+indice).val());
            var costoPieza = parseFloat($("#costoCM_"+indice).val());
            var costoHora = 860;//parseFloat($("#totalReng_"+indice).val());
            var horasTrabajo = parseFloat($("#horas_"+indice).val());

            //Recuperamos los valores finales (subtotal) y evitamos error de formato
            var limpieza = $("#subTotalMaterial").val().replace(",","");
            var subGral = parseFloat(limpieza);

            //Sacamos los nuevos valores
            var op_0 = cantidad * costoPieza;
            var op_1 = horasTrabajo * costoHora;
            var op_2 = op_1 + op_0;
            //console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);

            var conteoTotal = subGral - op_2;
            //console.log("Nuevo subtotal: "+conteoTotal);

            var anticipo = parseFloat($("#anticipoMaterial").val());
            var iva = conteoTotal * 0.16;
            //console.log("Nuevo iva: "+iva);

            var totalV1 = conteoTotal + iva;
            var total = totalV1 - anticipo;
            //console.log("Nuevo total: "+total);

            //Vaciamos los nuevos resultados
            $("#subTotalMaterial").val(parseFloat(conteoTotal));
            $("#ivaMaterial").val(parseFloat(iva));
            $("#totalMaterial").val(parseFloat(total));

            $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#totalMaterialLabel").text(total.toFixed(2));
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

            //Sacamos el indice del array
            var cadena = "";
            for (var i = pzas_individuales.length - 1; i >= 0; i--) {
                var temp = pzas_individuales[i];
                if ((temp != "")&&(temp != indice)) {
                    cadena = cadena+""+temp+"_";
                }
            }
            $("#refAutorizadas").val(cadena);

            //  Introducimos el valor a la cadena correspondinte
            $("#refRechazadas").val(pza_previa_rez+"_"+indice);

        //Guardamos el renglon por primera vez
        }else{
            //Restamos los valores del renglon en la cotización
            var cantidad = parseFloat($("#cantidad_"+indice).val());
            var costoPieza = parseFloat($("#costoCM_"+indice).val());
            var costoHora = 860; //parseFloat($("#totalReng_"+indice).val());
            var horasTrabajo = parseFloat($("#horas_"+indice).val());

            //Recuperamos los valores finales (subtotal) y evitamos error de formato
            var limpieza = $("#subTotalMaterial").val().replace(",","");
            var subGral = parseFloat(limpieza);

            //Sacamos los nuevos valores
            var op_0 = cantidad * costoPieza;
            var op_1 = horasTrabajo * costoHora;
            var op_2 = op_1 + op_0;
            //console.log("Renglon-> "+ op_1 + " + "+ op_0 +" = "+op_2);

            var conteoTotal = subGral - op_2;
            console.log("Nuevo subtotal: "+conteoTotal);

            var anticipo = parseFloat($("#anticipoMaterial").val());
            var iva = conteoTotal * 0.16;
            //console.log("Nuevo iva: "+iva);

            var totalV1 = conteoTotal + iva;
            var total = totalV1 - anticipo;
            //console.log("Nuevo total: "+total);

            //Vaciamos los nuevos resultados
            $("#subTotalMaterial").val(parseFloat(conteoTotal));
            $("#ivaMaterial").val(parseFloat(iva));
            $("#totalMaterial").val(parseFloat(total));

            $("#subTotalMaterialLabel").text(conteoTotal.toFixed(2));
            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#totalMaterialLabel").text(total.toFixed(2));
            $("#presupuestoMaterialLabel").text(totalV1.toFixed(2));

            $("#refRechazadas").val(pza_previa_rez+"_"+indice);
        }
    }

    //Desbloqueamos el boton de la firma, cuando ya se hayan marcado todos los campos
    var decisiones = pzas_individuales_rez.length + pzas_individuales.length;
    
    var renglonesCuenta = $("#indiceTablaMateria").val(); 
    
    if ((decisiones-1) == (renglonesCuenta)) {
        //Activamos la firma para terminar revision de jefe de taller
        $("#Coti_JDT_firma").attr("disabled", false);
        $("#Coti_JDT_firma").attr("data-toggle", 'modal');
    }
}


//Autoguardado de la tabla adicional de garantias ( en la vista de refacciones)
// Funciones de autoguardado de renglon
function  descripcionCollect_tabla2(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#tb2_cantidad_"+indice).val();
    var campos_2 = $("#tb2_descripcion_"+indice).val();
    var campos_3 = $("#tb2_pieza_"+indice).val();
    var campos_4 = $("#tb2_costoCM_"+indice).val();
    var campos_5 = $("#tb2_horas_"+indice).val();
    var campos_6 = $("#tb2_totalReng_"+indice).val();

    //Armamos el contenedor // Cantidad / Descripcion / Num. pieza / Costo Pza/ Horas MO / Total 
    $("#tablaPzsGarantias_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6);
}

function  piezaCollet_tabla2(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#tb2_cantidad_"+indice).val();
    var campos_2 = $("#tb2_descripcion_"+indice).val();
    var campos_3 = $("#tb2_pieza_"+indice).val();
    var campos_4 = $("#tb2_costoCM_"+indice).val();
    var campos_5 = $("#tb2_horas_"+indice).val();
    var campos_6 = $("#tb2_totalReng_"+indice).val();

    //Armamos el contenedor // Cantidad / Descripcion / Num. pieza / Costo Pza/ Horas MO / Total 
    $("#tablaPzsGarantias_"+indice).val(campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5+"_"+campos_6);
}

function  cantidadCollet_tabla2(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#tb2_cantidad_"+indice).val();
    var campos_2 = $("#tb2_descripcion_"+indice).val();
    var campos_3 = $("#tb2_pieza_"+indice).val();
    var campos_4 = $("#tb2_costoCM_"+indice).val();
    var campos_5 = $("#tb2_horas_"+indice).val();
    var campos_6 = $("#tb2_totalReng_"+indice).val();

    //Hacemos la operacion de los totales
    var pieza = parseFloat(campos_1) * parseFloat(campos_4);
    var mo = parseFloat(campos_5) * parseFloat(campos_6);

    var total = pieza + mo ;

    var totalGarantias = 0;
    for (var i = 1 ; i <= $("#renglonesGarantias").val() ; i++) {
        //Hacemos la operacion de los totales de los renglones
        var pieza_2 = parseFloat($("#tb2_cantidad_"+i).val()) * parseFloat($("#tb2_costoCM_"+i).val());
        var mo_2 = parseFloat($("#tb2_horas_"+i).val()) * parseFloat($("#tb2_totalReng_"+i).val());

        totalGarantias += pieza_2 + mo_2 ;
        //console.log(totalGarantias);
    }

    $("#totalRenglonGarantia_"+indice).text(total.toFixed(2));

    $("#subTotalMaterialLabel_tabla2").text(totalGarantias.toFixed(2));
    $("#ivaMaterialLabel_tabla2").text((totalGarantias*0.16).toFixed(2));
    $("#totalMaterialLabel_tabla2").text((totalGarantias*1.16).toFixed(2));

    //Armamos el contenedor // Cantidad / Descripcion / Num. pieza / Costo Pza/ Horas MO / Total 
    $("#tablaPzsGarantias_"+indice).val((indice+1)+"_"+campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5);
}

function  costoCollet_tabla2(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#tb2_cantidad_"+indice).val();
    var campos_2 = $("#tb2_descripcion_"+indice).val();
    var campos_3 = $("#tb2_pieza_"+indice).val();
    var campos_4 = $("#tb2_costoCM_"+indice).val();
    var campos_5 = $("#tb2_horas_"+indice).val();
    var campos_6 = $("#tb2_totalReng_"+indice).val();

    //Hacemos la operacion de los totales
    var pieza = parseFloat(campos_1) * parseFloat(campos_4);
    var mo = parseFloat(campos_5) * parseFloat(campos_6);

    var total = pieza + mo ;

    var totalGarantias = 0;
    for (var i = 1 ; i <= $("#renglonesGarantias").val() ; i++) {
        //Hacemos la operacion de los totales de los renglones
        var pieza_2 = parseFloat($("#tb2_cantidad_"+i).val()) * parseFloat($("#tb2_costoCM_"+i).val());
        var mo_2 = parseFloat($("#tb2_horas_"+i).val()) * parseFloat($("#tb2_totalReng_"+i).val());

        totalGarantias += pieza_2 + mo_2 ;
        //console.log(totalGarantias);
    }

    $("#totalRenglonGarantia_"+indice).text(total.toFixed(2));

    $("#subTotalMaterialLabel_tabla2").text(totalGarantias.toFixed(2));
    $("#ivaMaterialLabel_tabla2").text((totalGarantias*0.16).toFixed(2));
    $("#totalMaterialLabel_tabla2").text((totalGarantias*1.16).toFixed(2));

    //Armamos el contenedor // Cantidad / Descripcion / Num. pieza / Costo Pza/ Horas MO / Total 
    $("#tablaPzsGarantias_"+indice).val((indice+1)+"_"+campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5);
}

function  horasCollet_tabla2(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#tb2_cantidad_"+indice).val();
    var campos_2 = $("#tb2_descripcion_"+indice).val();
    var campos_3 = $("#tb2_pieza_"+indice).val();
    var campos_4 = $("#tb2_costoCM_"+indice).val();
    var campos_5 = $("#tb2_horas_"+indice).val();
    var campos_6 = $("#tb2_totalReng_"+indice).val();

    //Hacemos la operacion de los totales
    var pieza = parseFloat(campos_1) * parseFloat(campos_4);
    var mo = parseFloat(campos_5) * parseFloat(campos_6);

    var total = pieza + mo ;

    var totalGarantias = 0;
    for (var i = 1 ; i <= $("#renglonesGarantias").val() ; i++) {
        //Hacemos la operacion de los totales de los renglones
        var pieza_2 = parseFloat($("#tb2_cantidad_"+i).val()) * parseFloat($("#tb2_costoCM_"+i).val());
        var mo_2 = parseFloat($("#tb2_horas_"+i).val()) * parseFloat($("#tb2_totalReng_"+i).val());

        totalGarantias += pieza_2 + mo_2 ;
        //console.log(totalGarantias);
    }

    $("#totalRenglonGarantia_"+indice).text(total.toFixed(2));

    $("#subTotalMaterialLabel_tabla2").text(totalGarantias.toFixed(2));
    $("#ivaMaterialLabel_tabla2").text((totalGarantias*0.16).toFixed(2));
    $("#totalMaterialLabel_tabla2").text((totalGarantias*1.16).toFixed(2));

    //Armamos el contenedor // Cantidad / Descripcion / Num. pieza / Costo Pza/ Horas MO / Total 
    $("#tablaPzsGarantias_"+indice).val((indice+1)+"_"+campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5);
}

function  autosuma_tabla2(indice){
    //Recuperamos los valores de toda la fila
    var campos_1 = $("#tb2_cantidad_"+indice).val();
    var campos_2 = $("#tb2_descripcion_"+indice).val();
    var campos_3 = $("#tb2_pieza_"+indice).val();
    var campos_4 = $("#tb2_costoCM_"+indice).val();
    var campos_5 = $("#tb2_horas_"+indice).val();
    var campos_6 = $("#tb2_totalReng_"+indice).val();

    //Hacemos la operacion de los totales
    var pieza = parseFloat(campos_1) * parseFloat(campos_4);
    var mo = parseFloat(campos_5) * parseFloat(campos_6);

    var total = pieza + mo ;

    var totalGarantias = 0;
    for (var i = 1 ; i <= $("#renglonesGarantias").val() ; i++) {
        //Hacemos la operacion de los totales de los renglones
        var pieza_2 = parseFloat($("#tb2_cantidad_"+i).val()) * parseFloat($("#tb2_costoCM_"+i).val());
        var mo_2 = parseFloat($("#tb2_horas_"+i).val()) * parseFloat($("#tb2_totalReng_"+i).val());

        totalGarantias += pieza_2 + mo_2 ;
        console.log(totalGarantias);
    }

    $("#totalRenglonGarantia_"+indice).text(total.toFixed(2));

    $("#subTotalMaterialLabel_tabla2").text(totalGarantias.toFixed(2));
    $("#ivaMaterialLabel_tabla2").text((totalGarantias*0.16).toFixed(2));
    $("#totalMaterialLabel_tabla2").text((totalGarantias*1.16).toFixed(2));

    //Armamos el contenedor // Cantidad / Descripcion / Num. pieza / Costo Pza/ Horas MO / Total 
    $("#tablaPzsGarantias_"+indice).val((indice+1)+"_"+campos_1+"_"+campos_2+"_"+campos_3+"_"+campos_4+"_"+campos_5);
}