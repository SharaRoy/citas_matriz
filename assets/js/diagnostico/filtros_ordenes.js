//Condicionamos las busquedas por fecha 
$('#fecha_inicio').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_fin").attr('min',fecha_inicio);
    //console.log(fecha_inicio)
});

$('#fecha_fin').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_inicio").attr('max',fecha_fin);
    //console.log(fecha_fin)
});


/*$('#btnBusqueda_fechas').on('click',function(){
    console.log("Buscando por fechas");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_campo").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val();

    $.ajax({
        url: base+"ordenServicio/Documentacion_Ordenes/filtroPorFechas", 
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        //console.log(celdas);

                        var base_2 = $("#basePeticion").val().trim();

                        var orden = "<a href='"+base_2+"OrdenServicio_Revision/"+celdas[18]+"' class='btn btn-info' target='_blank' style='font-size: 9.5px;'>"+
                                        "ORDEN"+
                                    "</a>";

                        //Recuperamos los archivos oasis
                        var btnArchivos = '';
                        //console.log(celdas[15]);
                        if (celdas[15].length > 8) {
                            var archivos = celdas[15].split("~");
                            var conteo = 1;
                            for (var j = 0; j < archivos.length; j++) {
                                if (archivos[j] != "") {
                                    btnArchivos += "<a href='"+base_2+""+archivos[j]+"' class='btn btn-primary' target='_blank' style='font-size: 9px;margin-top:5px;'>"+
                                        "ARC. OASIS ("+conteo+")"+
                                    "</a>";
                                    conteo++;
                                }
                            }
                        }

                        //Validamos si hay multipunto
                        var multipunto = '';
                        var colorBtnMultipunto = (celdas[14] == '1') ? 'btn-success': 'btn-danger';
                        if (celdas[13] == "1") {
                            multipunto = "<a href='"+base_2+"Multipunto_Revision/"+celdas[18]+"' class='btn "+colorBtnMultipunto+"' target='_blank' style='font-size: 9.5px;'>"+
                                "MULTIPUNTO"+
                            "</a>";
                        }

                        //Validamos la cotizacion
                        var cotizacion = '';
                        if (celdas[8] == "1") {
                            if (celdas[9] != '') {
                                cotizacion = "<a href='"+base_2+"Presupuesto_Cliente/"+celdas[18]+"' class='btn btn-warning' target='_blank' style='font-size: 9.5px;'>"+
                                    "PRESUPUESTO"+
                                "</a>";
                            }else{
                                cotizacion = "<a href='"+base_2+"Alta_Presupuesto/"+celdas[18]+"' class='btn btn-warning' target='_blank' style='font-size: 9.5px;'>"+
                                    "PRESUPUESTO"+
                                "</a>";
                            }
                        }

                        //Validamos la voz cliente
                        var vc = '';
                        var audio = '';
                        if (celdas[12] == "1") {
                            vc = "<a href='"+base_2+"VCliente_Revision/"+celdas[18]+"' class='btn btn-secondary' target='_blank' style='font-size: 9.5px;'>"+
                                    "VOZ CLIENTE"+
                                "</a>";
                            //Validamos si hay audio disponible
                            if (celdas[11] != "") {
                                audio = "<a class='AudioModal' id='audio_"+(i+1)+"' onclick='addAudio("+(i+1)+");' data-value='"+celdas[11]+"' data-orden='"+celdas[1]+"' data-target='#muestraAudio' data-toggle='modal' style='color:blue;font-size:24px;'>"+
                                    "<i class='far fa-play-circle' style='font-size:24px;'></i>"+
                                "</a>";
                            } else {
                                audio = "<i class='fas fa-volume-mute' style='font-size:24px;color:red;'></i>";
                            }
                        } 

                        //Si es el jefe de taller
                        var comentario = "";
                        var dirige = $("#pOrigen").val();
                        if ((dirige == "JDT")||(dirige == "PCO")) {
                            comentario = "<td align='center' style='vertical-align: middle;background-color: #5bc0de;'>"+
                                    "<a class='addComentarioBtn' onclick='btnAddClick("+celdas[1]+")' title='Agregar Comentario' data-id='"+celdas[1]+"' data-target='#addComentario' data-toggle='modal'>"+
                                        "<i class='fas fa-comments' style='font-size: 18px;color: black;'></i>"+
                                    "</a>"+

                                    "<a class='historialCometarioBtn' onclick='btnHistorialClick("+celdas[1]+")' title='Historial comentarios' data-id='"+celdas[1]+"' data-target='#historialCometario' data-toggle='modal' style='margin-left: 20px;'>"+
                                        "<i class='fas fa-info' style='font-size: 18px;color: black;'></i>"+
                                    "</a>"+
                                "</td>";
                        }

                        //Validamos si hay servicio valet asignado
                        var valet = '';
                        if (celdas[17] == "1") {
                            valet = "<a href='"+base_2+"Servicio_valet_PDF/"+celdas[18]+"' class='btn btn-defalul' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
                                "VALET"+
                            "</a>";
                        }

                        var garantia_ext = "";
                        if ((dirige == "TEC")||(dirige == "JDT")) {
                            var base_garantias = $("#basGarantias").val();
                            if (celdas.length > 20) {
                                //if (celdas[21] != "NA") {
                                    var formatos = "";
                                    if (celdas[21] != "0") {
                                        formatos = "<a href='"+base_2+"Formatos_TecGarantia/"+celdas[18]+"' class='btn' target='_blank' style='font-size: 10px; background-color:  #1abc9c ;color:white;'>"+
                                                        "Ver Formato(s) "+
                                                    "</a>";
                                    } else {
                                        formatos = "<a href='#' class='btn btn-default' target='_blank' style='font-size: 10px; color:black;'>"+
                                                        "Sin Formato(s)"+
                                                    "</a>";
                                    }

                                    garantia_ext = "<td align='center' style='vertical-align: middle;'>"+
                                                        "<a href='"+base_garantias+"garantias/F1863/form_registro_labor?no_orden="+celdas[1]+"' class='btn' target='_blank' style='font-size: 11px; background-color: #2b3188;color:white;'>F. 1863</a>"+
                                                        "<br>"+
                                                        formatos+""+
                                                    "</td>";
                                //} else{
                                    //garantia_ext = "<td align='center' style='vertical-align: middle;'></td>"
                                //}
                            }
                        }
                        
                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:11px;background-color: "+celdas[19]+"; '>"+
                            //Indice
                            //"<td align='center' style='vertical-align: middle;'>"+(i+1)+"</td>"+
                            //NO. ORDEN
                            "<td align='center' style='vertical-align: middle;'>"+celdas[1]+"</td>"+
                            //NO. INTELISIS
                            "<td align='center' style='vertical-align: middle;'>"+celdas[2]+"</td>"+
                            //FECHA DIAGNÓSTICO
                            "<td align='center' style='vertical-align: middle;'>"+celdas[0]+"</td>"+
                            //VEHÍCULO
                            "<td align='center' style='vertical-align: middle;'>"+celdas[4]+"</td>"+
                            //PLACAS
                            "<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;'>"+celdas[5]+"</td>"+
                            //TÉCNICO
                            "<td align='center' style='vertical-align: middle;'>"+celdas[6]+"</td>"+
                            //CLIENTE
                            "<td align='center' style='vertical-align: middle;'>"+celdas[7]+"</td>"+
                            //ACCIONES
                            //ORDEN SERVICIO
                            "<td align='center' style='vertical-align: middle;'>"+
                                orden+""+
                            "</td>"+
                            //Archivo Oasis
                            "<td align='center' style='vertical-align: middle;'>"+
                                btnArchivos+""+
                            "</td>"+
                            //Multipunto
                            "<td align='center' style='width:10%;'>"+
                                multipunto+""+
                                "<label style='color:blue;font-weight: bold;'>"+((celdas[16] != '') ? celdas[16] : '')+"</label>"+
                            "</td>"+
                            //Cotizacion Multipunto
                            "<td align='center' style='width:10%;'>"+
                                cotizacion+""+
                            "</td>"+
                            //Voz Cliente
                            "<td align='center' style='width:15%;'>"+
                                vc+""+
                                audio+""+
                            "</td>"+
                            //SERVICIO VALET
                            "<td align='center' style='vertical-align: middle;'>"+
                                valet+""+
                            "</td>"+
                            //Comentarios cierre de orden
                            ""+comentario+""+
                            //Agregar refacciones para garantia
                            ""+garantia_ext+""+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});*/

/*function addAudio(renglon) {
    var base = $("#basePeticion").val();
    var orden = $("#audio_"+renglon).data("orden");
    var audio = $("#audio_"+renglon).data("value");

    //Imprimimos en el modal la informacion
    $("#tituloAudio").text("Evidencia de audio No. de Orden: "+orden);
    $("#reproductor").attr("src",base+""+audio);
}*/



//Actualizacion de documentacion por fecha
$("#actualizar_doc").on('click', function (e){
    var fecha = $("#busqueda_fecha").val();
    
    if (fecha != "") {
        $(".cargaIcono").css("display","inline-block");
        var base = $("#basePeticion").val();
        $.ajax({
            url: base+"ordenServicio/Documentacion_Ordenes/actualizar_documentacion", 
            method: 'post',
            data: {
                fecha : fecha
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        $("#error_act_doc").text("ACTUALIZANDO....");
                        location.reload();
                    } else {
                        $("#error_act_doc").text("SE PRODUJO UN ERROR AL ACTUALIZAR COD 1");
                    }

                }else{
                    $("#error_act_doc").text("SE PRODUJO UN ERROR AL ACTUALIZAR");
                }

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                $(".cargaIcono").css("display","none");
                $("#error_act_doc").text("SE PRODUJO UN ERROR AL ACTUALIZAR**");
            }
        });
    } else {
        $("#error_act_doc").text("FECHA INVALIDA");
    }
});
