/*
$('#btnBusqueda_tecnicos').on('click',function(){
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_campo").val();
    var origen_listado = $("#origen_lista").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val();

    $.ajax({
        url: base+"ordenServicio/Documentacion_Ordenes/busqueda_documentacion", 
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            campo : campo,
            listado : origen_listado,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {

                var data = JSON.parse(resp);

                var base = $("#basePeticion").val();
                var base_garantias = $("#basGarantias").val();
                var listado_o = $("#origen_lista").val();
                var pOrigen = $("#pOrigen").val();

                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                if (data.length > 0) {
                    for(i = 0; i < data.length; i++){
                        var btnArchivos = '';
                        //console.log(celdas[15]);
                        if (data[i].oasis != "") {
                            var archivos = data[i].oasis.split("~");
                            var conteo = 1;
                            for (var j = 0; j < archivos.length; j++) {
                                if (archivos[j] != "") {
                                    btnArchivos += "<a href='"+base+""+archivos[j]+"' class='btn btn-primary' target='_blank' style='font-size: 9px;margin-top:5px;'>"+
                                        "ARC. OASIS ("+conteo+")"+
                                    "</a>";
                                    conteo++;
                                }
                            }
                        }

                        var poliza_g = '';
                        //console.log(celdas[15]);
                        if (data[i].doc_poliza == "1") {
                            poliza_g += "<a href='"+base+"Ver_Polizas/"+data[i].id_cita_url+"' class='btn' target='_blank' style='color:white; font-size: 9px; margin-top:2px; background-color: #008080;'>"+
                                "P. GARANTÍA "+
                            "</a>";
                        }
                        /*if (data[i].doc_poliza.length > 0) {
                            var conteok = 1;
                            for (var jk = 0; jk < data[i].doc_poliza.length; jk++) {
                                if (data[i].doc_poliza[jk] != "") {
                                    poliza_g += "<a href='"+base+""+data[i].doc_poliza[jk]+"' class='btn' target='_blank' style='color:white; font-size: 9px; margin-top:2px; background-color: #008080;'>"+
                                        "P. GARANTÍA ("+conteok+")"+
                                    "</a>";
                                    conteok++;
                                }
                            }
                        }* /

                        var multipunto = '';
                        var colorBtnMultipunto = (data[i].firma_jefetaller == '1') ? 'btn-success': 'btn-danger';
                        if (data[i].multipunto == "1") {
                            multipunto = "<a href='"+base+"Multipunto_Revision/"+data[i].id_cita_url+"' class='btn "+colorBtnMultipunto+"' target='_blank' style='font-size: 9.5px;'>"+
                                "MULTIPUNTO"+
                            "</a>";
                        }

                        var presupuesto = '';
                        if (data[i].presupuesto == "1") {
                            if (data[i].presupuesto_afectado != '') {
                                presupuesto = "<a href='"+base+"Presupuesto_Cliente/"+data[i].id_cita_url+"' class='btn btn-warning' target='_blank' style='font-size: 9.5px;'>"+
                                    "PRESUPUESTO"+
                                "</a>";
                            }else{
                                presupuesto = "<a href='"+base+"Alta_Presupuesto/"+data[i].id_cita_url+"' class='btn btn-warning' target='_blank' style='font-size: 9.5px;'>"+
                                    "PRESUPUESTO"+
                                "</a>";
                            }
                        }

                        var vc = '';
                        var audio = '';
                        if (data[i].voz_cliente == "1") {
                            vc = "<a href='"+base+"VCliente_Revision/"+data[i].id_cita_url+"' class='btn btn-secondary' target='_blank' style='font-size: 9.5px;'>"+
                                    "VOZ CLIENTE"+
                                "</a>";
                            //Validamos si hay audio disponible
                            if (data[i].audio_vc != '') {
                                audio = "<a class='AudioModal' id='audio_"+(i+1)+"' onclick='addAudio("+(i+1)+");' data-value='"+data[i].audio_vc+"' data-orden='"+data[i].id_cita+"' data-target='#muestraAudio' data-toggle='modal' style='color:blue;font-size:24px;'>"+
                                    "<i class='far fa-play-circle' style='font-size:24px;'></i>"+
                                "</a>";
                            } else {
                                audio = "<i class='fas fa-volume-mute' style='font-size:24px;color:red;'></i>";
                            }
                        }

                        var garantia_ext = "";
                        var formatos = "";
                        if ((data[i].identificador == "T")||((data[i].identificador == "G")&&(data[i].diagnostico == "0"))) {
                            if (data[i].registros_garantias != "/") {
                                if (data[i].registros_garantias != "0") {
                                    formatos = "<a href='"+base+"Formatos_TecGarantia/"+data[i].id_cita_url+"' class='btn "+(( data[i].cl_btnfg == 1) ? "btn-success" : "btn-danger")+"' target='_blank' style='font-size: 10px;color:white;'>"+
                                                    "Ver Formato(s) "+
                                                "</a>";
                                } else {
                                    formatos = "<a href='#' class='btn btn-default' target='_blank' style='font-size: 10px; color:black;'>"+
                                                    "Sin Formato(s)"+
                                                "</a>";
                                }

                                garantia_ext = "<td align='center' style='vertical-align: middle;'>"+
                                                    "<a href='"+base_garantias+"garantias/F1863/form_registro_labor?no_orden="+data[i].id_cita+"' class='btn' target='_blank' style='font-size: 11px; background-color: #2b3188;color:white;'>F. 1863</a>"+
                                                    "<br>"+
                                                    formatos+""+
                                                "</td>";
                            }
                        } else {
                            if (data[i].registros_garantias != "/") {
                                garantia_ext = "<td align='center' style='vertical-align: middle;'><br></td>";
                            }
                        }

                        var garantia = "";
                        if (data[i].folio_garantia != "/") {
                            if (data[i].folio_garantia == "1") {
                                if (listado_o == "Asesor") {                                
                                    garantia = "<td align='center' style='vertical-align: middle;'>"+
                                        "<a href='"+base_garantias+"index.php/garantias/formato_sin_costos?numero_orden="+data[i].garantias_id_cita+"' class='btn btn-default' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
                                                "GARANTÍA "+
                                            "</a>"+
                                        "</td>";
                                }else{
                                    garantia = "<td align='center' style='vertical-align: middle;'>"+
                                        "<a href='"+base_garantias+"index.php/garantias/formato_folio_tecnico?numero_orden="+data[i].garantias_id_cita+"' class='btn btn-default' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
                                                "GARANTÍA "+
                                            "</a>"+
                                        "</td>";
                                }
                                
                            }else{
                                garantia = "<td align='center' style='vertical-align: middle;'>"+
                                            "<br>"+
                                        "</td>";
                            }
                        }
                            

                        if (data[i].comentarios != "/") {
                            var comentario = "";
                            if (data[i].sesion == "JDT") {
                                comentario = "<td align='center' style='vertical-align: middle;background-color: "+((data[i].comentarios == '1') ? '#5bc0de' : '#ffffff' )+";'>"+
                                    "<a class='addComentarioBtn' onclick='btnAddClick("+data[i].id_cita+")' title='Agregar Comentario' data-id='"+data[i].id_cita+"' data-target='#addComentario' data-toggle='modal'>"+
                                        "<i class='fas fa-comments' style='font-size: 18px;color: black;'></i>"+
                                    "</a>"+

                                    "<a class='historialCometarioBtn' onclick='btnHistorialClick("+data[i].id_cita+")' title='Historial comentarios' data-id='"+data[i].id_cita+"' data-target='#historialCometario' data-toggle='modal' style='margin-left: 20px;'>"+
                                        "<i class='fas fa-info' style='font-size: 18px;color: black;'></i>"+
                                    "</a>"+
                                "</td>";
                            } else {
                                comentario = "<td align='center' style='vertical-align: middle;background-color: "+((data[i].comentarios == '1') ? '#5bc0de' : '#ffffff' )+";'>"+
                                    "<a class='addComentarioBtn' onclick='btnAddClick("+data[i].id_cita+")' title='Agregar Comentario' data-id='"+data[i].id_cita+"' data-target='#addComentario' data-toggle='modal'>"+
                                        //"<i class='fas fa-comments' style='font-size: 18px;color: black;'></i>"+
                                        "<img src='"+base+"assets/imgs/img_icon/charla.png' style='width: 20px;float: left;margin-left:10px;'>"+
                                    "</a>"+

                                    "<a class='historialCometarioBtn' onclick='btnHistorialClick("+data[i].id_cita+")' title='Historial comentarios' data-id='"+data[i].id_cita+"' data-target='#historialCometario' data-toggle='modal' style='margin-left: 20px;'>"+
                                        //"<i class='fas fa-info' style='font-size: 18px;color: black;'></i>"+
                                        "<img src='"+base+"assets/imgs/img_icon/informacion.png' style='width: 20px;float: right;margin-right:10px;'>"+
                                    "</a>"+
                                "</td>";
                            }
                            
                        }

                        var valet = '';
                        
                        if ((listado_o == "Asesor")||((listado_o == "Garantias"))) {
                            if (data[i].servicio_valet == "1") {
                                valet = "<td align='center' style='vertical-align: middle;'>"+
                                    "<a href='"+base_2+"Servicio_valet_PDF/"+data[i].id_cita_url+"' class='btn btn-defalul' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
                                        "VALET"+
                                    "</a>"+
                                "</td>";  
                            }else{
                                valet = "<td align='center' style='vertical-align: middle;'>"+
                                    "<br>"+
                                "</td>"; 
                            }
                        }

                        var orden_garantia = "";
                        if (data[i].folio_garantia != "/") {
                            if (data[i].folio_garantia != "0") {
                                if (listado_o == "Tecnico") {
                                    orden_garantia = "<br> <a href='"+base+"OrdenServicio_RGTecnico/"+data[i].gid_cita_url+"' class='btn "+(( data[i].cl_btng == 1) ? "btn-success" : "btn-danger")+"' target='_blank' style='font-size: 9px;font-weight: bold; color:white;'>"+
                                                "OR. GARANTÍA"+
                                            "</a>";
                                }else if(listado_o == "JefeTaller"){
                                    orden_garantia = "<br> <a href='"+base+"OrdenServicio_RGJefeTaller/"+data[i].gid_cita_url+"' class='btn "+(( data[i].cl_btng == 1) ? "btn-success" : "btn-danger")+"' target='_blank' style='font-size: 9px;font-weight: bold; color:white;'>"+
                                                "OR. GARANTÍA"+
                                            "</a>";
                                }
                                    
                            }
                        }

                        var asociar_folio = "";
                        if (data[i].sesion == "JDT") {
                            asociar_folio = "<td align='center' style='vertical-align: middle;'>"+
                                            "<a href='"+base+"Asociacion_Folios/"+data[i].id_orden+"' class='btn' style='color:white;font-size: 9px; background-color: #9b59b6;''>ASIGNACIÓN</a>"+
                                        "</td>";
                        }

                        var kilometraje = "";
                        if (pOrigen == "PCO") {
                            kilometraje = "<td align='center' style='vertical-align: middle;'>"+data[i].km+ "</td>";
                        }

                        tabla.append("<tr style='font-size:11px;background-color: "+data[i].tipo_orden +"; '>"+
                            //NO.  ORDEN
                            "<td align='center' style='vertical-align: middle;'>"+data[i].identificador+ "-" +data[i].id_cita+ "</td>"+
                            //ORDEN INTELISIS
                            "<td align='center' style='vertical-align: middle;'>"+data[i].folio_externo+ "</td>"+
                            //APERTURA ORDEN
                            "<td align='center' style='vertical-align: middle;'>"+data[i].fecha_recepcion +" "+" </td>"+
                            //VEHÍCULO
                            "<td align='center' style='vertical-align: middle;'>"+data[i].vehiculo +"</td>"+
                            //PLACAS
                            "<td align='center' style='vertical-align: middle;'>"+data[i].placas +"</td>"+
                            //KILOMETRAJE
                            ""+kilometraje+""+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;'>"+data[i].asesor +"</td>"+
                            //TÉCNICO
                            "<td align='center' style='vertical-align: middle;'>"+data[i].tecnico +"</td>"+
                            //CLIENTE
                            "<td align='center' style='vertical-align: middle;'>"+data[i].cliente +"</td>"+
                            //ACCIONES
                            //ORDEN SERVICIO
                            "<td align='center' style='width:15%;vertical-align: middle;'>"+
                                "<a href='"+base+"OrdenServicio_Revision/"+data[i].id_cita_url+"' class='btn btn-info' target='_blank' style='font-size: 9.5px;'>"+
                                    "ORDEN"+
                                "</a>"+
                                orden_garantia+""+
                            "</td>"+
                            //Archivo Oasis
                            "<td align='center' style='vertical-align: middle;'>"+
                                btnArchivos+""+
                                poliza_g+""+
                            "</td>"+
                            //Multipunto
                            "<td align='center' style='width:10%;'>"+
                                multipunto+""+
                                "<label style='color:blue;font-weight: bold;'>"+((data[i].lavado != '') ? data[i].lavado : '')+"</label>"+
                            "</td>"+
                            //Cotizacion Multipunto
                            "<td align='center' style='width:10%;'>"+
                                presupuesto+""+
                            "</td>"+
                            //Voz Cliente
                            "<td align='center' style='width:15%;'>"+
                                vc+""+
                                audio+""+
                            "</td>"+
                            //SERVICIO VALET
                            ""+valet+""+
                            //G. F1863
                            ""+garantia_ext+""+
                            //FOLIO GARANTÍA
                            ""+garantia+""+
                            //ASOCIAR FOLIO GARANTÍA
                            ""+asociar_folio+""+
                            //COMENTARIOS
                            ""+comentario+""+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='15'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='15'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});
*/

function addAudio(renglon) {
    var base = $("#basePeticion").val();
    var orden = $("#audio_"+renglon).data("orden");
    var audio = $("#audio_"+renglon).data("value");

    //Imprimimos en el modal la informacion
    $("#tituloAudio").text("Evidencia de audio No. de Orden: "+orden);
    $("#reproductor").attr("src",base+""+audio);
}