function buscadorOrden() {
    //Recuperamos el nomero de orden ingresado
    var orden = $("#orderService").val().trim();
    var base = $("#basePeticion").val().trim();

    if (orden != "") {
        $.ajax({
            url: base+"ordenServicio/ordenServicio/searchOrden",
            method: 'post',
            data: {
                orden: orden,
            },
            success:function(resp){
                if (resp.indexOf("handler			</p>")<1) {
                    var respuesta = resp.split("_");

                    if (respuesta[0] == "ALTA") {
                        //Redireccionamos al formulario de alta
                        location.href = base+"OrdenServicio_Alta/"+respuesta[1];
                    }else {
                        //Cargamos la informacion del formulario
                        location.href = base+"OrdenServicio_Verifica/"+respuesta[1];
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
}
