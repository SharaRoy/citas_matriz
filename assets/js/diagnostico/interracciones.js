$(document).ready(function() {
    if ($("#cargaFormulario").val() == "1") {
        if ($("input[name='autorizacion_ausencia']").is(':checked') ) {
            //$(".firmaCliente").attr('hidden',true);
            //$(".mt").attr('disabled',true);
            //$("input[name='autorizacion_ausencia']").attr('disabled',true);
        }else{
            $(".firmaCliente").attr('hidden',false);
        }
        prellenado();
    }

});

$("input[name='autorizacion_ausencia']").on('change', function() {
    if ($("input[name='autorizacion_ausencia']").is(':checked') ) {
        $(".firmaCliente").attr('hidden',true);
    }else{
        $(".firmaCliente").attr('hidden',false);
    }
});

$("input[name='danos']").click(function () {
    if($("input:radio[name='danos']:checked").val() == "0"){
        $("#golpes").attr('disabled',true);
        $("#roto").attr('disabled',true);
        $("#rayones").attr('disabled',true);
        
        $("#limpio").attr('hidden',false);
        $("#canvas_3").attr('hidden',true);
    }else{
        $("#golpes").attr('disabled',false);
        $("#roto").attr('disabled',false);
        $("#rayones").attr('disabled',false);

        $("#limpio").attr('hidden',true);
        $("#canvas_3").attr('hidden',false);
    }
});

function buscadorOrden() {
    //Recuperamos el nomero de orden ingresado
    var orden = $("#orderService").val();
    var orden_prev = $("#noServicio").val();
    var base = $("#basePeticion").val();

    if ((orden != "")&&(orden != orden_prev)&&($("#cargaFormulario").val() != "1")) {
        console.log("Edicion");
        $.ajax({
            url: base+"ordenServicio/ordenServicio/searchOrden",
            method: 'post',
            data: {
                orden: orden,
            },
            success:function(resp){
                if (resp.indexOf("handler			</p>")<1) {
                    var ident = resp.split("=");
                    if (ident[0] == "ALTA") {
                        if (ident[2] != "") {
                            var error_1 = $("#error_orderService");
                            error_1.empty();
                            error_1.append('<label class="form-text text-danger">'+ident[2]+'</label>');
                            retorno = false;
                        }else{
                            if ($("#cargaFormulario").val() != "1") {
                                location.href = base+"OrdenServicio_Alta/"+ident[1];
                            }
                        }
                    }else {
                        //Cargamos la informacion del formulario
                        location.href = base+"OrdenServicio_Edicion/"+ident[1];
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    } else if($("#cargaFormulario").val() == "1"){
        prellenado();
    }
}

function prellenado() {
    var orden = $("#orderService").val();
    var base = $("#basePeticion").val();
    
    $.ajax({
        url: base+"multipunto/Multipunto/precarga",
        method: 'post',
        data: {
          serie: orden,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {

                var campos = resp.split("|");
                //console.log(campos);
                if (campos.length > 5) {
                    // $("#txtmodelo").val(campos[0]);
                    $("#noSerieText").val(campos[1]);
                    $("#txttorre").val(campos[2]);
                    $("#txtnombreCarro").val(campos[3]);
                    $(".nombreCliente").val(campos[3]);
                    $("#txtemail").val(campos[4]);
                    $(".asesorname").val(campos[5]);
                    //$("#txtEmpresa").val(campos[6]);
                    $("#tecniconame").val(campos[7]);
                    $("#txtmodelo").val(campos[8]);
                    $("#nombreCamapanias").text("Campaña(s): "+campos[9]);
                    $("#correoCompaniaLabel").val(campos[11]);
                }else{
                    $(".asesorname").val(campos[0]);
                }
            }
        //Cierre de success
        },
        error:function(error){
          console.log(error);
        //Cierre del error
        }
     //Cierre del ajax
    });
}
