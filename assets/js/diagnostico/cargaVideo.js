//Eleccion de radio para distingir el tipo de archivo que se subira
$("input[name='tipoEnvioEvidencia']").on('change',function(){
    var tipo = $("input[name='tipoEnvioEvidencia']:checked").val();
    if (tipo == "imagenes") {
        $("#uploadfilesVideo").attr("accept","image/*");
    }else {
        $("#uploadfilesVideo").attr("accept","video/*");
    }
});


$("#enviarVideo").on('click',function(){
    $("#comentarioVideo").val("");
    // $("#uploadfilesVideo").val("");
    $("#errorArchivoVideo").text("");
});


//Envio de video en la orden de servicio
$("#enviarVideo").on('click',function(){
    var idCita = $("#idCitaVideo").val().trim();
    var comentario = $("#comentarioVideo").prop('value');
    var archivo = $("#uploadfilesVideo");
    var tipoArchivo = $("input[name='tipoEnvioEvidencia']:checked").val();

    var validacionTipo = 0;

    //Validamos el tipo de archivo a subir
    if (tipoArchivo == "imagenes") {
        //Archivos tipo imagen
        var tipoCarga = $('#uploadfilesVideo')[0].files[0]['type'];
        var tipoFinal = tipoCarga.split("/");
        validacionTipo = (tipoFinal[0] == "image") ? 1 : 0;
    } else {
        //Archivos tipo video
        var tipoCarga = $('#uploadfilesVideo')[0].files[0]['type'];
        var tipoFinal = tipoCarga.split("/");
        validacionTipo = (tipoFinal[0] == "video") ? 1 : 0;
    }
    // console.log(tipoFinal);
    console.log("validacion: "+validacionTipo);

    //Verificamos que se haya insertado el id de la cita
    if ((idCita != "")&&(validacionTipo == 1)) {
        $("#errorIdCitaVideo").text("");
        //Verificamos que se haya insertado el video
        if ((archivo[0].files).length > 0) {
            //Bloqueamos los campos (para disimular carga)
            $("#idCitaVideo").attr("disabled", true);
            $("#comentarioVideo").attr("disabled", true);
            $("input[name='tipoEnvioEvidencia']").attr("disabled", true);
            $(".cargaIcono").css("display","inline-block");

            $("#errorArchivoVideo").text("");

            var paqueteDeDatos = new FormData();
            paqueteDeDatos.append('archivo', $('#uploadfilesVideo')[0].files[0]);
            paqueteDeDatos.append('idCita', $('#idCitaVideo').prop('value'));
            paqueteDeDatos.append('comentario', $('#comentarioVideo').val());
            paqueteDeDatos.append('tipoArchivo', $("input[name='tipoEnvioEvidencia']:checked").prop('value'));

             // $('#btnCerrarVideo').prop("disabled","disabled");
             // $('#enviarVideo').prop("disabled","disabled");
             envio(paqueteDeDatos);
        } else {
          $("#errorArchivoVideo").text("Falta insetar archivo de video/imagen");
        }
    }else{
        if (idCita == "") {
            $("#errorIdCitaVideo").text("Falta el No. de Orden");
        }else {
            $("#errorIdCitaVideo").text("El tipo de archivo no coincide con la selección");
        }
    }
});

//Funcion envio formulario
function envio(paqueteDeDatos){
    var base = $("#basePeticion").val().trim();
    $.ajax({
          url: base+"ordenServicio/OrdenServicio/videoAjax",
          type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
          contentType: false,
          data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
          processData: false,
          cache: false,
          success:function(resp){
              console.log(resp);
              //Desbloqueamos los campos (para disimular carga)
              $("#idCitaVideo").attr("disabled", false);
              $("#comentarioVideo").attr("disabled", false);
              $(".cargaIcono").css("display","none");
              $("input[name='tipoEnvioEvidencia']").attr("disabled", false);

              $('#errorVideo').text("Subiendo video.");

              $('#btnCerrarVideo').prop("disabled",false);
              $('#enviarVideo').prop("disabled",false);

              console.log(resp);
              if (resp == "OK") {
                  $('#OkResult').fadeIn(1000);
                  $('#OkResult').fadeOut(2000);
                  $('#btnCerrarVideo').prop("disabled",false);
                  $('#btnCerrarVideo').text("Cerrar");
                  $('#errorVideo').text("Archivo enviado exitosamente.");

                  //Limpiamos los campos
                  // $("#idCitaVideo").val("");
                  $("#comentarioVideo").val("");
                  $("#uploadfilesVideo").val("");

                  // $("#enviarVideo").attr("data-dismiss", "modal");
                  // $('#enviarVideo').css("display","none");
              }else if (resp == "EXISTE") {
                  $('errorResult').fadeIn(1000);
                  $('errorResult').fadeOut(2000);
                  // $('#btnCerrarVideo').prop("disabled",false);
                  // $('#enviarVideo').prop("disabled",false);
                  $('#errorVideo').text("Ya existe un video para esta orden");
                  $("#errorArchivoVideo").text("Ya existe un video para esta orden");
              }else {
                  $('errorResult').fadeIn(1000);
                  $('errorResult').fadeOut(2000);
                  // $('#btnCerrarVideo').prop("disabled",false);
                  // $('#enviarVideo').prop("disabled",false);
                  $('#errorVideo').text("Error al enviar el archivo.");
                  $("#errorArchivoVideo").text("Error al enviar el archivo.");
              }
          //Cierre de success
          },
          error:function(error){
              console.log(error);
              // $('#btnCerrarVideo').prop("disabled",false);
              // $('#enviarVideo').prop("disabled",false);

              //Desbloqueamos los campos (para disimular carga)
              $("#idCitaVideo").attr("disabled", false);
              $("#comentarioVideo").attr("disabled", false);
              $(".cargaIcono").css("display","none");
              $("input[name='tipoEnvioEvidencia']").attr("disabled", false);
              $('#errorVideo').text("Error de carga VERIFICAR PESO DEL ARCHIVO.");
          //Cierre del error
          }
      //Cierre del ajax
      });
}
