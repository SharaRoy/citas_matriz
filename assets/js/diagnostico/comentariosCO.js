//Modales para los comentarios (agregar)
function btnAddClick(idOrden){
    // var idOrden = $(this).data("id");
    var hoy = new Date();
    var hora = hoy.getHours()+":"+hoy.getMinutes()
    console.log("Id de Magic: "+idOrden);
    $("#idMagigAdd").val(idOrden);
    $("#titulo").val("");
    $("#HoraComentario").val(hora);
    $("#comentario").val("");
}

//Enviamos el comentario por ajax
$("#btnEnviarComentario").on('click',function(){
    //Recuperamos los valores para enviar
    var idHistorial = $("#idMagigAdd").val();
    var titulo = $("#titulo").val();
    var hora = $("#HoraComentario").val();
    var fecha = $("#fechaComentario").val();
    var comentario = $("#comentario_notas").val(); 
    //var comentario = document.getElementById('comentario').value;

    //Hacemos la peticion ajax de la busqueda
    var url = $("#basePeticion").val();
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");

    $.ajax({
        url: url+"ordenServicio/ComentariosCO/comentarioCO",
        method: 'post',
        data: {
            idHistorial : idHistorial,
            titulo : titulo,
            hora : hora,
            fecha : fecha,
            comentario : comentario,
        },
        success:function(resp){
            console.log(resp);

            if (resp.indexOf("handler			</p>") < 1) {
                if (resp == "OK") {
                    $('#OkResult').fadeIn(1000);
                    $('#OkResult').fadeOut(2000);
                }else {
                    $('errorResult').fadeIn(1000);
                    $('errorResult').fadeOut(2000);
                }
            }
            $(".cargaIcono").css("display","none");
            limpiar();
        //Cierre de success
        },
        error:function(error){
            console.log(error);
            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Limpiamos modal de comentario
function limpiar(){
    var hoy = new Date();
    var hora = hoy.getHours()+":"+hoy.getMinutes();
    $("#titulo").val("");
    $("#HoraComentario").val(hora);
    $("#comentario").val("");
};

//Modales para los comentarios (historial)
// $(".historialCometarioBtn").on('click',function(){
function btnHistorialClick(idHistorial){
    // var idOrden = $(this).data("id");
    console.log("Numero Orden Historia: "+idHistorial);
    $("#idMagigHistorial").val(idHistorial);

    //Enviamos el id de magic para recuperar los comentarios
    //Hacemos la peticion ajax de la busqueda
    var url = $("#basePeticion").val();

    $.ajax({
        url: url+"ordenServicio/ComentariosCO/loadHistoryCierreOrden",
        method: 'post',
        data: {
            idHistorial : idHistorial,
        },
        success:function(resp){
            console.log(resp);

            //identificamos la tabla a afectar
            var tabla = $("#tablaComnetario");
            tabla.empty();

            if ((resp.indexOf("handler			</p>") < 1)&&(resp.length > 10)) {
                //Fraccionamos la respuesta en renglones
                var campos = resp.split("|");

                //Recorremos los "renglones" obtenidos para partir los campos
                var registro = ( campos.length > 1) ? campos.length -1 : campos.length;

                for (var i = 0; i < registro; i++) {
                    var celdas = campos[i].split("_");
                    // console.log(celdas);
                    if (celdas.length>1) {
                        tabla.append("<tr style='font-size:13px;'>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 13px; width: 25%;font-family: Arial;'>"+celdas[0].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 13px; width: 25%;font-family: Arial;'>"+celdas[2].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 10px; width: 50%;font-family: Arial;'>"+celdas[1].toUpperCase()+"</td>"+
                        "</tr>");
                    }

                }
            }else{
                tabla.append("<tr style='font-size:13px;'>"+
                    "<td align='center' colspan='3' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 100%;font-family: Arial;'>No se encontraron registros de la busqueda</td>"+
                    "</td>"+
                "</tr>");
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}