$("#envio_formulario").on('click', function (e){
    $("#cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    //$("#formulario").css("background-color","#ddd");

    // Evitamos que salte el enlace.
    e.preventDefault();

    var validaciones = validar();

    if(validaciones){
        $('input').attr('disabled',false);
        $('textarea').attr('disabled',false);
        
    	var paqueteDatos = new FormData(document.getElementById('formulario'));
    	var base = $("#basePeticion").val();

        $.ajax({
            url: base+"ordenServicio/ordenServicio/guardar_formulario", 
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("=");
                    if (respuesta[0] == "OK") {
                        location.href = base+"Panel_Asesor/1";
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                    //$("#formulario_error").text(resp);
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                //$("#formulario").css("background-color","white");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                //$("#formulario").css("background-color","white");
                $("#formulario_error").text("SE PRODUJO UN ERROR EN EL ENVIO*");
                //location.href = base+"index.php/inventario/index/"+orden;
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
        $("#formulario_error").text("FALTAN CAMPOS POR COMPLETAR");
    }
});

function validar() {
    var retorno = true;

    if ($("#cargaFormulario").val() == "1") {
        var dataURL = canvas_3.toDataURL('image/png');
        $("#danosMarcas").val(dataURL);
    }

    var campo_1 = $("input[name='orderService']").val();
    if ((campo_1 == "")||(campo_1 == "0")) {
        var error_1 = $("#error_orderService");
        error_1.empty();
        error_1.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var error_1 = $("#error_orderService");
        error_1.empty();
    }

    if ($("input[name='autorizacion_ausencia']").is(':checked') ) {
        var campo_2 = $("#formulario input[name='motivo']:radio").is(':checked');
        if (!campo_2) {
            $("#error_autorizacion_ausencia").text("Falta indicar un motivo");
            retorno = false;
        }else{
            $("#error_firma_3").text(""); 
        }
    }else{
        var error_2 = $("#error_autorizacion_ausencia");
        error_2.empty();  
    }

    var tipo_formulario = $("input[name='cargaFormulario']").val();
    if (tipo_formulario == "1") {
        if (!$("input[name='autorizacion_ausencia']").is(':checked') ) {
            var firma_2 = $("input[name='rutaFirmaAcepta']").val();
            var nombre_2 = $("input[name='consumidor1Nombre']").val();
            if ((firma_2 == "")||(nombre_4 == "")) {
                var firma_2 = $("#error_firma_2");
                firma_2.empty();
                firma_2.append('<label class="form-text text-danger">Falta indicar un firma/nombre</label>');
                retorno = false;
            }else{
                var firma_2 = $("#error_firma_2");
                firma_2.empty();
            }

            var firma_4 = $("input[name='rutaFirmaConsumidor']").val();
            var nombre_4 = $("input[name='nombreConsumidor2']").val();
            if ((firma_4 == "")||(nombre_4 == "")) {
                $("#error_firma_4").text("Falta indicar un firma/nombre");
                retorno = false;
            }else{
                $("#error_firma_4").text("");
            }

            var firma_5 = $("input[name='rutaFirmaConsumidor_2']").val();
            if (firma_5 == "") {
                $("#error_firma_5").text("Falta indicar un firma");
                retorno = false;
            }else{
                $("#error_firma_5").text("");
            }

            var firma_7 = $("input[name='rutaFirmaConsumidor_3']").val();
            var nombre_7 = $("input[name='nombreConsumidor_3']").val();
            if ((firma_7 == "")||(nombre_7 == "")) {
                $("#error_firma_7").text("Falta indicar un firma/nombre");
                retorno = false;
            }else{
                $("#error_firma_7").text("");
            }

            if (!$("input[name='terminos_contrato']").is(':checked') ) {
                $("#error_terminos_contrato").text("Falta aceptar Términos y Condiciones");
                retorno = false;
            }else{
                $("#error_terminos_contrato").text("");
            }
        }else{
            $("#error_terminos_contrato").text("");
        }
    }

    var firma_1 = $("input[name='rutaFirmaDiagnostico']").val();
    var nombre_1 = $("input[name='nombreDiagnostico']").val();
    if ((firma_1 == "")||(nombre_1 == "")) {
        var firma_1 = $("#error_firma_1");
        firma_1.empty();
        firma_1.append('<label class="form-text text-danger">Falta indicar un firma/nombre</label>');
        retorno = false;
    }else{
        var firma_1 = $("#error_firma_1");
        firma_1.empty();
    }

    var firma_3 = $("input[name='rutaFirmaAsesor']").val();
    var nombre_3 = $("input[name='asesorNombre']").val();
    if ((firma_3 == "")||(nombre_3 == "")) {
        $("#error_firma_3").text("Falta indicar un firma/nombre");
        retorno = false;
    }else{
        $("#error_firma_3").text("");
    }


    var firma_6 = $("input[name='rutaFirmaDistribuidor_3']").val();
    var nombre_6 = $("input[name='nombreDistribuidor_3']").val();
    if ((firma_6 == "")||(nombre_6 == "")) {
        $("#error_firma_6").text("Falta indicar un firma/nombre");
        retorno = false;
    }else{
        $("#error_firma_6").text("");
    }

    var check_interiores = $(".interiores:checked").length; 
    if (check_interiores <= 24) {
        $("#error_interiores").text("Faltan opciones de indicar"); 
        retorno = false;
    }else{
        $("#error_interiores").text("");
    }

    var check_cauela = $(".cauela:checked").length; 
    if (check_cauela <= 5) {
        $("#error_cajuela").text("Faltan opciones de indicar");
        retorno = false;
    }else{
        $("#error_cajuela").text("");
    }

    var check_exteriores = $(".exteriores:checked").length; 
    if (check_exteriores <=  3) {
        $("#error_exteriores").text("Faltan opciones de indicar");
        retorno = false;
    }else{
        $("#error_exteriores").text("");
    }

    var check_documentacion = $(".documentacion:checked").length; 
    if (check_documentacion <= 3) {
        $("#error_documentacion").text("Faltan opciones de indicar");
        retorno = false;
    }else{
        $("#error_documentacion").text("");
    }

    var gas_1 = $("input[name='nivelGasolina_1']").val();
    var gas_2 = $("input[name='nivelGasolina_2']").val();
    if ((gas_1 == "")||(gas_2 == "")) {
        $("#error_gas").text("Falta indicar.");
        retorno = false;
    }else{
        $("#error_gas").text("");
    }

    var campo_7 = $("input[name='plazoGarantia']").val();
    if (campo_7 == "") {
        $("#error_plazoGarantia").text("Falta indicar.");
        retorno = false;
    }else{
        $("#error_plazoGarantia").text("");
    }

    return retorno;
}


// ------------------------------------------------------------------------------
//  -----------------------------------------------------------------------------

$("#envio_diag").on('click', function (e){
    $("#cargaIcono").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault();

    var validaciones = validar2();

    if(validaciones){
        $('input').attr('disabled',false);
        $('textarea').attr('disabled',false);
        
        var paqueteDatos = new FormData(document.getElementById('formulario_cliente'));
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"ordenServicio/ordenServicio/guardar_firmas", 
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("=");
                    if (respuesta[0] == "OK") {
                        location.reload();
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                    //$("#formulario_error").text(resp);
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                $("#formulario_error").text("SE PRODUJO UN ERROR EN EL ENVIO");
                //location.href = base+"index.php/inventario/index/"+orden;
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
        $("#formulario_error").text("FALTAN CAMPOS POR COMPLETAR");
    }
});

function validar2() {
    var retorno = true;

    var firma_2 = $("input[name='rutaFirmaAcepta']").val();
    if (firma_2 == "") {
        var firma_2 = $("#error_firma_2");
        firma_2.empty();
        firma_2.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var firma_2 = $("#error_firma_2");
        firma_2.empty();
    }

    var firma_4 = $("input[name='rutaFirmaConsumidor']").val();
    if (firma_4 == "") {
        $("#error_firma_4").text("Campo requerido");
        retorno = false;
    }else{
        $("#error_firma_4").text("");
    }

    var firma_5 = $("input[name='rutaFirmaConsumidor_2']").val();
    if (firma_5 == "") {
        $("#error_firma_5").text("Campo requerido");
        retorno = false;
    }else{
        $("#error_firma_5").text("");
    }

    var firma_7 = $("input[name='rutaFirmaConsumidor_3']").val();
    if (firma_7 == "") {
        $("#error_firma_7").text("Campo requerido");
        retorno = false;
    }else{
        $("#error_firma_7").text("");
    }

    if (!$("input[name='aceptar']").is(':checked') ) {
        $("#error_aceptar").text("Campo requerido");
        retorno = false;
    }else{
        $("#error_aceptar").text("");
    }

    if (!$("input[name='terminos_contrato']").is(':checked') ) {
        $("#error_terminos_contrato").text("Campo requerido");
        retorno = false;
    }else{
        $("#error_terminos_contrato").text("");
    }

    return retorno;
}