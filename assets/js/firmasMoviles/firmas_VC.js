$(document).ready(function() {
    //Recuperamos la medida de la pantalla
    // var alto = $( window ).height();
    // var ancho = $( window ).width();
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 499) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = (alto * 0.22)+10;
        var nvo_ancho = (ancho * 0.50)+10;
        console.log("Tablet");
    }else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        // console.log(destino);
        // Comprobamos el tipo de usuario que firmara
        if ((destino == "Diagnostico")||(destino == "Cliente")) {
            $('#rutaFirma').val(data);
            $("#firmaImg").attr("src",data);
        } else if (destino == "Consumidor_1") {
            $('#rutaFirmaAcepta').val(data);
            $("#firmaImgAcepta").attr("src",data);
        }else if (destino == "Asesor") {
            $('#rutaFirmaAsesor').val(data);
            $("#firmaFirmaAsesor").attr("src",data);
        }else if (destino == "El_Consumidor") {
            $('#rutaFirmaConsumidor_2').val(data);
            $("#firmaFirmaConsumi_2").attr("src",data);
        }else if (destino == "Distribuidor") {
            $('#rutaFirmaDistribuidor').val(data);
            $("#firmaFirmaDistribuidor").attr("src",data);
        }else if (destino == "Consumidor") {
            $('#rutaFirmaConsumidor').val(data);
            $("#firmaFirmaConsumidor").attr("src",data);
        }else if (destino == "Consumidor_2") {
            $('#rutaFirmaConsumidor').val(data);
            $("#firmaFirmaConsumidor").attr("src",data);
        }else if (destino == "Consumidor_3") {
            $('#rutaFirmaConsumidor_3').val(data);
            $("#firmaFirmaConsumidor_3").attr("src",data);
        }else if (destino == "Distribuidor_3") {
            $('#rutaFirmaDistribuidor_3').val(data);
            $("#firmaFirmaDistribuidor_3").attr("src",data);
        }

        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });
});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    $('#destinoFirma').val(firma);
    // Comprobamos el tipo de usuario que firmara
    if (firma == "Diagnostico") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma de quien elaboro el diagnóstico.");
    } else if (firma == "Consumidor_1") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Consumidor.");
    }else if (firma == "Asesor") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Asesor.");
    }else if (firma == "Consumidor_2"){
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Consumidor.");
    }else if (firma == "El_Consumidor"){
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Consumidor.");
    }else if (firma == "Distribuidor_3"){
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Asesor.");
    }else if (firma == "Consumidor_3"){
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Consumidor.");
    }
});
