$(document).ready(function() {
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 500) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.50;
        console.log("Tablet");
    }else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    // $("#canvas").prop("height",nvo_altura.toFixed(2));
    $("#canvas").prop("height","200");


    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        // console.log(destino);

        if (destino == "MecaOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaMecaOperario').val(data);
            $("#firmaMecaOperario").attr("src",data);
        }else if (destino == "MecaLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaMecaLider').val(data);
            $("#firmaMecaLider").attr("src",data);
        }else if (destino == "MecaCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaMecaCoord').val(data);
            $("#firmaMecaCoord").attr("src",data);
        }else if (destino == "LamiOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaLamiOperario').val(data);
            $("#firmaLamiOperario").attr("src",data);
        }else if (destino == "LamiLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaLamiLider').val(data);
            $("#firmaLamiLider").attr("src",data);
        }else if (destino == "LamiCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaLamiCoord').val(data);
            $("#firmaLamiCoord").attr("src",data);
        }else if (destino == "PrepOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaPrepOperario').val(data);
            $("#firmaPrepOperario").attr("src",data);
        }else if (destino == "PrepLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaPrepLider').val(data);
            $("#firmaPrepLider").attr("src",data);
        }else if (destino == "PrepCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaPrepCoord').val(data);
            $("#firmaPrepCoord").attr("src",data);
        }else if (destino == "PintOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaPintOperario').val(data);
            $("#firmaPintOperario").attr("src",data);
        }else if (destino == "PintLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaPintLider').val(data);
            $("#firmaPintLider").attr("src",data);
        }else if (destino == "PintCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaPintCoord').val(data);
            $("#firmaPintCoord").attr("src",data);
        }else if (destino == "ArmaOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaArmaOperario').val(data);
            $("#firmaArmaOperario").attr("src",data);
        }else if (destino == "ArmaLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaArmaLider').val(data);
            $("#firmaArmaLider").attr("src",data);
        }else if (destino == "ArmaCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaArmaCoord').val(data);
            $("#firmaArmaCoord").attr("src",data);
        }else if (destino == "DetallOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaDetallOperario').val(data);
            $("#firmaDetallOperario").attr("src",data);
        }else if (destino == "DetallLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaDetallLider').val(data);
            $("#firmaDetallLider").attr("src",data);
        }else if (destino == "DetallCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaDetallCoord').val(data);
            $("#firmaDetallCoord").attr("src",data);
        }else if (destino == "CaliCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaCaliCoord').val(data);
            $("#firmaCaliCoord").attr("src",data);
        }

        if (destino == "AsesorCostos") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaAsesorCostos').val(data);
            $("#firmaAsesorCostos").attr("src",data);
        }
        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
        var destino = $('#destinoFirma').val();
        // console.log(destino);

        if (destino == "MecaOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaMecaOperario').val("");
            $("#firmaMecaOperario").attr("src","");
        }else if (destino == "MecaLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaMecaLider').val("");
            $("#firmaMecaLider").attr("src","");
        }else if (destino == "MecaCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaMecaCoord').val("");
            $("#firmaMecaCoord").attr("src","");
        }else if (destino == "LamiOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaLamiOperario').val("");
            $("#firmaLamiOperario").attr("src","");
        }else if (destino == "LamiLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaLamiLider').val("");
            $("#firmaLamiLider").attr("src","");
        }else if (destino == "LamiCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaLamiCoord').val("");
            $("#firmaLamiCoord").attr("src","");
        }else if (destino == "PrepOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaPrepOperario').val("");
            $("#firmaPrepOperario").attr("src","");
        }else if (destino == "PrepLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaPrepLider').val("");
            $("#firmaPrepLider").attr("src","");
        }else if (destino == "PrepCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaPrepCoord').val("");
            $("#firmaPrepCoord").attr("src","");
        }else if (destino == "PintOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaPintOperario').val("");
            $("#firmaPintOperario").attr("src","");
        }else if (destino == "PintLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaPintLider').val("");
            $("#firmaPintLider").attr("src","");
        }else if (destino == "PintCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaPintCoord').val("");
            $("#firmaPintCoord").attr("src","");
        }else if (destino == "ArmaOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaArmaOperario').val("");
            $("#firmaArmaOperario").attr("src","");
        }else if (destino == "ArmaLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaArmaLider').val("");
            $("#firmaArmaLider").attr("src","");
        }else if (destino == "ArmaCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaArmaCoord').val("");
            $("#firmaArmaCoord").attr("src","");
        }else if (destino == "DetallOperario") {
            //Designamos el destino de la firma en la vista
            $('#rutaDetallOperario').val("");
            $("#firmaDetallOperario").attr("src","");
        }else if (destino == "DetallLider") {
            //Designamos el destino de la firma en la vista
            $('#rutaDetallLider').val("");
            $("#firmaDetallLider").attr("src","");
        }else if (destino == "DetallCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaDetallCoord').val("");
            $("#firmaDetallCoord").attr("src","");
        }else if (destino == "CaliCoord") {
            //Designamos el destino de la firma en la vista
            $('#rutaCaliCoord').val("");
            $("#firmaCaliCoord").attr("src","");
        }else if (destino == "AsesorCostos") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaAsesorCostos').val("");
            $("#firmaAsesorCostos").attr("src","");
        }
    });


$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    //Pasamos el valor al formulario local
    $('#destinoFirma').val(firma);

    if (firma == "MecaOperario") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA OPERARIO (MECÁNICA)");
    }else if (firma == "MecaLider") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA LÍDER (MECÁNICA)");
    }else if (firma == "MecaCoord") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA COORDINADOR (MECÁNICA)");
    }else if (firma == "LamiOperario") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA OPERARIO (LAMINADO)");
    }else if (firma == "LamiLider") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA LÍDER (LAMINADO)");
    }else if (firma == "LamiCoord") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA COORDINADOR (LAMINADO)");
    }else if (firma == "PrepOperario") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA OPERARIO (PREPARACIÓN)");
    }else if (firma == "PrepLider") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA LÍDER (PREPARACIÓN)");
    }else if (firma == "PrepCoord") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA COORDINADOR ()");
    }else if (firma == "PintOperario") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA OPERARIO (PINTURA)");
    }else if (firma == "PintLider") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA LÍDER (PINTURA)");
    }else if (firma == "PintCoord") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA COORDINADOR (PINTURA)");
    }else if (firma == "ArmaOperario") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA OPERARIO (ARMADO)");
    }else if (firma == "ArmaLider") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA LÍDER (ARMADO)");
    }else if (firma == "ArmaCoord") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA COORDINADOR (ARMADO)");
    }else if (firma == "DetallOperario") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA OPERARIO (DETALLADO)");
    }else if (firma == "DetallLider") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA LÍDER (DETALLADO)");
    }else if (firma == "DetallCoord") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA COORDINADOR (DETALLADO)");
    }else if (firma == "CaliCoord") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA COORDINADOR (C. DE CALIDAD)");
    }else {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA ASESOR QUE APRUEBA");
    }
});



});
