$(document).ready(function() {
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 500) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.53;
        console.log("Tablet");
    }else{
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        // console.log(destino);

        if (destino == "Asesor") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaAsesor').val(data);
            $("#firmaFirmaAsesor").attr("src",data);
        }else if (destino == "Tecnico") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaTecnico').val(data);
            $("#firmaFirmaTecnico").attr("src",data);
        }else if (destino == "JefeTaller") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaJefeTaller').val(data);
            $("#firmaFirmaJefeTaller").attr("src",data);
        }else if (destino == "AsesorCostos") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaAsesorCostos').val(data);
            $("#firmaAsesorCostos").attr("src",data);
            $(".terminarCotizacion").attr('disabled',false);
        }else if (destino == "Garantias") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaGarantia').val(data);
            $("#firmaFirmaGarantia").attr("src",data);
        }else if (destino == "Coti_JDT") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaJDT').val(data);
            $("#firmaFirmaJDT").attr("src",data);
        }else if (destino == "Req") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaReq').val(data);
            $("#firmaFirmaReq").attr("src",data);
        }else if (destino == "Ref_prep") {
            //Designamos el destino de la firma en la vista
            $('#rutafirmaPrep').val(data);
            $("#firmaPrep").attr("src",data);
        }else if (destino == "Req_firma_gerente") {
            //Designamos el destino de la firma en la vista
            $('#ruta_parte_req').val(data);
            $("#firma_parte_req").attr("src",data);

            $("#enviar_comentario_req").attr('disabled',false);
        }else { //Cliente
          //Designamos el destino de la firma en la vista
          $('#rutaFirmaConsumidor').val(data);
          $("#firmaFirmaConsumidor").attr("src",data);
        }

        signaturePad.clear();

        //En caso que la vista sea de la cotizacion de garantias, debloqueamos el boton
        if (($("#origen").val() == "Cotizacion Multipunto")&&(($("#modoVistaFormulario").val() == "3A")||($("#modoVistaFormulario").val() == "5A"))) {
            console.log("condiciones firmas 1");
            $("#aceptarCotizacion").attr('disabled',false);
            $("#aceptarCotizacionJDT").attr('disabled',false);
        }

        if (($("#origen").val() == "Presupuesto")&&(($("#modoVistaFormulario").val() == "4")||($("#modoVistaFormulario").val() == "5")||($("#modoVistaFormulario").val() == "9"))) {
            console.log("condiciones firmas 2");
            $(".terminarCotizacion").attr('disabled',false);
            $(".terminarCotizacion12").attr('disabled',false);
        }

        if (($("#modoVistaFormulario").val() == "1")&&($("#origen").val() == "PresupuestoAdicional")) {
            console.log("condiciones firmas 3");
            $(".terminarCotizacion").attr('disabled',false);
        }
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
        var destino = $('#destinoFirma').val();
        // console.log(destino);

        if (destino == "Asesor") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaAsesor').val("");
            $("#firmaFirmaAsesor").attr("src","");
        }else if (destino == "Tecnico") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaTecnico').val("");
            $("#firmaFirmaConsumidor").attr("src","");
        }else if (destino == "JefeTaller") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaJefeTaller').val("");
            $("#firmaFirmaJefeTaller").attr("src","");
        }else if (destino == "AsesorCostos") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaAsesorCostos').val("");
            $("#firmaAsesorCostos").attr("src","");
        }else if (destino == "Garantias") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaGarantia').val("");
            $("#firmaFirmaGarantia").attr("src","");
        }else if (destino == "Coti_JDT") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaJDT').val("");
            $("#firmaFirmaJDT").attr("src","");
        }else if (destino == "Req") {
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaReq').val("");
            $("#firmaFirmaReq").attr("src","");
        }else if (destino == "Ref_prep") {
            //Designamos el destino de la firma en la vista
            $('#rutafirmaPrep').val("");
            $("#firmaPrep").attr("src","");
        }else if (destino == "Req_firma_gerente") {
            //Designamos el destino de la firma en la vista
            $('#ruta_parte_req').val("");
            $("#firma_parte_req").attr("src","");

            $("#enviar_comentario_req").attr('disabled',true);
        }else { //Cliente
            //Designamos el destino de la firma en la vista
            $('#rutaFirmaConsumidor').val("");
            $("#firmaFirmaConsumidor").attr("src","");
        }

    });

});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    //Pasamos el valor al formulario local
    $('#destinoFirma').val(firma);

    if (firma == "Asesor") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL ASESOR");
    }else if (firma == "Tecnico") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL TÉCNICO");
    }else if (firma == "JefeTaller") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL JEFE DE TALLER");
    }else if (firma == "AsesorCostos") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA ASESOR QUE APRUEBA");
    }else if (firma == "Garantias") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL ENCARGADO DE GARANTÍAS");
    }else if (firma == "Coti_JDT") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL JEFE DE TALLER");
    }else if ((firma == "Req")||(firma == "Req_firma_gerente")) {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA");
    }else if (firma == "Ref_prep") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL TÉCNICO (PREPIKING)");
    }else {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL CLIENTE");
    }

});
