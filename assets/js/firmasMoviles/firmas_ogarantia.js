$(document).ready(function() {
    //Recuperamos la medida de la pantalla
    // var alto = $( window ).height();
    // var ancho = $( window ).width();
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 499) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.50;
        console.log("Tablet");
    }else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png')
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        var renglon = $('#renglon').val();

        // console.log(destino);
        // Comprobamos el tipo de usuario que firmara
        if (destino == "Gerente") {
            $('#rutaGerente_'+renglon).val(data);
            $("#firmaImgGerente_"+renglon).attr("src",data);
        }else {
            $('#rutaJDT_'+renglon).val(data);
            $("#firmaImgJDT_"+renglon).attr("src",data);
        }

        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });
});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    var renglon = $(this).data("id");

    $('#destinoFirma').val(firma);
    $('#renglon').val(renglon);

    // Comprobamos el tipo de usuario que firmara
    if (firma == "Gerente") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Técnico.");
    }else {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("Firma del Jefe de Taller.");
    }
});
