
$(".asignar").on('click',function() {
    //Recuperamos la fila que se editara
    var renglon = $(this).data("id");
    // console.log(renglon);
    //Recuperamos los valores para mostrar ala usuario
    var id = $("#id_"+renglon).val();
    var folio = $("#folio_"+renglon).val();
    var fecha = $("#fecha_"+renglon).val();
    var vehiculo = $("#vehiculo_"+renglon).val();
    var cliente = $("#cliente_"+renglon).val();
    var asesor = $("#asesor_"+renglon).val();
    var hora = $("#hora_"+renglon).val();

    //Imprmimimos el texto en el modal
    $("#infoRegistroScita1").text("  "+folio+".");
    $("#infoRegistroScita2").text("  "+vehiculo+".");
    $("#infoRegistroScita3").text("  "+cliente+".");
    $("#infoRegistroScita4").text("  "+asesor+".");
    $("#infoRegistroScita5").text("  "+fecha+".");
    $("#idRegistro").val(id);
    $("#folioTem").val(folio);
    $("#horaRegistro").val(hora);
    $("#fechReg").val(fecha);

});

$('#busqueda_tabla').keyup(function () {
    toSearch();
});


//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}

$(".comentario").on('click',function() {
    //Recuperamos la fila que se editara
    var renglon = $(this).data("id");
    console.log(renglon);
    //Recuperamos los valores para mostrar ala usuario
    var folio = $("#orden_"+renglon).val();
    var comentario = $("#comentario_"+renglon).val();
    var sistema = $("#sistema_"+renglon).val();
    var componante = $("#componente_"+renglon).val();
    var causaRaiz = $("#causaRaiz2_"+renglon).val();
    var cliente = $("#cliente_"+renglon).val();
    var asesor = $("#asesor_"+renglon).val();

    //Imprmimimos el texto en el modal
    $("#infoRegistroScita1").text("  "+folio+".");
    $("#infoRegistroScita2").text("  "+((comentario == "") ? "Sin comentarios" : comentario)+".");
    $("#infoRegistroScita3").text("  "+cliente+".");
    $("#infoRegistroScita4").text("  "+asesor+".");
    $("#idOrden").val(folio);
    $("#comentario").val(comentario);
    $("#sistema").val(sistema);
    $("#componente").val(componante);
    $("#causa").val(causaRaiz);

});

//Cuando se vacie el input, se carga nuevamente toda la tabla
function envioComentario(){
    console.log("click");
    //Recuperamos el comentario a guardar
    var idOrden = $("#idOrden").val();
    var comentario = $("#comentario").val();
    var sistema = $("#sistema").val();
    var componente = $("#componente").val();
    var causa = $("#causa").val();

    $("#errorIdCitaComentarioVC").css("color","blue");
    $("#errorIdCitaComentarioVC").text("Enviando comentario");

    $("#idOrden").attr("disabled",true);
    $("#comentario").attr("disabled",true);
    $("#sistema").attr("disabled",true);
    $("#componente").attr("disabled",true);
    $("#causa").attr("disabled",true);

    //Verificamos que no sea un campo vacio
    if ((comentario != "")&&(idOrden != "")) {
        //Si el filtro quedo vacio, jalar toda la tabla
        var url = $("#basePeticion").val();

        $.ajax({
            url: url+"vozCliente/BuscadorVC/saveComment",
            method: 'post',
            data: {
                idOrden: idOrden,
                comentario: comentario,
                sistema : sistema,
                componente : componente,
                causa : causa,
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    if (resp == "OK") {
                        $("#errorIdCitaComentarioVC").css("color","green");
                        $("#errorIdCitaComentarioVC").text("Comentario Guardador");

                        $("#idOrden").attr("disabled",false);
                        $("#comentario").attr("disabled",false);
                        $("#sistema").attr("disabled",false);
                        $("#componente").attr("disabled",false);
                        $("#causa").attr("disabled",false);
                        
                        $("#comentario").val("");
                        $("#sistema").val("");
                        $("#componente").val("");
                        $("#causa").val("");
                    }else {
                        $("#errorIdCitaComentarioVC").css("color","red");
                        $("#errorIdCitaComentarioVC").text("Ocurrio un error al intentar guardar");
                        $("#idOrden").attr("disabled",true);
                        $("#comentario").attr("disabled",true);
                        $("#sistema").attr("disabled",true);
                        $("#componente").attr("disabled",true);
                        $("#causa").attr("disabled",true);

                    }
                }else {
                    $("#errorIdCitaComentarioVC").css("color","red");
                    $("#errorIdCitaComentarioVC").text("Ocurrio un error al intentar guardar");
                    $("#idOrden").attr("disabled",true);
                    $("#comentario").attr("disabled",true);
                    $("#sistema").attr("disabled",true);
                    $("#componente").attr("disabled",true);
                    $("#causa").attr("disabled",true);
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else{
        console.log("ERROR DE CAMPOS");
        $("#errorIdCitaComentarioVC").css("color","RED");
        $("#errorIdCitaComentarioVC").text("Faltan campos del envio");
        $("#idOrden").attr("disabled",true);
        $("#comentario").attr("disabled",true);
        $("#sistema").attr("disabled",true);
        $("#componente").attr("disabled",true);
        $("#causa").attr("disabled",true);
    }

}

function actualiza(){
    window.location.reload();
}
