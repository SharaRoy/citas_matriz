$(document).ready(function() {
	if ($("input[name='cargaFormulario']").val() == "1") {
		$("input").attr("disabled", true);
	    $("textarea").attr("disabled", true);
	    //$(".cuadroFirma").attr("disabled", true);
	    //$(".cuadroFirma").attr("data-toggle", false);
	    $('a').attr('disabled',true);

	    $(".opcion_audio").attr("disabled", false);
	}else{
		liberar();
	}
});

$(".opcion_audio").change(function(){
    //desbloqueamos el boton de inicio
    $("#iniciar").attr("disabled", false);
});

var mediaRecorder;
var audioChunks;
var actionButton = document.getElementById('iniciar');

var recipiente = document.getElementById('blockAudio');
var audioBlob;
var audioUrl;
var audio;


$("#iniciar").on('click',function(){ 
	//Recuperamos la opcion marcada por el cliente
	var acepta_audio = $('input:radio[name=acepta_audio]:checked').val();
	console.log("audio permiso : "+acepta_audio);
	// var getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
	navigator.mediaDevices.getUserMedia({ audio: true })
	.then(stream => {
		if (acepta_audio == "0") {
			$("#textAudio").text("El cliente NO acepto la grabación del formulario, favor de recargar la página y quitar permisos al micrófono");
		}else{
			mediaRecorder = new MediaRecorder(stream);
			mediaRecorder.start();

			audioChunks = [];

			mediaRecorder.addEventListener("dataavailable", event => {
				audioChunks.push(event.data);
			});

			actionButton.disabled = true;
			liberar();
			$("#enviar").prop('disabled','disabled');
		}
		
	})
	// .then(() => new Promise(resolve => mediaRecorder.onloadedmetadata = resolve))
	.catch(e => {
			console.log(e);
			if (acepta_audio == "1") {
				$("#textAudio").text("El cliente acepto la grabación del formulario, favor de recargar la página y dar permisos al micrófono");
			} else {
				$("#textAudio").text("No se puede grabar el audio, proceder con el formulario nomalmente");
				$("#enviar").prop('disabled',false);
				$("#terminar").css('display','none');

				actionButton.disabled = true;
				liberar();
				$("#enviar").prop('disabled','disabled');
			}
				
		}
	);
});

function liberar(){
    $('input').prop('disabled',false);
    $('textarea').prop('disabled',false);
    $('a').attr('disabled',false);
    $('.centradoBarra').slider('enable');

    //$(".cuadroFirma").attr("disabled", false);
    //$(".cuadroFirma").attr("data-toggle", "modal");

    $("#diagramaFallas").css("display","block");
    $("#diagramaTemp").css("display","none");
}

$("#terminar").on('click',function(){
	mediaRecorder.stop();

	mediaRecorder.addEventListener("stop", () => {
		audioBlob = new Blob(audioChunks, { 'type' : 'audio/ogg; codecs=opus' });
		audioUrl = URL.createObjectURL(audioBlob);
		audio = new Audio(audioUrl);
		// audio.play();

		input(audioBlob);
	});

	$("#enviar").prop('disabled',false);
});


function input(audioBlob){
	var reader = new FileReader();
	reader.readAsDataURL(audioBlob);
	reader.onloadend = function() {
		base64data = reader.result;
		console.log(base64data.length);
		fraccion(base64data);
	}
}

function fraccion(base64data){
    var limite = base64data.length;
	var partes = limite/1000000;

	var inputs = partes.toFixed(0);
	console.log(inputs);

	var indiceIncio = 0;
	var indiceFin = 1000000
    var contador = 1;

	if (inputs>0) {
		for (var i = 1; i <= inputs; i++) {
			// contenedor.append("<input type='hidden' name='blockAudio[]' value='"+base64data.substring(indiceIncio, indiceFin)+"'>")
    		$("#blockAudio_"+i).val(base64data.substring(indiceIncio, indiceFin));
			console.log(base64data.substring(indiceIncio, indiceFin).length);
			indiceIncio += 1000000;
			indiceFin += 1000000;
        	contador++;
		}
		// contenedor.append("<input type='hidden' name='blockAudio[]' value='"+base64data.substring(indiceIncio, limite)+"'>")
        $("#blockAudio_"+contador).val(base64data.substring(indiceIncio, limite));
		console.log(base64data.substring(indiceIncio, limite).length);
	}else {
		// contenedor.append("<input type='hidden' name='blockAudio[]' value='"+base64data.substring(indiceIncio, limite)+"'>")
        $("#blockAudio_1").val(base64data.substring(indiceIncio, limite));
		console.log(base64data.substring(indiceIncio, limite).length);
	}
}


//Pregcarga y revision de datos
function precarga() {
    var orden = $("#noOrden").val();
    var base = $("#basePeticion").val();

    //console.log("Precarga Datos");
    $.ajax({
		url: base+"vozCliente/VC_Diagnostico/revision_datos",
		method: 'post',
		data: {
		  orden: orden,
		},
		success:function(resp){
			//console.log("Precarga vc: "+resp);
			if (resp.indexOf("handler			</p>")<1) { 
				var campos = resp.split("=");
				//Si existe la voz cliente con formato viejo
				if (campos[0] == "OK") {
				  	location.href = base+'VCliente2_Revision/'+campos[1];

				}else if (campos[0] == "OK1") {
					if ($("input[name='cargaFormulario']").val() == "2") {
						location.href = base+'VCliente_AddDiag/'+campos[1];
					} else {
						location.href = base+'VCliente_Revision/'+campos[1];
					}

				}else{
					if ($("input[name='cargaFormulario']").val() == "2") {
						location.href = base+'VCliente_Alta/'+campos[1];
					} else {
						if (campos.length == 2) {
							$("#idPivote_error").text(campos[1]);
						}else{
							$(".nombreCliente").val(campos[1]);
						  	$(".asesorname").val(campos[2]);
						  	$("#idPivote_error").text("");
						}
					}
				}
			}
		//Cierre de success
		},
		error:function(error){
		  console.log(error);
		//Cierre del error
		}
	//Cierre del ajax
	});
}
