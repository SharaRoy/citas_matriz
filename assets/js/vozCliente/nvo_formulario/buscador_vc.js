$('#btnBusqueda_vozcliente').on('click',function(){
    console.log("Buscando por fechas");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#carga_tabla").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var campo = $("#busqueda_campo").val();    
    var base = $("#basePeticion").val();

    campo = campo.trim();

    $.ajax({
        url: base+"vozCliente/VC_Diagnostico/filtrovoz_cliente", 
        method: 'post',
        data: {
            //idCita : idCita,
            campo : campo,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    console.log(campos.length);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("=");
                        console.log(celdas);

                        var audio = "";
                        //Validamos si hay audio disponible
                        if (celdas[6] != "") {
                            audio = "<a class='AudioModal' data-value='"+celdas[6]+"' data-orden='"+celdas[0]+"' data-target='#muestraAudio' data-toggle='modal' style='color:blue;font-size:24px;'>"+
                                    "<i class='far fa-play-circle' style='font-size:24px;'></i>"+
                                "</a>";
                        } else {
                            audio = "<i class='fas fa-volume-mute' style='font-size:24px;color:red;'></i>";
                        }

                        var comentario = "";
                        if (celdas[7] == "1") {
                            comentario = "<a class='btn btn-primary' onclick='pregunta("+celdas[0]+")' style='color:white;' data-id='"+(i+1)+"' data-target='#pregunta' data-toggle='modal'>"+ 
                                    "Agregar <br> comentario"+
                                "</a>";
                        } else {
                            comentario = "<a class='btn btn-primary' onclick='pregunta_2("+celdas[0]+")' style='color:white;' data-id='"+(i+1)+"' data-target='#pregunta2' data-toggle='modal'>"+
                                    "Agregar <br> comentario"+
                                "</a>";
                        }

                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:12px; color: black; background-color: "+((celdas[6] != "") ? '#f4f4f4' : '#ffffff')+"; '>"+
                            //NO. ORDEN
                            "<td align='center' style='vertical-align: middle;'>"
                                +celdas[0]+
                                "<input type='hidden' id='orden_"+(i+1)+"' value='"+(i+1)+"'>"+
                                "<input type='hidden' id='tipo_"+(i+1)+"' value='"+celdas[7]+"'>"+
                            "</td>"+
                            //NO. INTELISIS
                            //"<td align='center' style='vertical-align: middle;'>"
                                //+celdas[8]+
                            //"</td>"+
                            //APERTURA ORDEN
                            "<td align='center' style='vertical-align: middle;'>"+celdas[4]+"</td>"+
                            //TÉCNICO
                            "<td align='center' style='vertical-align: middle;'>"+celdas[1]+"</td>"+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;'>"+celdas[2]+"</td>"+
                            //CLIENTE
                            "<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
                            //PLACAS
                            //"<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
                            //MODELO
                            //"<td align='center' style='vertical-align: middle;'>"+celdas[4]+"</td>"+
                            //DETALLE FALLA
                            "<td align='center' style='vertical-align: middle;'>"
                                +celdas[5]+
                            "</td>"+
                            //Aciones
                            //AURDIOS
                            "<td align='center' style='vertical-align: middle;'>"
                                +audio+
                            "</td>"+
                            //EDITAR
                            "<td align='center' style='vertical-align: middle;'>"
                                +comentario+
                            "</td>"+
                        "</tr>");
                    }

                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $("#carga_tabla").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");


        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $("#carga_tabla").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

function pregunta(orden) {
    var base = $("#basePeticion").val();
    $("#carga_1").css("display","inline-block");

    //console.log("Precarga Datos");
    $.ajax({
        url: base+"vozCliente/VC_Diagnostico/recuperar_informacion",
        method: 'post',
        data: {
          orden: orden,
        },
        success:function(resp){
            //console.log("voz cliente anterior: "+resp);
            if (resp.indexOf("handler           </p>")<1) { 
                var campos = resp.split("=");
                //Si existe la voz cliente con formato viejo
                if (campos.length > 0) {
                    $("#infoRegistroScita1").text("  "+orden+".");
                    $("#infoRegistroScita3").text("  "+campos[1]+".");
                    $("#infoRegistroScita4").text("  "+campos[0]+".");
                    $("#idOrden").val(orden);
                    $("#comentario").val(campos[3]);
                    $("#sistema").val(campos[4]);
                    $("#componente").val(campos[5]);
                    $("#causa").val(campos[6]);

                    $("#tituloAudio").text("Evidencia de audio No. de Orden: "+orden);
                    $("#reproductor").attr("src",base+""+campos[7]);
                }
            }
            $("#carga_1").css("display","none");
        //Cierre de success
        },
        error:function(error){
          console.log(error);
          $("#carga_1").css("display","none");
        //Cierre del error
        }
    //Cierre del ajax
    });
}

function pregunta_2(orden) {
    var base = $("#basePeticion").val();
    $("#carga_2").css("display","inline-block");

    //console.log("Precarga Datos");
    $.ajax({
        url: base+"vozCliente/VC_Diagnostico/lista_diagnosticos",
        method: 'post',
        data: {
          orden: orden,
        },
        success:function(resp){
            console.log("voz cliente diagnosticos: ");
            if (resp.indexOf("handler           </p>")<1) {
                var info = resp.split("/-/");
                
                var datos = info[0].split("=");
                $("#info_reg_1").text("  "+orden+".");
                $("#info_reg_2").text("  "+datos[0]+".");
                $("#info_reg_3").text("  "+datos[1]+".");
                $("#tituloAudio").text("Evidencia de audio No. de Orden: "+orden);
                $("#reproductor").attr("src",base+""+datos[2]);

                $("#vc_id_cita").val(orden);

                //Dividimos los registros
                var filas = info[1].split("|");
                //Vaciamos el select donde se almacenan los resultados
                var select = $("#vaciado_diag");
                select.empty();
                //select.append("<option value=''>Selecciona...</option>");

                //Verificamos que se hayan encontrado resultados
                if (filas.length > 0) {
                    var regitro = ( filas.length > 1) ? filas.length -1 : filas.length;
                    select.append("<option value=''>Seleccionar...</option>");
                    for (var i = 0; i < regitro; i++) {
                        var campos = filas[i].split("=");
                        console.log(campos);
                        if ((i == 0) && (campos.length < 1)) {
                            select.append("<option value='0'>Sin Coincidencias...</option>");
                            break;
                        }
                        select.append("<option value='"+campos[0]+"'>Diag. "+campos[1]+"</option>");
                    }
                }else {
                    select.append("<option value='0'>Sin Coincidencias...</option>");
                }
            }else {
                console.log("Error");
            }
            $("#carga_2").css("display","none");
            $("#alertaconsulta").text("");
        //Cierre de success
        },
        error:function(error){
          console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}

//Guardar comentaros voz cliente primera version
$(".comentario").on('click',function() {
    //Recuperamos la fila que se editara
    var renglon = $(this).data("id");
    var orden = $(this).data("orden");
    var base = $("#basePeticion").val();
    $("#carga_1").css("display","inline-block");

    //console.log("Precarga Datos");
    $.ajax({
        url: base+"vozCliente/VC_Diagnostico/recuperar_informacion",
        method: 'post',
        data: {
          orden: orden,
        },
        success:function(resp){
            //console.log("voz cliente anterior: "+resp);
            if (resp.indexOf("handler           </p>")<1) { 
                var campos = resp.split("=");
                //Si existe la voz cliente con formato viejo
                if (campos.length > 0) {
                    $("#infoRegistroScita1").text("  "+orden+".");
                    $("#infoRegistroScita3").text("  "+campos[1]+".");
                    $("#infoRegistroScita4").text("  "+campos[0]+".");
                    $("#idOrden").val(orden);
                    $("#comentario").val(campos[3]);
                    $("#sistema").val(campos[4]);
                    $("#componente").val(campos[5]);
                    $("#causa").val(campos[6]);
                }else{
                    if (campos.length == 2) {
                        $("#idPivote_error").text(campos[1]);
                    }else{
                        $(".nombreCliente").val(campos[1]);
                        $(".asesorname").val(campos[2]);
                        $("#idPivote_error").text("");
                    }
                }
            }
            $("#carga_1").css("display","none");
        //Cierre de success
        },
        error:function(error){
          console.log(error);
          $("#carga_1").css("display","none");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Cuando se vacie el input, se carga nuevamente toda la tabla
function envioComentario(){
    $("#carga_1").css("display","inline-block");
    //Recuperamos el comentario a guardar
    var idOrden = $("#idOrden").val();
    var comentario = $("#comentario").val();
    var sistema = $("#sistema").val();
    var componente = $("#componente").val();
    var causa = $("#causa").val();

    $("#errorIdCitaComentarioVC").css("color","blue");
    $("#errorIdCitaComentarioVC").text("Enviando comentario");

    $("#idOrden").attr("disabled",true);
    $("#comentario").attr("disabled",true);
    $("#sistema").attr("disabled",true);
    $("#componente").attr("disabled",true);
    $("#causa").attr("disabled",true);

    //Verificamos que no sea un campo vacio
    if ((comentario != "")&&(idOrden != "")) {
        //Si el filtro quedo vacio, jalar toda la tabla
        var url = $("#basePeticion").val();

        $.ajax({
            url: url+"vozCliente/BuscadorVC/saveComment",
            method: 'post',
            data: {
                idOrden: idOrden,
                comentario: comentario,
                sistema : sistema,
                componente : componente,
                causa : causa,
            },
            success:function(resp){
                if (resp.indexOf("handler           </p>")<1) {
                    if (resp == "OK") {
                        $("#errorIdCitaComentarioVC").css("color","green");
                        $("#errorIdCitaComentarioVC").text("Cambios Guardados");

                        $("#idOrden").attr("disabled",false);
                        $("#comentario").attr("disabled",false);
                        $("#sistema").attr("disabled",false);
                        $("#componente").attr("disabled",false);
                        $("#causa").attr("disabled",false);
                        
                        $("#comentario").val("");
                        $("#sistema").val("");
                        $("#componente").val("");
                        $("#causa").val("");

                    }else {
                        $("#errorIdCitaComentarioVC").css("color","red");
                        $("#errorIdCitaComentarioVC").text("Ocurrio un error al intentar guardar");
                        $("#idOrden").attr("disabled",true);
                        $("#comentario").attr("disabled",true);
                        $("#sistema").attr("disabled",true);
                        $("#componente").attr("disabled",true);
                        $("#causa").attr("disabled",true);

                    }
                }else {
                    $("#errorIdCitaComentarioVC").css("color","red");
                    $("#errorIdCitaComentarioVC").text("Ocurrio un error al intentar guardar");
                    $("#idOrden").attr("disabled",true);
                    $("#comentario").attr("disabled",true);
                    $("#sistema").attr("disabled",true);
                    $("#componente").attr("disabled",true);
                    $("#causa").attr("disabled",true);
                }

                $("#carga_1").css("display","none");
            //Cierre de success
            },
            error:function(error){
                console.log(error);
                $("#carga_1").css("none");
                $("#errorIdCitaComentarioVC").css("color","red");
                $("#errorIdCitaComentarioVC").text("Error al enviar");
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else{
        console.log("ERROR DE CAMPOS");
        $("#errorIdCitaComentarioVC").css("color","RED");
        $("#errorIdCitaComentarioVC").text("Faltan campos del envio");
        $("#idOrden").attr("disabled",true);
        $("#comentario").attr("disabled",true);
        $("#sistema").attr("disabled",true);
        $("#componente").attr("disabled",true);
        $("#causa").attr("disabled",true);
    }
}

function actualiza(){
    window.location.reload();
}

//Guardar comentarios de voz cliente formato nuevo
$(".comentario2").on('click',function() {
    //Recuperamos la fila que se editara
    var renglon = $(this).data("id");
    var orden = $(this).data("orden");
    var base = $("#basePeticion").val();
    $("#carga_2").css("display","inline-block");

    //console.log("Precarga Datos");
    $.ajax({
        url: base+"vozCliente/VC_Diagnostico/lista_diagnosticos",
        method: 'post',
        data: {
          orden: orden,
        },
        success:function(resp){
            console.log("voz cliente diagnosticos: ");
            if (resp.indexOf("handler           </p>")<1) {
                var info = resp.split("/-/");
                
                var datos = info[0].split("=");
                $("#info_reg_1").text("  "+orden+".");
                $("#info_reg_2").text("  "+datos[0]+".");
                $("#info_reg_3").text("  "+datos[1]+".");

                $("#vc_id_cita").val(orden);

                //Dividimos los registros
                var filas = info[1].split("|");
                //Vaciamos el select donde se almacenan los resultados
                var select = $("#vaciado_diag");
                select.empty();
                //select.append("<option value=''>Selecciona...</option>");

                //Verificamos que se hayan encontrado resultados
                if (filas.length > 0) {
                    var regitro = ( filas.length > 1) ? filas.length -1 : filas.length;
                    select.append("<option value=''>Seleccionar...</option>");
                    for (var i = 0; i < regitro; i++) {
                        var campos = filas[i].split("=");
                        console.log(campos);
                        if ((i == 0) && (campos.length < 1)) {
                            select.append("<option value='0'>Sin Coincidencias...</option>");
                            break;
                        }
                        select.append("<option value='"+campos[0]+"'>Diag. "+campos[1]+"</option>");
                    }
                }else {
                    select.append("<option value='0'>Sin Coincidencias...</option>");
                }
            }else {
                console.log("Error");
            }
            $("#carga_2").css("display","none");
            $("#alertaconsulta").text("");
        //Cierre de success
        },
        error:function(error){
          console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Seleccionar un item para ver informacion
$("#vaciado_diag").change(function(){
    var valor = $("#vaciado_diag").val();
    var base = $("#basePeticion").val();

    if ((valor != "")&&(valor != "0")) {
        $("#carga_2").css("display","inline-block");
        $("#alertaconsulta").text("Recuperando información");
        $.ajax({
            url: base+"vozCliente/VC_Diagnostico/recuperar_informacion_2",
            method: 'post',
            data: {
                id_registro: valor,
            },
            success:function(resp){
                 console.log(resp);
                if (resp.indexOf("handler           </p>")<1) {
                    var campos = resp.split("=");
                    //Si existe la voz cliente con formato viejo
                    if (campos.length > 0) {
                        $("#registro_diag").val(valor);
                        $("#info_reg_4").text("  "+campos[0]+".");

                        $("#sistema_2").val(campos[1]);
                        $("#componente_2").val(campos[2]);
                        $("#causa_2").val(campos[3]);
                        $("#comentario_2").val(campos[4]);

                        $("#alertaconsulta").text("");
                    }else{
                        $("#alertaconsulta").text("Error al recuperar la información");
                    }
                }

                $("#carga_2").css("display","none");
                
            //Cierre de success
            },
            error:function(error){
                console.log(error);
                $("#carga_2").css("display","none");
                $("#alertaconsulta").text("");
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

function envioComentario2() {
    var sistema = document.getElementById('sistema_2').value;
    var componente = document.getElementById('componente_2').value;
    var causa = document.getElementById('causa_2').value;
    var comentario = document.getElementById('comentario_2').value;

    //Verficamos que no se borren la informacion previa
    if ((sistema != "")&&(componente != "")&&(causa != "")) {
        $("#carga_3").css("display","inline-block");
        $("#error_formato_vc").css("color","blue");
        $("#error_formato_vc").text("Enviando comentario");

        var paqueteDeDatos = new FormData();
        paqueteDeDatos.append('registro', $('#registro_diag').prop('value'));
        paqueteDeDatos.append('id_cita', $('#vc_id_cita').prop('value'));
        paqueteDeDatos.append('sistema', document.getElementById('sistema_2').value);
        paqueteDeDatos.append('componente', document.getElementById('componente_2').value);
        paqueteDeDatos.append('causa', document.getElementById('causa_2').value);
        paqueteDeDatos.append('comentario', document.getElementById('comentario_2').value);

        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"vozCliente/VC_Diagnostico/guardar_comentario",
            type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
            contentType: false,
            data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
            processData: false,
            cache: false,
            success:function(resp){
                if (resp.indexOf("handler           </p>")<1) {
                    if (resp == "OK") {
                        $("#error_formato_vc").css("color","darkgreen");
                        $("#error_formato_vc").text("Comentario guardado con exito");

                    }else {
                        $("#error_formato_vc").css("color","red");
                        $("#error_formato_vc").text(resp);

                    }
                }else {
                    $("#error_formato_vc").css("color","red");
                    $("#error_formato_vc").text("Ocurrio un error al enviar el comentario.");
                }

                $("#carga_3").css("display","none");
            //Cierre de success
            },
            error:function(error){
                console.log(error);
                $("#carga_3").css("none");
                $("#error_formato_vc").text("Error al enviar.");
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else{
        $("#error_formato_vc").text("los campos de: [Sistema, Componetes, Causa Raíz] no pueden quedar vacios");
    }
        
}