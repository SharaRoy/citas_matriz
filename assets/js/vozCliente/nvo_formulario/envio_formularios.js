//Multipunto señanlamiento de parte inferior
var lienzo = document.getElementById('diagramaFallas');
var contexto = lienzo.getContext("2d");
var background = new Image();


if ($("#fallasImg").val() == "") {
    background.src = $("#direccionFondo").val();
} else {
    background.src = $("#fallasImg").val();
}

background.onload = function(){
    contexto.drawImage(background,0,0,280,390);
}

 //Presionamos el boton para resetear la imagen (diagrama)
$("#resetoeDiagrama").on('click',function(){
    contexto.clearRect(0, 0, lienzo.width, lienzo.height);
    background.src = $("#direccionFondo").val();

    background.onload = function(){
        contexto.drawImage(background,0,0,280,390);
    }
});

$("#diagramaFallas").click(function(e){
    getPosition(e);
});

// var pointSize = 3;
function getPosition(event){
    var rect = lienzo.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    drawCoordinates(x,y);
}

function drawCoordinates(x,y){
    contexto.fillStyle = "#43a4bf";
    contexto.beginPath();
    contexto.arc(x, y, 8, 0, Math.PI * 2, true);
    contexto.fill();
}

//Se envia el formulario del diagnostico (sin audio y )
$("#diagnostico_add").on('click', function (e){
    //console.log("Se envia formulario");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#carga_1").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#formulario_diag").css("background-color","#ddd");

    var id_cita = $("#noOrden").val();
    $("#id_cita").val(id_cita);

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar_diag();
    if(validaciones){
        var paqueteDatos = new FormData(document.getElementById('formulario_diag'));
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"vozCliente/VC_Diagnostico/add_diagnostico",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("=");
                    if (respuesta[0] == "OK") {
                        //Si se guardo el formulario correctamente, reseteamos el formulario y debloqueamos el guardado de firmas
                        document.getElementById("formulario_diag").reset();
                        $("#noOrden").val(id_cita);
                        $("#enviar").prop('hidden',false);

                        var oprcion_audio = $('input:radio[name=acepta_audio]:checked').val()
                        if (oprcion_audio == "0") {
                            $("#enviar").prop('disabled',false);
                        } 

                        $("#formulario_error_diag").css("color","darkgreen");
                        $("#formulario_error_diag").text("Diagnósticos registrados para este número de orden: "+respuesta[1]);

                        resetear_indicadores();
                    } else {
                        $("#formulario_error_diag").css("color","darkred");
                        $("#formulario_error_diag").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error_diag").css("color","darkred");
                    $("#formulario_error_diag").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                }
                $("#carga_1").css("display","none");
                $("#formulario_diag").css("background-color","white");
            //Cierre de success
            },error:function(error){
                console.log(error);
                $("#carga_1").css("display","none");
                $("#formulario_diag").css("background-color","white");
                $("#formulario_error_diag").css("color","darkred");
                $("#formulario_error_diag").text("ERROR AL ENVIAR EL FORMULARIO DEL DIAGNÓSTICO");
            }
        });
    }else{
        $("#carga_1").css("display","none");
        $("#formulario_diag").css("background-color","white");
        $("#formulario_error_diag").css("color","darkred");
        $("#formulario_error_diag").text("FALTAN CAMPOS POR COMPLETAR");
    }
});


//Se envia el formulario del diagnostico (sin audio y )
$("#diagnostico").on('click', function (e){
    //console.log("Se envia formulario");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#carga_1").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#formulario_3").css("background-color","#ddd");

    var id_cita = $("#noOrden").val();
    $("#id_cita").val(id_cita);

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar_diag();
    if(validaciones){
    	var paqueteDatos = new FormData(document.getElementById('formulario_3'));
    	var base = $("#basePeticion").val();

        $.ajax({
            url: base+"vozCliente/VC_Diagnostico/guardar_formulario_diagnostico",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("=");
                    if (respuesta[0] == "OK") {
                        //Si se guardo el formulario correctamente, reseteamos el formulario y debloqueamos el guardado de firmas
                        document.getElementById("formulario_3").reset();
                        $("#noOrden").val(id_cita);
                        $("#enviar").prop('hidden',false);

                        var oprcion_audio = $('input:radio[name=acepta_audio]:checked').val()
                        if (oprcion_audio == "0") {
                            $("#enviar").prop('disabled',false);
                        } 

                        $("#formulario_error_diag").css("color","darkgreen");
                        $("#formulario_error_diag").text("Diagnósticos registrados para este número de orden: "+respuesta[1]);

                        resetear_indicadores();
                    } else {
                        $("#formulario_error_diag").css("color","darkred");
                        $("#formulario_error_diag").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error_diag").css("color","darkred");
                    $("#formulario_error_diag").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                }
                $("#carga_1").css("display","none");
                $("#formulario_3").css("background-color","white");
            //Cierre de success
            },error:function(error){
                console.log(error);
                $("#carga_1").css("display","none");
                $("#formulario_3").css("background-color","white");
                $("#formulario_error_diag").css("color","darkred");
                $("#formulario_error_diag").text("ERROR AL ENVIAR EL FORMULARIO DEL DIAGNÓSTICO");
            }
        });
    }else{
        $("#carga_1").css("display","none");
        $("#formulario_3").css("background-color","white");
        $("#formulario_error_diag").css("color","darkred");
        $("#formulario_error_diag").text("FALTAN CAMPOS POR COMPLETAR");
    }
});

function validar_diag() {
    var retorno = true;

    var dataURL = lienzo.toDataURL('image/png')
    $("#fallasImg").val(dataURL);

    var campo = $("input[name='idPivote']").val();
    if (campo == "") {
        $("#idPivote_error").text("Campo requerido");
        retorno = false;
    }else{
        $("#idPivote_error").text("");
    }

    //Valores de la falla
    var campo_1 = $("textarea[name='falla']").val();
    if (campo_1 == "") {
        $("#falla_error").text("Campo requerido");
        retorno = false;
    }else{
        $("#falla_error").text("");
    }

    var check_sentidos = $(".sentidos:checked").length; 
    if (check_sentidos == 0) {
        $("#sentidosFalla_error").text("Falta indicar una opcion de indicador de sentidos");
        retorno = false;
    }else{
        $("#sentidosFalla_error").text("");
    }

    var check_presenta = $(".presenta:checked").length; 
    if (check_presenta == 0) {
        $("#presentaFalla_error").text("Falta indicar donde se presenta la falla");
        retorno = false;
    }else{
        $("#presentaFalla_error").text("");
    }

    var check_percibe = $(".percibe:checked").length; 
    if (check_percibe == 0) {
        $("#percibeFalla_error").text("Falta indicar donde se percibe la falla");
        retorno = false;
    }else{
        $("#percibeFalla_error").text("");
    }

    //Indicadores de condiciones
    if ($("input[name='temperatura_check']").is(':checked')) {
        $("#temperatura_error").text("");
    }else{
        var cond_1 = $("input[name='tempAmbiente']").val();
        if (cond_1 == "") {
            $("#temperatura_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#temperatura_error").text("");
        }
    }

    if ($("input[name='humedad_check']").is(':checked')) {
        $("#humedad_error").text("");
    }else{
        var cond_2 = $("input[name='humedad']").val();
        if (cond_2 == "") {
            $("#humedad_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#humedad_error").text("");
        }
    }

    if ($("input[name='viento_check']").is(':checked')) {
        $("#viento_error").text("");
    }else{
        var cond_3 = $("input[name='viento']").val();
        if (cond_3 == "") {
            $("#viento_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#viento_error").text("");
        }
    }

    if ($("input[name='frenado_check']").is(':checked')) {
        $("#frenado_error").text("");
    }else{
        var cond_4 = $("input[name='frenoMedidor']").val();
        if (cond_4 == "") {
            $("#frenado_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#frenado_error").text("");
        }
    }

    if ($("input[name='transmision_1_check']").is(':checked')) {
        $("#transmision_error").text("");
    }else{
        var cond_5 = $("input[name='cambio']").val();
        var cond_5a = $("input[name='cambio_A']").val();
        if ((cond_5 == "")&&(cond_5a == "")) {
            $("#transmision_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#transmision_error").text("");
        }
    }

    if ($("input[name='rpm_check']").is(':checked')) {
        $("#rpm_error").text("");
    }else{
        var cond_6 = $("input[name='rpm']").val();
        if (cond_6 == "") {
            $("#rpm_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#rpm_error").text("");
        }
    }

    if ($("input[name='carga_check']").is(':checked')) {
        $("#carga_error").text("");
    }else{
        var cond_7 = $("input[name='carga']").val();
        if (cond_7 == "") {
            $("#carga_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#carga_error").text("");
        }
    }

    if ($("input[name='pasajeros_check']").is(':checked')) {
        $("#pasajeros_error").text("");
    }else{
        var cond_8 = $("input[name='pasajeros']").val();
        var cond_8a = $("input[name='cajuela_2']").val();
        if ((cond_8 == "")&&(cond_8a == "")) {
            $("#pasajeros_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#pasajeros_error").text("");
        }
    }

    if ($("input[name='estructura_check']").is(':checked')) {
        $("#estructura_error").text("");
    }else{
        var cond_9 = $("input[name='estructura']").val();
        if (cond_9 == "") {
            $("#estructura_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#estructura_error").text("");
        }
    }

    if ($("input[name='camino_check']").is(':checked')) {
        $("#camino_error").text("");
    }else{
        var cond_10 = $("input[name='camino']").val();
        if (cond_10 == "") {
            $("#camino_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#camino_error").text("");
        }
    }

    if ($("input[name='pendiente_check']").is(':checked')) {
        $("#pendiente_error").text("");
    }else{
        var cond_11 = $("input[name='pendiente']").val();
        if (cond_11 == "") {
            $("#pendiente_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#pendiente_error").text("");
        }
    }

    if ($("input[name='superficie_check']").is(':checked')) {
        $("#superficie_error").text("");
    }else{
        var cond_12 = $("input[name='superficie']").val();
        if (cond_12 == "") {
            $("#superficie_error").text("Campo requerido");
            retorno = false;
        }else{
            $("#superficie_error").text("");
        }
    }

    //Dercripciones del diagnostico
    var campo_2 = $("textarea[name='sistema']").val();
    if (campo_2 == "") {
        $("#sistema_error").text("Campo requerido");
        retorno = false;
    }else{
        $("#sistema_error").text("");
    }

    var campo_3 = $("textarea[name='componente']").val();
    if (campo_3 == "") {
        $("#componente_error").text("Campo requerido");
        retorno = false;
    }else{
        $("#componente_error").text("");
    }

    var campo_4 = $("textarea[name='causa']").val();
    if (campo_4 == "") {
        $("#causa_error").text("Campo requerido");
        retorno = false;
    }else{
        $("#causa_error").text("");
    }

    return retorno;
}

function resetear_indicadores() {
    contexto.clearRect(0, 0, lienzo.width, lienzo.height);
    background.src = $("#direccionFondo").val();

    background.onload = function(){
        contexto.drawImage(background,0,0,280,390);
    }

    //Reseteamos los slider
    //Condiciones ambientales
    $("#medidor_1").slider({range: "min",animate: true,min: -10,max: 60,value: 0});
    $("#medidor_2").slider({range: "min",animate: true,min: 1,max: 100,value: 1});
    $("#medidor_3").slider({range: "min",animate: true,min: 1,max: 120,value: 1});

    //Condiciones operativas
    $("#medidor_4").slider({range: "min",animate: true,min: 0,max: 260,value: 1});
    $("#medidor_5").slider({range: "min",animate: true,min: 1,max: 70,value: 1});
    $("#medidor_5_A").slider({range: "min",animate: true,min: 1,max: 100,value: 1});
    $("#medidor_6").slider({range: "min",animate: true,min: 0,max: 80,value: 1});
    $("#medidor_7").slider({range: "min",animate: true,min: 1,max: 100,value: 1});
    $("#medidor_8").slider({range: "min",animate: true,min: 1,max: 90,value: 1});
    $("#medidor_8_A").slider({range: "min",animate: true,min: 20,max: 100,value: 25});

    //Condiciones de camino
    $("#medidor_9").slider({range: "min",animate: true,min: 0,max: 100,value: 1});
    $("#medidor_10").slider({range: "min",animate: true,min: 0,max: 80,value: 1});
    $("#medidor_11").slider({range: "min",animate: true,min: 0,max: 80,value: 1});
    $("#medidor_12").slider({range: "min",animate: true,min: 1,max: 120,value: 1});
}

//Se envia las firmas y el diagnostico
$("#enviar").on('click', function (e){
    //console.log("Se envia formulario");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#carga_2").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $("#formulario").css("background-color","#ddd");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var validaciones = validar();
    if(validaciones){
        var paqueteDatos = new FormData(document.getElementById('formulario'));
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"vozCliente/VC_Diagnostico/guardar_formulario",
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                //console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("=");
                    if (respuesta[0] == "OK") {
                        formulario_p2();
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                }
                $("#carga_2").css("display","none");
                $("#formulario").css("background-color","white");
            //Cierre de success
            },error:function(error){
                console.log(error);
                $("#carga_2").css("display","none");
                $("#formulario").css("background-color","white");
                $("#formulario_error").text("ERROR AL ENVIAR EL FORMULARIO CON LAS FIRMAS");
            }
        });
    }else{
        $("#carga_2").css("display","none");
        $("#formulario").css("background-color","white");
        $("#formulario_error").text("FALTAN CAMPOS POR COMPLETAR");
    }
});

function formulario_p2() {
    var paqueteDatos = new FormData(document.getElementById('formulario_2'));
    paqueteDatos.append('id_cita', $('#noOrden').prop('value'));
    paqueteDatos.append('desicion_cliente', $('input:radio[name=acepta_audio]:checked').val());

    $("#carga_2").css("display","inline-block");
    var base = $("#basePeticion").val();

    $.ajax({
        url: base+"vozCliente/VC_Diagnostico/guardar_formulario_p2",
        type: 'post',
        contentType: false,
        data: paqueteDatos,
        processData: false,
        cache: false,
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                var respuesta = resp.split("=");
                if (respuesta[0] == "OK") {
                    location.href = base+respuesta[1]+"/4";
                } else if(respuesta[0] == ""){
                    $("#formulario_error").text("Enviando ....");
                    $("#carga_2").css("display","inline-block");
                }else {
                    $("#formulario_error").text(respuesta[0]);
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    //$("#carga_2").css("display","none");
                }
            }else{
                $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
            }
        //Cierre de success
        },
          error:function(error){
              console.log(error);
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $("#carga_2").css("display","none");
          }
    });
}

function validar() {
    var retorno = true;

    var campo_1 = $("input[name='id_cita']").val();
    if (campo_1 == "") {
        $("#idPivote_error").text("Campo requerido");
        retorno = false;
    }else{
        $("#idPivote_error").text("");
    }

    var campo_13 = $("input[name='rutaFirmaAsesor']").val();
    if (campo_13 == "") {
        $("#rutaFirmaAsesor_error").text("Firma requerida");
        retorno = false;
    }else{
        $("#rutaFirmaAsesor_error").text("");
    }

    var campo_14 = $("input[name='rutaFirmaAcepta']").val();
    if (campo_14 == "") {
        $("#rutaFirmaAcepta_error").text("Firma requerida");
        retorno = false;
    }else{
        $("#rutaFirmaAcepta_error").text("");
    }

    var campo_15 = $("input[name='nombreAsesor']").val();
    if (campo_15 == "") {
        $("#nombreAsesor_error").text("Campo requerido");
        retorno = false;
    }else{
        $("#nombreAsesor_error").text("");
    }

    var campo_15 = $("input[name='consumidor1Nombre']").val();
    if (campo_15 == "") {
        $("#consumidor1Nombre_error").text("Campo requerido");
        retorno = false;
    }else{
        $("#consumidor1Nombre_error").text("");
    }

    return retorno;
}
