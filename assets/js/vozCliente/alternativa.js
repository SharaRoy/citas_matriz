// ------------------------------------------------------------------------------
//  -----------------------------------------------------------------------------
$("#envio_diag").on('click', function (e){
    $("#cargaIcono").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault();

    var validaciones = validar();

    if(validaciones){
        $('input').attr('disabled',false);
        $('textarea').attr('disabled',false);
        
        var paqueteDatos = new FormData(document.getElementById('formulario_cliente'));
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"vozCliente/VozCliente/guardar_firma", 
            type: 'post',
            contentType: false,
            data: paqueteDatos,
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var respuesta = resp.split("=");
                    if (respuesta[0] == "OK") {
                        location.reload();
                    } else {
                        $("#formulario_error").text(respuesta[0]);
                    }

                }else{
                    $("#formulario_error").text("SE PRODUJO UN ERROR AL ENVIAR LA INFORMACIÓN");
                    //$("#formulario_error").text(resp);
                }
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#cargaIcono").css("display","none");
                $("#formulario_error").text("SE PRODUJO UN ERROR EN EL ENVIO");
                //location.href = base+"index.php/inventario/index/"+orden;
            }
        });
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $("#cargaIcono").css("display","none");
        $("#formulario_error").text("FALTAN CAMPOS POR COMPLETAR");
    }
});

function validar() {
    var retorno = true;

    var firma_2 = $("input[name='rutaFirmaAcepta']").val();
    if (firma_2 == "") {
        var firma_2 = $("#error_firma_2");
        firma_2.empty();
        firma_2.append('<label class="form-text text-danger">Campo requerido</label>');
        retorno = false;
    }else{
        var firma_2 = $("#error_firma_2");
        firma_2.empty();
    }

    return retorno;
}


// ------------------------------------------------------------------------------
//  -----------------------------------------------------------------------------
//Comenzando el calculo de valores
function cantidad() {

    var indice = $("#indiceProv").val();

    //Evitamos que deje el campo en blanco
    if ($("#valorDefaultB_"+indice).val() == "") {
        $("#valorDefaultB_"+indice).val(1);
    }

    var cantidad = parseInt($("#valorDefaultB_"+indice).val());
    var costoUni = parseFloat($("#valorDefaultC_"+indice).val());
    var refaccion = parseFloat($("#valorDefaultD_"+indice).val());
    var manoObra = parseFloat($("#valorDefaultE_"+indice).val());

    var subtotal_1 = cantidad*costoUni;
    var subtotal_2 = subtotal_1+refaccion+manoObra;

    //Vaciamos los valores finales
    $("#subtotalDD_"+indice).text("$ "+subtotal_2.toFixed(2));
    $("#inputSubTotal_"+indice).val(subtotal_2);

}

function costoSuma() {
    var indice = $("#indiceProv").val();

    //Evitamos que deje el campo en blanco
    if ($("#valorDefaultC_"+indice).val() == "") {
        $("#valorDefaultC_"+indice).val(0);
    }

    var cantidad = parseInt($("#valorDefaultB_"+indice).val());
    var costoUni = parseFloat($("#valorDefaultC_"+indice).val());
    var refaccion = parseFloat($("#valorDefaultD_"+indice).val());
    var manoObra = parseFloat($("#valorDefaultE_"+indice).val());

    var subtotal_1 = cantidad*costoUni;
    var subtotal_2 = subtotal_1+refaccion+manoObra;

    //Vaciamos los valores finales
    $("#subtotalDD_"+indice).text("$ "+subtotal_2.toFixed(2));
    $("#inputSubTotal_"+indice).val(subtotal_2);

}

function costoRefacc() {
    var indice = $("#indiceProv").val();

    //Evitamos que deje el campo en blanco
    if ($("#valorDefaultD_"+indice).val() == "") {
        $("#valorDefaultD_"+indice).val(0);
    }

    var cantidad = parseInt($("#valorDefaultB_"+indice).val());
    var costoUni = parseFloat($("#valorDefaultC_"+indice).val());
    var refaccion = parseFloat($("#valorDefaultD_"+indice).val());
    var manoObra = parseFloat($("#valorDefaultE_"+indice).val());

    var subtotal_1 = cantidad*costoUni;
    var subtotal_2 = subtotal_1+refaccion+manoObra;

    //Vaciamos los valores finales
    $("#subtotalDD_"+indice).text("$ "+subtotal_2.toFixed(2));
    $("#inputSubTotal_"+indice).val(subtotal_2);

}

function costoObra() {
    var indice = $("#indiceProv").val();

    //Evitamos que deje el campo en blanco
    if ($("#valorDefaultE_"+indice).val() == "") {
        $("#valorDefaultE_"+indice).val(0);
    }

    var cantidad = parseInt($("#valorDefaultB_"+indice).val());
    var costoUni = parseFloat($("#valorDefaultC_"+indice).val());
    var refaccion = parseFloat($("#valorDefaultD_"+indice).val());
    var manoObra = parseFloat($("#valorDefaultE_"+indice).val());

    var subtotal_1 = cantidad*costoUni;
    var subtotal_2 = subtotal_1+refaccion+manoObra;

    //Vaciamos los valores finales
    $("#subtotalDD_"+indice).text("$ "+subtotal_2.toFixed(2));
    $("#inputSubTotal_"+indice).val(subtotal_2);

}
