$(document).ready(function() {
    //Sección de variables para audio
    var mic;
    var recorder;
    var soundFile;
    // presionar el ratón cambiará el estado de grabar, a parar y a reproducir
    var state = 0;

    var tabla = $("#trabajoLista");
    var tabla_avisos = $("#avisos_tabla");
    var indice = 1;
    var indiceAvisos = 1;

    if ($("#respuesta").val() == "") {
        var estadoFormulario = false;
        $('input').prop('disabled','disabled');
        $('textarea').prop('disabled','disabled');
        $('a').attr('disabled','disabled');
        $('.centradoBarra').slider('disable');
    }

    //Tabla de trabajos realizados
    $("#agregar_btn").on('click',function(){
      //Recuperamos los campos de la fila default
      var campo_1 = $("#valorDefaultA_"+indice).val();
      var campo_2 = parseFloat($("#valorDefaultB_"+indice).val());
      var campo_3 = parseFloat($("#valorDefaultC_"+indice).val());
      var campo_4 = parseFloat($("#valorDefaultD_"+indice).val());
      var campo_5 = parseFloat($("#valorDefaultE_"+indice).val());
      var campo_6 = parseFloat($("#inputSubTotal_"+indice).val());

      //Comprobamos que se hayan llenado los campos
      if ((campo_1 != "")&&(campo_2 != 0)&&(campo_3 != 0)){
            //Evitamos la edicion de los  elementos
            $("#valorDefaultA_"+indice).prop('disabled','disabled');
            $("#valorDefaultB_"+indice).prop('disabled','disabled');
            $("#valorDefaultC_"+indice).prop('disabled','disabled');
            $("#valorDefaultD_"+indice).prop('disabled','disabled');
            $("#valorDefaultE_"+indice).prop('disabled','disabled');

            //Mostramos el boton para eliminar fila
            $("#eliminar_btn").css('display','block');

            //Descripcion / cantidad / costo unitario / refaccion / mano de obra / subtotal
            $("#valoresDD_"+indice).val(""+campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6);

            //Actualizamos los totales finales
            var pre_subTotal = parseFloat($("#subTotalGralImput").val());
            var subTotal = pre_subTotal + campo_6;
            var iva = subTotal * 0.16;
            var total = subTotal + iva;

            //Imprimimos los valores
            $("#subTotalGralImput").val(subTotal);
            $("#subTotalGral").text("$ "+subTotal.toFixed(2));
            $("#ivaGralImput").val(iva);
            $("#ivaGral").text("$ "+iva.toFixed(2));
            $("#totalGralImput").val(total);
            $("#totalGral").text("$ "+total.toFixed(2));

            //Aumentamos el indice para los renglones
            indice++;
            $("#indiceProv").val(indice);

            //Creamos el elemento donde se contendran todos los valores a enviar
            tabla.append("<tr id='fila_trabajo_"+indice+"'>"+
                "<td>"+
                    "<input type='text' class='input_field' id='valorDefaultA_"+indice+"' style='width:100%;text-align:left;'>"+
                "</td>"+
                "<td>"+
                    "<input type='number' class='input_field' min='1' value='1' onblur='cantidad()' id='valorDefaultB_"+indice+"' style='width:100%;'>"+
                "</td>"+
                "<td>"+
                    "<input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' min='0' value='0' onblur='costoSuma()' id='valorDefaultC_"+indice+"' style='width:100%;'>"+
                "</td>"+
                "<td>"+
                    "<input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' min='0' value='0' onblur='costoRefacc()' id='valorDefaultD_"+indice+"' style='width:100%;'>"+
                "</td>"+
                "<td>"+
                    "<input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' min='0' value='0' onblur='costoObra()' id='valorDefaultE_"+indice+"' style='width:100%;'>"+
                    "<input type='hidden' id='valoresDD_"+indice+"' name='valores[]'>"+
                "</td>"+
                "<td align='center'>"+
                    "<label id='subtotalDD_"+indice+"'></label>"+
                    "<input type='hidden' name='' id='inputSubTotal_"+indice+"' value=''>"+
                "</td>"+
            "</tr>");
        }else {
            //Regresamos el control al campo que falta rellenar
            if (campo_1 == "") {
                $("#valorDefaultA_"+indice).focus();
            }else if (campo_2 == 0) {
                $("#valorDefaultB_"+indice).focus();
            }else {
                $("#valorDefaultC_"+indice).focus();
            }

        }
    });

    //Eliminamos la ultima fila ingresada (trabajos realizados)
    $("#eliminar_btn").on('click',function(){
        if (indice > 1) {
            var indiceAnt = indice - 1;

            var campo_6 = parseFloat($("#inputSubTotal_"+indiceAnt).val());

            //Actualizamos los totales finales
            var pre_subTotal = parseFloat($("#subTotalGralImput").val());
            var subTotal = pre_subTotal - campo_6;
            var iva = subTotal * 0.16;
            var total = subTotal + iva;

            //Imprimimos los valores
            $("#subTotalGralImput").val(subTotal);
            $("#subTotalGral").text("$ "+subTotal.toFixed(2));
            $("#ivaGralImput").val(iva);
            $("#ivaGral").text("$ "+iva.toFixed(2));
            $("#totalGralImput").val(total);
            $("#totalGral").text("$ "+total.toFixed(2));

            // Impedimos que se elimine el primer renglon
            if (indice > 1) {
                //Eliminamos la ultima fila generada
                var fila = $("#fila_trabajo_"+indice);
                fila.remove();
            }


            //Aumentamos el indice para los renglones
            indice--;
            $("#indiceProv").val(indice);
            $("#valorDefaultA_"+indice).val("");
            $("#valorDefaultB_"+indice).val(1);
            $("#valorDefaultC_"+indice).val(0);
            $("#valorDefaultD_"+indice).val(0);
            $("#valorDefaultE_"+indice).val(0);
            $("#inputSubTotal_"+indice).val(0);
            $("#subtotalDD_"+indice).text("$ 0");
            $("#valoresDD_"+indice).val("");

            //Desbloqueamos el renglon para la edición
            $("#valorDefaultA_"+indice).prop('disabled',false);
            $("#valorDefaultB_"+indice).prop('disabled',false);
            $("#valorDefaultC_"+indice).prop('disabled',false);
            $("#valorDefaultD_"+indice).prop('disabled',false);
            $("#valorDefaultE_"+indice).prop('disabled',false);
        }
    });

    //Tabla de Avisos / Notificaciones / Autorizaciones
    $("#agregar").on('click',function(){
        //Recuperamos los campos de la fila default
        var campo_1 = $("#asunto_"+indiceAvisos).val();
        var campo_2 = $("#contacto_"+indiceAvisos).val();
        var campo_3 = $("#fecha_"+indiceAvisos).val();
        var campo_4 = $("#hora_"+indiceAvisos).val();
        var campo_5 = $("#presupuesto_"+indiceAvisos).val();

        //Comprobamos que se haya seleccionado un check
        // if($("input[name='autoriza_"+indiceAvisos+"']").is(':checked')){
            var campo_6 = $("input[name='autoriza_"+indiceAvisos+"']:checked").val();

            $("#predet_"+indiceAvisos).text("");
            //Comprobamos que se hayan llenado los campos
            if ((campo_1 != "")&&(campo_2 != "")&&(campo_3 != "")&&(campo_4 != "")){
                //Evitamos la edicion de los  elementos
                $("#asunto_"+indiceAvisos).prop('disabled','disabled');
                $("#contacto_"+indiceAvisos).prop('disabled','disabled');
                $("#fecha_"+indiceAvisos).prop('disabled','disabled');
                $("#hora_"+indiceAvisos).prop('disabled','disabled');
                $("#presupuesto_"+indiceAvisos).prop('disabled','disabled');
                $("input[name='autoriza_"+indiceAvisos+"']").prop('disabled','disabled');

                //Mostramos el boton para eliminar fila
                $("#eliminar").css('display','block');

                //Asunto / contacto / fecha / hora / presupuesto / autoriza
                $("#avisosValores_"+indiceAvisos).val(""+campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+campo_6);

                //Aumentamos el indice para los renglones
                indiceAvisos++;
                $("#indiceAvisos").val(indiceAvisos);

                //Recuperamos la fecha actual
                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                //Los meses de Enero - Septiembre se les agrega un "0" al inicio
                if (mm < 10) {
                    mm = "0"+mm;
                }
                //Los dias 1 -9  se les agrega un "0" al inicio
                if (dd < 10) {
                    dd = "0"+dd;
                }
                var yyyy = hoy.getFullYear();
                var hora = hoy.getHours();
                var min = hoy.getMinutes();

                //Creamos el elemento donde se contendran todos los valores a enviar
                tabla_avisos.append("<tr id='fila_avisos_"+indiceAvisos+"'>"+
                    "<td>"+
                        "<input type='text' class='input_field' id='asunto_"+indiceAvisos+"' style='width:100%;text-align:left;'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' class='input_field' id='contacto_"+indiceAvisos+"' style='width:100%;'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='date' class='input_field' min='"+yyyy+"-"+mm+"-"+dd+"' value='"+yyyy+"-"+mm+"-"+dd+"' id='fecha_"+indiceAvisos+"' style='width:100%;'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='time' class='input_field' min='"+hora+":"+min+"' value='"+hora+":"+min+"' id='hora_"+indiceAvisos+"' style='width:100%;'>"+
                    "</td>"+
                    "<td>"+
                        "<input type='text' class='input_field' id='presupuesto_"+indiceAvisos+"' style='width:100%;'>"+
                        "<input type='hidden' id='avisosValores_"+indiceAvisos+"' name='avisosValues[]'>"+
                    "</td>"+
                    "<td align='center'>"+
                        "Si  <input type='radio' value='1' name='autoriza_"+indiceAvisos+"'>"+
                        "<label class='error' id='predet_"+indiceAvisos+"'></label>"+
                    "</td>"+
                    "<td align='center'>"+
                        "No  <input type='radio' value='0' name='autoriza_"+indiceAvisos+"'>"+
                    "</td>"+
                "</tr>");
            }else {
                //Regresamos el control al campo que falta rellenar
                if (campo_1 == "") {
                    $("#asunto_"+indiceAvisos).focus();
                }else if (campo_2 == "") {
                    $("#contacto_"+indiceAvisos).focus();
                }else if (campo_4 == "") {
                    $("#fecha_"+indiceAvisos).focus();
                }else {
                  $("#hora_"+indiceAvisos).focus();
                }

            }
        // }else {
            // $("#predet_"+indiceAvisos).text("Falta seleccionar este campo");
        // }
    });

    //Eliminamos la ultima fila ingresada (trabajos realizados)
    $("#eliminar").on('click',function(){
        if (indiceAvisos > 1) {
            var fila = $("#fila_avisos_"+indiceAvisos);
            fila.remove();

            //Recuperamos la fecha actual
            var hoy = new Date();
            var dd = hoy.getDate();
            var mm = hoy.getMonth()+1;
            //Los meses de Enero - Septiembre se les agrega un "0" al inicio
            if (mm < 10) {
                mm = "0"+mm;
            }
            //Los dias 1 -9  se les agrega un "0" al inicio
            if (dd < 10) {
                dd = "0"+dd;
            }
            var yyyy = hoy.getFullYear();
            var hora = hoy.getHours();
            var min = hoy.getMinutes();

            //Decrementamos el indice para los renglones
            indiceAvisos--;
            $("#indiceAvisos").val(indiceAvisos);
            $("#asunto_"+indiceAvisos).val("");
            $("#contacto_"+indiceAvisos).val("");
            $("#fecha_"+indiceAvisos).val(yyyy+"-"+mm+"-"+dd);
            $("#hora_"+indiceAvisos).val(hora+":"+min);
            $("#presupuesto_"+indiceAvisos).val("");
            $("#avisosValores_"+indiceAvisos).val("");

            //Desbloqueamos el renglon para la edición
            $("#asunto_"+indiceAvisos).prop('disabled',false);
            $("#contacto_"+indiceAvisos).prop('disabled',false);
            $("#fecha_"+indiceAvisos).prop('disabled',false);
            $("#hora_"+indiceAvisos).prop('disabled',false);
            $("#presupuesto_"+indiceAvisos).prop('disabled',false);
            $("input[name='autoriza_"+indiceAvisos+"']").prop('disabled',false);
        }
    });


});
