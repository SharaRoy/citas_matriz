$(document).ready(function() {
    //Comprobamos quien entra al formulario y que tipo de proceso llevara
    var tipoFormulario = $("#estatusSlider").val();

    //Para formulario de alta
    //Alta registrada por un asesor
    if (tipoFormulario == "1") {
        $(".seccionTecnico").attr("disabled", true);
        //Bloqueamos las firmas
        $("#tecniconame").attr("disabled", true);
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);

        $("#Tecnico_firma").attr("disabled", true);
        $("#Tecnico_firma").attr("data-toggle", false);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        $("#Cliente_firma").attr("disabled", true);
        $("#Cliente_firma").attr("data-toggle", false);

    //Alta registrada por un tecnico
    }else if (tipoFormulario == "1A") {
        //Bloqueamos las firmas
        $("#asesorname").attr("disabled", true);
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);

        $("#Asesor_firma").attr("disabled", true);
        $("#Asesor_firma").attr("data-toggle", false);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        $("#Cliente_firma").attr("disabled", true);
        $("#Cliente_firma").attr("data-toggle", false);

    //Editar por parte del tecnico despues de que la alta fuera por el asesor
    }else if (tipoFormulario == "2") {
        //Si es edicion, bloqueamos la edicion de niveles de gasolina
        $('#slider-2').slider('disable');

        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        // $("#nombreConsumidor").attr("disabled", true);
        $("#asesorname").attr("disabled", true);

        $("#Asesor_firma").attr("disabled", true);
        $("#Asesor_firma").attr("data-toggle", false);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        // $("#Cliente_firma").attr("disabled", true);
        // $("#Cliente_firma").attr("data-toggle", false);

        if ($("#Tecnico_firma_2").val() == "1") {
            $("#guardarInspeccion").attr("disabled", true);
        }

    //Si el tecnico intenta volver a editar
    }else if (tipoFormulario == "2A") {
        console.log("Reglas edita Asesor");
        //Si es edicion, bloqueamos la edicion de niveles de gasolina
        $('#slider-2').slider('disable');

        $(".seccionTecnico").attr("disabled", true);
        // $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        // $("#multipuntoOrden").attr("disabled", false);

        $("#asesorNombre").attr("disabled", false);
        $("#Asesor_firma").attr("disabled", false);
        $("#Asesor_firma").attr("data-toggle", "modal");

        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);

        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        $("#Cliente_firma").attr("disabled", true);
        $("#Cliente_firma").attr("data-toggle", false);

        if ($("#Tecnico_firma_2").val() == "1") {
            $("#guardarInspeccion").attr("disabled", true);
        }
        $("#guardarInspeccion").attr("disabled", false);

    //Si es edicion, por parte de un asesor (va afirmar)
    }else if (tipoFormulario == "2B") {
        console.log("Reglas del tecnico");
        $('#slider-2').slider();
        $(".seccionAsesor").attr("disabled", false);
        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        // $("#nombreConsumidor").attr("disabled", true);

        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        // $("#Cliente_firma").attr("disabled", true);
        // $("#Cliente_firma").attr("data-toggle", false);

        $("#asesorNombre").attr("disabled", true);
        $("#Asesor_firma").attr("disabled", true);
        $("#Asesor_firma").attr("data-toggle", false);

        if (($("#Asesor_firma_2").val() == "1")&&($("#Tecnico_firma_2").val() == "1")) {
            $("#guardarInspeccion").attr("disabled", true);
        }

    }else if (tipoFormulario == "3A") {
        console.log("Reglas del jefe de taller");
        //Si es edicion, bloqueamos la edicion de niveles de gasolina
        $('#slider-2').slider('disable');
        $(".seccionAsesor").attr("disabled", true);
        $(".seccionTecnico").attr("disabled", true);
        $("textarea").attr("disabled", true);

        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        // $("#nombreConsumidor").attr("disabled", true);
        $("#tecniconame").attr("disabled", true);

        $("#Tecnico_firma").attr("disabled", true);
        $("#Tecnico_firma").attr("data-toggle", false);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        // $("#Cliente_firma").attr("disabled", true);
        // $("#Cliente_firma").attr("data-toggle", false);

    //Si es edicion, por parte de un jefe de servicio
    }else if (tipoFormulario == "3") {
        console.log("Reglas de admin");
        //Si es edicion, bloqueamos la edicion de niveles de gasolina
        $('#slider-2').slider('disable');
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        console.log("jefe taller");
    }

    //Verificamos que haya firmado el asesor
    if (($("#Asesor_firma_2").val() == "0") && ($("#dirige").val() == "JDT")) {
        console.log("Entro aqui");
        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        //Bloqueamos los campos
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        $("#multipuntoOrden").removeAttr("disabled");
        $("#guardarInspeccion").attr("disabled", true);
    //Verfificamos que haya firmado el tecnico
    }else if (($("#Tecnico_firma_2").val() == "0") && ($("#dirige").val() == "JDT")) {
        console.log("Entro aca");
        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        //Bloqueamos los campos
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        $("#multipuntoOrden").removeAttr("disabled");
        $("#guardarInspeccion").attr("disabled", true);
    }

    $("textarea").attr("disabled", false);

    var firma_1 = $("#Asesor_firma_2").val();
    var firma_2 = $("#Tecnico_firma_2").val();
    var firma_3 = $("#JefeTaller_firma_2").val();
    var firma_4 = $("#Cliente_firma_2").val();

    //Bloqueamos dependiedo las firmas
    if (firma_1 == "1") {
        // $('#slider-2').slider('disable');
        // $(".seccionAsesor").attr("disabled", true);
    }

    if (firma_2 == "1") {
        $('#slider-2').slider('disable');
        $(".seccionTecnico").attr("disabled", true);
        $(".seccionAsesor").attr("disabled", true);
        $("textarea").attr("disabled", true);
    }

    if (firma_3 == "1") {
        $('#slider-2').slider('disable');
        $(".seccionAsesor").attr("disabled", true);
        $(".seccionTecnico").attr("disabled", true);
        $("textarea").attr("disabled", true);
    }

    if (firma_4 == "1") {
        $('#slider-2').slider('disable');
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        // $("#multipuntoOrden").attr("disabled", false);
    }

    if ($("#dirige").val() == "ADMIN") {
        console.log("es panel");
        $('#slider-2').slider('disable');
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);

        //Bloqueamos las firmas
        $(".cuadroFirma").attr("disabled", true);
        $(".cuadroFirma").attr("data-toggle", false);
    }

    // if ($("#dirige").val() != "JDT") {
    //     $("#JefeTallerNombre").attr("disabled", true);
    //     $("#JefeTaller_firma").attr("disabled", true);
    //     $("#JefeTaller_firma").attr("data-toggle", false);
    // }else {
    //     $("#JefeTallerNombre").removeAttr("disabled");
    //     $("#JefeTaller_firma").removeAttr("disabled");
    //     $("#JefeTaller_firma").attr("data-toggle", "modal");
    // }

    //Si ya estan todas las 3 primeras firmas, desbloqueamos la firma del cliente
    if (($("#Asesor_firma_2").val() == "1")&&($("#Tecnico_firma_2").val() == "1" )&&($("#JefeTaller_firma_2").val() == "1")&&($("#Cliente_firma_2").val() == "0")) {
        //Comprobamos que sea el panel del asesor
        if ($("#dirige").val() == "ASE") {
            $("#nombreConsumidor").removeAttr("disabled");
            $("#Cliente_firma").removeAttr("disabled");
            $("#Cliente_firma").attr("data-toggle", "modal");

            $("#guardarInspeccion").css("display","block");
            $("#guardarInspeccion").removeAttr("disabled");
            $("#btnRegresar").css("display","none");
        //Si es cualquier otro panel bloqueamos la firma del cliente
        }else {
            $("#nombreConsumidor").attr("disabled", true);
            $("#Cliente_firma").attr("disabled", true);
            $("#Cliente_firma").attr("data-toggle", false);
        }

    //De lo contrario, bloqueamos la firma del cliente
    }else {
        $("#nombreConsumidor").attr("disabled", true);
        $("#Cliente_firma").attr("disabled", true);
        $("#Cliente_firma").attr("data-toggle", false);
    }

    //Comprobamos si ya existen todas las firmas (fi falta alguna de las firmas, aun se permite editar)
    if (($("#Asesor_firma_2").val() == "0")||($("#Tecnico_firma_2").val() == "0" )||($("#JefeTaller_firma_2").val() == "0")||($("#Cliente_firma_2").val() == "0")) {
        $("#superUsuarioName").removeAttr("disabled");
        $("#superUsuarioPass").removeAttr("disabled");
    //De lo contrario, bloqueamos los campos de superusuario
    } else {
        $("#superUsuarioName").attr("disabled", true);
        $("#superUsuarioPass").attr("disabled", true);
    }

    // $("#superUsuarioName").removeAttr("disabled", false);
    // $("#superUsuarioPass").removeAttr("disabled", false);


});
