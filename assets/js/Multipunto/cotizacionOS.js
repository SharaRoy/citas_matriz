var tabla = $("#cotizacionTabla");

//Tabla de control de materiales
$("#agregar").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceCoti").val();

    //Recuperamos los campos de la fila default
    var campo_1 = $("#accionesTxt_"+indice).val();
    var campo_2 = $("#descripcionTxt_"+indice).val();
    var campo_3 = $("#precioTxt_"+indice).val();
    var campo_4 = $("#cantidadTxt_"+indice).val();
    var campo_5 = $("#descuentoTxt_"+indice).val();
    // var campo_6 = $("#pIvaTxt_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ((campo_1 != "")&&(campo_2 != "")&&(campo_3 != "")&&(campo_4 != "")){
        //Evitamos la edicion de los  elementos
        $("#accionesTxt_"+indice).prop('disabled','disabled');
        $("#descripcionTxt_"+indice).prop('disabled','disabled');
        $("#precioTxt_"+indice).prop('disabled','disabled');
        $("#cantidadTxt_"+indice).prop('disabled','disabled');
        $("#descuentoTxt_"+indice).prop('disabled','disabled');
        // $("#pIvaTxt_"+indice).prop('disabled','disabled');

        $("#accionesTxt_"+indice).val(campo_1);
        $("#descripcionTxt_"+indice).val(campo_2);

        //Mostramos el boton para eliminar fila
        $("#eliminar").css('display','block');

        //Hacemos la opercaion de sintesis
        var total_fila = parseFloat($("#totalFila_siva_"+indice).val());
        var descuentoFila = parseFloat($("#descuentoTxt_"+indice).val());
        var subTotalAnterior = parseFloat($("#subtotal_orden").val());
        var subTotalNuevo = subTotalAnterior + total_fila;

        //Sacamos el iva de la cuenta

        var subTotalIva = subTotalNuevo * 0.16;
        var anticipo = parseFloat($("#anticipo_orden").val());
        if (anticipo == "") {
            anticipo = 0;
        }
        var total = subTotalNuevo + subTotalIva - anticipo - descuentoFila;

        //Imprimimos los resultados
        $("#txt_iva_orden").text("$"+subTotalIva.toFixed(2));
        $("#iva_orden").val(subTotalIva);
        $("#txt_subtotal_orden").text("$"+subTotalNuevo.toFixed(2));
        $("#subtotal_orden").val(subTotalNuevo);
        $("#txt_total_orden").text("$"+total.toFixed(2));
        $("#total_orden").val(total);

        //Acciones / Descripción / Precio / Cantidad / Descuento / total de la fila
        $("#valoresCEx_"+indice).val(""+campo_1+"_"+campo_2+"_"+campo_3+"_"+campo_4+"_"+campo_5+"_"+total_fila);

        //Aumentamos el indice para los renglones
        indice++;
        $("#indiceCoti").val(indice);

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla.append("<tr id='fila_txtCoti_"+indice+"'>"+
            "<td style='height:30px;width:15%'>"+
                "<input class='input_field modalForm' type='text' name='accionesTxt_"+indice+"' id='accionesTxt_"+indice+"' value='' style='width:70%;'>"+
            "</td>"+
            "<td style='width:20%'>"+
                "<input class='input_field modalForm' type='text' name='descripcionTxt_"+indice+"' id='descripcionTxt_"+indice+"' value='' style='width:70%;'>"+
            "</td>"+
            "<td style='width:15%'>"+
                "<input class='input_field modalForm' type='text' onkeyup='precio()' name='precioTxt_"+indice+"' id='precioTxt_"+indice+"' value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:70%;'>"+
            "</td>"+
            "<td style='width:13%'>"+
                "<input class='input_field modalForm' type='text' onkeyup='precio()' name='cantidadTxt_"+indice+"' id='cantidadTxt_"+indice+"' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:70%;'>"+
            "</td>"+
            "<td style='width:12%'>"+
                "<input class='input_field modalForm' type='text' onkeyup='precio()' name='descuentoTxt_"+indice+"' id='descuentoTxt_"+indice+"' value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:70%;'>"+
            "</td>"+
            "<td style='width:10%'>"+
                "<label id='pIvaTxt_label_"+indice+"'>$0</label>"+
                "<input class='input_field' type='hidden' name='pIvaTxt_"+indice+"' id='pIvaTxt_"+indice+"' value='0'>"+
            "</td>"+
            "<td style='width:15%'>"+
                "<label for='' id='labelTxt_"+indice+"'></label>"+
                "<input type='hidden' name='totalFila_"+indice+"' id='totalFila_"+indice+"' value=''>"+
                "<input type='hidden' name='valoresCEx[]' id='valoresCEx_"+indice+"' value=''>"+
                "<input type='hidden' name='totalFila_siva_"+indice+"' id='totalFila_siva_"+indice+"' value='0'>"+
            "</td>"+
        "</tr>");


    }else {
        //Regresamos el control al campo que falta rellenar
        if (campo_1 == "") {
            $("#accionesTxt_"+indice).focus();
        }else if (campo_2 == "") {
            $("#descripcionTxt_"+indice).focus();
        }else if (campo_3 == "") {
            $("#precioTxt_"+indice).focus();
        }else {
            $("#cantidadTxt_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada (control de materiales)
$("#eliminar").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = parseInt($("#indiceCoti").val());
    console.log(indice);

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        var fila = $("#fila_txtCoti_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceCoti").val(indice);

        //Rehacemos las operaciones de los totales
        var total_fila = parseFloat($("#totalFila_siva_"+indice).val());
        var descuentoFila = parseFloat($("#descuentoTxt_"+indice).val());
        var subTotalAnterior = parseFloat($("#subtotal_orden").val());
        var subTotalNuevo = subTotalAnterior - total_fila;

        var subTotalIva = subTotalNuevo * 0.16;
        var anticipo = parseFloat($("#anticipo_orden").val());
        if (anticipo == "") {
            anticipo = 0;
        }
        var total = subTotalNuevo + subTotalIva - anticipo - descuentoFila;

        //Imprimimos los resultados
        $("#txt_iva_orden").text("$"+subTotalIva.toFixed(2));
        $("#iva_orden").val(subTotalIva);
        $("#txt_subtotal_orden").text("$"+subTotalNuevo.toFixed(2));
        $("#subtotal_orden").val(subTotalNuevo);
        $("#txt_total_orden").text("$"+total.toFixed(2));
        $("#total_orden").val(total);

        //Restablecemos los valores default del reglon anterior
        $("#accionesTxt_"+indice).val("");
        $("#descripcionTxt_"+indice).val("");
        $("#precioTxt_"+indice).val(0);
        $("#cantidadTxt_"+indice).val(1);
        $("#descuentoTxt_"+indice).val(0);
        $("#pIvaTxt_"+indice).val(0);
        $("#valoresCEx"+indice).val("");

        $("#pIvaTxt_label_"+indice).text("$0");
        $("#pIvaTxt_"+indice).val(0);

        //total de la fila
        $("#labelTxt_"+indice).text("$0");
        $("#totalFila_"+indice).val(0);

        //Desbloqueamos el renglon para la edición
        $("#accionesTxt_"+indice).prop('disabled',false);
        $("#descripcionTxt_"+indice).prop('disabled',false);
        $("#precioTxt_"+indice).prop('disabled',false);
        $("#cantidadTxt_"+indice).prop('disabled',false);
        $("#descuentoTxt_"+indice).prop('disabled',false);

        if (indice == 1) {
            $("#anticipo_orden").val(0);
        }
    }
});
