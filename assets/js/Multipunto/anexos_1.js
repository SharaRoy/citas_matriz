//Botones para el llenado automatico de los checks en la multipunto
$("#btns-aprobado").on('click',function(){
    $(".BtnVerde").prop('checked',true);
});

$("#btns-futuro").on('click',function(){
    $(".BtnAmarillo").prop('checked',true);
});

$("#btns-inmediato").on('click',function(){
    $(".BtnRojo").prop('checked',true);
});

if (($("#multipuntoOrden").length > 0) && ($("#multipuntoOrden").val() != "0") && ($("#multipuntoOrden").val() != "")) {
    multipuntoConsulta();
}

var tipoCV_2 = $("#estatusSlider").val();
if ((tipoCV_2 == "1A") || (tipoCV_2 == "2A") || (tipoCV_2 == "2")) {
    var destino_1 = "Tecnico";
}else{
    var destino_1 = "Asesor";
}

if ($("#multipuntoOrden").length > 0) {
    var base = $("#basePeticion").val().trim();
    var serie_1 = $("#multipuntoOrden").val().trim();

    if ((serie_1 != "")&&(serie_1 != " ")&&(serie_1 != "0")) {
        $("#ventanaOculta").attr("src",base+'Alta_Presupuesto/'+serie_1);
    }else {
        $("#ventanaOculta").attr("src",base+"Alta_Presupuesto/0");
    }
}

function multipuntoConsulta() {
    console.log("Confirma Datos");
    var base = $("#basePeticion").val().trim();
    var serie_2 = $("#multipuntoOrden").val().trim();
    var anterior = $("#idCita").val().trim();
    $("#ventanaOculta").attr("src",base+'Alta_Presupuesto/'+serie_2);

    if (serie_2 == "0") {
        $("#multipuntoOrden").removeAttr("disabled");
    }

    if ((serie_2 != "")&&(serie_2 != "0")&&(serie_2 != anterior)) {
        $("#estatusSlider").val("4");
        $.ajax({
            url: base+"multipunto/Multipunto/confirmRegistry",
            method: 'post',
            data: {
                serie_2: serie_2,
            },
            success:function(resp){
                console.log(resp);
                var dirige = $("#dirige").val();
                var formulario = $("#estatusSlider").val(); 
                //Si existe la hoja multipunto
                var campos = resp.split("|");
                if (campos[0] != "ERROR") {
                    $("#errorConsulta").text("");

                    //Separamos las celdas
                    
                    console.log(campos);
                    if (campos.length > 30) {
                        $("#nombreCamapanias").text("Campaña(s): "+campos[27]);

                        //Si tiene la firma del jefe de talle
                        if (campos[32] != "") {
                            location.href = base+"Multipunto_N_Edita/"+((campos.length > 50) ? campos[61] : campos[33]);
                        //Si tiene la firma del asesor y del tecnico
                        }else if ((campos[31] != "") && (campos[28] != "")) {
                            //Redirigimos la vista precargada, identificamos si entro un asesor, un tecnico o el jefe de taller
                            //Si quien edita es el jefe de taller
                            if (dirige == "JDT") {
                                if (formulario != "3") {
                                    location.href = base+"Multipunto_Edita_JefeT/"+((campos.length > 50) ? campos[61] : campos[33]);
                                }
                            //Si quien edita es el tecnico o el asesor, ya no pueden editar nada
                            } else{
                                //Evitamos la recarga
                                if (formulario != "2A") {
                                    location.href = base+"Multipunto_N_Edita/"+((campos.length > 50) ? campos[61] : campos[33]);
                                }
                            }
                        //Si tiene la firma del asesor pero no del tecnico
                        }else if ((campos[31] == "") && (campos[28] != "")) {
                            //Si quien edita es el jefe de taller
                            if (dirige == "JDT") {
                                if (formulario != "3") {
                                    location.href = base+"Multipunto_Edita_JefeT/"+((campos.length > 50) ? campos[61] : campos[33]);
                                }
                            //Si quien edita es el tecnico o el asesor, ya no pueden editar nada
                            } else if (dirige == "TEC") {

                                //Evitamos la recarga
                                if (formulario != "2B") {
                                    location.href = base+"Multipunto_Edita_Tec/"+((campos.length > 50) ? campos[61] : campos[33]);
                                }else {
                                    //Evitamos la recarga
                                    if ((formulario != "2A")&&(dirige == "ASE")) {
                                        location.href = base+"Multipunto_N_Edita/"+((campos.length > 50) ? campos[61] : campos[33]);
                                    }
                                }
                            }else {
                                //Evitamos la recarga
                                if (formulario != "2A") {
                                    location.href = base+"Multipunto_N_Edita/"+((campos.length > 50) ? campos[61] : campos[33]);
                                }
                            }

                        //Si tiene la firma del tecnico pero no del asesor
                        }else if ((campos[31] != "") && (campos[28] == "")) {
                            //Comprobamos que no estemos ya en esa vista
                            if (dirige == "ASE") {
                                location.href = base+"Multipunto_Edita_Asesor/"+((campos.length > 50) ? campos[61] : campos[33]);
                            }else  {
                                //Evitamos la recarga
                                if (formulario != "2A") {
                                    location.href = base+"Multipunto_N_Edita/"+((campos.length > 50) ? campos[61] : campos[33]);
                                }
                            }
                        }
                    }
                //Si no existe la hoja multipunto, validamos quien edita si el asesor o el tecnico
                } else {
                    //Si es un asesor quien crea la hoja
                    if (dirige == "ASE") {
                        location.href = base+"Multipunto_Alta/"+campos[1];
                    //Si es un tecnico quien ingreso el numero de orden
                    } else if (dirige == "TEC") {
                        location.href = base+"Multipunto_Alta_Tec/"+campos[1];
                    //Si es un jefe de taller
                    }else {
                        // location.href = base+"Multipunto_Alta/"+serie_2+"/";
                        $("#errorConsulta").text("No. de orden invalido, favor de verificar.");
                        $("#guardarInspeccion").attr("disabled",true);
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
}
