$(document).ready(function() {
    var asesor = $("#asesorLogin").val();
    console.log("Aseor: "+asesor);

    $(".asesorname").val(asesor);
});

var tipoCV = $("#estatusSlider").val();
if ((tipoCV == "1A") || (tipoCV == "2A") || (tipoCV == "2")) {
    var destino = "Tecnico";
}else{
    var destino = "Asesor";
}

if ($("#noOrden").length > 0) {
    var orden = $("#noOrden").val().trim();
    var base = $("#basePeticion").val().trim();

    if ((orden != "")&&(orden != " ")) {
        $("#ventanaOculta").attr("src",base+'Alta_Presupuesto/'+orden);
    }else {
        $("#ventanaOculta").attr("src",base+"Alta_Presupuesto/0");
    }

    $("#idOrdenTemp").val(orden);
    precarga();
}

//Aciones para mostrar la cotizacion en la multipunto
$("#cotizaExt").on('click',function(){
    //Recuperamos el estado del div
    var estado = $("#divCotizacion").val();
    console.log(estado);
    //Realizamos la accion corespondiente basado en el estado del recuadro
    //Si la sección de la cotizacion esta oculta
    if (estado == 0) {
        //Cambiamos el icono
        $("#iconCoti").removeClass("fa-eye");
        $("#iconCoti").addClass("fa-eye-slash");
        //Cambiamos el valor del input
        $("#divCotizacion").val(1);
        //Mostramos panel
        $("#espacioCoti").css('display','inline-block');
    }else {
        //Cambiamos el icono
        $("#iconCoti").removeClass("fa-eye-slash");
        $("#iconCoti").addClass("fa-eye");
        //Cambiamos el valor del input
        $("#divCotizacion").val(0);
        //Mostramos panel
        $("#espacioCoti").css('display','none');
    }
});

function precarga() {
    var serie = $("#noOrden").val().trim();
    var base = $("#basePeticion").val().trim();

    if ((serie != "")&&(serie != " ")) {
        $("#cotizaExt").css("display","inline");
        $("#ventanaOculta").attr("src",base+'Alta_Presupuesto/'+serie);
    }

    $("#idCitaVideo").val(serie);
    $("#idOrdenTemp").val(serie);

    console.log("Precarga Datos");
    $.ajax({
          url: base+"multipunto/Multipunto/precarga",
          method: 'post',
          data: {
              serie: serie,
          },
          success:function(resp){
               console.log("Precarga : "+resp);
              if (resp.indexOf("handler			</p>")<1) {

                  var campos = resp.split("|");
                  console.log(campos);
                  if (campos.length > 5) {
                      // $("#txtmodelo").val(campos[0]);
                      $("#noSerieText").val(campos[1]);
                      $("#txttorre").val(campos[2]);
                      $("#txtnombreCarro").val(campos[3]);
                      $(".nombreCliente").val(campos[3]);
                      $("#txtemail").val(campos[4]);
                      $(".asesorname").val(campos[5]);
                      //$("#txtEmpresa").val(campos[6]);
                      $("#tecniconame").val(campos[7]);
                      $("#txtmodelo").val(campos[8]);
                      $("#nombreCamapanias").text("Campaña(s): "+campos[9]);
                      $("#correoCompaniaLabel").val(campos[11]);

                      if (((campos[(campos.length)-1] != '2')||(campos[(campos.length)-1] != '3'))&&($("#formularioHoja").val() == "OrdenServicio")) {
                          $('#errorConsultaServer').text('El cliente no llego, orden bloqueada');
                          //$('input').attr('disabled',true);
                      }
                  }else{
                      //$(".asesorname").val(campos[0]);
                      if (((campos[(campos.length)-1] != '2')||(campos[(campos.length)-1] != '3'))&&($("#formularioHoja").val() == "OrdenServicio")) {
                          $('#errorConsultaServer').text('El cliente no llego, orden bloqueada');
                          //$('input').attr('disabled',true);
                      }
                  }

                  //Verificamos si el nuevo numero de orden no existe
                  if ($("#formularioHoja").val() == "OrdenServicio") {
                      revisionExtra(serie);
                  }else if ($("#formularioHoja").val() == "MultipuntoHoja") {
                      revisionExtraMult(serie);
                  }
              }
          //Cierre de success
          },
          error:function(error){
              console.log(error);
          //Cierre del error
          }
      //Cierre del ajax
      });
}

//Revisamos si existe la orden, sin salir de la pagina ni recargar
function revisionExtra(serie){
    var base = $("#basePeticion").val().trim();

    if (serie != "") {
        $.ajax({
            url: base+"ordenServicio/ordenServicio/searchOrden",
            method: 'post',
            data: {
                orden: serie,
            },
            success:function(resp){
              console.log("aquilll");
                if (resp.indexOf("handler			</p>")<1) {
                    var respuesta = resp.split("_");

                    if (respuesta[0] != "ALTA") {
                        //Redireccionamos al formulario de alta
                        location.href = base+"OrdenServicio_Verifica/"+respuesta[1];
                        // $("#errorConsultaServer").text("Ya existe una orden de servicio con ese numero de orden");
                    }else{
                        $("#errorConsultaServer").text("");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
}

//Revisamos si existe la orden, sin salir de la pagina ni recargar
function revisionExtraMult(serie){
    var base = $("#basePeticion").val().trim();

    if (serie != "") {
        $.ajax({
            url: base+"multipunto/Multipunto/searchOrden",
            method: 'post',
            data: {
                orden: serie,
            },
            success:function(resp){
              console.log("aqui");
              console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
                    var respuesta = resp.split("_");

                    if (respuesta[0] != "ALTA") {
                        //Redireccionamos al formulario de edicion
                        if ($("#estatusSlider").val() == "1") {
                            location.href = base+"Multipunto_Edita_Asesor/"+respuesta[1];
                        }else {
                            location.href = base+"Multipunto_N_Edita/"+respuesta[1];
                        }
                        $("#errorConsultaServer").text("Ya existe una multipunto con ese numero de orden");
                    }else{
                        $("#errorConsultaServer").text("");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
}
