//Comenzando el calculo de valores
function precio() {
    var indice = $("#indiceCoti").val();

    //Evitamos que deje el campo en blanco
    if ($("#precioTxt_"+indice).val() == "") {
        var precio = 0;
    }else {
        var precio = parseFloat($("#precioTxt_"+indice).val());
    }

    if ($("#cantidadTxt_"+indice).val() == "") {
        var cantidad = 1;
        $("#cantidadTxt_"+indice).val(1);
    }else {
        var cantidad = parseFloat($("#cantidadTxt_"+indice).val());
    }

    if ($("#descuentoTxt_"+indice).val() == "") {
        var descuento = 0;
        $("#descuentoTxt_"+indice).val(0);
    }else {
        var descuento = parseFloat($("#descuentoTxt_"+indice).val());
    }

    // var precio = parseFloat($("#precioTxt_"+indice).val());
    // var cantidad = parseFloat($("#cantidadTxt_"+indice).val());
    // var descuento = parseFloat($("#descuentoTxt_"+indice).val());

    //Precio con iva
    var precioIva = precio * 1.16;

    var subtotal_1 = precioIva*cantidad;
    var subtotal_2 = subtotal_1-descuento;

    //Vaciamos los valores finales
    //Iva
    $("#pIvaTxt_label_"+indice).text("$ "+precioIva.toFixed(2));
    $("#pIvaTxt_"+indice).val(precioIva);

    //total de la fila
    $("#labelTxt_"+indice).text("$ "+subtotal_2.toFixed(2));
    $("#totalFila_"+indice).val(subtotal_2);

    //total de fila sin iva
    var totalIVA = precio * cantidad;
    $("#totalFila_siva_"+indice).val(totalIVA);

}

function cantidad() {
    var indice = $("#indiceCoti").val();

    //Evitamos que deje el campo en blanco
    if ($("#precioTxt_"+indice).val() == "") {
        var precio = 0;
        $("#precioTxt_"+indice).val(0);
    }else {
        var precio = parseFloat($("#precioTxt_"+indice).val());
    }

    if ($("#cantidadTxt_"+indice).val() == "") {
        var cantidad = 1;
    }else {
        var cantidad = parseFloat($("#cantidadTxt_"+indice).val());
    }

    if ($("#descuentoTxt_"+indice).val() == "") {
        var descuento = 0;
        $("#descuentoTxt_"+indice).val(0);
    }else {
        var descuento = parseFloat($("#descuentoTxt_"+indice).val());
    }

    // var precio = parseFloat($("#precioTxt_"+indice).val());
    // var cantidad = parseFloat($("#cantidadTxt_"+indice).val());
    // var descuento = parseFloat($("#descuentoTxt_"+indice).val());

    //Precio con iva
    var precioIva = precio * 1.16;

    var subtotal_1 = precioIva*cantidad;
    var subtotal_2 = subtotal_1-descuento;

    //Vaciamos los valores finales
    //Iva
    $("#pIvaTxt_label_"+indice).text("$ "+precioIva.toFixed(2));
    $("#pIvaTxt_"+indice).val(precioIva);

    //total de la fila
    $("#labelTxt_"+indice).text("$ "+subtotal_2.toFixed(2));
    $("#totalFila_"+indice).val(subtotal_2);

    //total de fila sin iva
    var totalIVA = precio * cantidad;
    $("#totalFila_siva_"+indice).val(totalIVA);

}

function descuento() {
    var indice = $("#indiceCoti").val();

    //Evitamos que deje el campo en blanco
    if ($("#precioTxt_"+indice).val() == "") {
        var precio = 0;
        $("#precioTxt_"+indice).val(0);
    }else {
        var precio = parseFloat($("#precioTxt_"+indice).val());
    }

    if ($("#cantidadTxt_"+indice).val() == "") {
        var cantidad = 1;
        $("#cantidadTxt_"+indice).val(1);
    }else {
        var cantidad = parseFloat($("#cantidadTxt_"+indice).val());
    }

    if ($("#descuentoTxt_"+indice).val() == "") {
        var descuento = 0;
    }else {
        var descuento = parseFloat($("#descuentoTxt_"+indice).val());
    }

    // var precio = parseFloat($("#precioTxt_"+indice).val());
    // var cantidad = parseFloat($("#cantidadTxt_"+indice).val());
    // var descuento = parseFloat($("#descuentoTxt_"+indice).val());

    //Precio con iva
    var precioIva = precio * 1.16;

    var subtotal_1 = precioIva*cantidad;
    var subtotal_2 = subtotal_1-descuento;

    //Vaciamos los valores finales
    //Iva
    $("#pIvaTxt_label_"+indice).text("$ "+precioIva.toFixed(2));
    $("#pIvaTxt_"+indice).val(precioIva);

    //total de la fila
    $("#labelTxt_"+indice).text("$ "+subtotal_2.toFixed(2));
    $("#totalFila_"+indice).val(subtotal_2);

    //total de fila sin iva
    var totalIVA = precio * cantidad;
    $("#totalFila_siva_"+indice).val(totalIVA);
}

function anticipo(){
    //Evitamos que deje el campo en blanco
    if ($("#anticipo_orden").val() == "") {
        $("#anticipo_orden").val(0);
    }

    //Volvemos a calcular el total
    var subTotal = parseFloat($("#iva_orden").val());
    var iva = parseFloat($("#subtotal_orden").val());

    var anticipo = parseFloat($("#anticipo_orden").val());
    
    var opera_1 = subTotal + iva;
    var total = opera_1 - anticipo;

    $("#txt_total_orden").text("$"+total.toFixed(2));
    $("#total_orden").val(total);

}
