$(document).ready(function() {
    //Comprobamos quien entra al formulario y que tipo de proceso llevara
    var tipoFormulario = $("#estatusSlider").val();
    var dirige = $("#dirige").val();

    console.log("Probando validaciones");
    
    //Ocultamos botones de las firmas (para evitar conflictos)
    $("#Asesor_firma").css("display","none");
    $("#Tecnico_firma").css("display","none");
    $("#JefeTaller_firma").css("display","none");
    $("#Cliente_firma").css("display","none"); 

    //Verificar que firma se puede mostrar
    if (dirige == "ASE") {
        $("#Asesor_firma").css("display","inline-block");
        $("#Cliente_firma").css("display","inline-block");
        //Si es el asesor quien ingresa a la orden, se oculta el boton de cotizacion
        $("#cotizaExt").css("display","none");
    }else if (dirige == "TEC") {
        $("#Tecnico_firma").css("display","inline-block");
    }else if (dirige == "JDT") {
        $("#JefeTaller_firma").css("display","inline-block");
    }else {
        $("#Asesor_firma").css("display","inline-block");
        $("#Tecnico_firma").css("display","inline-block");
        $("#JefeTaller_firma").css("display","inline-block");
        $("#Cliente_firma").css("display","inline-block");
    }

    //Para formulario de alta
    //Alta registrada por un asesor
    if ((tipoFormulario == "1")&&(dirige == "ASE")) {
        $(".seccionTecnico").attr("disabled", true);
        //Bloqueamos las firmas
        $("#tecniconame").attr("disabled", true);
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);

        $("#Tecnico_firma").attr("disabled", true);
        $("#Tecnico_firma").attr("data-toggle", false);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        $("#Cliente_firma").attr("disabled", true);
        $("#Cliente_firma").attr("data-toggle", false);

        //Si es el asesor quien ingresa a la orden, se oculta el boton de cotizacion
        $("#cotizaExt").css("display","none");

    //Alta registrada por un tecnico
    }else if ((tipoFormulario == "1A")&&(dirige == "TEC")) {
        //Bloqueamos las firmas
        $("#asesorname").attr("disabled", true);
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);

        $("#Asesor_firma").attr("disabled", true);
        $("#Asesor_firma").attr("data-toggle", false);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        $("#Cliente_firma").attr("disabled", true);
        $("#Cliente_firma").attr("data-toggle", false);

    //Editar por parte del tecnico despues de que la alta fuera por el asesor
    }else if ((tipoFormulario == "2")&&(dirige == "TEC")) {
        //Si es edicion, bloqueamos la edicion de niveles de gasolina
        $('#slider-2').slider('disable');

        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        // $("#nombreConsumidor").attr("disabled", true);
        $("#asesorname").attr("disabled", true);

        $("#Asesor_firma").attr("disabled", true);
        $("#Asesor_firma").attr("data-toggle", false);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        // $("#Cliente_firma").attr("disabled", true);
        // $("#Cliente_firma").attr("data-toggle", false);

        if ($("#Tecnico_firma_2").val() == "1") {
            $("#guardarInspeccion").attr("disabled", true);
        }

    //Si el tecnico intenta volver a editar
    //}else if ((tipoFormulario == "2A")&&(dirige == "TEC")) {
    }else if (($("#Tecnico_firma_2").val() == "1")&&(dirige == "TEC")) {
        console.log("Reglas edita Asesor");
        //Si es edicion, bloqueamos la edicion de niveles de gasolina
        $('#slider-2').slider('disable');

        $(".seccionTecnico").attr("disabled", true);
        $(".seccionAsesor").attr("disabled", true);
        // $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        // $("#multipuntoOrden").attr("disabled", false);

        $("#asesorNombre").attr("disabled", false);
        $("#Asesor_firma").attr("disabled", false);
        $("#Asesor_firma").attr("data-toggle", "modal");

        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);

        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        $("#Cliente_firma").attr("disabled", true);
        $("#Cliente_firma").attr("data-toggle", false);

        if ($("#Tecnico_firma_2").val() == "1") {
            $("#guardarInspeccion").attr("disabled", true);
        }
        $("#guardarInspeccion").attr("disabled", false);

    //Si es edicion, por parte de un asesor (va afirmar)
    }else if ((tipoFormulario == "2B")&&(dirige == "ASE")) {
        console.log("Reglas del tecnico");
        $('#slider-2').slider();
        $(".seccionAsesor").attr("disabled", false);
        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        // $("#nombreConsumidor").attr("disabled", true);

        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        // $("#Cliente_firma").attr("disabled", true);
        // $("#Cliente_firma").attr("data-toggle", false);

        $("#asesorNombre").attr("disabled", true);
        $("#Asesor_firma").attr("disabled", true);
        $("#Asesor_firma").attr("data-toggle", false);

        if (($("#Asesor_firma_2").val() == "1")&&($("#Tecnico_firma_2").val() == "1")) {
            $("#guardarInspeccion").attr("disabled", true);
        }

        //Si es el asesor quien ingresa a la orden, se oculta el boton de cotizacion
        $("#cotizaExt").css("display","none");

    }else if (tipoFormulario == "3A") {
        console.log("Reglas del jefe de taller");
        //Si es edicion, bloqueamos la edicion de niveles de gasolina
        $('#slider-2').slider('disable');
        $(".seccionAsesor").attr("disabled", true);
        $(".seccionTecnico").attr("disabled", true);
        $("textarea").attr("disabled", true);

        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        // $("#nombreConsumidor").attr("disabled", true);
        $("#tecniconame").attr("disabled", true);

        $("#Tecnico_firma").attr("disabled", true);
        $("#Tecnico_firma").attr("data-toggle", false);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        // $("#Cliente_firma").attr("disabled", true);
        // $("#Cliente_firma").attr("data-toggle", false);

    //Si es edicion, por parte de un jefe de servicio
    }else if (tipoFormulario == "3") {
        console.log("Reglas de admin");
        //Si es edicion, bloqueamos la edicion de niveles de gasolina
        $('#slider-2').slider('disable');
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        console.log("jefe taller");
    }

    //Condiciones para el jefe de taller
    if ((($("#Asesor_firma_2").val() == "0") || ($("#Tecnico_firma_2").val() == "0")) && ($("#dirige").val() == "JDT")) {
        console.log("Entro aqui");
        //Bloqueamos las firmas
        $("#JefeTallerNombre").attr("disabled", true);
        $("#nombreConsumidor").attr("disabled", true);
        $("#JefeTaller_firma").attr("disabled", true);
        $("#JefeTaller_firma").attr("data-toggle", false);
        //Bloqueamos los campos
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        $("#multipuntoOrden").removeAttr("disabled");
        $("#guardarInspeccion").attr("disabled", true);
    //Verfificamos que haya firmado el tecnico
    }
    //else if (($("#Tecnico_firma_2").val() == "0") && ($("#dirige").val() == "JDT")) {
    //     console.log("Entro aca");
    //     //Bloqueamos las firmas
    //     $("#JefeTallerNombre").attr("disabled", true);
    //     $("#nombreConsumidor").attr("disabled", true);
    //     $("#JefeTaller_firma").attr("disabled", true);
    //     $("#JefeTaller_firma").attr("data-toggle", false);
    //     //Bloqueamos los campos
    //     $("input").attr("disabled", true);
    //     $("textarea").attr("disabled", true);
    //     $("#multipuntoOrden").removeAttr("disabled");
    //     $("#guardarInspeccion").attr("disabled", true);
    // }

    $("textarea").attr("disabled", false);

    //Comprobamos firmas
    var firma_1 = $("#Asesor_firma_2").val();
    var firma_2 = $("#Tecnico_firma_2").val();
    var firma_3 = $("#JefeTaller_firma_2").val();
    var firma_4 = $("#Cliente_firma_2").val();

    //Bloqueamos dependiedo las firmas
    //Firma del asesor (no bloquea nada, solo su firma)
    // if (firma_1 == "1") {
    //     $('#slider-2').slider('disable');
    //     $(".seccionAsesor").attr("disabled", true);
    // }

    //Firma del tecnico (bloqueamos todos los campos del formulario)
    if (firma_2 == "1") {
        console.log("Firma del tecnico");
        $('#slider-2').slider('disable');
        $(".seccionTecnico").attr("disabled", true);
        $(".seccionAsesor").attr("disabled", true);
        $("textarea").attr("disabled", true);
    }else if ((tipoFormulario == "1A")&&(dirige == "TEC")) {
        //$('#slider-2').slider('disable');
        $(".seccionTecnico").attr("disabled", false);
        $(".seccionAsesor").attr("disabled", false);
        $("textarea").attr("disabled", false);
    }

    //Firma del jefe de taller (se bloquea todo, solo falta firma del cliente)
    if (firma_3 == "1") {
        console.log("Firma del jefe de taller");
        $('#slider-2').slider('disable');
        $(".seccionAsesor").attr("disabled", true);
        $(".seccionTecnico").attr("disabled", true);
        $("textarea").attr("disabled", true);
    }

    //Firma del cliente, impide la edicion con clave maestra
    if (firma_4 == "1") {
        console.log("Firma del cliente");
        $('#slider-2').slider('disable');
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        // $("#multipuntoOrden").attr("disabled", false);
    }

    if ($("#dirige").val() == "ADMIN") {
        console.log("es panel");
        $('#slider-2').slider('disable');
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);

        //Bloqueamos las firmas
        $(".cuadroFirma").attr("disabled", true);
        $(".cuadroFirma").attr("data-toggle", false);
    }

    // if ($("#dirige").val() != "JDT") {
    //     $("#JefeTallerNombre").attr("disabled", true);
    //     $("#JefeTaller_firma").attr("disabled", true);
    //     $("#JefeTaller_firma").attr("data-toggle", false);
    // }else {
    //     $("#JefeTallerNombre").removeAttr("disabled");
    //     $("#JefeTaller_firma").removeAttr("disabled");
    //     $("#JefeTaller_firma").attr("data-toggle", "modal");
    // }

    //Si ya estan todas las 3 primeras firmas, desbloqueamos la firma del cliente
    if (($("#Asesor_firma_2").val() == "1")&&($("#Tecnico_firma_2").val() == "1" )&&($("#JefeTaller_firma_2").val() == "1")&&($("#Cliente_firma_2").val() == "0")) {
        //Comprobamos que sea el panel del asesor
        if ($("#dirige").val() == "ASE") {
            console.log("1");
            //Revisamos que el diagnostico este lleno
            if ($("#firma_diagnostico").val() == "1") {
                $("#nombreConsumidor").removeAttr("disabled");
                $("#Cliente_firma").removeAttr("disabled");
                $("#Cliente_firma").attr("data-toggle", "modal");

                $("#guardarInspeccion").css("display","block");
                $("#guardarInspeccion").removeAttr("disabled");
                $("#btnRegresar").css("display","none");
            }else{
                $("#errorConsultaServer").text("No se ha firmado el diagnóstico.");
            }
        //Si es cualquier otro panel bloqueamos la firma del cliente
        }else {
            console.log("2");
            $("#nombreConsumidor").attr("disabled", true);
            $("#Cliente_firma").attr("disabled", true);
            $("#Cliente_firma").attr("data-toggle", false);
        }

    //De lo contrario, bloqueamos la firma del cliente
    }else {
        console.log("3");
        $("#nombreConsumidor").attr("disabled", true);
        $("#Cliente_firma").attr("disabled", true);
        $("#Cliente_firma").attr("data-toggle", false);
    }

    //Comprobamos si ya existen todas las firmas (f+si falta alguna de las firmas, aun se permite editar)
    if (($("#Asesor_firma_2").val() == "0")||($("#Tecnico_firma_2").val() == "0" )||($("#JefeTaller_firma_2").val() == "0")||($("#Cliente_firma_2").val() == "0")) {
        $("#superUsuarioName").removeAttr("disabled");
        $("#superUsuarioPass").removeAttr("disabled");
        console.log("4");
    //De lo contrario, bloqueamos los campos de superusuario
    } else {
        $("#superUsuarioName").attr("disabled", true);
        $("#superUsuarioPass").attr("disabled", true);
        console.log("5");
    }

    //Verificamos que la unidad este trabajando, de lo contrario se bloquea   (si ya la firmo, no se imprime el mensaje)
    if ((dirige == "TEC")&&($("#unidadTrabajando").val() == "FALSE")&&($("#Tecnico_firma_2").val() == "0")&&($("#idCita").val() != "0")) {
        console.log("Unidad sin trabajar");
        //Bloqueamos todo
        $("#guardarInspeccion").attr("disabled", true);
        //Campos de edicion
        $('#slider-2').slider('disable');
        $("input").attr("disabled", true);
        $("textarea").attr("disabled", true);
        //Firma del tecnico
        $("#Tecnico_firma").attr("disabled", true);
        $("#Tecnico_firma").attr("data-toggle", false);
        //Mensaje de error
        $("#errorConsultaServer").text("La unidad no se ha trabajado, verificar estatus de la unidad");

    //Si la unidad no se puso en el estaus de trabajando en ningun momento pero se firmo ...
    }else if ((dirige == "TEC")&&($("#unidadTrabajando").val() == "FALSE")&&($("#Tecnico_firma_2").val() == "1")&&($("#idCita").val() != "0")) {
        //Mensaje de error
        $("#errorConsultaServer").text("La unidad no se trabajo.");
    }

});
