  // $(document).ready(function() {
  //     var bloqueo = $("#estatusSlider").val();
  //     if (bloqueo == "2") {
  //         $('#slider-2').slider('disable');
  //     }
  // });

  //Inicializar progress
  var level = 20;
  $("#slider-2").css('background', '#ffffff');
  $("#slider-2").css('width', '290px');

  if ($('#valorBateria').val() == 0) {
      $("#slider-2").css('background', '#ffffff');
  }else if(($('#valorBateria').val() >= 1) && ($('#valorBateria').val() < 35)){
      $('#slider-2').css('background','#ff0000');
  }else if (($('#valorBateria').val() >= 35) && ($('#valorBateria').val() < 50)) {
      $('#slider-2').css('background','#ffb400');
  }else if (($('#valorBateria').val() >= 50) && ($('#valorBateria').val() < 60)) {
      $("#slider-2").css('background', '#ffff00');
  }else if(($('#valorBateria').val() > 60) && ($('#valorBateria').val() < 70)){
      $("#slider-2").css('background', '#c5ea4c');
  }else{
      $("#slider-2").css('background', '#00ff00');
  }


  $("#slider-2").slider({
      range:false,
      min: 0,
      max: 100,
      //values: [ 35, 200 ],
      value:$('#valorBateria').val(),
      slide: function(event, ui) {
         // console.log(ui.value);
          $('#valorBateria').val(ui.value);
          var nivel = $('#valorBateria').val();
          $('#labelNB').text(ui.value+"%");
          if((nivel >= 0) && (nivel < 35)){
              $('#slider-2').css('background','#ff0000');
              // $("#slider-2").css('height', level + (ui.value / 10));
          }else if ((nivel >= 35) && (nivel < 50)) {
              $('#slider-2').css('background','#ffb400');
          }else if ((nivel >= 50) && (nivel < 60)) {
              $("#slider-2").css('background', '#ffff00');
          }else if((nivel > 60) && (nivel < 70)){
              $("#slider-2").css('background', '#c5ea4c');
          }else{
              $("#slider-2").css('background', '#00ff00');
          }
      }
  });
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  //Multipunto señanlamiento de parte inferior
  var canvas_dia = document.getElementById('diagramaInferior');
  var ctx_dia = canvas_dia.getContext("2d");
  var background = new Image();
  if ($("#danosInferior").val() == "") {
      background.src = $("#direccionFondo").val();
  } else {
      background.src = $("#danosInferior").val();
  }

  background.onload = function(){
      ctx_dia.drawImage(background,0,0,380,200);
  }

  $("#diagramaInferior").click(function(e){
      console.log("click");
      getPosition(e);
  });

  // var pointSize = 3;
  function getPosition(event){
      var rect = canvas_dia.getBoundingClientRect();
      var x = event.clientX - rect.left;
      var y = event.clientY - rect.top;
      drawCoordinates(x,y);
  }

  function drawCoordinates(x,y){
    	ctx_dia.fillStyle = "#ffc107";
      ctx_dia.beginPath();
      ctx_dia.arc(x, y, 8, 0, Math.PI * 2, true);
      ctx_dia.fill();
      // var contenedor = $("#coleccionCoordenadas");
      // contenedor.append("<input type='checkbox' name='coordenadasInferior[]' value='"+x+"_"+y+"' style='display: none;' checked>");
  }

  //Presionamos el boton de enviamo
  $("#guardarInspeccion").on('click',function(){
      $("input").removeAttr("disabled");
      $("textarea").removeAttr("disabled");

      //Bloqueamos botones de envio
      //$("#guardarInspeccion").attr('disabled',true);
      $(".cargaIcono").css("display","inline-block");
      $("#msm_envio").text("ENVIANDO....");

      //Solo guarda el tecnico
      if (($("#estatusSlider").val() == "2") || ($("#estatusSlider").val() == "1A") || ($("#estatusSlider").val() == "2B")) {
          var dataURL = canvas_dia.toDataURL('image/png');
          $("#danosInferior").val(dataURL);
      }else if (($("#estatusSlider").val() == "2A")&&($("#dirige").val() == "TEC")) {
          var dataURL = canvas_dia.toDataURL('image/png');
          $("#danosInferior").val(dataURL);
      }

      document.forms[0].submit();
  });
