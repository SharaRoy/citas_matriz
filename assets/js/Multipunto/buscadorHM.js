// $(".asignar").on('click',function() {
//     //Recuperamos la fila que se editara
//     var renglon = $(this).data("id");
//     // console.log(renglon);
//     //Recuperamos los valores para mostrar ala usuario
//     var id = $("#id_"+renglon).val();
//     var folio = $("#folio_"+renglon).val();
//     var fecha = $("#fecha_"+renglon).val();
//     var vehiculo = $("#vehiculo_"+renglon).val();
//     var cliente = $("#cliente_"+renglon).val();
//     var asesor = $("#asesor_"+renglon).val();
//     var hora = $("#hora_"+renglon).val();
//
//     //Imprmimimos el texto en el modal
//     $("#infoRegistroScita1").text("  "+folio+".");
//     $("#infoRegistroScita2").text("  "+vehiculo+".");
//     $("#infoRegistroScita3").text("  "+cliente+".");
//     $("#infoRegistroScita4").text("  "+asesor+".");
//     $("#infoRegistroScita5").text("  "+fecha+".");
//     $("#idRegistro").val(id);
//     $("#folioTem").val(folio);
//     $("#horaRegistro").val(hora);
//     $("#fechReg").val(fecha);
//
// });

$('#busqueda_tabla').keyup(function () {
    toSearch();
});


//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}

$(".comentarioHM").on('click',function() {
    //Recuperamos la fila que se editara
    var renglon = $(this).data("id");
    $("#guardarEvento").attr("hidden",false);
    //console.log(renglon);
    //Recuperamos los valores para mostrar ala usuario
    var idOrden = $("#orden_"+renglon).val();
    var comentario = $("#comentario_"+renglon).val();
    var sistema = $("#sistema_"+renglon).val();
    var componente = $("#componente_"+renglon).val();
    var causa = $("#causaRaiz_"+renglon).val();
    var cliente = $("#nombreCliente_"+renglon).val();
    var asesor = $("#asesor_"+renglon).val();
    var tecnico = $("#tecnico_"+renglon).val();

    //Imprmimimos el texto en el modal
    // $("#infoRegistroScita1").text("  "+idOrden+".");
    $("#infoRegistroScita3").text("  "+cliente+".");
    $("#infoRegistroScita4").text("  "+asesor+".");
    $("#infoRegistroScita5").text("  "+tecnico+".");

    $("#idOrden").val(idOrden);
    $("#comentario").val(comentario);
    $("#sistema").val(sistema);
    $("#componente").val(componente);
    $("#causa").val(causa);
});

//Cuando se vacie el input, se carga nuevamente toda la tabla
function envioComentario(){
    console.log("click");
    //Recuperamos el comentario a guardar
    var idOrden = $("#idOrden").val();
    var comentario = $("#comentario").val();
    var sistema = $("#sistema").val();
    var componente = $("#componente").val();
    var causa = $("#causa").val();

    $("#errorIdCitaComentarioHM").css("color","blue");
    $("#errorIdCitaComentarioHM").text("Enviando comentario");

    $("#idOrden").attr("disabled",true);
    $("#comentario").attr("disabled",true);
    $("#sistema").attr("disabled",true);
    $("#componente").attr("disabled",true);
    $("#causa").attr("disabled",true);

    //Verificamos que no sea un campo vacio
    if (idOrden != "") {
        //Si el filtro quedo vacio, jalar toda la tabla
        var url = $("#basePeticion").val();

        $.ajax({
            url: url+"multipunto/Anexos/saveComment",
            method: 'post',
            data: {
                idOrden: idOrden,
                comentario: comentario,
                sistema: sistema,
                componente: componente,
                causa: causa,
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler			</p>")<1) {
                    if (resp == "OK") {
                        $("#errorIdCitaComentarioHM").css("color","green");
                        $("#errorIdCitaComentarioHM").text("Comentario Guardador");
                        // $("#idOrden").attr("disabled",false);
                        // $("#comentario").attr("disabled",false);
                        // $("#sistema").attr("disabled",false);
                        // $("#componente").attr("disabled",false);
                        // $("#causa").attr("disabled",false);
                        // $("#comentario").val("");
                        // $("#comentario").val("");
                        // $("#comentario").val("");
                        // $("#comentario").val("");
                        $("#guardarEvento").attr("hidden",true);
                    }else {
                        $("#errorIdCitaComentarioHM").css("color","red");
                        $("#errorIdCitaComentarioHM").text("Ocurrio un error al intentar guardar");
                        $("#idOrden").attr("disabled",false);
                        $("#comentario").attr("disabled",false);
                        $("#sistema").attr("disabled",false);
                        $("#componente").attr("disabled",false);
                        $("#causa").attr("disabled",false);

                    }
                }else {
                    $("#errorIdCitaComentarioHM").css("color","red");
                    $("#errorIdCitaComentarioHM").text("Ocurrio un error al intentar guardar");
                    $("#idOrden").attr("disabled",false);
                    $("#comentario").attr("disabled",false);
                    $("#sistema").attr("disabled",false);
                    $("#componente").attr("disabled",false);
                    $("#causa").attr("disabled",false);
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else{
        console.log("ERROR DE CAMPOS");
        $("#errorIdCitaComentarioHM").css("color","RED");
        $("#errorIdCitaComentarioHM").text("Faltan campos del envio");
        $("#idOrden").attr("disabled",false);
        $("#comentario").attr("disabled",false);
        $("#sistema").attr("disabled",false);
        $("#componente").attr("disabled",false);
        $("#causa").attr("disabled",false);
    }

}

function actualiza(){
    window.location.reload();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

$('#btnBusqueda_HM').on('click',function(){
    console.log("Buscando hoja multipunto");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var campo = $("#busqueda_campo").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"multipunto/Anexos/filtroHM", 
        method: 'post',
        data: {
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length; 
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("=");

                        console.log(celdas);
                        var comentario = "<a class='btn btn-primary' style='color:white;' onclick='fcomentariosHM("+(i+1)+")' data-id='"+(i+1)+"' data-target='#pregunta' data-toggle='modal' >"+
                                            "Agregar <br> comentario"+
                                        "</a>";

                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:12px;'>"+
                            "<td align='center' style='width:10%;vertical-align:middle;'>"+
                                celdas[0]+""+
                                "<input type='hidden' id='orden_"+(i+1)+"' value='"+celdas[0]+"'>"+
                            "</td>"+
                            "<td align='center' style='width:10%;vertical-align:middle;'>"+
                                celdas[1]+""+
                            "</td>"+
                            "<td align='center' style='width:15%;vertical-align:middle;'>"+
                                celdas[2]+""+
                                "<input type='hidden' id='nombreCliente_"+(i+1)+"' value='"+celdas[2]+"'>"+
                            "</td>"+
                            "<td align='center' style='width:15%;vertical-align:middle;'>"+
                                celdas[3]+""+
                                "<input type='hidden' id='asesor_"+(i+1)+"' value='"+celdas[3]+"'>"+
                            "</td>"+
                            "<td align='center' style='width:15%;vertical-align:middle;'>"+
                                celdas[4]+""+
                                "<input type='hidden' id='tecnico_"+(i+1)+"' value='"+celdas[4]+"'>"+
                            "</td>"+
                            "<td align='center' style='width:10%;vertical-align:middle;'>"+
                                celdas[5]+""+
                                "<input type='hidden' id='sistema_"+(i+1)+"' value='"+celdas[5]+"'>"+
                            "</td>"+
                            "<td align='center' style='width:10%;vertical-align:middle;'>"+
                                celdas[6]+""+
                                "<input type='hidden' id='componente_"+(i+1)+"' value='"+celdas[6]+"'>"+
                            "</td>"+
                            "<td align='center' style='width:15%;vertical-align:middle;'>"+
                                celdas[7]+""+
                                "<input type='hidden' id='causaRaiz_"+(i+1)+"' value='"+celdas[7]+"'>"+
                            "</td>"+
                            "<td align='center' style='width:15%;vertical-align:middle;'>"+
                                celdas[8]+""+
                                "<input type='hidden' id='comentario_"+(i+1)+"' value='"+((celdas[8] != "") ? celdas[8] : 'Sin comentarios')+"'>"+
                            "</td>"+
                            "<td align='center' style='width:15%;'>"+
                                comentario+""+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});


function fcomentariosHM(renglon) {
    $("#guardarEvento").attr("hidden",false);
    //console.log(renglon);
    //Recuperamos los valores para mostrar ala usuario
    var idOrden = $("#orden_"+renglon).val();
    var comentario = $("#comentario_"+renglon).val();
    var sistema = $("#sistema_"+renglon).val();
    var componente = $("#componente_"+renglon).val();
    var causa = $("#causaRaiz_"+renglon).val();
    var cliente = $("#nombreCliente_"+renglon).val();
    var asesor = $("#asesor_"+renglon).val();
    var tecnico = $("#tecnico_"+renglon).val();

    //Imprmimimos el texto en el modal
    // $("#infoRegistroScita1").text("  "+idOrden+".");
    $("#infoRegistroScita3").text("  "+cliente+".");
    $("#infoRegistroScita4").text("  "+asesor+".");
    $("#infoRegistroScita5").text("  "+tecnico+".");

    $("#idOrden").val(idOrden);
    $("#comentario").val(comentario);
    $("#sistema").val(sistema);
    $("#componente").val(componente);
    $("#causa").val(causa);
}