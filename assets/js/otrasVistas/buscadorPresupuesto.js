//Condicionamos las busquedas por fecha 
$('#fecha_inicio').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_fin").attr('min',fecha_inicio);
    //console.log(fecha_inicio)
});

$('#fecha_fin').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_inicio").attr('max',fecha_fin);
    //console.log(fecha_fin)
});


$('#btnBusqueda_fechas_presupuesto').on('click',function(){
    console.log("Buscando por fechas");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_campo").val();
    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"multipunto/ProactivoAnalisisHistorial/filtroPorFechas",
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");

                    var base_2 = $("#basePeticion").val().trim();

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("=");
                        //console.log(celdas);

                        if (celdas[6] == "M") {
                            var ver = "<a href='"+base_2+"Cotizacion_Cliente/"+celdas[10]+"' class='btn btn-primary' target='_blank' style='font-size: 9.5px;'>"+
                                    "VER"+
                                "</a>";
                        } else {
                            var ver = "<a href='"+base_2+"Cotizacion_Ext_Cliente/"+celdas[10]+"' class='btn btn-primary' target='_blank' style='font-size: 9.5px;'>"+
                                    "VER"+
                                "</a>";
                        }

                        //Recuperamos los archivos oasis
                        var estatusColor = ((celdas[4] == "1") ? "background-color: #ec8d8d;" : "background-color: #ffc65e;");

                        var base_cita = $("#baseCitas").val();
                        var urlCita = 'parent.window.location="'+base_cita+'citas/agendar_cita/'+celdas[0]+'/0/0/0/1"';
                        
                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:11px;'>"+
                            //Indice
                            "<td align='center' style='vertical-align: middle;'>"+(i+1)+"</td>"+
                            //NO. ORDEN
                            "<td align='center' style='vertical-align: middle;'>"+celdas[0]+"</td>"+
                            //NO. ORDEN INTELISIS
                            "<td align='center' style='vertical-align: middle;'>"+celdas[9]+"</td>"+
                            //FECHA DE LA ORDEN
                            "<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
                            //SERIE
                            "<td align='center' style='vertical-align: middle;'>"+celdas[7]+"</td>"+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;'>"+celdas[1]+"</td>"+
                            //CLIENTE
                            "<td align='center' style='vertical-align: middle;'>"+celdas[2]+"</td>"+
                            //TELEFONO CLIENTE
                            "<td align='center' style='vertical-align: middle;'>"+celdas[5]+"</td>"+
                            //ESTATUS
                            "<td align='center' style='vertical-align: middle;"+estatusColor+"'>"+((celdas[4] == "1") ? "RECHAZADA" : "INCOMPLETA")+"</td>"+
                            //ACCIONES
                            //VER
                            "<td align='center' style='vertical-align: middle;'>"+
                                ver+""+
                            "</td>"+
                            //AGENDAR CITA
                            "<td align='center' style='vertical-align:middle;font-weight: initial;'>"+
                                "<a href='javascript: void(0);' title='Agregar Cita' onclick='"+urlCita+"' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-plus'></i>"+
                                "</a>"+
                            "</td>"+
                            //AGREGAR COMENTARIO
                            "<td align='center' style='vertical-align:middle;font-weight: initial;'>"+
                                "<a class='addComentarioBtn' onclick='btnAddClick("+celdas[8]+")' title='Agregar Comentario' data-id='"+celdas[8]+"' data-target='#addComentario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-comments'></i>"+
                                "</a>"+
                            "</td>"+
                            //HISTORIAL COMETARIOS
                            "<td align='center' style='vertical-align:middle;font-weight: initial;'>"+
                                "<a class='historialCometarioBtn' onclick='btnHistorialClick("+celdas[8]+")' title='Historial comentarios' data-id='"+celdas[8]+"' data-target='#historialCometario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-info'></i>"+
                                "</a>"+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='13'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});


