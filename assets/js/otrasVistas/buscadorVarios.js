//Cambia el valor de la fecha
function busqueda(fecha){
    // var fecha = $("#busqueda_tabla_fecha").val();
    console.log(fecha);
    //Evitamos que se borre el historial
    if (fecha != "") {
        //Fraccionamos la fecha para reacomodarla
        var frag = fecha.split("-");
        //Reacomodamos la fecha en formato d ela tabla
        var buscar = frag[2]+"/"+frag[1]+"/"+frag[0];
        //Convertimos la nueva fecha en una expresion regular
        var expFech = new RegExp(buscar, 'i');
        //Hacemos una busqueda por la tabla
        $('.campos_buscar tr').hide();
        $('.campos_buscar tr').filter(function () {
            var respuesta = false;
            if (expFech.test($(this).text())) {
                respuesta = true;
            }
            return respuesta;
        }).show();
    }else{
        var expFech = new RegExp("", 'i');
        //Hacemos una busqueda por la tabla
        $('.campos_buscar tr').hide();
        $('.campos_buscar tr').filter(function () {
            var respuesta = false;
            if (expFech.test($(this).text())) {
                respuesta = true;
            }
            return respuesta;
        }).show();
    }
}

//Al precionar el boton de busqueda general de la tabla
 $('#busqueda_tabla').keyup(function () {
     toSearch();
 });

//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}

//Peticion de busqueda por periodo de fechas
$("#busquedaFechas").on('click',function(){
    busquedaGral(1);
});

//Limpiamos la busqueda y recuperamos la lista original
$("#limpiarBusqueda").on('click',function(){
    $("#busqueda_tabla_fecha_Ini").val("");
    $("#busqueda_tabla_fecha_Fin").val("");
    location.reload();
});

/*function validaGral(){
    if (($("#busqueda_tabla").val() != "")&&($("#busqueda_tabla").val() != " ")) {
        busquedaGral(2);
    }
    console.log("aqui busqueda");
}*/

function busquedaGral(origen){
    // console.log("Busqueda");
    var inicio = $("#busqueda_tabla_fecha_Ini").val();
    var fin = $("#busqueda_tabla_fecha_Fin").val();
    /*var busquedaGral = $("#busqueda_tabla").val();
    var busqueda = 2;

    //Verficamos si es un periodo de tiempo o una fecha en concreto
    if (fin == "") {
        busqueda = 1;
    }

    if (origen == 2) {
        $("#busqueda_tabla_fecha_Ini").val("");
        $("#busqueda_tabla_fecha_Fin").val("");
    }else {
        busquedaGral = "";
        $("#busqueda_tabla").val("");
    }*/

    //Hacemos la peticion ajax de la busqueda
    var url = $("#basePeticion").val();

    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");
    
    $("#errorEnvioCotizacion").text("");

    $.ajax({
        url: url+"multipunto/ProactivoAnalisisHistorial/ajaxHistoriaIndicadores",
        method: 'post',
        data: {
            fecha1: inicio,
            fecha2: fin,
            //campoGral:busquedaGral,
            //tipo: busqueda,
        },
        success:function(resp){
            // console.log(resp.length);
            // console.log(resp);

            //identificamos la tabla a afectar
            var tabla = $(".campos_buscar");
            tabla.empty();

            if ((resp.indexOf("handler			</p>") < 1)&&(resp.length > 10)) {
                //Fraccionamos la respuesta en renglones
                var campos = resp.split("|");

                //Recorremos los "renglones" obtenidos para partir los campos
                var registro = ( campos.length > 1) ? campos.length -1 : campos.length;

                for (var i = 0; i < registro; i++) {
                    var celdas = campos[i].split("_"); 
                    // console.log(celdas);
                    if (celdas.length>13) {
                        //Creamos las celdas en la tabla
                        var camp1 = (celdas[5] == 'V') ? '#c7ecc7' : ((celdas[5] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp2 = (celdas[6] == 'V') ? '#c7ecc7' : ((celdas[6] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp3 = (celdas[7] == 'V') ? '#c7ecc7' : ((celdas[7] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp4 = (celdas[8] == 'V') ? '#c7ecc7' : ((celdas[8] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp5 = (celdas[9] == 'V') ? '#c7ecc7' : ((celdas[9] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp6 = (celdas[10] == 'V') ? '#c7ecc7' : ((celdas[10] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp7 = (celdas[11] == 'V') ? '#c7ecc7' : ((celdas[11] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp8 = (celdas[12] == 'V') ? '#c7ecc7' : ((celdas[12] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp9 = (celdas[13] == 'V') ? '#c7ecc7' : ((celdas[13] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp10 = (celdas[14] == 'V') ? '#c7ecc7' : ((celdas[14] == 'A') ? '#f5ff51' : '#f5acaa');
                        var camp11 = (celdas[15] == 'V') ? '#c7ecc7' : ((celdas[15] == 'A') ? '#f5ff51' : '#f5acaa');

                        //Campos indicadores anexados
                        var campA = (celdas[17] == 'V') ? '#c7ecc7' : ((celdas[17] == 'A') ? '#f5ff51' : '#f5acaa');
                        var campB = (celdas[18] == 'V') ? '#c7ecc7' : ((celdas[18] == 'A') ? '#f5ff51' : '#f5acaa');
                        var campC = (celdas[19] == 'V') ? '#c7ecc7' : ((celdas[19] == 'A') ? '#f5ff51' : '#f5acaa');
                        var campD = (celdas[20] == 'V') ? '#c7ecc7' : ((celdas[20] == 'A') ? '#f5ff51' : '#f5acaa');

                        //Campos de pieza cambiadas
                        var indicador1 = (celdas[22] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador2 = (celdas[23] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador3 = (celdas[24] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador4 = (celdas[25] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador5 = (celdas[26] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador6 = (celdas[27] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador7 = (celdas[28] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador8 = (celdas[29] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador9 = (celdas[30] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador10 = (celdas[31] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador11 = (celdas[32] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador12 = (celdas[33] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador13 = (celdas[34] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador14 = (celdas[35] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';
                        var indicador15 = (celdas[36] == '1') ? '<i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>' : '';

                        var base_cita = $("#baseCitas").val();
                        var urlCita = "'"+base_cita+"citas/agendar_cita/0/0/0/"+celdas[21]+"'";
                        console.log(celdas[21]);
                        tabla.append("<tr style='font-size:11px;'>"+
                            "<td align='left' style='vertical-align:middle;font-weight: initial;font-size: 9px;'>"+celdas[16]+"</td>"+
                            "<td align='left' style='vertical-align:middle;font-weight: initial;font-size: 9px;'>"+celdas[0].toUpperCase()+"</td>"+
                            "<td align='left' style='vertical-align:middle;font-weight: initial;font-size: 9px;'>"+celdas[1].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;font-size: 9px;'>"+celdas[2].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;font-size: 9px;'>"+celdas[3].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;font-size: 9px;'>"+celdas[4].toUpperCase()+"</td>"+

                            // Indicadores con color
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp1+";'>"+indicador1+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+campA+";'>"+indicador2+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp2+";'>"+indicador3+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp3+";'>"+indicador4+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+campB+";'>"+indicador5+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp4+";'>"+indicador6+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp5+";'>"+indicador7+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp6+";'>"+indicador8+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+campC+";'>"+indicador9+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp7+";'>"+indicador10+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp8+";'>"+indicador11+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+campD+";'>"+indicador12+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp9+";'>"+indicador13+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp10+";'>"+indicador14+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-size: 8px;background-color: "+camp11+";'>"+indicador15+"</td>"+

                            "<td align='center' style='vertical-align:middle;font-weight: initial;font-size: 8px;'>"+
                                "<a href='"+url+'Multipunto_PDF/'+celdas[16]+"' class='btn btn-info' target='_blank' style='font-size: 11px;'>PDF</a>"+
                            "</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;'>"+
                                "<a href='javascript: void(0);' title='Agregar Cita' onclick='parent.window.location="+urlCita+"' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-plus'></i>"+
                                "</a>"+
                            "</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;'>"+
                                "<a class='addComentarioBtn' onclick='btnAddClick("+celdas[21]+")' title='Agregar Comentario' data-id='"+celdas[21]+"' data-target='#addComentario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-comments'></i>"+
                                "</a>"+
                            "</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: initial;'>"+
                                "<a class='historialCometarioBtn' onclick='btnHistorialClick("+celdas[21]+")' title='Historial comentarios' data-id='"+celdas[21]+"' data-target='#historialCometario' data-toggle='modal' style='font-size: 18px;color: black;'>"+
                                    "<i class='fas fa-info'></i>"+
                                "</a>"+
                            "</td>"+
                        "</tr>");
                    }

                }

                //Limpiamos el input
                // $("#busqueda_tabla").val("");

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#campos_buscar").css("color","black");
            }else{
                tabla.append("<tr style='font-size:13px;'>"+
                    "<td align='center' colspan='17' style='width:100%;'>No se encontraron registros de la busqueda</td>"+
                    "</td>"+
                "</tr>");

                // console.log(resp);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#campos_buscar").css("color","black");

            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            $("#campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
}

//Modales para los comentarios (agregar)
function btnAddClick(idMagic){
    // var idMagic = $(this).data("id");
    var hoy = new Date();
    var hora = hoy.getHours()+":"+hoy.getMinutes()
    console.log("Id de Magic: "+idMagic);
    $("#idMagigAdd").val(idMagic);
    $("#titulo").val("");
    $("#HoraComentario").val(hora);
    $("#comentario").val("");
}

//Enviamos el comentario por ajax
$("#btnEnviarComentario").on('click',function(){
    //Recuperamos los valores para enviar
    var idHistorial = $("#idMagigAdd").val();
    var titulo = $("#titulo").val();
    var hora = $("#HoraComentario").val();
    var fecha = $("#fechaComentario").val();
    var comentario = $("#comentario").val();

    //Hacemos la peticion ajax de la busqueda
    var url = $("#basePeticion").val();
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");

    $.ajax({
        url: url+"multipunto/ProactivoAnalisisHistorial/comentarioMagic", 
        method: 'post',
        data: {
            idHistorial : idHistorial,
            titulo : titulo,
            hora : hora,
            fecha : fecha,
            comentario : comentario,
        },
        success:function(resp){
            console.log(resp);

            if (resp.indexOf("handler			</p>") < 1) {
                if (resp == "OK") {
                    $('#OkResult').fadeIn(1000);
                    $('#OkResult').fadeOut(2000);
                }else {
                    $('errorResult').fadeIn(1000);
                    $('errorResult').fadeOut(2000);
                }
            }
            $(".cargaIcono").css("display","none");
            limpiar();
        //Cierre de success
        },
        error:function(error){
            console.log(error);
            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Limpiamos modal de comentario
function limpiar(){
    var hoy = new Date();
    var hora = hoy.getHours()+":"+hoy.getMinutes();
    $("#titulo").val("");
    $("#HoraComentario").val(hora);
    $("#comentario").val("");
};

//Modales para los comentarios (historial)
// $(".historialCometarioBtn").on('click',function(){
function btnHistorialClick(idHistorial){
    // var idMagic = $(this).data("id");
    console.log("Id de Magic Historia: "+idHistorial);
    $("#idMagigHistorial").val(idHistorial);

    //Enviamos el id de magic para recuperar los comentarios
    //Hacemos la peticion ajax de la busqueda
    var url = $("#basePeticion").val();

    $.ajax({
        url: url+"multipunto/ProactivoAnalisisHistorial/loadHistoryMagic",
        method: 'post',
        data: {
            idHistorial : idHistorial,
        },
        success:function(resp){
            // console.log(resp);

            //identificamos la tabla a afectar
            var tabla = $("#tablaComnetario");
            tabla.empty();

            if ((resp.indexOf("handler			</p>") < 1)&&(resp.length > 10)) {
                //Fraccionamos la respuesta en renglones
                var campos = resp.split("|");

                //Recorremos los "renglones" obtenidos para partir los campos
                var registro = ( campos.length > 1) ? campos.length -1 : campos.length;

                for (var i = 0; i < registro; i++) {
                    var celdas = campos[i].split("_");
                    // console.log(celdas);
                    if (celdas.length>2) {
                        tabla.append("<tr style='font-size:13px;'>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 25%;font-family: Arial;'>"+celdas[0].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 50%;font-family: Arial;'>"+celdas[1].toUpperCase()+"</td>"+
                            "<td align='center' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 25%;font-family: Arial;'>"+celdas[2].toUpperCase()+"</td>"+
                        "</tr>");
                    }

                }
            }else{
                tabla.append("<tr style='font-size:13px;'>"+
                    "<td align='center' colspan='3' style='vertical-align:middle;font-weight: 400;font-size: 15px; width: 100%;font-family: Arial;'>No se encontraron registros de la busqueda</td>"+
                    "</td>"+
                "</tr>");
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}
