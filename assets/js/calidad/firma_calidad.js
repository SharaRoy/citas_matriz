$(document).ready(function() {
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 500) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.53;
        console.log("Tablet");
    }else{
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var destino = $('#destinoFirma').val();
        // console.log(destino);

    		//Designamos el destino de la firma en la vista
    		$('#firmaJefeTaller').val(data);
    		$("#firmaJDT").attr("src",data);

        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
        var destino = $('#destinoFirma').val();
        // console.log(destino);

		//Designamos el destino de la firma en la vista
		$('#firmaJefeTaller').val("");
		$("#firmaJDT").attr("src","");

    });

});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    //Pasamos el valor al formulario local
    $('#destinoFirma').val(firma);

  	//Cambiamos el encabezado del modal
  	$("#firmaDigitalLabel").text("FIRMA DEL JEFE DE TALLER");
});

// Buscador dinamico para la tabla

$('#busqueda_tabla').keyup(function () {
    toSearch();
});

//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}
