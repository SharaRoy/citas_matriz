// Lado izquierdo de la pantalla
//Trabajo de afinacion
$('.motorCheck').change(function(){
    if($(this).is(':checked')){
        $('.motorCheck').not(this).prop('checked', false);
    }
});

$('.bujiaCheck').change(function(){
    if($(this).is(':checked')){
        $('.bujiaCheck').not(this).prop('checked', false);
    }
});

$('.filtroCombustibleCheck').change(function(){
    if($(this).is(':checked')){
        $('.filtroCombustibleCheck').not(this).prop('checked', false);
    }
});

$('.fAireCheck').change(function(){
    if($(this).is(':checked')){
        $('.fAireCheck').not(this).prop('checked', false);
    }
});

$('.tiempoEnCheck').change(function(){
    if($(this).is(':checked')){
        $('.tiempoEnCheck').not(this).prop('checked', false);
    }
});

$('.cBujiaCheck').change(function(){
    if($(this).is(':checked')){
        $('.cBujiaCheck').not(this).prop('checked', false);
    }
});

//Trabajo de frenos
$('.balatasCheck').change(function(){
    if($(this).is(':checked')){
        $('.balatasCheck').not(this).prop('checked', false);
    }
});

$('.pCarreteraCheck').change(function(){
    if($(this).is(':checked')){
        $('.pCarreteraCheck').not(this).prop('checked', false);
    }
});

$('.nLiquidoCheck').change(function(){
    if($(this).is(':checked')){
        $('.nLiquidoCheck').not(this).prop('checked', false);
    }
});

$('.ruidosFrenosCheck').change(function(){
    if($(this).is(':checked')){
        $('.ruidosFrenosCheck').not(this).prop('checked', false);
    }
});

$('.pedalFrenoCheck').change(function(){
    if($(this).is(':checked')){
        $('.pedalFrenoCheck').not(this).prop('checked', false);
    }
});

//Trabajo de alineación
$('.alineacion_1Check').change(function(){
    if($(this).is(':checked')){
        $('.alineacion_1Check').not(this).prop('checked', false);
    }
});

$('.alineacion_2Check').change(function(){
    if($(this).is(':checked')){
        $('.alineacion_2Check').not(this).prop('checked', false);
    }
});

$('.alineacion_3Check').change(function(){
    if($(this).is(':checked')){
        $('.alineacion_3Check').not(this).prop('checked', false);
    }
});

$('.alineacion_4Check').change(function(){
    if($(this).is(':checked')){
        $('.alineacion_4Check').not(this).prop('checked', false);
    }
});

$('.alineacion_5Check').change(function(){
    if($(this).is(':checked')){
        $('.alineacion_5Check').not(this).prop('checked', false);
    }
});

$('.alineacion_6Check').change(function(){
    if($(this).is(':checked')){
        $('.alineacion_6Check').not(this).prop('checked', false);
    }
});

//Trabajo de AA
$('.trabajoAA_1Check').change(function(){
    if($(this).is(':checked')){
        $('.trabajoAA_1Check').not(this).prop('checked', false);
    }
});

$('.trabajoAA_2Check').change(function(){
    if($(this).is(':checked')){
        $('.trabajoAA_2Check').not(this).prop('checked', false);
    }
});

$('.trabajoAA_3Check').change(function(){
    if($(this).is(':checked')){
        $('.trabajoAA_3Check').not(this).prop('checked', false);
    }
});

$('.trabajoAA_4Check').change(function(){
    if($(this).is(':checked')){
        $('.trabajoAA_4Check').not(this).prop('checked', false);
    }
});

//Lado Derecho del formulario
// Revision general
$('.general_1Check').change(function(){
    if($(this).is(':checked')){
        $('.general_1Check').not(this).prop('checked', false);
    }
});

$('.general_2Check').change(function(){
    if($(this).is(':checked')){
        $('.general_2Check').not(this).prop('checked', false);
    }
});

$('.general_3Check').change(function(){
    if($(this).is(':checked')){
        $('.general_3Check').not(this).prop('checked', false);
    }
});

$('.general_4Check').change(function(){
    if($(this).is(':checked')){
        $('.general_4Check').not(this).prop('checked', false);
    }
});

$('.general_5Check').change(function(){
    if($(this).is(':checked')){
        $('.general_5Check').not(this).prop('checked', false);
    }
});

$('.general_6Check').change(function(){
    if($(this).is(':checked')){
        $('.general_6Check').not(this).prop('checked', false);
    }
});

//Trabajos transmision automatica
$('.transmision_auto_1Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_1Check').not(this).prop('checked', false);
    }
});

$('.transmision_auto_2Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_2Check').not(this).prop('checked', false);
    }
});

$('.transmision_auto_3Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_3Check').not(this).prop('checked', false);
    }
});

$('.transmision_auto_4Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_4Check').not(this).prop('checked', false);
    }
});

$('.transmision_auto_5Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_5Check').not(this).prop('checked', false);
    }
});

$('.transmision_auto_6Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_6Check').not(this).prop('checked', false);
    }
});
$('.transmision_auto_7Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_7Check').not(this).prop('checked', false);
    }
});

$('.transmision_auto_8Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_8Check').not(this).prop('checked', false);
    }
});

$('.transmision_auto_9Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_9Check').not(this).prop('checked', false);
    }
});

$('.transmision_auto_10Check').change(function(){
    if($(this).is(':checked')){
        $('.transmision_auto_10Check').not(this).prop('checked', false);
    }
});

//Trabajos de lavado
$('.lavado_1Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_1Check').not(this).prop('checked', false);
    }
});

$('.lavado_2Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_2Check').not(this).prop('checked', false);
    }
});

$('.lavado_3Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_3Check').not(this).prop('checked', false);
    }
});

$('.lavado_4Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_4Check').not(this).prop('checked', false);
    }
});

$('.lavado_5Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_5Check').not(this).prop('checked', false);
    }
});

$('.lavado_6Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_6Check').not(this).prop('checked', false);
    }
});
$('.lavado_7Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_7Check').not(this).prop('checked', false);
    }
});

$('.lavado_8Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_8Check').not(this).prop('checked', false);
    }
});

$('.lavado_9Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_9Check').not(this).prop('checked', false);
    }
});

$('.lavado_10Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_10Check').not(this).prop('checked', false);
    }
});

$('.lavado_11Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_11Check').not(this).prop('checked', false);
    }
});

$('.lavado_12Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_12Check').not(this).prop('checked', false);
    }
});

$('.lavado_13Check').change(function(){
    if($(this).is(':checked')){
        $('.lavado_13Check').not(this).prop('checked', false);
    }
});
