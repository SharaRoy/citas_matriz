//Botones para crear refacciones bajo el rol del tecnico
//Saku1994
//Agregar refacciones por parte del tecnico
$("#agregarRefaccionTec").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Recuperamos los campos obligatorios
    var condi_1 = $("#cantidad_"+indice).val();
    var condi_2 = $("#descripcion_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ( (condi_1 != "") && (condi_2 != "") ){
        var tabla = $("#cuerpoMateriales");

        var tipo_orden = "";
        if ($("#ordenT").length) {
            tipo_orden = $("#ordenT").val();
        }

        $(".aceptarCotizacion").attr("disabled",false);
        // console.log("Paso validacion");
        //Evitamos la edicion de los  elementos        
        $("#cantidad_"+indice).attr('disabled','disabled');
        $("#descripcion_"+indice).attr('disabled','disabled');
        $("#costoCM_"+indice).attr('disabled','disabled');
        $("#horas_"+indice).attr('disabled','disabled');
        $("#totalReng_"+indice).attr('disabled','disabled');
        $("#pieza_"+indice).attr('disabled','disabled');
        $("#prioridad_"+indice).attr('disabled','disabled');
        $("input[name='existe_"+indice+"']").attr('disabled','disabled');

        //Nuevo campo
        $("#ref_"+indice).attr('disabled','disabled');

        //Bloqueamos el boton de buscar anterior
        $("#refaccionBusqueda_"+indice).attr("disabled", true);
        $("#refaccionBusqueda_"+indice).attr("data-toggle", false);

        //Mostramos el boton para eliminar fila
        $("#eliminarCM").attr('disabled','disabled');

        //Recuperamos los valores de toda la fila
        var fila = "";
        //0 Cantidad
        fila += parseFloat($("#cantidad_"+indice).val())+"_";
        //1 Descripcion
        fila += $("#descripcion_"+indice).val().replace('_', '-')+"_";
        //2 NUM. PIEZA
        fila += $("#pieza_"+indice).val().replace('_', '-')+"_";
        //3 EXISTE
        if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
            fila += $("input[name='existe_"+indice+"']:checked").val()+"_";
        }else {
            fila += ""+"_";
        }
        //4 PRECIO / P
        fila += parseFloat($("#costoCM_"+indice).val())+"_";
        //5 HORAS
        fila += parseFloat($("#horas_"+indice).val())+"_";
        //6 COSTO MO
        fila += parseFloat($("#totalReng_"+indice).val())+"_";

        var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
        var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
        
        //7 TOTAL RENGLON
        var total = refaccion+mo;

        fila += total.toFixed(2)+"_";

        //8 PRIORIDAD DE LA REFACCION
        fila += $("#prioridad_"+indice).val()+"_";

        //9 REPARACION
        fila += $("#ref_"+indice).val()+"_";

        //Si es una orden de bodyshop agregamos un campo
        if (tipo_orden == "3") {
            //Costo Aseguradora
            fila += $("#costoaseg_"+indice).val()+"_";
        }else{
            fila += "_";
        }

        if( $('#rep_add_'+indice).is(':checked') ) {
            fila += "1_";
        } else{
            fila += "0_";
        }

        $("#total_renglon_label_"+indice).text(total.toFixed(2));
        //Vaciamos lo recolectado en la variable
        $("#valoresCM_"+indice).val(fila);
        console.log(fila.split("_"));

        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());

        //Calculamos los nuevos valores
        var nvo_subtotal = subtotal + total;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;

        //Imprimimos los valores
        $("#subTotalMaterialLabel").text(nvo_subtotal.toFixed(2));
        $("#subTotalMaterial").val(nvo_subtotal);

        $("#ivaMaterialLabel").text(iva.toFixed(2));
        $("#ivaMaterial").val(iva);

        $("#totalMaterialLabel").text(presupuesto.toFixed(2));
        $("#totalMaterial").val(presupuesto);

        $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));

        //Aumentamos el indice para los renglones
        indice++;
        $("#indiceTablaMateria").val(indice);

        //confirmamos el tipo de formulario
        if ($("#cargaFormulario").val() == "1") {
            var prp = "disabled";
        }else {
            var prp = "";
        }

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla.append("<tr id='fila_"+indice+"'>"+
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='1' class='input_field_lg' id='ref_"+indice+"' name='ref_"+indice+"' style='width:90%;text-align:left;color:black;'></textarea>"+
            "</td>"+
            "<td align='center' style='border:1px solid #337ab7;'>"+
                "<input type='number' step='any' min='0'  class='input_field' id='cantidad_"+indice+"' name='cantidad_"+indice+"' value='1' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:90%;color:black;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='2' class='input_field_lg' id='descripcion_"+indice+"' name='descripcion_"+indice+"' style='width:90%;text-align:left;color:black;'></textarea>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='2' class='input_field_lg' id='pieza_"+indice+"' name='pieza_"+indice+"' style='width:90%;text-align:left;color:black;'></textarea>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' class='input_field' id='apuntaSi_"+indice+"' value='SI' name='existe_"+indice+"' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' id='apuntaNo_"+indice+"' class='input_field' name='existe_"+indice+"' value='NO' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' id='apuntaPlanta_"+indice+"' class='input_field' name='existe_"+indice+"' value='PLANTA' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "$<input type='number' step='any' min='0' class='input_field' id='costoCM_"+indice+"' name='costoRef_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='color:black;width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;"+((tipo_orden != '3') ? 'display: none;' : '')+"' align='center'>"+
                "$<input type='number' step='any' min='0' class='input_field' id='costoaseg_"+indice+"' onblur='actualizaValores("+indice+")' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='color:black;width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='number' step='any' min='0' class='input_field' id='horas_"+indice+"' name='hotasMo_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='color:black;width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "$<input type='number' step='any' min='0' id='totalReng_"+indice+"' name='totalReng_"+indice+"' class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='990' style='color:black;width:90%;text-align:left;' disabled>"+
                "<input type='hidden' id='totalOperacion_"+indice+"' value='0'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<label id='total_renglon_label_"+indice+"' style='color:darkblue;'>$0.00</label>"+
            "</td>"+
            "<td style='display:none;border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='number' step='any' min='0' class='input_field' id='prioridad_"+indice+"' name='prioridad_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='2' style='width:70%;text-align:center;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='checkbox' class='input_field' id='check_g_"+indice+"' onclick='habilitar_rep("+indice+")'  name='pzaGarantia[]' value='"+indice+"' style='transform: scale(1.5);'>"+
                "<input type='hidden' id='valoresCM_"+indice+"' name='registros[]'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input id='rep_add_"+indice+"' type='checkbox' onclick='actualiza_fila("+indice+")' class='input_field' value='"+indice+"' name='rep_add[]' style='transform: scale(1.5);' disabled>"+
            "</td>"+
        "</tr>");
    }else {
        //Regresamos el control al campo que falta rellenar
        if (condi_1 == "") {
            $("#cantidad_"+indice).focus();
        }else{
            $("#descripcion_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada
$("#eliminarRefaccionTec").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        var indice_anterior = indice - 1;

        //Restamos el monto del renglon eliminado
        var refaccion = parseFloat($("#cantidad_"+indice_anterior).val()) * parseFloat($("#costoCM_"+indice_anterior).val());
        var mo = parseFloat($("#horas_"+indice_anterior).val()) * parseFloat($("#totalReng_"+indice_anterior).val());
        
        var total = refaccion+mo;
        //console.log("total a eliminar: "+total);
        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());
        //console.log("subtotal actual: "+subtotal);
        //Calculamos los nuevos valores
        var nvo_subtotal = subtotal - total;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;
        //console.log("subtotal nuevo: "+nvo_subtotal);
        //console.log("iva nuevo: "+iva);
        //console.log("total nuevo: "+presupuesto);

        //Verificamos que los resultados no sean negativos para imprimirlos
        if (nvo_subtotal > -1) {
            //Imprimimos los valores
            $("#subTotalMaterialLabel").text(nvo_subtotal.toFixed(2));
            $("#subTotalMaterial").val(nvo_subtotal);

            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#ivaMaterial").val(iva);

            $("#totalMaterialLabel").text(presupuesto.toFixed(2));
            $("#totalMaterial").val(presupuesto);

            $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));
        }
        
        //Eliminamos el renglon
        var fila = $("#fila_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceTablaMateria").val(indice);

        //Bloqueamos el envio del presupuesto si este no tiene informacion
        if (indice == 1) {
            $(".aceptarCotizacion").attr("disabled",true);
        }

        //Restablecemos los valores default del reglon anterior
        $("#cantidad_"+indice).val("1");
        $("#descripcion_"+indice).val("");
        $("#pieza_"+indice).val("");
        $("#costoCM_"+indice).val("0");
        $("#horas_"+indice).val("0");
        $("#totalOperacion_"+indice).val("0")
        $("#totalReng_"+indice).val("990");
        $("#prioridad_"+indice).val("2");
        $("#total_renglon_label_"+indice).text("$0.00");

        $("input[name='existe_"+indice+"']:radio").attr('disabled',false);

        //Desbloqueamos el renglon para la edición
        $("#cantidad_"+indice).attr('disabled',false);
        $("#descripcion_"+indice).attr('disabled',false);
        $("#costoCM_"+indice).attr('disabled',false);
        $("#horas_"+indice).attr('disabled',false);
        $("#pieza_"+indice).attr('disabled',false);
        $("#prioridad_"+indice).attr('disabled',false);
        $("input[name='existe_"+indice+"']").attr('disabled',false);
        $("#totalReng_"+indice).attr('disabled',false);
    }else {
        $(".aceptarCotizacion").attr("disabled",true);
    }
});

//************************************************************************//

//Botones para crear refacciones bajo el rol de ventanilla
//Agregar refacciones por parte de VENTANILLA
$("#agregarRefaccionRef").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Recuperamos los campos obligatorios
    var condi_1 = $("#cantidad_"+indice).val();
    var condi_2 = $("#descripcion_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ( (condi_1 != "") && (condi_2 != "") ){
        var tabla = $("#cuerpoMateriales");

        //Bloqueamos el boton de buscar anterior
        $("#refaccionBusqueda_"+indice).attr("disabled", true);
        $("#refaccionBusqueda_"+indice).attr("data-toggle", false);

        //Mostramos el boton para eliminar fila
        $("#eliminarCM").attr('disabled','disabled');

        //Recuperamos los valores de toda la fila
        var fila = "";
        //0 Cantidad
        fila += parseFloat($("#cantidad_"+indice).val())+"_";
        //1 Descripcion
        fila += $("#descripcion_"+indice).val().replace('_', '-')+"_";
        //2 NUM. PIEZA
        fila += $("#pieza_"+indice).val().replace('_', '-')+"_";
        //3 EXISTE
        if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
            fila += $("input[name='existe_"+indice+"']:checked").val()+"_";
        }else {
            fila += ""+"_";
        }
        //4 PRECIO / P
        fila += parseFloat($("#costoCM_"+indice).val())+"_";
        //5 HORAS
        fila += parseFloat($("#horas_"+indice).val())+"_";
        //6 COSTO MO
        fila += parseFloat($("#totalReng_"+indice).val())+"_";

        var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
        var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
        
        //7 TOTAL RENGLON
        var total = refaccion+mo;

        fila += total.toFixed(2)+"_";

        //8 PRIORIDAD DE LA REFACCION
        fila += $("#prioridad_"+indice).val()+"_";

        //9 REPARACION
        fila += $("#ref_"+indice).val()+"_";

        console.log("click");

        $("#total_renglon_label_"+indice).text(total.toFixed(2));
        //Vaciamos lo recolectado en la variable
        $("#valoresCM_"+indice).val(fila);
        console.log(fila);

        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());

        //Calculamos los nuevos valores
        var nvo_subtotal = subtotal + total;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;

        //Imprimimos los valores
        $("#subTotalMaterialLabel").text(nvo_subtotal.toFixed(2));
        $("#subTotalMaterial").val(nvo_subtotal);

        $("#ivaMaterialLabel").text(iva.toFixed(2));
        $("#ivaMaterial").val(iva);

        $("#totalMaterialLabel").text(presupuesto.toFixed(2));
        $("#totalMaterial").val(presupuesto);

        $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));

        //Aumentamos el indice para los renglones
        indice++;
        $("#indiceTablaMateria").val(indice);

        //confirmamos el tipo de formulario
        if ($("#cargaFormulario").val() == "1") {
            var prp = "disabled";
        }else {
            var prp = "";
        }

        var hoy = new Date();

        var mes = hoy.getMonth()+1;
        var dia = hoy.getDate();

        var fecha = hoy.getFullYear() + '/' +(mes<10 ? '0' : '') + mes + '/' +(dia<10 ? '0' : '') + dia;

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla.append("<tr id='fila_"+indice+"'>"+
                //<!-- REF -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' id='ref_"+indice+"' name='ref_"+indice+"' style='width:90%;text-align:left;color:black;'></textarea>"+
                "</td>"+
                //<!-- CANTIDAD -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<input type='number' step='any' min='0' class='input_field' onblur='actualizaValores("+indice+")' id='cantidad_"+indice+"' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>"+
                "</td>"+
                //<!-- D E S C R I P C I Ó N -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' onblur='actualizaValores("+indice+")' id='descripcion_"+indice+"' style='width:90%;text-align:left;'></textarea>"+
                "</td>"+

                //<!-- GRUPO -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes("+indice+")' id='grupo_"+indice+"' style='width:90%;text-align:left;'>FM</textarea>"+
                "</td>"+
                //<!-- PREFIJO -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes("+indice+")' id='prefijo_"+indice+"' style='width:90%;text-align:left;'></textarea>"+
                "</td>"+
                //<!-- BASICO -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes("+indice+")' id='basico_"+indice+"' style='width:90%;text-align:left;'></textarea>"+
                "</td>"+
                //<!-- SUFIJO -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes("+indice+")' id='sufijo_"+indice+"' style='width:90%;text-align:left;'></textarea>"+
                "</td>"+

                //<!-- NUM. PIEZA -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' onblur='actualizaValores("+indice+")' id='pieza_"+indice+"' style='width:90%;text-align:left;'></textarea>"+
                "</td>"+
                //<!-- EXISTE -->
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<input type='radio' id='apuntaSi_"+indice+"' class='input_field' onclick='actualizaValores("+indice+")' name='existe_"+indice+"' value='SI' style='transform: scale(1.5);'>"+
                "</td>"+
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<input type='radio' id='apuntaNo_"+indice+"' class='input_field' onclick='actualizaValores("+indice+")' name='existe_"+indice+"' value='NO' style='transform: scale(1.5);'>"+
                "</td>"+
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<input type='radio' id='apuntaPlanta_"+indice+"' class='input_field' onclick='actualizaValores("+indice+")' name='existe_"+indice+"' value='PLANTA' style='transform: scale(1.5);'"+
                "</td>"+
                //<!-- FECHA TENTANTIVA - ENTREGA -->
                "<td style='border:1px solid #337ab7;' align='center'> "+
                    "<input type='date' class='input_field' min='"+fecha+"' id='fecha_tentativa_"+indice+"' value='"+fecha+"' style='display: none;'> "+
                "</td>"+
                //<!-- PRECIO REFACCIÓN -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "$<input type='number' step='any' min='0' class='input_field' onblur='actualizaValores("+indice+")' id='costoCM_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
                "</td>"+
                //<!-- HORAS -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<input type='number' step='any' min='0' class='input_field' onblur='actualizaValores("+indice+")' id='horas_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
                "</td>"+
                //<!-- COSTO MO -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "$<input type='number' step='any' min='0' id='totalReng_"+indice+"' onblur='actualizaValores("+indice+")'  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='990' style='width:90%;text-align:left;' disabled>"+
                    
                    //<!-- total de la refaccion -->
                    "<input type='hidden' id='totalOperacion_"+indice+"' value='0'>"+

                    //<!-- Indice interno de la recaccion -->
                    "<input type='hidden' id='id_refaccion_"+indice+"' name='id_registros[]' value='0'>"+

                    //<!-- Informacion del renglon -->
                    "<input type='hidden' id='valoresCM_"+indice+"' name='registros[]' value=''>"+

                    //<!-- Informacion del aprueba jefe de taller -->
                    "<input type='hidden' id='aprueba_jdt_"+indice+"' value='0'>"+
                "</td>"+
                //<!-- TOTAL DE REPARACÓN -->
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<label id='total_renglon_label_"+indice+"' style='color:darkblue;'>$0.00</label>"+
                "</td>"+
                //<!-- PRIORIDAD <br> REFACCION -->
                //<!--<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                    //<input type='number' step='any' min='0' class='input_field' id='prioridad_"+indice+"' name='prioridad_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='2' style='width:70%;text-align:center;'>
                //</td>-->
                //<!-- PZA. GARANTÍA -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<input type='checkbox' class='input_field' name='pzaGarantia[]' onclick='refaccionGarantia("+indice+")' value='"+indice+"' id='garantia_check_"+indice+"' style='transform: scale(1.5);' >"+

                    //<!-- Informacion pieza con garantia -->
                    "<input type='hidden' name='garantia[]' id='garantia_"+indice+"' value='0'>"+
                    //<!-- Informacion de prioridad de la refaccion -->
                    "<input type='hidden' name='prioridad_"+indice+"' id='prioridad_"+indice+"' value='2'>"+

                    "<input type='hidden' id='valoresGarantias_"+indice+"' name='registrosGarantias[]' value='0_0_0'>"+
                "</td>"+
                //<!-- REPARACÓN ADICIONAL -->
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<label style='color:darkblue;'>NO</label>"+
                "</td>"+
                //<!-- BUSCAR REFACCION EN INVENTARIO -->
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    //<!--<a id='refaccionBusqueda_"+indice+"' class='refaccionesBox btn btn-warning' data-id='"+indice+"' data-target='#refacciones' data-toggle='modal' style='color:white;'>
                        //<i class='fa fa-search'></i>
                    //</a>-->
                "</td>"+
        "</tr>");

    }else {
        //Regresamos el control al campo que falta rellenar
        if (condi_1 == "") {
            $("#cantidad_"+indice).focus();
        }else{
            $("#descripcion_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada
$("#eliminarRefaccionRef").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();
    var limite_edicion = $("#indice_limite").val();

    //Verificamos que no este en el primer renglón
    if (indice > limite_edicion) {
        var indice_anterior = indice - 1;

        //Restamos el monto del renglon eliminado
        var refaccion_rg = parseFloat($("#cantidad_"+indice_anterior).val()) * parseFloat($("#costoCM_"+indice_anterior).val());
        var mo_rg = parseFloat($("#horas_"+indice_anterior).val()) * parseFloat($("#totalReng_"+indice_anterior).val());
        
        var total_rg = refaccion_rg+mo_rg;

        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());
        var nvo_subtotal_rg = subtotal - total_rg;
        var iva_rg = nvo_subtotal_rg * 0.16;
        var presupuesto_rg = nvo_subtotal_rg * 1.16;

        //Verificamos que los resultados no sean negativos para imprimirlos
        if (nvo_subtotal_rg > -1) {
            //Imprimimos los valores
            $("#subTotalMaterialLabel").text(nvo_subtotal_rg.toFixed(2));
            $("#subTotalMaterial").val(nvo_subtotal_rg);

            $("#ivaMaterialLabel").text(iva_rg.toFixed(2));
            $("#ivaMaterial").val(iva_rg);

            $("#totalMaterialLabel").text(presupuesto_rg.toFixed(2));
            $("#totalMaterial").val(presupuesto_rg);

            $("#presupuestoMaterialLabel").text(presupuesto_rg.toFixed(2));
        }

        //Verificamos si fue una refaccion marcada con garantia
        if ($('#garantia_check_'+indice).is(':checked') ) {
            console.log("Renglon con garantia, para eliminar");
            //Recuperamos los totales de la tabla de garantia
            //Calculamos los nuevos totales
            var subtotal = 0;

            for (var i = 1; i < indice ; i++) {
                //Verificamos que la refaccion este aprovada por el jefe de taller
                if ($("#garantia_"+i).val() == "1") {
                    //Como alternativa, se puede hacer la cuenta completa por renglon
                    var refaccion = parseFloat($("#tb2_cantidad_"+i).val()) * parseFloat($("#tb2_costoCM_"+i).val());
                    var mo = parseFloat($("#tb2_horas_"+i).val()) * parseFloat($("#tb2_totalReng_"+i).val());

                    var totalRenglon = refaccion+mo;
                    //console.log(totalRenglon);
                    subtotal += totalRenglon;
                }
            }

            var iva = subtotal * 0.16;
            var presupuesto = subtotal * 1.16;

            //Vaciamos los nuevos resultados
            $("#subTotalMaterialLabel_tabla2").text(subtotal.toFixed(2));
            $("#ivaMaterialLabel_tabla2").text(iva.toFixed(2));
            $("#totalMaterialLabel_tabla2").text(presupuesto.toFixed(2));

            //Eliminamos el renglon
            var tb2_fila = $("#tb2_fila_"+indice);
            tb2_fila.remove();
        }
        
        //Eliminamos el renglon
        var fila = $("#fila_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceTablaMateria").val(indice);

        //Bloqueamos el envio del presupuesto si este no tiene informacion
        if (indice == 1) {
            $(".aceptarCotizacion").attr("disabled",true);
        }

        //Restablecemos los valores default del reglon anterior
        $("#cantidad_"+indice).val("1");
        $("#descripcion_"+indice).val("");
        $("#pieza_"+indice).val("");
        $("#costoCM_"+indice).val("0");
        $("#horas_"+indice).val("0");
        $("#totalOperacion_"+indice).val("0")
        $("#totalReng_"+indice).val("990");
        $("#prioridad_"+indice).val("2");
        $("#total_renglon_label_"+indice).text("$0.00");

        $("input[name='existe_"+indice+"']:radio").attr('disabled',false);

        //Desbloqueamos el renglon para la edición
        $("#cantidad_"+indice).attr('disabled',false);
        $("#descripcion_"+indice).attr('disabled',false);
        $("#costoCM_"+indice).attr('disabled',false);
        $("#horas_"+indice).attr('disabled',false);
        $("#pieza_"+indice).attr('disabled',false);
        $("#prioridad_"+indice).attr('disabled',false);
        $("input[name='existe_"+indice+"']").attr('disabled',false);
        $("#totalReng_"+indice).attr('disabled',false);
    }else {
        $(".aceptarCotizacion").attr("disabled",true);
    }
});

//************************************************************************//

//Botones para crear refacciones bajo el rol del tecnico (Actualizacion)
//Agregar refacciones por parte del tecnico
$("#agregarRefaccionActTec").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Recuperamos los campos obligatorios
    var condi_1 = $("#cantidad_"+indice).val();
    var condi_2 = $("#descripcion_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ( (condi_1 != "") && (condi_2 != "") ){
        var tabla = $("#cuerpoMateriales");

        $(".aceptarCotizacion").attr("disabled",false);
        // console.log("Paso validacion");
        //Evitamos la edicion de los  elementos
        $("#cantidad_"+indice).attr('disabled','disabled');
        $("#descripcion_"+indice).attr('disabled','disabled');
        $("#costoCM_"+indice).attr('disabled','disabled');
        $("#horas_"+indice).attr('disabled','disabled');
        $("#totalReng_"+indice).attr('disabled','disabled');
        $("#pieza_"+indice).attr('disabled','disabled');
        $("#prioridad_"+indice).attr('disabled','disabled');
        $("input[name='existe_"+indice+"']").attr('disabled','disabled');

        $("#ref_"+indice).attr('disabled','disabled');

        //Bloqueamos el boton de buscar anterior
        $("#refaccionBusqueda_"+indice).attr("disabled", true);
        $("#refaccionBusqueda_"+indice).attr("data-toggle", false);

        //Mostramos el boton para eliminar fila
        $("#eliminarCM").attr('disabled','disabled');

        //Recuperamos los valores de toda la fila
        var fila = "";

        var tipo_orden = "";
        if ($("#ordenT").length) {
            tipo_orden = $("#ordenT").val();
        }

        //0 Cantidad
        fila += parseFloat($("#cantidad_"+indice).val())+"_";
        //1 Descripcion
        fila += $("#descripcion_"+indice).val().replace('_', '-')+"_";
        //2 NUM. PIEZA
        fila += $("#pieza_"+indice).val().replace('_', '-')+"_";
        //3 EXISTE
        if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
            fila += $("input[name='existe_"+indice+"']:checked").val()+"_";
        }else {
            fila += ""+"_";
        }
        //4 PRECIO / P
        fila += parseFloat($("#costoCM_"+indice).val())+"_";
        //5 HORAS
        fila += parseFloat($("#horas_"+indice).val())+"_";
        //6 COSTO MO
        fila += parseFloat($("#totalReng_"+indice).val())+"_";

        var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
        var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
        
        //7 TOTAL RENGLON
        var total = refaccion+mo;

        fila += total.toFixed(2)+"_";

        //8 PRIORIDAD DE LA REFACCION
        fila += $("#prioridad_"+indice).val()+"_";
        //9 REPARACION
        fila += $("#ref_"+indice).val()+"_";

        //Si es una orden de bodyshop agregamos un campo
        if (tipo_orden == "3") {
            //Costo Aseguradora
            fila += $("#costoaseg_"+indice).val()+"_";
        }else{
            fila += "_";
        }

        if( $('#rep_add_'+indice).is(':checked') ) {
            fila += "1_";
        } else{
            fila += "0_";
        }

        $("#total_renglon_label_"+indice).text(total.toFixed(2));
        //Vaciamos lo recolectado en la variable
        $("#valoresCM_"+indice).val(fila);
        console.log(fila.split("_"));

        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());

        //Calculamos los nuevos valores
        var nvo_subtotal = subtotal + total;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;

        //Imprimimos los valores
        $("#subTotalMaterialLabel").text(nvo_subtotal.toFixed(2));
        $("#subTotalMaterial").val(nvo_subtotal);

        $("#ivaMaterialLabel").text(iva.toFixed(2));
        $("#ivaMaterial").val(iva);

        $("#totalMaterialLabel").text(presupuesto.toFixed(2));
        $("#totalMaterial").val(presupuesto);

        $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));

        //Aumentamos el indice para los renglones
        indice++;
        $("#indiceTablaMateria").val(indice);

        //confirmamos el tipo de formulario
        if ($("#cargaFormulario").val() == "1") {
            var prp = "disabled";
        }else {
            var prp = "";
        }

        var hoy = new Date();

        var mes = hoy.getMonth()+1;
        var dia = hoy.getDate();

        var fecha = hoy.getFullYear() + '/' +(mes<10 ? '0' : '') + mes + '/' +(dia<10 ? '0' : '') + dia;

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla.append("<tr id='fila_"+indice+"'>"+
                //<!-- REP -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' id='ref_"+indice+"' name='ref_"+indice+"' style='width:90%;text-align:left;color:black;'></textarea>"+
                "</td>"+
                //<!-- CANTIDAD -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico("+indice+")' id='cantidad_"+indice+"' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>"+
                "</td>"+
                //<!-- D E S C R I P C I Ó N -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' onblur='actualizaValoresTecnico("+indice+")' id='descripcion_"+indice+"' style='width:90%;text-align:left;'></textarea>"+
                "</td>"+
                //<!-- NUM. PIEZA -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<textarea rows='1' class='input_field_lg' onblur='actualizaValoresTecnico("+indice+")' id='pieza_"+indice+"' style='width:90%;text-align:left;'></textarea>"+
                "</td>"+
                //<!-- EXISTE -->
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<input type='radio' id='apuntaSi_"+indice+"' class='input_field' onclick='actualizaValoresTecnico("+indice+")' name='existe_"+indice+"' value='SI' style='transform: scale(1.5);'>"+
                "</td>"+
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<input type='radio' id='apuntaNo_"+indice+"' class='input_field' onclick='actualizaValoresTecnico("+indice+")' name='existe_"+indice+"' value='NO' style='transform: scale(1.5);'>"+
                "</td>"+
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<input type='radio' id='apuntaPlanta_"+indice+"' class='input_field' onclick='actualizaValoresTecnico("+indice+")' name='existe_"+indice+"' value='PLANTA' style='transform: scale(1.5);'"+
                "</td>"+
                //<!-- FECHA TENTANTIVA - ENTREGA -->
                //"<td style='border:1px solid #337ab7;' align='center'> "+
                //    "<input type='date' class='input_field' min='"+fecha+"' id='fecha_tentativa_"+indice+"' value='"+fecha+"' style='display: none;'> "+
                //"</td>"+
                //<!-- PRECIO REFACCIÓN -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "$<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico("+indice+")' id='costoCM_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
                "</td>"+
                //<!-- HORAS -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico("+indice+")' id='horas_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
                "</td>"+
                //<!-- COSTO MO -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "$<input type='number' step='any' min='0' id='totalReng_"+indice+"' onblur='actualizaValoresTecnico("+indice+")'  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='990' style='width:90%;text-align:left;' disabled>"+
                    
                    //<!-- total de la refaccion -->
                    "<input type='hidden' id='totalOperacion_"+indice+"' value='0'>"+

                    //<!-- Indice interno de la recaccion -->
                    "<input type='hidden' id='id_refaccion_"+indice+"' name='id_registros[]' value='0'>"+

                    //<!-- Informacion del renglon -->
                    "<input type='hidden' id='valoresCM_"+indice+"' name='registros[]' value=''>"+

                    //<!-- Informacion del aprueba jefe de taller -->
                    "<input type='hidden' id='aprueba_jdt_"+indice+"' value='0'>"+
                "</td>"+
                //<!-- TOTAL DE REPARACÓN -->
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<label id='total_renglon_label_"+indice+"' style='color:darkblue;'>$0.00</label>"+
                "</td>"+
                //<!-- PRIORIDAD <br> REFACCION -->
                "<td style='display:none;border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico("+indice+")' id='prioridad_"+indice+"' name='prioridad_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='2' style='width:70%;text-align:center;'>"+
                "</td>"+
                //<!-- PZA. GARANTÍA -->
                "<td style='border:1px solid #337ab7;' align='center'>"+
                    "<input type='checkbox' class='input_field' id='check_g_"+indice+"' onclick='habilitar_rep("+indice+")'  name='pzaGarantia[]' value='"+indice+"' style='transform: scale(1.5);'>"+

                    //<!-- Informacion pieza con garantia -->
                    "<input type='hidden' name='garantia[]' id='garantia_"+indice+"' value='0'>"+
                    //<!-- Informacion de prioridad de la refaccion -->
                    "<input type='hidden' name='prioridad_"+indice+"' id='prioridad_"+indice+"' value='2'>"+

                    "<input type='hidden' id='valoresGarantias_"+indice+"' name='registrosGarantias[]' value='0_0_0'>"+
                "</td>"+
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    "<input id='rep_add_"+indice+"' type='checkbox' onclick='actualiza_fila("+indice+")' class='input_field' value='"+indice+"' name='rep_add[]' style='transform: scale(1.5);' disabled>"+
                "</td>"+
                //<!-- BUSCAR REFACCION EN INVENTARIO -->
                "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                    //<!--<a id='refaccionBusqueda_"+indice+"' class='refaccionesBox btn btn-warning' data-id='"+indice+"' data-target='#refacciones' data-toggle='modal' style='color:white;'>
                        //<i class='fa fa-search'></i>
                    //</a>-->
                "</td>"+
        "</tr>");
    }else {
        //Regresamos el control al campo que falta rellenar
        if (condi_1 == "") {
            $("#cantidad_"+indice).focus();
        }else{
            $("#descripcion_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada
$("#eliminarRefaccionActTec").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();
    var limite_edicion = $("#indice_limite").val();

    //Verificamos que no este en el primer renglón
    if (indice > limite_edicion) {
        var indice_anterior = indice - 1;

        //Restamos el monto del renglon eliminado
        var refaccion = parseFloat($("#cantidad_"+indice_anterior).val()) * parseFloat($("#costoCM_"+indice_anterior).val());
        var mo = parseFloat($("#horas_"+indice_anterior).val()) * parseFloat($("#totalReng_"+indice_anterior).val());
        
        var total = refaccion+mo;
        //console.log("total a eliminar: "+total);
        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());
        //console.log("subtotal actual: "+subtotal);
        //Calculamos los nuevos valores
        var nvo_subtotal = subtotal - total;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;
        //console.log("subtotal nuevo: "+nvo_subtotal);
        //console.log("iva nuevo: "+iva);
        //console.log("total nuevo: "+presupuesto);

        //Verificamos que los resultados no sean negativos para imprimirlos
        if (nvo_subtotal > -1) {
            //Imprimimos los valores
            $("#subTotalMaterialLabel").text(nvo_subtotal.toFixed(2));
            $("#subTotalMaterial").val(nvo_subtotal);

            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#ivaMaterial").val(iva);

            $("#totalMaterialLabel").text(presupuesto.toFixed(2));
            $("#totalMaterial").val(presupuesto);

            $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));
        }
        
        //Eliminamos el renglon
        var fila = $("#fila_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceTablaMateria").val(indice);

        //Bloqueamos el envio del presupuesto si este no tiene informacion
        if (indice == 1) {
            $(".aceptarCotizacion").attr("disabled",true);
        }

        //Restablecemos los valores default del reglon anterior
        $("#cantidad_"+indice).val("1");
        $("#descripcion_"+indice).val("");
        $("#pieza_"+indice).val("");
        $("#costoCM_"+indice).val("0");
        $("#horas_"+indice).val("0");
        $("#totalOperacion_"+indice).val("0")
        $("#totalReng_"+indice).val("990");
        $("#prioridad_"+indice).val("2");
        $("#total_renglon_label_"+indice).text("$0.00");

        $("input[name='existe_"+indice+"']:radio").attr('disabled',false);

        //Desbloqueamos el renglon para la edición
        $("#cantidad_"+indice).attr('disabled',false);
        $("#descripcion_"+indice).attr('disabled',false);
        $("#costoCM_"+indice).attr('disabled',false);
        $("#horas_"+indice).attr('disabled',false);
        $("#pieza_"+indice).attr('disabled',false);
        $("#prioridad_"+indice).attr('disabled',false);
        $("input[name='existe_"+indice+"']").attr('disabled',false);
        $("#totalReng_"+indice).attr('disabled',false);
    }else {
        $(".aceptarCotizacion").attr("disabled",true);
    }
});