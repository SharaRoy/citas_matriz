//Alta de presupuesto para enviar por ajax
//Liberamos los campos cuando se envie la cotizacion
$(".terminarCotizacion_alta").on('click', function (e){
  console.log("Se envia presupuesto alta");
  //Creamos visualmente el efecto de carga
  //Si se hizo bien la conexión, mostramos el simbolo de carga
  $(".cargaIcono").css("display","inline-block");
  //Aparentamos la carga de la tabla
  $("#envio_presupuesto_contenedor").css("background-color","#ddd");

  // Evitamos que salte el enlace.
  e.preventDefault(); 
  /* Creamos un nuevo objeto FormData. Este sustituye al atributo enctype = "multipart/form-data" que, tradicionalmente, se incluía en los formularios (y que aún se incluye, cuando son enviados 
	desde HTML. */
	var paqueteCotizacion = new FormData(document.getElementById('formulario_presupuesto'));
	paqueteCotizacion.append('aceptarCotizacion', $(this).prop('value')); 

	/* Se envia el paquete de datos por ajax. */
	var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"presupuestos/Presupuestos/guardarRegistroTecAjax",
        type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
        contentType: false,
        data: paqueteCotizacion, // Al atributo data se le asigna el objeto FormData.
        processData: false,
        cache: false,
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
            	//Si el registro se guardo correctamente, redireccionamos a la vista de no edicion
              	if (resp == "OK") {
                  //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                  $(".cargaIcono").css("display","none");
                  //Regresamos los colores de la tabla a la normalidad
                  $("#envio_presupuesto_contenedor").css("background-color","white");
              		var id_cita = $('#idOrdenTempCotiza').val();
              		location.href = base+"Alta_Presupuesto/"+id_cita;

              	//De lo contrario, mandamos un mensaje
              	} else {
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    $("#envio_presupuesto_contenedor").css("background-color","white");
              	     $("#errorEnvio").text(resp)
              	}
            }else{
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $(".cargaIcono").css("display","none");
              //Regresamos los colores de la tabla a la normalidad
              $("#envio_presupuesto_contenedor").css("background-color","white");
              $("#errorEnvio").text("ERROR AL ENVIAR EL PRESUPUESTO");
            }
		
		//Cierre de success
        },
          error:function(error){
              console.log(error);
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $(".cargaIcono").css("display","none");
              //Regresamos los colores de la tabla a la normalidad
              $("#envio_presupuesto_contenedor").css("background-color","white");
              $("#errorEnvio").text("ERROR 02 AL ENVIAR EL PRESUPUESTO");
              location.href = base+"Alta_Presupuesto/"+id_cita;
		      }
	   }); 
});


//Liberamos los campos cuando se envie la cotizacion
$("#envio_firma_req").on('click', function (e){
  console.log("Se envia presupuesto requisicion");
  //Creamos visualmente el efecto de carga
  //Si se hizo bien la conexión, mostramos el simbolo de carga
  $(".cargaIcono").css("display","inline-block");
  //Aparentamos la carga de la tabla
  $("#envio_presupuesto_contenedor").css("background-color","#ddd");

  // Evitamos que salte el enlace.
  e.preventDefault(); 
  /* Creamos un nuevo objeto FormData. Este sustituye al atributo enctype = "multipart/form-data" que, tradicionalmente, se incluía en los formularios (y que aún se incluye, cuando son enviados 
  desde HTML. */
  var paqueteCotizacion = new FormData();
  paqueteCotizacion.append('destino_vista', $("input[name='destino_vista']").prop('value')); 
  paqueteCotizacion.append('id_cita', $("input[name='id_cita']").prop('value')); 
  paqueteCotizacion.append('rutaFirmaReq', $("input[name='rutaFirmaReq']").prop('value')); 
  paqueteCotizacion.append('nombre', $("input[name='nombre_encargado']").prop('value')); 
  paqueteCotizacion.append('tipo_ruta', $("input[name='ruta_destino']").prop('value')); 

  /* Se envia el paquete de datos por ajax. */
  var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"presupuestos/Presupuestos/actualiza_firma_requisicion", 
        type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
        contentType: false,
        data: paqueteCotizacion, // Al atributo data se le asigna el objeto FormData.
        processData: false,
        cache: false,
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //Si el registro se guardo correctamente, redireccionamos a la vista de no edicion
                if (resp == "OK") {
                    var destino = $("input[name='ruta_destino']").val();

                    if ($("input[name='destino_vista']").val() == "G") {
                        var pdf = "";
                        if (destino == "1") {
                            pdf = base+"Requisicion_Descarga/"+$("input[name='id_cita']").val();
                            window.open(pdf,"PDF REQUISICIÓN","width=200,height=120,scrollbars=NO");
                        }else if (destino == "2") {
                            pdf = base+"RecibirGarantias_Descarga/"+$("input[name='id_cita']").val();
                            window.open(pdf,"PDF RECEPCIÓN DE PIEZAS","width=200,height=120,scrollbars=NO");
                        }
                    }
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    $("#envio_presupuesto_contenedor").css("background-color","white");
                    setTimeout(location.reload(), 6000); 
                    //location.reload();

                //De lo contrario, mandamos un mensaje
                } else {
                  //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                  $(".cargaIcono").css("display","none");
                  //Regresamos los colores de la tabla a la normalidad
                  $("#envio_presupuesto_contenedor").css("background-color","white");
                  $("#errorEnvio").text(resp)
                }
            }else{
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $(".cargaIcono").css("display","none");
              //Regresamos los colores de la tabla a la normalidad
              $("#envio_presupuesto_contenedor").css("background-color","white");
              $("#errorEnvio").text("ERROR AL ENVIAR EL PRESUPUESTO");
            }
    
        //Cierre de success
        },
          error:function(error){
              console.log(error);
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $(".cargaIcono").css("display","none");
              //Regresamos los colores de la tabla a la normalidad
              $("#envio_presupuesto_contenedor").css("background-color","white");
              $("#errorEnvio").text("ERROR 02 AL ENVIAR EL PRESUPUESTO");
          }
     }); 
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Comentarios previos a la firma de requisicion de garantias
$("#vaciado_refacciones").change(function(){
    var info = $("#vaciado_refacciones").val();
    var refaccion = info.split("||");

    if (info != "") {
        $("#comentario_pieza").attr('disabled',false);
        $("#parte_id_refaccion").val(refaccion[0]);
        $("#comentario_pieza").val(refaccion[1]);

        $("#comentario_asc").attr('disabled',false);
    }else{
        $("#error_formato").text("No se selecciono ninguna refacción");  childNodes
    }
});

$("#enviar_comentario_req").on('click', function (e){
    // Evitamos que salte el enlace.
    e.preventDefault(); 

    var valida_campos = validar_pza_notas();

    if (valida_campos) {
        $("#carga_3").css("display","inline-block");
        $("#error_formato").text("");
        
        /* Creamos un nuevo objeto FormData. Este sustituye al atributo enctype = "multipart/form-data" que, tradicionalmente, se incluía en los formularios (y que aún se incluye, cuando son enviados 
        desde HTML. */
        var paqueteCotizacion = new FormData();
        paqueteCotizacion.append('id_refaccion', $("#parte_id_refaccion").prop('value')); 
        paqueteCotizacion.append('id_cita', $("#idOrdenTempCotiza").prop('value')); 
        paqueteCotizacion.append('inspeccion', $("#comentario_pieza").prop('value')); 
        paqueteCotizacion.append('asc', $("#comentario_asc").prop('value')); 
        paqueteCotizacion.append('firma_base', $("#ruta_parte_req").prop('value')); 
        paqueteCotizacion.append('nombre_firma', $("#nombre_gerente").prop('value')); 

        /* Se envia el paquete de datos por ajax. */
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"presupuestos/Presupuestos/notas_garantias_refaccion",
            type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
            contentType: false,
            data: paqueteCotizacion, // Al atributo data se le asigna el objeto FormData.
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var campos = resp.split("=");
                    //Si el registro se guardo correctamente, redireccionamos a la vista de no edicion
                    if (campos[0] == "OK") {

                        var pza = $("#comentario_pieza").val();
                        var asc = $("#comentario_asc").val();
                        var nombre = $("#nombre_gerente").val();

                        //Bajamos la informacion
                        var tabla = $("#comentarios_registrados");
                        tabla.append("<tr>"+
                                    "<td style='width: 60%;font-size: 12px;color: black;text-align: justify;' rowspan='2'>"+
                                        "** ) Se inspeccionaron las partes reemplazadas <span style='color:darkblue;'>"+pza+"</span> en la acción de servicio #<span style='color:darkblue;'>"+asc+"></span> y se verificó que fueran reemplazadas."+
                                    "</td>"+
                                    "<td style='width: 40%;border-bottom: 0.5px solid black;font-size: 11px;color: black;'  align='center'>"+
                                        "<img class='marcoImg' src='"+base+""+campos[1]+"' style='height:1cm;width:2cm;'>"+

                                        "<br>"+
                                        "<span style='color:darkblue;'>"+nombre+"</span>"+
                                    "</td>"+
                                "</tr>"+
                                "<tr>"+
                                    "<td style='font-size: 10px;color: black;' align='center'>"+
                                        "Gerente de servicio"+
                                    "</td>"+
                                "</tr>"
                            );

                        var opcion = $("#parte_id_refaccion").val()+"||"+$("#comentario_pieza").val();
                        
                        $("#comentario_pieza").attr('disabled',true);
                        $("#parte_id_refaccion").val("");
                        $("#comentario_pieza").val("");

                        $("#comentario_asc").attr('disabled',true);
                        $("#comentario_asc").val("");

                        $("#enviar_comentario_req").attr('disabled',true);

                        $('#ruta_parte_req').val("");
                        $("#firma_parte_req").attr("src",base+"assets/imgs/fondo_bco.jpeg");

                        //$("#vaciado_refacciones option[value=" +opcion + "]").hide();

                        //Vamos limpiando el select
                        var sel = document.getElementById("vaciado_refacciones"); 
                        
                        for (var i = 0; i < sel.length; i++){
                            var opt = sel[i];
                            if (opt.value == opcion) {
                                //var texto = sel.options[i].text;
                                sel.options[sel.selectedIndex].style.display = "none";
                                $("#vaciado_refacciones option[value="+ 0 +"]").attr("selected",true);
                            }
                        }
                        
                        $("#carga_3").css("display","none");
                    //De lo contrario, mandamos un mensaje
                    } else {
                      //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                      $("#carga_3").css("display","none");
                      $("#error_formato").text(campos[0]);
                    }
                }else{
                  //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                  $("#carga_3").css("display","none");
                  $("#error_formato").text("ERROR AL ENVIAR EL PRESUPUESTO");
                }
        
            //Cierre de success
            }, error:function(error){
                console.log(error);
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $("#carga_3").css("display","none");
                $("#error_formato").text("ERROR *** AL ENVIAR EL PRESUPUESTO");
            }
        });
    }else{
        $("#error_formato").text("Faltan campo por completar");
    }      
});

function validar_pza_notas(){
    var validar_campos = true;

    if ($("#parte_id_refaccion").val() == "") {
        validar_campos = false;
    }

    if ($("#comentario_pieza").val() == "") {
        validar_campos = false;
    }

    if ($("#comentario_asc").val() == "") {
        validar_campos = false;
    }

    if ($("#ruta_parte_req").val() == "") {
        validar_campos = false;
    }

    return validar_campos;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$("#envio_garantia_form").on('click', function (e){
    console.log("Se envia presupuesto del proceso adicional para garantias");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    $("input").attr("disabled", false);
    $("textarea").attr("disabled", false);

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    /* Creamos un nuevo objeto FormData. Este sustituye al atributo enctype = "multipart/form-data" que, tradicionalmente, se incluía en los formularios (y que aún se incluye, cuando son enviados 
    desde HTML. */
    var paqueteCotizacion = new FormData(document.getElementById('procesar_adicionales_garantias'));

    /* Se envia el paquete de datos por ajax. */
    var base = $("#basePeticion").val();

    $.ajax({
        url: base+"api/Operaciones_Garantias/actualiza_ref_ventanilla",
        type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
        contentType: false,
        data: paqueteCotizacion, // Al atributo data se le asigna el objeto FormData.
        processData: false,
        cache: false,
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //Si el registro se guardo correctamente, redireccionamos a la vista de no edicion
                if (resp == "OK") {
                  //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                  $(".cargaIcono").css("display","none");
                    location.href = base+"Listar_Ventanilla/4";

                //De lo contrario, mandamos un mensaje
                } else {
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    $("#errorEnvio").text(resp)
                }
            }else{
                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#errorEnvio").text("ERROR AL ENVIAR EL PRESUPUESTO");
            }
        
        //Cierre de success
        }, error:function(error){
            console.log(error);
            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            $("#errorEnvio").text("ERROR 02 INTERNO");
        }
   });
});

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$("#actualiza_tecncio_btn").on('click', function (e){
  console.log("Se envia presupuesto edicion del tecncio");
  //Creamos visualmente el efecto de carga
  //Si se hizo bien la conexión, mostramos el simbolo de carga
  $(".cargaIcono").css("display","inline-block");

  // Evitamos que salte el enlace.
  e.preventDefault(); 
  /* Creamos un nuevo objeto FormData. Este sustituye al atributo enctype = "multipart/form-data" que, tradicionalmente, se incluía en los formularios (y que aún se incluye, cuando son enviados 
    desde HTML. */
    var paqueteCotizacion = new FormData(document.getElementById('form_edita_tec'));

    /* Se envia el paquete de datos por ajax. */
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"presupuestos/Presupuestos/actualizar_tecAjax",
        type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
        contentType: false,
        data: paqueteCotizacion, // Al atributo data se le asigna el objeto FormData.
        processData: false,
        cache: false,
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //Si el registro se guardo correctamente, redireccionamos a la vista de no edicion
                if (resp == "OK") {
                  //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                  $(".cargaIcono").css("display","none");
                    var id_cita = $('#idOrdenTempCotiza').val();
                    //location.href = base+"Presupuesto_Tecnico/"+id_cita;
                    location.reload();

                //De lo contrario, mandamos un mensaje
                } else {
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    $("#errorEnvio").text(resp)
                }
            }else{
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $(".cargaIcono").css("display","none");
              $("#errorEnvio").text("ERROR AL ENVIAR EL PRESUPUESTO");
            }
        
        //Cierre de success
        },error:function(error){
              console.log(error);
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $(".cargaIcono").css("display","none");
              //Regresamos los colores de la tabla a la normalidad
              $("#envio_presupuesto_contenedor").css("background-color","white");
              $("#errorEnvio").text("ERROR 02 AL ENVIAR EL PRESUPUESTO");
            }
       }); 
});

$("#enviar_costos_g").on('click', function (e){
    console.log("Se envia costos ford garantia");
    //Creamos visualmente el efecto de carga
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $("#envio_costo_g").css("display","inline-block");

    // Evitamos que salte el enlace.
    e.preventDefault(); 
    var base = $("#basePeticion").val().trim();
    var precio_pieza = $("#precio_pieza_g").val();
    var horas_mo = $("#horas_mo_g").val();
    var precio_mo = $("#precio_mo_g").val();

    var total = $("#total_g").val();
    var indice = $("#index").val();

    var pd_presupuesto = new FormData();
    pd_presupuesto.append('ref_clase', $("#refaccion_cg").val());
    pd_presupuesto.append('panel', $("#panel").val());
    pd_presupuesto.append('id_cita', $("#idOrdenTempCotiza").val());
    pd_presupuesto.append('costo_ford', precio_pieza);
    pd_presupuesto.append('horas_mo_g', horas_mo);
    pd_presupuesto.append('costo_mo_g', precio_mo);

    $("#tb2_costoCM_"+indice).val(precio_pieza);
    $("#tb2_horas_"+indice).val(horas_mo);
    $("#tb2_totalReng_"+indice).val(precio_mo);
    $("#tb2_renglon_label_"+indice).text(total);

    $.ajax({
        url: base+"presupuestos/Presupuestos/actualizar_datos_garantia",
        type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
        contentType: false,
        data: pd_presupuesto, // Al atributo data se le asigna el objeto FormData.
        processData: false,
        cache: false,
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //Si el registro se guardo correctamente, redireccionamos a la vista de no edicion
                if (resp == "OK") {
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $("#enviar_costos_g").css('display','none');
                    $("#envio_costo_g").css("display","none");
                    $("#errorEnvio").text("");
                //De lo contrario, mandamos un mensaje
                } else {
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $("#envio_costo_g").css("display","none");
                    $("#errorEnvio").text(resp);
                }
            }else{
              //Si ya se termino de cargar la tabla, ocultamos el icono de carga
              $("#envio_costo_g").css("display","none");
              $("#errorEnvio").text("ERROR AL ENVIAR LA INFORMACIÓN");
            }
        
        //Cierre de success
        }, error:function(error){
            console.log(error);
            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $("#envio_costo_g").css("display","none");
            $("#errorEnvio").text("ERROR 02 AL ENVIAR LA INFORMACIÓN 404");
        }
   });
});