//Busquedas para ventanilla
$('#btnBusqueda_venatnilla').on('click',function(){
    console.log("Buscando filtro en ventanilla");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    //var valor_cliente = $("#filtro_cliente").val();
    var valor_ventanillas = $("#filtro_ventanillas").val();
    var campo = $("#busqueda_ventanilla").val();

    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"presupuestos/PresupuestosBusquedas/filtroCotizacionesVentanilla",
        method: 'post',
        data: {
            campo : campo,
            estado_refaccion : valor_ventanillas,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        //console.log(celdas);

                        var base_2 = $("#basePeticion").val().trim();        

                        //Validamos la la aprobación del cliente
                        var apruebaCliente = '';
                        if (celdas[12] != "") {
                            if (celdas[12] == 'Si') {
                                apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                            } else if(celdas[12] == 'Val'){
                                apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                            } else{
                                apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                            }
                        } else{
                            apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                        }

                        var acciones = "";
                        //Presupuestos tradicionales
                        if ((celdas[2] == "1")&&(celdas[12] != "")&&(celdas[12] != "No")) {
                            if (celdas[5] == "0") {
                                acciones = "<a class='btn' style='background-color: #f5acaa;font-size: 10px;' href='"+base_2+"Presupuesto_Tradicional/"+celdas[0]+"'>"+
                                            "SOLICITAR<br>REFACCIÓN"+
                                        "</a>";
                            }else if(celdas[5] == "1"){
                                acciones = "<a class='btn' style='background-color: #f5ff51;font-size: 10px;' href='"+base_2+"Presupuesto_Tradicional/"+celdas[0]+"'>"+
                                            "RECIBIR<br>REFACCIÓN"+
                                        "</a>";
                            }else if(celdas[5] == "2"){
                                acciones = "<a class='btn' style='background-color: #c7ecc7;font-size: 10px;' href='"+base_2+"Presupuesto_Tradicional/"+celdas[0]+"'>"+
                                            "ENTREGAR<br>REFACCIÓN"+
                                        "</a>";
                            }else{
                                acciones = "<a class='btn' style='background-color: #5bc0de;font-size: 10px;' href='"+base_2+"Presupuesto_Tradicional/"+celdas[0]+"'>"+
                                            "VER"+
                                        "</a>";
                            }
                        //Presupuesto con garatia
                        }else if(celdas[2] == "2"){
                            if (celdas[6] == "0") {
                                acciones = "<a class='btn' style='background-color: #f5acaa;font-size: 10px;' href='"+base_2+"Presupuesto_Garantias/"+celdas[0]+"'>"+
                                            "SOLICITAR<br>REFACCIÓN"+
                                        "</a>";
                            }else if(celdas[6] == "1"){
                                acciones = "<a class='btn' style='background-color: #f5ff51;font-size: 10px;' href='"+base_2+"Presupuesto_Garantias/"+celdas[0]+"'>"+
                                            "RECIBIR<br>REFACCIÓN"+
                                        "</a>";
                            }else if(celdas[6] == "2"){
                                acciones = "<a class='btn' style='background-color: #c7ecc7;font-size: 10px;' href='"+base_2+"Presupuesto_Garantias/"+celdas[0]+"'>"+
                                            "ENTREGAR<br>REFACCIÓN"+
                                        "</a>";
                            }else{
                                acciones = "<a class='btn' style='background-color: #5bc0de;font-size: 10px;' href='"+base_2+"Presupuesto_Garantias/"+celdas[0]+"'>"+
                                            "VER"+
                                        "</a>";
                            }
                        //Presupuesto afectado por el cliente
                        }else if((celdas[12] != "")&&(celdas[2] == "1")){
                            acciones = "<a class='btn' style='background-color: #5bc0de;font-size: 10px;' href='"+base_2+"Presupuesto_Tradicional/"+celdas[0]+"'>"+
                                            "VER"+
                                        "</a>";
                        //Presupuesto editado por ventanilla
                        }else if(celdas[4] != "0"){
                            acciones = "<a class='btn btn-info' style='font-size: 10px;' href='"+base_2+"Presupuesto_Ventanilla/"+celdas[0]+"'>"+
                                            "VER"+
                                        "</a>";
                        //Presupuesto por editar por ventanilla
                        }else {
                            acciones = "<a class='btn btn-primary' style='font-size: 10px;' href='"+base_2+"Presupuesto_Ventanilla/"+celdas[0]+"'>"+
                                            "EDITAR"+
                                        "</a>";
                        }

                        var requisicion = "";
                        //Validar requisicion para presupuesto tradicional
                        if (celdas[2] == "1") {
                            //Validamos que ya se haya afectado el presupuesto
                            if ((celdas[12] != "")&&(celdas[12] != "No")) {
                                //Verificamos si sera la firma o el pdf
                                if (celdas[9] != "") {
                                    requisicion = "<a class='btn btn-secondary' style='color:white;' href='"+base_2+"Requisicion_T/"+celdas[0]+"'>REQUISICIÓN</button>";
                                } else {
                                    requisicion = "<a class='btn btn-success' style='color:white;' href='"+base_2+"Requisicion_Firma_T/"+celdas[0]+"'>REQUISICIÓN<br>FIRMA</button>";
                                }
                            }
                        //Validar requisicion para presupuesto con garantia
                        }else{
                            //Verificamos si sera la firma o el pdf
                            if (celdas[10] != "") {
                                requisicion = "<a class='btn btn-secondary' style='color:white;' href='"+base_2+"Requisicion_G/"+celdas[0]+"'>REQUISICIÓN</button>";
                            } else {
                                requisicion = "<a class='btn btn-success' style='color:white;' href='"+base_2+"Requisicion_Firma_G/"+celdas[0]+"'>REQUISICIÓN<br>FIRMA</button>";
                            }
                        }

                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:11px;'>"+
                            //Indice
                            //"<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                            //NO. ORDEN
                            "<td align='center' style='width:10%;background-color:"+((celdas[4] == "0") ? '#ffffff' : ((celdas[4] == "1") ? '#68ce68' : '#b776e6;color:white'))+";vertical-align: middle;width:10%;'>"+
                                celdas[0]+"<br>"+
                                ((celdas[4] == "2") ? "Se envio al asesor" : "")+""+
                            "</td>"+
                            //NO. ORDEN DMS
                            //"<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[16]+"</td>"+
                            //TIPO PRESUPUESTO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[1]+"</td>"+
                            //SERIE
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[13]+"</td>"+
                            //VEHÍCULO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[16]+"</td>"+
                            //PLACAS
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[17]+"</td>"+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[15]+"</td>"+
                            //TÉCNICO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[14]+"</td>"+
                            //APRUEBA CLIENTE
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+apruebaCliente+"</td>"+
                            //ESTADO REFACCIONES
                            "<td align='center' style='vertical-align: middle;width:15%;"+celdas[18]+"'>"+
                                ""+celdas[19]+""
                            +"</td>"+
                            //ACCIONES
                            //VER ORDEN DE SERVICIO
                            "<td align='center' style='width:15%;vertical-align: middle;'>"+
                                "<a href='"+base_2+"OrdenServicio_Revision/"+celdas[0]+"' class='btn btn-warning' target='_blank' style='font-size: 10px;'>ORDEN</a>"+
                            "</td>"+
                            //ACCIONES
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                ""+acciones+""+
                            "</td>"+
                            //Requisicion
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                ""+requisicion+""+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Condicionamos las busquedas por fecha 
$('#fecha_inicio').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_fin").attr('min',fecha_inicio);
    //console.log(fecha_inicio)
});

$('#fecha_fin').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_inicio").attr('max',fecha_fin);
    //console.log(fecha_fin)
});

//Busquedas para el jefe de taller
$('#btnBusqueda_jefetaller').on('click',function(){
    console.log("Buscando filtro en jefe de taller");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_gral").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"presupuestos/PresupuestosBusquedas/filtroCotizacionesGeneral",
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        console.log(celdas);

                        var base_2 = $("#basePeticion").val().trim();        

                        //Validamos la la aprobación del cliente
                        var apruebaCliente = '';
                        if (celdas[12] != "") {
                            if (celdas[12] == 'Si') {
                                apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                            } else if(celdas[12] == 'Val'){
                                apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                            } else{
                                apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                            }
                        } else{
                            apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                        }

                        var revisar = "";
                        //Validar requisicion para presupuesto tradicional
                        if (celdas[8] == "1") {
                            revisar = "<a class='btn btn-info' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_JDT/"+celdas[0]+"'>VER</button>";
                        } else {
                            revisar = "<a class='btn btn-primary' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_JDT/"+celdas[0]+"'>REVISAR</button>";
                        }

                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:11px;'>"+
                            //Indice
                            //"<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                            //NO. ORDEN
                            "<td align='center' style='width:10%;background-color:"+((celdas[8] == "0") ? '#ffffff' : '#68ce68')+";vertical-align: middle;width:10%;'>"+
                                celdas[0]+"<br>"+
                            "</td>"+
                            //NO. ORDEN DMS
                            //"<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[16]+"</td>"+
                            //TIPO PRESUPUESTO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[1]+"</td>"+
                            //SERIE
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[13]+"</td>"+
                            //VEHÍCULO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[16]+"</td>"+
                            //PLACAS
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[17]+"</td>"+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[15]+"</td>"+
                            //TÉCNICO
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[14]+"</td>"+
                            //APRUEBA CLIENTE
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+apruebaCliente+"</td>"+
                            //ACCIONES
                            //VER ORDEN DE SERVICIO
                            "<td align='center' style='width:15%;vertical-align: middle;'>"+
                                "<a href='"+base_2+"OrdenServicio_Revision/"+celdas[0]+"' class='btn btn-warning' target='_blank' style='font-size: 10px;'>ORDEN</a>"+
                            "</td>"+
                            //ACCIONES
                            "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                ""+revisar+""+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Busquedas para el asesor
$('#btnBusqueda_asesor').on('click',function(){
    console.log("Buscando filtro en asesor");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_gral").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"presupuestos/PresupuestosBusquedas/filtroCotizacionesGeneral",
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        //console.log(celdas);

                        //Comprobamos que solo sean presupuestos con refacciones tradicionales
                        if (celdas[2] == "1") {
                            var base_2 = $("#basePeticion").val().trim();        

                            //Validamos la la aprobación del cliente
                            var apruebaCliente = '';
                            if (celdas[12] != "") {
                                if (celdas[12] == 'Si') {
                                    apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                                } else if(celdas[12] == 'Val'){
                                    apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                                } else{
                                    apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                                }
                            } else{
                                apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                            }

                            var revisar = "";
                            //Validamos si la orden se editara o se mostrara bloqueada
                            if (celdas[7] == "SI") {
                                revisar = "<a class='btn btn-info' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_Asesor/"+celdas[0]+"'>VER</button>";
                            } else {
                                revisar = "<a class='btn btn-primary' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_Asesor/"+celdas[0]+"'>REVISAR</button>";
                            }

                            var afectar = "";
                            //Validamos si el asesor ya puedes afectar el presupuesto (como cliente) o si ya se afecto
                            if ((celdas[7] == "SI")&&(celdas[12] == "")) {
                                afectar = "<a class='btn btn-warning' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_Cliente/"+celdas[0]+"'>AFECTAR<br>PRESUPUESTO</button>";
                            } else if ((celdas[7] == "SI")&&(celdas[12] != "")) {
                                afectar = "<a class='btn btn-success' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_Final/"+celdas[0]+"'>PRESUPUESTO<br>FINAL</button>";
                            }

                            //Creamos las celdas en la tabla
                            tabla.append("<tr style='font-size:11px;'>"+
                                //Indice
                                //"<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                                //NO. ORDEN
                                "<td align='center' style='width:10%;background-color:"+((celdas[7] == "NO") ? '#ffffff' : '#68ce68')+";vertical-align: middle;width:10%;'>"+
                                    celdas[0]+
                                "</td>"+
                                //NO. ORDEN DMS
                                //"<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[16]+"</td>"+
                                //TIPO PRESUPUESTO
                                //"<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[1]+"</td>"+
                                //SERIE
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[13]+"</td>"+
                                //VEHÍCULO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[16]+"</td>"+
                                //PLACAS
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[17]+"</td>"+
                                //ASESOR
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[15]+"</td>"+
                                //TÉCNICO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[14]+"</td>"+
                                //APRUEBA CLIENTE
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+apruebaCliente+"</td>"+
                                //EDO. REFACCIONES
                                "<td align='center' style='vertical-align: middle;width:15%;"+celdas[18]+"'>"+celdas[19]+"</td>"+
                                //ACCIONES
                                //VER ORDEN DE SERVICIO
                                //"<td align='center' style='width:15%;vertical-align: middle;'>"+
                                //    "<a href='"+base_2+"OrdenServicio_Revision/"+celdas[0]+"' class='btn btn-warning' target='_blank' style='font-size: 10px;'>ORDEN</a>"+
                                //"</td>"+
                                //ACCIONES
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                    ""+revisar+""+
                                "</td>"+
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                    ""+afectar+""+
                                "</td>"+
                            "</tr>");
                        }

                        
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Busquedas para garantias
$('#btnBusqueda_garantias').on('click',function(){
    console.log("Buscando filtro para el panel de garantias");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_gral").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"presupuestos/PresupuestosBusquedas/filtroCotizacionesGeneral",
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            campo : campo,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        //console.log(celdas);

                        //Comprobamos que solo sean presupuestos con refacciones tradicionales
                        if (celdas[20] == "1") {
                            var base_2 = $("#basePeticion").val().trim();        

                            //Validamos la la aprobación del cliente
                            var apruebaCliente = '';
                            if (celdas[12] != "") {
                                if (celdas[12] == 'Si') {
                                    apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                                } else if(celdas[12] == 'Val'){
                                    apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                                } else{
                                    apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                                }
                            } else{
                                apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                            }

                            var revisar = "";
                            //Validamos si la orden se editara o se mostrara bloqueada
                            if (celdas[3] == "1") {
                                revisar = "<a class='btn btn-info' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_Garantia/"+celdas[0]+"'>VER</button>";
                            } else {
                                revisar = "<a class='btn btn-primary' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_Garantia/"+celdas[0]+"'>REVISAR</button>";
                            }



                            //Creamos las celdas en la tabla
                            tabla.append("<tr style='font-size:11px;'>"+
                                //Indice
                                //"<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                                //NO. ORDEN
                                "<td align='center' style='width:10%;background-color:"+((celdas[3] == "0") ? '#ffffff' : '#68ce68')+";vertical-align: middle;width:10%;'>"+
                                    celdas[0]+
                                "</td>"+
                                //NO. ORDEN DMS
                                //"<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[16]+"</td>"+
                                //TIPO PRESUPUESTO
                                //"<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[1]+"</td>"+
                                //SERIE
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[13]+"</td>"+
                                //VEHÍCULO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[16]+"</td>"+
                                //PLACAS
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[17]+"</td>"+
                                //ASESOR
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[15]+"</td>"+
                                //TÉCNICO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[14]+"</td>"+
                                //APRUEBA CLIENTE
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+apruebaCliente+"</td>"+
                                //EDO. REFACCIONES
                                "<td align='center' style='vertical-align: middle;width:15%;"+celdas[18]+"'>"+celdas[19]+"</td>"+
                                //ACCIONES
                                //VER ORDEN DE SERVICIO
                                "<td align='center' style='width:15%;vertical-align: middle;'>"+
                                    "<a href='"+base_2+"OrdenServicio_Revision/"+celdas[0]+"' class='btn btn-warning' target='_blank' style='font-size: 10px;'>ORDEN</a>"+
                                "</td>"+
                                //ACCIONES
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                    ""+revisar+""+
                                "</td>"+
                            "</tr>");
                        }

                        
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

//Busquedas para panel de tecnicos
$('#btnBusqueda_tecnico').on('click',function(){
    console.log("Buscando filtro para el panel de tecnicos");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_gral").val();
    //var id_tec = $("#usuarioActivo").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"presupuestos/PresupuestosBusquedas/filtroCotizacionesTec",
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            campo : campo,
            //id_tec : id_tec
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    //console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        //console.log(celdas);

                        //Comprobamos que solo sean presupuestos con refacciones tradicionales
                        if (celdas[20] == "1") {
                            var base_2 = $("#basePeticion").val().trim();        

                            //Validamos la la aprobación del cliente
                            var apruebaCliente = '';
                            if (celdas[12] != "") {
                                if (celdas[12] == 'Si') {
                                    apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                                } else if(celdas[12] == 'Val'){
                                    apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                                } else{
                                    apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                                }
                            } else{
                                apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                            }

                            var editar = "<a class='btn btn-primary' style='color:white;font-size:10px;' href='"+base_2+"Presupuesto_Tecnico/"+celdas[0]+"'>VER</button>";
                            
                            //Creamos las celdas en la tabla
                            tabla.append("<tr style='font-size:11px;'>"+
                                //Indice
                                //"<td align='center' style='vertical-align: middle;width:10%;'>"+(i+1)+"</td>"+
                                //NO. ORDEN
                                "<td align='center' style='width:10%;vertical-align: middle;width:10%;'>"+
                                    celdas[0]+
                                "</td>"+
                                //NO. ORDEN DMS
                                //"<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[16]+"</td>"+
                                //TIPO PRESUPUESTO
                                //"<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[1]+"</td>"+
                                //SERIE
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[13]+"</td>"+
                                //VEHÍCULO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[16]+"</td>"+
                                //PLACAS
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[17]+"</td>"+
                                //ASESOR
                                "<td align='center' style='vertical-align: middle;width:10%;'>"+celdas[15]+"</td>"+
                                //TÉCNICO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[14]+"</td>"+
                                //APRUEBA CLIENTE
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+apruebaCliente+"</td>"+
                                //EDO. PROCESO
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+celdas[11]+"</td>"+
                                //ACCIONES
                                //VER ORDEN DE SERVICIO
                                "<td align='center' style='width:15%;vertical-align: middle;'>"+
                                    "<a href='"+base_2+"OrdenServicio_Revision/"+celdas[0]+"' class='btn btn-warning' target='_blank' style='font-size: 10px;'>ORDEN</a>"+
                                "</td>"+
                                //ACCIONES
                                "<td align='center' style='vertical-align: middle;width:15%;'>"+
                                    ""+editar+""+
                                "</td>"+
                            "</tr>");
                        }

                        
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='9'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});