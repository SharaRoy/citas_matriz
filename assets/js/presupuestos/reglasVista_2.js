//Cargamos la informacion en los campos de cabecera del presupuesto
if ($("#idOrdenTempCotiza").length > 0) {
    var serie = $("#idOrdenTempCotiza").val().trim();

    if ((serie != "")&&(serie != " ")&&(serie != "0")) {
        cargarDatosCoti(serie);
    }
}

//Verificamos que no se quede el numero orden vacio
function validarEstado(x){
    if ($("#idOrdenTempCotiza").val() == "" ) {
        //$("#aceptarCotizacion").attr("disabled", true);
        //$("#terminarCotizacion").attr("disabled", true);
    } else {
        //$("#aceptarCotizacion").attr("disabled", false);
        //$("#terminarCotizacion").attr("disabled", false);

        //Hacemos la peticion nuevamente para recuperar los datos
        cargarDatosCoti($("#idOrdenTempCotiza").val());
    }
}

function cargarDatosCoti(idOrden) {
    var url = $("#basePeticion").val();
    try{
        $.ajax({
            url: url+"multipunto/Multipunto/cargaDatos",
            method: 'post',
            data: {
                idOrden: idOrden,
            },
            success:function(resp){
                // console.log(resp);
                console.log("Cotizacion: "+resp);
                if (resp.indexOf("handler           </p>")<1) {

                    var campos = resp.split("_");
                    // console.log(campos);
                    if (campos.length > 2) {
                        $("#noSerieText").val(campos[0]);
                        $("#txtmodelo").val(campos[1]);
                        $("#placas").val(campos[2]);
                        $("#uen").val(campos[3]);
                        $("#tecnico").val(campos[4]);
                        $("#asesors").val(campos[5]);
                        $("#nombreCamapanias").text("Campaña(s): "+campos[6]);
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    } catch (e) {
        console.log(e);
    } finally {

    }
}

//Si es la vista de solo vista (tecnico), bloqueamos todos los campos
if ($("#modoVistaFormulario").val() == "2A") {
	console.log("Vista solo informativa");
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
}

//Si es la vista de ventanilla y ya se libero de ventanilla, se bloquea
if (($("#modoVistaFormulario").val() == "3")&& (($("#envioRefacciones").val() != "0")&&($("#envioRefacciones").val() != "3"))) {
	console.log("Vista refacciones [Presupuesto liberado]");
    //Ocultamos boton de buscade para refacciones
    $(".refaccionesBox").css("display","none");
    //Ocualtamos boton de actualizar
    $(".terminarCotizacion").css("display","none");
    //Ocultamos los botones para agregar refacciones
    $("#panel_refacciones").css("display","none");
    //Bloqueamos los campos para edicion
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
}

//Si es la vista deL JEFE DE TALLER y ya se libero , se bloquea
if (($("#modoVistaFormulario").val() == "4")&&($("#envioJefeTaller").val() != "0")) {
	console.log("Vista jefe de taller [Presupuesto liberado]");
    //Ocualtamos boton de actualizar
    $(".terminarCotizacion").css("display","none");
    //Bloqueamos los campos para edicion
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
}

//Si es la vista del asesor y ya firmo el presupuesto, se bloquea
if (($("#modoVistaFormulario").val() == "2")&&($("#finalizaAsesor").val() == "1")) {
	console.log("Vista Asesor [Presupuesto liberado]**");
    //Ocualtamos boton de actualizar
    $(".terminarCotizacion").css("display","none");
    //Bloqueamos los campos para edicion
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
}

//Si es la vista de garantias y ya firmo el presupuesto, se bloquea
if (($("#modoVistaFormulario").val() == "5")&&($("#envioGarantias").val() == "1")) {
	console.log("Vista Garantias [Presupuesto liberado]***");
    //Ocualtamos boton de actualizar
    $(".terminarCotizacion").css("display","none");
    //Bloqueamos los campos para edicion
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);
}

//Si es la vista de ventanilla para la requisicion y ya firmo el tecnico, se bloquea
if (($("#modoVistaFormulario").val() == "9")&&($("#envioGarantias").val() == "1")) {
	console.log("Vista Garantias 2 [Presupuesto liberado]****");
    //Ocualtamos boton de actualizar
    $(".terminarCotizacion").css("display","none");
    //Bloqueamos los campos para edicion
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);

    if ($("#dirige").val() == "Requisicion_G") {
        $(".terminarCotizacion12").attr('disabled',false);
    }
}

    //Desbloqueamos la edicion de precios
    $("#precio_mo_g").attr("disabled", false);
    $("#horas_mo_g").attr("disabled", false);
    $("#precio_pieza_g").attr("disabled", false);

// Buscador dinamico para la tabla
$('#busqueda_tabla').keyup(function () {
    toSearch();
});

//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}