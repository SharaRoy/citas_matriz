function verificar_acepta(i) {
    var seleccion = $('input:radio[name=radio_'+i+']:checked').val();
    console.log(seleccion);
    $("#autoriza_"+i).val(seleccion);
}

//Funciones con operaciones y autoguardado
function actualizaValores(indice){
    //Recuperamos los valores de toda la fila
    var fila = "";
    //0 Cantidad
    fila += parseFloat($("#cantidad_"+indice).val())+"_";
    //1 Descripcion
    fila += $("#descripcion_"+indice).val().replace('_', '-')+"_";
    //2 NUM. PIEZA
    fila += $("#pieza_"+indice).val().replace('_', '-')+"_";

    if ($("#pieza_"+indice).val().length > 4) {
        $("#prefijo_"+indice).attr('disabled', true);
        $("#sufijo_"+indice).attr('disabled', true);
        $("#basico_"+indice).attr('disabled', true);
    }else{
        $("#prefijo_"+indice).attr('disabled', false);
        $("#sufijo_"+indice).attr('disabled', false);
        $("#basico_"+indice).attr('disabled', false);
    }

    //3 EXISTE
    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var existe = $("input[name='existe_"+indice+"']:checked").val();
        //Verificamos si el input seleccionado es "PLANTA" para mostrar la fecha tentativa
        if ((existe == "PLANTA")&&($("#descripcion_"+indice).val() != "")) {
            existe += "*"+$("#fecha_tentativa_"+indice).val();
        }
    }else {
        var existe = "";
    }
    fila += existe+"_";
    //4 PRECIO / P
    fila += parseFloat($("#costoCM_"+indice).val())+"_";
    //5 HORAS
    fila += parseFloat($("#horas_"+indice).val())+"_";
    //6 COSTO MO
    fila += parseFloat($("#totalReng_"+indice).val())+"_";

    var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
    var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
    
    //7 TOTAL RENGLON
    var total = refaccion+mo;

    fila += total.toFixed(2)+"_";

    //8 PRIORIDAD DE LA REFACCION
    fila += $("#prioridad_"+indice).val()+"_";

    //prefijo - sufijo - basico
    fila += $("#prefijo_"+indice).val()+"_";
    fila += $("#sufijo_"+indice).val()+"_";
    fila += $("#basico_"+indice).val()+"_";

    //12 REPARACION
    fila += $("#ref_"+indice).val()+"_";

    $("#total_renglon_label_"+indice).text(total.toFixed(2));
    //Vaciamos lo recolectado en la variable
    $("#valoresCM_"+indice).val(fila);
    console.log(fila);

    //Calculamos los nuevos totales
    var limite = $("#indiceTablaMateria").val();
    var envio_jdt = $("#envioJefeTaller").val();
    var subtotal = 0;

    for (var i = 1; i <= limite ; i++) {
        //Verificamos si ya se afecto por el jefe de taller
        if (envio_jdt == "1") {
            //Verificamos que la refaccion este aprobada por el jefe de taller
            if ($("#aprueba_jdt_"+i).val() == "1") {
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var refaccion = parseFloat($("#cantidad_"+i).val()) * parseFloat($("#costoCM_"+i).val());
                var mo = parseFloat($("#horas_"+i).val()) * parseFloat($("#totalReng_"+i).val());

                var totalRenglon = refaccion+mo;
                //console.log(totalRenglon);
                subtotal += totalRenglon;
            }
        //De lo contrario se hace la suma completa
        }else{
            //Como alternativa, se puede hacer la cuenta completa por renglon
            var refaccion = parseFloat($("#cantidad_"+i).val()) * parseFloat($("#costoCM_"+i).val());
            var mo = parseFloat($("#horas_"+i).val()) * parseFloat($("#totalReng_"+i).val());

            var totalRenglon = refaccion+mo;
            subtotal += totalRenglon;
        }
    }

    //Generamos los nuevos subtotales
    var anticipo = parseFloat($("#anticipoMaterial").val());
    var cancelado = parseFloat($("#canceladoMaterial").val());
    //Total sin iva y sin anticipo
    var subtotal_limpio = subtotal - cancelado;

    var iva = subtotal_limpio * 0.16;
    var presupuesto = subtotal_limpio * 1.16;
    //Total con iva y con anticipo
    var presupuesto_total = presupuesto - anticipo;

    //Vaciamos los nuevos resultados
    $("#subTotalMaterial").val(parseFloat(subtotal_limpio));
    $("#ivaMaterial").val(parseFloat(iva));
    $("#totalMaterial").val(parseFloat(presupuesto_total));

    $("#subTotalMaterialLabel").text(subtotal.toFixed(2));
    $("#ivaMaterialLabel").text(iva.toFixed(2));
    $("#totalMaterialLabel").text(presupuesto_total.toFixed(2));

    $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));

    //Si la eleccion es "Planta" mostramos la fecha tentaniva
    if ($("input[name='existe_"+indice+"']:checked").val() == "PLANTA") {
        $("#fecha_tentativa_"+indice).css('display','inline-table');
    }else{
        $("#fecha_tentativa_"+indice).css('display','none');
    }

    if ($("#tb2_cantidad_"+indice).length > 0) {
        $("#tb2_cantidad_"+indice).val($("#cantidad_"+indice).val());
        $("#tb2_descripcion_"+indice).val($("#descripcion_"+indice).val());
        $("#tb2_pieza_"+indice).val($("#pieza_"+indice).val());
    }
}

function actualizaValorespartes(indice){
    //Recuperamos los valores de toda la fila
    var fila = "";
    var data_1 = $("#prefijo_"+indice).val();
    var data_2 = $("#basico_"+indice).val();
    var data_3 = $("#sufijo_"+indice).val();
    var data_4 = $("#grupo_"+indice).val();

    var clave_compuesta = data_4+data_1+data_2+data_3;
    //console.log(clave_compuesta);
    console.log(clave_compuesta.length);
    if (clave_compuesta.length > 4) {
        $("#pieza_"+indice).attr('disabled', true);
    }else{
        $("#pieza_"+indice).attr('disabled', false);
    }

    //0 Cantidad
    fila += parseFloat($("#cantidad_"+indice).val())+"_";
    //1 Descripcion
    fila += $("#descripcion_"+indice).val()+"_";
    //2 NUM. PIEZA
    $("#pieza_"+indice).val(clave_compuesta);

    fila += $("#pieza_"+indice).val()+"_";
    //3 EXISTE
    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var existe = $("input[name='existe_"+indice+"']:checked").val();
        //Verificamos si el input seleccionado es "PLANTA" para mostrar la fecha tentativa
        if ((existe == "PLANTA")&&($("#descripcion_"+indice).val() != "")) {
            existe += "*"+$("#fecha_tentativa_"+indice).val();
        }
    }else {
        var existe = "";
    }
    fila += existe+"_";
    //4 PRECIO / P
    fila += parseFloat($("#costoCM_"+indice).val())+"_";
    //5 HORAS
    fila += parseFloat($("#horas_"+indice).val())+"_";
    //6 COSTO MO
    fila += parseFloat($("#totalReng_"+indice).val())+"_";

    var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
    var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
    
    //7 TOTAL RENGLON
    var total = refaccion+mo;

    fila += total.toFixed(2)+"_";

    //8 PRIORIDAD DE LA REFACCION
    fila += $("#prioridad_"+indice).val()+"_";
    //prefijo - sufijo - basico
    fila += data_1+"_";
    fila += data_3+"_";
    fila += data_2+"_";

    //12 REPARACION
    fila += $("#ref_"+indice).val()+"_";

    $("#total_renglon_label_"+indice).text(total.toFixed(2));
    //Vaciamos lo recolectado en la variable
    $("#valoresCM_"+indice).val(fila);
    //console.log("basico: "+fila);
    
    envio_partes(indice);
}

function envio_partes(indice) {
    var data_1 = $("#prefijo_"+indice).val();
    var data_2 = $("#basico_"+indice).val();
    var data_3 = $("#sufijo_"+indice).val();
    var data_4 = $("#pieza_"+indice).val();
    var refaccion = $("#id_refaccion_"+indice).val();

    var base = $("#basePeticion").val();

    if (data_2 != "") {
        $.ajax({
            url: base+"presupuestos/Presupuestos/grabar_desgloce_piezas", 
            method: 'post',
            data: {
                prefijo : data_1,
                basico : data_2,
                sufijo : data_3,
                clave : data_4,
                refaccion: refaccion,
            },
            success:function(resp){
                if (resp.indexOf("handler           </p>")<1) {
                    
                    if (resp == "OK") {
                        console.log("OK");
                    }else {
                        console.log(resp);
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
}

//Autoguardado de la tabla adicional de garantias ( en la vista de refacciones)
// Funciones de autoguardado de renglon
function  actualizaValoresGarantias(indice){
    //Recuperamos los valores de toda la fila
    var campos_3 = parseFloat($("#tb2_cantidad_"+indice).val());
    var campos_4 = parseFloat($("#tb2_costoCM_"+indice).val());
    var campos_5 = parseFloat($("#tb2_horas_"+indice).val());
    var campos_6 = parseFloat($("#tb2_totalReng_"+indice).val());

    //Grabamos en el respaldo
    $("#valoresGarantias_"+indice).val(campos_4+"_"+campos_5+"_"+campos_6);

    //Calculamos los totales de garantia (solo informativo)
    var total = (campos_3*campos_4) + (campos_5*campos_6);

    //Imprimimos el total del renglon
    $("#tb2_renglon_label_"+indice).text(total.toFixed(2));

    //Calculamos los nuevos totales
    var limite = $("#indiceTablaMateria").val();
    var subtotal = 0;

    for (var i = 1; i <= limite ; i++) {
        //Verificamos que la refaccion este aprobada por el jefe de taller
        if ($('#garantia_check_'+i).is(':checked') ) {
            //Como alternativa, se puede hacer la cuenta completa por renglon
            var refaccion = parseFloat($("#tb2_cantidad_"+i).val()) * parseFloat($("#tb2_costoCM_"+i).val());
            var mo = parseFloat($("#tb2_horas_"+i).val()) * parseFloat($("#tb2_totalReng_"+i).val());

            var totalRenglon = refaccion+mo;
            //console.log(totalRenglon);
            subtotal += totalRenglon;
        }
    }

    var iva = subtotal * 0.16;
    var presupuesto = subtotal * 1.16;

    //Vaciamos los nuevos resultados
    $("#subTotalMaterialLabel_tabla2").text(subtotal.toFixed(2));
    $("#ivaMaterialLabel_tabla2").text(iva.toFixed(2));
    $("#totalMaterialLabel_tabla2").text(presupuesto.toFixed(2));

    //envio_datos_garantias(indice);
}

function subir_costos(indice){
    var costo_pza = parseFloat($("#tb2_costoCM_"+indice).val());
    var horas = parseFloat($("#tb2_horas_"+indice).val());
    var costo_mo = parseFloat($("#tb2_totalReng_"+indice).val());
    var cantidad = parseFloat($("#tb2_cantidad_"+indice).val());

    var total = (costo_pza*cantidad) + (horas*costo_mo);

    var descripcion = $("#tb2_descripcion_"+indice).val();
    var clave = $("#tb2_pieza_"+indice).val();

    //Vaciamos los 
    $("#precio_mo_g").val(costo_mo.toFixed(2));
    $("#horas_mo_g").val(horas.toFixed(2));
    $("#precio_pieza_g").val(costo_pza.toFixed(2));
    $("#cantidad_cg").val(cantidad);
    $("#total_g").val(total.toFixed(2));

    $("#descripcion_g").text(descripcion);
    $("#pieza_g").text(clave);

    var ref = $("#id_refaccion_"+indice).val();
    $("#refaccion_cg").val(ref);
    $("#index").val(indice);
}

function actualiza_costo_g(){
    var costo_mo = parseFloat($("#precio_mo_g").val());
    var horas = parseFloat($("#horas_mo_g").val());
    var costo_pza = parseFloat($("#precio_pieza_g").val());
    var cantidad = parseFloat($("#cantidad_cg").val());

    var total = (costo_pza*cantidad) + (horas*costo_mo);
    //Vaciamos los 
    $("#precio_mo_g").val(costo_mo.toFixed(2));
    $("#horas_mo_g").val(horas.toFixed(2));
    $("#precio_pieza_g").val(costo_pza.toFixed(2));
    $("#total_g").val(total.toFixed(2));
}

function cerrar_costos_g(){
    $("#enviar_costos_g").css("display","inline-block");
    $("#envio_costo_g").css("display","none");
    $("#precio_mo_g").val(0);
    $("#horas_mo_g").val(0);
    $("#precio_pieza_g").val(0);
    $("#cantidad_cg").val(0);
    $("#total_g").val(0);

    $("#descripcion_g").text("");
    $("#pieza_g").text("");
    $("#refaccion_cg").val(0);
    $("#index").val(0);
}

//Agregar refaccion con garantia desde ventanilla
function  refaccionGarantia(indice){
    //Cargamos los datos de edicion de la tabla
    var limite = $("#indiceTablaMateria").val();
    
    //Revisamos si se agrega una refaccion con garantia
    if ($('#garantia_check_'+indice).is(':checked') ) {
        console.log("Agrega refaccion con garantia renglon: "+indice);
        //Recuperamos los nuevos valores del renglon superior
        var cantidad = parseFloat($("#cantidad_"+indice).val());
        var descripcion =$("#descripcion_"+indice).val();
        var pieza =$("#pieza_"+indice).val();

        //Agregamos renglon con garantia
        var tabla = $("#cuerpoMaterialesGarantias");

        tabla.append("<tr id='tb2_fila_"+indice+"'>"+
            //<!-- CANTIDAD -->
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias("+indice+")' id='tb2_cantidad_"+indice+"' value='"+cantidad+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;' disabled>"+
            "</td>"+
            //<!-- D E S C R I P C I Ó N -->
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='1' class='input_field_lg' onblur='actualizaValoresGarantias("+indice+")' id='tb2_descripcion_"+indice+"' style='width:90%;text-align:left;' disabled>"+descripcion+"</textarea>"+
            "</td>"+
            //<!-- NUM. PIEZA -->
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='1' class='input_field_lg' onblur='actualizaValoresGarantias("+indice+")' id='tb2_pieza_"+indice+"' style='width:90%;text-align:left;' disabled>"+pieza+"</textarea>"+
            "</td>"+
            //<!-- COSTO PZA FORD -->
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "$<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias("+indice+")' id='tb2_costoCM_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
            "</td>"+
            //<!-- HORAS -->
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias("+indice+")' id='tb2_horas_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+
            "</td>"+
            //<!-- COSTO MO GARANTÍAS -->
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "$<input type='number' step='any' min='0' id='tb2_totalReng_"+indice+"' onblur='actualizaValoresGarantias("+indice+")'  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>"+

            "</td>"+
            //<!-- TOTAL REFACCIÓN -->
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                //<!-- Calculamos el total para visualizar -->
                "<label id='tb2_renglon_label_"+indice+"' style='color:darkblue;'>$0.00</label>"+
            "</td>"+
        "</tr>");
    //De lo contrario, se elimina el renglon
    }else{
        console.log("Quitar refaccion con garantia renglon:"+indice);
        var subtotal = 0;

        for (var i = 1; i <= limite ; i++) {
            //Verificamos que la refaccion este aprobada por el jefe de taller
            if ($('#garantia_check_'+i).is(':checked') ) {
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var refaccion = parseFloat($("#tb2_cantidad_"+i).val()) * parseFloat($("#tb2_costoCM_"+i).val());
                var mo = parseFloat($("#tb2_horas_"+i).val()) * parseFloat($("#tb2_totalReng_"+i).val());

                var totalRenglon = refaccion+mo;
                //console.log(totalRenglon);
                subtotal += totalRenglon;
            }
        }

        var iva = subtotal * 0.16;
        var presupuesto = subtotal * 1.16;

        //Vaciamos los nuevos resultados
        $("#subTotalMaterialLabel_tabla2").text(subtotal.toFixed(2));
        $("#ivaMaterialLabel_tabla2").text(iva.toFixed(2));
        $("#totalMaterialLabel_tabla2").text(presupuesto.toFixed(2));
        
        //Eliminamos el renglon
        var tb2_fila = $("#tb2_fila_"+indice);
        tb2_fila.remove();
    }
}

//Reahacemos las sumatoria y el renglon en la vista del asesor
function actualizaSumaFinal(indice) {
    //Recuperamos los valores de toda la fila
    var fila = "";
    //0 Cantidad
    fila += parseFloat($("#cantidad_"+indice).val())+"_";
    //1 Descripcion
    fila += $("#descripcion_"+indice).val()+"_";
    //2 NUM. PIEZA
    fila += $("#pieza_"+indice).val()+"_";
    //3 EXISTE
    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var existe = $("input[name='existe_"+indice+"']:checked").val();
        //Verificamos si el input seleccionado es "PLANTA" para mostrar la fecha tentativa
        if ((existe == "PLANTA")&&($("#descripcion_"+indice).val() != "")) {
            existe += "*"+$("#fecha_tentativa_"+indice).val();
        }
    }else {
        var existe = "";
    }
    fila += existe+"_";
    //4 PRECIO / P
    fila += parseFloat($("#costoCM_"+indice).val())+"_";
    //5 HORAS
    fila += parseFloat($("#horas_"+indice).val())+"_";
    //6 COSTO MO
    fila += parseFloat($("#totalReng_"+indice).val())+"_";

    var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
    var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
    
    //7 TOTAL RENGLON
    var total = refaccion+mo;

    fila += total.toFixed(2)+"_";

    //8 PRIORIDAD DE LA REFACCION
    fila += $("#prioridad_"+indice).val()+"_";

    //9 REPARACION
    fila += $("#ref_"+indice).val()+"_";

    $("#total_renglon_label_"+indice).text(total.toFixed(2));
    //Vaciamos lo recolectado en la variable
    $("#valoresCM_"+indice).val(fila);
    console.log(fila);

    //Calculamos los nuevos totales, solo con las piezas autorizadas por el asesor
    var limite = $("#indiceTablaMateria").val();
    var subtotal = 0;

    for (var i = 1; i <= limite ; i++) {
        //Verificamos que la refaccion este aprobada por el jefe de taller
        if ($('#asesor_check_'+i).is(':checked') ) {
            //Evitamos sumar las garantias
            if ($("#garantia_"+i).val() == "0") {
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var refaccion = parseFloat($("#cantidad_"+i).val()) * parseFloat($("#costoCM_"+i).val());
                var mo = parseFloat($("#horas_"+i).val()) * parseFloat($("#totalReng_"+i).val());

                var totalRenglon = refaccion+mo;
                //console.log(totalRenglon);
                subtotal += totalRenglon;
            }
        }
    }

    //Generamos los nuevos subtotales
    var anticipo = parseFloat($("#anticipoMaterial").val());
    var cancelado = parseFloat($("#canceladoMaterial").val());
    //Total sin iva y sin anticipo
    var subtotal_limpio = subtotal - cancelado;

    var iva = subtotal_limpio * 0.16;
    var presupuesto = subtotal_limpio * 1.16;
    //Total con iva y con anticipo
    var presupuesto_total = presupuesto - anticipo;

    //Vaciamos los nuevos resultados
    $("#subTotalMaterial").val(parseFloat(subtotal_limpio));
    $("#ivaMaterial").val(parseFloat(iva));
    $("#totalMaterial").val(parseFloat(presupuesto_total));

    $("#subTotalMaterialLabel").text(subtotal.toFixed(2));
    $("#ivaMaterialLabel").text(iva.toFixed(2));
    $("#totalMaterialLabel").text(presupuesto_total.toFixed(2));

    $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));
}

//Actualizamos registros y sumatoria en la re-edicion del tecnico
//Funciones con operaciones y autoguardado
function actualizaValoresTecnico(indice){
    //Recuperamos los valores de toda la fila
    var fila = "";
    var tipo_orden = "";
    if ($("#ordenT").length) {
        tipo_orden = $("#ordenT").val();
    }
    //0 Cantidad
    fila += parseFloat($("#cantidad_"+indice).val())+"_";
    //1 Descripcion
    fila += $("#descripcion_"+indice).val().replace('_', '-')+"_";
    //2 NUM. PIEZA
    fila += $("#pieza_"+indice).val().replace('_', '-')+"_";
    //3 EXISTE
    if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
        var existe = $("input[name='existe_"+indice+"']:checked").val();
        //Verificamos si el input seleccionado es "PLANTA" para mostrar la fecha tentativa
        if ((existe == "PLANTA")&&($("#descripcion_"+indice).val() != "")) {
            existe += "*"+$("#fecha_tentativa_"+indice).val();
        }
    }else {
        var existe = "";
    }
    fila += existe+"_";
    //4 PRECIO / P
    fila += parseFloat($("#costoCM_"+indice).val())+"_";
    //5 HORAS
    fila += parseFloat($("#horas_"+indice).val())+"_";
    //6 COSTO MO
    fila += parseFloat($("#totalReng_"+indice).val())+"_";

    var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
    var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
    
    //7 TOTAL RENGLON
    var total = refaccion+mo;

    fila += total.toFixed(2)+"_";

    //8 PRIORIDAD DE LA REFACCION
    fila += $("#prioridad_"+indice).val()+"_";

    //9 REPARACION
    fila += $("#ref_"+indice).val()+"_";

    //Si es una orden de bodyshop agregamos un campo
    if (tipo_orden == "3") {
        //Costo Aseguradora
        fila += $("#costoaseg_"+indice).val()+"_";
    }else{
        fila += "_";
    }

    if( $('#rep_add_'+indice).is(':checked') ) {
        fila += "1_";
    } else{
        fila += "0_";
    }

    $("#total_renglon_label_"+indice).text(total.toFixed(2));
    //Vaciamos lo recolectado en la variable
    $("#valoresCM_"+indice).val(fila);
    console.log(fila.split("_"));

    //Calculamos los nuevos totales
    var limite = $("#indiceTablaMateria").val();
    var envio_jdt = $("#envioJefeTaller").val();
    var subtotal = 0;

    for (var i = 1; i <= limite ; i++) {
        if ($("#cantidad_"+i).length > 0) {
            //Como alternativa, se puede hacer la cuenta completa por renglon
            var refaccion = parseFloat($("#cantidad_"+i).val()) * parseFloat($("#costoCM_"+i).val());
            var mo = parseFloat($("#horas_"+i).val()) * parseFloat($("#totalReng_"+i).val());

            var totalRenglon = refaccion+mo;
            subtotal += totalRenglon;
        } 
    }

    //Generamos los nuevos subtotales
    var anticipo = parseFloat($("#anticipoMaterial").val());
    var cancelado = 0; //parseFloat($("#canceladoMaterial").val());
    //Total sin iva y sin anticipo
    var subtotal_limpio = subtotal - cancelado;

    var iva = subtotal_limpio * 0.16;
    var presupuesto = subtotal_limpio * 1.16;
    //Total con iva y con anticipo
    var presupuesto_total = presupuesto - anticipo;

    //Vaciamos los nuevos resultados
    $("#subTotalMaterial").val(parseFloat(subtotal_limpio));
    $("#ivaMaterial").val(parseFloat(iva));
    $("#totalMaterial").val(parseFloat(presupuesto_total));

    $("#subTotalMaterialLabel").text(subtotal.toFixed(2));
    $("#ivaMaterialLabel").text(iva.toFixed(2));
    $("#totalMaterialLabel").text(presupuesto_total.toFixed(2));

    $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));
}

//Liberamos los campos cuando se envie la cotizacion
$(".terminarCotizacion").on('click',function() {
    //zconsole.log("click  terminar Cotizacion"+id);
    $("input").attr("disabled", false);
    $("textarea").attr("disabled", false);
    //document.forms[0].submit();
});

//Envio de entrega de refacciones
$(".proceso_solicitud").on('click',function() {
    //Recuperamos el id_de la cita
    var id_cita = $("#idOrdenTempCotiza").val().trim();
    //Recuperamos el nuevo estatus
    var estatus = $("#valorEvento").val().trim();
    //Recuperamos el tipo de refacciones que se afectaran
    var presupuesto = $("#evento").val().trim();

    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");

    var base = $("#basePeticion").val().trim();

    //Verificamos que ninguna variable se mande vacia
    if ((id_cita != "")&&(estatus != "")&&(presupuesto != "")) {
        $.ajax({
            url: base+"presupuestos/Presupuestos/cambioEstatusRefacciones",
            method: 'post',
            data: {
                id_cita : id_cita,
                estatus : estatus,
                presupuesto : presupuesto
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        $(".cargaIcono").css("display","none");
                        location.reload();
                    }else{
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        $("#errorEnvioCotizacion").text(resp);
                    }
                }else{
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    $("#errorEnvioCotizacion").text(resp);
                }

            //Cierre de success
            },
            error:function(error){
                console.log(error);

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#errorEnvioCotizacion").text("Error al enviar la petición");
            //Cierre del error
            }
        //Cierre del ajax
        });
    //De lo contrario enviamos un mensaje
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $(".cargaIcono").css("display","none");
        $("#errorEnvioCotizacion").text("Verificar el numero de orden");
    }
});

//Ocultamos botones para envio total 
$("#c_parcial").on('click',function() {
    //Limpiamos el color de los renglones
    $(".fila").css("background-color","#ffffff");
    //Ocultamos los botones
    $(".todo_p").css('display','none');
    $(".parcial_p").css('display','inline-block');
    $(".eleccionColumna").css('display','revert');

    //Limpiamos los valores de cancelado
    $("#canceladoMaterial").val(0);
    $("#canceladoMaterialLabel").text("0.00");
});

//Mostramos los botones para envio total
$("#c_cancelar_p").on('click',function() {
    //Limpiamos el color de los renglones
    $(".fila").css("background-color","#ffffff");
    //Ocultamos los botones
    $(".parcial_p").css('display','none');
    $(".todo_p").css('display','inline-block');
    $(".eleccionColumna").css('display','none');

    //Limpiamos el total de la opercion
    var anticipo = parseFloat($("#anticipoMaterial").val());
    //Limpiamos los valores de cancelado
    var nvo_cancelado = $("#canceladoMaterial_temp").val();
    $("#canceladoMaterial").val(nvo_cancelado);
    $("#canceladoMaterialLabel").text(nvo_cancelado);

    
    //Imprimimos los valores
    $("#canceladoMaterial").val(nvo_cancelado);
    $("#canceladoMaterialLabel").text(nvo_cancelado.toFixed(2));

    //Recuperamos el total establecido
    var old_subtotal = parseFloat($("#subTotalMaterial").val());
    
    //Obtenemos nuevos valores a mostrar
    var nvo_subtotal = old_subtotal - nvo_cancelado;
    var iva = nvo_subtotal * 0.16;
    var presupuesto = nvo_subtotal * 1.16;
    var presupuesto_final = presupuesto - anticipo;

    //Imprimimos el nuevo subtotal
    $("#presupuestoTemptoMaterialLabel").text(nvo_subtotal.toFixed(2));

    //Imprimimos el iva
    $("#ivaMaterial").val(iva);
    $("#ivaMaterialLabel").text(iva.toFixed(2));
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(presupuesto);
    $("#totalMaterialLabel").text(presupuesto.toFixed(2));
    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text(presupuesto_final.toFixed(2));

    //location.reload();
});

//Botones para aceptar/rechara una refaccion (presupuesto parcial)
//Aceptar refaccion
function aceptarRefaccion(renglon){
    //Pintamos el renglon
    $("#fila_indicador_"+renglon).css("background-color","#c7ecc7");
    //Ocultamos el boton preionado
    $("#interruptorA_"+renglon).css('display','none');
    $("#interruptorB_"+renglon).css('display','revert');

    //Verificamos si ya se habia hecho algun cambio (para las operaciones)
    if ($("#bandera_decision_"+renglon).val() == "0") {
        //Activamos la bandera
        $("#bandera_decision_"+renglon).val("1");
    //Si ya se habia activado la bandera, entonces se saca el valor del renglon del cancelado
    }else{
        var anticipo = parseFloat($("#anticipoMaterial").val());

        var cancelado = parseFloat($("#canceladoMaterial").val());
        var renglon_valor = parseFloat($("#total_renglon_"+renglon).val());

        //Le quitamos el valor del renglon a total cancelado
        var nvo_cancelado = cancelado - renglon_valor;
        //Imprimimos los valores
        $("#canceladoMaterial").val(nvo_cancelado);
        $("#canceladoMaterialLabel").text(nvo_cancelado.toFixed(2));

        //Recuperamos el total establecido
        var old_subtotal = parseFloat($("#subTotalMaterial").val());
        
        //Obtenemos nuevos valores a mostrar
        var nvo_subtotal = old_subtotal - nvo_cancelado;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;
        var presupuesto_final = presupuesto - anticipo;

        //Imprimimos el nuevo subtotal
        $("#presupuestoTemptoMaterialLabel").text(nvo_subtotal.toFixed(2));

        //Imprimimos el iva
        $("#ivaMaterial").val(iva);
        $("#ivaMaterialLabel").text(iva.toFixed(2));
        //Imprimimos el total  menos el anticipo
        $("#totalMaterial").val(presupuesto);
        $("#totalMaterialLabel").text(presupuesto.toFixed(2));
        //Imprimimos el total con iva (presupuesto original)
        $("#presupuestoMaterialLabel").text(presupuesto_final.toFixed(2));
    }

    //Guardamos la decision
    $("#decision_"+renglon).val("1");
}

//rechazar refaccion
function rechazarRefaccion(renglon){
    //Pintamos el renglon
    $("#fila_indicador_"+renglon).css("background-color","#f5acaa");
    //Ocultamos el boton preionado
    $("#interruptorB_"+renglon).css('display','none');
    $("#interruptorA_"+renglon).css('display','revert');

    //Verificamos si ya se habia hecho algun cambio (para las operaciones)
    if ($("#bandera_decision_"+renglon).val() == "0") {
        //Activamos la bandera
        $("#bandera_decision_"+renglon).val("1");   
    }

    var anticipo = parseFloat($("#anticipoMaterial").val());

    var cancelado = parseFloat($("#canceladoMaterial").val());
    var renglon_valor = parseFloat($("#total_renglon_"+renglon).val());

    //Le quitamos el valor del renglon a total cancelado
    var nvo_cancelado = cancelado + renglon_valor;

    //Imprimimos los valores
    $("#canceladoMaterial").val(nvo_cancelado);
    $("#canceladoMaterialLabel").text(nvo_cancelado.toFixed(2));

    //Recuperamos el total establecido
    var old_subtotal = parseFloat($("#subTotalMaterial").val());
    
    //Obtenemos nuevos valores a mostrar
    var nvo_subtotal = old_subtotal - nvo_cancelado;
    //Imprimimos el nuevo subtotal
    $("#presupuestoTemptoMaterialLabel").text((old_subtotal - nvo_cancelado).toFixed(2));

    var iva = nvo_subtotal * 0.16;
    //Imprimimos el iva
    $("#ivaMaterial").val(iva);
    $("#ivaMaterialLabel").text(iva.toFixed(2));

    var presupuesto = nvo_subtotal * 1.16;
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(presupuesto);
    $("#totalMaterialLabel").text(presupuesto.toFixed(2));
    
    var presupuesto_final = presupuesto - anticipo;
    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text(presupuesto_final.toFixed(2));

    //Guardamos la decision
    $("#decision_"+renglon).val("0");
}

//Modal para envio de del presupuesto afectado por el cliente
$("#c_aceptar").on('click',function() {
    $("#recargar_presupuesto").css("background-color","#4cae4c");
    $("#recargar_presupuesto").css("color","white");

    $("#AlertaModalCuerpo").css("background-color","#c7ecc7");
    $("#AlertaModalCuerpo").css("border-color","#4cae4c");

    $("#tituloForm").text("¿Está seguro de aceptar totalmente el presupuesto? NO podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");
    
    //Llenamos los datos que se enviaran
    //Para el caso del presupuesto aceptado, el monto a cancelar es "0"
    $("#desicion_cancelado").val("0");
    $("#desicion_subtotal").val(parseFloat($("#subTotalMaterial").val()));

    //Para el caso del presupuesto aceptado, todo se va en uno
    var renglones = "";
    for (var i = 1 ; i <= $("#indiceTablaMateria").val(); i++) {
        var id = $("#id_refaccion_"+i).val();
        //var desicion = $("#decision_"+i).val().trim();
        renglones += id+"-"+"1"+"=";
    }

    $("#desicion").val(renglones);
    $("#aprobacion_refacciones").val("Si");
});

$("#c_enviar_presupuesto").on('click',function() {
    $("#recargar_presupuesto").css("background-color","#eea236");
    $("#recargar_presupuesto").css("color","white");

    $("#AlertaModalCuerpo").css("background-color","#f0ad4e");
    $("#AlertaModalCuerpo").css("border-color","#eea236");

    $("#tituloForm").text("¿Está seguro de enviar el siguiente presupuesto? NO podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");
    
    //Llenamos los datos que se enviaran
    //Para el caso del presupuesto parcial, recuperamos la seleccion de ids con sus respectivas decisiones
    var renglones = "";
    for (var i = 1 ; i <= $("#indiceTablaMateria").val(); i++) {
        var id = $("#id_refaccion_"+i).val();
        var desicion = $("#decision_"+i).val();
        renglones += id+"-"+desicion+"=";
    }

    $("#desicion").val(renglones);
    //Para el caso del presupuesto parcial, el monto a cancelar lo establece el usuario
    $("#desicion_cancelado").val(parseFloat($("#canceladoMaterial").val()));
    $("#desicion_subtotal").val(parseFloat($("#subTotalMaterial").val()));
    $("#aprobacion_refacciones").val("Val");
});

$("#c_rechazar").on('click',function() {
    $("#recargar_presupuesto").css("background-color","#d43f3a");
    $("#recargar_presupuesto").css("color","white");

    $("#AlertaModalCuerpo").css("background-color","#f5acaa");
    $("#AlertaModalCuerpo").css("border-color","#d43f3a");

    $("#tituloForm").text("¿Está seguro de rechazar totalmente el presupuesto? NO  podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");

    //Llenamos los datos que se enviaran
    //Para el caso del presupuesto rechazado, el monto a cancelar lo establece el usuario
    $("#desicion_cancelado").val(parseFloat($("#subTotalMaterial").val()));
    $("#desicion_subtotal").val(parseFloat($("#subTotalMaterial").val()));
    //Para el caso del presupuesto rechazado, todo se va en ceros
    var renglones = "";
    for (var i = 1 ; i <= $("#indiceTablaMateria").val(); i++) {
        var id = $("#id_refaccion_"+i).val();
        //var desicion = $("#decision_"+i).val().trim();
        renglones += id+"-"+"0"+"=";
    }

    $("#desicion").val(renglones);
    $("#aprobacion_refacciones").val("No");
});

//Envio de presupuesto afectado por el cliente
$("#enviar_presupuesto").on('click',function() {
    //Recuperamos la valoracion del renglon
    var renglones = $("#desicion").val().trim();
    //Recuperamos el monto a cancelar
    var cancelado = $("#desicion_cancelado").val().trim();
    //Recuperamos el subtotal original
    var subtotal = $("#desicion_subtotal").val().trim();
    //Recuperamos la categoria del presupuesto
    var desicion = $("#aprobacion_refacciones").val().trim();
    //Recuperamos id de la cita
    var id_cita = $("#id_cita").val().trim();

    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");

    var base = $("#basePeticion").val().trim();

    //Verificamos que ninguna variable se mande vacia
    if ((renglones != "")&&(desicion != "")&&(id_cita != "")) {
        $.ajax({
            url: base+"presupuestos/Presupuestos/presupuestoAfectado",
            method: 'post',
            data: {
                renglones : renglones,
                cancelado : cancelado,
                subtotal : subtotal,
                desicion : desicion,
                id_cita : id_cita
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        $(".cargaIcono").css("display","none");
                        $("#recargar_presupuesto").attr('hidden',false);
                        $("#cancelarCoti").attr('hidden',true);
                        $("#enviar_presupuesto").attr('hidden',true);
                        $("#tituloForm").attr('hidden',true);
                        $("#errorEnvioCotizacion").text("Presupuesto afectado");
                        //location.reload();
                    }else{
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        $("#errorEnvioCotizacion").text(resp);
                    }
                }else{
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    $("#errorEnvioCotizacion").text("Error al enviar la petición");
                }

            //Cierre de success
            },
            error:function(error){
                console.log(error);

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#errorEnvioCotizacion").text("Error al enviar la petición");
            //Cierre del error
            }
        //Cierre del ajax
        });
    //De lo contrario enviamos un mensaje
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $(".cargaIcono").css("display","none");
        $("#errorEnvioCotizacion").text("Error al intentar enviar el presupuesto");
    }
});


//Proceso para eliminar una refaccion del presupuesto (solo tecnicos)
$(".refaccion_baja").on('click',function() {
    //Simulamos cargar los datos
    $(".cargaIcono").css("display","inline-block");

    //Recuperamos el id de la refaccion a afectar
    var renglon = $(this).data("id");
    $("#id_refaccion").val($("#id_refaccion_"+renglon).val());
    $("#renglon_eliminar").val(renglon);

    console.log("renglon: "+renglon);
    console.log("id: "+$("#id_refaccion_"+renglon).val());

    //Mostramos la informacion preventiva
    var cantidad = $("#cantidad_"+renglon).val();
    var descripcion =  $("#descripcion_"+renglon).val();
    var numero_pza = $("#pieza_"+renglon).val();
    var costo = $("#costoCM_"+renglon).val();

    $("#label_refaccion").text(cantidad+" "+descripcion+" "+numero_pza+" "+costo);

    //Detenemos la simulacion de cargar los datos
    $(".cargaIcono").css("display","none");
});

//Enviamos la peticion para dar de baja la refaccion
$("#btn_envio_refaccion").on('click',function() {
    //Recuperamos el id de la refaccion a eliminar
    var id_refaccion = $("#id_refaccion").val().trim();
    //Recuperamos id de la cita
    var id_cita = $("#id_cita").val().trim();

    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");

    var base = $("#basePeticion").val().trim();

    //Verificamos que ninguna variable se mande vacia
    if ((id_refaccion != "")&&(id_cita != "")) {
        $.ajax({
            url: base+"presupuestos/Presupuestos/refaccionesBaja",
            method: 'post',
            data: {
                id_refaccion : id_refaccion,
                id_cita : id_cita,
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        $(".cargaIcono").css("display","none");
                        $("#cerrar_Envio").css('display','none');
                        $("#btn_envio_refaccion").css('display','none');
                        $("#contenido").css('display','none');

                        $("#notificacion_cerrar_Envio").css('display',"inline-block");
                        $("#notificacion").text("Refaccion eliminada del presupuesto");

                        //Ocultamos la fila 
                        var renglon = $("#renglon_eliminar").val();
                        //Eliminamos el renglon
                        var fila = $("#fila_"+renglon);
                        fila.remove();

                        //Recalculamos valores
                        //Calculamos los nuevos totales
                        var limite = $("#indiceTablaMateria").val();
                        var subtotal = 0;

                        for (var i = 1; i <= limite ; i++) {
                            if ($("#cantidad_"+i).length > 0) {
                                //Como alternativa, se puede hacer la cuenta completa por renglon
                                var refaccion = parseFloat($("#cantidad_"+i).val()) * parseFloat($("#costoCM_"+i).val());
                                var mo = parseFloat($("#horas_"+i).val()) * parseFloat($("#totalReng_"+i).val());

                                var totalRenglon = refaccion+mo;
                                subtotal += totalRenglon;
                            }                            
                        }

                        //Generamos los nuevos subtotales
                        var anticipo = parseFloat($("#anticipoMaterial").val());
                        var cancelado = 0; //parseFloat($("#canceladoMaterial").val());
                        //Total sin iva y sin anticipo
                        var subtotal_limpio = subtotal - cancelado;

                        var iva = subtotal_limpio * 0.16;
                        var presupuesto = subtotal_limpio * 1.16;
                        //Total con iva y con anticipo
                        var presupuesto_total = presupuesto - anticipo;

                        //Vaciamos los nuevos resultados
                        $("#subTotalMaterial").val(parseFloat(subtotal_limpio));
                        $("#ivaMaterial").val(parseFloat(iva));
                        $("#totalMaterial").val(parseFloat(presupuesto_total));

                        $("#subTotalMaterialLabel").text(subtotal.toFixed(2));
                        $("#ivaMaterialLabel").text(iva.toFixed(2));
                        $("#totalMaterialLabel").text(presupuesto_total.toFixed(2));

                        $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));
                        //location.reload();
                    }else{
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        $("#notificacion").text(resp);
                    }
                }else{
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    $("#notificacion").text("Error al enviar la petición");
                }

            //Cierre de success
            },
            error:function(error){
                console.log(error);

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#notificacion").text("Error al enviar la petición");
            //Cierre del error
            }
        //Cierre del ajax
        });
    //De lo contrario enviamos un mensaje
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $(".cargaIcono").css("display","none");
        $("#errorEnvioCotizacion").text("Error al intentar enviar el presupuesto");
    }
});

//Proceso para refacciones (autorizar anticipo)
//Validamos credenciales del usuario
/*$("#btnCredencialAnticipo").on('click',function() {
    $("#AnticipoError").text("");
    //Recuperamos las credenciales
    var usuario = $("#usuarioAnticipo").val();
    var pass = $("#passAnticipo").val();

    //Verificamos los campos
    if ((usuario != "") && (pass != "")) {
        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/Cotizaciones/usuarioAnticipo",
            method: 'post',
            data: {
                user: usuario,
                pass: pass
            },
            success:function(resp){
                // console.log(resp);
                // console.log("Autorizacion anticipo: "+resp);
                if (resp.indexOf("handler           </p>")<1) {
                    if (resp == "OK") {
                        //Si las claves con correctas mostramos opcion de anticipo
                        $("#addAnticipo").css('display','block');
                        $("#credencialesAnticipo").css('display','none');
                    }else {
                        $("#AnticipoError").text(resp+".");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else {
        $("#AnticipoError").text("INFORMACIÓN INCOMPLETA.");
    }
});

//Guardamos el anticipo de la cotizacion
$("#btnAddAnticipo").on('click',function() {
    $("#AnticipoError").text("");
    //Recuperamos los valores
    var cantidad = $("#cantidadAnticipo").val();
    var comentario = $("#comentarioAnticipo").val();
    var idCoti = $("#idCotizacion").val();

    //Verificamos los campos
    if ((cantidad != "") && (idCoti != "")) {
        //Hacesmos la cuenta existente
        // Evitamos la basura en cotizaciones anteriores
        var conteoTotal = 0;
        var renglonesCuenta = $("#indiceTablaMateria").val();
        for (var i = 1; i <= renglonesCuenta; i++) {
            if (($("#totalOperacion_"+i).val() != "0")&&($("#horas_"+i).val() != "0")) {
                // conteoTotal += parseFloat($("#totalOperacion_"+i).val());
                //Como alternativa, se puede hacer la cuenta completa por renglon
                var cantidadPza = parseFloat($("#cantidad_"+i).val());
                var costoPza = parseFloat($("#costoCM_"+i).val());
                var horasMo = parseFloat($("#horas_"+i).val());
                var costoMo = parseFloat($("#totalReng_"+i).val());
                var totalRenglon = (cantidadPza*costoPza) + (horasMo*costoMo);
                // console.log(totalRenglon);
                conteoTotal += totalRenglon;
            }
        }
        // console.log("conteo: "+conteoTotal);

        var iva = conteoTotal * 0.16;
        var total = conteoTotal + iva;
        var totalAnticipo = total - cantidad;

        var url = $("#basePeticion").val();
        $.ajax({
            url: url+"multipunto/Cotizaciones/agregarAnticipo",
            method: 'post',
            data: {
                cantidad: cantidad,
                comentario: comentario,
                idCoti: idCoti,
                total: totalAnticipo
            },
            success:function(resp){
                console.log(resp);
                // console.log("Autorizacion anticipo: "+resp);
                if (resp.indexOf("handler           </p>")<1) {
                    if (resp == "OK") {
                        //Si se guardo correctamente la información

                        $("#credencialesAnticipo").css('display','block');
                        $("#addAnticipo").css('display','none');

                        $("#credencialesAnticipo").css('display','block');
                        $("#addAnticipo").css('display','none');
                        $("#passAnticipo").attr("disabled", true);
                        $("#usuarioAnticipo").attr("disabled", true);
                        $("#btnCredencialAnticipo").css('display','none');

                        //Bajamos el anticipo a la tabla
                        $("#totalMaterial").val(parseFloat(totalAnticipo));
                        $("#totalMaterialLabel").text(totalAnticipo.toFixed(2));

                        $("#anticipoMaterial").val(parseFloat(cantidad));
                        $("#anticipoMaterialLabel").text(parseFloat(cantidad).toFixed(2));

                        $("#presupuestoMaterialLabel").text(total.toFixed(2));

                        $("#AnticipoError").css("color","green");
                        $("#AnticipoError").text("ANTICIPO GUARDADO.");
                        $("#anticipoNota").text(comentario);
                    }else {
                        $("#AnticipoError").text(resp+".");
                    }
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }else {
        $("#AnticipoError").text("INFORMACIÓN INCOMPLETA.");
    }
});*/