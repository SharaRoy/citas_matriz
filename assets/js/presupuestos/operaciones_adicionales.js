//Agregar refacciones por parte del tecnico
$("#agregarRefaccionAsesor").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Recuperamos los campos obligatorios
    var condi_1 = $("#cantidad_"+indice).val();
    var condi_2 = $("#descripcion_"+indice).val();

    //Comprobamos que se hayan llenado los campos
    if ( (condi_1 != "") && (condi_2 != "") ){
        var tabla = $("#cuerpoMateriales");

        $(".aceptarCotizacion").attr("disabled",false);
        // console.log("Paso validacion");
        //Evitamos la edicion de los  elementos
        $("#cantidad_"+indice).attr('disabled','disabled');
        $("#descripcion_"+indice).attr('disabled','disabled');
        $("#costoCM_"+indice).attr('disabled','disabled');
        $("#horas_"+indice).attr('disabled','disabled');
        $("#totalReng_"+indice).attr('disabled','disabled');
        $("#pieza_"+indice).attr('disabled','disabled');
        $("#prioridad_"+indice).attr('disabled','disabled');
        $("input[name='existe_"+indice+"']").attr('disabled','disabled');

        //Bloqueamos el boton de buscar anterior
        $("#refaccionBusqueda_"+indice).attr("disabled", true);
        $("#refaccionBusqueda_"+indice).attr("data-toggle", false);

        //Mostramos el boton para eliminar fila
        $("#eliminarCM").attr('disabled','disabled');

        //Recuperamos los valores de toda la fila
        var fila = "";
        //0 Cantidad
        fila += parseFloat($("#cantidad_"+indice).val())+"_";
        //1 Descripcion
        fila += $("#descripcion_"+indice).val()+"_";
        //2 NUM. PIEZA
        fila += $("#pieza_"+indice).val()+"_";
        //3 EXISTE
        if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
            fila += $("input[name='existe_"+indice+"']:checked").val()+"_";
        }else {
            fila += ""+"_";
        }
        //4 PRECIO / P
        fila += parseFloat($("#costoCM_"+indice).val())+"_";
        //5 HORAS
        fila += parseFloat($("#horas_"+indice).val())+"_";
        //6 COSTO MO
        fila += parseFloat($("#totalReng_"+indice).val())+"_";

        var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
        var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
        
        //7 TOTAL RENGLON
        var total = refaccion+mo;

        fila += total.toFixed(2)+"_";

        //8 PRIORIDAD DE LA REFACCION
        fila += $("#prioridad_"+indice).val()+"_";

        $("#total_renglon_label_"+indice).text(total.toFixed(2));
        //Vaciamos lo recolectado en la variable
        $("#valoresCM_"+indice).val(fila);
        console.log(fila);

        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());

        //Calculamos los nuevos valores
        var nvo_subtotal = subtotal + total;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;

        //Imprimimos los valores
        $("#subTotalMaterialLabel").text(nvo_subtotal.toFixed(2));
        $("#subTotalMaterial").val(nvo_subtotal);

        $("#ivaMaterialLabel").text(iva.toFixed(2));
        $("#ivaMaterial").val(iva);

        $("#totalMaterialLabel").text(presupuesto.toFixed(2));
        $("#totalMaterial").val(presupuesto);

        $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));

        //Aumentamos el indice para los renglones
        indice++;
        $("#indiceTablaMateria").val(indice);

        //confirmamos el tipo de formulario
        if ($("#cargaFormulario").val() == "1") {
            var prp = "disabled";
        }else {
            var prp = "";
        }

        //Creamos el elemento donde se contendran todos los valores a enviar
        tabla.append("<tr id='fila_"+indice+"'>"+
            "<td align='center' style='border:1px solid #337ab7;'>"+
                "<input type='number' step='any' min='0'  class='input_field' id='cantidad_"+indice+"' name='cantidad_"+indice+"' value='1' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:90%;color:black;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='2' class='input_field_lg' id='descripcion_"+indice+"' name='descripcion_"+indice+"' style='width:90%;text-align:left;color:black;'></textarea>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;' align='center'>"+
                "<textarea rows='2' class='input_field_lg' id='pieza_"+indice+"' name='pieza_"+indice+"' style='width:90%;text-align:left;color:black;'></textarea>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' class='input_field' id='apuntaSi_"+indice+"' value='SI' name='existe_"+indice+"' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' id='apuntaNo_"+indice+"' class='input_field' name='existe_"+indice+"' value='NO' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='radio' id='apuntaPlanta_"+indice+"' class='input_field' name='existe_"+indice+"' value='PLANTA' style='transform: scale(1.5);'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "$<input type='number' step='any' min='0' class='input_field' id='costoCM_"+indice+"' name='costoRef_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='color:black;width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='number' step='any' min='0' class='input_field' id='horas_"+indice+"' name='hotasMo_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='color:black;width:90%;text-align:left;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "$<input type='number' step='any' min='0' id='totalReng_"+indice+"' name='costoMo_"+indice+"' class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='990' style='color:black;width:90%;text-align:left;' disabled>"+
                "<input type='hidden' id='totalOperacion_"+indice+"' value='0'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<label id='total_renglon_label_"+indice+"' style='color:darkblue;'>$0.00</label>"+
            "</td>"+
            "<td style='display:none;border:1px solid #337ab7;vertical-align: middle;' align='center'>"+
                "<input type='number' step='any' min='0' class='input_field' id='prioridad_"+indice+"' name='prioridad_"+indice+"' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='2' style='width:70%;text-align:center;'>"+
            "</td>"+
            "<td style='border:1px solid #337ab7;border-top:1px solid #337ab7;' align='center'>"+
                "<input type='checkbox' class='input_field' name='pzaGarantia[]' value='"+indice+"' style='transform: scale(1.5);'>"+
                "<input type='hidden' id='valoresCM_"+indice+"' name='registros[]'>"+
            "</td>"+
        "</tr>");
    }else {
        //Regresamos el control al campo que falta rellenar
        if (condi_1 == "") {
            $("#cantidad_"+indice).focus();
        }else{
            $("#descripcion_"+indice).focus();
        }
    }
});

//Eliminamos la ultima fila ingresada
$("#eliminarRefaccionAsesor").on('click',function(){
    //Recuperamos el indice de renglones
    var indice = $("#indiceTablaMateria").val();

    //Verificamos que no este en el primer renglón
    if (indice > 1) {
        var indice_anterior = indice - 1;

        //Restamos el monto del renglon eliminado
        var refaccion = parseFloat($("#cantidad_"+indice_anterior).val()) * parseFloat($("#costoCM_"+indice_anterior).val());
        var mo = parseFloat($("#horas_"+indice_anterior).val()) * parseFloat($("#totalReng_"+indice_anterior).val());
        
        var total = refaccion+mo;
        //console.log("total a eliminar: "+total);
        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());
        //console.log("subtotal actual: "+subtotal);
        //Calculamos los nuevos valores
        var nvo_subtotal = subtotal - total;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;
        //console.log("subtotal nuevo: "+nvo_subtotal);
        //console.log("iva nuevo: "+iva);
        //console.log("total nuevo: "+presupuesto);

        //Verificamos que los resultados no sean negativos para imprimirlos
        if (nvo_subtotal > -1) {
            //Imprimimos los valores
            $("#subTotalMaterialLabel").text(nvo_subtotal.toFixed(2));
            $("#subTotalMaterial").val(nvo_subtotal);

            $("#ivaMaterialLabel").text(iva.toFixed(2));
            $("#ivaMaterial").val(iva);

            $("#totalMaterialLabel").text(presupuesto.toFixed(2));
            $("#totalMaterial").val(presupuesto);

            $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));
        }
        
        //Eliminamos el renglon
        var fila = $("#fila_"+indice);
        fila.remove();

        //Decrementamos el indice para los renglones
        indice--;
        $("#indiceTablaMateria").val(indice);

        //Bloqueamos el envio del presupuesto si este no tiene informacion
        if (indice == 1) {
            $(".aceptarCotizacion").attr("disabled",true);
        }

        //Restablecemos los valores default del reglon anterior
        $("#cantidad_"+indice).val("1");
        $("#descripcion_"+indice).val("");
        $("#pieza_"+indice).val("");
        $("#costoCM_"+indice).val("0");
        $("#horas_"+indice).val("0");
        $("#totalOperacion_"+indice).val("0")
        $("#totalReng_"+indice).val("990");
        $("#prioridad_"+indice).val("2");
        $("#total_renglon_label_"+indice).text("$0.00");

        $("input[name='existe_"+indice+"']:radio").attr('disabled',false);

        //Desbloqueamos el renglon para la edición
        $("#cantidad_"+indice).attr('disabled',false);
        $("#descripcion_"+indice).attr('disabled',false);
        $("#costoCM_"+indice).attr('disabled',false);
        $("#horas_"+indice).attr('disabled',false);
        $("#pieza_"+indice).attr('disabled',false);
        $("#prioridad_"+indice).attr('disabled',false);
        $("input[name='existe_"+indice+"']").attr('disabled',false);
        $("#totalReng_"+indice).attr('disabled',false);
    }else {
        $(".aceptarCotizacion").attr("disabled",true);
    }
});


//Vista del cliente
//Liberamos los campos cuando se envie la cotizacion
//$(".terminarCotizacion").on('click',function() {
    //console.locg("click en condicional 2");
    //$("input").attr("disabled", false);
    //$("textarea").attr("disabled", false);
    //document.forms[0].submit();
//});

//Envio de entrega de refacciones
$(".proceso_solicitud").on('click',function() {
    //Recuperamos el id_de la cita
    var id_cita = $("#idOrdenTempCotiza").val().trim();
    //Recuperamos el nuevo estatus
    var estatus = $("#valorEvento").val().trim();
    //Recuperamos el tipo de refacciones que se afectaran
    var presupuesto = $("#evento").val().trim();

    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");

    var base = $("#basePeticion").val().trim();

    //Verificamos que ninguna variable se mande vacia
    if ((id_cita != "")&&(estatus != "")&&(presupuesto != "")) {
        $.ajax({
            url: base+"presupuestos/Presupuestos/cambioEstatusRefacciones",
            method: 'post',
            data: {
                id_cita : id_cita,
                estatus : estatus,
                presupuesto : presupuesto
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        $(".cargaIcono").css("display","none");
                        location.reload();
                    }else{
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        $("#errorEnvioCotizacion").text(resp);
                    }
                }else{
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    $("#errorEnvioCotizacion").text(resp);
                }

            //Cierre de success
            },
            error:function(error){
                console.log(error);

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#errorEnvioCotizacion").text("Error al enviar la petición");
            //Cierre del error
            }
        //Cierre del ajax
        });
    //De lo contrario enviamos un mensaje
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $(".cargaIcono").css("display","none");
        $("#errorEnvioCotizacion").text("Verificar el numero de orden");
    }
});

//Ocultamos botones para envio total 
$("#c_parcial").on('click',function() {
    //Limpiamos el color de los renglones
    $(".fila").css("background-color","#ffffff");
    //Ocultamos los botones
    $(".todo_p").css('display','none');
    $(".parcial_p").css('display','inline-block');
    $(".eleccionColumna").css('display','revert');
});

//Mostramos los botones para envio total
$("#c_cancelar_p").on('click',function() {
    //Limpiamos el color de los renglones
    $(".fila").css("background-color","#ffffff");
    //Ocultamos los botones
    $(".parcial_p").css('display','none');
    $(".todo_p").css('display','inline-block');
    $(".eleccionColumna").css('display','none');

    //Limpiamos el total de la opercion
    var anticipo = parseFloat($("#anticipoMaterial").val());
    var nvo_cancelado = 0;
    //Imprimimos los valores
    $("#canceladoMaterial").val(nvo_cancelado);
    $("#canceladoMaterialLabel").text(nvo_cancelado.toFixed(2));

    //Recuperamos el total establecido
    var old_subtotal = parseFloat($("#subTotalMaterial").val());
    
    //Obtenemos nuevos valores a mostrar
    var nvo_subtotal = old_subtotal - nvo_cancelado;
    var iva = nvo_subtotal * 0.16;
    var presupuesto = nvo_subtotal * 1.16;
    var presupuesto_final = presupuesto - anticipo;

    //Imprimimos el nuevo subtotal
    $("#presupuestoTemptoMaterialLabel").text(nvo_subtotal.toFixed(2));

    //Imprimimos el iva
    $("#ivaMaterial").val(iva);
    $("#ivaMaterialLabel").text(iva.toFixed(2));
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(presupuesto);
    $("#totalMaterialLabel").text(presupuesto.toFixed(2));
    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text(presupuesto_final.toFixed(2));
});

//Botones para aceptar/rechara una refaccion (presupuesto parcial)
//Aceptar refaccion
function aceptarRefaccion(renglon){
    //Pintamos el renglon
    $("#fila_indicador_"+renglon).css("background-color","#c7ecc7");
    //Ocultamos el boton preionado
    $("#interruptorA_"+renglon).css('display','none');
    $("#interruptorB_"+renglon).css('display','revert');

    //Verificamos si ya se habia hecho algun cambio (para las operaciones)
    if ($("#bandera_decision_"+renglon).val() == "0") {
        //Activamos la bandera
        $("#bandera_decision_"+renglon).val("1");
    //Si ya se habia activado la bandera, entonces se saca el valor del renglon del cancelado
    }else{
        var anticipo = parseFloat($("#anticipoMaterial").val());

        var cancelado = parseFloat($("#canceladoMaterial").val());
        var renglon_valor = parseFloat($("#total_renglon_"+renglon).val());

        //Le quitamos el valor del renglon a total cancelado
        var nvo_cancelado = cancelado - renglon_valor;
        //Imprimimos los valores
        $("#canceladoMaterial").val(nvo_cancelado);
        $("#canceladoMaterialLabel").text(nvo_cancelado.toFixed(2));

        //Recuperamos el total establecido
        var old_subtotal = parseFloat($("#subTotalMaterial").val());
        
        //Obtenemos nuevos valores a mostrar
        var nvo_subtotal = old_subtotal - nvo_cancelado;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;
        var presupuesto_final = presupuesto - anticipo;

        //Imprimimos el nuevo subtotal
        $("#presupuestoTemptoMaterialLabel").text(nvo_subtotal.toFixed(2));

        //Imprimimos el iva
        $("#ivaMaterial").val(iva);
        $("#ivaMaterialLabel").text(iva.toFixed(2));
        //Imprimimos el total  menos el anticipo
        $("#totalMaterial").val(presupuesto);
        $("#totalMaterialLabel").text(presupuesto.toFixed(2));
        //Imprimimos el total con iva (presupuesto original)
        $("#presupuestoMaterialLabel").text(presupuesto_final.toFixed(2));
    }

    //Guardamos la decision
    $("#decision_"+renglon).val("1");
}

//rechazar refaccion
function rechazarRefaccion(renglon){
    //Pintamos el renglon
    $("#fila_indicador_"+renglon).css("background-color","#f5acaa");
    //Ocultamos el boton preionado
    $("#interruptorB_"+renglon).css('display','none');
    $("#interruptorA_"+renglon).css('display','revert');

    //Verificamos si ya se habia hecho algun cambio (para las operaciones)
    if ($("#bandera_decision_"+renglon).val() == "0") {
        //Activamos la bandera
        $("#bandera_decision_"+renglon).val("1");   
    }

    var anticipo = parseFloat($("#anticipoMaterial").val());

    var cancelado = parseFloat($("#canceladoMaterial").val());
    var renglon_valor = parseFloat($("#total_renglon_"+renglon).val());

    //Le quitamos el valor del renglon a total cancelado
    var nvo_cancelado = cancelado + renglon_valor;

    //Imprimimos los valores
    $("#canceladoMaterial").val(nvo_cancelado);
    $("#canceladoMaterialLabel").text(nvo_cancelado.toFixed(2));

    //Recuperamos el total establecido
    var old_subtotal = parseFloat($("#subTotalMaterial").val());
    
    //Obtenemos nuevos valores a mostrar
    var nvo_subtotal = old_subtotal - nvo_cancelado;
    //Imprimimos el nuevo subtotal
    $("#presupuestoTemptoMaterialLabel").text((old_subtotal - nvo_cancelado).toFixed(2));

    var iva = nvo_subtotal * 0.16;
    //Imprimimos el iva
    $("#ivaMaterial").val(iva);
    $("#ivaMaterialLabel").text(iva.toFixed(2));

    var presupuesto = nvo_subtotal * 1.16;
    //Imprimimos el total  menos el anticipo
    $("#totalMaterial").val(presupuesto);
    $("#totalMaterialLabel").text(presupuesto.toFixed(2));
    
    var presupuesto_final = presupuesto - anticipo;
    //Imprimimos el total con iva (presupuesto original)
    $("#presupuestoMaterialLabel").text(presupuesto_final.toFixed(2));

    //Guardamos la decision
    $("#decision_"+renglon).val("0");
}

//Modal para envio de del presupuesto afectado por el cliente
$("#c_aceptar").on('click',function() {
    $("#recargar_presupuesto").css("background-color","#4cae4c");
    $("#recargar_presupuesto").css("color","white");

    $("#AlertaModalCuerpo").css("background-color","#c7ecc7");
    $("#AlertaModalCuerpo").css("border-color","#4cae4c");

    $("#tituloForm").text("¿Está seguro de aceptar totalmente el presupuesto? NO podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");
    
    //Llenamos los datos que se enviaran
    //Para el caso del presupuesto aceptado, el monto a cancelar es "0"
    $("#desicion_cancelado").val("0");
    $("#desicion_subtotal").val(parseFloat($("#subTotalMaterial").val()));

    //Para el caso del presupuesto aceptado, todo se va en uno
    var renglones = "";
    for (var i = 1 ; i <= $("#indiceTablaMateria").val(); i++) {
        var id = $("#id_refaccion_"+i).val().trim();
        //var desicion = $("#decision_"+i).val().trim();
        renglones += id+"-"+"1"+"=";
    }

    $("#desicion").val(renglones);
    $("#aprobacion_refacciones").val("Si");
});

$("#c_enviar_presupuesto").on('click',function() {
    $("#recargar_presupuesto").css("background-color","#eea236");
    $("#recargar_presupuesto").css("color","white");

    $("#AlertaModalCuerpo").css("background-color","#f0ad4e");
    $("#AlertaModalCuerpo").css("border-color","#eea236");

    $("#tituloForm").text("¿Está seguro de enviar el siguiente presupuesto? NO podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");
    
    //Llenamos los datos que se enviaran
    //Para el caso del presupuesto parcial, recuperamos la seleccion de ids con sus respectivas decisiones
    var renglones = "";
    for (var i = 1 ; i <= $("#indiceTablaMateria").val(); i++) {
        var id = $("#id_refaccion_"+i).val().trim();
        var desicion = $("#decision_"+i).val().trim();
        renglones += id+"-"+desicion+"=";
    }

    $("#desicion").val(renglones);
    //Para el caso del presupuesto parcial, el monto a cancelar lo establece el usuario
    $("#desicion_cancelado").val(parseFloat($("#canceladoMaterial").val()));
    $("#desicion_subtotal").val(parseFloat($("#subTotalMaterial").val()));
    $("#aprobacion_refacciones").val("Val");
});

$("#c_rechazar").on('click',function() {
    $("#recargar_presupuesto").css("background-color","#d43f3a");
    $("#recargar_presupuesto").css("color","white");

    $("#AlertaModalCuerpo").css("background-color","#f5acaa");
    $("#AlertaModalCuerpo").css("border-color","#d43f3a");

    $("#tituloForm").text("¿Está seguro de rechazar totalmente el presupuesto? NO  podrás cambiar de decisión");
    $("#tituloForm").css("font-weight","bold");

    //Llenamos los datos que se enviaran
    //Para el caso del presupuesto rechazado, el monto a cancelar lo establece el usuario
    $("#desicion_cancelado").val(parseFloat($("#subTotalMaterial").val()));
    $("#desicion_subtotal").val(parseFloat($("#subTotalMaterial").val()));
    //Para el caso del presupuesto rechazado, todo se va en ceros
    var renglones = "";
    for (var i = 1 ; i <= $("#indiceTablaMateria").val(); i++) {
        var id = $("#id_refaccion_"+i).val().trim();
        //var desicion = $("#decision_"+i).val().trim();
        renglones += id+"-"+"0"+"=";
    }

    $("#desicion").val(renglones);
    $("#aprobacion_refacciones").val("No");
});

//Envio de presupuesto afectado por el cliente
$("#enviar_presupuesto").on('click',function() {
    //Recuperamos la valoracion del renglon
    var renglones = $("#desicion").val().trim();
    //Recuperamos el monto a cancelar
    var cancelado = $("#desicion_cancelado").val().trim();
    //Recuperamos el subtotal original
    var subtotal = $("#desicion_subtotal").val().trim();
    //Recuperamos la categoria del presupuesto
    var desicion = $("#aprobacion_refacciones").val().trim();
    //Recuperamos id de la cita
    var id_cita = $("#id_cita").val().trim();

    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    $("#errorEnvioCotizacion").text("");

    var base = $("#basePeticion").val().trim();

    //Verificamos que ninguna variable se mande vacia
    if ((renglones != "")&&(desicion != "")&&(id_cita != "")) {
        $.ajax({
            url: base+"presupuestos/Presupuestos_adicional/presupuestoAfectado",
            method: 'post',
            data: {
                renglones : renglones,
                cancelado : cancelado,
                subtotal : subtotal,
                desicion : desicion,
                id_cita : id_cita
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        $(".cargaIcono").css("display","none");
                        $("#recargar_presupuesto").attr('hidden',false);
                        $("#cancelarCoti").attr('hidden',true);
                        $("#enviar_presupuesto").attr('hidden',true);
                        $("#tituloForm").attr('hidden',true);
                        //location.reload();
                    }else{
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        $("#errorEnvioCotizacion").text(resp);
                    }
                }else{
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    $("#errorEnvioCotizacion").text("Error al enviar la petición");
                }

            //Cierre de success
            },
            error:function(error){
                console.log(error);

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                $("#errorEnvioCotizacion").text("Error al enviar la petición");
            //Cierre del error
            }
        //Cierre del ajax
        });
    //De lo contrario enviamos un mensaje
    }else{
        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
        $(".cargaIcono").css("display","none");
        $("#errorEnvioCotizacion").text("Error al intentar enviar el presupuesto");
    }
});