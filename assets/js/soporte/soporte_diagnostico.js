$(document).ready(function() {
    var status_edicion = $("#orden_abierta").val();
    var tipo_edicion = $("#tipo_vista").val();
    //Si el clinete ya firmo no se pueden editar las firmas
    if (status_edicion == "0") {
        $("#error_envio_f").css('color','red');
        $("#proceso_edicion_dig").css('display','none');
        $("#resultado_proceso").text("LA ORDEN DE SERVICIO YA FUE CERRADA, YA NO SE PERMITE LA EDICIÓN");
    }else{
    	$("#proceso_edicion_dig").css('display','inline-block');
    	$("#resultado_proceso").text("");
    }
});

$("#proceso_edicion_dig").on('click', function (e){
    console.log("click proceso");
    var orden = $("#id_cita").val();

    $(".cargaIcono").css("display","inline-block");
    var base = $("#basePeticion").val();
    var tipo_edicion = $("#tipo_vista").val();

    var usuario = $("input[name='nombre_edicion']").val();
    var motivo = $("#motivo_edicion").val();

    if ((usuario != "") && (motivo != "")) {
        var pq_datos = new FormData();
        pq_datos.append('orden', orden);
        pq_datos.append('usuario_log', $("input[name='no_user']").prop('value')); 
        pq_datos.append('n_usuario_log', $("input[name='nom_user']").prop('value'));
        
        pq_datos.append('tipo_pre', tipo_edicion); 
        pq_datos.append('usuario', usuario); 
        pq_datos.append('motivo', motivo);

        if (tipo_edicion == "1") {
            pq_datos.append('nv_orden', $("input[name='nvo_num_orden']").prop('value')); 
        }

        $.ajax({
            url: base+"soporte/Soporte/editar_diagnostico", 
            type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
            contentType: false,
            data: pq_datos, // Al atributo data se le asigna el objeto FormData.
            processData: false,
            cache: false,
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    var datos = resp.split("=");
                    if (datos[0] == "OK") {
                        $("#error_envio_f").css('color','darkgreen');
                        $("#error_envio_f").text("ACTUALIZADO CON EXITO....");
                        
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        revisar_evidencias(datos[1]);
                    } else {
                        //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                        $(".cargaIcono").css("display","none");
                        $("#error_envio_f").css('color','red');
                        $("#error_envio_f").text(datos[0]);

                        //$("#resultado_proceso").css('color','red');
                        //$("#resultado_proceso").text(datos[0]);
                    }
                }else{
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    $("#error_envio_f").css('color','red');
                    $("#error_envio_f").text("SE PRODUJO UN ERROR");

                    //$("#resultado_proceso").css('color','red');
                    //$("#resultado_proceso").text("SE PRODUJO UN ERROR");
                }
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                $(".cargaIcono").css("display","none");
                //$("#error_envio_f").css('color','red');
                //$("#error_envio_f").text("SE PRODUJO UN ERROR**");

                $("#resultado_proceso").css('color','red');
                $("#resultado_proceso").text("SE PRODUJO UN ERROR");
            }
        });
    } else {
        $(".cargaIcono").css("display","none");
        $("#resultado_proceso").text("Se debe indicar quien realiza la edición y el motivo");
    }
});

function revisar_evidencias(liga){
    $(".cargaIcono").css("display","inline-block");    
    $("#error_envio_f").css('color','darkgreen');
    $("#error_envio_f").text("ACTUALIZADO CON EXITO....(Revisando evidencias)");

    var archivos = $('#evidencias')[0].files;

    var url = $("#basePeticion").val();
    var total_archivos = archivos.length;
    if (total_archivos > 0) {
        for (var i = archivos.length - 1; i == 0; i--) {
            var pq_datos = new FormData();
            pq_datos.append('orden', $("#id_cita").val());
            pq_datos.append('archivo',archivos[i]);
            pq_datos.append('liga_int',liga);
            pq_datos.append('limite',i);
            pq_datos.append('soporte','Diagnostico');

            if (archivos[i].name != "") {
                $.ajax({
                    url: url+"soporte/Soporte/cargar_evidencias",
                    //url: url+"presupuestos/Presupuestos/enviar_archivos_v2",
                    type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
                    contentType: false,
                    data: pq_datos, // Al atributo data se le asigna el objeto FormData.
                    processData: false,
                    cache: false,
                    success:function(resp){
                        console.log(resp);
                        if (resp.indexOf("handler           </p>")<1) {
                            if (resp == "OK") {
                                $("#error_envio_f").text("ACTUALIZADO CON EXITO....(Cargando evidencia ["+(total_archivos - i)+" / "+total_archivos+"])");
                            } else if (resp == "FIN") { 
                                document.getElementById("evidencias").value = "";
                                $("#error_envio_f").text("ACTUALIZADO CON EXITO....(Evidencia cargada)");
                                $(".cargaIcono").css("display","none");
                            } else {
                                $(".cargaIcono").css("display","none");   
                                $("#error_envio_f").text(resp);
                            }    
                        }
                        
                    //Cierre de success
                    },
                    error:function(error){
                        console.log(error);
                        $("#errorEnvio").text("Error al enviar los archivos");
                        $(".cargaIcono").css("display","none");    
                    //Cierre del error
                    }
                //Cierre del ajax
                }); 
            }
        } 
    }else{
        $("#error_envio_f").text("ACTUALIZADO CON EXITO....(Sin evidencia)");
    }
}

