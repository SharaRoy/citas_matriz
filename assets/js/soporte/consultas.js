$(document).ready(function() {
    var carga_orden = $("#busqueda_gral").val();
    var tipo_edicion = $("#tipo_vista").val();
    if (((carga_orden != "")&&(carga_orden != "0"))&&(tipo_edicion == "Panel")) {
        recopilar_informacion(carga_orden);
    }
});

$("#ingreso_login").on('click', function (e){
    var usuario = $("input[name='inputUsuario']").val();
    var pass = $("input[name='inputPassword']").val();

    if ((usuario != "")&&(pass != "")) {
        $(".cargaIcono").css("display","inline-block");
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"soporte/Soporte_login/verifica_login", 
            method: 'post',
            data: {
                usuario : usuario,
                pass : pass,
            },
            success:function(resp){
                console.log(resp);
                if (resp.indexOf("handler         </p>")<1) {
                    if (resp == "OK") {
                        $("#error_formulario").text("INICIANDO....");
                        location.href = base+"Panel_soporte/0";
                    } else {
                        $("#error_formulario").text("EL USUARIO Y/O CONTRASEÑA SON INCORRECTAS");
                    }
                }else{
                    $("#error_formulario").text("SE PRODUJO UN ERROR");
                }

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                $(".cargaIcono").css("display","none");
                $("#error_formulario").text("SE PRODUJO UN ERROR**");
            }
        });
        //document.forms[0].submit();
    } else {
        $("#error_formulario").text("FALTA INDICAR ALGUN CAMPO");
    }
});


$("#buscar").on('click', function (e){
    e.preventDefault();
    var orden = $("#busqueda_gral").val();

    recopilar_informacion(orden);
});

function recopilar_informacion(orden){
    $(".cargaIcono").css("display","inline-block");
    var base = $("#basePeticion").val();

    if ((orden != "")&&(orden != "0")) {
        $.ajax({
            url: base+"soporte/Soporte/cargar_datos", 
            method: 'post',
            data: {
                orden : orden,
            },
            success:function(resp){
                //console.log(resp);

                if (resp.indexOf("handler         </p>")<1) {
                    var data = JSON.parse(resp);
                    
                    if (data["informacion"] == 1) {
                        $("#error_busqueda").text("");

                        //Llenamos los campos de informacion
                        $("#lb_no_orden").text(data["id_cita"]);
                        $("#lb_folio").text(data["folio_externo"]);
                        $("#lb_serie").text(data["serie"]);
                        $("#lb_placas").text(data["placas"]);

                        $("#lb_modelo").text(data["vehiculo"]);
                        $("#lb_frecepcion").text(data["fecha_recepcion"]);
                        $("#lb_fpromesa").text(data["fecha_promesa"]);

                        $("#lb_asesor").text(data["asesor"]);
                        $("#lb_tecnico").text(data["tecnico"]);
                        $("#lb_cliente").text(data["cliente"]);

                        //Botones de documentación
                        if (data["documentacion"] == 0) {
                            $("#btn_doc_act").css("display","none");
                            $("#btn_doc_alta").css("display","inline-block");
                        } else {
                            $("#btn_doc_alta").css("display","none");
                            $("#btn_doc_act").css("display","inline-block");
                        }

                        $("#reenvio_orden").css("display","inline-block");
                        $("#historial_soporte").attr('href', base+'Soporte_Historial/'+data["id_cita_url"]);
                        if (data["historial_soporte"] == 0) {
                            $("#historial_soporte").css("display","none");
                        } else {
                            $("#historial_soporte").css("display","inline-block");
                        }

                        if (data["diagnostico"] == "1") {
                            $("#opciones_diag").css("display","inline-block");
                            $("#btn-doc-diag").css("display","inline-block");

                            //Asignamos la ruta de la multipunto
                            $("#btn1-diag").attr('href', base+'Soporte_Diagnostico/'+data["id_cita_url"]+'/1');
                            $("#btn2-diag").attr('href', base+'Soporte_Diagnostico/'+data["id_cita_url"]+'/2');
                            $("#btn3-diag").attr('href', base+'Soporte_Diagnostico/'+data["id_cita_url"]+'/3');

                            $("#btn-doc-diag").attr('href', base+'OrdenServicio_Revision/'+data["id_cita_url"]+'');
                        } else {
                            $("#opciones_diag").css("display","none");
                            $("#btn-doc-diag").css("display","none");
                        }

                        //Visualizar opciones de la documentacion
                        if (data["multipunto"] == "1") {
                            $("#opciones_hm").css("display","inline-block");
                            $("#btn-doc-hm").css("display","inline-block");

                            //Asignamos la ruta de la multipunto
                            $("#btn1-hm").attr('href', base+'Soporte_Multipunto/'+data["id_cita_url"]+'/1');
                            $("#btn2-hm").attr('href', base+'Soporte_Multipunto/'+data["id_cita_url"]+'/2');
                            $("#btn3-hm").attr('href', base+'Soporte_Multipunto/'+data["id_cita_url"]+'/3');
                            $("#btn4-hm").attr('href', base+'Soporte_Multipunto/'+data["id_cita_url"]+'/4');
                            $("#btn5-hm").attr('href', base+'Soporte_Multipunto/'+data["id_cita_url"]+'/5');

                            $("#btn-doc-hm").attr('href', base+'Multipunto_Revision/'+data["id_cita_url"]+'');
                        } else {
                            $("#opciones_hm").css("display","none");
                            $("#btn-doc-hm").css("display","none");
                        }

                        //Visualizar opciones de la documentacion
                        if (data["presupuesto"] == "1") {
                            $("#opciones_pm").css("display","inline-block");
                            $("#btn-doc-pm").css("display","inline-block");

                            //Asignamos la ruta de la multipunto
                            $("#btn1-pm").attr('href', base+'Soporte_Presupuesto/'+data["id_cita_url"]+'/4');
                            $("#btn2-pm").attr('href', base+'Soporte_Presupuesto/'+data["id_cita_url"]+'/1');
                            $("#btn3-pm").attr('href', base+'Soporte_Presupuesto/'+data["id_cita_url"]+'/2');
                            $("#btn4-pm").attr('href', base+'Soporte_Presupuesto/'+data["id_cita_url"]+'/3');
                            $("#btn5-pm").attr('href', base+'Soporte_Presupuesto/'+data["id_cita_url"]+'/5');

                            //Validamos si se habilitan o no las opciones
                            if (data["presupuesto_datos"]["envio_ventanilla"] == 0) {
                                $("#btn4-pm").css('display','none');
                                $("#label4-pm").text("El presupuesto no ha llegado a este paso");
                            } else{
                                $("#btn4-pm").css('display','inline-block');
                                $("#label4-pm").text("");
                            }

                            if (data["presupuesto_datos"]["envio_jdt"] == 0) {
                                $("#btn3-pm").css('display','none');
                                $("#label3-pm").text("El presupuesto no ha llegado a este paso");
                            } else{
                                $("#btn3-pm").css('display','inline-block');
                                $("#label3-pm").text("");
                            }

                            if (data["presupuesto_datos"]["envio_asesor"] == 0) {
                                $("#btn2-pm").css('display','none');
                                $("#label2-pm").text("El presupuesto no ha llegado a este paso");
                            } else{
                                $("#btn2-pm").css('display','inline-block');
                                $("#label2-pm").text("");
                            }

                            console.log(data["presupuesto_datos"]["ref_cgarantias"]);
                            if (data["presupuesto_datos"]["ref_cgarantias"] == 0) {
                                $("#btn5-pm").css('display','none');
                                $("#label5-pm").text("El presupuesto no tiene refacciones con garantías");
                            } else{
                                if (data["presupuesto_datos"]["envio_garantias"] == 0) {
                                    $("#btn5-pm").css('display','none');
                                    $("#label5-pm").text("El presupuesto no ha llegado a este paso");
                                } else{
                                    $("#btn5-pm").css('display','inline-block');
                                    $("#label5-pm").text("");
                                }
                            }
                            
                            $("#btn-doc-pm").attr('href', base+'Alta_Presupuesto/'+data["id_cita_url"]+'');
                        } else {
                            $("#opciones_pm").css("display","none");
                            $("#btn-doc-pm").css("display","none");
                        }

                        //Visualizar opciones de la documentacion
                        if (data["voz_cliente"] == "1") {
                            $("#opciones_vc").css("display","inline-block");
                            $("#btn-doc-vc").css("display","inline-block");
                        } else {
                            $("#opciones_vc").css("display","none");
                            $("#btn-doc-vc").css("display","none");
                        }

                        if (data["evidencia_voz_cliente"] != "") {
                            $("#reproductor").css("display","inline-block");
                            
                            $("#btn-doc-vc").attr('href', base+'VCliente_Revision/'+data["id_cita_url"]+'');
                            $("#reproductor").attr("src",base+""+data["evidencia_voz_cliente"]);
                        } else {
                            $("#reproductor").css("display","none");
                        }

                    } else {
                        $("#error_busqueda").text("NO SE ENCONTRO LA ORDEN");
                    }
                }else{
                    $("#error_busqueda").text("SE PRODUJO UN ERROR");
                }

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
            //Cierre de success
            },
              error:function(error){
                console.log(error);
                $(".cargaIcono").css("display","none");
                $("#error_busqueda").text("SE PRODUJO UN ERROR**");
            }
        });
    }else{
        $("#error_busqueda").text("NO SE INGRESO UN NO. DE ORDEN VALIDO");
    }
}
    
function actualizar_doc(destino){
    var orden = $("#busqueda_gral").val();

    $(".cargaIcono").css("display","inline-block");
    var base = $("#basePeticion").val();

    $.ajax({
        url: base+"soporte/Soporte/actualizar_documentacion", 
        method: 'post',
        data: {
            orden : orden,
            destino : destino
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                if (resp == "OK") {
                    $("#actualizacion_orden").text("ACTUALIZADO CON EXITO....");
                    if (destino == "1") {
                        $("#btn_doc_act").css("display","inline-block");
                        $("#btn_doc_alta").css("display","none");
                    }
                } else {
                    $("#actualizacion_orden").text(resp);
                }
            }else{
                $("#actualizacion_orden").text("SE PRODUJO UN ERROR");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre de success
        },
          error:function(error){
            console.log(error);
            $(".cargaIcono").css("display","none");
            $("#actualizacion_orden").text("SE PRODUJO UN ERROR**");
        }
    });
}

function reenvio_orden() {
    $(".cargaIcono").css("display","inline-block");

    var orden = $("#busqueda_gral").val();
    var base = $("#basePeticion").val();

    $.ajax({
        url: base+"soporte/Soporte/reenvio_orden", 
        method: 'post',
        data: {
            id_cita: orden,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                if (resp == "OK") {
                    window.location.reload();
                } else {
                    $("#actualizacion_orden").text(resp);
                }
            }else{
                $("#actualizacion_orden").text("SE PRODUJO UN ERROR EN RENVIAR LA ORDEN");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}