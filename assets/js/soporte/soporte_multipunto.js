$(document).ready(function() {
    var termina_cliente = $("#firma_cliente").val();
    var tipo_edicion = $("#tipo_vista").val();
    //Si el clinete ya firmo no se pueden editar las firmas
    if (termina_cliente == "1") {
        $(".btnfirma").css('display','none');
        $("#error_envio_f").css('color','red');
        
        $("#resultado_proceso").text("HOJA MULTIPUNTO YA FUE FIRMADO POR EL CLIENTE, LA EDICIÓN ESTA LIMITADA A INDICADORES");
        
        if(tipo_edicion != '4'){
        	$("#proceso_edicion_hm").css('display','none');
        }

    }else{
    	$(".btnfirma").css('display','inline-block');
    	$("#proceso_edicion_hm").css('display','inline-block');
    	$("#resultado_proceso").text("");
    }
});

function eliminar_firma(origen){
	$("input[name='origne_firma']").val(origen);

	if(origen == "tecnico"){
		$("#titulo_firma").text("¿Esta seguro de que quiere eliminar la firma del técnico?");
		$("#alerta_ftec").css("display","inline-block");
	} else if(origen == "jefetaller"){
		$("#titulo_firma").text("¿Esta seguro de que quiere eliminar la firma del jefe de taller?")
		$("#alerta_ftec").css("display","none");
	}
}

function eliminar_reg(id_reg){
	$("#reg_elim_lb").text("# "+id_reg);
	$("#id_reg_eliminar").val(id_reg);
}

$("#proceso_edicion_hm").on('click', function (e){
	console.log("click");
	var orden = $("#id_cita").val();

    $(".cargaIcono").css("display","inline-block");
    var base = $("#basePeticion").val();
    var tipo_edicion = $("#tipo_vista").val();

    if (tipo_edicion == "4") {
  		var usuario = $("input[name='nombre_edicion_2']").val();
    	var motivo = $("#motivo_edicion_2").val();
  	} else {
  		var usuario = $("input[name='nombre_edicion']").val();
    	var motivo = $("#motivo_edicion").val();
  	}

    if ((usuario != "") && (motivo != "")) {
    	var pq_datos = new FormData();
	  	pq_datos.append('orden', orden);

	  	if (tipo_edicion == "4") {
	  		pq_datos.append('usuario_log', $("input[name='no_user_2']").prop('value')); 
	  		pq_datos.append('n_usuario_log', $("input[name='nom_user_2']").prop('value'));

	  		var bloque = $("input[name='bloque_asignado']").val();
	  		var color = $("#bloque_ind_color_"+bloque).val();
	  		var indicador = $("#bloque_ind_opc_"+bloque).val();
	  		//Para el bloque uno, solo son dos valores, se hace un ajuste
	  		if (bloque == "1") {
	  			if (indicador != "4") {
	  				if (color == "Aprobado") {
	  					color = "Si";
		  			} else {
		  				color = "No";
		  			}
	  			}
	  		}

	  		pq_datos.append('bloque_edita', bloque);
	  		pq_datos.append('indicador', indicador);
	  		pq_datos.append('indicador_color', color);
	  	} else {
	  		pq_datos.append('usuario_log', $("input[name='no_user']").prop('value')); 
	  		pq_datos.append('n_usuario_log', $("input[name='nom_user']").prop('value'));
	  	}
	  	
	  	pq_datos.append('tipo_pre', tipo_edicion); 
	  	pq_datos.append('usuario', usuario); 
	  	pq_datos.append('motivo', motivo);

	  	if (tipo_edicion == "1") {
	  		pq_datos.append('firma_remv', $("input[name='origne_firma']").prop('value'));
	  	}

	  	if (tipo_edicion == "3") {
	  		pq_datos.append('indicador_gral', $("input[name='id_reg_eliminar']").prop('value'));
	  	} else {
	  		pq_datos.append('indicador_gral', $("input[name='ref_gral']").prop('value'));
	  	}

	  	pq_datos.append('indicador_gral2', $("input[name='ref_gral2']").prop('value'));
	  	pq_datos.append('indicador_fi', $("input[name='ref_fi']").prop('value'));
	  	pq_datos.append('indicador_ti', $("input[name='ref_ti']").prop('value'));
	  	pq_datos.append('indicador_fd', $("input[name='ref_fd']").prop('value'));
	  	pq_datos.append('indicador_td', $("input[name='ref_td']").prop('value'));
	  	pq_datos.append('indicador_gral3', $("input[name='ref_gral3']").prop('value'));

	  	if (tipo_edicion == "5") {
	  		pq_datos.append('nv_orden', $("input[name='nvo_num_orden']").prop('value')); 
	  	}

	  	$.ajax({
	        url: base+"soporte/Soporte/editar_hojamultipunto", 
	        type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
	        contentType: false,
	        data: pq_datos, // Al atributo data se le asigna el objeto FormData.
	        processData: false,
	        cache: false,
	        success:function(resp){
	            console.log(resp);
	            if (resp.indexOf("handler         </p>")<1) {
	                var datos = resp.split("=");
                    if (datos[0] == "OK") {
	                	if ((tipo_edicion == "1")||(tipo_edicion == "4")) {
	                		$("#error_envio_f").css('color','darkgreen');
	                		$("#error_envio_f").text("ACTUALIZADO CON EXITO....");
                		    
                		    var archivos = $('#evidencias')[0].files;
						    var total_archivos = archivos.length;

						    if (total_archivos > 0) {
						    	revisar_evidencias(datos[1]);
	                		} else {
	                			location.reload();
	                		}
	                    	
	                	} else if(tipo_edicion == "2") {
	                		$("#resultado_proceso").css('color','darkgreen');
	                		$("#resultado_proceso").text("HOJA MULTIPUNTO LISTA PARA INICIARSE POR EL ASESOR");

	                		var archivos = $('#evidencias')[0].files;
						    var total_archivos = archivos.length;

						    if (total_archivos > 0) {
						    	revisar_evidencias(datos[1]);
	                		}
	                	} else if(tipo_edicion == "3") {
	                		$("#resultado_proceso").css('color','darkgreen');
	                		$("#resultado_proceso").text("HOJA MULTIPUNTO REASIGNADA, VOLVER A PROBAR EN EL TABLERO");
	                		
	                		var archivos = $('#evidencias')[0].files;
						    var total_archivos = archivos.length;

						    if (total_archivos > 0) {
						    	revisar_evidencias(datos[1]);
	                		} else {
	                			location.reload();
	                		}
	                	}
	                	//Si ya se termino de cargar la tabla, ocultamos el icono de carga
	            		$(".cargaIcono").css("display","none");
	                    
	                } else {
	                	//Si ya se termino de cargar la tabla, ocultamos el icono de carga
	            		$(".cargaIcono").css("display","none");
	                    $("#error_envio_f").css('color','red');
	                    $("#error_envio_f").text(datos[0]);

	                    $("#resultado_proceso").css('color','red');
	                    $("#resultado_proceso").text(datos[0]);
	                }
	            }else{
	            	//Si ya se termino de cargar la tabla, ocultamos el icono de carga
	            	$(".cargaIcono").css("display","none");
	                $("#error_envio_f").css('color','red');
	                $("#error_envio_f").text("SE PRODUJO UN ERROR");

	                $("#resultado_proceso").css('color','red');
	                $("#resultado_proceso").text("SE PRODUJO UN ERROR");
	            }
	        //Cierre de success
	        },
	          error:function(error){
	            console.log(error);
	            $(".cargaIcono").css("display","none");
	            $("#error_envio_f").css('color','red');
	            $("#error_envio_f").text("SE PRODUJO UN ERROR**");

	            $("#resultado_proceso").css('color','red');
	            $("#resultado_proceso").text("SE PRODUJO UN ERROR");
	        }
	    });
    } else {
    	$("#resultado_proceso").text("Se debe indicar quien realiza la edición y el motivo");
    }
});

function revisar_evidencias(liga){
    $(".cargaIcono").css("display","inline-block");    
    $("#error_envio_f").css('color','darkgreen');
    $("#error_envio_f").text("ACTUALIZADO CON EXITO....(Revisando evidencias)");

    var archivos = $('#evidencias')[0].files;

    var url = $("#basePeticion").val();
    var total_archivos = archivos.length;
    if (total_archivos > 0) {
        for (var i = archivos.length - 1; i == 0; i--) {
            var pq_datos = new FormData();
            pq_datos.append('orden', $("#id_cita").val());
            pq_datos.append('archivo',archivos[i]);
            pq_datos.append('liga_int',liga);
            pq_datos.append('limite',i);
            pq_datos.append('soporte','Hoja Multipunto');

            if (archivos[i].name != "") {
                $.ajax({
                    url: url+"soporte/Soporte/cargar_evidencias",
                    //url: url+"presupuestos/Presupuestos/enviar_archivos_v2",
                    type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
                    contentType: false,
                    data: pq_datos, // Al atributo data se le asigna el objeto FormData.
                    processData: false,
                    cache: false,
                    success:function(resp){
                        console.log(resp);
                        if (resp.indexOf("handler           </p>")<1) {
                            if (resp == "OK") {
                                $("#error_envio_f").text("ACTUALIZADO CON EXITO....(Cargando evidencia ["+(total_archivos - i)+" / "+total_archivos+"])");
                            } else if (resp == "FIN") { 
                                document.getElementById("evidencias").value = "";
                                $("#error_envio_f").text("ACTUALIZADO CON EXITO....(Evidencia cargada)");
                                $(".cargaIcono").css("display","none");
                            } else {
                                $(".cargaIcono").css("display","none");   
                                $("#error_envio_f").text(resp);
                            }    
                        }
                        
                    //Cierre de success
                    },
                    error:function(error){
                        console.log(error);
                        $("#errorEnvio").text("Error al enviar los archivos");
                        $(".cargaIcono").css("display","none");    
                    //Cierre del error
                    }
                //Cierre del ajax
                }); 
            }
        } 
    }else{
        $("#error_envio_f").text("ACTUALIZADO CON EXITO....(Sin evidencia)");
    }
}

function cambiar_indicador(bloque){
	if (bloque == "1") {
		$(".bloques_ind").css('display','none');
		$("#bloque_indicador_1").css('display','inline-block');
	} else if (bloque == "2") {
		$(".bloques_ind").css('display','none');
		$("#bloque_indicador_2").css('display','inline-block');
	}  else if (bloque == "3") {
		$(".bloques_ind").css('display','none');
		$("#bloque_indicador_3").css('display','inline-block');
	}  else if (bloque == "4") {
		$(".bloques_ind").css('display','none');
		$("#bloque_indicador_4").css('display','inline-block');
	}  else if (bloque == "5") {
		$(".bloques_ind").css('display','none');
		$("#bloque_indicador_5").css('display','inline-block');
	}  else if (bloque == "6") {
		$(".bloques_ind").css('display','none');
		$("#bloque_indicador_6").css('display','inline-block');
	}

	$("#bloque_asignado").val(bloque);
}
