$(document).ready(function() {
    var tipo_vista = $("#vista_informativa").val();
    if (tipo_vista == "1") {
        $('input').attr('disabled',true);
        $('textarea').attr('disabled',true);
    }
});

function regresar_presupuesto(presupuesto){
	var firma_asesor = $("#firma_asesor").val();
	var firma_jefetaller = $("#firma_jefetaller").val();
	var firma_garantia = $("#firma_garantia").val();
	var afecta_cliente = $("#afecta_cliente").val();
	var edo_ref = $("#edo_ref").val();

	if (presupuesto == "asesor") {
		//Reglas para validar edicion
		if (edo_ref != "0") {
			$("#resultado_proceso").text("Ya se solicitarón las refacciones en ventanilla, para edición debe reiniciarse todo el proceso del presupuesto en el panel del técnico");
		}else{
			envio_edicion(presupuesto);
		}
	} else if (presupuesto == "garantias") {
		if (firma_asesor != "") {
			$("#resultado_proceso").text("El presupuesto ya fue firmado por el asesor, para edición debe retirarse la firma");
		} else {
			envio_edicion(presupuesto);
		}
	} else if (presupuesto == "jtaller") {
		if (firma_asesor != "") {
			$("#resultado_proceso").text("El presupuesto ya fue firmado por el asesor, para edición debe retirarse la firma ");
		} else {
			envio_edicion(presupuesto);
		}
	} else if (presupuesto == "ventanilla") {
		if (firma_jefetaller != "") {
			$("#resultado_proceso").text("El presupuesto ya fue firmado por el jefe de taller, para edición debe retirarse la firma");
		} else if (firma_asesor != "") {
			$("#resultado_proceso").text("El presupuesto ya fue firmado por el asesor, para edición debe retirarse la firma y posteriormente la firma del jefe de taller");
		}else {
			envio_edicion(presupuesto);
		}
	}
}

function envio_edicion(tipo_pre){
    var orden = $("#id_cita").val();

    $(".cargaIcono").css("display","inline-block");
    var base = $("#basePeticion").val();

    var usuario = $("input[name='nombre_edicion']").val();
    var motivo = $("#motivo_edicion").val();

    if ((usuario != "") && (motivo != "")) {
		var pq_datos = new FormData();
	  	pq_datos.append('orden', $("input[name='id_cita']").prop('value')); 
	  	pq_datos.append('usuario_log', $("input[name='no_user']").prop('value')); 
	  	pq_datos.append('n_usuario_log', $("input[name='nom_user']").prop('value')); 
	  	pq_datos.append('tipo_pre', tipo_pre); 
	  	pq_datos.append('usuario', usuario); 
	  	pq_datos.append('motivo', motivo); 

	    $.ajax({
	        url: base+"soporte/Soporte/editar_presupuesto", 
	        type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
	        contentType: false,
	        data: pq_datos, // Al atributo data se le asigna el objeto FormData.
	        processData: false,
	        cache: false,
	        success:function(resp){
	            console.log(resp);
	            if (resp.indexOf("handler         </p>")<1) {
	                if (resp == "OK") {
	                    $("#actualizacion_orden").text("ACTUALIZADO CON EXITO....");
	                    location.reload();
	                } else {
	                    $("#actualizacion_orden").text(resp);
	                }
	            }else{
	                $("#actualizacion_orden").text("SE PRODUJO UN ERROR");
	            }

	            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
	            $(".cargaIcono").css("display","none");
	        //Cierre de success
	        },
	          error:function(error){
	            console.log(error);
	            $(".cargaIcono").css("display","none");
	            $("#actualizacion_orden").text("SE PRODUJO UN ERROR**");
	        }
	    });
    } else {
    	$("#resultado_proceso").text("Se debe indicar quien realiza la edición y el motivo");
    }

	    
}