//Firma
var limpiar_4 = document.getElementById("limpiar4");
var canvas_4 = document.getElementById("canvas_4");
var ctx_4 = canvas_4.getContext("2d");
var cw_4 = canvas_4.width = 430,
cx_4 = cw_4 / 2;
var ch_4 = canvas_4.height = 200,
cy_4 = ch_4 / 2;

var dibujar_4 = false;
var factorDeAlisamiento_4 = 5;
var Trazados_4 = [];
var puntos_4 = [];
ctx_4.lineJoin = "round";

limpiar_4.addEventListener('click', function(evt) {
    dibujar_4 = false;
    ctx_4.clearRect(0, 0, cw_4, ch_4);
    Trazados_4.length = 0;
    puntos_4.length = 0;
}, false);

canvas_4.addEventListener('mousedown', function(evt) {
    dibujar_4 = true;
    //ctx_4.clearRect(0, 0, cw_4, ch_4);
    puntos_4.length = 0;
    ctx_4.beginPath();
}, false);

canvas_4.addEventListener('mouseup', function(evt) {
    redibujarTrazados_4();
}, false);

canvas_4.addEventListener("mouseout", function(evt) {
    redibujarTrazados_4();
}, false);

canvas_4.addEventListener("mousemove", function(evt) {
    if (dibujar_4) {
        var m = oMousePos_4(canvas_4, evt);
        puntos_4.push(m);
        ctx_4.lineTo(m.x, m.y);
        ctx_4.stroke();
    }
}, false);

function reducirArray_4(n,elArray) {
    var nuevoArray = [];
    nuevoArray[0] = elArray[0];
    for (var i = 0; i < elArray.length; i++) {
        if (i % n == 0) {
            nuevoArray[nuevoArray.length] = elArray[i];
        }
    }
    nuevoArray[nuevoArray.length - 1] = elArray[elArray.length - 1];
    Trazados_4.push(nuevoArray);
}

function calcularPuntoDeControl_4(ry, a, b) {
    var pc = {}
    pc.x = (ry[a].x + ry[b].x) / 2;
    pc.y = (ry[a].y + ry[b].y) / 2;
    return pc;
}

function alisarTrazado_4(ry) {
    if (ry.length > 1) {
        var ultimoPunto = ry.length - 1;
        ctx_4.beginPath();
        ctx_4.moveTo(ry[0].x, ry[0].y);
        for (i = 1; i < ry.length - 2; i++) {
            var pc = calcularPuntoDeControl_4(ry, i, i + 1);
            ctx_4.quadraticCurveTo(ry[i].x, ry[i].y, pc.x, pc.y);
        }
        ctx_4.quadraticCurveTo(ry[ultimoPunto - 1].x, ry[ultimoPunto - 1].y, ry[ultimoPunto].x, ry[ultimoPunto].y);
        ctx_4.stroke();
    }
}

function redibujarTrazados_4(){
    dibujar_4 = false;
    ctx_4.clearRect(0, 0, cw_4, ch_4);
    reducirArray_4(factorDeAlisamiento_4,puntos_4);
    for(var i = 0; i < Trazados_4.length; i++)
    alisarTrazado_4(Trazados_4[i]);
}

function oMousePos_4(canvas_4, evt) {
    var ClientRect = canvas_4.getBoundingClientRect();
    return { //objeto
      x: Math.round(evt.clientX - ClientRect.left),
      y: Math.round(evt.clientY - ClientRect.top)
    }
}

//Guardar firma como imagen
$("#btnFirma_4").on('click', function(){
    guardarImagen_4(canvas_4);
    // $('#firmaAsesor').modal('hide');
});

function guardarImagen_4(canvas_4){
    var datoscanvas_4 = canvas_4.toDataURL(); //  'image/jpge'
    document.getElementById("firmaFirmaAsesor").src = datoscanvas_4;
    document.getElementById("rutaFirmaAsesor").value = datoscanvas_4;
}
