//Firma   (#canvas_1)
var limpiar2 = document.getElementById("limpiar2");
var canvas1 = document.getElementById("canvas_1");
var ctx_2 = canvas1.getContext("2d");
var cw_2 = canvas1.width = 430,
cx_2 = cw_2 / 2;
var ch_2 = canvas1.height = 200,
cy_2 = ch_2 / 2;

var dibujar_2 = false;
var factorDeAlisamiento_2 = 5;
var Trazados_2 = [];
var puntos_2 = [];
ctx_2.lineJoin = "round";

limpiar2.addEventListener('click', function(evt) {
    dibujar_2 = false;
    ctx_2.clearRect(0, 0, cw_2, ch_2);
    Trazados_2.length = 0;
    puntos_2.length = 0;
}, false);

canvas1.addEventListener('mousedown', function(evt) {
    dibujar_2 = true;
    //ctx_2.clearRect(0, 0, cw_2, ch_2);
    puntos_2.length = 0;
    ctx_2.beginPath();
}, false);

canvas1.addEventListener('mouseup', function(evt) {
    redibujarTrazados_2();
}, false);

canvas1.addEventListener("mouseout", function(evt) {
    redibujarTrazados_2();
}, false);

canvas1.addEventListener("mousemove", function(evt) {
    if (dibujar_2) {
        var m = oMousePos_2(canvas1, evt);
        puntos_2.push(m);
        ctx_2.lineTo(m.x, m.y);
        ctx_2.stroke();
    }
}, false);

function reducirArray_2(n,elArray) {
    var nuevoArray = [];
    nuevoArray[0] = elArray[0];
    for (var i = 0; i < elArray.length; i++) {
        if (i % n == 0) {
            nuevoArray[nuevoArray.length] = elArray[i];
        }
    }
    nuevoArray[nuevoArray.length - 1] = elArray[elArray.length - 1];
    Trazados_2.push(nuevoArray);
}

function calcularPuntoDeControl_2(ry, a, b) {
    var pc = {}
    pc.x = (ry[a].x + ry[b].x) / 2;
    pc.y = (ry[a].y + ry[b].y) / 2;
    return pc;
}

function alisarTrazado_2(ry) {
    if (ry.length > 1) {
        var ultimoPunto = ry.length - 1;
        ctx_2.beginPath();
        ctx_2.moveTo(ry[0].x, ry[0].y);
        for (i = 1; i < ry.length - 2; i++) {
            var pc = calcularPuntoDeControl_2(ry, i, i + 1);
            ctx_2.quadraticCurveTo(ry[i].x, ry[i].y, pc.x, pc.y);
        }
        ctx_2.quadraticCurveTo(ry[ultimoPunto - 1].x, ry[ultimoPunto - 1].y, ry[ultimoPunto].x, ry[ultimoPunto].y);
        ctx_2.stroke();
    }
}

function redibujarTrazados_2(){
    dibujar_2 = false;
    ctx_2.clearRect(0, 0, cw_2, ch_2);
    reducirArray_2(factorDeAlisamiento_2,puntos_2);
    for(var i = 0; i < Trazados_2.length; i++)
    alisarTrazado_2(Trazados_2[i]);
}

function oMousePos_2(canvas1, evt) {
    var ClientRect = canvas1.getBoundingClientRect();
    return { //objeto
      x: Math.round(evt.clientX - ClientRect.left),
      y: Math.round(evt.clientY - ClientRect.top)
    }
}

//Guardar firma como imagen
$("#btnFirma").on('click', function(){
    guardarImagen_2(canvas1);
    // $('#firmaAcepta').modal('hide');
});

function guardarImagen_2(canvas1){
    var datoscanvas1 = canvas1.toDataURL(); //  'image/jpge'
    document.getElementById("firmaImgAcepta").src = datoscanvas1;
    document.getElementById("rutaFirmaAcepta").value = datoscanvas1;
}
