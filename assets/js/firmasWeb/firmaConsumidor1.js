//Firma
var limpiar_5 = document.getElementById("limpiar5");
var canvas_5 = document.getElementById("canvas_5");
var ctx_5 = canvas_5.getContext("2d");
var cw_5 = canvas_5.width = 430,
cx_5 = cw_5 / 2;
var ch_5 = canvas_5.height = 200,
cy_5 = ch_5 / 2;

var dibujar_5 = false;
var factorDeAlisamiento_5 = 5;
var Trazados_5 = [];
var puntos_5 = [];
ctx_5.lineJoin = "round";

limpiar_5.addEventListener('click', function(evt) {
    dibujar_5 = false;
    ctx_5.clearRect(0, 0, cw_5, ch_5);
    Trazados_5.length = 0;
    puntos_5.length = 0;
}, false);

canvas_5.addEventListener('mousedown', function(evt) {
    dibujar_5 = true;
    //ctx_5.clearRect(0, 0, cw_5, ch_5);
    puntos_5.length = 0;
    ctx_5.beginPath();
}, false);

canvas_5.addEventListener('mouseup', function(evt) {
    redibujarTrazados_5();
}, false);

canvas_5.addEventListener("mouseout", function(evt) {
    redibujarTrazados_5();
}, false);

canvas_5.addEventListener("mousemove", function(evt) {
    if (dibujar_5) {
        var m = oMousePos_5(canvas_5, evt);
        puntos_5.push(m);
        ctx_5.lineTo(m.x, m.y);
        ctx_5.stroke();
    }
}, false);

function reducirArray_5(n,elArray) {
    var nuevoArray = [];
    nuevoArray[0] = elArray[0];
    for (var i = 0; i < elArray.length; i++) {
        if (i % n == 0) {
            nuevoArray[nuevoArray.length] = elArray[i];
        }
    }
    nuevoArray[nuevoArray.length - 1] = elArray[elArray.length - 1];
    Trazados_5.push(nuevoArray);
}

function calcularPuntoDeControl_5(ry, a, b) {
    var pc = {}
    pc.x = (ry[a].x + ry[b].x) / 2;
    pc.y = (ry[a].y + ry[b].y) / 2;
    return pc;
}

function alisarTrazado_5(ry) {
    if (ry.length > 1) {
        var ultimoPunto = ry.length - 1;
        ctx_5.beginPath();
        ctx_5.moveTo(ry[0].x, ry[0].y);
        for (i = 1; i < ry.length - 2; i++) {
            var pc = calcularPuntoDeControl_5(ry, i, i + 1);
            ctx_5.quadraticCurveTo(ry[i].x, ry[i].y, pc.x, pc.y);
        }
        ctx_5.quadraticCurveTo(ry[ultimoPunto - 1].x, ry[ultimoPunto - 1].y, ry[ultimoPunto].x, ry[ultimoPunto].y);
        ctx_5.stroke();
    }
}

function redibujarTrazados_5(){
    dibujar_5 = false;
    ctx_5.clearRect(0, 0, cw_5, ch_5);
    reducirArray_5(factorDeAlisamiento_5,puntos_5);
    for(var i = 0; i < Trazados_5.length; i++)
    alisarTrazado_5(Trazados_5[i]);
}

function oMousePos_5(canvas_5, evt) {
    var ClientRect = canvas_5.getBoundingClientRect();
    return { //objeto
      x: Math.round(evt.clientX - ClientRect.left),
      y: Math.round(evt.clientY - ClientRect.top)
    }
}

//Guardar firma como imagen
$("#btnFirma_5").on('click', function(){
    guardarImagen_5(canvas_5);
    // $('#firmaConsumidor').modal('hide');
});

function guardarImagen_5(canvas_5){
    var datoscanvas_5 = canvas_5.toDataURL(); //  'image/jpge'
    document.getElementById("firmaFirmaConsumidor").src = datoscanvas_5;
    document.getElementById("rutaFirmaConsumidor").value = datoscanvas_5;
}
