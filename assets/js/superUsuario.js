// Bloqueamos la multipunto
if (($("#formularioHoja").val() == "MultipuntoHoja") && ($("#estatusSlider").val() == "3")) {
    // console.log("aqui");
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);

    $(".cuadroFirma").attr("disabled", true);
    $(".cuadroFirma").attr("data-toggle", false);
    $(".camposContrato").attr("disabled", true);

    $("#superUsuarioName").attr("disabled", false);
    $("#superUsuarioPass").attr("disabled", false);

    //Si es una edicion Del jefe de taller y no ha firmado
    if ($("#JefeTaller_firma_2").val() == "0") {
        //Bloqueamos las firmas
        $("#JefeTaller_firma").attr("disabled", false);
        $("#JefeTaller_firma").attr("data-toggle", "modal");
        $("#JefeTallerNombre").attr("disabled", false);
        $("#guardarInspeccion").attr("disabled", false);
    }

    //Si no ha firma el cliente
    if ($("#Cliente_firma_2").val() == "0") {
        //Bloqueamos las firmas
        $("#nombreConsumidor").attr("disabled", false);
        $("#nombreConsumidor").attr("data-toggle", "modal");
        $("#Cliente_firma").attr("disabled", false);
        $("#guardarInspeccion").attr("disabled", false);
    }

    // $("#superUsuarioEnvio").attr("data-dismiss", "modal");
//Bloqueamos para orden de servicio
} else if (($("#formularioHoja").val() == "OrdenServicio") && ($("#cargaFormulario").val() == "2")) {
    $("input").attr("disabled", true);
    $("textarea").attr("disabled", true);

    $(".cuadroFirma").attr("disabled", true);
    $(".cuadroFirma").attr("data-toggle", false);
    $(".camposContrato").attr("disabled", true);

    $("#superUsuarioName").attr("disabled", false);
    $("#superUsuarioPass").attr("disabled", false);
    $("input").attr("disabled", true);

    $("#permisoProv").attr("disabled", false);
    $("#permisoProv").attr("data-toggle", "modal");
    $("#idCitaVideo").attr("disabled", false);
    $("#comentarioVideo").attr("disabled", false);
    $("#uploadfilesVideo").attr("disabled", false);
    $("input[name='tipoEnvioEvidencia']").attr("disabled", false); 
    console.log("entro OS");
}

//Para otra edicion
if ((($("#formularioHoja").val() == "Garantia") || ($("#formularioHoja").val() == "Garantia2")) && ($("#modoVistaFormulario").val() == "2")) {

    $("input").attr("disabled", true);
    $("#agregarT2").css("display", 'none');
    $("#eliminarT2").css("display", 'none');
    $("#agregarT1").css("display", 'none');
    $("#eliminarT1").css("display", 'none');

    $("#aceptarCotizacion").css("display", 'none');

    $("#superUsuarioName").attr("disabled", false);
    $("#superUsuarioPass").attr("disabled", false);
}

$("#superUsuarioEnvio").on('click',function(){
    console.log("Ento click");
    //Recuperamos los valores de usuario y login
    var usuario = $("#superUsuarioName").val();
    var pass = $("#superUsuarioPass").val();

    console.log(usuario);
    console.log(pass);
    //Comprobamos las credenciales
    if ((usuario == "ADMIN") && (pass == "ADMIN123")) {
        console.log("entro VALIDACION");
        //Verificamos que el formulario no este finalizado para poder editar
        //if ($("#completoFormulario").val() == "0") {
            console.log("Aqui");
            $("input").attr("disabled", false);
            $("textarea").attr("disabled", false);

            $(".cuadroFirma").attr("disabled", false);
            $(".cuadroFirma").attr("data-toggle", "modal");
            $(".camposContrato").attr("disabled", true);

            $("#superUsuarioEnvio").attr("data-dismiss", "modal");
            $("#superUsuarioEnvio").css("display", "none");
            $("#superUsuarioName").attr("disabled", true);
            $("#superUsuarioPass").attr("disabled", true);

            $("#guardarInspeccion").css("display","block");
            $("#guardarInspeccion").removeAttr("disabled");

        //}

        //Si existe el elemento, (garantias - vista)
        if ($("#eliminarT1").length > 0) {
            console.log("Aca");
            $("input").attr("disabled", false);

            $("#agregarT2").css("display", 'inline-table');
            $("#eliminarT2").css("display", 'inline-table');
            $("#agregarT1").css("display", 'inline-table');
            $("#eliminarT1").css("display", 'inline-table');
            $("#aceptarCotizacion").css("display", 'inline-table');
            $("#regresarBtn").css("display", 'none');

            $("#superUsuarioEnvio").attr("data-dismiss", "modal");
            $("#superUsuarioEnvio").css("display", "none");
            $("#superUsuarioName").attr("disabled", true);
            $("#superUsuarioPass").attr("disabled", true);

            $("#guardarInspeccion").css("display","block");
            $("#guardarInspeccion").removeAttr("disabled");
        }


    } else {
        $("#superUsuarioError").text("Credenciales incorrectas");

    }

});
