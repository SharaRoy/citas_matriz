$(document).ready(function() {
    $("#busquedaClaveBtn").attr("disabled",false);
    $("#claveBuscar").attr("disabled",false);

    $("#busquedaDescripBtn").attr("disabled",true);
    $("#descipBuscar").attr("disabled",true);

    $("#claveRefaccion").attr("disabled",true);
    $("#descripcionRefacciones").attr("disabled",true);
    $("#precioRefacciones").attr("disabled",true);
    $("#existenciaRefacciones").attr("disabled",true);
    $("#tipoBuscar").attr("disabled",true);
});

//Agregamos el indice del renglon a editar
$(".refaccionesBox").on('click',function(){
    var renglon = $(this).data("id");
        // console.log("renglon->"+renglon);
    $("#indiceOperacion").val(renglon);

    //Enviamos el valor de la descripcion al cuadro de busqueda
    var descripcion = $("#descripcion_"+renglon).val();
    $("#descipBuscar").val(descripcion);
});

//Verificamos el tipo de filtro que se utilizara para
$("input[name='filtros']").on('change',function(){
    var filtro = $("input[name='filtros']:checked").val();

    if (filtro == "descripcion") {
        $("#busquedaClaveBtn").attr("disabled",true);
        $("#claveBuscar").attr("disabled",true);
        $("#claveBuscar").val("");

        $("#busquedaDescripBtn").attr("disabled",false);
        $("#descipBuscar").attr("disabled",false);
    }else {
        $("#busquedaClaveBtn").attr("disabled",false);
        $("#claveBuscar").attr("disabled",false);

        $("#busquedaDescripBtn").attr("disabled",true);
        $("#descipBuscar").attr("disabled",true);
        $("#descipBuscar").val("");
    }
});

//Buscar por clave
$("#busquedaClaveBtn").on('click',function(){
    var valor = $("#claveBuscar").val();
    var base = $("#basePeticion").val();
    if (valor != "") {
        $(".cargaIcono").css("display","inline-block");
        $("#alertaConexion").text("Conectando con intelisi...");

        //Si se hizo bien la conexión, mostramos el simbolo de carga
        $(".cargaIcono").css("display","inline-block");
        //Aparentamos la carga de la tabla
        $("#tabla_refacciones").css("background-color","#ccc");
        $("#tabla_refacciones").css("color","#6f5e5e");

        $.ajax({
                url: base+"formularios/otrosFormularios/Refacciones/filtrosDescripcion",
                method: 'post',
                data: {
                    valor_1: valor,
                    dato_1: "clave",
                },
                success:function(resp){
                    //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        //Dividimos los registros
                        var filas = resp.split("|");
                        //Vaciamos el select donde se almacenan los resultados
                        var tabla = $("#tabla_refacciones");
                        tabla.empty();

                        //Verificamos que se hayan encontrado resultados
                        if (filas.length > 0) {
                            var regitro = ( filas.length > 1) ? filas.length -1 : filas.length;
                            for (var i = 0; i < regitro; i++) {
                                var campos = filas[i].split("_");
                                // console.log(campos);
                                // if (campos[5] == "MO") {
                                //     var tipo = "Mano de Obra";
                                // }else {
                                //     var tipo = "Refacción";
                                // }

                                if (campos[2] == "Servicio") {
                                    var tipo = "Mano de Obra";
                                }else if (campos[2] == "Normal") {
                                    var tipo = "Refacción";
                                }else {
                                    var tipo = campos[2];
                                }

                                tabla.append("<tr>"+
                                    "<td>"+(i+1)+"</td>"+
                                    "<td>"+tipo+"</td>"+
                                    "<td>"+campos[0]+"</td>"+
                                    "<td>"+campos[1]+"</td>"+
                                    "<td>"+campos[3]+"</td>"+
                                    "<td>"+campos[4]+"</td>"+
                                "</tr>");
                            }
                        }else {
                            tabla.append("<tr><td colspan='7'> Sin Coincidencias...</td></tr>");
                        }
                    }else {
                        console.log("Error");
                    }
                    $("#alertaConexion").text("");
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    $("#tabla_refacciones").css("background-color","white");
                    $("#tabla_refacciones").css("color","black");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $("#alertaConexion").text("");
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    $("#tabla_refacciones").css("background-color","white");
                    $("#tabla_refacciones").css("color","black");
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

//Buscar por descripción
$("#busquedaDescripBtn").on('click',function(){
    var valor = $("#descipBuscar").val();
    var base = $("#basePeticion").val();

    if (valor != "") {
        $(".cargaIcono").css("display","inline-block");
        $("#alertaConexion").text("Conectando con intelisi...");

        //Si se hizo bien la conexión, mostramos el simbolo de carga
        $(".cargaIcono").css("display","inline-block");
        //Aparentamos la carga de la tabla
        $("#tabla_refacciones").css("background-color","#ccc");
        $("#tabla_refacciones").css("color","#6f5e5e");

        $.ajax({
                url: base+"formularios/otrosFormularios/Refacciones/filtrosDescripcion",
                method: 'post',
                data: {
                    valor_1: valor,
                    dato_1: "descripcion",
                },
               success:function(resp){
                    //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        //Dividimos los registros
                        var filas = resp.split("|");
                        //Vaciamos el select donde se almacenan los resultados
                        var tabla = $("#tabla_refacciones");
                        tabla.empty();

                        //Verificamos que se hayan encontrado resultados
                        if (filas.length > 0) {
                            var regitro = ( filas.length > 1) ? filas.length -1 : filas.length;
                            for (var i = 0; i < regitro; i++) {
                                var campos = filas[i].split("_");
                                // console.log(campos);
                                // if (campos[5] == "MO") {
                                //     var tipo = "Mano de Obra";
                                // }else {
                                //     var tipo = "Refacción";
                                // }

                                if (campos[2] == "Servicio") {
                                    var tipo = "Mano de Obra";
                                }else if (campos[2] == "Normal") {
                                    var tipo = "Refacción";
                                }else {
                                    var tipo = campos[2];
                                }

                                tabla.append("<tr>"+
                                    "<td>"+(i+1)+"</td>"+
                                    "<td>"+tipo+"</td>"+
                                    "<td>"+campos[0]+"</td>"+
                                    "<td>"+campos[1]+"</td>"+
                                    "<td>"+campos[3]+"</td>"+
                                    "<td>"+campos[4]+"</td>"+
                                "</tr>");
                            }
                        }else {
                            tabla.append("<tr><td colspan='7'> Sin Coincidencias...</td></tr>");
                        }
                    }else {
                        console.log("Error");
                    }
                    $("#alertaConexion").text("");
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    $("#tabla_refacciones").css("background-color","white");
                    $("#tabla_refacciones").css("color","black");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $("#alertaConexion").text("");
                    //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                    $(".cargaIcono").css("display","none");
                    //Regresamos los colores de la tabla a la normalidad
                    $("#tabla_refacciones").css("background-color","white");
                    $("#tabla_refacciones").css("color","black");
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
});

