//Anteriormente en el footer (principal|)
if ($("#idOrdenFormulario").length > 0) {
    var serie = $("#idOrdenFormulario").val().trim();
    if ((serie != "")&&(serie != " ")&&(serie != "0")) {
        cargarDatosFormularioQuere(serie);
    }
}

//Verificamos que no se quede el numero orden vacio
function validarFormulario(x){
    if ($("#idOrdenFormulario").val() == "" ) {
        $("#aceptarCotizacion").attr("disabled", true);
        $("#terminarCotizacion").attr("disabled", true);
    } else {
        $("#aceptarCotizacion").attr("disabled", false);
        $("#terminarCotizacion").attr("disabled", false);
        //Hacemos la peticion nuevamente para recuperar los datos
        cargarDatosFormularioQuere($("#idOrdenFormulario").val());
    }
}

function cargarDatosFormularioQuere(idOrden) {
    console.log("Formulario entrada");
    $.ajax({
        url: "https://planificadorempresarial.mx/cs/citas_ford/horariossa/citas/citas_queretaro/"+"formularios/Queretaro/Formularios_1/loadData",
        method: 'post',
        data: {
            idOrden: idOrden,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler			</p>")<1) {

                var campos = resp.split("|");
                console.log(campos);
                if (campos.length > 5) {
                    $("#txtFecha").val(campos[0]);
                    $("#txtHoraCita").val(campos[1]);
                    $("#txtCliente").val(campos[2]);
                    $("#txtApe1").val(campos[3]);
                    $("#txtApe2").val(campos[4]);
                    $("#txtTelefono").val(campos[5]);
                    $("#txtCorreo").val(campos[6]);
                    $("#txtPlacas").val(campos[7]);
                    $("#txtAnio").val(campos[8]);
                    $("#txtUen").val(campos[9]);
                    // $("#uen").val(campos[10]);
                    $("#txtAsesor").val(campos[11]);
                    $("#txtServicio").val(campos[12]);
                    $("#txtTipoServicio").val(campos[13]);

                }
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}
