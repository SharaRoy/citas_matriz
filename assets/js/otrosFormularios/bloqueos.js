if ($("#modoVistaFormulario").val() == "2") {
   $("input").attr("disabled", true);

   $(".cuadroFirma").attr("disabled", true);
   $(".cuadroFirma").attr("data-toggle", false);
   $(".camposContrato").attr("disabled", true);

   $("#superUsuarioName").attr("disabled", false);
   $("#superUsuarioPass").attr("disabled", false);
}
