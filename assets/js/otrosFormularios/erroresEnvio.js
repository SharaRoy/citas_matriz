$(".renvioOrden").on('click',function(){
	var idOrden = $(this).data("orden");
	var base = $("#basePeticion").val();

    $.ajax({
        url: base+"otros_Modulos/ErroresEnvios/reenvioDeOrden",
        method: 'post',
        data: {
            idOrden: idOrden,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler			</p>")<1) {
            	window.location.reload();
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
});

$('#busqueda_tabla').keyup(function () {
    toSearch();
});


//Funcion para buscar por campos
function toSearch() {
    var general = new RegExp($('#busqueda_tabla').val(), 'i');
    // var rex = new RegExp(valor, 'i');
    $('.campos_buscar tr').hide();
    $('.campos_buscar tr').filter(function () {
        var respuesta = false;
        if (general.test($(this).text())) {
            respuesta = true;
        }
        return respuesta;
    }).show();
}

//Busqueda por fechas
//Condicionamos las busquedas por fecha 
$('#fecha_inicio').change(function() {
    //Recuperamos el valor obtenido
    var fecha_inicio = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_fin").attr('min',fecha_inicio);
    //console.log(fecha_inicio)
});

$('#fecha_fin').change(function() {
    //Recuperamos el valor obtenido
    var fecha_fin = $(this).val();

    //Limitamos que la fecha fin no sea mayor a la fecha inicio
    $("#fecha_inicio").attr('max',fecha_fin);
    //console.log(fecha_fin)
});


$('#btnBusqueda_fechas').on('click',function(){
    console.log("Buscando por fechas");
    //Si se hizo bien la conexión, mostramos el simbolo de carga
    $(".cargaIcono").css("display","inline-block");
    //Aparentamos la carga de la tabla
    $(".campos_buscar").css("background-color","#ddd");
    $(".campos_buscar").css("color","#6f5e5e");

    //Recuperamos las fechas a consultar
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_fin = $("#fecha_fin").val();
    var campo = $("#busqueda_campo").val();

    //Identificamos la lista que se consulta
    //var lista = $("#origenRegistro").val();
    
    var base = $("#basePeticion").val().trim();

    $.ajax({
        url: base+"otros_Modulos/ErroresEnvios/filtro_ordenes",
        method: 'post',
        data: {
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            campo : campo,
        },
        success:function(resp){
            //console.log(resp);
            if (resp.indexOf("handler         </p>")<1) {
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                //Verificamos que exista un resultado
                if (resp.length > 8) {
                    //Fraccionamos la respuesta en renglones
                    var campos = resp.split("|");
                    console.log(campos);

                    //Recorremos los "renglones" obtenidos para partir los campos
                    var regitro = ( campos.length > 1) ? (campos.length -1) : campos.length;
                    for (var i = 0; i < regitro; i++) {
                        var celdas = campos[i].split("="); 
                        
                        //Creamos las celdas en la tabla
                        tabla.append("<tr style='font-size:11px;'>"+
                            //Indice
                            "<td align='center' style='vertical-align: middle;'>"+(i+1)+"</td>"+
                            //NO. ORDEN
                            "<td align='center' style='vertical-align: middle;'>"+celdas[0]+"</td>"+
                            //ASESOR
                            "<td align='center' style='vertical-align: middle;'>"+celdas[2]+"</td>"+
                            //CLIENTE
                            "<td align='center' style='vertical-align: middle;'>"+celdas[3]+"</td>"+
                            //FECHA RECEPCIÓN
                            "<td align='center' style='vertical-align: middle;'>"+celdas[4]+"</td>"+
                            //FECHA PROMESA
                            "<td align='center' style='vertical-align: middle;'>"+celdas[5]+"</td>"+
                            //ENSAJE DE ERROR
                            "<td align='center' style='vertical-align: middle;width:25%;'><label class='error'>"+celdas[1]+"</label></td>"+
                            //REENVIO
                            "<td align='center' style='vertical-align: middle;'>"+
                                "<a class='btn btn-warning' onclick='reenvio_Orden("+celdas[0]+");' data-orden='"+celdas[0]+"' style='color:white;'>"+
                                    "Reenviar Orden"+
                                "</a>"+
                            "</td>"+
                        "</tr>");
                    }
                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='8'>No se encontraron resultados</td>"+
                    "</tr>");
                }
            }else{
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='8'>No se encontraron resultados</td>"+
                "</tr>");
            }

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre de success
        },
        error:function(error){
            console.log(error);

            //Si ya se termino de cargar la tabla, ocultamos el icono de carga
            $(".cargaIcono").css("display","none");
            //Regresamos los colores de la tabla a la normalidad
            $(".campos_buscar").css("background-color","white");
            $(".campos_buscar").css("color","black");
        //Cierre del error
        }
    //Cierre del ajax
    });
});

function reenvio_Orden(idOrden = "") {
    var base = $("#basePeticion").val();

    console.log("envio pos busqueda--->"+idOrden);

    $.ajax({
        url: base+"otros_Modulos/ErroresEnvios/reenvioDeOrden",
        method: 'post',
        data: {
            idOrden: idOrden,
        },
        success:function(resp){
            console.log(resp);
            if (resp.indexOf("handler           </p>")<1) {
                window.location.reload();
            }
        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
}