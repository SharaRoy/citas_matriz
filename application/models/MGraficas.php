<?php

class MGraficas extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        //Inicializamos la clase para la base de datos
        // $DB2 = $this->load->database('gerencia', TRUE);
    }


    //Busqueda de registros
    public function get_result($campo = "",$value = "",$tabla = ""){
        $DB2 = $this->load->database('gerencia', TRUE);
        $result = $DB2->where($campo,$value)->get($tabla)->result();
        return $result;
    }

    //Busqueda de registros con distentas citas
    public function get_result_distict($campo = "",$value = "",$dist_campo = "",$tabla = ""){
        $DB2 = $this->load->database('gerencia', TRUE);
        $result = $DB2->where($campo,$value)->group_by($dist_campo)->get($tabla)->result();
        return $result;
    }

    //Busqueda de registros con dos parametros
    public function get_result_two_field($campo = "",$value = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        $DB2 = $this->load->database('gerencia', TRUE);
        $result = $DB2->where($campo,$value)->where($campo_2,$value_2)->get($tabla)->result();
        return $result;
    }

    public function save_register($table = "", $data = ""){
        $DB2 = $this->load->database('gerencia', TRUE);
        $result = $DB2->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $DB2->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }


    public function get_table($table = ""){
        $DB2 = $this->load->database('gerencia', TRUE);
        $result = $DB2->get($table)->result();
        return $result;
    }


}
