<?php

class Verificacion extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        //Inicializamos la clase para la base de datos
        // $DB2 = $this->load->database('private', TRUE);
    }

    public function login($usuario = "")
    {
        $DB2 = $this->load->database('private', TRUE);
        $sql = "SELECT adminId,adminPassword,adminNombre FROM admin WHERE adminUsername = ? ";
        $query = $DB2->query($sql,array($usuario));
        return $query;
    }

    // public function login($usuario = "")
    // {
    //     $DB2 = $this->load->database('private', TRUE);
    //     $sql = "SELECT adminId,adminPassword FROM admin WHERE adminUsername = ? AND adminStatus = ?";
    //     $query = $DB2->query($sql,array($usuario,1));
    //     // $data = $query->result();
    //     return $query;
    // }

    //Probamos la conexion
    public function allUsers()
    {
        $DB2 = $this->load->database('private', TRUE);
        $sql = "SELECT * FROM admin ";
        $query = $DB2->query($sql);
        $data = $query->result();
        return $data;
    }

    public function dataAsesor($id = 0)
    {
        $DB2 = $this->load->database('private', TRUE);

        $sql = "SELECT * FROM admin WHERE  adminId = ?";
        $query = $DB2->query($sql,array($id));
        return $query;
    }

    public function get_result($campo,$value,$tabla){
        $DB2 = $this->load->database('private', TRUE);
        $result = $DB2->where($campo,$value)->get($tabla)->result();
        return $result;
    }

    public function save_register($table, $data){
        $DB2 = $this->load->database('private', TRUE);
        $result = $DB2->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $DB2->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    //Recuperamos el color
    public function getColor($id=''){
        $DB2 = $this->load->database('private', TRUE);
        $q = $DB2->where('id',$id)->select("color")->get('colores');
        if($q->num_rows()==1){
            $retorno = $q->row()->color;
        }else{
            $retorno = '';
        }

        return $retorno;
    }

    public function recuperar_usuario($id=''){
        $DB2 = $this->load->database('private', TRUE);
        $q = $DB2->where('adminId',$id)->select("adminUsername")->limit(1)->get('admin');
        if($q->num_rows()==1){
            $retorno = $q->row()->adminUsername;
        }else{
            $retorno = '';
        }

        return $retorno;
    }

    public function getLastId()
    {
        $DB2 = $this->load->database('private', TRUE);
        $query = $DB2->query("SELECT proyectoId FROM bodyshop ORDER BY proyectoId DESC LIMIT 1");

        $row = $query->row();
        if (isset($row)){
          $retorno = $query->row(0)->proyectoId;
        }else {
          $retorno = 1;
        }

        return $retorno;
    }

    public function update_table_row($table,$data,$id_table,$id){
        $DB2 = $this->load->database('private', TRUE);
        $result = $DB2->update($table, $data, array($id_table=>$id));
        return $result;
    }

    public function get_table($table){
        $DB2 = $this->load->database('private', TRUE);
        $result = $DB2->get($table)->result();
        return $result;
    }



}
