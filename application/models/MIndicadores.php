<?php

class MIndicadores extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        //Inicializamos la clase para la base de datos
        $this->load->database('default', TRUE);
    }

    //Buscamos en los presupuestos adicionales
    //Buscar refacciones solo por tipo de orden (obtener mano de obra)
    public function total_tipoOrden($tipo_orden = '',$tipo_operacion = '')
    {
        $sql = "SELECT prerM.activo,prerM.horas_mo,prerM.costo_mo,prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prer.fecha_alta,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias , prerM.costo_mo_garantias ".
            "FROM presupuesto_registro AS prer ".            
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            //"LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE o.id_tipo_orden = ? AND c.id_status_color = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($tipo_orden,$tipo_operacion,'5','6'));
        $data = $query->result();
        return $data;
    }

    //Buscar refacciones solo por tecnico (obtener mano de obra)
    public function total_tecnico($tipo_orden = '',$tipo_operacion = '',$id_tecnico = '')
    {
        $sql = "SELECT prerM.activo,prerM.horas_mo,prerM.costo_mo,prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prer.fecha_alta,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias , prerM.costo_mo_garantias ".
            "FROM presupuesto_registro AS prer ".            
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            //"LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE o.id_tipo_orden = ? AND c.id_status_color = ? AND c.id_tecnico = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($tipo_orden,$tipo_operacion,$id_tecnico,'5','6'));
        $data = $query->result();
        return $data;
    }

    //Buscar refacciones por tipo de orden y fechas (obtener mano de obra)
    public function total_tipoOrden_Fecha($tipo_orden = '',$tipo_operacion = '',$fecha = "")
    {
        $sql = "SELECT prerM.activo,prerM.horas_mo,prerM.costo_mo,prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prer.fecha_alta,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias , prerM.costo_mo_garantias ".
            "FROM presupuesto_registro AS prer ".            
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            //"LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE o.id_tipo_orden = ? AND c.id_status_color = ? AND o.fecha_recepcion = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($tipo_orden,$tipo_operacion,$fecha,'5','6'));
        $data = $query->result();
        return $data;
    }

    //Buscar refacciones por tipo de orden y fechas (obtener mano de obra)
    public function total_tipoOrden_Fecha2($tipo_orden = '',$tipo_operacion = '',$fecha_inicio = "",$fecha_fin = "")
    {
        $sql = "SELECT prerM.activo,prerM.horas_mo,prerM.costo_mo,prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prer.fecha_alta,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias , prerM.costo_mo_garantias ".
            "FROM presupuesto_registro AS prer ".            
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            //"LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE o.id_tipo_orden = ? AND c.id_status_color = ? AND o.fecha_recepcion BETWEEN ? AND ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($tipo_orden,$tipo_operacion,$fecha_inicio,$fecha_fin,'5','6'));
        $data = $query->result();
        return $data;
    }

    //Buscar refacciones por tipo de orden, fechas y tecnico (obtener mano de obra)
    public function total_tipoOrden_FechaTecnico($tipo_orden = '',$tipo_operacion = '',$id_tecnico = '',$fecha = "")
    {
        $sql = "SELECT prerM.activo,prerM.horas_mo,prerM.costo_mo,prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prer.fecha_alta,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias , prerM.costo_mo_garantias ".
            "FROM presupuesto_registro AS prer ".            
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            //"LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE o.id_tipo_orden = ? AND c.id_status_color = ? AND c.id_tecnico = ? AND o.fecha_recepcion = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($tipo_orden,$tipo_operacion,$id_tecnico,$fecha,'5','6'));
        $data = $query->result();
        return $data;
    }

    //Buscar refacciones por tipo de orden, fechas y tecnico (obtener mano de obra)
    public function total_tipoOrden_FechaTecnico2($tipo_orden = '',$tipo_operacion = '',$id_tecnico = '',$fecha_inicio = "",$fecha_fin = "")
    {
        $sql = "SELECT prerM.activo,prerM.horas_mo,prerM.costo_mo,prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prer.fecha_alta,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias , prerM.costo_mo_garantias ".
            "FROM presupuesto_registro AS prer ".            
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            //"LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE o.id_tipo_orden = ? AND c.id_status_color = ? AND c.id_tecnico = ?  AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) AND o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($tipo_orden,$tipo_operacion,$id_tecnico,'5','6',$fecha_inicio,$fecha_fin));
        $data = $query->result();
        return $data;
    }

    //Indicadores de garantia
    public function garantia_indicador()
    {
        $sql = "SELECT o.id_cita,o.fecha_recepcion, c.id_tecnico, g.estatus, t.nombre AS tecnico  ".
            "FROM garantias AS g ".            
            "INNER JOIN ordenservicio AS o ON o.id_cita = o.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            //"LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            //"WHERE g.estatus = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            //"ORDER BY o.fecha_recepcion DESC";
            "ORDER BY o.fecha_recepcion DESC";

        //$query = $this->db->query($sql,array('5','6'));
        $query = $this->db->query($sql,array());
        $data = $query->result();
        return $data;
    }

    //Indicador de garantias por estatus
    public function garantia_estatus($estatus = '')
    {
        $sql = "SELECT o.id_cita,o.fecha_recepcion, c.id_tecnico, g.estatus, t.nombre AS tecnico  ".
            "FROM garantias AS g ".            
            "LEFT JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            //"WHERE g.estatus = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            //"ORDER BY o.fecha_recepcion DESC";
            "WHERE g.estatus = ? ".
            "ORDER BY o.fecha_recepcion DESC";

        //$query = $this->db->query($sql,array($estatus,'5','6'));
        $query = $this->db->query($sql,array($estatus));
        $data = $query->result();
        return $data;
    }

    //Indicador de garantias por estatus y por fecha
    public function garantia_Fecha($estatus = '',$fecha = '')
    {
        $sql = "SELECT o.id_cita,o.fecha_recepcion, c.id_tecnico, g.estatus, t.nombre AS tecnico  ".
            "FROM garantias AS g ".            
            "LEFT JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            //"WHERE g.estatus = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            //"ORDER BY o.fecha_recepcion DESC";
            "WHERE g.estatus = ? AND o.fecha_recepcion = ? ".
            "ORDER BY o.fecha_recepcion DESC";

        //$query = $this->db->query($sql,array($estatus,'5','6'));
        $query = $this->db->query($sql,array($estatus,$fecha));
        $data = $query->result();
        return $data;
    }

    //Indicador de garantias por estatus y entre fechas
    public function garantia_Fecha2($estatus = '',$fecha = '',$fecha_2 = '')
    {
        $sql = "SELECT o.id_cita,o.fecha_recepcion, c.id_tecnico, g.estatus, t.nombre AS tecnico  ".
            "FROM garantias AS g ".            
            "LEFT JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            //"WHERE g.estatus = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            //"ORDER BY o.fecha_recepcion DESC";
            "WHERE g.estatus = ? AND o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY o.fecha_recepcion DESC";

        //$query = $this->db->query($sql,array($estatus,'5','6'));
        $query = $this->db->query($sql,array($estatus,$fecha,$fecha_2));
        $data = $query->result();
        return $data;
    }

    //Indicador de garantias por estatus y tecnico
    public function garantia_tecnico($estatus = '',$tecnico = '')
    {
        $sql = "SELECT o.id_cita,o.fecha_recepcion, c.id_tecnico, g.estatus, t.nombre AS tecnico  ".
            "FROM garantias AS g ".            
            "LEFT JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            //"WHERE g.estatus = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            //"ORDER BY o.fecha_recepcion DESC";
            "WHERE g.estatus = ? AND c.id_tecnico = ? ".
            "ORDER BY o.fecha_recepcion DESC";

        //$query = $this->db->query($sql,array($estatus,'5','6'));
        $query = $this->db->query($sql,array($estatus,$tecnico));
        $data = $query->result();
        return $data;
    }

    //Indicador de garantias por estatus y tecnico y fecha
    public function garantia_FechaTecnico($estatus = '',$tecnico = '',$fecha = '')
    {
        $sql = "SELECT o.id_cita,o.fecha_recepcion, c.id_tecnico, g.estatus, t.nombre AS tecnico  ".
            "FROM garantias AS g ".            
            "LEFT JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            //"WHERE g.estatus = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            //"ORDER BY o.fecha_recepcion DESC";
            "WHERE g.estatus = ? AND c.id_tecnico = ? AND o.fecha_recepcion = ? ".
            "ORDER BY o.fecha_recepcion DESC";

        //$query = $this->db->query($sql,array($estatus,'5','6'));
        $query = $this->db->query($sql,array($estatus,$tecnico,$fecha));
        $data = $query->result();
        return $data;
    }

    //Indicador de garantias por estatus y tecnico y fecha
    public function garantia_FechaTecnico2($estatus = '',$tecnico = '',$fecha = '',$fecha_2 = '')
    {
        $sql = "SELECT o.id_cita,o.fecha_recepcion, c.id_tecnico, g.estatus, t.nombre AS tecnico  ".
            "FROM garantias AS g ".            
            "LEFT JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            //"WHERE g.estatus = ? AND (o.id_situacion_intelisis = ? OR o.id_situacion_intelisis = ?) ".
            //"ORDER BY o.fecha_recepcion DESC";
            "WHERE g.estatus = ? AND c.id_tecnico = ? AND o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY o.fecha_recepcion DESC";

        //$query = $this->db->query($sql,array($estatus,'5','6'));
        $query = $this->db->query($sql,array($estatus,$tecnico,$fecha,$fecha_2));
        $data = $query->result();
        return $data;
    }
    

    //garantias (montos en pesos)
    public function monto_garantia_estatus($estatus = '')
    {
        $sql = "SELECT prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias, ". 
            "prerM.costo_mo_garantias,prerM.costo_ford,prerM.cantidad,t.nombre AS tecnico, g.estatus, prerM.autoriza_garantia,prerM.activo ".
            "FROM garantias AS g ".             
            "INNER JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "LEFT JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE g.estatus = ? ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($estatus));
        $data = $query->result();
        return $data;
    }

    //garantias (montos en pesos)
    public function monto_garantia_Fecha($estatus = '',$fecha = '')
    {
        $sql = "SELECT prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias, ". 
            "prerM.costo_mo_garantias,prerM.costo_ford,prerM.cantidad,t.nombre AS tecnico, g.estatus, prerM.autoriza_garantia,prerM.activo ".
            "FROM garantias AS g ".             
            "INNER JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "LEFT JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE g.estatus = ? AND o.fecha_recepcion = ? ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($estatus,$fecha));
        $data = $query->result();
        return $data;
    }

    //garantias (montos en pesos)
    public function monto_garantia_Fecha2($estatus = '',$fecha = '',$fecha_2 = '')
    {
        $sql = "SELECT prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias, ". 
            "prerM.costo_mo_garantias,prerM.costo_ford,prerM.cantidad,t.nombre AS tecnico, g.estatus, prerM.autoriza_garantia,prerM.activo ".
            "FROM garantias AS g ".             
            "INNER JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "LEFT JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE g.estatus = ? AND o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($estatus,$fecha,$fecha_2));
        $data = $query->result();
        return $data;
    }

    public function monto_garantia_tecnico($estatus = '',$tecnico = '')
    {
        $sql = "SELECT prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias, ". 
            "prerM.costo_mo_garantias,prerM.costo_ford,prerM.cantidad,t.nombre AS tecnico, g.estatus, prerM.autoriza_garantia,prerM.activo ".
            "FROM garantias AS g ".             
            "INNER JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "LEFT JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE g.estatus = ? AND c.id_tecnico = ? ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($estatus,$tecnico));
        $data = $query->result();
        return $data;
    }

    public function monto_garantia_FechaTecnico($estatus = '',$tecnico = '',$fecha = '')
    {
        $sql = "SELECT prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias, ". 
            "prerM.costo_mo_garantias,prerM.costo_ford,prerM.cantidad,t.nombre AS tecnico, g.estatus, prerM.autoriza_garantia,prerM.activo ".
            "FROM garantias AS g ".             
            "INNER JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "LEFT JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE g.estatus = ? AND c.id_tecnico = ? AND o.fecha_recepcion = ? ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($estatus,$tecnico,$fecha));
        $data = $query->result();
        return $data;
    }

    public function monto_garantia_FechaTecnico2($estatus = '',$tecnico = '',$fecha = '',$fecha_2)
    {
        $sql = "SELECT prerM.autoriza_cliente, o.id_cita,o.fecha_recepcion,prerM.autoriza_asesor,c.id_tecnico,prerM.horas_mo_garantias, ". 
            "prerM.costo_mo_garantias,prerM.costo_ford,prerM.cantidad,t.nombre AS tecnico, g.estatus, prerM.autoriza_garantia,prerM.activo ".
            "FROM garantias AS g ".             
            "INNER JOIN ordenservicio AS o ON o.id_cita = g.idOrden ".
            "LEFT JOIN citas AS c ON o.id_cita = c.id_cita ".
            //"INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "LEFT JOIN presupuesto_multipunto AS prerM ON prerM.id_cita = o.id_cita ".
            "WHERE g.estatus = ? AND c.id_tecnico = ? AND o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY o.fecha_recepcion DESC";
            //"ORDER BY prer.id DESC";

        $query = $this->db->query($sql,array($estatus,$tecnico,$fecha,$fecha_2));
        $data = $query->result();
        return $data;
    }


}

