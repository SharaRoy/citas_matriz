<?php

class MConsultas extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        //Inicializamos la clase para la base de datos
        $this->load->database('default', TRUE);
    }

    public function get_table($table){
        $result = $this->db->limit(50)->get($table)->result();
        return $result;
    }

    public function get_table_desc(){
        $query = $this->db->query("SELECT * FROM cotizacion_multipunto ORDER BY fech_actualiza DESC");
        $result = $query->result();
        return $result;
    }

    public function get_table_desc_2(){
        $query = $this->db->query("SELECT * FROM diagnostico ORDER BY fechaRegistro DESC");
        $result = $query->result();
        return $result;
    }

    public function get_result($campo,$value,$tabla){
        $result = $this->db->where($campo,$value)->limit(100)->get($tabla)->result();
        return $result;
  	}

    public function audio_evi($id_cita){
        $q = $this->db->where('idPivote',$id_cita)->select('evidencia')->limit(1)->get('voz_cliente');

        if($q->num_rows()==1){
            $retorno = $q->row()->evidencia;
        }else{
            $retorno = '';
        }

        return $result;
  	}

    public function get_items_T1($idOrden){
        $result = $this->db->where('idorden',$idOrden)
            ->$this->db->where('tipo','1')
            ->$this->db->where('tipo','2')
            ->get('articulos_orden')->result();
        return $result;
    }

    public function get_items_T2($idOrden){
        $result = $this->db->where('idorden',$idOrden)
            ->$this->db->where('tipo','1')
            ->get('articulos_orden')->result();
        return $result;
    }

    public function last_id_table()
    {
        $query = $this->db->query("SELECT idDiagnostico FROM diagnostico_sc ORDER BY idDiagnostico DESC LIMIT 1");
        return $query;
    }

    public function last_id_table_2()
    {
        $query = $this->db->query("SELECT idDiagnostico FROM diagnostico ORDER BY idDiagnostico DESC LIMIT 1");
        return $query;
    }

    public function merge_table($id)
    {
        $query = $this->db->query("INSERT INTO diagnostico SELECT * FROM diagnostico_sc WHERE idDiagnostico = ?",array($id));
        return $query;
    }

    public function save_register($table, $data){
        $result = $this->db->insert($table, $data);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        return $result;
    }

    public function joint_table_car($id)
    {
        $sql = "SELECT o.id,o.nombre_compania,c.asesor,c.vehiculo_modelo, c.vehiculo_numero_serie, o.numero_interno AS notorre, o.color,c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, c.datos_email ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "WHERE c.id_cita = ?";
        $query = $this->db->query($sql,array($id));
        $data = $query->result();
    		return $data;
    }

    public function update_table_row($table,$data,$id_table,$id){
            $result = $this->db->update($table, $data, array($id_table=>$id));
        return $result;
    }

    public function delete_row($table,$id_table,$id){
		    $result = $this->db->delete($table, array($id_table=>$id));
        return $result;
  	}

    //funciones modelo
    public function getTipoOrden($id=''){
        $q = $this->db->where('id',$id)->select('tipo_orden')->get('cat_tipo_orden');
        if($q->num_rows()==1){
            $retorno = $q->row()->tipo_orden;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function getTipoOrdenIdentificacion($id=''){
        $q = $this->db->where('id',$id)->select('identificador')->get('cat_tipo_orden');
        if($q->num_rows()==1){
            $retorno = $q->row()->identificador;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    //Obtiene comentario cita
    public function getColorCitaAuto($id=''){
        $q = $this->db->where('id_cita',$id)->select("ct.color")->join('cat_colores ct','c.id_color = ct.id')->where('c.activo',1)->get('citas c ');
        if($q->num_rows()==1){
            $retorno = $q->row()->color;
        }else{
            $retorno = '';
        }
          return $retorno;
    }

    public function getAnticipoOrden($idOrden = 0){
        $q = $this->db->where('id',$idOrden)->select('anticipo')->get('ordenservicio');
        if($q->num_rows()==1){
            $retorno = (int)$q->row()->anticipo;
        }else{
            $retorno = 0;
        }
        return $retorno;
    }

   //Obtiene si la cotización fue aceptada o no
    public function EstatusCotizacionUser($idorden=''){
       $q = $this->db->where('id',$idorden)->select('cotizacion_confirmada')->get('ordenservicio');
        if($q->num_rows()==1){
            $retorno = $q->row()->cotizacion_confirmada;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function citas($idorden='')
    {
        $query = $this->db->where('o.id',$idorden)
            ->join('citas AS c','c.id_cita=o.id_cita')
            ->join('tecnicos AS t','c.id_tecnico = t.id','left')
            ->join('cat_tipo_pago AS cp','o.id_tipo_pago=cp.id','left')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            ->select('o.*,c.*,cp.tipo_pago,o.vehiculo_kilometraje AS km,cto.identificador,t.nombre AS tecnico')
            ->get('ordenservicio AS o')
            ->result();
        return $query;
    }

    public function tecnicos($idCita='')
    {
        $query = $this->db->where('tc.id_cita',$idCita)
            ->join('tecnicos AS t','tc.id_tecnico=t.id')
            ->select('tc.*,t.nombre AS tecnico')
            ->get('tecnicos_citas AS tc')
            ->result();
        return $query;
    }

    public function apiRequest()
    {
        //cat_monedas
        $sql = "SELECT  o.*,c.* ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "ORDER BY o.id DESC LIMIT 50";
        // "WHERE c.id_cita = ?";
        // $query = $this->db->query($sql,array($id));
        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;
    }

    public function apiRequest_2()
    {
        //cat_monedas
        $sql = "SELECT  o.*,c.*,t.nombre AS tecnico, cat.categoria, subcat.subcategoria ".
        "FROM citas AS c ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "INNER JOIN ordenservicio AS oN o.id_cita = c.id_cita ".
        "LEFT JOIN categorias AS cat ON cat.id = o.idcategoria ".
        "LEFT JOIN subcategorias AS subcat ON subcat.id = o.idsubcategoria ".
        "ORDER BY o.id DESC LIMIT 50";
        // "WHERE c.id_cita = ?";
        // $query = $this->db->query($sql,array($id));
        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;
    }

    //Teléfono del asesor
    public function telefono_asesor($idorden=''){
        $q = $this->db->where('id_cita',$idorden)->select('telefono_asesor')->from('ordenservicio')->get();
        if($q->num_rows()==1){
            return $q->row()->telefono_asesor;
        }else{
            return '';
        }
    }

    //Teléfono del tecnico
    public function telefono_tecnico($idorden=''){
        $query = $this->db->where('c.id_cita',$idorden)
                  ->join('tecnicos AS t','c.id_tecnico = t.id')
                  ->select('t.tel')->from('citas AS c')->get();
        if($query->num_rows() == 1){
            return $query->row()->tel;
        }else{
            return '';
        }
    }

    public function precargaConsulta($idOrden = 0)
    {
        $sql = "SELECT  o.*,c.*,t.nombre AS tecnico, cat.categoria, subcat.subcategoria, c.id_cita AS orden_cita ".
        "FROM citas AS c ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN categorias AS cat ON cat.id = o.idcategoria ".
        "LEFT JOIN subcategorias AS subcat ON subcat.id = o.idsubcategoria ".
        "WHERE c.id_cita = ?";
        $query = $this->db->query($sql,array($idOrden));
        $data = $query->result();
        return $data;
    }

    public function articulosTitulos($idOrden = 0)
    {
        $sql = "SELECT atr.*,gps.clave AS grupoDes, opgps.articulo AS operacionDes ".
        "FROM articulos_orden AS atr ".
        "LEFT JOIN grupo_operacion AS opgps ON opgps.id = atr.grupo ".
        "LEFT JOIN grupos AS gps ON gps.id = opgps.idgrupo ".
        "WHERE atr.idorden = ? AND atr.cancelado = ?";
        $query = $this->db->query($sql,array($idOrden,'0'));
        $data = $query->result();
        return $data;
    }

    public function last_registre()
    {
        $query = $this->db->query("SELECT idRegistro FROM formulario_quere1 ORDER BY idRegistro DESC LIMIT 1");
        return $query;
    }

    public function Unidad_entregada($id_cita=''){
        $q = $this->db->where('id_cita',$id_cita)->select('unidad_entregada')->get('citas');
        if($q->num_rows()==1){
            $respuesta = $q->row()->unidad_entregada;
        }else{
            $respuesta = '0';
        }
        return $respuesta;
    }

    public function tecnicosOrdenes($idTecnico = 0)
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, ".  // coti.idCotizacion, coti.aceptoTermino,
            "c.vehiculo_placas, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus, o.folioIntelisis, prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet ".
            "FROM citas AS c ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            //"LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            //Comentarios cierre de orden
            //"LEFT JOIN comentarios_cierre_orden AS comentario ON comentario.id_cita = c.id_cita ".
            "WHERE c.id_tecnico = ? ".
            "ORDER BY o.id DESC LIMIT 60";
        $query = $this->db->query($sql,array($idTecnico));
        $data = $query->result(); 
        return $data;
    }

    public function listaOrdenes()
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, ".  // coti.idCotizacion, coti.aceptoTermino,
            "c.vehiculo_placas, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus, o.folioIntelisis, prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet ".
            "FROM citas AS c ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            //"LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            //Comentarios cierre de orden
            //"LEFT JOIN comentarios_cierre_orden AS comentario ON comentario.id_cita = c.id_cita ".
            "ORDER BY o.id DESC LIMIT 60";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array());
        $data = $query->result();
        return $data;
    }

    public function recuperar_documentaciones()
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, coti.idCotizacion, coti.aceptoTermino, c.id_tecnico, c.vehiculo_numero_serie,o.vehiculo_identificacion, vc.id AS vc_id, vc.audio, o.folioIntelisis, ".
            "c.vehiculo_placas, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus, o.folioIntelisis, prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet, d.idDiagnostico ".
            "FROM citas AS c ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            "LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Voz Cliente Nuevo Formato
            "LEFT JOIN vc_registro AS vc ON vc.id_cita = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            //Comentarios cierre de orden
            //"LEFT JOIN comentarios_cierre_orden AS comentario ON comentario.id_cita = c.id_cita ".
            "ORDER BY o.id DESC LIMIT 10000";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array());
        $data = $query->result();
        return $data;
    }

    public function recuperar_documentaciones_fecha($fecha = "")
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, coti.idCotizacion, coti.aceptoTermino, c.id_tecnico, c.vehiculo_numero_serie,o.vehiculo_identificacion, vc.id AS vc_id, vc.audio, o.folioIntelisis, ".
            "c.vehiculo_placas, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus, prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet, d.idDiagnostico ".
            "FROM citas AS c ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            "LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Voz Cliente Nuevo Formato
            "LEFT JOIN vc_registro AS vc ON vc.id_cita = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            //Comentarios cierre de orden
            //"LEFT JOIN comentarios_cierre_orden AS comentario ON comentario.id_cita = c.id_cita ".
            "WHERE o.fecha_recepcion = ? ".
            "ORDER BY o.id DESC LIMIT 100";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array($fecha));
        $data = $query->result();
        return $data;
    }

    public function recuperar_documentaciones_cita($id_cita = "")
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, c.id_tecnico, c.vehiculo_numero_serie,o.vehiculo_identificacion, vc.id AS vc_id, vc.audio, o.folioIntelisis, o.fecha_entrega, o.id_tipo_orden, ".
            "c.vehiculo_placas, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus, prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet, d.idDiagnostico, o.hora_recepcion, o.hora_entrega ".
            "FROM citas AS c ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Voz Cliente Nuevo Formato
            "LEFT JOIN vc_registro AS vc ON vc.id_cita = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            "WHERE o.id_cita = ? ".
            "ORDER BY o.id DESC LIMIT 1";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array($id_cita));
        $data = $query->result();
        return $data;
    }

    public function orden_completa($id_cita = '')
    {
        $sql = "SELECT  t.nombre AS tecnico,c.id_tecnico,  o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, o.fecha_recepcion, o.fecha_entrega ,c.vehiculo_modelo, c.vehiculo_placas, o.folioIntelisis, c.vehiculo_anio, c.vehiculo_numero_serie, o.vehiculo_identificacion ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "WHERE c.id_cita = ? ".
        "ORDER BY o.fecha_recepcion DESC LIMIT 1";
        $query = $this->db->query($sql,array($id_cita));
        $data = $query->result();
        return $data;
    }

    public function get_table_list($table,$campo){
        $result = $this->db->order_by($campo, "DESC")
            ->limit(200)
            ->get($table)
            ->result();
        return $result;
    }

    public function api_orden($idOrden = 0)
    {
        $sql = "SELECT  o.*,c.*,o.id_cita as id_Cita_orden,t.nombre AS tecnico, t.clave AS clave_tecnico,cat.categoria, subcat.subcategoria, o.folioIntelisis, t.num_tec, ".
        "o.numero_cliente as num_Cliente_orden, c.numero_cliente as num_Cliente_cita, o.vehiculo_kilometraje AS km , cto.identificador ".

        "FROM citas AS c ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN categorias AS cat ON cat.id = o.idcategoria ".
        "LEFT JOIN subcategorias AS subcat ON subcat.id = o.idsubcategoria ".
        "LEFT JOIN cat_tipo_orden AS cto ON cto.id = o.id_tipo_orden ".
        //Estatus para intelisis
        // "LEFT JOIN cat_status_intelisis_orden AS st ON st.id = o.id_status_intelisis ".
        // "LEFT JOIN cat_situacion_intelisis AS sit ON sit.id = o.id_situacion_intelisis ".
        // "LEFT JOIN cat_status_orden AS stod ON stod.id = o.id_status ".
        "WHERE c.id_cita = ?";
        $query = $this->db->query($sql,array($idOrden));

        $data = $query->result();
        return $data;
    }

    //Consula para filtros de refacciones
    public function filtro_Sec($campo_1 = "",$campo_2 = "",$campo_3 = "",$tabla = "",$valor = "",$valor_2 = "")
    {
        $this->db->distinct($campo_1);
        $this->db->select($campo_1);
        $this->db->from($tabla);
        $this->db->where($campo_3,$valor_2);
        $this->db->like($campo_2,$valor,'after');

        $this->db->order_by($campo_1, "ASC");
        $query = $this->db->get()->result();
        return $query;
    }

    //Consula para filtros de refacciones
    public function filtro_princ($campo_1 = "",$tabla = "",$select = "")
    {
        $this->db->distinct($campo_1);
        $this->db->select($campo_1);
        $this->db->from($tabla);
        $this->db->order_by($campo_1, "ASC");
        $query = $this->db->get()->result();
        return $query;
    }

    //Consulta para filtros de refacciones
    public function filtro_descripcion($campo_1 = "",$valor = "",$tabla = "")
    {
        $this->db->distinct('clave_comprimida');
        $this->db->select('*');
        $this->db->from($tabla);
        $this->db->like($campo_1,$valor);

        $this->db->order_by($campo_1, "ASC");
        $query = $this->db->get()->result();
        return $query;
    }

    //Paginacion de refacciones
    public function get_total($tabla = "")
    {
        return $this->db->count_all($tabla);
    }

    public function get_current_page_records($limit, $start,$tabla)
    {
        $this->db->limit($limit, $start);
        $query = $this->db->get($tabla);

        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row)
            {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }

    //Recuperar una tabla en orden asecendente
    public function get_table_order($table,$campo){
        $this->db->order_by($campo, "ASC");
        $result = $this->db->get($table)->result();
        return $result;
    }

    //Consula para filtros de refacciones
    public function filtro_busqueda($valor = "",$tabla = "")
    {
        // $this->db->distinct('clave_comprimida');
        $this->db->select('*');
        $this->db->from($tabla);
        $this->db->like('descripcion',$valor,'both');
        $this->db->or_like('clave_comprimida',$valor,'both');

        $this->db->order_by('descripcion', "ASC");
        $query = $this->db->get()->result();
        return $query;
    }

    //Recuperar una tabla en orden asecendente limitada a 1000 registros
    public function get_table_order_limit($table,$campo){
        $this->db->order_by($campo, "ASC");
        $this->db->limit(500);
        $result = $this->db->get($table)->result();
        return $result;
    }

    //Recuperamos el id del historial proactivo
    public function idHistorialProactivo($serie = '')
    {
        $q = $this->db->where('serie_larga',$serie)->select('id')->limit(1)->get('magic');
        if($q->num_rows()==1){
            $respuesta = $q->row()->id;
        }else{
            $respuesta = '0';
        }
        return $respuesta;
    }

    //Recuperamos las cotizaciones rechazadas
    public function quotesList() 
    {
        $sql = "SELECT c.asesor, o.id_cita, o.fecha_recepcion, c.asesor, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, o.folioIntelisis, ".
        "cm.aceptoTermino, cm.identificador, c.vehiculo_numero_serie, o.vehiculo_identificacion, cm.contenido, o.telefono_movil ,o.otro_telefono ".
        "FROM ordenservicio AS o ".
        "INNER JOIN cotizacion_multipunto AS cm ON o.id_cita = cm.idOrden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE cm.aceptoTermino != ? AND cm.aceptoTermino != ?".
        "ORDER BY o.id DESC ".
        "LIMIT 130";

        $query = $this->db->query($sql,array('Si',''));
        $data = $query->result();
        return $data;
    }

    //Recuperamos el id del historial proactivo
    public function idFichaPerfil($idPerfil = '')
    {
        $q = $this->db->where('id',$idPerfil)->select('*')->limit(1)->get('magic')->result();
        return $q;
    }

    //Recuperamos el id del estatus en color de servicio
    public function tipo_servicio($id_cita=''){
        $q = $this->db->where('id_cita',$id_cita)->select('id_status_color')->from('citas')->get();
        if($q->num_rows()==1){
            $respuesta = $q->row()->id_status_color;
        }else{
            $respuesta = '';
        }
        return $respuesta;
    }

    public function listaOrdenes_historial(){
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, o.folioIntelisis ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "ORDER BY o.id DESC ".
        "LIMIT 300 ";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array());
        $data = $query->result();
        return $data;
    }

    //Filtros para magic
    public function busquedaMagicCampo($comodin = '')
    {
        $sql = "SELECT id FROM magic ".
        "WHERE nombre LIKE ? OR ap LIKE ? OR am LIKE ? OR calle LIKE ? OR colonia LIKE ? ".
        "OR cp LIKE ? OR municipio LIKE ? OR tel_principal LIKE ? OR tel_adicional LIKE ? OR dirigirse_con LIKE ? OR rfc LIKE ? ".
        "OR correo LIKE ? OR tel_secundario LIKE ? OR notorre LIKE ? OR km_actual LIKE ? OR placas LIKE ? OR no_siniestro LIKE ? ".
        "OR no_orden LIKE ? OR serie_larga LIKE ? OR codigoCte LIKE ? OR razon_social LIKE ? ".
        "ORDER BY fecha_recepcion DESC LIMIT 40";

        $contenido = array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%");

        $query = $this->db->query($sql,$contenido);
        $data = $query->result();
        return $data;
    }

    public function busquedaMagicFecha($fech1 = '',$fech2 = '')
    {
        $sql = "SELECT id FROM magic WHERE fecha_recepcion BETWEEN ? AND ? ORDER BY fecha DESC LIMIT 40";

        $query = $this->db->query($sql,array($fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    public function busquedaMagicTodo($comodin = '',$fech1 = '',$fech2 = '')
    {
        $sql = "SELECT id FROM magic ".
        "WHERE nombre LIKE ? OR ap LIKE ? OR am LIKE ? OR calle LIKE ? OR colonia LIKE ? ".
        "OR cp LIKE ? OR municipio LIKE ? OR tel_principal LIKE ? OR tel_adicional LIKE ? OR dirigirse_con LIKE ? OR rfc LIKE ? ".
        "OR correo LIKE ? OR tel_secundario LIKE ? OR notorre LIKE ? OR km_actual LIKE ? OR placas LIKE ? OR no_siniestro LIKE ? ".
        "OR no_orden LIKE ? OR serie_larga LIKE ? OR codigoCte LIKE ? OR razon_social LIKE ? AND fecha_recepcion BETWEEN ? AND ? ".
        "ORDER BY fecha_recepcion DESC LIMIT 40";

        $contenido = array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",$fech1,$fech2);

        $query = $this->db->query($sql,$contenido);
        $data = $query->result();
        return $data;

    }

    //Filtros para magic actualizado (ordenes)
    public function busquedaMagic_CitasCampo($comodin = '')
    {
        $sql = "SELECT c.id_cita FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "WHERE c.email LIKE ? OR c.vehiculo_anio LIKE ? OR c.vehiculo_modelo LIKE ? OR c.vehiculo_version LIKE ? OR ".
        "c.vehiculo_placas LIKE ? OR c.vehiculo_numero_serie LIKE ? OR c.comentarios_servicio LIKE ? OR c.asesor LIKE ? OR ".
        "c.datos_email LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? OR ".
        "c.datos_telefono LIKE ? OR c.servicio LIKE ? OR c.ubicacion_unidad LIKE ? OR c.numero_cliente LIKE ? OR ".
        "o.telefono_asesor LIKE ? OR o.extension_asesor LIKE ? OR o.numero_interno LIKE ? OR o.nombre_compania LIKE ? OR o.nombre_contacto_compania LIKE ? OR ".
        "o.am_contacto LIKE ? OR o.rfc LIKE ? OR o.correo_compania LIKE ? OR o.calle LIKE ? OR o.nointerior LIKE ? OR o.noexterior LIKE ? OR ".
        "o.colonia LIKE ? OR o.municipio LIKE ? OR o.cp LIKE ? OR o.estado LIKE ? OR o.telefono_movil LIKE ? OR o.otro_telefono LIKE ? OR o.ap_contacto LIKE ? OR ".
        "o.vehiculo_identificacion LIKE ? OR o.vehiculo_kilometraje LIKE ? OR o.motor LIKE ? OR o.transmision LIKE ? OR o.numero_cliente LIKE ? ".
        "ORDER BY o.id DESC LIMIT 40";

        $contenido = array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
            "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%");

        $query = $this->db->query($sql,$contenido);
        $data = $query->result();
        return $data;
    }

    public function busquedaMagic_CitasFecha($fech1 = '0000-00-00',$fech2 = '0000-00-00')
    {
        $sql = "SELECT id_cita FROM ordenservicio WHERE fecha_recepcion BETWEEN ? AND ? ORDER BY fecha_recepcion DESC LIMIT 40";

        $query = $this->db->query($sql,array($fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    public function busquedaMagic_CitasTodo($comodin = '',$fech1 = '0000-00-00',$fech2 = '0000-00-00')
    {
        $sql = "SELECT c.id_cita FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "WHERE c.email LIKE ? OR c.vehiculo_anio LIKE ? OR c.vehiculo_modelo LIKE ? OR c.vehiculo_version LIKE ? OR ".
        "c.vehiculo_placas LIKE ? OR c.vehiculo_numero_serie LIKE ? OR c.comentarios_servicio LIKE ? OR c.asesor LIKE ? OR ".
        "c.datos_email LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? OR ".
        "c.datos_telefono LIKE ? OR c.servicio LIKE ? OR c.ubicacion_unidad LIKE ? OR c.numero_cliente LIKE ? OR ".
        "o.telefono_asesor LIKE ? OR o.extension_asesor LIKE ? OR o.numero_interno LIKE ? OR o.nombre_compania LIKE ? OR o.nombre_contacto_compania LIKE ? OR ".
        "o.am_contacto LIKE ? OR o.rfc LIKE ? OR o.correo_compania LIKE ? OR o.calle LIKE ? OR o.nointerior LIKE ? OR o.noexterior LIKE ? OR ".
        "o.colonia LIKE ? OR o.municipio LIKE ? OR o.cp LIKE ? OR o.estado LIKE ? OR o.telefono_movil LIKE ? OR o.otro_telefono LIKE ? OR o.ap_contacto LIKE ? OR ".
        "o.vehiculo_identificacion LIKE ? OR o.vehiculo_kilometraje LIKE ? OR o.motor LIKE ? OR o.transmision LIKE ? OR o.numero_cliente LIKE ? ".
        "OR o.fecha_recepcion BETWEEN ? AND ? ORDER BY o.fecha_recepcion DESC LIMIT 40";

        $contenido = array("%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
        "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
        "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
        "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",
        "%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%","%".$comodin."%",$fech1,$fech2);

        $query = $this->db->query($sql,$contenido);
        $data = $query->result();
        return $data;

    }

    //Consulta para ordenes entregadas en dia de hoy
    public function orderClosed($fecha = "")
    {
        $sql = "SELECT o.id_cita FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita WHERE c.unidad_entregada = 1 AND o.fecha_entrega = ?";
        $query = $this->db->query($sql,array($fecha));
        $data = $query->result();
        return $data;
    }

    //Recuperamos el id del historial proactivo
    public function nombreOperador($idPerfil = '')
    {
        $q = $this->db->where('id',$idPerfil)->select('nombre')->get('operadores');
        if($q->num_rows()==1){
            $retorno = $q->row()->nombre;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    //----
    public function last_id_tableVC()
    {
        $query = $this->db->query("SELECT idRegistro FROM voz_cliente_temp ORDER BY idRegistro DESC LIMIT 1");
        return $query;
    }

    public function last_id_tableVC_2()
    {
        $query = $this->db->query("SELECT idRegistro FROM voz_cliente ORDER BY idRegistro DESC LIMIT 1");
        return $query;
    }

    public function merge_tableVC($id = 0)
    {
        $query = $this->db->query("INSERT INTO voz_cliente SELECT * FROM voz_cliente_temp WHERE idRegistro = ?",array($id));
        return $query;
    }

    public function get_table_desc_VC(){
        $query = $this->db->query("SELECT * FROM voz_cliente ORDER BY fecha_registro DESC LIMIT 300");
        $result = $query->result();
        return $result;
    }

    public function dataMultipuntoTec($idTecnico = 0)
    {
        $query = $this->db->where("t.id",$idTecnico)
            ->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')
            ->join('citas AS c','c.id_cita = mg.orden')
            ->join('ordenservicio AS o','o.id_cita = c.id_cita')
            ->join('tecnicos AS t','c.id_tecnico = t.id')
            ->select('mg3.comentario,mg3.sistema,mg3.componente,mg3.causaRaiz,mg3.asesor,mg3.nombreCliente,mg.orden,mg.fechaCaptura,o.folioIntelisis,mg3.tecnico')
            ->order_by("o.id", "DESC")
            ->limit(50)
            ->get('multipunto_general AS mg')
            ->result();
        return $query;
    }

    public function dataMultipunto()
    {
        $query = $this->db->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')
            // ->join('citas AS c','c.id_cita = mg.orden')
            // ->join('tecnicos AS t','c.id_tecnico = t.id')
            ->join('ordenservicio AS o','o.id_cita = mg.orden')
            ->select('mg3.comentario, mg3.sistema, mg3.componente, mg3.causaRaiz, mg3.asesor, mg3.nombreCliente, mg.orden, mg.fechaCaptura, o.folioIntelisis, mg3.tecnico')
            ->order_by("o.id", "DESC")
            ->limit(50)
            ->get('multipunto_general AS mg')
            ->result();
        return $query;
    }

    public function tecnicos_citas($idCita = ""){
        $sql = "SELECT id,fecha, hora_inicio_dia, fecha_fin, hora_fin FROM tecnicos_citas WHERE id_cita = ? ORDER BY id ASC";
        $query = $this->db->query($sql,$idCita);
        $data = $query->result();
        return $data;
    }

    //Recuperar informacion de la orden
    public function notificacionTecnicoCita($idOrden = '')
    {
        $sql = "SELECT tc.*, st.nombre, c.comentarios_servicio, catOp.tipo_operacion, t.nombre AS tecnico, c.vehiculo_modelo, c.asesor, c.vehiculo_placas, c.vehiculo_anio, o.orden_magic, c.id_status_color, o.folioIntelisis, c.vehiculo_numero_serie, o.vehiculo_identificacion, c.id_cita ".
            "FROM ordenservicio AS o ".
            "INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
            "INNER JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN tecnicos_citas AS tc ON tc.id_cita = o.id_cita ".
            "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "INNER JOIN cat_tipo_operacion AS catOp ON catOp.id = o.id_tipo_operacion ".
            "WHERE o.id_cita = ? LIMIT 1";

        $query = $this->db->query($sql,$idOrden);
        $data = $query->result();
        return $data;
    }

    public function update_table_row_2($table,$data,$id_table,$id,$id_table_2,$id_2){
        $result = $this->db->update($table, $data, array($id_table=>$id,$id_table_2=>$id_2));
        return $result;
    }

    //Recuperamos el id de la orden de magic
    public function id_orden_magic($idorden=''){
        $q = $this->db->where('id_cita',$idorden)->select('orden_magic')->limit(1)->from('ordenservicio')->get();
        if($q->num_rows()==1){
            return $q->row()->orden_magic;
        }else{
            return '';
        }
    }

    //Verificar si el tecnico tiene la orden asignada
    public function tecnicoCitas($idCita = 0, $idTecnico = 0)
    {
        $q = $this->db->where('id_cita',$idCita)->where('id_tecnico',$idTecnico)->select('asesor')->get('citas');
        if($q->num_rows()==1){
            $respuesta = '1';
        }else{
            $respuesta = '0';
        }
        return $respuesta;
    }

    //Verificamos si esta la unidad en lavado
    public function estatusLavado($id_cita = '')
    {
        $q = $this->db->where('id_cita',$id_cita)->select('id_lavador_cambio_estatus')->get('lavado');
        if($q->num_rows()==1){
            // 0 = Entro a lavado
            // 1 = Lo estan lavando
            // 2 = Termino lavado
            $retorno = 'TERMINADO';
            //$retorno = $q->row()->id_lavador_cambio_estatus;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    //Validar status de orden
    public function estatusOrden($id_cita ='')
    {
        //Si está en 2 o 3 si debes dejarlo
        $query = $this->db->where('c.id_cita',$id_cita)
            ->where('c.cita_previa','1')
            ->join('aux AS a','c.id_horario = a.id')
            ->select('a.id_status_cita')
            ->from('citas AS c')
            ->get();

        if($query->num_rows() == 1){
            $data = $query->row()->id_status_cita;
        }else{
            $data = '0';
        }

        return $data;
    }

    //Recuperar informacion para lista en cotizacion
    public function listaDiagnosticoDatos()
    {
        $sql = "SELECT t.nombre AS tecnico, o.id_cita AS orden_cita, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, o.fecha_recepcion,c.vehiculo_modelo,c.vehiculo_placas,d.*, o.folioIntelisis ".  
            "FROM diagnostico AS d ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = d.noServicio ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".            
            "ORDER BY o.id DESC LIMIT 220";

        $query = $this->db->query($sql);
        $data = $query->result();
        return $data;
    }

    //Verificamos si existe algun comentario para la orden
    public function validarComentarioCO($idOrden = '')
    {
        $q = $this->db->where('id_cita',$idOrden)->select('id')->limit(1)->get('comentarios_cierre_orden');
        if($q->num_rows()==1){
            $respuesta = '1';
        }else{
            $respuesta = '0';
        }
        return $respuesta;
    }

    //Duplicar registros
    public function duplicar_registros($tabla = '',$idOrden = '',$idOrdenNvo = '',$campo = '', $indice = '')
    {
        $registro = $this->db->where($campo,$idOrden)->select('*')->limit(1)->get($tabla)->result();
        $result = $this->db->insert($tabla, $registro);
        //Comprobamos que se guarden correctamente el registro
        if ($result) {
            $result = $this->db->insert_id();
        }else {
            $result = 0;
        }
        //$query = $this->db->insert($table, $data);
        //$query = $this->db->query("INSERT INTO diagnostico SELECT * FROM diagnostico_sc WHERE idDiagnostico = ?",array($id));
        return $result;
    }

    //Informacion de ordenes para imprimir busqueda
    public function busquedaOrdenesFechas_1($fech1 = '',$fech2 = '') 
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, ".
            "c.vehiculo_placas, coti.idCotizacion, coti.aceptoTermino, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus, o.folioIntelisis , prer.id AS id_presupuesto, prer.acepta_cliente , valet.id AS idValet ".
            "FROM citas AS c ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            "LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".            
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            "WHERE o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY o.id DESC LIMIT 200";

        $query = $this->db->query($sql,array($fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    //Informacion de ordenes para imprimir busqueda
    public function busquedaOrdenesFecha($fech1 = '')
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, o.folioIntelisis, ".
            "c.vehiculo_placas, coti.idCotizacion, coti.aceptoTermino, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus , prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet ".
            "FROM citas AS c ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            "LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            "WHERE o.fecha_recepcion =  ? " .        
            "ORDER BY o.id DESC LIMIT 200";

        $query = $this->db->query($sql,array($fech1));
        $data = $query->result();
        return $data;
    }

     //Informacion de ordenes para imprimir busqueda por campo
    public function busquedaOrdenesCampo($campo = '')
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, o.folioIntelisis, ".
            "c.vehiculo_placas, coti.idCotizacion, coti.aceptoTermino, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus , prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet ".
            "FROM citas AS c ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            "LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            "WHERE t.nombre LIKE ? OR o.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR ".
            "c.datos_apellido_materno LIKE ? OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? " .        
            "ORDER BY o.id DESC LIMIT 200";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%'));
        $data = $query->result();
        return $data;
    }

    //Informacion de ordenes para imprimir busqueda por campo
    public function busquedaOrdenesCampo_Fecha($campo = '',$fech1 = '',$fech2 = '')
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, o.folioIntelisis, ".
            "c.vehiculo_placas, coti.idCotizacion, coti.aceptoTermino, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus , prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet ".
            "FROM citas AS c ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            "LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            "WHERE t.nombre LIKE ? OR o.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR ".
            "c.datos_apellido_materno LIKE ? OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY o.id DESC LIMIT 200";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',$fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    //Informacion de ordenes para imprimir busqueda por campo
    public function busquedaOrdenesCampo_Fecha_1($campo = '',$fecha = '')
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,o.fecha_recepcion,c.vehiculo_modelo, o.folioIntelisis, ".
            "c.vehiculo_placas, coti.idCotizacion, coti.aceptoTermino, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus , prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet ".
            "FROM citas AS c ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
            //Cotizacion Multipunto
            "LEFT JOIN cotizacion_multipunto AS coti ON coti.idOrden = c.id_cita ".
            //Presupuesto Multipunto (nuevas cotizaciones)
            "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
            //Multipunto
            "LEFT JOIN multipunto_general AS mg ON mg.orden = c.id_cita ".
            "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
            //Voz Cliente
            "LEFT JOIN voz_cliente AS voz ON voz.idPivote = c.id_cita ".
            //Servicio valet
            "LEFT JOIN servicio_valet AS valet ON valet.id_cita = c.id_cita ".
            //Lavado
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = c.id_cita ".
            "WHERE t.nombre LIKE ? OR o.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR ".
            "c.datos_apellido_materno LIKE ? OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND o.fecha_recepcion =  ? ".
            "ORDER BY o.id DESC LIMIT 200";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',$fecha));
        $data = $query->result();
        return $data;
    }

    //Listar cotizaciones
    //Recuperar informacion para lista en cotizacion
    public function listaCotizacionDatos()
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,cm.*, o.folioIntelisis ".  
            "FROM cotizacion_multipunto AS cm ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = cm.idOrden ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE cm.identificador = ? ".
            "ORDER BY cm.fech_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('M'));
        $data = $query->result();
        return $data;
    }

    public function listaCotizacionDatosGarantia()
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,cm.*, o.fecha_recepcion, o.folioIntelisis ".  
            "FROM cotizacion_multipunto AS cm  ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = cm.idOrden ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE cm.pzaGarantia != ? ".
            "ORDER BY cm.fech_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array(''));
        $data = $query->result();
        return $data;
    }

    //Recuperar informacion para lista en cotizacion por tecnico
    public function listaCotizacionDatosTecnicos($id_Tecnico = '')
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,cm.*, o.folioIntelisis ".  
            "FROM cotizacion_multipunto AS cm ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = cm.idOrden ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE c.id_tecnico = ? AND cm.identificador = ? ".
            "ORDER BY cm.fech_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array($id_Tecnico,'M'));
        $data = $query->result();
        return $data;
    }

    //Recuperar informacion para lista en cotizacion para el jefe de taller
    public function listaCotizacionDatosJDT()
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,cm.*, o.folioIntelisis ".  
            "FROM cotizacion_multipunto AS cm ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = cm.idOrden ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE cm.aprobacionRefaccion != ? AND cm.identificador = ? ".
            "ORDER BY cm.fech_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('0','M'));
        $data = $query->result();
        return $data;
    }

    //Recuperar informacion para lista en cotizacion
    public function listaCotizacionBusqueda($campo = '')
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,cm.*, o.folioIntelisis ".  
            "FROM cotizacion_multipunto AS cm ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = cm.idOrden ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR c.vehiculo_modelo LIKE ? OR c.asesor LIKE ? OR c.vehiculo_placas LIKE ? OR cm.idOrden LIKE ? AND cm.identificador = ?".
            "ORDER BY o.id DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','M'));
        $data = $query->result();
        return $data;
    }

    //Adicionales consultas para conexion intelisis
    //Recuperamos el id de la orden de intelisis
    public function id_orden_intelisis($idorden=''){
        $q = $this->db->where('id_cita',$idorden)->select('folioIntelisis')->limit(1)->from('ordenservicio')->get();
        if($q->num_rows()==1){
            return $q->row()->folioIntelisis;
        }else{
            return '';
        }
    }

    //Recuperamos el id de la orden de intelisis
    public function ids_intelisis($idorden=''){
        $q = $this->db->where('id_cita',$idorden)->select('idIntelisis,idVentaIntelisis')->from('ordenservicio')->get();
        $data = $q->result();

        return $data;
    }

    public function get_result_not($value = ""){   //
        //$sql = "SELECT c.*, o.* FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita WHERE o.errorEnvio != ? OR WHERE o.idVentaIntelisis = ? AND o.fecha_recepcion > ? ORDER BY o.fecha_recepcion DESC LIMIT 50";
        $sql = "SELECT c.*, o.* FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.idVentaIntelisis = ? AND o.folioIntelisis = ? AND o.fecha_recepcion > ?".
        "ORDER BY o.id DESC LIMIT 100";
        $query = $this->db->query($sql,array($value,$value,"2019-10-28"));
        $data = $query->result();
        return $data;
    }

    public function get_result_not_fecha($fecha = ""){   
        $sql = "SELECT c.*, o.* FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.idVentaIntelisis = ? ".
        "AND o.folioIntelisis = ? ".
        "AND o.fecha_recepcion = ? ORDER BY o.id DESC LIMIT 100";
        $query = $this->db->query($sql,array('','',$fecha));
        $data = $query->result();
        return $data;
    }

    public function get_result_not_cita($id_cita = ""){   
        $sql = "SELECT c.*, o.* FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.idVentaIntelisis = ? ".
        "AND o.folioIntelisis = ? ".
        "AND o.id_cita like ? ORDER BY o.id DESC LIMIT 100";
        $query = $this->db->query($sql,array('','','%'.$id_cita.'%'));
        $data = $query->result();
        return $data;
    }

    public function get_result_not_fecha_1($fecha_1 = "",$fecha_2 = ""){   
        $sql = "SELECT c.*, o.* FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.idVentaIntelisis = ? ".
        "AND o.folioIntelisis = ? ".
        "AND o.fecha_recepcion BETWEEN ? AND ? ORDER BY o.id DESC LIMIT 100";
        $query = $this->db->query($sql,array('','',$fecha_1,$fecha_2));
        $data = $query->result();
        return $data;
    }

    public function get_result_not_folio($value = ""){   //
        $sql = "SELECT c.*, o.* FROM ordenservicio AS o INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.idVentaIntelisis != ? ".
        "AND o.folioIntelisis = ? ".
        "AND o.fecha_recepcion > ? ORDER BY o.fecha_recepcion DESC LIMIT 50";
        $query = $this->db->query($sql,array($value,$value,'2019-11-06'));
        $data = $query->result();
        return $data;
    }

    public function multipuntoApi($id_cita = 0)
    {
        $sql = "SELECT o.idVentaIntelisis,mg.id,mg.bateriaEstado,mg.bateriaCambio,mg.bateria,mg3.tecnico, o.folioIntelisis,t.num_tec, ".
        "fi.espesoBalataFI,fi.espesorBalataFIcambio,fi.espesorBalataFImm,fi.espesorDiscoFI,fi.espesorDiscoFIcambio,fi.espesorDiscoFImm, ".
        "ti.espesoBalataTI,ti.espesorBalataTIcambio,ti.espesorBalataTImm,ti.espesorDiscoTI,ti.espesorDiscoTIcambio,ti.espesorDiscoTImm,ti.diametroTamborTI,ti.diametroTamborTIcambio, ".
        "fd.espesoBalataFD,fd.espesorBalataFDcambio,fd.espesorBalataFDmm,fd.espesorDiscoFD,fd.espesorDiscoFDcambio,fd.espesorDiscoFDmm, ".
        "td.espesoBalataTD,td.espesorBalataTDcambio,td.espesorBalataTDmm,td.espesorDiscoTD,td.espesorDiscoTDcambio,td.espesorDiscoTDmm,td.diametroTamborTD,td.diametroTamborTDcambio ".
        
        "FROM multipunto_general AS mg ".
        "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
        "LEFT JOIN frente_izquierdo AS fi ON fi.idOrden = mg.id ".
        "LEFT JOIN trasero_izquierdo AS ti ON ti.idOrden = mg.id ".
        "LEFT JOIN frente_derecho AS fd ON fd.idOrden = mg.id ".
        "LEFT JOIN trasero_derecho AS td ON td.idOrden = mg.id ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = mg.orden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "WHERE mg.orden = ?";

        $query = $this->db->query($sql,array($id_cita));
        $data = $query->result();
        return $data;
    }

    //Buscar cotizaciones rechazdas por serie
    public function cotizaciones_por_serie($serie = '')
    {
        $sql = "SELECT o.id_cita, cm.aceptoTermino,c.vehiculo_numero_serie,o.vehiculo_identificacion,cm.contenido ".
        "FROM ordenservicio AS o ".
        "INNER JOIN cotizacion_multipunto AS cm ON o.id_cita = cm.idOrden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE c.vehiculo_numero_serie = ? OR o.vehiculo_identificacion = ? AND cm.aceptoTermino = ? AND cm.aceptoTermino = ?".
        "ORDER BY o.id DESC ".
        "LIMIT 200";

        $query = $this->db->query($sql,array($serie,$serie,'No','Val'));
        $data = $query->result();
        return $data;
    }

    //Ordenes de manera informativa por estaus
    public function tecnicosOrdenesEstatus($idTecnico = 0)
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, o.fecha_recepcion,c.vehiculo_modelo,c.vehiculo_placas, cat_ct.status, cat_ct.color,o.folioIntelisis ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN cat_status_citas_tecnicos AS cat_ct ON cat_ct.id = c.id_status ".
        "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "WHERE c.id_tecnico = ? ".
        "AND cat_ct.id = ? OR cat_ct.id = ? OR cat_ct.id = ? OR cat_ct.id = ? OR cat_ct.id = ?".
        "ORDER BY o.id DESC LIMIT 220";
        $query = $this->db->query($sql,array($idTecnico,"15","16","17","18","19"));
        $data = $query->result();
        return $data;
    }

    public function listaOrdenesEstatus()
    {
        $sql = "SELECT  t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, o.fecha_recepcion,c.vehiculo_modelo,c.vehiculo_placas, cat_ct.status, cat_ct.color,o.folioIntelisis ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        "LEFT JOIN cat_status_citas_tecnicos AS cat_ct ON cat_ct.id = c.id_status ".
        "LEFT JOIN diagnostico AS d ON d.noServicio = c.id_cita ".
        "WHERE cat_ct.id = ? OR cat_ct.id = ? OR cat_ct.id = ? OR cat_ct.id = ? OR cat_ct.id = ? ".
        "ORDER BY o.id DESC";
        // "WHERE c.id_tecnico = ?";
        $query = $this->db->query($sql,array("15","16","17","18","19"));
        $data = $query->result();
        return $data;
    }

    //Validar si pasó la unidad por trabajando
    public function unidadTrabajando($id_cita = ''){
        $query = "SELECT * FROM transiciones_estatus WHERE id_cita = ? AND (status_nuevo = ? OR status_anterior = ?)";
        $result = $this->db->query($query,array($id_cita,'trabajando','trabajando'))->result();

        if(count($result)>0){
          $retorno = "TRUE";
        }else{
          $retorno = "FALSE";
        }

        return $retorno;
    }


    //Recuperamos las cotizaciones rechazadas por fecha
    public function quotesList_Fecha($fecha = "")
    {
        $sql = "SELECT c.asesor,o.id_cita,o.fecha_recepcion,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, cm.aceptoTermino,cm.identificador,c.vehiculo_numero_serie,o.vehiculo_identificacion,cm.contenido,o.telefono_movil ,o.otro_telefono,o.folioIntelisis  ".
        "FROM ordenservicio AS o ".
        "INNER JOIN cotizacion_multipunto AS cm ON o.id_cita = cm.idOrden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.fecha_recepcion = ? AND cm.aceptoTermino != ? AND cm.aceptoTermino != ? ".
        "ORDER BY o.fecha_recepcion DESC LIMIT 200";

        $query = $this->db->query($sql,array($fecha,'Si',''));
        $data = $query->result();
        return $data;
    }

    //Recuperamos las cotizaciones rechazadas entre fechas
    public function quotesList_Fechas_1($fecha_1 = "",$fecha_2 = "")
    {
        $sql = "SELECT c.asesor,o.id_cita,o.fecha_recepcion,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, cm.aceptoTermino,cm.identificador,c.vehiculo_numero_serie,o.vehiculo_identificacion,cm.contenido,o.telefono_movil ,o.otro_telefono,o.folioIntelisis  ".
        "FROM ordenservicio AS o ".
        "INNER JOIN cotizacion_multipunto AS cm ON o.id_cita = cm.idOrden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.fecha_recepcion  BETWEEN ? AND ? AND cm.aceptoTermino != ? AND cm.aceptoTermino != ? ".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 200";

        $query = $this->db->query($sql,array($fecha_1,$fecha_2,'Si',''));
        $data = $query->result();
        return $data;
    }

    //Recuperamos las cotizaciones rechazadas por un campo
    public function quotesList_campo($campo = "")
    {
        $sql = "SELECT c.asesor,o.id_cita,o.fecha_recepcion,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, cm.aceptoTermino,cm.identificador,c.vehiculo_numero_serie,o.vehiculo_identificacion,cm.contenido,o.telefono_movil ,o.otro_telefono,o.folioIntelisis ".
        "FROM ordenservicio AS o ".
        "INNER JOIN cotizacion_multipunto AS cm ON o.id_cita = cm.idOrden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.id_cita LIKE ? OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? OR c.asesor LIKE ? OR c.datos_nombres LIKE ? OR ".
        "c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? AND cm.aceptoTermino != ? AND cm.aceptoTermino != ? ".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 200";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','Si',''));
        $data = $query->result();
        return $data;
    }

    //Recuperamos las cotizaciones rechazadas por fecha y un campo
    public function quotesList_fechaCampo($fecha = "",$campo = "")
    {
        $sql = "SELECT c.asesor,o.id_cita,o.fecha_recepcion,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, cm.aceptoTermino,cm.identificador,c.vehiculo_numero_serie,o.vehiculo_identificacion,cm.contenido,o.telefono_movil ,o.otro_telefono,o.folioIntelisis  ".
        "FROM ordenservicio AS o ".
        "INNER JOIN cotizacion_multipunto AS cm ON o.id_cita = cm.idOrden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.id_cita LIKE ? OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? OR c.asesor LIKE ? OR c.datos_nombres LIKE ? OR ".
        "c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? AND o.fecha_recepcion = ? AND cm.aceptoTermino != ? AND cm.aceptoTermino != ? ".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 200";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',$fecha,'Si',''));
        $data = $query->result();
        return $data;
    }

    //Recuperamos las cotizaciones rechazadas entre fechas y un campo
    public function quotesList_fechaCampo_1($fecha_1 = "",$fecha_2 = "",$campo = "")
    {
        $sql = "SELECT c.asesor,o.id_cita,o.fecha_recepcion,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, cm.aceptoTermino,cm.identificador,c.vehiculo_numero_serie,o.vehiculo_identificacion,cm.contenido,o.telefono_movil ,o.otro_telefono,o.folioIntelisis  ".
        "FROM ordenservicio AS o ".
        "INNER JOIN cotizacion_multipunto AS cm ON o.id_cita = cm.idOrden ".
        "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
        "WHERE o.id_cita LIKE ? OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? OR c.asesor LIKE ? OR c.datos_nombres LIKE ? OR ".
        "c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? AND o.fecha_recepcion  BETWEEN ? AND ? AND cm.aceptoTermino != ? AND cm.aceptoTermino != ? ".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 200";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',$fecha_1,$fecha_2,'Si',''));
        $data = $query->result();
        return $data;
    }

/******************************* Comienzan nuevos procedimientos para los presupuesto *************************************/
/************************************************************************************************************************/    

    //Busquedas y consultas (nuevos) de los presupuestos
    public function get_result_field($campo_1 = "",$value_1 = "",$campo_2 = "",$value_2 = "",$tabla = ""){
        //$result = $this->db->where($campo_1,$value_1)->where($campo_2,$value_2)->get($tabla)->result();
        $result = $this->db->where($campo_1,$value_1)
        ->like($campo_2,$value_2,'both')
        ->order_by("id","DESC")
        ->limit(100)
        ->get($tabla)->result();
        return $result;
    }

    //Buscar cotizaciones rechazdas por serie
    public function presupuesto_por_serie($serie = '')
    {
        $sql = "SELECT prer.id_cita FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        //Presupuesto Multipunto (nuevas cotizaciones)
        "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
        "WHERE prer.serie = ? AND prer.acepta_cliente = ? OR ?".
        "ORDER BY o.id DESC ".
        "LIMIT 200";

        $query = $this->db->query($sql,array($serie,'No','Val'));
        $data = $query->result();
        return $data;
    }

    //Recuperamos las cotizaciones rechazadas
    public function presupuesto_proactivo()
    {
        $sql = "SELECT c.asesor,o.id_cita,o.fecha_recepcion,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno, prer.acepta_cliente, c.vehiculo_numero_serie,o.vehiculo_identificacion,o.telefono_movil ,o.otro_telefono,o.folioIntelisis,prer.identificador,prer.serie ".
        "FROM citas AS c ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = c.id_cita ".
        //Presupuesto Multipunto (nuevas cotizaciones)
        "LEFT JOIN presupuesto_registro AS prer ON prer.id_cita = c.id_cita ".
        "WHERE prer.acepta_cliente = ? OR ? ".
        "ORDER BY o.fecha_recepcion DESC ".
        "LIMIT 60";

        $query = $this->db->query($sql,array('No','Val'));
        $data = $query->result();
        return $data;
    }

    //Presupuestos de ventanilla
    public function presupuestoVentanilla($fecha='')
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.identificador = ?  AND o.fecha_recepcion > ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 30";

        $query = $this->db->query($sql,array('M',$fecha));
        $data = $query->result();
        return $data;
    }

    //Recuperar informacion para lista en cotizacion
    public function listaPresupuestoCampo($campo = '')
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR c.vehiculo_modelo LIKE ? OR c.asesor LIKE ? OR c.vehiculo_placas LIKE ? OR prer.id_cita LIKE ? ".
            "OR c.vehiculo_numero_serie  LIKE ?  OR o.vehiculo_identificacion LIKE ? AND prer.identificador = ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','M'));
        $data = $query->result();
        return $data;
    }

    public function listaCotizacionCampoEstatus($campo = '',$status = '')
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,cm.*, o.folioIntelisis ".  
            "FROM cotizacion_multipunto AS cm ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = cm.idOrden ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR c.vehiculo_modelo LIKE ? OR c.asesor LIKE ? OR c.vehiculo_placas LIKE ? OR cm.idOrden LIKE ? AND cm.identificador = ? AND cm.estado = ? AND cm.aceptoTermino != ? ".
            "ORDER BY o.id DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','M',$status,''));
        $data = $query->result();
        return $data;
    }

    public function listaCotizacionEstatus($status = '')
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,cm.*, o.folioIntelisis ".  
            "FROM cotizacion_multipunto AS cm ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = cm.idOrden ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE cm.identificador = ? AND cm.estado = ? AND cm.aceptoTermino != ? ".
            "ORDER BY o.id DESC LIMIT 100";

        $query = $this->db->query($sql,array('M',$status,''));
        $data = $query->result();
        return $data;
    }

    //Recuperar informacion para lista en cotizacion
    public function listaPresupuestoEstatusCampo($campo = '',$status = '')
    {
        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.estado_refacciones = ? AND prer.acepta_cliente != ? AND (t.nombre LIKE ? OR c.vehiculo_modelo LIKE ? OR c.asesor LIKE ? OR c.vehiculo_placas LIKE ? OR prer.id_cita LIKE ? ".
                        "OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ?) AND prer.identificador = ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 50";

        $query = $this->db->query($sql,array($status,'','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','M'));
        $data = $query->result();
        return $data;
    }

    //Presupuestos a listar del jefe de taller
    public function presupuestosJDT($fecha = '')
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.envia_ventanilla != ? AND prer.identificador = ? AND o.fecha_recepcion > ? ".
            "ORDER BY prer.termina_ventanilla DESC LIMIT 50";

        $query = $this->db->query($sql,array("0","M",$fecha));
        $data = $query->result();
        return $data;
    }

    //Presupuestos a listar del asesor
    public function presupuestosAsesor($fecha = '')
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            //"WHERE prer.envio_jdt != ? " .
            "WHERE prer.envio_jdt != ? AND prer.identificador = ? AND o.fecha_recepcion > ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 50";

        $query = $this->db->query($sql,array("0","M",$fecha));
        $data = $query->result();
        return $data;
    }

    //Presupuestos a listar en la vista de garantias
    public function presupuestosGarantias()
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.ref_garantias > ? AND prer.envio_jdt != ? AND prer.identificador = ?".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 80";

        $query = $this->db->query($sql,array("0","0","M"));
        $data = $query->result();
        return $data;
    }

    //Presupuestos a listar en la vista de los tecnicos (sin seleccion)
    public function presupuestosCreados($fecha='')
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.identificador = ? AND o.fecha_recepcion > ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 80";

        $query = $this->db->query($sql,array("M",$fecha));
        $data = $query->result();
        return $data;
    }

    //Presupuestos a listar en la vista de garantias
    public function presupuestosAfectados()
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.acepta_cliente != ? AND prer.identificador = ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 80";

        $query = $this->db->query($sql,array("","M"));
        $data = $query->result();
        return $data;
    }

    //Presupuestos a listar en la vista de los tecnicos (solo de un tecnico)
    public function presupuestosCreados_Tec($id_tecnico = "",$fecha = '')
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* ".  
            "FROM presupuesto_registro AS prer ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE c.id_tecnico = ? AND prer.identificador = ? AND o.fecha_recepcion > ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 80";

        $query = $this->db->query($sql,array($id_tecnico,"M",$fecha));
        $data = $query->result();
        return $data;
    }


    //***********************Busqueda de presupustos por estatus y fecha **********************************//
    //
    //Recuperar informacion para lista en cotizacion
    public function listaPresupuestoEstatus($status = '')
    {
        $sql = "SELECT t.nombre AS tecnico, c.vehiculo_modelo, c.asesor, c.vehiculo_placas, prer.*, o.nombre_compania, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.estado_refacciones = ? AND (prer.acepta_cliente != ? AND prer.acepta_cliente != ?) AND prer.firma_asesor != ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array($status,'','No',''));
        $data = $query->result();
        //echo $this->db->last_query();die();
        return $data;
    }

    public function PresupuestoFechaVentanilla($fecha = "")
    {
       $sql = "SELECT t.nombre AS tecnico, c.vehiculo_modelo, c.asesor, c.vehiculo_placas, prer.*, o.nombre_compania, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE o.fecha_recepcion = ? AND prer.identificador = ? AND prer.acepta_cliente != ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array($fecha,"M",""));
        $data = $query->result();
        return $data;
    }

    //Busqueda de presupuestos
    public function PresupuestoFechas_1Ventanilla($fecha_inicio = "",$fecha_fin = "")
    {
       $sql = "SELECT t.nombre AS tecnico, c.vehiculo_modelo, c.asesor, c.vehiculo_placas, prer.*, o.nombre_compania, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.identificador = ? AND prer.acepta_cliente != ? AND o.fecha_recepcion BETWEEN ? AND ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array("M","",$fecha_inicio,$fecha_fin));
        $data = $query->result();
        return $data;
    }

    public function PresupuestoFechasVentanilla_Edo($status = "",$fecha = "")
    {
       $sql = "SELECT t.nombre AS tecnico, c.vehiculo_modelo, c.asesor, c.vehiculo_placas, prer.*, o.nombre_compania, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE o.fecha_recepcion = ? AND prer.estado_refacciones = ? AND (prer.acepta_cliente != ? AND prer.acepta_cliente != ?) AND prer.firma_asesor != ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array($fecha,$status,'','No',''));
        $data = $query->result();
        return $data;
    }

    public function PresupuestoFechaVentanilla_Edo_1($status = "",$fecha_inicio = "",$fecha_fin = "")
    {
       $sql = "SELECT t.nombre AS tecnico, c.vehiculo_modelo, c.asesor, c.vehiculo_placas, prer.*, o.nombre_compania, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.estado_refacciones = ? AND (prer.acepta_cliente != ? AND prer.acepta_cliente != ?) AND prer.firma_asesor != ? AND o.fecha_recepcion BETWEEN ? AND ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array($status,'','No','',$fecha_inicio,$fecha_fin));
        $data = $query->result();
        return $data;
    }

    public function refacciones_ventanilla($id_cita='')
    {
        $sql = "SELECT estado_entrega, id_cita, cantidad, descripcion, num_pieza FROM presupuesto_multipunto ".
        "WHERE id_cita = ? AND (autoriza_cliente = ? OR pza_garantia = ?) ".
        "ORDER BY id DESC ";

        $query = $this->db->query($sql,array($id_cita,'1','1'));
        $data = $query->result();
        return $data;
    }

    //*********************** Fin .Busqueda de presupustos por estatus y fecha **********************************//

    //Busqueda de presupuestos
    public function busquedaPresupuestoFecha($fecha = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.envia_ventanilla != ? AND o.fecha_recepcion =  ? AND prer.identificador = ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array("0",$fecha,"M"));
        $data = $query->result();
        return $data;
    }

    //Busqueda de presupuestos
    public function busquedaPresupuestoFechas_1($fecha_inicio = "",$fecha_fin = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.envia_ventanilla != ? AND prer.identificador = ? AND o.fecha_recepcion BETWEEN ? AND ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array("0","M",$fecha_inicio,$fecha_fin));
        $data = $query->result();
        return $data;
    }

    //Busqueda de presupuestos
    public function busquedaPresupuestoCampo($campo = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR prer.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR ".
            "c.datos_apellido_materno LIKE ? OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND prer.envia_ventanilla != ? AND prer.identificador = ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',"0","M"));
        $data = $query->result();
        return $data;
    }
   
    //Busqueda de presupuestos
    public function busquedaPresupuestoCampo_Fecha_1($campo = "",$fecha = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR prer.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? ".
            "OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND prer.envia_ventanilla != ? AND prer.identificador = ? AND o.fecha_recepcion =  ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',"M","0",$fecha));
        $data = $query->result();
        return $data;
    }

    //Busqueda de presupuestos
    public function busquedaPresupuestoCampo_Fecha($campo = "",$fecha_inicio = "",$fecha_fin = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR o.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? ".
            "OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND prer.envia_ventanilla != ? AND prer.identificador = ? AND o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',"0","M",$fecha_inicio,$fecha_fin));
        $data = $query->result();
        return $data;
    }

        //Busqueda de presupuestos
    public function busquedaPresupuestoFecha_Tec($fecha = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE o.fecha_recepcion =  ? AND prer.identificador = ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array($fecha,"M"));
        $data = $query->result();
        return $data;
    }

    //Busqueda de presupuestos
    public function busquedaPresupuestoFechas_1_Tec($fecha_inicio = "",$fecha_fin = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.identificador = ? AND o.fecha_recepcion BETWEEN ? AND ? " .
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array("M",$fecha_inicio,$fecha_fin));
        $data = $query->result();
        return $data;
    }

    //Busqueda de presupuestos
    public function busquedaPresupuestoCampo_Tec($campo = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR prer.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? ".
            "OR c.datos_apellido_materno LIKE ? OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND prer.identificador = ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',"M"));
        $data = $query->result();
        return $data;
    }
   
    //Busqueda de presupuestos
    public function busquedaPresupuestoCampo_Fecha_1_Tec($campo = "",$fecha = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR prer.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? ".
            "OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND o.fecha_recepcion =  ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',$fecha));
        $data = $query->result();
        return $data;
    }

    //Busqueda de presupuestos
    public function busquedaPresupuestoCampo_Fecha_Tec($campo = "",$fecha_inicio = "",$fecha_fin = "")
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.*,c.id_tecnico ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE t.nombre LIKE ? OR prer.id_cita LIKE ? OR c.vehiculo_placas LIKE ? OR c.datos_nombres LIKE ? OR c.datos_apellido_paterno LIKE ? OR c.datos_apellido_materno LIKE ? ".
            "OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND o.fecha_recepcion BETWEEN ? AND ? ".
            "ORDER BY prer.fecha_actualiza DESC LIMIT 100";

        $query = $this->db->query($sql,array('%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%','%'.$campo.'%',$fecha_inicio,$fecha_fin));
        $data = $query->result();
        return $data;
    }

/******************************* Termina nuevos procedimientos para los presupuesto *************************************/    

    public function get_table_garantias()
    {
        $result = $this->db->join('cat_estatus_garantia AS cat_g','cat_g.id = g.estatus')
            ->select('g.*,cat_g.estatus AS estatus_actual')
            ->order_by("g.idRegistro", "DESC")
            ->limit(200)
            ->get('garantias AS g')
            ->result();
        return $result;
    }

    public function valet_por_asignar($campo = '')
    {
        $q = $this->db->where('id_cita',$campo)->select('*')->order_by("id", "DESC")->limit(80)->get('servicio_valet')->result();
        return $q;
    }

    public function valet_asignardas($campo = '')
    {
        $q = $this->db->where('id_cita !=',$campo)->select('*')->order_by("id", "DESC")->limit(80)->get('servicio_valet')->result();
        return $q;
    }

    public function buscar_orden($serie = '',$placas = '')
    {
        $q = $this->db->join('citas AS c','c.id_cita = o.id_cita') 
            ->where('o.vehiculo_identificacion',$serie)
            ->or_where('c.vehiculo_numero_serie',$serie)
            ->or_where('c.vehiculo_placas',$placas)
            ->select('o.id_cita')
            ->order_by("o.id", "DESC")
            ->limit(1)
            ->get('ordenservicio AS o');

        if($q->num_rows()==1){
            $respuesta = $q->row()->id_cita;
        }else{
            $respuesta = '0';
        }

        return $respuesta;
    }

    public function buscar_magic($serie = '',$placas = '')
    {
        $q = $this->db->where('serie_larga',$serie)
            ->or_where('placas',$placas)
            ->select('id')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('magic');

        if($q->num_rows()==1){
            $respuesta = $q->row()->id;
        }else{
            $respuesta = '0';
        }

        return $respuesta;
    }

    public function buscar_valetsorden($campo = '')
    {
        $q = $this->db->where('id_cita','0')
            ->like('placas',$campo,'both')
            ->or_like('modelo',$campo,'both')
            ->or_like('serie',$campo,'both')
            ->or_like('marca',$campo,'both')
            ->or_like('correo',$campo,'both')
            ->or_like('cliente',$campo,'both')
            ->select('*')
            ->order_by("id", "DESC")
            ->limit(100)
            ->get('servicio_valet')->result();
        
        return $q;
    }

    public function buscar_valet($campo = '')
    {
        $q = $this->db->like('id_cita',$campo,'both')
            ->or_like('placas',$campo,'both')
            ->or_like('modelo',$campo,'both')
            ->or_like('serie',$campo,'both')
            ->or_like('marca',$campo,'both')
            ->or_like('correo',$campo,'both')
            ->or_like('cliente',$campo,'both')
            ->select('*')
            ->order_by("id", "DESC")
            ->limit(100)
            ->get('servicio_valet')->result();
        
        return $q;
    }

    //Buscador de multipuntos ( lista multipunto) por tecnico
    public function busquedaMultipuntoTec($idTecnico = 0,$id_cita = "")
    {
        $query = $this->db->where("t.id",$idTecnico)
            ->where("mg.orden",$id_cita)
            ->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')
            ->join('citas AS c','c.id_cita = mg.orden')
            ->join('ordenservicio AS o','o.id_cita = c.id_cita')
            ->join('tecnicos AS t','c.id_tecnico = t.id')
            ->select('mg3.comentario,mg3.sistema,mg3.componente,mg3.causaRaiz,mg3.asesor,mg3.nombreCliente,mg.orden,mg.fechaCaptura,o.folioIntelisis,mg3.tecnico')
            ->order_by("o.id", "DESC")
            ->limit(100)
            ->get('multipunto_general AS mg')
            ->result();
        return $query;
    }

    //Buscador de multipuntos ( lista multipunto)
    public function busquedaMultipunto($id_cita = "")
    {
        $query = $this->db->where("mg.orden",$id_cita)
            ->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')
            ->join('ordenservicio AS o','o.id_cita = mg.orden')
            ->select('mg3.comentario,mg3.sistema,mg3.componente,mg3.causaRaiz,mg3.asesor,mg3.nombreCliente,mg.orden,mg.fechaCaptura,o.folioIntelisis,mg3.tecnico')
            ->order_by("o.id", "DESC")
            ->limit(100)
            ->get('multipunto_general AS mg')
            ->result();
        return $query;
    }

    /*********************************************/
    public function ordenes_tec_principal($id_tecnico='',$fecha = '')
    {
        $result = $this->db->where('doc.fecha_recepcion >=', $fecha)
        ->where('c.id_tecnico',$id_tecnico)

        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')

        ->select("doc.id_cita")
        ->order_by("doc.id", "DESC")
        ->get('documentacion_ordenes AS doc');
            
        return $result->result_array();
    }

    public function ordenes_tec_operacion($id_tecnico='',$fecha = '')
    {
        $result = $this->db->group_start()
            ->where('atr.id_tecnico', $id_tecnico)
            ->group_start()
                ->where('doc.fecha_recepcion >=', $fecha)
            ->group_end()
        ->group_end()

        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('articulos_orden AS atr','atr.idorden = o.id')

        ->select("doc.id_cita")
        ->order_by("doc.id", "DESC")
        ->get('documentacion_ordenes AS doc');
            
        return $result->result_array();
    }

    public function doc_tb_orden_tec_cita($id_cita = ''){
        $q = $this->db->where('doc.id_cita',$id_cita)
        ->where('doc.tipo_vita','1')

        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
        ->order_by("doc.id", "DESC")
        ->limit(1)
        ->get('documentacion_ordenes AS doc');
        
        if($q->num_rows()==1){
            $query = $q->result_array();
            $retorno = $query[0];
        }else{
            $retorno = NULL;
        }

        return $retorno;
    }

    public function doc_tb_ordenes_tec($id_tec = '',$fecha = ""){
        $result = $this->db->where('doc.fecha_recepcion >=', $fecha)
        ->where('doc.id_tecnico',$id_tec)
        ->where('doc.tipo_vita','1')

        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
        ->order_by("doc.id", "DESC")
        //->limit(15)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    public function doc_tb_ordenes_mes($fecha = ""){
        $result = $this->db->where('doc.fecha_recepcion >=', $fecha)
            ->where('doc.tipo_vita','1')
        
            ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
            ->join('citas AS c','c.id_cita = doc.id_cita')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            
            ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
            ->order_by("doc.id", "DESC")
            ->limit(100)
            ->get('documentacion_ordenes AS doc')->result();
        //echo $this->db->last_query();die();
        return $result;
    }

    public function doc_tb_ordenes($fecha = ""){
        $result = $this->db->where('doc.fecha_recepcion >', $fecha)
            ->where('doc.tipo_vita','1')
        
            ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
            ->join('citas AS c','c.id_cita = doc.id_cita')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            
            ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
            ->order_by("doc.id", "DESC")
            ->limit(50)
            ->get('documentacion_ordenes AS doc')->result();
        //echo $this->db->last_query();die();
        return $result;
    }

    //Listado de ordenes para documentacion filtro
    public function filtro_doc_fecha($fecha = '')
    {
        $result = $this->db->where('doc.fecha_recepcion', $fecha)
        
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        //->join('estatus AS st','st.id = c.id_status_color')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
        ->order_by("doc.id", "DESC")
        ->limit(100)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    //Listado de ordenes para documentacion filtro
    public function filtro_doc_fecha2($fecha = '',$fecha2 = '')
    {
        $result = $this->db->where('doc.fecha_recepcion >=', $fecha)
        ->where('doc.fecha_recepcion <=', $fecha2)
        
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        //->join('estatus AS st','st.id = c.id_status_color')
        
        ->select('doc.*, lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
        ->order_by("doc.id", "DESC")
        ->limit(100)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    //Listado de ordenes para documentacion filtro
    public function filtro_doc_campo($campo = '')
    {
        $result = $this->db->group_start()
            ->where('doc.tipo_vita','1')
            ->group_start()
                ->like('doc.asesor',$campo,'both')
                ->or_like('doc.tecnico',$campo,'both')
                ->or_like('doc.id_cita',$campo,'both')
                ->or_like('doc.cliente',$campo,'both')
                ->or_like('doc.placas',$campo,'both')
                ->or_like('doc.serie',$campo,'both')
            ->group_end()
        ->group_end()
        
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        //->join('estatus AS st','st.id = c.id_status_color')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
        ->order_by("doc.id", "DESC")
        ->limit(100)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    //Listado de ordenes para documentacion filtro
    public function filtro_doc_campo_fecha1($campo = '',$fecha = '',$fecha2 = '')
    {
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion >=', $fecha)
            ->where('doc.fecha_recepcion <=', $fecha2)
            ->where('doc.tipo_vita','1')
            ->group_start()
                ->like('doc.asesor',$campo,'both')
                ->or_like('doc.tecnico',$campo,'both')
                ->or_like('doc.id_cita',$campo,'both')
                ->or_like('doc.cliente',$campo,'both')
                ->or_like('doc.placas',$campo,'both')
                ->or_like('doc.serie',$campo,'both')
            ->group_end()
        ->group_end()
        
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        //->join('estatus AS st','st.id = c.id_status_color')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
        ->order_by("doc.id", "DESC")
        ->limit(100)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    //Listado de ordenes para documentacion filtro
    public function filtro_doc_campo_fecha($campo = '',$fecha = '')
    {
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion', $fecha)
            ->where('doc.tipo_vita','1')
            ->group_start()
                ->like('doc.asesor',$campo,'both')
                ->or_like('doc.tecnico',$campo,'both')
                ->or_like('doc.id_cita',$campo,'both')
                ->or_like('doc.cliente',$campo,'both')
                ->or_like('doc.placas',$campo,'both')
                ->or_like('doc.serie',$campo,'both')
            ->group_end()
        ->group_end()
        
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        //->join('estatus AS st','st.id = c.id_status_color')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, o.vehiculo_kilometraje')
        ->order_by("doc.id", "DESC")
        ->limit(100)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    public function doc_tb_ordenes_firmas(){
        $sql = "SELECT doc.*, lavado.id_lavador_cambio_estatus, o.autorizacion_ausencia, o.firma_diagnostico, o.firma_vc, carta.id AS id_renuncia, o.firma_protect FROM documentacion_ordenes AS doc ".
            "INNER JOIN ordenservicio AS o ON o.id_cita = doc.id_cita ".
            "LEFT JOIN carta_renuncia AS carta ON carta.id_cita = doc.id_cita ".
            "LEFT JOIN lavado AS lavado ON lavado.id_cita = doc.id_cita ".
            "WHERE o.autorizacion_ausencia = ? AND (o.firma_vc = ? OR o.firma_vc = ? OR o.firma_protect = ?) ".
            "ORDER BY doc.id DESC LIMIT 100";
        $query = $this->db->query($sql,array('1','0','0','0'));
        $data = $query->result();
        return $data;
        //echo $this->db->last_query();die();
    }


    #-----------------------------------------------------------------------
    public function recopilacion_multipuntos()
    {
        $sql = "SELECT t.nombre AS tecnico, c.vehiculo_modelo, c.asesor,c.vehiculo_placas, o.folioIntelisis,c.datos_telefono,o.telefono_movil, o.otro_telefono,c.vehiculo_numero_serie,o.vehiculo_identificacion ,o.fecha_recepcion,o.hora_recepcion,o.id_cita, mg3.nombreCliente, mg.fechaCaptura, mg.bateriaEstado,mg.bateriaCambio,".
        "fi.dNeumaticoFI,fi.dNeumaticoFIcambio,fi.espesoBalataFI,fi.espesorDiscoFI, fi.pneumaticoFIcambio, fi.espesorBalataFIcambio, fi.espesorDiscoFIcambio, ".
        "ti.dNeumaticoTI,ti.dNeumaticoTIcambio,ti.espesoBalataTI,ti.espesorDiscoTI,ti.diametroTamborTI, ti.pneumaticoTIcambio, ti.espesorBalataTIcambio, ti.espesorDiscoTIcambio, ti.diametroTamborTIcambio, ".
        "fd.dNeumaticoFD,fd.dNeumaticoFDcambio,fd.espesoBalataFD,fd.espesorDiscoFD, fd.pneumaticoFDcambio, fd.espesorBalataFDcambio, fd.espesorDiscoFDcambio, ".
        "td.dNeumaticoTD,td.dNeumaticoTDcambio,td.espesoBalataTD,td.espesorDiscoTD,td.diametroTamborTD, td.pneumaticoTDcambio, td.espesorBalataTDcambio, td.espesorDiscoTDcambio, td.diametroTamborTDcambio ".


        "FROM multipunto_general AS mg ".
        "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
        "LEFT JOIN frente_izquierdo AS fi ON fi.idOrden = mg.id ".
        "LEFT JOIN trasero_izquierdo AS ti ON ti.idOrden = mg.id ".
        "LEFT JOIN frente_derecho AS fd ON fd.idOrden = mg.id ".
        "LEFT JOIN trasero_derecho AS td ON td.idOrden = mg.id ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = mg.orden ".
        "INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        //"LEFT JOIN tecnicos_citas AS tc ON tc.id_cita = o.id_cita ".
        "WHERE mg3.firmaTecnico != ? ".

        "ORDER BY o.id DESC LIMIT 10000";
        $query = $this->db->query($sql,array(''));
        $data = $query->result();
        return $data;
    }

    public function recopilacion_multipuntos_fecha($fecha = '')
    {
        $sql = "SELECT t.nombre AS tecnico, c.vehiculo_modelo, c.asesor,c.vehiculo_placas, o.folioIntelisis,c.datos_telefono,o.telefono_movil, o.otro_telefono,c.vehiculo_numero_serie,o.vehiculo_identificacion ,o.fecha_recepcion,o.hora_recepcion,o.id_cita, mg3.nombreCliente, mg.fechaCaptura, mg.bateriaEstado,mg.bateriaCambio,".
        "fi.dNeumaticoFI,fi.dNeumaticoFIcambio,fi.espesoBalataFI,fi.espesorDiscoFI, fi.pneumaticoFIcambio, fi.espesorBalataFIcambio, fi.espesorDiscoFIcambio, ".
        "ti.dNeumaticoTI,ti.dNeumaticoTIcambio,ti.espesoBalataTI,ti.espesorDiscoTI,ti.diametroTamborTI, ti.pneumaticoTIcambio, ti.espesorBalataTIcambio, ti.espesorDiscoTIcambio, ti.diametroTamborTIcambio, ".
        "fd.dNeumaticoFD,fd.dNeumaticoFDcambio,fd.espesoBalataFD,fd.espesorDiscoFD, fd.pneumaticoFDcambio, fd.espesorBalataFDcambio, fd.espesorDiscoFDcambio, ".
        "td.dNeumaticoTD,td.dNeumaticoTDcambio,td.espesoBalataTD,td.espesorDiscoTD,td.diametroTamborTD, td.pneumaticoTDcambio, td.espesorBalataTDcambio, td.espesorDiscoTDcambio, td.diametroTamborTDcambio ".

        "FROM multipunto_general AS mg ".
        "LEFT JOIN multipunto_general_3 AS mg3 ON mg3.idOrden = mg.id ".
        "LEFT JOIN frente_izquierdo AS fi ON fi.idOrden = mg.id ".
        "LEFT JOIN trasero_izquierdo AS ti ON ti.idOrden = mg.id ".
        "LEFT JOIN frente_derecho AS fd ON fd.idOrden = mg.id ".
        "LEFT JOIN trasero_derecho AS td ON td.idOrden = mg.id ".
        "INNER JOIN ordenservicio AS o ON o.id_cita = mg.orden ".
        "INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
        "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
        //"LEFT JOIN tecnicos_citas AS tc ON tc.id_cita = o.id_cita ".
        "WHERE o.fecha_recepcion = ? AND mg3.firmaTecnico != ? ".
        "ORDER BY o.id DESC";
        /*"WHERE o.fecha_recepcion = ? AND (mg.bateriaEstado like ? OR ? OR fi.dNeumaticoFI like ? OR ? OR fi.espesoBalataFI like ? OR  ? OR fi.espesorDiscoFI like ? OR ? ".
        "OR ti.dNeumaticoTI like ? OR ? OR ti.espesoBalataTI like ? OR ? OR ti.espesorDiscoTI like ? OR ? OR ti.diametroTamborTI like ? OR ? ".
        "OR fd.dNeumaticoFD like ? OR ? OR fd.espesoBalataFD like ? OR ? OR fd.espesorDiscoFD like ? OR ? ".
        "OR td.dNeumaticoTD like ? OR ? OR td.espesoBalataTD like ? OR ? OR td.espesorDiscoTD like ? OR ? OR td.diametroTamborTD like ? OR ? )". 

        "ORDER BY o.id DESC";

        $query = $this->db->query($sql,array($fecha,'Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato','Futuro','Inmediato')); */
        $query = $this->db->query($sql,array($fecha,''));
        $data = $query->result();
        return $data;
    }

    //Recuperar informacion de la orden
    public function datos_generales_orden($id_cita = '')
    {
        $sql = "SELECT tc.*, st.nombre, t.nombre AS tecnico, c.vehiculo_modelo, c.asesor,c.vehiculo_placas, o.folioIntelisis,c.datos_telefono,o.telefono_movil, o.otro_telefono,c.vehiculo_numero_serie,o.vehiculo_identificacion ,o.fecha_recepcion,o.hora_recepcion, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, o.nombre_compania ".  
            "FROM ordenservicio AS o ".
            "INNER JOIN citas AS c ON c.id_cita = o.id_cita ".
            "INNER JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "INNER JOIN tecnicos_citas AS tc ON tc.id_cita = o.id_cita ".
            "INNER JOIN estatus AS st ON st.id = c.id_status_color ".
            "INNER JOIN cat_tipo_operacion AS catOp ON catOp.id = o.id_tipo_operacion ".
            "WHERE o.id_cita = ? LIMIT 1";

        $query = $this->db->query($sql,$id_cita);
        $data = $query->result();
        return $data;
    }

    //Recuperamos los indicadores multipunto con la nueva tabla
    public function indicadores_multipunto($fecha_limite)
    {
        $result = $this->db->where("fecha_recepcion >=",$fecha_limite)
        ->select('*')
        ->order_by("id", "DESC")
        ->limit(50)
        ->get('indicadores_multipunto')->result();
        return $result;
    }

    public function indicadores_multipunto_fecha($fecha = "")
    {
        $result = $this->db->like("fecha_recepcion",$fecha,"after")
        ->select('*')
        ->order_by("id", "DESC")
        ->limit(100)
        ->get('indicadores_multipunto')->result();
        return $result;
    }

    public function indicadores_multipunto_fecha2($fecha = "",$fecha_2 = "")
    {
        $result = $this->db->where("fecha_recepcion >=",$fecha)
        ->where("fecha_recepcion <=",$fecha_2)
        ->select('*')
        ->order_by("id", "DESC")
        ->limit(100)
        ->get('indicadores_multipunto')->result();
        return $result;
    }

    public function indicadores_multipunto_campo($campo = "")
    {
        $result = $this->db->like("id_cita",$campo,"both")
        ->or_like('folio',$campo,'both')
        ->or_like('cliente',$campo,'both')
        ->or_like('serie',$campo,'both')
        ->or_like('modelo',$campo,'both')
        ->or_like('placas',$campo,'both')
        ->select('*')
        ->order_by("id", "DESC")
        ->limit(100)
        ->get('indicadores_multipunto')->result();
        return $result;
    }

    public function indicadores_multipunto_campo_fecha2($campo = "",$fecha = "",$fecha_2 = "")
    {
        $result = $this->db->where("fecha_recepcion >=",$fecha)
        ->where("fecha_recepcion <=",$fecha_2)
        ->where("id_cita",$campo)
        ->select('*')
        ->order_by("id", "DESC")
        ->limit(100)
        ->get('indicadores_multipunto')->result();
        return $result;
    }

    public function indicadores_multipunto_campo_fecha($campo = "",$fecha = "")
    {
        $result = $this->db->like("fecha_recepcion",$fecha,"after")
        ->like("id_cita",$campo,"both")
        ->select('*')
        ->order_by("id", "DESC")
        ->limit(100)
        ->get('indicadores_multipunto')->result();
        return $result;
    }

///////////////////______________________ CONSULTAS PSNI _______________________//////////////////////////////7

    public function busquedaClientesRefFecha($fech1 = '',$fech2 = '')
    {
        $sql = "SELECT * FROM refacciones_cliente WHERE fecha_recibe BETWEEN ? AND ? ORDER BY id DESC LIMIT 100";

        $query = $this->db->query($sql,array($fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    /*public function busquedaClientesRefFecha_1($fecha = '')
    {
        $sql = "SELECT * FROM refacciones_cliente WHERE fecha_recibe = ? ORDER BY id DESC LIMIT 100";

        $query = $this->db->query($sql,array($fecha));
        $data = $query->result();
        return $data;
    } */

    public function busquedaClientesRefFecha_1($fecha = ""){
        $result = $this->db->where('ref_c.fecha_recibe',$fecha)
            ->where('ref_c.baja',"0")
            ->join('presupuesto_registro AS pre_reg','pre_reg.id_cita = ref_c.id_cita','left')
            ->select('ref_c.fecha_recibe, ref_c.id, ref_c.id_cita, ref_c.estatus, ref_c.tecnico, ref_c.asesor, ref_c.folio, ref_c.unidad_taller, ref_c.baja, ref_c.serie, ref_c.estatus, ref_c.telefono_principal, ref_c.telefono_secundario, ref_c.modeloVehiculo,pre_reg.acepta_cliente,pre_reg.estado_refacciones')
            ->order_by('ref_c.id', "DESC")
            ->limit(80)
            ->get('refacciones_cliente AS ref_c')->result();
        return $result;
    }

    public function busquedaGarantiasRefFecha($fech1 = '',$fech2 = '')
    {
        $sql = "SELECT * FROM refacciones_garantias WHERE fecha_recibe BETWEEN ? AND ? ORDER BY id DESC LIMIT 100";

        $query = $this->db->query($sql,array($fech1,$fech2));
        $data = $query->result();
        return $data;
    }

    public function busquedaGarantiasRefFecha_1($fecha = '')
    {
        $sql = "SELECT * FROM refacciones_garantias WHERE fecha_recibe = ? ORDER BY id DESC LIMIT 100";

        $query = $this->db->query($sql,array($fecha));
        $data = $query->result();
        return $data;
    }

    public function ultimo_estatus($tabla = '',$id_cita = ''){
        $q = $this->db->where('id_cita',$id_cita)->select('estatus_actual,fecha_fin,cambio_num')->order_by('id', "DESC")->limit(1)->get($tabla);
        if($q->num_rows()==1){
            $respuesta = [$q->row()->estatus_actual,$q->row()->fecha_fin,$q->row()->cambio_num];
        }else{
            $respuesta = ['','0000-00-00 00:00:00','0'];
        }
        return $respuesta;
    }

    //Recuperamos el id del historial proactivo 
    public function last_id_refacciones($tabla = '')
    {
        $q = $this->db->select('id')->order_by("id", "DESC")->limit(1)->get($tabla);
        if($q->num_rows()==1){
            $respuesta = $q->row()->id;
        }else{
            $respuesta = '0';
        }
        return $respuesta;
    }

    public function refacciones_rechazadas_pm($id_cita='')
    {
        $result = $this->db->where('pre_reg.id_cita',$id_cita)
            ->where('pre_reg.acepta_cliente != ','')
            ->join('presupuesto_multipunto AS pre_ref','pre_ref.id_cita = pre_reg.id_cita')
            ->select('pre_ref.id, pre_ref.cantidad, pre_ref.descripcion, pre_ref.num_pieza, pre_ref.pza_garantia, pre_ref.autoriza_cliente, pre_reg.acepta_cliente')
            ->order_by("pre_ref.id", "ASC")
            ->get('presupuesto_registro AS pre_reg')
            ->result();
        return $result;
    }

    public function refacciones_rechazadas_cm($id_cita='')
    {
        $result = $this->db->where('coti.idOrden',$id_cita)
            ->where('coti.aceptoTermino != ','')
            ->select('coti.contenido, coti.aceptoTermino')
            ->order_by("coti.idCotizacion", "ASC")
            ->get('cotizacion_multipunto AS coti')
            ->result();
        return $result;
    }

        public function estado_refacciones($id_cita=''){
        $q = $this->db->where('id_cita',$id_cita)->select('acepta_cliente,estado_refacciones')->get('presupuesto_registro');
        if($q->num_rows()==1){
            $respuesta = [$q->row()->acepta_cliente,$q->row()->estado_refacciones];
        }else{
            $respuesta = ['',''];
        }
        return $respuesta;
    }

    public function lista_clietes_refacciones(){
        $result = $this->db->where('ref_c.baja',"0")
            ->join('presupuesto_registro AS pre_reg','pre_reg.id_cita = ref_c.id_cita','left')
            ->select('ref_c.fecha_recibe, ref_c.id, ref_c.id_cita, ref_c.estatus, ref_c.tecnico, ref_c.asesor, ref_c.folio, ref_c.unidad_taller, ref_c.baja, ref_c.serie, ref_c.estatus, ref_c.telefono_principal, ref_c.telefono_secundario, pre_reg.acepta_cliente,pre_reg.estado_refacciones')
            ->order_by('ref_c.id', "DESC")
            ->limit(80)
            ->get('refacciones_cliente AS ref_c')->result();
        return $result;
    }

    public function id_cita_spni_clientes($serie=''){
        $q = $this->db->where('serie',$serie)->select('id_cita')->order_by("id", "DESC")->limit(1)->get('refacciones_cliente');
        if($q->num_rows()==1){
            $respuesta = $q->row()->id_cita;
        }else{
            $respuesta = '0';
        }
        return $respuesta;
    }

///////////////////______________________ CONSULTAS PSNI _______________________//////////////////////////////7

    public function buscar_cartarenuncia($campo = '')
    {
        $q = $this->db->like('id_cita',$campo)
            ->or_like('placas',$campo,'both')
            ->or_like('serie',$campo,'both')
            ->or_like('unidad',$campo,'both')
            ->or_like('nombre_cliente',$campo,'both')
            ->or_like('nombre_asesor',$campo,'both')
            ->or_like('tecnico',$campo,'both')
            ->select('*')
            ->order_by("id", "DESC")
            ->limit(100)
            ->get('carta_renuncia')->result();
        
        return $q;
    }

    public function servicio_orden($id_cita = '')
    {
        $query = $this->db->where('o.id_cita',$id_cita)
            ->join('citas AS c','c.id_cita = o.id_cita')
            ->join('estatus AS st','st.id = c.id_status_color')
            ->select('st.nombre')
            ->limit(1)
            ->get('ordenservicio AS o');

        if($query->num_rows()==1){
          $servicio = $query->row()->nombre;
        }else{
          $servicio = '';
        }

        return $servicio;
    }

    public function monto_servicio($id_cita = '')
    {
        $query = $this->db->where('o.id_cita',$id_cita)
            ->join('citas AS c','c.id_cita = o.id_cita')
            //->join('estatus AS st','st.id = c.id_status_color')
            ->select('o.total_orden')
            ->limit(1)
            ->get('ordenservicio AS o');

        if($query->num_rows()==1){
          $servicio = $query->row()->total_orden;
        }else{
          $servicio = '0';
        }

        return $servicio;
    }

    //Presupuestos de ventanilla
    public function presupuesto_graficas()
    {
       $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas, prer.serie, prer.id_cita, prer.folio_externo, prer.fecha_actualiza, prer.acepta_cliente, prer.estado_refacciones, prer.ubicacion_proceso ".  
            "FROM presupuesto_registro AS prer ".
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita ".
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita ".
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id ".
            "WHERE prer.identificador = ?  ". //AND o.fecha_recepcion > ?
            "ORDER BY prer.fecha_actualiza DESC LIMIT 1000";

        $query = $this->db->query($sql,array('M'));
        $data = $query->result();
        return $data;
    }

    //Nueva voz cliente
    public function total_diag_vc($id_cita = "")
    {
        $num_results = $this->db->select('id')
        ->from('vc_diagnostico')
        ->where('id_cita',$id_cita)
        ->count_all_results();

        return $num_results;
    }

    public function lista_vc($fecha = "")
    {
        $query = $this->db->where("o.fecha_recepcion >", $fecha)
            ->where("doc.voz_cliente", "1")

            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
            ->join('citas AS c','c.id_cita = o.id_cita')
            ->join('voz_cliente AS vc_a','vc_a.idPivote = o.id_cita','left')
            ->join('vc_registro AS vc','vc.id_cita = o.id_cita','left')

            ->select('doc.vehiculo, doc.placas, doc.serie, doc.asesor, doc.tecnico, doc.cliente, vc_a.idRegistro as id_vc_a, vc_a.definicionFalla, vc_a.evidencia, o.fecha_recepcion, o.hora_recepcion, o.id_cita, vc.id as id_vc, vc.audio, vc.n_diagnosticos ')
            
            ->order_by("o.id", "DESC")
            ->limit(30)
            ->get('documentacion_ordenes AS doc')
            ->result();
        return $query;
    }

    public function busqueda_vc($campo = '')
    {
        $query = $this->db->group_start()
                ->where("doc.voz_cliente", "1")
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                    ->or_like('c.vehiculo_modelo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
            ->join('citas AS c','c.id_cita = o.id_cita')
            ->join('voz_cliente AS vc_a','vc_a.idPivote = o.id_cita','left')
            ->join('vc_registro AS vc','vc.id_cita = o.id_cita','left')

            ->select('doc.vehiculo, doc.placas, doc.serie, doc.asesor, doc.tecnico, doc.cliente, vc_a.idRegistro as id_vc_a, vc_a.definicionFalla, vc_a.evidencia, o.fecha_recepcion, o.hora_recepcion, o.id_cita, vc.id as id_vc, vc.audio, vc.n_diagnosticos, doc.folio_externo ')
            
            ->order_by("o.id", "DESC")
            ->limit(200)
            ->get('documentacion_ordenes AS doc')
            ->result();
        return $query;
    }

    public function tipo_orde_asociada($id_cita=''){
        $q = $this->db->where('o.id_cita',$id_cita)
            ->join('ordenservicio AS o','o.id_tipo_orden = cto.id')
            ->select('cto.identificador')
            ->limit(1)
            ->get('cat_tipo_orden AS cto');

        if($q->num_rows()==1){
            $retorno = $q->row()->identificador;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    // Validar existencia de archivos
    public function existencia_diagnostico($id_cita=''){
        $q = $this->db->where('noServicio',$id_cita)->select('idDiagnostico')->order_by('idDiagnostico','DESC')->limit(1)->get('diagnostico');
        if($q->num_rows()==1){
            $retorno = $q->row()->idDiagnostico;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function existencia_multipunto($id_cita=''){
        $q = $this->db->where('orden',$id_cita)->select('id')->order_by('id','DESC')->limit(1)->get('multipunto_general');
        if($q->num_rows()==1){
            $retorno = $q->row()->id;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function existencia_vozcliente($id_cita=''){
        $q = $this->db->where('idPivote',$id_cita)->select('idRegistro')->order_by('idRegistro','DESC')->limit(1)->get('voz_cliente');
        if($q->num_rows()==1){
            $retorno = $q->row()->id;
        }else{
            $retorno = '';
        }
        return $retorno;
    }

    public function existencia_vozcliente_new($id_cita=''){
        $q = $this->db->where('id_cita',$id_cita)->select('id')->order_by('id','DESC')->limit(1)->get('vc_registro');
        if($q->num_rows()==1){
            $retorno = $q->row()->id;
        }else{
            $retorno = '';
        }
        return $retorno;
    }


    // ---------------------------------------------------------------------
    // ----------------------- Api Garantias -------------------------------
    // ---------------------------------------------------------------------
    public function firmas_req_firmas($id_cita = '')
    {
        $query = $this->db->where('id_cita',$id_cita)
            ->select('termina_requisicion_garantias,termina_requisicion')
            ->limit(1)
            ->get('presupuesto_registro');

        if($query->num_rows()==1){
          $fecha = [$query->row()->termina_requisicion_garantias,$query->row()->termina_requisicion];
        }else{
          $fecha = [NULL,NULL];
        }

        return $fecha;
    }

    public function publica_asociada($id_cita = '')
    {
        $query = $this->db->where('orden_asociada',$id_cita)
            ->select('id_cita,folio_asociado')
            ->limit(1)
            ->get('citas');

        if($query->num_rows()==1){
          //$servicio = [$query->row()->id_cita,$query->row()->folio_asociado];
          $servicio = $query->row()->id_cita;
        }else{
          $servicio = NULL;
        }

        return $servicio;
    }

    public function folio_garantia($folio = '', $id_cita = "")
    {
        $query = $this->db->where('c.folio_asociado',$folio)
            ->where('c.folio_asociado !=',NULL)
            ->where('o.id_cita !=',$id_cita)
            ->join('ordenservicio AS o','o.id_cita = c.id_cita')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('o.id_cita, cto.identificador')
            ->get('citas AS c');

        return $query->result_array();
    }

    public function recuperar_evidencia_presupuesto($id_cita='')
    {
        $q = $this->db->where('id_cita',$id_cita)
            ->select('archivos_presupuesto, archivo_tecnico, archivo_jdt')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('presupuesto_registro');

        if($q->num_rows()==1){
            $retorno = array("archivos" => $q->row()->archivos_presupuesto."|".$q->row()->archivo_tecnico."|".$q->row()->archivo_jdt);
        }else{
            $retorno = array("archivos" => "");
        }

        return $retorno;
    }

    public function recuperar_evidencia_hm($id_cita=''){
        $q = $this->db->where('mg.orden',$id_cita)
        ->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')
        ->join('presupuesto_registro AS pr','pr.id_cita = mg.orden','left')

        ->select('mg3.archivo, pr.archivos_presupuesto, pr.archivo_tecnico, pr.archivo_jdt')
        ->order_by("mg.id", "DESC")
        ->limit(1)
        ->get('multipunto_general AS mg');

        if($q->num_rows()==1){
            //$respuesta = $q->row()->archivo;
            $retorno = array("archivos" => $q->row()->archivo."|".$q->row()->archivos_presupuesto."|".$q->row()->archivo_tecnico."|".$q->row()->archivo_jdt);
        }else{
            //$respuesta = '';
            $retorno = array("archivos" => "");
        }
        return $retorno;
    }


    public function revisar_presupuesto($id_cita='')
    {
        $q = $this->db->where('id_cita',$id_cita)
            ->select('id,acepta_cliente,firma_requisicion')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('presupuesto_registro');

        if($q->num_rows()==1){
            $query = $q->result_array();
            $retorno = $query[0];
        }else{
            $retorno = array("id" => '0', "acepta_cliente" => "", "firma_requisicion" => "");
        }

        return $retorno;
    }

    public function revisar_requisicion($id_cita='')
    {
        $q = $this->db->where('id_cita',$id_cita)
            ->where('ref_garantias >','0')
            ->select('firma_requisicion')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('presupuesto_registro');

        if($q->num_rows()==1){
            if ($q->row()->firma_requisicion != "") {
                $retorno = 1;
            } else {
                $retorno = 2;
            }
        }else{
            $retorno = 0;
        }

        return $retorno;
    }

    public function verificar_orden_garantia($id_cita = '') 
    {
       /* $query = $this->db->where('o.id_cita', $id_cita)
            ->where('o.id_tipo_orden', '2')
                    
            ->join('presupuesto_registro AS pr','pr.id_cita = doc.id_cita','left')
            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')

            ->select('o.id_tipo_orden')
            ->order_by("doc.id", "DESC")
            ->limit(1)
            ->get('documentacion_ordenes AS doc');

        if($query->num_rows()==1){
            $tipo_orden = TRUE;
        }else{
            $tipo_orden = FALSE;
        }

        return $tipo_orden;
        */
        $query = $this->db->where('o.id_cita', $id_cita)
            ->where('o.id_tipo_orden', '2')
                    
            ->join('presupuesto_registro AS pr','pr.id_cita = o.id_cita','left')
            ->join('documentacion_ordenes AS doc','o.id_cita = doc.id_cita','left')

            ->select('o.id_tipo_orden')
            ->order_by("o.id", "DESC")
            ->limit(1)
            ->get('ordenservicio AS o');

        if($query->num_rows()==1){
            $tipo_orden = TRUE;
        }else{
            $tipo_orden = FALSE;
        }

        return $tipo_orden;
    }

    public function info_garantia($id_cita='')
    {
        $query = $this->db->where('o.id_cita',$id_cita)
            ->join('citas AS c','c.id_cita = o.id_cita')

            ->join('tecnicos AS t','c.id_tecnico = t.id','left')
            ->join('operadores AS op','c.asesor = op.nombre','left')

            //->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden')
            ->join('diagnostico AS diag','diag.noServicio = o.id_cita','left')
            ->join('categorias AS cat','cat.id = o.idcategoria','left')
            ->join('subcategorias AS subcat','subcat.id = o.idsubcategoria','left')

            ->select('o.vehiculo_kilometraje AS km, o.hora_recepcion, o.fecha_recepcion, o.hora_entrega, o.fecha_entrega, 
                c.vehiculo_placas, c.vehiculo_anio, c.vehiculo_modelo, o.numero_interno AS notorre, o.color,o.telefono_movil,'.
                ' o.otro_telefono, o.telefono_asesor, o.nombre_compania, o.calle ,o.nointerior ,o.noexterior ,o.colonia, o.cp ,o.municipio ,o.estado , c.vehiculo_numero_serie, o.vehiculo_identificacion, '. 
                't.nombre AS tecnico, t.idStars AS tidStars, op.idStars AS aidStars, c.asesor, '.
                'cat.categoria, subcat.subcategoria, c.datos_telefono ,c.datos_nombres ,c.datos_apellido_paterno ,c.datos_apellido_materno, diag.archivoOasis AS oasis , c.orden_asociada, o.inicio_vigencia, o.folioIntelisis')
            ->get('ordenservicio AS o')
            ->result();
        return $query;
    }

    public function ref_garantias($id_cita='')
    {
        $query = $this->db->where('pg.id_cita',$id_cita)
            ->where('pg.envio_garantia','1')
            ->join('presupuesto_multipunto AS pm','pm.id_cita = pg.id_cita')
            ->select('pm.pza_garantia, pm.num_pieza, pm.descripcion, pm.cantidad, pm.ref, pm.costo_pieza, pm.horas_mo_garantias, pm.costo_mo_garantias, pm.costo_ford, pm.prefijo, pm.sufijo, pm.basico, pm.id')
            ->order_by("pm.id", "DESC")
            ->get('presupuesto_registro AS pg')
            ->result();
        return $query;
    }

    public function presupuesto_piezas($id_cita='')
    {
        $query = $this->db->where('pg.id_cita',$id_cita)
            ->where('pg.ref_garantias >=', '0')
            ->join('presupuesto_multipunto AS pm','pm.id_cita = pg.id_cita')
            ->select('pm.pza_garantia, pm.num_pieza, pm.descripcion, pm.cantidad, pm.ref, pm.costo_pieza, pm.horas_mo, pm.costo_mo, pm.horas_mo_garantias, pm.costo_ford, pm.costo_mo_garantias, pm.pza_garantia, pm.autoriza_garantia, pg.envio_garantia, pm.activo, pm.prefijo, pm.sufijo, pm.basico, pm.id')
            ->order_by("pm.id", "DESC")
            ->get('presupuesto_registro AS pg')
            ->result();
        return $query;
    }

    public function cotizaciones_garantias()
    {
        $query = $this->db->where('prer.ref_garantias !=','0')
            ->where('prer.envio_jdt','1')

            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            ->join('documentacion_ordenes AS doc','doc.id_cita = o.id_cita')
            //->join('citas AS c','c.id_cita = o.id_cita')
            //->join('tecnicos AS t','t.id = c.id_tecnico')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            //->select('t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas, c.datos_nombres ,c.datos_apellido_paterno ,c.datos_apellido_materno, c.vehiculo_numero_serie, o.vehiculo_identificacion, prer.estado_refacciones, prer.acepta_cliente, prer.id_cita, prer.folio_externo, prer.envio_garantia, prer.serie, cto.identificador,doc.oasis, prer.firma_requisicion')
            ->select('doc.tecnico, doc.vehiculo, doc.asesor, doc.placas, doc.cliente, doc.serie, prer.estado_refacciones, prer.acepta_cliente, prer.id_cita, prer.folio_externo, prer.envio_garantia, cto.identificador,doc.oasis, doc.multipunto, prer.firma_requisicion, doc.archivo_poliza')
            ->order_by("prer.fecha_actualiza", "DESC")
            ->limit(60)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $query;
    }

    public function cotizaciones_garantias_campo($campo = "")
    {
        $query = $this->db->group_start()
                ->where('prer.envia_ventanilla','1')
                ->group_start()
                    ->like('c.asesor',$campo,'both')
                    ->or_like('t.nombre',$campo,'both')
                    ->or_like('prer.id_cita',$campo,'both')
                    ->or_like('c.vehiculo_placas',$campo,'both')
                    ->or_like('c.vehiculo_numero_serie',$campo,'both')
                    ->or_like('prer.serie',$campo,'both')
                    ->or_like('prer.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            ->join('documentacion_ordenes AS doc','doc.id_cita = o.id_cita')
            ->join('citas AS c','c.id_cita = o.id_cita')
            ->join('tecnicos AS t','t.id = c.id_tecnico')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            
            ->select('t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas, c.datos_nombres ,c.datos_apellido_paterno ,c.datos_apellido_materno, c.vehiculo_numero_serie, o.vehiculo_identificacion, prer.estado_refacciones, prer.acepta_cliente, prer.id_cita, prer.folio_externo, prer.envio_garantia, prer.serie, prer.ref_garantias, cto.identificador, doc.oasis, prer.firma_requisicion, doc.multipunto, doc.archivo_poliza')
            ->order_by("prer.fecha_actualiza", "DESC")
            ->limit(100)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $query;
    }

    public function cotizaciones_garantias_fecha($fecha = '')
    {
        $result = $this->db->where('prer.envia_ventanilla','1')
            ->where('o.fecha_recepcion', $fecha)

            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            ->join('documentacion_ordenes AS doc','doc.id_cita = o.id_cita')
            ->join('citas AS c','c.id_cita = o.id_cita')
            ->join('tecnicos AS t','t.id = c.id_tecnico')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            
            ->select('t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas, c.datos_nombres ,c.datos_apellido_paterno ,c.datos_apellido_materno, c.vehiculo_numero_serie, o.vehiculo_identificacion, prer.estado_refacciones, prer.acepta_cliente, prer.id_cita, prer.folio_externo, prer.envio_garantia, prer.serie, prer.ref_garantias, cto.identificador, doc.oasis, prer.firma_requisicion, doc.multipunto, doc.archivo_poliza')

            ->order_by("prer.fecha_actualiza", "DESC")
            ->limit(100)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function cotizaciones_garantias_fecha1($fecha = '',$fecha2 = "")
    {
        $result = $this->db->where('prer.envia_ventanilla','1')
            ->where('o.fecha_recepcion >=', $fecha)
            ->where('o.fecha_recepcion <=', $fecha2)

            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            ->join('documentacion_ordenes AS doc','doc.id_cita = o.id_cita')
            ->join('citas AS c','c.id_cita = o.id_cita')
            ->join('tecnicos AS t','t.id = c.id_tecnico')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            
            ->select('t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas, c.datos_nombres ,c.datos_apellido_paterno ,c.datos_apellido_materno, c.vehiculo_numero_serie, o.vehiculo_identificacion, prer.estado_refacciones, prer.acepta_cliente, prer.id_cita, prer.folio_externo, prer.envio_garantia, prer.serie, prer.ref_garantias, cto.identificador, doc.oasis, prer.firma_requisicion, doc.multipunto, doc.archivo_poliza')

            ->order_by("prer.fecha_actualiza", "DESC")
            ->limit(100)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function cotizaciones_garantias_campofecha($campo = "",$fecha = "")
    {
        $query = $this->db->group_start()
                ->where('prer.envia_ventanilla','1')
                ->where('o.fecha_recepcion', $fecha)
                ->group_start()
                    ->like('c.asesor',$campo,'both')
                    ->or_like('t.nombre',$campo,'both')
                    ->or_like('prer.id_cita',$campo,'both')
                    ->or_like('c.vehiculo_placas',$campo,'both')
                    ->or_like('c.vehiculo_numero_serie',$campo,'both')
                    ->or_like('prer.serie',$campo,'both')
                    ->or_like('prer.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            ->join('documentacion_ordenes AS doc','doc.id_cita = o.id_cita')
            ->join('citas AS c','c.id_cita = o.id_cita')
            ->join('tecnicos AS t','t.id = c.id_tecnico')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            
            ->select('t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas, c.datos_nombres ,c.datos_apellido_paterno ,c.datos_apellido_materno, c.vehiculo_numero_serie, o.vehiculo_identificacion, prer.estado_refacciones, prer.acepta_cliente, prer.id_cita, prer.folio_externo, prer.envio_garantia, prer.serie, prer.ref_garantias, cto.identificador, doc.oasis, prer.firma_requisicion, doc.multipunto, doc.archivo_poliza')
            ->order_by("prer.fecha_actualiza", "DESC")
            ->limit(100)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $query;
    }

    public function cotizaciones_garantias_campofecha1($campo = "",$fecha = '',$fecha2 = "")
    {
        $query = $this->db->group_start()
                ->where('prer.envia_ventanilla','1')
                ->where('o.fecha_recepcion >=', $fecha)
                ->where('o.fecha_recepcion <=', $fecha2)
                ->group_start()
                    ->like('c.asesor',$campo,'both')
                    ->or_like('t.nombre',$campo,'both')
                    ->or_like('prer.id_cita',$campo,'both')
                    ->or_like('c.vehiculo_placas',$campo,'both')
                    ->or_like('c.vehiculo_numero_serie',$campo,'both')
                    ->or_like('prer.serie',$campo,'both')
                    ->or_like('prer.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            ->join('documentacion_ordenes AS doc','doc.id_cita = o.id_cita')
            ->join('citas AS c','c.id_cita = o.id_cita')
            ->join('tecnicos AS t','t.id = c.id_tecnico')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            
            ->select('t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas, c.datos_nombres ,c.datos_apellido_paterno ,c.datos_apellido_materno, c.vehiculo_numero_serie, o.vehiculo_identificacion, prer.estado_refacciones, prer.acepta_cliente, prer.id_cita, prer.folio_externo, prer.envio_garantia, prer.serie, prer.ref_garantias, cto.identificador, doc.oasis, prer.firma_requisicion, doc.multipunto, doc.archivo_poliza')
            ->order_by("prer.fecha_actualiza", "DESC")
            ->limit(100)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $query;
    }

    public function garantias_doc($fecha = '')
    {
        $result = $this->db->where('doc.fecha_recepcion >=', $fecha)
            ->where('o.id_tipo_orden', '2')

            ->join('presupuesto_registro AS pr','pr.id_cita = doc.id_cita','left')
            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
            ->join('citas AS c','c.id_cita = doc.id_cita')
            ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.*,lavado.id_lavador_cambio_estatus, o.vehiculo_identificacion, c.orden_asociada, o.id_tipo_orden, cto.identificador, c.folio_asociado, pr.firma_requisicion, o.folioIntelisis, pr.archivos_presupuesto, pr.archivo_tecnico, pr.archivo_jdt')
            ->order_by("doc.id", "DESC")
            //->limit(500)
            ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    public function garantias_doc_asociado($id_cita = "")
    {
        $result = $this->db->where('doc.id_cita', $id_cita)

            ->join('presupuesto_registro AS pr','pr.id_cita = doc.id_cita','left')
            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
            ->join('citas AS c','c.id_cita = doc.id_cita')
            ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.*,lavado.id_lavador_cambio_estatus, o.vehiculo_identificacion, c.orden_asociada, o.id_tipo_orden, cto.identificador , c.folio_asociado, o.folioIntelisis')
            ->order_by("doc.id", "DESC")
            ->limit(1)
            ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    //Listado de ordenes para documentacion filtro
    public function garantias_doc_campo($campo = '')
    {
        $result = $this->db->group_start()
            ->where('o.id_tipo_orden', '2')
            //->or_where('pr.ref_garantias >', '0')
            ->group_start()
                ->like('doc.asesor',$campo,'both')
                ->or_like('doc.tecnico',$campo,'both')
                ->or_like('doc.id_cita',$campo,'both')
                ->or_like('doc.cliente',$campo,'both')
                ->or_like('doc.placas',$campo,'both')
                ->or_like('doc.serie',$campo,'both')
                ->or_like('doc.folio_externo',$campo,'both')
            ->group_end()
        ->group_end()

        ->join('presupuesto_registro AS pr','pr.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')


        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.vehiculo_identificacion, pr.ref_garantias, c.orden_asociada, o.id_tipo_orden , cto.identificador , c.folio_asociado, pr.firma_requisicion, o.folioIntelisis, pr.archivos_presupuesto, pr.archivo_tecnico, pr.archivo_jdt')
        ->order_by("doc.id", "DESC")
        ->limit(150)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
        //echo $this->db->last_query();die();
    }

    //Listado de ordenes para documentacion filtro
    public function garantias_doc_fecha($fecha = '')
    {
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion', $fecha)
            ->where('o.id_tipo_orden', '2')
            //->or_where('pr.ref_garantias >', '0')
        ->group_end()

        ->join('presupuesto_registro AS pr','pr.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.vehiculo_identificacion, pr.ref_garantias, c.orden_asociada, o.id_tipo_orden , cto.identificador , c.folio_asociado, pr.firma_requisicion, o.folioIntelisis, pr.archivos_presupuesto, pr.archivo_tecnico, pr.archivo_jdt')
        ->order_by("doc.id", "DESC")
        ->limit(150)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    public function garantias_doc_fecha2($fecha = '',$fecha2 = '')
    {
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion >=', $fecha)
            ->where('doc.fecha_recepcion <=', $fecha2)
            ->where('o.id_tipo_orden', '2')
            //->or_where('pr.ref_garantias >', '0')
        ->group_end()
        
        ->join('presupuesto_registro AS pr','pr.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.vehiculo_identificacion, pr.ref_garantias, c.orden_asociada, o.id_tipo_orden , cto.identificador , c.folio_asociado, pr.firma_requisicion, o.folioIntelisis, pr.archivos_presupuesto, pr.archivo_tecnico, pr.archivo_jdt')
        ->order_by("doc.id", "DESC")
        ->limit(150)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    //Listado de ordenes para documentacion filtro
    public function garantias_doc_campo_fecha1($campo = '',$fecha = '',$fecha2 = '')
    {
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion >=', $fecha)
            ->where('doc.fecha_recepcion <=', $fecha2)
            ->where('o.id_tipo_orden', '2')
            //->or_where('pr.ref_garantias >', '0')
            ->group_start()
                ->like('doc.asesor',$campo,'both')
                ->or_like('doc.tecnico',$campo,'both')
                ->or_like('doc.id_cita',$campo,'both')
                ->or_like('doc.cliente',$campo,'both')
                ->or_like('doc.placas',$campo,'both')
                ->or_like('doc.serie',$campo,'both')
            ->group_end()
        ->group_end()

        ->join('presupuesto_registro AS pr','pr.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.vehiculo_identificacion, pr.ref_garantias, c.orden_asociada, o.id_tipo_orden , cto.identificador , c.folio_asociado, pr.firma_requisicion, o.folioIntelisis, pr.archivos_presupuesto, pr.archivo_tecnico, pr.archivo_jdt')
        ->order_by("doc.id", "DESC")
        ->limit(150)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    //Listado de ordenes para documentacion filtro
    public function garantias_doc_campo_fecha($campo = '',$fecha = '')
    {
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion', $fecha)
            ->where('o.id_tipo_orden', '2')
            //->or_where('pr.ref_garantias >', '0')
            ->group_start()
                ->like('doc.asesor',$campo,'both')
                ->or_like('doc.tecnico',$campo,'both')
                ->or_like('doc.id_cita',$campo,'both')
                ->or_like('doc.cliente',$campo,'both')
                ->or_like('doc.placas',$campo,'both')
                ->or_like('doc.serie',$campo,'both')
            ->group_end()
        ->group_end()

        ->join('presupuesto_registro AS pr','pr.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.vehiculo_identificacion, pr.ref_garantias, c.orden_asociada, o.id_tipo_orden , cto.identificador , c.folio_asociado, pr.firma_requisicion, o.folioIntelisis, pr.archivos_presupuesto, pr.archivo_tecnico, pr.archivo_jdt')
        ->order_by("doc.id", "DESC")
        ->limit(150)
        ->get('documentacion_ordenes AS doc')->result();
        return $result;
    }

    // ---------------------------------------------------------------------
    // ----------------------- Api Garantias -------------------------------
    // ---------------------------------------------------------------------./
    
/***************************************************************************************************************/
/***************************************************************************************************************/ 
    public function presupuestosGarantias_table($fecha = '')
    {
        $result = $this->db->where('prer.envio_jdt !=','0')
            ->where('prer.ref_garantias >','0')
            ->where('prer.identificador','M')
            ->where('doc.fecha_recepcion >', $fecha)

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor,prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt,prer.serie, doc.fecha_recepcion, doc.folio_externo, prer.estado_refacciones, prer.envio_garantia, prer.firma_requisicion, prer.firma_requisicion_garantias')
            ->order_by('prer.termina_ventanilla', 'DESC')
            ->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosGarantias_table_campo($campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envio_jdt !=','0')
                ->where('prer.ref_garantias >','0')
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion, doc.folio_externo, prer.envio_garantia, prer.firma_requisicion, prer.firma_requisicion_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosGarantias_tabl_fecha_2($fecha_1 = "", $fecha_2 = "",$campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envio_jdt !=','0')
                ->where('prer.ref_garantias >','0')
                ->where('doc.fecha_recepcion >=', $fecha_1)
                ->where('doc.fecha_recepcion <=', $fecha_2)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion, doc.folio_externo, prer.envio_garantia, prer.firma_requisicion, prer.firma_requisicion_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosGarantias_tabl_fecha($fecha_1 = "",$campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envio_jdt !=','0')
                ->where('prer.ref_garantias >','0')
                ->where('doc.fecha_recepcion', $fecha_1)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion, doc.folio_externo, prer.envio_garantia, prer.firma_requisicion, prer.firma_requisicion_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosJDT_table($fecha = '')
    {
        $result = $this->db->where('prer.envia_ventanilla !=','0')
            ->where('prer.identificador','M')
            ->where('doc.fecha_recepcion >', $fecha)

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor,prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt,prer.serie, doc.fecha_recepcion, doc.folio_externo, prer.estado_refacciones')
            ->order_by('prer.termina_ventanilla', 'DESC')
            ->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosjdt_table_campo($campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envia_ventanilla !=','0')
                ->where('prer.identificador','M')
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosjdt_tabl_fecha_2($fecha_1 = "", $fecha_2 = "",$campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envia_ventanilla !=','0')
                ->where('doc.fecha_recepcion >=', $fecha_1)
                ->where('doc.fecha_recepcion <=', $fecha_2)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosjdt_tabl_fecha($fecha_1 = "",$campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envia_ventanilla !=','0')
                ->where('doc.fecha_recepcion', $fecha_1)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosAsesor_table($fecha = '')
    {
        $result = $this->db->where('prer.envio_jdt','1')
            ->where('prer.identificador','M')
            ->where('doc.fecha_recepcion >', $fecha)

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')
            //->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            //->join('citas AS c ','c.id_cita = o.id_cita')
            //->join('tecnicos AS t ','c.id_tecnico = t.id')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            //->order_by('prer.fecha_actualiza', 'DESC')
            ->order_by('prer.termina_jdt', 'DESC')
            ->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosAsesor_table_campo($campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envio_jdt','1')
                ->where('prer.identificador','M')
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosAsesor_tabl_fecha_2($fecha_1 = "", $fecha_2 = "",$campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envio_jdt','1')
                ->where('doc.fecha_recepcion >=', $fecha_1)
                ->where('doc.fecha_recepcion <=', $fecha_2)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosAsesor_tabl_fecha($fecha_1 = "",$campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.envio_jdt','1')
                ->where('doc.fecha_recepcion', $fecha_1)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosJDTStatus_table($fecha = '')
    {
        $result = $this->db->where('prer.acepta_cliente !=','')
            ->where('prer.identificador','M')
            ->where('doc.fecha_recepcion >', $fecha)

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->order_by('prer.termina_jdt', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosJDTStatus_table_f($fecha = '')
    {
        $result = $this->db->where('prer.acepta_cliente !=','')
            ->where('prer.identificador','M')
            ->where('doc.fecha_recepcion', $fecha)

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->order_by('prer.termina_jdt', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosJDTStatus_table_campo($campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.acepta_cliente !=','')
                ->where('prer.identificador','M')
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosJDTStatus_tabl_fecha_2($fecha_1 = "", $fecha_2 = "",$campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.acepta_cliente !=','')
                ->where('doc.fecha_recepcion >=', $fecha_1)
                ->where('doc.fecha_recepcion <=', $fecha_2)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosJDTStatus_tabl_fecha($fecha_1 = "", $fecha_2 = "")
    {
        $result = $this->db->group_start()
                ->where('prer.acepta_cliente !=','')
                ->where('doc.fecha_recepcion >=', $fecha_1)
                ->where('doc.fecha_recepcion <=', $fecha_2)
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosJDTStatus_tabl_fechacampo($fecha_1 = "",$campo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.acepta_cliente !=','')
                ->where('doc.fecha_recepcion', $fecha_1)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosCreados_table($fecha = '')
    {
        $result = $this->db->where('prer.identificador','M')
            ->where('doc.fecha_recepcion >', $fecha)

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, doc.fecha_recepcion,doc.folio_externo, prer.firma_requisicion')
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosCreados_tec_table($id_tecnico = '',$fecha = '')
    {
        $result = $this->db->where('prer.identificador','M')
            ->where('doc.id_tecnico',$id_tecnico)
            ->where('doc.fecha_recepcion >', $fecha)

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, doc.fecha_recepcion,doc.folio_externo, prer.firma_requisicion')
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosTec_table_campo($campo = "")
    {
        $result = $this->db->group_start()
                //->where('prer.envio_jdt','1')
                ->where('prer.identificador','M')
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.ubicacion_proceso, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo, prer.firma_requisicion')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosTec_tabl_fecha_2($fecha_1 = "", $fecha_2 = "",$campo = "")
    {
        $result = $this->db->group_start()
                //->where('prer.envio_jdt','1')
                ->where('doc.fecha_recepcion >=', $fecha_1)
                ->where('doc.fecha_recepcion <=', $fecha_2)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.ubicacion_proceso, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo, prer.firma_requisicion')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosTec_tabl_fecha($fecha_1 = "",$campo = "")
    {
        $result = $this->db->group_start()
                //->where('prer.envio_jdt','1')
                ->where('doc.fecha_recepcion', $fecha_1)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.ubicacion_proceso, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion,doc.folio_externo, prer.firma_requisicion')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosCreados_ventanilla($fecha = '')
    {
        $result = $this->db->where('prer.identificador','M')
            ->where('doc.fecha_recepcion >', $fecha)

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.envia_ventanilla, prer.estado_refacciones, prer.firma_requisicion, prer.solicita_psni, prer.ref_garantias, doc.fecha_recepcion,doc.folio_externo, prer.anexa_garantias')
            ->order_by('prer.fecha_actualiza', 'DESC')
            ->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosVentanilla_table_campo($campo = "")
    {
        $result = $this->db->group_start()
                //->where('prer.envio_jdt','1')
                ->where('prer.identificador','M')
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.envia_ventanilla, prer.estado_refacciones, prer.firma_requisicion, prer.solicita_psni, prer.ref_garantias, doc.fecha_recepcion,doc.folio_externo, prer.anexa_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosVentanilla_tabl_fecha_2($fecha_1 = "", $fecha_2 = "",$campo = "")
    {
        $result = $this->db->group_start()
                //->where('prer.envio_jdt','1')
                ->where('doc.fecha_recepcion >=', $fecha_1)
                ->where('doc.fecha_recepcion <=', $fecha_2)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.envia_ventanilla, prer.estado_refacciones, prer.firma_requisicion, prer.solicita_psni, prer.ref_garantias, doc.fecha_recepcion,doc.folio_externo, prer.anexa_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosVentanilla_tabl_fecha($fecha_1 = "",$campo = "")
    {
        $result = $this->db->group_start()
                //->where('prer.envio_jdt','1')
                ->where('doc.fecha_recepcion', $fecha_1)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.envia_ventanilla, prer.estado_refacciones, prer.firma_requisicion, prer.solicita_psni, prer.ref_garantias, doc.fecha_recepcion,doc.folio_externo, prer.anexa_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosVentanilla_table_campo_status($campo = "",$edo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.estado_refacciones',$edo)
                ->where('prer.identificador','M')
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.envia_ventanilla, prer.estado_refacciones, prer.firma_requisicion, prer.solicita_psni, prer.ref_garantias, doc.fecha_recepcion,doc.folio_externo, prer.anexa_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosVentanilla_tabl_fecha_2_status($fecha_1 = "", $fecha_2 = "",$campo = "",$edo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.estado_refacciones',$edo)
                ->where('doc.fecha_recepcion >=', $fecha_1)
                ->where('doc.fecha_recepcion <=', $fecha_2)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.envia_ventanilla, prer.estado_refacciones, prer.firma_requisicion, prer.solicita_psni, prer.ref_garantias, doc.fecha_recepcion,doc.folio_externo, prer.anexa_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function presupuestosVentanilla_tabl_fecha_status($fecha_1 = "",$campo = "",$edo = "")
    {
        $result = $this->db->group_start()
                ->where('prer.estado_refacciones',$edo)
                ->where('doc.fecha_recepcion', $fecha_1)
                ->group_start()
                    ->like('doc.asesor',$campo,'both')
                    ->or_like('doc.tecnico',$campo,'both')
                    ->or_like('doc.id_cita',$campo,'both')
                    ->or_like('doc.cliente',$campo,'both')
                    ->or_like('doc.placas',$campo,'both')
                    ->or_like('doc.serie',$campo,'both')
                    ->or_like('doc.vehiculo',$campo,'both')
                    ->or_like('doc.folio_externo',$campo,'both')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.envia_ventanilla, prer.estado_refacciones, prer.firma_requisicion, prer.solicita_psni, prer.ref_garantias, doc.fecha_recepcion,doc.folio_externo, prer.anexa_garantias')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->limit(1000)
            ->get('presupuesto_registro AS prer')
            ->result(); 
        return $result;
    }

    public function listado_audotoriacalidad($fecha = '') 
    {
        $result = $this->db->where('doc.fecha_recepcion >',$fecha)
            ->join('documentacion_ordenes AS doc','doc.id_cita = au.idOrden')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, au.proxServ, au.fechaProxServ, au.idOrden AS id_cita, au.nombreMecanico, au.nombreAuditor, au.nombreJefeTaller, au.fechaRegistro, doc.folio_externo, doc.serie')
            ->order_by('au.idRegistro', 'DESC')
            //->limit(800)
            ->get('auditoriacalidad AS au')
            ->result();
        return $result;
    }

    public function busqueda_auditoria($campo = '')
    {
        $query = $this->db->like('au.idOrden',$campo,'both')
            ->or_like('doc.tecnico',$campo,'both')
            ->or_like('doc.vehiculo',$campo,'both')
            ->or_like('doc.placas',$campo,'both')
            ->or_like('doc.serie',$campo,'both')
            ->or_like('doc.asesor',$campo,'both')

            ->join('documentacion_ordenes AS doc','doc.id_cita = au.idOrden')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, au.proxServ, au.fechaProxServ, au.idOrden AS id_cita, au.nombreMecanico, au.nombreAuditor, au.nombreJefeTaller, au.fechaRegistro, doc.folio_externo, doc.serie')
            ->order_by('au.idRegistro', 'DESC')
            ->limit(100)
            ->get('auditoriacalidad AS au')
            ->result();
        return $query;
    }

    public function buscar_ref_adicioanl($id_cita = '',$id_garantia = '')
    {
        $query = $this->db->where('id_garantia', $id_garantia)
            //->where('id_cita', $id_cita)
            
            ->select('id')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('presupuesto_multipunto');

        if($query->num_rows()==1){
            $resultado = TRUE;
        }else{
            $resultado = FALSE;
        }

        return $resultado;
    }

    public function existencia_presupuesto($id_cita = '')
    {
        $query = $this->db->where('id_cita', $id_cita)
            ->select('id')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('presupuesto_registro');

        if($query->num_rows()==1){
            $resultado = TRUE;
        }else{
            $resultado = FALSE;
        }

        return $resultado;
    }

    public function id_cita_refaccion($id_recaccion = '')
    {
        $query = $this->db->where('id',$id_recaccion)
            ->select('id_cita')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('presupuesto_multipunto');

        if($query->num_rows()==1){
          $id_cita = $query->row()->id_cita;
        }else{
          $id_cita = "SD";
        }

        return $id_cita;
    }

    public function garantia_asociada($id_cita = '')
    {
        $query = $this->db->where('id_cita',$id_cita)
            ->select('orden_asociada')
            ->limit(1)
            ->get('citas');

        if($query->num_rows()==1){
          $servicio = $query->row()->orden_asociada;
        }else{
          $servicio = NULL;
        }

        return $servicio;
    }

    public function recuperar_id_cita($id_orden = 0)
    {
        $query = $this->db->where('id',$id_orden)
            ->select('id_cita')
            ->limit(1)
            ->get('ordenservicio');

        if($query->num_rows()==1){
          $id_cita = $query->row()->id_cita;
        }else{
          $id_cita = NULL;
        }

        return $id_cita;
    }

    public function total_ref_garantia($id_cita = "")
    {
        $num_results = $this->db->select('id')
        ->from('presupuesto_multipunto')
        ->where('id_cita',$id_cita)
        ->where('pza_garantia',"1")
        ->count_all_results();

        return $num_results;
    }

    public function listado_ordenes_doc($fecha = ""){
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion >=', $fecha)
            ->where('c.orden_asociada', NULL)
            ->group_start()
                ->where('o.id_tipo_orden', 1)
                ->or_where('o.id_tipo_orden', 4)
                ->or_where('o.id_tipo_orden', 2)
            ->group_end()
        ->group_end()

        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador')
        ->order_by("doc.id", "DESC")
        //->limit(1000)
        ->get('documentacion_ordenes AS doc')->result();
        //echo $this->db->last_query();die();
        return $result;
    }

    public function existencia_archivos_polizas($id_cita = '')
    {
        $query = $this->db->where('id_cita', $id_cita)
            ->select('id')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('archivos_poliza_garantias');

        if($query->num_rows()==1){
            $resultado = 1;
        }else{
            $resultado = 0;
        }

        return $resultado;
    }

    public function tipo_orden($id_cita = '')
    {
        $query = $this->db->where('id_cita', $id_cita)
            ->select('id_tipo_orden')
            ->order_by("id", "DESC")
            ->limit(1)
            ->get('ordenservicio');

        if($query->num_rows()==1){
            $resultado = $query->row()->id_tipo_orden;
        }else{
            $resultado = "";
        }

        return $resultado;
    }

    public function proactivo_list_rechazado($fecha = ""){
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion >=', $fecha)
            ->group_start()
                ->where('prer.acepta_cliente', 'No')
                ->or_where('prer.acepta_cliente', 'Val')
            ->group_end()
        ->group_end()
                

        ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        //->join('citas AS c','c.id_cita = doc.id_cita')
        
        ->select('doc.asesor, doc.id_cita, doc.fecha_recepcion, doc.asesor, doc.cliente, doc.serie ,o.telefono_movil , o.otro_telefono, doc.folio_externo, prer.identificador, prer.acepta_cliente')

        ->order_by("doc.fecha_recepcion", "DESC")
        //->limit(1000)
        ->get('presupuesto_registro AS prer')->result();
        //echo $this->db->last_query();die();
        return $result;
    }

    public function recuperar_audios_vc()
    {
        $query = $this->db->distinct('o.id_cita')
            ->group_start()
                ->where("o.fecha_recepcion <", "2021-10-01")
                ->where("o.fecha_recepcion >=", "2021-09-01")
                ->group_start()
                    ->where("vc.audio !=", "")
                    ->or_where("vc_a.evidencia !=", "")
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','o.id_cita = doc.id_cita','left')
            ->join('voz_cliente AS vc_a','vc_a.idPivote = o.id_cita','left')
            ->join('vc_registro AS vc','vc.id_cita = o.id_cita','left')

            ->select('doc.vehiculo, doc.placas, doc.serie, doc.asesor, doc.tecnico, doc.cliente, vc_a.idRegistro as id_vc_a, vc_a.definicionFalla, vc_a.evidencia, o.fecha_recepcion, o.hora_recepcion, o.id_cita, vc.id as id_vc, vc.audio,  vc.fecha_registro AS registro_voz_cliente_new, vc_a.fecha_registro AS registro_voz_cliente_old, vc_a.nombreAsesor, vc_a.nombreCliente, vc.nombre_asesor, vc.nombre_cliente ')
            
            ->order_by("o.fecha_recepcion ASC", "o.hora_recepcion ASC")
            //->limit(500)
            ->get('ordenservicio AS o')
            ->result();
        return $query;
    }

    public function recuperar_totales_vc_caudio_old($anio = "")
    {
        $num_results = $this->db->select('idRegistro')
        ->from('voz_cliente')
        ->group_start()
            ->where("fecha_registro <", ($anio+1)."-01-01")
            ->where("fecha_registro >=", $anio."-01-01")
            ->group_start()
                ->where("evidencia !=", "")
            ->group_end()
        ->group_end()
        
        ->count_all_results();

        return $num_results;
    }

    public function recuperar_totales_vc_saudio_old($anio = "")
    {
        $num_results = $this->db->select('idRegistro')
        ->from('voz_cliente')
        ->group_start()
            ->where("fecha_registro <", ($anio+1)."-01-01")
            ->where("fecha_registro >=", $anio."-01-01")
            ->group_start()
                ->where("evidencia", "")
            ->group_end()
        ->group_end()
        ->count_all_results();

        return $num_results;
    }

    public function recuperar_totales_vc_caudio_new($anio = "")
    {
        $num_results = $this->db->select('id')
        ->from('vc_registro')
        ->group_start()
            ->where("fecha_registro <", ($anio+1)."-01-01")
            ->where("fecha_registro >=", $anio."-01-01")
            ->group_start()
                ->where("audio !=", "")
            ->group_end()
        ->group_end()
        ->count_all_results();

        return $num_results;
    }

    public function recuperar_totales_vc_saudio_new($anio = "")
    {
        $num_results = $this->db->select('id')
        ->from('vc_registro')
        ->group_start()
            ->where("fecha_registro <", ($anio+1)."-01-01")
            ->where("fecha_registro >=", $anio."-01-01")
            ->group_start()
                ->where("audio", "")
            ->group_end()
        ->group_end()
        ->count_all_results();

        return $num_results;
    }

    public function presupuesto_graficas_garantias()
    {
        $query = $this->db->distinct('prer.id_cita')
            ->group_start()
                ->where("prer.ref_garantias >", "0")
                ->where("prer.identificador", "M")
                ->group_start()
                    ->where("prer.fecha_alta <", "2021-10-20")
                    ->where("prer.fecha_alta >=", "2021-10-01")
                ->group_end()
            ->group_end()

            ->join('ordenservicio AS o','o.id_cita = prer.id_cita','left')
            ->join('citas AS c','c.id_cita = o.id_cita','left')
            ->join('tecnicos AS t','c.id_tecnico = t.id','left')
            ->join('subcategorias AS subcat','subcat.id = o.idsubcategoria','left')

            ->select('t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas, o.vehiculo_identificacion, prer.id_cita, prer.folio_externo, prer.fecha_actualiza, prer.acepta_cliente, prer.estado_refacciones, prer.ubicacion_proceso, subcat.subcategoria, c.datos_nombres ,c.datos_apellido_paterno ,c.datos_apellido_materno ')
            
            ->order_by("prer.fecha_alta ASC")
            ->get('presupuesto_registro AS prer')
            ->result();
        return $query;
    }

/***************************************************************************************************************/
//                              Bloque de panel asesor tecnico (Inicio)
/***************************************************************************************************************/
    public function recuperar_id_orden($id_cita=''){
        $q = $this->db->where('id_cita',$id_cita)->select('id,id_tipo_orden')->from('ordenservicio')->limit(1)->get();
        if($q->num_rows()==1){
            return [$q->row()->id , $q->row()->id_tipo_orden];
        }else{
            return [0,0];
        }
    }

    public function tipo_orden_valida($id_cita = '')
    {
        $q = $this->db->where('o.id_cita',$id_cita)
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
            ->select('cto.identificador, cto.tipo_orden')
            ->limit(1)
            ->get('ordenservicio AS o');
            
        if($q->num_rows()==1){
            $query = $q->result_array();
            $retorno = $query[0];
        }else{
            $retorno = [];
        }

        return $retorno;
    }
    
    public function presupuestosAsesTec_table($fecha = '')
    {
        $result = $this->db->group_start()
                ->like('prer.identificador','M','both')
                ->where('doc.fecha_recepcion >=', $fecha)
                ->where('prer.envio_jdt','1')
                ->where('prer.envia_ventanilla !=','0')
                //->where('doc.tipo_vita','2')
                ->group_start()
                    ->where('o.id_tipo_orden', '3')
                    ->or_where('o.id_tipo_orden', '4')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita')
            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
            //->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion, o.id_tipo_orden, doc.folio_externo') //, cto.identificador
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosAsesTec_tableHyP($fecha = '', $tipo_orden = '',$asesor = '')
    {
        $result = $this->db->group_start()
                ->like('prer.identificador','M','both')
                //->where('doc.fecha_recepcion >=', $fecha)
                ->where('prer.envio_jdt','1')
                ->where('prer.envia_ventanilla !=','0')
                ->group_start()
                    //->where('o.id_tipo_orden', $tipo_orden)
                    ->like('doc.asesor', $asesor)
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita')
            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            //->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion, o.id_tipo_orden, doc.folio_externo')  //, cto.identificador
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosAsesTec_tableHyP2($fecha = '', $tipo_orden = '')
    {
        $result = $this->db->group_start()
                ->like('prer.identificador','M','both')
                //->where('doc.fecha_recepcion >=', $fecha)
                ->where('prer.envio_jdt','1')
                ->where('prer.envia_ventanilla !=','0')
                ->group_start()
                    ->where('o.id_tipo_orden', $tipo_orden)
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita')
            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            //->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ref_garantias, prer.acepta_cliente, prer.envio_jdt, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion, o.id_tipo_orden, doc.folio_externo')  //, cto.identificador
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosCAsesTec_table($fecha = '')
    {
        $result = $this->db->group_start()
                ->like('prer.identificador','M','both')
                ->where('doc.fecha_recepcion >=', $fecha)
                //->where('doc.tipo_vita','2')
                ->group_start()
                    ->where('o.id_tipo_orden', '3')
                    ->or_where('o.id_tipo_orden', '4')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita')
            ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
            //->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion, o.id_tipo_orden, doc.folio_externo') //, cto.identificador
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function presupuestosCAsesTec_tableHyP($fecha = '', $tipo_orden = '',$asesor)
    {
        $result = $this->db->group_start()
                ->like('prer.identificador','M','both')
                //->where('doc.fecha_recepcion >=', $fecha)
                ->group_start()
                    //->where('doc.tipo_vita','2')
                    ->like('doc.asesor', $asesor)
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = prer.id_cita')
            ->join('ordenservicio AS o','o.id_cita = prer.id_cita')
            //->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, prer.id_cita, prer.ubicacion_proceso, prer.acepta_cliente, prer.serie, prer.estado_refacciones, prer.firma_asesor, doc.fecha_recepcion, o.id_tipo_orden, doc.folio_externo')  //, cto.identificador
            ->order_by('prer.fecha_actualiza', 'DESC')
            //->limit(30)
            ->get('presupuesto_registro AS prer')
            ->result();
        return $result;
    }

    public function doc_tb_ordenes_body($fecha = ""){
        $result = $this->db->group_start()
            ->where('doc.fecha_recepcion >=', $fecha)
            ->group_start()
                ->where('o.id_tipo_orden', '3')
                ->or_where('o.id_tipo_orden', '4')
                ->or_where('doc.tipo_vita','2')
            ->group_end()
        ->group_end()

        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, c.id_asesor, o.hora_recepcion')
        ->order_by("doc.id", "DESC")
        ->get('documentacion_ordenes AS doc')
        ->result_array();
        //echo $this->db->last_query();die();
        return $result;
    }

    public function doc_tb_ordenes_body2($fecha = "",$asesor = ''){
        $result = $this->db->like('doc.asesor', $asesor)

        ->join('lavado AS lavado','lavado.id_cita = doc.id_cita','left')
        ->join('ordenservicio AS o','o.id_cita = doc.id_cita')
        ->join('citas AS c','c.id_cita = doc.id_cita')
        ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')
        
        ->select('doc.*,lavado.id_lavador_cambio_estatus, o.id_tipo_orden, c.id_status_color, c.orden_asociada, o.id AS id_orden, cto.identificador, c.id_asesor, o.hora_recepcion')
        ->order_by("doc.id", "DESC")
        ->get('documentacion_ordenes AS doc')
        ->result_array();
        //echo $this->db->last_query();die();
        return $result;
    }

    public function voz_cliente_athyp($fecha = '', $tipo_orden = '')
    {
        $result = $this->db->group_start()
                ->where('doc.fecha_recepcion >=', $fecha)
                ->group_start()
                    ->where('o.id_tipo_orden', $tipo_orden)
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = vc.idPivote')
            ->join('ordenservicio AS o','o.id_cita = vc.idPivote')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, doc.cliente, doc.fecha_recepcion, doc.serie, o.id_tipo_orden, vc.idPivote, vc.idRegistro, vc.fecha_registro, vc.definicionFalla, vc.nombreCliente, vc.nombreAsesor, vc.evidencia ,vc.componente , vc.sistema , vc.causaRaiz , vc.comentario, vc.idRegistro, doc.id_cita,  cto.identificador')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->get('voz_cliente AS vc')
            ->result();
        return $result;
    }

    public function voz_cliente_at($fecha = '')
    {
        $result = $this->db->group_start()
                ->where('doc.fecha_recepcion >=', $fecha)
                ->group_start()
                    ->where('o.id_tipo_orden', '3')
                    ->or_where('o.id_tipo_orden', '4')
                ->group_end()
            ->group_end()

            ->join('documentacion_ordenes AS doc','doc.id_cita = vc.idPivote')
            ->join('ordenservicio AS o','o.id_cita = vc.idPivote')
            ->join('cat_tipo_orden AS cto','cto.id = o.id_tipo_orden','left')

            ->select('doc.tecnico, doc.vehiculo, doc.placas, doc.asesor, doc.cliente, doc.fecha_recepcion, doc.serie, o.id_tipo_orden, vc.idPivote, vc.idRegistro, vc.fecha_registro, vc.definicionFalla, vc.nombreCliente, vc.nombreAsesor, vc.evidencia ,vc.componente , vc.sistema , vc.causaRaiz , vc.comentario, vc.idRegistro, doc.id_cita,  cto.identificador')
            ->order_by('doc.fecha_recepcion', 'DESC')
            ->get('voz_cliente AS vc')
            ->result();
        return $result;
    }

    public function identificador_preg($id_cita=''){
        $q = $this->db->where('id_cita',$id_cita)->select('identificador')->order_by("id", "DESC")->limit(1)->get('presupuesto_registro');
        if($q->num_rows()==1){
            $respuesta = $q->row()->identificador;
        }else{
            $respuesta = 'M';
        }
        return $respuesta;
    }

    public function ordenesEstatusHYP($fecha = '',$asesor = "")
    {
        $query = $this->db->group_start()
                    ->where('cat_ct.id >=', '15')
                    //->or_where('cat_ct.id', '16')
                    //->or_where('cat_ct.id', '17')
                    //->or_where('cat_ct.id', '18')
                    ->where('cat_ct.id <=', '19')
                ->group_start()
                    ->like('c.asesor', $asesor)
                ->group_end()
            ->group_end()

            ->join('ordenservicio AS o','o.id_cita = c.id_cita')
            //->join('tecnicos AS t','c.id_tecnico = t.id','left')
            ->join('documentacion_ordenes AS doc','o.id_cita = doc.id_cita','left')
            ->join('cat_status_citas_tecnicos AS cat_ct','cat_ct ON cat_ct.id = c.id_status')

            ->select('doc.tecnico, o.id_cita AS orden_cita, doc.oasis  AS archivoOasis, doc.asesor, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, o.fecha_recepcion, c.vehiculo_modelo, c.vehiculo_placas, cat_ct.status, cat_ct.color')
            
            ->order_by("o.id", "DESC")
            ->get('citas AS c')
            ->result();
        return $query;
    }

    public function ordenesEstatusHYP_Gral($fecha = '')
    {
        $query = $this->db->group_start()
                    ->where('cat_ct.id >=', '15')
                    //->or_where('cat_ct.id', '16')
                    //->or_where('cat_ct.id', '17')
                    //->or_where('cat_ct.id', '18')
                    ->where('cat_ct.id <=', '19')
                ->group_start()
                    ->where('o.id_tipo_orden', '3')
                    ->or_where('o.id_tipo_orden', '4')
                ->group_end()
            ->group_end()

            ->join('ordenservicio AS o','o.id_cita = c.id_cita')
            //->join('tecnicos AS t','c.id_tecnico = t.id','left')
            ->join('documentacion_ordenes AS doc','o.id_cita = doc.id_cita','left')
            ->join('cat_status_citas_tecnicos AS cat_ct','cat_ct ON cat_ct.id = c.id_status')

            ->select('doc.tecnico, o.id_cita AS orden_cita, doc.oasis  AS archivoOasis, doc.asesor, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, o.fecha_recepcion, c.vehiculo_modelo, c.vehiculo_placas, cat_ct.status, cat_ct.color')
            
            ->order_by("o.id", "DESC")
            ->get('citas AS c')
            ->result();
        return $query;
    }

    public function valida_refaccion_multipunto($id_cita = '',$descripcion = '',$cantidad = '')
    {
        $q = $this->db->where('id_cita',$id_cita)
            ->where('cantidad',$cantidad)
            ->where('descripcion',$descripcion)
            ->select('id')->get('presupuesto_multipunto');

        if($q->num_rows()==1){
            $retorno = (int)$q->row()->id;
        }else{
            $retorno = 0;
        }
        return $retorno;
    }

    public function valida_refaccion_registro($id_cita = '')
    {
        $q = $this->db->where('id_cita',$id_cita)
            ->select('id')->get('presupuesto_registro');

        if($q->num_rows()==1){
            $retorno = (int)$q->row()->id;
        }else{
            $retorno = 0;
        }
        return $retorno; 
    }

/***************************************************************************************************************/
//                              Bloque de panel asesor tecnico (Fin)
/***************************************************************************************************************/


/***************************************************************************************************************/
//                              Bloque de soporte (inicio)
/***************************************************************************************************************/
    public function recuperar_historial($campo = '',$tabla = '')
    {
        $query = $this->db->where('id_cita',$campo)
            ->select('*')
            ->order_by("id", "DESC")
            ->get($tabla);

        return $query->result_array();
    }

    public function evidencias_historica($campo = '')
    {
        $query = $this->db->where('ref_historico',$campo)
            ->select('*')
            ->order_by("id", "DESC")
            ->get('registro_evidencia_soporte');

        return $query->result_array();
    }

    public function recuperar_multipuntos($campo = '')
    {
        $query = $this->db->like('mg.orden',$campo,'both')
            ->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id')

            ->select('mg.id, mg.orden, mg3.firmaAsesor, mg3.firmaTecnico, mg3.firmaCliente, mg3.firmaJefeTaller')
            ->order_by("mg.id", "DESC")
            ->get('multipunto_general AS mg');

        return $query->result_array();
    }

    public function recuperar_elementos_soporte($id_cita = '')
    {
        $q = $this->db->where('o.id_cita',$id_cita)

            ->join('ordenservicio AS o','o.id_cita = c.id_cita')
            ->join('tecnicos AS t','c.id_tecnico = t.id')
            //Diagnostico (Orden parte 2 / Invetario)
            ->join('diagnostico AS d','d.noServicio = c.id_cita','left')
            //Presupuesto Multipunto (nuevas cotizaciones)
            ->join('presupuesto_registro AS prer','prer.id_cita = c.id_cita','left')
            //Multipunto
            ->join('multipunto_general AS mg','mg.orden = c.id_cita','left')
            ->join('multipunto_general_3 AS mg3','mg3.idOrden = mg.id','left')
            //Voz Cliente
            ->join('voz_cliente AS voz','voz.idPivote = c.id_cita','left')
            //Voz Cliente Nuevo Formato
            ->join('vc_registro AS vc','vc.id_cita = c.id_cita','left')
            //Servicio valet
            ->join('servicio_valet AS valet','valet.id_cita = c.id_cita','left')
            //Lavado
            ->join('lavado AS lavado','lavado.id_cita = c.id_cita','left')
            //Documentacion de ordenes
            ->join('documentacion_ordenes AS doc','doc.id_cita = c.id_cita','left')

            ->select('t.nombre AS tecnico, o.id_cita AS orden_cita, d.archivoOasis, c.asesor, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, o.fecha_recepcion, c.vehiculo_modelo, c.id_tecnico, c.vehiculo_numero_serie, o.vehiculo_identificacion, vc.id AS vc_id, vc.audio, o.folioIntelisis, o.fecha_entrega, c.vehiculo_placas, mg3.firmaJefeTaller, mg.id AS idMultipunto, voz.evidencia, voz.idRegistro AS idVoz, lavado.id_lavador_cambio_estatus, prer.id AS id_presupuesto, prer.acepta_cliente, valet.id AS idValet, d.idDiagnostico, o.hora_recepcion, o.hora_entrega, prer.firma_garantia, prer.envia_ventanilla, prer.firma_asesor, prer.firma_jdt, prer.ref_garantias, mg3.firmaAsesor, mg3.firmaTecnico, mg3.firmaCliente, doc.id AS documentacion')

            ->order_by("o.id", "DESC")
            ->limit(1)
            ->get('citas AS c');

        if($q->num_rows()==1){
            $query = $q->result_array();
            $retorno = $query[0];
        }else{
            $retorno = [];
        }

        return $retorno;
    }

    public function revision_garantias($id_cita = "")
    {
        $num_results = $this->db->where('id_cita',$id_cita)
            ->where('activo','1')
            ->where('pza_garantia','1')
            ->select('id')
            ->from('presupuesto_multipunto')
        ->count_all_results();

        return $num_results;
    }

    public function revisar_soporte($id_cita = "")
    {
        $num_results = $this->db->where('id_cita',$id_cita)
            ->select('id')
            ->from('registro_historial_soporte')
        ->count_all_results();

        if ($num_results > 0) {
            $retorno = 1;
        } else {
            $retorno = 0;
        }
        
        return $retorno;
    }

    public function datos_cabecera($id_cita = '')
    {
        $q = $this->db->where('o.id_cita',$id_cita)

            ->join('ordenservicio AS o','o.id_cita = c.id_cita')
            ->join('tecnicos AS t','c.id_tecnico = t.id','left')
            ->join('diagnostico AS d','d.noServicio = c.id_cita','left')
            ->join('categorias AS cat','cat.id = o.idcategoria','left')
            ->join('subcategorias AS subcat','subcat.id = o.idsubcategoria','left')

            ->select('t.nombre AS tecnico, o.id_cita AS orden_cita, c.asesor, o.fecha_recepcion, c.vehiculo_modelo, c.vehiculo_numero_serie, o.vehiculo_identificacion, o.folioIntelisis, c.vehiculo_placas, cat.categoria, subcat.subcategoria, c.datos_nombres, c.datos_apellido_paterno, c.datos_apellido_materno, o.id_situacion_intelisis')

            ->order_by("o.id", "DESC")
            ->limit(1)
            ->get('citas AS c');

        if($q->num_rows()==1){
            $query = $q->result_array();
            $retorno = $query[0];
        }else{
            $retorno = [];
        }

        return $retorno;
    }

    public function recuperar_index_diag($id_cita = '')
    {
        $q = $this->db->where('noServicio',$id_cita)
            ->select('idDiagnostico')
            ->order_by('idDiagnostico', "DESC")
            ->limit(1)
            ->get('diagnostico');

        if($q->num_rows()==1){
            $retorno = $q->row()->idDiagnostico;
        }else{
            $retorno = 0;
        }

        return $retorno;
    }

    public function recuperar_index_contrato($id_cita = '')
    {
        $q = $this->db->where('identificador',$id_cita)
            ->select('idContracto')
            ->order_by('idContracto', "DESC")
            ->limit(1)
            ->get('contrato');

        if($q->num_rows()==1){
            $retorno = $q->row()->idContracto;
        }else{
            $retorno = 0;
        }

        return $retorno;
    }

    public function recuperar_firmas_orden($id_cita = '')
    {
        $q = $this->db->where('noServicio',$id_cita)
            ->select('firmaAsesor, firmaConsumidor2, fechaRegistro, nombreAsesor, nombreConsumidor2')
            ->order_by('idDiagnostico', "DESC")
            ->limit(1)
            ->get('diagnostico');

        if($q->num_rows()==1){
            $query = $q->result_array();
            $retorno = $query[0];
        }else{
            $retorno = [];
        }

        return $retorno;
    }

    public function recuperar_exp_contrato()
    {
        $q = $this->db->select('noRegistro, noExpediente, fechDia, fechMes, fechAnio')
            ->order_by('idContracto', "DESC")
            ->limit(1)
            ->get('contrato');

        if($q->num_rows()==1){
            $query = $q->result_array();
            $retorno = $query[0];
        }else{
            $retorno = [];
        }

        return $retorno;
    }
/***************************************************************************************************************/
//                              Bloque de soporte (fin)
/***************************************************************************************************************/


}

