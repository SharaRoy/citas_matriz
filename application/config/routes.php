<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Pivote';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Validaciones de los usuarios
$route['Principal_Tec/(:any)'] = 'Pivote/index/PT/$1';
$route['Principal_Asesor/(:any)'] = 'Pivote/index/PA/$1';
$route['Principal_JefeTaller/(:any)'] = 'Pivote/index/JT/$1';
$route['Principal_Otro/(:any)'] = 'Pivote/index/OT/$1';
$route['Principal_CierreOrden/(:any)'] = 'Pivote/index/PCO/$1'; 

//Paneles principales
//$route['Panel_Tec/(:any)'] = 'Validar_URL/panel_tecnico/$1'; //'Pivote/panel/TEC/$1';
$route['Panel_Tec/(:any)'] = 'Pivote/panel/TEC/$1';

//$route['Panel_Asesor/(:any)'] = 'Validar_URL/panel_asesor/$1'; //'Pivote/panel/ASE/$1';
$route['Panel_Asesor/(:any)'] = 'Pivote/panel/ASE/$1';

//$route['Panel_JefeTaller/(:any)'] = 'Validar_URL/panel_asesor/$1'; //'Pivote/panel/JDT/$1';
$route['Panel_JefeTaller/(:any)'] = 'Pivote/panel/JDT/$1';

$route['Panel/(:any)'] = 'Pivote/panel/OT/$1';
$route['Principal/(:any)'] = 'Pivote/index/ADMIN/$1';

// Redireccionamiento Multipunto
//$route['Multipunto_Alta/(:any)'] = 'Validar_URL/alta_multipunto/$1'; //'multipunto/Multipunto/index/$1'; 
$route['Multipunto_Alta/(:any)'] = 'multipunto/Multipunto/index/$1';

//Rutas usadas desde el planificador
$route['Multipunto_Edita/(:any)'] = 'Verificar/index/HM/$1';
$route['Multipunto_Principal/(:any)'] = 'multipunto/Multipunto/comodinEditaPrincipal/$1';
$route['Multipunto_PDF/(:any)'] = 'multipunto/Multipunto/setDataPDF/$1'; 


$route['Multipunto_Verifica/(:any)'] = 'multipunto/Multipunto/setData/$1';

//$route['Multipunto_N_Edita/(:any)'] = 'Validar_URL/n_edita_multipunto/$1'; //'multipunto/Multipunto/comodin/$1';
$route['Multipunto_N_Edita/(:any)'] = 'multipunto/Multipunto/comodin/$1';

//Rutas para el asesor
//$route['Multipunto_Edita_Asesor/(:any)'] = 'Validar_URL/edita_asesor_multipunto/$1'; //'multipunto/Multipunto/comodinEditaA/$1';
$route['Multipunto_Edita_Asesor/(:any)'] = 'multipunto/Multipunto/comodinEditaA/$1';

//Rutas para el tecnico
$route['Multipunto_Alta_Tec/(:any)'] = 'multipunto/Multipunto/comodinAlta/$1';
$route['Multipunto_Edita_Tec/(:any)'] = 'multipunto/Multipunto/comodinEditaTec/$1';

$route['Multipunto_Edita_JefeT/(:any)'] = 'multipunto/Multipunto/comodinEditFinal/$1';
// $route['Multipunto_Finalizada/(:any)'] = 'multipunto/Multipunto/comodinFinal/$1';
$route['Multipunto_Lista/(:any)'] = 'multipunto/Anexos/index/$1';

$route['Multipunto_Cotizacion/(:any)'] = 'multipunto/Cotizaciones/comodinRedireccion/$1';
$route['Cotizacion_PDF/(:any)'] = 'multipunto/Cotizaciones/setDataPDF/$1';
$route['Cotizacion_Cliente/(:any)'] = 'multipunto/Cotizaciones/comodinCliente/$1';
$route['Cotizacion_Edita/(:any)'] = 'multipunto/Cotizaciones/comodinEditaSU/$1';
$route['Listar_Cotizacion/(:any)'] = 'multipunto/Cotizaciones/comodinLista/$1';
$route['Cotizacion_Status_Lista/(:any)'] = 'multipunto/Cotizaciones/comodinStatusCliente/$1';
$route['Cotizacion_Edita_Status/(:any)'] = 'multipunto/Cotizaciones/comodinEditaStatus/$1';

$route['Requisicion/(:any)'] = 'multipunto/Cotizaciones/loadCoat/$1';
$route['Cotizacion_Refacciones/(:any)'] = 'multipunto/Cotizaciones/comodinRefStatus/$1';

//Analisis Multipunto
$route['Lista_resumen/(:any)'] = 'multipunto/Analisis/index/$1';

//Auditoria de calidad en la multipunto
$route['Auditoria_Calidad_Lista/(:any)'] = 'multipunto/AuditoriaCalidad/comodinLista/$1';
$route['Auditoria_Calidad/(:any)'] = 'multipunto/AuditoriaCalidad/index/$1';
$route['Auditoria_Calidad_PDF/(:any)'] = 'multipunto/AuditoriaCalidad/setDataPDF/$1';

//Redireccionamiento Orden de servicio
$route['OrdenServicio_Alta/(:any)'] = 'ordenServicio/ordenServicio/index/$1';
$route['OrdenServicio_Edita/(:any)'] = 'Verificar/index/OS/$1';
//$route['OrdenServicio_Verifica/(:any)'] = 'ordenServicio/ordenServicio/setData/$1';
$route['OrdenServicio_Verifica/(:any)'] = 'ordenServicio/ordenServicio/setDataVerificar/$1';
$route['OrdenServicio_Edicion/(:any)'] = 'ordenServicio/ordenServicio/setDataFormulario/$1';
$route['OrdenServicio_PDF/(:any)'] = 'ordenServicio/ordenServicio/setDataPDF/$1';

// Listas de documentacion
$route['OrdenServicio_Lista/(:any)'] = 'ordenServicio/ordenServicio/listData/$1';

$route['Documentacion_Asesores/(:any)'] = 'ordenServicio/Documentacion_Ordenes/documentacion_tb_asesor/$1';
$route['Documentacion_Tecnicos/(:any)'] = 'ordenServicio/Documentacion_Ordenes/documentacion_tb_tecnico/$1';
$route['Documentacion_JefeTaller/(:any)'] = 'ordenServicio/Documentacion_Ordenes/documentacion_tb_jdt/$1';
$route['Documentacion_CierreOrden/(:any)'] = 'ordenServicio/Documentacion_Ordenes/documentacion_tb_cierre/$1';
$route['Documentacion_Garantias/(:any)'] = 'ordenServicio/Documentacion_Ordenes/documentacion_tb_garantia/$1';
$route['Documentacion_Ventanilla/(:any)'] = 'ordenServicio/Documentacion_Ordenes/documentacion_tb_ventanilla/$1';

// Redireccionamiento Control de materiales
$route['CtrolMaterial_Alta/(:any)'] = 'ordenServicio/OrdenServicioP2/index/$1';
$route['CtrolMaterial_Edita/(:any)'] = 'Verificar/index/OS_CM/$1';
$route['CtrolMaterial_Verifica/(:any)'] = 'ordenServicio/ordenServicioP2/setDataControl/$1';
$route['CtrolMaterial_PDF/(:any)'] = 'ordenServicio/OrdenServicioP2/setDataPDFControl/$1';

// Redireccionamiento Control de materiales
//$route['VCliente_Alta/(:any)'] = 'vozCliente/VozCliente/index/$1';
//$route['VCliente_PDF/(:any)'] = 'vozCliente/VozCliente/setDataPDF_R/$1';
//$route['VCliente_Lista/(:any)'] = 'vozCliente/BuscadorVC/listaVC/$1';
//$route['VCliente_Temp/(:any)'] = 'vozCliente/BuscadorVC/index/$1';

// Redireccionamiento Buscador de ordenes sin $idOrden
$route['Buscador/(:any)'] = 'ordenServicio/Buscador/index/$1';
$route['Buscador_cotizacion/(:any)'] = 'multipunto/Cotizaciones/loadDataSearch/$1';
$route['Listar_cotizacion/(:any)'] = 'multipunto/Cotizaciones/comodinLista/$1';

$route['AudioEv/(:any)'] = 'assets/audios/$1';

//Garantias (formulario santa annita)
$route['Garantia/(:any)'] = 'garantias/Operaciones/index/$1';
$route['Garantia_Edit/(:any)'] = 'garantias/Operaciones/index/$1';
$route['Garantia_PDF/(:any)'] = 'garantias/Operaciones/setDataPDF/$1';
$route['Garantias_lista/(:any)'] = 'garantias/Operaciones/listData/$1';

$route['Garantia_F1863/(:any)'] = 'garantias/Operaciones2/index/$1';
$route['Garantia_F1863_Edit/(:any)'] = 'garantias/Operaciones2/index/$1';
$route['Garantia_F1863_PDF/(:any)'] = 'garantias/Operaciones2/setDataPDF/$1';
// $route['Garantias_F1863_lista/(:any)'] = 'garantias/Operaciones2/listData/$1';

//Otros formularios
$route['Formulario_Qro_A/(:any)'] = 'formularios/Queretaro/Formularios_1/index/$1';
$route['Formulario_Qro_L/(:any)'] = 'formularios/Queretaro/Formularios_1/limpiar/$1';
$route['Formulario_Qro_E/(:any)'] = 'formularios/Queretaro/Formularios_1/setData/$1';
$route['Formulario_Qro_T/(:any)'] = 'formularios/Queretaro/Formularios_1/listData/$1';

$route['Formulario_Previo/(:any)'] = 'previa/FormatoPrevio/index/$1';
$route['Formulario_Previo_L/(:any)'] = 'previa/FormatoPrevio/listData/$1';
$route['Formulario_Previo_PDF/(:any)'] = 'previa/FormatoPrevio/setDataPDF/$1';

//Vistas cargadas en modales (interna o extenamente del sistema)
$route['Campanias/(:any)'] = 'formularios/otrosFormularios/Contenedores/campanias/$1';
$route['Archivo_Oasis/(:any)'] = 'ordenServicio/ordenServicio/modalOasis/$1';

//Nuevas rutas
$route['Lista_Ordenes/(:any)'] = 'garantias/General/listaOrdenesHistoria/$1';
$route['Lista_Por_Firmas/(:any)'] = 'garantias/General/listaPorFirmar/$1';
$route['Garantia_Firma/(:any)'] = 'garantias/General/setDataP1/$1';
$route['Garantia_F1863_Firma/(:any)'] = 'garantias/General/setDataP2/$1';
$route['Garantias_Panel/(:any)'] = 'garantias/General/index/$1';

//Modulos externos, analisis
$route['Cotizacion_Rechazadas/(:any)'] = 'multipunto/ProactivoAnalisisHistorial/cotizacionesRechazadas/$1';
$route['Multipunto_Indicadores/(:any)'] = 'multipunto/IndicadoresProactivo/lista/$1';

$route['PresupuesoN_Ext_Cliente/(:any)'] = 'multipunto/ProactivoAnalisisHistorial/comodinVista_E/$1'; 

$route['Archivo_OSP1/(:any)'] = 'ordenServicio/ordenServicio/modalArchivo/$1';
$route['PDF_Perfil/(:any)'] = 'formularios/otrosFormularios/Contenedores/pdfBusqueda/$1';
$route['PDF_BusquedaMagic/(:any)'] = 'formularios/otrosFormularios/Contenedores/idsBusquedaMagic/$1';

//Inspeccion visual body shop
$route['IV_BodyShop/(:any)'] = 'otros_Modulos/InspeccionBS/index/$1';
$route['IV_BodyShop_PDF/(:any)'] = 'otros_Modulos/InspeccionBS/setDataPDF/$1';
$route['IV_BodyShop_Lista/(:any)'] = 'otros_Modulos/InspeccionBS/listData/$1';
$route['IV_BodyShop_ListadoGral/(:any)'] = 'otros_Modulos/InspeccionBS/listDataReading/$1';

$route['IV_BodyShop_Panel/(:any)'] = 'otros_Modulos/InspeccionBS/panelAcceso/$1';
$route['IV_BodyShop_Login/(:any)'] = 'Verificar/index/$1'; 

//Vistas de revision
$route['OrdenServicio_Revision/(:any)'] = 'ordenServicio/ordenServicio/setDataRevision/$1';
$route['Multipunto_Revision/(:any)'] = 'multipunto/Multipunto/setDataRevision/$1';

$route['VCliente2_Revision/(:any)'] = 'vozCliente/VozCliente/setDataRevision/$1';
$route['VCliente_Revision/(:any)'] = 'vozCliente/VC_Diagnostico/index/$1';

//Brincar validaciones de la cotizacion en proceso
$route['Listar_Cotizacion_Actualiza/(:any)'] = 'multipunto/Cotizaciones/comodinListaTec/$1';
$route['Cotizacion_Actualiza/(:any)'] = 'multipunto/Cotizaciones/comodinEditaProcesado/$1';

//Cotizaciones con piezas de garantia
$route['Cotizacion_Edita_Garantias/(:any)'] = 'multipunto/Cotizaciones/comodinEditaGarantias/$1';
$route['Lista_Cotizaciones_Garantias/(:any)'] = 'garantias/General/listaCotizaciones/$1';

//Cotizaciones revisar por jefe de taller
$route['Cotizacion_Revisa_JDT/(:any)'] = 'multipunto/Cotizaciones/comodinEditaJDT/$1';
$route['Lista_Cotizaciones_JDT/(:any)'] = 'multipunto/Cotizaciones/comodinListaJDT/$1';

//Rutas-Redireccionamiento de inventario
$route['Inventario_Lista/(:any)'] = 'ordenServicio/Inventario/listData/$1';
$route['Inventario_Alta/(:any)'] = 'ordenServicio/Inventario/index/$1';
$route['Inventario_Alta_Previo/(:any)'] = 'ordenServicio/Inventario/comodinPrellenado/$1';
$route['Inventario_Editar/(:any)'] = 'ordenServicio/Inventario/index/$1';
$route['Inventario_PDF/(:any)'] = 'ordenServicio/Inventario/setDataPDF/$1';
$route['Inventario_Revision/(:any)'] = 'ordenServicio/Inventario/setDataRevision/$1';

//Cargar estatus de ordenes para tecnicos
$route['Lista_Status_Tecnico/(:any)'] = 'ordenServicio/ordenServicio/listDataStatusOrder/$1';

//Para integracion con intelisis
$route['Inventario_Refacciones/(:any)'] = 'formularios/otrosFormularios/Refacciones/index/$1';
$route['ErroresEnvio/(:any)'] = 'otros_Modulos/ErroresEnvios/index/$1'; 

//Cotizacion iframe
//$route['Ext_Cotizacion/(:any)'] = 'multipunto/CotizacionesExternas/comodinRedireccion/$1';
//$route['Cotizacion_Ext_PDF/(:any)'] = 'multipunto/CotizacionesExternas/setDataPDF/$1';
//$route['Cotizacion_Ext_Cliente/(:any)'] = 'multipunto/CotizacionesExternas/comodinCliente/$1';
//$route['Cotizacion_Ext_Edita/(:any)'] = 'multipunto/CotizacionesExternas/comodinEditaSU/$1';

/**************************************************************************************************/
/**************************************************************************************************/

//*****************************  Rutas del asesor *************************************//
$route['Listar_Asesor/(:any)'] = 'presupuestos/Presupuestos/comodinListaAsesor/$1';
$route['Presupuesto_Asesor/(:any)'] = 'presupuestos/Presupuestos/comodinEditaAsesor/$1';

//*****************************  Ruras de ventanilla ***********************************//
$route['Listar_Ventanilla/(:any)'] = 'presupuestos/Presupuestos/comodinListaVentanilla/$1'; 
$route['Presupuesto_Tradicional/(:any)'] = 'presupuestos/Presupuestos/comodinEntrega_T/$1';
$route['Presupuesto_Garantias/(:any)'] = 'presupuestos/Presupuestos/comodinEntrega_G/$1';
$route['Requisicion_T/(:any)'] = 'presupuestos/Presupuestos/comodinRequisicion_T_PDF/$1';
$route['Requisicion_G/(:any)'] = 'presupuestos/Presupuestos/comodinRequisicion_G_PDF/$1';
$route['Requisicion_Firma_T/(:any)'] = 'presupuestos/Presupuestos/comodinRequisicion_T/$1';
$route['Requisicion_Firma_G/(:any)'] = 'presupuestos/Presupuestos/comodinRequisicion_G/$1';
$route['Presupuesto_Ventanilla/(:any)'] = 'presupuestos/Presupuestos/comodinEditaVentanilla/$1';

//*****************************  Rutas garantias ***********************************//
$route['Listar_Garantias/(:any)'] = 'presupuestos/Presupuestos/comodinListaGarantias/$1';
$route['Presupuesto_Garantia/(:any)'] = 'presupuestos/Presupuestos/comodinEditaGarantias/$1';

//*****************************  Rutas jefe de taller ***********************************//
$route['Listar_JefeTaller/(:any)'] = 'presupuestos/Presupuestos/comodinListaJefeTaller/$1';
$route['Listar_JDT_Estatus/(:any)'] = 'presupuestos/Presupuestos/comodinListaJDTEstatus/$1';
$route['Presupuesto_JDT/(:any)'] = 'presupuestos/Presupuestos/comodinEditaJefeTaller/$1';
$route['Presupuesto_Estatus/(:any)'] = 'presupuestos/Presupuestos/comodinEditaJDTEstatus/$1';

//*****************************  Rutas tecnico ***********************************//
//Alta / Vista desde la multipunto
$route['Alta_Presupuesto/(:any)'] = 'presupuestos/Presupuestos/comodinAltaTec/$1';
$route['Listar_Tecnico/(:any)'] = 'presupuestos/Presupuestos/comodinListaTecnico/$1';
$route['Presupuesto_Tecnico/(:any)'] = 'presupuestos/Presupuestos/comodinEditaTecnico/$1';

//*****************************  Rutas Cliente ***********************************//
$route['Presupuesto_Cliente/(:any)'] = 'presupuestos/Presupuestos/comodinEditaCliente/$1';
$route['Presupuesto_PDF/(:any)'] = 'presupuestos/Presupuestos/comodinGeneraPdf/$1';

//*****************************  Rutas generales ***********************************//
///Proceso de cotizaciones multipunto (tradicional)
$route['Multipunto_Presupuesto/(:any)'] = 'presupuestos/Presupuestos/index/$1';
$route['Ext_Cotizacion/(:any)'] = 'presupuestos/Presupuestos_adicional/index/$1';
$route['Cotizacion_Ext_Cliente/(:any)'] = 'presupuestos/Presupuestos_adicional/comodinCliente/$1'; 
$route['Cotizacion_Ext_PDF/(:any)'] = 'presupuestos/Presupuestos_adicional/comodinGeneraPdf/$1';

//*****************************  Indicadores del sistema  ***********************************//
//Ventas de mano de obra
$route['Indicador_VentaMO/(:any)'] = 'conexion/Estadisticas_Indicadores/index/$1';
//Garantias para mostrar totdales
$route['Indicador_GarantiasConteo/(:any)'] = 'conexion/Estadisticas_Indicadores/garantias_conteo/$1';
$route['Indicador_GarantiasMonto/(:any)'] = 'conexion/Estadisticas_Indicadores/garantias_monto/$1';

//Graficar de analisis de tiempos
$route['Graficas_Lista/(:any)'] = 'otros_Modulos/Graficas/index/$1';
$route['Graficas_Cotizacion/(:any)'] = 'otros_Modulos/Graficas/cargaGrafica/$1';

//*****************************  Nuevo formulario  ***********************************//
$route['Servicio_valet/(:any)'] = 'otros_Modulos/Servicio_Valet/index/$1';
$route['Servicio_valet_PDF/(:any)'] = 'otros_Modulos/Servicio_Valet/comodinPdf/$1';
$route['Servicio_valet_Lista/(:any)'] = 'otros_Modulos/Servicio_Valet/cargarLista/$1';
$route['Servicio_valet_Asignar/(:any)'] = 'otros_Modulos/Servicio_Valet/porAsignar/$1';

//Rutas para mostrar, dar de alta y/o editar el historial de garantias - refacciones
$route['HG_Lista/(:any)'] = 'otros_Modulos/Historial_garantias/comodinLista/$1'; 
$route['HG_Alta/(:any)'] = 'otros_Modulos/Historial_garantias/index/$1';
$route['HG_Editar/(:any)'] = 'otros_Modulos/Historial_garantias/index/$1';

//Rutas para mostrar, dar de alta y/o editar el historial de clientes - refacciones 
$route['ClienteRF_Lista/(:any)'] = 'otros_Modulos/Clientes_Refacciones/comodinLista/$1';
$route['ClienteRF_Alta/(:any)'] = 'otros_Modulos/Clientes_Refacciones/index/$1';
$route['ClienteRF_Editar/(:any)'] = 'otros_Modulos/Clientes_Refacciones/index/$1';   

$route['CartaRenuncia_Lista/(:any)'] = 'otros_Modulos/Renuncia_Protect/comodinLista/$1';
$route['CartaRenuncia/(:any)'] = 'otros_Modulos/Renuncia_Protect/index/$1';
$route['CartaRenuncia_PDF/(:any)'] = 'otros_Modulos/Renuncia_Protect/setDataPDF/$1';  

$route['Exportar_Refacciones/(:any)'] = 'presupuestos/PresupuestosBusquedas/descargar_filtro/$1';  

//Nueva ruta para firma exclusive del cliente
$route['Documentacion_PorFirmar/(:any)'] = 'ordenServicio/Documentacion_Ordenes/documentos_cliente/$1';
$route['Documentacion_PorFirmarAsesor'] = 'ordenServicio/Documentacion_Ordenes/documentos_cliente_asesor';
$route['OrdenServicio_Firmas/(:any)'] = 'ordenServicio/ordenServicio/setDataClienteFirma/$1';
$route['VCliente_Firmas/(:any)'] = 'vozCliente/VozCliente/setDataClienteFirma/$1';
$route['CartaRenuncia_Firmas/(:any)'] = 'otros_Modulos/Renuncia_Protect/setDataClienteFirma/$1';

//Procesos adicionales garantias
$route['OrdenServicio_RGarantia/(:any)'] = 'api/Operaciones_Garantias/setDataRevisionFirmas/$1';  
$route['Formatos_TecGarantia/(:any)'] = 'ordenServicio/Documentacion_Ordenes/mostrat_formatos_garantias/$1'; 
$route['Asociacion_Folios/(:any)'] = 'ordenServicio/Documentacion_Ordenes/operaciones_orden_folio/$1';

$route['OrdenServicio_RGTecnico/(:any)'] = 'api/Operaciones_Garantias/setDataRevisionTecnico/$1';
$route['OrdenServicio_RGJefeTaller/(:any)'] = 'api/Operaciones_Garantias/setDataRevisionJefeTaller/$1';
$route['OrdenServicio_RGPDF/(:any)'] = 'api/Operaciones_Garantias/setDataFirmasPDF/$1';

$route['Presupuesto_FinalGarantias/(:any)'] = 'presupuestos/Presupuestos/comodinModuloGarantia/$1';

$route['Requisicion_Descarga/(:any)'] = 'presupuestos/Presupuestos/comodinRequisicion_PDF_Descarga/$1';
$route['Listado_Polizas/(:any)'] = 'api/Operaciones_Garantias/ordenes_tb_poliza/$1'; 
$route['Cargar_Polizas/(:any)'] = 'api/Operaciones_Garantias/carga_archivo_poliza/$1'; 
$route['Ver_Polizas/(:any)'] = 'api/Operaciones_Garantias/vista_archivo_poliza/$1';

$route['RecibirGarantias_Descarga/(:any)'] = 'presupuestos/Presupuestos/comodinRecibePza_G_PDF_Descarga/$1';
$route['Recibir_Req_GPDF/(:any)'] = 'presupuestos/Presupuestos/comodinRecibePza_G_PDF/$1';
$route['Recibir_Req_G/(:any)'] = 'presupuestos/Presupuestos/comodinRecibePza_G/$1';

// Redireccionamiento Nuevas estructura Voz Cliente
$route['VCliente_Alta/(:any)'] = 'vozCliente/VC_Diagnostico/index/$1';
$route['VCliente_AddDiag/(:any)'] = 'vozCliente/VC_Diagnostico/setDataFormulario/$1';
$route['VCliente_PDF/(:any)'] = 'vozCliente/VC_Diagnostico/setDataPDF/$1';
$route['VCliente_Lista/(:any)'] = 'vozCliente/VC_Diagnostico/listado/$1';

$route['VCliente2_Alta/(:any)'] = 'vozCliente/VozCliente/index/$1';
$route['VCliente2_PDF/(:any)'] = 'vozCliente/VozCliente/setDataPDF/$1';
$route['VCliente_Temp/(:any)'] = 'vozCliente/BuscadorVC/index/$1';

$route['Reporte_Garantia_Presupuestos/(:any)'] = 'otros_Modulos/Graficas/reporte_tiempos_presupuestos/$1';

///////////////////////// --------------------------------------------------------------------------
$route['Login_AseTecnico'] = 'mod_asetec/Login_asetec/index';
$route['Panel_AsesorBody'] = 'mod_asetec/Login_asetec/panel';

$route['Alta_PresupuestoHYP/(:any)/(:num)'] = 'mod_asetec/Presupuestos_HYP/comodinAltaHYP/$1/$2';
//$route['Vista_PresupuestoHYP/(:any)'] = 'mod_asetec/Presupuestos_HYP/index_temp/$1';

$route['Listar_AsesorTecnico/(:any)'] = 'mod_asetec/Presupuestos_HYP/comodinListaAsestec/$1';
$route['Listar_AsesorTecnicoLC/(:any)'] = 'mod_asetec/Presupuestos_HYP/comodinListaAsestecCreadas/$1';

$route['VCLista_OrdenesBody'] = 'mod_asetec/VC_DiagnosticoHYP/lista_asestec';

$route['Documentacion_OrdenesBody'] = 'mod_asetec/Documentacion_OrdenesHYP/documentacion_tb_asesortec';
$route['Status_OrdenesBody'] = 'mod_asetec/Documentacion_OrdenesHYP/listStatusOrderHYP';

//****************************************  Ligas soporte  **********************************************//
$route['Login_soporte'] = 'soporte/Soporte_login/index/';
$route['Panel_soporte/(:any)'] = 'soporte/Soporte/index/$1';
$route['Soporte_Diagnostico/(:any)/(:any)'] = 'soporte/Soporte/verificacion_diag/$1/$2';
$route['Soporte_Multipunto/(:any)/(:any)'] = 'soporte/Soporte/verificacion_hm/$1/$2';
$route['Soporte_Presupuesto/(:any)/(:any)'] = 'soporte/Soporte/verificacion_pm/$1/$2';
$route['Soporte_Historial/(:any)'] = 'soporte/Soporte/historial/$1';