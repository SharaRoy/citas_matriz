<div style="margin: 20px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-2">
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Garantias_Panel/5";?>';">
                        Regresar
                    </button>
                </div>
                <div class="col-sm-8" align="center">
                    <h3 align="center">GARANTÍAS</h3>
                </div>
                <div class="col-sm-2" align="right">
                    <input type="button" class="btn btn-success" style="color:white; margin-top:20px;" onclick="location.href='<?=base_url()."Garantia/0";?>';" value="Nueva Garantía">
                </div>
            </div>

            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-sm-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ccc;">
                                    <!--<td align="center" style="width: 5%;vertical-align: middle;" rowspan="2">#</td>-->
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>NO ORDEN</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>FOLIO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;" rowspan="2"><strong>TELEFONO</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;" rowspan="2"><strong>ESTATUS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>FECHA RECIBE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>FECHA ENTREGA</strong></td>
                                    <!-- <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>NO. DISTRIBUIDOR</strong></td> -->
                                    <td align="center" colspan="4"><strong>ACCIONES</strong></td>
                                </tr>
                                <tr style="font-size:13px;background-color: #ddd;">
                                    <td align="center" style="width: 10%;"><strong>GARANTÍA</strong></td>
                                    <!-- <td align="center" style="width: 10%;"><strong>PDF</strong></td> -->
                                    <td align="center" style="width: 10%;"><strong>G. F1863</strong></td>
                                    <td align="center" style="width: 10%;"><strong>PDF</strong></td>
                                    <td align="center" style="width: 10%;"><strong>CAMBIAR<br>ESTATUS</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idRegistro)): ?>
                                    <?php foreach ($idRegistro as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <!--<td align="center" style="width:10%;">
                                                <?php echo $index+1; ?>
                                            </td>-->
                                            <td align="center" style="width:10%;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $folio[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $idAsesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $nombreProp[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $telefono[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <label id="estatusLB_<?php echo $index; ?>">
                                                    <?php echo $estatus[$index]; ?>
                                                </label>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $fechaRecibe[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $fechaEntrega[$index]; ?>
                                            </td>                                            
                                            <!-- <td align="center" style="width:10%;">
                                                <?php echo $noDistribuidor[$index]; ?>
                                            </td> -->
                                            <td align="center" style="width:15%;">
                                                <input type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Garantia_Edit/".$id_cita_url[$index];?>';" value="Editar">
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php if ($garantia2[$index] == "1"): ?>
                                                    <input type="button" class="btn btn-success" style="color:white;" onclick="location.href='<?=base_url()."Garantia_F1863/".$id_cita_url[$index];?>';" value="Nuevo">
                                                <?php elseif ($garantia2[$index] == "3"): ?>
                                                    <input type="button" class="btn btn-warning" style="color:white;" onclick="location.href='<?=base_url()."Garantia_F1863_Firma/".$id_cita_url[$index];?>';" value="Firmar">
                                                <?php else: ?>
                                                    <input type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Garantia_F1863_Edit/".$id_cita_url[$index];?>';" value="Editar">
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <a href="<?=base_url()."Garantia_PDF/".$id_cita_url[$index];?>" class="btn btn-primary" target="_blank">Ver</a>
                                            </td>
                                            <td>
                                                <a class="cambioEstatus btn btn-success" onclick='informacion_visual(<?php echo $index; ?>);'data-target="#cambiarEstatus" data-toggle="modal" style="color: white;">
                                                    Cambio Estatus
                                                </a>
                                                <input type="hidden" id="info_garantia_<?php echo $index; ?>" name="" value="<?php echo $idOrden[$index].'_'.$estatus[$index]; ?> ">
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="12" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para agregar un comentario -->
<div class="modal fade" id="cambiarEstatus" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">CAMBIO DE ESTATUS</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>

                <div class="row infoTituo">
                    <div class="col-sm-12">
                        <h4 id="">No. de Orden:</h4>
                        <h5 id="cita_estatus" style="color: darkblue;"></h5>
                        <h4 id="">Estatus Actual:</h4>
                        <h5 id="titulo_estatus" style="color: darkblue;"></h5>
                    </div>
                </div>

                <br>
                <div class="row infoTituo">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6">
                        <h5>Estatus:</h5>
                        <select class="form-control" id="estatus" style="font-size:13px;width:100%;">
                            <?php if (isset($id_estatus)): ?>
                                <?php foreach ($id_estatus as $index => $valor): ?>
                                    <option value="<?php echo $id_estatus[$index]; ?>" style=""><?php echo $estatusList[$index]; ?></option>
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>
                    <div class="col-sm-3"></div>
                </div>

                <br>
                <div class="row infoTituo">
                    <div class="col-sm-12">
                        <h5>Comentario:</h5>
                        <textarea rows='3' class='form-control' name="" id='comentario' style='width:100%;text-align:left;font-size:12px;'></textarea>

                        <br>
                        <h5>Usuario que edita:</h5>
                        <input type="text" name="" class="form-control" id="usuario" value="<?php if(isset($usuarioReg)) echo $usuarioReg; ?>" style="width: 100%;" disabled>
                    </div>
                </div>

                <div class="row" align="center">
                    <h4 id="finalizado" style="color:darkgreen;" align="center"></h4>
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="id_cita" value="">
                <input type="hidden" class='form-control' name="" id="id_renglon" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success infoTituo" id="btn_actualizar_estatus">Actualizar Estatus</button>
            </div>
        </div>
    </div>
</div>
