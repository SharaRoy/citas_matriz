<?php 
    // Eliminamos cache
    //la pagina expira en una fecha pasada
    header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT"); 
    //ultima actualizacion ahora cuando la cargamos
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
    //no guardar en CACHE
    header ("Cache-Control: no-cache, must-revalidate"); 
    header ("Pragma: no-cache");

    ob_start();

?>

<style media="screen">
    .respuesta{
        color: black;
    }

    #encabezado {
        color: #fff;
        background-color: #337ab7;
        border-color: #337ab7;
        border-radius: 4px;
    }

    p, label{
        text-align: justify;
        font-size:8px;
        /*color: #337ab7;*/
    }

    .tituloTxt{
        font-size:8px;
        color: #337ab7;
    }
    .division{
        background-color: #ddd;
    }
    .headers_table{
        border:1px solid black;
        border-style: double;
    }
    td,tr{
        padding: 0px 0px 0px 0px !important;
        /* font-size: 10px; */
        color:#337ab7;
    }

    .divCheck{
        width:40px;
        height:10px;
        border: 1px solid;
        align-content: center;
        text-align: center;
        background-color: #ffffff;
    }
</style>

<page backtop="7mm" backbottom="5mm" backleft="5mm" backright="5mm" style="">
    <table style="width:100%;border: 1px solid #337ab7;margin-left:20px;margin-right:20px;">
        <tr>
            <td style="width:30%;" align="left">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="margin-left: 20px;width:180px;height:40px;">
            </td>
            <td style="width:65%;" align="center">
                <h4 align="center">Formulario Garantía F1863</h4>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                (REP) NÚMERO DE REPARACIÓN, (MIL) LUZ INDICADORA DE LA FALLA P-MIL, (DTC) CÓDIGO DE FALLA
            </td>
        </tr>
        <tr>
            <td style="width:30%;" align="right">
                DTC <br> TREN MOTRIZ
            </td>
            <td style="width:60%;" align="right" style="padding-right:20px;">
                <label class="respuesta">No. Orden: <?php if(isset($idIntelisis)) echo $idIntelisis; else echo " "; ?></label>
                <br>
                Folio Intelisis:&nbsp;
                <label style="color:darkblue;">
                    <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                </label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width:100%;margin-left:20px;margin-right:20px;">
                    <tr>
                        <td style="width:20%;">
                            <table>
                                <tr>
                                    <td colspan="2" align="center" style="width:30%;">REP <br>N°</td>
                                    <td style="width:5%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td align="center" style="width:65%;"> LUZ INDICADORA <br>DE FALLA <br>ENCENDIDA (S/N) </td>
                                </tr>
                                <?php for ($x = 1; $x < 8; $x ++): ?>
                                    <tr>
                                        <td align="center" style="border-bottom:1px solid #337ab7;">
                                            <label class="respuesta"><?php if(isset($xrep)) {if($xrep[0][$x-1] != "") echo $xrep[0][$x-1]; else echo "<br>";} else echo "<br>"; ?></label>
                                        </td>
                                        <td align="center" style="border-bottom:1px solid #337ab7;">
                                            <label class="respuesta"><?php if(isset($xrep)) {if($xrep[1][$x-1] != "") echo $xrep[1][$x-1]; else echo "<br>";} else echo "<br>"; ?></label>
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td align="center" style="border-bottom:1px solid #337ab7;">
                                            <label class="respuesta"><?php if(isset($xluzFalla)) {if($xluzFalla[0][$x-1] != "") echo $xluzFalla[0][$x-1]; else echo "<br>";} else echo "<br>"; ?></label>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            </table>
                        </td>

                        <td style="width:75%;">
                            <table style="width:100%;margin-left:5px;">
                                <tr>
                                    <td align="right">
                                        KOEO
                                    </td>
                                    <?php for ($x = 1; $x < 36; $x ++): ?>
                                        <?php if (($x % 6) != 0): ?>
                                            <td align="center" style="border-bottom:1px solid #337ab7;">
                                                <label class="respuesta">&nbsp;<?php if(isset($xkoeo)) {if($xkoeo[0][$x-1] != "") echo $xkoeo[0][$x-1]; else echo "&nbsp;&nbsp;&nbsp;";} else echo "&nbsp;&nbsp;&nbsp;"; ?>&nbsp;</label>
                                            </td>
                                        <?php else: ?>
                                            <td align="center">
                                                <label class="respuesta">&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                            </td>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </tr>

                                <tr>
                                    <td align="right">
                                        KOEC
                                    </td>
                                    <?php for ($x = 1; $x < 36; $x ++): ?>
                                        <?php if (($x % 6) != 0): ?>
                                            <td align="center" style="border-bottom:1px solid #337ab7;">
                                                <label class="respuesta">&nbsp;<?php if(isset($xkoec)) {if($xkoec[0][$x-1] != "") echo $xkoec[0][$x-1]; else echo "&nbsp;&nbsp;&nbsp;";} else echo "&nbsp;&nbsp;&nbsp;"; ?>&nbsp;</label>
                                            </td>
                                        <?php else: ?>
                                            <td align="center">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </tr>

                                <tr>
                                    <td align="right">
                                        KOER
                                    </td>
                                    <?php for ($x = 1; $x < 36; $x ++): ?>
                                        <?php if (($x % 6) != 0): ?>
                                            <td align="center" style="border-bottom:1px solid #337ab7;">
                                                <label class="respuesta">&nbsp;<?php if(isset($xkoer)) {if($xkoer[0][$x-1] != "") echo $xkoer[0][$x-1]; else echo "&nbsp;&nbsp;&nbsp;";} else echo "&nbsp;&nbsp;&nbsp;"; ?>&nbsp;</label>
                                            </td>
                                        <?php else: ?>
                                            <td align="center">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </tr>

                                <tr>
                                    <td align="left">
                                        CARROCERIA:
                                    </td>
                                    <?php for ($x = 1; $x < 36; $x ++): ?>
                                        <?php if (($x % 6) != 0): ?>
                                            <td align="center" style="border-bottom:1px solid #337ab7;">
                                                <label class="respuesta">&nbsp;<?php if(isset($xcarroceria)) {if($xcarroceria[0][$x-1] != "") echo $xcarroceria[0][$x-1]; else echo "&nbsp;&nbsp;&nbsp;";} else echo "&nbsp;&nbsp;&nbsp;"; ?>&nbsp;</label>
                                            </td>
                                        <?php else: ?>
                                            <td align="center">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </tr>

                                <tr>
                                    <td align="left">
                                        CHASSIS:
                                    </td>
                                    <?php for ($x = 1; $x < 36; $x ++): ?>
                                        <?php if (($x % 6) != 0): ?>
                                            <td align="center" style="border-bottom:1px solid #337ab7;">
                                                <label class="respuesta">&nbsp;<?php if(isset($xchassis)) {if($xchassis[0][$x-1] != "") echo $xchassis[0][$x-1]; else echo "&nbsp;&nbsp;&nbsp;";} else echo "&nbsp;&nbsp;&nbsp;"; ?>&nbsp;</label>
                                            </td>
                                        <?php else: ?>
                                            <td align="center">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </tr>

                                <tr>
                                    <td align="left">
                                        INDEFINIDO:
                                    </td>
                                    <?php for ($x = 1; $x < 36; $x ++): ?>
                                        <?php if (($x % 6) != 0): ?>
                                            <td align="center" style="border-bottom:1px solid #337ab7;">
                                                <label class="respuesta">&nbsp;<?php if(isset($xindefinido)) {if($xindefinido[0][$x-1] != "") echo $xindefinido[0][$x-1]; else echo "&nbsp;&nbsp;&nbsp;";} else echo "&nbsp;&nbsp;&nbsp;"; ?>&nbsp;</label>
                                            </td>
                                        <?php else: ?>
                                            <td align="center">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </tr>

                                <tr>
                                    <td align="left">
                                        OTRO
                                    </td>
                                    <?php for ($x = 1; $x < 36; $x ++): ?>
                                        <?php if (($x % 6) != 0): ?>
                                            <td align="center" style="border-bottom:1px solid #337ab7;">
                                                <label class="respuesta">&nbsp;<?php if(isset($xotro)) {if($xotro[0][$x-1] != "") echo $xotro[0][$x-1]; else echo "&nbsp;&nbsp;&nbsp;";} else echo "&nbsp;&nbsp;&nbsp;"; ?>&nbsp;</label>
                                            </td>
                                        <?php else: ?>
                                            <td align="center">
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width:100%;border: 1px solid #337ab7;margin-left:10px;">
                    <tr>
                        <td rowspan="2" style="width:32%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align="center">
                            COMENTARIOS DEL MECÁNICO <br>
                            INCLUIDA LA DESCRIPCIÓN DE LA CAUDA DEL PROBLEMA
                        </td>
                        <td rowspan="2" style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align="center">
                            NÚMERO REP
                        </td>
                        <td rowspan="2" style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align="center">
                            CLAVE DE DEFECTO
                        </td>
                        <td colspan="4" style="border-bottom: 1px solid #337ab7; font-size: 10px;" align="center">
                            REGISTRO DE LABOR
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;">
                            RENTORNO  <br>DE PARTES <br>A VENTANILLA <br>BÁSICO/FECHA
                        </td>
                        <td align="center" style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;">
                            MECÁNICO <br>CLAVE
                        </td>
                        <td align="center" style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;">
                            COSTO O <br>TIEMPO UTILIZADO
                        </td>
                        <td align="center" style="width:13%; border-bottom: 1px solid #337ab7; font-size: 10px;">
                            RELOJ <br>(CHECADAS)
                        </td>
                    </tr>
                    <?php if (isset($xcontenidoLabor)): ?>
                        <?php foreach ($xcontenidoLabor as $index => $valor): ?>
                            <tr>
                                <td style="width:32%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'>
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][0]; ?></label>
                                </td>
                                <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'>
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][1]; ?></label>
                                </td>
                                <td  style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'>
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][2]; ?></label>
                                </td>
                                <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;">
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][3]; ?></label>
                                </td>
                                <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" rowspan='2'>
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][4]; ?></label>
                                </td>
                                <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" rowspan='2'>
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][5]; ?></label>
                                </td>
                                <td align='left' style="width:15%;border-bottom: 1px solid #337ab7; font-size: 10px;">
                                    INICIO <label class="respuesta"><?php echo $xcontenidoLabor[$index][6]; ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:32%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'>
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][7]; ?></label>
                                </td>
                                <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'>
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][8]; ?></label>
                                </td>
                                <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'>
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][9]; ?></label>
                                </td>
                                <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;">
                                    <label class="respuesta"><?php echo $xcontenidoLabor[$index][10]; ?></label>
                                </td>
                                <td align='left' style="width:13%; border-bottom: 1px solid #337ab7; font-size: 10px;">
                                    TERMINACIÓN <label class="respuesta"><?php echo $xcontenidoLabor[$index][11]; ?></label>
                                </td>
                            </tr>

                            <?php if (($index % 7) == 0): ?>
                                <?php break; ?>
                            <?php endif; ?>

                        <?php endforeach; ?>

                        <?php if (count($xcontenidoLabor) < 9): ?>
                            <?php for ($x = count($xcontenidoLabor); $x < 9 ; $x++): ?>
                                <tr>
                                    <td style="width:32%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                    <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                    <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                    <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;"><br></td>
                                    <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" rowspan='2'><br></td>
                                    <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" rowspan='2'><br></td>
                                    <td align='left' style="width:15%;border-bottom: 1px solid #337ab7; font-size: 10px;"></td>
                                </tr>
                                <tr>
                                    <td style="width:32%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                    <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                    <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                    <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;"><br></td>
                                    <td align='left' style="width:13%; border-bottom: 1px solid #337ab7; font-size: 10px;"></td>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php for ($x = 0; $x < 9 ; $x++): ?>
                            <tr>
                                <td style="width:32%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;"><br></td>
                                <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" rowspan='2'><br></td>
                                <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" rowspan='2'><br></td>
                                <td align='left' style="width:15%;border-bottom: 1px solid #337ab7; font-size: 10px;"></td>
                            </tr>
                            <tr>
                                <td style="width:32%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                <td style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;" align='center'><br></td>
                                <td align='center' style="width:10%;border-right: 1px solid #337ab7; border-bottom: 1px solid #337ab7; font-size: 10px;"><br></td>
                                <td align='left' style="width:13%; border-bottom: 1px solid #337ab7; font-size: 10px;"></td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif; ?>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width:100%;">
                    <tr>
                        <td style="width:15%;"></td>
                        <td style="width:30%;" align="center">
                            <?php if (isset($firma1)): ?>
                                <?php if ($firma1 != ""): ?>
                                    <img class='marcoImg' src='<?php echo base_url().$firma1; ?>' id='firma1' style='width:120px;height:40px;'>
                                <?php else: ?>
                                    <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma1' style='width:120px;height:40px;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma1' style='width:120px;height:40px;'>
                            <?php endif; ?>
                            <br>
                            <label class="respuesta"><?php if(isset($nombre1)) echo $nombre1; else echo ""; ?></label>
                        </td>
                        <td style="width:30%;" align="center">
                            <?php if (isset($firma2)): ?>
                                <?php if ($firma2 != ""): ?>
                                    <img class='marcoImg' src='<?php echo base_url().$firma2; ?>' id='firma1' style='width:120px;height:40px;'>
                                <?php else: ?>
                                    <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma1' style='width:120px;height:40px;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma1' style='width:120px;height:40px;'>
                            <?php endif; ?>
                            <br>
                            <label class="respuesta"><?php if(isset($nombre2)) echo $nombre2; else echo ""; ?></label>
                        </td>
                        <td style="width:10%;"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</page>

<?php 
    use Spipu\Html2Pdf\Html2Pdf;

    //Orientación  de la hoja P->Vertical   L->Horizontal
    $html2pdf = new Html2Pdf('L', 'A4', 'en',TRUE,'UTF-8',NULL);
    
    try {

        $html = ob_get_clean();

        ob_clean();
        /* Limpiamos la salida del búfer y lo desactivamos */
        //ob_end_clean();

        $html2pdf->setDefaultFont('Helvetica');     //Helvetica
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($html);
        $html2pdf->setTestTdInOnePage(FALSE);
        $html2pdf->Output();

    } catch (Html2PdfException $e) {
        $html2pdf->clean();
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }

?>