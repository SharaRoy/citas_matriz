<section style="padding: 20px 20px 20px 20px;">
    <!-- <div class="row" style="background-color:white;"> -->
    <div class="row">
        <div class="col-sm-4" align="left" style="">
            <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="margin-left: 20px;width:280px;height:60px;">
        </div>
        <div class="col-sm-8" align="center" style="margin-top:20px;">
            <h4>Formulario Garantía F1863</h4>
        </div>
    </div>

    <!-- Alerta para proceso del registro -->
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Fallo el proceso.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Campos incompletos.</strong>
    </div>

    <form name="" method="post" action="<?php echo base_url()."garantias/Operaciones2/validateForm";?>" style="margin:15px; border: 1px solid;">
        <div class="row" >
            <div class="col-sm-12" align="center">
                (REP) NÚMERO DE REPARACIÓN, (MIL) LUZ INDICADORA DE LA FALLA P-MIL, (DTC) CÓDIGO DE FALLA
            </div>
        </div>

        <br>
        <div class="row" class="table-responsive">
            <div class="col-sm-3">
                <table style="width:100%;margin-left:10px;">
                    <tr>
                        <td colspan="2" align="center" style="width:30%;">REP N°</td>
                        <td align="center" style="width:70%;"> LUZ INDICADORA DE <br>FALLA ENCENDIDA (S/N) </td>
                    </tr>
                    <?php for ($x = 1; $x < 8; $x ++): ?>
                      <tr>
                          <td>
                              <input type="text" class="input_field" name="rep_1_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value="<?php echo set_value("rep_1_".$x); ?>" style="width:0.7cm;border-bottom: 1px solid;">
                          </td>
                          <td>
                              <input type="text" class="input_field" name="rep_2_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value="<?php echo set_value("rep_2_".$x); ?>" style="width:0.7cm;border-bottom: 1px solid;">
                          </td>
                          <td align="center">
                              <input type="text" class="input_field" name="luz_falla_<?php echo $x; ?>"  maxlength="1" value="<?php echo set_value("luz_falla_".$x); ?>" style="width:1.5cm;border-bottom: 1px solid;">
                          </td>
                      </tr>
                    <?php endfor; ?>
                </table>
            </div>

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-6">
                        DTC <br> TREN MOTRIZ
                    </div>
                    <div class="col-sm-6" align="right">
                        No. Orden:
                        <input type="text" class='input_field' name="idOrden" value="<?php if(set_value("idOrden") != ""){ echo set_value("idOrden");} else {if(isset($idOrden)) echo $idOrden;} ?>" style="width:2cm;">
                        <br>
                        <?php echo form_error('idOrden', '<span class="error">', '</span>'); ?>

                        <br>
                        Folio Intelisis:&nbsp;
                        <label style="color:darkblue;">
                            <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                        </label>
                    </div>
                </div>

                <table style="width:100%;margin-right:10px;">
                    <tr>
                        <td align="right">
                            KOEO
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="koeo_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php echo set_value("koeo_".$x); ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="right">
                            KOEC
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="koec_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php echo set_value("koec_".$x); ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="right">
                            KOER
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="koer_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php echo set_value("koer_".$x); ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="left">
                            CARROCERIA:
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="carroceria_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value="<?php echo set_value("carroceria_".$x); ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="left">
                            CHASSIS:
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="chassis_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value="<?php echo set_value("chassis_".$x); ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="left">
                            INDEFINIDO:
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="indefinido_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php echo set_value("indefinido_".$x); ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="left">
                            OTRO
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="otro_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php echo set_value("otro_".$x); ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>
                </table>
            </div>
        </div>

        <br><br>
        <div class="row" class="table-responsive">
            <div class="col-sm-4"></div>
            <div class="col-sm-6" align="right">
                <label for="" style="color:#005dff;"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                <br>
                <label for="" style="color:#005dff;"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
            </div>
            <div class="col-sm-2" align="right">
              <a id="agregarT1" class="btn btn-success" style="color:white;">
                  <i class="fas fa-plus"></i>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a id="eliminarT1" class="btn btn-danger" style="color:white; width: 1cm;">
                  <i class="fas fa-minus"></i>
              </a>
            </div>
        </div>
        <div class="row" class="table-responsive">
            <div class="col-sm-12">
                <table style="width:100%;">
                    <thead>
                        <tr>
                            <td rowspan="2" style="width:30%;border:1px solid;" align="center">
                                COMENTARIOS DEL MECÁNICO <br>
                                INCLUIDA LA DESCRIPCIÓN DE LA CAUDA DEL PROBLEMA
                            </td>
                            <td rowspan="2"style="width:10%;border:1px solid;" align="center">
                                NÚMERO REP
                            </td>
                            <td rowspan="2" style="width:10%;border:1px solid;" align="center">
                                CLAVE DE DEFECTO
                            </td>
                            <td colspan="4" style="border:1px solid;" align="center">
                                REGISTRO DE LABOR
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width:10%;border:1px solid;">
                                RENTORNO  <br>DE PARTES <br>A VENTANILLA <br>BÁSICO/FECHA
                            </td>
                            <td align="center" style="width:10%;border:1px solid;">
                                MECÁNICO <br>CLAVE
                            </td>
                            <td align="center" style="width:10%;border:1px solid;">
                                COSTO O <br>TIEMPO UTILIZADO
                            </td>
                            <td align="center" style="width:10%;border:1px solid;">
                                RELOJ <br>(CHECADAS)
                                <input type="hidden" id="indice" name="" value="1">
                            </td>
                        </tr>
                    </thead>

                    <tbody id="cuerpoProblema">
                        <tr id='fila_a_1'>
                            <td style='width:30%;border:1px solid;' align='center'>
                                <input type='text' class='input_field' id='cometario_a_1' onblur='comentario_A(1)'  value='' style='width:100%;'>
                            </td>
                            <td style='width:10%;border:1px solid;' align='center'>
                                <input type='text' class='input_field' id='ref_a_1' onblur='ref_A(1)'  value='' style='width:100%;'>
                            </td>
                            <td  style='width:10%;border:1px solid;' align='center'>
                                <input type='text' class='input_field' id='clave_a_1' onblur='clave_A(1)'  value='' style='width:100%;'>
                            </td>
                            <td align='center' style='width:10%;border:1px solid;'>
                                <input type='date' class='input_field' id='fecha_a_1' onblur='retorno_A(1)'  value='<?php echo $hoy; ?>' max="<?php echo $hoy; ?>" min="<?php echo $hoy; ?>" style='width:100%;'>
                            </td>
                            <td align='center' style='width:10%;border:1px solid;' rowspan='2'>
                                <input type='text' class='input_field' id='mecanico_1' onblur='mecanico(1)'  value='' style='width:80%;'>
                            </td>
                            <td align='center' style='width:10%;border:1px solid;' rowspan='2'>
                                <input type='text' class='input_field' id='costo_1' onblur='costo(1)'  value='' style='width:80%;'>
                            </td>
                            <td align='left' style='width:15%;border:1px solid;'>
                                INICIO <input type='text' class='input_field' id='tinicio_a_1' onblur='tInicio(1)'  value='<?php echo $hora; ?>' max="<?php echo $hora; ?>" min="<?php echo $hora; ?>" style='width:60%;'>
                            </td>
                        </tr>
                        <tr id='fila_b_1'>
                            <td style='width:30%;border:1px solid;' align='center'>
                                <input type='text' class='input_field' id='cometario_b_1' onblur='comentario_B(1)'  value='' style='width:100%;'>
                            </td>
                            <td style='width:10%;border:1px solid;' align='center'>
                                <input type='text' class='input_field' id='ref_b_1' onblur='ref_B(1)'  value='' style='width:100%;'>
                            </td>
                            <td  style='width:10%;border:1px solid;' align='center'>
                                <input type='text' class='input_field' id='clave_b_1' onblur='clave_B(1)'  value='' style='width:100%;'>
                            </td>
                            <td align='center' style='width:10%;border:1px solid;'>
                                <input type='date' class='input_field' id='fecha_b_1' onblur='retorno_B(1)'  value='<?php echo $hoy; ?>' max="<?php echo $hoy; ?>" min="<?php echo $hoy; ?>" style='width:100%;'>
                            </td>
                            <td align='left' style='width:15%;border:1px solid;'>
                                TERMINACIÓN  <input type='text' class='input_field' id='tinicio_b_1' onblur='tFinal(1)'  value='<?php echo $hora; ?>' max="<?php echo $hora; ?>" min="<?php echo $hora; ?>" style='width:1.5cm;'>
                                <input type='hidden' id='renglon_1' name='contenedor[]' value=''>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-3" align="center">
                <input type='hidden' id='rutaFirma1' name='rutaFirma1' value='<?php echo set_value("rutaFirma1"); ?>'>
                <img class='marcoImg' src='<?php if(set_value("rutaFirma1") != "") echo set_value("rutaFirma1"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma1' style='width:160px;height:70px;'>
                <br>
                <input type="text" class='input_field' name="nombre1" value="<?php echo set_value("nombre1"); ?>">
                <br>
                <?php echo form_error('rutaFirma1', '<span class="error">', '</span>'); ?>
                <?php echo form_error('nombre1', '<span class="error">', '</span>'); ?>
                <br>
                <a id="firma_1" class='cuadroFirma btn btn-primary' data-value="Firma1" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                    <!-- <i class='fas fa-pen-fancy'></i>  -->
                    Firma
                </a>
            </div>
            <div class="col-sm-3" align="center">
                <input type='hidden' id='rutaFirma2' name='rutaFirma2' value='<?php echo set_value("rutaFirma2"); ?>'>
                <img class='marcoImg' src='<?php if(set_value("rutaFirma2") != "") echo set_value("rutaFirma2"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma2' style='width:160px;height:70px;'>
                <br>
                <input type="text" class='input_field' name="nombre2" value="<?php echo set_value("nombre2"); ?>">
                <br>
                <?php echo form_error('rutaFirma2', '<span class="error">', '</span>'); ?>
                <?php echo form_error('nombre2', '<span class="error">', '</span>'); ?>
                <br>
                <a id="firma_2" class='cuadroFirma btn btn-primary' data-value="Firma2" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                    <!-- <i class='fas fa-pen-fancy'></i>  -->
                    Firma
                </a>
            </div>
            <div class="col-sm-3"></div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" align="center">
                <input type="hidden" name="modoVistaFormulario" id="" value="1">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <button type="submit" class="btn btn-success" name="" id="" >Guardar</button>
            </div>
            <div class="col-sm-4"></div>
        </div>

    </form>
</section>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
