<!-- <section class="" style="margin:30px;color:#340f7b;"> -->
<section class="">
    <!-- <div class="row" style="background-color:white;"> -->
    <form name="" method="post" action="<?php echo base_url()."garantias/Operaciones/validateForm";?>">
        <div class="row">
            <div class="col-sm-9" align="center" style="">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:280px;height:60px;">
                <a id="superUsuarioA" class="h2_title_blue" data-target="#superUsuario" data-toggle="modal" style="color:blue;margin-top:15px;cursor: pointer;">
                    <i class="fas fa-user"></i>
                </a>
            </div>
            <div class="col-sm-3" align="center" style="padding-top:20px;">
                No. Orden:
                <input type="text" class='input_field' name="idOrden" value="<?php if(set_value("idOrden") != ""){ echo set_value("idOrden");} else {if(isset($idOrden)) echo $idOrden;} ?>" style="width:2cm;">
                <br>
                <?php echo form_error('idOrden', '<span class="error">', '</span>'); ?>

                <br>
                Folio Intelisis:&nbsp;
                <label style="color:darkblue;">
                    <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 table-responsive">
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">REP</td>
                        <td align="center">GASTOS MISC.</td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            <input type="text" class="input_field" name="repPrestamo" value="<?php if(set_value("repPrestamo") != "") echo set_value("repPrestamo"); else if(isset($repGastosMisc)) echo $repGastosMisc[0]; ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('repPrestamo', '<span class="error">', '</span>'); ?>
                        </td>
                        <td>
                            PRESTAMO / RENTA DE AUTO: DÍAS <input type="text" class="input_field" name="dias" value="<?php if(set_value("dias") != "") echo set_value("dias"); else if(isset($diasGM)) echo $diasGM; ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('dias', '<span class="error">', '</span>'); ?>
                            PRECIO: <input type="text" class="input_field" name="precio" value="<?php if(set_value("precio") != "") echo set_value("precio"); else if(isset($precioGM)) echo $precioGM; ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('precio', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            <input type="text" class="input_field" name="repPagoAlCliente" value="<?php if(set_value("repPagoAlCliente") != "") echo set_value("repPagoAlCliente"); else if(isset($repGastosMisc)) echo $repGastosMisc[1]; ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('repPagoAlCliente', '<span class="error">', '</span>'); ?>
                        </td>
                        <td>
                            PAGO AL CLIENTE CANT. <input type="text" class="input_field" name="pagoAlCliente" value="<?php if(set_value("pagoAlCliente") != "") echo set_value("pagoAlCliente"); else if(isset($pagoClienteGM)) echo $pagoClienteGM; ?>" style="width:3cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('pagoAlCliente', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            <input type="text" class="input_field" name="repGrua" value="<?php if(set_value("repGrua") != "") echo set_value("repGrua"); else if(isset($repGastosMisc)) echo $repGastosMisc[2]; ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('repGrua', '<span class="error">', '</span>'); ?>
                        </td>
                        <td>
                            GRUA: CANT.  <input type="text" class="input_field" name="grua" value="<?php if(set_value("grua") != "") echo set_value("grua"); else if(isset($gruaGM)) echo $gruaGM; ?>" style="width:3cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('grua', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            <input type="text" class="input_field" name="repOtros" value="<?php if(set_value("repOtros") != "") echo set_value("repOtros"); else if(isset($repGastosMisc)) echo $repGastosMisc[3]; ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('repOtros', '<span class="error">', '</span>'); ?>
                        </td>
                        <td>
                            OTROS: CANT.  <input type="text" class="input_field" name="otros" value="<?php if(set_value("otros") != "") echo set_value("otros"); else if(isset($otrosGM)) echo $otrosGM; ?>" style="width:3cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('otros', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="2">
                                        <br>
                                        <p align="justify" style="text-align:justify;margin-left:10px;margin-right:10px;font-size:10px;">
                                            El automóvil aquí descripto queda depositado en sus talleres para los trabajos que por garantía he solicitado. Autorizo el uso del vehículo dentro de los limites de
                                            ciudad para usos de prueba o inspección de las reparaciones señaladas. Estoy de acuerdo en que la empresa no se hace responsable por daños al automóvil por causas de
                                            fuerza mayor o por perdidas de artículos olvidados en el vehículo  y no reportadas o entregadas a la Gerencia de Servicio para su custodia.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50%;font-size:10px;" align="center">
                                        <?php if (isset($firmaCliente)): ?>
                                            <?php if ($firmaCliente != ""): ?>
                                                <img class="marcoImg" src="<?php echo base_url().$firmaCliente; ?>" alt="" style="width:130px;height:40px;">
                                            <?php else: ?>
                                                <img class="marcoImg" src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:130px;height:40px;">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img class="marcoImg" src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:130px;height:40px;">
                                        <?php endif; ?>
                                        <!-- <input type="hidden" id="rutaFirmaCliente" name="rutaFirmaCliente" value="<?= set_value('rutaFirmaCliente');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaCliente');?>" id="firmaImgCliente" alt="" style="width:130px;height:40px;">
                                        <a class="cuadroFirma btn btn-primary" data-value="Cliente" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                            Firmar
                                        </a> -->
                                        <br>
                                        FIRMA DEL CLIENTE
                                    </td>
                                    <td style="width:50%;font-size:10px;" align="center">
                                        <?php if (isset($firmaGerente)): ?>
                                            <?php if ($firmaGerente != ""): ?>
                                                <img class="marcoImg" src="<?php echo base_url().$firmaGerente; ?>" alt="" style="width:130px;height:40px;">
                                            <?php else: ?>
                                                <img class="marcoImg" src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:130px;height:40px;">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img class="marcoImg" src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:130px;height:40px;">
                                        <?php endif; ?>
                                        <!-- <input type="hidden" id="rutaFirmaGerente" name="rutaFirmaGerente" value="<?= set_value('rutaFirmaGerente');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaGerente');?>" id="firmaImgGerente" alt="" style="width:130px;height:40px;">
                                        <a class="cuadroFirma btn btn-primary" data-value="Gerente" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                            Firmar
                                        </a> -->
                                        <br>
                                        FIRMA GERENTE DE SERVICIO(fecha).
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-5 table-responsive">
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            IDENTIFICACIÓN <br>ASESOR DE SERVICIO
                            <br>
                            <input type="text" class="input_field" name="idAsesor" value="<?php if(set_value("idAsesor") != "") echo set_value("idAsesor"); else if(isset($idAsesor)) echo $idAsesor; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('idAsesor', '<span class="error">', '</span>'); ?>
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            PLACAS
                            <br><br>
                            <input type="text" class="input_field" name="placas" value="<?php if(set_value("placas") != "") echo set_value("placas"); else if(isset($placas)) echo $placas; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('placas', '<span class="error">', '</span>'); ?>
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            AÑO
                            <br><br>
                            <input type="text" class="input_field" name="anio" value="<?php if(set_value("anio") != "") echo set_value("anio"); else if(isset($anio)) echo $anio; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('anio', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            TIPO
                            <br><br>
                            <input type="text" class="input_field" name="tipo" value="<?php if(set_value("tipo") != "") echo set_value("tipo"); else if(isset($tipo)) echo $tipo; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('tipo', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            HORA DE RECIBO
                            <br>
                            <input type="text" class="input_field" name="horaRecibo"  value="<?php if(set_value("horaRecibo") != "") echo set_value("horaRecibo"); else if(isset($horaRecibo)) echo $horaRecibo; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('horaRecibo', '<span class="error">', '</span>'); ?>
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            HORA PROMEDIO
                            <br>
                            <input type="text" class="input_field" name="horaPromedio" value="<?php if(set_value("horaPromedio") != "") echo set_value("horaPromedio"); else if(isset($horaPromedio)) echo $horaPromedio; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('horaPromedio', '<span class="error">', '</span>'); ?>
                        </td>
                        <td colspan="2" align="center">
                            TELEFONO
                            <br>
                            <input type="text" class="input_field" name="telefono" value="<?php if(set_value("telefono") != "") echo set_value("telefono"); else if(isset($telefono)) echo $telefono; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('telefono', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
                <br><br>
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            NOMBRE DEL PROPIETARIO
                            <br>
                            <input type="text" class="input_field" name="nombreProp" value="<?php if(set_value("nombreProp") != "") echo set_value("nombreProp"); else if(isset($nombreProp)) echo $nombreProp; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('nombreProp', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            CLIENTE VISITANTE
                            <br>
                            <input type="text" class="input_field" name="clienteVisita" value="<?php if(set_value("clienteVisita") != "") echo set_value("clienteVisita"); else if(isset($clienteVisita)) echo $clienteVisita; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('clienteVisita', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td colspan="2" align="center">
                            DIRECCIÓN
                            <br>
                            <input type="text" class="input_field" name="direccion" value="<?php if(set_value("direccion") != "") echo set_value("direccion"); else if(isset($domicilio)) echo $domicilio; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('direccion', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center">
                            CIUDAD/ESTADO
                            <br>
                            <input type="text" class="input_field" name="estado" value="<?php if(set_value("estado") != "") echo set_value("estado"); else if(isset($estado)) echo $estado; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('estado', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            CÓDIGO POSTAL
                            <br>
                            <input type="text" class="input_field" maxlength="6" name="cPostal" value="<?php if(set_value("cPostal") != "") echo set_value("cPostal"); else if(isset($cp)) echo $cp; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('cPostal', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td colspan="2" align="center">
                            IDENTIFICACIÓN DEL VEHÍCULO/NÚMERO DE SERIE
                            <br>
                            <table style="width:100%;">
                                <tr>
                                    <?php if (isset($idVehiculo)): ?>
                                        <?php for ($i = 0; $i <count($idVehiculo)-1 ;$i++): ?>
                                            <td style="border-right: 1px solid #340f7b;">
                                                <input type="text" maxlength="1" class="input_field" name="idVehiculo_<?php echo $i+1; ?>" value="<?php if(set_value('idVehiculo_'.($i+1)) != "") echo set_value('idVehiculo_'.($i+1)); else if(isset($idVehiculo)) echo $idVehiculo[$i]; ?>" style="width:80%;text-align:center;color:#340f7b;">
                                                <?php echo form_error('idVehiculo_'.($i+1), '<span class="error">', '</span>'); ?>
                                            </td>
                                        <?php endfor; ?>
                                    <?php else: ?>
                                        <?php for ($i = 0; $i <17 ;$i++): ?>
                                            <td style="border-right: 1px solid #340f7b;">
                                                <input type="text" maxlength="1" class="input_field" name="idVehiculo_<?php echo $i+1; ?>" value="<?php echo set_value("idVehiculo_".($i+1)); ?>" style="width:80%;text-align:center;color:#340f7b;">
                                                <?php echo form_error('idVehiculo_'.($i+1), '<span class="error">', '</span>'); ?>
                                            </td>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-3 table-responsive">
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center" rowspan="2">
                            <br>
                            <input type="text" class="input_field" name="noTorre" value="<?php if(set_value("noTorre") != "") echo set_value("noTorre"); else if(isset($noTorre)) echo $noTorre; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('noTorre', '<span class="error">', '</span>'); ?>
                            <br><br>
                            TORRE No.
                        </td>
                        <td>
                            FOLIO:
                            <br>
                            <input type="text" class="input_field" name="folio" value="<?php if(set_value("folio") != "") echo set_value("folio"); else if(isset($folio)) echo $folio; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('folio', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td>
                            CONTINUACIÓN FOLIO
                            <br>
                            <input type="text" class="input_field" name="folioContinue" value="<?php if(set_value("folioContinue") != "") echo set_value("folioContinue"); else if(isset($folioContinue)) echo $folioContinue; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('folioContinue', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>

                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center" colspan="2">
                            FECHA Y KILOMETRAJE AL RECIBIR EL VEHÍCULO
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            FECHA:
                            <table>
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesRecibe" value="<?php if(set_value("mesRecibe") != "") echo set_value("mesRecibe"); else if(isset($fechaRecibe)) echo $fechaRecibe[1]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('mesRecibe', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaRecibe" value="<?php if(set_value("diaRecibe") != "") echo set_value("diaRecibe"); else if(isset($fechaRecibe)) echo $fechaRecibe[0]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('diaRecibe', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioRecibe" value="<?php if(set_value("anioRecibe") != "") echo set_value("anioRecibe"); else if(isset($fechaRecibe)) echo $fechaRecibe[2]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('anioRecibe', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            KM:<br><br>
                            <table>
                                <tr>
                                    <?php if (isset($kmRecibe)): ?>
                                        <?php for ($i = 0; $i <count($kmRecibe)-1 ;$i++): ?>
                                            <td style="border-right: 1px solid #340f7b;">
                                                <input type="text" maxlength="1" class="input_field" name="kmRecibe_<?php echo $i+1; ?>" value="<?php if(set_value('kmRecibe_'.($i+1)) != "") echo set_value('kmRecibe_'.($i+1)); else if(isset($kmRecibe)) echo $kmRecibe[$i]; ?>" style="width:80%;text-align:center;color:#340f7b;">
                                                <?php echo form_error('kmRecibe_'.($i+1), '<span class="error">', '</span>'); ?>
                                            </td>
                                        <?php endfor; ?>
                                    <?php else: ?>
                                        <?php for ($i = 0; $i <6 ;$i++): ?>
                                            <td style="border-right: 1px solid #340f7b;">
                                                <input type="text" maxlength="1" class="input_field" name="kmRecibe_<?php echo $i+1; ?>" value="<?php echo set_value("kmRecibe_".($i+1)); ?>" style="width:80%;text-align:center;color:#340f7b;">
                                                <?php echo form_error('kmRecibe_'.($i+1), '<span class="error">', '</span>'); ?>
                                            </td>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center" colspan="2">
                            FECHA Y KILOMETRAJE AL ENTREGAR EL VEHÍCULO
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            FECHA:
                            <table>
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesEntrega" value="<?php if(set_value("mesEntrega") != "") echo set_value("mesEntrega"); else if(isset($fechaEntrega)) echo $fechaEntrega[1]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('mesEntrega', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaEntrega" value="<?php if(set_value("diaEntrega") != "") echo set_value("diaEntrega"); else if(isset($fechaEntrega)) echo $fechaEntrega[0]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('diaEntrega', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioEntrega" value="<?php if(set_value("anioEntrega") != "") echo set_value("anioEntrega"); else if(isset($fechaEntrega)) echo $fechaEntrega[2]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('anioEntrega', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            KM:<br><br>
                            <table>
                                <tr>
                                    <?php if (isset($kmEntrega)): ?>
                                        <?php for ($i = 0; $i <count($kmEntrega)-1 ;$i++): ?>
                                            <td style="border-right: 1px solid #340f7b;">
                                                <input type="text" maxlength="1" class="input_field" name="kmEntrega_<?php echo $i+1; ?>" value="<?php if(set_value('kmEntrega_'.($i+1)) != "") echo set_value('kmEntrega_'.($i+1)); else if(isset($kmEntrega)) echo $kmEntrega[$i]; ?>" style="width:80%;text-align:center;color:#340f7b;">
                                                <?php echo form_error('kmEntrega_'.($i+1), '<span class="error">', '</span>'); ?>
                                            </td>
                                        <?php endfor; ?>
                                    <?php else: ?>
                                        <?php for ($i = 0; $i <6 ;$i++): ?>
                                            <td style="border-right: 1px solid #340f7b;">
                                                <input type="text" maxlength="1" class="input_field" name="kmEntrega_<?php echo $i+1; ?>" value="<?php echo set_value("kmEntrega_".($i+1)); ?>" style="width:80%;text-align:center;color:#340f7b;">
                                                <?php echo form_error('kmEntrega_'.($i+1), '<span class="error">', '</span>'); ?>
                                            </td>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            FECHA DE INICIO DE GARANTÍA
                            <table>
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesGarantia" value="<?php if(set_value("mesGarantia") != "") echo set_value("mesGarantia"); else if(isset($fechaGarantia)) echo $fechaGarantia[1]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.8cm;">
                                        <?php echo form_error('mesGarantia', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaGarantia" value="<?php if(set_value("diaGarantia") != "") echo set_value("diaGarantia"); else if(isset($fechaGarantia)) echo $fechaGarantia[0]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.8cm;">
                                        <?php echo form_error('diaGarantia', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioGarantia" value="<?php if(set_value("anioGarantia") != "") echo set_value("anioGarantia"); else if(isset($fechaGarantia)) echo $fechaGarantia[2]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('anioGarantia', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            N° DE DISTRIBUIDOR
                            <br>
                            <input type="text" class="input_field" name="noDistribuidor" value="<?php if(set_value("noDistribuidor") != "") echo set_value("noDistribuidor"); else if(isset($noDistribuidor)) echo $noDistribuidor; ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('noDistribuidor', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            FECHA DE REPARACIÓN:
                            <table style="font-size:8px;">
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesReparacion" value="<?php if(set_value("mesReparacion") != "") echo set_value("mesReparacion"); else if(isset($fechaReparacion)) echo $fechaReparacion[1]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.8cm;">
                                        <?php echo form_error('mesReparacion', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaReparacion" value="<?php if(set_value("diaReparacion") != "") echo set_value("diaReparacion"); else if(isset($fechaReparacion)) echo $fechaReparacion[0]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.8cm;">
                                        <?php echo form_error('diaReparacion', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioReparacion" value="<?php if(set_value("anioReparacion") != "") echo set_value("anioReparacion"); else if(isset($fechaReparacion)) echo $fechaReparacion[2]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('anioReparacion', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-10 table-responsive">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-6" align="right">
                        <label for="" style="color:#005dff;"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                        <br>
                        <label for="" style="color:#005dff;"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
                    </div>
                    <div class="col-sm-2" align="right">
                      <a id="agregarT1" class="btn btn-success" style="color:white;">
                          <i class="fas fa-plus"></i>
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a id="eliminarT1" class="btn btn-danger" style="color:white; width: 1cm;">
                          <i class="fas fa-minus"></i>
                      </a>
                    </div>
                </div>
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            REP<br>No.
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            TIPO DE GARANTÍA
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            DAÑOS EN RELACIÓN
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            AUTORIZ N°1
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            AUTORIZ N°2
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            PARTES <br>TOTAL $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            M DE OBRA <br>TOTAL $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            MISC <br>TOTAL $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            IVA $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            PARTICIPACIÓN <br>CLIENTE $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            PARTICIPACIÓN <br>DISTRIBUIDOR $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            REPARACIÓN <br>TOTAL $
                            <input type='hidden' id='indiceTGarantia' value='<?php if(isset($contenedor)) echo count($contenedor)+1; else echo "1"; ?>'>
                        </td>
                    </tr>
                    <tbody id="tabla_garantia">
                        <?php if (isset($contenedor)): ?>
                            <?php foreach ($contenedor as $index => $valor): ?>
                                <tr id='fila_reparacion_<?php echo $index+1; ?>' style='border: 1px solid #340f7b;'>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <?php echo $index+1; ?>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='garantia_<?php echo $index+1; ?>' onblur='garantiaT1(<?php echo $index+1; ?>)' value='<?php echo $contenedor[$index][0]; ?>' style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='reparacion_<?php echo $index+1; ?>' onblur="reparacionT1(<?php echo $index+1; ?>)" value='<?php echo $contenedor[$index][1]; ?>' style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='autoriz_n1_<?php echo $index+1; ?>' onblur="autoriz_n1T1(<?php echo $index+1; ?>)" value='<?php echo $contenedor[$index][2]; ?>' style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='autoriz_n2_<?php echo $index+1; ?>' onblur="autoriz_n2T1(<?php echo $index+1; ?>)" value='<?php echo $contenedor[$index][3]; ?>' style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='partes_<?php echo $index+1; ?>' onblur="autosuma_partes(<?php echo $index+1; ?>)" value='<?php echo number_format($contenedor[$index][4],2); ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='obra_<?php echo $index+1; ?>' onblur="autosuma_MObra(<?php echo $index+1; ?>)" value='<?php echo number_format($contenedor[$index][5],2); ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='misc_<?php echo $index+1; ?>' onblur="autosuma_misc(<?php echo $index+1; ?>)" value='<?php echo number_format($contenedor[$index][6],2); ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <label id='labelIva_<?php echo $index+1; ?>'><?php echo number_format($contenedor[$index][7],2); ?></label>
                                        <!-- <input type='text' class='input_field' id='iva_<?php echo $index+1; ?>' onblur="autosuma_iva(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'> -->
                                        <input type='hidden' class='input_field' id='iva_<?php echo $index+1; ?>' value='<?php echo number_format($contenedor[$index][7],2); ?>'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='cliente_<?php echo $index+1; ?>' onblur="autosuma_cliente(<?php echo $index+1; ?>)" value='<?php echo number_format($contenedor[$index][8],2); ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='distribuidor_<?php echo $index+1; ?>' onblur="autosuma_distribuidor(<?php echo $index+1; ?>)" value='<?php echo number_format($contenedor[$index][9],2); ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <label for="" id="labelTotal_<?php echo $index+1; ?>" ><?php echo number_format( $contenedor[$index][10],2); ?></label>
                                        <input type='hidden' id='total_<?php echo $index+1; ?>' value='<?php echo number_format( $contenedor[$index][10],2); ?>'>
                                        <input type='hidden' id='contenedor_<?php echo $index+1; ?>' name='contenedor[]' value='<?php  echo $contenedorColeccion[$index]; ?>'>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            <!-- Agregamos un renglon vacio para actualizar -->
                            <tr id='fila_reparacion_<?php echo count($contenedor)+1; ?>' style='border: 1px solid #340f7b;'>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    1
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='garantia_<?php echo count($contenedor)+1; ?>' onblur='garantiaT1(<?php echo count($contenedor)+1; ?>)' value='' style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='reparacion_<?php echo count($contenedor)+1; ?>' onblur="reparacionT1(<?php echo count($contenedor)+1; ?>)" value='' style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='autoriz_n1_<?php echo count($contenedor)+1; ?>' onblur="autoriz_n1T1(<?php echo count($contenedor)+1; ?>)" value='' style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='autoriz_n2_<?php echo count($contenedor)+1; ?>' onblur="autoriz_n2T1(<?php echo count($contenedor)+1; ?>)" value='' style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='partes_<?php echo count($contenedor)+1; ?>' onblur="autosuma_partes(<?php echo count($contenedor)+1; ?>)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='obra_<?php echo count($contenedor)+1; ?>' onblur="autosuma_MObra(<?php echo count($contenedor)+1; ?>)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='misc_<?php echo count($contenedor)+1; ?>' onblur="autosuma_misc(<?php echo count($contenedor)+1; ?>)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <label id='labelIva_<?php echo count($contenedor)+1; ?>'>0</label>
                                    <!-- <input type='text' class='input_field' id='iva_<?php echo count($contenedor)+1; ?>' onblur="autosuma_iva(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'> -->
                                    <input type='hidden' class='input_field' id='iva_<?php echo count($contenedor)+1; ?>' value='0'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='cliente_<?php echo count($contenedor)+1; ?>' onblur="autosuma_cliente(<?php echo count($contenedor)+1; ?>)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='distribuidor_<?php echo count($contenedor)+1; ?>' onblur="autosuma_distribuidor(<?php echo count($contenedor)+1; ?>)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <label for="" id="labelTotal_<?php echo count($contenedor)+1; ?>" >0</label>
                                    <input type='hidden' id='total_<?php echo count($contenedor)+1; ?>' value='0'>
                                    <input type='hidden' id='contenedor_<?php echo count($contenedor)+1; ?>' name='contenedor[]' value=''>
                                </td>
                            </tr>
                        <?php else: ?>
                            <tr id='fila_reparacion_1' style='border: 1px solid #340f7b;'>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    1
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='garantia_1' onblur='garantiaT1(1)' value='' style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='reparacion_1' onblur="reparacionT1(1)" value='' style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='autoriz_n1_1' onblur="autoriz_n1T1(1)" value='' style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='autoriz_n2_1' onblur="autoriz_n2T1(1)" value='' style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='partes_1' onblur="autosuma_partes(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='obra_1' onblur="autosuma_MObra(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='misc_1' onblur="autosuma_misc(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <label id='labelIva_1'>0</label>
                                    <!-- <input type='text' class='input_field' id='iva_1' onblur="autosuma_iva(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'> -->
                                    <input type='hidden' class='input_field' id='iva_1' value='0'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='cliente_1' onblur="autosuma_cliente(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='distribuidor_1' onblur="autosuma_distribuidor(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <label for="" id="labelTotal_1" >0</label>
                                    <input type='hidden' id='total_1' value='0'>
                                    <input type='hidden' id='contenedor_1' name='contenedor[]' value=''>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr style='border: 1px solid #340f7b;'>
                            <td style='border-right: 1px solid #340f7b;' align='right' colspan="5">
                                TOTALES
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <?php if (isset($totalPartes)): ?>
                                    <label for="" id="labelTotalPartes"><?php echo number_format($totalPartes,2); ?></label>
                                    <input type='hidden' class='input_field' id='TotalPartes' name="TotalPartes" value="<?php echo number_format($totalPartes,2); ?>">
                                <?php else: ?>
                                    <label for="" id="labelTotalPartes">0</label>
                                    <input type='hidden' class='input_field' id='TotalPartes' name="TotalPartes" value="0">
                                <?php endif; ?>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <?php if (isset($totalObra)): ?>
                                    <label for="" id="labelTotalObra"><?php echo number_format($totalObra,2); ?></label>
                                    <input type='hidden' class='input_field' id='TotalObra' name="TotalObra" value="<?php echo number_format($totalObra,2); ?>">
                                <?php else: ?>
                                    <label for="" id="labelTotalObra">0</label>
                                    <input type='hidden' class='input_field' id='TotalObra' name="TotalObra" value="0">
                                <?php endif; ?>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <?php if (isset($totalMisc)): ?>
                                    <label for="" id="labelTotalMisc"><?php echo number_format($totalMisc,2); ?></label>
                                    <input type='hidden' class='input_field' id='TotalMisc' name="TotalMisc" value="<?php echo number_format($totalMisc,2); ?>">
                                <?php else: ?>
                                    <label for="" id="labelTotalMisc">0</label>
                                    <input type='hidden' class='input_field' id='TotalMisc' name="TotalMisc" value="0">
                                <?php endif; ?>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <?php if (isset($totalIva)): ?>
                                    <label for="" id="labelTotalIva"><?php echo number_format($totalIva,2); ?></label>
                                    <input type='hidden' class='input_field' id='TotalIva' name="TotalIva" value="<?php echo number_format($totalIva,2); ?>">
                                <?php else: ?>
                                    <label for="" id="labelTotalIva">0</label>
                                    <input type='hidden' class='input_field' id='TotalIva' name="TotalIva" value="0">
                                <?php endif; ?>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <?php if (isset($totalCliente)): ?>
                                    <label for="" id="labelTotalCliente"><?php echo number_format($totalCliente,2); ?></label>
                                    <input type='hidden' class='input_field' id='TotalCliente' name="TotalCliente" value="<?php echo number_format($totalCliente,2); ?>">
                                <?php else: ?>
                                    <label for="" id="labelTotalCliente">0</label>
                                    <input type='hidden' class='input_field' id='TotalCliente' name="TotalCliente" value="0">
                                <?php endif; ?>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <?php if (isset($totalDistribuidor)): ?>
                                    <label for="" id="labelTotalDistribuidor"><?php echo number_format($totalDistribuidor,2); ?></label>
                                    <input type='hidden' class='input_field' id='TotalDistribuidor' name="TotalDistribuidor" value="<?php echo number_format($totalDistribuidor,2); ?>">
                                <?php else: ?>
                                    <label for="" id="labelTotalDistribuidor">0</label>
                                    <input type='hidden' class='input_field' id='TotalDistribuidor' name="TotalDistribuidor" value="0">
                                <?php endif; ?>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <?php if (isset($totalReparacion)): ?>
                                    <label for="" id="labelTotalReparacion"><?php echo number_format($totalReparacion,2); ?></label>
                                    <input type='hidden' class='input_field' id='TotalReparacion' name="TotalReparacion" value="<?php echo number_format($totalReparacion,2); ?>">
                                <?php else: ?>
                                    <label for="" id="labelTotalReparacion">0</label>
                                    <input type='hidden' class='input_field' id='TotalReparacion' name="TotalReparacion" value="0">
                                <?php endif; ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="col-sm-2 table-responsive">
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center" rowspan="3">
                            VERIFICACIÓN OASIS
                            <br><br>
                            <input type="text" class="input_field" name="oasis" value="<?php echo set_value("oasis"); ?>" style="width:100%;text-align:center;color:#340f7b;">
                            <?php echo form_error('oasis', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            GTIA. DE REFACCIONES
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center">
                            FECHA DE INST. P VTA. DE LA PARTE
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center">
                            <table style="width:100%;">
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesVta" value="<?php if(set_value("mesVta") != "") echo set_value("mesVta"); else if(isset($fechaVta)) echo $fechaVta[1]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.7cm;">
                                        <?php echo form_error('mesVta', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaVta" value="<?php if(set_value("diaVta") != "") echo set_value("diaVta"); else if(isset($fechaVta)) echo $fechaVta[0]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.7cm;">
                                        <?php echo form_error('diaVta', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioVta" value="<?php if(set_value("anioVta") != "") echo set_value("anioVta"); else if(isset($fechaVta)) echo $fechaVta[2]; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.7cm;">
                                        <?php echo form_error('anioVta', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center" colspan="2">
                            TOT.KMS TRANSC. DE LA INST. O VTA
                            <br>
                            <input type="text" class="input_field" name="totKms" value="<?php if(set_value("totKms") != "") echo set_value("totKms"); else if(isset($kmsVta)) echo $kmsVta; ?>" style="width:100%;text-align:center;color:#340f7b;">
                            <?php echo form_error('totKms', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center" colspan="2">
                            N° REF/DOC. ORIGINA/REMISIÓN
                            <br>
                            <input type="text" class="input_field" name="noRefDoc" value="<?php if(set_value("noRefDoc") != "") echo set_value("noRefDoc"); else if(isset($noRefDoc)) echo $noRefDoc; ?>" style="width:100%;text-align:center;color:#340f7b;">
                            <?php echo form_error('noRefDoc', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row">
              <div class="col-sm-12 table-responsive">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-6" align="right">
                        <label for="" style="color:#005dff;"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                        <br>
                        <label for="" style="color:#005dff;"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
                    </div>
                    <div class="col-sm-2" align="right">
                      <a id="agregarT2" class="btn btn-success" style="color:white;">
                          <i class="fas fa-plus"></i>
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a id="eliminarT2" class="btn btn-danger" style="color:white; width: 1cm;">
                          <i class="fas fa-minus"></i>
                      </a>
                      <input type="hidden" id="indice_tabla_partes" value="<?php if(isset($contenidoPartes)) echo count($contenidoPartes)+1; else echo "1"; ?>">
                    </div>
                </div>

                <table style="width:100%;border: 1px solid #340f7b;">
                    <thead>
                        <tr style="border: 1px solid #340f7b;">
                            <td style="border-right: 1px solid #340f7b;width:67%;" align="center" colspan="12">
                                <h4 align="center">PARTES Y OPERCACIONES</h4>
                            </td>
                            <td colspan="3" style="background-color:#cbcbec;width:33%;">
                                <p align="justify" style="text-align:justify;margin-left:10px;margin-right:10px;font-size:9px;">
                                    Con el fin de recibir mas y mejores servicios, autorizo a <strong><?=SUCURSAL ?>, S.A. DE C.V.</strong> para el
                                    envío de mensajes de texto y llamadas a los números y/o correos electrónicos que brindo, para notificaciones y seguimiento.
                                </p>
                            </td>
                        </tr>
                        <tr style="border: 1px solid #340f7b;">
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                REP <br>N°
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                PREFIJO
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                BÁSICO
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                SUFIJO
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                NOMBRE
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                CANTIDAD
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                PRECIO <br>C/U
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                IMPORTE <br>PARTES
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                CLAVE <br>DETECTO
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                OPERACIÓN N°
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                TIEMPO <br>TARIFA
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                IMPORTE <br>MANO DE OBRA
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                MECANICO <br>CLAVE
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                DESCRIPCIÓN QUEJA DEL CLIENTE
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                CÓDIGO
                            </td>
                        </tr>
                    </thead>
                    <tbody id="tabla_partes">
                        <!-- Verificamos que exista la variable -->
                        <?php if (isset($contenidoPartes)): ?>
                            <?php foreach ($contenidoPartes as $index => $valor): ?>
                                <tr id='fila_partes_<?php echo $index+1; ?>' style='border: 1px solid #340f7b;'>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <?php echo $index+1; ?>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='prefijo_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][0]; ?>' onblur='prefijoT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='basico_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][1]; ?>' onblur='basicoT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='sufijo_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][2]; ?>' onblur='sufijoT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='nombre_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][3]; ?>' onblur='nombreT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][4]; ?>' onblur='cantidadT2(<?php echo $index+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='precio_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][5]; ?>' onblur='precioT2(<?php echo $index+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='importePartes_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][6]; ?>' onblur='importePartesT2(<?php echo $index+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='clave_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][7]; ?>' onblur='claveT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='operacion_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][8]; ?>' onblur='operacionT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='tiempo_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][9]; ?>' onblur='tiempoT2(<?php echo $index+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='importeObra_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][10]; ?>' onblur='importeObraT2(<?php echo $index+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='mecanico_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][11]; ?>' onblur='mecanicoT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='descripcion_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][12]; ?>' onblur='descripcionT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                    </td>
                                    <td style='border-right: 1px solid #340f7b;' align='center'>
                                        <input type='text' class='input_field' id='codigo_<?php echo $index+1; ?>' value='<?php echo $contenidoPartes[$index][13]; ?>' onblur='codigoT2(<?php echo $index+1; ?>)' style='width:100%;'>
                                        <input type='hidden' name='contenido[]' id='contenido_<?php echo $index+1; ?>' value='<?php echo $contenidoPartesColeccion[$index]; ?>'>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            <!-- Creamos un renglon vacio para edicion -->
                            <tr id='fila_partes_<?php echo count($contenidoPartes)+1; ?>' style='border: 1px solid #340f7b;'>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <?php echo count($contenidoPartes)+1; ?>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='prefijo_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='prefijoT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='basico_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='basicoT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='sufijo_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='sufijoT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='nombre_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='nombreT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='cantidad_<?php echo count($contenidoPartes)+1; ?>' value='0' onblur='cantidadT2(<?php echo count($contenidoPartes)+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='precio_<?php echo count($contenidoPartes)+1; ?>' value='0' onblur='precioT2(<?php echo count($contenidoPartes)+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='importePartes_<?php echo count($contenidoPartes)+1; ?>' value='0' onblur='importePartesT2(<?php echo count($contenidoPartes)+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='clave_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='claveT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='operacion_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='operacionT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='tiempo_<?php echo count($contenidoPartes)+1; ?>' value='0' onblur='tiempoT2(<?php echo count($contenidoPartes)+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='importeObra_<?php echo count($contenidoPartes)+1; ?>' value='0' onblur='importeObraT2(<?php echo count($contenidoPartes)+1; ?>)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='mecanico_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='mecanicoT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='descripcion_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='descripcionT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='codigo_<?php echo count($contenidoPartes)+1; ?>' value='' onblur='codigoT2(<?php echo count($contenidoPartes)+1; ?>)' style='width:100%;'>
                                    <input type='hidden' name='contenido[]' id='contenido_<?php echo count($contenidoPartes)+1; ?>' value=''>
                                </td>
                            </tr>
                        <?php else: ?>
                            <tr id='fila_partes_1' style='border: 1px solid #340f7b;'>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    1
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='prefijo_1' value='' onblur='prefijoT2(1)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='basico_1' value='' onblur='basicoT2(1)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='sufijo_1' value='' onblur='sufijoT2(1)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='nombre_1' value='' onblur='nombreT2(1)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='cantidad_1' value='0' onblur='cantidadT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='precio_1' value='0' onblur='precioT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='importePartes_1' value='0' onblur='importePartesT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='clave_1' value='' onblur='claveT2(1)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='operacion_1' value='' onblur='operacionT2(1)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='tiempo_1' value='0' onblur='tiempoT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='importeObra_1' value='0' onblur='importeObraT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='mecanico_1' value='' onblur='mecanicoT2(1)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='descripcion_1' value='' onblur='descripcionT2(1)' style='width:100%;'>
                                </td>
                                <td style='border-right: 1px solid #340f7b;' align='center'>
                                    <input type='text' class='input_field' id='codigo_1' value='' onblur='codigoT2(1)' style='width:100%;'>
                                    <input type='hidden' name='contenido[]' id='contenido_1' value=''>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" align="center">
                <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="2">
                <input type="hidden" name="" id="formularioHoja" value="Garantia">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <button type="button" id="regresarBtn" class="btn btn-dark" onclick="location.href='<?=base_url()."Garantias_lista/4";?>';" >
                    Regresar
                </button>
                <button type="submit" class="btn btn-primary" name="" id="aceptarCotizacion" >Actualizar</button>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </form>

</section>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div id="signatureparent_cont_<?php echo $index+1; ?>">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Formulario de superusuario -->
<div class="modal fade " id="superUsuario" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Ingreso.</h5>
            </div>
            <div class="modal-body" style="font-size:12px;">
                <label for="" style="font-size: 14px;font-weight: initial;">Usuario:</label>
                <br>
                <input type="text" class="input_field" type="text" id="superUsuarioName" value="" style="width:100%;">
                <br><br>
                <label for="" style="font-size: 14px;font-weight: initial;">Password:</label>
                <br>
                <input type="password" class="input_field" type="text" id="superUsuarioPass" value="" style="width:100%;">
                <br><br><br>
                <h4 class="error" style="font-size: 16px;font-weight: initial;" align="center" id="superUsuarioError">:</h4>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="superUsuarioEnvio">Validar</button>
            </div>
        </div>
    </div>
</div>
