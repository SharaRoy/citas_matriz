<?php 
    // Eliminamos cache
    //la pagina expira en una fecha pasada
    header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT"); 
    //ultima actualizacion ahora cuando la cargamos
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
    //no guardar en CACHE
    header ("Cache-Control: no-cache, must-revalidate"); 
    header ("Pragma: no-cache");

    ob_start();

?>

<style media="screen">
    .respuesta{
        color:black;
    }

    #encabezado {
        color: #fff;
        background-color: #337ab7;
        border-color: #337ab7;
        border-radius: 4px;
    }

    p, label{
        text-align: justify;
        font-size:8px;
        /*color: #337ab7;*/
    }

    .tituloTxt{
        font-size:8px;
        color: #337ab7;
    }
    .division{
        background-color: #ddd;
    }
    .headers_table{
        border:1px solid black;
        border-style: double;
    }
    td,tr{
        padding: 0px 0px 0px 0px !important;
        /* font-size: 8px; */
        color:#337ab7;
    }

    .divCheck{
        width:40px;
        height:10px;
        border: 1px solid;
        align-content: center;
        text-align: center;
        background-color: #ffffff;
    }
</style>

<page backtop="7mm" backbottom="5mm" backleft="5mm" backright="5mm" style="font-size: 8px;">
    <!-- Primera tabla -->
    <table style="width:100%;">
        <tr>
            <td colspan="2">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:200px;height:50px;">
            </td>
            <td align="right" style="padding-right:20px;">
                <label class="respuesta">No. Orden:  <?php if(isset($idOrden)) echo $idOrden; else echo " "; ?></label>

                <br>
                Folio Intelisis:&nbsp;
                <label style="color:darkblue;">
                    <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                </label>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width:100%;border: 0.5px solid #340f7b;">
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;width:8%;border-bottom: 0.5px solid #340f7b;" align="center">REP</td>
                        <td align="center" style="border-bottom: 0.5px solid #340f7b;">GASTOS MISC.</td>
                    </tr>
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;" align="center">
                            <label class="respuesta">
                                <?php if (isset($repGastosMisc)): ?>
                                    <?php echo $repGastosMisc[0]; ?>
                                <?php else: ?>
                                    <?php echo "0"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td style="border-bottom: 0.5px solid #340f7b;">
                            PRESTAMO / RENTA DE AUTO: DÍAS 
                            <label class="respuesta"><u>&nbsp;&nbsp;
                            <?php if (isset($diasGM)): ?>
                                <?php echo $diasGM ?>
                            <?php else: ?>
                                <?php echo "<br>"; ?>
                            <?php endif; ?>
                            &nbsp;&nbsp;</u></label>
                            PRECIO:
                            <label class="respuesta"><u>&nbsp;&nbsp;
                            <?php if (isset($precioGM)): ?>
                                <?php echo $precioGM ?>
                            <?php else: ?>
                                <?php echo "<br>"; ?>
                            <?php endif; ?>
                            &nbsp;&nbsp;</u></label>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;" align="center">
                            <label class="respuesta">
                                <?php if (isset($repGastosMisc)): ?>
                                    <?php echo $repGastosMisc[1]; ?>
                                <?php else: ?>
                                    <?php echo "0"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td style="border-bottom: 0.5px solid #340f7b;">
                            PAGO AL CLIENTE CANT.
                            <label class="respuesta"><u>&nbsp;&nbsp;
                            <?php if (isset($pagoClienteGM)): ?>
                                <?php echo $pagoClienteGM; ?>
                            <?php else: ?>
                                <?php echo "<br>"; ?>
                            <?php endif; ?>
                            &nbsp;&nbsp;</u></label>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;" align="center">
                            <label class="respuesta">
                                <?php if (isset($repGastosMisc)): ?>
                                    <?php echo $repGastosMisc[2]; ?>
                                <?php else: ?>
                                    <?php echo "0"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td style="border-bottom: 0.5px solid #340f7b;">
                            GRUA: CANT.
                            <label class="respuesta"><u>&nbsp;&nbsp;
                            <?php if (isset($gruaGM)): ?>
                                <?php echo $gruaGM; ?>
                            <?php else: ?>
                                <?php echo "<br>"; ?>
                            <?php endif; ?>
                            &nbsp;&nbsp;</u></label>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;" align="center">
                            <label class="respuesta">
                                <?php if (isset($repGastosMisc)): ?>
                                    <?php echo $repGastosMisc[3]; ?>
                                <?php else: ?>
                                    <?php echo "0"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td style="border-bottom: 0.5px solid #340f7b;">
                            OTROS: CANT.
                            <label class="respuesta"><u>&nbsp;&nbsp;
                            <?php if (isset($otrosGM)): ?>
                                <?php echo $otrosGM; ?>
                            <?php else: ?>
                                <?php echo "<br>"; ?>
                            <?php endif; ?>
                            &nbsp;&nbsp;</u></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table style="">
                                <tr>
                                    <td colspan="2" style="width:300px;">
                                        <p class="justify" style="font-size:8px;margin-left:6px;margin-right:6px;">
                                            El automóvil aquí descripto queda depositado en sus talleres para los trabajos que por garantía he solicitado. Autorizo el uso del vehículo dentro de los limites de
                                            ciudad para usos de prueba o inspección de las reparaciones señaladas. Estoy de acuerdo en que la empresa no se hace responsable por daños al automóvil por causas de
                                            fuerza mayor o por perdidas de artículos olvidados en el vehículo y no reportadas o entregadas a la Gerencia de Servicio para su custodia.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="width:300px;" align="center">
                                        <?php if (isset($firmaCliente)): ?>
                                            <?php if ($firmaCliente != ""): ?>
                                                <img class="marcoImg" src="<?php echo base_url().$firmaCliente; ?>" alt="" style="width:80px;height:18px;">
                                            <?php else: ?>
                                                <img class="marcoImg" src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:80px;height:18px;">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img class="marcoImg" src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:80px;height:18px;">
                                        <?php endif; ?>
                                        <br>
                                        FIRMA DEL CLIENTE
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="width:300px;" align="center">
                                        <?php if (isset($firmaGerente)): ?>
                                            <?php if ($firmaGerente != ""): ?>
                                                <img class="marcoImg" src="<?php echo base_url().$firmaGerente; ?>" alt="" style="width:80px;height:18px;">
                                            <?php else: ?>
                                                <img class="marcoImg" src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:80px;height:18px;">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img class="marcoImg" src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:80px;height:18px;">
                                        <?php endif; ?>
                                        <br>
                                        FIRMA GERENTE DE SERVICIO(fecha).
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>

            <td>
                <table style="width:100%;border: 0.5px solid #340f7b;">
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;width:100px;" align="center">
                            IDENTIFICACIÓN <br>ASESOR DE SERVICIO
                            <br>
                            <label class="respuesta">
                                <?php if (isset($idAsesor)): ?>
                                    <?php echo $idAsesor; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;width:60px;" align="center">
                            PLACAS
                            <br><br>
                            <label class="respuesta">
                                <?php if (isset($placas)): ?>
                                    <?php echo $placas; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;width:30px;" align="center">
                            AÑO
                            <br><br>
                            <label class="respuesta">
                                <?php if (isset($anio)): ?>
                                    <?php echo $anio; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;width:50px;" align="center">
                            TIPO
                            <br><br>
                            <label class="respuesta">
                                <?php if (isset($tipo)): ?>
                                    <?php echo $tipo; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    <!-- </tr> -->
                    <!-- <tr> -->
                        <td style="border-right: 0.5px solid #340f7b;width:80px;" align="center">
                            HORA DE RECIBO
                            <br><br>
                            <label class="respuesta">
                                <?php if (isset($horaRecibo)): ?>
                                    <?php echo $horaRecibo; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;width:80px;" align="center">
                            HORA PROMEDIO
                            <br><br>
                            <label class="respuesta">
                                <?php if (isset($horaPromedio)): ?>
                                    <?php echo $horaPromedio; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td colspan="2" align="center" style="width:77px;">
                            TELEFONO
                            <br><br>
                            <label class="respuesta">
                                <?php if (isset($telefono)): ?>
                                    <?php echo $telefono; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    </tr>
                </table>

                <table style="width:100%;border: 0.5px solid #340f7b;">
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:340px;" align="center">
                            NOMBRE DEL PROPIETARIO
                            <br><br><br>
                            <label class="respuesta">
                                <?php if (isset($nombreProp)): ?>
                                    <?php echo $nombreProp; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td align="center" style="border-bottom: 0.5px solid #340f7b;width:130px;">
                            CLIENTE VISITANTE
                            <br><br><br>
                            <label class="respuesta">
                                <?php if (isset($clienteVisita)): ?>
                                    <?php echo $clienteVisita; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" style="border-bottom: 0.5px solid #340f7b;">
                            DIRECCIÓN
                            <br><br><br>
                            <label class="respuesta">
                                <?php if (isset($domicilio)): ?>
                                    <?php echo $domicilio; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="border-bottom: 0.5px solid #340f7b;">
                            CIUDAD/ESTADO
                            <br><br><br>
                            <label class="respuesta">
                                <?php if (isset($estado)): ?>
                                    <?php echo $estado; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td align="center" style="border-bottom: 0.5px solid #340f7b;">
                            CÓDIGO POSTAL
                            <br><br><br>
                            <label class="respuesta">
                                <?php if (isset($cp)): ?>
                                    <?php echo $cp; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            IDENTIFICACIÓN DEL VEHÍCULO/NÚMERO DE SERIE
                            <br><br>
                            <table style="width:100%;">
                                <tr>
                                    <?php if (isset($idVehiculo)): ?>
                                        <?php for ($i = 0; $i <count($idVehiculo)-1 ;$i++): ?>
                                            <td style="border-right: 0.5px solid #340f7b;height:25px;width:26px;">
                                                <label class="respuesta"><?php echo $idVehiculo[$i]; ?></label>
                                            </td>
                                        <?php endfor; ?>
                                    <?php else: ?>
                                        <?php for ($i = 0; $i <17 ;$i++): ?>
                                            <td style="border-right: 0.5px solid #340f7b;height:25px;width:26px;">
                                                <label class="respuesta">0</label>
                                            </td>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>

            <td>
                <table style="width:100%;border: 0.5px solid #340f7b;">
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;width:90px;" align="center" rowspan="2">
                            <br>
                            <label class="respuesta">
                                <?php if (isset($noTorre)): ?>
                                    <?php echo $noTorre; ?>
                                <?php else: ?>
                                    <?php echo "X"; ?>
                                <?php endif; ?>
                            </label>
                            <br><br>
                            TORRE No.
                        </td>
                        <td style="width:165px;">
                            FOLIO:
                            <br>
                            <label class="respuesta">
                                <?php if (isset($folio)): ?>
                                    <?php echo $folio; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:165px;">
                            CONTINUACIÓN FOLIO
                            <br>
                            <label class="respuesta">
                                <?php if (isset($folioContinue)): ?>
                                    <?php echo $folioContinue; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    </tr>
                </table>
                <br>

                <table style="width:100%;border: 0.5px solid #340f7b;">
                    <tr>
                        <td align="center" colspan="2" style="border-bottom: 0.5px solid #340f7b;height:15px;">
                            FECHA Y KILOMETRAJE AL RECIBIR EL VEHÍCULO
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;" align="center">
                            FECHA:
                            <table style="width:100%;">
                                <tr>
                                    <td style="border-right: 0.5px solid #340f7b;width:43px;" align="center">
                                        <?php if (isset($fechaRecibe)): ?>
                                            <label class="respuesta"><?php echo $fechaRecibe[1]; ?></label>
                                        <?php else: ?>
                                            <label class="respuesta"><?php echo "0"; ?></label>
                                        <?php endif; ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 0.5px solid #340f7b;width:43px;" align="center">
                                        <?php if (isset($fechaRecibe)): ?>
                                            <label class="respuesta"><?php echo $fechaRecibe[0]; ?></label>
                                        <?php else: ?>
                                            <label class="respuesta"><?php echo "0"; ?></label>
                                        <?php endif; ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center" style="width:43px;">
                                        <?php if (isset($fechaRecibe)): ?>
                                            <label class="respuesta"><?php echo $fechaRecibe[2]; ?></label>
                                        <?php else: ?>
                                            <label class="respuesta"><?php echo "0"; ?></label>
                                        <?php endif; ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-bottom: 0.5px solid #340f7b;">
                            KM:
                            <table>
                                <tr>
                                    <?php if (isset($kmRecibe)): ?>
                                        <?php for ($i = 0; $i <count($kmRecibe)-1 ;$i++): ?>
                                            <td style="border-left: 0.5px solid #340f7b;width:16px;height: 16px;" align="center">
                                                <label class="respuesta"><?php echo $kmRecibe[$i]; ?></label>
                                            </td>
                                        <?php endfor; ?>
                                    <?php else: ?>
                                        <?php for ($i = 0; $i <6 ;$i++): ?>
                                            <td style="border-left: 0.5px solid #340f7b;width:16px;height: 16px;" align="center">
                                                <label class="respuesta">0</label>
                                            </td>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2"style="height:15px;border-bottom: 0.5px solid #340f7b;">
                            FECHA Y KILOMETRAJE AL ENTREGAR EL VEHÍCULO
                        </td>
                    </tr>
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;" align="center">
                            FECHA:
                            <table>
                                <tr>
                                    <td style="border-right: 0.5px solid #340f7b;width:43px;" align="center">
                                        <?php if (isset($fechaEntrega)): ?>
                                            <label class="respuesta"><?php echo $fechaEntrega[1]; ?></label>
                                        <?php else: ?>
                                            <label class="respuesta"><?php echo "0"; ?></label>
                                        <?php endif; ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 0.5px solid #340f7b;width:43px;" align="center">
                                        <?php if (isset($fechaEntrega)): ?>
                                            <label class="respuesta"><?php echo $fechaEntrega[0]; ?></label>
                                        <?php else: ?>
                                            <label class="respuesta"><?php echo "0"; ?></label>
                                        <?php endif; ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center" style="width:43px;">
                                        <?php if (isset($fechaEntrega)): ?>
                                            <label class="respuesta"><?php echo $fechaEntrega[2]; ?></label>
                                        <?php else: ?>
                                            <label class="respuesta"><?php echo "0"; ?></label>
                                        <?php endif; ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            KM:
                            <table>
                                <tr>
                                    <?php if (isset($kmEntrega)): ?>
                                        <?php for ($i = 0; $i <count($kmEntrega)-1 ;$i++): ?>
                                            <td style="border-left: 0.5px solid #340f7b;width:16px;height: 18px;" align="center">
                                                <label class="respuesta"><?php echo $kmEntrega[$i]; ?></label>
                                            </td>
                                        <?php endfor; ?>
                                    <?php else: ?>
                                        <?php for ($i = 0; $i <6 ;$i++): ?>
                                            <td style="border-left: 0.5px solid #340f7b;width:16px;height: 18px;" align="center">
                                                <label class="respuesta">0</label>
                                            </td>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table style="width:100%;border: 0.5px solid #340f7b;font-size:7px;">
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;" align="center">
                            FECHA DE INICIO DE <br>GARANTÍA <br>
                            <table style="font-size:8px;">
                                <tr>
                                    <td style="border-right: 0.5px solid #340f7b;width:25px;" align="center">
                                        <label class="respuesta">
                                            <?php if (isset($fechaGarantia)): ?>
                                                <?php echo $fechaGarantia[1]; ?>
                                            <?php else: ?>
                                                <?php echo ""; ?>
                                            <?php endif; ?>
                                        </label>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 0.5px solid #340f7b;width:25px;" align="center">
                                        <label class="respuesta">
                                            <?php if (isset($fechaGarantia)): ?>
                                                <?php echo $fechaGarantia[0]; ?>
                                            <?php else: ?>
                                                <?php echo ""; ?>
                                            <?php endif; ?>
                                        </label>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center" style="width:30px;">
                                        <label class="respuesta">
                                            <?php if (isset($fechaGarantia)): ?>
                                                <?php echo $fechaGarantia[2]; ?>
                                            <?php else: ?>
                                                <?php echo ""; ?>
                                            <?php endif; ?>
                                        </label>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;width:75px;" align="center">
                            N° DE DISTRIBUIDOR  <br>
                            <br>
                            <label class="respuesta">
                                <?php if (isset($noDistribuidor)): ?>
                                    <?php echo $noDistribuidor; ?>
                                <?php else: ?>
                                    <?php echo "X"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td>
                            FECHA DE REPARACIÓN:  <br><br>
                            <table style="font-size:8px;">
                                <tr>
                                    <td style="border-right: 0.5px solid #340f7b;width:24px;" align="center">
                                        <?php if (isset($fechaReparacion)): ?>
                                            <label class="respuesta"><?php echo $fechaReparacion[1]; ?></label>
                                        <?php else: ?>
                                            <label class="respuesta"><?php echo ""; ?></label>
                                        <?php endif; ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 0.5px solid #340f7b;width:24px;" align="center">
                                        <?php if (isset($fechaReparacion)): ?>
                                            <label class="respuesta"><?php echo $fechaReparacion[0]; ?></label>
                                        <?php else: ?>
                                            <label class="respuesta"><?php echo ""; ?></label>
                                        <?php endif; ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center" style="width:30px;">
                                        <label class="respuesta">
                                            <?php if (isset($fechaReparacion)): ?>
                                                <?php echo $fechaReparacion[2]; ?>
                                            <?php else: ?>
                                                <?php echo ""; ?>
                                            <?php endif; ?>
                                        </label>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table style="width:100%;">
        <tr>
            <td>
                <table style="width:100%;border: 0.5px solid #340f7b;font-size:8px;">
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;" align="center">
                            REP<br>No.
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                            TIPO DE <br>GARANTÍA
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center">
                            DAÑOS EN <br>RELACIÓN
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center">
                            AUTORIZ <br>N°1
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center">
                            AUTORIZ <br>N°2
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center">
                            PARTES <br>TOTAL $
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center">
                            M DE OBRA <br>TOTAL $
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center">
                            MISC <br>TOTAL $
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center">
                            IVA <br>$
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                            PARTICIPACIÓN <br>CLIENTE $
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:95px;" align="center">
                            PARTICIPACIÓN <br>DISTRIBUIDOR $
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                            REPARACIÓN <br>TOTAL $
                        </td>
                    </tr>
                    <tbody>
                        <!-- Verificamos que exista la variable -->
                        <?php if (isset($contenedor)): ?>
                            <!-- Si existe la variable, verificamos que tenga informacion para imprimir -->
                            <?php if (count($contenedor)>0): ?>
                                <?php foreach ($contenedor as $index => $valor): ?>
                                    <tr>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;height:18px;' align='center'>
                                            <label class="respuesta"><?php echo $index+1; ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'>
                                            <label class="respuesta"><?php echo $contenedor[$index][0]; ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'>
                                            <label class="respuesta"><?php echo $contenedor[$index][1]; ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'>
                                            <label class="respuesta"><?php echo $contenedor[$index][2]; ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'>
                                            <label class="respuesta"><?php echo $contenedor[$index][3]; ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'>
                                            <label class="respuesta"><?php echo number_format($contenedor[$index][4],2); ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'>
                                            <label class="respuesta"><?php echo number_format($contenedor[$index][5],2); ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'>
                                            <label class="respuesta"><?php echo number_format($contenedor[$index][6],2); ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;' align='center'>
                                            <label class="respuesta"><?php echo number_format($contenedor[$index][7],2); ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'>
                                            <label class="respuesta"><?php echo number_format($contenedor[$index][8],2); ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:95px;' align='center'>
                                            <label class="respuesta"><?php echo number_format($contenedor[$index][9],2); ?></label>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'>
                                            <label class="respuesta"><?php echo number_format( $contenedor[$index][10],2); ?></label>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <!-- Verificamos si faltan filas que rellenar -->
                                <?php if (count($contenedor)<5): ?>
                                    <?php for ($i = count($contenedor)-1; $i < 5; $i++): ?>
                                        <tr>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;height:18px;' align='center'>
                                                <?php echo $i+1; ?>
                                            </td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:95px;' align='center'></td>
                                            <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                        </tr>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            <!-- De lo contrario imprimimos filas vacias -->
                            <?php else: ?>
                                <?php for ($i = 0; $i < 5; $i++): ?>
                                    <tr>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;height:18px;' align='center'>
                                            <?php echo $i+1; ?>
                                        </td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:95px;' align='center'></td>
                                        <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                    </tr>
                                <?php endfor; ?>
                            <?php endif; ?>
                        <!-- De lo contrario imprimimos las filas vacias -->
                        <?php else: ?>
                            <?php for ($i = 0; $i < 5; $i++): ?>
                                <tr>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;height:18px;' align='center'>
                                        <?php echo $i+1; ?>
                                    </td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:95px;' align='center'></td>
                                    <td style='border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;' align='center'></td>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr style='border: 0.5px solid #340f7b;'>
                            <td style='border-right: 0.5px solid #340f7b;height:18px;' align='center' colspan="5">
                                TOTALES
                            </td>
                            <td style='border-right: 0.5px solid #340f7b;' align='center'>
                                <label class="respuesta">
                                    <?php if (isset($totalPartes)): ?>
                                        <?php echo number_format($totalPartes,2); ?>
                                    <?php else: ?>
                                        <?php echo "0.00"; ?>
                                    <?php endif; ?>
                                </label>
                            </td>
                            <td style='border-right: 0.5px solid #340f7b;' align='center'>
                                <label class="respuesta">
                                    <?php if (isset($totalObra)): ?>
                                        <?php echo number_format($totalObra,2); ?>
                                    <?php else: ?>
                                        <?php echo "0.00"; ?>
                                    <?php endif; ?>
                                </label>
                            </td>
                            <td style='border-right: 0.5px solid #340f7b;' align='center'>
                                <label class="respuesta">
                                    <?php if (isset($totalMisc)): ?>
                                        <?php echo number_format($totalMisc,2); ?>
                                    <?php else: ?>
                                        <?php echo "0.00"; ?>
                                    <?php endif; ?>
                                </label>
                            </td>
                            <td style='border-right: 0.5px solid #340f7b;' align='center'>
                                <label class="respuesta">
                                    <?php if (isset($totalIva)): ?>
                                        <?php echo number_format($totalIva,2); ?>
                                    <?php else: ?>
                                        <?php echo "0.00"; ?>
                                    <?php endif; ?>
                                </label>
                            </td>
                            <td style='border-right: 0.5px solid #340f7b;' align='center'>
                                <label class="respuesta">
                                    <?php if (isset($totalCliente)): ?>
                                        <?php echo number_format($totalCliente,2); ?>
                                    <?php else: ?>
                                        <?php echo "0.00"; ?>
                                    <?php endif; ?>
                                </label>
                            </td>
                            <td style='border-right: 0.5px solid #340f7b;' align='center'>
                                <label class="respuesta">
                                    <?php if (isset($totalDistribuidor)): ?>
                                        <?php echo number_format($totalDistribuidor,2); ?>
                                    <?php else: ?>
                                        <?php echo "0.00"; ?>
                                    <?php endif; ?>
                                </label>
                            </td>
                            <td style='border-right: 0.5px solid #340f7b;' align='center'>
                                <label class="respuesta">
                                    <?php if (isset($totalReparacion)): ?>
                                        <?php echo number_format($totalReparacion,2); ?>
                                    <?php else: ?>
                                        <?php echo "0.00"; ?>
                                    <?php endif; ?>
                                </label>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </td>

            <td>
                <table style="width:100%;border: 0.5px solid #340f7b;">
                    <tr style="border: 0.5px solid #340f7b;">
                        <td style="border-right: 0.5px solid #340f7b;width:60px;height:70px;" align="center" rowspan="3">
                            VERIFICACIÓN<br> OASIS
                            <br>
                            <label class="respuesta">
                                <?php if (isset($oasis)): ?>
                                    <?php echo $oasis; ?>
                                <?php else: ?>
                                    <?php echo "<br>"; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                        <td align="center" style="border-bottom: 0.5px solid #340f7b;">
                            GTIA. DE REFACCIONES
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="border-bottom: 0.5px solid #340f7b;">
                            FECHA DE INST. P VTA. DE LA PARTE
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table style="">
                                <tr>
                                    <td style="border-right: 0.5px solid #340f7b;width:40px;height:30px;" align="center">
                                        <label class="respuesta">
                                            <?php if (isset($fechaVta)): ?>
                                                <?php echo $fechaVta[1]; ?>
                                            <?php else: ?>
                                                <?php echo "<br>"; ?>
                                            <?php endif; ?>
                                        </label>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 0.5px solid #340f7b;width:40px;" align="center">
                                        <label class="respuesta">
                                            <?php if (isset($fechaVta)): ?>
                                                <?php echo $fechaVta[0]; ?>
                                            <?php else: ?>
                                                <?php echo "<br>"; ?>
                                            <?php endif; ?>
                                        </label>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center" style="width:45px;">
                                        <label class="respuesta">
                                            <?php if (isset($fechaVta)): ?>
                                                <?php echo $fechaVta[2]; ?>
                                            <?php else: ?>
                                                <?php echo "<br>"; ?>
                                            <?php endif; ?>
                                        </label>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="border-top: 0.5px solid #340f7b;">
                            TOT.KMS TRANSC. DE LA INST. O VTA
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="border-top: 0.5px solid #340f7b; height:30px;width:200px;">
                            <label class="respuesta">
                                <?php if (isset($kmsVta)): ?>
                                    <?php echo $kmsVta; ?>
                                <?php else: ?>
                                    <?php echo ""; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="border-top: 0.5px solid #340f7b;">
                            N° REF/DOC. ORIGINA/REMISIÓN
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" style="height:30px;width:200px;">
                            <label class="respuesta">
                                <?php if (isset($noRefDoc)): ?>
                                    <?php echo $noRefDoc; ?>
                                <?php else: ?>
                                    <?php echo ""; ?>
                                <?php endif; ?>
                            </label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table style="width:100%;border: 0.5px solid #340f7b;font-size:7px;">
        <tr>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;" align="center" colspan="12">
                <label for="" style="font-size:12px;">PARTES Y OPERCACIONES</label>
            </td>
            <td colspan="3" style="border-bottom: 0.5px solid #340f7b;background-color:#cbcbec;width:335px;font-size:7px;">
                <p align="justify" style="text-align:justify;margin-left:5px;margin-right:5px;font-size:7px;width:335px;">
                    Con el fin de recibir mas y mejores servicios, autorizo a <strong><?=SUCURSAL?>, S.A. DE C.V.</strong> para el envío de mensajes de texto y llamadas a los números y/o correos electrónicos que brindo, para notificaciones y seguimiento.
                    <br>
                </p>
            </td>
        </tr>
        <tr>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;height:30px;" align="center">
                REP <br>N°
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center">
                PREFIJO
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center">
                BÁSICO
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center">
                SUFIJO
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                NOMBRE
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:55px;" align="center">
                CANTIDAD
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center">
                PRECIO <br>C/U
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center">
                IMPORTE <br>PARTES
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center">
                CLAVE <br>DETECTO
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                OPERACIÓN N°
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center">
                TIEMPO <br>TARIFA
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center">
                IMPORTE <br>MANO DE OBRA
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                MECANICO <br>CLAVE
            </td>
            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:165px;" align="center">
                DESCRIPCIÓN QUEJA DEL CLIENTE
            </td>
            <td style="border-bottom: 0.5px solid #340f7b;width:50px;" align="center">
                CÓDIGO
            </td>
        </tr>
        <!-- Verificamos que exista la variable -->
        <?php if (isset($contenidoPartes)): ?>
            <!-- Si existe la variable, verificamos que tenga informacion para imprimir -->
            <?php if (count($contenidoPartes)>0): ?>
                <?php foreach ($contenidoPartes as $index => $valor): ?>
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;" align="center">
                            <label class="respuesta"><?php echo $index+1; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;height:18px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][0]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][1]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][2]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][3]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:55px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][4]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][5]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][6]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][7]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][8]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][9]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][10]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][11]; ?></label>
                        </td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:165px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][12]; ?></label>
                        </td>
                        <td style="border-bottom: 0.5px solid #340f7b;width:50px;" align="center">
                            <label class="respuesta"><?php echo $contenidoPartes[$index][13]; ?></label>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <!-- Verificamos si faltan filas que rellenar -->
                <?php if (count($contenidoPartes)<9): ?>
                    <?php for ($i = count($contenidoPartes)-1; $i < 9; $i++): ?>
                        <tr>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;height:18px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:55px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                            <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:165px;" align="center"></td>
                            <td style="border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                        </tr>
                    <?php endfor; ?>
                <?php endif; ?>
            <!-- De lo contrario imprimimos filas vacias -->
            <?php else: ?>
                <?php for ($i = 0; $i < 9; $i++): ?>
                    <tr>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;height:18px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:55px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                        <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:165px;" align="center"></td>
                        <td style="border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                    </tr>
                <?php endfor; ?>
            <?php endif; ?>
        <!-- De lo contrario imprimimos las filas vacias -->
        <?php else: ?>
            <?php for ($i = 0; $i < 9; $i++): ?>
                <tr>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:20px;height:18px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:55px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:65px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:60px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:70px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:80px;" align="center"></td>
                    <td style="border-right: 0.5px solid #340f7b;border-bottom: 0.5px solid #340f7b;width:165px;" align="center"></td>
                    <td style="border-bottom: 0.5px solid #340f7b;width:50px;" align="center"></td>
                </tr>
            <?php endfor; ?>
        <?php endif; ?>
    </table>
</page>

<?php 
    use Spipu\Html2Pdf\Html2Pdf;

    $html2pdf = new Html2Pdf('L', 'A4', 'en',TRUE,'UTF-8',NULL);
    
    try {

        $html = ob_get_clean();

        ob_clean();
        /* Limpiamos la salida del búfer y lo desactivamos */
        //ob_end_clean();

        $html2pdf->setDefaultFont('Helvetica');     //Helvetica
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($html);
        $html2pdf->setTestTdInOnePage(FALSE);
        $html2pdf->Output();

    } catch (Html2PdfException $e) {
        $html2pdf->clean();
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }

?>