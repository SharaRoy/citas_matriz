<section class="">
    <!-- <div class="row" style="background-color:white;"> -->
    <form name="" method="post" action="<?php echo base_url()."garantias/Operaciones/validateForm";?>">
        <div class="row">
            <div class="col-sm-9" align="center" style="">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:280px;height:60px;">
            </div>
            <div class="col-sm-3" align="right" style="padding-top:20px;">
                <label style="font-weight: 500;font-size: 14px;">No. Orden:</label>
                <input id="noOrdenServicio" onblur="verificaGarantias()" type="text" class='input_field' name="idOrden" value="<?php echo set_value("idOrden"); ?>" style="width:2cm;">
                <?php echo form_error('idOrden', '<br><span class="error">', '</span>'); ?>

                <br>
                Folio Intelisis:&nbsp;
                <label style="color:darkblue;">
                    <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                </label>
            </div>
            <div class="col-sm-3" align="right" style="padding-top:20px;">
                <!-- <label style="font-weight: 500;font-size: 14px;">No. Orden Intelisis: <u>&nbsp;&nbsp;&nbsp; <?php if(isset($idIntelisis)) if($idIntelisis != NULL) echo $idIntelisis; else echo "...";  else echo "..."; ?>&nbsp;&nbsp;&nbsp;</u></label> -->
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 table-responsive">
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">REP</td>
                        <td align="center">GASTOS MISC.</td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            <input type="text" class="input_field" name="repPrestamo" value="<?php echo set_value("repPrestamo"); ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('repPrestamo', '<span class="error">', '</span>'); ?>
                        </td>
                        <td>
                            PRESTAMO / RENTA DE AUTO: DÍAS <input type="text" class="input_field" name="dias" value="<?php echo set_value("dias"); ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('dias', '<span class="error">', '</span>'); ?>
                            PRECIO: <input type="text" class="input_field" name="precio" value="<?php echo set_value("precio"); ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('precio', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            <input type="text" class="input_field" name="repPagoAlCliente" value="<?php echo set_value("repPagoAlCliente"); ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('repPagoAlCliente', '<span class="error">', '</span>'); ?>
                        </td>
                        <td>
                            PAGO AL CLIENTE CANT. <input type="text" class="input_field" name="pagoAlCliente" value="<?php echo set_value("pagoAlCliente"); ?>" style="width:3cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('pagoAlCliente', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            <input type="text" class="input_field" name="repGrua" value="<?php echo set_value("repGrua"); ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('repGrua', '<span class="error">', '</span>'); ?>
                        </td>
                        <td>
                            GRUA: CANT.  <input type="text" class="input_field" name="grua" value="<?php echo set_value("grua"); ?>" style="width:3cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('grua', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            <input type="text" class="input_field" name="repOtros" value="<?php echo set_value("repOtros"); ?>" style="width:1cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('repOtros', '<span class="error">', '</span>'); ?>
                        </td>
                        <td>
                            OTROS: CANT.  <input type="text" class="input_field" name="otros" value="<?php echo set_value("otros"); ?>" style="width:3cm;text-align:center;color:#340f7b;">
                            <?php echo form_error('otros', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="2">
                                        <br>
                                        <p align="justify" style="text-align:justify;margin-left:10px;margin-right:10px;font-size:10px;">
                                            El automóvil aquí descripto queda depositado en sus talleres para los trabajos que por garantía he solicitado. Autorizo el uso del vehículo dentro de los limites de
                                            ciudad para usos de prueba o inspección de las reparaciones señaladas. Estoy de acuerdo en que la empresa no se hace responsable por daños al automóvil por causas de
                                            fuerza mayor o por perdidas de artículos olvidados en el vehículo  y no reportadas o entregadas a la Gerencia de Servicio para su custodia.
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:50%;font-size:10px;" align="center">
                                        <input type="hidden" id="rutaFirmaCliente" name="rutaFirmaCliente" value="<?= set_value('rutaFirmaCliente');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaCliente');?>" id="firmaImgCliente" alt="" style="width:130px;height:40px;">
                                        <!-- <a class="cuadroFirma btn btn-primary" data-value="Cliente" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                            Firmar
                                        </a> -->
                                        <br>
                                        FIRMA DEL CLIENTE
                                        <br><br>
                                    </td>
                                    <td style="width:50%;font-size:10px;" align="center">
                                        <input type="hidden" id="rutaFirmaGerente" name="rutaFirmaGerente" value="<?= set_value('rutaFirmaGerente');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaGerente');?>" id="firmaImgGerente" alt="" style="width:130px;height:40px;">
                                        <!-- <a class="cuadroFirma btn btn-primary" data-value="Gerente" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                            Firmar
                                        </a> -->
                                        <br>
                                        FIRMA GERENTE DE SERVICIO(fecha).
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-5 table-responsive">
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            IDENTIFICACIÓN <br>ASESOR DE SERVICIO
                            <br>
                            <input type="text" class="input_field" name="idAsesor" id="idAsesor" value="<?php echo set_value("idAsesor"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('idAsesor', '<span class="error">', '</span>'); ?>
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            PLACAS
                            <br><br>
                            <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value("placas"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('placas', '<span class="error">', '</span>'); ?>
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            AÑO
                            <br><br>
                            <input type="text" class="input_field" name="anio" id="anio" value="<?php echo set_value("anio"); ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('anio', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            TIPO
                            <br><br>
                            <input type="text" class="input_field" name="tipo" id="tipo" value="<?php echo set_value("tipo"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('tipo', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            HORA DE RECIBO
                            <br>
                            <input type="text" class="input_field" name="horaRecibo"  value="<?php echo $hora ;?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('horaRecibo', '<span class="error">', '</span>'); ?>
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            HORA PROMEDIO
                            <br>
                            <input type="text" class="input_field" name="horaPromedio" value="<?php echo set_value("horaPromedio"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('horaPromedio', '<span class="error">', '</span>'); ?>
                        </td>
                        <td colspan="2" align="center">
                            TELEFONO
                            <br>
                            <input type="text" class="input_field" name="telefono" id="telefono" value="<?php echo set_value("telefono"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('telefono', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
                <br><br>
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            NOMBRE DEL PROPIETARIO
                            <br>
                            <input type="text" class="input_field" name="nombreProp" id="nombreProp" value="<?php echo set_value("nombreProp"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('nombreProp', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            CLIENTE VISITANTE
                            <br>
                            <input type="text" class="input_field" name="clienteVisita" id="clienteVisita" value="<?php echo set_value("clienteVisita"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('clienteVisita', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td colspan="2" align="center">
                            DIRECCIÓN
                            <br>
                            <input type="text" class="input_field" name="direccion" id="direccion" value="<?php echo set_value("direccion"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('direccion', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center">
                            CIUDAD/ESTADO
                            <br>
                            <input type="text" class="input_field" name="estado" id="estado" value="<?php echo set_value("estado"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('estado', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            CÓDIGO POSTAL
                            <br>
                            <input type="text" class="input_field" maxlength="6" name="cPostal" id="cPostal" value="<?php echo set_value("cPostal"); ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('cPostal', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td colspan="2" align="center">
                            IDENTIFICACIÓN DEL VEHÍCULO/NÚMERO DE SERIE
                            <br>
                            <table style="width:100%;">
                                <tr>
                                    <?php for ($i = 0; $i <17 ;$i++): ?>
                                        <td style="border-right: 1px solid #340f7b;">
                                          <input type="text" maxlength="1" class="input_field" id="idVehiculo_<?php echo $i+1; ?>" name="idVehiculo_<?php echo $i+1; ?>" value="<?php echo set_value("idVehiculo_".($i+1)); ?>" style="width:80%;text-align:center;color:#340f7b;">
                                          <?php echo form_error('idVehiculo_'.($i+1), '<span class="error">', '</span>'); ?>
                                        </td>
                                    <?php endfor; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-3 table-responsive">
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center" rowspan="2">
                            <br>
                            <input type="text" class="input_field" name="noTorre" id="noTorre" value="<?php echo set_value("noTorre"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('noTorre', '<span class="error">', '</span>'); ?>
                            <br><br>
                            TORRE No.
                        </td>
                        <td>
                            FOLIO:
                            <br>
                            <input type="text" class="input_field" name="folio" value="<?php echo set_value("folio"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('folio', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td>
                            CONTINUACIÓN FOLIO
                            <br>
                            <input type="text" class="input_field" name="folioContinue" value="<?php echo set_value("folioContinue"); ?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('folioContinue', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>

                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center" colspan="2">
                            FECHA Y KILOMETRAJE AL RECIBIR EL VEHÍCULO
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            FECHA:
                            <table>
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesRecibe" value="<?php if(isset($mes)) echo $mes; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('mesRecibe', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaRecibe" value="<?php if(isset($dia)) echo $dia; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('diaRecibe', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioRecibe" value="<?php if(isset($anio)) echo $anio; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('anioRecibe', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            KM:<br><br>
                            <table>
                                <tr>
                                    <?php for ($i = 0; $i <6 ;$i++): ?>
                                        <td style="border-right: 1px solid #340f7b;">
                                          <input type="text" maxlength="1" class="input_field" id="kmRecibe_<?php echo $i+1; ?>" name="kmRecibe_<?php echo $i+1; ?>" value="<?php echo set_value("kmRecibe_".($i+1)); ?>" style="width:80%;text-align:center;color:#340f7b;">
                                          <?php echo form_error('kmRecibe_'.($i+1), '<span class="error">', '</span>'); ?>
                                        </td>
                                    <?php endfor; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center" colspan="2">
                            FECHA Y KILOMETRAJE AL ENTREGAR EL VEHÍCULO
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            FECHA:
                            <table>
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesEntrega" value="<?php if(isset($mes)) echo $mes; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('mesEntrega', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaEntrega" value="<?php if(isset($dia)) echo $dia; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('diaEntrega', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioEntrega" value="<?php if(isset($anio)) echo $anio; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('anioEntrega', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            KM:<br><br>
                            <table>
                                <tr>
                                    <?php for ($i = 0; $i <6 ;$i++): ?>
                                        <td style="border-right: 1px solid #340f7b;">
                                          <input type="text" maxlength="1" class="input_field" name="kmEntrega_<?php echo $i+1; ?>" value="<?php echo set_value("kmEntrega_".($i+1)); ?>" style="width:80%;text-align:center;color:#340f7b;">
                                          <?php echo form_error('kmEntrega_'.($i+1), '<span class="error">', '</span>'); ?>
                                        </td>
                                    <?php endfor; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            FECHA DE INICIO DE GARANTÍA
                            <table>
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesGarantia" value="<?php if(isset($mes)) echo $mes; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.8cm;">
                                        <?php echo form_error('mesGarantia', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaGarantia" value="<?php if(isset($dia)) echo $dia; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.8cm;">
                                        <?php echo form_error('diaGarantia', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioGarantia" value="<?php if(isset($anio)) echo $anio; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('anioGarantia', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            N° DE DISTRIBUIDOR
                            <br>
                            <input type="text" class="input_field" name="noDistribuidor" value="<?=BIN_SUCURSAL?>" style="width:80%;text-align:center;color:#340f7b;">
                            <?php echo form_error('noDistribuidor', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            FECHA DE REPARACIÓN:
                            <table style="font-size:8px;">
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesReparacion" value="<?php if(isset($mes)) echo $mes; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.8cm;">
                                        <?php echo form_error('mesReparacion', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaReparacion" value="<?php if(isset($dia)) echo $dia; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.8cm;">
                                        <?php echo form_error('diaReparacion', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioReparacion" value="<?php if(isset($anio)) echo $anio; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:1cm;">
                                        <?php echo form_error('anioReparacion', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-10 table-responsive">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-6" align="right">
                        <label for="" style="color:#005dff;"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                        <br>
                        <label for="" style="color:#005dff;"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
                    </div>
                    <div class="col-sm-2" align="right">
                      <a id="agregarT1" class="btn btn-success" style="color:white;">
                          <i class="fas fa-plus"></i>
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a id="eliminarT1" class="btn btn-danger" style="color:white; width: 1cm;">
                          <i class="fas fa-minus"></i>
                      </a>
                    </div>
                </div>
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            REP<br>No.
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            TIPO DE GARANTÍA
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            DAÑOS EN RELACIÓN
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            AUTORIZ N°1
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            AUTORIZ N°2
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            PARTES <br>TOTAL $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            M DE OBRA <br>TOTAL $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            MISC <br>TOTAL $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            IVA $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            PARTICIPACIÓN <br>CLIENTE $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            PARTICIPACIÓN <br>DISTRIBUIDOR $
                        </td>
                        <td style="border-right: 1px solid #340f7b;" align="center">
                            REPARACIÓN <br>TOTAL $
                            <input type='hidden' id='indiceTGarantia' value='1'>
                        </td>
                    </tr>
                    <tbody id="tabla_garantia">
                        <tr id='fila_reparacion_1' style='border: 1px solid #340f7b;'>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                1
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' onblur='garantiaT1(1)' id='garantia_1' value='' style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' onblur="reparacionT1(1)" id='reparacion_1' value='' style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' onblur="autoriz_n1T1(1)" id='autoriz_n1_1' value='' style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' onblur="autoriz_n2T1(1)" id='autoriz_n2_1' value='' style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='partes_1' onblur="autosuma_partes(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='obra_1' onblur="autosuma_MObra(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='misc_1' onblur="autosuma_misc(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label id='labelIva_1'>0</label>
                                <!-- <input type='text' class='input_field' id='iva_1' onblur="autosuma_iva(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'> -->
                                <input type='hidden' class='input_field' id='iva_1' value='0'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='cliente_1' onblur="autosuma_cliente(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='distribuidor_1' onblur="autosuma_distribuidor(1)" value='0' onkeypress='return event.charCode >= 46 && event.charCode <= 57'  style='width:95%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label for="" id="labelTotal_1" >0</label>
                                <input type='hidden' id='total_1' value='0'>
                                <input type='hidden' id='contenedor_1' name='contenedor[]' value=''>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr style='border: 1px solid #340f7b;'>
                            <td style='border-right: 1px solid #340f7b;' align='right' colspan="5">
                                TOTALES
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label for="" id="labelTotalPartes">0</label>
                                <input type='hidden' class='input_field' id='TotalPartes' name="TotalPartes" value="0">
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label for="" id="labelTotalObra">0</label>
                                <input type='hidden' class='input_field' id='TotalObra' name="TotalObra" value="0">
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label for="" id="labelTotalMisc">0</label>
                                <input type='hidden' class='input_field' id='TotalMisc' name="TotalMisc" value="0">
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label for="" id="labelTotalIva">0</label>
                                <input type='hidden' class='input_field' id='TotalIva' name="TotalIva" value="0">
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label for="" id="labelTotalCliente">0</label>
                                <input type='hidden' class='input_field' id='TotalCliente' name="TotalCliente" value="0">
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label for="" id="labelTotalDistribuidor">0</label>
                                <input type='hidden' class='input_field' id='TotalDistribuidor' name="TotalDistribuidor" value="0">
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <label for="" id="labelTotalReparacion">0</label>
                                <input type='hidden' class='input_field' id='TotalReparacion' name="TotalReparacion" value="0">
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="col-sm-2 table-responsive">
                <table style="width:100%;border: 1px solid #340f7b;">
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center" rowspan="3">
                            VERIFICACIÓN OASIS
                            <br><br>
                            <input type="text" class="input_field" name="oasis" value="<?php echo set_value("oasis"); ?>" style="width:100%;text-align:center;color:#340f7b;">
                            <?php echo form_error('oasis', '<span class="error">', '</span>'); ?>
                        </td>
                        <td align="center">
                            GTIA. DE REFACCIONES
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center">
                            FECHA DE INST. P VTA. DE LA PARTE
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center">
                            <table style="width:100%;">
                                <tr>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="mesVta" value="<?php if(isset($mes)) echo $mes; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.7cm;">
                                        <?php echo form_error('mesVta', '<span class="error">', '</span>'); ?>
                                        <br>
                                        MES
                                    </td>
                                    <td style="border-right: 1px solid #340f7b;" align="center">
                                        <input type="text" class="input_field" name="diaVta" value="<?php if(isset($dia)) echo $dia; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.7cm;">
                                        <?php echo form_error('diaVta', '<span class="error">', '</span>'); ?>
                                        <br>
                                        DÍA
                                    </td>
                                    <td align="center">
                                        <input type="text" class="input_field" name="anioVta" value="<?php if(isset($anio)) echo $anio; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' style="width:0.7cm;">
                                        <?php echo form_error('anioVta', '<span class="error">', '</span>'); ?>
                                        <br>
                                        AÑO
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td align="center" colspan="2">
                            TOT.KMS TRANSC. DE LA INST. O VTA
                            <br>
                            <input type="text" class="input_field" name="totKms" value="<?php echo set_value("totKms"); ?>" style="width:100%;text-align:center;color:#340f7b;">
                            <?php echo form_error('totKms', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border: 1px solid #340f7b;">
                        <td style="border-right: 1px solid #340f7b;" align="center" colspan="2">
                            N° REF/DOC. ORIGINA/REMISIÓN
                            <br>
                            <input type="text" class="input_field" name="noRefDoc" value="<?php echo set_value("noRefDoc"); ?>" style="width:100%;text-align:center;color:#340f7b;">
                            <?php echo form_error('noRefDoc', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row">
              <div class="col-sm-12 table-responsive">
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-6" align="right">
                        <label for="" style="color:#005dff;"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                        <br>
                        <label for="" style="color:#005dff;"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
                    </div>
                    <div class="col-sm-2" align="right">
                      <a id="agregarT2" class="btn btn-success" style="color:white;">
                          <i class="fas fa-plus"></i>
                      </a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <a id="eliminarT2" class="btn btn-danger" style="color:white; width: 1cm;">
                          <i class="fas fa-minus"></i>
                      </a>
                      <input type="hidden" id="indice_tabla_partes" value="1">
                    </div>
                </div>

                <table style="width:100%;border: 1px solid #340f7b;">
                    <thead>
                        <tr style="border: 1px solid #340f7b;">
                            <td style="border-right: 1px solid #340f7b;width:67%;" align="center" colspan="12">
                                <h4 align="center">PARTES Y OPERCACIONES</h4>
                            </td>
                            <td colspan="3" style="background-color:#cbcbec;width:33%;">
                                <p align="justify" style="text-align:justify;margin-left:10px;margin-right:10px;font-size:9px;">
                                    Con el fin de recibir mas y mejores servicios, autorizo a <strong><?=SUCURSAL ?>, S.A. DE C.V.</strong> para el
                                    envío de mensajes de texto y llamadas a los números y/o correos electrónicos que brindo, para notificaciones y seguimiento.
                                </p>
                            </td>
                        </tr>
                        <tr style="border: 1px solid #340f7b;">
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                REP <br>N°
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                PREFIJO
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                BÁSICO
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                SUFIJO
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                NOMBRE
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                CANTIDAD
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                PRECIO <br>C/U
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                IMPORTE <br>PARTES
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                CLAVE <br>DETECTO
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                OPERACIÓN N°
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                TIEMPO <br>TARIFA
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                IMPORTE <br>MANO DE OBRA
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                MECANICO <br>CLAVE
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                DESCRIPCIÓN QUEJA DEL CLIENTE
                            </td>
                            <td style="border-right: 1px solid #340f7b;" align="center">
                                CÓDIGO
                            </td>
                        </tr>
                    </thead>
                    <tbody id="tabla_partes">
                        <tr id='fila_partes_1' style='border: 1px solid #340f7b;'>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                1
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='prefijo_1' value='' onblur='prefijoT2(1)' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='basico_1' value='' onblur='basicoT2(1)' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='sufijo_1' value='' onblur='sufijoT2(1)' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='nombre_1' value='' onblur='nombreT2(1)' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='cantidad_1' value='0' onblur='cantidadT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='precio_1' value='0' onblur='precioT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='importePartes_1' value='0' onblur='importePartesT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='clave_1' value='' onblur='claveT2(1)' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='operacion_1' value='' onblur='operacionT2(1)' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='tiempo_1' value='0' onblur='tiempoT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='importeObra_1' value='0' onblur='importeObraT2(1)' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='mecanico_1' value='' onblur='mecanicoT2(1)' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='descripcion_1' value='' onblur='descripcionT2(1)' style='width:100%;'>
                            </td>
                            <td style='border-right: 1px solid #340f7b;' align='center'>
                                <input type='text' class='input_field' id='codigo_1' value='' onblur='codigoT2(1)' style='width:100%;'>
                                <input type='hidden' name='contenido[]' id='contenido_1' value=''>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-3" align="center"></div>

            <div class="col-sm-3" align="center">
                FIRMA DEL TÉCNICO
                <br>
                <input type="hidden" id="rutaFirmaTecnico" name="rutaFirmaTecnico" value="<?= set_value('rutaFirmaTecnico');?>" style="background-color:#ddd;">
                <img class="marcoImg" src="<?php if(set_value('rutaFirmaTecnico') != "") echo set_value('rutaFirmaTecnico');  ?>" id="firmaFirmaTecnico" alt="" style="height:2cm;width:4cm;">
                <br>
                <input type="text" class="input_field" name="nombreTecnico" id="tecniconame" style="width:100%;" value="<?= set_value('nombreTecnico');?>">
                <?php echo form_error('rutaFirmaTecnico', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('nombreTecnico', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>
            <div class="col-sm-3" align="center">
                FIRMA PROVISIONAL.
                <br>
                <input type="hidden" id="rutaFirmaProv" name="rutaFirmaProv" value="<?= set_value('rutaFirmaProv');?>" style="background-color:#ddd;">
                <img class="marcoImg" src="<?php if(set_value('rutaFirmaProv') != "") echo set_value('rutaFirmaProv');  ?>" id="firmaFirmaJefeTaller" alt="" style="height:2cm;width:4cm;">
                <br>
                <input type="text" class="input_field" name="nombreProv" id="nombreProv" style="width:100%;" value="<?php if(set_value('nombreProv') != "") echo set_value('nombreProv');  ?>" style="background-color:#ddd;">
                <?php echo form_error('rutaFirmaProv', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('nombreProv', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>

            <div class="col-sm-3" align="center"></div>
        </div>
        <div class="row">
            <div class="col-sm-3" align="center"></div>
            <div class="col-sm-3" align="center">
                <!-- <a class="cuadroFirma btn btn-primary" data-value="Tecnico" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                    Firmar
                </a> -->
            </div>
            <div class="col-sm-3" align="center">
                <!-- <a class="cuadroFirma btn btn-primary" data-value="Extra" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                    Firmar
                </a> -->
            </div>
            <div class="col-sm-3" align="center"></div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" align="center">
                <input type="hidden" name="modoVistaFormulario" id="" value="1">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
                <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="" >Guardar</button>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </form>

</section>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
