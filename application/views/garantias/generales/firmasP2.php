<section style="padding: 20px 20px 20px 20px;">
    <!-- <div class="row" style="background-color:#cfeff9;"> -->
    <div class="row">
        <div class="col-sm-4" align="left" style="">
            <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:280px;height:60px;">
        </div>
        <div class="col-sm-8" align="center" style="margin-top:20px;">
            <h4>
                Formulario Garantía F1863
                <!-- <a id="superUsuarioA" class="h2_title_blue" data-target="#superUsuario" data-toggle="modal" style="color:blue;margin-top:15px;cursor: pointer;">
                    <i class="fas fa-user"></i>
                </a> -->
            </h4>
        </div>
    </div>

    <!-- Alerta para proceso del registro -->
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Fallo el proceso.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Campos incompletos.</strong>
    </div>

    <form name="" method="post" action="<?php echo base_url()."garantias/General/validateFormP2";?>" style="margin:15px; border: 1px solid;">
        <div class="row" >
            <div class="col-sm-12" align="center">
                (REP) NÚMERO DE REPARACIÓN, (MIL) LUZ INDICADORA DE LA FALLA P-MIL, (DTC) CÓDIGO DE FALLA
            </div>
        </div>

        <br>
        <div class="row" class="table-responsive">
            <div class="col-sm-3">
                <table style="width:100%;margin-left:10px;">
                    <tr>
                        <td colspan="2" align="center" style="width:30%;">REP N°</td>
                        <td align="center" style="width:70%;"> LUZ INDICADORA DE <br>FALLA ENCENDIDA (S/N) </td>
                    </tr>
                    <?php for ($x = 1; $x < 8; $x ++): ?>
                      <tr>
                          <td>
                              <input type="text" class="input_field" name="rep_1_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value="<?php if(set_value("rep_1_".$x) != ""){ echo set_value("rep_1_".$x);} else {if(isset($xrep)) echo $xrep[0][$x-1];} ?>" style="width:0.7cm;border-bottom: 1px solid;">
                          </td>
                          <td>
                              <input type="text" class="input_field" name="rep_2_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value="<?php if(set_value("rep_2_".$x) != ""){ echo set_value("rep_2_".$x);} else {if(isset($xrep)) echo $xrep[1][$x-1];} ?>" style="width:0.7cm;border-bottom: 1px solid;">
                          </td>
                          <td align="center">
                              <input type="text" class="input_field" name="luz_falla_<?php echo $x; ?>"  maxlength="1" value="<?php if(set_value("luz_falla_".$x) != ""){ echo set_value("luz_falla_".$x);} else {if(isset($xluzFalla)) echo $xluzFalla[0][$x-1];} ?>" style="width:1.5cm;border-bottom: 1px solid;">
                          </td>
                      </tr>
                    <?php endfor; ?>
                </table>
            </div>

            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-6">
                        DTC <br> TREN MOTRIZ
                    </div>
                    <div class="col-sm-3" align="right">
                        <label style="font-weight: 500;font-size: 14px;">No. Orden:</label>
                        <input type="text" class='input_field' name="" value="<?php if(set_value("idOrden") != ""){ echo set_value("idOrden");} else {if(isset($idOrden)) echo $idOrden;} ?>" style="width:2cm;">
                        <br>
                        <?php echo form_error('idOrden', '<span class="error">', '</span>'); ?>
                    </div>
                    <div class="col-sm-3" align="right"></div>
                </div>

                <table style="width:100%;margin-right:10px;">
                    <tr>
                        <td align="right">
                            KOEO <br>
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="koeo_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(set_value("koeo_".$x) != ""){ echo set_value("koeo_".$x);} else {if(isset($xkoeo)) echo $xkoeo[0][$x-1];} ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="right">
                            KOEC
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="koec_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(set_value("koec_".$x) != ""){ echo set_value("koec_".$x);} else {if(isset($xkoec)) echo $xkoec[0][$x-1];} ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="right">
                            KOER
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="koer_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(set_value("koer_".$x) != ""){ echo set_value("koer_".$x);} else {if(isset($xkoer)) echo $xkoer[0][$x-1];} ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="left">
                            CARROCERIA:
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="carroceria_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value="<?php if(set_value("carroceria_".$x) != ""){ echo set_value("carroceria_".$x);} else {if(isset($xcarroceria)) echo $xcarroceria[0][$x-1];} ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="left">
                            CHASSIS:
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="chassis_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57'value="<?php if(set_value("chassis_".$x) != ""){ echo set_value("chassis_".$x);} else {if(isset($xchassis)) echo $xchassis[0][$x-1];} ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="left">
                            INDEFINIDO:
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="indefinido_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(set_value("indefinido_".$x) != ""){ echo set_value("indefinido_".$x);} else {if(isset($xindefinido)) echo $xindefinido[0][$x-1];} ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>

                    <tr>
                        <td align="left">
                            OTRO
                        </td>
                        <?php for ($x = 1; $x < 36; $x ++): ?>
                            <?php if (($x % 6) != 0): ?>
                                <td>
                                    <input type="text" class="input_field" name="otro_<?php echo $x; ?>"  maxlength="1" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(set_value("otro_".$x) != ""){ echo set_value("otro_".$x);} else {if(isset($xotro)) echo $xotro[0][$x-1];} ?>" style="width:0.4cm;border-bottom: 1px solid;">
                                </td>
                            <?php else: ?>
                                <td>
                                    <label for="">&nbsp;&nbsp;</label>
                                </td>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </tr>
                </table>
            </div>
        </div>

        <br><br>
        <div class="row" class="table-responsive">
            <div class="col-sm-4"></div>
            <div class="col-sm-6" align="right">
                <label for="" style="color:#005dff;"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                <br>
                <label for="" style="color:#005dff;"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
            </div>
            <div class="col-sm-2" align="right">
              <a id="agregarT1" class="btn btn-success" style="color:white;">
                  <i class="fas fa-plus"></i>
              </a>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <a id="eliminarT1" class="btn btn-danger" style="color:white; width: 1cm;">
                  <i class="fas fa-minus"></i>
              </a>
            </div>
        </div>
        <div class="row" class="table-responsive">
            <div class="col-sm-12">
                <table style="width:100%;">
                    <thead>
                        <tr>
                            <td rowspan="2" style="width:30%;border:1px solid;" align="center">
                                COMENTARIOS DEL MECÁNICO <br>
                                INCLUIDA LA DESCRIPCIÓN DE LA CAUDA DEL PROBLEMA
                            </td>
                            <td rowspan="2"style="width:10%;border:1px solid;" align="center">
                                NÚMERO REP
                            </td>
                            <td rowspan="2" style="width:10%;border:1px solid;" align="center">
                                CLAVE DE DEFECTO
                            </td>
                            <td colspan="4" style="border:1px solid;" align="center">
                                REGISTRO DE LABOR
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="width:10%;border:1px solid;">
                                RENTORNO  <br>DE PARTES <br>A VENTANILLA <br>BÁSICO/FECHA
                            </td>
                            <td align="center" style="width:10%;border:1px solid;">
                                MECÁNICO <br>CLAVE
                            </td>
                            <td align="center" style="width:10%;border:1px solid;">
                                COSTO O <br>TIEMPO UTILIZADO
                            </td>
                            <td align="center" style="width:10%;border:1px solid;">
                                RELOJ <br>(CHECADAS)
                                <input type="hidden" id="indice" name="" value="<?php if(isset($xcontenidoLabor)) echo count($xcontenidoLabor)+1; else echo "1"; ?>">
                            </td>
                        </tr>
                    </thead>

                    <tbody id="cuerpoProblema">
                        <?php if (isset($xcontenidoLabor)): ?>
                            <?php foreach ($xcontenidoLabor as $index => $valor): ?>
                                <tr id='fila_a_<?php echo $index+1; ?>'>
                                    <td style='width:30%;border:1px solid;' align='center'>
                                        <input type='text' class='input_field' id='cometario_a_<?php echo $index+1; ?>' onblur='comentario_A(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][0]; ?>' style='width:100%;' disabled>
                                    </td>
                                    <td style='width:10%;border:1px solid;' align='center'>
                                        <input type='text' class='input_field' id='ref_a_<?php echo $index+1; ?>' onblur='ref_A(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][1]; ?>' style='width:100%;' disabled>
                                    </td>
                                    <td  style='width:10%;border:1px solid;' align='center'>
                                        <input type='text' class='input_field' id='clave_a_<?php echo $index+1; ?>' onblur='clave_A(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][2]; ?>' style='width:100%;' disabled>
                                    </td>
                                    <td align='center' style='width:10%;border:1px solid;'>
                                        <input type='date' class='input_field' id='fecha_a_<?php echo $index+1; ?>' onblur='retorno_A(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][3]; ?>' max="<?php echo $hoy; ?>" min="<?php echo $hoy; ?>" style='width:100%;' disabled>
                                    </td>
                                    <td align='center' style='width:10%;border:1px solid;' rowspan='2'>
                                        <input type='text' class='input_field' id='mecanico_<?php echo $index+1; ?>' onblur='mecanico(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][4]; ?>' style='width:80%;' disabled>
                                    </td>
                                    <td align='center' style='width:10%;border:1px solid;' rowspan='2'>
                                        <input type='text' class='input_field' id='costo_<?php echo $index+1; ?>' onblur='costo(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][5]; ?>' style='width:80%;' disabled>
                                    </td>
                                    <td align='left' style='width:15%;border:1px solid;'>
                                        INICIO <input type='text' class='input_field' id='tinicio_a_<?php echo $index+1; ?>' onblur='tInicio(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][6]; ?>' max="<?php echo $hora; ?>" min="<?php echo $hora; ?>" style='width:60%;' disabled>
                                    </td>
                                </tr>
                                <tr id='fila_b_<?php echo $index+1; ?>'>
                                    <td style='width:30%;border:1px solid;' align='center'>
                                        <input type='text' class='input_field' id='cometario_b_<?php echo $index+1; ?>' onblur='comentario_B(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][7]; ?>' style='width:100%;' disabled>
                                    </td>
                                    <td style='width:10%;border:1px solid;' align='center'>
                                        <input type='text' class='input_field' id='ref_b_<?php echo $index+1; ?>' onblur='ref_B(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][8]; ?>' style='width:100%;' disabled>
                                    </td>
                                    <td  style='width:10%;border:1px solid;' align='center'>
                                        <input type='text' class='input_field' id='clave_b_<?php echo $index+1; ?>' onblur='clave_B(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][9]; ?>' style='width:100%;' disabled>
                                    </td>
                                    <td align='center' style='width:10%;border:1px solid;'>
                                        <input type='date' class='input_field' id='fecha_b_<?php echo $index+1; ?>' onblur='retorno_B(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][10]; ?>' style='width:100%;' disabled>
                                    </td>
                                    <td align='left' style='width:15%;border:1px solid;'>
                                        TERMINACIÓN  <input type='text' class='input_field' id='tinicio_b_<?php echo $index+1; ?>' onblur='tFinal(<?php echo $index+1; ?>)'  value='<?php echo $xcontenidoLabor[$index][11]; ?>' style='width:1.5cm;' disabled>
                                        <input type='hidden' id='renglon_<?php echo $index+1; ?>' name='contenedor[]' value='<?php echo $xcontenidoRenglon[$index]; ?>'>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            <!-- Creamos un renglon vacio para la edición -->
                            <tr id='fila_a_<?php echo count($xcontenidoLabor)+1; ?>'>
                                <td style='width:30%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='cometario_a_<?php echo count($xcontenidoLabor)+1; ?>' onblur='comentario_A(<?php echo count($xcontenidoLabor)+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td style='width:10%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='ref_a_<?php echo count($xcontenidoLabor)+1; ?>' onblur='ref_A(<?php echo count($xcontenidoLabor)+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td  style='width:10%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='clave_a_<?php echo count($xcontenidoLabor)+1; ?>' onblur='clave_A(<?php echo count($xcontenidoLabor)+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td align='center' style='width:10%;border:1px solid;'>
                                    <input type='date' class='input_field' id='fecha_a_<?php echo count($xcontenidoLabor)+1; ?>' onblur='retorno_A(<?php echo count($xcontenidoLabor)+1; ?>)'  value='<?php echo $hoy; ?>' max="<?php echo $hoy; ?>" min="<?php echo $hoy; ?>" style='width:100%;'>
                                </td>
                                <td align='center' style='width:10%;border:1px solid;' rowspan='2'>
                                    <input type='text' class='input_field' id='mecanico_<?php echo count($xcontenidoLabor)+1; ?>' onblur='mecanico(<?php echo count($xcontenidoLabor)+1; ?>)'  value='' style='width:80%;'>
                                </td>
                                <td align='center' style='width:10%;border:1px solid;' rowspan='2'>
                                    <input type='text' class='input_field' id='costo_<?php echo count($xcontenidoLabor)+1; ?>' onblur='costo(<?php echo count($xcontenidoLabor)+1; ?>)'  value='' style='width:80%;'>
                                </td>
                                <td align='left' style='width:15%;border:1px solid;'>
                                    INICIO <input type='text' class='input_field' id='tinicio_a_<?php echo count($xcontenidoLabor)+1; ?>' onblur='tInicio(<?php echo count($xcontenidoLabor)+1; ?>)'  value='<?php echo $hora; ?>' max="<?php echo $hora; ?>" min="<?php echo $hora; ?>" style='width:60%;'>
                                </td>
                            </tr>
                            <tr id='fila_b_<?php echo count($xcontenidoLabor)+1; ?>'>
                                <td style='width:30%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='cometario_b_<?php echo count($xcontenidoLabor)+1; ?>' onblur='comentario_B(<?php echo count($xcontenidoLabor)+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td style='width:10%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='ref_b_<?php echo count($xcontenidoLabor)+1; ?>' onblur='ref_B(<?php echo count($xcontenidoLabor)+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td  style='width:10%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='clave_b_<?php echo count($xcontenidoLabor)+1; ?>' onblur='clave_B(<?php echo count($xcontenidoLabor)+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td align='center' style='width:10%;border:1px solid;'>
                                    <input type='date' class='input_field' id='fecha_b_<?php echo count($xcontenidoLabor)+1; ?>' onblur='retorno_B(<?php echo count($xcontenidoLabor)+1; ?>)'  value='<?php echo $hoy; ?>' style='width:100%;'>
                                </td>
                                <td align='left' style='width:15%;border:1px solid;'>
                                    TERMINACIÓN  <input type='text' class='input_field' id='tinicio_b_<?php echo count($xcontenidoLabor)+1; ?>' onblur='tFinal(<?php echo count($xcontenidoLabor)+1; ?>)'  value='<?php echo $hora; ?>' style='width:1.5cm;'>
                                    <input type='hidden' id='renglon_<?php echo count($xcontenidoLabor)+1; ?>' name='contenedor[]' value=''>
                                </td>
                            </tr>
                        <?php else: ?>
                            <tr id='fila_a_1'>
                                <td style='width:30%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='cometario_a_1' onblur='comentario_A(1)'  value='' style='width:100%;'>
                                </td>
                                <td style='width:10%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='ref_a_1' onblur='ref_A(<?php echo $index+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td  style='width:10%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='clave_a_1' onblur='clave_A(<?php echo $index+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td align='center' style='width:10%;border:1px solid;'>
                                    <input type='date' class='input_field' id='fecha_a_1' onblur='retorno_A(<?php echo $index+1; ?>)'  value='<?php echo $hoy; ?>' max="<?php echo $hoy; ?>" min="<?php echo $hoy; ?>" style='width:100%;'>
                                </td>
                                <td align='center' style='width:10%;border:1px solid;' rowspan='2'>
                                    <input type='text' class='input_field' id='mecanico_1' onblur='mecanico(<?php echo $index+1; ?>)'  value='' style='width:80%;'>
                                </td>
                                <td align='center' style='width:10%;border:1px solid;' rowspan='2'>
                                    <input type='text' class='input_field' id='costo_1' onblur='costo(<?php echo $index+1; ?>)'  value='' style='width:80%;'>
                                </td>
                                <td align='left' style='width:15%;border:1px solid;'>
                                    INICIO <input type='text' class='input_field' id='tinicio_a_1' onblur='tInicio(<?php echo $index+1; ?>)'  value='<?php echo $hora; ?>' max="<?php echo $hora; ?>" min="<?php echo $hora; ?>" style='width:60%;'>
                                </td>
                            </tr>
                            <tr id='fila_b_1'>
                                <td style='width:30%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='cometario_b_1' onblur='comentario_B(<?php echo $index+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td style='width:10%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='ref_b_1' onblur='ref_B(<?php echo $index+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td  style='width:10%;border:1px solid;' align='center'>
                                    <input type='text' class='input_field' id='clave_b_1' onblur='clave_B(<?php echo $index+1; ?>)'  value='' style='width:100%;'>
                                </td>
                                <td align='center' style='width:10%;border:1px solid;'>
                                    <input type='date' class='input_field' id='fecha_b_1' onblur='retorno_B(<?php echo $index+1; ?>)'  value='<?php echo $hoy; ?>' style='width:100%;'>
                                </td>
                                <td align='left' style='width:15%;border:1px solid;'>
                                    TERMINACIÓN  <input type='text' class='input_field' id='tinicio_b_1' onblur='tFinal(<?php echo $index+1; ?>)'  value='<?php echo $hora; ?>' style='width:1.5cm;'>
                                    <input type='hidden' id='renglon_1' name='contenedor[]' value=''>
                                </td>
                            </tr>
                        <?php endif; ?>

                    </tbody>
                </table>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-3" align="center">
                <?php if (isset($firma1)): ?>
                    <?php if ($firma1 != ""): ?>
                        <input type='hidden' id='rutaFirma1' name='rutaFirma1' value='<?php if(set_value("rutaFirma1") != ""){ echo set_value("rutaFirma1");} else {if(isset($firma1)) echo $firma1;} ?>'>
                        <img class='marcoImg' src='<?php echo base_url().$firma1; ?>' id='firma1' style='width:160px;height:70px;'>
                    <?php else: ?>
                        <input type='hidden' id='rutaFirma1' name='rutaFirma1' value='<?php echo set_value("rutaFirma1"); ?>'>
                        <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma1' style='width:160px;height:70px;'>
                    <?php endif; ?>
                <?php else: ?>
                    <input type='hidden' id='rutaFirma1' name='rutaFirma1' value='<?php echo set_value("rutaFirma1"); ?>'>
                    <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma1' style='width:160px;height:70px;'>
                <?php endif; ?>

                <br>
                <input type="text" class='input_field' name="nombre1" value="<?php if(set_value("nombre1") != ""){ echo set_value("nombre1");} else {if(isset($nombre1)) echo $nombre1;} ?>">
                <br>
                <?php echo form_error('rutaFirma1', '<span class="error">', '</span>'); ?>
                <?php echo form_error('nombre1', '<span class="error">', '</span>'); ?>
                <br>
                <?php if (isset($firma1)): ?>
                    <?php if ($firma1 == ""): ?>
                        <a id="firma_1" class='cuadroFirma btn btn-primary' data-value="Firma1" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                            <!-- <i class='fas fa-pen-fancy'></i>  -->
                            Firma
                        </a>
                    <?php endif; ?>
                <?php else: ?>
                    <a id="firma_1" class='cuadroFirma btn btn-primary' data-value="Firma1" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                        <!-- <i class='fas fa-pen-fancy'></i>  -->
                        Firma
                    </a>
                <?php endif; ?>

            </div>
            <div class="col-sm-3" align="center">
                <?php if (isset($firma2)): ?>
                    <?php if ($firma2 != ""): ?>
                        <input type='hidden' id='rutaFirma2' name='rutaFirma2' value='<?php if(set_value("rutaFirma2") != ""){ echo set_value("rutaFirma2");} else {if(isset($firma2)) echo $firma2;} ?>'>
                        <img class='marcoImg' src='<?php echo base_url().$firma2; ?>' id='firma2' style='width:160px;height:70px;'>
                    <?php else: ?>
                        <input type='hidden' id='rutaFirma2' name='rutaFirma2' value='<?php echo set_value("rutaFirma2"); ?>'>
                        <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma2' style='width:160px;height:70px;'>
                    <?php endif; ?>
                <?php else: ?>
                    <input type='hidden' id='rutaFirma2' name='rutaFirma2' value='<?php echo set_value("rutaFirma2"); ?>'>
                    <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firma2' style='width:160px;height:70px;'>
                <?php endif; ?>
                <br>
                <input type="text" class='input_field' name="nombre2" value="<?php if(set_value("nombre2") != ""){ echo set_value("nombre2");} else {if(isset($nombre2)) echo $nombre2;} ?>">
                <br>
                <?php echo form_error('rutaFirma2', '<span class="error">', '</span>'); ?>
                <?php echo form_error('nombre2', '<span class="error">', '</span>'); ?>
                <br>
                <?php if (isset($firma2)): ?>
                    <?php if ($firma2 == ""): ?>
                        <a id="firma_2" class='cuadroFirma btn btn-primary' data-value="Firma2" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                            <!-- <i class='fas fa-pen-fancy'></i>  -->
                            Firma
                        </a>
                    <?php endif; ?>
                <?php else: ?>
                    <a id="firma_2" class='cuadroFirma btn btn-primary' data-value="Firma2" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                        <!-- <i class='fas fa-pen-fancy'></i>  -->
                        Firma
                    </a>
                <?php endif; ?>
            </div>
            <div class="col-sm-3"></div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" align="center">
                <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="3">
                <input type="hidden" name="" id="formularioHoja" value="Garantia2">
                <input type="hidden" id='idOrdenInput' name="idOrden" value="<?php if(set_value("idOrden") != ""){ echo set_value("idOrden");} else {if(isset($idOrden)) echo $idOrden;} ?>" style="width:2cm;">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <!-- <button type="button" id="regresarBtn" class="btn btn-dark" onclick="location.href='<?=base_url()."Garantias_lista/4";?>';" >
                    Regresar
                </button>
                <button type="submit" class="btn btn-success" name="" id="aceptarCotizacion" >Actualizar</button> -->
                <?php if (isset($firma2)): ?>
                    <?php if (($firma2 == "")||($firma1 == "")): ?>
                        <button type="submit" class="btn btn-success" name="" id="aceptarCotizacion" >Actualizar</button>
                    <?php else: ?>
                      <button type="button" id="regresarBtn" class="btn btn-dark" onclick="location.href='<?=base_url()."Lista_Por_Firmas/_";?>';" >
                          Regresar
                      </button>
                    <?php endif; ?>
                <?php else: ?>
                    <button type="submit" class="btn btn-success" name="" id="aceptarCotizacion" >Actualizar</button>
                <?php endif; ?>
            </div>
            <div class="col-sm-4"></div>
        </div>

    </form>
</section>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Formulario de superusuario -->
<div class="modal fade " id="superUsuario" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Ingreso.</h5>
            </div>
            <div class="modal-body" style="font-size:12px;">
                <label for="" style="font-size: 14px;font-weight: initial;">Usuario:</label>
                <br>
                <input type="text" class="input_field" type="text" id="superUsuarioName" value="" style="width:100%;">
                <br><br>
                <label for="" style="font-size: 14px;font-weight: initial;">Password:</label>
                <br>
                <input type="password" class="input_field" type="text" id="superUsuarioPass" value="" style="width:100%;">
                <br><br><br>
                <h4 class="error" style="font-size: 16px;font-weight: initial;" align="center" id="superUsuarioError">:</h4>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="superUsuarioEnvio">Validar</button>
            </div>
        </div>
    </div>
</div>
