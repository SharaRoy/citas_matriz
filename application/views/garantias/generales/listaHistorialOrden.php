<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Ordenes de servicio</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <!--<div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>-->
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_fechas" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <!--<i class="fa fa-trash"></i>-->
                                Limpiar Busqueda
                            </button>
                        </div>
                        <!-- formulario de busqueda -->
                        <!--<div class="col-md-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!- - /.formulario de busqueda -->
                    </div>
                    
                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:12px;background-color: #eee;">
                                    <td align="center" rowspan="2" style="width: 5%;"></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>NO. <br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>APERTURA<br> ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>PLACAS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>TÉCNICO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>CLIENTE</strong></td>
                                    <!-- <td align="center" style="width: 10%;"><strong>FECHA MODIFICACIÓN</strong></td> -->
                                    <td align="center" colspan="6" style=""><strong>ACCIONES</strong></td>
                                </tr>
                                <tr style="background-color: #ddd;">
                                    <td align="center" rowspan="2" style="width: 5%;font-size:9px;">ORDEN</td>
                                    <td align="center" rowspan="2" style="width: 5%;font-size:9px;">ARC.OASIS</td>
                                    <td align="center" rowspan="2" style="width: 5%;font-size:9px;">MULTIPUNTO</td>
                                    <td align="center" rowspan="2" style="width: 5%;font-size:9px;">COTIZACIÓN</td>
                                    <td align="center" rowspan="2" style="width: 5%;font-size:9px;">VOZ CLIENTE</td>
                                    <?php if ($this->session->userdata('rolIniciado')): ?>
                                        <?php if (($this->session->userdata('usuario') == "juan")||($this->session->userdata('usuario') == "fordplasematriz")||($this->session->userdata('usuario') == "Angel Salas")||($this->session->userdata('usuario') == "egarcia1")||($this->session->userdata('usuario') == "shara")): ?>
                                            <td align="center" rowspan="2" style="width: 5%;font-size:9px;">COMENTARIOS</td>
                                        <?php endif ?>    
                                    <?php endif ?>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <td align="center" style="vertical-align: middle;">
                                                <!-- <?php echo count($idOrden) - $index; ?> -->
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $fechaDiag[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombreAsesor[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombreConsumidor2[$index]; ?>
                                            </td>
                                            <!-- <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $fechaAct[$index]; ?>
                                            </td> -->
                                            <td align="center" style="vertical-align: middle;">
                                                <!--<a href="<?=base_url().'OrdenServicio_PDF/'.$idOrden[$index];?>" class="btn btn-info" target="_blank" style="font-size: 11px;">ORDEN</a>-->
                                                <a href="<?=base_url().'OrdenServicio_Revision/'.$idOrden[$index];?>" class="btn btn-info" target="_blank" style="font-size: 10px;">ORDEN</a>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($archivoOasis[$index] != ""): ?>
                                                    <?php $indice = 0; ?>
                                                    <?php for ($i = 0; $i < count($archivoOasis[$index]); $i++): ?>
                                                        <?php if ($archivoOasis[$index][$i] != ""): ?>
                                                            <a href="<?=base_url().$archivoOasis[$index][$i];?>" class="btn btn-primary" target="_blank" style="font-size: 10px;margin-top:5px;">
                                                                ARC. OASIS (<?php echo $indice++; ?>)
                                                            </a>
                                                            <br>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>
                                                    <!-- <a href="<?=base_url().$archivoOasis[$index];?>" class="btn btn-primary" target="_blank" style="font-size: 11px;">
                                                        ARC. OASIS (<?php echo $i+1; ?>)
                                                    </a> -->
                                                <?php else: ?>
                                                    <button type="button" class="btn btn-default" name="" style="font-size: 10px;">SIN <br> ARCHIVO</button>
                                                <?php endif; ?> 
                                            </td>

                                            <?php if (isset($pOrigen)): ?>
                                                <?php if (($pOrigen == "JDT")||($pOrigen == "ASE")||($pOrigen == "PCO")): ?>
                                                    <td align="center" style="vertical-align: middle;">
                                                        <?php if ($multipunto[$index] != ""): ?>
                                                            <!--<a href="<?=base_url().'Multipunto_PDF/'.$idOrden[$index];?>" class="btn btn-danger" target="_blank" style="font-size: 11px;">MULTIPUNTOS</a> -->
                                                            <a href="<?=base_url().'Multipunto_Revision/'.$idOrden[$index];?>" class="btn <?php if($firmasMultipunto[$index] != "") echo 'btn-success'; else echo 'btn-danger'; ?>" target="_blank" style="font-size: 10px;">MULTIPUNTOS</a>
                                                        <?php endif; ?>
                                                        <label style="color:blue;font-weight: bold;"><?php if(isset($lavado[$index])) echo $lavado[$index]; ?></label>
                                                    </td>

                                                <?php else: ?>
                                                    <td align="center" style="vertical-align: middle;">
                                                        <?php if ($multipunto[$index] != ""): ?>
                                                            <!--<a href="<?=base_url().'Multipunto_PDF/'.$idOrden[$index];?>" class="btn btn-danger" target="_blank" style="font-size: 11px;">MULTIPUNTOS</a> -->
                                                            <a href="<?=base_url().'Multipunto_Revision/'.$idOrden[$index];?>" class="btn btn-danger" target="_blank" style="font-size: 10px;">MULTIPUNTOS</a>
                                                        <?php endif; ?>
                                                        <label style="color:blue;font-weight: bold;"><?php if(isset($lavado[$index])) echo $lavado[$index]; ?></label>
                                                    </td>
                                                <?php endif ?>
                                                
                                            <?php else: ?>                                                
                                                <td align="center" style="vertical-align: middle;">
                                                    <?php if ($multipunto[$index] != ""): ?>
                                                        <!--<a href="<?=base_url().'Multipunto_PDF/'.$idOrden[$index];?>" class="btn btn-danger" target="_blank" style="font-size: 11px;">MULTIPUNTOS</a> -->
                                                        <a href="<?=base_url().'Multipunto_Revision/'.$idOrden[$index];?>" class="btn btn-danger" target="_blank" style="font-size: 10px;">MULTIPUNTOS</a>
                                                    <?php endif; ?>
                                                    <label style="color:blue;font-weight: bold;"><?php if(isset($lavado[$index])) echo $lavado[$index]; ?></label>
                                                </td>
                                            <?php endif ?>
                                            
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($cotizacion[$index] != ""): ?>
                                                    <?php if ($cotizacionFinaliza[$index] != ""): ?>
                                                        <a href="<?=base_url()."Cotizacion_Cliente/".$idOrden[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">COTIZACIÓN</a>
                                                    <?php else: ?>
                                                        <a href="<?=base_url()."Cotizacion_PDF/".$idOrden[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">COTIZACIÓN</a>
                                                    <?php endif ?>   
                                                <?php endif; ?>
                                            </td>
                                            
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($vozCliente[$index] != ""): ?>
                                                    <!--<a href="<?=base_url().'VCliente_PDF/'.$idOrden[$index];?>" class="btn btn-dark" target="_blank" style="font-size: 11px;color:white;height:100%;">VOZ CLIENTE</a> -->
                                                    <a href="<?=base_url().'VCliente_Revision/'.$idOrden[$index];?>" class="btn btn-dark" target="_blank" style="font-size: 10px;color:white;height:100%;">VOZ CLIENTE</a>
                                                    
                                                    <br>
                                                    <?php if ($vozAudio[$index] != ""): ?>
                                                        <a class="AudioModal" data-value="<?php echo $vozAudio[$index]; ?>" data-orden="<?php echo $idOrden[$index]; ?>" data-target="#muestraAudio" data-toggle="modal" style="color:blue;font-size:24px;">
                                                            <i class="far fa-play-circle" style="font-size:24px;"></i>
                                                        </a>.
                                                    <?php else: ?>
                                                        <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>

                                            <!--Opcion de cometario para jefe de taller y cierre de orden-->
                                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                                <?php if ((($pOrigen == "JDT")||($pOrigen == "PCO"))&&($this->session->userdata('usuario') == "juan")||($this->session->userdata('usuario') == "fordplasematriz")||($this->session->userdata('usuario') == "Angel Salas")||($this->session->userdata('usuario') == "egarcia1")||($this->session->userdata('usuario') == "shara")): ?>
                                                    <td align="center" style="vertical-align: middle;">
                                                        <a class="addComentarioBtn" onclick="btnAddClick(<?php echo $idOrden[$index]; ?>)" title="Agregar Comentario" data-id="<?php echo $idOrden[$index]; ?>" data-target="#addComentario" data-toggle="modal">
                                                            <i class="fas fa-comments" style="font-size: 18px;color: gray;"></i>
                                                        </a>

                                                        <a class="historialCometarioBtn" onclick="btnHistorialClick(<?php echo $idOrden[$index]; ?>)" title="Historial comentarios" data-id="<?php echo $idOrden[$index]; ?>" data-target="#historialCometario" data-toggle="modal">
                                                            <i class="fas fa-info" style="font-size: 18px;color: gray;"></i>
                                                        </a>
                                                    </td>
                                                <?php endif ?>
                                            <?php endif ?>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="10" style="width:100%;" align="center">
                                            <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                                <h4>Sin registros que mostrar</h4>
                                            <?php else: ?>
                                                <h4>Sesión Expirada</h4>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" autoplay style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>
