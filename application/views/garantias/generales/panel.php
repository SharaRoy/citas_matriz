<!-- Contenido principal (Formulario) -->
<section class="content">
    <div class="row">
        <div class="col-sm-12" align="center" style="">
            <br><br>
            <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:54%;">
            <!-- <h2 style="font-size: 30px; color: #716c6c;">BIENVENIDOS A:</h2>
    				<img src="<?php echo base_url(); ?>assets/imgs/logos/LOGO-SOHEX.png" alt="" class="logo" style="width:360px;height:120px;">
    				<h3 style="font-size: 20px;font-style: italic;font-weight: 400;color: #716c6c;">ESTAMOS CAMBIANDO Y TRABAJANDO POR TI!</h3> -->
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10" align="center">
             <h3>PANEL GARANTÍAS</h3> 
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <div class="alert alert-success" align="center" style="display:none;">
                <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
            </div>
            <div class="alert alert-danger" align="center" style="display:none;">
                <strong style="font-size:20px !important;">Fallo el proceso.</strong>
            </div>
            <div class="alert alert-warning" align="center" style="display:none;">
                <strong style="font-size:20px !important;">Campos incompletos.</strong>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
    
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='<?=base_url()."Garantia/0";?>';" >
                Nueva Garantía
            </button>
        </div>
        <div class="col-sm-2"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."Garantias_lista/4";?>';" >
                Garantías
            </button>
        </div>
        <div class="col-sm-2"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href='<?=base_url()."Lista_Por_Firmas/4";?>';" >
                Garantías por firmar
            </button>
        </div>
        <div class="col-sm-2"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <button type="button" class="btn btn-lg btn-block" style="background-color: #7090ac;color:white;" onclick="location.href='<?=base_url()."Documentacion_Garantias/4";?>';" >Documentación Ordenes</button>
        </div>
        <div class="col-sm-2"></div>
    </div>
    
    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
             <button type="button" class="btn btn-lg btn-block" style="background-color:lightgreen;" onclick="location.href='<?=base_url()."Listar_Garantias/4";?>';" >
                    Presupuestos Multipunto con Garantías
            </button>
        </div>
        <div class="col-sm-2"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <a href="https://sohex.mx/cs/linea_proceso/" class="btn btn-lg btn-block" style="color:blue;background-color: #ccc;border: 1px solid;" target="_blank">
                MANUAL DE SEGUIMIENTO
            </a>
        </div>
        <div class="col-sm-2"></div>
    </div>
    
    <br><br>
</section>
<!-- Contenido principal (Formulario) -->
