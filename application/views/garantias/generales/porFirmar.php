<div >
    <br><br>
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <br>
    <div class="panel-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12">
                    <h3 align="center">GARANTÍAS</h3>
                </div>
            </div>

            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-sm-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;">
                                    <td align="center" style="width: 5%;vertical-align: middle;" rowspan="2">#</td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>No ORDEN</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>FOLIO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;" rowspan="2"><strong>TELEFONO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>FECHA RECIBE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>FECHA ENTREGA</strong></td>
                                    <!-- <td align="center" style="width: 10%;vertical-align: middle;" rowspan="2"><strong>NO. DISTRIBUIDOR</strong></td> -->
                                    <td align="center" colspan="2"><strong>ACCIONES</strong></td>
                                </tr>
                                <tr style="font-size:14px;">
                                    <td align="center" style="width: 10%;"><strong>GARANTÍA</strong></td>
                                    <!-- <td align="center" style="width: 10%;"><strong>PDF</strong></td> -->
                                    <td align="center" style="width: 10%;"><strong>G. F1863</strong></td>
                                    <!-- <td align="center" style="width: 10%;"><strong>PDF</strong></td> -->
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idRegistro)): ?>
                                    <?php foreach ($idRegistro as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="width:10%;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $folio[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $idAsesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $nombreProp[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $telefono[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $fechaRecibe[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $fechaEntrega[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php if (($firmaVal[$index] != "")&&($firmaVal2[$index] != "")): ?>
                                                    <input type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Garantia_Edit/".$id_cita_url[$index];?>';" value="Editar">
                                                <?php else: ?>
                                                    <input type="button" class="btn btn-warning" style="color:white;" onclick="location.href='<?=base_url()."Garantia_Firma/".$id_cita_url[$index];?>';" value="Firmar">
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php if ($garantia2[$index] == "1"): ?>
                                                    <input type="button" class="btn btn-success" style="color:white;" onclick="location.href='<?=base_url()."Garantia_F1863/".$id_cita_url[$index];?>';" value="Nuevo">
                                                <?php elseif ($garantia2[$index] == "3"): ?>
                                                    <input type="button" class="btn btn-warning" style="color:white;" onclick="location.href='<?=base_url()."Garantia_F1863_Firma/".$id_cita_url[$index];?>';" value="Firmar">
                                                <?php else: ?>
                                                    <input type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Garantia_F1863_Edit/".$id_cita_url[$index];?>';" value="Editar">
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="11" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
