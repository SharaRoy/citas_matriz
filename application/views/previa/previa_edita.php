<div style="margin:20px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."previa/FormatoPrevio/validateForm"?>" autocomplete="on">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Registro actualizado con exito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
        </div>

        <br>
        <table style="width:100%;border: 2px solid;" class="table-responsive">
            <tr>
                <td style="width: 4%;border: 1px solid;" align="center">
                    <a id="superUsuarioA" class="h2_title_blue" data-target="#superUsuario" data-toggle="modal" style="color:blue;margin-top:20px;cursor: pointer;">
                        <i class="fas fa-user"></i>
                    </a>
                </td>
                <td style="width:17%;border-bottom: 2px solid;vertical-align: middle;" align="center">
                    <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:170px;height:80px;">
                </td>
                <td style="width: 14%;border-bottom: 2px solid;border-left: 1px solid;vertical-align: middle;" align="center">
                    <img src="<?php echo base_url(); ?>assets/imgs/logos/lincoln.png" alt="" class="logo" style="width:150px;height:80px;">
                </td>
                <td style="width: 14%;border-bottom: 2px solid;border-left: 1px solid;vertical-align: middle;" align="center">
                    <img src="<?php echo base_url(); ?>assets/imgs/logos/mercury.png" alt="" class="logo" style="width:150px;height:80px;">
                </td>
                <td style="width: 25%;border-bottom: 2px solid;border-left: 1px solid;vertical-align: middle;" align="center">
                    <h4>LISTA DE REQUERIMIENTOS <br>PARA PREVIA ENTREGA</h4>
                </td>
                <td style="width: 25%;">
                    <table style="width:100%;border: 2px solid;">
                        <tr>
                            <td style="width:100%;border-bottom: 2px solid;" colspan="2">
                                <strong>VIN:</strong>
                                <input type="text" class="input_field" name="txtVin" value="<?php if(set_value("txtVin") != "") echo set_value("txtVin"); else {if(isset($vin)) echo $vin;} ?>" style="width:80%;"><br>
                                <?php echo form_error('txtVin', '<span class="error">', '</span>'); ?>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <strong>UNIDAD:</strong>
                                <input type="text" class="input_field" name="txtUnidad" value="<?php if(set_value("txtUnidad") != "") echo set_value("txtUnidad"); else {if(isset($unidad)) echo $unidad;} ?>" style="width:60%;"><br>
                                <?php echo form_error('txtUnidad', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width:50%;">
                                <strong>CAT:</strong>
                                <input type="text" class="input_field" name="txtCat" value="<?php if(set_value("txtCat") != "") echo set_value("txtCat"); else {if(isset($cat)) echo $cat;} ?>" style="width:60%;"><br>
                                <?php echo form_error('txtCat', '<span class="error">', '</span>'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:100%;border-bottom: 2px solid;" colspan="2">
                                <strong>COLOR:</strong>
                                <input type="text" class="input_field" name="txtColor" value="<?php if(set_value("txtColor") != "") echo set_value("txtColor"); else {if(isset($color)) echo $color;} ?>" style="width:80%;"><br>
                                <?php echo form_error('txtColor', '<span class="error">', '</span>'); ?>
                                <br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <!-- Lado izquierdo de la hoja -->
                <td style="" align="right" colspan="4">
                    <table style="width:100%;margin-left: 5px;margin-right:5px;">
                        <tr>
                            <td style="" align="right" colspan="2">
                                <br>
                                <label for=""><u>
                                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REVISIONES</strong></u>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtA1" <?php echo set_checkbox('txtA1', '1'); ?> <?php if(isset($revisiones)) if( $revisiones[0] == "1") echo "checked"; ?>>&nbsp;&nbsp;A1<br>
                                <?php echo form_error('txtA1', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Inspeccionar carga de batería a 12.4V antes de encender el motor. Recargar si el indicador está en rojo.
                                Baterías sin indicador, recargar a 12.4 Volts o más* Voltaje =
                                <input type="text" class="input_field" name="txtInputA1" value="<?php if(set_value("txtInputA1") != "") echo set_value("txtInputA1"); else {if(isset($voltaje)) echo $voltaje;} ?>" style="width:2cm;"><br>
                                <?php echo form_error('txtInputA1', '<span class="error">', '</span>'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtA3" <?php echo set_checkbox('txtA3', '1'); ?> <?php if(isset($revisiones)) if( $revisiones[1] == "1") echo "checked"; ?>>&nbsp;&nbsp;A3<br>
                                <?php echo form_error('txtA3', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Inspeccionar visualmente todos los componentes debajo del cofre. Verifique fugas.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtA4" <?php echo set_checkbox('txtA4', '1'); ?> <?php if(isset($revisiones)) if( $revisiones[2] == "1") echo "checked"; ?>>&nbsp;&nbsp;A4<br>
                                <?php echo form_error('txtA4', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Inspeccionar visualmente llantas y componentes por debajo del vehículo. Verifique fugas.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtA8" <?php echo set_checkbox('txtA8', '1'); ?> <?php if(isset($revisiones)) if( $revisiones[3] == "1") echo "checked"; ?>>&nbsp;&nbsp;A8<br>
                                <?php echo form_error('txtA8', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Inspeccionar visualmente la localización y el buen estado del kit para inflar llantas (si está equipado).
                            </td>
                        </tr>

                        <tr>
                            <td style="" align="right" colspan="2">
                                <label for=""><u>
                                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VERIFIQUE Y PONER A ESPECIFICACIÓN</strong></u>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtB1" <?php echo set_checkbox('txtB1', '1'); ?> <?php if(isset($especificacion)) if( $especificacion[0] == "1") echo "checked"; ?>>&nbsp;&nbsp;B1<br>
                                <?php echo form_error('txtB1', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar las presiones de llantas (incluyendo la de refacción) (ajuste a temperatura ambiente). *
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtB3" <?php echo set_checkbox('txtB3', '1'); ?> <?php if(isset($especificacion)) if( $especificacion[1] == "1") echo "checked"; ?>>&nbsp;&nbsp;B3<br>
                                <?php echo form_error('txtB3', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar todos los niveles de fluidos de motor, transmisión, frenos, sistema enfriamiento, embrague, acumulador, dirección, diferencial y recipiente del lavaparabrisas delantero y trasero.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtB4" <?php echo set_checkbox('txtB4', '1'); ?> <?php if(isset($especificacion)) if( $especificacion[2] == "1") echo "checked"; ?>>&nbsp;&nbsp;B4<br>
                                <?php echo form_error('txtB4', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                 Verificar par de apriete terminales de acumulador.
                            </td>
                        </tr>

                        <tr>
                            <td style="" align="right" colspan="2">
                                <label for=""><u>
                                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REVISAR OPERACIÓN</strong></u>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF2" <?php echo set_checkbox('txtF2', '1'); ?> <?php if(isset($operacion)) if( $operacion[0] == "1") echo "checked"; ?>>&nbsp;&nbsp;F2<br>
                                <?php echo form_error('txtF2', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                 Cinturones de seguridad (incluyendo la hebilla del asiento central, si está equipado).
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF3" <?php echo set_checkbox('txtF3', '1'); ?> <?php if(isset($operacion)) if( $operacion[1] == "1") echo "checked"; ?>>&nbsp;&nbsp;F3<br>
                                <?php echo form_error('txtF3', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                 Seguros y mecanismos de posición de los respaldos de los asientos.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF6" <?php echo set_checkbox('txtF6', '1'); ?> <?php if(isset($operacion)) if( $operacion[2] == "1") echo "checked"; ?>>&nbsp;&nbsp;F6<br>
                                <?php echo form_error('txtF6', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Luces: Faros; Traseras; Direccionales; Intermitentes; de Posición; de Cortesía; de Reversa:
                                Faros para niebla y de Alta intensidad (si está equipado); además de los Indicadores de advertencia de los instrumentos,
                                equipo de sonido USB y entrada auxiliar. *
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF7" <?php echo set_checkbox('txtF7', '1'); ?> <?php if(isset($operacion)) if( $operacion[3] == "1") echo "checked"; ?>>&nbsp;&nbsp;F7<br>
                                <?php echo form_error('txtF7', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Indicador de las bolsas de aire y del tablero para su operación apropiada
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF12" <?php echo set_checkbox('txtF12', '1'); ?> <?php if(isset($operacion)) if( $operacion[4] == "1") echo "checked"; ?>>&nbsp;&nbsp;F12<br>
                                <?php echo form_error('txtF12', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Freno de estacionamiento (incluyendo indicador de advertencia).*
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF15" <?php echo set_checkbox('txtF15', '1'); ?> <?php if(isset($operacion)) if( $operacion[5] == "1") echo "checked"; ?>>&nbsp;&nbsp;F15<br>
                                <?php echo form_error('txtF15', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Claxon y claxon óptico
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF16" <?php echo set_checkbox('txtF16', '1'); ?> <?php if(isset($operacion)) if( $operacion[6] == "1") echo "checked"; ?>>&nbsp;&nbsp;F16<br>
                                <?php echo form_error('txtF16', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Limpiaparabrisas y rociadores, incluyendo los sensores de lluvia, si está incluido.*
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF20" <?php echo set_checkbox('txtF20', '1'); ?> <?php if(isset($operacion)) if( $operacion[7] == "1") echo "checked"; ?>>&nbsp;&nbsp;F20<br>
                                <?php echo form_error('txtF20', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                El sistema de calefacción, aire acondicionado, ventilación, desempañador, así como el ventilador en el motor (sist enfriamiento).
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF21" <?php echo set_checkbox('txtF21', '1'); ?> <?php if(isset($operacion)) if( $operacion[8] == "1") echo "checked"; ?>>&nbsp;&nbsp;F21<br>
                                <?php echo form_error('txtF21', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Sistema de advertencia del uso de cinturones de seguridad.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtF24" <?php echo set_checkbox('txtF24', '1'); ?> <?php if(isset($operacion)) if( $operacion[9] == "1") echo "checked"; ?>>&nbsp;&nbsp;F24<br>
                                <?php echo form_error('txtF24', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Seguridad, dispositivos de entrada.
                                (cierre de seguros, llaves del sistema de antirrobo pasivo, todos los transmisores remotos).
                                Colocar tarjeta de codificación (si está equipado) en la información del usuario Colocar tarjeta de acceso sin llave.
                            </td>
                        </tr>

                        <tr>
                            <td style="" align="right" colspan="2">
                                <label for=""><u>
                                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AJUSTE Y VERIFICACIONES</strong></u>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ1" <?php echo set_checkbox('txtJ1', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[0] == "1") echo "checked"; ?>>&nbsp;&nbsp;J1<br>
                                <?php echo form_error('txtJ1', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Radio / reloj / Alarma volumétrica y perimetral / tiempo. *
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ2" <?php echo set_checkbox('txtJ2', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[1] == "1") echo "checked"; ?>>&nbsp;&nbsp;J2<br>
                                <?php echo form_error('txtJ2', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Ajustar la brújula de acuerdo a la variación de la zona*.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ4" <?php echo set_checkbox('txtJ4', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[2] == "1") echo "checked"; ?>>&nbsp;&nbsp;J4<br>
                                <?php echo form_error('txtJ4', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Apagar el monitor de la presión de llantas, si está activado*.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ5" <?php echo set_checkbox('txtJ5', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[3] == "1") echo "checked"; ?>>&nbsp;&nbsp;J5<br>
                                <?php echo form_error('txtJ5', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar suspensión de aire (si está equipado). *
                                Para Expedition / Navigator – Activar a través del Centro de Mensajes.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ6" <?php echo set_checkbox('txtJ6', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[4] == "1") echo "checked"; ?>>&nbsp;&nbsp;J6<br>
                                <?php echo form_error('txtJ6', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Sistema de entretenimiento de los asientos traseros – instalar las baterías del control remoto y audífonos.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ7" <?php echo set_checkbox('txtJ7', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[5] == "1") echo "checked"; ?>>&nbsp;&nbsp;J7<br>
                                <?php echo form_error('txtJ7', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Ajustar el idioma y el recordatorio de cambio del aceite en el Centro Electrónico de Mensajes SYNC, FIS, My Ford Touch.
                                Verificar funcionamiento USB y entrada auxiliar.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ8" <?php echo set_checkbox('txtJ8', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[6] == "1") echo "checked"; ?>>&nbsp;&nbsp;J8<br>
                                <?php echo form_error('txtJ8', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Activar los estribos desplegables automáticos en el centro de mensajes.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ9" <?php echo set_checkbox('txtJ9', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[7] == "1") echo "checked"; ?>>&nbsp;&nbsp;J9<br>
                                <?php echo form_error('txtJ9', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar el funcionamiento del mecanismo abatible de los espejos y vidrios eléctricos, reprograma One Touch si es necesario.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtJ11" <?php echo set_checkbox('txtJ11', '1'); ?> <?php if(isset($ajuste)) if( $ajuste[8] == "1") echo "checked"; ?>>&nbsp;&nbsp;J11<br>
                                <?php echo form_error('txtJ11', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Retirar los plásticos de protección lateral.
                            </td>
                        </tr>

                        <tr>
                            <td style="" align="right" colspan="2">
                                <label for=""><u>
                                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRUEBA DE CAMINO</strong></u>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH1" <?php echo set_checkbox('txtH1', '1'); ?> <?php if(isset($prueba)) if( $prueba[0] == "1") echo "checked"; ?>>&nbsp;&nbsp;H1<br>
                                <?php echo form_error('txtH1', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar manejabilidad durante la prueba de camino.
                                Si hay un problema o el indicador check Engine/ servicio de mantenimiento" está activado,
                                realizar las rutinas de diagnóstico y las reparaciones por garantía pertinentes.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH2" <?php echo set_checkbox('txtH2', '1'); ?> <?php if(isset($prueba)) if( $prueba[1] == "1") echo "checked"; ?>>&nbsp;&nbsp;H2<br>
                                <?php echo form_error('txtH2', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar que el movimiento de la mariposa del cuerpo de aceleración desde la posición de aceleración hasta la de marcha lenta, sea suave.
                                (verificar con el motor encendido y apagado).
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH3" <?php echo set_checkbox('txtH3', '1'); ?> <?php if(isset($prueba)) if( $prueba[2] == "1") echo "checked"; ?>>&nbsp;&nbsp;H3<br>
                                <?php echo form_error('txtH3', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Revisar rechinidos, golpeteos, vibraciones y sonidos de viento debidos a el sistema de flujo de aire.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH5" <?php echo set_checkbox('txtH5', '1'); ?> <?php if(isset($prueba)) if( $prueba[3] == "1") echo "checked"; ?>>&nbsp;&nbsp;H5<br>
                                <?php echo form_error('txtH5', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar funcionamiento del sistema de frenos (incluyendo el freno de mano).*
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH6" <?php echo set_checkbox('txtH6', '1'); ?> <?php if(isset($prueba)) if( $prueba[4] == "1") echo "checked"; ?>>&nbsp;&nbsp;H6<br>
                                <?php echo form_error('txtH6', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar el control del sistema de dirección para asegurar una alineación y centrado del volante adecuados.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH8" <?php echo set_checkbox('txtH8', '1'); ?> <?php if(isset($prueba)) if( $prueba[5] == "1") echo "checked"; ?>>&nbsp;&nbsp;H8<br>
                                <?php echo form_error('txtH8', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Verificar el comportamiento del embrague y la transmisión.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH10" <?php echo set_checkbox('txtH10', '1'); ?> <?php if(isset($prueba)) if( $prueba[6] == "1") echo "checked"; ?>>&nbsp;&nbsp;H10<br>
                                <?php echo form_error('txtH10', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Control de velocidad.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH12" <?php echo set_checkbox('txtH12', '1'); ?> <?php if(isset($prueba)) if( $prueba[7] == "1") echo "checked"; ?>>&nbsp;&nbsp;H12<br>
                                <?php echo form_error('txtH12', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Caja de transferencia.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtH14" <?php echo set_checkbox('txtH14', '1'); ?> <?php if(isset($prueba)) if( $prueba[8] == "1") echo "checked"; ?>>&nbsp;&nbsp;H14<br>
                                <?php echo form_error('txtH14', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Asistencia en maniobras de estacionamiento de reversa, sensores y cámara de reversa.
                            </td>
                        </tr>

                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtCod1" <?php echo set_checkbox('txtCod1', '1'); ?> <?php if(isset($cod_Confg)) if( $cod_Confg[0] == "1") echo "checked"; ?>><br>
                                <?php echo form_error('txtCod1', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Código de Llaves.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtCod2" <?php echo set_checkbox('txtCod2', '1'); ?> <?php if(isset($cod_Confg)) if( $cod_Confg[1] == "1") echo "checked"; ?>><br>
                                <?php echo form_error('txtCod2', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Código Acceso sin llave botonera.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtCod3" <?php echo set_checkbox('txtCod3', '1'); ?> <?php if(isset($cod_Confg)) if( $cod_Confg[2] == "1") echo "checked"; ?>><br>
                                <?php echo form_error('txtCod3', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Clave DOT.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtCod4" <?php echo set_checkbox('txtCod4', '1'); ?> <?php if(isset($cod_Confg)) if( $cod_Confg[3] == "1") echo "checked"; ?>><br>
                                <?php echo form_error('txtCod4', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Código Estéreo.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtCod5" <?php echo set_checkbox('txtCod5', '1'); ?> <?php if(isset($cod_Confg)) if( $cod_Confg[4] == "1") echo "checked"; ?>><br>
                                <?php echo form_error('txtCod5', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Configuración especial FordInteractive Sytem.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtCod6" <?php echo set_checkbox('txtCod6', '1'); ?> <?php if(isset($cod_Confg)) if( $cod_Confg[5] == "1") echo "checked"; ?>><br>
                                <?php echo form_error('txtCod6', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Configuración en español Sync.
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 6%;font-size:10px;vertical-align: middle;">
                                <input type="checkbox" value="1" name="txtCod7" <?php echo set_checkbox('txtCod7', '1'); ?> <?php if(isset($cod_Confg)) if( $cod_Confg[6] == "1") echo "checked"; ?>><br>
                                <?php echo form_error('txtCod7', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="width: 94%;font-size:10px;padding-left: 5px;vertical-align: middle;" align="left">
                                Configuración en español computadora de viaje.
                            </td>
                        </tr>

                    </table>
                </td>

                <!-- Lado derecho de la hoja -->
                <td style="" align="right" colspan="2">
                    <table style="width:98%;margin-left: 5px;">
                        <tr>
                            <td style="width:50%;">
                                <br><br>
                            </td>
                            <td style="width:50%;border: 2px solid;">
                                <br>
                                <strong>Folio:</strong>
                                <input type="text" class="input_field" name="txtFolio" value="<?php if(set_value("txtFolio") != "") echo set_value("txtFolio"); else {if(isset($folio)) echo $folio;} ?>" style="width:50%;" disabled>
                                <br>
                                <?php echo form_error('txtFolio', '<span class="error">', '</span>'); ?>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:50%;">
                                <br><br>
                            </td>
                            <td style="width:50%;border: 2px solid;">
                                <br>
                                <strong>No. de Orden:</strong>
                                <input type="text" class="input_field" name="txtOrden" value="<?php if(set_value("txtOrden") != "") echo set_value("txtOrden"); else {if(isset($idOrden)) echo $idOrden;} ?>" style="width:50%;" disabled>
                                <br>
                                <?php echo form_error('txtOrden', '<span class="error">', '</span>'); ?>
                                <br>

                                <br>
                                Folio Intelisis:&nbsp;
                                <label style="color:darkblue;">
                                    <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td style="" align="right" colspan="2">
                                <br><br>
                                <label for=""><u>
                                    <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ESTÁNDARES DE APARIENCIA</strong></u>
                                </label>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" align="left" colspan="2">
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:8%;">
                                            <input type="checkbox" value="1" name="txtAP1" <?php echo set_checkbox('txtAP1', '1'); ?> <?php if(isset($apariencia)) if( $apariencia[0] == "1") echo "checked"; ?>>&nbsp;&nbsp;AP1<br>
                                            <?php echo form_error('txtAP1', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td style="width:95%;">
                                            Llevar a cabo una limpieza detallada del vehículo, tanto en exteriores como en interiores, además de eliminar la suciedad de los elementos del limpiaparabrisas.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;" align="left" colspan="2">
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:8%;">
                                            <input type="checkbox" value="1" name="txtAP2" <?php echo set_checkbox('txtAP2', '1'); ?> <?php if(isset($apariencia)) if( $apariencia[1] == "1") echo "checked"; ?>>&nbsp;&nbsp;AP2<br>
                                            <?php echo form_error('txtAP2', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td style="width:95%;">
                                            Inspeccionar daños de piezas metálicas o pintura, tanto en exteriores como en interiores y retocar lo que haga falta.
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:14px;" align="center" colspan="2">
                                <br>
                                - NO ELIMINAR LA CUBIERTA PROTECTORA DEL VOLANTE Y ASIENTOS HASTA QUE SEA <br> ENTREGADO AL CLIENTE.
                                <br>
                                - NO ELIMINAR LA CUBIERTA PROTECTORA DE LA PANTALLA DEL AIRE ACONDICIONADO, NI <br> LA DEL RADIO. EL CLIENTE LAS RETIRARÁ POSTERIORMENTE.
                                <br><br>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:14px;border-top:2px solid;" align="center" colspan="2">
                                <br>
                                TARJETA DEL CLIENTE E INFORMACIÓN  DEL DISTRIBUIDOR
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center" colspan="2">
                                <input class="input_field" type="text" name="txtDistribuidor" id="txtDistribuidor" style="width:90%;text-align:center;" value="<?php if(set_value("txtDistribuidor") != "") echo set_value("txtDistribuidor"); else {if(isset($distribuidor)) echo $distribuidor;} ?>">
                                <br>
                                <?php echo form_error('txtDistribuidor', '<span class="error">', '</span>'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center" colspan="2">
                                Distribuidor
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center" colspan="2">
                                <input class="input_field" type="text" name="txtDireccion" id="txtDireccion" value="<?php if(set_value("txtDireccion") != "") echo set_value("txtDireccion"); else {if(isset($direccion)) echo $direccion;} ?>" style="width:90%;text-align:center;">
                                <br>
                                <?php echo form_error('txtDireccion', '<span class="error">', '</span>'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center" colspan="2">
                                Dirección
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center" colspan="2">
                                <input class="input_field" type="text" name="txtCiudad" id="txtCiudad" value="<?php if(set_value("txtCiudad") != "") echo set_value("txtCiudad"); else {if(isset($municipio)) echo $municipio;} ?>" style="width:90%;text-align:center;">
                                <br>
                                <?php echo form_error('txtCiudad', '<span class="error">', '</span>'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center" colspan="2">
                                Municipio/Delegación
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center">
                                <input class="input_field" type="text" name="txtEstado" id="txtEstado" value="<?php if(set_value("txtEstado") != "") echo set_value("txtEstado"); else {if(isset($estados)) echo $estados;} ?>" style="width:90%;text-align:center;">
                                <br>
                                <?php echo form_error('txtEstado', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style="font-size:12px;" align="center">
                                <input class="input_field" type="text" name="txtCp" maxlength="6" id="txtCp" value="<?php if(set_value("txtCp") != "") echo set_value("txtCp"); else {if(isset($cp)) echo $cp;} ?>" style="width:90%;text-align:center;">
                                <br>
                                <?php echo form_error('txtCp', '<span class="error">', '</span>'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center">
                                Estado
                            </td>
                            <td style="font-size:12px;" align="center">
                                Código Postal
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:12px;" align="center" colspan="2">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width:20%;">
                                            <input class="input_field" type="text" name="txtNumero" id="txtNumero" value="<?php if(set_value("txtNumero") != "") echo set_value("txtNumero"); else {if(isset($numero)) echo $numero;} ?>"  style="width:90%;text-align:center;">
                                            <br>
                                            <?php echo form_error('txtNumero', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td style="width:20%;">
                                            <input class="input_field" type="text" name="txtRadio" id="txtRadio" value="<?php if(set_value("txtRadio") != "") echo set_value("txtRadio"); else {if(isset($codigo)) echo $codigo;} ?>"  style="width:90%;text-align:center;">
                                            <br>
                                            <?php echo form_error('txtRadio', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td style="width:20%;">
                                            <input class="input_field" type="text" name="txtPuerta" id="txtPuerta" value="<?php if(set_value("txtPuerta") != "") echo set_value("txtPuerta"); else {if(isset($apertura)) echo $apertura;} ?>"  style="width:90%;text-align:center;">
                                            <br>
                                            <?php echo form_error('txtPuerta', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td style="width:20%;">
                                            <input class="input_field" type="text" name="txtDot" id="txtDot" value="<?php if(set_value("txtDot") != "") echo set_value("txtDot"); else {if(isset($llantas)) echo $llantas;} ?>"  style="width:90%;text-align:center;">
                                            <br>
                                            <?php echo form_error('txtDot', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td style="width:20%;">
                                            <input class="input_field" type="text" name="txtReserva" id="txtReserva" value="<?php if(set_value("txtReserva") != "") echo set_value("txtReserva"); else {if(isset($reserva)) echo $reserva;} ?>"  style="width:90%;text-align:center;">
                                            <br>
                                            <?php echo form_error('txtReserva', '<span class="error">', '</span>'); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;" align="center">
                                            Número de llave  /
                                        </td>
                                        <td style="width:20%;" align="center">
                                            Código de Radio  /
                                        </td>
                                        <td style="width:20%;" align="center">
                                            Apertura de puerta  /
                                        </td>
                                        <td style="width:20%;" align="center">
                                            DOT Llantas  /
                                        </td>
                                        <td style="width:20%;" align="center">
                                            Número de reserva
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td style="padding-left:10px;" align="left" colspan="2">
                                <br><br>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;font-size:14px;font-weight: initial;text-align: justify;" align="justify" colspan="2">
                                <input type="checkbox" value="1" name="txtOasis1" <?php echo set_checkbox('txtOasis1', '1'); ?> <?php if(isset($oasis)) if( $oasis[0] == "1") echo "checked"; ?>>&nbsp;
                                <?php echo form_error('txtOasis1', '<span class="error">', '</span>'); ?>&nbsp;&nbsp;
                                El OASIS ha sido verificado para identificar las acciones de servicio en campo (ASC) y boletines técnicos de servicio (BTS).
                                Confirmo que las ASC y los BTS han sido verificadas.
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;font-size:14px;font-weight: initial;text-align: justify;" align="justify" colspan="2">
                                <input type="checkbox" value="1" name="txtOasis2" <?php echo set_checkbox('txtOasis2', '1'); ?> <?php if(isset($oasis)) if( $oasis[1] == "1") echo "checked"; ?>>&nbsp;
                                <?php echo form_error('txtOasis2', '<span class="error">', '</span>'); ?>&nbsp;&nbsp;
                                El OASIS ha sido verificado y revisado usando el código de sintoma de previas (804000) para identificar cualquier
                                intrucción adicional requerida.
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;font-size:14px;font-weight: initial;text-align: justify;" align="justify" colspan="2">
                                <input type="checkbox" value="1" name="txtOasis3" <?php echo set_checkbox('txtOasis3', '1'); ?> <?php if(isset($oasis)) if( $oasis[2] == "1") echo "checked"; ?>>&nbsp;
                                <?php echo form_error('txtOasis3', '<span class="error">', '</span>'); ?>&nbsp;&nbsp;
                                Certifico que todas las verificaciones establecidas en la presete guia, han sido realizadas en este vehículo de acuerdo con
                                los procedimientos y manuales correspondientes. Todas las operaciones necesarias han sido realizadas por un técnico de servicio.
                                En el entendido de que este es un acuerdo entre Ventas y Servicio, debera ser guardado por el distribuidor en el historial
                                del vehículo.
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;font-size:14px;font-weight: initial;" align="center" colspan="2">
                                NOMBRE Y FIRMA DEL TÉCNICO
                                <br>
                                <input type="hidden" id="rutaFirmaTecnico" name="rutaFirmaTecnico" value="<?php if(isset($firmaTecnico)) echo $firmaTecnico; ?>">
                                <img class="marcoImg" src="<?php if(isset($firmaTecnico)) if($firmaTecnico != "") echo base_url().$firmaTecnico; else echo base_url().'assets/imgs/fondo_bco.jpeg'; else echo base_url().'assets/imgs/fondo_bco.jpeg';  ?>" id="firmaFirmaTecnico" alt="" style="height:2cm;width:4cm;">
                                <br>
                                <input type="text" class="input_field" id="nombreTecnico" name="nombreTecnico" value="<?php if(set_value("nombreTecnico") != "") echo set_value("nombreTecnico"); else {if(isset($tecnico)) echo $tecnico;} ?>" style="width:90%">
                                <?php echo form_error('rutaFirmaTecnico', '<br><span class="error">', '</span>'); ?>
                                <?php echo form_error('nombreTecnico', '<br><span class="error">', '</span>'); ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:10px;font-size:14px;font-weight: initial;text-align: center;" align="center" colspan="2">
                                <label for="">Fecha:</label>
                                <input type="date" class="input_field" name="txtFecha" value="<?php if(isset($fechaCaptura)) echo $fechaCaptura; else echo date("Y-m-d"); ?>" min="<?php echo date("Y-m-d"); ?>" max="<?php echo date("Y-m-d"); ?>" style="width:50%" disabled>
                                <br>
                                <?php echo form_error('txtFecha', '<span class="error">', '</span>'); ?>
                            </td>
                        </tr>

                        <tr>
                            <td style="font-size:14px;border-top:2px solid;" align="left" colspan="2">
                                <br>
                                NO DESCONECTAR VELOCÍMETRO BAJO NINGUNA CIRCUNSTANCIA
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:14px;" align="left" colspan="2">
                                <br>
                                VERIFIQUE / INSPECCIONE EL VEHÍCULO DE LA SIGUIENTE MANERA: REALICE LOS AJUSTES ESPECÍFICOS QUE SE REQUIEREN.
                                REFIÉRASE AL MANUAL DE TALLER O LA GUÍA DEL PROPIETARIO.
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:14px;" align="left" colspan="2">
                                <br>
                                NOTA: ALGUNOS PUNTOS DE ESTA LISTA PUEDEN SER NO APLICABLES A SU VEHÍCULO
                                <br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="font-size:16px;" align="center" colspan="6">
                    <br>
                    TODAS LAS REPARACIONES NECESARIAS DETERMINADAS DURANTE LAS INSPECCIONES DE PREVIA ENTREGA,
                    DEBEN SER REALIZADAS ANTES DE LA ENTREGA DEL VEHÍCULO.
                    <br>
                </td>
            </tr>
            <tr>
                <td style="font-size:16px;border-top:2px solid;" align="center" colspan="6">
                    <br>
                    Copyright <?php echo date("Y"); ?>, Ford Motor Company
                    <br>
                </td>
            </tr>
        </table>

        <br>
        <div class="row">
            <div class="col-sm-12" align="center">
                <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="2">
                <input type="hidden" name="completoFormulario" id="completoFormulario" value="<?php if(isset($completoFormulario)) echo $completoFormulario; else echo "0"; ?>">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="tipoRegistro" id="tipoRegistro" value="<?php if(isset($tipoRegistro)) echo $tipoRegistro; ?>">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button> -->
                <button type="submit" class="btn btn-success" name="Aceptar" id="guardarInspeccion" style="display:none;" >Actualizar</button>
            </div>
        </div>

    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Formulario de superusuario -->
<div class="modal fade " id="superUsuario" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Ingreso.</h5>
            </div>
            <div class="modal-body" style="font-size:12px;">
                <label for="" style="font-size: 14px;font-weight: initial;">Usuario:</label>
                <br>
                <input type="text" class="input_field" type="text" id="superUsuarioName" value="" style="width:100%;">
                <br><br>
                <label for="" style="font-size: 14px;font-weight: initial;">Password:</label>
                <br>
                <input type="password" class="input_field" type="text" id="superUsuarioPass" value="" style="width:100%;">
                <br><br><br>
                <h4 class="error" style="font-size: 16px;font-weight: initial;" align="center" id="superUsuarioError">:</h4>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="superUsuarioEnvio">Validar</button>
            </div>
        </div>
    </div>
</div>
