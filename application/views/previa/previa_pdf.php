<?php 
    // Eliminamos cache
    //la pagina expira en una fecha pasada
    header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT"); 
    //ultima actualizacion ahora cuando la cargamos
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
    //no guardar en CACHE
    header ("Cache-Control: no-cache, must-revalidate"); 
    header ("Pragma: no-cache");

    ob_start();

?>

<style media="screen">
    label,td{
        font-size: 8px;
    }
    td{
        padding-top: 0px;
        padding-bottom: 0px;
    }

    .divCheck{
        /* position: relative; */
        width:6px;
        height:6px;
        border: 0.5px solid;
        background-color: #ffffff;
    }

    .divCheckB{
        /* position: relative; */
        width:10px;
        height:10px;
        border: 0.5px solid;
        background-color: #ffffff;
    }

    .ladoB {
        font-size: 10px;
    }
</style>

<page backtop="1mm" backbottom="1mm" backleft="3mm" backright="3mm">
    <table style="min-width:800px;border: 2px solid;">
        <tr>
            <td style="width: 30px;border: 1px solid;writing-mode: vertical-lr;">
                <?php if(isset($folio)) echo $folio; else echo "N01990598"; ?>
            </td>
            <td style="width:170px;border-bottom: 2px solid;vertical-align: middle;" align="center">
                <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:150px;height:40px;">
            </td>
            <td style="width: 160px;border-bottom: 2px solid;border-left: 1px solid;vertical-align: middle;" align="center">
                <img src="<?php echo base_url(); ?>assets/imgs/logos/lincoln.png" alt="" class="logo" style="width:140px;height:40px;">
            </td>
            <td style="width: 160px;border-bottom: 2px solid;border-left: 1px solid;vertical-align: middle;" align="center">
                <img src="<?php echo base_url(); ?>assets/imgs/logos/mercury.png" alt="" class="logo" style="width:140px;height:40px;">
            </td>
            <td style="width: 260px;border-bottom: 2px solid;border-left: 1px solid;vertical-align: middle;" align="center">
                <h4>LISTA DE REQUERIMIENTOS <br>PARA PREVIA ENTREGA</h4>
            </td>
            <td style="width: 240px;">
                <table style="width:100%;border: 2px solid;">
                    <tr>
                        <td style="width:100%;border-bottom: 2px solid;font-size:13px;" colspan="2">
                            <strong>VIN:</strong>
                            <?php if(isset($vin)) echo $vin; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;font-size:13px;">
                            <strong>UNIDAD:</strong>
                            <?php if(isset($unidad)) echo $unidad; ?>
                        </td>
                        <td style="width:50%;font-size:13px;">
                            <strong>CAT:</strong>
                            <?php if(isset($cat)) echo $cat; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;font-size:13px;" colspan="2">
                            <strong>COLOR:</strong>
                            <?php if(isset($color)) echo $color; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <!-- Lado izquierdo de la hoja -->
            <td style="" align="right" colspan="4">
                <table style="width:510px;margin-left: 5px;margin-right:5px;">
                    <tr>
                        <td style="width:510px;" align="right" colspan="3">
                            <label for=""><u>
                                <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REVISIONES</strong></u>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($revisiones)): ?>
                                    <?php if ($revisiones[0] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;A1&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Inspeccionar carga de batería a 12.4V antes de encender el motor. Recargar si el indicador está en rojo.
                            Baterías sin indicador, recargar a 12.4 Volts o más* Voltaje = <u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($voltaje)) echo $voltaje ?>&nbsp;&nbsp;</u>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($revisiones)): ?>
                                    <?php if ($revisiones[1] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;A3&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Inspeccionar visualmente todos los componentes debajo del cofre. Verifique fugas.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($revisiones)): ?>
                                    <?php if ($revisiones[2] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;A4&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Inspeccionar visualmente llantas y componentes por debajo del vehículo. Verifique fugas.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($revisiones)): ?>
                                    <?php if ($revisiones[3] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;A8&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Inspeccionar visualmente la localización y el buen estado del kit para inflar llantas (si está equipado).
                        </td>
                    </tr>

                    <tr>
                        <td style="width:510px;" align="right" colspan="3">
                            <label for=""><u>
                                <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VERIFIQUE Y PONER A ESPECIFICACIÓN</strong></u>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($especificacion)): ?>
                                    <?php if ($especificacion[0] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;B1&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar las presiones de llantas (incluyendo la de refacción) (ajuste a temperatura ambiente). *
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($especificacion)): ?>
                                    <?php if ($especificacion[1] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;B3&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar todos los niveles de fluidos de motor, transmisión, frenos, sistema enfriamiento, embrague, acumulador, dirección, diferencial y recipiente del lavaparabrisas delantero y trasero.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($especificacion)): ?>
                                    <?php if ($especificacion[2] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;B4&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                             Verificar par de apriete terminales de acumulador.
                        </td>
                    </tr>

                    <tr>
                        <td style="width:510px;" align="right" colspan="3">
                            <label for=""><u>
                                <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REVISAR OPERACIÓN</strong></u>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[0] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F2&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                             Cinturones de seguridad (incluyendo la hebilla del asiento central, si está equipado).
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[1] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F3&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                             Seguros y mecanismos de posición de los respaldos de los asientos.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[2] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F6&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Luces: Faros; Traseras; Direccionales; Intermitentes; de Posición; de Cortesía; de Reversa:
                            Faros para niebla y de Alta intensidad (si está equipado); además de los Indicadores de advertencia de los instrumentos,
                            equipo de sonido USB y entrada auxiliar. *
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[3] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F7&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Indicador de las bolsas de aire y del tablero para su operación apropiada
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[4] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F12&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Freno de estacionamiento (incluyendo indicador de advertencia).*
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[5] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F15&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Claxon y claxon óptico
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[6] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F16&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Limpiaparabrisas y rociadores, incluyendo los sensores de lluvia, si está incluido.*
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[7] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F20&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            El sistema de calefacción, aire acondicionado, ventilación, desempañador, así como el ventilador en el motor (sist enfriamiento).
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[8] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F21&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Sistema de advertencia del uso de cinturones de seguridad.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($operacion)): ?>
                                    <?php if ($operacion[9] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;F24&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Seguridad, dispositivos de entrada.
                            (cierre de seguros, llaves del sistema de antirrobo pasivo, todos los transmisores remotos).
                            Colocar tarjeta de codificación (si está equipado) en la información del usuario Colocar tarjeta de acceso sin llave.
                        </td>
                    </tr>

                    <tr>
                        <td style="width:510px;" align="right" colspan="3">
                            <label for=""><u>
                                <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AJUSTE Y VERIFICACIONES</strong></u>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[0] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J1&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Radio / reloj / Alarma volumétrica y perimetral / tiempo. *
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[1] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J2&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Ajustar la brújula de acuerdo a la variación de la zona*.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[2] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J4&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Apagar el monitor de la presión de llantas, si está activado*.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[3] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J5&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar suspensión de aire (si está equipado). *
                            Para Expedition / Navigator – Activar a través del Centro de Mensajes.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[4] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J6&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Sistema de entretenimiento de los asientos traseros – instalar las baterías del control remoto y audífonos.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[5] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J7&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Ajustar el idioma y el recordatorio de cambio del aceite en el Centro Electrónico de Mensajes SYNC, FIS, My Ford Touch.
                            Verificar funcionamiento USB y entrada auxiliar.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[6] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J8&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Activar los estribos desplegables automáticos en el centro de mensajes.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[7] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J9&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar el funcionamiento del mecanismo abatible de los espejos y vidrios eléctricos, reprograma One Touch si es necesario.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($ajuste)): ?>
                                    <?php if ($ajuste[8] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;J11&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Retirar los plásticos de protección lateral.
                        </td>
                    </tr>

                    <tr>
                        <td style="width:510px;" align="right" colspan="3">
                            <label for=""><u>
                                <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRUEBA DE CAMINO</strong></u>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[0] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H1&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar manejabilidad durante la prueba de camino.
                            Si hay un problema o el indicador check Engine/ servicio de mantenimiento" está activado,
                            realizar las rutinas de diagnóstico y las reparaciones por garantía pertinentes.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[1] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H2&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar que el movimiento de la mariposa del cuerpo de aceleración desde la posición de aceleración hasta la de marcha lenta, sea suave.
                            (verificar con el motor encendido y apagado).
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[2] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H3&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Revisar rechinidos, golpeteos, vibraciones y sonidos de viento debidos a el sistema de flujo de aire.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[3] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H5&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar funcionamiento del sistema de frenos (incluyendo el freno de mano).*
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[4] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H6&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar el control del sistema de dirección para asegurar una alineación y centrado del volante adecuados.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[5] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H8&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Verificar el comportamiento del embrague y la transmisión.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[6] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H10&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Control de velocidad.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[7] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H12&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Caja de transferencia.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15px;" align="left">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($prueba)): ?>
                                    <?php if ($prueba[8] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 15px;" align="left">
                            <label for="" style="float:right;">&nbsp;H14&nbsp;</label>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Asistencia en maniobras de estacionamiento de reversa, sensores y cámara de reversa.
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 30px;" align="left" colspan="2">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($cod_Confg)): ?>
                                    <?php if ($cod_Confg[0] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Código de Llaves.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px;" align="left" colspan="2">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($cod_Confg)): ?>
                                    <?php if ($cod_Confg[1] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Código Acceso sin llave botonera.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px;" align="left" colspan="2">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($cod_Confg)): ?>
                                    <?php if ($cod_Confg[2] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Clave DOT.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px;" align="left" colspan="2">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($cod_Confg)): ?>
                                    <?php if ($cod_Confg[3] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Código Estéreo.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px;" align="left" colspan="2">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($cod_Confg)): ?>
                                    <?php if ($cod_Confg[4] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Configuración especial FordInteractive Sytem.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px;" align="left" colspan="2">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($cod_Confg)): ?>
                                    <?php if ($cod_Confg[5] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Configuración en español Sync.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px;" align="left" colspan="2">
                            <div class="divCheck" style="float:left;">
                                <?php if (isset($cod_Confg)): ?>
                                    <?php if ($cod_Confg[6] == "1"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:7px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="width: 490px;padding-left: 5px;vertical-align: middle;" align="left">
                            Configuración en español computadora de viaje.
                        </td>
                    </tr>

                </table>
            </td>

            <!-- Lado derecho de la hoja -->
            <td style="" align="right" colspan="2">
                <table style="width:490px;margin-left: 5px;margin-right:5px;">
                    <tr>
                        <td style="width:260px;">
                            <br><br>
                        </td>
                        <td style="width:230px;border: 2px solid;font-size:13px;" align="left">
                            <strong>No. de Orden:</strong>
                            <?php if(isset($idOrden)) echo $idOrden; ?>
                            <!--<strong>Folio Intelisis:</strong>
                            <?php if(isset($idIntelisis)) echo $idIntelisis; ?>-->
                        </td>
                    </tr>
                    <tr>
                        <td style="width:260px;">
                            <br><br>
                        </td>
                        <td style="width:230px;border: 2px solid;font-size:13px;" align="left">
                            <strong>Folio:</strong>
                            <?php if(isset($folio)) echo $folio; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="" align="right" colspan="2">
                            <label for=""><u>
                                <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ESTÁNDARES DE APARIENCIA</strong></u>
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="width:490px;" align="left" colspan="2">
                            <table style="width:490px;">
                                <tr>
                                    <td style="width: 10px;" align="left">
                                        <div class="divCheckB" style="float:left;">
                                            <?php if (isset($apariencia)): ?>
                                                <?php if ($apariencia[0] == "1"): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                    <td style="width: 20px; font-size:8px;" align="left">
                                        &nbsp;AP1&nbsp;
                                    </td>
                                    <td style="width: 460px;vertical-align: middle; font-size:8px;padding-left:3px;" align="justify">
                                        Llevar a cabo una limpieza detallada del vehículo, tanto en exteriores como en interiores, además de eliminar la suciedad de los elementos del limpiaparabrisas.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="width:490px;" align="left" colspan="2">
                            <table style="width:490px;">
                                <tr>
                                    <td style="width: 10px;" align="left">
                                        <div class="divCheckB" style="float:left;">
                                            <?php if (isset($apariencia)): ?>
                                                <?php if ($apariencia[1] == "1"): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                    <td style="width: 20px; font-size:8px;" align="left">
                                        &nbsp;AP2&nbsp;
                                    </td>
                                    <td style="width: 460px;vertical-align: middle; font-size:8px;padding-left:3px;" align="justify">
                                        Inspeccionar daños de piezas metálicas o pintura, tanto en exteriores como en interiores y retocar lo que haga falta.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="width:490px;font-size:10px;font-weight:bold;" align="center" colspan="2">
                            <br>
                            - NO ELIMINAR LA CUBIERTA PROTECTORA DEL VOLANTE Y ASIENTOS HASTA QUE SEA <br> ENTREGADO AL CLIENTE.
                            <br>
                            - NO ELIMINAR LA CUBIERTA PROTECTORA DE LA PANTALLA DEL AIRE ACONDICIONADO, NI <br> LA DEL RADIO. EL CLIENTE LAS RETIRARÁ POSTERIORMENTE.
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="width:490px;font-size:10px;border-top:2px solid;font-weight:bold;" align="center" colspan="2">
                            <br>
                            TARJETA DEL CLIENTE E INFORMACIÓN  DEL DISTRIBUIDOR
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="font-size:10px;width:490px;border-bottom:0.5px solid;" align="center" colspan="2">
                            <?php if(isset($distribuidor))echo $distribuidor; else echo "<br>"; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="font-size:11px;width:490px;" align="center" colspan="2">
                            Distribuidor
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="font-size:10px;width:490px;border-bottom:0.5px solid;" align="center" colspan="2">
                            <?php if(isset($direccion))echo $direccion; else echo "<br>"; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="font-size:11px;width:490px;" align="center" colspan="2">
                            Dirección
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="font-size:10px;width:490px;border-bottom:0.5px solid;" align="center" colspan="2">
                            <?php if(isset($municipio))echo $municipio; else echo "<br>"; ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="font-size:11px;width:490px;" align="center" colspan="2">
                            Municipio/Delegación
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:10px;border-bottom:0.5px solid;" align="center">
                            <?php if(isset($estados))echo $estados; else echo "<br>"; ?>
                        </td>
                        <td style="font-size:10px;border-bottom:0.5px solid;" align="center">
                            <?php if(isset($cp))echo $cp; else echo "<br>"; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:11px;" align="center">
                            Estado
                        </td>
                        <td style="font-size:11px;" align="center">
                            Código Postal
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="width:490px;font-size:10px;" align="left" colspan="2">
                            <table style="width:490px;">
                                <tr>
                                    <td style="width:98px;font-size:10px;border-bottom:0.5px solid;">
                                        <?php if(isset($numero))echo $numero; else echo "<br>";?>
                                    </td>
                                    <td style="width:98px;font-size:10px;border-bottom:0.5px solid;">
                                        <?php if(isset($codigo))echo $codigo; else echo "<br>";?>
                                    </td>
                                    <td style="width:98px;font-size:10px;border-bottom:0.5px solid;">
                                        <?php if(isset($apertura))echo $apertura; else echo "<br>";?>
                                    </td>
                                    <td style="width:98px;font-size:10px;border-bottom:0.5px solid;">
                                        <?php if(isset($llantas))echo $llantas; else echo "<br>";?>
                                    </td>
                                    <td style="width:98px;font-size:10px;border-bottom:0.5px solid;">
                                        <?php if(isset($reserva))echo $reserva; else echo "<br>";?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:98px;font-size:11px;" align="center">
                                        Número de llave  /
                                    </td>
                                    <td style="width:98px;font-size:11px;" align="center">
                                        Código de Radio  /
                                    </td>
                                    <td style="width:98px;font-size:11px;" align="center">
                                        Apertura de puerta  /
                                    </td>
                                    <td style="width:98px;font-size:11px;" align="center">
                                        DOT Llantas  /
                                    </td>
                                    <td style="width:98px;font-size:11px;" align="center">
                                        Número de reserva
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="ladoB" style="width:490px;" align="left" colspan="2">
                            <br>
                            <table style="width:490px;">
                                <tr>
                                    <td style="width: 10px;" align="left">
                                        <div class="divCheckB" style="float:left;">
                                            <?php if (isset($oasis)): ?>
                                                <?php if ($oasis[0] == "1"): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                    <td style="width: 480px; font-size:10px;padding-left:4px;" align="justify">
                                        El OASIS ha sido verificado para identificar las acciones de servicio en campo (ASC) y boletines técnicos de servicio (BTS).
                                        Confirmo que las ASC y los BTS han sido verificadas.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="width:490px;" align="left" colspan="2">
                            <table style="width:490px;">
                                <tr>
                                    <td style="width: 10px;" align="left">
                                        <div class="divCheckB" style="float:left;">
                                            <?php if (isset($oasis)): ?>
                                                <?php if ($oasis[1] == "1"): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                    <td style="width: 480px; font-size:10px;padding-left:4px;" align="justify">
                                        El OASIS ha sido verificado y revisado usando el código de sintoma de previas (804000) para identificar cualquier
                                        intrucción adicional requerida.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="ladoB" style="width:490px;" align="left" colspan="2">
                            <table style="width:490px;">
                                <tr>
                                    <td style="width: 10px;" align="left">
                                        <div class="divCheckB" style="float:left;">
                                            <?php if (isset($oasis)): ?>
                                                <?php if ($oasis[2] == "1"): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                    <td style="width: 480px; font-size:10px;padding-left:4px;" align="justify">
                                        Certifico que todas las verificaciones establecidas en la presete guia, han sido realizadas en este vehículo de acuerdo con
                                        los procedimientos y manuales correspondientes. Todas las operaciones necesarias han sido realizadas por un técnico de servicio.
                                        En el entendido de que este es un acuerdo entre Ventas y Servicio, debera ser guardado por el distribuidor en el historial
                                        del vehículo.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:490px;" class="ladoB" align="center" colspan="2">
                            NOMBRE Y FIRMA DEL TÉCNICO
                            <table style="width:490px;">
                                <tr>
                                    <td style="width: 390px;font-size:10px;" align="center">
                                        <?php if (isset($firmaTecnico)): ?>
                                            <?php if ($firmaTecnico != ""): ?>
                                                <img src="<?php echo base_url().$firmaTecnico; ?>" alt="" style="height:30px;width:80px;">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" style="height:30px;width:80px;">
                                        <?php endif; ?>
                                        <br>
                                        <?php if(isset($tecnico)) echo $tecnico;?><br>
                                    </td>
                                    <td style="padding-left:10px;font-size:10px;width: 100px;font-weight: initial;" align="left">
                                        Fecha:&nbsp; <u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($fecha)) echo $fecha; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="font-size:11px;border-top:2px solid;padding-left:15px;width:490px;font-weight:bold;" class="ladoB" align="left" colspan="2">
                            <br>
                            NO DESCONECTAR VELOCÍMETRO BAJO NINGUNA CIRCUNSTANCIA
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:11px;padding-left:15px;width:490px;font-weight:bold;" class="ladoB" align="left" colspan="2">
                            <br>
                            VERIFIQUE / INSPECCIONE EL VEHÍCULO DE LA SIGUIENTE MANERA: REALICE LOS AJUSTES ESPECÍFICOS QUE SE REQUIEREN.
                            REFIÉRASE AL MANUAL DE TALLER O LA GUÍA DEL PROPIETARIO.
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size:11px;padding-left:15px;width:490px;font-weight:bold;" class="ladoB" align="left" colspan="2">
                            <br>
                            NOTA: ALGUNOS PUNTOS DE ESTA LISTA PUEDEN SER NO APLICABLES A SU VEHÍCULO
                            <br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-size:14px;font-weight:bold;" align="center" colspan="6">
                <br>
                TODAS LAS REPARACIONES NECESARIAS DETERMINADAS DURANTE LAS INSPECCIONES DE PREVIA ENTREGA,
                DEBEN SER REALIZADAS ANTES DE LA ENTREGA DEL VEHÍCULO.
                <br>
            </td>
        </tr>
        <tr>
            <td style="font-size:12px;font-weight:bold;border-top:2px solid;" align="center" colspan="6">
                <br>
                Copyright <?php echo date("Y"); ?>, Ford Motor Company
                <br>
            </td>
        </tr>
    </table>
</page>

<?php 
    use Spipu\Html2Pdf\Html2Pdf;

    //Orientación  de la hoja P->Vertical   L->Horizontal
    $html2pdf = new Html2Pdf('L', 'A4', 'en',TRUE,'UTF-8',NULL);
    
    try {

        $html = ob_get_clean();

        ob_clean();
        /* Limpiamos la salida del búfer y lo desactivamos */
        //ob_end_clean();

        $html2pdf->setDefaultFont('Helvetica');     //Helvetica
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($html);
        $html2pdf->setTestTdInOnePage(FALSE);
        $html2pdf->Output();

    } catch (Html2PdfException $e) {
        $html2pdf->clean();
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }

?>