<div class="container">
    <br><br>
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Formatos de Previa</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;">
                                    <td align="center" style="width: 5%;">#</td>
                                    <td align="center" style="width: 10%;"><strong>NO. ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>FECHA REGISTRO</strong></td>
                                    <td align="center" style="width: 15%;"><strong>DISTRIBUIDOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>VIN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>CAT</strong></td>
                                    <td align="center" style="width: 10%;"><strong>UNIDAD</strong></td>
                                    <td align="center" style="width: 10%;"><strong>COLOR</strong></td>
                                    <td align="center" style="width: 15%;"><strong>TÉCNICO</strong></td>
                                    <td align="center" style="width: 15%;" colspan="2"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="">
                                                <?php echo count($idOrden)-$index; ?>
                                            </td>
                                            <td align="center" style="">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="">
                                                <?php echo $fechaCaptura[$index]; ?>
                                            </td>
                                            <td align="center" style="">
                                                <?php echo $distribuidor[$index]; ?>
                                            </td>
                                            <td align="center" style="">
                                                <?php echo $vin[$index]; ?>
                                            </td>
                                            <td align="center" style="">
                                                <?php echo $cat[$index]; ?>
                                            </td>
                                            <td align="center" style="">
                                                <?php echo $unidad[$index]; ?>
                                            </td>
                                            <td align="center" style="">
                                                <?php echo $color[$index]; ?>
                                            </td>
                                            <td align="center" style="">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="">
                                                <button type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Formulario_Previo/".$id_cita_url[$index];?>';">Ver</button>
                                            </td>
                                            <td align="center" style="">
                                                <a href="<?=base_url()."Formulario_Previo_PDF/".$id_cita_url[$index];?>" class="btn btn-warning" target="_blank">PDF</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="9" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <!-- <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar"> -->

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="rolVista" value="lista">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
