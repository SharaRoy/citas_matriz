<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-4" align="left" style="padding-top: 20px;">
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                        Regresar
                    </button>
                </div>
                <div class="col-sm-8">
                    <h3 align="right">Documentación de Ordenes</h3>
                </div>
            </div>
            
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_fechas" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <!-- formulario de busqueda -->
                    </div>

                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:12px;background-color: #eee;">
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>NO. <br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>APERTURA<br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>PLACAS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>TÉCNICO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>CLIENTE</strong></td>
                                    <td align="center" colspan="6" style=""><strong>ACCIONES</strong></td>
                                </tr>
                                <tr style="background-color: #ddd;">
                                    <td align="center" style="width: 5%;font-size:9px;">ORDEN</td>
                                    <td align="center" style="width: 5%;font-size:9px;">ARC.OASIS</td>
                                    <td align="center" style="width: 5%;font-size:9px;">MULTIPUNTO</td>
                                    <td align="center" style="width: 5%;font-size:9px;">COTIZACIÓN</td>
                                    <td align="center" style="width: 5%;font-size:9px;">VOZ CLIENTE</td>
                                    <td align="center" style="width: 5%;font-size:9px;">SERVICIO VALET</td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $fechaDiag[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombreAsesor[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombreConsumidor2[$index]; ?>
                                            </td>
                                            <!-- Orden de servicio (Vista rapida) -->
                                            <td align="center" style="vertical-align: middle;">
                                                <a href="<?=base_url().'OrdenServicio_Revision/'.$id_cita_url[$index];?>" class="btn btn-info" target="_blank" style="font-size: 10px;">ORDEN</a>
                                            </td>

                                            <!-- Archivos Oasis -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($archivoOasis[$index] != ""): ?>
                                                    <?php $indice = 1; ?>
                                                    <?php for ($i = 0; $i < count($archivoOasis[$index]); $i++): ?>
                                                        <?php if ($archivoOasis[$index][$i] != ""): ?>
                                                            <a href="<?=base_url().$archivoOasis[$index][$i];?>" class="btn btn-primary" target="_blank" style="font-size: 10px;margin-top:5px;">
                                                                ARC. OASIS (<?php echo $indice++; ?>)
                                                            </a>
                                                            <br>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>
                                                <?php else: ?>
                                                    <button type="button" class="btn btn-default" name="" style="font-size: 10px;">SIN <br> ARCHIVO</button>
                                                <?php endif; ?> 
                                            </td>
                                            
                                            <!-- Hoja Multipunto (Vista rapida), con indicador de lavado y firma jefe de tallar -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($multipunto[$index] != ""): ?>
                                                    <a href="<?=base_url().'Multipunto_Revision/'.$id_cita_url[$index];?>" class="btn <?php if($firmasMultipunto[$index] != "") echo 'btn-success'; else echo 'btn-danger'; ?>" target="_blank" style="font-size: 10px;">MULTIPUNTOS</a>
                                                <?php endif; ?>
                                                
                                                <label style="color:blue;font-weight: bold;">
                                                    <?php if(isset($lavado[$index])) echo $lavado[$index]; ?>
                                                </label>
                                            </td>
                                            
                                            <!-- Presupuestos multipunto (solo nuevos formatos) -->
                                            <td align="center" style="vertical-align: middle;">
                                                <!-- presupuestos antes del 4 de dic del 2019-->
                                                <?php if ($presupuesto[$index] != ""): ?>
                                                    <?php if ($presupuestoFinaliza[$index] != ""): ?>
                                                        <a href="<?=base_url()."Presupuesto_Cliente/".$id_cita_url[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">COTIZACIÓN</a>
                                                    <?php else: ?>
                                                        <a href="<?=base_url()."Alta_Presupuesto/".$id_cita_url[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">COTIZACIÓN</a>
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            </td> 
                                            
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($vozCliente[$index] != ""): ?>
                                                    <a href="<?=base_url().'VCliente_Revision/'.$id_cita_url[$index];?>" class="btn btn-dark" target="_blank" style="font-size: 10px;color:white;height:100%;">VOZ CLIENTE</a>
                                                    
                                                    <br>
                                                    <?php if ($vozAudio[$index] != ""): ?>
                                                        <a class="AudioModal" data-value="<?php echo $vozAudio[$index]; ?>" data-orden="<?php echo $idOrden[$index]; ?>" data-target="#muestraAudio" data-toggle="modal" style="color:blue;font-size:24px;">
                                                            <i class="far fa-play-circle" style="font-size:24px;"></i>
                                                        </a>.
                                                    <?php else: ?>
                                                        <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>

                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($valet[$index] != ""): ?>
                                                    <a href="<?=base_url().'Servicio_valet_PDF/'.$id_cita_url[$index];?>" class="btn btn-default" target="_blank" style="font-size: 10px;background-color:  #a569bd; color:white;">VALET</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="14" style="width:100%;" align="center">
                                            <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                                <h4>Sin registros que mostrar</h4>
                                            <?php else: ?>
                                                <h4>Sesión Expirada</h4>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="pOrigen" id="pOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" autoplay style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para agregar un comentario -->
<div class="modal fade" id="addComentario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Ingresar comentario</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="row">
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Titulo</h4>
                        <input type="text" class="form-control" name="" id="titulo" value="">
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Fecha</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </div>
                            <input type="date" class="form-control" id="fechaComentario" max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d"); ?>" style="padding-top: 0px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h4>Hora</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-clock"></i>
                            </div>
                            <input type="text" class="form-control" id="HoraComentario" value="<?php echo date("H:m"); ?>">
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Comentario</h4>
                        <textarea rows='3' class='form-control' name="" id='comentario_notas' style='width:100%;text-align:left;font-size:12px;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="idMagigAdd" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnEnviarComentario">Enviar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver el historial de comentarios -->
<div class="modal fade" id="historialCometario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Historial de comentarios</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="" style="width:100%;">
                            <thead>
                                <tr style="background-color: #ddd;">
                                    <td style="width:25%;"><h5 align="center">Fecha</h5></td>
                                    <td style="width:25%;"><h5 align="center">Titulo</h5></td>
                                    <td style="width:50%;"><h5 align="center">Comentario</h5></td>
                                </tr>
                            </thead>
                            <tbody id="tablaComnetario">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="idMagigHistorial" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>