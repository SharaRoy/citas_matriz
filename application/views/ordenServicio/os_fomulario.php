<div class="" style="margin:30px;">
    <form id="formulario" method="post" action="" autocomplete="on" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            DIAGNÓSTICO Y POSIBLES CONSECUENCIAS
                            <a id="superUsuarioA" class="h2_title_blue" data-target="#superUsuario" data-toggle="modal" style="color:white;margin-top:15px;cursor: pointer;">
                                <i class="fas fa-user"></i>
                            </a>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    <b>No. Orden:</b>
                                    <input type="text" class="input_field" name="orderService" id="orderService" onblur="buscadorOrden()" value="<?php if(isset($id_cita)) echo $id_cita; ?>">
                                    <input type="hidden"  name="noServicio" value="<?php if(isset($id_cita)) echo $id_cita; ?>">
                                    <div class="error" id="error_orderService"></div>
                                    <input type="hidden" name="inicio_formulario" value="<?= date('Y-m-d H:i:s') ?>">
                                </p>
                            </div>
                            
                            <div class="col-md-6" align="right">
                                <?php if ($this->session->userdata('rolIniciado')): ?>
                                    <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($this->session->userdata('rolIniciado') == "ASETEC"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_AsesorBody";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($this->session->userdata('rolIniciado') == "PCO"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Principal_CierreOrden/5";?>';">
                                            Cerrar Sesión
                                        </button>
                                    <?php else: ?>
                                        <!--<button type="button" class="btn btn-dark">
                                            Sesión Expirada
                                        </button>-->
                                    <?php endif; ?>
                                <?php else: ?>
                                    <!--<button type="button" class="btn btn-dark">
                                        Sesión Expirada
                                    </button>-->
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="row" hidden>
                            <div class="col-md-3"></div>
                            <div class="col-md-7">
                                <?php if (isset($tipo_form)): ?>
                                    <?php if ($tipo_form == "1"): ?>
                                        <table style="width: 100%;border: 1.5px solid orange;">
                                            <tr>
                                                <td rowspan="<?= count($motivos_ausencia)+1 ?>" style="width: 55%;vertical-align: middle;padding-left: 10px;">
                                                    <input type="checkbox" style="transform: scale(1.3);" name="autorizacion_ausencia" value="1">  <!-- < ?php if(isset($autorizacion_ausencia)) if($autorizacion_ausencia == "1") echo "checked";?>  -->
                                                    &nbsp;&nbsp;
                                                    Autorización por Ausencia
                                                    
                                                </td>
                                                <td style="width: 45%;vertical-align: middle;">
                                                    <span class="error" id="error_autorizacion_ausencia"></span>
                                                </td>
                                            </tr>
                                            <?php foreach ($motivos_ausencia as $x => $row): ?>
                                                <tr>
                                                    <td style="width: 45%;vertical-align: middle;">
                                                        <input type="radio" style="transform: scale(1.3);" class="mt" value="<?= $row->id ?>" name="motivo" <?php if(isset($motivo)) if($row->id == $motivo) echo "checked";?>>
                                                        &nbsp;&nbsp;
                                                        <?= $row->nombre ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach ?>
                                        </table>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                        </div>
                        <br>
                        <p align="justify" style="text-align: justify;">
                            Para elaborar el diagnóstico de <input type="text" name="vehiculo" class="input_field" style="width:5cm;" value="<?php if(isset($tipo_servicio)) echo $tipo_servicio; ?>"> del vehículo,
                            autorizo en forma expresa a desarmar las partes indispensables del mismo y sus componentes, a efecto de obtener un diagnóstico adecuado de él,
                            en el entendido de que el vehículo se me devolverá en las mismas condiciones en que fuera entregado, excepto en caso de que como consecuencia
                            inevitable resulte imposible o ineficaz para su funcionamiento así entregarlo, por causa no imputable al proveedor, lo cual
                            si(&nbsp;<input type="radio" style="transform: scale(1.3);" value="1" name="aceptoTermino" <?php if(isset($terminos)) if($terminos == "1") echo "checked";?>>&nbsp;)
                            &nbsp;&nbsp;No&nbsp;(&nbsp;<input type="radio" style="transform: scale(1.3);" value="0" name="aceptoTermino" <?php if(isset($terminos)) {if($terminos == "0") echo "checked";} else echo "checked"; ?>>&nbsp;)&nbsp;&nbsp;
                            acepto.
                            En todo caso me obligo a pagar el importe del diagnóstico y los trabajos necesarios para realizarlo en caso de no autorizar la reparación en términos del contrato y acepto que el diagnóstico se realice en un plazo de 
                            <input type="text" name="dias" class="input_field" style="width:5cm;" value="<?php if(isset($dias)) echo $dias; else echo "Por definir"; ?>">
                            días hábiles a partir de la firma del presente.
                        </p>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-3" align="center">
                                Nombre y firma de quien elabora el diagnóstico.<br>
                                <?php if (isset($firmaDiagnostico)): ?>
                                    <?php if ($firmaDiagnostico != ""): ?>
                                        <img class="marcoImg" src="<?= base_url() ?><?php if(isset($firmaDiagnostico)) {if($firmaDiagnostico != "") echo $firmaDiagnostico; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaImg" alt="">
                                        <br><br>
                                        <input type="hidden" id="rutaFirma" name="rutaFirmaDiagnostico" value="<?php if(isset($firmaDiagnostico)) echo $firmaDiagnostico; ?>">
                                        <input type="text" id="nombreDiagnostico" onblur='llenadoAsesor()' class="input_field asesorname" name="nombreDiagnostico" style="width:100%;" value="<?php if(isset($nombreDiagnostico)) echo $nombreDiagnostico; ?>">
                                    <?php else: ?>
                                        <input type="hidden" id="rutaFirma" name="rutaFirmaDiagnostico" value="">
                                        <img class="marcoImg" src="" id="firmaImg" alt="">
                                        <br><br>
                                        <input type="text" id="nombreDiagnostico" onblur='llenadoAsesor()' class="input_field asesorname" name="nombreDiagnostico" style="width:100%;" value="<?php if(isset($nombreDiagnostico)) echo $nombreDiagnostico; ?>">

                                        <br><br>
                                        <a class="cuadroFirma btn btn-primary" data-value="Diagnostico" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                    <?php endif ?>
                                <?php else: ?>
                                    <input type="hidden" id="rutaFirma" name="rutaFirmaDiagnostico" value="<?= set_value('rutaFirmaDiagnostico');?>">
                                    <img class="marcoImg" src="<?= set_value('rutaFirmaDiagnostico');?>" id="firmaImg" alt="">
                                    <br><br>
                                    <input type="text" id="nombreDiagnostico" onblur='llenadoAsesor()' class="input_field asesorname" name="nombreDiagnostico" style="width:100%;" value="<?php if(isset($nombreDiagnostico)) echo $nombreDiagnostico; ?>">

                                    <br><br>
                                    <a class="cuadroFirma btn btn-primary" data-value="Diagnostico" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif ?>
                                <div class="error" id="error_firma_1"></div>
                            </div>
                            <div class="col-md-3" align="center">
                                Fecha de elaboración del diagnóstico. <br>
                                <?php if (isset($fecha_diagnostico)): ?>
                                    <input type="date" class="input_field" style="width:3cm;" value="<?php if(isset($fecha_diagnostico)) echo $fecha_diagnostico; else echo "2019-01-01"; ?>" disabled>
                                <?php else: ?>
                                    <input class="input_field" type="date" id="start" name="trip_start" value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>">
                                <?php endif ?>
                            </div>
                            <div class="col-md-3" align="center">
                                Nombre y firma del consumidor aceptando diagnóstico.<br>
                                <?php if (isset($firmaConsumidor1)): ?>
                                    <?php if ($firmaConsumidor1 != ""): ?>
                                        <img class="marcoImg" src="<?= base_url() ?><?php if(isset($firmaConsumidor1)) {if($firmaConsumidor1 != "") echo $firmaConsumidor1; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaImgAcepta" alt="">
                                        <br><br>
                                        <label for=""><?php if(isset($nombreConsumido1)) echo $nombreConsumido1; ?></label>
                                        <input type="hidden" onblur='llenadoCliente()' id="consumidor1Nombre" class="input_field nombreCliente" name="consumidor1Nombre" style="width:100%;" value="<?php if(isset($nombreConsumido1)) echo $nombreConsumido1; ?>">
                                        <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?php if(isset($firmaConsumidor1)) echo $firmaConsumidor1; ?>">

                                    <?php else: ?>
                                        <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?= set_value('rutaFirmaAcepta');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">
                                        <br><br>
                                        <input type="text" onblur='llenadoCliente()' id="consumidor1Nombre" class="input_field nombreCliente" name="consumidor1Nombre" style="width:100%;" value="<?php if(isset($nombreConsumido1)) echo $nombreConsumido1; ?>">

                                        <br><br>
                                        <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                    <?php endif ?>
                                <?php else: ?>
                                    <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?= set_value('rutaFirmaAcepta');?>">
                                    <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">
                                    <br><br>
                                    <input type="text" onblur='llenadoCliente()' id="consumidor1Nombre" class="input_field nombreCliente" name="consumidor1Nombre" style="width:100%;" value="<?php if(isset($nombreConsumido1)) echo $nombreConsumido1; ?>">

                                    <br><br>
                                    <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif ?>
                                <div class="error" id="error_firma_2"></div>
                            </div>
                            <div class="col-md-3" align="center">
                                Fecha aceptación. <br>
                                <?php if (isset($fecha_aceptacion)): ?>
                                    <input type="date" class="input_field" style="width:3cm;" value="<?php if(isset($fecha_aceptacion)) echo $fecha_aceptacion; else echo "2019-01-01"; ?>" disabled>
                                <?php else: ?>
                                    <input class="input_field" type="date" id="start1" name="trip_start_1" value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" min="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" max="<?php if(isset($limiteAno)) echo $limiteAno; ?>-12-31">
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" align="center">
                            <strong>DIAGNÓSTICO Y POSIBLES CONSECUENCIAS</strong>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-12" style="min-width: 570px;" align="center">
                                <table width="100%" class="table table-bordered ">
                                    <tr>
                                        <td>
                                            Si&nbsp;<input type="radio" style="transform: scale(1.3);" value="1" name="danos" <?php if(isset($danos)) {if($danos == "1") echo "checked";}else echo "checked"; ?>>
                                            No&nbsp;<input type="radio" style="transform: scale(1.3);" value="0" name="danos" <?php if(isset($danos)) if($danos == "0") echo "checked";?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="radio" style="transform: scale(1.3);" id="golpes" name="marcasRadio" checked value="hit">&nbsp;Golpes
                                            &nbsp;&nbsp;<input type="radio" style="transform: scale(1.3);" id="roto" name="marcasRadio" value="broken">&nbsp;Roto / Estrellado
                                            &nbsp;&nbsp;<input type="radio" style="transform: scale(1.3);" id="rayones" name="marcasRadio" value="scratch">&nbsp;Rayones
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <?php if (isset($danosImg)): ?>
                                                <?php if ($danosImg != ''): ?>
                                                    <img src="<?php echo base_url().$danosImg; ?>" alt="" style="width:420px; height:500px;">
                                                    <canvas id="canvas_3" width="420" height="500" hidden>
                                                        Su navegador no soporta canvas.
                                                    </canvas>
                                                <?php else: ?>
                                                    <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" id="limpio" style="width:420px; height:500px;" hidden>
                                                    <canvas id="canvas_3" width="420" height="500">
                                                        Su navegador no soporta canvas.
                                                    </canvas>
                                                <?php endif ?>
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" id="limpio" style="width:420px; height:500px;" hidden>
                                                <canvas id="canvas_3" width="420" height="500">
                                                    Su navegador no soporta canvas.
                                                </canvas>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <br>
                            <div class="col-md-12 table-responsive" align="center">
                                <?php if (isset($danosImg)): ?>
                                    <?php if ($danosImg != ''): ?>
                                        <a href="<?php if(isset($danosImg)) echo base_url().$danosImg; else echo base_url().'assets/imgs/car.jpeg';?>" class="btn btn-default" download="Diagrama_Diagnostico">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            Descargar diagrama
                                        </a>
                                    <?php else: ?>
                                        <a href="#" class="btn btn-default" id="btn-download" download="Diagrama_Diagnostico">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                            Descargar diagrama
                                        </a>

                                        <button type="button" class="btn btn-primary" id="resetoeDiagrama" style="margin-left: 20px;">
                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                            Restaurar diagrama
                                        </button>
                                    <?php endif ?>
                                <?php else: ?>
                                    <a href="#" class="btn btn-default" id="btn-download" download="Diagrama_Diagnostico">
                                        <i class="fa fa-download" aria-hidden="true"></i>
                                        Descargar diagrama
                                    </a>

                                    <button type="button" class="btn btn-primary" id="resetoeDiagrama" style="margin-left: 20px;">
                                        <i class="fa fa-refresh" aria-hidden="true"></i>
                                        Restaurar diagrama
                                    </button>
                                <?php endif ?>

                                <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car.jpeg'; ?>">
                                <input type="hidden" name="danosMarcas" id="danosMarcas" value="<?php if(isset($danosImg)) echo $danosImg; ?>"> 
                                <input type="hidden" name="" id="validaClick_1" value="0">
                            </div> 
                        </div>

                        <div class="panel-body">
                            <div class="col-md-12" style="min-width: 600px; margin-left: -10px;">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                Interiores
                                                <br>
                                                <span class="error" id ="error_interiores"></span>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                Si
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                No
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                (No cuenta)<br>
                                                NC
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Llavero.</td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   llaveroCheck" name="interiores[]" value="Llavero.Si" <?php if(isset($interiores)) if(in_array("Llavero.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   llaveroCheck" name="interiores[]" value="Llavero.No" <?php if(isset($interiores)) if(in_array("Llavero.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   llaveroCheck" name="interiores[]" value="Llavero.NC" <?php if(isset($interiores)) if(in_array("Llavero.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Seguro rines.</td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   SeguroRinesCheck" name="interiores[]" value="SeguroRines.Si" <?php if(isset($interiores)) if(in_array("SeguroRines.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   SeguroRinesCheck" name="interiores[]" value="SeguroRines.No" <?php if(isset($interiores)) if(in_array("SeguroRines.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   SeguroRinesCheck" name="interiores[]" value="SeguroRines.NC" <?php if(isset($interiores)) if(in_array("SeguroRines.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Indicadores de falla Activados:</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <table style="width:100%;">
                                                    <tr>
                                                        <td>
                                                            <img src="<?php echo base_url();?>assets/imgs/05.png" style="width:1.2cm;">
                                                        </td>
                                                        <td>
                                                            <img src="<?php echo base_url();?>assets/imgs/02.png" style="width:1.2cm;">
                                                        </td>
                                                        <td>
                                                            <img src="<?php echo base_url();?>assets/imgs/07.png" style="width:1.2cm;">
                                                        </td>
                                                        <td>
                                                            <img src="<?php echo base_url();?>assets/imgs/01.png" style="width:1.2cm;">
                                                        </td>
                                                        <td>
                                                            <img src="<?php echo base_url();?>assets/imgs/06.png" style="width:1.2cm;">
                                                        </td>
                                                        <td>
                                                            <img src="<?php echo base_url();?>assets/imgs/04.png" style="width:1.2cm;">
                                                        </td>
                                                        <td>
                                                            <img src="<?php echo base_url();?>assets/imgs/03.png" style="width:1.2cm;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.Si" <?php if(isset($interiores)) if(in_array("IndicadorGasolina.Si",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.No" <?php if(isset($interiores)) if(in_array("IndicadorGasolina.No",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.3);" class="interiores   IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.NC" <?php if(isset($interiores)) if(in_array("IndicadorGasolina.NC",$interiores)) echo "checked";?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.3);" name="interiores[]" value="IndicadorGasolina" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.Si" <?php if(isset($interiores)) if(in_array("IndicadorMantenimento.Si",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.No" <?php if(isset($interiores)) if(in_array("IndicadorMantenimento.No",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.3);" class="interiores   IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.NC" <?php if(isset($interiores)) if(in_array("IndicadorMantenimento.NC",$interiores)) echo "checked";?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.3);" name="interiores[]" value="IndicadorMantenimento" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.3);" class="interiores   SistemaABSCheck" name="interiores[]" value="SistemaABS.Si" <?php if(isset($interiores)) if(in_array("SistemaABS.Si",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.3);" class="interiores   SistemaABSCheck" name="interiores[]" value="SistemaABS.No" <?php if(isset($interiores)) if(in_array("SistemaABS.No",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.3);" class="interiores   SistemaABSCheck" name="interiores[]" value="SistemaABS.NC" <?php if(isset($interiores)) if(in_array("SistemaABS.NC",$interiores)) echo "checked";?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.3);" name="interiores[]" value="SistemaABS" <?php echo set_checkbox('interiores[]', 'SistemaABS'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.Si" <?php if(isset($interiores)) if(in_array("IndicadorFrenos.Si",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.No" <?php if(isset($interiores)) if(in_array("IndicadorFrenos.No",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.3);" class="interiores   IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.NC" <?php if(isset($interiores)) if(in_array("IndicadorFrenos.NC",$interiores)) echo "checked";?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.3);" name="interiores[]" value="IndicadorFrenos" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.Si" <?php if(isset($interiores)) if(in_array("IndicadorBolsaAire.Si",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.No" <?php if(isset($interiores)) if(in_array("IndicadorBolsaAire.No",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.3);" class="interiores   IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.NC" <?php if(isset($interiores)) if(in_array("IndicadorBolsaAire.NC",$interiores)) echo "checked";?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.3);" name="interiores[]" value="IndicadorBolsaAire" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.Si" <?php if(isset($interiores)) if(in_array("IndicadorTPMS.Si",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.No" <?php if(isset($interiores)) if(in_array("IndicadorTPMS.No",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.3);" class="interiores   IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.NC" <?php if(isset($interiores)) if(in_array("IndicadorTPMS.NC",$interiores)) echo "checked";?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.3);" name="interiores[]" value="IndicadorTPMS" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.Si" <?php if(isset($interiores)) if(in_array("IndicadorBateria.Si",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.3);" class="interiores   IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.No" <?php if(isset($interiores)) if(in_array("IndicadorBateria.No",$interiores)) echo "checked";?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.3);" class="interiores   IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.NC" <?php if(isset($interiores)) if(in_array("IndicadorBateria.NC",$interiores)) echo "checked";?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.3);" name="interiores[]" value="IndicadorBateria" <?php echo set_checkbox('interiores[]', 'IndicadorBateria'); ?>> -->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Indicador de falla activados.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="testigos" name="interiores[]" value="IndicadorDeFalla" <?php echo set_checkbox('interiores[]', 'Indicador de falla'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   IndicadorDeFallaCheck" name="interiores[]" value="IndicadorDeFalla.Si" <?php if(isset($interiores)) if(in_array("IndicadorDeFalla.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   IndicadorDeFallaCheck" name="interiores[]" value="IndicadorDeFalla.No" <?php if(isset($interiores)) if(in_array("IndicadorDeFalla.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   IndicadorDeFallaCheck" name="interiores[]" value="IndicadorDeFalla.NC" <?php if(isset($interiores)) if(in_array("IndicadorDeFalla.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rociadores y Limpiaparabrisas.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="rociador" name="interiores[]" value="Rociadores" <?php echo set_checkbox('interiores[]', 'Rociadores'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   RociadoresCheck" name="interiores[]" value="Rociadores.Si" <?php if(isset($interiores)) if(in_array("Rociadores.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   RociadoresCheck" name="interiores[]" value="Rociadores.No" <?php if(isset($interiores)) if(in_array("Rociadores.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   RociadoresCheck" name="interiores[]" value="Rociadores.NC" <?php if(isset($interiores)) if(in_array("Rociadores.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Claxon.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="claxon" name="interiores[]" value="Claxon" <?php echo set_checkbox('interiores[]', 'Claxon'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   ClaxonCheck" name="interiores[]" value="Claxon.Si" <?php if(isset($interiores)) if(in_array("Claxon.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   ClaxonCheck" name="interiores[]" value="Claxon.No" <?php if(isset($interiores)) if(in_array("Claxon.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   ClaxonCheck" name="interiores[]" value="Claxon.NC" <?php if(isset($interiores)) if(in_array("Claxon.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                Luces
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                Delanteras.
                                            </td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="lucesd" name="interiores[]" value="LucesDelanteras" <?php echo set_checkbox('interiores[]', 'LucesDelanteras'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.Si" <?php if(isset($interiores)) if(in_array("LucesDelanteras.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.No" <?php if(isset($interiores)) if(in_array("LucesDelanteras.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.NC" <?php if(isset($interiores)) if(in_array("LucesDelanteras.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                Traseras.
                                            </td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="lucest" name="interiores[]" value="LucesTraseras" <?php echo set_checkbox('interiores[]', 'LucesTraseras'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesTraserasCheck" name="interiores[]" value="LucesTraseras.Si" <?php if(isset($interiores)) if(in_array("LucesTraseras.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesTraserasCheck" name="interiores[]" value="LucesTraseras.No" <?php if(isset($interiores)) if(in_array("LucesTraseras.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesTraserasCheck" name="interiores[]" value="LucesTraseras.NC" <?php if(isset($interiores)) if(in_array("LucesTraseras.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                Stop.
                                            </td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="lucess" name="interiores[]" value="LucesStop" <?php echo set_checkbox('interiores[]', 'LucesStop'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesStopCheck" name="interiores[]" value="LucesStop.Si" <?php if(isset($interiores)) if(in_array("LucesStop.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesStopCheck" name="interiores[]" value="LucesStop.No" <?php if(isset($interiores)) if(in_array("LucesStop.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   LucesStopCheck" name="interiores[]" value="LucesStop.NC" <?php if(isset($interiores)) if(in_array("LucesStop.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Radio / Caratulas.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="radio" name="interiores[]" value="Caratulas" <?php echo set_checkbox('interiores[]', 'Caratulas'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   CaratulasCheck" name="interiores[]" value="Caratulas.Si" <?php if(isset($interiores)) if(in_array("Caratulas.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   CaratulasCheck" name="interiores[]" value="Caratulas.No" <?php if(isset($interiores)) if(in_array("Caratulas.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   CaratulasCheck" name="interiores[]" value="Caratulas.NC" <?php if(isset($interiores)) if(in_array("Caratulas.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pantallas.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="pantallas" name="interiores[]" value="Pantallas" <?php echo set_checkbox('interiores[]', 'Pantallas'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   PantallasCheck" name="interiores[]" value="Pantallas.Si" <?php if(isset($interiores)) if(in_array("Pantallas.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   PantallasCheck" name="interiores[]" value="Pantallas.No" <?php if(isset($interiores)) if(in_array("Pantallas.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   PantallasCheck" name="interiores[]" value="Pantallas.NC" <?php if(isset($interiores)) if(in_array("Pantallas.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>A/C.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="aire" name="interiores[]" value="AA" <?php echo set_checkbox('interiores[]', 'AA'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   AACheck" name="interiores[]" value="AA.Si" <?php if(isset($interiores)) if(in_array("AA.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   AACheck" name="interiores[]" value="AA.No" <?php if(isset($interiores)) if(in_array("AA.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   AACheck" name="interiores[]" value="AA.NC" <?php if(isset($interiores)) if(in_array("AA.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Encendedor.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="encendedor" name="interiores[]" value="Encendedor" <?php echo set_checkbox('interiores[]', 'Encendedor'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   EncendedorCheck" name="interiores[]" value="Encendedor.Si" <?php if(isset($interiores)) if(in_array("Encendedor.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   EncendedorCheck" name="interiores[]" value="Encendedor.No" <?php if(isset($interiores)) if(in_array("Encendedor.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   EncendedorCheck" name="interiores[]" value="Encendedor.NC" <?php if(isset($interiores)) if(in_array("Encendedor.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Vidrios.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="vidrios" name="interiores[]" value="Vidrios" <?php echo set_checkbox('interiores[]', 'Vidrios'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   VidriosCheck" name="interiores[]" value="Vidrios.Si" <?php if(isset($interiores)) if(in_array("Vidrios.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   VidriosCheck" name="interiores[]" value="Vidrios.No" <?php if(isset($interiores)) if(in_array("Vidrios.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   VidriosCheck" name="interiores[]" value="Vidrios.NC" <?php if(isset($interiores)) if(in_array("Vidrios.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Espejos.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="espejos" name="interiores[]" value="Espejos" <?php echo set_checkbox('interiores[]', 'Espejos'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   EspejosCheck" name="interiores[]" value="Espejos.Si" <?php if(isset($interiores)) if(in_array("Espejos.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   EspejosCheck" name="interiores[]" value="Espejos.No" <?php if(isset($interiores)) if(in_array("Espejos.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   EspejosCheck" name="interiores[]" value="Espejos.NC" <?php if(isset($interiores)) if(in_array("Espejos.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Seguros eléctricos.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="segurosE" name="interiores[]" value="SegurosEléctricos" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.Si" <?php if(isset($interiores)) if(in_array("SegurosEléctricos.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.No" <?php if(isset($interiores)) if(in_array("SegurosEléctricos.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.NC" <?php if(isset($interiores)) if(in_array("SegurosEléctricos.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Disco compacto.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="disco" name="interiores[]" value="CD" <?php echo set_checkbox('interiores[]', 'CD'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   CDCheck" name="interiores[]" value="CD.Si" <?php if(isset($interiores)) if(in_array("CD.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   CDCheck" name="interiores[]" value="CD.No" <?php if(isset($interiores)) if(in_array("CD.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   CDCheck" name="interiores[]" value="CD.NC" <?php if(isset($interiores)) if(in_array("CD.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Asientos y vestiduras.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="vestiduras" name="interiores[]" value="Vestiduras" <?php echo set_checkbox('interiores[]', 'Vestiduras'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   VestidurasCheck" name="interiores[]" value="Vestiduras.Si" <?php if(isset($interiores)) if(in_array("Vestiduras.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   VestidurasCheck" name="interiores[]" value="Vestiduras.No" <?php if(isset($interiores)) if(in_array("Vestiduras.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   VestidurasCheck" name="interiores[]" value="Vestiduras.NC" <?php if(isset($interiores)) if(in_array("Vestiduras.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tapetes.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="tapetes" name="interiores[]" value="Tapetes" <?php echo set_checkbox('interiores[]', 'Tapetes'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   TapetesCheck" name="interiores[]" value="Tapetes.Si" <?php if(isset($interiores)) if(in_array("Tapetes.Si",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   TapetesCheck" name="interiores[]" value="Tapetes.No" <?php if(isset($interiores)) if(in_array("Tapetes.No",$interiores)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="interiores   TapetesCheck" name="interiores[]" value="Tapetes.NC" <?php if(isset($interiores)) if(in_array("Tapetes.NC",$interiores)) echo "checked";?>>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" align="center">
                            Cajuela / Exteriores / Documentación
                        </div>
                        <div class="panel-body table-responsive">
                            <div class="col-md-4">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="headers_table" align="center" style="border:1px solid black;border-style: double;">
                                            <td>
                                                Cajuela
                                                <br>
                                                <span class="error" id ="error_cajuela"></span>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                Si
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                No
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                No cuenta<br>
                                                (NC)
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Herramienta.</td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  HerramientaCheck" name="cajuela[]" value="Herramienta.Si" <?php if(isset($cajuela)) if(in_array("Herramienta.Si",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  HerramientaCheck" name="cajuela[]" value="Herramienta.No" <?php if(isset($cajuela)) if(in_array("Herramienta.No",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  HerramientaCheck" name="cajuela[]" value="Herramienta.NC" <?php if(isset($cajuela)) if(in_array("Herramienta.NC",$cajuela)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gato / Llave.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="gato" name="cajuela[]" value="Llave" <?php echo set_checkbox('cajuela[]', 'Llave'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  eLlaveCheck" name="cajuela[]" value="eLlave.Si" <?php if(isset($cajuela)) if(in_array("eLlave.Si",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  eLlaveCheck" name="cajuela[]" value="eLlave.No" <?php if(isset($cajuela)) if(in_array("eLlave.No",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  eLlaveCheck" name="cajuela[]" value="eLlave.NC" <?php if(isset($cajuela)) if(in_array("eLlave.NC",$cajuela)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Reflejantes.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="reflejante" name="cajuela[]" value="Reflejantes" <?php echo set_checkbox('cajuela[]', 'Reflejantes'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  ReflejantesCheck" name="cajuela[]" value="Reflejantes.Si" <?php if(isset($cajuela)) if(in_array("Reflejantes.Si",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  ReflejantesCheck" name="cajuela[]" value="Reflejantes.No" <?php if(isset($cajuela)) if(in_array("Reflejantes.No",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  ReflejantesCheck" name="cajuela[]" value="Reflejantes.NC" <?php if(isset($cajuela)) if(in_array("Reflejantes.NC",$cajuela)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cables.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="cables" name="cajuela[]" value="Cables" <?php echo set_checkbox('cajuela[]', 'Cables'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  CablesCheck" name="cajuela[]" value="Cables.Si" <?php if(isset($cajuela)) if(in_array("Cables.Si",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  CablesCheck" name="cajuela[]" value="Cables.No" <?php if(isset($cajuela)) if(in_array("Cables.No",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  CablesCheck" name="cajuela[]" value="Cables.NC" <?php if(isset($cajuela)) if(in_array("Cables.NC",$cajuela)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Extintor.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="extintor" name="cajuela[]" value="Extintor" <?php echo set_checkbox('cajuela[]', 'Extintor'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  ExtintorCheck" name="cajuela[]" value="Extintor.Si" <?php if(isset($cajuela)) if(in_array("Extintor.Si",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  ExtintorCheck" name="cajuela[]" value="Extintor.No" <?php if(isset($cajuela)) if(in_array("Extintor.No",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  ExtintorCheck" name="cajuela[]" value="Extintor.NC" <?php if(isset($cajuela)) if(in_array("Extintor.NC",$cajuela)) echo "checked";?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Llanta Refacción.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" ng-model="llanta" name="cajuela[]" value="LlantaRefacción" <?php echo set_checkbox('cajuela[]', 'LlantaRefacción'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.Si" <?php if(isset($cajuela)) if(in_array("LlantaRefaccion.Si",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.No" <?php if(isset($cajuela)) if(in_array("LlantaRefaccion.No",$cajuela)) echo "checked";?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.3);" class="cauela  LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.NC" <?php if(isset($cajuela)) if(in_array("LlantaRefaccion.NC",$cajuela)) echo "checked";?>>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr class="headers_table" align="center" style="border:1px solid black;border-style: double;">
                                                    <td>
                                                        Exteriores
                                                        <br>
                                                        <span class="error" id ="error_exteriores"></span>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        Si
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        No
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Tapones rueda.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" ng-model="tapones" name="exteriores[]" value="TaponesRueda" <?php echo set_checkbox('exteriores[]', 'TaponesRueda'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="exteriores  TaponesRuedaCheck" name="exteriores[]" value="TaponesRueda.Si" <?php if(isset($exteriores)) if(in_array("TaponesRueda.Si",$exteriores)) echo "checked";?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="exteriores  TaponesRuedaCheck" name="exteriores[]" value="TaponesRueda.No" <?php if(isset($exteriores)) if(in_array("TaponesRueda.No",$exteriores)) echo "checked";?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Gomas de limpiadores.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" ng-model="gomas" name="exteriores[]" value="Gomas de limpiadores" <?php echo set_checkbox('exteriores[]', 'Gomas de limpiadores'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="exteriores  GotasCheck" name="exteriores[]" value="Gotas.Si" <?php if(isset($exteriores)) if(in_array("Gotas.Si",$exteriores)) echo "checked";?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="exteriores  GotasCheck" name="exteriores[]" value="Gotas.No" <?php if(isset($exteriores)) if(in_array("Gotas.No",$exteriores)) echo "checked";?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Antena.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" ng-model="antena" name="exteriores[]" value="Antena" <?php echo set_checkbox('exteriores[]', 'Antena'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="exteriores  AntenaCheck" name="exteriores[]" value="Antena.Si" <?php if(isset($exteriores)) if(in_array("Antena.Si",$exteriores)) echo "checked";?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="exteriores  AntenaCheck" name="exteriores[]" value="Antena.No" <?php if(isset($exteriores)) if(in_array("Antena.No",$exteriores)) echo "checked";?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tapón de gasolina.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" ng-model="gasolina" name="exteriores[]" value="Tapón de gasolina" <?php echo set_checkbox('exteriores[]', 'Tapón de gasolina'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="exteriores  TaponGasolinaCheck" name="exteriores[]" value="TaponGasolina.Si" <?php if(isset($exteriores)) if(in_array("TaponGasolina.Si",$exteriores)) echo "checked";?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="exteriores  TaponGasolinaCheck" name="exteriores[]" value="TaponGasolina.No" <?php if(isset($exteriores)) if(in_array("TaponGasolina.No",$exteriores)) echo "checked";?>>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-6">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr class="headers_table" align="center" style="border:1px solid black;border-style: double;">
                                                    <td>
                                                        Documentación
                                                        <br>
                                                        <span class="error" id ="error_documentacion"></span>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        Si
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        No
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Póliza garantía/Manual prop.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" ng-model="poliza" name="documentacion[]" value="Póliza garantía" <?php echo set_checkbox('documentacion[]', 'Póliza garantía'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="documentacion  PolizaGarantiaCheck" name="documentacion[]" value="PolizaGarantia.Si" <?php if(isset($documentacion)) if(in_array("PolizaGarantia.Si",$documentacion)) echo "checked";?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="documentacion  PolizaGarantiaCheck" name="documentacion[]" value="PolizaGarantia.No" <?php if(isset($documentacion)) if(in_array("PolizaGarantia.No",$documentacion)) echo "checked";?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Seguro de Rines.</td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="documentacion  SeguroRinesDocCheck" name="documentacion[]" value="SeguroRinesDoc.Si" <?php if(isset($documentacion)) if(in_array("SeguroRinesDoc.Si",$documentacion)) echo "checked";?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="documentacion  SeguroRinesDocCheck" name="documentacion[]" value="SeguroRinesDoc.No" <?php if(isset($documentacion)) if(in_array("SeguroRinesDoc.No",$documentacion)) echo "checked";?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Certificado verificación.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" ng-model="verificacion" name="documentacion[]" value="Certificado verificación" <?php echo set_checkbox('documentacion[]', 'Certificado verificación'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="documentacion  cVerificacionCheck" name="documentacion[]" value="cVerificacion.Si" <?php if(isset($documentacion)) if(in_array("cVerificacion.Si",$documentacion)) echo "checked";?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="documentacion  cVerificacionCheck" name="documentacion[]" value="cVerificacion.No" <?php if(isset($documentacion)) if(in_array("cVerificacion.No",$documentacion)) echo "checked";?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tarjeta de circulación.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" ng-model="circulacion" name="documentacion[]" value="Tarjeta de circulación" <?php echo set_checkbox('documentacion[]', 'Tarjeta de circulación'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="documentacion  tCirculacionCheck" name="documentacion[]" value="tCirculacion.Si" <?php if(isset($documentacion)) if(in_array("tCirculacion.Si",$documentacion)) echo "checked";?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.3);" class="documentacion  tCirculacionCheck" name="documentacion[]" value="tCirculacion.No" <?php if(isset($documentacion)) if(in_array("tCirculacion.No",$documentacion)) echo "checked";?>>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td align="center" style="width:40%">
                                                        <img src="<?php echo base_url().'assets/imgs/gas.jpeg'; ?>" style="width:50px;" alt="">
                                                    </td>
                                                    <td style="margin-left:5px;" colspan="2">
                                                        Nivel de Gasolina:&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="number" name="nivelGasolina_1" class="input_field" style="width:1.5cm;" min="0" value="<?php if(isset($nivelGasolinaDiv)) echo $nivelGasolinaDiv[0]; else echo '0'; ?>">
                                                        <img src="<?php echo base_url().'assets/imgs/diagonal.jpg'; ?>" style="width:20px; height:20px;" alt="">
                                                        <input type="number" name="nivelGasolina_2" class="input_field" style="width:1.5cm;" min="0" value="<?php if(isset($nivelGasolinaDiv)) echo $nivelGasolinaDiv[1]; else echo '0'; ?>">
                                                        
                                                        <br>
                                                        <span class="error" id ="error_gas"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>¿Deja artículos personales?</td>
                                                    <td>
                                                      (Si&nbsp;<input type="radio" style="transform: scale(1.3);" value="1" name="articulos" <?php if(isset($articulos)) {if($articulos == "1") echo "checked"; } ?>>
                                                      No&nbsp;<input type="radio" style="transform: scale(1.3);" value="0" name="articulos" <?php if(isset($articulos)) {if($articulos == "0") echo "checked"; }else echo "checked"; ?>>)<br>
                                                        <?php echo form_error('articulos', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>¿Cuales?</td>
                                                    <td>
                                                        <input type="text" class="input_field" value="<?php if(isset($cualesArticulos)) echo $cualesArticulos; else echo 'Ninguno'; ?>" name="cualesArticulos">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>¿Desea reportar algo más?</td>
                                                    <td>
                                                        <input type="text" class="input_field" value="<?php if(isset($reporte)) echo $reporte; else echo 'Ninguno'; ?>" name="reporte">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2>IMPORTANTE</h2><br>
                                    <p align="justify" style="text-align: justify;">
                                        Favor de recoger su unidad en un plazo no mayor a 48 Hrs., contados a partir de la fecha en que su
                                        asesor le informe que la unidad esta lista, pasado este plazo deberá cubrir el amacenaje por guarda de
                                        vehículo conforme al contrato.<br>
                                        Manifiesto bajo protesta que soy el dueño del vehículo, o tengo orden y/o autorización de él para
                                        utilizarlo y ordenar ésta reparación, en su representación, autorizando en su nombre y en el propio
                                        la realización de los trabajos descritos, así como la colocación de las piezas, refacciones, repuestos
                                        y materiales e insumos necesarios para efectuarlos, comprometiéndome en su nombre y en el propio a pagar
                                        el importe de la reparación; también manifiesto mi conformidad con el inventario realizado y que consta
                                        en el ejemplar del presente que recibo; en cualquier caso que las partes acuerden que el vehículo vaya a
                                        ser recogido o entregado por personal del PROVEEDOR en el domicilio del CONSUMIDOR, ello solo será mediante
                                        previo acuerdo u orden del CONSUMIDOR por escrito y debidamente aceptada por el PROVEEDOR, con un costo de
                                        $<input type="text" ng-model="costo" class="input_field" name="costo" style="width:3cm;" value="<?php if(isset($costo)) {if($costo != '') echo $costo; else echo 'Por definir'; }else echo 'Por definir'; ?>">
                                        (MONEDA NACIONAL);
                                        será en todo caso obligación del personal del PROVEEDOR identificarse plenamente ante el CONSUMIDOR como tal.<br>
                                        Finalmente manifiesto mi conformidad con los términos y condiciones previstas en el contrato inscrito al reverso,
                                        el cual manifiesto que he leído, obligándome en lo personal y en nombre del propietario del automóvil en
                                        los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día
                                        <input type="number" class="input_field" max="31" min="1" name="diaValue" style="width:3cm;" value="<?php if(isset($diaValue)) echo $diaValue; ?>">
                                        de
                                        <input type="text" class="input_field" name="mesValue" style="width:3cm;" value="<?php if(isset($mesValue)) echo $mesValue; ?>">
                                        de
                                        <input type="number" min="<?php if(isset($limiteAno)) echo $limiteAno; ?>" class="input_field" name="anioValue" style="width:3cm;" value="<?php if(isset($limiteAno)) echo $limiteAno; ?>">.
                                    </p>
                                </div>
                            </div>

                            <br><br>
                                <div class="row">
                                    <div class="col-md-12" align="right">
                                        <a id ="permisoProv" class="btn btn-dark" data-target="#videoEvidencia" data-toggle="modal" style="color:white;">
                                            Subir video
                                        </a>
                                        <br>
                                        <label for="" class="" id="errorVideo"></label>
                                    </div>
                                </div>
                            <br><br>

                            <div class="row">
                                <div class="col-md-6" align="center">
                                    <?php if (isset($firmaAsesor)): ?>
                                        <?php if ($firmaAsesor != ""): ?>
                                            <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?php if(isset($firmaAsesor)) echo $firmaAsesor; ?>">
                                            <img class="marcoImg" src="<?= base_url() ?><?php if(isset($firmaAsesor)) {if($firmaAsesor != "") echo $firmaAsesor; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaFirmaAsesor" alt="">
                                            <br><br>
                                            <label for="">
                                                <u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreAsesor)) echo $nombreAsesor; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                            </label>
                                            <input type="hidden" id="asesorNombre" class="input_field asesorname" name="asesorNombre" style="width:100%;" value="<?php if(isset($nombreAsesor)) echo $nombreAsesor; ?>">
                                            
                                        <?php else: ?>
                                            <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                                            <img class="marcoImg" src="<?= set_value('rutaFirmaAsesor');?>" id="firmaFirmaAsesor" alt="">
                                            <br><br>
                                            <input type="text" id="asesorNombre" class="input_field asesorname" name="asesorNombre" style="width:100%;" value="<?php if(isset($nombreAsesor)) echo $nombreAsesor; ?>">

                                            <br>
                                            <a class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                        <?php endif ?>
                                    <?php else: ?>
                                        <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaAsesor');?>" id="firmaFirmaAsesor" alt="">
                                        <br><br>
                                        <input type="text" id="asesorNombre" class="input_field asesorname" name="asesorNombre" style="width:100%;" value="<?php if(isset($nombreAsesor)) echo $nombreAsesor; ?>">

                                        <br>
                                        <a class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                    <?php endif ?>
                                    
                                    <br><br>
                                    Nombre y firma del asesor de servicio.
                                    <br>
                                    <span class="error" id ="error_firma_3"></span>
                                    <br>
                                </div>

                                <div class="col-md-6" align="center">
                                    <?php if (isset($firmaConsumidor2)): ?>
                                        <?php if ($firmaConsumidor2 != ""): ?>
                                            <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?php if(isset($firmaConsumidor2)) echo $firmaConsumidor2; ?>">
                                            <img class="marcoImg" src="<?= base_url() ?><?php if(isset($firmaConsumidor2)) {if($firmaConsumidor2 != "") echo $firmaConsumidor2; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaFirmaConsumidor" alt="">
                                            <br><br>
                                            <label for=""><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreConsumidor2)) echo $nombreConsumidor2; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
                                            <input type="hidden" id="nombreConsumidor2" class="input_field nombreCliente" name="nombreConsumidor2" style="width:100%;" value="<?php if(isset($nombreConsumidor2)) echo $nombreConsumidor2; ?>">
                                            
                                        <?php else: ?>
                                            <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?= set_value('rutaFirmaConsumidor');?>">
                                            <img class="marcoImg" src="<?= set_value('rutaFirmaConsumidor');?>" id="firmaFirmaConsumidor" alt="">
                                            <br>
                                            <input type="text" id="nombreConsumidor2" class="input_field nombreCliente" name="nombreConsumidor2" style="width:100%;" value="<?php if(isset($nombreConsumidor2)) echo $nombreConsumidor2; ?>">

                                            <br>
                                            <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                        <?php endif ?>
                                    <?php else: ?>
                                        <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?= set_value('rutaFirmaConsumidor');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaConsumidor');?>" id="firmaFirmaConsumidor" alt="">
                                        <br>
                                        <input type="text" id="nombreConsumidor2" class="input_field nombreCliente" name="nombreConsumidor2" style="width:100%;" value="<?php if(isset($nombreConsumidor2)) echo $nombreConsumidor2; ?>">

                                        <br>
                                        <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                    <?php endif ?>
                                    
                                    <br><br>
                                    Firma del consumidor<br>
                                    ACEPTA PRESUPUESTO E INVENTARIO<br>
                                    <br>
                                    <span class="error" id ="error_firma_4"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br>
        <br><br>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        <strong>CONTRATO DE ADHESIÓN</strong>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <table style="width:100%">
                                <tr>
                                    <td>
                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                            <?=SUCURSAL?>, S.A. DE C.V. <br>
                                            <?=$this->config->item('encabezado_contrato')?>
                                        </p>
                                    </td>
                                    <td>
                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                            Folio No.:
                                            <input type="text" class="input_field camposContrato" name="folio" value="<?= ((isset($folio)) ? $folio : $id_cita) ?>" style="width:3cm;" disabled> <br>
                                            <span class="error" id ="error_folio"></span>
                                            Fecha:
                                            <input type="date" class="input_field camposContrato" name="fecha" value="<?= ((isset($fecha_f)) ? $fecha_f : $hoy) ?>" style="width:3cm;" disabled> <br>
                                            <span class="error" id ="error_fecha"></span>
                                            Hora:
                                            <input type="text" class="input_field camposContrato" name="hora" value="<?= ((isset($hora)) ? $hora : date('H:i')) ?>" style="width:3cm;" disabled> <br>
                                            <span class="error" id ="error_hora"></span>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <br>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <h6 align="center" style="font-weight:bold;"><strong></strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                CONTRATO DE PRESTACIÓN DE SERVICIO DE REPARACIÓN Y MANTENIMIENTO DE VEHÍCULOS QUE CELEBRAN POR UNA PARTE
                                <input type="text" class="input_field camposContrato" name="empresa" id="txtEmpresa" value="<?= SUCURSAL ?>" style="width:7cm;" disabled>, S.A. DE C.V., DISTRIBUIDOR AUTORIZADO FORD Y POR LA OTRA EL CONSUMIDOR CUYO NOMBRE SE SEÑALA EN EL ANVERSO DEL PRESENTE, A QUIENES EN LO SUCESIVO Y PARA EFECTO DEL PRESENTE CONTRATO SE LE DENOMINARA “EL PROVEEDOR” Y “EL CONSUMIDOR”, RESPECTIVAMENTE.
                            </p>
                            <h6 align="center"><strong>DECLARACIONES</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                Las partes denominadas CONSUMIDOR y PROVEEDOR, se reconocen las personalidades con las cuales se ostentan y que se encuentran especificadas en este documento (orden de servicio y contrato), por lo cual, están dispuestas a sujetarse a las condiciones que se establecen en las siguientes:
                            </p>
                            <h6 align="center"><strong>CLÁUSULAS</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>1.- </b>Objeto: El PROVEEDOR realizará todas las operaciones de mantenimiento, reparaciones y composturas, solicitadas por el CONSUMIDOR que suscribe el presente contrato, a las que se someterá el vehículo que al anverso se detalla, para obtener las condiciones de funcionamiento de acuerdo a lo solicitado por el CONSUMIDOR y que serán realizadas a cargo y por cuenta del CONSUMIDOR. EL PROVEEDOR no condicionará en modo alguno la prestación de servicios de reparación o mantenimiento del vehículo, a la adquisición o renta de otros productos o servicios en el mismo establecimiento o en otro taller o agencia predeterminada.
                                El precio total de los servicios contratados se establece en el presupuesto que forma parte del presente y se describe en su anverso; dicho precio será pagado por el CONSUMIDOR al recibo del vehículo reparado, o en su caso, al momento de ser celebrado el presente contrato, por concepto de anticipo, la cantidad que resulte conforme a lo que establece la cláusula 14, y el resto en la fecha de entrega del vehículo reparado; todo pago efectuado por el CONSUMIDOR deberá efectuarse en el establecimiento del PROVEEDOR al contado y en Moneda Nacional o extranjera al tipo de cambio vigente al día de pago, de contado, ya sea en efectivo, o mediante tarjeta de crédito o deposito o transferencia bancaria efectuada con anterioridad a la entrega del vehículo.
                            </p>
                            <h6 align="center"><strong>DE LAS CONDICIONES GENERALES:</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>2.-</b> Las partes están de acuerdo en que las condiciones generales en las que se encuentra el automóvil de acuerdo con el inventario visual al momento de su recepción, son las que se definen en la orden de servicio-presupuesto que se encuentra al anverso del presente contrato.
                            </p>
                            <h6 align="center"><strong>DEL DIAGNÓSTICO Y DEL PRESUPUESTO.</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>3.-</b> Las partes convienen en que en caso de que el automóvil motivo de este contrato se deje en las instalaciones del PROVEEDOR para diagnóstico y presupuesto, éste, en un plazo no mayor de lo asentado en este contrato, realizará y entregará al CONSUMIDOR dicho diagnóstico y presupuesto en el que hará constar el precio detallado de las operaciones de mano de obra, de partes, reparación de partes, materiales, insumos, así como el tiempo en que se efectuaría la reparación para que, en su caso, el CONSUMIDOR lo apruebe. El presupuesto que así se entregue tendrá una vigencia de 3 días hábiles.<br>
                                <b>4.-</b> El PROVEEDOR hace saber al CONSUMIDOR que al efectuar el diagnostico a su automóvil, las posibles consecuencias son las señaladas en el anverso del presente contrato, por lo que de no aceptarse la reparación, el vehículo le será entregado en las condiciones en que fue recibido para el diagnóstico, excepto en caso de que como consecuencia inevitable resulte imposible o ineficaz para su funcionamiento así entregarlo, porque al entregarlo armado sin repararlo, sea posible que se cause un daño mayor al mismo vehículo, o su uso constituya un riesgo para el usuario o terceros, por causa no imputable al PROVEEDOR, caso en el cual éste sólo se hará responsable de dichas consecuencias por causas imputables a él o sus empleados, el CONSUMIDOR enterado de esas consecuencias se responsabiliza de ellas.<br>
                                <b>5.-</b> El PROVEEDOR se obliga ante el CONSUMIDOR a respetar el presupuesto contenido en el presente contrato.<br>
                                <b>6.-</b> El CONSUMIDOR se obliga pagar a el PROVEEDOR por el diagnostico de su automóvil la cantidad de
                                $ <input type="text" class="input_field camposContrato" name="cantidadAu" value="<?= ((isset($pagoConsumidor)) ? $pagoConsumidor : 'Por definir') ?>" style="width:4cm;">(MONEDA NACIONAL),
                                siempre y cuando no apruebe la reparación. En caso de autorizar la reparación, el diagnóstico no será cobrado.
                            </p>
                            <h6 align="center"><strong>DE LA PRESTACIÓN DE SERVICIOS</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>7.-</b> Las partes convienen en que la fecha de aceptación del presupuesto es la que se indica en el anverso de este contrato.<br>
                                <b>8.-</b> El PROVEEDOR se obliga a hacer la entrega del automóvil reparado el día previamente establecido en el presupuesto. El PROVEEDOR se obliga a emplear partes y refacciones nuevas apropiadas para los servicios contratados salvo que el CONSUMIDOR ordene o autorice expresamente y por escrito se utilicen otras o las provea, de conformidad con lo establecido en el artículo 60 de la Ley Federal de Protección al Consumidor, caso en el cual la reparación no tendrá garantía en lo que se relacione a esas partes y refacciones, o a lo que éstas afecten, salvo en cuanto a la mano de obra que haya correspondido a su colocación. El retraso en la entrega de las refacciones por parte del CONSUMIDOR, si éste se compromete a proporcionarlas, prorrogará por el mismo lapso de demora, la fecha de entrega del vehículo.
                                <br>
                                <b>9.-</b> El CONSUMIDOR, puede desistir en cualquier momento de la prestación del servicio, pagando al PROVEEDOR el importe por los trabajos efectuados y partes colocadas o adquiridas, hasta el retiro del automóvil; en este caso, las refacciones adquiridas y que no hayan sido colocadas en el vehículo a repararse, serán entregadas al CONSUMIDOR.
                                <br>
                                <b>10.</b>- El PROVEEDOR, podrá utilizar el automóvil solo para recorridos de prueba, dentro de un radio de máximo 15 (quince) kilómetros alrededor del local en el que se presta el servicio, a efecto de realizar pruebas o verificar las reparaciones efectuadas; el PROVEEDOR no podrá usar el vehículo para fines propios o de terceros. El PROVEEDOR se hace responsable por los daños causados al vehículo, como consecuencia de los recorridos de prueba efectuados por parte de su personal. Cuando el CONSUMIDOR, solicite que él o un representante suyo sea quien conduzca el automóvil en un recorrido de prueba, el riesgo, será por su cuenta.
                                <br>
                                <b>11.</b>- El PROVEEDOR se hace responsable por las posibles descomposturas, daños o pérdidas parciales o totales imputables a él o a sus subalternos que sufra el vehículo, el equipo y los aditamentos adicionales que el CONSUMIDOR le haya notificado que existen en el momento de la recepción del mismo, o que se causen a terceros, mientras el vehículo se encuentre bajo su resguardo, salvo los ocasionados por desperfectos mecánicos o a resultas de piezas gastadas o sentidas, por incendio motivado por deficiencia de la instalación eléctrica, o fallas en el sistema de combustible, preexistentes y no causadas por el PROVEEDOR; para tal efecto el PROVEEDOR cuenta con un seguro suficiente para cubrir dichas eventualidades, bajo póliza expedida por compañía de seguros autorizada al efecto. El PROVEEDOR no se hace responsable por la pérdida de objetos dejados en el interior del automóvil, aun con la cajuela cerrada, salvo que estos le hayan sido notificados y puestos bajo su resguardo al momento de la recepción del automóvil. El PROVEEDOR tampoco se hace responsable por daños causados por fuerza mayor o caso fortuito, ni por la situación legal del automóvil cuando éste previamente haya sido robado o se hubiere utilizado en la comisión de algún ilícito; lo anterior, salvo que alguna de éstas cuestiones resultara legalmente imputable al PROVEEDOR; así mismo, el CONSUMIDOR, libera al PROVEEDOR de cualquier responsabilidad que surja o pueda surgir con relación al origen, posesión o cualquier otro derecho inherente al vehículo o a partes y componentes del mismo, obligándose en lo personal y en nombre del propietario, a responder
                                de su procedencia.
                            </p>
                            <h6 align="center"><strong>DEL PRECIO Y FORMAS DE PAGO</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>12.-</b> El CONSUMIDOR acepta haber tenido a su disposición los precios de la tarifa de mano de obra, de las refacciones y materiales a usar en las reparaciones, ofrecidas por el PROVEEDOR; los incrementos que resulten durante la reparación, por costos no previsibles y/o incrementos que resulten al momento de la ejecución de la reparación ordenada, deberán ser autorizados por el CONSUMIDOR, por vía telefónica siempre y cuando estos no excedan del 20% presupuestado. Si el incremento citado es superior al 20%, el CONSUMIDOR lo tendrá que autorizar en forma escrita o por correo electrónico proveniente del mismo señalado como propio de él en el anverso del presente contrato. El tiempo que, en su caso, transcurra para cumplir esta condición, modificará la fecha de entrega, en la misma proporción. Todas las quejas y sugerencias serán atendidas en el domicilio, correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su anverso.<br>
                                <b>13.-</b> Será obligación del PROVEEDOR expedir la factura correspondiente por los servicios y productos que preste o enajene; el importe total del servicio, así como el precio por concepto de refacciones, mano de obra, materiales y accesorios quedará especificado en ella, conforme a la ley.
                                <br>
                                <b>14.-</b> El CONSUMIDOR se obliga a pagar de contado al PROVEEDOR (conforme a lo establecido en la cláusula 1), en las instalaciones de éste y previo a la entrega del automóvil, el importe de los trabajos efectuados y partes colocadas o adquiridas hasta el retiro del mismo, de conformidad con el presupuesto elaborado para tal efecto. En caso de reparaciones cuyo presupuesto sea superior a $15,000.00 (QUINCE MIL PESOS 00/100 M.N.) el CONSUMIDOR deberá pagar un anticipo del 50% sobre el monto del presupuesto elaborado, y el saldo restante lo pagará a la entrega del automóvil debidamente reparado.
                            </p>
                            <h6 align="center"><strong>DE LA ENTREGA.</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>15.-</b> El PROVEEDOR se obliga a hacer la entrega del automóvil reparado en la fecha y hora establecida en este contrato, en las propias instalaciones del PROVEEDOR, pudiendo ampliarse dicho plazo en caso fortuito o de fuerza mayor, en cuyo caso será obligación del PROVEEDOR dar aviso previo al CONSUMIDOR de la causa y del tiempo que se ampliará el plazo de entrega.
                                <br>
                                <b>16.-</b> Las refacciones reemplazadas en las reparaciones quedarán a disposición del CONSUMIDOR al momento de recoger el automóvil, salvo que éste expreselo contrario o que las refacciones, partes o piezas sean las cambiadas en ejercicio de la garantía o se trate de residuos considerados peligrosos, de acuerdo conlas disposiciones legales aplicables. Si el CONSUMIDOR, al recoger su automóvil se niega a recibir las partes reemplazadas, el PROVEEDOR podrá disponer de ellas.
                                <br>
                                <b>17.-</b> El CONSUMIDOR se obliga a hacer el pago, a recoger el automóvil y, en su caso, las refacciones que fueran reemplazadas, dentro del horario establecido por el PROVEEDOR, en un término no mayor de 48 horas, contadas a partir del día y hora fijadas para su entrega, o bien de que se haya entregado el presupuesto y éste no hubiera sido autorizado. Si dentro de éste plazo el automóvil no pudiera circular por cualquier restricción, el plazo citado se ampliará 24 horas más. En caso de que el CONSUMIDOR no haga el pago y recoja el vehículo en el tiempo establecido, se obliga a pagar el almacenaje por la guarda del automóvil, caso en el que pagará la suma que resulte equivalente a  2 (dos) días de salario mínimo general vigente en la zona en la que se encuentran las instalaciones del PROVEEDOR, por cada día de retraso el cumplimiento de esas obligaciones. Si transcurridos 60 días naturales, contados a partir de la fecha en que debió entregarse el automóvil, no ha sido reclamado por el CONSUMIDOR, el PROVEEDOR notificará este hecho a las autoridades competentes y  podrá dar inicio a las acciones legales que correspondan para el cobro de los servicios y/o refacciones otorgados, así como del almacenaje.
                                <br>
                                <b>18.-</b> En el momento en que el PROVEEDOR entregue y el CONSUMIDOR reciba el automóvil reparado, se entenderá que esto fue a completa satisfacción del CONSUMIDOR en lo que respecta a sus condiciones generales de acuerdo al inventario visual, mismas que fueron descritos en la orden de servicio, así como también en cuanto a la reparación efectuada, sin afectar sus derechos a ejercer la garantía.
                            </p>
                            <h6 align="center"><strong>DE LA RESCISIÓN Y PENAS CONVENCIONALES</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>19.-</b> Es causa de rescisión del presente contrato: A.- Que EL PROVEEDOR incumpla en la fecha, hora y lugar de entrega del vehículo por causas propias, caso en el cual el CONSUMIDOR notificará por escrito del incumplimiento al PROVEEDOR, y éste hará entrega inmediata del vehículo debidamente reparado conforme al diagnóstico y presupuesto establecido, descontando del precio pactado para la prestación del servicio, la suma equivalente al 2% (dos por ciento) del total de la reparación, que se pacta como pena convencional; B.- Que el CONSUMIDOR incumpla con el pago de la reparación ordenada, en el término previsto en la cláusula 19, caso en el cual el PROVEEDOR le notificará por escrito su incumplimiento, y podrá optar por exigir la recisión o cumplimiento forzoso de la obligación, cobrando la misma pena pactada del 2% al
                                CONSUMIDOR, por la mora.
                            </p>
                            <h6 align="center"><strong>DE LAS GARANTÍAS</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>20.-</b> <u>Las reparaciones a que se refiere el presupuesto aceptado por el CONSUMIDOR y éste contrato están garantizadas por 60 días naturales contados a partir de la fecha de la entrega del vehículo ya reparado, en mano de obra y en las refacciones, piezas y accesorios</u>, y trabajos que por su naturaleza hayan tenido que ser realizados en otros talleres <u>tendrá la especificada por el fabricante o taller respectivo, siempre y cuando no sea menor a la expresada, y no se manifieste mal uso, negligencia o descuido en el uso de ellas, de conformidad a lo establecido en el artículo 77 de la Ley Federal de Protección al Consumidor</u>.  Si el vehículo es intervenido por un tercero, el PROVEEDOR no será responsable y la garantía quedará sin efecto. Las reclamaciones por garantía se harán en el establecimiento del PROVEEDOR, para lo cual el CONSUMIDOR deberá presentar su vehículo en dicho establecimiento. Las reparaciones efectuadas por el PROVEEDOR en cumplimiento a la garantía del servicio serán sin cargo alguno para el CONSUMIDOR, salvo aquellos trabajos que no deriven de las reparaciones aceptadas en el presupuesto. No se computará dentro del plazo de garantía el tiempo que dure la reparación y/o mantenimiento del vehículo para el cumplimiento de la misma. Esta garantía cubre cualquier falla, deficiencia o irregularidad relacionada con la reparación efectuada por el PROVEEDOR, por causas imputables al mismo y solo será válida siempre y cuando el automóvil se haya utilizado en condiciones de uso normal, se hayan observado en su uso las indicaciones de manejo y servicio que se le hubiera dado. En todo caso, el PROVEEDOR será corresponsable y solidario con los terceros del cumplimiento o incumplimiento de las garantías por ellos otorgadas en lo que se relacione a las piezas, refacciones y accesorios, colocadas en el vehículo y de los servicios a éste realizados, siempre que hayan sido contratados ante el CONSUMIDOR.
                                <br>
                                <b>21.-</b> Toda reclamación dentro del término de garantía, deberá ser realizada ante el PROVEEDOR que efectuó la reparación y en el domicilio del mismo indicado en el anverso del presente contrato.  En caso de que sea necesario hacer válida la garantía en un domicilio diverso al del PROVEEDOR, los gastos por ello deberán ser cubiertos por éste, siempre y cuando la garantía proceda, dichos gastos sean indispensables para tal fin, y sea igualmente indispensable realizar la reparación del vehículo en domicilio diverso al del PROVEEDOR, pero en dado caso, si el automóvil fallase fuera de la entidad donde se localiza el PROVEEDOR, éste inmediatamente después de que tenga conocimiento de la falla o deficiencia, podrá indicar al CONSUMIDOR en donde se encuentra la agencia distribuidora de la marca más cercana, para hacer efectiva la garantía por conducto de ésta, si procede, debiendo acreditar con la factura correspondiente la reparación efectuada, con el objeto, de no hacer ningún cargo por ello al CONSUMIDOR. Con el objeto de dar cumplimiento a las obligaciones que ésta cláusula le impone, El PROVEEDOR señala como teléfonos de atención al CONSUMIDOR los que aparecen en éste contrato. Para los efectos de la atención y resolución de quejas y reclamaciones, estas deberán ser presentadas dentro de días y horas hábiles, que son los detallados en la parte superior del presente, ante la Gerencia de Servicio ubicada en las instalaciones del mismo PROVEEDOR, detallando en forma expresa la causa o motivo de la reclamación; la Gerencia de Servicio procederá a analizar la queja y resolverá lo conducente dentro de un lapso de tres días hábiles, procediendo a reparar el vehículo o manifestando las causas de improcedencia de la reclamación, en forma escrita.
                            </p>
                            <h6 align="center"><strong>DE LA INFORMACIÓN Y PUBLICIDAD</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>22.-</b> El PROVEEDOR se obliga a observar en todo momento lo dispuesto por los capítulos III y IV de la Ley Federal de Protección al Consumidor, en cuanto a la información, publicidad, promociones y ofertas.
                                <br>
                                <b>23.-</b> El CONSUMIDOR (Si&nbsp;<input class="camposContrato" type="radio" value="1" name="ceder" style="transform: scale(1.5);" <?= ((isset($condicionalInfo)) ? (($condicionalInfo == "1") ? 'checked' : '') : '') ?> >
                                &nbsp;&nbsp;No&nbsp;<input class="camposContrato" type="radio" value="0" name="ceder" style="transform: scale(1.5);" <?= (isset($condicionalInfo)) ? (($condicionalInfo == "0") ? 'checked' : '') : 'checked' ?> >)&nbsp;&nbsp;
                                acepta que el PROVEEDOR ceda o transmita a tercero, con fines mercadotécnicos o publicitarios, la información proporcionada por él con motivo del presente contrato, y
                                (Si&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="publicar" <?= (isset($condicionalPublicidad)) ? (($condicionalPublicidad == "1") ? 'checked' : '') : '' ?> >&nbsp;&nbsp;
                                No&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="publicar" <?= (isset($condicionalPublicidad)) ? (($condicionalPublicidad == "0") ? 'checked' : '') : 'checked' ?> >)&nbsp;&nbsp;
                                acepta que el PROVEEDOR le envíe publicidad sobre bienes y servicios, firmando en éste espacio.
                                <br>
                            </p>

                            <!-- Firma del consumidor de aceptacion -->
                            <br>
                            <p align="center">
                                <?php if (isset($firmaCliente)): ?>
                                    <?php if ($firmaCliente != ""): ?>
                                        <img class='marcoImg' src='<?= base_url() ?><?php if(isset($firmaCliente)) {if($firmaCliente != "") echo $firmaCliente; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>' id='' style='width:130px;height:40px;'>
                                    <?php else: ?>
                                        <input type="hidden" id="rutaFirmaConsumidor_2" name="rutaFirmaConsumidor_2" value="">
                                        <img class="marcoImg" src="" id="firmaFirmaConsumi_2">

                                        <br>
                                        <a class="cuadroFirma firmaCliente btn btn-primary" data-value="El_Consumidor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <input type="hidden" id="rutaFirmaConsumidor_2" name="rutaFirmaConsumidor_2" value="">
                                    <img class="marcoImg" src="" id="firmaFirmaConsumi_2">

                                    <br>
                                    <a class="cuadroFirma firmaCliente btn btn-primary" data-value="El_Consumidor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif; ?>

                                <br>
                                <span class="error" id ="error_firma_5"></span>
                                <br>
                                Firma o rubrica de autorización del  consumidor
                            </p>

                            <h6 align="center"><strong>DE LA INTERPRETACIÓN Y CUMPLIMIENTO</strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                <b>24.-</b> La Procuraduría Federal del Consumidor, es competente para conocer y resolver en la vía administrativa de cualquier controversia que se suscite sobre la interpretación o
                                cumplimiento del presente contrato, por lo que las partes están de acuerdo en someterse a ella en términos de ley, para resolver sobre la interpretación o cumplimiento de los términos
                                del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-174-SCFI-2007, Prácticas Comerciales- Elementos de Información
                                para la Prestación de Servicios en General y cualquier otra disposición aplicable. Sin perjuicio de lo anterior en caso de persistir la inconformidad, las partes se someten a la
                                jurisdicción de los tribunales competentes del domicilio del PROVEEDOR, renunciando en forma expresa a cualquier otra jurisdicción o al fuero que pudiera corresponderles en razón de sus
                                domicilios presente o futuros o por cualquier otra razón.
                            </p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6" align="center">
                            EL DISTRIBUIDOR (NOMBRE Y FIRMA).
                            <br>
                            <?php if (isset($firmaDistribuidor)): ?>
                                <?php if ($firmaDistribuidor != ""): ?>
                                    <img class='marcoImg' src='<?= base_url() ?><?php if(isset($firmaDistribuidor)) {if($firmaDistribuidor != "") echo $firmaDistribuidor; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>' id='' style='width:130px;height:40px;'>

                                    <br>
                                    <input type="text" class="input_field camposContrato" name="nombreDistribuidor_3" style="width:100%;" value="<?php if(isset($nombreDistribuidor)) echo $nombreDistribuidor; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaFirmaDistribuidor_3" name="rutaFirmaDistribuidor_3" value="">
                                    <img class="marcoImg" src="" id="firmaFirmaDistribuidor_3">
                                    <br>
                                    <input type="text" id="nombreDistribuidor_3" class="input_field asesorname" name="nombreDistribuidor_3" style="width:100%;" value="<?php if(isset($nombreDistribuidor)) echo $nombreDistribuidor; ?>">

                                    <br>
                                    <a class="cuadroFirma btn btn-primary" data-value="Distribuidor_3" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaFirmaDistribuidor_3" name="rutaFirmaDistribuidor_3" value="">
                                <img class="marcoImg" src="" id="firmaFirmaDistribuidor_3">
                                <br>
                                <input type="text" id="nombreDistribuidor_3" class="input_field asesorname" name="nombreDistribuidor_3" style="width:100%;" value="<?php if(isset($nombreDistribuidor)) echo $nombreDistribuidor; ?>">

                                <br>
                                <a class="cuadroFirma btn btn-primary" data-value="Distribuidor_3" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif; ?>

                            <br>
                            <span class="error" id ="error_firma_6"></span>
                        </div>

                        <div class="col-md-6" align="center">
                            EL CLIENTE (NOMBRE Y FIRMA)
                            <br>
                            <?php if (isset($firmaConsumidor)): ?>
                                <?php if ($firmaConsumidor != ""): ?>
                                    <img class='marcoImg' src='<?= base_url() ?><?php if(isset($firmaConsumidor)) {if($firmaConsumidor != "") echo $firmaConsumidor; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>' id='' style='width:130px;height:40px;'>
                                    <input type="hidden" class="input_field camposContrato" name="nombreConsumidor_3" style="width:100%;" value="<?php if(isset($nombreConsumidor)) echo $nombreConsumidor; ?>">

                                    <br>
                                    <input type="text" class="input_field camposContrato" name="nombreConsumidor_3" style="width:100%;" value="<?php if(isset($nombreConsumidor)) echo $nombreConsumidor; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaFirmaConsumidor_3" name="rutaFirmaConsumidor_3" value="">
                                    <img class="marcoImg" src="" id="firmaFirmaConsumidor_3" alt="">
                                    <br>
                                    <input type="text" class="input_field camposContrato" name="nombreConsumidor_3" style="width:100%;" value="<?php if(isset($nombreConsumidor)) echo $nombreConsumidor; ?>">

                                    <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_3" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaFirmaConsumidor_3" name="rutaFirmaConsumidor_3" value="">
                                <img class="marcoImg" src="" id="firmaFirmaConsumidor_3" alt="">
                                <br>
                                <input type="text" class="input_field camposContrato nombreCliente" name="nombreConsumidor_3" style="width:100%;" value="<?php if(isset($nombreConsumidor)) echo $nombreConsumidor; ?>">

                                <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_3" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif; ?>
                            
                            <br>
                            <span class="error" id ="error_firma_7"></span>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-sm-12" align="center">
                            <h6 align="center"><strong>AVISO DE PRIVACIDAD </strong></h6>
                            <p align="justify" style="text-align: justify; font-size:11px;">
                                EL Aviso de Privacidad Integral podrá ser consultado en nuestra página en internet <strong><?=SUCURSAL_WEB?>/Privacidad/</strong>, o lo puede
                                solicitar al correo electrónico <strong>wflores@fordplasencia.com</strong>, u obtener personalmente en el Área de Atención a la Privacidad,
                                en nuestras instalaciones.<br>
                                <input class="camposContrato" type="checkbox" value="1" name="aceptar" <?= ((isset($aceptaTerminos)) ? (($aceptaTerminos == '1') ? 'checked' : '') : '') ?>>&nbsp;&nbsp;
                                Al marcar el recuadro precedente y remitir mis datos, otorgo mi consentimiento para que mis datos personales sean tratados conforme a lo señalado en el Aviso de Privacidad.
                                <br>
                                <span class="error" id ="error_aceptar"></span>
                                <br>
                                Este contrato fue registrado ante la Procuraduría Federal del Consumidor. Registro público de contratos de adhesión y aprobado e inscrito con el
                                número <input type="text" class="input_field camposContrato" name="numeroR" value="<?= ((isset($noRegistro)) ? $noRegistro : '84-2019') ?>" style="width:3cm;" disabled>, Expediente No. <input type="text" class="input_field camposContrato" name="expediente" value="<?= ((isset($noExpediente)) ? $noExpediente : 'PFC.B.E.7-000169-2019') ?>" style="width:3cm;" disabled>,
                                de fecha <input type="number" class="input_field camposContrato" name="diaN" value="<?= ((isset($fechDia)) ? $fechDia : '09') ?>" style="width:2cm;" disabled> de <input type="text" class="input_field camposContrato" name="mesN" value="<?= ((isset($fechMes)) ? $fechMes : 'Enero') ?>" style="width:2cm;" disabled> de
                                <input type="number" class="input_field camposContrato" name="anoN" value="<?= ((isset($fechAnio)) ? $fechAnio : '2019') ?>" style="width:3cm;" disabled>. <br>
                                Cualquier variación del presente contrato en perjuicio de EL CONSUMIDOR, frente al contrato de adhesión registrado, se tendrá por no puesta. <br><br>
                                <strong>“DESEO ADHERIRME AL CONTRATO TIPO DE REPARACION Y MANTENIMIENTO DE VEHICULOS FORD DE PROFECO”</strong>
                            </p>

                            <br><br>
                            <p align="justify" style="text-align: justify; font-size:14px;font-weight: bold;">
                                <input class="camposContrato" type="checkbox" value="1" name="terminos_contrato" <?php if(isset($terminos_contrato)) if($terminos_contrato == "1") echo "checked"; ?>>&nbsp;&nbsp;Acepto Términos y Condiciones del Contrato de Adhesión.
                                <br>
                                <span class="error" id ="error_terminos_contrato"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" align="center">
            <div class="col-md-12"  align="center">
                <br>
                <i class="fas fa-spinner cargaIcono"></i>
                <span class="error" id ="formulario_error"></span>

                <br>
                <input type="button" class="btn btn-success" name="envio_formulario" id="envio_formulario" value="Guardar diagnóstico">
                
                <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="<?= ((isset($tipo_form)) ? $tipo_form : '1') ?>">
                <input type="hidden" name="asesorLogin" id="asesorLogin" value="<?php if(set_value('asesorLogin') != "") echo set_value('asesorLogin'); elseif (isset($asesor)) echo $asesor;?>">
                <input type="hidden" name="completoFormulario" id="completoFormulario" value="<?php if(isset($completoFormulario)) echo $completoFormulario; else echo "0"; ?>">
                <input type="hidden" name="panelOrigen" id="panelOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                <input type="hidden" name="" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
                <input type="hidden" name="" id="formularioHoja" value="OrdenServicio">
                <input type="hidden" name="idServicio" id="" value="<?php if(isset($idServicio)) echo $idServicio; ?>">
            </div>
        </div>
        <br>
    </form>
</div>

<!-- Modal para la firma del quien elaboro el diagnóstico -->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                    <!-- <p id="limpiar">limpiar canvas</p> -->
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Formulario de superusuario -->
<div class="modal fade " id="superUsuario" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Ingreso.</h5>
            </div>
            <div class="modal-body" style="font-size:12px;">
                <label for="" style="font-size: 14px;font-weight: initial;">Usuario:</label>
                <br>
                <input type="text" class="input_field" type="text" id="superUsuarioName" value="" style="width:100%;">
                <br><br>
                <label for="" style="font-size: 14px;font-weight: initial;">Password:</label>
                <br>
                <input type="password" class="input_field" type="text" id="superUsuarioPass" value="" style="width:100%;">
                <br><br><br>
                <h4 class="error" style="font-size: 16px;font-weight: initial;" align="center" id="superUsuarioError">:</h4>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="superUsuarioEnvio">Validar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para cargar video evidencia -->
<div class="modal fade" id="videoEvidencia" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document" style="z-index: 2000;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Carga de video</h4>
            </div>
            <div class="modal-body">
                <br>
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <br>
                <form method="post" id="formularioTempVideo" action="" autocomplete="on" enctype="multipart/form-data">
                    <div class="row" style="font-size:14px;">
                        <div class="col-md-3">
                            <br><br>
                            <label style="font-size:12px;">Cita</label>
                            <input type="text" class="input_field" id="idCitaVideo" name="idCitaVideo" value="<?php if(isset($id_cita)) echo $id_cita; ?>" ><br>
                            <label for="" class="error" id="errorIdCitaVideo"></label>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <div class="row">
                                <br>
                                <input type='radio' class='BigRadio' name='tipoEnvioEvidencia' value="videos" checked>
                                <label style="font-size:12px;margin-top:10px;">Video</label>
                            </div>
                            <br>
                            <div class="row">
                                <br>
                                <input type='radio' class='BigRadio' name='tipoEnvioEvidencia' value="imagenes">
                                <label style="font-size:12px;margin-top:10px;">Imagen</label>
                            </div>
                        </div>
                        <div class="col-md-4" align="right">
                            <br><br>
                            <input type="file" id="uploadfilesVideo" name="uploadfilesVideo" accept="video/*" style="width:100%;">
                            <br>
                            <!-- <input type="button" value="Subir" id='upload'> -->
                            <label for="" class="error" id="errorArchivoVideo"></label>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-size:12px;margin-top:10px;">Comentario:</label>
                            <textarea type="text" class="input_field_lg" id="comentarioVideo" name="comentarioVideo"style="width:100%;"><?php echo  set_value('comentarioVideo');?></textarea>
                        </div>
                    </div>
                </form>
            </div>

            <br>
            <div class="row">
                <div class="col-md-12"  align="center">
                    <i class="fas fa-spinner cargaIcono"></i>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" id="btnCerrarVideo">Cancelar</button>
                <button type="button" class="btn btn-success" id="enviarVideo">Guardar</button>
            </div>
            <br><br>
        </div>
    </div>
</div>
