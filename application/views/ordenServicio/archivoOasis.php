<div class="container">
    <form name="" id="formulario" method="post" action="<?=base_url()."ordenServicio/ordenServicio/validateModalOasis"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso concluido con exito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Ocurrio un error durante el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <h3 align="center" style="color:#340f7b;">
            Archivo Oasis Ford
        </h3>
        <div class="row">
            <div class="col-sm-6">
                <label for="" style="font-size: 13px;font-weight: inherit;">No. de Orden:</label>
                <br>
                <input type="text" name="" class="form-control" value="<?php if(isset($idOrden)) echo $idOrden; else echo "0"; ?>" disabled>
                <?php echo form_error('idOrden', '<br><span class="error">', '</span>'); ?>
            </div>

            <div class="col-sm-6" style="margin-top: 23px;">
                <input type="file" class="btn btn-default" name="archivo[]" value="" multiple>
                <?php echo form_error('archivo', '<br><span class="error">', '</span>'); ?>
            </div>
            <input type="hidden" name="tempFile" value="<?php if(isset($archivoOasisL)) echo $archivoOasisL;?>">
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" align="center">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="idOrden" id="" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                <?php if ($existe == 1): ?>
                    <input type="submit" class="btn btn-success" id="guardarArchivo" value="<?php if (isset($archivoOasisL)) if($archivoOasisL != "") echo "Actualizar Archivo"; else echo "Guardar Archivo"; else echo "Guardar Archivo";?>">
                <?php endif; ?>
                <br>
                <label class="labelUsuarioError error" style="font-size:14px; display:none;">A ocurrido un error al intentar guardar la información, favor de verificar los campos!!</label>
                <br>
            </div>
            <div class="col-sm-4"></div>
        </div>
    </form>

    <?php if ($existe == 0): ?>
        <br>
        <h4 class="error" align="center" style="text-align:center;">No existe un diagnóstico para esta orden</h4>
        <br>
    <?php elseif ($existe == 3): ?>
        <br>
        <h4 class="error" align="center" style="text-align:center;">No existe la voz cliente para ese documento</h4>
        <br>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10" align="center">
            <?php if (isset($archivoOasisL)): ?>
                <?php if ($archivoOasisL != ""): ?>
                    <div class="alert alert-primary" align="center">
                        <h5>Ya existen un archivos para esta orden.</h5>
                        <?php for ($i = 0; $i < count($archivos); $i++): ?>
                            <?php if ($archivos[$i] != ""): ?>
                                <a href="<?php echo base_url().$archivos[$i]; ?>" class="btn btn-info" target="_blank">Archivo (<?php echo $i+1; ?>)</a>
                            <?php endif; ?>
                        <?php endfor; ?>

                        <!-- <a href="<?php echo base_url().$archivoOasisL; ?>" class="btn btn-info btn-block" target="_blank">Archivo Oasis Ford</a> -->
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>
