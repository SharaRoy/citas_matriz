<div class="" style="margin:30px;">
    <form name="" class="formulario" id="formulario" method="post" action="<?=base_url()."ordenServicio/ordenServicio/validateView"?>" autocomplete="on" enctype="multipart/form-data">

        <!-- Alerta para proceso del registro -->
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title" align="center">DIAGNÓSTICO Y POSIBLES CONSECUENCIAS</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <p>
                                    <b>No.Orden Servicio:</b>
                                    <input type="text" class="input_field" id="noOrden" onblur="precarga()" name="orderService" value="<?php if(set_value('orderService') != "") echo set_value('orderService'); else echo $idCita;?>"><br>
                                    <label for="" id="errorConsultaServer" class="error"></label>
                                    <?php echo form_error('orderService', '<br><span class="error">', '</span>'); ?>
                                </p>

                                <br>
                                Folio Intelisis:&nbsp;
                                <label style="color:darkblue;">
                                    <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                                </label>
                            </div>
                            <div class="col-md-5">
                                <p align="justify" style="text-align: justify;color:red;font-size:9px;">
                                    <strong>*</strong> Para un diagnostico sin cita, dejar el este campo en "0".
                                </p>
                            </div>
                            <div class="col-md-4" align="right">
                                <?php if (isset($pOrigen)): ?>
                                    <?php if ($pOrigen == "ASE"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($pOrigen == "TEC"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($pOrigen == "JDT"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                            Regresar
                                        </button>
                                    <?php else: ?>
                                        <!--<button type="button" class="btn btn-dark" >
                                            Sesión Expirada
                                        </button>-->
                                    <?php endif; ?>
                                <?php else: ?>
                                    <!--<button type="button" class="btn btn-dark" >
                                        Sesión Expirada
                                    </button>-->
                                <?php endif; ?>
                            </div>
                        </div>

                        <br>
                        <p align="justify" style="text-align: justify;">
                            Para elaborar el diagnóstico de <input type="text" name="vehiculo" class="input_field" style="width:5cm;" value="<?= set_value('vehiculo');?>"> del vehículo,
                            autorizo en forma expresa a desarmar las partes indispensables del mismo y sus componentes, a efecto de obtener un diagnóstico adecuado de él,
                            en el entendido de que el vehículo se me devolverá en las mismas condiciones en que fuera entregado, excepto en caso de que como consecuencia
                            inevitable resulte imposible o ineficaz para su funcionamiento así entregarlo, por causa no imputable al proveedor, lo cual si
                            (Si&nbsp;<input type="radio" style="transform: scale(1.5);" value="Si" name="aceptoTermino" <?php echo set_checkbox('aceptoTermino', 'Si'); ?>>&nbsp;&nbsp;
                            No&nbsp;<input type="radio" style="transform: scale(1.5);" value="No" name="aceptoTermino" <?php echo set_checkbox('aceptoTermino', 'No'); ?>>)&nbsp;&nbsp;acepto.
                            <!-- <input type="radio" style="transform: scale(1.5);" value="si" ng-model="usuario" />/ no <input type="radio" style="transform: scale(1.5);" value="no" ng-model="usuario" /> {{usuario}}   -->
                            En todo caso me obligo a pagar el importe del diagnóstico y los trabajos necesarios para realizarlo en caso de no autorizar la
                            reparación en términos del contrato y acepto que el diagnóstico se realice en un plazo de <input type="text" ng-model="dias" name="dias" class="input_field" style="width:5cm;" value="<?= set_value('dias');?>">
                            días hábiles a partir de la firma del presente.
                        </p>
                    </div>

                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-3" align="center">
                                Nombre y firma de quien elabora el diagnóstico.<br>
                                <input type="hidden" id="rutaFirma" name="rutaFirmaDiagnostico" value="<?= set_value('rutaFirmaDiagnostico');?>">
                                <img class="marcoImg" src="<?= set_value('rutaFirmaDiagnostico');?>" id="firmaImg" alt="">
                                <br><br>
                                <input type="text" id="nombreDiagnostico" onblur='llenadoAsesor()' class="input_field asesorname" name="nombreDiagnostico" style="width:100%;" value="<?= set_value('nombreDiagnostico');?>">
                                <?php echo form_error('rutaFirmaDiagnostico', '<span class="error">', '</span>'); ?><br>
                                <?php echo form_error('nombreDiagnostico', '<span class="error">', '</span>'); ?><br>
                            </div>
                            <div class="col-md-3" align="center">
                                Fecha de elaboración del diagnóstico
                                <div class="container">
                                    <div class="row">
                                        <div class='col-md-2'>
                                            <input class="input_field" type="date" id="start" name="trip_start" value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" min="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" max="<?php if(isset($limiteAno)) echo $limiteAno; ?>-12-31">
                                            <br><?php echo form_error('trip_start', '<span class="error">', '</span>'); ?><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" align="center">
                                Nombre y firma del consumidor aceptando diagnóstico.<br>
                                <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?= set_value('rutaFirmaAcepta');?>">
                                <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">
                                <br><br>
                                <input type="text" onblur='llenadoCliente()' id="consumidor1Nombre" class="input_field nombreCliente" name="consumidor1Nombre" style="width:100%;" value="<?= set_value('consumidor1Nombre');?>">
                                <?php echo form_error('rutaFirmaAcepta', '<span class="error">', '</span>'); ?><br>
                                <?php echo form_error('consumidor1Nombre', '<span class="error">', '</span>'); ?><br>
                            </div>
                            <div class="col-md-3" align="right">
                                Fecha aceptación
                                <input class="input_field" type="date" ng-model="datetime1" id="start1" name="trip_start_1" value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" min="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" max="<?php if(isset($limiteAno)) echo $limiteAno; ?>-12-31">
                                <br><?php echo form_error('trip_start_1', '<span class="error">', '</span>'); ?><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3" align="center">
                                <br><br>
                                <a class="cuadroFirma btn btn-primary" data-value="Diagnostico" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            </div>
                            <div class="col-md-3" align="center"></div>
                            <div class="col-md-3" align="center">
                                <br><br>
                                <a class="cuadroFirma btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            </div>
                            <div class="col-md-3" align="right"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" align="center">
                            <strong>INSPECCIÓN VISUAL E INVENTARIO EN RECEPCIÓN</strong>
                        </div>
                        
                        <div class="panel-body">
                            <div class="col-md-12 table-responsive" align="center">
                                <table width="100%" class="table table-bordered ">
                                    <tr>
                                        <td>
                                            Si&nbsp;<input class="BigRadio" type="radio" value="1" name="danos" <?php echo set_checkbox('danos', '1'); ?> checked>
                                            No&nbsp;<input class="BigRadio" type="radio" value="0" name="danos" <?php echo set_checkbox('danos', '0'); ?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input class="BigRadio" type="radio" id="golpes" name="marcasRadio" checked value="hit">&nbsp;Golpes
                                            &nbsp;&nbsp;<input class="BigRadio" type="radio" id="roto" name="marcasRadio" value="broken">&nbsp;Roto / Estrellado
                                            &nbsp;&nbsp;<input class="BigRadio" type="radio" id="rayones" name="marcasRadio" value="scratch">&nbsp;Rayones
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <canvas id="canvas_3" width="420" height="500">
                                                Su navegador no soporta canvas.
                                            </canvas>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col-md-12 table-responsive" align="center">
                                <br>
                                <a href="#" class="btn btn-default" id="btn-download" download="Diagrama_Diagnostico">
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                    Descargar diagrama
                                </a>

                                <button type="button" class="btn btn-primary" id="resetoeDiagrama" style="margin-left: 20px;">
                                    <i class="fa fa-refresh" aria-hidden="true"></i>
                                    Restaurar diagrama
                                </button>

                                <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car.jpeg'; ?>">
                                <input type="hidden" name="danosMarcas" id="danosMarcas" value="<?= set_value('danosMarcas');?>">
                                <input type="hidden" name="" id="validaClick_1" value="0">
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-12" style="min-width: 600px;">
                                <table width="100%" class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                Interiores <br>
                                                <?php echo form_error('interiores', '<span class="error">', '</span>'); ?>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                Si
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                No
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                (No cuenta)<br>
                                                NC
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Llavero.</td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck" name="interiores[]" value="Llavero.Si" <?php echo set_checkbox('interiores[]', 'Llavero.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck" name="interiores[]" value="Llavero.No" <?php echo set_checkbox('interiores[]', 'Llavero.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="llaveroCheck" name="interiores[]" value="Llavero.NC" <?php echo set_checkbox('interiores[]', 'Llavero.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Seguro rines.</td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.Si" <?php echo set_checkbox('interiores[]', 'SeguroRines.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.No" <?php echo set_checkbox('interiores[]', 'SeguroRines.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.NC" <?php echo set_checkbox('interiores[]', 'SeguroRines.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">Indicadores de falla Activados:</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <table style="width:100%;">
                                                    <tr>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <img src="<?php echo base_url();?>assets/imgs/05.png" style="width:1.2cm;">
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <img src="<?php echo base_url();?>assets/imgs/02.png" style="width:1.2cm;">
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <img src="<?php echo base_url();?>assets/imgs/07.png" style="width:1.2cm;">
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <img src="<?php echo base_url();?>assets/imgs/01.png" style="width:1.2cm;">
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <img src="<?php echo base_url();?>assets/imgs/06.png" style="width:1.2cm;">
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <img src="<?php echo base_url();?>assets/imgs/04.png" style="width:1.2cm;">
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <img src="<?php echo base_url();?>assets/imgs/03.png" style="width:1.2cm;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.Si" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.Si'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.No" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.No'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.NC" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.NC'); ?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.5);" name="interiores[]" value="IndicadorGasolina" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.Si" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.Si'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.No" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.No'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.NC" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.NC'); ?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.5);" name="interiores[]" value="IndicadorMantenimento" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="SistemaABSCheck" name="interiores[]" value="SistemaABS.Si" <?php echo set_checkbox('interiores[]', 'SistemaABS.Si'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="SistemaABSCheck" name="interiores[]" value="SistemaABS.No" <?php echo set_checkbox('interiores[]', 'SistemaABS.No'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="SistemaABSCheck" name="interiores[]" value="SistemaABS.NC" <?php echo set_checkbox('interiores[]', 'SistemaABS.NC'); ?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.5);" name="interiores[]" value="SistemaABS" <?php echo set_checkbox('interiores[]', 'SistemaABS'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.Si" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.Si'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.No" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.No'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.NC" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.NC'); ?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.5);" name="interiores[]" value="IndicadorFrenos" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.Si" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.Si'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.No" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.No'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.NC" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.NC'); ?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.5);" name="interiores[]" value="IndicadorBolsaAire" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.Si" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.Si'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.No" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.No'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.NC" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.NC'); ?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.5);" name="interiores[]" value="IndicadorTPMS" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS'); ?>> -->
                                                        </td>
                                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                                            <input type="checkbox" style="margin-right: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.Si" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.Si'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; margin-right: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.No" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.No'); ?>>
                                                            <input type="checkbox" style="margin-left: 6px; transform: scale(1.5);" class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.NC" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.NC'); ?>>
                                                            <!-- <input type="checkbox" style="transform: scale(1.5);" name="interiores[]" value="IndicadorBateria" <?php echo set_checkbox('interiores[]', 'IndicadorBateria'); ?>> -->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Indicador de falla activados.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="testigos" name="interiores[]" value="IndicadorDeFalla" <?php echo set_checkbox('interiores[]', 'Indicador de falla'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck" name="interiores[]" value="IndicadorDeFalla.Si" <?php echo set_checkbox('interiores[]', 'IndicadorDeFalla.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck" name="interiores[]" value="IndicadorDeFalla.No" <?php echo set_checkbox('interiores[]', 'IndicadorDeFalla.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="IndicadorDeFallaCheck" name="interiores[]" value="IndicadorDeFalla.NC" <?php echo set_checkbox('interiores[]', 'IndicadorDeFalla.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rociadores y Limpiaparabrisas.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="rociador" name="interiores[]" value="Rociadores" <?php echo set_checkbox('interiores[]', 'Rociadores'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck" name="interiores[]" value="Rociadores.Si" <?php echo set_checkbox('interiores[]', 'Rociadores.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck" name="interiores[]" value="Rociadores.No" <?php echo set_checkbox('interiores[]', 'Rociadores.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="RociadoresCheck" name="interiores[]" value="Rociadores.NC" <?php echo set_checkbox('interiores[]', 'Rociadores.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Claxon.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="claxon" name="interiores[]" value="Claxon" <?php echo set_checkbox('interiores[]', 'Claxon'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck" name="interiores[]" value="Claxon.Si" <?php echo set_checkbox('interiores[]', 'Claxon.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck" name="interiores[]" value="Claxon.No" <?php echo set_checkbox('interiores[]', 'Claxon.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ClaxonCheck" name="interiores[]" value="Claxon.NC" <?php echo set_checkbox('interiores[]', 'Claxon.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                Luces
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                Delanteras.
                                            </td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="lucesd" name="interiores[]" value="LucesDelanteras" <?php echo set_checkbox('interiores[]', 'LucesDelanteras'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.Si" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.No" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.NC" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                Traseras.
                                            </td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="lucest" name="interiores[]" value="LucesTraseras" <?php echo set_checkbox('interiores[]', 'LucesTraseras'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.Si" <?php echo set_checkbox('interiores[]', 'LucesTraseras.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.No" <?php echo set_checkbox('interiores[]', 'LucesTraseras.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.NC" <?php echo set_checkbox('interiores[]', 'LucesTraseras.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                Stop.
                                            </td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="lucess" name="interiores[]" value="LucesStop" <?php echo set_checkbox('interiores[]', 'LucesStop'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck" name="interiores[]" value="LucesStop.Si" <?php echo set_checkbox('interiores[]', 'LucesStop.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck" name="interiores[]" value="LucesStop.No" <?php echo set_checkbox('interiores[]', 'LucesStop.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LucesStopCheck" name="interiores[]" value="LucesStop.NC" <?php echo set_checkbox('interiores[]', 'LucesStop.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Radio / Caratulas.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="radio" name="interiores[]" value="Caratulas" <?php echo set_checkbox('interiores[]', 'Caratulas'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck" name="interiores[]" value="Caratulas.Si" <?php echo set_checkbox('interiores[]', 'Caratulas.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck" name="interiores[]" value="Caratulas.No" <?php echo set_checkbox('interiores[]', 'Caratulas.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CaratulasCheck" name="interiores[]" value="Caratulas.NC" <?php echo set_checkbox('interiores[]', 'Caratulas.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Pantallas.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="pantallas" name="interiores[]" value="Pantallas" <?php echo set_checkbox('interiores[]', 'Pantallas'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck" name="interiores[]" value="Pantallas.Si" <?php echo set_checkbox('interiores[]', 'Pantallas.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck" name="interiores[]" value="Pantallas.No" <?php echo set_checkbox('interiores[]', 'Pantallas.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="PantallasCheck" name="interiores[]" value="Pantallas.NC" <?php echo set_checkbox('interiores[]', 'Pantallas.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>A/C</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="aire" name="interiores[]" value="AA" <?php echo set_checkbox('interiores[]', 'AA'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="AACheck" name="interiores[]" value="AA.Si" <?php echo set_checkbox('interiores[]', 'AA.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="AACheck" name="interiores[]" value="AA.No" <?php echo set_checkbox('interiores[]', 'AA.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="AACheck" name="interiores[]" value="AA.NC" <?php echo set_checkbox('interiores[]', 'AA.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Encendedor.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="encendedor" name="interiores[]" value="Encendedor" <?php echo set_checkbox('interiores[]', 'Encendedor'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck" name="interiores[]" value="Encendedor.Si" <?php echo set_checkbox('interiores[]', 'Encendedor.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck" name="interiores[]" value="Encendedor.No" <?php echo set_checkbox('interiores[]', 'Encendedor.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="EncendedorCheck" name="interiores[]" value="Encendedor.NC" <?php echo set_checkbox('interiores[]', 'Encendedor.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Vidrios.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="vidrios" name="interiores[]" value="Vidrios" <?php echo set_checkbox('interiores[]', 'Vidrios'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck" name="interiores[]" value="Vidrios.Si" <?php echo set_checkbox('interiores[]', 'Vidrios.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck" name="interiores[]" value="Vidrios.No" <?php echo set_checkbox('interiores[]', 'Vidrios.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="VidriosCheck" name="interiores[]" value="Vidrios.NC" <?php echo set_checkbox('interiores[]', 'Vidrios.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Espejos.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="espejos" name="interiores[]" value="Espejos" <?php echo set_checkbox('interiores[]', 'Espejos'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck" name="interiores[]" value="Espejos.Si" <?php echo set_checkbox('interiores[]', 'Espejos.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck" name="interiores[]" value="Espejos.No" <?php echo set_checkbox('interiores[]', 'Espejos.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="EspejosCheck" name="interiores[]" value="Espejos.NC" <?php echo set_checkbox('interiores[]', 'Espejos.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Seguros eléctricos.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="segurosE" name="interiores[]" value="SegurosEléctricos" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.Si" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.No" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.NC" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Disco compacto.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="disco" name="interiores[]" value="CD" <?php echo set_checkbox('interiores[]', 'CD'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CDCheck" name="interiores[]" value="CD.Si" <?php echo set_checkbox('interiores[]', 'CD.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CDCheck" name="interiores[]" value="CD.No" <?php echo set_checkbox('interiores[]', 'CD.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CDCheck" name="interiores[]" value="CD.NC" <?php echo set_checkbox('interiores[]', 'CD.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Asientos y vestiduras.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="vestiduras" name="interiores[]" value="Vestiduras" <?php echo set_checkbox('interiores[]', 'Vestiduras'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck" name="interiores[]" value="Vestiduras.Si" <?php echo set_checkbox('interiores[]', 'Vestiduras.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck" name="interiores[]" value="Vestiduras.No" <?php echo set_checkbox('interiores[]', 'Vestiduras.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="VestidurasCheck" name="interiores[]" value="Vestiduras.NC" <?php echo set_checkbox('interiores[]', 'Vestiduras.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tapetes.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="tapetes" name="interiores[]" value="Tapetes" <?php echo set_checkbox('interiores[]', 'Tapetes'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck" name="interiores[]" value="Tapetes.Si" <?php echo set_checkbox('interiores[]', 'Tapetes.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck" name="interiores[]" value="Tapetes.No" <?php echo set_checkbox('interiores[]', 'Tapetes.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="TapetesCheck" name="interiores[]" value="Tapetes.NC" <?php echo set_checkbox('interiores[]', 'Tapetes.NC'); ?>>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" align="center">
                            Cajuela / Exteriores / Documentación
                        </div>

                        <div class="panel-body">
                            <div class="col-md-4">
                                <table class="table table-bordered" style="min-width:300px;">
                                    <thead>
                                        <tr class="headers_table" align="center" style="border:1px solid black;border-style: double;">
                                            <td>
                                                Cajuela <br>
                                                <!-- (Si&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="cajuReq" <?php echo set_checkbox('cajuReq', '1'); ?>>
                                                No&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="cajuReq" <?php echo set_checkbox('cajuReq', '0'); ?>>)<br> -->
                                                <?php echo form_error('cajuela', '<span class="error">', '</span>'); ?>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                Si
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                No
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                No cuenta<br>
                                                (NC)
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Herramienta.</td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck" name="cajuela[]" value="Herramienta.Si" <?php echo set_checkbox('cajuela[]', 'Herramienta.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck" name="cajuela[]" value="Herramienta.No" <?php echo set_checkbox('cajuela[]', 'Herramienta.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="HerramientaCheck" name="cajuela[]" value="Herramienta.NC" <?php echo set_checkbox('cajuela[]', 'Herramienta.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Gato / Llave.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="gato" name="cajuela[]" value="Llave" <?php echo set_checkbox('cajuela[]', 'Llave'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck" name="cajuela[]" value="eLlave.Si" <?php echo set_checkbox('cajuela[]', 'eLlave.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck" name="cajuela[]" value="eLlave.No" <?php echo set_checkbox('cajuela[]', 'eLlave.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="eLlaveCheck" name="cajuela[]" value="eLlave.NC" <?php echo set_checkbox('cajuela[]', 'eLlave.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Reflejantes.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="reflejante" name="cajuela[]" value="Reflejantes" <?php echo set_checkbox('cajuela[]', 'Reflejantes'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.Si" <?php echo set_checkbox('cajuela[]', 'Reflejantes.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.No" <?php echo set_checkbox('cajuela[]', 'Reflejantes.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.NC" <?php echo set_checkbox('cajuela[]', 'Reflejantes.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cables.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="cables" name="cajuela[]" value="Cables" <?php echo set_checkbox('cajuela[]', 'Cables'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck" name="cajuela[]" value="Cables.Si" <?php echo set_checkbox('cajuela[]', 'Cables.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck" name="cajuela[]" value="Cables.No" <?php echo set_checkbox('cajuela[]', 'Cables.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="CablesCheck" name="cajuela[]" value="Cables.NC" <?php echo set_checkbox('cajuela[]', 'Cables.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Extintor.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="extintor" name="cajuela[]" value="Extintor" <?php echo set_checkbox('cajuela[]', 'Extintor'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck" name="cajuela[]" value="Extintor.Si" <?php echo set_checkbox('cajuela[]', 'Extintor.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck" name="cajuela[]" value="Extintor.No" <?php echo set_checkbox('cajuela[]', 'Extintor.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="ExtintorCheck" name="cajuela[]" value="Extintor.NC" <?php echo set_checkbox('cajuela[]', 'Extintor.NC'); ?>>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Llanta Refacción.</td>
                                            <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" ng-model="llanta" name="cajuela[]" value="LlantaRefacción" <?php echo set_checkbox('cajuela[]', 'LlantaRefacción'); ?>>
                                            </td> -->
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.Si" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.Si'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.No" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.No'); ?>>
                                            </td>
                                            <td align="center" style="padding: 4px 0px 3px 0px;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.NC" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.NC'); ?>>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table table-bordered" style="width:100%;">
                                            <thead>
                                                <tr class="headers_table" align="center" style="border:1px solid black;border-style: double;">
                                                    <td style="width:70%;">
                                                        Exteriores
                                                        <!-- (Si&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="exteReq" <?php echo set_checkbox('exteReq', '1'); ?>>
                                                        No&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="exteReq" <?php echo set_checkbox('exteReq', '0'); ?>>)<br> -->
                                                        <?php echo form_error('exteriores', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td align="center" style="width:15%;">
                                                        Si
                                                    </td>
                                                    <td align="center" style="width:15%;">
                                                        No
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Tapones rueda.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" ng-model="tapones" name="exteriores[]" value="TaponesRueda" <?php echo set_checkbox('exteriores[]', 'TaponesRueda'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="TaponesRuedaCheck" name="exteriores[]" value="TaponesRueda.Si" <?php echo set_checkbox('exteriores[]', 'TaponesRueda.Si'); ?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="TaponesRuedaCheck" name="exteriores[]" value="TaponesRueda.No" <?php echo set_checkbox('exteriores[]', 'TaponesRueda.No'); ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Gomas de limpiadores.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" ng-model="gomas" name="exteriores[]" value="Gomas de limpiadores" <?php echo set_checkbox('exteriores[]', 'Gomas de limpiadores'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="GotasCheck" name="exteriores[]" value="Gotas.Si" <?php echo set_checkbox('exteriores[]', 'Gotas.Si'); ?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="GotasCheck" name="exteriores[]" value="Gotas.No" <?php echo set_checkbox('exteriores[]', 'Gotas.No'); ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Antena.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" ng-model="antena" name="exteriores[]" value="Antena" <?php echo set_checkbox('exteriores[]', 'Antena'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="AntenaCheck" name="exteriores[]" value="Antena.Si" <?php echo set_checkbox('exteriores[]', 'Antena.Si'); ?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="AntenaCheck" name="exteriores[]" value="Antena.No" <?php echo set_checkbox('exteriores[]', 'Antena.No'); ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tapón de gasolina.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" ng-model="gasolina" name="exteriores[]" value="Tapón de gasolina" <?php echo set_checkbox('exteriores[]', 'Tapón de gasolina'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="TaponGasolinaCheck" name="exteriores[]" value="TaponGasolina.Si" <?php echo set_checkbox('exteriores[]', 'TaponGasolina.Si'); ?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="TaponGasolinaCheck" name="exteriores[]" value="TaponGasolina.No" <?php echo set_checkbox('exteriores[]', 'TaponGasolina.No'); ?>>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-6">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr class="headers_table" align="center" style="border:1px solid black;border-style: double;">
                                                    <td>
                                                        Documentación
                                                        <!-- (Si&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="docReq" <?php echo set_checkbox('docReq', '1'); ?>>
                                                        No&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="docReq" <?php echo set_checkbox('docReq', '0'); ?>>)<br> -->
                                                        <?php echo form_error('documentacion', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        Si
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        No
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Póliza garantía/Manual prop.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" ng-model="poliza" name="documentacion[]" value="Póliza garantía" <?php echo set_checkbox('documentacion[]', 'Póliza garantía'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="PolizaGarantiaCheck" name="documentacion[]" value="PolizaGarantia.Si" <?php echo set_checkbox('documentacion[]', 'PolizaGarantia.Si'); ?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="PolizaGarantiaCheck" name="documentacion[]" value="PolizaGarantia.No" <?php echo set_checkbox('documentacion[]', 'PolizaGarantia.No'); ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Seguro de Rines.</td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesDocCheck" name="documentacion[]" value="SeguroRinesDoc.Si" <?php echo set_checkbox('documentacion[]', 'SeguroRinesDoc.Si'); ?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="SeguroRinesDocCheck" name="documentacion[]" value="SeguroRinesDoc.No" <?php echo set_checkbox('documentacion[]', 'SeguroRinesDoc.No'); ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Certificado verificación.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" ng-model="verificacion" name="documentacion[]" value="Certificado verificación" <?php echo set_checkbox('documentacion[]', 'Certificado verificación'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="cVerificacionCheck" name="documentacion[]" value="cVerificacion.Si" <?php echo set_checkbox('documentacion[]', 'cVerificacion.Si'); ?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="cVerificacionCheck" name="documentacion[]" value="cVerificacion.No" <?php echo set_checkbox('documentacion[]', 'cVerificacion.No'); ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tarjeta de circulación.</td>
                                                    <!-- <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" ng-model="circulacion" name="documentacion[]" value="Tarjeta de circulación" <?php echo set_checkbox('documentacion[]', 'Tarjeta de circulación'); ?>>
                                                    </td> -->
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="tCirculacionCheck" name="documentacion[]" value="tCirculacion.Si" <?php echo set_checkbox('documentacion[]', 'tCirculacion.Si'); ?>>
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5);" class="tCirculacionCheck" name="documentacion[]" value="tCirculacion.No" <?php echo set_checkbox('documentacion[]', 'tCirculacion.No'); ?>>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td align="center" style="width:40%">
                                                        <img src="<?php echo base_url().'assets/imgs/gas.jpeg'; ?>" style="width:50px;" alt="">
                                                    </td>
                                                    <td style="margin-left:5px;" colspan="2">
                                                        Nivel de Gasolina:&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <!-- </td> -->
                                                    <!-- <td align="center" style="margin-left:5px;width:30%;"> -->
                                                        <input type="number" name="nivelGasolina_1" class="input_field" style="width:1.5cm;" min="0" value="<?= set_value('nivelGasolina_1');?>">
                                                        <!-- <label for="" style="font-size:16px!important;"> <strong>/</strong> </label> -->
                                                        <img src="<?php echo base_url().'assets/imgs/diagonal.jpg'; ?>" style="width:20px; height:20px;" alt="">
                                                        <input type="number" name="nivelGasolina_2" class="input_field" style="width:1.5cm;" min="0" value="<?= set_value('nivelGasolina_2');?>">
                                                        <br>
                                                        <?php echo form_error('nivelGasolina_1', '<span class="error">', '</span>'); ?>
                                                        <?php echo form_error('nivelGasolina_2', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <!-- <tr>
                                                    <td colspan="2" align="center">
                                                        <label>Nivel de gasolina.</label>
                                                        <br>
                                                        <canvas id="nivel_gasolina" width="250" height="150">
                                                            Su navegador no soporta canvas
                                                        </canvas>
                                                        <br>
                                                        <div id="medidor" style="width:300px;"></div>
                                                        <input type="hidden" name="nivelGasolina" id="nivelGasolina" value="<?= set_value('costo');?>">
                                                        <br>
                                                        <?php echo form_error('nivelGasolina', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr> -->
                                                <tr>
                                                    <td>¿Deja artículos personales?</td>
                                                    <td>
                                                        (Si&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="articulos" <?php echo set_checkbox('articulos', '1'); ?>>
                                                        No&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="articulos" <?php echo set_checkbox('articulos', '0'); ?>>)<br>
                                                        <?php echo form_error('articulos', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>¿Cuales?</td>
                                                    <td>
                                                        <input type="text" ng-model="cuales" class="input_field" name="cualesArticulos"  value="<?php echo  set_value('cualesArticulos');?>">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>¿Desea reportar algo más?</td>
                                                    <td>
                                                        <input type="text" ng-model="reportar" class="input_field" name="reporte"  value="<?php echo  set_value('reporte');?>">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body" style="width: 99%;">
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <h2>IMPORTANTE</h2><br>
                                    <p align="justify" style="text-align: justify;">
                                        Favor de recoger su unidad en un plazo no mayor a 48 Hrs., contados a partir de la fecha en que su
                                        asesor le informe que la unidad esta lista, pasado este plazo deberá cubrir el amacenaje por guarda de
                                        vehículo conforme al contrato.<br>
                                        Manifiesto bajo protesta que soy el dueño del vehículo, o tengo orden y/o autorización de él para
                                        utilizarlo y ordenar ésta reparación, en su representación, autorizando en su nombre y en el propio
                                        la realización de los trabajos descritos, así como la colocación de las piezas, refacciones, repuestos
                                        y materiales e insumos necesarios para efectuarlos, comprometiéndome en su nombre y en el propio a pagar
                                        el importe de la reparación; también manifiesto mi conformidad con el inventario realizado y que consta
                                        en el ejemplar del presente que recibo; en cualquier caso que las partes acuerden que el vehículo vaya a
                                        ser recogido o entregado por personal del PROVEEDOR en el domicilio del CONSUMIDOR, ello solo será mediante
                                        previo acuerdo u orden del CONSUMIDOR por escrito y debidamente aceptada por el PROVEEDOR, con un costo de
                                        $<input type="text" ng-model="costo" class="input_field" name="costo" style="width:3cm;" value="<?= set_value('costo');?>">
                                        <?php echo form_error('costo', '<span class="error">', '</span>'); ?>
                                        (MONEDA NACIONAL);
                                        será en todo caso obligación del personal del PROVEEDOR identificarse plenamente ante el CONSUMIDOR como tal.<br>
                                        Finalmente manifiesto mi conformidad con los términos y condiciones previstas en el contrato inscrito al reverso,
                                        el cual manifiesto que he leído, obligándome en lo personal y en nombre del propietario del automóvil en
                                        los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día
                                        <input type="number" class="input_field" max="31" min="1" name="diaValue" style="width:3cm;" value="<?php if(isset($dia)) echo $dia; ?>">
                                        <?php echo form_error('diaValue', '<span class="error">', '</span>'); ?>
                                        de
                                        <input type="text" class="input_field" name="mesValue" style="width:3cm;" value="<?php if(isset($mes)) echo $mes; ?>">
                                        <?php echo form_error('mesValue', '<span class="error">', '</span>'); ?>
                                        de
                                        <input type="number"  min="<?php if(isset($limiteAno)) echo $limiteAno; ?>" max="<?php if(isset($limiteAno)) echo $limiteAno; ?>" class="input_field" name="anioValue" style="width:3cm;" value="<?php if(isset($limiteAno)) echo $limiteAno; ?>">
                                        <?php echo form_error('anioValue', '<span class="error">', '</span>'); ?>
                                        .
                                    </p>
                                </div>
                            </div>

                            <br><br>
                                <div class="row">
                                    <div class="col-md-12" align="right">
                                        <a class="cuadroFirma btn btn-dark" data-target="#videoEvidencia" data-toggle="modal" style="color:white;">
                                            Subir video
                                        </a>
                                        <br>
                                        <label for="" class="" id="errorVideo"></label>
                                    </div>
                                </div>
                            <br><br>
                            <div class="row">
                                <div class="col-md-6" align="center">
                                    Nombre y firma del asesor de servicio.
                                    <br>
                                    <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                                    <img class="marcoImg" src="<?= set_value('rutaFirmaAsesor');?>" id="firmaFirmaAsesor" alt="">
                                    <br><br>
                                    <input type="text" id="asesorNombre" class="input_field asesorname" name="asesorNombre" style="width:90%;" value="<?= set_value('asesorNombre');?>">

                                    <br><br>
                                    <?php echo form_error('rutaFirmaAsesor', '<span class="error">', '</span>'); ?><br>
                                    <?php echo form_error('asesorNombre', '<span class="error">', '</span>'); ?><br>
                                    <br><br>
                                </div>

                                <div class="col-md-6" align="center">
                                    Firma del consumidor<br>
                                    ACEPTA PRESUPUESTO E INVENTARIO<br>
                                    <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?= set_value('rutaFirmaConsumidor');?>">
                                    <img class="marcoImg" src="<?= set_value('rutaFirmaConsumidor');?>" id="firmaFirmaConsumidor" alt="">
                                    <br>
                                    <input type="text" id="nombreConsumidor2" class="input_field nombreCliente" name="nombreConsumidor2" style="width:90%;" value="<?= set_value('nombreConsumidor2');?>">

                                    <br><br>
                                    <?php echo form_error('rutaFirmaConsumidor', '<span class="error">', '</span>'); ?><br>
                                    <?php echo form_error('nombreConsumidor2', '<span class="error">', '</span>'); ?><br>
                                    <br><br>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6" align="center">
                                    <a class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                </div>
                                <div class="col-md-6" align="center">
                                    <a class="cuadroFirma btn btn-primary" data-value="Consumidor_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" align="center">
                            <strong>CONTRATO DE ADHESIÓN</strong>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <table style="width:100%">
                                    <tr>
                                      <td>
                                          <p align="justify" style="text-align: justify; font-size:12px;">
                                                <?=SUCURSAL?>, S.A. DE C.V. <br>
                                                <?=$this->config->item('encabezado_contrato')?>
                                          </p>
                                      </td>
                                      <td>
                                        <p align="justify" style="text-align: justify; font-size:12px;">
                                            Folio No.:
                                            <input type="text" class="input_field" name="folio" value="<?php if(set_value('folio') != "") echo set_value('folio'); else echo $idCita;?>" style="width:3cm;"> <br>
                                            <?php echo form_error('folio', '<span class="error">', '</span>'); ?><br>
                                            Fecha:
                                            <input type="date" class="input_field" name="fecha" value="<?php echo date("Y-m-d"); ?>" min="<?php if(isset($limiteAno)) echo $limiteAno; ?>-01-01" max="<?php if(isset($limiteAno)) echo $limiteAno; ?>-12-31" style="width:3cm;"> <br>
                                            <?php echo form_error('fecha', '<span class="error">', '</span>'); ?><br>
                                            Hora:
                                            <input type="text" class="input_field" name="hora" value="<?php echo date("H:i"); ?>" style="width:3cm;"> <br>
                                            <?php echo form_error('hora', '<span class="error">', '</span>'); ?><br>
                                        </p>
                                      </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <br>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    CONTRATO DE PRESTACIÓN DE SERVICIO DE REPARACIÓN Y MANTENIMIENTO DE VEHÍCULOS QUE CELEBRAN POR UNA PARTE
                                    <input type="text" class="input_field" name="empresa" id="txtEmpresa" value="PLASENCIA MOTORS DE GUADALAJARA " style="width:7cm;">, S.A. DE C.V.,
                                    DISTRIBUIDOR AUTORIZADO FORD Y POR LA OTRA EL CONSUMIDOR CUYO NOMBRE SE SEÑALA EN EL ANVERSO DEL PRESENTE,
                                    A QUIENES EN LO SUCESIVO Y PARA EFECTO DEL PRESENTE CONTRATO SE LE DENOMINARA “EL PROVEEDOR” Y “EL CONSUMIDOR”,
                                    RESPECTIVAMENTE.
                                </p>
                                <h6 align="center"><strong>DECLARACIONES</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    Las partes denominadas CONSUMIDOR y PROVEEDOR, se reconocen las personalidades con las cuales se ostentan y que
                                    se encuentran especificadas en este documento (orden de servicio y contrato), por lo cual, están dispuestas a
                                    sujetarse a las condiciones que se establecen en las siguientes:
                                </p>
                                <h6 align="center"><strong>CLÁUSULAS</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>1.- </b>Objeto: El PROVEEDOR realizará todas las operaciones de mantenimiento, reparaciones y composturas, solicitadas
                                    por el CONSUMIDOR que suscribe el presente contrato, a las que se someterá el vehículo que al anverso se detalla, para obtener
                                    las condiciones de funcionamiento de acuerdo a lo solicitado por el CONSUMIDOR y que serán realizadas a cargo y por cuenta del
                                    CONSUMIDOR. EL PROVEEDOR no condicionará en modo alguno la prestación de servicios de reparación o mantenimiento del vehículo,
                                    a la adquisición o renta de otros productos o servicios en el mismo establecimiento o en otro taller o agencia predeterminada.
                                    El precio total de los servicios contratados se establece en el presupuesto que forma parte del presente y se describe en su anverso;
                                    dicho precio será pagado por el CONSUMIDOR al recibo del vehículo reparado, o en su caso, al momento de ser celebrado el presente
                                    contrato, por concepto de anticipo, la cantidad que resulte conforme a lo que establece la cláusula 14, y el resto en la fecha de
                                    entrega del vehículo reparado; todo pago efectuado por el CONSUMIDOR deberá efectuarse en el establecimiento del PROVEEDOR al contado
                                    y en Moneda Nacional o extranjera al tipo de cambio vigente al día de pago, de contado, ya sea en efectivo, o mediante tarjeta de
                                    crédito o deposito o transferencia bancaria efectuada con anterioridad a la entrega del vehículo.
                                </p>
                                <h6 align="center"><strong>DE LAS CONDICIONES GENERALES:</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>2.-</b> Las partes están de acuerdo en que las condiciones generales en las que se encuentra el automóvil de acuerdo con el inventario
                                    visual al momento de su recepción, son las que se definen en la orden de servicio-presupuesto que se encuentra al anverso del presente contrato.
                                </p>
                                <h6 align="center"><strong>DEL DIAGNÓSTICO Y DEL PRESUPUESTO.</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>3.-</b> Las partes convienen en que en caso de que el automóvil motivo de este contrato se deje en las instalaciones
                                    del PROVEEDOR para diagnóstico y presupuesto, éste, en un plazo no mayor de lo asentado en este contrato, realizará y
                                    entregará al CONSUMIDOR dicho diagnóstico y presupuesto en el que hará constar el precio detallado de las operaciones de
                                    mano de obra, de partes, reparación de partes, materiales, insumos, así como el tiempo en que se efectuaría la reparación
                                    para que, en su caso, el CONSUMIDOR lo apruebe. El presupuesto que así se entregue tendrá una vigencia de 3 días hábiles.<br>
                                    <b>4.-</b> El PROVEEDOR hace saber al CONSUMIDOR que al efectuar el diagnostico a su automóvil, las posibles consecuencias son
                                    las señaladas en el anverso del presente contrato, por lo que de no aceptarse la reparación, el vehículo le será entregado en
                                    las condiciones en que fue recibido para el diagnóstico, excepto en caso de que como consecuencia inevitable resulte imposible
                                    o ineficaz para su funcionamiento así entregarlo, porque al entregarlo armado sin repararlo, sea posible que se cause un daño
                                    mayor al mismo vehículo, o su uso constituya un riesgo para el usuario o terceros, por causa no imputable al PROVEEDOR, caso en
                                    el cual éste sólo se hará responsable de dichas consecuencias por causas imputables a él o sus empleados, el CONSUMIDOR enterado
                                    de esas consecuencias se responsabiliza de ellas.<br>
                                    <b>5.-</b> El PROVEEDOR se obliga ante el CONSUMIDOR a respetar el presupuesto contenido en el presente contrato.<br>
                                    <b>6.-</b> El CONSUMIDOR se obliga pagar a el PROVEEDOR por el diagnostico de su automóvil la cantidad de
                                    $ <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="cantidadAu" value="<?php if(set_value('cantidadAu') != "") echo set_value('cantidadAu'); else echo 'Por definir';?>" style="width:4cm;">(MONEDA NACIONAL),
                                    siempre y cuando no apruebe la reparación. En caso de autorizar la reparación, el diagnóstico no será cobrado.
                                </p>
                                <h6 align="center"><strong>DE LA PRESTACIÓN DE SERVICIOS</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>7.-</b> Las partes convienen en que la fecha de aceptación del presupuesto es la que se indica en el anverso de este contrato.<br>
                                    <b>8.-</b> El PROVEEDOR se obliga a hacer la entrega del automóvil reparado el día previamente establecido en el presupuesto. El PROVEEDOR
                                    se obliga a emplear partes y refacciones nuevas apropiadas para los servicios contratados salvo que el CONSUMIDOR ordene o autorice expresamente
                                    y por escrito se utilicen otras o las provea, de conformidad con lo establecido en el artículo 60 de la Ley Federal de Protección al
                                    Consumidor, caso en el cual la reparación no tendrá garantía en lo que se relacione a esas partes y refacciones, o a lo que éstas afecten, salvo en
                                    cuanto a la mano de obra que haya correspondido a su colocación. El retraso en la entrega de las refacciones por parte del CONSUMIDOR, si éste se
                                    compromete a proporcionarlas, prorrogará por el mismo lapso de demora, la fecha de entrega del vehículo.<br>
                                    <b>9.-</b> El CONSUMIDOR, puede desistir en cualquier momento de la prestación del servicio, pagando al PROVEEDOR el importe por los trabajos efectuados
                                    y partes colocadas o adquiridas, hasta el retiro del automóvil; en este caso, las refacciones adquiridas y que no hayan sido colocadas en el vehículo a
                                    repararse, serán entregadas al CONSUMIDOR.<br>
                                    <b>10.</b>- El PROVEEDOR, podrá utilizar el automóvil solo para recorridos de prueba, dentro de un radio de máximo 15 (quince) kilómetros alrededor del
                                    local en el que se presta el servicio, a efecto de realizar pruebas o verificar las reparaciones efectuadas; el PROVEEDOR no podrá usar el vehículo para
                                    fines propios o de terceros. El PROVEEDOR se hace responsable por los daños causados al vehículo, como consecuencia de los recorridos de prueba efectuados
                                    por parte de su personal. Cuando el CONSUMIDOR, solicite que él o un representante suyo sea quien conduzca el automóvil en un recorrido de prueba,
                                    el riesgo, será por su cuenta.<br>
                                    <b>11.</b>- El PROVEEDOR se hace responsable por las posibles descomposturas, daños o pérdidas parciales o totales imputables a él o a sus subalternos que sufra
                                    el vehículo, el equipo y los aditamentos adicionales que el CONSUMIDOR le haya notificado que existen en el momento de la recepción del mismo, o que se causen a
                                    terceros, mientras el vehículo se encuentre bajo su resguardo, salvo los ocasionados por desperfectos mecánicos o a resultas de piezas gastadas o sentidas, por
                                    incendio motivado por deficiencia de la instalación eléctrica, o fallas en el sistema de combustible, preexistentes y no causadas por el PROVEEDOR; para tal efecto
                                    el PROVEEDOR cuenta con un seguro suficiente para cubrir dichas eventualidades, bajo póliza expedida por compañía de seguros autorizada al efecto. El PROVEEDOR no
                                    se hace responsable por la pérdida de objetos dejados en el interior del automóvil, aun con la cajuela cerrada, salvo que estos le hayan sido notificados y puestos
                                    bajo su resguardo al momento de la recepción del automóvil. El PROVEEDOR tampoco se hace responsable por daños causados por fuerza mayor o caso fortuito, ni por la
                                    situación legal del automóvil cuando éste previamente haya sido robado o se hubiere utilizado en la comisión de algún ilícito; lo anterior, salvo que alguna de éstas
                                    cuestiones resultara legalmente imputable al PROVEEDOR; así mismo, el CONSUMIDOR, libera al PROVEEDOR de cualquier responsabilidad que surja o pueda surgir con relación
                                    al origen, posesión o cualquier otro derecho inherente al vehículo o a partes y componentes del mismo, obligándose en lo personal y en nombre del propietario, a responder
                                    de su procedencia.
                                </p>
                                <h6 align="center"><strong>DEL PRECIO Y FORMAS DE PAGO</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>12.-</b> El CONSUMIDOR acepta haber tenido a su disposición los precios de la tarifa de mano de obra, de las refacciones y materiales a usar
                                    en las reparaciones, ofrecidas por el PROVEEDOR; los incrementos que resulten durante la reparación, por costos no previsibles y/o incrementos
                                    que resulten al momento de la ejecución de la reparación ordenada, deberán ser autorizados por el CONSUMIDOR, por vía telefónica siempre y cuando
                                    estos no excedan del 20% presupuestado. Si el incremento citado es superior al 20%, el CONSUMIDOR lo tendrá que autorizar en forma escrita o por
                                    correo electrónico proveniente del mismo señalado como propio de él en el anverso del presente contrato. El tiempo que, en su caso, transcurra para
                                    cumplir esta condición, modificará la fecha de entrega, en la misma proporción. Todas las quejas y sugerencias serán atendidas en el domicilio,
                                    correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su anverso.<br>
                                    <b>13.-</b> Será obligación del PROVEEDOR expedir la factura correspondiente por los servicios y productos que preste o enajene; el importe total del
                                    servicio, así como el precio por concepto de refacciones, mano de obra, materiales y accesorios quedará especificado en ella, conforme a la ley.<br>
                                    <b>14.-</b> El CONSUMIDOR se obliga a pagar de contado al PROVEEDOR (conforme a lo establecido en la cláusula 1), en las instalaciones de éste y previo
                                    a la entrega del automóvil, el importe de los trabajos efectuados y partes colocadas o adquiridas hasta el retiro del mismo, de conformidad con el presupuesto
                                    elaborado para tal efecto. En caso de reparaciones cuyo presupuesto sea superior a $15,000.00 (QUINCE MIL PESOS 00/100 M.N.) el CONSUMIDOR deberá pagar un
                                    anticipo del 50% sobre el monto del presupuesto elaborado, y el saldo restante lo pagará a la entrega del automóvil debidamente reparado.
                                </p>
                                <h6 align="center"><strong>DE LA ENTREGA.</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>15.-</b> El PROVEEDOR se obliga a hacer la entrega del automóvil reparado en la fecha y hora establecida en este contrato, en las propias instalaciones del
                                    PROVEEDOR, pudiendo ampliarse dicho plazo en caso fortuito o de fuerza mayor, en cuyo caso será obligación del PROVEEDOR dar aviso previo al CONSUMIDOR de la
                                    causa y del tiempo que se ampliará el plazo de entrega.<br>
                                    <b>16.-</b> Las refacciones reemplazadas en las reparaciones quedarán a disposición del CONSUMIDOR al momento de recoger el automóvil, salvo que éste exprese
                                    lo contrario o que las refacciones, partes o piezas sean las cambiadas en ejercicio de la garantía o se trate de residuos considerados peligrosos, de acuerdo con
                                    las disposiciones legales aplicables. Si el CONSUMIDOR, al recoger su automóvil se niega a recibir las partes reemplazadas, el PROVEEDOR podrá disponer de ellas.<br>
                                    <b>17.-</b> El CONSUMIDOR se obliga a hacer el pago, a recoger el automóvil y, en su caso, las refacciones que fueran reemplazadas, dentro del horario establecido
                                    por el PROVEEDOR, en un término no mayor de 48 horas, contadas a partir del día y hora fijadas para su entrega, o bien de que se haya entregado el presupuesto y éste
                                    no hubiera sido autorizado. Si dentro de éste plazo el automóvil no pudiera circular por cualquier restricción, el plazo citado se ampliará 24 horas más. En caso de
                                    que el CONSUMIDOR no haga el pago y recoja el vehículo en el tiempo establecido, se obliga a pagar el almacenaje por la guarda del automóvil, caso en el que pagará la
                                    suma que resulte equivalente a  2 (dos) días de salario mínimo general vigente en la zona en la que se encuentran las instalaciones del PROVEEDOR, por cada día de
                                    retraso el cumplimiento de esas obligaciones. Si transcurridos 60 días naturales, contados a partir de la fecha en que debió entregarse el automóvil, no ha sido
                                    reclamado por el CONSUMIDOR, el PROVEEDOR notificará este hecho a las autoridades competentes y  podrá dar inicio a las acciones legales que correspondan para el
                                    cobro de los servicios y/o refacciones otorgados, así como del almacenaje.<br>
                                    <b>18.-</b> En el momento en que el PROVEEDOR entregue y el CONSUMIDOR reciba el automóvil reparado, se entenderá que esto fue a completa satisfacción del CONSUMIDOR en
                                    lo que respecta a sus condiciones generales de acuerdo al inventario visual, mismas que fueron descritos en la orden de servicio, así como también en cuanto a la reparación
                                    efectuada, sin afectar sus derechos a ejercer la garantía.
                                </p>
                                <h6 align="center"><strong>DE LA RESCISIÓN Y PENAS CONVENCIONALES</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>19.-</b> Es causa de rescisión del presente contrato: A.- Que EL PROVEEDOR incumpla en la fecha, hora y lugar de entrega del vehículo por causas propias, caso en el
                                    cual el CONSUMIDOR notificará por escrito del incumplimiento al PROVEEDOR, y éste hará entrega inmediata del vehículo debidamente reparado conforme al diagnóstico y
                                    presupuesto establecido, descontando del precio pactado para la prestación del servicio, la suma equivalente al 2% (dos por ciento) del total de la reparación, que se
                                    pacta como pena convencional; B.- Que el CONSUMIDOR incumpla con el pago de la reparación ordenada, en el término previsto en la cláusula 19, caso en el cual el PROVEEDOR
                                    le notificará por escrito su incumplimiento, y podrá optar por exigir la recisión o cumplimiento forzoso de la obligación, cobrando la misma pena pactada del 2% al
                                    CONSUMIDOR, por la mora.
                                </p>
                                <h6 align="center"><strong>DE LAS GARANTÍAS</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>20.-</b> <u>Las reparaciones a que se refiere el presupuesto aceptado por el CONSUMIDOR y éste contrato están garantizadas por 60 días naturales contados a partir de la fecha
                                    de la entrega del vehículo ya reparado, en mano de obra y en las refacciones, piezas y accesorios</u>, y trabajos que por su naturaleza hayan tenido que ser realizados en otros
                                    talleres <u>tendrá la especificada por el fabricante o taller respectivo, siempre y cuando no sea menor a la expresada, y no se manifieste mal uso, negligencia o descuido en el
                                    uso de ellas, de conformidad a lo establecido en el artículo 77 de la Ley Federal de Protección al Consumidor</u>.  Si el vehículo es intervenido por un tercero, el PROVEEDOR no
                                    será responsable y la garantía quedará sin efecto. Las reclamaciones por garantía se harán en el establecimiento del PROVEEDOR, para lo cual el CONSUMIDOR deberá presentar
                                    su vehículo en dicho establecimiento. Las reparaciones efectuadas por el PROVEEDOR en cumplimiento a la garantía del servicio serán sin cargo alguno para el CONSUMIDOR,
                                    salvo aquellos trabajos que no deriven de las reparaciones aceptadas en el presupuesto. No se computará dentro del plazo de garantía el tiempo que dure la reparación y/o
                                    mantenimiento del vehículo para el cumplimiento de la misma. Esta garantía cubre cualquier falla, deficiencia o irregularidad relacionada con la reparación efectuada por
                                    el PROVEEDOR, por causas imputables al mismo y solo será válida siempre y cuando el automóvil se haya utilizado en condiciones de uso normal, se hayan observado en su uso
                                    las indicaciones de manejo y servicio que se le hubiera dado. En todo caso, el PROVEEDOR será corresponsable y solidario con los terceros del cumplimiento o incumplimiento
                                    de las garantías por ellos otorgadas en lo que se relacione a las piezas, refacciones y accesorios, colocadas en el vehículo y de los servicios a éste realizados, siempre que
                                    hayan sido contratados ante el CONSUMIDOR.<br>
                                    <b>21.-</b> Toda reclamación dentro del término de garantía, deberá ser realizada ante el PROVEEDOR que efectuó la reparación y en el domicilio del mismo indicado en el anverso
                                    del presente contrato.  En caso de que sea necesario hacer válida la garantía en un domicilio diverso al del PROVEEDOR, los gastos por ello deberán ser cubiertos por éste, siempre
                                    y cuando la garantía proceda, dichos gastos sean indispensables para tal fin, y sea igualmente indispensable realizar la reparación del vehículo en domicilio diverso al del PROVEEDOR,
                                    pero en dado caso, si el automóvil fallase fuera de la entidad donde se localiza el PROVEEDOR, éste inmediatamente después de que tenga conocimiento de la falla o deficiencia, podrá
                                    indicar al CONSUMIDOR en donde se encuentra la agencia distribuidora de la marca más cercana, para hacer efectiva la garantía por conducto de ésta, si procede, debiendo acreditar con
                                    la factura correspondiente la reparación efectuada, con el objeto, de no hacer ningún cargo por ello al CONSUMIDOR. Con el objeto de dar cumplimiento a las obligaciones que ésta
                                    cláusula le impone, El PROVEEDOR señala como teléfonos de atención al CONSUMIDOR los que aparecen en éste contrato. Para los efectos de la atención y resolución de quejas y
                                    reclamaciones, estas deberán ser presentadas dentro de días y horas hábiles, que son los detallados en la parte superior del presente, ante la Gerencia de Servicio ubicada en las
                                    instalaciones del mismo PROVEEDOR, detallando en forma expresa la causa o motivo de la reclamación; la Gerencia de Servicio procederá a analizar la queja y resolverá lo conducente
                                    dentro de un lapso de tres días hábiles, procediendo a reparar el vehículo o manifestando las causas de improcedencia de la reclamación, en forma escrita.
                                </p>
                                <h6 align="center"><strong>DE LA INFORMACIÓN Y PUBLICIDAD</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>22.-</b> El PROVEEDOR se obliga a observar en todo momento lo dispuesto por los capítulos III y IV de la Ley Federal de Protección al Consumidor, en cuanto a la información, publicidad,
                                     promociones y ofertas.<br>
                                    <b>23.-</b> El CONSUMIDOR (Si&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="ceder" <?php echo set_checkbox('ceder', '1'); ?>>&nbsp;&nbsp;No&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="ceder" <?php echo set_checkbox('ceder', '0'); ?>>)&nbsp;&nbsp;
                                    <?php echo form_error('ceder', '<span class="error">', '</span>'); ?>
                                    acepta que el PROVEEDOR ceda o transmita a tercero, con fines mercadotécnicos o publicitarios, la información proporcionada por él con motivo del presente contrato, y
                                    (Si&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="publicar" <?php echo set_checkbox('publicar', '1'); ?>>&nbsp;&nbsp;No&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="publicar" <?php echo set_checkbox('publicar', '0'); ?>>)&nbsp;&nbsp;
                                    acepta que el PROVEEDOR le envíe publicidad sobre bienes y servicios, firmando en éste espacio.
                                    <br>
                                </p>
                                <p align="center">
                                    <input type="hidden" id="rutaFirmaConsumidor_2" name="rutaFirmaConsumidor_2" value="<?= set_value('rutaFirmaConsumidor_2');?>">
                                    <img class="marcoImg" src="<?= set_value('rutaFirmaConsumidor_2');?>" id="firmaFirmaConsumi_2" alt="">
                                    <br><br>
                                    <?php echo form_error('rutaFirmaConsumidor_2', '<span class="error">', '</span>'); ?>
                                    <br>
                                    <a class="cuadroFirma btn btn-primary" data-value="El_Consumidor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>.
                                </p>
                                <h6 align="center"><strong>DE LA INTERPRETACIÓN Y CUMPLIMIENTO</strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    <b>24.-</b> La Procuraduría Federal del Consumidor, es competente para conocer y resolver en la vía administrativa de cualquier controversia que se suscite sobre la interpretación o
                                    cumplimiento del presente contrato, por lo que las partes están de acuerdo en someterse a ella en términos de ley, para resolver sobre la interpretación o cumplimiento de los términos
                                    del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-174-SCFI-2007, Prácticas Comerciales- Elementos de Información
                                    para la Prestación de Servicios en General y cualquier otra disposición aplicable. Sin perjuicio de lo anterior en caso de persistir la inconformidad, las partes se someten a la
                                    jurisdicción de los tribunales competentes del domicilio del PROVEEDOR, renunciando en forma expresa a cualquier otra jurisdicción o al fuero que pudiera corresponderles en razón de sus
                                    domicilios presente o futuros o por cualquier otra razón.
                                </p>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-6" align="center">
                                EL DISTRIBUIDOR (NOMBRE Y FIRMA).
                                <br>
                                <input type="hidden" id="rutaFirmaDistribuidor_3" name="rutaFirmaDistribuidor_3" value="<?= set_value('rutaFirmaDistribuidor_3');?>">
                                <img class="marcoImg" src="<?= set_value('rutaFirmaDistribuidor_3');?>" id="firmaFirmaDistribuidor_3" alt="">
                                <br>
                                <input type="text" id="nombreDistribuidor_3" class="input_field asesorname" name="nombreDistribuidor_3" style="width:100%;" value="<?= set_value('nombreDistribuidor_3');?>">
                                <?php echo form_error('rutaFirmaDistribuidor_3', '<span class="error">', '</span>'); ?><br>
                                <?php echo form_error('nombreDistribuidor_3', '<span class="error">', '</span>'); ?><br>
                            </div>

                            <div class="col-md-6" align="center">
                                EL CONSUMIDOR (NOMBRE Y FIRMA)
                                <br>
                                <input type="hidden" id="rutaFirmaConsumidor_3" name="rutaFirmaConsumidor_3" value="<?= set_value('rutaFirmaConsumidor_3');?>">
                                <img class="marcoImg" src="<?= set_value('rutaFirmaConsumidor_3');?>" id="firmaFirmaConsumidor_3" alt="">
                                <br>
                                <input type="text" id="nombreConsumidor_3" class="input_field nombreCliente" name="nombreConsumidor_3" style="width:100%;" value="<?= set_value('nombreConsumidor_3');?>">
                                <?php echo form_error('rutaFirmaConsumidor_3', '<span class="error">', '</span>'); ?><br>
                                <?php echo form_error('nombreConsumidor_3', '<span class="error">', '</span>'); ?><br>
                            </div>
                        </div>

                        <br>
                        <div class="panel-body">
                            <div class="col-md-6" align="center">
                                <a class="cuadroFirma btn btn-primary" data-value="Distribuidor_3" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            </div>
                            <div class="col-md-6" align="center">
                                <a class="cuadroFirma btn btn-primary" data-value="Consumidor_3" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="col-md-12" align="center">
                                <h6 align="center"><strong>AVISO DE PRIVACIDAD </strong></h6>
                                <p align="justify" style="text-align: justify; font-size:11px;">
                                    EL Aviso de Privacidad Integral podrá ser consultado en nuestra página en internet <strong><?=SUCURSAL_WEB?>/PRIVACIDAD/</strong>, o lo puede
                                    solicitar al correo electrónico <strong>DATOSPERSONALES@FORDPLASENCIA.COM</strong>, u obtener personalmente en el Área de Atención a la Privacidad,
                                    en nuestras instalaciones.<br>
                                    <input type="checkbox" style="transform: scale(1.5);" value="1" name="aceptar" <?php echo set_checkbox('aceptar', '1'); ?>>&nbsp;&nbsp;
                                    <?php echo form_error('aceptar', '<span class="error">', '</span>'); ?>&nbsp;&nbsp;
                                    Al marcar el recuadro precedente y remitir mis datos, otorgo mi consentimiento para que mis datos personales sean tratados conforme a lo señalado en el Aviso de Privacidad.<br>
                                    Este contrato fue registrado ante la Procuraduría Federal del Consumidor. Registro público de contratos de adhesión y aprobado e inscrito con el
                                    número <input type="text" class="input_field" name="numeroR" value="84-2019" style="width:3cm;">, Expediente No. <input type="text" class="input_field" name="expediente" value="PFC.B.E.7-000169-2019" style="width:4cm;">,
                                    de fecha <input type="number" class="input_field" name="diaN" value="09" style="width:2cm;"> de <input type="text" class="input_field" name="mesN" value="Enero" style="width:2cm;"> de
                                    <input type="number" class="input_field" name="anoN" value="2019" style="width:3cm;">. <br>
                                    Cualquier variación del presente contrato en perjuicio de EL CONSUMIDOR, frente al contrato de adhesión registrado, se tendrá por no puesta. <br><br>
                                    <strong>“DESEO ADHERIRME AL CONTRATO TIPO DE REPARACION Y MANTENIMIENTO DE VEHICULOS FORD DE PROFECO”</strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row" align="center">
            <div class="col-md-12" align="center">
                <input type="button" class="btn btn-success" name="enviar" id="enviarOrdenForm" value="Guardar diagnóstico">
                <!-- <button class="btn btn-primary" ng-click="save()" value="">Guardar Orden</button> -->
                <!-- <input type="hidden" name="idCita" id="idCita" value="<?php if(isset($idCita)) echo $idCita; else echo "0";  ?>"> -->
                <input type="hidden" name="asesorLogin" id="asesorLogin" value="<?php if(set_value('asesorLogin') != "") echo set_value('asesorLogin'); elseif (isset($asesor)) echo $asesor;?>">
                <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="1">
                <input type="hidden" name="panelOrigen" id="panelOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                <input type="hidden" name="" id="formularioHoja" value="OrdenServicio">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            </div>
        </div>
        <br>
    </form>
</div>



<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                    <!-- <p id="limpiar">limpiar canvas</p> -->
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para cargar video evidencia -->
<div class="modal fade" id="videoEvidencia" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document" style="z-index: 2000;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Carga de video</h4>
            </div>
            <div class="modal-body">
                <br>
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <br>
                <form method="post" id="formularioTempVideo" action="" autocomplete="on" enctype="multipart/form-data">
                    <div class="row" style="font-size:14px;">
                        <div class="col-md-3">
                            <br><br>
                            <label style="font-size:12px;">Cita</label>
                            <input type="text" class="input_field" id="idCitaVideo" name="idCitaVideo" value="<?php if(isset($idCita)) echo $idCita; else echo "0";  ?>" ><br>
                            <label for="" class="error" id="errorIdCitaVideo"></label>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-3">
                            <div class="row">
                                <br>
                                <input type='radio' class='BigRadio' name='tipoEnvioEvidencia' value="videos" checked>
                                <label style="font-size:12px;margin-top:10px;">Video</label>
                            </div>
                            <br>
                            <div class="row">
                                <br>
                                <input type='radio' class='BigRadio' name='tipoEnvioEvidencia' value="imagenes">
                                <label style="font-size:12px;margin-top:10px;">Imagen</label>
                            </div>
                        </div>
                        <div class="col-md-4" align="right">
                            <br><br>
                            <input type="file" id="uploadfilesVideo" name="uploadfilesVideo" accept="video/*" style="width:100%;">
                            <br>
                            <!-- <input type="button" value="Subir" id='upload'> -->
                            <label for="" class="error" id="errorArchivoVideo"></label>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <label style="font-size:12px;margin-top:10px;">Comentario:</label>
                            <textarea type="text" class="input_field_lg" id="comentarioVideo" name="comentarioVideo"style="width:100%;"><?php echo  set_value('comentarioVideo');?></textarea>
                        </div>
                    </div>
                </form>
            </div>

            <br>
            <div class="row">
                <div class="col-md-12"  align="center">
                    <i class="fas fa-spinner cargaIcono"></i>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal" id="btnCerrarVideo">Cancelar</button>
                <button type="button" class="btn btn-success" id="enviarVideo">Guardar</button>
            </div>
            <br><br>
        </div>
    </div>
</div>
