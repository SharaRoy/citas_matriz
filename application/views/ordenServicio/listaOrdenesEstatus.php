<div class="panel-body">
    <div class="col-md-12">            
        <div class="row">
            <div class="col-sm-8">
                <h3 align="right">Ordenes de servicio por estatus</h3>
            </div>
            <div class="col-sm-4" align="right" style="padding-top: 20px;">
                <?php if (isset($pOrigen)): ?>
                    <?php if ($pOrigen == "ASE"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($pOrigen == "TEC"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($pOrigen == "JDT"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($pOrigen == "PCO"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Principal_CierreOrden/5";?>';">
                            Cerrar Sesión
                        </button>
                    <?php elseif ($pOrigen == "GARANTIA"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Garantias_Panel/5";?>';">
                            Cerrar Sesión
                        </button>
                    <?php else: ?>
                        <!--<button type="button" class="btn btn-dark" >
                            Sesión Expirada
                        </button>-->
                    <?php endif; ?>
                <?php else: ?>
                    <!--<button type="button" class="btn btn-dark" >
                        Sesión Expirada
                    </button>-->
                <?php endif; ?>
            </div>
        </div>
        
        <br>
        <div class="panel panel-default">
            <div class="panel-body" style="border:2px solid black;">
                <div class="row">
                    <div class="col-md-8"></div>
                    <!--<div class="col-md-3">
                        <h5>Fecha Inicio:</h5>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </div>
                            <input type="date" class="form-control" id="fecha_inicio_estatus" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h5>Fecha Fin:</h5>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            </div>
                            <input type="date" class="form-control" id="fecha_fin_estatus" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <br><br>
                        <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_fechas_estatus" style="margin-right: 15px;"> 
                            <i class="fa fa-search"></i>
                        </button>

                        <button class="btn btn-secondary" type="button" title="Limpiar Busqueda" id="btnLimpiar" onclick="location.reload()">
                            <i class="fa fa-trash"></i>
                        </button>
                    </div> -->
                    <!-- formulario de busqueda -->
                    <div class="col-md-3">
                        <h5>Busqueda Gral.:</h5>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </div>
                            <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                        </div>
                    </div>
                    <!-- /.formulario de busqueda -->
                </div>
                
                <br>
                <div class="form-group" align="center">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <table class="table table-bordered table-responsive" style="width:100%;max-height: 11cm;overflow-y: scroll;">
                        <thead>
                            <tr style="font-size:12px;background-color: #eee;">
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 4%;"></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 8%;"><strong>ORDEN</strong></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 8%;"><strong>ORDEN INTELISIS</strong></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 10%;"><strong>FECHA APERTURA</strong></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 10%;"><strong>VEHÍCULO</strong></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 10%;"><strong>PLACAS</strong></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 11%;"><strong>ASESOR</strong></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 11%;"><strong>TÉCNICO</strong></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 16%;"><strong>CLIENTE</strong></td>
                                <td align="center" rowspan="2" style="vertical-align: middle;width: 10%;"><strong>ESTATUS</strong></td>
                            </tr>
                        </thead> 
                        <tbody class="campos_buscar">
                            <?php if (isset($idOrden)): ?>
                                <?php if (count($idOrden)>0): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="vertical-align: middle;">
                                                <!-- <?php echo count($idOrden) - $index; ?> -->
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $fechaDiag[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombreAsesor[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombreConsumidor2[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;color:white;background-color: <?=$color[$index]; ?>;">
                                                <?php echo strtoupper($status[$index]); ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="9" align="center">
                                            <h4 style="text-align: center;">Sin ordenes que mostrar</h4>
                                        </td>
                                    </tr>
                                <?php endif ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="9" align="center">
                                        <h4 style="text-align: center;">Sin ordenes que mostrar</h4>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>

                    <!-- <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar"> -->

                    <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal para mostrar el audio de la voz cliente-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para agregar un comentario -->
<div class="modal fade" id="addComentario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Ingresar comentario</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="row">
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Titulo</h4>
                        <input type="text" class="form-control" name="" id="titulo" value="">
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Fecha</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </div>
                            <input type="date" class="form-control" id="fechaComentario" max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d"); ?>" style="padding-top: 0px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h4>Hora</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-clock"></i>
                            </div>
                            <input type="text" class="form-control" id="HoraComentario" value="<?php echo date("H:m"); ?>">
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Comentario</h4>
                        <textarea rows='3' class='form-control' name="" id='comentario' style='width:100%;text-align:left;font-size:12px;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="idMagigAdd" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnEnviarComentario">Enviar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver el historial de comentarios -->
<div class="modal fade" id="historialCometario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Historial de comentarios</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="" style="width:100%;">
                            <thead>
                                <tr style="background-color: #ddd;">
                                    <td style="width:25%;"><h4 align="center">Fecha</h4></td>
                                    <td style="width:25%;"><h4 align="center">Titulo</h4></td>
                                    <td style="width:50%;"><h4 align="center">Comentario</h4></td>
                                </tr>
                            </thead>
                            <tbody id="tablaComnetario">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="idMagigHistorial" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>