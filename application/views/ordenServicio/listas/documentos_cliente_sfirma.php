<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-4" align="left" style="padding-top: 20px;">
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/4";?>';">
                        Regresar
                    </button>
                </div>
                <div class="col-sm-8">
                    <h3 align="right">Documentación pendiente (Autorización por Ausencia)</h3>
                </div>
            </div>
            
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row" style="display: none;">
                        <div class="col-md-3">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_fechas" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <!-- formulario de busqueda -->
                    </div>

                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:12px;background-color: #eee;">
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>#</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>NO. <br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>APERTURA<br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" colspan="6" style=""><strong>ACCIONES</strong></td>
                                </tr>
                                <tr style="background-color: #ddd;">
                                    <td align="center" style="width: 5%;font-size:9px;">ORDEN</td>
                                    <!--<td align="center" style="width: 5%;font-size:9px;">MULTIPUNTO</td>-->
                                    <td align="center" style="width: 5%;font-size:9px;">VOZ CLIENTE</td>
                                    <td align="center" style="width: 5%;font-size:9px;">CARTA RENUNCIA</td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($id_cita)): ?>
                                    <?php foreach ($id_cita as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $id_cita[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $fecha_recepcion[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombre_asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $cliente[$index]; ?>
                                            </td>
                                            <!-- Orden de servicio (Vista rapida) -->
                                            <td align="center" style="vertical-align: middle;">
                                                <a href="<?=base_url().'OrdenServicio_Firmas/'.$id_cita_en[$index];?>" class="btn btn-info" target="_blank" style="font-size: 12px;">DIAGNOSTICO</a>
                                                <br>
                                                <span style="font-weight: bold;font-size: 10px;color: <?= (($firma_diag[$index] == "1") ? "darkgreen" : "darkred") ?>">
                                                    <?= (($firma_diag[$index] == "1") ? "FIRMADO" : "SIN FIRMAR") ?>
                                                </span>
                                            </td>
                                            
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($voz_cliente[$index] == "1"): ?>
                                                    <a href="<?=base_url().'VCliente_Firmas/'.$id_cita_en[$index];?>" class="btn btn-dark" target="_blank" style="font-size: 10px;color:white;height:100%;">VOZ CLIENTE</a>
                                                    
                                                    <br>
                                                    <?php if ($vc_udio[$index] != ""): ?>
                                                        <a class="AudioModal" data-value="<?php echo $vc_udio[$index]; ?>" data-orden="<?php echo $id_cita[$index]; ?>" data-target="#muestraAudio" data-toggle="modal" style="color:blue;font-size:24px;">
                                                            <i class="far fa-play-circle" style="font-size:24px;"></i>
                                                        </a>
                                                    <?php else: ?>
                                                        <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                                                    <?php endif; ?>
                                                    <br>
                                                    <span style="font-weight: bold;font-size: 10px;color: <?= (($firma_vc[$index] == "1") ? "darkgreen" : "darkred") ?>">
                                                        <?= (($firma_vc[$index] == "1") ? "FIRMADO" : "SIN FIRMAR") ?>
                                                    </span>
                                                <?php endif; ?>
                                            </td>

                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($carta_renuncia[$index] != NULL): ?>
                                                    <a href="<?=base_url().'CartaRenuncia_Firmas/'.$id_cita_en[$index];?>" class="btn btn-primary" target="_blank" style="font-size: 10px;color:white;height:100%;">CARTA RENUNCIA</a>
                                                    <br>
                                                    <span style="font-weight: bold;font-size: 10px;color: <?= (($firma_cr[$index] == "1") ? "darkgreen" : "darkred") ?>">
                                                        <?= (($firma_cr[$index] == "1") ? "FIRMADO" : "SIN FIRMAR") ?>
                                                    </span>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="14" style="width:100%;" align="center">
                                            <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                                <h4>Sin registros que mostrar</h4>
                                            <?php else: ?>
                                                <h4>Sesión Expirada</h4>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="pOrigen" id="pOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                        <input type="hidden" id="rol" value="Asesor">
                        <input type="hidden" id="origen_lista" value="Cliente">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" autoplay style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>
