<style>
	label, input{
		font-size: 14px;	
		font-family: arial;
	}
	
</style>


<br>
<div class="row">
    <div class="col-md-2" style="padding-top: 20px;padding-left: 35px;">
        <?php if ($this->session->userdata('rolIniciado')): ?>
            <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Documentacion_Asesores/5";?>';">
                    Regresar
                </button>
            <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Documentacion_Tecnicos/5";?>';">
                    Regresar
                </button>
            <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Documentacion_JefeTaller/5";?>';">
                    Regresar
                </button>
            <?php else: ?>
                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Documentacion_JefeTaller/5";?>';">
                    Regresar
                </button>
            <?php endif; ?>
        <?php else: ?>
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Documentacion_JefeTaller/5";?>';">
                Regresar
            </button>
        <?php endif; ?>
    </div>
    <div class="col-sm-10" style="padding-right: 35px;">
        <h3 align="right">Asignación operaciones a folio de garantías</h3>
    </div>
</div>

<div style="margin:20px;border:1px solid darkblue; padding: 10px; ">
    <div class="row">
        <div class="col-md-3">
            <label >No. Orden : </label>&nbsp;
            <input type="text" class="form-control" id="id_cita" value="<?= ((isset($cabecera['id_cita'])) ? $cabecera['id_cita'] : "") ?>" style="width:100%;" disabled>
        </div>
        <div class="col-md-3">
            <label >Orden Intelisis : </label>&nbsp;
            <input type="text" class="form-control"  id="folio_externo" value="<?= ((isset($cabecera['folio_externo'])) ? $cabecera['folio_externo'] : "") ?>" style="width:100%;" disabled>
        </div>
        <div class="col-md-3">
            <label >No.Serie (VIN):</label>&nbsp;
            <input class="form-control" type="text"  id="serie" value="<?= ((isset($cabecera['serie'])) ? $cabecera['serie'] : "") ?>" style="width:100%;" disabled>
        </div>
        <div class="col-md-3">
            <label >Modelo:</label>&nbsp;
            <input class="form-control" type="text" id="modelo" value="<?= ((isset($cabecera['modelo'])) ? $cabecera['modelo'] : "") ?>" style="width:100%;" disabled>
        </div>
    </div>
    
    <br>
    <div class="row">
        <div class="col-md-3">
            <label >Placas</label>&nbsp;
            <input type="text" class="form-control" id="placas" value="<?= ((isset($cabecera['placas'])) ? $cabecera['placas'] : "") ?>" style="width:100%;" disabled>
        </div>
        <div class="col-md-3">
            <label >Unidad</label>&nbsp;
            <input class="form-control" type="text" id="anio" value="<?= ((isset($cabecera['anio'])) ? $cabecera['anio'] : "") ?>" style="width:100%;" disabled>
        </div>
        <div class="col-md-3">
            <label >Técnico</label>&nbsp;
            <input class="form-control" type="text" id="tecnico" value="<?= ((isset($cabecera['tecnico'])) ? $cabecera['tecnico'] : "") ?>" style="width:100%;" disabled>
        </div>
        <div class="col-md-3">
            <label >Asesor</label>&nbsp;
            <input class="form-control" type="text" id="asesors" value="<?= ((isset($cabecera['asesor'])) ? $cabecera['asesor'] : "") ?>" style="width:100%;" disabled>
        </div>
    </div>

    <br><br>

    <br>
    <div class="form-group" align="center">
        <i class="fas fa-spinner cargaIcono" id="carga_tabla"></i>
        <table class="table table-bordered" style="width:99vw;">
            <thead>
            	<tr style="background-color: #eee;">
            		<td style="width:8%;" align="center">
            			ACCIONES
            		</td>
            		<td align="center">
            			DESCRPCIÓN
            		</td>
                    <td style="width:10%;" align="center">
                        FOLIO
                    </td>
                    <td style="width:8%;" align="center">
                        N° REP
                    </td>
            	</tr>
            </thead>
            <tbody>
                <?php if ($operaciones != NULL): ?>
                    <?php foreach ($operaciones as $i => $registro): ?>
                    	<tr id="renglon_asignado_<?= $registro->id; ?>" style="background-color:  <?= (($registro->folio_asociado != "") ? "#c7ecc7" : "#ffffff" )  ?>;"  >
                    		<td align="center">
                    			<a  onclick="asociar_btn(<?= $registro->id; ?>)" id="btn_<?= $registro->id; ?>" data-value="<?= $registro->id; ?>" data-target="#asociar" data-toggle="modal" title="Asociar operación F1863" style="color:blue;" <?= (($registro->folio_asociado != "") ? "hidden" : "" )  ?>>
                                    <i class="fa fa-refresh" style="font-size:18px;"></i>
                                </a>
                    		</td>
                    		<td>
                    			<?= $registro->descripcion; ?>
                    		</td>
                            <td align="center">
                                <label id="label_<?= $registro->id; ?>" ><?= $registro->folio_asociado; ?></label>
                            </td>
                            <td align="center">
                                <label id="label_rep_<?= $registro->id; ?>" ><?= (($registro->n_reparacion_garantia != NULL) ? $registro->n_reparacion_garantia : ""); ?></label>
                            </td>
                    	</tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="4" style="width:100%;" align="center">
                            <h4>Sin registros que mostrar</h4>
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="asociar" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Folios garantías</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <select class="form-control" id="select_folio" onchange="mostrar_reparaciones()">
                        	<option value="">Selecciona folio</option>
                        	<?php if (count($folios) > 0): ?>
								<?php foreach ($folios as $f => $folio): ?>
									<option value="<?= $folios[$f]['id_parte_operacion'] ?>">
                                        <?= $folios[$f]['id_parte_operacion'].'-'.$folios[$f]['queja_cliente'] ?>
                                    </option>
								<?php endforeach; ?>
							<?php endif; ?>
                        </select>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <select class="form-control" id="select_reparacion" hidden="">
                            <option value="">Selecciona reparación</option>
                            <?php if (count($reparaciones) > 0): ?>
                                <?php foreach ($reparaciones as $r => $rep): ?>
                                    <option value="<?= $reparaciones[$r]['numero_reparacion'] ?>" <?= ((count($folios) == "1") ? "selected" : "") ?>>
                                        REP # <?= $reparaciones[$r]['numero_reparacion'] ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <input type="hidden" id="n_reparaciones" value="<?= count($reparaciones) ?>">
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono" id="carga_select"></i>
                        <h5 class="error" id="error_relacion">
                            
                        </h5>
                        <br>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" id="renglon" value="">
                <input type="button" onclick="enviar_asociacion()" class="btn btn-success" value="Guardar">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function mostrar_reparaciones(){
        var folio = $("#select_folio").val();
        if (folio != "") {
            var reparaciones = $("#n_reparaciones").val();
            if (reparaciones > 0) {
                $("#select_reparacion").attr('hidden',false);
            }
        }else{
            $("#select_reparacion").attr('hidden',true);
        }
    }

    function asociar_btn(renglon){
        //var renglon = $(this).data("value");
        $("#renglon").val(renglon);
    }

    function enviar_asociacion(){
        var id_cita = $("#id_cita").val();
        var folio = $("#select_folio").val();
        var reparacion = $("#select_reparacion").val();
        var base = $("#basePeticion").val().trim();
        var renglon = $("#renglon").val();

        var reparaciones = $("#n_reparaciones").val();
        if (reparaciones == 0) {
            reparacion = 0;
        }

        if ((folio != "")&&(reparacion != "")) {
            $("#carga_select").css("display","inline-block");

            $.ajax({
                url: base+"ordenServicio/Documentacion_Ordenes/asociar_folios",
                method: 'post',
                data: {
                    id_cita: id_cita,
                    renglon: renglon,
                    folio: folio,
                    reparacion: reparacion,
                },
                success:function(resp){
                    if (resp.indexOf("handler         </p>")<1) {
                        if (resp == "1") {
                            $("#error_relacion").text("FOLIO ASIGNADO CORRECTAMENTE");
                            var renglon = $("#renglon").val();
                            $("#renglon_asignado_"+renglon).css("background-color","#c7ecc7");
                            $("#btn_"+renglon).attr("hidden",true);
                            $("#label_"+renglon).text(folio);
                            $("#label_rep_"+renglon).text(reparacion);
                        } else {
                            $("#error_relacion").text("OCURRIO UN ERROR DURANTE EL ENVIO COD 1");
                        }
                    }else{
                        $("#error_relacion").text("OCURRIO UN ERROR DURANTE EL ENVIO COD 2");
                    }

                    $("#carga_select").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $("#error_relacion").text("ERROR COD 3");
                    $("#carga_select").css("display","none");
                //Cierre del error
                }
            //Cierre del ajax
            });

        }else{
            if (folio == "") {
                $("#error_relacion").text("NO SE SELECCIONO NINGUN FOLIO");
            }else{
                $("#error_relacion").text("NO SE SELECCIONO NINGUNA REPARACIÓN");
            }
        }
    }
</script>