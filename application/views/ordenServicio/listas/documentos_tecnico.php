<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-4" align="left" style="padding-top: 20px;">
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/4";?>';">
                        Regresar
                    </button>
                </div>
                <div class="col-sm-8">
                    <h3 align="right">Documentación de Ordenes</h3>
                </div>
            </div>
            
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-3">
                            <h5>Busqueda No Orden.:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_tecnicos" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <!-- formulario de busqueda -->
                    </div>

                    <br>
                    <div class="row" style="border-top: 0.5px solid darkblue;">
                        <div class="col-md-7"></div>
                        <div class="col-md-3" align="right">
                            <h5>Actualizar documentacón:</h5>
                            <div class="input-group">
                                <input type="date" class="form-control" id="busqueda_fecha" max="<?= date("Y-m-d") ?>" value="<?= date("Y-m-d") ?>" style="padding-top: 0px;">
                            </div>
                        </div>

                        <div class="col-md-2" align="center">
                            <br><br>
                            <button class="btn btn-warning" type="button" name="" id="actualizar_doc" style="margin-right: 15px;"> 
                                <i class="fa fa-refresh"></i>
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <h5 style="color:red;text-align: center;" id="error_act_doc"></h5>
                         <!-- id="bootstrap-data-table2"-->
                        <table  class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:12px;background-color: #eee;">
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>NO. <br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>APERTURA<br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>PLACAS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>TÉCNICO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;"><strong>CLIENTE</strong></td>
                                    <!--<td align="center" colspan="7" style=""><strong>ACCIONES</strong></td>-->
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                </tr>
                                <tr style="background-color: #ddd;">
                                    <td align="center" style="width: 5%;font-size:9px;">ORDEN</td>
                                    <td align="center" style="width: 5%;font-size:9px;">ARC.OASIS</td>
                                    <td align="center" style="width: 5%;font-size:9px;">MULTIPUNTO</td>
                                    <td align="center" style="width: 5%;font-size:9px;">PRESUPUESTO</td>
                                    <td align="center" style="width: 5%;font-size:9px;">VOZ CLIENTE</td>
                                    <!--<td align="center" style="width: 5%;font-size:9px;">SERVICIO VALET</td>-->
                                    <td align="center" style="width: 5%;font-size:9px;">G. F1863</td>
                                    <td align="center" style="width: 5%;font-size:9px;">FOLIO GARANTÍA</td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($id_cita)): ?>
                                    <?php foreach ($id_cita as $index => $valor): ?>
                                        <tr style="font-size:11px;background-color: <?= $bg_tr[$index] ?>;">
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $tipo_orden[$index]."-".$id_cita[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $folio_externo[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $fecha_recepcion[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $nombre_asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $cliente[$index]; ?>
                                            </td>
                                            <!-- Orden de servicio (Vista rapida) -->
                                            <td align="center" style="vertical-align: middle;">
                                                <a href="<?=base_url().'OrdenServicio_Revision/'.$id_cita_url[$index];?>" class="btn btn-info" target="_blank" style="font-size: 9px;">ORDEN</a>
                                                
                                                <br>
                                                <?php if ($garantias[$index] == "1"): ?>
                                                    <a href="<?=base_url().'OrdenServicio_RGTecnico/'.$garantias_id_cita[$index];?>" class="btn <?= (($gpza_firmas[$index] == 1) ? 'btn-success' : 'btn-danger') ?>" target="_blank" style="font-size: 10px; color:white;">OR. GARANTÍA</a>
                                                <?php endif; ?> 
                                            </td>

                                            <!-- Archivos Oasis -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($oasis[$index] != ""): ?>
                                                    <a href="<?=base_url().$oasis[$index];?>" class="btn btn-primary" target="_blank" style="font-size: 9px;margin-top:5px;">
                                                        ARC. OASIS
                                                    </a>
                                                <?php else: ?>
                                                    <button type="button" class="btn btn-default" name="" style="font-size: 8px;">SIN <br> ARCHIVO</button>
                                                <?php endif; ?> 

                                                <?php if ($doc_poliza[$index] == "1"): ?>
                                                    <a href="<?=base_url().'Ver_Polizas/'.$id_cita_url[$index]; ?>" class="btn" target="_blank" style="color: white; font-size: 9px;margin-top:2px; background-color: #008080;">
                                                        P. GARANTÍA
                                                    </a>
                                                <?php endif; ?>
                                            </td>
                                            
                                            <!-- Hoja Multipunto (Vista rapida), con indicador de lavado y firma jefe de tallar -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($multipunto[$index] == "1"): ?>
                                                    <a href="<?=base_url().'Multipunto_Revision/'.$id_cita_url[$index];?>" class="btn <?php if($firma_jefetaller[$index] == "1") echo 'btn-success'; else echo 'btn-danger'; ?>" target="_blank" style="font-size: 10px;">MULTIPUNTOS</a>
                                                <?php endif; ?>
                                                
                                                <label style="color:blue;font-weight: bold;">
                                                    <?php if(isset($lavado[$index])) echo $lavado[$index]; ?>
                                                </label>
                                            </td>
                                            
                                            <!-- Presupuestos multipunto (solo nuevos formatos) -->
                                            <td align="center" style="vertical-align: middle;">
                                                <!-- presupuestos antes del 4 de dic del 2019-->
                                                <?php if ($presupuesto[$index] == "1"): ?>
                                                    <?php if ($presupuesto_afecta[$index] != ""): ?>
                                                        <a href="<?=base_url()."Presupuesto_Cliente/".$id_cita_url[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">PRESUPUESTO</a>
                                                    <?php else: ?>
                                                        <a href="<?=base_url()."Alta_Presupuesto/".$id_cita_url[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">PRESUPUESTO</a>
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            </td> 
                                            
                                            <td align="center" style="vertical-align: middle; ">
                                                <?php if ($voz_cliente[$index] == "1"): ?>
                                                    <a href="<?=base_url().'VCliente_Revision/'.$id_cita_url[$index];?>" class="btn btn-dark" target="_blank" style="font-size: 10px;color:white;height:100%;">VOZ CLIENTE</a>
                                                    
                                                    <br>
                                                    <?php if ($vc_udio[$index] != ""): ?>
                                                        <a class="AudioModal" data-value="<?php echo $vc_udio[$index]; ?>" data-orden="<?php echo $id_cita[$index]; ?>" data-target="#muestraAudio" data-toggle="modal" style="color:blue;font-size:24px;">
                                                            <i class="far fa-play-circle" style="font-size:24px;"></i>
                                                        </a>.
                                                    <?php else: ?>
                                                        <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>

                                            <!--<td align="center" style="vertical-align: middle;">
                                                <?php if ($servicio_valet[$index] == "1"): ?>
                                                    <a href="<?=base_url().'Servicio_valet_PDF/'.$id_cita_url[$index];?>" class="btn btn-default" target="_blank" style="font-size: 10px;background-color:  #a569bd; color:white;">VALET</a>
                                                <?php endif; ?>
                                            </td>-->

                                            <!-- Link para abrir la carga de refacciones -->
                                            <td align='center' style='vertical-align: middle;'>

                                                <?php if ( ($tipo_orden[$index] == "T")||  ($tipo_orden[$index] == "I") || ($tipo_orden[$index] == "R") || ( ($diagnostico[$index] == "0")&&($tipo_orden[$index] == "G") ) ): ?>
                                                    <a href='<?= API_GARANTIA ?>garantias/F1863/form_registro_labor?perfil=<?php echo $this->session->userdata('rolIniciado'); ?>&no_orden=<?= $id_cita[$index];?>' class='btn' target='_blank' style='font-size: 11px; background-color: #2b3188;color:white;'>F. 1863</a>


                                                    <?php if ($formatos_g[$index] != '0'): ?>
                                                        <a href='<?=base_url().'Formatos_TecGarantia/'.$id_cita_url[$index];?>' class='btn <?= (($formatos_gfirma[$index] == 1) ? 'btn-success' : 'btn-danger') ?>' target='_blank' style='font-size: 10px; color:white;'>
                                                            Ver Formato(s) 
                                                        </a>
                                                    <?php else: ?>
                                                        <button class='btn btn-default' style='font-size: 10px; color:black;'>
                                                            Sin Formato(s)
                                                        </button>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            </td>

                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($garantias[$index] == "1"): ?>
                                                    <a href="<?= API_GARANTIA ?>index.php/garantias/formato_folio_tecnico?numero_orden=<?= $garantias_id_cita[$index];?>" class="btn btn-default" target="_blank" style="font-size: 10px;background-color:  #a569bd; color:white;">GARANTÍA</a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="15" style="width:100%;" align="center">
                                            <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                                <h4>Sin registros que mostrar</h4>
                                            <?php else: ?>
                                                <h4>Sesión Expirada</h4>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="pOrigen" id="pOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                        <input type="hidden" id="origen_lista" value="Tecnico">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" autoplay style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>


<script>
    var conf_tabla = $('#bootstrap-data-table2');
    conf_tabla.DataTable({
        "language": {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": ",",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
              }
        },
        "responsive": true,
        "ordering": false,
        "searching": false,
        //"bPaginate": false,
        //"paging": false,
        //"bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        //"bAutoWidth": false,
        //"processing": true,
        //"serverSide": true
    });

    $('#btnBusqueda_tecnicos').on('click',function(){
        //Si se hizo bien la conexión, mostramos el simbolo de carga
        $(".cargaIcono").css("display","inline-block");
        //Aparentamos la carga de la tabla
        $(".campos_buscar").css("background-color","#ddd");
        $(".campos_buscar").css("color","#6f5e5e");

        //Recuperamos las fechas a consultar
        var fecha_inicio = $("#fecha_inicio").val();
        var fecha_fin = $("#fecha_fin").val();
        var campo = $("#busqueda_campo").val();
        var origen_listado = $("#origen_lista").val();

        //Identificamos la lista que se consulta
        //var lista = $("#origenRegistro").val();
        
        var base = $("#basePeticion").val();

        $.ajax({
            url: base+"ordenServicio/Documentacion_Ordenes/busqueda_documentacion", 
            method: 'post',
            data: {
                fecha_inicio : fecha_inicio,
                fecha_fin : fecha_fin,
                campo : campo,
                listado : origen_listado,
            },
            success:function(resp){
                //console.log(resp);
                //identificamos la tabla a afectar
                var tabla = $(".campos_buscar");
                tabla.empty();

                conf_tabla.DataTable().clear().draw();
                conf_tabla.DataTable().destroy();

                if (resp.indexOf("handler         </p>")<1) {

                    var data = JSON.parse(resp);

                    var base = $("#basePeticion").val();
                    var base_garantias = $("#basGarantias").val();
                    var listado_o = $("#origen_lista").val();
                    var pOrigen = $("#pOrigen").val();

                    if (data.length > 0) {
                        for(i = 0; i < data.length; i++){
                            var btnArchivos = '';
                            //console.log(celdas[15]);
                            if (data[i].oasis != "") {
                                var archivos = data[i].oasis.split("~");
                                var conteo = 1;
                                for (var j = 0; j < archivos.length; j++) {
                                    if (archivos[j] != "") {
                                        btnArchivos += "<a href='"+base+""+archivos[j]+"' class='btn btn-primary' target='_blank' style='font-size: 9px;margin-top:5px;'>"+
                                            "ARC. OASIS ("+conteo+")"+
                                        "</a>";
                                        conteo++;
                                    }
                                }
                            }

                            var poliza_g = '';
                            //console.log(celdas[15]);
                            if (data[i].doc_poliza == "1") {
                                poliza_g += "<a href='"+base+"Ver_Polizas/"+data[i].id_cita_url+"' class='btn' target='_blank' style='color:white; font-size: 9px; margin-top:2px; background-color: #008080;'>"+
                                    "P. GARANTÍA "+
                                "</a>";
                            }
                            /*if (data[i].doc_poliza.length > 0) {
                                var conteok = 1;
                                for (var jk = 0; jk < data[i].doc_poliza.length; jk++) {
                                    if (data[i].doc_poliza[jk] != "") {
                                        poliza_g += "<a href='"+base+""+data[i].doc_poliza[jk]+"' class='btn' target='_blank' style='color:white; font-size: 9px; margin-top:2px; background-color: #008080;'>"+
                                            "P. GARANTÍA ("+conteok+")"+
                                        "</a>";
                                        conteok++;
                                    }
                                }
                            }*/

                            var multipunto = '';
                            var colorBtnMultipunto = (data[i].firma_jefetaller == '1') ? 'btn-success': 'btn-danger';
                            if (data[i].multipunto == "1") {
                                multipunto = "<a href='"+base+"Multipunto_Revision/"+data[i].id_cita_url+"' class='btn "+colorBtnMultipunto+"' target='_blank' style='font-size: 9.5px;'>"+
                                    "MULTIPUNTO"+
                                "</a>";
                            }

                            var presupuesto = '';
                            if (data[i].presupuesto == "1") {
                                if (data[i].presupuesto_afectado != '') {
                                    presupuesto = "<a href='"+base+"Presupuesto_Cliente/"+data[i].id_cita_url+"' class='btn btn-warning' target='_blank' style='font-size: 9.5px;'>"+
                                        "PRESUPUESTO"+
                                    "</a>";
                                }else{
                                    presupuesto = "<a href='"+base+"Alta_Presupuesto/"+data[i].id_cita_url+"' class='btn btn-warning' target='_blank' style='font-size: 9.5px;'>"+
                                        "PRESUPUESTO"+
                                    "</a>";
                                }
                            }

                            var vc = '';
                            var audio = '';
                            if (data[i].voz_cliente == "1") {
                                vc = "<a href='"+base+"VCliente_Revision/"+data[i].id_cita_url+"' class='btn btn-secondary' target='_blank' style='font-size: 9.5px;'>"+
                                        "VOZ CLIENTE"+
                                    "</a>";
                                //Validamos si hay audio disponible
                                if (data[i].audio_vc != '') {
                                    audio = "<a class='AudioModal' id='audio_"+(i+1)+"' onclick='addAudio("+(i+1)+");' data-value='"+data[i].audio_vc+"' data-orden='"+data[i].id_cita+"' data-target='#muestraAudio' data-toggle='modal' style='color:blue;font-size:24px;'>"+
                                        "<i class='far fa-play-circle' style='font-size:24px;'></i>"+
                                    "</a>";
                                } else {
                                    audio = "<i class='fas fa-volume-mute' style='font-size:24px;color:red;'></i>";
                                }
                            }

                            var garantia_ext = "";
                            var formatos = "";
                            if ( (data[i].identificador == "T") || (data[i].identificador == "I") || (data[i].identificador == "R") || ((data[i].identificador == "G")&&(data[i].diagnostico == "0")) ) {
                                if (data[i].registros_garantias != "/") {
                                    if (data[i].registros_garantias != "0") {
                                        formatos = "<a href='"+base+"Formatos_TecGarantia/"+data[i].id_cita_url+"' class='btn "+(( data[i].cl_btnfg == 1) ? "btn-success" : "btn-danger")+"' target='_blank' style='font-size: 10px;color:white;'>"+
                                                        "Ver Formato(s) "+
                                                    "</a>";
                                    } else {
                                        formatos = "<a href='#' class='btn btn-default' target='_blank' style='font-size: 10px; color:black;'>"+
                                                        "Sin Formato(s)"+
                                                    "</a>";
                                    }

                                    garantia_ext = "<td align='center' style='vertical-align: middle;'>"+
                                                        "<a href='"+base_garantias+"garantias/F1863/form_registro_labor?perfil=<?php echo $this->session->userdata('rolIniciado'); ?>&no_orden="+data[i].id_cita+"' class='btn' target='_blank' style='font-size: 11px; background-color: #2b3188;color:white;'>F. 1863</a>"+
                                                        "<br>"+
                                                        formatos+""+
                                                    "</td>";
                                }
                            } else {
                                if (data[i].registros_garantias != "/") {
                                    garantia_ext = "<td align='center' style='vertical-align: middle;'><br></td>";
                                }
                            }

                            var garantia = "";
                            if (data[i].folio_garantia != "/") {
                                if (data[i].folio_garantia == "1") {
                                    if (listado_o == "Asesor") {
                                        garantia = "<td align='center' style='vertical-align: middle;'>"+
                                            "<a href='"+base_garantias+"index.php/garantias/formato_sin_costos?numero_orden="+data[i].garantias_id_cita+"' class='btn btn-default' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
                                                    "GARANTÍA "+
                                                "</a>"+
                                            "</td>";
                                    }else{
                                        garantia = "<td align='center' style='vertical-align: middle;'>"+
                                            "<a href='"+base_garantias+"index.php/garantias/formato_folio_tecnico?numero_orden="+data[i].garantias_id_cita+"' class='btn btn-default' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
                                                    "GARANTÍA "+
                                                "</a>"+
                                            "</td>";
                                    }
                                    
                                }else{
                                    garantia = "<td align='center' style='vertical-align: middle;'>"+
                                                "<br>"+
                                            "</td>";
                                }
                            }else if(listado_o == "Tecnico"){
                                garantia = "<td align='center' style='vertical-align: middle;'>"+
                                                "<br>"+
                                            "</td>";
                            }
                                

                            if (data[i].comentarios != "/") {
                                var comentario = "";
                                if (data[i].sesion == "JDT") {
                                    comentario = "<td align='center' style='vertical-align: middle;background-color: "+((data[i].comentarios == '1') ? '#5bc0de' : '#ffffff' )+";'>"+
                                        "<a class='addComentarioBtn' onclick='btnAddClick("+data[i].id_cita+")' title='Agregar Comentario' data-id='"+data[i].id_cita+"' data-target='#addComentario' data-toggle='modal'>"+
                                            "<i class='fas fa-comments' style='font-size: 18px;color: black;'></i>"+
                                        "</a>"+

                                        "<a class='historialCometarioBtn' onclick='btnHistorialClick("+data[i].id_cita+")' title='Historial comentarios' data-id='"+data[i].id_cita+"' data-target='#historialCometario' data-toggle='modal' style='margin-left: 20px;'>"+
                                            "<i class='fas fa-info' style='font-size: 18px;color: black;'></i>"+
                                        "</a>"+
                                    "</td>";
                                } else {
                                    comentario = "<td align='center' style='vertical-align: middle;background-color: "+((data[i].comentarios == '1') ? '#5bc0de' : '#ffffff' )+";'>"+
                                        "<a class='addComentarioBtn' onclick='btnAddClick("+data[i].id_cita+")' title='Agregar Comentario' data-id='"+data[i].id_cita+"' data-target='#addComentario' data-toggle='modal'>"+
                                            //"<i class='fas fa-comments' style='font-size: 18px;color: black;'></i>"+
                                            "<img src='"+base+"assets/imgs/img_icon/charla.png' style='width: 20px;float: left;margin-left:10px;'>"+
                                        "</a>"+

                                        "<a class='historialCometarioBtn' onclick='btnHistorialClick("+data[i].id_cita+")' title='Historial comentarios' data-id='"+data[i].id_cita+"' data-target='#historialCometario' data-toggle='modal' style='margin-left: 20px;'>"+
                                            //"<i class='fas fa-info' style='font-size: 18px;color: black;'></i>"+
                                            "<img src='"+base+"assets/imgs/img_icon/informacion.png' style='width: 20px;float: right;margin-right:10px;'>"+
                                        "</a>"+
                                    "</td>";
                                }
                                
                            }

                            var valet = '';
                            
                            if ((listado_o == "Asesor")||((listado_o == "Garantias"))) {
                                if (data[i].servicio_valet == "1") {
                                    valet = "<td align='center' style='vertical-align: middle;'>"+
                                        "<a href='"+base_2+"Servicio_valet_PDF/"+data[i].id_cita_url+"' class='btn btn-defalul' target='_blank' style='font-size: 10px;background-color:  #a569bd; color:white;'>"+
                                            "VALET"+
                                        "</a>"+
                                    "</td>";  
                                }else{
                                    valet = "<td align='center' style='vertical-align: middle;'>"+
                                        "<br>"+
                                    "</td>"; 
                                }
                            }

                            var orden_garantia = "";
                            if (data[i].folio_garantia != "/") {
                                if (data[i].folio_garantia != "0") {
                                    if (listado_o == "Tecnico") {
                                        orden_garantia = "<br> <a href='"+base+"OrdenServicio_RGTecnico/"+data[i].gid_cita_url+"' class='btn "+(( data[i].cl_btng == 1) ? "btn-success" : "btn-danger")+"' target='_blank' style='font-size: 9px;font-weight: bold; color:white;'>"+
                                                    "OR. GARANTÍA"+
                                                "</a>";
                                    }else if(listado_o == "JefeTaller"){
                                        orden_garantia = "<br> <a href='"+base+"OrdenServicio_RGJefeTaller/"+data[i].gid_cita_url+"' class='btn "+(( data[i].cl_btng == 1) ? "btn-success" : "btn-danger")+"' target='_blank' style='font-size: 9px;font-weight: bold; color:white;'>"+
                                                    "OR. GARANTÍA"+
                                                "</a>";
                                    }
                                        
                                }
                            }

                            var asociar_folio = "";
                            if (data[i].sesion == "JDT") {
                                asociar_folio = "<td align='center' style='vertical-align: middle;'>"+
                                                "<a href='"+base+"Asociacion_Folios/"+data[i].id_orden+"' class='btn' style='color:white;font-size: 9px; background-color: #9b59b6;''>ASIGNACIÓN</a>"+
                                            "</td>";
                            }

                            var kilometraje = "";
                            if (pOrigen == "PCO") {
                                kilometraje = "<td align='center' style='vertical-align: middle;'>"+data[i].km+ "</td>";
                            }

                            tabla.append("<tr style='font-size:11px;background-color: "+data[i].tipo_orden +"; '>"+
                                //NO.  ORDEN
                                "<td align='center' style='vertical-align: middle;'>"+data[i].identificador+ "-" +data[i].id_cita+ "</td>"+
                                //ORDEN INTELISIS
                                "<td align='center' style='vertical-align: middle;'>"+data[i].folio_externo+ "</td>"+
                                //APERTURA ORDEN
                                "<td align='center' style='vertical-align: middle;'>"+data[i].fecha_recepcion +" "+" </td>"+
                                //VEHÍCULO
                                "<td align='center' style='vertical-align: middle;'>"+data[i].vehiculo +"</td>"+
                                //PLACAS
                                "<td align='center' style='vertical-align: middle;'>"+data[i].placas +"</td>"+
                                //KILOMETRAJE
                                ""+kilometraje+""+
                                //ASESOR
                                "<td align='center' style='vertical-align: middle;'>"+data[i].asesor +"</td>"+
                                //TÉCNICO
                                "<td align='center' style='vertical-align: middle;'>"+data[i].tecnico +"</td>"+
                                //CLIENTE
                                "<td align='center' style='vertical-align: middle;'>"+data[i].cliente +"</td>"+
                                //ACCIONES
                                //ORDEN SERVICIO
                                "<td align='center' style='width:15%;vertical-align: middle;'>"+
                                    "<a href='"+base+"OrdenServicio_Revision/"+data[i].id_cita_url+"' class='btn btn-info' target='_blank' style='font-size: 9.5px;'>"+
                                        "ORDEN"+
                                    "</a>"+
                                    orden_garantia+""+
                                "</td>"+
                                //Archivo Oasis
                                "<td align='center' style='vertical-align: middle;'>"+
                                    btnArchivos+""+
                                    poliza_g+""+
                                "</td>"+
                                //Multipunto
                                "<td align='center' style='width:10%;'>"+
                                    multipunto+""+
                                    "<label style='color:blue;font-weight: bold;'>"+((data[i].lavado != '') ? data[i].lavado : '')+"</label>"+
                                "</td>"+
                                //Cotizacion Multipunto
                                "<td align='center' style='width:10%;'>"+
                                    presupuesto+""+
                                "</td>"+
                                //Voz Cliente
                                "<td align='center' style='width:15%;'>"+
                                    vc+""+
                                    audio+""+
                                "</td>"+
                                //SERVICIO VALET
                                ""+valet+""+
                                //G. F1863
                                ""+garantia_ext+""+
                                //FOLIO GARANTÍA
                                ""+garantia+""+
                                //ASOCIAR FOLIO GARANTÍA
                                ""+asociar_folio+""+
                                //COMENTARIOS
                                ""+comentario+""+
                            "</tr>");
                        }
                    }else{
                        tabla.append("<tr style='font-size:14px;'>"+
                            "<td align='center' colspan='15'>No se encontraron resultados</td>"+
                        "</tr>");
                    }


                }else{
                    tabla.append("<tr style='font-size:14px;'>"+
                        "<td align='center' colspan='15'>No se encontraron resultados</td>"+
                    "</tr>");
                }

                conf_tabla.DataTable({
                    "language": {
                        "decimal": "",
                        "emptyTable": "No hay información",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                        "infoPostFix": ",",
                        "thousands": ",",
                        "lengthMenu": "Mostrar _MENU_ Entradas",
                        "loadingRecords": "Cargando...",
                        "search": "Buscar:",
                        "zeroRecords": "Sin resultados encontrados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                          }
                    },
                    "responsive": true,
                    "ordering": false,
                    "searching": false,
                    //"bPaginate": false,
                    //"paging": false,
                    //"bLengthChange": false,
                    "bFilter": true,
                    "bInfo": false,
                    //"bAutoWidth": false,
                    //"processing": true,
                    //"serverSide": true
                });

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $(".campos_buscar").css("background-color","white");
                $(".campos_buscar").css("color","black");
            //Cierre de success
            },
            error:function(error){
                console.log(error);

                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
                $(".cargaIcono").css("display","none");
                //Regresamos los colores de la tabla a la normalidad
                $(".campos_buscar").css("background-color","white");
                $(".campos_buscar").css("color","black");
            //Cierre del error
            }
        //Cierre del ajax
        });
    });
</script>
