<style>
    label, input{
        font-size: 14px;    
        font-family: arial;
    }
    
</style>

<br>
<div class="row">
    <div class="col-md-2" style="padding-top: 20px;padding-left: 35px;">
        <?php if ($origen_apertura == ""): ?>
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listado_Polizas/5";?>';">
                Regresar
            </button>
        <?php endif; ?>
    </div>
    <div class="col-sm-10" style="padding-right: 35px;">
        <h3 align="right">Archivos Poliza de Garantías</h3>
    </div>
</div>

<form id="form_polizas" method="post" action="" autocomplete="on" enctype="multipart/form-data">
    <div style="margin:20px;border:1px solid darkblue; padding: 10px; ">
        <div class="row">
            <div class="col-md-3">
                <label >No. Orden : </label>&nbsp;
                <input type="text" class="form-control" name="id_cita" value="<?= ((isset($cabecera['id_cita'])) ? $cabecera['id_cita'] : "") ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label >Orden Intelisis : </label>&nbsp;
                <input type="text" class="form-control"  id="folio_externo" value="<?= ((isset($cabecera['folio_externo'])) ? $cabecera['folio_externo'] : "") ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label >No.Serie (VIN):</label>&nbsp;
                <input class="form-control" type="text"  id="serie" value="<?= ((isset($cabecera['serie'])) ? $cabecera['serie'] : "") ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label >Modelo:</label>&nbsp;
                <input class="form-control" type="text" id="modelo" value="<?= ((isset($cabecera['modelo'])) ? $cabecera['modelo'] : "") ?>" style="width:100%;" disabled>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label >Placas</label>&nbsp;
                <input type="text" class="form-control" id="placas" value="<?= ((isset($cabecera['placas'])) ? $cabecera['placas'] : "") ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label >Unidad</label>&nbsp;
                <input class="form-control" type="text" id="anio" value="<?= ((isset($cabecera['anio'])) ? $cabecera['anio'] : "") ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label >Técnico</label>&nbsp;
                <input class="form-control" type="text" id="tecnico" value="<?= ((isset($cabecera['tecnico'])) ? $cabecera['tecnico'] : "") ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label >Asesor</label>&nbsp;
                <input class="form-control" type="text" id="asesors" value="<?= ((isset($cabecera['asesor'])) ? $cabecera['asesor'] : "") ?>" style="width:100%;" disabled>
            </div>
        </div>

        <br>
        <?php if ($origen_apertura == ""): ?>
            <div class="row">
                <div class="col-sm-3"></div>

                <div class="col-sm-4" style="margin-top: 23px;">
                    <input type="file" class="btn btn-default" id="archivo" name="archivo[]"  value="" multiple>
                </div>

                <div class="col-sm-2" align="right" style="margin-top: 23px;">
                    <input type="button" class="btn btn-success" onclick="envio_archivos()" value="Guardar">
                </div>

                <div class="col-sm-3"></div>
            </div>
        <?php endif; ?>

        <br>

        <div class="form-group table-responsive" align="center">
            <i class="fas fa-spinner cargaIcono" id="carga_tabla"></i>
            <h5 class="error" id="error_carga"></h5>

            <table class="table table-bordered" style="width:99vw; font-size:14px;">
                <thead>
                    <tr>
                        <td colspan="3" align="center">
                            <h4 style="font-weight: bold;">ARCHIVOS CARGADOS</h4>
                        </td>
                    </tr>
                    <tr style="background-color: #eee;">
                        <td style="width:40%;" align="center">
                            ARCHIVO
                        </td>
                        <td align="center">
                            FECHA DE CARGA
                        </td>
                        <td style="width:30%;" align="center">
                            SUBIDO POR:
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($archivos != NULL): ?>
                        <?php $indice = 1;  ?>
                        <?php foreach ($archivos as $i => $registro): ?>
                            <tr>
                                <td align="center">
                                    <a  href="<?=base_url().''.$registro->url_archivo;?>" class="btn btn-block btn-warning" target="_blank">
                                        Archivo # <?= $indice++ ?>
                                    </a>
                                </td>
                                <td style="vertical-align: middle;" align="center">
                                    <?php 
                                        $fecha = new DateTime($registro->fecha_creacion);
                                        echo $fecha->format('d-m-Y H:i');
                                     ?>
                                </td>
                                <td align="center" style="vertical-align: middle;">
                                    <?= $registro->usuario; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="3" style="width:100%;" align="center">
                                <h4>Sin archivos que mostrar</h4>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</form>

<script>

    function envio_archivos(){
        var base = $("#basePeticion").val().trim();
        
        var archivos = $('#archivo')[0].files;

        if (archivos.length > 0) {
            $("#error_carga").text("");
            $(".cargaIcono").css("display","inline-block");

            var paqueteDatos = new FormData(document.getElementById('form_polizas'));
            paqueteDatos.append('id_cita', $("input[name='id_cita']").prop('value'));

            $.ajax({
                url: base+"api/Operaciones_Garantias/subir_polizas",
                type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
                contentType: false,
                data: paqueteDatos, // Al atributo data se le asigna el objeto FormData.
                processData: false,
                cache: false,
                success:function(resp){
                    if (resp.indexOf("handler         </p>")<1) {
                        if (resp == "OK") {
                            location.reload();
                        } else {
                            $("#error_carga").text("OCURRIO UN ERROR DURANTE LA CARGA DE ARCHIVOS");
                        }
                    }else{
                        $("#error_carga").text("OCURRIO UN ERROR DURANTE EL ENVIO COD 2");
                    }

                    $(".cargaIcono").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $("#carga_select").text("ERROR COD 3");
                    $(".cargaIcono").css("display","none");
                //Cierre del error
                }
            //Cierre del ajax
            });
        } else {
            $("#error_carga").text("NO SE SELECCIONO NINGUN ARCHIVO PARA CARGAR");
        }
    }
</script>