<div style="margin:10px;">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-4" align="left" style="padding-top: 20px;">
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/4";?>';">
                        Regresar
                    </button>
                </div>
                <div class="col-sm-8">
                    <h3 align="right">Listado de Ordenes para Cargar Polizas</h3>
                </div>
            </div>
            
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <h5 style="color:red;text-align: center;" id="error_act_doc"></h5>
                        
                        <table <?= (($ordenes != NULL) ? 'id="bootstrap-data-table"' : '') ?> class="table table-striped table-bordered table-responsive" style="width: 100%;">
                            <thead>
                                <tr style="font-size:12px;background-color: #eee;">
                                    <td align="center" style="width: 10%;vertical-align: middle;">
                                        <strong>NO. <br>ORDEN</strong>
                                    </td>
                                    <td align="center" style="width: 10%;vertical-align: middle;">
                                        <strong>ORDEN<br>INTELISIS</strong>
                                    </td>
                                    <td align="center" style="width: 10%;vertical-align: middle;">
                                        <strong>APERTURA<br>ORDEN</strong>
                                    </td>
                                    <td align="center" style="width: 10%;vertical-align: middle;">
                                        <strong>VEHÍCULO</strong>
                                    </td>
                                    <td align="center" style="width: 10%;vertical-align: middle;">
                                        <strong>PLACAS</strong>
                                    </td>
                                    <td align="center" style="width: 10%;vertical-align: middle;">
                                        <strong>ASESOR</strong>
                                    </td>
                                    <td align="center" style="width: 10%;vertical-align: middle;">
                                        <strong>TÉCNICO</strong>
                                    </td>
                                    <td align="center" style="width: 10%;vertical-align: middle;">
                                        <strong>CLIENTE</strong>
                                    </td>
                                    <td align="center" style=""><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if ($ordenes != NULL): ?>
                                    <?php foreach ($ordenes as $i => $registro): ?>
                                        <tr style="font-size:11px;background-color: <?= (($registro->id_tipo_orden == '6') ? '#fadbd8' : '#fdfefe') ?>;">
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registro->identificador."-".$registro->id_cita; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registro->folio_externo; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php 
                                                    $fecha = new DateTime($registro->fecha_recepcion.' 00:00:00');
                                                    echo $fecha->format('d-m-Y');
                                                 ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registro->vehiculo; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registro->placas; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registro->asesor; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registro->tecnico; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registro->cliente; ?>
                                            </td>
                                            <!-- Orden de servicio (Vista rapida) -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php 
                                                    $id = (double)$registro->id_cita*CONST_ENCRYPT;
                                                    $url_id = base64_encode($id);
                                                    $url = str_replace("=", "" ,$url_id);
                                                ?>

                                                <a href="<?=base_url().'Cargar_Polizas/'.$url;?>" class="btn btn-<?=(($registro->archivo_poliza == '1') ? 'success' : 'warning') ?> " style="font-size: 10px;">CARGAR ARCHIVO(S)</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="15" style="width:100%;" align="center">
                                            <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                                <h4>Sin registros que mostrar</h4>
                                            <?php else: ?>
                                                <h4>Sesión Expirada</h4>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="pOrigen" id="pOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                        <input type="hidden" id="origen_lista" value="Asesor">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>


<script>
    /*$(document).ready(function() {
        var base = $("#basePeticion").val();
        $(".cargaIcono").css("display","inline-block");

        $.ajax({
            url: base+"api/Operaciones_Garantias/cargar_lista",
            type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
            contentType: false,
            data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
            processData: false,
            cache: false,
            success:function(resp){
                //console.log(resp);
                var data = JSON.parse(resp);
                //console.log(data);

                llenar_tabla(data);

                if (estado_ref != "") {
                    $("#exportar").attr('href',base+'Exportar_Refacciones/'+estado_ref+"."+fecha_inicio+"."+fecha_fin);
                    $("#exportar").attr('hidden',false);
                }else{
                    $("#exportar").attr('hidden',true);
                }

                $(".cargaIcono").css("display","none");
            //Cierre de success
            },
            error:function(error){
                console.log(error);
                $(".cargaIcono").css("display","none");
                $('#errorVideo').text("Error de carga VERIFICAR.");
            //Cierre del error
            }
        //Cierre del ajax
        });
    });*/
</script>