<!-- Contenido principal (Formulario) -->
<section class="content" style="margin: 30px;">
    <div class="panel-flotante">
        <div class="row" style="margin-top:30px;">
            <div class="col-sm-4" align="center"></div>
            <div class="col-sm-4" align="center" style="">
                <img src="<?= base_url().$this->config->item('logo'); ?>" class="logo" style="max-width:100%;">
            </div>
            <div class="col-sm-4" align="center"></div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10" align="center">
                <h3>FORMATOS F. 1863 DE LA ORDEN <?= $id_cita ?></h3>
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            </div>
            <div class="col-sm-1"></div>
        </div>

        <?php if (count($ligas) > 0): ?>
            <?php for ($i = 0; $i < count($ligas) ; $i++): ?>
                <br>
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-4">
                        <br>
                        <a href='<?= $ligas[$i]["url"];?>' class='btn btn-lg btn-block' style="font-size: 17px; background-color: <?= (($ligas[$i]["firma_jefe_taller_completo"] == "si") ? '#109b46; color:white;' : '#c39bd3; color:black;') ?>">REPARACIÓN # <?= ($i+1) ?></a> 
                    </div>
                    <div class="col-sm-5">
                        <table class="table table-striped table-bordered table-responsive" style="width: 100%;">
                            <tr>
                                <td style="font-size: 13px;" align="center">
                                    Firma Técnico
                                </td>
                                <td style="font-size: 13px;" align="center">
                                    Firma Jefe de Taller
                                </td>
                                <td style="font-size: 13px;" align="center">
                                    Proceso terminado
                                </td>
                                <td style="font-size: 13px;" align="center">
                                    Fecha de Termino
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px;color:darkblue; vertical-align: middle;" align="center">
                                    <?= strtoupper($ligas[$i]["firma_jefe_taller_completo"]);?>
                                </td>
                                <td style="font-size: 11px;color:darkblue; vertical-align: middle;" align="center">
                                    <?= strtoupper($ligas[$i]["firma_tecnico_completo"]);?>
                                </td>
                                <td style="font-size: 11px;color:darkblue; vertical-align: middle;" align="center">
                                    <?= strtoupper($ligas[$i]["proceso_terminado"]);?>
                                </td>
                                <td style="font-size: 11px;color:darkblue; vertical-align: middle;" align="center">
                                    <?php 
                                        if ($ligas[$i]["proceso_terminado_fecha"] != NULL) {
                                            $fecha = new DateTime($ligas[$i]["proceso_terminado_fecha"]);
                                            echo $fecha->format('d-m-Y H:i:s');
                                        }else{
                                            echo "PROCESO SIN TERMINAR";
                                        }
                                    ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            <?php endfor ?>
        <?php else: ?>
            <br>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <button type="button" class="btn btn-lg btn-block btn-default" style="font-size: 17px;" >
                        SIN FORMATOS
                    </button>
                </div>
                <div class="col-sm-2"></div>
            </div>
        <?php endif ?>

        <br><br><br>
    </div>
    <br><br>
</section>
<!-- Contenido principal (Formulario) -->
