<div class="" style="margin:30px;">
	<div class="row">
		<div class="col-sm-12">
			<table style="width:100%;">
		        <tr>
		            <td width="20%">
		                <img src="<?php echo base_url(); ?>assets/imgs/logos/logo_queretaro_2.png" alt="" class="logo" style="width:160px;height:40px;">
		            </td>
		            <td style="padding-left:10px; width:66%;" colspan="2" align="center">
		                <strong>
		                    <span style="color: #337ab7;" style="">
		                        MYLSA QUERETARO, S.A. DE C.V.
		                    </span>
		                </strong>

		                <p class="justify" style="text-align: center;">
		                    AV. CONSTITUYENTES #42, COL. VILLAS DEL SOL, QUERETARO, QRO.<br>
		                    Tels. (442) 238-7400, EXT.411, Fax. (442) 238-7400   R.F.C. MYB020125CM3<br>
		                    Email:  m.cristina@fordmylsaqueretaro.mx / www.fordmylsaqueretaro.mx <br>
		                    Horario de Recepción y Entrega de Unidades: Lunes a Viernes de 7 AM a 6 PM Sábado 7 AM a 1 PM <br>
		                    Horario de Caja Lunes a Viernes 8 AM a 8 PM Sábado 9 AM a 3 PM<br>
		                </p>
		            </td>
		            <td width="20%" align="" style="padding-left:10px;" rowspan="2">
		                <div>
		                    <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:100px;height:35px;"><br>
		                    <span style="color: #337ab7;" style="font-size:9px;color: #337ab7;">ORDEN DE SERVICIO</span> <br>
		                </div>

		                <div style="border:1px solid #084f8c;height:20px;width:100px;" align="center" >
		                      <span class="" style="color:black;">
		                          <?php if (isset($orden_id_cita)) echo $orden_identificador."-".$orden_id_cita; else echo "0"; ?>
		                      </span>
		                      <br><br>
		                </div>
		            </td>
		        </tr>
		        <tr>
		            <td align="center">
		                <?php if (isset($orden_campania)): ?>
		                    <?php if ($orden_campania == 1): ?>
		                        <img src="<?php echo base_url(); ?>assets/imgs/sem_verde.png" alt="" class="logo" style="width:30px;height:30px;"><br>
		                    <?php elseif ($orden_campania == 2): ?>
		                        <img src="<?php echo base_url(); ?>assets/imgs/sem_amar.png" alt="" class="logo" style="width:30px;height:30px;"><br>
		                    <?php else: ?>
		                        <img src="<?php echo base_url(); ?>assets/imgs/sem_rojo.png" alt="" class="logo" style="width:30px;height:30px;"><br>
		                    <?php endif; ?>
		                <?php else: ?>
		                    <img src="<?php echo base_url(); ?>assets/imgs/circulo.png" alt="" class="logo" style="width:30px;height:30px;"><br>
		                <?php endif; ?>
		            </td>
		            <td align="justify" colspan="2">
		                <?php if (isset($orden_campania)): ?>
		                    <?php if ($orden_campania == 1): ?>
		                        <p align="justify" style="color:#5cb85c;">
		                            [VERDE]<br>
		                            CAMPAÑA: <?php echo $nombreCampania; ?> <br>
		                            VEHÍCULO VERIFICADO SIN ASC PENDIENTE
		                        </p>
		                    <?php elseif ($orden_campania == 2): ?>
		                        <p align="justify" style="color:#f1c40f;">
		                            [AMARILLO]<br>
		                            CAMPAÑA: <?php echo $nombreCampania; ?> <br>
		                            VEHÍCULO CON ASC PENDIENTE QUE NO REQUIERE REFACCIONES O SE TIENEN LAS REFACCIONES DISPONIBLES
		                        </p>
		                    <?php else: ?>
		                        <p align="justify" style="color:#e74c3c;">
		                            [ROJO]<br>
		                            CAMPAÑA: <?php echo $nombreCampania; ?> <br>
		                            VEHÍCULO CON ASC PENDIENTE Y NO SE TIENEN LAS REFACCIONES
		                        </p>
		                    <?php endif; ?>
		                <?php else: ?>
		                    SIN REGISTRO DE CAMPAÑA
		                <?php endif; ?>
		            </td>
		            <!-- <td></td> -->
		        </tr>
		    </table>

		    <br><br>
		    <table style="width:100%;">
		    	<tr>
		    		<td style="width: 30%">
		    			<table style="border: 1px solid #084f8c;width: 100%;">
		    				<tr style="border-bottom: 1px solid #084f8c;">
		    					<td colspan="2" align="center">
		    						Asesor
		    					</td>
		    				</tr>
		    				<tr style="border-bottom: 1px solid #084f8c;">
		    					<td colspan="2" align="center">
		    						<span style="color: #337ab7;">
					                    <?php if (isset($orden_asesor)) echo $orden_asesor; else echo "<br>"; ?>
					                </span>
		    					</td>
		    				</tr>
		    				<tr>
		    					<td align="center" style="">
					                Teléfono 
					                <span style="color: #337ab7;">
					                    <?php if (isset($orden_telefono_asesor)) echo $orden_telefono_asesor; else echo "<br> "; ?>
					                </span>
					            </td>
					            <td align="center" style="border-left: 1px solid #084f8c;">
					                Extensión 
					                <span style="color: #337ab7;">
					                    <?php if (isset($orden_extension_asesor)) echo $orden_extension_asesor; else echo "<br> "; ?>
					                </span>
					            </td>
		    				</tr>
		    			</table>
		    		</td>
		    		<td style="width: 5%;"><br></td>
					<td style="width: 30%;">
		    			<p class="justify">
	                        DESPUÉS DE 48 HORAS. DE TERMINADO SU VEHÍCULO CAUSA PENSIÓN A RAZON DE 2 SALARIOS MINIMOS GENERALES POR CADA DÍA DE RETRASO VIGENTE EN LA ZONA DE LAS INSTALACIONES DEL PROVEEDOR
	                    </p>
		    		</td>
		    		<td style="width: 5%;"><br></td>
		    		<td style="width: 30%">
		    			<table style="border: 1px solid #084f8c; width: 100%;">
		    				<tr style="border-bottom: 1px solid #084f8c;">
		    					<td align="left">
		    						Orden de Servicio
		    					</td>
		    				</tr>
		    				<tr style="border-bottom: 1px solid #084f8c;">
		    					<td align="left">
		    						Número
					                <span style="color: #337ab7;">
					                    <?php if (isset($orden_id_cita)) echo $orden_id_cita; else echo "<br>"; ?>
					                </span>
		    					</td>
		    				</tr>
		    				<tr style="">
		    					<td align="left">
		    						Número interno (torre)
					                <span style="color: #337ab7;">
					                    <?php if (isset($orden_numero_interno)) echo $orden_numero_interno; else echo "<br>"; ?>
					                </span>
		    					</td>
		    				</tr>
		    			</table>
		    		</td>
		    	</tr>
		    </table>

		    <br>
		    <table style="width:100%;">
		        <tr>
		            <td style="width:11%;">Fecha de recepción</td>
		            <td style="width:11%;border:0.5px solid #084f8c;" align="center">
		                <label for="" style="color: #337ab7;"><?php if (isset($orden_fecha_recepcion)) echo $orden_fecha_recepcion; else echo "01-01-2019"; ?></label>
		            </td>
		            <td style="width: 3%;"><br></td>
		            <td style="width:11%;">Hora de recepción</td>
		            <td style="width:11%;border:0.5px solid #084f8c;" align="center">
		                <label for="" style="color: #337ab7;"><?php if (isset($orden_hora_recepcion)) echo $orden_hora_recepcion; else echo "00:00"; ?></label>
		            </td>
		            <td style="width: 3%;"><br></td>
		            <td style="width:11%;">Fecha para entrega</td>
		            <td style="width:11%;border:0.5px solid #084f8c;" align="center">
		                <label for="" style="color: #337ab7;"><?php if (isset($orden_fecha_entrega)) echo $orden_fecha_entrega; else echo "01-01-2019"; ?></label>
		            </td>
		            <td style="width: 3%;"><br></td>
		            <td style="width:11%;">Hora entrega</td>
		            <td style="width:11%;border:0.5px solid #084f8c;" align="center">
		                <label for="" style="color: #337ab7;"><?php if (isset($orden_hora_entrega)) echo $orden_hora_entrega; else echo "00:00"; ?></label>
		            </td>
		        </tr>
		    </table>
		</div>
	</div>

	<br>
	<div class="row">
		<div class="col-sm-12">
			<table style="width:100%;border:1px solid #084f8c;">
		        <tr>
		            <td colspan="3" align="center" style="width:100%;border-bottom:1px solid #084f8c;font-size:9px;">
		                <strong>DATOS DEL CONSUMIDOR</strong>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Nombre(s) del Consumidor</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_datos_nombres)) echo $orden_datos_nombres; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Apellido Paterno</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_datos_apellido_paterno)) echo $orden_datos_apellido_paterno; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Apellido Materno</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_datos_apellido_materno)) echo $orden_datos_apellido_materno; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="3" class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Nombre de la compañía</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_nombre_compania)) echo $orden_nombre_compania; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Nombre(s) del  contacto de la compañía</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_nombre_contacto_compania)) echo $orden_nombre_contacto_compania; else echo "<br>";?>
		                </span>
		              </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Apellido Paterno del Contacto</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_ap_contacto)) echo $orden_ap_contacto; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Apellido Materno del Contacto</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_am_contacto)) echo $orden_am_contacto; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Correo Electrónico del Cosumidor</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_datos_email)) echo $orden_datos_email; else echo "<br>";?>
		                </span>
		              </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">RFC</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_rfc)) echo $orden_rfc; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Correo Electrónico de la Compañia</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_correo_compania)) echo $orden_correo_compania; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="3" class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Dirección (Calle, Nº Exterior, Nº Interior, Colonia)</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_domicilio)) echo $orden_domicilio; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Municipio / Delegación </span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_municipio)) echo $orden_municipio; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Código Postal</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_cp)) echo $orden_cp; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Estado</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_estado)) echo $orden_estado; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="2" class="borde w50">
		                <span class="color-blue fs10">Teléfono</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_telefono_movil)) echo $orden_telefono_movil; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Otro teléfono</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_otro_telefono)) echo $orden_otro_telefono; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		    </table>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<table style="width:100%;border:1px solid #084f8c;">
		        <tr>
		            <td colspan="3" align="center" style="width:100%;border-bottom:1px solid #084f8c;font-size:9px;">
		                <strong>DATOS DEL VÉHÍCULO</strong>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde">
		                <span class="color-blue fs10">Placas del vehículo</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_placas)) echo $orden_vehiculo_placas; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde">
		                <span class="color-blue fs10">Número de identificación vehícular</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_identificacion)) echo $orden_vehiculo_identificacion; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde">
		                <span class="color-blue fs10">Kilometraje</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_kilometraje)) echo $orden_vehiculo_kilometraje; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde">
		                <span class="color-blue fs10">Marca / Línea del vehículo</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_marca)) echo $orden_vehiculo_marca; else echo "<br>";?>
							<?php if(isset($orden_categoria)) echo " - ".$orden_categoria; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde">
		                <span class="color-blue fs10">Año Modelo</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_anio)) echo $orden_vehiculo_anio; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde">
		                <span class="color-blue fs10">Color</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($color)) echo $color; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		    </table>
		</div>
		<div class="col-sm-1"></div>
	</div>

	<br>
	<div class="row">
		<div class="col-sm-12">
		    <table style="width: 100%;">
		        <tr>
		            <td style="width:33%;">
		                <span class="color-blue fs10">
		                    <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 1): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Público </strong>(La reparación es pagada por el cliente)
		                </span>
		            </td>
		            <td style="width:33%;">
		                <span class="color-blue fs10">
		                    <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 2): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Garantía </strong>(la reparación es pagada por FORD México)
		                </span>
		            </td>
		            <td style="width:33%;">
		                <span class="color-blue fs10">
		                    <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 3): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Hojalatería y Pintura </strong>(Reparación de partes exteriores del vehículo)
		                </span>
		            </td>
		        </tr>
		    </table>

		    <br>
		    <table style="width: 100%;">
		        <tr>
		            <td style="width:80px;"><br></td>
		            <td style="width:50%;" align="center">
		                <span class="color-blue fs10">
		                    <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 4): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Interna </strong>(Reparación de unidades del Distribuidor)
		                </span>
		            </td>
		            <td style="width: 30px;"><br></td>
		            <td style="width:50%;" align="center">
		              <span class="color-blue fs10">
		                  <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 5): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Extensión de Garantía </strong>(órdenes con contrato de Extensión de Garantía)
		                </span>
		            </td>
		        </tr>
		    </table>
		</div>
	</div>

	<br>
	<div class="row">
		<div col-sm-12>
			<table style="width:100%;border:1px solid #084f8c;">
		        <tr>
		            <td colspan="8" align="center" style="border-bottom:1px solid #084f8c;">
		                DESCRIPCIÓN DEL TRABAJO Y DESGLOCE DEL PRESUPUESTO (COTIZACIÓN)
		            </td>
		        </tr>
		        <tr>
		            <td align="center" style="width:10%;">GP</td>
		            <td align="center" style="width:10%;">OP</td>
		            <td align="center" style="width:25%;">Descripción</td>
		            <td align="center" style="width:10%;">Precio</td>
		            <td align="center" style="width:10%;">Cantidad</td>
		            <td align="center" style="width:10%;"></td>
		            <td align="center" style="width:10%;">Incluye IVA</td>
		            <td align="center" style="width:15%;border-left:1px solid #084f8c;">Total</td>
		        </tr>

		        <?php if (isset($item_descripcion)): ?>
		            <?php foreach ($item_descripcion as $index => $valor): ?>
		                <tr>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $gp_items[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $op_items[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:25%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_descripcion[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_precio_unitario[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_cantidad[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            Dto. <?php echo $item_descuento[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_operacion[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:15%;border-left:1px solid #084f8c;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_total[$index]; ?>
		                        </span>
		                    </td>
		                </tr>
		                <?php if($index >= 8) break; ?>
		            <?php endforeach; ?>
		            
		            <?php if (isset($gp_op_HM)): ?>
		                <tr>
		                    <td colspan="7" align="center" style="border-top:0.5px solid #337ab7;">
		                        COTIZACIÓN EXTRA (MULTIPNUTO)
		                    </td>
		                    <td align="center" style="width:15%;border-left:1px solid #084f8c;"></td>
		                </tr>
		                <?php foreach ($gp_op_HM as $index => $valor): ?>
		                    <tr style="padding:0px;">
		                        <td align="center" style="padding:0px;color:black;" colspan="2">
		                            <?php echo $pieza_HM[$index]; ?>
		                        </td>
		                        <td align="center" style="width:25%;color:black;">
		                            <?php echo $descripcion_HM[$index]; ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                            <?php echo number_format($precio_HM[$index],2); ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                            <?php if (ctype_digit($cantidad_HM[$index])): ?>
		                                <?php echo number_format($cantidad_HM[$index],2); ?>
		                            <?php else: ?>
		                                <?php echo $cantidad_HM[$index]; ?>
		                            <?php endif ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                                MO : <?php echo number_format($mo_hm[$index],2); ?> 
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                                SI
		                        </td>
		                        <td align="center" style="width:15%;border-left:1px solid #084f8c;color:black;">
		                            <?php echo number_format($total_fila_HM[$index],2); ?>
		                        </td>
		                    </tr>
		                    <?php if(($index + $Coti_1) >= 7) break; ?>
		                <?php endforeach; ?>
		            <?php endif; ?>

		            <!-- Completamos los renglones -->
		            <?php if ($totalRenglones<7): ?>
		                <?php for ($i = 0 ; $i < 7-$totalRenglones; $i++): ?>
		                    <tr>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:25%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:15%;border-left:1px solid #084f8c;"><br></td>
		                    </tr>
		                <?php endfor; ?>
		            <?php endif; ?>
		        <?php else: ?>
		            <?php if (isset($gp_op_HM)): ?>
		                <tr>
		                    <td colspan="7" align="center" style="border-top:0.5px solid #337ab7;">
		                        COTIZACIÓN EXTRA (MULTIPNUTO)
		                    </td>
		                    <td align="center" style="width:15%;border-left:1px solid #084f8c;"></td>
		                </tr>
		                <?php foreach ($gp_op_HM as $index => $valor): ?>
		                    <tr style="padding:0px;">
		                        <td align="center" style="padding:0px;color:black;" colspan="2">
		                            <?php echo $pieza_HM[$index]; ?>
		                        </td>
		                        <td align="center" style="width:25%;color:black;">
		                            <?php echo $descripcion_HM[$index]; ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                            <?php echo number_format($precio_HM[$index],2); ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                            <?php if (ctype_digit($cantidad_HM[$index])): ?>
		                                <?php echo number_format($cantidad_HM[$index],2); ?>
		                            <?php else: ?>
		                                <?php echo $cantidad_HM[$index]; ?>
		                            <?php endif ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                                MO : <?php echo number_format($mo_hm[$index],2); ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                                SI
		                        </td>
		                        <td align="center" style="width:15%;border-left:1px solid #084f8c;color:black;">
		                            <?php echo number_format($total_fila_HM[$index],2); ?>
		                        </td>
		                    </tr>
		                    <?php if(($index + $Coti_1) >= 7) break; ?>
		                <?php endforeach; ?>
		            <?php endif; ?>
		            <?php for ($i = 0 ; $i < 7-$totalRenglones; $i++): ?>
		                <tr>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:25%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:15%;border-left:1px solid #084f8c;"><br></td>
		                </tr>
		            <?php endfor; ?>
		        <?php endif ?>

		        <tr class="">
		            <td colspan="7" align="right" style="border-top:1px solid #084f8c;">
		                <span style="color: black;">SUB-TOTAL :</span>
		            </td>
		            <td align="center" style="width:15%;border-left:1px solid #084f8c;border-top:1px solid #084f8c;color:black;">
		                <span style="color: #337ab7;">
		                    <?php if(isset($subtotal_items)) echo number_format($subtotal_items,2); else echo "$0.00";?>
		                </span>
		            </td>
		        </tr>
		        <tr class="">
		            <td colspan="7" align="right">
		                <span style="color: black;">I.V.A. :</span>
		            </td>
		            <td align="center" style="width:15%;border-left:1px solid #084f8c;color:black;">
		                <span style="color: #337ab7;">
		                    <?php if(isset($iva_items)) echo number_format($iva_items,2); else echo "$0.00";?>
		                </span>
		            </td>
		        </tr>
		        <tr class="">
		            <td colspan="7" align="right">
		                <span style="color: black;">PRESUPUESTO TOTAL:</span>
		            </td>
		            <td align="center" style="width:15%;border-left:1px solid #084f8c;color:black;">
		                <span style="color: #337ab7;">
		                    <?php if(isset($total_items)) echo number_format($total_items,2); else echo "$0.00";?>
		                </span>
		            </td>
		        </tr>
		        <tr class="">
		            <td colspan="7" align="right" style="border-top:1px solid #084f8c;border-left:1px solid #084f8c;color:black;">
		                <span style="color: black;">ANTICIPO:</span>
		            </td>
		            <td width="15%" align="center" style="border-top:1px solid #084f8c;color:black;border-left:1px solid #084f8c;">
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_anticipo)) echo number_format($orden_anticipo,2); else echo "$0.00";?>
		                </span>
		            </td>
		        </tr>
		        <tr class="">
		            <td colspan="7" align="right">
		                <span style="color: black;">TOTAL</span>
		            </td>
		            <td width="15%" align="center" style="color:black;border-left:1px solid #084f8c;color:black;">
		                <strong style="color: #337ab7;">
		                    <?php if(isset($presupuesto_items)) echo number_format($presupuesto_items,2); else echo "$0.00";?>
		                </strong>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="8" style="border-top:1px solid #084f8c;">
		                <p align="justify">
		                    Este presupuesto tiene una vigencia de 3 (tres) días hábiles. Si después del diagnóstico y presupuesto, durante la reparación los precios de refacciones sufríecen
		                    algún cambio debido a fluctuaciones cambiarias, antes de hacer el trabjo se recabará autorización del cleinte. El pago deberá  ser efectuado en efectivo o mediante
		                    tarjeta de crédito o débito bancaria, a la entrega de la unidad. Se requerirá  anticipo en el caso de la cláusula 14. En caso de cancelación aplican cargos descriptos en la cláusula 9.
		                </p>
		            </td>
		        </tr>        
		    </table>
		</div>
	</div>


    <br><br>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        DIAGNÓSTICO Y POSIBLES CONSECUENCIAS
                        <a id="superUsuarioA" class="h2_title_blue" data-target="#superUsuario" data-toggle="modal" style="color:white;margin-top:15px;cursor: pointer;">
                            <i class="fas fa-user"></i>
                        </a>
                    </h3>
                </div>
                <div class="panel-body">
                    <br>
                    <p align="justify" style="text-align: justify;">
                        Para elaborar el diagnóstico de <label for="" style="color:#337ab7;"><u><strong><?php if(isset($vehiculo)) echo $vehiculo; ?></strong></u></label> del vehículo,
                        autorizo en forma expresa a desarmar las partes indispensables del mismo y sus componentes, a efecto de obtener un diagnóstico adecuado de él,
                        en el entendido de que el vehículo se me devolverá en las mismas condiciones en que fuera entregado, excepto en caso de que como consecuencia
                        inevitable resulte imposible o ineficaz para su funcionamiento así entregarlo, por causa no imputable al proveedor, lo cual
                        si
                        (&nbsp;<?php if (isset($terminos)): ?>
	                        <?php if (strtoupper($terminos) == "1"): ?>
	                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
	                        <?php endif; ?>
	                    <?php endif; ?>)&nbsp;
	                    no(&nbsp;<?php if (isset($terminos)): ?>
	                        <?php if (strtoupper($terminos) == "0"): ?>
	                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
	                        <?php endif; ?>
	                    <?php endif; ?>)&nbsp;&nbsp;
                        acepto.

                        En todo caso me obligo a pagar el importe del diagnóstico y los trabajos necesarios para realizarlo en caso de no autorizar la
                        reparación en términos del contrato y acepto que el diagnóstico se realice en un plazo de 
                        <label for="" style="color:#337ab7;"><u>&nbsp;<strong><?php if(isset($dias)) echo $dias; else echo "0";?></strong>&nbsp;</u></label> 
                        días hábiles a partir de la firma del presente.
                    </p>
                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-sm-3" align="center">
                            Nombre y firma de quien elabora el diagnóstico.<br>
                            <?php if (isset($firmaDiagnostico)): ?>
			                    <?php if ($firmaDiagnostico != ""): ?>
			                        <img src="<?php echo base_url().$firmaDiagnostico; ?>" alt="" style='width:130px;height:40px;'>
			                    <?php endif; ?>
			                <?php endif; ?>
			                <br>
			                <label for="" style="color:#337ab7;"><?php if(isset($nombreDiagnostico)) echo $nombreDiagnostico; ?></label>
                        </div>
                        <div class="col-sm-3" align="center">
                            Fecha de elaboración del diagnóstico. <br>
                            <input type="date" class="input_field" style="width:3cm;" value="<?php if(isset($fecha_diagnostico)) echo $fecha_diagnostico; else echo "2019-01-01"; ?>" disabled>
                        </div>
                        <div class="col-sm-3" align="center">
                            Nombre y firma del consumidor aceptando diagnóstico.<br> 
                            <?php if (isset($firmaConsumidor1)): ?>
                                <?php if ($firmaConsumidor1 != ""): ?>
                                    <img class="marcoImg" src="<?= base_url() ?><?php if(isset($firmaConsumidor1)) {if($firmaConsumidor1 != "") echo $firmaConsumidor1; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaImgAcepta" alt="">
                                <?php else: ?>
                                    <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">

                                    <br><br>
                                    <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif ?>
                            <?php else: ?>
                                <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">

                                <br><br>
                                <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif ?>

                            <div class="error" id="error_firma_2"></div>
				            
				            <br>
				            <label for="" style="color:#337ab7;"><?php if(isset($nombreConsumido1)) echo $nombreConsumido1; ?></label>
                        </div>
                        <div class="col-sm-3" align="center">
                            Fecha aceptación. <br>
                            <input type="date" class="input_field" style="width:3cm;" value="<?php if(isset($fecha_aceptacion)) echo $fecha_aceptacion; else echo "2019-01-01"; ?>" disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        <strong>DIAGNÓSTICO Y POSIBLES CONSECUENCIAS</strong>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 table-responsive" style="width: 100%;min-width: 400px;">
                            <img src="<?php echo base_url().'assets/imgs/carroceria.png'; ?>" alt="" style="width:390px; height:50px;">
                            
                            <?php if (isset($danosImg)): ?>
                            	<?php if ($danosImg != ""): ?>
                            		<img src="<?php echo base_url().$danosImg; ?>" alt="" style="width:420px; height:500px;">
                            	<?php else: ?>
                            		<img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:420px; height:500px;">
                            	<?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:420px; height:500px;">
                            <?php endif; ?>

                            <a href="#" class="button" download="<?php if(isset($danosImg)) echo base_url().$danosImg; else echo base_url().'assets/imgs/car.jpeg';?>">Descargar diagrama</a>
                            <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car.jpeg'; ?>">
                            <input type="hidden" name="danosMarcas" id="danosMarcas" value="">
                            <canvas id="canvas_3" width="420" height="500" hidden>
                                Su navegador no soporta canvas.
                            </canvas>
                        </div>

                        <div class="col-sm-6 table-responsive" style="width: 100%;min-width: 400px;">
                            <table class="" style="width:100%; border:1px solid #084f8c;" >
			                    <tr>
			                        <td colspan="2" style="border-bottom:1px solid #084f8c;">
			                            Interiores Opera 
			                            Si(<img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">)
			                            /No(<strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>) 
			                            /No cuenta (NC)
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Llavero.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Llavero.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Llavero.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>)
			                                <?php elseif (in_array("Llavero.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Seguro rines.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("SeguroRines.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("SeguroRines.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("SeguroRines.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td colspan="2">
			                            Indicadores de falla Activados:
			                        </td>
			                    </tr>
			                    <tr>
			                        <td colspan="2" style="border-bottom: 1px solid #084f8c;">
			                            <table class="" style="width:100%; font-size:7px;color:#337ab7;" >
			                                <tr>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/05.png" style="width:30px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/02.png" style="width:30px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/07.png" style="width:30px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/01.png" style="width:30px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/06.png" style="width:30px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/04.png" style="width:30px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/03.png" style="width:30px;">
			                                    </td>
			                                </tr>
			                                <tr>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorGasolina.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorGasolina.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorGasolina.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorMantenimento.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorMantenimento.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorMantenimento.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("SistemaABS.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("SistemaABS.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("SistemaABS.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorFrenos.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorFrenos.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorFrenos.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorBolsaAire.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorBolsaAire.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorBolsaAire.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorTPMS.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorTPMS.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorTPMS.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorBateria.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorBateria.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorBateria.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                </tr>
			                            </table>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Indicador de falla activados.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("IndicadorDeFalla.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("IndicadorDeFalla.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("IndicadorDeFalla.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Rociadores y Limpiaparabrisas.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Rociadores.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Rociadores.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Rociadores.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Claxon.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Claxon.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Claxon.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Claxon.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td colspan="2" style="border-bottom:1px solid #084f8c;">
			                            Luces:
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            &nbsp;&nbsp;&nbsp;&nbsp;Delanteras.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("LucesDelanteras.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("LucesDelanteras.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("LucesDelanteras.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            &nbsp;&nbsp;&nbsp;&nbsp;Traseras.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("LucesTraseras.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("LucesTraseras.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("LucesTraseras.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            &nbsp;&nbsp;&nbsp;&nbsp;Stop.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("LucesStop.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("LucesStop.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("LucesStop.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Radio / Caratulas.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Caratulas.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Caratulas.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Caratulas.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Pantallas.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Pantallas.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Pantallas.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Pantallas.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            A/C
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("AA.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("AA.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("AA.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Encendedor.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Encendedor.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Encendedor.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Encendedor.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Vidrios.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Vidrios.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Vidrios.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Vidrios.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Espejos.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Espejos.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Espejos.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Espejos.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Seguros eléctricos.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("SegurosEléctricos.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("SegurosEléctricos.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("SegurosEléctricos.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Disco compacto.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("CD.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("CD.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("CD.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Asientos y vestiduras.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Vestiduras.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Vestiduras.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Vestiduras.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;">
			                            Tapetes.
			                        </td>
			                        <td style="border-left:1px solid #084f8c;width:30%;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Tapetes.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Tapetes.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Tapetes.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                </table>
                        </div>
                    </div>
                    
                    <div class="panel-body">

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        Cajuela / Exteriores / Documentación
                    </div>

                    <div class="row">
                        <div class="col-sm-4 table-responsive">
                            <table class="table table-bordered" style="min-width:300px;">
                                <tr class="headers_table" align="center">
			                        <td colspan="2" align="center" style="border-bottom:1px solid #084f8c;">
			                            <strong style="font-size:13px!important;">Cajuela&nbsp;&nbsp;&nbsp;&nbsp; </strong>
			                            Si(<img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">)
			                            /No(<strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>) 
			                            /No cuenta (NC)
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;width:70%;">
			                            Herramienta.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("Herramienta.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Herramienta.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Herramienta.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;">
			                            Gato / Llave.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("eLlave.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("eLlave.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("eLlave.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;">
			                            Reflejantes.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("Reflejantes.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Reflejantes.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Reflejantes.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;">
			                            Cables.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("Cables.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Cables.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Cables.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;">
			                            Extintor.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("Extintor.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Extintor.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Extintor.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td>
			                            Llanta Refacción.
			                        </td>
			                        <td align="center" style="border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("LlantaRefaccion.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("LlantaRefaccion.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("LlantaRefaccion.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
                            </table>
                        </div>

                        <div class="col-sm-8 table-responsive">
                            <div class="row">
                                <div class="col-sm-6">
                                    <table class="table table-bordered">
                                        <tr class="headers_table" align="center">
					                        <td colspan="2" style="border-bottom:1px solid #084f8c;">
					                            <label style="font-size:13px!important;">Exteriores&nbsp;&nbsp;&nbsp;&nbsp;</label>
					                            Si(<img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">)
			                            		/No(<strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>) 
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Tapones rueda.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($exteriores)): ?>
					                                <?php if (in_array("TaponesRueda.Si",$exteriores)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("TaponesRueda.No",$exteriores)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Gomas de limpiadores.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($exteriores)): ?>
					                                <?php if (in_array("Gotas.Si",$exteriores)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("Gotas.No",$exteriores)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;">
					                            Antena.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
					                            <?php if (isset($exteriores)): ?>
					                                <?php if (in_array("Antena.Si",$exteriores)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("Antena.No",$exteriores)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td>
					                            Tapón de gasolina.
					                        </td>
					                        <td align="center" style="border-left:1px solid #084f8c;">
					                            <?php if (isset($exteriores)): ?>
					                                <?php if (in_array("TaponGasolina.Si",$exteriores)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("TaponGasolina.No",$exteriores)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                </table>
                                </div>

                                <div class="col-sm-6">
                                    <table class="table table-bordered">
                                        <tr class="headers_table" align="center">
					                        <td colspan="2" style="border-bottom:1px solid #084f8c;">
					                            <label style="font-size:13px!important;">Documentación&nbsp;&nbsp;&nbsp;&nbsp;</label>
					                            Si(<img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">)
			                            		/No(<strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>) 
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Póliza garantía/Manual prop.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($documentacion)): ?>
					                                <?php if (in_array("PolizaGarantia.Si",$documentacion)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("PolizaGarantia.No",$documentacion)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Seguro de Rines.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($documentacion)): ?>
					                                <?php if (in_array("SeguroRinesDoc.Si",$documentacion)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("SeguroRinesDoc.No",$documentacion)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Certificado verificación.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($documentacion)): ?>
					                                <?php if (in_array("cVerificacion.Si",$documentacion)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("cVerificacion.No",$documentacion)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td>
					                            Tarjeta de circulación.
					                        </td>
					                        <td align="center" style="border-left:1px solid #084f8c;">
					                            <?php if (isset($documentacion)): ?>
					                                <?php if (in_array("tCirculacion.Si",$documentacion)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("tCirculacion.No",$documentacion)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12" align="center">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="width:40%">
                                                    <img src="<?php echo base_url().'assets/imgs/gas.jpeg'; ?>" style="width:50px;" alt="">
                                                </td>
                                                <td style="margin-left:5px;" colspan="2">
                                                    Nivel de Gasolina:&nbsp;&nbsp;&nbsp;&nbsp;
                                                	<label for="" style="color:#337ab7;"> <strong><?php if(isset($nivelGasolina)) echo $nivelGasolina; else echo "0/0";?></strong> </label>
                                                    <br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Deja artículos personales?</td>
                                                <td>
	                                                Si&nbsp;
	                                                [<?php if (isset($articulos)): ?>
						                                <?php if (strtoupper($articulos) == "1"): ?>
						                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
						                                    <?php endif; ?>
						                                <?php endif; ?>]
	                                                No&nbsp;
	                                                [<?php if (isset($articulos)): ?>
					                                    <?php if (strtoupper($articulos) == "0"): ?>
					                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                    <?php endif; ?>
					                               <?php endif; ?>]
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Cuales?</td>
                                                <td>
                                                    <label for="" style="color:#337ab7;"><?php if(isset($cualesArticulos))  echo $cualesArticulos; else echo "Ninguno";?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Desea reportar algo más?</td>
                                                <td>
                                                    <label for="" style="color:#337ab7;"> <?php if(isset($reporte))  echo $reporte; else echo "Ninguno";?></label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2>IMPORTANTE</h2><br>
                                <p align="justify" style="text-align: justify;">
                                    Favor de recoger su unidad en un plazo no mayor a 48 Hrs., contados a partir de la fecha en que su
                                    asesor le informe que la unidad esta lista, pasado este plazo deberá cubrir el amacenaje por guarda de
                                    vehículo conforme al contrato.<br>
                                    Manifiesto bajo protesta que soy el dueño del vehículo, o tengo orden y/o autorización de él para
                                    utilizarlo y ordenar ésta reparación, en su representación, autorizando en su nombre y en el propio
                                    la realización de los trabajos descritos, así como la colocación de las piezas, refacciones, repuestos
                                    y materiales e insumos necesarios para efectuarlos, comprometiéndome en su nombre y en el propio a pagar
                                    el importe de la reparación; también manifiesto mi conformidad con el inventario realizado y que consta
                                    en el ejemplar del presente que recibo; en cualquier caso que las partes acuerden que el vehículo vaya a
                                    ser recogido o entregado por personal del PROVEEDOR en el domicilio del CONSUMIDOR, ello solo será mediante
                                    previo acuerdo u orden del CONSUMIDOR por escrito y debidamente aceptada por el PROVEEDOR, con un costo de
                                    $<label for="" style="color:#337ab7;"><u style="color:#337ab7;">&nbsp;&nbsp;<?php if(isset($costo)) echo $costo; else ""; ?>&nbsp;&nbsp;</u></label>
                                    (MONEDA NACIONAL);
                                    será en todo caso obligación del personal del PROVEEDOR identificarse plenamente ante el CONSUMIDOR como tal.<br>
                                    Finalmente manifiesto mi conformidad con los términos y condiciones previstas en el contrato inscrito al reverso,
                                    el cual manifiesto que he leído, obligándome en lo personal y en nombre del propietario del automóvil en
                                    los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día
                                    los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día
				                    <label for="" style="color:#337ab7;"><u style="color:#337ab7;">&nbsp;&nbsp;<?php if(isset($diaValue)) echo $diaValue; else "";?>&nbsp;&nbsp;</u></label> de
				                    <label for="" style="color:#337ab7;"><u style="color:#337ab7;">&nbsp;&nbsp;<?php if(isset($mesValue)) echo $mesValue; else echo "";?>&nbsp;&nbsp;</u></label> de
				                    <label for="" style="color:#337ab7;"><u style="color:#337ab7;">&nbsp;&nbsp;<?php if(isset($anioValue)) echo $anioValue; else echo "";?>&nbsp;&nbsp;</u></label>.
                                    .
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6" align="center">
                                <?php if (isset($firmaAsesor)): ?>
				                    <?php if ($firmaAsesor != ""): ?>
				                        <img src="<?php echo base_url().$firmaAsesor; ?>" alt="" style='width:130px;height:40px;'>
				                    <?php endif; ?>
				                <?php endif; ?>
				                
				                <br>
				                <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreAsesor)) echo $nombreAsesor; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
                                <br><br>
                                Nombre y firma del asesor de servicio.
                            </div>

                            <div class="col-sm-6" align="center">
                                <?php if (isset($firmaConsumidor2)): ?>
                                	<?php if ($firmaConsumidor2 != ""): ?>
                                        <img class="marcoImg" src="<?= base_url() ?><?php if(isset($firmaConsumidor2)) {if($firmaConsumidor2 != "") echo $firmaConsumidor2; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaFirmaConsumidor" alt="">
                                        
                                    <?php else: ?>
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaConsumidor');?>" id="firmaFirmaConsumidor" alt="">

                                        <br>
                                        <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                    <?php endif ?>
                                <?php else: ?>
                                    <img class="marcoImg" src="<?= set_value('rutaFirmaConsumidor');?>" id="firmaFirmaConsumidor" alt="">

                                    <br>
                                    <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif ?>

                                <br>
                                <span class="error" id ="error_firma_4"></span>
                                <br>
                                <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreConsumidor2)) echo $nombreConsumidor2; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
                                <br><br>
                                Firma del consumidor<br>
                                ACEPTA PRESUPUESTO E INVENTARIO<br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">
                    <strong>CONTRATO DE ADHESIÓN</strong>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <table style="width:100%">
                            <tr>
                                <td style="width:35%;">
                                    <p align="left" style="text-align: left; font-size:12px;">
                                        <strong>MYLSA QUERETARO, S.A. DE C.V. </strong><br>
                                        AV. CONSTITUYENTES #42, <br>
                                        COL. VILLAS DEL SOL, QUERETARO, QRO.<br>
                                        RFC MYB020125CM3
                                    </p>
                                </td>
                                <td style="width:30%;">
                                    <p align="left" style="text-align: left; font-size:12px;">
                                        Horarios de Atención<br>
                                        Lunes a Viernes 8:00 a 18:00 hrs. <br>
                                        Sábados de 8:00 a 13:00 hrs
                                    </p>
                                </td>
                                <td style="width:35%;">
                                    <p align="justify" style="text-align: left; font-size:12px;padding-left: 15px;">
                                        Folio No.:
                                        <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<?php if(isset($folio)) echo $folio; ?>&nbsp;&nbsp;</u></label>
                                        <br>
                                        Fecha:
                                        <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<?php if(isset($fecha)) echo $fecha; ?>&nbsp;&nbsp;</u></label>
                                        <br>
                                        Hora:
                                        <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<?php if(isset($hora)) echo $hora; ?>&nbsp;&nbsp;</u></label>
                                        <br>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="font-size:12px;">
                                  Tels. (442) 238-7400, EXT.411, Fax. (442) 238-7400 <br>
                                  Email:  m.cristina@fordmylsaqueretaro.mx (Quejas y Reclamaciones)
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <br>
                <div class="panel-body">
                    <div class="col-md-12">
                        <h6 align="center" style="font-weight:bold;"><strong></strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>1.- </b> En  virtud  de  este  contrato  (*),  el  Distribuidor  presta  el  servicio  de  reparación  y/o  mantenimiento  al  Cliente  (Consumidor),
                            del  vehículo  cuyas características se detallan en este contrato.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>2.- </b> El  Cliente  expresa  ser el  dueño del  vehículo y/o  estar facultado  para  autorizar la  reparación  y/o  mantenimiento del  vehículo descrito
                            en  el  presente contrato,  por lo que acepta  las condiciones y términos bajo los cuales se realizará  la prestación del  servicio descrita  en el  presente contrato. Asimismo,
                            es sabedor de las posibles consecuencias que puede sufrir el vehículo con motivo de su reparación y/o mantenimiento y se responsabiliza de las mismas. El consumidor acepta haber
                            tenido a la vista los precios por mano de obra,  partes y/o refacciones a emplear en las operaciones a efectuar por parte del Distribuidor.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>3.- </b> El precio total por concepto de la prestación del servicio de reparación y/o mantenimiento será cubierto en las instalaciones del Distribuidor y en moneda nacional
                            en la forma y términos expresados en este contrato,  incluyendo,  en su caso,  las partes y/o  refacciones y los servicios adicionales que el  cliente haya aceptado previamente.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>4.- </b> En la situación de que el Cliente solicite, o en su caso, el Distribuidor avise al Cliente de servicios adicionales a los establecidos en el presente contrato, éste
                            último los podrá autorizar vía telefónica. Asimismo,  todas las quejas y sugerencias serán atendidas en el domicilio, teléfonos y horarios de atención señalados en el  carátula
                            o anverso del  presente contrato.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>5.- </b> Las condiciones generales del vehículo materia de reparación y/o mantenimiento,  son las siguientes:
                            Exteriores:
                            ( <?php if(isset($condiciones)) if(in_array("Limpiadores",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?>  )  Limpiadores (plumas);
                            ( <?php if(isset($condiciones)) if(in_array("Luces",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> )  Unidades de luces;
                            ( <?php if(isset($condiciones)) if(in_array("Antena",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Antena;
                            ( <?php if(isset($condiciones)) if(in_array("Espejos",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Espejos  laterales;
                            ( <?php if(isset($condiciones)) if(in_array("Cristales",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> )  Cristales;
                            ( <?php if(isset($condiciones)) if(in_array("Tapones",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> )  Tapones  de  ruedas;
                            ( <?php if(isset($condiciones)) if(in_array("Molduras",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> )  Molduras  completas;
                            ( <?php if(isset($condiciones)) if(in_array("TapoGasolina",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Tapón  de  gasolina;
                            ( <?php if(isset($condiciones)) if(in_array("Claxon",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> )  Claxon;
                            Interiores:
                            ( <?php if(isset($condiciones)) if(in_array("Inst.Tableros",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Instrumentos de tablero;
                            ( <?php if(isset($condiciones)) if(in_array("Calefacción",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Calefacción;
                            ( <?php if(isset($condiciones)) if(in_array("AA",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Aire acondicionado;
                            ( <?php if(isset($condiciones)) if(in_array("Radio/Tipo",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Radiompo;
                            ( <?php if(isset($condiciones)) if(in_array("Bocinas",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Bocinas;
                            ( <?php if(isset($condiciones)) if(in_array("Encendedor",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Encendedor;
                            ( <?php if(isset($condiciones)) if(in_array("EspejoRet",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Espejo retrovisor;
                            ( <?php if(isset($condiciones)) if(in_array("Ceniceros",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Ceniceros;
                            ( <?php if(isset($condiciones)) if(in_array("Cinturones",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Cinturones de  seguridad;
                            ( <?php if(isset($condiciones)) if(in_array("Tapetes",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Tapetes;
                            ( <?php if(isset($condiciones)) if(in_array("Manijas",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Manijas  y/o controles  interiores;
                            ( <?php if(isset($condiciones)) if(in_array("Equipo",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Equipo adicional;
                            ( <?php if(isset($condiciones)) if(in_array("Accesorios",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Accesorios;
                            ( <?php if(isset($condiciones)) if(in_array("Aditamentos",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Aditamentos especiales:
                            ( <?php if(isset($condiciones)) if(in_array("Otros",$condiciones)) echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) Otros.
                            El  vehículo se encuentra en las siguientes condiciones generales:<br>
                            Aspectos mecánicos: <label class="colorResp"><u>&nbsp;&nbsp;<strong style="color:darkblue;"><?php if(isset($aspMecanico)) echo $aspMecanico; else echo "Sin registrar"; ?></strong>&nbsp;&nbsp;</u></label>
                            Aspectos de carrocería: <label class="colorResp"><u>&nbsp;&nbsp;<strong style="color:darkblue;"><?php if(isset($aspCarroceria)) echo $aspCarroceria; else echo "Sin registrar"; ?></strong>&nbsp;&nbsp;</u></label>
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>6.- </b> La prestación del servicio de reparación y/o mantenimiento del vehículo materia de este contrato, se otorga:
                            ( <?php if(isset($garantia)) if($garantia == "Sin") echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) sin garantía;
                            ( <?php if(isset($garantia)) if($garantia == "Con") echo "<img src='".$checkImgA."' style='width:10px;'>";?> ) con garantía
                            por un plazo de  <label class="colorResp"><u>&nbsp;&nbsp;<strong style="color:darkblue;"><?php if(isset($plazo)) echo $plazo; else echo "Sin registrar"; ?></strong>&nbsp;&nbsp;</u></label>
                            (Art.  77  de  la  LFPC*  no  podrá  ser  inferior a  90  días)  contados  a  partir de  la  entrega  del  vehículo.  Para  la  garantía  en  partes,  piezas, refacciones y
                            accesorios,  El Distribuidor transmitirá la otorgada por el fabricante,  la garantía deberá hacerse válida en el domicilio, teléfonos y horarios de atención señalados en la
                            carátula o anverso del presente contrato,  siempre y cuando no se haya efectuado una reparación por un tercero.  El tiempo que dure  la  reparación  y/o  mantenimiento  del
                            vehículo,  bajo  la  protección  de  la  garantía,  no  es  computable  dentro  del  plazo  de  la  misma.  Las  partes  y/o refacciones empleadas en  la  reparación y/o mantenimiento
                            del  vehículo materia de este contrato,  son  nuevas y apropiadas  para  el  funcionamiento del mismo.  De igual forma,  los gastos en que incurra el Cliente para hacer válida la
                            garantía en un domicilio diverso al del  Distribuidor,  deberán ser cubiertos por éste.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>7.- </b> El  Distribuidor será el responsable por las descomposturas, daños o pérdidas parciales o totales imputables a él,  mientras el vehículo se encuentre bajo su resguardo
                            para llevar a cabo la prestación del servicio de reparación y/o mantenimiento, o como consecuencia de la prestación del  servicio,  o bien, en el cumplimiento de la garantía,  de acuerdo
                            a  lo establecido en el presente contrato. Asimismo,  el  Cliente autoriza  al  Distribuidor a  usar el vehículo para efectos  de  prueba  o  verificación  de  las operaciones  a  realizar
                            o  realizadas.  El  Cliente  libera  al  Distribuidor de  cualquier responsabilidad  que  hubiere surgido o pudiera surgir con relación al origen,  propiedad o posesión del vehículo.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>8.- </b> El  cliente podrá  revocar su consentimiento,  en  un  plazo de 5  días  hábiles  mediante  aviso  personal,  correo electrónico o  correo certificado,
                            siempre y cuando no se hayan iniciado los trabajos de reparación y/o mantenimiento.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>9.- </b> En caso de que apliquen  restricciones,  estas se le darán a conocer al cliente.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>10.- </b>  En caso de que el  consumidor cancele la operación, está  obligado a  pagar de manera  inmediata  y  previa a  la entrega del vehículo,  el  importe de
                            las operaciones efectuadas y partes y/o refacciones colocadas o adquiridas hasta el  retiro del  mismo.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>11.- </b> Son  causas  de  rescisión  del  presente contrato:  (i) Que  el  Distribuidor incumpla  en  la fecha  y lugar de  entrega  del  vehículo  por causas  imputables a  él.
                            -El  Cliente  le  notificará  por escrito  el  incumplimiento  de  dicha  obligación  y  el  Distribuidor entregará  de  manera  inmediata  el  vehículo,  debiendo descontar del  monto  total  de  la  operación,
                            la  cantidad  equivalente  al <label class="colorResp"><u>&nbsp;&nbsp;<strong style="color:darkblue;"><?php if(isset($penaConveniencia)) echo $penaConveniencia; else echo "Sin registrar"; ?></strong>&nbsp;&nbsp;</u></label> % 
                            por concepto de  pena  convencional  (ii) Que  el  Cliente  incumpla  con  su obligación  de  pago.
                            - En  el  evento que el  Cliente  incumpla  con  el  pago  por concepto de  la  reparación y/o mantenimiento del  vehículo,  el  Distribuidor le notificará  por escrito su incumplimiento y
                            podrá  exigirle la rescisión o cumplimiento  del  contrato  por mora,  más la pena  convencional  del <label class="colorResp"><u>&nbsp;&nbsp;<strong style="color:darkblue;"><?php if(isset($totalOperacion)) echo $totalOperacion; else echo "Sin registrar"; ?></strong>&nbsp;&nbsp;</u></label> % 
                            del monto total de la operación.  Las penas convencionales deberán ser equitativas y de la misma magnitud para  las partes.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>12.- </b> El Consumidor deberá recoger el vehículo en la fecha y lugar establecida en el  presente contrato,  en caso contrario, se obliga a pagar al  Distribuidor, la  cantidad  que  resulte
                            por concepto  de almacenaje  del  vehículo  por cada  día que transcurra,  tomando como  referencia  una  tarifa  no mayor al  precio general  establecido para  estacionamientos  públicos  ubicados
                            en  la  localidad  del  Distribuidor.  Transcurrido  un  plazo  de  15 días  naturales  a  partir de la fecha señalada para la entrega del vehículo,  y el  Cliente no acuda a recoger el  mismo, el
                            Distribuidor sin responsabilidad alguna, pondrá a disposición de la  autoridad  correspondiente  dicho  vehículo.  Sin  perjuicio  de  lo  anterior,  el  Distribuidor  podrá  realizar  el  cobro
                            correspondiente  por  concepto  de almacenaje.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>13.- </b> El Distribuidor se obliga a expedir la factura o comprobante de pago por las operaciones efectuadas, en  la cual  se especificarán los precios por mano de obra,  refacciones,  materiales
                            y accesorios empleados,  así como la garantía  que en su caso se otorgue,  conforme al  artículo 62 de la  Ley Federal  de Protección al Consumidor.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>14.- </b> El  Distribuidor se obliga a:  (i) No ceder o transmitir a terceros,  con fines mercadotécnicos o publicitarios,  los datos e  información proporcionada por el  consumidor con  motivo del
                            presente  contrato  (ii)  No  enviar  publicidad  sobre  bienes  y  servicios,  salvo  autorización  expresa  del  consumidor en  la presente cláusula.
                        </p>

                        <!-- Firma del consumidor de aceptacion -->
                        <br>
                        <p align="center">
                            <?php if (isset($firmaCliente)): ?>
                                <?php if ($firmaCliente != ""): ?>
                                    <img class='marcoImg' src='<?= base_url() ?><?php if(isset($firmaCliente)) {if($firmaCliente != "") echo $firmaCliente; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>' id='' style='width:130px;height:40px;'>
                                <?php else: ?>
                                    <img class="marcoImg" src="" id="firmaFirmaConsumi_2">

                                    <br>
                                    <a class="cuadroFirma firmaCliente btn btn-primary" data-value="El_Consumidor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <img class="marcoImg" src="" id="firmaFirmaConsumi_2">

                                <br>
                                <a class="cuadroFirma firmaCliente btn btn-primary" data-value="El_Consumidor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif; ?>

                            <br>
		                	<label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreConsumidor)) echo $nombreConsumidor; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>

                            <br>
                            <span class="error" id ="error_firma_5"></span>
                            <br>
                            Firma o rubrica de autorización del  consumidor
                        </p>

                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>15.-</b> Las partes están de acuerdo en someterse a la competencia de la Procuraduría Federal del Consumidor en la vía administrativa para resolver cualquier
                            controversia que se suscite sobre  la  interpretación o cumplimiento de los términos y condiciones del  presente  contrato y de las disposiciones de la  Ley Federal
                            de  Protección  al  Consumidor,  la  Norma  Oficial  Mexicana  NOM-174-SCFl-2007,  Prácticas  comerciales-Elementos  de  información  para  la prestación de servicios
                            en general y cualquier otra disposición  aplicable,  sin perjuicio del derecho que tienen  las partes de someterse a la jurisdicción de los Tribunales competentes del
                            domicilio del Distribuidor,  renunciando las partes expresamente a  cualquier otra jurisdicción que pudiera corresponderles por razón de sus domicilios futuros.
                        </p>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>15.-</b> El  Cliente y el  Distribuidor aceptan la realización  de la prestación  del  servicio de reparación  y/o mantenimiento,  en  los términos establecidos en
                            este contrato, y sabedores de su alcance legal, lo firman por duplicado.
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6" align="center">
                        EL DISTRIBUIDOR (NOMBRE Y FIRMA).
                        <br>
                        <?php if (isset($firmaDistribuidor)): ?>
		                    <?php if ($firmaDistribuidor != ""): ?>
		                        <img src="<?php echo base_url().$firmaDistribuidor; ?>" alt="" style='width:130px;height:40px;'>
		                    <?php endif; ?>
		                <?php endif; ?>
		                
		                <br>
		                <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreDistribuidor)) echo $nombreDistribuidor; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
                    </div>

                    <div class="col-md-6" align="center">
                        EL CLIENTE (NOMBRE Y FIRMA)
                        <br>
                        <?php if (isset($firmaConsumidor)): ?>
                            <?php if ($firmaConsumidor != ""): ?>
                                <img class='marcoImg' src='<?= base_url() ?><?php if(isset($firmaCliente)) {if($firmaCliente != "") echo $firmaConsumidor; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>' id='' style='width:130px;height:40px;'>

                            <?php else: ?>
                                <img class="marcoImg" src="" id="firmaFirmaConsumidor_3" alt="">
                                <br>

                                <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_3" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif; ?>
                        <?php else: ?>
                            <img class="marcoImg" src="" id="firmaFirmaConsumidor_3" alt="">
                            <br>

                            <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Consumidor_3" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                        <?php endif; ?>

                        <br>
		                <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreConsumidor)) echo $nombreConsumidor; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
                        
                        <br>
                        <span class="error" id ="error_firma_7"></span>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="col-md-12" align="center">
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>(*)</b> El presente contrato fue registrado en la Procuraduría Federal del Consumidor bajo número 8524-2018 de fecha 30 DE AGOSTO DE 2018.<br>
                            *LFPC.- Ley Federal de Protección al Consumidor.
                        </p>

                        <h6 style="text-align: center"><strong>AVISO DE PRIVACIDAD </strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            EL Aviso de Privacidad Integral podrá ser consultado en nuestra página en internet <strong>http://fordmylsaqueretaro.mx/Privacidad/</strong>, o lo puede
                            solicitar al correo electrónico <strong>m.cristina@fordmylsaqueretaro.mx </strong>, u obtener personalmente en el Área de Atención a la Privacidad,
                            en nuestras instalaciones.<br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br><br>
    <?php if (isset($terminos_contrato)): ?>
    	<?php if ($terminos_contrato == "0"): ?>
    		<form id="formulario_cliente" method="post" action="" autocomplete="on" enctype="multipart/form-data">
		    	<input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?php if(isset($firmaConsumidor1)) echo $firmaConsumidor1; ?>">
				<input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?php if(isset($firmaConsumidor2)) echo $firmaConsumidor2; ?>">
				<input type="hidden" id="rutaFirmaConsumidor_2" name="rutaFirmaConsumidor_2" value="<?php if(isset($firmaCliente)) echo $firmaCliente; ?>">
				<input type="hidden" id="rutaFirmaConsumidor_3" name="rutaFirmaConsumidor_3" value="<?php if(isset($firmaConsumidor)) echo $firmaConsumidor; ?>">

				<input type="hidden" name="id_cita" id="" value="<?php if(isset($id_cita)) echo $id_cita; ?>">
				<input type="hidden" name="cargaFormulario" id="cargaFormulario" value="<?= ((isset($tipo_form)) ? $tipo_form : '1') ?>">

				<br>
				<div class="row" align="center">
		            <div class="col-md-12"  align="center">
		            	<p align="justify" style="text-align: justify; font-size:14px;font-weight: bold;">
		                    <input class="camposContrato" type="checkbox" value="1" name="terminos_contrato" <?php if(isset($terminos_contrato)) if($terminos_contrato == "1") echo "checked"; ?>>&nbsp;&nbsp;Acepto Términos y Condiciones del Contrato de Adhesión.
		                    <br>
		                    <span class="error" id ="error_terminos_contrato"></span>
		                </p>

		                 <br>   
						<i class="fas fa-spinner cargaIcono"></i>
				        <span class="error" id ="formulario_error"></span>

				        <br>
				        <input type="button" class="btn btn-success" id="envio_diag" value="Guardar Contrato">
					</div>
				</div>
		    </form>
		<?php else: ?>
			<div class="row" align="center">
	            <div class="col-md-12"  align="center">
	            	<p align="justify" style="text-align: justify; font-size:14px;font-weight: bold;">
	                    <input class="camposContrato" type="checkbox" readonly value="1" name="terminos_contrato" <?php if(isset($terminos_contrato)) if($terminos_contrato == "1") echo "checked"; ?> disabled>&nbsp;&nbsp;Acepto Términos y Condiciones del Contrato de Adhesión.
	                    <br>
	                    <span class="error" id ="error_terminos_contrato"></span>
	                </p>
				</div>
			</div>
    	<?php endif ?>
    <?php else: ?>
    	<div class="row" align="center">
            <div class="col-md-12"  align="center">
            	<p align="justify" style="text-align: justify; font-size:14px;font-weight: bold;">
                    <input class="camposContrato" type="checkbox" readonly value="1" name="terminos_contrato" <?php if(isset($terminos_contrato)) if($terminos_contrato == "1") echo "checked"; ?> disabled>&nbsp;&nbsp;Acepto Términos y Condiciones del Contrato de Adhesión.
                    <br>
                    <span class="error" id ="error_terminos_contrato"></span>
                </p>
			</div>
		</div>
    <?php endif ?>
		    
    <br><br>

	<!-- Modal para la firma del quien elaboro el diagnóstico-->
	<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
	            </div>
	            <div class="modal-body">
	                <!-- signature -->
	                <div class="signatureparent_cont_1">
	                    <!-- <div id="signature" ></div> -->
	                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
	                        Su navegador no soporta canvas
	                    </canvas>
	                    <!-- <p id="limpiar">limpiar canvas</p> -->
	                </div>
	                <!-- signature -->
	            </div>
	            <div class="modal-footer">
	                <input type="hidden" name="" id="destinoFirma" value="">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
	                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
	                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
	            </div>
	        </div>
	    </div>
	</div>
