<div class="" style="margin:30px;">
	<div class="row">
		<div class="col-sm-12">
			<table style="width:100%;">
		        <tr>
		            <td width="20%">
		                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:100%;">
		            </td>
		            <td style="padding-left:10px; width:66%;" colspan="2" align="center">
		                <strong>
		                    <span style="color: #337ab7;" style="">
		                        <?= SUCURSAL ?>, S.A. DE C.V.
		                    </span>
		                </strong>

		                <p class="justify" style="text-align: center;">
		                    <?=$this->config->item('encabezados_txt')?>
		                </p>
		            </td>
		            <td width="20%" align="" style="padding-left:10px;" rowspan="2">
		                <div>
		                    <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:100px;height:35px;"><br>
		                    <span style="color: #337ab7;" style="font-size:9px;color: #337ab7;">ORDEN DE SERVICIO</span> <br>
		                </div>

		                <div style="border:1px solid #084f8c;height:20px;width:100px;" align="center" >
		                      <span class="" style="color:black;">
		                          <?php if (isset($idIntelisis)) echo $idIntelisis; else echo "0"; ?>
		                      </span>
		                      <br><br>
		                </div>
		            </td>
		        </tr>
		        <tr>
		            <td align="center">
		                <?php if (isset($orden_campania)): ?>
		                    <?php if ($orden_campania == 1): ?>
		                        <img src="<?php echo base_url(); ?>assets/imgs/sem_verde.png" alt="" class="logo" style="width:30px;height:30px;"><br>
		                    <?php elseif ($orden_campania == 2): ?>
		                        <img src="<?php echo base_url(); ?>assets/imgs/sem_amar.png" alt="" class="logo" style="width:30px;height:30px;"><br>
		                    <?php else: ?>
		                        <img src="<?php echo base_url(); ?>assets/imgs/sem_rojo.png" alt="" class="logo" style="width:30px;height:30px;"><br>
		                    <?php endif; ?>
		                <?php else: ?>
		                    <img src="<?php echo base_url(); ?>assets/imgs/circulo.png" alt="" class="logo" style="width:30px;height:30px;"><br>
		                <?php endif; ?>
		            </td>
		            <td align="justify" colspan="2">
		                <?php if (isset($orden_campania)): ?>
		                    <?php if ($orden_campania == 1): ?>
		                        <p align="justify" style="color:#5cb85c;">
		                            [VERDE]<br>
		                            CAMPAÑA: <?php echo $nombreCampania; ?> <br>
		                            VEHÍCULO VERIFICADO SIN ASC PENDIENTE
		                        </p>
		                    <?php elseif ($orden_campania == 2): ?>
		                        <p align="justify" style="color:#f1c40f;">
		                            [AMARILLO]<br>
		                            CAMPAÑA: <?php echo $nombreCampania; ?> <br>
		                            VEHÍCULO CON ASC PENDIENTE QUE NO REQUIERE REFACCIONES O SE TIENEN LAS REFACCIONES DISPONIBLES
		                        </p>
		                    <?php else: ?>
		                        <p align="justify" style="color:#e74c3c;">
		                            [ROJO]<br>
		                            CAMPAÑA: <?php echo $nombreCampania; ?> <br>
		                            VEHÍCULO CON ASC PENDIENTE Y NO SE TIENEN LAS REFACCIONES
		                        </p>
		                    <?php endif; ?>
		                <?php else: ?>
		                    SIN REGISTRO DE CAMPAÑA
		                <?php endif; ?>
		            </td>
		            <!-- <td></td> -->
		        </tr>
		    </table>

		    <br><br>
		    <table style="width:100%;">
		    	<tr>
		    		<td style="width: 30%">
		    			<table style="border: 1px solid #084f8c;width: 100%;">
		    				<tr style="border-bottom: 1px solid #084f8c;">
		    					<td colspan="2" align="center">
		    						Asesor
		    					</td>
		    				</tr>
		    				<tr style="border-bottom: 1px solid #084f8c;">
		    					<td colspan="2" align="center">
		    						<span style="color: #337ab7;">
					                    <?php if (isset($orden_asesor)) echo $orden_asesor; else echo "<br>"; ?>
					                </span>
		    					</td>
		    				</tr>
		    				<tr>
		    					<td align="center" style="">
					                Teléfono 
					                <span style="color: #337ab7;">
					                    <?php if (isset($orden_telefono_asesor)) echo $orden_telefono_asesor; else echo "<br> "; ?>
					                </span>
					            </td>
					            <td align="center" style="border-left: 1px solid #084f8c;">
					                Extensión 
					                <span style="color: #337ab7;">
					                    <?php if (isset($orden_extension_asesor)) echo $orden_extension_asesor; else echo "<br> "; ?>
					                </span>
					            </td>
		    				</tr>
		    			</table>
		    		</td>
		    		<td style="width: 5%;"><br></td>
					<td style="width: 30%;">
		    			<p class="justify">
	                        DESPUÉS DE 48 HORAS. DE TERMINADO SU VEHÍCULO CAUSA PENSIÓN A RAZON DE 2 SALARIOS MINIMOS GENERALES POR CADA DÍA DE RETRASO VIGENTE EN LA ZONA DE LAS INSTALACIONES DEL PROVEEDOR
	                    </p>
		    		</td>
		    		<td style="width: 5%;"><br></td>
		    		<td style="width: 30%">
		    			<table style="border: 1px solid #084f8c; width: 100%;">
		    				<tr style="border-bottom: 1px solid #084f8c;">
		    					<td align="left">
		    						Orden de Servicio
		    					</td>
		    				</tr>
		    				<tr style="border-bottom: 1px solid #084f8c;">
		    					<td align="left">
		    						Número
					                <span style="color: #337ab7;">
					                    <?php if (isset($orden_id_cita)) echo $orden_id_cita; else echo "<br>"; ?>
					                </span>
		    					</td>
		    				</tr>
		    				<tr style="">
		    					<td align="left">
		    						Número interno (torre)
					                <span style="color: #337ab7;">
					                    <?php if (isset($orden_numero_interno)) echo $orden_numero_interno; else echo "<br>"; ?>
					                </span>
		    					</td>
		    				</tr>
		    			</table>
		    		</td>
		    	</tr>
		    </table>

		    <br>
		    <table style="width:100%;">
		        <tr>
		            <td style="width:11%;">Fecha de recepción</td>
		            <td style="width:11%;border:0.5px solid #084f8c;" align="center">
		                <label for="" style="color: #337ab7;"><?php if (isset($orden_fecha_recepcion)) echo $orden_fecha_recepcion; else echo "01-01-2019"; ?></label>
		            </td>
		            <td style="width: 3%;"><br></td>
		            <td style="width:11%;">Hora de recepción</td>
		            <td style="width:11%;border:0.5px solid #084f8c;" align="center">
		                <label for="" style="color: #337ab7;"><?php if (isset($orden_hora_recepcion)) echo $orden_hora_recepcion; else echo "00:00"; ?></label>
		            </td>
		            <td style="width: 3%;"><br></td>
		            <td style="width:11%;">Fecha para entrega</td>
		            <td style="width:11%;border:0.5px solid #084f8c;" align="center">
		                <label for="" style="color: #337ab7;"><?php if (isset($orden_fecha_entrega)) echo $orden_fecha_entrega; else echo "01-01-2019"; ?></label>
		            </td>
		            <td style="width: 3%;"><br></td>
		            <td style="width:11%;">Hora entrega</td>
		            <td style="width:11%;border:0.5px solid #084f8c;" align="center">
		                <label for="" style="color: #337ab7;"><?php if (isset($orden_hora_entrega)) echo $orden_hora_entrega; else echo "00:00"; ?></label>
		            </td>
		        </tr>
		    </table>
		</div>
	</div>

	<br>
	<div class="row">
		<div class="col-sm-12">
			<table style="width:100%;border:1px solid #084f8c;">
		        <tr>
		            <td colspan="3" align="center" style="width:100%;border-bottom:1px solid #084f8c;font-size:9px;">
		                <strong>DATOS DEL CONSUMIDOR</strong>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Nombre(s) del Consumidor</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_datos_nombres)) echo $orden_datos_nombres; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Apellido Paterno</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_datos_apellido_paterno)) echo $orden_datos_apellido_paterno; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Apellido Materno</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_datos_apellido_materno)) echo $orden_datos_apellido_materno; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="3" class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Nombre de la compañía</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_nombre_compania)) echo $orden_nombre_compania; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Nombre(s) del  contacto de la compañía</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_nombre_contacto_compania)) echo $orden_nombre_contacto_compania; else echo "<br>";?>
		                </span>
		              </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Apellido Paterno del Contacto</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_ap_contacto)) echo $orden_ap_contacto; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Apellido Materno del Contacto</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_am_contacto)) echo $orden_am_contacto; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Correo Electrónico del Cosumidor</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_datos_email)) echo $orden_datos_email; else echo "<br>";?>
		                </span>
		              </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">RFC</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_rfc)) echo $orden_rfc; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Correo Electrónico de la Compañia</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_correo_compania)) echo $orden_correo_compania; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="3" class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Dirección (Calle, Nº Exterior, Nº Interior, Colonia)</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_domicilio)) echo $orden_domicilio; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;">
		                <span class="color-blue fs10">Municipio / Delegación </span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_municipio)) echo $orden_municipio; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Código Postal</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_cp)) echo $orden_cp; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-bottom:0.5px solid #337ab7;border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Estado</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_estado)) echo $orden_estado; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="2" class="borde w50">
		                <span class="color-blue fs10">Teléfono</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_telefono_movil)) echo $orden_telefono_movil; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde" style="border-left: 0.5px solid #337ab7;">
		                <span class="color-blue fs10">Otro teléfono</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_otro_telefono)) echo $orden_otro_telefono; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		    </table>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-1"></div>
		<div class="col-sm-10">
			<table style="width:100%;border:1px solid #084f8c;">
		        <tr>
		            <td colspan="3" align="center" style="width:100%;border-bottom:1px solid #084f8c;font-size:9px;">
		                <strong>DATOS DEL VÉHÍCULO</strong>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde">
		                <span class="color-blue fs10">Placas del vehículo</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_placas)) echo $orden_vehiculo_placas; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde">
		                <span class="color-blue fs10">Número de identificación vehícular</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_identificacion)) echo $orden_vehiculo_identificacion; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde">
		                <span class="color-blue fs10">Kilometraje</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_kilometraje)) echo $orden_vehiculo_kilometraje; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		        <tr>
		            <td class="borde">
		                <span class="color-blue fs10">Marca / Línea del vehículo</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_marca)) echo $orden_vehiculo_marca; else echo "<br>";?> 
							<?php if(isset($orden_categoria)) echo " - ".$orden_categoria; else echo "<br";?>
		                </span>
		            </td>
		            <td class="borde">
		                <span class="color-blue fs10">Año Modelo</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_vehiculo_anio)) echo $orden_vehiculo_anio; else echo "<br>";?>
		                </span>
		            </td>
		            <td class="borde">
		                <span class="color-blue fs10">Color</span><br>
		                <span style="color: #337ab7;">
		                    <?php if(isset($color)) echo $color; else echo "<br>";?>
		                </span>
		            </td>
		        </tr>
		    </table>
		</div>
		<div class="col-sm-1"></div>
	</div>

	<br>
	<div class="row">
		<div class="col-sm-12">
		    <table style="width: 100%;">
		        <tr>
		            <td style="width:33%;">
		                <span class="color-blue fs10">
		                    <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 1): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Público </strong>(La reparación es pagada por el cliente)
		                </span>
		            </td>
		            <td style="width:33%;">
		                <span class="color-blue fs10">
		                    <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 2): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Garantía </strong>(la reparación es pagada por FORD México)
		                </span>
		            </td>
		            <td style="width:33%;">
		                <span class="color-blue fs10">
		                    <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 3): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Hojalatería y Pintura </strong>(Reparación de partes exteriores del vehículo)
		                </span>
		            </td>
		        </tr>
		    </table>

		    <br>
		    <table style="width: 100%;">
		        <tr>
		            <td style="width:33%;">
		                <span class="color-blue fs10">
		                    <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 4): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Interna </strong>(Reparación de unidades del Distribuidor)
		                </span>
		            </td>
		            <td style="width:33%;">
		              <span class="color-blue fs10">
		                  <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 5): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Extensión de Garantía </strong>(órdenes con contrato de Extensión de Garantía)
		                </span>
		            </td>
		            <td style="width:33%;">
		              <span class="color-blue fs10">
		                  <span style="color: #000" style="color: #337ab7;">
		                        <?php if (isset($orden_id_tipo_orden)): ?>
		                            <?php if ($orden_id_tipo_orden == 6): ?>
		                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
		                            <?php else: ?>
		                                <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                            <?php endif; ?>
		                        <?php else: ?>
		                            <img src="<?php echo base_url().'assets/imgs/circulo.png'; ?>" style="width:10px;" alt="">
		                        <?php endif; ?>
		                    </span>
		                    &nbsp;&nbsp;<strong>Reclamación</strong>
		                </span>
		            </td>
		        </tr>
		    </table>
		</div>
	</div>

	<br>
	<div class="row">
		<div col-sm-12>
			<table style="width:100%;border:1px solid #084f8c;">
		        <tr>
		            <td colspan="8" align="center" style="border-bottom:1px solid #084f8c;">
		                DESCRIPCIÓN DEL TRABAJO Y DESGLOCE DEL PRESUPUESTO (COTIZACIÓN)
		            </td>
		        </tr>
		        <tr>
		            <td align="center" style="width:10%;">GP</td>
		            <td align="center" style="width:10%;">OP</td>
		            <td align="center" style="width:25%;">Descripción</td>
		            <td align="center" style="width:10%;">Precio</td>
		            <td align="center" style="width:10%;">Cantidad</td>
		            <td align="center" style="width:10%;"></td>
		            <td align="center" style="width:10%;">Incluye IVA</td>
		            <td align="center" style="width:15%;border-left:1px solid #084f8c;">Total</td>
		        </tr>

		        <?php if (isset($item_descripcion)): ?>
		            <?php foreach ($item_descripcion as $index => $valor): ?>
		                <tr>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $gp_items[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $op_items[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:25%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_descripcion[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_precio_unitario[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_cantidad[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            Dto. <?php echo $item_descuento[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:10%;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_operacion[$index]; ?>
		                        </span>
		                    </td>
		                    <td align="center" style="width:15%;border-left:1px solid #084f8c;">
		                        <span style="color: #337ab7;">
		                            <?php echo $item_total[$index]; ?>
		                        </span>
		                    </td>
		                </tr>
		            <?php endforeach; ?>
		            
		            <?php if (isset($gp_op_HM)): ?>
		                <tr>
		                    <td colspan="7" align="center" style="border-top:0.5px solid #337ab7;">
		                        COTIZACIÓN EXTRA (MULTIPNUTO)
		                    </td>
		                    <td align="center" style="width:15%;border-left:1px solid #084f8c;"></td>
		                </tr>
		                <?php foreach ($gp_op_HM as $index => $valor): ?>
		                    <tr style="padding:0px;">
		                        <td align="center" style="padding:0px;color:black;" colspan="2">
		                            <?php echo $pieza_HM[$index]; ?>
		                        </td>
		                        <td align="center" style="width:25%;color:black;">
		                            <?php echo $descripcion_HM[$index]; ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                            <?php echo number_format($precio_HM[$index],2); ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                            <?php if (ctype_digit($cantidad_HM[$index])): ?>
		                                <?php echo number_format($cantidad_HM[$index],2); ?>
		                            <?php else: ?>
		                                <?php echo $cantidad_HM[$index]; ?>
		                            <?php endif ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                                MO : <?php echo number_format($mo_hm[$index],2); ?> 
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                                SI
		                        </td>
		                        <td align="center" style="width:15%;border-left:1px solid #084f8c;color:black;">
		                            <?php echo number_format($total_fila_HM[$index],2); ?>
		                        </td>
		                    </tr>
		                <?php endforeach; ?>
		            <?php endif; ?>

		            <!-- Completamos los renglones -->
		            <?php if ($totalRenglones<7): ?>
		                <?php for ($i = 0 ; $i < 7-$totalRenglones; $i++): ?>
		                    <tr>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:25%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:10%;"><br></td>
		                        <td align="center" style="width:15%;border-left:1px solid #084f8c;"><br></td>
		                    </tr>
		                <?php endfor; ?>
		            <?php endif; ?>
		        <?php else: ?>
		            <?php if (isset($gp_op_HM)): ?>
		                <tr>
		                    <td colspan="7" align="center" style="border-top:0.5px solid #337ab7;">
		                        COTIZACIÓN EXTRA (MULTIPNUTO)
		                    </td>
		                    <td align="center" style="width:15%;border-left:1px solid #084f8c;"></td>
		                </tr>
		                <?php foreach ($gp_op_HM as $index => $valor): ?>
		                    <tr style="padding:0px;">
		                        <td align="center" style="padding:0px;color:black;" colspan="2">
		                            <?php echo $pieza_HM[$index]; ?>
		                        </td>
		                        <td align="center" style="width:25%;color:black;">
		                            <?php echo $descripcion_HM[$index]; ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                            <?php echo number_format($precio_HM[$index],2); ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                            <?php if (ctype_digit($cantidad_HM[$index])): ?>
		                                <?php echo number_format($cantidad_HM[$index],2); ?>
		                            <?php else: ?>
		                                <?php echo $cantidad_HM[$index]; ?>
		                            <?php endif ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                                MO : <?php echo number_format($mo_hm[$index],2); ?>
		                        </td>
		                        <td align="center" style="width:10%;color:black;">
		                                SI
		                        </td>
		                        <td align="center" style="width:15%;border-left:1px solid #084f8c;color:black;">
		                            <?php echo number_format($total_fila_HM[$index],2); ?>
		                        </td>
		                    </tr>
		                <?php endforeach; ?>
		            <?php endif; ?>
		            <?php for ($i = 0 ; $i < 7-$totalRenglones; $i++): ?>
		                <tr>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:25%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:10%;"><br></td>
		                    <td align="center" style="width:15%;border-left:1px solid #084f8c;"><br></td>
		                </tr>
		            <?php endfor; ?>
		        <?php endif ?>

		        <tr class="">
		            <td colspan="7" align="right" style="border-top:1px solid #084f8c;">
		                <span style="color: black;">SUB-TOTAL :</span>
		            </td>
		            <td align="center" style="width:15%;border-left:1px solid #084f8c;border-top:1px solid #084f8c;color:black;">
		                <span style="color: #337ab7;">
		                    <?php if(isset($subtotal_items)) echo number_format($subtotal_items,2); else echo "$0.00";?>
		                </span>
		            </td>
		        </tr>
		        <tr class="">
		            <td colspan="7" align="right">
		                <span style="color: black;">I.V.A. :</span>
		            </td>
		            <td align="center" style="width:15%;border-left:1px solid #084f8c;color:black;">
		                <span style="color: #337ab7;">
		                    <?php if(isset($iva_items)) echo number_format($iva_items,2); else echo "$0.00";?>
		                </span>
		            </td>
		        </tr>
		        <tr class="">
		            <td colspan="7" align="right">
		                <span style="color: black;">PRESUPUESTO TOTAL:</span>
		            </td>
		            <td align="center" style="width:15%;border-left:1px solid #084f8c;color:black;">
		                <span style="color: #337ab7;">
		                    <?php if(isset($total_items)) echo number_format($total_items,2); else echo "$0.00";?>
		                </span>
		            </td>
		        </tr>
		        <tr class="">
		            <td colspan="7" align="right" style="border-top:1px solid #084f8c;border-left:1px solid #084f8c;color:black;">
		                <span style="color: black;">ANTICIPO:</span>
		            </td>
		            <td width="15%" align="center" style="border-top:1px solid #084f8c;color:black;border-left:1px solid #084f8c;">
		                <span style="color: #337ab7;">
		                    <?php if(isset($orden_anticipo)) echo number_format($orden_anticipo,2); else echo "$0.00";?>
		                </span>
		            </td>
		        </tr>
		        <tr class="">
		            <td colspan="7" align="right">
		                <span style="color: black;">TOTAL</span>
		            </td>
		            <td width="15%" align="center" style="color:black;border-left:1px solid #084f8c;color:black;">
		                <strong style="color: #337ab7;">
		                    <?php if(isset($presupuesto_items)) echo number_format($presupuesto_items,2); else echo "$0.00";?>
		                </strong>
		            </td>
		        </tr>
		        <tr>
		            <td colspan="8" style="border-top:1px solid #084f8c;">
		                <p align="justify">
		                    Este presupuesto tiene una vigencia de 3 (tres) días hábiles. Si después del diagnóstico y presupuesto, durante la reparación los precios de refacciones sufríecen
		                    algún cambio debido a fluctuaciones cambiarias, antes de hacer el trabjo se recabará autorización del cleinte. El pago deberá  ser efectuado en efectivo o mediante
		                    tarjeta de crédito o débito bancaria, a la entrega de la unidad. Se requerirá  anticipo en el caso de la cláusula 14. En caso de cancelación aplican cargos descriptos en la cláusula 9.
		                </p>
		            </td>
		        </tr>        
		    </table>
		</div>
	</div>


    <br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        DIAGNÓSTICO Y POSIBLES CONSECUENCIAS
                        <a id="superUsuarioA" class="h2_title_blue" data-target="#superUsuario" data-toggle="modal" style="color:white;margin-top:15px;cursor: pointer;">
                            <i class="fas fa-user"></i>
                        </a>
                    </h3>
                </div>
                <div class="panel-body">
                    <br>
                    <p align="justify" style="text-align: justify;">
                        Para elaborar el diagnóstico de <label for="" style="color:#337ab7;"><u><strong><?php if(isset($vehiculo)) echo $vehiculo; ?></strong></u></label> del vehículo,
                        autorizo en forma expresa a desarmar las partes indispensables del mismo y sus componentes, a efecto de obtener un diagnóstico adecuado de él,
                        en el entendido de que el vehículo se me devolverá en las mismas condiciones en que fuera entregado, excepto en caso de que como consecuencia
                        inevitable resulte imposible o ineficaz para su funcionamiento así entregarlo, por causa no imputable al proveedor, lo cual
                        si
                        (&nbsp;<?php if (isset($terminos)): ?>
	                        <?php if (strtoupper($terminos) == "1"): ?>
	                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
	                        <?php endif; ?>
	                    <?php endif; ?>)&nbsp;
	                    no(&nbsp;<?php if (isset($terminos)): ?>
	                        <?php if (strtoupper($terminos) == "0"): ?>
	                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:10px;" alt="">
	                        <?php endif; ?>
	                    <?php endif; ?>)&nbsp;&nbsp;
                        acepto.

                        En todo caso me obligo a pagar el importe del diagnóstico y los trabajos necesarios para realizarlo en caso de no autorizar la
                        reparación en términos del contrato y acepto que el diagnóstico se realice en un plazo de 
                        <label for="" style="color:#337ab7;"><u>&nbsp;<strong><?php if(isset($dias)) echo $dias; else echo "0";?></strong>&nbsp;</u></label> 
                        días hábiles a partir de la firma del presente.
                    </p>
                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-3" align="center">
                            Nombre y firma de quien elabora el diagnóstico.<br>
                            <?php if (isset($firmaDiagnostico)): ?>
			                    <?php if ($firmaDiagnostico != ""): ?>
			                        <img src="<?php echo base_url().$firmaDiagnostico; ?>" alt="" style='width:130px;height:40px;'>
			                    <?php endif; ?>
			                <?php endif; ?>
			                <br>
			                <label for="" style="color:#337ab7;"><?php if(isset($nombreDiagnostico)) echo $nombreDiagnostico; ?></label>
                        </div>
                        <div class="col-md-3" align="center">
                            Fecha de elaboración del diagnóstico. <br>
                            <input type="date" class="input_field" style="width:3cm;" value="<?php if(isset($fecha_diagnostico)) echo $fecha_diagnostico; else echo "2019-01-01"; ?>" disabled>
                        </div>
                        <div class="col-md-3" align="center">
                            Nombre y firma del consumidor aceptando diagnóstico.<br>
                            <?php if (isset($firmaConsumidor1)): ?>
				                  <?php if ($firmaConsumidor1 != ""): ?>
				                      <img src="<?php echo base_url().$firmaConsumidor1; ?>" alt="" style='width:130px;height:40px;'>
				                  <?php endif; ?>
				              <?php endif; ?>
				              <br>
				              <label for="" style="color:#337ab7;"><?php if(isset($nombreConsumido1)) echo $nombreConsumido1; ?></label>
                        </div>
                        <div class="col-md-3" align="center">
                            Fecha aceptación. <br>
                            <input type="date" class="input_field" style="width:3cm;" value="<?php if(isset($fecha_aceptacion)) echo $fecha_aceptacion; else echo "2019-01-01"; ?>" disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        <strong>DIAGNÓSTICO Y POSIBLES CONSECUENCIAS</strong>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-6" style="min-width: 400px;" align="center">
                            <img src="<?php echo base_url().'assets/imgs/carroceria.png'; ?>" alt="" style="width:390px; height:50px;">
                            
                            <?php if (isset($danosImg)): ?>
                            	<?php if ($danosImg != ""): ?>
                            		<img src="<?php echo base_url().$danosImg; ?>" alt="" style="width:420px; height:500px;">
                            	<?php else: ?>
                            		<img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:420px; height:500px;">
                            	<?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:420px; height:500px;">
                            <?php endif; ?>

                            <a href="#" class="button" download="<?php if(isset($danosImg)) echo base_url().$danosImg; else echo base_url().'assets/imgs/car.jpeg';?>">Descargar diagrama</a>
                            <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car.jpeg'; ?>">
                            <input type="hidden" name="danosMarcas" id="danosMarcas" value="">
                            <canvas id="canvas_3" width="420" height="500" hidden>
                                Su navegador no soporta canvas.
                            </canvas>
                        </div>

                        <div class="col-md-6" style="width: 100%;min-width: 400px;">
                            <table class="" style="width:100%; border:1px solid #084f8c;" >
			                    <tr>
			                        <td colspan="2" style="border-bottom:1px solid #084f8c;">
			                            Interiores Opera 
			                            Si(<img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">)
			                            /No(<strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>) 
			                            /No cuenta (NC)
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Llavero.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Llavero.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Llavero.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>)
			                                <?php elseif (in_array("Llavero.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Seguro rines.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("SeguroRines.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("SeguroRines.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("SeguroRines.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td colspan="2">
			                            Indicadores de falla Activados:
			                        </td>
			                    </tr>
			                    <tr>
			                        <td colspan="2" style="border-bottom: 1px solid #084f8c;">
			                            <table class="" style="width:100%; font-size:7px;color:#337ab7;" >
			                                <tr>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/05.png" style="width:20px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/02.png" style="width:20px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/07.png" style="width:20px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/01.png" style="width:20px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/06.png" style="width:20px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/04.png" style="width:20px;">
			                                    </td>
			                                    <td style="width: 29px;" align="center">
			                                        <img src="<?php echo base_url();?>assets/imgs/03.png" style="width:20px;">
			                                    </td>
			                                </tr>
			                                <tr>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorGasolina.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorGasolina.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorGasolina.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorMantenimento.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorMantenimento.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorMantenimento.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("SistemaABS.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("SistemaABS.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("SistemaABS.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorFrenos.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorFrenos.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorFrenos.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorBolsaAire.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorBolsaAire.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorBolsaAire.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorTPMS.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorTPMS.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorTPMS.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                    <td align="center">
			                                        <?php if (isset($interiores)): ?>
			                                            <?php if (in_array("IndicadorBateria.Si",$interiores)): ?>
			                                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                            <?php elseif (in_array("IndicadorBateria.No",$interiores)): ?>
			                                                <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                            <?php elseif (in_array("IndicadorBateria.NC",$interiores)): ?>
			                                                <label style='color:#337ab7;'>NC</label>
			                                            <?php endif; ?>
			                                        <?php endif; ?>
			                                    </td>
			                                </tr>
			                            </table>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Indicador de falla activados.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("IndicadorDeFalla.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("IndicadorDeFalla.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("IndicadorDeFalla.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Rociadores y Limpiaparabrisas.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Rociadores.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Rociadores.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Rociadores.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Claxon.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Claxon.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Claxon.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Claxon.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td colspan="2" style="border-bottom:1px solid #084f8c;">
			                            Luces:
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            &nbsp;&nbsp;&nbsp;&nbsp;Delanteras.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("LucesDelanteras.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("LucesDelanteras.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("LucesDelanteras.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            &nbsp;&nbsp;&nbsp;&nbsp;Traseras.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("LucesTraseras.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("LucesTraseras.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("LucesTraseras.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            &nbsp;&nbsp;&nbsp;&nbsp;Stop.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("LucesStop.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("LucesStop.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("LucesStop.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Radio / Caratulas.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Caratulas.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Caratulas.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Caratulas.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Pantallas.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Pantallas.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Pantallas.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Pantallas.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            A/C
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("AA.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("AA.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("AA.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Encendedor.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Encendedor.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Encendedor.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Encendedor.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Vidrios.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Vidrios.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Vidrios.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Vidrios.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Espejos.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Espejos.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Espejos.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Espejos.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Seguros eléctricos.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("SegurosEléctricos.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("SegurosEléctricos.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("SegurosEléctricos.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Disco compacto.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("CD.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("CD.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("CD.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;border-bottom:1px solid #084f8c;">
			                            Asientos y vestiduras.
			                        </td>
			                        <td style="width:30%;border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Vestiduras.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Vestiduras.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Vestiduras.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr align="">
			                        <td style="width:70%;">
			                            Tapetes.
			                        </td>
			                        <td style="border-left:1px solid #084f8c;width:30%;" align="center">
			                            <?php if (isset($interiores)): ?>
			                                <?php if (in_array("Tapetes.Si",$interiores)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Tapetes.No",$interiores)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Tapetes.NC",$interiores)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                </table>
                        </div>
                    </div>
                    
                    <div class="panel-body">

                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        Cajuela / Exteriores / Documentación
                    </div>

                    <div class="panel-body">
                        <div class="col-md-4">
                            <table class="table table-bordered" style="min-width:300px;">
                                <tr class="headers_table" align="center">
			                        <td colspan="2" align="center" style="border-bottom:1px solid #084f8c;">
			                            <strong style="font-size:13px!important;">Cajuela&nbsp;&nbsp;&nbsp;&nbsp; </strong>
			                            Si(<img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">)
			                            /No(<strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>) 
			                            /No cuenta (NC)
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;width:70%;">
			                            Herramienta.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("Herramienta.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Herramienta.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Herramienta.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;">
			                            Gato / Llave.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("eLlave.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("eLlave.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("eLlave.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;">
			                            Reflejantes.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("Reflejantes.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Reflejantes.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Reflejantes.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;">
			                            Cables.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("Cables.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Cables.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Cables.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="border-bottom:1px solid #084f8c;">
			                            Extintor.
			                        </td>
			                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("Extintor.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("Extintor.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("Extintor.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td>
			                            Llanta Refacción.
			                        </td>
			                        <td align="center" style="border-left:1px solid #084f8c;">
			                            <?php if (isset($cajuela)): ?>
			                                <?php if (in_array("LlantaRefaccion.Si",$cajuela)): ?>
			                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
			                                <?php elseif (in_array("LlantaRefaccion.No",$cajuela)): ?>
			                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
			                                <?php elseif (in_array("LlantaRefaccion.NC",$cajuela)): ?>
			                                    <label style='color:#337ab7;'>NC</label>
			                                <?php endif; ?>
			                            <?php endif; ?>
			                        </td>
			                    </tr>
                            </table>
                        </div>

                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-bordered">
                                        <tr class="headers_table" align="center">
					                        <td colspan="2" style="border-bottom:1px solid #084f8c;">
					                            <label style="font-size:13px!important;">Exteriores&nbsp;&nbsp;&nbsp;&nbsp;</label>
					                            Si(<img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">)
			                            		/No(<strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>) 
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Tapones rueda.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($exteriores)): ?>
					                                <?php if (in_array("TaponesRueda.Si",$exteriores)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("TaponesRueda.No",$exteriores)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Gomas de limpiadores.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($exteriores)): ?>
					                                <?php if (in_array("Gotas.Si",$exteriores)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("Gotas.No",$exteriores)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;">
					                            Antena.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;">
					                            <?php if (isset($exteriores)): ?>
					                                <?php if (in_array("Antena.Si",$exteriores)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("Antena.No",$exteriores)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td>
					                            Tapón de gasolina.
					                        </td>
					                        <td align="center" style="border-left:1px solid #084f8c;">
					                            <?php if (isset($exteriores)): ?>
					                                <?php if (in_array("TaponGasolina.Si",$exteriores)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("TaponGasolina.No",$exteriores)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                </table>
                                </div>

                                <div class="col-md-6">
                                    <table class="table table-bordered">
                                        <tr class="headers_table" align="center">
					                        <td colspan="2" style="border-bottom:1px solid #084f8c;">
					                            <label style="font-size:13px!important;">Documentación&nbsp;&nbsp;&nbsp;&nbsp;</label>
					                            Si(<img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">)
			                            		/No(<strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>) 
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Póliza garantía/Manual prop.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($documentacion)): ?>
					                                <?php if (in_array("PolizaGarantia.Si",$documentacion)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("PolizaGarantia.No",$documentacion)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Seguro de Rines.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($documentacion)): ?>
					                                <?php if (in_array("SeguroRinesDoc.Si",$documentacion)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("SeguroRinesDoc.No",$documentacion)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td style="border-bottom:1px solid #084f8c;width:70%;">
					                            Certificado verificación.
					                        </td>
					                        <td align="center" style="border-bottom:1px solid #084f8c;border-left:1px solid #084f8c;width:30%;">
					                            <?php if (isset($documentacion)): ?>
					                                <?php if (in_array("cVerificacion.Si",$documentacion)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("cVerificacion.No",$documentacion)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                    <tr>
					                        <td>
					                            Tarjeta de circulación.
					                        </td>
					                        <td align="center" style="border-left:1px solid #084f8c;">
					                            <?php if (isset($documentacion)): ?>
					                                <?php if (in_array("tCirculacion.Si",$documentacion)): ?>
					                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                <?php elseif (in_array("tCirculacion.No",$documentacion)): ?>
					                                    <strong style="font-size: 12px;font-weight: bold;color:#337ab7;">X</strong>
					                                <?php endif; ?>
					                            <?php endif; ?>
					                        </td>
					                    </tr>
					                </table>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12" align="center">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="width:40%">
                                                    <img src="<?php echo base_url().'assets/imgs/gas.jpeg'; ?>" style="width:50px;" alt="">
                                                </td>
                                                <td style="margin-left:5px;" colspan="2">
                                                    Nivel de Gasolina:&nbsp;&nbsp;&nbsp;&nbsp;
                                                	<label for="" style="color:#337ab7;"> <strong><?php if(isset($nivelGasolina)) echo $nivelGasolina; else echo "0/0";?></strong> </label>
                                                    <br>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Deja artículos personales?</td>
                                                <td>
	                                                Si&nbsp;
	                                                [<?php if (isset($articulos)): ?>
						                                <?php if (strtoupper($articulos) == "1"): ?>
						                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
						                                    <?php endif; ?>
						                                <?php endif; ?>]
	                                                No&nbsp;
	                                                [<?php if (isset($articulos)): ?>
					                                    <?php if (strtoupper($articulos) == "0"): ?>
					                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
					                                    <?php endif; ?>
					                               <?php endif; ?>]
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Cuales?</td>
                                                <td>
                                                    <label for="" style="color:#337ab7;"><?php if(isset($cualesArticulos))  echo $cualesArticulos; else echo "Ninguno";?></label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>¿Desea reportar algo más?</td>
                                                <td>
                                                    <label for="" style="color:#337ab7;"> <?php if(isset($reporte))  echo $reporte; else echo "Ninguno";?></label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>IMPORTANTE</h2><br>
                                <p align="justify" style="text-align: justify;">
                                    Favor de recoger su unidad en un plazo no mayor a 48 Hrs., contados a partir de la fecha en que su
                                    asesor le informe que la unidad esta lista, pasado este plazo deberá cubrir el amacenaje por guarda de
                                    vehículo conforme al contrato.<br>
                                    Manifiesto bajo protesta que soy el dueño del vehículo, o tengo orden y/o autorización de él para
                                    utilizarlo y ordenar ésta reparación, en su representación, autorizando en su nombre y en el propio
                                    la realización de los trabajos descritos, así como la colocación de las piezas, refacciones, repuestos
                                    y materiales e insumos necesarios para efectuarlos, comprometiéndome en su nombre y en el propio a pagar
                                    el importe de la reparación; también manifiesto mi conformidad con el inventario realizado y que consta
                                    en el ejemplar del presente que recibo; en cualquier caso que las partes acuerden que el vehículo vaya a
                                    ser recogido o entregado por personal del PROVEEDOR en el domicilio del CONSUMIDOR, ello solo será mediante
                                    previo acuerdo u orden del CONSUMIDOR por escrito y debidamente aceptada por el PROVEEDOR, con un costo de
                                    $<label for="" style="color:#337ab7;"><u style="color:#337ab7;">&nbsp;&nbsp;<?php if(isset($costo)) echo $costo; else ""; ?>&nbsp;&nbsp;</u></label>
                                    (MONEDA NACIONAL);
                                    será en todo caso obligación del personal del PROVEEDOR identificarse plenamente ante el CONSUMIDOR como tal.<br>
                                    Finalmente manifiesto mi conformidad con los términos y condiciones previstas en el contrato inscrito al reverso,
                                    el cual manifiesto que he leído, obligándome en lo personal y en nombre del propietario del automóvil en
                                    los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día
                                    los términos del mismo, por lo que se suscribe de plena conformidad, siendo el día
				                    <label for="" style="color:#337ab7;"><u style="color:#337ab7;">&nbsp;&nbsp;<?php if(isset($diaValue)) echo $diaValue; else "";?>&nbsp;&nbsp;</u></label> de
				                    <label for="" style="color:#337ab7;"><u style="color:#337ab7;">&nbsp;&nbsp;<?php if(isset($mesValue)) echo $mesValue; else echo "";?>&nbsp;&nbsp;</u></label> de
				                    <label for="" style="color:#337ab7;"><u style="color:#337ab7;">&nbsp;&nbsp;<?php if(isset($anioValue)) echo $anioValue; else echo "";?>&nbsp;&nbsp;</u></label>.
                                    .
                                </p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6" align="center">
                                 <?php if (isset($firmaAsesor)): ?>
				                    <?php if ($firmaAsesor != ""): ?>
				                        <img src="<?php echo base_url().$firmaAsesor; ?>" alt="" style='width:130px;height:40px;'>
				                    <?php endif; ?>
				                <?php endif; ?>
				                
				                <br>
				                <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreAsesor)) echo $nombreAsesor; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
                                <br><br>
                                Nombre y firma del asesor de servicio.
                            </div>

                            <div class="col-md-6" align="center">
                                <?php if (isset($firmaConsumidor2)): ?>
					                  <?php if ($firmaConsumidor2 != ""): ?>
					                      <img src="<?php echo base_url().$firmaConsumidor2; ?>" alt="" style='width:130px;height:40px;'>
					                  <?php endif; ?>
					              <?php endif; ?>

                                <br><br>
                                <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreConsumidor2)) echo $nombreConsumidor2; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
                                <br><br>
                                Firma del consumidor<br>
                                ACEPTA PRESUPUESTO E INVENTARIO<br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <button class="btn btn-primary" ng-click="save()" value="">Guardar Orden</button> -->
    </div>

    <br><br>
    <br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">
                    <strong>CONTRATO DE ADHESIÓN</strong>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <table style="width:100%">
                            <tr>
                              <td>
                                  <p align="justify" style="text-align: justify; font-size:12px;">
                                      	<?=SUCURSAL?>, S.A. DE C.V. <br>
                                        <?=$this->config->item('encabezado_contrato')?>
                                  </p>
                              </td>
                              <td>
                                <p align="justify" style="text-align: justify; font-size:12px;">
                                    Folio No.: <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<strong><?php if(isset($folio)) echo $folio; ?></strong>&nbsp;&nbsp;</u></label> <br>
				                    Fecha: <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<strong><?php if(isset($fecha)) echo $fecha; ?></strong>&nbsp;&nbsp;</u></label> <br>
				                    Hora: <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<strong><?php if(isset($hora)) echo $hora; else echo "Sin registrar"; ?></strong>&nbsp;&nbsp;</u></label> <br>
                                </p>
                              </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <br>
                <div class="panel-body">
                    <div class="col-md-12">
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            CONTRATO DE PRESTACIÓN DE SERVICIO DE REPARACIÓN Y MANTENIMIENTO DE VEHÍCULOS QUE CELEBRAN POR UNA PARTE
                            <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<strong><?php if(isset($empresa)) echo $empresa; ?></strong>&nbsp;&nbsp;</u></label>, S.A. DE C.V.,
                            DISTRIBUIDOR AUTORIZADO FORD Y POR LA OTRA EL CONSUMIDOR CUYO NOMBRE SE SEÑALA EN EL ANVERSO DEL PRESENTE,
                            A QUIENES EN LO SUCESIVO Y PARA EFECTO DEL PRESENTE CONTRATO SE LE DENOMINARA “EL PROVEEDOR” Y “EL CONSUMIDOR”,
                            RESPECTIVAMENTE.
                        </p>
                        <h6 align="center"><strong>DECLARACIONES</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            Las partes denominadas CONSUMIDOR y PROVEEDOR, se reconocen las personalidades con las cuales se ostentan y que
                            se encuentran especificadas en este documento (orden de servicio y contrato), por lo cual, están dispuestas a
                            sujetarse a las condiciones que se establecen en las siguientes:
                        </p>
                        <h6 align="center"><strong>CLÁUSULAS</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>1.- </b>Objeto: El PROVEEDOR realizará todas las operaciones de mantenimiento, reparaciones y composturas, solicitadas
                            por el CONSUMIDOR que suscribe el presente contrato, a las que se someterá el vehículo que al anverso se detalla, para obtener
                            las condiciones de funcionamiento de acuerdo a lo solicitado por el CONSUMIDOR y que serán realizadas a cargo y por cuenta del
                            CONSUMIDOR. EL PROVEEDOR no condicionará en modo alguno la prestación de servicios de reparación o mantenimiento del vehículo,
                            a la adquisición o renta de otros productos o servicios en el mismo establecimiento o en otro taller o agencia predeterminada.
                            El precio total de los servicios contratados se establece en el presupuesto que forma parte del presente y se describe en su anverso;
                            dicho precio será pagado por el CONSUMIDOR al recibo del vehículo reparado, o en su caso, al momento de ser celebrado el presente
                            contrato, por concepto de anticipo, la cantidad que resulte conforme a lo que establece la cláusula 14, y el resto en la fecha de
                            entrega del vehículo reparado; todo pago efectuado por el CONSUMIDOR deberá efectuarse en el establecimiento del PROVEEDOR al contado
                            y en Moneda Nacional o extranjera al tipo de cambio vigente al día de pago, de contado, ya sea en efectivo, o mediante tarjeta de
                            crédito o deposito o transferencia bancaria efectuada con anterioridad a la entrega del vehículo.
                        </p>
                        <h6 align="center"><strong>DE LAS CONDICIONES GENERALES:</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>2.-</b> Las partes están de acuerdo en que las condiciones generales en las que se encuentra el automóvil de acuerdo con el inventario
                            visual al momento de su recepción, son las que se definen en la orden de servicio-presupuesto que se encuentra al anverso del presente contrato.
                        </p>
                        <h6 align="center"><strong>DEL DIAGNÓSTICO Y DEL PRESUPUESTO.</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>3.-</b> Las partes convienen en que en caso de que el automóvil motivo de este contrato se deje en las instalaciones
                            del PROVEEDOR para diagnóstico y presupuesto, éste, en un plazo no mayor de lo asentado en este contrato, realizará y
                            entregará al CONSUMIDOR dicho diagnóstico y presupuesto en el que hará constar el precio detallado de las operaciones de
                            mano de obra, de partes, reparación de partes, materiales, insumos, así como el tiempo en que se efectuaría la reparación
                            para que, en su caso, el CONSUMIDOR lo apruebe. El presupuesto que así se entregue tendrá una vigencia de 3 días hábiles.<br>
                            <b>4.-</b> El PROVEEDOR hace saber al CONSUMIDOR que al efectuar el diagnostico a su automóvil, las posibles consecuencias son
                            las señaladas en el anverso del presente contrato, por lo que de no aceptarse la reparación, el vehículo le será entregado en
                            las condiciones en que fue recibido para el diagnóstico, excepto en caso de que como consecuencia inevitable resulte imposible
                            o ineficaz para su funcionamiento así entregarlo, porque al entregarlo armado sin repararlo, sea posible que se cause un daño
                            mayor al mismo vehículo, o su uso constituya un riesgo para el usuario o terceros, por causa no imputable al PROVEEDOR, caso en
                            el cual éste sólo se hará responsable de dichas consecuencias por causas imputables a él o sus empleados, el CONSUMIDOR enterado
                            de esas consecuencias se responsabiliza de ellas.<br>
                            <b>5.-</b> El PROVEEDOR se obliga ante el CONSUMIDOR a respetar el presupuesto contenido en el presente contrato.<br>
                            <b>6.-</b> El CONSUMIDOR se obliga pagar a el PROVEEDOR por el diagnostico de su automóvil la cantidad de
                            $ <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<strong><?php if(isset($pagoConsumidor)) echo $pagoConsumidor; ?></strong>&nbsp;&nbsp;</u></label>(MONEDA NACIONAL),
                            siempre y cuando no apruebe la reparación. En caso de autorizar la reparación, el diagnóstico no será cobrado.
                        </p>
                        <h6 align="center"><strong>DE LA PRESTACIÓN DE SERVICIOS</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>7.-</b> Las partes convienen en que la fecha de aceptación del presupuesto es la que se indica en el anverso de este contrato.<br>
                            <b>8.-</b> El PROVEEDOR se obliga a hacer la entrega del automóvil reparado el día previamente establecido en el presupuesto. El PROVEEDOR
                            se obliga a emplear partes y refacciones nuevas apropiadas para los servicios contratados salvo que el CONSUMIDOR ordene o autorice expresamente
                            y por escrito se utilicen otras o las provea, de conformidad con lo establecido en el artículo 60 de la Ley Federal de Protección al
                            Consumidor, caso en el cual la reparación no tendrá garantía en lo que se relacione a esas partes y refacciones, o a lo que éstas afecten, salvo en
                            cuanto a la mano de obra que haya correspondido a su colocación. El retraso en la entrega de las refacciones por parte del CONSUMIDOR, si éste se
                            compromete a proporcionarlas, prorrogará por el mismo lapso de demora, la fecha de entrega del vehículo.<br>
                            <b>9.-</b> El CONSUMIDOR, puede desistir en cualquier momento de la prestación del servicio, pagando al PROVEEDOR el importe por los trabajos efectuados
                            y partes colocadas o adquiridas, hasta el retiro del automóvil; en este caso, las refacciones adquiridas y que no hayan sido colocadas en el vehículo a
                            repararse, serán entregadas al CONSUMIDOR.<br>
                            <b>10.</b>- El PROVEEDOR, podrá utilizar el automóvil solo para recorridos de prueba, dentro de un radio de máximo 15 (quince) kilómetros alrededor del
                            local en el que se presta el servicio, a efecto de realizar pruebas o verificar las reparaciones efectuadas; el PROVEEDOR no podrá usar el vehículo para
                            fines propios o de terceros. El PROVEEDOR se hace responsable por los daños causados al vehículo, como consecuencia de los recorridos de prueba efectuados
                            por parte de su personal. Cuando el CONSUMIDOR, solicite que él o un representante suyo sea quien conduzca el automóvil en un recorrido de prueba,
                            el riesgo, será por su cuenta.<br>
                            <b>11.</b>- El PROVEEDOR se hace responsable por las posibles descomposturas, daños o pérdidas parciales o totales imputables a él o a sus subalternos que sufra
                            el vehículo, el equipo y los aditamentos adicionales que el CONSUMIDOR le haya notificado que existen en el momento de la recepción del mismo, o que se causen a
                            terceros, mientras el vehículo se encuentre bajo su resguardo, salvo los ocasionados por desperfectos mecánicos o a resultas de piezas gastadas o sentidas, por
                            incendio motivado por deficiencia de la instalación eléctrica, o fallas en el sistema de combustible, preexistentes y no causadas por el PROVEEDOR; para tal efecto
                            el PROVEEDOR cuenta con un seguro suficiente para cubrir dichas eventualidades, bajo póliza expedida por compañía de seguros autorizada al efecto. El PROVEEDOR no
                            se hace responsable por la pérdida de objetos dejados en el interior del automóvil, aun con la cajuela cerrada, salvo que estos le hayan sido notificados y puestos
                            bajo su resguardo al momento de la recepción del automóvil. El PROVEEDOR tampoco se hace responsable por daños causados por fuerza mayor o caso fortuito, ni por la
                            situación legal del automóvil cuando éste previamente haya sido robado o se hubiere utilizado en la comisión de algún ilícito; lo anterior, salvo que alguna de éstas
                            cuestiones resultara legalmente imputable al PROVEEDOR; así mismo, el CONSUMIDOR, libera al PROVEEDOR de cualquier responsabilidad que surja o pueda surgir con relación
                            al origen, posesión o cualquier otro derecho inherente al vehículo o a partes y componentes del mismo, obligándose en lo personal y en nombre del propietario, a responder
                            de su procedencia.
                        </p>
                        <h6 align="center"><strong>DEL PRECIO Y FORMAS DE PAGO</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>12.-</b> El CONSUMIDOR acepta haber tenido a su disposición los precios de la tarifa de mano de obra, de las refacciones y materiales a usar
                            en las reparaciones, ofrecidas por el PROVEEDOR; los incrementos que resulten durante la reparación, por costos no previsibles y/o incrementos
                            que resulten al momento de la ejecución de la reparación ordenada, deberán ser autorizados por el CONSUMIDOR, por vía telefónica siempre y cuando
                            estos no excedan del 20% presupuestado. Si el incremento citado es superior al 20%, el CONSUMIDOR lo tendrá que autorizar en forma escrita o por
                            correo electrónico proveniente del mismo señalado como propio de él en el anverso del presente contrato. El tiempo que, en su caso, transcurra para
                            cumplir esta condición, modificará la fecha de entrega, en la misma proporción. Todas las quejas y sugerencias serán atendidas en el domicilio,
                            correo, teléfonos y horarios de atención señalados en la parte superior del presente o en su anverso.<br>
                            <b>13.-</b> Será obligación del PROVEEDOR expedir la factura correspondiente por los servicios y productos que preste o enajene; el importe total del
                            servicio, así como el precio por concepto de refacciones, mano de obra, materiales y accesorios quedará especificado en ella, conforme a la ley.<br>
                            <b>14.-</b> El CONSUMIDOR se obliga a pagar de contado al PROVEEDOR (conforme a lo establecido en la cláusula 1), en las instalaciones de éste y previo
                            a la entrega del automóvil, el importe de los trabajos efectuados y partes colocadas o adquiridas hasta el retiro del mismo, de conformidad con el presupuesto
                            elaborado para tal efecto. En caso de reparaciones cuyo presupuesto sea superior a $15,000.00 (QUINCE MIL PESOS 00/100 M.N.) el CONSUMIDOR deberá pagar un
                            anticipo del 50% sobre el monto del presupuesto elaborado, y el saldo restante lo pagará a la entrega del automóvil debidamente reparado.
                        </p>
                        <h6 align="center"><strong>DE LA ENTREGA.</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>15.-</b> El PROVEEDOR se obliga a hacer la entrega del automóvil reparado en la fecha y hora establecida en este contrato, en las propias instalaciones del
                            PROVEEDOR, pudiendo ampliarse dicho plazo en caso fortuito o de fuerza mayor, en cuyo caso será obligación del PROVEEDOR dar aviso previo al CONSUMIDOR de la
                            causa y del tiempo que se ampliará el plazo de entrega.<br>
                            <b>16.-</b> Las refacciones reemplazadas en las reparaciones quedarán a disposición del CONSUMIDOR al momento de recoger el automóvil, salvo que éste exprese
                            lo contrario o que las refacciones, partes o piezas sean las cambiadas en ejercicio de la garantía o se trate de residuos considerados peligrosos, de acuerdo con
                            las disposiciones legales aplicables. Si el CONSUMIDOR, al recoger su automóvil se niega a recibir las partes reemplazadas, el PROVEEDOR podrá disponer de ellas.<br>
                            <b>17.-</b> El CONSUMIDOR se obliga a hacer el pago, a recoger el automóvil y, en su caso, las refacciones que fueran reemplazadas, dentro del horario establecido
                            por el PROVEEDOR, en un término no mayor de 48 horas, contadas a partir del día y hora fijadas para su entrega, o bien de que se haya entregado el presupuesto y éste
                            no hubiera sido autorizado. Si dentro de éste plazo el automóvil no pudiera circular por cualquier restricción, el plazo citado se ampliará 24 horas más. En caso de
                            que el CONSUMIDOR no haga el pago y recoja el vehículo en el tiempo establecido, se obliga a pagar el almacenaje por la guarda del automóvil, caso en el que pagará la
                            suma que resulte equivalente a  2 (dos) días de salario mínimo general vigente en la zona en la que se encuentran las instalaciones del PROVEEDOR, por cada día de
                            retraso el cumplimiento de esas obligaciones. Si transcurridos 60 días naturales, contados a partir de la fecha en que debió entregarse el automóvil, no ha sido
                            reclamado por el CONSUMIDOR, el PROVEEDOR notificará este hecho a las autoridades competentes y  podrá dar inicio a las acciones legales que correspondan para el
                            cobro de los servicios y/o refacciones otorgados, así como del almacenaje.<br>
                            <b>18.-</b> En el momento en que el PROVEEDOR entregue y el CONSUMIDOR reciba el automóvil reparado, se entenderá que esto fue a completa satisfacción del CONSUMIDOR en
                            lo que respecta a sus condiciones generales de acuerdo al inventario visual, mismas que fueron descritos en la orden de servicio, así como también en cuanto a la reparación
                            efectuada, sin afectar sus derechos a ejercer la garantía.
                        </p>
                        <h6 align="center"><strong>DE LA RESCISIÓN Y PENAS CONVENCIONALES</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>19.-</b> Es causa de rescisión del presente contrato: A.- Que EL PROVEEDOR incumpla en la fecha, hora y lugar de entrega del vehículo por causas propias, caso en el
                            cual el CONSUMIDOR notificará por escrito del incumplimiento al PROVEEDOR, y éste hará entrega inmediata del vehículo debidamente reparado conforme al diagnóstico y
                            presupuesto establecido, descontando del precio pactado para la prestación del servicio, la suma equivalente al 2% (dos por ciento) del total de la reparación, que se
                            pacta como pena convencional; B.- Que el CONSUMIDOR incumpla con el pago de la reparación ordenada, en el término previsto en la cláusula 19, caso en el cual el PROVEEDOR
                            le notificará por escrito su incumplimiento, y podrá optar por exigir la recisión o cumplimiento forzoso de la obligación, cobrando la misma pena pactada del 2% al
                            CONSUMIDOR, por la mora.
                        </p>
                        <h6 align="center"><strong>DE LAS GARANTÍAS</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>20.-</b> <u>Las reparaciones a que se refiere el presupuesto aceptado por el CONSUMIDOR y éste contrato están garantizadas por 60 días naturales contados a partir de la fecha
                            de la entrega del vehículo ya reparado, en mano de obra y en las refacciones, piezas y accesorios</u>, y trabajos que por su naturaleza hayan tenido que ser realizados en otros
                            talleres <u>tendrá la especificada por el fabricante o taller respectivo, siempre y cuando no sea menor a la expresada, y no se manifieste mal uso, negligencia o descuido en el
                            uso de ellas, de conformidad a lo establecido en el artículo 77 de la Ley Federal de Protección al Consumidor</u>.  Si el vehículo es intervenido por un tercero, el PROVEEDOR no
                            será responsable y la garantía quedará sin efecto. Las reclamaciones por garantía se harán en el establecimiento del PROVEEDOR, para lo cual el CONSUMIDOR deberá presentar
                            su vehículo en dicho establecimiento. Las reparaciones efectuadas por el PROVEEDOR en cumplimiento a la garantía del servicio serán sin cargo alguno para el CONSUMIDOR,
                            salvo aquellos trabajos que no deriven de las reparaciones aceptadas en el presupuesto. No se computará dentro del plazo de garantía el tiempo que dure la reparación y/o
                            mantenimiento del vehículo para el cumplimiento de la misma. Esta garantía cubre cualquier falla, deficiencia o irregularidad relacionada con la reparación efectuada por
                            el PROVEEDOR, por causas imputables al mismo y solo será válida siempre y cuando el automóvil se haya utilizado en condiciones de uso normal, se hayan observado en su uso
                            las indicaciones de manejo y servicio que se le hubiera dado. En todo caso, el PROVEEDOR será corresponsable y solidario con los terceros del cumplimiento o incumplimiento
                            de las garantías por ellos otorgadas en lo que se relacione a las piezas, refacciones y accesorios, colocadas en el vehículo y de los servicios a éste realizados, siempre que
                            hayan sido contratados ante el CONSUMIDOR.<br>
                            <b>21.-</b> Toda reclamación dentro del término de garantía, deberá ser realizada ante el PROVEEDOR que efectuó la reparación y en el domicilio del mismo indicado en el anverso
                            del presente contrato.  En caso de que sea necesario hacer válida la garantía en un domicilio diverso al del PROVEEDOR, los gastos por ello deberán ser cubiertos por éste, siempre
                            y cuando la garantía proceda, dichos gastos sean indispensables para tal fin, y sea igualmente indispensable realizar la reparación del vehículo en domicilio diverso al del PROVEEDOR,
                            pero en dado caso, si el automóvil fallase fuera de la entidad donde se localiza el PROVEEDOR, éste inmediatamente después de que tenga conocimiento de la falla o deficiencia, podrá
                            indicar al CONSUMIDOR en donde se encuentra la agencia distribuidora de la marca más cercana, para hacer efectiva la garantía por conducto de ésta, si procede, debiendo acreditar con
                            la factura correspondiente la reparación efectuada, con el objeto, de no hacer ningún cargo por ello al CONSUMIDOR. Con el objeto de dar cumplimiento a las obligaciones que ésta
                            cláusula le impone, El PROVEEDOR señala como teléfonos de atención al CONSUMIDOR los que aparecen en éste contrato. Para los efectos de la atención y resolución de quejas y
                            reclamaciones, estas deberán ser presentadas dentro de días y horas hábiles, que son los detallados en la parte superior del presente, ante la Gerencia de Servicio ubicada en las
                            instalaciones del mismo PROVEEDOR, detallando en forma expresa la causa o motivo de la reclamación; la Gerencia de Servicio procederá a analizar la queja y resolverá lo conducente
                            dentro de un lapso de tres días hábiles, procediendo a reparar el vehículo o manifestando las causas de improcedencia de la reclamación, en forma escrita.
                        </p>
                        <h6 align="center"><strong>DE LA INFORMACIÓN Y PUBLICIDAD</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>22.-</b> El PROVEEDOR se obliga a observar en todo momento lo dispuesto por los capítulos III y IV de la Ley Federal de Protección al Consumidor, en cuanto a la información, publicidad,
                             promociones y ofertas.<br>
                            <b>23.-</b> El CONSUMIDOR <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<strong><?php if(isset($condicionalInfo)){if($condicionalInfo == "1") echo "Si"; else echo "No";} ?></strong>&nbsp;&nbsp;</u></label> acepta que el PROVEEDOR ceda o transmita a tercero, con fines mercadotécnicos o publicitarios, la información proporcionada por él con motivo del presente contrato, y <label for="" style="color:#337ab7;"><u>&nbsp;&nbsp;<strong><?php if(isset($condicionalPublicidad)) {if($condicionalPublicidad == "1") echo "Si"; else echo "No";} ?></strong>&nbsp;&nbsp;</u></label> acepta que el PROVEEDOR le envíe publicidad sobre bienes y servicios, firmando en éste espacio.
                            <br>
                        </p>
                        <p align="center">
                            <?php if (isset($firmaCliente)): ?>
                                <?php if ($firmaCliente != ""): ?>
                                    <img class='marcoImg' src='<?php echo base_url().$firmaCliente ?>' id='' style='width:130px;height:40px;'>
                                <?php endif; ?>
                            <?php endif; ?>
                            <br><br>
                        </p>
                        <h6 align="center"><strong>DE LA INTERPRETACIÓN Y CUMPLIMIENTO</strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            <b>24.-</b> La Procuraduría Federal del Consumidor, es competente para conocer y resolver en la vía administrativa de cualquier controversia que se suscite sobre la interpretación o
                            cumplimiento del presente contrato, por lo que las partes están de acuerdo en someterse a ella en términos de ley, para resolver sobre la interpretación o cumplimiento de los términos
                            del presente contrato y de las disposiciones de la Ley Federal de Protección al Consumidor, la Norma Oficial Mexicana NOM-174-SCFI-2007, Prácticas Comerciales- Elementos de Información
                            para la Prestación de Servicios en General y cualquier otra disposición aplicable. Sin perjuicio de lo anterior en caso de persistir la inconformidad, las partes se someten a la
                            jurisdicción de los tribunales competentes del domicilio del PROVEEDOR, renunciando en forma expresa a cualquier otra jurisdicción o al fuero que pudiera corresponderles en razón de sus
                            domicilios presente o futuros o por cualquier otra razón.
                        </p>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="col-md-6" align="center">
                        EL DISTRIBUIDOR (NOMBRE Y FIRMA).
                        <br>
                        <?php if (isset($firmaDistribuidor)): ?>
                            <?php if ($firmaDistribuidor != ""): ?>
                                <img class='marcoImg' src='<?php echo base_url().$firmaDistribuidor ?>' id='' style='width:130px;height:40px;'>
                            <?php endif; ?>
                        <?php endif; ?>
                        
                        <br>
                		<label for="" style="color:#337ab7;"><u><?php if(isset($nombreDistribuidor)) echo $nombreDistribuidor; ?></u></label>
                    </div>

                    <div class="col-md-6" align="center">
                        EL CONSUMIDOR (NOMBRE Y FIRMA)
                        <br>
                        <?php if (isset($firmaConsumidor)): ?>
                            <?php if ($firmaConsumidor != ""): ?>
                                <img class='marcoImg' src='<?php echo base_url().$firmaConsumidor ?>' id='' style='width:130px;height:40px;'>
                            <?php endif; ?>
                        <?php endif; ?>
                        <br>
                        <label for="" style="color:#337ab7;"><u><?php if(isset($nombreConsumidor)) echo $nombreConsumidor; ?></u></label>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="col-md-12" align="center">
                        <h6 align="center"><strong>AVISO DE PRIVACIDAD </strong></h6>
                        <p align="justify" style="text-align: justify; font-size:11px;">
                            EL Aviso de Privacidad Integral podrá ser consultado en nuestra página en internet <strong><?=SUCURSAL_WEB?>/Privacidad/</strong>, o lo puede
                            solicitar al correo electrónico <strong>datospersonales@fordplasencia.com</strong>, u obtener personalmente en el Área de Atención a la Privacidad,
                            en nuestras instalaciones.<br>
                            <label for="" style="color:#337ab7;">[&nbsp;<strong><?php if(isset($aceptarTerminos)){if($aceptarTerminos == "1") echo "Si"; else echo "No";} ?></strong>&nbsp;]</label>&nbsp;&nbsp;
                            Al marcar el recuadro precedente y remitir mis datos, otorgo mi consentimiento para que mis datos personales sean tratados conforme a lo señalado en el Aviso de Privacidad.<br>
                            Este contrato fue registrado ante la Procuraduría Federal del Consumidor. Registro público de contratos de adhesión y aprobado e inscrito con el número <label for="" style="color: #337ab7;"><strong><?php if(isset($noRegistro)) echo $noRegistro; else echo "";?></strong></label>, Expediente No. <label for="" style="color: #337ab7;"><strong><?php if(isset($noExpediente)) echo $noExpediente; else echo "";?></strong></label>, de fecha <label for="" style="color: #337ab7;"><strong><?php if(isset($fechDia)) echo $fechDia; else echo "01"; ?></strong></label> de 
		                    <label for="" style="color: #337ab7;"><strong><?php if(isset($fechMes)) echo $fechMes; else echo "01";?></strong></label> de <label for="" style="color: #337ab7;"><strong><?php if(isset($fechAnio)) echo $fechAnio; else echo "2019";?></strong></label>. <br>
		                    Cualquier variación del presente contrato en perjuicio de EL CONSUMIDOR, frente al contrato de adhesión registrado, se tendrá por no puesta. <br><br>
                            <strong>“DESEO ADHERIRME AL CONTRATO TIPO DE REPARACION Y MANTENIMIENTO DE VEHICULOS FORD DE PROFECO”</strong>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
