<div style="margin:30px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <br>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-8">
                <h3 align="center">Auditoría de Calidad</h3>
            </div>
            <div class="col-sm-2" align="right">
                <button type="button" class="btn btn-dark" style="color:white; margin-top:20px;" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                    Regresar
                </button>
            </div>
            <div class="col-sm-2" align="right">
                <input type="button" class="btn btn-success" style="color:white; margin-top:20px;" onclick="location.href='<?=base_url()."Auditoria_Calidad/0";?>';" value="Nueva Auditoría">
            </div>
        </div>

        <div class="col-md-12">
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #eee;">
                                    <td align="center" style="width: 10%;"></td>
                                    <td align="center" style="width: 10%;"><strong>NO. ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>PROXIMO SERVICIO</strong></td>
                                    <td align="center" style="width: 10%;"><strong>FECHA PROX. SERV.</strong></td>
                                    <td align="center" style="width: 10%;"><strong>MECANICO</strong></td>
                                    <td align="center" style="width: 10%;"><strong>AUDITOR</strong></td>
                                    <td align="center" style="width: 20%;"><strong>JEFE DE TALLER</strong></td>
                                    <td align="center" style="width: 10%;"><strong>SERVICIO REALIZADO EL:</strong></td>
                                    <td align="center" style="width: 10%;" colspan="1"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="width:10%;">
                                                <?php echo count($idOrden) - $index; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $proxServ[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $fechaProxServ[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $mecanico[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $auditor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $jefeTaller[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $fechaRegistro[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <a class="btn btn-primary" href="<?=base_url()."Auditoria_Calidad_PDF/".$id_cita_url[$index];?>" target="_blank">Ver PDF</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="9" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <!-- <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar"> -->

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="rolVista" value="lista">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
