<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
 
    <head>
        <title>AUDITORIA DE CALIDAD <?php if(isset($idOrden)) echo $idOrden;?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="estilos.css" />
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <style>
            @page { 
                sheet-size: A4;
                size: auto; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 5mm; 
                /*margin-top: 5mm; /* <any of the usual CSS values for margins> */
                /*margin-left: 5mm;*/
                /*margin-right: 5mm;*/
                /*margin-bottom: 10mm;*/
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 5mm; /* <any of the usual CSS values for margins> */
                margin-footer: 5mm; /* <any of the usual CSS values for margins> */
            }
        </style>
    </head>
     
    <body>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="1">
                        <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:200px;height:40px;">
                    </td>
                    <td>
                        <label class="" style="font-size:14px;color:red;">AUDITORÍA DE CALIDAD</label>                
                    </td>
                </tr>
                <tr>
                    <td style="width:50%;">
                        <table style="width:100%;padding-left: 7px;">
                            <tr>
                                <td colspan="4" style="width:290px;" align="justify">
                                    <h6 style="text-align:justify;color:#337ab7;font-weight: bold;font-size:10px;">DEPARTAMENTO DE SERVICIO</h6>
                                    <p style="text-align:justify;color:#337ab7;font-size:11px;" align="justify">
                                        De acuerdo a nuestros registros, su próximo servicio será de <?php if(isset($proxServ)) echo "<label style='color:black;'><u style ='color:black;'>&nbsp;&nbsp;".$proxServ."&nbsp;&nbsp;</u></label>"; else echo "<label style='color:black;'><u style ='color:black;'>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>";  ?>.Kms.
                                        Y su promedio de recorrido, la fecha probable para realizarlo será el  <?php if(isset($fechaProxServ)) echo "<label style='color:black;'><u style ='color:black;'>&nbsp;&nbsp;".$fechaProxServ."&nbsp;&nbsp;</u></label>"; else echo "<label style='color:black;'><u>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>";  ?>.
                                        favor de hacer su reservación para el próximo TEL: 
                                    </p>
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="background-color:#337ab7;color:white;font-size:9px;padding:5px;">
                                    HOJA DE REVISIÓN PARA TRABAJOS DE AFINACIÓN
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;"></td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    BIEN
                                </td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    MAL
                                </td>
                                <td style="width:45px;color:red;font-size: 10px;" align="center">
                                    REVISADO
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    MOTOR LIMPIO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[0] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[0] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[0] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    BUJIAS NUEVAS (SI REQUIERE)
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[1] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[1] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[1] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    FILTRO DE COMBUSTIBLE NUEVO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[2] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[2] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[2] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    FILTRO DE AIRE NUEVO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[3] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[3] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[3] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    CABLES DE BUJIAS NUEVOS (SI REQUIERE)
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[4] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[4] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[4] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    TIEMPO INICIAL DE ENCENDIDO DENTRO DE<br>
                                    ESPECIFICACIONES
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[5] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[5] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($afinacion)): ?>
                                        <?php if ($afinacion[5] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="background-color:#337ab7;color:white;font-size:9px;padding:5px;">
                                    HOJA DE REVISIÓN PARA TRABAJO DE FRENOS
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;"></td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    BIEN
                                </td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    MAL
                                </td>
                                <td style="width:45px;color:red;font-size: 10px;" align="center">
                                    REVISADO
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    ASENTAMIENTO DE BALATAS
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[0] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[0] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[0] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    SE REALIZÓ PRUEBA DE CARRETERA
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[1] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[1] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[1] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    NIVEL LÍQUIDO DE FRENOS CORRECTO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[2] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[2] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[2] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    FRENOS RUIDOSOS
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[3] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[3] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[3] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    AL APLICAR PEDAL DE FRENO SE JALA EL VEHÍCULO <br>
                                    HACIA ALGÚN LADO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[4] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[4] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($frenos)): ?>
                                        <?php if ($frenos[4] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="background-color:#337ab7;color:white;font-size:9px;padding:5px;">
                                    HOJA DE REVISIÓN PARA TRABAJOS DE ALINEACIÓN, BALANCEO Y EJE TRASERO
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;"></td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    BIEN
                                </td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    MAL
                                </td>
                                <td style="width:45px;color:red;font-size: 10px;" align="center">
                                    REVISADO
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    SE EFECTUÓ PRUEBA DE CARRETERA
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[0] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[0] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[0] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    EL VEHÍCULO TIENDE A IRSE HACIA UN LADO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[1] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[1] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[1] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    AL FRENAR EL VEHÍCULO CONSERVA SU
                                    TRAYECTORIA SIN JALARSE HACIA UN LADO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[2] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[2] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[2] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    EXISTE VIBRACIÓN EN EL VEHÍCULO A
                                    DETERMINADA VELOCIDAD
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[3] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[3] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[3] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    TUERCAS DE RUEDA APRETADAS SEGÚN
                                    ESPECIFICACIÓN
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[4] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[4] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[4] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    EJE TRASERO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[5] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[5] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($alineacion)): ?>
                                        <?php if ($alineacion[5] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="background-color:#337ab7;color:white;font-size:9px;padding:5px;">
                                    HOJA DE REVISIÓN PARA TRABAJOS DE AIRE ACONDICIONADO Y CALEFACCIÓN
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;"></td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    BIEN
                                </td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    MAL
                                </td>
                                <td style="width:45px;color:red;font-size: 10px;" align="center">
                                    REVISADO
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    CONTROL DE TEMPERATURA FUNCIONA CORRECTAMENTE
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[0] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[0] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[0] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    SELECTOR DE VELOCIDADES DEL MOTOR SOPLADOR FUNCIONA BIEN
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[1] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[1] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[1] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    FLUJO DE AIRE CORRECTO, DEPENDIENDO FUNCIÓN SELECCIONADA (DEFROST, PANEL A/C, MANUAL A/C, ETC.)
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[2] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[2] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[2] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:170px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    TEMPERATURA DE AIRE ADECUADA (UTILIZANDO TERMÓMETRO)
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[3] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[3] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($trabajoAA)): ?>
                                        <?php if ($trabajoAA[3] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>

                        </table>
                    </td>

                    <td style="width:300px;">
                        <table style="width:300px;padding-left: 7px;">
                            <tr>
                                <td colspan="4" style="background-color:#337ab7;color:white;font-size:9px;padding-left:5px;width:300px;">
                                    HOJA DE REVISIÓN GENERAL DE LA UNIDAD EN CUALQUIER TIPO DE REPARACIÓN
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;"></td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    BIEN
                                </td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    MAL
                                </td>
                                <td style="width:45px;color:red;font-size: 10px;" align="center">
                                    REVISADO
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    DAÑOS EN LA CARROCERÍA
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[0] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[0] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[0] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    DAÑOS EN LA VESTIDURA
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[1] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[1] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[1] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    VOLANTE DE DIRECCIÓN LIMPIO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[2] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[2] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[2] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    CARROCERÍA LIMPIA
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[3] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[3] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[3] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    CENICEROS LIMPIOS
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[4] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[4] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[4] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    FUNCIONAMIENTO ADECUADO DE CINTURONES
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[5] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[5] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($general)): ?>
                                        <?php if ($general[5] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="background-color:#337ab7;color:white;font-size:9px;padding-left:5px;width:300px;">
                                    HOJA DE REVISIÓN PARA TRABAJOS DE TRANSMISIÓN AUTOMÁTICA
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;"></td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    BIEN
                                </td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    MAL
                                </td>
                                <td style="width:45px;color:red;font-size: 10px;" align="center">
                                    REVISADO
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    PRESIÓN DE CONTROL DENTRO DE ESPECIFICACIONES
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[0] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[0] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[0] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    PRESIÓN DE ACELERACIÓN DENTRO DE ESPECIFICACIONES
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[1] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[1] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[1] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    SE REALIZÓ PRUEBA DE CARRETERA
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[2] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[2] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[2] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    NIVEL DE FLUIDO DE TRANSMISIÓN CORRECTO (EN CALIENTE)
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[3] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[3] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[3] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    LOS CAMBIOS DE VELOCIDAD SE EFECTÚAN CORRECTAMENTE
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[4] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[4] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[4] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    SELECTOR DE VELOCIDADES AJUSTADO CORRECTAMENTE
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[5] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[5] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[5] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    CAMBIOS DE VELOCIDAD BRUSCO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[6] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[6] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[6] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    CAMBIOS DE VELOCIDAD DEMASIADO SUAVES
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[7] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[7] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[7] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    SE SOBRERREVOLUCIONA EL MOTOR AL EFECTUAR LOS CAMBIOS
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[8] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[8] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[8] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    SE PATINA LA TRANSMISIÓN
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[9] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[9] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($transmision)): ?>
                                        <?php if ($transmision[9] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="background-color:#337ab7;color:white;font-size:9px;padding-left:5px;width:300px;">
                                    HOJA DE REVISIÓN PARA TRABAJOS DE LAVADO Y LUBRICACIÓN
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;"></td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    BIEN
                                </td>
                                <td style="width:35px;color:red;font-size: 10px;" align="center">
                                    MAL
                                </td>
                                <td style="width:45px;color:red;font-size: 10px;" align="center">
                                    REVISADO
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    NIVEL DE ACEITE DE MOTOR CORRECTO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[0] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[0] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[0] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    FILTRO DE ACEITE DE MOTOR NUEVO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[1] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[1] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[1] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    NIVEL DE LÍQUIDO DE FRENOS CORRECTO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[2] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[2] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[2] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    NIVEL DE LÍQUIDO DE TRANSMISIÓN AUTOMÁTICA CORRECTO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[3] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[3] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[3] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    NIVEL DE FLUIDO DE EMBRAGUE HIDRÁILICO CORRECTO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[4] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[4] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[4] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    MOTOR LIMPIO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[5] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[5] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[5] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    BASTIDOR LIMPIO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[6] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[6] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[6] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    TERMINALES DE ACUMULADOR LIMPIO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[7] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[7] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[7] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    BISAGRAS DE PUERTAS LUBRICADAS CORRECTAMENTE
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[8] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[8] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[8] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    FILTRO DE ACEITE DE MOTOR APRETADO CORRECTAMENTE
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[9] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[9] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[9] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    TAPÓN DE DRENADO DE ACEITE DE CARTER APRETADO CORRECTAMENTE
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[10] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[10] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[10] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    NIVEL DE REFRIGERANTE DE MOTOR CORRECTO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[11] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[11] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[11] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:180px;padding-top:5px;padding-bottom:5px;font-size:8px;">
                                    NIVEL DE AGUA LIMPIA PARABRISAS CORRECTO
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[12] == "Bien"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:35px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[12] == "Mal"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="width:45px;border:1px solid #337ab7;color:#337ab7;" align="center">
                                    <?php if (isset($lavado)): ?>
                                        <?php if ($lavado[12] == "Revisado"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width: 10px;" >
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="border:2px solid #337ab7;width:700px;border-radius: 8px;margin-left:20px;font-size:8px;">
                            <tr>
                                 <td style="width:50%;padding-bottom:5px;font-size:8px;color:#337ab7;">
                                    <br>
                                    UNIDAD RECHAZADA:
                                    <span style="color:black;"><?php if(isset($unidadRechazada)) echo "<u>&nbsp;&nbsp;".$unidadRechazada."&nbsp;&nbsp;</u>"; else echo "<u>&nbsp;&nbsp;&nbsp;&nbsp;</u>";  ?></span>
                                </td>
                                 <td style="width:50%;padding-bottom:5px;font-size:8px;color:#337ab7;">
                                    NOMBRE DEL JEFE DE TALLER:
                                    <span style="color:black;"><?php if(isset($nombreJefeTaller)) echo "<u>&nbsp;&nbsp;".$nombreJefeTaller."&nbsp;&nbsp;</u>"; else echo "<u>&nbsp;&nbsp;&nbsp;&nbsp;</u>";  ?></span>
                                    <img class='marcoImg' src='<?php if(isset($firmaJefeTaller)) if($firmaJefeTaller != "") echo base_url().$firmaJefeTaller; else echo base_url().'assets/imgs/fondo_bco.jpeg'; else echo  base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaJDT' style='width:80px;height:30px;'>
                                </td>
                            </tr>
                            <tr>
                                 <td style="width:50%;padding-bottom:5px;font-size:8px;color:#337ab7;">
                                    UNIDAD ACEPTADA:
                                    <span style="color:black;"><?php if(isset($unidadAprobada)) echo "<u>&nbsp;&nbsp;".$unidadAprobada."&nbsp;&nbsp;</u>"; else echo "<u>&nbsp;&nbsp;&nbsp;&nbsp;</u>";  ?></span>
                                    <br>
                                </td>
                                 <td style="width:50%;padding-bottom:5px;font-size:8px;color:#337ab7;">
                                    NOMBRE DEL AUDITOR:
                                    <span style="color:black;"><?php if(isset($nombreAuditor)) echo "<u>&nbsp;&nbsp;".$nombreAuditor."&nbsp;&nbsp;</u>"; else echo "<u>&nbsp;&nbsp;&nbsp;&nbsp;</u>";  ?></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                 <td style="width:50%;padding-bottom:5px;font-size:8px;color:#337ab7;">
                                    NOMBRE DEL MECÁNICO:
                                    <span style="color:black;"><?php if(isset($nombreMecanico)) echo "<u>&nbsp;&nbsp;".$nombreMecanico."&nbsp;&nbsp;</u>"; else echo "<u>&nbsp;&nbsp;&nbsp;&nbsp;</u>";  ?></span>
                                    <br>
                                </td>
                                 <td style="width:50%;padding-bottom:5px;font-size:8px;color:#337ab7;">
                                    FECHA:
                                    <span style="color:black;"><?php if(isset($fechaRegistro)) echo "<u>&nbsp;&nbsp;".$fechaRegistro."&nbsp;&nbsp;</u>"; else echo "<u>&nbsp;&nbsp;&nbsp;&nbsp;</u>";  ?></span>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                 <td style="width:50%;padding-bottom:5px;font-size:8px;color:#337ab7;">
                                    COMENTARIO:
                                    <p align="justify" style="text-align: justify; font-size:7px;margin-bottom: 0px;color:black">
                                        <?php if(isset($comentario)) echo $comentario; ?>
                                    </p>
                                </td>
                                <td style="width:50%;padding-top:5px;padding-bottom:5px;font-size:9px;">
                                    No. DE ORDEN DE SERVICIO:
                                    <span style="color:black;"><?php if(isset($idOrden)) echo "<u>&nbsp;&nbsp;".$idOrden."&nbsp;&nbsp;</u>"; else echo "<u>&nbsp;&nbsp;&nbsp;&nbsp;</u>";  ?></span>

                                    <br>
                                    Orden Intelisis: &nbsp;&nbsp;
                                    <label class="" style="color:darkblue;"><?php if(isset($orden_intelisis)) echo $orden_intelisis;  ?></label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>

<?php 
    try {
        $html = ob_get_clean();
        ob_clean();

        $mpdf = new \mPDF('utf-8', 'A4-P');
        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','debug' => TRUE]);
        
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        
        //$mpdf->AddPage();
        //$hoja_2 = '<br>';
        //$mpdf->WriteHTML($hoja_2);
        
        $mpdf->Output();

    //} catch (Html2PdfException $e) {
    } catch (Exception $e) {
        //$html2pdf->clean();
        //$formatter = new ExceptionFormatter($e);
        //echo $formatter->getHtmlMessage(); 
        echo "No se pudo cargar el PDF";
    }

 ?>