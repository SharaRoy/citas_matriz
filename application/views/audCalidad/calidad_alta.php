<br><br>
<div class="" style="margin:20px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."multipunto/AuditoriaCalidad/validateForm"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row">
            <div class="col-sm-8" align="right">
                <h2 style="text-align:center;color:red;">AUDITORÍA DE CALIDAD </h2>
            </div>
            <div class="col-sm-4" align="right">
                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                    Regresar
                </button>
            </div>
        </div>

        <br>
        <div class="row table-responsive">
            <!-- Lado derecho del formulario -->
            <div class="col-sm-6">
                <h5 style="text-align:justify;color:#337ab7;font-weight: bold;">DEPARTAMENTO DE SERVICIO</h5>
                Folio Intelisis: &nbsp;&nbsp;<label style="color:darkblue;"><?php if(isset($idIntelisis)) echo $idIntelisis;  ?></label>
                <p style="text-align:justify;color:#337ab7;" align="justify">
                    De acuerdo a nuestros registros, su próximo servicio será de <input class="input_field" type="text" name="proxServicio" style="width:2.5cm;" value="<?php echo set_value('proxServicio');?>"> <?php echo form_error('proxServicio', '<span class="error">', '</span>'); ?>.Kms.
                    Y su promedio de recorrido, la fecha probable para realizarlo será el <input class="input_field" type="date" min="<?php echo date("Y-m-d"); ?>" name="fechaAproxServ" style="width:2.5cm;" value="<?php if(set_value('fechaAproxServ') != "") echo set_value('fechaAproxServ'); else echo date("Y-m-d");?>"> <?php echo form_error('fechaAproxServ', '<span class="error">', '</span>'); ?>.
                    favor de hacer su reservación para el próximo TEL: 
                </p>

                <br><br>
                <table style="width:100%;padding-left: 7px;">
                    <tr>
                        <td colspan="4" style="background-color:#337ab7;color:white;font-size:14px;padding-left:5px;">
                            HOJA DE REVISIÓN PARA TRABAJOS DE AFINACIÓN
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;"></td>
                        <td style="width:15%;color:red;" align="center">
                            BIEN
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            MAL
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            REVISADO
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            MOTOR LIMPIO
                            <?php echo form_error('afinacion_1[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="motorCheck" name="afinacion_1[]" value="Bien" <?php echo set_checkbox('afinacion_1[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="motorCheck" name="afinacion_1[]" value="Mal" <?php echo set_checkbox('afinacion_1[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="motorCheck" name="afinacion_1[]" value="Revisado" <?php echo set_checkbox('afinacion_1[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            BUJIAS NUEVAS (SI REQUIERE)
                            <?php echo form_error('afinacion_2[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="bujiaCheck" name="afinacion_2[]" value="Bien" <?php echo set_checkbox('afinacion_2[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="bujiaCheck" name="afinacion_2[]" value="Mal" <?php echo set_checkbox('afinacion_2[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="bujiaCheck" name="afinacion_2[]" value="Revisado" <?php echo set_checkbox('afinacion_2[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            FILTRO DE COMBUSTIBLE NUEVO
                            <?php echo form_error('afinacion_3[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="filtroCombustibleCheck" name="afinacion_3[]" value="Bien" <?php echo set_checkbox('afinacion_3[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="filtroCombustibleCheck" name="afinacion_3[]" value="Mal" <?php echo set_checkbox('afinacion_3[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="filtroCombustibleCheck" name="afinacion_3[]" value="Revisado" <?php echo set_checkbox('afinacion_3[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            FILTRO DE AIRE NUEVO
                            <?php echo form_error('afinacion_4[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="fAireCheck" name="afinacion_4[]" value="Bien" <?php echo set_checkbox('afinacion_4[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="fAireCheck" name="afinacion_4[]" value="Mal" <?php echo set_checkbox('afinacion_4[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="fAireCheck" name="afinacion_4[]" value="Revisado" <?php echo set_checkbox('afinacion_4[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            CABLES DE BUJIAS NUEVOS (SI REQUIERE)
                            <?php echo form_error('afinacion_5[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="cBujiaCheck" name="afinacion_5[]" value="Bien" <?php echo set_checkbox('afinacion_5[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="cBujiaCheck" name="afinacion_5[]" value="Mal" <?php echo set_checkbox('afinacion_5[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="cBujiaCheck" name="afinacion_5[]" value="Revisado" <?php echo set_checkbox('afinacion_5[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            TIEMPO INICIAL DE ENCENDIDO DENTRO DE
                            ESPECIFICACIONES
                            <?php echo form_error('afinacion_6[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="tiempoEnCheck" name="afinacion_6[]" value="Bien" <?php echo set_checkbox('afinacion_6[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="tiempoEnCheck" name="afinacion_6[]" value="Mal" <?php echo set_checkbox('afinacion_6[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="tiempoEnCheck" name="afinacion_6[]" value="Revisado" <?php echo set_checkbox('afinacion_6[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="background-color:#337ab7;color:white;font-size:14px;padding-left:5px;">
                            HOJA DE REVISIÓN PARA TRABAJO DE FRENOS
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;"></td>
                        <td style="width:15%;color:red;" align="center">
                            BIEN
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            MAL
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            REVISADO
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            ASENTAMIENTO DE BALATAS
                            <?php echo form_error('t_frenos_1[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="balatasCheck" name="t_frenos_1[]" value="Bien" <?php echo set_checkbox('t_frenos_1[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="balatasCheck" name="t_frenos_1[]" value="Mal" <?php echo set_checkbox('t_frenos_1[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="balatasCheck" name="t_frenos_1[]" value="Revisado" <?php echo set_checkbox('t_frenos_1[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            SE REALIZÓ PRUEBA DE CARRETERA
                            <?php echo form_error('t_frenos_2[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="pCarreteraCheck" name="t_frenos_2[]" value="Bien" <?php echo set_checkbox('t_frenos_2[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="pCarreteraCheck" name="t_frenos_2[]" value="Mal" <?php echo set_checkbox('t_frenos_2[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="pCarreteraCheck" name="t_frenos_2[]" value="Revisado" <?php echo set_checkbox('t_frenos_2[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            NIVEL LÍQUIDO DE FRENOS CORRECTO
                            <?php echo form_error('t_frenos_3[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="nLiquidoCheck" name="t_frenos_3[]" value="Bien" <?php echo set_checkbox('t_frenos_3[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="nLiquidoCheck" name="t_frenos_3[]" value="Mal" <?php echo set_checkbox('t_frenos_3[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="nLiquidoCheck" name="t_frenos_3[]" value="Revisado" <?php echo set_checkbox('t_frenos_3[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            FRENOS RUIDOSOS
                            <?php echo form_error('t_frenos_4[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="ruidosFrenosCheck" name="t_frenos_4[]" value="Bien" <?php echo set_checkbox('t_frenos_4[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="ruidosFrenosCheck" name="t_frenos_4[]" value="Mal" <?php echo set_checkbox('t_frenos_4[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="ruidosFrenosCheck" name="t_frenos_4[]" value="Revisado" <?php echo set_checkbox('t_frenos_4[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            AL APLICAR PEDAL DE FRENO SE JALA EL VEHÍCULO
                            HACIA ALGÚN LADO
                            <?php echo form_error('t_frenos_5[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="pedalFrenoCheck" name="t_frenos_5[]" value="Bien" <?php echo set_checkbox('t_frenos_5[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="pedalFrenoCheck" name="t_frenos_5[]" value="Mal" <?php echo set_checkbox('t_frenos_5[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="pedalFrenoCheck" name="t_frenos_5[]" value="Revisado" <?php echo set_checkbox('t_frenos_5[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="background-color:#337ab7;color:white;font-size:14px;padding-left:5px;">
                            HOJA DE REVISIÓN PARA TRABAJOS DE ALINEACIÓN, BALANCEO Y EJE TRASERO
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;"></td>
                        <td style="width:15%;color:red;" align="center">
                            BIEN
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            MAL
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            REVISADO
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            SE EFECTUÓ PRUEBA DE CARRETERA
                            <?php echo form_error('alineacion_1[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_1Check" name="alineacion_1[]" value="Bien" <?php echo set_checkbox('alineacion_1[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_1Check" name="alineacion_1[]" value="Mal" <?php echo set_checkbox('alineacion_1[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_1Check" name="alineacion_1[]" value="Revisado" <?php echo set_checkbox('alineacion_1[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            EL VEHÍCULO TIENDE A IRSE HACIA UN LADO
                            <?php echo form_error('alineacion_2[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_2Check" name="alineacion_2[]" value="Bien" <?php echo set_checkbox('alineacion_2[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_2Check" name="alineacion_2[]" value="Mal" <?php echo set_checkbox('alineacion_2[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_2Check" name="alineacion_2[]" value="Revisado" <?php echo set_checkbox('alineacion_2[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            AL FRENAR EL VEHÍCULO CONSERVA SU
                            TRAYECTORIA SIN JALARSE HACIA UN LADO
                            <?php echo form_error('alineacion_3[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_3Check" name="alineacion_3[]" value="Bien" <?php echo set_checkbox('alineacion_3[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_3Check" name="alineacion_3[]" value="Mal" <?php echo set_checkbox('alineacion_3[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_3Check" name="alineacion_3[]" value="Revisado" <?php echo set_checkbox('alineacion_3[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            EXISTE VIBRACIÓN EN EL VEHÍCULO A
                            DETERMINADA VELOCIDAD
                            <?php echo form_error('alineacion_4[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_4Check" name="alineacion_4[]" value="Bien" <?php echo set_checkbox('alineacion_4[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_4Check" name="alineacion_4[]" value="Mal" <?php echo set_checkbox('alineacion_4[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_4Check" name="alineacion_4[]" value="Revisado" <?php echo set_checkbox('alineacion_4[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            TUERCAS DE RUEDA APRETADAS SEGÚN
                            ESPECIFICACIÓN
                            <?php echo form_error('alineacion_5[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_5Check" name="alineacion_5[]" value="Bien" <?php echo set_checkbox('alineacion_5[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_5Check" name="alineacion_5[]" value="Mal" <?php echo set_checkbox('alineacion_5[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_5Check" name="alineacion_5[]" value="Revisado" <?php echo set_checkbox('alineacion_5[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            EJE TRASERO
                            <?php echo form_error('alineacion_6[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_6Check" name="alineacion_6[]" value="Bien" <?php echo set_checkbox('alineacion_6[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_6Check" name="alineacion_6[]" value="Mal" <?php echo set_checkbox('alineacion_6[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="alineacion_6Check" name="alineacion_6[]" value="Revisado" <?php echo set_checkbox('alineacion_6[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="background-color:#337ab7;color:white;font-size:14px;padding-left:5px;">
                            HOJA DE REVISIÓN PARA TRABAJOS DE AIRE ACONDICIONADO Y CALEFACCIÓN
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;"></td>
                        <td style="width:15%;color:red;" align="center">
                            BIEN
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            MAL
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            REVISADO
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            CONTROL DE TEMPERATURA FUNCIONA CORRECTAMENTE
                            <?php echo form_error('trabajoAA_1[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_1Check" name="trabajoAA_1[]" value="Bien" <?php echo set_checkbox('trabajoAA_1[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_1Check" name="trabajoAA_1[]" value="Mal" <?php echo set_checkbox('trabajoAA_1[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_1Check" name="trabajoAA_1[]" value="Revisado" <?php echo set_checkbox('trabajoAA_1[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                          SELECTOR DE VELOCIDADES DEL MOTOR SOPLADOR FUNCIONA BIEN
                            <?php echo form_error('trabajoAA_2[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_2Check" name="trabajoAA_2[]" value="Bien" <?php echo set_checkbox('trabajoAA_2[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_2Check" name="trabajoAA_2[]" value="Mal" <?php echo set_checkbox('trabajoAA_2[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_2Check" name="trabajoAA_2[]" value="Revisado" <?php echo set_checkbox('trabajoAA_2[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            FLUJO DE AIRE CORRECTO, DEPENDIENDO FUNCIÓN SELECCIONADA (DEFROST, PANEL A/C, MANUAL A/C, ETC.)
                            <?php echo form_error('trabajoAA_3[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_3Check" name="trabajoAA_3[]" value="Bien" <?php echo set_checkbox('trabajoAA_3[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_3Check" name="trabajoAA_3[]" value="Mal" <?php echo set_checkbox('trabajoAA_3[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_3Check" name="trabajoAA_3[]" value="Revisado" <?php echo set_checkbox('trabajoAA_3[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            TEMPERATURA DE AIRE ADECUADA (UTILIZANDO TERMÓMETRO)
                            <?php echo form_error('trabajoAA_4[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_4Check" name="trabajoAA_4[]" value="Bien" <?php echo set_checkbox('trabajoAA_4[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_4Check" name="trabajoAA_4[]" value="Mal" <?php echo set_checkbox('trabajoAA_4[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="trabajoAA_4Check" name="trabajoAA_4[]" value="Revisado" <?php echo set_checkbox('trabajoAA_4[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                </table>
            </div>

            <!-- Lado izquierdo del formulario -->
            <div class="col-sm-6">
                <table style="width:100%;padding-left: 7px;">
                    <tr>
                        <td colspan="4" style="background-color:#337ab7;color:white;font-size:14px;padding-left:5px;">
                          HOJA DE REVISIÓN GENERAL DE LA UNIDAD EN CUALQUIER TIPO DE REPARACIÓN
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;"></td>
                        <td style="width:15%;color:red;" align="center">
                            BIEN
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            MAL
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            REVISADO
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            DAÑOS EN LA CARROCERÍA
                            <?php echo form_error('general_1[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_1Check" name="general_1[]" value="Bien" <?php echo set_checkbox('general_1[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_1Check" name="general_1[]" value="Mal" <?php echo set_checkbox('general_1[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_1Check" name="general_1[]" value="Revisado" <?php echo set_checkbox('general_1[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            DAÑOS EN LA VESTIDURA
                            <?php echo form_error('general_2[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_2Check" name="general_2[]" value="Bien" <?php echo set_checkbox('general_2[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_2Check" name="general_2[]" value="Mal" <?php echo set_checkbox('general_2[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_2Check" name="general_2[]" value="Revisado" <?php echo set_checkbox('general_2[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            VOLANTE DE DIRECCIÓN LIMPIO
                            <?php echo form_error('general_3[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_3Check" name="general_3[]" value="Bien" <?php echo set_checkbox('general_3[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_3Check" name="general_3[]" value="Mal" <?php echo set_checkbox('general_3[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_3Check" name="general_3[]" value="Revisado" <?php echo set_checkbox('general_3[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            CARROCERÍA LIMPIA
                            <?php echo form_error('general_4[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_4Check" name="general_4[]" value="Bien" <?php echo set_checkbox('general_4[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_4Check" name="general_4[]" value="Mal" <?php echo set_checkbox('general_4[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_4Check" name="general_4[]" value="Revisado" <?php echo set_checkbox('general_4[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            CENICEROS LIMPIOS
                            <?php echo form_error('general_5[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_5Check" name="general_5[]" value="Bien" <?php echo set_checkbox('general_5[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_5Check" name="general_5[]" value="Mal" <?php echo set_checkbox('general_5[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_5Check" name="general_5[]" value="Revisado" <?php echo set_checkbox('general_5[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            FUNCIONAMIENTO ADECUADO DE CINTURONES
                            <?php echo form_error('general_6[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_6Check" name="general_6[]" value="Bien" <?php echo set_checkbox('general_6[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_6Check" name="general_6[]" value="Mal" <?php echo set_checkbox('general_6[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="general_6Check" name="general_6[]" value="Revisado" <?php echo set_checkbox('general_6[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="background-color:#337ab7;color:white;font-size:14px;padding-left:5px;">
                            HOJA DE REVISIÓN PARA TRABAJOS DE TRANSMISIÓN AUTOMÁTICA
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;"></td>
                        <td style="width:15%;color:red;" align="center">
                            BIEN
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            MAL
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            REVISADO
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            PRESIÓN DE CONTROL DENTRO DE ESPECIFICACIONES
                            <?php echo form_error('transmision_auto_1[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_1Check" name="transmision_auto_1[]" value="Bien" <?php echo set_checkbox('transmision_auto_1[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_1Check" name="transmision_auto_1[]" value="Mal" <?php echo set_checkbox('transmision_auto_1[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_1Check" name="transmision_auto_1[]" value="Revisado" <?php echo set_checkbox('transmision_auto_1[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            PRESIÓN DE ACELERACIÓN DENTRO DE ESPECIFICACIONES
                            <?php echo form_error('transmision_auto_2[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_2Check" name="transmision_auto_2[]" value="Bien" <?php echo set_checkbox('transmision_auto_2[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_2Check" name="transmision_auto_2[]" value="Mal" <?php echo set_checkbox('transmision_auto_2[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_2Check" name="transmision_auto_2[]" value="Revisado" <?php echo set_checkbox('transmision_auto_2[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            SE REALIZÓ PRUEBA DE CARRETERA
                            <?php echo form_error('transmision_auto_3[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_3Check" name="transmision_auto_3[]" value="Bien" <?php echo set_checkbox('transmision_auto_3[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_3Check" name="transmision_auto_3[]" value="Mal" <?php echo set_checkbox('transmision_auto_3[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_3Check" name="transmision_auto_3[]" value="Revisado" <?php echo set_checkbox('transmision_auto_3[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            NIVEL DE FLUIDO DE TRANSMISIÓN CORRECTO (EN CALIENTE)
                            <?php echo form_error('transmision_auto_4[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_4Check" name="transmision_auto_4[]" value="Bien" <?php echo set_checkbox('transmision_auto_4[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_4Check" name="transmision_auto_4[]" value="Mal" <?php echo set_checkbox('transmision_auto_4[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_4Check" name="transmision_auto_4[]" value="Revisado" <?php echo set_checkbox('transmision_auto_4[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            LOS CAMBIOS DE VELOCIDAD SE EFECTÚAN CORRECTAMENTE
                            <?php echo form_error('transmision_auto_5[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_5Check" name="transmision_auto_5[]" value="Bien" <?php echo set_checkbox('transmision_auto_5[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_5Check" name="transmision_auto_5[]" value="Mal" <?php echo set_checkbox('transmision_auto_5[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_5Check" name="transmision_auto_5[]" value="Revisado" <?php echo set_checkbox('transmision_auto_5[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            SELECTOR DE VELOCIDADES AJUSTADO CORRECTAMENTE
                            <?php echo form_error('transmision_auto_6[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_6Check" name="transmision_auto_6[]" value="Bien" <?php echo set_checkbox('transmision_auto_6[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_6Check" name="transmision_auto_6[]" value="Mal" <?php echo set_checkbox('transmision_auto_6[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_6Check" name="transmision_auto_6[]" value="Revisado" <?php echo set_checkbox('transmision_auto_6[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            CAMBIOS DE VELOCIDAD BRUSCO
                            <?php echo form_error('transmision_auto_7[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_7Check" name="transmision_auto_7[]" value="Bien" <?php echo set_checkbox('transmision_auto_7[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_7Check" name="transmision_auto_7[]" value="Mal" <?php echo set_checkbox('transmision_auto_7[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_7Check" name="transmision_auto_7[]" value="Revisado" <?php echo set_checkbox('transmision_auto_7[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            CAMBIOS DE VELOCIDAD DEMASIADO SUAVES
                            <?php echo form_error('transmision_auto_8[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_8Check" name="transmision_auto_8[]" value="Bien" <?php echo set_checkbox('transmision_auto_8[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_8Check" name="transmision_auto_8[]" value="Mal" <?php echo set_checkbox('transmision_auto_8[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_8Check" name="transmision_auto_8[]" value="Revisado" <?php echo set_checkbox('transmision_auto_8[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            SE SOBRERREVOLUCIONA EL MOTOR AL EFECTUAR LOS CAMBIOS
                            <?php echo form_error('transmision_auto_9[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_9Check" name="transmision_auto_9[]" value="Bien" <?php echo set_checkbox('transmision_auto_9[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_9Check" name="transmision_auto_9[]" value="Mal" <?php echo set_checkbox('transmision_auto_9[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_9Check" name="transmision_auto_9[]" value="Revisado" <?php echo set_checkbox('transmision_auto_9[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            SE PATINA LA TRANSMISIÓN
                            <?php echo form_error('transmision_auto_10[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_10Check" name="transmision_auto_10[]" value="Bien" <?php echo set_checkbox('transmision_auto_10[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_10Check" name="transmision_auto_10[]" value="Mal" <?php echo set_checkbox('transmision_auto_10[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="transmision_auto_10Check" name="transmision_auto_10[]" value="Revisado" <?php echo set_checkbox('transmision_auto_10[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="background-color:#337ab7;color:white;font-size:14px;padding-left:5px;">
                          HOJA DE REVISIÓN PARA TRABAJOS DE LAVADO Y LUBRICACIÓN
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;"></td>
                        <td style="width:15%;color:red;" align="center">
                            BIEN
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            MAL
                        </td>
                        <td style="width:15%;color:red;" align="center">
                            REVISADO
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            NIVEL DE ACEITE DE MOTOR CORRECTO
                            <?php echo form_error('lavado_1[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_1Check" name="lavado_1[]" value="Bien" <?php echo set_checkbox('lavado_1[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_1Check" name="lavado_1[]" value="Mal" <?php echo set_checkbox('lavado_1[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_1Check" name="lavado_1[]" value="Revisado" <?php echo set_checkbox('lavado_1[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            FILTRO DE ACEITE DE MOTOR NUEVO
                            <?php echo form_error('lavado_2[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_2Check" name="lavado_2[]" value="Bien" <?php echo set_checkbox('lavado_2[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_2Check" name="lavado_2[]" value="Mal" <?php echo set_checkbox('lavado_2[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_2Check" name="lavado_2[]" value="Revisado" <?php echo set_checkbox('lavado_2[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            NIVEL DE LÍQUIDO DE FRENOS CORRECTO
                            <?php echo form_error('lavado_3[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_3Check" name="lavado_3[]" value="Bien" <?php echo set_checkbox('lavado_3[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_3Check" name="lavado_3[]" value="Mal" <?php echo set_checkbox('lavado_3[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_3Check" name="lavado_3[]" value="Revisado" <?php echo set_checkbox('lavado_3[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                          NIVEL DE LÍQUIDO DE TRANSMISIÓN AUTOMÁTICA CORRECTO
                            <?php echo form_error('lavado_4[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_4Check" name="lavado_4[]" value="Bien" <?php echo set_checkbox('lavado_4[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_4Check" name="lavado_4[]" value="Mal" <?php echo set_checkbox('lavado_4[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_4Check" name="lavado_4[]" value="Revisado" <?php echo set_checkbox('lavado_4[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            NIVEL DE FLUIDO DE EMBRAGUE HIDRÁILICO CORRECTO
                            <?php echo form_error('lavado_5[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_5Check" name="lavado_5[]" value="Bien" <?php echo set_checkbox('lavado_5[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_5Check" name="lavado_5[]" value="Mal" <?php echo set_checkbox('lavado_5[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_5Check" name="lavado_5[]" value="Revisado" <?php echo set_checkbox('lavado_5[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            MOTOR LIMPIO
                            <?php echo form_error('lavado_6[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_6Check" name="lavado_6[]" value="Bien" <?php echo set_checkbox('lavado_6[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_6Check" name="lavado_6[]" value="Mal" <?php echo set_checkbox('lavado_6[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_6Check" name="lavado_6[]" value="Revisado" <?php echo set_checkbox('lavado_6[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            BASTIDOR LIMPIO
                            <?php echo form_error('lavado_7[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_7Check" name="lavado_7[]" value="Bien" <?php echo set_checkbox('lavado_7[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_7Check" name="lavado_7[]" value="Mal" <?php echo set_checkbox('lavado_7[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_7Check" name="lavado_7[]" value="Revisado" <?php echo set_checkbox('lavado_7[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            TERMINALES DE ACUMULADOR LIMPIO
                            <?php echo form_error('lavado_8[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_8Check" name="lavado_8[]" value="Bien" <?php echo set_checkbox('lavado_8[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_8Check" name="lavado_8[]" value="Mal" <?php echo set_checkbox('lavado_8[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_8Check" name="lavado_8[]" value="Revisado" <?php echo set_checkbox('lavado_8[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            BISAGRAS DE PUERTAS LUBRICADAS CORRECTAMENTE
                            <?php echo form_error('lavado_9[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_9Check" name="lavado_9[]" value="Bien" <?php echo set_checkbox('lavado_9[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_9Check" name="lavado_9[]" value="Mal" <?php echo set_checkbox('lavado_9[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_9Check" name="lavado_9[]" value="Revisado" <?php echo set_checkbox('lavado_9[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                          FILTRO DE ACEITE DE MOTOR APRETADO CORRECTAMENTE
                            <?php echo form_error('lavado_10[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_10Check" name="lavado_10[]" value="Bien" <?php echo set_checkbox('lavado_10[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_10Check" name="lavado_10[]" value="Mal" <?php echo set_checkbox('lavado_10[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_10Check" name="lavado_10[]" value="Revisado" <?php echo set_checkbox('lavado_10[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            TAPÓN DE DRENADO DE ACEITE DE CARTER APRETADO CORRECTAMENTE
                            <?php echo form_error('lavado_11[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_11Check" name="lavado_11[]" value="Bien" <?php echo set_checkbox('lavado_11[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_11Check" name="lavado_11[]" value="Mal" <?php echo set_checkbox('lavado_11[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_11Check" name="lavado_11[]" value="Revisado" <?php echo set_checkbox('lavado_11[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            NIVEL DE REFRIGERANTE DE MOTOR CORRECTO
                            <?php echo form_error('lavado_12[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_12Check" name="lavado_12[]" value="Bien" <?php echo set_checkbox('lavado_12[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_12Check" name="lavado_12[]" value="Mal" <?php echo set_checkbox('lavado_12[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_12Check" name="lavado_12[]" value="Revisado" <?php echo set_checkbox('lavado_12[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:55%;padding-top:5px;padding-bottom:5px;">
                            NIVEL DE AGUA LIMPIA PARABRISAS CORRECTO
                            <?php echo form_error('lavado_13[]', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_13Check" name="lavado_13[]" value="Bien" <?php echo set_checkbox('lavado_13[]', 'Bien'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_13Check" name="lavado_13[]" value="Mal" <?php echo set_checkbox('lavado_13[]', 'Mal'); ?>>
                        </td>
                        <td style="width:15%;border:1px solid #337ab7;color:#337ab7;" align="center">
                            <input type="checkbox" class="lavado_13Check" name="lavado_13[]" value="Revisado" <?php echo set_checkbox('lavado_13[]', 'Revisado'); ?>>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row table-responsive">
            <div class="col-sm-12">
                <table style="border:2px solid #337ab7;width:90%;border-radius: 4px;margin:30px;">
                    <tr>
                        <td style="width:35%;padding-top:5px;padding-bottom:5px;">
                            UNIDAD RECHAZADA:
                            <input class="input_field" type="text" name="unidadRechazada" style="width:60%;" value="<?php echo set_value('unidadRechazada');?>">
                            <?php echo form_error('unidadRechazada', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:40%;padding-top:5px;padding-bottom:5px;">
                            NOMBRE DEL JEFE DE TALLER:
                            <input class="input_field" type="text" name="nombreJefeTaller" style="width:40%;" value="JUAN CHAVEZ">
                            <?php echo form_error('nombreJefeTaller', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="width:15%;padding-top:5px;padding-bottom:5px;">
                            <input type='hidden' id='firmaJefeTaller' name='firmaJefeTaller' value='<?php echo set_value("firmaJefeTaller"); ?>'>
                            <img class='marcoImg' src='<?php if(set_value("firmaJefeTaller") != "") echo set_value("firmaJefeTaller"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaJDT' style='width:80px;height:30px;'>
                            <a id="AsesorCostos" class='cuadroFirma btn btn-primary' data-value="JDT" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                <i class='fas fa-pen-fancy'></i>
                            </a>
                            <?php echo form_error('firmaJefeTaller', '<br><span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:45%;padding-top:5px;padding-bottom:5px;">
                            UNIDAD ACEPTADA:
                            <input class="input_field" type="text" name="unidadAprobada" style="width:60%;" value="<?php echo set_value('unidadAprobada');?>">
                            <?php echo form_error('unidadAprobada', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="padding-top:5px;padding-bottom:5px;" colspan="2">
                            NOMBRE DEL AUDITOR:
                            <input class="input_field" type="text" name="nombreAuditor" style="width:60%;" value="<?php echo set_value('nombreAuditor');?>">
                            <?php echo form_error('nombreAuditor', '<br><span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:45%;padding-top:5px;padding-bottom:5px;">
                            NOMBRE DEL MECÁNICO:
                            <input class="input_field" type="text" name="nombreMecanico" style="width:60%;" value="<?php echo set_value('nombreMecanico');?>">
                            <?php echo form_error('nombreMecanico', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="padding-top:5px;padding-bottom:5px;" colspan="2">
                            FECHA:
                            <input class="input_field" type="date" min="<?php echo date("Y-m-d"); ?>" max="<?php echo date("Y-m-d"); ?>" name="fechaRegistro" style="width:60%;text-align:center;" value="<?php if(set_value('fechaRegistro') != "") echo set_value('fechaRegistro'); else echo date("Y-m-d");?>">
                            <?php echo form_error('fechaRegistro', '<br><span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="2">
                            COMENTARIO:<br>
                            <textarea rows='2' name="comentario" class="form-control" style='width:90%;text-align:left;font-size:14px;'><?php echo set_value('comentario'); ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:45%;padding-top:5px;padding-bottom:5px;">
                            <br>
                        </td>
                        <td style="padding-top:5px;padding-bottom:5px;" colspan="2">
                            No. DE ORDEN DE SERVICIO:
                            <input class="input_field" type="text" name="noOrden" style="width:60%;" value="<?php if(isset($idOrden)) echo $idOrden; else echo set_value('noOrden');?>">
                            <?php echo form_error('noOrden', '<br><span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br><br>
        <div class="col-sm-12" align="center">
            <input type="hidden" name="modoVistaFormulario" id="" value="1">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <button type="submit" class="btn btn-primary" name="aceptarCotizacion" id="aceptarCotizacion" >Guardar</button>
        </div>
    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
