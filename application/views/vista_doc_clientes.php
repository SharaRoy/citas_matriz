<div style="margin:10px;">
    <div class="panel-body" style="display: <?= (($liquidar_firmas == '1') ? 'none' : 'inline') ?>" >
        <div class="row">
            <div class="col-sm-12">
                <table style="width:100%;">
                    <tr>
                        <td width="20%">
                            <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:100%;">
                        </td>
                        <td style="padding-left:10px; width:66%;" align="center">
                            <strong>
                                <span style="color: #337ab7;" style="">
                                    <?= SUCURSAL ?>, S.A. DE C.V.
                                </span>
                            </strong>

                            <p class="justify" style="text-align: center;">
                                <?=$this->config->item('encabezados_txt')?>
                            </p>
                        </td>
                        <td width="25%" align="" style="padding-left:10px;">
                            <div>
                                <img src="<?= base_url() ?>assets/imgs/logo.png" alt="" class="logo" style="width:100px;height:35px;"><br>
                            </div>
                        </td>
                    </tr>
                </table> 

                <br><br>
                <table style="width:100%;border: 1.5px solid darkblue;border-radius: 4px;">
                    <tr style="border-bottom: 1.5px solid darkblue;color: #337ab7;">
                        <td colspan="6" style="vertical-align: middle;">
                            <h4 style="text-align: center;font-weight: bold;">
                               DATOS GENERALES DE LA ORDEN
                            </h4>
                        </td>
                    </tr>
                    <tr style="border-bottom: 1.5px solid darkblue;">
                        <td style="width: 15%;padding-left: 6px;">
                            <label style="font-size: 13px;"><strong>NO. ORDEN: </strong></label>
                            <br>
                            <label style="font-size: 11px;color: darkblue;" class="formato">
                                <?php if(isset($id_cita)) echo $id_cita; else echo ""; ?>
                            </label>
                        </td>

                        <td style="width: 20%;">
                            <label style="font-size: 13px;"><strong>SERIE: </strong></label>
                            <br>
                            <label style="font-size: 11px;color: darkblue;" class="formato">
                                <?php if(isset($serie)) echo strtoupper($serie); else echo ""; ?>
                            </label>
                        </td>

                        <td style="width: 15%;">
                            <label style="font-size: 13px;"><strong>PLACAS: </strong></label>
                            <br>
                            <label style="font-size: 11px;color: darkblue;" class="formato">
                                <?php if(isset($placas)) echo strtoupper($placas); else echo ""; ?>
                            </label>
                        </td>

                        <td>
                            <label style="font-size: 13px;"><strong>UNIDAD: </strong></label>
                            <br>
                            <label style="font-size: 11px;color: darkblue;" class="formato">
                                <?php if(isset($categoria)) echo strtoupper($categoria); else echo ""; ?>
                            </label>
                        </td>

                        <td style="width: 30%;">
                            <label style="font-size: 13px;"><strong>MODELO: </strong></label>
                            <br>
                            <label style="font-size: 11px;color: darkblue;" class="formato">
                                <?php if(isset($modelo)) echo strtoupper($modelo); else echo ""; ?>
                            </label>
                        </td>

                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-left: 6px;">
                            <label style="font-size: 13px;"><strong>TÉCNICO: </strong></label>
                            <br>
                            <label style="font-size: 11px;color: darkblue;" class="formato">
                                <?php if(isset($tecnico)) echo strtoupper($tecnico); else echo ""; ?>
                            </label>
                        </td>
       
                        <td colspan="2">
                            <label style="font-size: 13px;"><strong>ASESOR: </strong></label>
                            <br>
                            <label style="font-size: 11px;color: darkblue;" class="formato">
                                <?php if(isset($asesor)) echo strtoupper($asesor); else echo ""; ?>
                            </label>
                        </td>

                        <td colspan="2">
                            <label style="font-size: 13px;"><strong>CLIENTE: </strong></label>
                            <br>
                            <label style="font-size: 11px;color: darkblue;" class="formato">
                                <?php if(isset($cliente)) echo strtoupper($cliente); else echo ""; ?>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 align="center">
                            Solicitud de firmas en documentacion: <br>
                            Autorización por ausencia
                        </h3>
                    </div>
                </div>
                
                <br>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <br>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-4">
                                <?php if (isset($diagnostico)): ?>
                                    <div style="height: 4.4cm; border:2px solid darkorange;background-color: #f5cba7;border-radius: 15px;">
                                        <br>
                                        <h4 style="text-align: center;color: #c0392b;">DIAGNÓSTICO</h4>
                                        <a href="<?=base_url().'OrdenServicio_Firmas/'.$id_cita_enc;?>" class="btn btn-primary" target="_blank" style="font-size: 14px;margin-top: 10px;font-weight: bold;margin-left: 20px;">
                                            <i class="fa fa-indent x2" aria-hidden="true"></i>
                                            REVISAR/FIRMAR
                                        </a>
                                        <br>
                                        <a href="<?=base_url().'OrdenServicio_PDF/'.$id_cita_enc;?>" class="btn btn-warning" target="_blank" style="font-size: 14px;margin-top: 10px;font-weight: bold;margin-left: 20px;">
                                            <i class="fa fa-file-pdf-o x2" aria-hidden="true"></i>
                                            PDF
                                        </a>
                                    </div>
                                <?php endif ?>
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-4">
                                <?php if (isset($voz_cliente)): ?>
                                    <?php if ($voz_cliente == "1"): ?>
                                        <div style="height: 4.4cm; border:2px solid darkblue;background-color: #a2d9ce;border-radius: 15px;">
                                            <br>
                                            <h4 style="text-align: center;color: darkblue;">VOZ CLIENTE</h4>
                                            <a href="<?=base_url().'VCliente_Firmas/'.$id_cita_enc;?>" class="btn btn-primary" target="_blank" style="font-size: 14px;margin-top: 10px;font-weight: bold;margin-left: 20px;">
                                                <i class="fa fa-indent x2" aria-hidden="true"></i>
                                                REVISAR/FIRMAR
                                            </a>
                                            <br>
                                            <a href="<?=base_url().'VCliente_PDF/'.$id_cita_enc;?>" class="btn btn-warning" target="_blank" style="font-size: 14px;margin-top: 10px;font-weight: bold;margin-left: 20px;">
                                                <i class="fa fa-file-pdf-o x2" aria-hidden="true"></i>
                                                PDF
                                            </a>
                                        </div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-4">
                                <?php if (isset($carta_renuncia)): ?>
                                    <?php if ($carta_renuncia == "1"): ?>
                                        <div style="height: 4.4cm; border:2px solid #4a235a;background-color:  #e8daef;border-radius: 15px;">
                                            <br>
                                            <h4 style="text-align: center;color: #4a235a;">FORD PROTECT</h4>
                                            <a href="<?=base_url().'CartaRenuncia_Firmas/'.$id_cita_enc;?>" class="btn btn-primary" target="_blank" style="font-size: 14px;margin-top: 10px;font-weight: bold;margin-left: 20px;">
                                                <i class="fa fa-indent x2" aria-hidden="true"></i>
                                                REVISAR/FIRMAR
                                            </a>
                                            <br>
                                            <a href="<?=base_url().'CartaRenuncia_PDF/'.$id_cita_enc;?>" class="btn btn-warning" target="_blank" style="font-size: 14px;margin-top: 10px;font-weight: bold;margin-left: 20px;">
                                                <i class="fa fa-file-pdf-o x2" aria-hidden="true"></i>
                                                PDF
                                            </a>
                                        </div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-body" style="display: <?= (($liquidar_firmas == '0') ? 'none' : 'inline') ?>">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <div class="panel panel-primary">
                                <div class="panel-heading" align="center">
                                    <h3 class="panel-title">
                                        YA SE HA COMPLETADO LA DOCUMENTACIÓN PENDIENTE
                                        <br>
                                        ¡ESTA URL HA CADUCADO!
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-md-12" align="center" style="">
                            <img src="<?php echo base_url(); ?>assets/imgs/logos/logo_queretaro_2.png" alt="" class="logo" style="max-height: 2.5cm;">
                        </div>
                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </div>
</div>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" autoplay style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>
