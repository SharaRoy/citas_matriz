<div style="margin: 30px;" >
    <div class="row">
        <div class="col-md-12">
            <!-- Encabezado -->
            <table style="width: 100%;">
                <tr>
                    <td width="20%">
                        <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:150px;height:40px;">
                    </td>
                    <td style="width:60%;" colspan="2" align="center">
                        <strong>
                            <span class="color-blue" style="font-size:12px;color: #337ab7;" align="center">
                                <?= SUCURSAL ?>, S.A. DE C.V.
                            </span>
                        </strong>

                        <p class="justify" style="font-size:12px;" align="center">
                            <?=$this->config->item('encabezados_txt')?>
                        </p>
                    </td>
                    <td width="20%" align="" style="">
                        <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:150px;height:40px;"><br>
                    </td>
                </tr>
            </table>
        </div>
    </div>


    <br>
    <div class="row">
        <div class="col-md-12">
            <!-- Informacion de cabecera -->
            <table style="width: 100%;">
                <tr style="">
                    <td colspan="4" style="width: 50%;"></td>

                    <td style="padding-left:5px;width: 12.5%;border-top-left-radius: 4px; border-bottom-left-radius: 4px;border: 1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">OR.: </label>
                        <label style="font-weight: bold;"> <?php if(isset($idOrden)) echo $idOrden; else echo '0'; ?> </label>
                    </td>

                    <!--<td style="padding-left:5px;width: 12.5%;border:  1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">ORDEN INTELISIS:</label>
                        <label style="font-weight: bold;"> <?php if(isset($orden_intelisis)) echo $orden_intelisis; else echo ''; ?> </label>
                    </td>-->

                    <td style="padding-left:5px;width: 12.5%;border:  1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">COLOR: </label>
                        <label style="font-weight: bold;"> <?php if(isset($color)) echo $color; else echo ''; ?> </label>
                    </td>

                    <td style="padding-left:5px;width: 12.5%;border-top-right-radius: 4px; border-bottom-right-radius: 4px;border: 1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">TORRE: </label>
                        <label style="font-weight: bold;"> <?php if(isset($torre)) echo $torre; else echo ''; ?> </label>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:5px;border-top-left-radius: 4px;border: 1px solid #337ab7;border-left: 1px solid #337ab7;" colspan="5">
                        <label style="color: #337ab7;font-weight: bold;">NOMBRE CLIENTE: </label>
                        <label style="font-weight: bold;"> <?php if(isset($cliente)) echo $cliente; else echo ''; ?> </label>
                    </td>
                    <td style="padding-left:5px;width: 12.5%;border: 1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">MODELO: </label>
                        <label style="font-weight: bold;"> <?php if(isset($modelo)) echo $modelo; else echo ''; ?> </label>
                    </td>
                    <td style="padding-left:5px;width: 12.5%;border: 1px solid #337ab7;border-top-right-radius: 4px;">
                        <label style="color: #337ab7;font-weight: bold;">KMS.: </label>
                        <label style="font-weight: bold;"> <?php if(isset($km)) echo $km; else echo ''; ?> </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="width: 12.5%;border-bottom-left-radius: 4px;border: 1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">PLACAS: </label>
                        <label style="font-weight: bold;"> <?php if(isset($placas)) echo $placas; else echo ''; ?> </label>
                    </td>
                    <td colspan="2" style="padding-left:5px;width: 12.5%;border: 1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">FECHA:</label>
                        <label style="font-weight: bold;"> <?php if(isset($fecha)) echo $fecha; else echo ''; ?> </label>
                    </td>
                    <td style="padding-left:5px;width: 12.5%;border: 1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">MOTOR: </label>
                        <label style="font-weight: bold;"> <?php if(isset($motor)) echo $motor; else echo ''; ?> </label>
                    </td>
                    <td colspan="2" style="padding-left:5px;width: 12.5%;border-bottom-right-radius: 4px;border: 1px solid #337ab7;">
                        <label style="color: #337ab7;font-weight: bold;">NO. SERIE: </label>
                        <label style="font-weight: bold;"> <?php if(isset($serie)) echo $serie; else echo ''; ?> </label>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <!--Primera parte de la vista, diagrama e interiores -->
    <h5 style="color: #337ab7;">INSPECCIÓN VISUAL E INVENTARIO EN RECEPCIÓN</h5>

    <br>
    <!-- Tabla para contener todo el cuerpo del formulario -->
    <div class="row">
        <!--Lado izquierdo de la pagina -->
        <div class="col-md-4" style="float: right;">
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <!-- Tabla de indicadores -->
                    <table style="width:100%;border:0.5px solid #337ab7;float: right;">
                        <tr>
                            <td colspan="2" style="border-bottom:0.5px solid #337ab7;">
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:60px;">
                                            <strong style="font-size:13px;color: #337ab7;">Interiores</strong>
                                        </td>
                                        <td style="width:90px;color:#337ab7;">
                                            Opera Si(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">)
                                            /No(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">)
                                            /<br>No cuenta (NC)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Póliza Garantía/Manual de Prop
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("PólizaGarantia.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("PólizaGarantia.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("PólizaGarantia.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Seguro rines.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("SeguroRines.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("SeguroRines.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("SeguroRines.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="color:#337ab7;">
                                Indicadores de falla Activados:
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-bottom: 0.5px solid #337ab7;">
                                <table style="width:100%;">
                                    <tr>
                                        <td>
                                            <img src="<?php echo base_url();?>assets/imgs/05.png" style="width:20px;">
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url();?>assets/imgs/02.png" style="width:20px;">
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url();?>assets/imgs/07.png" style="width:20px;">
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url();?>assets/imgs/01.png" style="width:20px;">
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url();?>assets/imgs/06.png" style="width:20px;">
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url();?>assets/imgs/04.png" style="width:20px;">
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url();?>assets/imgs/03.png" style="width:20px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("IndicadorGasolina.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                                <?php elseif (in_array("IndicadorGasolina.No",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                                <?php elseif (in_array("IndicadorGasolina.NC",$interiores)): ?>
                                                    <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("IndicadorMantenimento.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                                <?php elseif (in_array("IndicadorMantenimento.No",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                                <?php elseif (in_array("IndicadorMantenimento.NC",$interiores)): ?>
                                                    <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("SistemaABS.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                                <?php elseif (in_array("SistemaABS.No",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                                <?php elseif (in_array("SistemaABS.NC",$interiores)): ?>
                                                    <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("IndicadorFrenos.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                                <?php elseif (in_array("IndicadorFrenos.No",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                                <?php elseif (in_array("IndicadorFrenos.NC",$interiores)): ?>
                                                    <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("IndicadorBolsaAire.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                                <?php elseif (in_array("IndicadorBolsaAire.No",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                                <?php elseif (in_array("IndicadorBolsaAire.NC",$interiores)): ?>
                                                    <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("IndicadorTPMS.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                                <?php elseif (in_array("IndicadorTPMS.No",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                                <?php elseif (in_array("IndicadorTPMS.NC",$interiores)): ?>
                                                    <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($interiores)): ?>
                                                <?php if (in_array("IndicadorBateria.Si",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                                <?php elseif (in_array("IndicadorBateria.No",$interiores)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                                <?php elseif (in_array("IndicadorBateria.NC",$interiores)): ?>
                                                    <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Rociadores y Limpiaparabrisas.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Rociadores.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Rociadores.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Rociadores.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Claxon.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Claxon.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Claxon.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Claxon.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-bottom:0.5px solid #337ab7; color:#337ab7;">
                                Luces:
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                &nbsp;&nbsp;&nbsp;&nbsp;Delanteras.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("LucesDelanteras.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("LucesDelanteras.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("LucesDelanteras.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                &nbsp;&nbsp;&nbsp;&nbsp;Traseras.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("LucesTraseras.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("LucesTraseras.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("LucesTraseras.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                &nbsp;&nbsp;&nbsp;&nbsp;Stop.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("LucesStop.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("LucesStop.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("LucesStop.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Radio / Caratulas.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Caratulas.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Caratulas.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Caratulas.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Pantallas.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Pantallas.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Pantallas.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Pantallas.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                A/C
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("AA.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("AA.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("AA.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Encendedor.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Encendedor.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Encendedor.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Encendedor.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Vidrios.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Vidrios.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Vidrios.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Vidrios.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Espejos.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Espejos.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Espejos.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Espejos.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Seguros eléctricos.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("SegurosEléctricos.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("SegurosEléctricos.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("SegurosEléctricos.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Disco compacto.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("CD.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("CD.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("CD.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:75%;border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Asientos y vestiduras.
                            </td>
                            <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Vestiduras.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Vestiduras.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Vestiduras.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:70%;color:#337ab7;">
                                Tapetes.
                            </td>
                            <td style="border-left:0.5px solid #337ab7;width:30%;" align="center">
                                <?php if (isset($interiores)): ?>
                                    <?php if (in_array("Tapetes.Si",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Tapetes.No",$interiores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Tapetes.NC",$interiores)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <br>
            <div class="row">
                <div class="col-md-12">
                    <!-- Tabla para indicadores de cajuela -->
                    <table style="width:100%;border:0.5px solid #337ab7;float: right;">
                        <tr class="" align="center">
                            <td colspan="2" align="center" style="border-bottom:0.5px solid #337ab7;">
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:60px;">
                                            <strong style="font-size:13px;color:#337ab7;">Cajuela</strong>
                                        </td>
                                        <td style="width:90px;color:#337ab7;">
                                            Opera Si(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">)
                                            /No(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">)
                                            /<br>No cuenta (NC)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Herramienta.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($cajuela)): ?>
                                    <?php if (in_array("Herramienta.Si",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Herramienta.No",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Herramienta.NC",$cajuela)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:0.5px solid #337ab7; color:#337ab7;">
                                Gato / Llave.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;">
                                <?php if (isset($cajuela)): ?>
                                    <?php if (in_array("eLlave.Si",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("eLlave.No",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("eLlave.NC",$cajuela)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Reflejantes.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;">
                                <?php if (isset($cajuela)): ?>
                                    <?php if (in_array("Reflejantes.Si",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Reflejantes.No",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Reflejantes.NC",$cajuela)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Cables.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;">
                                <?php if (isset($cajuela)): ?>
                                    <?php if (in_array("Cables.Si",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Cables.No",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Cables.NC",$cajuela)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:0.5px solid #337ab7;color:#337ab7;">
                                Extintor.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;">
                                <?php if (isset($cajuela)): ?>
                                    <?php if (in_array("Extintor.Si",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Extintor.No",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Extintor.NC",$cajuela)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#337ab7;">
                                Llanta Refacción.
                            </td>
                            <td align="center" style="border-left:0.5px solid #337ab7;">
                                <?php if (isset($cajuela)): ?>
                                    <?php if (in_array("LlantaRefaccion.Si",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("LlantaRefaccion.No",$cajuela)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("LlantaRefaccion.NC",$cajuela)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>NC</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
        </div>

        <!--Lado derecho de la pagina -->
        <div class="col-md-8" style="float: left; padding-left: 20px;">
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <!--Diagrama de daños -->
                    <table style="width:100%;float: left;" align="center">
                        <tr>
                            <td>
                                <img src="<?php echo base_url().'assets/imgs/carroceria.png'; ?>" alt="" style="width:260px; height:40px;">
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <?php if (isset($danosImg)): ?>
                                    <?php if ($danosImg != ""): ?>
                                        <img src="<?php echo base_url().$danosImg; ?>" alt="" style="width:310px;height: 210px;">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:310px;height: 210px;">
                                    <?php endif; ?>
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:310px;height: 210px;">
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <br>
            <!--Tabla de indicadores cajuela, exterior, cofre -->
            <div class="row">
                <!-- Tablas del lado izquierdo -->
                <div class="col-md-6">
                    <br>
                    <!-- Indicadores de exterior -->
                    <table style="width:100%;border:0.5px solid #337ab7;">
                        <tr class="" align="center">
                            <td colspan="2" align="center" style="border-bottom:0.5px solid #337ab7;">
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:60px;">
                                            <strong style="font-size:13px;color:#337ab7;">Exteriores</strong>
                                        </td>
                                        <td style="width:70px;color:#337ab7;">
                                            Si(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">)
                                            /No(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;width:70%;color:#337ab7;">
                                Tapones rueda.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;width:30%;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("TaponesRueda.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("TaponesRueda.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;width:70%;color:#337ab7;">
                                Gomas de limpiadores.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;width:30%;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("Gotas.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Gotas.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;color:#337ab7;">
                                Antena.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("Antena.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Antena.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;color:#337ab7;">
                                Tapón de gasolina.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("TaponGasolina.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("TaponGasolina.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;color:#337ab7;">
                                Costado Derecho.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("CostadoDerecho.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("CostadoDerecho.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;color:#337ab7;">
                                Parte Delantera.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("ParteDelantera.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("ParteDelantera.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;color:#337ab7;">
                                Interior, asientos, alfombra.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("Alfombra.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Alfombra.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;color:#337ab7;">
                                Costado Izquierdo.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("CostadoIzquierdo.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("CostadoIzquierdo.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid #337ab7;color:#337ab7;">
                                Parte Trasera.
                            </td>
                            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("ParteTrasera.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("ParteTrasera.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#337ab7;">
                                Cristales y Faros.
                            </td>
                            <td align="center" style="border-left:1px solid #337ab7;">
                                <?php if (isset($exteriores)): ?>
                                    <?php if (in_array("FarosCristal.Si",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("FarosCristal.No",$exteriores)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                
                    <br>
                    <!-- Deja articulos y nivel de gasolina -->
                    <table style="width:100%;">
                        <tr>
                            <td align="center" style="width:40%">
                                <img src="<?php echo base_url().'assets/imgs/gas.jpeg'; ?>" style="width:50px;" alt="">
                            </td>
                            <td style="margin-left:5px;border:0.5px solid #337ab7;border-right: 0.5px solid #ffffff;width:30%;">
                                Nivel de <br>Gasolina:
                            </td>
                            <td align="center" style="margin-left:5px;border:0.5px solid #337ab7;border-left: 0.5px solid #ffffff;width:30%;">
                                <label for=""> <strong><?php if(isset($nivelGasolina)) echo $nivelGasolina; else echo "0/0";?></strong> </label>
                            </td>
                        </tr>
                    </table>

                    <br>
                    <table style="width:100%;border:0.5px solid #337ab7;float: right;">
                        <tr>
                            <td align="left">
                                <label style="font-size:12px;color: #337ab7;">¿Deja artículos personales?</label>
                                <?php if (isset($articulos)): ?>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php if (strtoupper($articulos) == "1"): ?>
                                        <label style="font-size:12px;">&nbsp;SI&nbsp;</label>
                                    <?php else: ?>
                                        <label style="font-size:12px;">&nbsp;NO&nbsp;</label>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label style="font-size:12px;color: #337ab7;">¿Cuales? </label>
                                <p style="font-size:12px;" align="justify">
                                    <?php if(isset($cualesArticulos))  echo $cualesArticulos; else echo "Ninguno";?>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- Tablas del lado derecho -->
                <div class="col-md-6">
                    <!-- Indicadores de cofre -->
                    <table style="width:100%;border:0.5px solid #337ab7;margin-top: 20px;">
                        <tr class="" align="center">
                            <td colspan="2" align="center" style="border-bottom:0.5px solid #337ab7;">
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:15%;" align="left">
                                            <strong style="font-size:13px;color:#337ab7;">Cofre</strong>
                                        </td>
                                        <td style="width:85%;color:#337ab7;" align="left">
                                            Nivel Correcto(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">)
                                            /&nbsp;Nivel incorrecto(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">)
                                            /&nbsp;Fuga (F)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:60%;color:#337ab7;">
                                Aceite de Motor.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($cofre)): ?>
                                    <?php if (in_array("AceiteMotor.Si",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("AceiteMotor.No",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("AceiteMotor.F",$cofre)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Liquido de Frenos.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($cofre)): ?>
                                    <?php if (in_array("LiquidoFrenos.Si",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("LiquidoFrenos.No",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("LiquidoFrenos.F",$cofre)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Limpiaparabrisas.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($cofre)): ?>
                                    <?php if (in_array("Limpiaparabrisas.Si",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Limpiaparabrisas.No",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Limpiaparabrisas.F",$cofre)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Anticongelante.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($cofre)): ?>
                                    <?php if (in_array("Anticongelante.Si",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Anticongelante.No",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Anticongelante.F",$cofre)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                                <td style="width:70%;color:#337ab7;">
                                    Liquido de Dirección.
                                </td>
                                <td align="center" style="border-left:0.5px solid #337ab7;width:30%;">
                                    <?php if (isset($cofre)): ?>
                                        <?php if (in_array("LiquidoDirección.Si",$cofre)): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                        <?php elseif (in_array("LiquidoDirección.No",$cofre)): ?>
                                            <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                        <?php elseif (in_array("LiquidoDirección.F",$cofre)): ?>
                                            <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                    </table>

                    <br>
                    <!-- Indicadores de inferior -->
                    <table style="width:100%;border:0.5px solid #337ab7;">
                        <tr class="" align="center">
                            <td colspan="2" align="center" style="border-bottom:0.5px solid #337ab7;">
                                <table style="width:100%;">
                                    <tr>
                                        <td style="width:30%;">
                                            <strong style="font-size:13px;color:#337ab7;">Inferior</strong>
                                        </td>
                                        <td style="width:70%;color:#337ab7;">
                                            Bien(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">)
                                            /&nbsp;Mal(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">)
                                            /&nbsp; Fuga (F)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Sistema de Escape.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($inferior)): ?>
                                    <?php if (in_array("SisEscape.Si",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("SisEscape.No",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("SisEscape.F",$inferior)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Amortiguadores.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($inferior)): ?>
                                    <?php if (in_array("Amortiguador.Si",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Amortiguador.No",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Amortiguador.F",$inferior)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Tuberias.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($inferior)): ?>
                                    <?php if (in_array("Tuberias.Si",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Tuberias.No",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Tuberias.F",$inferior)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Transeje / Transmisión.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($inferior)): ?>
                                    <?php if (in_array("Transmisión.Si",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Transmisión.No",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Transmisión.F",$inferior)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Sistema de Dirección.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($inferior)): ?>
                                    <?php if (in_array("SisDirección.Si",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("SisDirección.No",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("SisDirección.F",$inferior)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;color:#337ab7;">
                                Chasis sucio.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($inferior)): ?>
                                    <?php if (in_array("Chasis.Si",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Chasis.No",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Chasis.F",$inferior)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr align="">
                            <td style="width:70%;color:#337ab7;">
                                Golpes Especifico.
                            </td>
                            <td align="center" style="border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($inferior)): ?>
                                    <?php if (in_array("Golpes.Si",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                    <?php elseif (in_array("Golpes.No",$inferior)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:14px;" alt="">
                                    <?php elseif (in_array("Golpes.F",$inferior)): ?>
                                        <?php echo "<label style='color:black;font-size:13px;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <label style="font-size:13px;color: #337ab7;">Observaciones:</label><br>
            <p style="font-size:12px;" align="justify">
                <?php if (isset($observaciones)): ?>
                    <?php echo $observaciones; ?>
                <?php endif; ?>
            </p>
        </div>

        <div class="col-md-8">
            <br>
            <!-- Indicadores de sistema de freno -->
            <!--<table style="width: 100%; border: 0.5px solid #337ab7;">
                <tr>
                    <td colspan="3" rowspan="2" align="left">
                        <label style="font-size:10px;font-weight:bold;color:#337ab7;">Sistema de frenos</label>
                    </td>
                    <td colspan="3" align="right" style="color:#337ab7;">
                        SOLO REVISAR 2 RUEDAS
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="color:#337ab7;">
                        ACEPTADA (<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">)
                        / REQUIERE REVISIÓN(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">)
                    </td>
                </tr>
                <tr style="border: 0.5px solid #337ab7;">
                    <td align="center" style="color:#337ab7;">
                        Ruedas
                    </td>
                    <td align="center" style="color:#337ab7;" colspan="2">
                        Balata/Zapata
                    </td>
                    <td align="center" style="color:#337ab7;" colspan="2">
                        Disco/Tambor
                    </td>
                    <td align="center" style="color:#337ab7;">
                        Neumático
                    </td>
                </tr>
                <tr style="border: 0.5px solid #337ab7;">
                    <td align="left" style="border: 0.5px solid #337ab7;color:#337ab7;">
                        Delantera Derecha
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedaDD.Balata.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedaDD.Balata.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedaDD.Zapata.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedaDD.Zapata.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedaDD.Disco.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedaDD.Disco.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedaDD.Tambor.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedaDD.Tambor.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedaDD.Neumatico.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedaDD.Neumatico.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr style="border: 0.5px solid #337ab7;">
                    <td align="left" style="width:25%;border: 0.5px solid #337ab7;color:#337ab7;">
                        Delantera Izquierda
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasDI.Balata.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasDI.Balata.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasDI.Zapata.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasDI.Zapata.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasDI.Disco.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasDI.Disco.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasDI.Tambor.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasDI.Tambor.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasDI.Neumatico.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasDI.Neumatico.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr style="border: 0.5px solid #337ab7;">
                    <td align="left" style="width:25%;border: 0.5px solid #337ab7;color:#337ab7;">
                        Trasera Derecha
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTD.Balata.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTD.Balata.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTD.Zapata.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTD.Zapata.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTD.Disco.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTD.Disco.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTD.Tambor.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTD.Tambor.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTD.Neumatico.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTD.Neumatico.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr style="border: 0.5px solid #337ab7;">
                    <td align="left" style="width:25%;border: 0.5px solid #337ab7;color:#337ab7;">
                        Trasera Izquierda
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTI.Balata.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTI.Balata.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTI.Zapata.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTI.Zapata.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTI.Disco.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTI.Disco.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTI.Tambor.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTI.Tambor.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("RuedasTI.Neumatico.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("RuedasTI.Neumatico.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr style="border: 0.5px solid #337ab7;">
                    <td align="left" style="width:40%;border: 0.5px solid #337ab7;color:#337ab7;">
                        Refacción
                    </td>
                    <td align="center" style="">
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                    </td>
                    <td align="center" style="">
                    </td>
                    <td align="center" style="border-right: 0.5px solid #337ab7;">
                    </td>
                    <td align="center" style="">
                        <?php if (isset($sisFrenos)): ?>
                            <?php if (in_array("Refaccion.Neumatico.Aceptada",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                            <?php elseif (in_array("Refaccion.Neumatico.Revisión",$sisFrenos)): ?>
                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>-->
            <img src="<?php echo base_url().'assets/imgs/sis_frenos_img.PNG'; ?>" style="width: 100%;height: 200px;">
            <h5 class="error" style="font-weight: bold;">**NOTA: Esta medición no realizará porque será realizada en la inspección multipunto</h5>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-3" align="center">
            <?php if (isset($firmaAsesor)): ?>
                <?php if ($firmaAsesor != ""): ?>
                    <img src="<?php echo base_url().$firmaAsesor; ?>" alt="" width="80" height="30">
                <?php else: ?>
                    <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" width="80" height="30">
                <?php endif; ?>
            <?php else: ?>
                <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" width="80" height="30">
            <?php endif; ?>
            <br>
            <label for="" style="color:black;"><?php if(isset($asesor)) echo $asesor; else echo ""; ?></label><br>
            <label style=""></label><br>
            <label style="">Nombre y firma del asesor.</label>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-3" align="center">
            <?php if (isset($firmaCliente)): ?>
                <?php if ($firmaCliente != ""): ?>
                    <img src="<?php echo base_url().$firmaCliente; ?>" alt="" width="120" height="45">
                <?php else: ?>
                    <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" width="120" height="45">
                <?php endif; ?>
            <?php else: ?>
                <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" width="120" height="45">
            <?php endif; ?>
            <br>
            <label for="" style="color:black;"><?php if(isset($cliente)) echo $cliente; else echo ""; ?></label> <br>
            <label style=""></label><br>
            <label style="">Nombre y firma del cliente.</label>
        </div>
        <div class="col-md-2"></div>
    </div>

</div>