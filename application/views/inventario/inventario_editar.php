<div class="" style="margin:20px;">
    <table style="width: 100%;">
        <tr>
            <td width="20%">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:150px;height:40px;">
            </td>
            <td style="width:60%;" colspan="2" align="center">
                <strong>
                    <span class="color-blue" style="font-size:12px;color: #337ab7;" align="center">
                        <?= SUCURSAL ?>, S.A. DE C.V.
                    </span>
                </strong>

                <p class="justify" style="font-size:8px;" align="center">
                    <?=$this->config->item('encabezados_txt')?>
                </p>
            </td>
            <td width="20%" align="" style="">
                <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:150px;height:40px;"><br>
            </td>
        </tr>
    </table>

    <br>
    <form name="" class="formulario" id="formulario" method="post" action="<?=base_url()."ordenServicio/Inventario/validateView"?>" autocomplete="on" enctype="multipart/form-data">

        <!-- Alerta para proceso del registro -->
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <!-- Datos de cabezera para el formulario -->
        <div class="row">
            <div class="col-md-12">
                <table style="width: 100%;">
                    <tr>
                        <td colspan="2"></td>
                        <td style="padding-left:5px;">
                            <label>OR.: </label>
                        </td>
                        <td>
                            <input type="text" class="input_field" id="noOrden" name="orderService" value="<?php if(set_value('orderService') != "") echo set_value('orderService'); else {if(isset($idOrden)) echo $idOrden;}?>" style="width: 100%;">
                            <?php echo form_error('orderService', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <!--<td style="padding-left:5px;">
                            <label>ORDEN INTELISIS:</label>
                            <br>
                        </td>
                        <td>
                            <input type="text" class="input_field" name="orden_CDK" value="<?php if(set_value('orden_CDK') != "") echo set_value('orden_CDK'); else {if(isset($orden_intelisis)) echo $orden_intelisis;}?>" style="width: 100%;">
                        </td>-->
                        <td style="padding-left:5px;">
                            <label>COLOR: </label>
                            <br>
                        </td>
                        <td>
                            <input type="text" class="input_field" id="color" name="color" value="<?php if(set_value('color') != "") echo set_value('color');  elseif(isset($color)) echo $color;?>" style="width: 100%;">
                            <?php echo form_error('color', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="padding-left:5px;">
                            <label>TORRE: </label>
                            <br>
                        </td>
                        <td>
                            <input type="text" class="input_field" id="torre" name="torre" value="<?php if(set_value('torre') != "") echo set_value('torre');  elseif(isset($torre)) echo $torre;?>" style="width: 100%;">
                            <?php echo form_error('torre', '<br><span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:5px;">
                            <label>NOMBRE CLIENTE: </label>
                            <br>
                        </td>
                        <td colspan="3">
                            <input type="text" class="input_field cliente" id="cliente" name="cliente" value="<?php if(set_value('cliente') != "") echo set_value('cliente');  elseif(isset($cliente)) echo $cliente;?>" style="width: 100%;">
                            <?php echo form_error('cliente', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="padding-left:5px;">
                            <label>MODELO: </label>
                            <br>
                        </td>
                        <td>
                            <input type="text" class="input_field" id="modelo" name="modelo" value="<?php if(set_value('modelo') != "") echo set_value('modelo');  elseif(isset($modelo)) echo $modelo;?>" style="width: 100%;">
                            <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="padding-left:5px;">
                            <label>KMS.: </label>
                            <br>
                        </td>
                        <td>
                            <input type="text" class="input_field" id="km" name="km" value="<?php if(set_value('km') != "") echo set_value('km');  elseif(isset($km)) echo $km;?>" style="width: 100%;">
                            <?php echo form_error('km', '<br><span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:5px;">
                            <label>PLACAS: </label>
                            <br>
                        </td>
                        <td>
                            <input type="text" class="input_field" id="placas" name="placas" value="<?php if(set_value('placas') != "") echo set_value('placas');  elseif(isset($placas)) echo $placas;?>">
                            <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="padding-left:5px;">
                            <label>FECHA:</label>
                            <br>
                        </td>
                        <td>
                            <input type="date" class="input_field" id="fecha" name="fecha" value="<?php if(set_value('fecha') != "") echo set_value('fecha'); else echo date("Y-m-d");?>" style="width: 100%;">
                            <?php echo form_error('fecha', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="padding-left:5px;">
                            <label>MOTOR: </label>
                            <br>
                        </td>
                        <td>
                            <input type="text" class="input_field" id="motor" name="motor" value="<?php if(set_value('motor') != "") echo set_value('motor');  elseif(isset($motor)) echo $motor;?>" style="width: 100%;">
                            <?php echo form_error('motor', '<br><span class="error">', '</span>'); ?>
                        </td>
                        <td style="padding-left:5px;">
                            <label>NO. SERIE: </label>
                            <br>
                        </td>
                        <td>
                            <input type="text" class="input_field" id="serie" name="serie" value="<?php if(set_value('serie') != "") echo set_value('serie');  elseif(isset($serie)) echo $serie;?>" style="width: 100%;">
                            <?php echo form_error('serie', '<br><span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center">
                        <strong>INSPECCIÓN VISUAL E INVENTARIO EN RECEPCIÓN</strong>
                    </div>

                    <!-- Diagrama del vehiculo -->
                    <div class="panel-body">
                        <div class="col-md-12" align="center" style="min-width: 600px;">
                            <table width="100%" class="table table-bordered">
                                <tr>
                                    <td>
                                        Si&nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="danos" <?php if(isset($danos)) if($danos == "1") echo "checked";?>>&nbsp;&nbsp;
                                        No&nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="danos" <?php if(isset($danos)) if($danos == "0") echo "checked";?>>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="radio" style="transform: scale(1.5);" id="golpes" name="marcasRadio" checked value="hit">&nbsp;Golpes
                                        &nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);" id="roto" name="marcasRadio" value="broken">&nbsp;Roto / Estrellado
                                        &nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);" id="rayones" name="marcasRadio" value="scratch">&nbsp;Rayones
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <?php if (isset($danosImg)): ?>
                                            <?php if ($danosImg != ""): ?>
                                                <img src="<?php echo base_url().$danosImg; ?>" alt="" style="width:420px;height: 400px">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:420px;height: 400px">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:420px;height: 400px">
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>

                            <canvas id="canvas_3" width="420" height="400" hidden>
                                Su navegador no soporta canvas.
                            </canvas>

                            <br>
                            <a href="<?php if(isset($danosImg)) echo base_url().$danosImg; else echo base_url().'assets/imgs/car.jpeg';?>" class="btn btn-warning" id="btn-download" download="Diagrama_Inventario_OR_<?php if(isset($idOrden)) echo $idOrden;?>">
                                <i class="fa fa-download" aria-hidden="true"></i>
                                Descargar diagrama
                            </a>

                            <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car.jpeg'; ?>">
                            <input type="hidden" name="danosMarcas" id="danosMarcas" value="<?php if(set_value('danosMarcas') != "") echo set_value('danosMarcas'); elseif(isset($danosImg)) echo base_url().$danosImg; ?>">
                            <input type="hidden" name="" id="validaClick_1" value="0">
                        </div>
                    </div>

                    <!-- Tabla interiores -->
                    <div class="panel-body">
                        <div class="col-md-12" style="min-width: 600px;">
                            <table class="" style="width:100%;">
                                <tbody>
                                    <tr style="border:1px solid black;">
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <h4 style="font-weight:bold;">Interiores </h4>
                                            <?php echo form_error('interiores', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            Si
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            No
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            (No cuenta)<br>
                                            NC
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Póliza Garantía / Manual de Prop.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="llaveroCheck" name="interiores[]" value="PólizaGarantia.Si" <?php echo set_checkbox('interiores[]', 'PólizaGarantia.Si'); ?> <?php  if (isset($interiores)) if (in_array("PólizaGarantia.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="llaveroCheck" name="interiores[]" value="PólizaGarantia.No" <?php echo set_checkbox('interiores[]', 'PólizaGarantia.No'); ?> <?php  if (isset($interiores)) if (in_array("PólizaGarantia.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="llaveroCheck" name="interiores[]" value="PólizaGarantia.NC" <?php echo set_checkbox('interiores[]', 'PólizaGarantia.NC'); ?> <?php  if (isset($interiores)) if (in_array("PólizaGarantia.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Seguro rines.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.Si" <?php echo set_checkbox('interiores[]', 'SeguroRines.Si'); ?> <?php  if (isset($interiores)) if (in_array("SeguroRines.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.No" <?php echo set_checkbox('interiores[]', 'SeguroRines.No'); ?> <?php  if (isset($interiores)) if (in_array("SeguroRines.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.NC" <?php echo set_checkbox('interiores[]', 'SeguroRines.NC'); ?> <?php  if (isset($interiores)) if (in_array("SeguroRines.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;border-bottom:1px solid white;">
                                        <td colspan="4">Indicadores de falla Activados:</td>
                                    </tr>
                                    <tr style="border:1px solid black;border-top:1px solid white;">
                                        <td colspan="4">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <img src="<?php echo base_url();?>assets/imgs/05.png" style="width:1.2cm;">
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <img src="<?php echo base_url();?>assets/imgs/02.png" style="width:1.2cm;">
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <img src="<?php echo base_url();?>assets/imgs/07.png" style="width:1.2cm;">
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <img src="<?php echo base_url();?>assets/imgs/01.png" style="width:1.2cm;">
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <img src="<?php echo base_url();?>assets/imgs/06.png" style="width:1.2cm;">
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <img src="<?php echo base_url();?>assets/imgs/04.png" style="width:1.2cm;">
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <img src="<?php echo base_url();?>assets/imgs/03.png" style="width:1.2cm;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5); margin-right: 6px;"class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.Si" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.Si'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorGasolina.Si",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px; margin-right: 6px; "class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.No" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.No'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorGasolina.No",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px;"class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.NC" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.NC'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorGasolina.NC",$interiores)) echo "checked"; ?>>
                                                        <!-- <input type="checkbox" style="transform: scale(1.5); margin: 6px;"name="interiores[]" value="IndicadorGasolina" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina'); ?>> -->
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5); margin-right: 6px;"class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.Si" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.Si'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorMantenimento.Si",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px; margin-right: 6px; "class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.No" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.No'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorMantenimento.No",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px;"class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.NC" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.NC'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorMantenimento.NC",$interiores)) echo "checked"; ?>>
                                                        <!-- <input type="checkbox" style="transform: scale(1.5); margin: 6px;"name="interiores[]" value="IndicadorMantenimento" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento'); ?>> -->
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5); margin-right: 6px;"class="SistemaABSCheck" name="interiores[]" value="SistemaABS.Si" <?php echo set_checkbox('interiores[]', 'SistemaABS.Si'); ?> <?php  if (isset($interiores)) if (in_array("SistemaABS.Si",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px; margin-right: 6px; "class="SistemaABSCheck" name="interiores[]" value="SistemaABS.No" <?php echo set_checkbox('interiores[]', 'SistemaABS.No'); ?> <?php  if (isset($interiores)) if (in_array("SistemaABS.No",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px;"class="SistemaABSCheck" name="interiores[]" value="SistemaABS.NC" <?php echo set_checkbox('interiores[]', 'SistemaABS.NC'); ?> <?php  if (isset($interiores)) if (in_array("SistemaABS.NC",$interiores)) echo "checked"; ?>>
                                                        <!-- <input type="checkbox" style="transform: scale(1.5); margin: 6px;"name="interiores[]" value="SistemaABS" <?php echo set_checkbox('interiores[]', 'SistemaABS'); ?>> -->
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5); margin-right: 6px;"class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.Si" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.Si'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorFrenos.Si",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px; margin-right: 6px; "class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.No" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.No'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorFrenos.No",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px;"class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.NC" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.NC'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorFrenos.NC",$interiores)) echo "checked"; ?>>
                                                        <!-- <input type="checkbox" style="transform: scale(1.5); margin: 6px;"name="interiores[]" value="IndicadorFrenos" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos'); ?>> -->
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5); margin-right: 6px;"class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.Si" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.Si'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorBolsaAire.Si",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5);margin-left: 6px; margin-right: 6px; "class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.No" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.No'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorBolsaAire.No",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px;"class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.NC" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.NC'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorBolsaAire.NC",$interiores)) echo "checked"; ?>>
                                                        <!-- <input type="checkbox" style="transform: scale(1.5); margin: 6px;"name="interiores[]" value="IndicadorBolsaAire" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire'); ?>> -->
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5); margin-right: 6px;" class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.Si" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.Si'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorTPMS.Si",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px; margin-right: 6px; " class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.No" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.No'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorTPMS.No",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5);margin-left: 6px;" class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.NC" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.NC'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorTPMS.NC",$interiores)) echo "checked"; ?>>
                                                        <!-- <input type="checkbox" style="transform: scale(1.5); margin: 6px;"name="interiores[]" value="IndicadorTPMS" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS'); ?>> -->
                                                    </td>
                                                    <td align="center" style="padding: 4px 0px 3px 0px;">
                                                        <input type="checkbox" style="transform: scale(1.5); margin-right: 6px;"class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.Si" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.Si'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorBateria.Si",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px; margin-right: 6px; "class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.No" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.No'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorBateria.No",$interiores)) echo "checked"; ?>>
                                                        <input type="checkbox" style="transform: scale(1.5); margin-left: 6px;"class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.NC" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.NC'); ?> <?php  if (isset($interiores)) if (in_array("IndicadorBateria.NC",$interiores)) echo "checked"; ?>>
                                                        <!-- <input type="checkbox" style="transform: scale(1.5); margin: 6px;"name="interiores[]" value="IndicadorBateria" <?php echo set_checkbox('interiores[]', 'IndicadorBateria'); ?>> -->
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Rociadores y Limpiaparabrisas.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="RociadoresCheck" name="interiores[]" value="Rociadores.Si" <?php echo set_checkbox('interiores[]', 'Rociadores.Si'); ?> <?php  if (isset($interiores)) if (in_array("Rociadores.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="RociadoresCheck" name="interiores[]" value="Rociadores.No" <?php echo set_checkbox('interiores[]', 'Rociadores.No'); ?> <?php  if (isset($interiores)) if (in_array("Rociadores.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="RociadoresCheck" name="interiores[]" value="Rociadores.NC" <?php echo set_checkbox('interiores[]', 'Rociadores.NC'); ?> <?php  if (isset($interiores)) if (in_array("Rociadores.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Claxon.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="ClaxonCheck" name="interiores[]" value="Claxon.Si" <?php echo set_checkbox('interiores[]', 'Claxon.Si'); ?> <?php  if (isset($interiores)) if (in_array("Claxon.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="ClaxonCheck" name="interiores[]" value="Claxon.No" <?php echo set_checkbox('interiores[]', 'Claxon.No'); ?> <?php  if (isset($interiores)) if (in_array("Claxon.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="ClaxonCheck" name="interiores[]" value="Claxon.NC" <?php echo set_checkbox('interiores[]', 'Claxon.NC'); ?> <?php  if (isset($interiores)) if (in_array("Claxon.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;border-bottom:1px solid white;">
                                        <td colspan="4">
                                            Luces
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;border-top: 1px solid white;">
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            Delanteras.
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.Si" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.Si'); ?> <?php  if (isset($interiores)) if (in_array("LucesDelanteras.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.No" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.No'); ?> <?php  if (isset($interiores)) if (in_array("LucesDelanteras.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.NC" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.NC'); ?> <?php  if (isset($interiores)) if (in_array("LucesDelanteras.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            Traseras.
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.Si" <?php echo set_checkbox('interiores[]', 'LucesTraseras.Si'); ?> <?php  if (isset($interiores)) if (in_array("LucesTraseras.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.No" <?php echo set_checkbox('interiores[]', 'LucesTraseras.No'); ?> <?php  if (isset($interiores)) if (in_array("LucesTraseras.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.NC" <?php echo set_checkbox('interiores[]', 'LucesTraseras.NC'); ?> <?php  if (isset($interiores)) if (in_array("LucesTraseras.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            Stop.
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesStopCheck" name="interiores[]" value="LucesStop.Si" <?php echo set_checkbox('interiores[]', 'LucesStop.Si'); ?> <?php  if (isset($interiores)) if (in_array("LucesStop.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesStopCheck" name="interiores[]" value="LucesStop.No" <?php echo set_checkbox('interiores[]', 'LucesStop.No'); ?> <?php  if (isset($interiores)) if (in_array("LucesStop.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="LucesStopCheck" name="interiores[]" value="LucesStop.NC" <?php echo set_checkbox('interiores[]', 'LucesStop.NC'); ?> <?php  if (isset($interiores)) if (in_array("LucesStop.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Radio / Carátulas.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="CaratulasCheck" name="interiores[]" value="Caratulas.Si" <?php echo set_checkbox('interiores[]', 'Caratulas.Si'); ?> <?php  if (isset($interiores)) if (in_array("Caratulas.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="CaratulasCheck" name="interiores[]" value="Caratulas.No" <?php echo set_checkbox('interiores[]', 'Caratulas.No'); ?> <?php  if (isset($interiores)) if (in_array("Caratulas.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="CaratulasCheck" name="interiores[]" value="Caratulas.NC" <?php echo set_checkbox('interiores[]', 'Caratulas.NC'); ?> <?php  if (isset($interiores)) if (in_array("Caratulas.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Pantallas, FIS.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="PantallasCheck" name="interiores[]" value="Pantallas.Si" <?php echo set_checkbox('interiores[]', 'Pantallas.Si'); ?> <?php  if (isset($interiores)) if (in_array("Pantallas.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="PantallasCheck" name="interiores[]" value="Pantallas.No" <?php echo set_checkbox('interiores[]', 'Pantallas.No'); ?> <?php  if (isset($interiores)) if (in_array("Pantallas.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="PantallasCheck" name="interiores[]" value="Pantallas.NC" <?php echo set_checkbox('interiores[]', 'Pantallas.NC'); ?> <?php  if (isset($interiores)) if (in_array("Pantallas.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>A/C</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="AACheck" name="interiores[]" value="AA.Si" <?php echo set_checkbox('interiores[]', 'AA.Si'); ?> <?php  if (isset($interiores)) if (in_array("AA.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="AACheck" name="interiores[]" value="AA.No" <?php echo set_checkbox('interiores[]', 'AA.No'); ?> <?php  if (isset($interiores)) if (in_array("AA.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="AACheck" name="interiores[]" value="AA.NC" <?php echo set_checkbox('interiores[]', 'AA.NC'); ?> <?php  if (isset($interiores)) if (in_array("AA.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Encendedor.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="EncendedorCheck" name="interiores[]" value="Encendedor.Si" <?php echo set_checkbox('interiores[]', 'Encendedor.Si'); ?> <?php  if (isset($interiores)) if (in_array("Encendedor.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="EncendedorCheck" name="interiores[]" value="Encendedor.No" <?php echo set_checkbox('interiores[]', 'Encendedor.No'); ?> <?php  if (isset($interiores)) if (in_array("Encendedor.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="EncendedorCheck" name="interiores[]" value="Encendedor.NC" <?php echo set_checkbox('interiores[]', 'Encendedor.NC'); ?> <?php  if (isset($interiores)) if (in_array("Encendedor.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Vidrios.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="VidriosCheck" name="interiores[]" value="Vidrios.Si" <?php echo set_checkbox('interiores[]', 'Vidrios.Si'); ?> <?php  if (isset($interiores)) if (in_array("Vidrios.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="VidriosCheck" name="interiores[]" value="Vidrios.No" <?php echo set_checkbox('interiores[]', 'Vidrios.No'); ?> <?php  if (isset($interiores)) if (in_array("Vidrios.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="VidriosCheck" name="interiores[]" value="Vidrios.NC" <?php echo set_checkbox('interiores[]', 'Vidrios.NC'); ?> <?php  if (isset($interiores)) if (in_array("Vidrios.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Espejos.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="EspejosCheck" name="interiores[]" value="Espejos.Si" <?php echo set_checkbox('interiores[]', 'Espejos.Si'); ?> <?php  if (isset($interiores)) if (in_array("Espejos.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="EspejosCheck" name="interiores[]" value="Espejos.No" <?php echo set_checkbox('interiores[]', 'Espejos.No'); ?> <?php  if (isset($interiores)) if (in_array("Espejos.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="EspejosCheck" name="interiores[]" value="Espejos.NC" <?php echo set_checkbox('interiores[]', 'Espejos.NC'); ?> <?php  if (isset($interiores)) if (in_array("Espejos.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Seguros Eléctricos.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.Si" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.Si'); ?> <?php  if (isset($interiores)) if (in_array("SegurosEléctricos.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.No" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.No'); ?> <?php  if (isset($interiores)) if (in_array("SegurosEléctricos.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.NC" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.NC'); ?> <?php  if (isset($interiores)) if (in_array("SegurosEléctricos.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>CD, Artículos Personales, Guantera.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="CDCheck" name="interiores[]" value="CD.Si" <?php echo set_checkbox('interiores[]', 'CD.Si'); ?> <?php  if (isset($interiores)) if (in_array("CD.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="CDCheck" name="interiores[]" value="CD.No" <?php echo set_checkbox('interiores[]', 'CD.No'); ?> <?php  if (isset($interiores)) if (in_array("CD.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="CDCheck" name="interiores[]" value="CD.NC" <?php echo set_checkbox('interiores[]', 'CD.NC'); ?> <?php  if (isset($interiores)) if (in_array("CD.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Asientos y vestiduras.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="VestidurasCheck" name="interiores[]" value="Vestiduras.Si" <?php echo set_checkbox('interiores[]', 'Vestiduras.Si'); ?> <?php  if (isset($interiores)) if (in_array("Vestiduras.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="VestidurasCheck" name="interiores[]" value="Vestiduras.No" <?php echo set_checkbox('interiores[]', 'Vestiduras.No'); ?> <?php  if (isset($interiores)) if (in_array("Vestiduras.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="VestidurasCheck" name="interiores[]" value="Vestiduras.NC" <?php echo set_checkbox('interiores[]', 'Vestiduras.NC'); ?> <?php  if (isset($interiores)) if (in_array("Vestiduras.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Tapetes.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="TapetesCheck" name="interiores[]" value="Tapetes.Si" <?php echo set_checkbox('interiores[]', 'Tapetes.Si'); ?> <?php  if (isset($interiores)) if (in_array("Tapetes.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="TapetesCheck" name="interiores[]" value="Tapetes.No" <?php echo set_checkbox('interiores[]', 'Tapetes.No'); ?> <?php  if (isset($interiores)) if (in_array("Tapetes.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="TapetesCheck" name="interiores[]" value="Tapetes.NC" <?php echo set_checkbox('interiores[]', 'Tapetes.NC'); ?> <?php  if (isset($interiores)) if (in_array("Tapetes.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <!--<tr style="border:1px solid black;">
                                        <td>OASIS Actualización SYNC.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="OasisCheck" name="interiores[]" value="Oasis.Si" <?php echo set_checkbox('interiores[]', 'Oasis.Si'); ?> <?php  if (isset($interiores)) if (in_array("Oasis.Si",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="OasisCheck" name="interiores[]" value="Oasis.No" <?php echo set_checkbox('interiores[]', 'Oasis.No'); ?> <?php  if (isset($interiores)) if (in_array("Oasis.No",$interiores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;"class="OasisCheck" name="interiores[]" value="Oasis.NC" <?php echo set_checkbox('interiores[]', 'Oasis.NC'); ?> <?php  if (isset($interiores)) if (in_array("Oasis.NC",$interiores)) echo "checked"; ?>>
                                        </td>
                                    </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- Cajuela, exterior, documentos, articulos y nivel de gasolina -->
                    <div class="panel-body">
                        <div class="col-md-6 table-responsive">
                            <table class="" style="width:100%;min-width: 300px;">
                                <thead>
                                    <tr class="headers_table" align="center" style="border:1px solid black;">
                                        <td>
                                            <h4 style="font-weight:bold;">Cofre </h4>
                                            <?php echo form_error('cofre', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            Nivel <br>Correcto
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            Nivel <br>Incorrecto
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            Fuga (F)
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style="border:1px solid black;border-top:1px solid white;">
                                        <td>Aceite de Motor.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_1" name="cofre[]" value="AceiteMotor.Si" <?php echo set_checkbox('cofre[]', 'AceiteMotor.Si'); ?> <?php  if (isset($cofre)) if (in_array("AceiteMotor.Si",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_1" name="cofre[]" value="AceiteMotor.No" <?php echo set_checkbox('cofre[]', 'AceiteMotor.No'); ?> <?php  if (isset($cofre)) if (in_array("AceiteMotor.No",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_1" name="cofre[]" value="AceiteMotor.F" <?php echo set_checkbox('cofre[]', 'AceiteMotor.F'); ?> <?php  if (isset($cofre)) if (in_array("AceiteMotor.F",$cofre)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Liquido de Frenos.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_2" name="cofre[]" value="LiquidoFrenos.Si" <?php echo set_checkbox('cofre[]', 'LiquidoFrenos.Si'); ?> <?php  if (isset($cofre)) if (in_array("LiquidoFrenos.Si",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_2" name="cofre[]" value="LiquidoFrenos.No" <?php echo set_checkbox('cofre[]', 'LiquidoFrenos.No'); ?> <?php  if (isset($cofre)) if (in_array("LiquidoFrenos.No",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_2" name="cofre[]" value="LiquidoFrenos.F" <?php echo set_checkbox('cofre[]', 'LiquidoFrenos.F'); ?> <?php  if (isset($cofre)) if (in_array("LiquidoFrenos.F",$cofre)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Limpiaparabrisas.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_3" name="cofre[]" value="Limpiaparabrisas.Si" <?php echo set_checkbox('cofre[]', 'Limpiaparabrisas.Si'); ?> <?php  if (isset($cofre)) if (in_array("Limpiaparabrisas.Si",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_3" name="cofre[]" value="Limpiaparabrisas.No" <?php echo set_checkbox('cofre[]', 'Limpiaparabrisas.No'); ?> <?php  if (isset($cofre)) if (in_array("Limpiaparabrisas.No",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_3" name="cofre[]" value="Limpiaparabrisas.F" <?php echo set_checkbox('cofre[]', 'Limpiaparabrisas.F'); ?> <?php  if (isset($cofre)) if (in_array("Limpiaparabrisas.F",$cofre)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Anticongelante.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofres_4" name="cofre[]" value="Anticongelante.Si" <?php echo set_checkbox('cofre[]', 'Anticongelante.Si'); ?> <?php  if (isset($cofre)) if (in_array("Anticongelante.Si",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofres_4" name="cofre[]" value="Anticongelante.No" <?php echo set_checkbox('cofre[]', 'Anticongelante.No'); ?> <?php  if (isset($cofre)) if (in_array("Anticongelante.No",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofres_4" name="cofre[]" value="Anticongelante.F" <?php echo set_checkbox('cofre[]', 'Anticongelante.F'); ?> <?php  if (isset($cofre)) if (in_array("Anticongelante.F",$cofre)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Liquido de Dirección.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_5" name="cofre[]" value="LiquidoDirección.Si" <?php echo set_checkbox('cofre[]', 'LiquidoDirección.Si'); ?> <?php  if (isset($cofre)) if (in_array("LiquidoDirección.Si",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_5" name="cofre[]" value="LiquidoDirección.No" <?php echo set_checkbox('cofre[]', 'LiquidoDirección.No'); ?> <?php  if (isset($cofre)) if (in_array("LiquidoDirección.No",$cofre)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="cofre_5" name="cofre[]" value="LiquidoDirección.F" <?php echo set_checkbox('cofre[]', 'LiquidoDirección.F'); ?> <?php  if (isset($cofre)) if (in_array("LiquidoDirección.F",$cofre)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6 table-responsive">
                            <table class="" style="width:100%;min-width: 300px;border:1px solid black;">
                                <thead>
                                    <tr class="headers_table" align="center" style="border:1px solid black;">
                                        <td>
                                            <h4 style="font-weight:bold;">Cajuela </h4>
                                            <?php echo form_error('cajuela', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            Si
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            No
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            No cuenta<br>
                                            (NC)
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style="border:1px solid black;">
                                        <td>Herramienta.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="HerramientaCheck" name="cajuela[]" value="Herramienta.Si" <?php echo set_checkbox('cajuela[]', 'Herramienta.Si'); ?> <?php  if (isset($cajuela)) if (in_array("Herramienta.Si",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="HerramientaCheck" name="cajuela[]" value="Herramienta.No" <?php echo set_checkbox('cajuela[]', 'Herramienta.No'); ?> <?php  if (isset($cajuela)) if (in_array("Herramienta.No",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="HerramientaCheck" name="cajuela[]" value="Herramienta.NC" <?php echo set_checkbox('cajuela[]', 'Herramienta.NC'); ?> <?php  if (isset($cajuela)) if (in_array("Herramienta.NC",$cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Gato / Llave.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="eLlaveCheck" name="cajuela[]" value="eLlave.Si" <?php echo set_checkbox('cajuela[]', 'eLlave.Si'); ?> <?php  if (isset($cajuela)) if (in_array("eLlave.Si",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="eLlaveCheck" name="cajuela[]" value="eLlave.No" <?php echo set_checkbox('cajuela[]', 'eLlave.No'); ?> <?php  if (isset($cajuela)) if (in_array("eLlave.No",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="eLlaveCheck" name="cajuela[]" value="eLlave.NC" <?php echo set_checkbox('cajuela[]', 'eLlave.NC'); ?> <?php  if (isset($cajuela)) if (in_array("eLlave.NC",$cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Reflejantes.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.Si" <?php echo set_checkbox('cajuela[]', 'Reflejantes.Si'); ?> <?php  if (isset($cajuela)) if (in_array("Reflejantes.Si",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.No" <?php echo set_checkbox('cajuela[]', 'Reflejantes.No'); ?> <?php  if (isset($cajuela)) if (in_array("Reflejantes.No",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.NC" <?php echo set_checkbox('cajuela[]', 'Reflejantes.NC'); ?> <?php  if (isset($cajuela)) if (in_array("Reflejantes.NC",$cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Cables.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CablesCheck" name="cajuela[]" value="Cables.Si" <?php echo set_checkbox('cajuela[]', 'Cables.Si'); ?> <?php  if (isset($cajuela)) if (in_array("Cables.Si",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CablesCheck" name="cajuela[]" value="Cables.No" <?php echo set_checkbox('cajuela[]', 'Cables.No'); ?> <?php  if (isset($cajuela)) if (in_array("Cables.No",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CablesCheck" name="cajuela[]" value="Cables.NC" <?php echo set_checkbox('cajuela[]', 'Cables.NC'); ?> <?php  if (isset($cajuela)) if (in_array("Cables.NC",$cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Extintor.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ExtintorCheck" name="cajuela[]" value="Extintor.Si" <?php echo set_checkbox('cajuela[]', 'Extintor.Si'); ?> <?php  if (isset($cajuela)) if (in_array("Extintor.Si",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ExtintorCheck" name="cajuela[]" value="Extintor.No" <?php echo set_checkbox('cajuela[]', 'Extintor.No'); ?> <?php  if (isset($cajuela)) if (in_array("Extintor.No",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ExtintorCheck" name="cajuela[]" value="Extintor.NC" <?php echo set_checkbox('cajuela[]', 'Extintor.NC'); ?> <?php  if (isset($cajuela)) if (in_array("Extintor.NC",$cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Llanta Refacción.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.Si" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.Si'); ?> <?php  if (isset($cajuela)) if (in_array("LlantaRefaccion.Si",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.No" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.No'); ?> <?php  if (isset($cajuela)) if (in_array("LlantaRefaccion.No",$cajuela)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.NC" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.NC'); ?> <?php  if (isset($cajuela)) if (in_array("LlantaRefaccion.NC",$cajuela)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6 table-responsive">
                            <table class="" style="width:100%;min-width: 300px;border:1px solid black;">
                                <thead>
                                    <tr class="headers_table" align="center" style="border:1px solid black;">
                                        <td>
                                            <h4 style="font-weight:bold;">Inferior </h4>
                                            <?php echo form_error('inferior', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            Bien
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            Mal
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            Fuga (F)
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style="border:1px solid black;">
                                        <td>Sistema de Escape.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_1" name="inferior[]" value="SisEscape.Si" <?php echo set_checkbox('inferior[]', 'SisEscape.Si'); ?> <?php  if (isset($inferior)) if (in_array("SisEscape.Si",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_1" name="inferior[]" value="SisEscape.No" <?php echo set_checkbox('inferior[]', 'SisEscape.No'); ?> <?php  if (isset($inferior)) if (in_array("SisEscape.No",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_1" name="inferior[]" value="SisEscape.F" <?php echo set_checkbox('inferior[]', 'SisEscape.F'); ?> <?php  if (isset($inferior)) if (in_array("SisEscape.F",$inferior)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Amortiguadores.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_2" name="inferior[]" value="Amortiguador.Si" <?php echo set_checkbox('inferior[]', 'Amortiguador.Si'); ?> <?php  if (isset($inferior)) if (in_array("Amortiguador.Si",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_2" name="inferior[]" value="Amortiguador.No" <?php echo set_checkbox('inferior[]', 'Amortiguador.No'); ?> <?php  if (isset($inferior)) if (in_array("Amortiguador.No",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_2" name="inferior[]" value="Amortiguador.F" <?php echo set_checkbox('inferior[]', 'Amortiguador.F'); ?> <?php  if (isset($inferior)) if (in_array("Amortiguador.F",$inferior)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Tuberias.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_3" name="inferior[]" value="Tuberias.Si" <?php echo set_checkbox('inferior[]', 'Tuberias.Si'); ?> <?php  if (isset($inferior)) if (in_array("Tuberias.Si",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_3" name="inferior[]" value="Tuberias.No" <?php echo set_checkbox('inferior[]', 'Tuberias.No'); ?> <?php  if (isset($inferior)) if (in_array("Tuberias.No",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_3" name="inferior[]" value="Tuberias.F" <?php echo set_checkbox('inferior[]', 'Tuberias.F'); ?> <?php  if (isset($inferior)) if (in_array("Tuberias.F",$inferior)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Transeje / Transmisión.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_4" name="inferior[]" value="Transmisión.Si" <?php echo set_checkbox('inferior[]', 'Transmisión.Si'); ?> <?php  if (isset($inferior)) if (in_array("Transmisión.Si",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_4" name="inferior[]" value="Transmisión.No" <?php echo set_checkbox('inferior[]', 'Transmisión.No'); ?> <?php  if (isset($inferior)) if (in_array("Transmisión.No",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_4" name="inferior[]" value="Transmisión.F" <?php echo set_checkbox('inferior[]', 'Transmisión.F'); ?> <?php  if (isset($inferior)) if (in_array("Transmisión.F",$inferior)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Sistema de Dirección.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_5" name="inferior[]" value="SisDirección.Si" <?php echo set_checkbox('inferior[]', 'SisDirección.Si'); ?> <?php  if (isset($inferior)) if (in_array("SisDirección.Si",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_5" name="inferior[]" value="SisDirección.No" <?php echo set_checkbox('inferior[]', 'SisDirección.No'); ?> <?php  if (isset($inferior)) if (in_array("SisDirección.No",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_5" name="inferior[]" value="SisDirección.F" <?php echo set_checkbox('inferior[]', 'SisDirección.F'); ?> <?php  if (isset($inferior)) if (in_array("SisDirección.F",$inferior)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Chasis sucio.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_6" name="inferior[]" value="Chasis.Si" <?php echo set_checkbox('inferior[]', 'Chasis.Si'); ?> <?php  if (isset($inferior)) if (in_array("Chasis.Si",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_6" name="inferior[]" value="Chasis.No" <?php echo set_checkbox('inferior[]', 'Chasis.No'); ?> <?php  if (isset($inferior)) if (in_array("Chasis.No",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_6" name="inferior[]" value="Chasis.F" <?php echo set_checkbox('inferior[]', 'Chasis.F'); ?> <?php  if (isset($inferior)) if (in_array("Chasis.F",$inferior)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Golpes Especifico.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_7" name="inferior[]" value="Golpes.Si" <?php echo set_checkbox('inferior[]', 'Golpes.Si'); ?> <?php  if (isset($inferior)) if (in_array("Golpes.Si",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_7" name="inferior[]" value="Golpes.No" <?php echo set_checkbox('inferior[]', 'Golpes.No'); ?> <?php  if (isset($inferior)) if (in_array("Golpes.No",$inferior)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="inferior_7" name="inferior[]" value="Golpes.F" <?php echo set_checkbox('inferior[]', 'Golpes.F'); ?> <?php  if (isset($inferior)) if (in_array("Golpes.F",$inferior)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="" style="width:100%;min-width: 300px;margin-top: 7px;">
                                <tbody>
                                    <tr>
                                        <td align="center" style="width:40%">
                                            <img src="<?php echo base_url().'assets/imgs/gas.jpeg'; ?>" style="width:40px;" alt="">
                                        </td>
                                        <td style="margin-left:5px;">
                                            Nivel de Gasolina:&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="number" name="nivelGasolina_1" class="input_field" style="width:1.5cm;" min="0" value="<?php if(isset($nivelGasolinaDiv)) echo $nivelGasolinaDiv[0]; else echo " 0 "; ?>">
                                            <img src="<?php echo base_url().'assets/imgs/diagonal.jpg'; ?>" style="width:20px; height:20px;" alt="">
                                            <input type="number" name="nivelGasolina_2" class="input_field" style="width:1.5cm;" min="0" value="<?php if(isset($nivelGasolinaDiv)) echo $nivelGasolinaDiv[1]; else echo " 0 "; ?>">
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>¿Deja artículos personales?</td>
                                        <td>
                                          (Si&nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);" value="1" name="articulos" <?php if(isset($articulos)) if($articulos == "1") echo "checked" ?>>
                                          &nbsp;&nbsp;
                                          No&nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);" value="0" name="articulos" <?php if(isset($articulos)) if($articulos == "0") echo "checked" ?>>)<br>
                                            <?php echo form_error('articulos', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>¿Cuales?</td>
                                        <td>
                                            <textarea class="form-control" name="cualesArticulos" rows="1" placeholder="Articulos..." style="width:95%"><?php if(set_value('cualesArticulos') != "") echo set_value('cualesArticulos'); else if(isset($cualesArticulos)) echo $cualesArticulos; ?></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-md-6 table-responsive">
                            <br><br>
                            <table class="" style="width:100%;min-width: 300px;border:1px solid black;">
                                <thead>
                                    <tr class="headers_table" align="center" style="border:1px solid black;">
                                        <td>
                                            <h4 style="font-weight:bold;">Exteriores </h4>
                                            <?php echo form_error('exteriores', '<span class="error">', '</span>'); ?>
                                        </td>
                                        <td align="center" style="width:15%;">
                                            Si
                                        </td>
                                        <td align="center" style="width:15%;">
                                            No
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr style="border:1px solid black;">
                                        <td>Tapones rueda.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="TaponesRuedaCheck" name="exteriores[]" value="TaponesRueda.Si" <?php echo set_checkbox('exteriores[]', 'TaponesRueda.Si'); ?> <?php  if (isset($exteriores)) if (in_array("TaponesRueda.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="TaponesRuedaCheck" name="exteriores[]" value="TaponesRueda.No" <?php echo set_checkbox('exteriores[]', 'TaponesRueda.No'); ?> <?php  if (isset($exteriores)) if (in_array("TaponesRueda.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Gomas de limpiadores.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="GotasCheck" name="exteriores[]" value="Gotas.Si" <?php echo set_checkbox('exteriores[]', 'Gotas.Si'); ?> <?php  if (isset($exteriores)) if (in_array("Gotas.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="GotasCheck" name="exteriores[]" value="Gotas.No" <?php echo set_checkbox('exteriores[]', 'Gotas.No'); ?> <?php  if (isset($exteriores)) if (in_array("Gotas.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Antena.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="AntenaCheck" name="exteriores[]" value="Antena.Si" <?php echo set_checkbox('exteriores[]', 'Antena.Si'); ?> <?php  if (isset($exteriores)) if (in_array("Antena.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="AntenaCheck" name="exteriores[]" value="Antena.No" <?php echo set_checkbox('exteriores[]', 'Antena.No'); ?> <?php  if (isset($exteriores)) if (in_array("Antena.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Tapón de gasolina.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="TaponGasolinaCheck" name="exteriores[]" value="TaponGasolina.Si" <?php echo set_checkbox('exteriores[]', 'TaponGasolina.Si'); ?> <?php  if (isset($exteriores)) if (in_array("TaponGasolina.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="TaponGasolinaCheck" name="exteriores[]" value="TaponGasolina.No" <?php echo set_checkbox('exteriores[]', 'TaponGasolina.No'); ?> <?php  if (isset($exteriores)) if (in_array("TaponGasolina.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Costado Derecho.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CostadoDerechoCheck" name="exteriores[]" value="CostadoDerecho.Si" <?php echo set_checkbox('exteriores[]', 'CostadoDerecho.Si'); ?> <?php  if (isset($exteriores)) if (in_array("CostadoDerecho.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CostadoDerechoCheck" name="exteriores[]" value="CostadoDerecho.No" <?php echo set_checkbox('exteriores[]', 'CostadoDerecho.No'); ?> <?php  if (isset($exteriores)) if (in_array("CostadoDerecho.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Parte Delantera.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ParteDelanteraCheck" name="exteriores[]" value="ParteDelantera.Si" <?php echo set_checkbox('exteriores[]', 'ParteDelantera.Si'); ?> <?php  if (isset($exteriores)) if (in_array("ParteDelantera.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ParteDelanteraCheck" name="exteriores[]" value="ParteDelantera.No" <?php echo set_checkbox('exteriores[]', 'ParteDelantera.No'); ?> <?php  if (isset($exteriores)) if (in_array("ParteDelantera.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Interior, asientos, alfombra.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="AlfombraCheck" name="exteriores[]" value="Alfombra.Si" <?php echo set_checkbox('exteriores[]', 'Alfombra.Si'); ?> <?php  if (isset($exteriores)) if (in_array("Alfombra.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="AlfombraCheck" name="exteriores[]" value="Alfombra.No" <?php echo set_checkbox('exteriores[]', 'Alfombra.No'); ?> <?php  if (isset($exteriores)) if (in_array("Alfombra.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Costado Izquierdo.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CostadoIzquierdoCheck" name="exteriores[]" value="CostadoIzquierdo.Si" <?php echo set_checkbox('exteriores[]', 'CostadoIzquierdo.Si'); ?> <?php  if (isset($exteriores)) if (in_array("CostadoIzquierdo.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CostadoIzquierdoCheck" name="exteriores[]" value="CostadoIzquierdo.No" <?php echo set_checkbox('exteriores[]', 'CostadoIzquierdo.No'); ?> <?php  if (isset($exteriores)) if (in_array("CostadoIzquierdo.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Parte Trasera.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ParteTraseraCheck" name="exteriores[]" value="ParteTrasera.Si" <?php echo set_checkbox('exteriores[]', 'ParteTrasera.Si'); ?> <?php  if (isset($exteriores)) if (in_array("ParteTrasera.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ParteTraseraCheck" name="exteriores[]" value="ParteTrasera.No" <?php echo set_checkbox('exteriores[]', 'ParteTrasera.No'); ?> <?php  if (isset($exteriores)) if (in_array("ParteTrasera.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                    <tr style="border:1px solid black;">
                                        <td>Cristales y Faros.</td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CristalCheck" name="exteriores[]" value="FarosCristal.Si" <?php echo set_checkbox('exteriores[]', 'FarosCristal.Si'); ?> <?php  if (isset($exteriores)) if (in_array("FarosCristal.Si",$exteriores)) echo "checked"; ?>>
                                        </td>
                                        <td align="center" style="padding: 4px 0px 3px 0px;">
                                            <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="CristalCheck" name="exteriores[]" value="FarosCristal.No" <?php echo set_checkbox('exteriores[]', 'FarosCristal.No'); ?> <?php  if (isset($exteriores)) if (in_array("FarosCristal.No",$exteriores)) echo "checked"; ?>>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <br>
                    <!-- Sistema de frenos -->
                    <div class="panel-body">
                        <div class="col-md-12 table-responsive" align="right">
                            <!--<table style="border: 1px solid black;width:100%;">
                                <tr>
                                    <td colspan="4" style="width:50%;vertical-align: middle;">
                                        <h4 style="font-weight: bold;">Sistema de frenos</h4>
                                    </td>
                                    <td colspan="8" align="right" style="width:50%;font-size:13px;vertical-align: middle;">
                                        SOLO REVISAR 2 RUEDAS
                                    </td>
                                </tr>
                                <tr style="font-size:13px; background-color: #ccc;">
                                    <td align="center" rowspan="2" style="width:100%;border: 1px solid black;font-size:13px;border-top: 1px solid white;">
                                        Ruedas
                                    </td>
                                    <td align="center" colspan="2" style="width:25%;border: 1px solid black;font-size:12px;border-top: 1px solid white;">
                                        Balata
                                    </td>
                                    <td align="center" colspan="2" style="width:25%;border: 1px solid black;font-size:12px;border-top: 1px solid white;">
                                        Zapata
                                    </td>
                                    <td align="center" colspan="2" style="width:25%;border: 1px solid black;font-size:12px;border-top: 1px solid white;">
                                        Disco
                                    </td>
                                    <td align="center" colspan="2" style="width:25%;border: 1px solid black;font-size:12px;border-top: 1px solid white;">
                                        Tambor
                                    </td>
                                    <td align="center" colspan="2" style="width:25%;border: 1px solid black;font-size:12px;border-top: 1px solid white;">
                                        Neumático
                                    </td>
                                </tr>
                                <tr style="font-size:11px;background-color: #eee;">
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Aceptada
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Requiere <br> revisión
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Aceptada
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Requiere <br> revisión
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Aceptada
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Requiere <br> revisión
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Aceptada
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Requiere <br> revisión
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Aceptada
                                    </td>
                                    <td style="width:25%;border: 1px solid black;font-size:11px;border-top: 1px solid white;" align="center">
                                        Requiere <br> revisión
                                    </td>
                                </tr>
                                <tr style="">
                                    <td align="left" style="width:25%;border: 1px solid black;font-size:13px;">
                                        Delantera Derecha
                                        <?php echo form_error('ruedaDD', '<br><span class="error">', '</span>'); ?>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT1A" value="RuedaDD.Balata.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Balata.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Balata.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT1A" value="RuedaDD.Balata.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Balata.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Balata.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT1B" value="RuedaDD.Zapata.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Zapata.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Zapata.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT1B" value="RuedaDD.Zapata.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Zapata.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Zapata.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT2A" value="RuedaDD.Disco.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Disco.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Disco.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT2A" value="RuedaDD.Disco.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Disco.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Disco.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT2B" value="RuedaDD.Tambor.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Tambor.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Tambor.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT2B" value="RuedaDD.Tambor.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Tambor.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Tambor.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT3" value="RuedaDD.Neumatico.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Neumatico.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Neumatico.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT3" value="RuedaDD.Neumatico.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Neumatico.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedaDD.Neumatico.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                </tr>
                                <tr style="">
                                    <td align="left" style="width:25%;border: 1px solid black;font-size:13px;">
                                        Delantera Izquierda
                                        <?php echo form_error('ruedaDI', '<br><span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT4A" value="RuedasDI.Balata.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Balata.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Balata.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT4A" value="RuedasDI.Balata.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Balata.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Balata.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT4B" value="RuedasDI.Zapata.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Zapata.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Zapata.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT4B" value="RuedasDI.Zapata.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Zapata.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Zapata.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT5A" value="RuedasDI.Disco.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Disco.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Disco.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT5A" value="RuedasDI.Disco.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Disco.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Disco.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT5B" value="RuedasDI.Tambor.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Tambor.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Tambor.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT5B" value="RuedasDI.Tambor.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Tambor.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Tambor.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT6" value="RuedasDI.Neumatico.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Neumatico.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Neumatico.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT6" value="RuedasDI.Neumatico.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Neumatico.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasDI.Neumatico.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                </tr>
                                <tr style="">
                                    <td align="left" style="width:25%;border: 1px solid black;font-size:13px;">
                                        Trasera Derecha
                                        <?php echo form_error('ruedaTD', '<br><span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT7A" value="RuedasTD.Balata.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Balata.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Balata.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT7A" value="RuedasTD.Balata.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Balata.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Balata.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT7B" value="RuedasTD.Zapata.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Zapata.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Zapata.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT7B" value="RuedasTD.Zapata.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Zapata.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Zapata.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT8A" value="RuedasTD.Disco.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Disco.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Disco.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT8A" value="RuedasTD.Disco.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Disco.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Disco.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT8B" value="RuedasTD.Tambor.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Tambor.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Tambor.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT8B" value="RuedasTD.Tambor.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Tambor.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Tambor.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT9" value="RuedasTD.Neumatico.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Neumatico.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Neumatico.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT9" value="RuedasTD.Neumatico.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTD.Neumatico.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTD.Neumatico.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                </tr>
                                <tr style="">
                                    <td align="left" style="width:25%;border: 1px solid black;font-size:13px;">
                                        Trasera Izquierda
                                        <?php echo form_error('ruedaTI', '<br><span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT10A" value="RuedasTI.Balata.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Balata.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Balata.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT10A" value="RuedasTI.Balata.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Balata.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Balata.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT10B" value="RuedasTI.Zapata.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Zapata.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Zapata.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT10B" value="RuedasTI.Zapata.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Zapata.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Zapata.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT11A" value="RuedasTI.Disco.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Disco.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Disco.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT11A" value="RuedasTI.Disco.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Disco.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Disco.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT11B" value="RuedasTI.Tambor.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Tambor.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Tambor.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT11B" value="RuedasTI.Tambor.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Tambor.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Tambor.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>

                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT12" value="RuedasTI.Neumatico.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Neumatico.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Neumatico.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:10%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT12" value="RuedasTI.Neumatico.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Neumatico.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("RuedasTI.Neumatico.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                </tr>
                                <tr style="">
                                    <td align="left" style="width:40%;border: 1px solid black;font-size:13px;">
                                        Refacción
                                        <?php echo form_error('refaccion', '<br><span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:13px;" colspan="4">

                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:13px;" colspan="4">
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT13" value="Refaccion.Neumatico.Aceptada" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'Refaccion.Neumatico.Aceptada'); ?> <?php  if (isset($sisFrenos)) if (in_array("Refaccion.Neumatico.Aceptada",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                    <td align="center" style="width:20%;border: 1px solid black;font-size:12px;">
                                        <input type="checkbox" style="transform: scale(1.5); margin: 6px;" class="ruedaT13" value="Refaccion.Neumatico.Revisión" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'Refaccion.Neumatico.Revisión'); ?> <?php  if (isset($sisFrenos)) if (in_array("Refaccion.Neumatico.Revisión",$sisFrenos)) echo "checked"; ?>>
                                    </td>
                                </tr>
                            </table>-->
                            <img src="<?php echo base_url().'assets/imgs/sis_frenos_img.PNG'; ?>" style="width: 50%;height: 200px;">
                            <h5 class="error" style="font-weight: bold;">**NOTA: Esta medición no realizará porque será realizada en la inspección multipunto</h5>
                        </div>
                    </div>

                    <br>
                    <!-- Observaciones y firmas -->
                    <div class="panel-body">
                        <div class="col-md-5 table-responsive">
                            <h6>Observaciones:</h6>
                            <textarea class="form-control" name="observaciones" rows="3" placeholder="Observaciones..." style="width:100%"><?php if(set_value('observaciones') != "") echo set_value('observaciones'); else if(isset($observaciones)) echo $observaciones; ?></textarea>
                        </div>

                        <div class="col-md-7 table-responsive">
                            <table class="" style="width:100%">
                                <tr>
                                    <td align="center">
                                        Nombre y firma del asesor.<br>
                                        <?php if (isset($firmaAsesor)): ?>
                                            <?php if ($firmaAsesor != ""): ?>
                                                <img class="marcoImg" src="<?php if(isset($firmaAsesor)) echo base_url().$firmaAsesor;?>" id="firmaFirmaAsesor" alt="" style="height:2cm;width:4cm;">
                                            <?php else: ?>
                                                <img class="marcoImg" src="<?= set_value('rutaFirmaAsesor');?>" id="firmaFirmaAsesor" alt="">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img class="marcoImg" src="<?= set_value('rutaFirmaAsesor');?>" id="firmaFirmaAsesor" alt="">
                                        <?php endif; ?>

                                        <br><br>
                                        <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?php if(set_value('rutaFirmaAsesor') != "") echo set_value('rutaFirmaAsesor'); else if(isset($firmaAsesor)) echo $firmaAsesor; ?>">
                                        <input type="text" class="input_field" id="asesor" name="asesor" style="width:100%;" value="<?php if(set_value('asesor') != "") echo set_value('asesor'); else if(isset($asesor)) echo $asesor; ?>">
                                    </td>
                                    <td align="center">
                                        Nombre y firma del cliente.<br>
                                        <?php if (isset($firmaCliente)): ?>
                                            <?php if ($firmaCliente != ""): ?>
                                                <img class="marcoImg" src="<?php if(isset($firmaCliente)) echo base_url().$firmaCliente;?>" id="firmaImgAcepta" alt="" style="height:2cm;width:4cm;">
                                            <?php else: ?>
                                                <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">
                                        <?php endif; ?>
                                        
                                        <br><br>
                                        <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?php if(set_value('rutaFirmaAcepta') != "") echo set_value('rutaFirmaAcepta'); else if(isset($firmaCliente)) echo $firmaCliente; ?>">
                                        <input type="text" class="input_field nombreCliente" id="otroClienteN" name="" style="width:100%;" value="<?php if(set_value('cliente') != "") echo set_value('cliente'); else if(isset($cliente)) echo $cliente; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <br><br>
                                        <?php if (isset($firmaAsesor)): ?>
                                            <?php if ($firmaAsesor == ""): ?>
                                                <a class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Asesor </a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <a class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Asesor </a>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <br><br>
                                        <?php if (isset($firmaCliente)): ?>
                                            <?php if ($firmaCliente == ""): ?>
                                                <a class="cuadroFirma btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Cliente </a>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <a class="cuadroFirma btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Cliente </a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <br>
        <div class="row" align="center">
            <div class="col-md-12" align="center">
                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Inventario_Lista/5";?>';" style="margin-right: 20px;">
                    Regresar
                </button>

                <input type="button" class="btn btn-success" name="enviar" id="enviarOrdenForm" value="Actualizar Inventario">

                <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="2">
                <input type="hidden" name="panelOrigen" id="panelOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                <input type="hidden" name="" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
                <input type="hidden" name="" id="formularioHoja" value="Inventario">
                <input type="hidden" name="idServicio" id="" value="<?php if(isset($idServicio)) echo $idServicio; ?>">
            </div>
        </div>
        <br>
    </form>
</div>



<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                    <!-- <p id="limpiar">limpiar canvas</p> -->
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

