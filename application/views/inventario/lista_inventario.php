<div style="margin:10px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Proceso completado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se completo el proceso.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-8">
                    <h3 align="right">Ordenes de servicio - Inventarios</h3>
                </div>
                <div class="col-sm-1" align="center" style="padding-top: 20px;">
                    <?php if (isset($pOrigen)): ?>
                        <?php if ($pOrigen == "ASE"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($pOrigen == "TEC"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($pOrigen == "JDT"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                Regresar
                            </button>
                        <?php else: ?>
                            <!--<button type="button" class="btn btn-dark" >
                                Sesión Expirada
                            </button>-->
                        <?php endif; ?>
                    <?php else: ?>
                        <!--<button type="button" class="btn btn-dark" >
                            Sesión Expirada
                        </button>-->
                    <?php endif; ?>
                </div>
                <div class="col-sm-3" align="right" style="padding-top: 20px;padding-right: 15px;">
                    <!--<input type="button" class="btn btn-success" style="color:white;" onclick="location.href='<?=base_url()."Inventario_Alta/0";?>';" value="NUEVO INVENTARIO">-->
                </div>
            </div>
            
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #eee;">
                                    <td align="center" style="width: 5%;"></td>
                                    <td align="center" style="width: 10%;"><strong>NO. ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" style="width: 10%;"><strong>FECHA RECEPCIÓN</strong></td>
                                    <td align="center" style="width: 15%;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" style="width: 15%;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 15%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 15%;"><strong>TÉCNICO</strong></td>
                                    <td align="center" style="width: 20%;"><strong>CLIENTE</strong></td>
                                    <td align="center" style=""><strong>DIAGNÓSTICO PREVIO</strong></td>
                                    <td align="center" style=""><strong>INVENTARIO</strong></td>
                                    <td align="center" style="" colspan="2"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="vertical-align: middle;">
                                                <!-- <?php echo count($idOrden) - $index; ?> -->
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $fechaDiag[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $vehiculo[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombreAsesor[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $nombreConsumidor2[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($diagnostico[$index] == "1"): ?>
                                                    SI
                                                <?php else: ?>
                                                    NO
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($diagnostico[$index] == "1"): ?>
                                                    <?php if ($inventario[$index] == "0"): ?>
                                                        <a href="<?=base_url().'Inventario_Alta/'.$id_cita_url[$index];?>" class="btn btn-success" style="font-size: 11px;">CREAR</a>
                                                    <?php else: ?>
                                                        <a href="<?=base_url().'Inventario_Editar/'.$id_cita_url[$index];?>" class="btn btn-primary" style="font-size: 11px;">EDITAR</a>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($diagnostico[$index] == "1"): ?>
                                                    <?php if ($inventario[$index] != "0"): ?>
                                                        <a href="<?=base_url().'Inventario_Revision/'.$id_cita_url[$index];?>" class="btn btn-secondary" target="_blank" style="font-size: 11px;">VER</a>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($diagnostico[$index] == "1"): ?>
                                                    <?php if ($inventario[$index] != "0"): ?>
                                                        <a href="<?=base_url().'Inventario_PDF/'.$id_cita_url[$index];?>" class="btn btn-dark" target="_blank" style="font-size: 11px;">PDF</a>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="12" style="width:100%;" align="center">
                                            <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                                <h4>Sin registros que mostrar</h4>
                                            <?php else: ?>
                                                <h4>Sesión Expirada</h4>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
