<!-- Contenido principal (Formulario) -->
<section class="content">
    <div class="row" style="background-color:white;margin-top:30px;">
        <div class="col-sm-9" align="center" style="">
            <div class="row">
              <div class="col-sm-3" align="center"></div>
              <div class="col-sm-9" align="center">
                    <img src="<?= base_url().$this->config->item('logo'); ?>" class="logo" style="max-height:2.5cm;max-width: 85%;">
                </div>
            </div>
        </div>
        <div class="col-sm-2" align="right">
            <br>
            <button type="button" title="Cerrar Sesión" class="btn btn-dark btn-lg" onclick="location.href='<?=base_url()."Principal_Asesor/4";?>';" >
                <!-- Cerrar Sesión -->
                <i class="fas fa-sign-out-alt" style="font-size:20px;"></i>
                <i class="fas fa-door-open" style="font-size:20px;"></i>
            </button>
        </div>
    </div>

    <!-- Verificamos si la sesion esta activa -->
    <?php if (isset($sesionActiva)): ?>
        <?php if ($sesionActiva == "1"): ?>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <h3>PANEL ASESOR</h3>
                    <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    <div class="alert alert-success" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                    </div>
                    <div class="alert alert-danger" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                    </div>
                    <div class="alert alert-warning" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Campos incompletos.</strong>
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-lg btn-block" style="background-color: #7090ac;color:white;" onclick="location.href='<?=base_url()."Documentacion_Asesores/4";?>';" >Documentación Ordenes</button>
                </div>
                <div class="col-sm-1"></div> 
            </div>

            <!--<br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-lg btn-block" style="background-color:  #d4ac0d;color:white;" onclick="location.href='<?=base_url()."Documentacion_PorFirmarAsesor/";?>';" >Documentación (Autorización por Ausencia)</button>
                </div>
                <div class="col-sm-1"></div> 
            </div>-->

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-lg btn-block btn-default" style="background-color:  #6c3483; color:white;" onclick="location.href='<?=base_url()."Servicio_valet_Lista/0";?>';" >Servicio Valet</button>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-lg btn-block btn-default" style="background-color:  #6c3483; color:white;" onclick="location.href='<?=base_url()."Servicio_valet_Asignar/0";?>';" >Servicio Valet (sin asignar)</button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-lg btn-block btn-default" style="background-color:  #bd2b8c; color:white;" onclick="location.href='<?=base_url()."CartaRenuncia_Lista/4";?>';">Carta de Renuncia a Beneficios</button>
                </div>
                <div class="col-sm-1"></div>
            </div>
            
            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-success btn-lg btn-block" onclick="location.href='<?=base_url()."Multipunto_Edita_Asesor/0";?>';">Multipunto Alta</button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."OrdenServicio_Verifica/0";?>';" > 
                        Diagnóstico (Orden de Servicio parte 2)
                    </button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href='<?=base_url()."CtrolMaterial_Verifica/0";?>';" >Control de Materiales alta</button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-lg btn-block" onclick="location.href='<?=base_url()."VCliente_Alta/0";?>';"  style="background-color: #008080; color:white;"> 
                        Alta Voz Cliente
                    </button>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-lg btn-block" onclick="location.href='<?=base_url()."VCliente_AddDiag/0";?>';"  style="background-color: #008080; color:white;"> 
                        Agregar diagnóstico Voz Cliente
                    </button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-lg btn-block" onclick="location.href='<?=base_url()."VCliente_Lista/4";?>';" style="background-color: #008080; color:white;"> 
                        Lista Voz Cliente
                    </button>
                </div>
                <div class="col-sm-4">
                    <div class="input-group">
                        <div class="input-group-addon" style="background-color: #3f3a3a;">
                            <a class="btn btn-dark" id="busqueda_vozCliente" style="color:white;height:100%;">
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                        <input type="text" name="q" class="form-control" id="busqueda_tabla_voz" placeholder="Buscar VC..." style="min-height:1.3cm;width: 100%;">
                    </div>
                    <label for="" class="error" id="error_tabla_voz"></label>
                    <!-- <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='<?=base_url()."VCliente_Alta/0";?>';" >Voz de cliente</button> -->
                </div>
                <div class="col-sm-1">
                    <a href="" id="linkPDFVoz" class="btn btn-info btn-lg btn-block" target="_blank" style="display:none;"><i class="far fa-eye" style="font-size:30px;margin-left: -7px;"></i></a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="location.href='<?=base_url()."Buscador/4";?>';" >
                        Diagnósticos sin cita
                    </button>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="location.href='<?=base_url()."VCliente_Temp/4";?>';" >
                        Voz Cliente sin cita
                    </button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-lg btn-block" style="background-color:lightgreen;" onclick="location.href='<?=base_url()."Listar_Asesor/4";?>';" >
                        Presupuestos Multipunto
                    </button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-lg btn-block" style="background-color: #2cb2f0;color:white;" onclick="location.href='<?=base_url()."Listado_Polizas/4";?>';" >Archivos polizas de garantías</button>
                </div>
                <div class="col-sm-1"></div> 
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="ocultarPsni();">
                        PSNI
                    </button>
                    <input type="hidden" id="estadoPsni" name="" value="0">
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row" id="psniContenedor" style="display: none;">
                <div class="col-sm-2"></div>
                <div class="col-sm-8" style="border : 1px solid black; padding-top: 10px;padding-bottom: 10px;">
                    <br>
                    <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."HG_Lista/4";?>';" >
                        Garantías
                    </button>

                    <br>
                    <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."ClienteRF_Lista/4";?>';" >
                        Clientes
                    </button>

                    <br>
                </div>
                <div class="col-sm-2"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <!-- <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='<?=base_url()."Campanias/4";?>';" >Campañas</button> -->
                    <!-- <a href="https://www.wslx.dealerconnection.com/login.cgi?WslIP=177.248.216.4&back=https://www.wslx.dealerconnection.com/Federation/idpLogin.cgi" class="btn btn-info btn-lg btn-block" target="_blank">ASC. Oasis Ford</a> -->
                    <a href="http://www.fordmazatlan.com.mx/" class="btn btn-danger btn-lg btn-block" target="_blank">ASC. Oasis Ford</a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <a href="<?=BASE_CITAS.'index.php/login' ?>" target="_blank"  class="btn btn-dark btn-lg btn-block">Acceso al Planificador</a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <a href="https://www.planificadorempresarial.mx/cs/citas_ford/horariossa/citas/citas_sa/Principal_Asesor/4" class="btn btn-lg btn-block" style="background-color:#ebee90;" target="_blank">
                        Panel Asesores (Santa Anita)
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <a href="https://sohex.mx/cs/linea_proceso/" class="btn btn-lg btn-block" style="color:blue;background-color: #ccc;border: 1px solid;" target="_blank">
                        MANUAL DE SEGUIMIENTO
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-sm-12" align="center" style="background-color:#eee;">
                    <br><br><br>
                    <h4>Sesión Expirada</h4>
                    <br><br><br>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <div class="row">
            <div class="col-sm-12" align="center" style="background-color:#eee;">
                <br><br><br>
                <h4>Sesión Expirada</h4>
                <br><br><br>
            </div>
        </div>
    <?php endif; ?>

    <br>

</section>
<!-- Contenido principal (Formulario) -->
