<div class="row">
		<div class="col-sm-12" align="center" style="">
				<h2 style="font-size: 30px; color: #716c6c;">BIENVENIDOS A:</h2>
				<img src="<?php echo base_url(); ?>assets/imgs/logos/LOGO-SOHEX.png" alt="" class="logo" style="width:360px;height:120px;">
				<h3 style="font-size: 20px;font-style: italic;font-weight: 400;color: #716c6c;">ESTAMOS CAMBIANDO Y TRABAJANDO POR TI!</h3>
		</div>
</div>
<br><br>

<section class="container">
		<div class="container" ng-controller="serviceCtrl">
				<div class="row">
						<div class="col-sm-2"></div>
						<div class="col-sm-8">
								<div class="panel panel-primary">
										<div class="panel-heading" align="center">
												<h3 class="panel-title">Verificación de identidad</h3>
										</div>
										<!-- Alerta para proceso del registro -->
						        <div class="alert alert-danger" align="center" style="display:none;">
						            <strong style="font-size:16px !important;">Fallo al autentificar usuario.</strong>
						        </div>
						        <div class="alert alert-warning" align="center" style="display:none;">
						            <strong style="font-size:16px !important;">Campos incorrectos.</strong>
						        </div>
										<div class="panel-body">
												<form name="" method="post" action="<?php echo base_url()."Verificar/edit";?>" id="Login">
														<div class="form-group" style="margin-top:10px;font-size:16px;" align="left">
																<input type="text" class="form-control" id="" name="inputUsuario" placeholder="Usuario" value="<?= set_value('inputUsuario');?>" style="width:100%;">
																<?php echo form_error('inputUsuario', '<span class="error">', '</span>'); ?>
														</div>
														<div class="form-group" style="margin-top:10px;font-size:16px;" align="left">
																<input type="password" class="form-control" id="" name="inputPassword" placeholder="Contraseña"  style="width:100%;">
																<?php echo form_error('inputPassword', '<span class="error">', '</span>'); ?>
														</div>
														<input type="hidden" name="" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
														<input type="hidden" name="loginOrigen" id="loginOrigen" value="JDT">
														<input type="submit" name="ingreso" class="btn btn-info btn-lg" value="Iniciar">
												</form>
										</div>
										<!-- <div class="panel-footer">
												<div class="row">
														<p style="font-size:14px;color:blue">
																&nbsp;&nbsp;&nbsp;&nbsp;Usuario temporal de prueba:<br>
																&nbsp;&nbsp;&nbsp;&nbsp;Usuario:</strong>Admin<br>
																&nbsp;&nbsp;&nbsp;&nbsp;Pass:</strong>Admin123<br>
														</p>
												</div>
									  </div> -->
								</div>
						</div>
						<div class="col-sm-2"></div>
				</div>
		</div>

		<br><br>
		<div class="row">
				<div class="col-md-12" align="center" style="">
						<img src="<?= base_url().$this->config->item('logo'); ?>" class="logo" style="max-height:2.5cm;">
				</div>
		</div>
</section>
