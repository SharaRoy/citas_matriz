<!-- Contenido principal (Formulario) -->
<section class="content">
    <div class="row" style="background-color:white;margin-top:30px;">
        <div class="col-sm-10" align="center" style="">
            <div class="row">
              <div class="col-sm-3" align="center"></div>
              <div class="col-sm-9" align="center">
                    <img src="<?= base_url().$this->config->item('logo'); ?>" class="logo" style="max-height:2.5cm;">
                </div>
            </div>
        </div>
        <div class="col-sm-2" align="right">
            <br>
            <button type="button" title="Cerrar Sesión" class="btn btn-dark btn-lg" onclick="location.href='<?=base_url()."Principal_Otro/4";?>';" >
                <!-- Cerrar Sesión -->
                <i class="fas fa-sign-out-alt" style="font-size:20px;"></i>
                <i class="fas fa-door-open" style="font-size:20px;"></i>
            </button>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10" align="center">
            <h3>PANEL VENTANILLA</h3>
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <div class="alert alert-success" align="center" style="display:none;">
                <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
            </div>
            <div class="alert alert-danger" align="center" style="display:none;">
                <strong style="font-size:20px !important;">Fallo el proceso.</strong>
            </div>
            <div class="alert alert-warning" align="center" style="display:none;">
                <strong style="font-size:20px !important;">Campos incompletos.</strong>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <button type="button" class="btn btn-lg btn-block" style="font-size: 17px;background-color:lightgreen;" onclick="location.href='<?=base_url()."Listar_Ventanilla/4";?>';" >
                Presupuestos Multipunto
            </button>
        </div>
        <div class="col-sm-2"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <button type="button" class="btn btn-lg btn-block" style="background-color: #7090ac;color:white;" onclick="location.href='<?=base_url()."Documentacion_Ventanilla/4";?>';" >Documentación Ordenes</button>
        </div>
        <div class="col-sm-2"></div> 
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="ocultarPsni();">
                PSNI
            </button>
            <input type="hidden" id="estadoPsni" name="" value="0">
        </div>
        <div class="col-sm-2"></div>
    </div>

    <br>
    <div class="row" id="psniContenedor" style="display: none;">
        <div class="col-sm-2"></div>
        <div class="col-sm-8" style="border : 1px solid black; padding-top: 10px;padding-bottom: 10px;">
            <br>
            <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."HG_Lista/4";?>';" >
                Garantías
            </button>

            <br>
            <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."ClienteRF_Lista/4";?>';" >
                Clientes
            </button>

            <br>
        </div>
        <div class="col-sm-2"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <a href="https://sohex.mx/cs/linea_proceso/" class="btn btn-lg btn-block" style="color:blue;background-color: #ccc;border: 1px solid;" target="_blank">
                MANUAL DE SEGUIMIENTO
            </a>
        </div>
        <div class="col-sm-2"></div>
    </div>
</section>
<!-- Contenido principal (Formulario) -->
