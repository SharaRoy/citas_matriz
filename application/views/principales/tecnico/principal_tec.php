<!-- Contenido principal (Formulario) -->
<section class="content">
    <div class="row" style="background-color:white;margin-top:30px;">
        <div class="col-sm-9" align="center" style="">
            <div class="row">
              <div class="col-sm-3" align="center"></div>
              <div class="col-sm-9" align="center">
                  <img src="<?= base_url().$this->config->item('logo'); ?>" class="logo" style="max-height:2.5cm;">
                </div>
            </div>
        </div>
        <div class="col-sm-2" align="right" style="">
            <br>
            <button type="button" title="Cerrar Sesión" class="btn btn-dark btn-lg" onclick="location.href='<?=base_url()."Principal_Tec/4";?>';" >
                <!-- Cerrar Sesión -->
                <i class="fas fa-sign-out-alt" style="font-size:20px;"></i>
                <i class="fas fa-door-open" style="font-size:20px;"></i>
            </button>
        </div>
    </div>

    <?php if (isset($sesionActiva)): ?>
        <?php if ($sesionActiva == "1"): ?>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10" align="center">
                    <h3>PANEL TÉCNICO</h3>
                    <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    <div class="alert alert-success" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                    </div>
                    <div class="alert alert-danger" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                    </div>
                    <div class="alert alert-warning" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Campos incompletos.</strong>
                    </div>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-success btn-lg btn-block" onclick="location.href='<?=base_url()."Multipunto_Edita_Asesor/0";?>';">Multipunto Alta</button>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-success btn-lg btn-block" onclick="location.href='<?=base_url()."Multipunto_Lista/4";?>';">Lista Multipunto</button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href='<?=base_url()."VCliente_Lista/4";?>';" >
                        Lista VC
                    </button> 
                </div>
                <div class="col-sm-1"></div>
            </div>

            <!--<br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="location.href='<?=base_url()."Listar_Cotizacion_Actualiza/4";?>';" >Cotizaciones Multipunto Creadas</button>
                </div>
                <div class="col-sm-1"></div>
            </div>-->

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                     <button type="button" class="btn btn-lg btn-block" style="background-color:lightgreen;" onclick="location.href='<?=base_url()."Listar_Tecnico/4";?>';" >
                            Presupuesto Multipunto Creados
                    </button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-lg btn-block" style="background-color: #7090ac;color:white;" onclick="location.href='<?=base_url()."Documentacion_Tecnicos/4";?>';" >Documentación Ordenes</button>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <button type="button" class="btn btn-lg btn-block" style="background-color: #5cb8a5;border-color: #4c8bae;color:white;" onclick="location.href='<?=base_url()."Lista_Status_Tecnico/4";?>';" >Estatus Ordenes</button>
                </div>
                <div class="col-sm-1"></div>
            </div>
    
            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <!-- <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='<?=base_url()."Campanias/4";?>';" >Campañas</button> -->
                    <!-- <a href="https://www.wslx.dealerconnection.com/login.cgi?WslIP=177.248.216.4&back=https://www.wslx.dealerconnection.com/Federation/idpLogin.cgi" class="btn btn-info btn-lg btn-block" target="_blank">ASC. Oasis Ford</a> -->
                    <a href="http://www.fordmazatlan.com.mx/" class="btn btn-info btn-lg btn-block" target="_blank">ASC. Oasis Ford</a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-1"></div>
                <div class="col-sm-10">
                    <a href="https://sohex.mx/cs/linea_proceso/" class="btn btn-lg btn-block" style="color:blue;background-color: #ccc;border: 1px solid;" target="_blank">
                        MANUAL DE SEGUIMIENTO
                    </a>
                </div>
                <div class="col-sm-1"></div>
            </div>

            <br><br>
        <?php else: ?>
            <div class="row">
                <div class="col-sm-12" align="center" style="background-color:#eee;">
                    <br><br><br>
                    <h4>Sesión Expirada</h4>
                    <br><br><br>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <div class="row">
            <div class="col-sm-12" align="center" style="background-color:#eee;">
                <br><br><br>
                <h4>Sesión Expirada</h4>
                <br><br><br>
            </div>
        </div>
    <?php endif; ?>

</section>
<!-- Contenido principal (Formulario) -->
