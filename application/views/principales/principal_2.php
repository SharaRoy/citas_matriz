<br><br>
<!-- Contenido principal (Formulario) -->
<section class="content">
    <br>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10" align="center">
            <h3>MENÚ</h3>
        </div>
        <div class="col-sm-1"></div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-success btn-lg btn-block" onclick="location.href='<?=base_url()."multipunto/Multipunto/index/0";?>';">Multipunto Alta</button>
        </div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-success btn-lg btn-block" onclick="location.href='<?=base_url()."Verificar/index/HM/1";?>';">Multipunto Editar</button>
        </div>
        <div class="col-sm-3" align="center">
            <button type="button" class="btn btn-success btn-lg btn-block" onclick="location.href='<?=base_url()."multipunto/Multipunto/setDataPDF/1";?>';" >Multipunto PDF</button>
        </div>
        <div class="col-sm-1"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."ordenServicio/ordenServicio/index/0";?>';" >Orden de servicio alta</button>
        </div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."Verificar/index/OS/";?>';" >Orden de servicio editar</button>
        </div>
        <div class="col-sm-3" align="center">
            <button type="button" class="btn btn-warning btn-lg btn-block" onclick="location.href='<?=base_url()."ordenServicio/ordenServicio/setDataPDF/1";?>';">Orden de servicio PDF</button>
        </div>
        <div class="col-sm-1"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href='<?=base_url()."ordenServicio/OrdenServicioP2/index/0";?>';" >Control de Materiales alta</button>
        </div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href='<?=base_url()."Verificar/index/1/OS_CM";?>';" >Control de Materiales editar</button>
        </div>
        <div class="col-sm-3" align="center">
            <button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href='<?=base_url()."ordenServicio/OrdenServicioP2/setDataPDFControl/1";?>';" >Control de Materiales PDF</button>
        </div>
        <div class="col-sm-1"></div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='<?=base_url()."vozCliente/VozCliente/index/0";?>';" >Voz de cliente</button>
        </div>
        <div class="col-sm-3">
        </div>
        <div class="col-sm-3" align="center">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='<?=base_url()."vozCliente/VozCliente/setDataPDF/1";?>';" >Voz de cliente PDF</button>
        </div>
        <div class="col-sm-1"></div>
    </div>

    <!-- <br>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='<?=base_url()."terminos/Terminos/index/1";?>';">Terminos</button>
        </div>
        <div class="col-sm-3">
            <button type="button" class="btn btn-info btn-lg btn-block" onclick="location.href='<?=base_url()."terminos/Terminos/setDataPDF/1";?>';">Terminos PDF (id=1)</button>
        </div>
        <div class="col-sm-3"></div>
    </div> -->

    <br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3">
        </div>
        <div class="col-sm-3">
          <button type="button" class="btn btn-dark btn-lg btn-block" onclick="location.href='<?=base_url()."ordenServicio/Buscador/index/";?>';" >Buscar inventario</button>
        </div>
        <div class="col-sm-3" align="center">
        </div>
        <div class="col-sm-1"></div>
    </div>

    <br>
    <!-- <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-3">
        </div>
        <div class="col-sm-3">
          <button type="button" class="btn btn-default btn-lg btn-block" onclick="location.href='<?=base_url()."Principal/consulta/0";?>';" >Pruebas base</button>
        </div>
        <div class="col-sm-3" align="center">
        </div>
        <div class="col-sm-1"></div>
    </div> -->
</section>
<!-- Contenido principal (Formulario) -->
