<div class="panel-flotante" style="margin:20px;">
    <form name="" id="formulario_presupuesto" method="post" autocomplete="on" enctype="multipart/form-data"> <!-- <?php //base_url()."presupuestos/Presupuestos/validateFormR2"?> -->
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="form-control" name="idOrdenTemp" id="idOrdenTempCotiza" onblur="datos_cabecera()" value="<?= ((isset($id_cita)) ? $id_cita : '') ?>" style="width:100%;">
            </div>
            <div class="col-md-3">
                <label for="">Or. Intelisis :</label>&nbsp;
                <input type="text" class="form-control" name="folio" id="folio" value="<?= ((isset($folio)) ? $folio : '') ?>" style="width:100%;">
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN) :</label>&nbsp;
                <input class="form-control" type="text" name="serie" id="serie" value="<?= ((isset($serie)) ? $serie : '') ?>" style="width:100%;">
            </div>
            <div class="col-md-3">
                <label for="">Modelo :</label>&nbsp;
                <input class="form-control" type="text" name="modelo" id="txtmodelo" value="<?= ((isset($modelo)) ? $modelo : '') ?>" style="width:100%;">
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas :</label>&nbsp;
                <input type="text" class="form-control" name="placas" id="placas" value="<?= ((isset($placas)) ? $placas : '') ?>" style="width:100%;">
            </div>
            <div class="col-md-3">
                <label for="">Unidad :</label>&nbsp;
                <input class="form-control" type="text" name="uen" id="uen" value="<?= ((isset($vehiculo)) ? $vehiculo : '') ?>" style="width:100%;">
            </div>
            <div class="col-md-3">
                <label for="">Técnico :</label>&nbsp;
                <input class="form-control" type="text" name="tecnico" id="tecnico" value="<?= ((isset($tecnico)) ? $tecnico : '') ?>" style="width:100%;">
            </div>
            <div class="col-md-3">
                <label for="">Asesor :</label>&nbsp;
                <input class="form-control" type="text" name="asesors" id="asesors" value="<?= ((isset($asesor)) ? $asesor : '') ?>" style="width:100%;">
            </div>
        </div>
        
        <br>
        <div class="row table-responsive" align="right">
            <div class="col-md-4" align="right"></div>
            <div class="col-md-8" align="right">
                <div class="row">
                    <div class="col-md-9" align="right">
                        <label for="" class="error"> <strong>*</strong> Guardar renglon [<i class="fas fa-plus"></i>] </label>
                        <br>
                        <label for="" class="error"> <strong>*</strong> Eliminar renglon [<i class="fas fa-minus"></i>] </label>
                    </div>
                    <div class="col-md-1" align="right">
                        <a id="agregarRefaccionTec" class="btn btn-success" style="color:white;">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="col-md-1" align="right">
                        <a id="eliminarRefaccionTec" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                            <i class="fas fa-minus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                CANTIDAD
                            </td>
                            <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 10px;" align="center" colspan="3">
                                EXISTE
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PRECIO REFACCIÓN
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;<?= (($tipo_orden != '3') ? 'display:none;' : '') ?>" align="center" rowspan="2">
                                PRECIO ASEGURADORA
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                COSTO MO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                TOTAL DE REPARACÓN
                            </td>
                            <td style="width:8%;display: none;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PRIORIDAD <br> REFACCION
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PZA. GARANTÍA
                                <input type="hidden" name="indiceTablaMateria" id="indiceTablaMateria" value="1">
                                <input type='hidden' id='pzsGarantias' name='pzsGarantias' value="">
                            </td>
                        </tr>
                        <tr style="background-color: #ddd;">
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                SI
                            </td>
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                NO
                            </td>
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                PLANTA
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <tr id='fila_1'>
                            <td align='center' style="border:1px solid #337ab7;vertical-align: middle;">
                                <input type='number' step='any' min='0' class='input_field' name="cantidad_1" id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:90%;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <textarea rows='2' class='input_field_lg' id='descripcion_1' name="descripcion_1" style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <textarea rows='2' class='input_field_lg' id='pieza_1' name="pieza_1" style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaSi_1' class='input_field' name='existe_1' value="SI" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaNo_1' class='input_field' name='existe_1' value="NO" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaPlanta_1' class='input_field' name='existe_1' value="PLANTA" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                $<input type='number' step='any' min='0' class='input_field' id='costoCM_1' name='costoRef_1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:70%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;<?= (($tipo_orden != '3') ? 'display: none;' : '') ?>' align='center'>
                                $<input type='number' step='any' min='0' class='input_field' id='costoaseg_1' name='costoaseg_1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:70%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='number' step='any' min='0' class='input_field' id='horas_1' name='hotasMo_1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:70%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                $<input type='number' step='any' min='0' id='totalReng_1' class='input_field' name='costoMo_1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='612' style='width:70%;text-align:left;'>
                                <input type='hidden' id='totalOperacion_1' value='0'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <label id="total_renglon_label_1" style="color:darkblue;">$0.00</label>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;display: none;' align='center'>
                                <input type='number' step='any' min='0' class='input_field' id='prioridad_1' name='prioridad_1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='2' style='width:70%;text-align:center;'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='checkbox' class='input_field' value="1" name='pzaGarantia[]' style='transform: scale(1.5);'>
                                <input type='hidden' id='valoresCM_1' name='registros[]'>
                                <br>
                                <?php echo form_error('registros', '<span class="error">', '</span>'); ?>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="<?= (($tipo_orden != '3') ? '9' : '10') ?>" align="right">
                                SUB-TOTAL
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                                <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subTotalMaterial)) echo $subTotalMaterial; else echo "0"; ?>">
                            </td>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="<?= (($tipo_orden != '3') ? '9' : '10') ?>" align="right">
                                I.V.A.
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                                <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($ivaMaterial)) echo $ivaMaterial; else echo "0"; ?>">
                            </td>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="<?= (($tipo_orden != '3') ? '9' : '10') ?>" align="right">
                                PRESUPUESTO TOTAL
                            </td>
                            <td colspan="1" align="center">
                                $<label id="presupuestoMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format(($subTotalMaterial+$ivaMaterial),2); else echo "0"; ?></label>
                            </td>
                            <td colspan="3"></td>
                        </tr>
                        <tr style="border-top:1px solid #337ab7;">
                            <td colspan="<?= (($tipo_orden != '3') ? '9' : '10') ?>" align="right">
                                ANTICIPO
                            </td>
                            <td align="center">
                                $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                                <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                            </td>
                            <td colspan="3">
                                <label for="" id="anticipoNota" style="color:blue;"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="<?= (($tipo_orden != '3') ? '9' : '10') ?>" align="right">
                                TOTAL A PAGAR
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                                <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($totalMaterial)) echo $totalMaterial; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="<?= (($tipo_orden != '3') ? '9' : '10') ?>" align="right">
                                FIRMA DEL ASESOR QUE APRUEBA
                            </td>
                            <td align="center" colspan="3">
                              <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php echo set_value("rutaFirmaAsesorCostos"); ?>'>
                              <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-sm-12" hidden>
                <div class="alert alert-warning" align="left">
                    <strong style="font-size:12px !important;">
                        Prioridad de refacciones:
                        0 = Sin prioridad, 1 = Prioridad Baja, 2 = Prioridad Media, 3 = Prioridad Alta
                    </strong>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-5">
                <h4>Notas del asesor:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($nota_asesor)){ if($nota_asesor != "") echo $nota_asesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-7" align="right">
                <h5>Archivo(s) de la cotización:</h5><br>
                <!--<input type="file" multiple name="uploadfiles[]">-->
                <input type="file" style="width:50%;margin:20px;" id="uploadfiles" accept="image/*,video/*" multiple>
                <input type="hidden" name="uploadfiles_resp" value="<?php if(isset($archivos_presupuesto_lineal)) echo $archivos_presupuesto_lineal;?>">
            </div>
        </div>

        <br>
        <div class="row" style="border: 1px solid black; background-color:#eee;">
            <div class="col-sm-6">
                <h5 style="color:#337ab7;font-weight:400;">Comentario del técnico para Refacciones:</h5>
                <textarea id="comentarioTecnico" name="comentarioTecnico" rows="2" style="width:100%;border-radius:4px;" placeholder="Clave de la refacción..."><?php echo set_value("comentarioTecnico"); ?></textarea>
            </div>
            <div class="col-sm-6" align="right">
                <h5 style="color:#337ab7;font-weight:400;">Archivo(s) del técnico para Refacciones:</h5>
                <!--<input id="uploadfilesTec" type="file" accept="image/*" multiple name="uploadfilesTec[]" >-->
                <input type="file" style="width:50%;margin:20px;" id="uploadfilesTec" accept="image/*,video/*" multiple>
                
            </div>
            <br>
        </div>
        <input type="hidden" name="tempFileTecnico" value="<?php if(isset($archivoImgTecnico)) echo $archivoImgTecnico;?>">
    
        <br><br>
        <div class="row">
            <div class="col-sm-3" align="center"></div>
            <div class="col-sm-3" align="center">
                <!-- Datos basicos -->
                <input type="button" class="btn btn-success terminarCotizacion_alta" name="aceptarCotizacion" value="Enviar Cotización a Ventanilla" <?= ((!in_array($usuario, $usuariosPermitidos)) ? 'hidden' : ((!in_array($tipo_orden, $tipos_orden_permitido)) ? 'hidden' : '')) ?>>
            </div>
            <div class="col-sm-3" align="center">
                <input type="button" class="btn btn-success terminarCotizacion_alta" name="aceptarCotizacion" style="margin-left: 20px;" value="Enviar Cotización al Asesor" <?= ((!in_array($usuario, $usuariosPermitidos)) ? 'hidden' : ((!in_array($tipo_orden, $tipos_orden_permitido)) ? 'hidden' : '')) ?>>
            </div>
            <div class="col-sm-3" align="center"></div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-4" align="center"></div>
            <div class="col-sm-4" align="center">
                <i class="fas fa-spinner cargaIcono"></i>
                <br>

                <label class="labelUsuarioError error" style="font-size:14px; display:none;">
                    A ocurrido un error al intentar guardar la información, favor de verificar los campos!!
                </label>
                <br>
            </div>
            <div class="col-sm-4" align="center">
                <br>
            </div>
        </div>

        <div class="col-sm-12" align="center">
            <i class="fas fa-spinner cargaIcono"></i>
            <h4 class="error" id="errorEnvio"></h4>

            <input type="hidden" name="modoVistaFormulario" id="" value="1">
            
            <input type="hidden" id="usuario" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
            <input type="hidden" id="vista" value="<?php if(isset($vista)) echo $vista; ?>">
            <input type="hidden" id="ordenT" value="<?php if(isset($tipo_orden)) echo $tipo_orden; ?>">
            <input type="hidden" id="curl_2" value="<?php if(isset($id_cita_url)) echo $id_cita_url; ?>">
            <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
            <input type="hidden" name="tipoRegistro" id="tipoRegistro" value="<?php if(isset($tipoRegistro)) echo $tipoRegistro; ?>">

            <input type="hidden" name="refAutorizadas" id="refAutorizadas" value="<?php if (isset($pzsAprobadasJDT_2)) echo $pzsAprobadasJDT_2; else echo ''?>">
        </div>
    
        <br><br>
        <div class="col-sm-12">
            <div class="alert alert-danger" align="left">
                <strong style="font-size:16px !important;">NOTA: Las refacciones marcadas con garnatías seran enviadas al modulo de garantías para su revisión.</strong>
            </div>
        </div>
    </form>

    <br><br><br>
</div>


<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script>
    function datos_cabecera(){
        var id_cita = $("#idOrdenTempCotiza").val();
        var url = $("#basePeticion").val();
        try{
            $.ajax({
                url: url+"mod_asetec/Presupuestos_HYP/revisar_datos",
                method: 'post',
                data: {
                    id_cita: id_cita,
                },
                success:function(resp){
                    //console.log(resp);
                    if (resp.indexOf("handler           </p>")<1) {
                        var data = JSON.parse(resp);
                        //console.log(data);
                        $("#folio").val(data.folio);
                        $("#serie").val(data.serie);
                        $("#txtmodelo").val(data.modelo);
                        $("#placas").val(data.placas);
                        $("#uen").val(data.vehiculo);
                        $("#tecnico").val(data.tecnico);
                        $("#asesors").val(data.asesor);

                        if (data.pr_id != 0) {
                            var usuario = $("#usuario").val();
                            location.href = url+"Alta_PresupuestoHYP/"+data.id_cita_url+"/"+usuario;
                        }
                    }
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                //Cierre del error
                }
            //Cierre del ajax
            });
        } catch (e) {
            console.log(e);
        } finally {
            console.log("Finalizo");
        }
    }
</script>