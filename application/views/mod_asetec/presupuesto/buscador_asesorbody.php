<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Presupuestos Multipunto</h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_AsesorBody";?>';">
                        Regresar
                    </button>
                </div>
            </div>

            <br>
            <div class="panel panel-default" align="center">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row" hidden>
                        <div class="col-sm-3" style="padding: 0px;padding-left: 30px;">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_gral" placeholder="Buscar...">
                            </div>
                            <input type="hidden" name="" id="indicador" value="JefeTaller">
                        </div>

                        <div class="col-sm-2" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_asesor_table" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <i class="fas fa-spinner cargaIcono"></i>

                    <table <?php if ($presupuestos != NULL) echo 'id="bootstrap-data-table2"'; ?> class="table table-striped table-bordered table-responsive" style="width: 100%;">
                        <thead>
                            <tr style="background-color: #eee;">
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>NO. ORDEN</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA RECEPCIÓN</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>SERIE</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>VEHÍCULO</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>APRUEBA CLIENTE</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>EDO. REFACCIONES</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                            </tr>
                        </thead>
                        <tbody id="imprimir_busqueda">
                            <?php if ($presupuestos != NULL): ?>
                                <?php foreach ($presupuestos as $i => $registro): ?>
                                    <tr style="font-size:12px;">
                                        <td align="center" style="vertical-align: middle;background-color: <?= (($registro->firma_asesor != '') ? "#68ce68" : "#fffff") ?>;">
                                            <?= $registro->id_cita ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php 
                                                $fecha = new DateTime($registro->fecha_recepcion.' 00:00:00');
                                                echo $fecha->format('d-m-Y');
                                             ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->serie ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->vehiculo ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->placas ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->asesor ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->tecnico ?>
                                        </td>

                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?php if ($registro->acepta_cliente != ""): ?>
                                                <?php if ($registro->acepta_cliente == "Si"): ?>
                                                    <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                <?php elseif ($registro->acepta_cliente == "Val"): ?>
                                                    <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                <?php else: ?>
                                                    <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                            <?php endif; ?>

                                            <?php 
                                                if ($registro->acepta_cliente != "No") {
                                                    if ($registro->estado_refacciones == "0") {
                                                        $color = "background-color:#f5acaa;";
                                                    } elseif ($registro->estado_refacciones == "1") {
                                                        $color = "background-color:#f5ff51;";
                                                    } elseif ($registro->estado_refacciones == "2") {
                                                        $color = "background-color:#5bc0de;";
                                                    }else{
                                                        $color = "background-color:#c7ecc7;";
                                                    }
                                                } else {
                                                    $color = "background-color:red;color:white;";
                                                }
                                             ?>
                                        </td>

                                        <td align="center" style="vertical-align: middle; <?= $color ?>">
                                                <?php if (($registro->estado_refacciones == "0")&&($registro->acepta_cliente != "No")): ?>
                                                    SIN SOLICITAR
                                                <?php elseif ($registro->acepta_cliente == "No"): ?>
                                                    RECHAZADA
                                                <?php elseif ($registro->estado_refacciones == "1"): ?>
                                                    SOLICITADAS
                                                <?php elseif ($registro->estado_refacciones == "2"): ?>
                                                    RECIBIDAS
                                                <?php elseif ($registro->estado_refacciones == "3"): ?>
                                                    ENTREGADAS
                                                <?php else: ?>
                                                    SIN SOLICITAR *
                                                <?php endif ?>
                                            </td>

                                        <td align="center" style="width:15%;vertical-align: middle;">
                                            <?php 
                                                $id = (double)$registro->id_cita*CONST_ENCRYPT;
                                                $url_id = base64_encode($id);
                                                $url = str_replace("=", "" ,$url_id);
                                            ?>

                                            <a href="<?=base_url().'OrdenServicio_Revision/'.$url;?>" class="btn btn-warning" target="_blank" style="font-size: 12px;">ORDEN</a>
                                        </td>    

                                        <td align="center" style="vertical-align: middle;">
                                            <!-- Verificamos si se va a editar o se va visualizar el presupuesto -->
                                            <?php if ($registro->firma_asesor != ''): ?>
                                                <a href="<?=base_url()."Presupuesto_Asesor/".$url;?>" class="btn btn-info" style="color:white;font-size: 12px;">VER</a>
                                            <?php else: ?>
                                                <a href="<?=base_url()."Presupuesto_Asesor/".$url;?>" class="btn btn-primary" style="color:white;font-size: 12px;">REVISAR</a>
                                            <?php endif ?>
                                        </td>

                                         <td align="center" style="width:15%;vertical-align: middle;">
                                            <!--Si ya firmo el asesor y el cliente aun no toma una decision, dejamos afectar al asesor-->
                                            <?php if (($registro->firma_asesor != '')&&($registro->acepta_cliente == "")): ?>
                                                <a href="<?=base_url()."Presupuesto_Cliente/".$url;?>" class="btn btn-warning" style="color:white;font-size: 10px;">AFECTAR<br>PRESUPUESTO</a>

                                            <!--Si ya firmo el asesor y el cliente ya toma una decisionmostramos cotizacion final-->
                                            <?php elseif (($registro->firma_asesor != '')&&($registro->acepta_cliente != "")): ?>
                                                <a href="<?=base_url()."Presupuesto_Cliente/".$url;?>" class="btn btn-success" style="color:white;font-size: 10px;">PRESUPUESTO<br>FINAL</a>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="10" style="width:100%;" align="center">
                                        <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                        <h4>Sin registros que mostrar</h4>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>

<script>
    var conf_tabla = $('#bootstrap-data-table2');
    conf_tabla.DataTable({
        "language": {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": ",",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
              }
        },
        "responsive": true,
        "ordering": false,
        "searching": true,
        //"bPaginate": false,
        //"paging": false,
        //"bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        //"bAutoWidth": false,
        //"processing": true,
        //"serverSide": true
    });
</script>
