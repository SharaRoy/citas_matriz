<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-4" align="left" style="padding-top: 20px;">
                    <?php if ($this->session->userdata('rolIniciado')): ?>
                        <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "ASETEC"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_AsesorBody";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "PCO"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Principal_CierreOrden/5";?>';">
                                Cerrar Sesión
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "GARANTIA"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Garantias_Panel/5";?>';">
                                Cerrar Sesión
                            </button>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-8">
                    <h3 align="right">Documentación de Ordenes</h3>
                </div>
            </div>
            
            <br>
            <div class="panel panel-default">
                <div class="panel-body panel-flotante" style="border: 2px solid darkblue;">
                    <div class="row" hidden>
                        <div class="col-md-3">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?= date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?= date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_tecnicos" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <!-- formulario de busqueda -->
                    </div>

                    <br>
                    <div class="row" hidden>
                        <div class="col-md-7"></div>
                        <div class="col-md-3" align="right">
                            <h5>Actualizar documentacón:</h5>
                            <div class="input-group">
                                <input type="date" class="form-control" id="busqueda_fecha" max="<?= date("Y-m-d") ?>" value="<?= date("Y-m-d") ?>" style="padding-top: 0px;">
                            </div>
                        </div>

                        <div class="col-md-2" align="center">
                            <br><br>
                            <button class="btn btn-warning" type="button" id="actualizar_doc" style="margin-right: 15px;"> 
                                <i class="fa fa-refresh"></i>
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <h5 style="color:red;text-align: center;" id="error_act_doc"></h5>
                        <table <?= (isset($registros) ? ((count($registros) > 0) ? 'id="bootstrap-data-table2"' : '' ) : '') ?> class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:12px;background-color: #eee;">
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>NO. <br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>APERTURA<br>ORDEN</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                    <td align="center" rowspan="2" style="width: 10%;vertical-align: middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 5%;font-size:9px;"><br></td>
                                    <td align="center" style="width: 5%;font-size:9px;"><br></td>
                                    <td align="center" style="width: 5%;font-size:9px;"><br></td>
                                    <td align="center" style="width: 5%;font-size:9px;"><br></td>
                                    <td align="center" style="width: 5%;font-size:9px;"><br></td>
                                    <!--<td align="center" style="width: 5%;font-size:9px;">SERVICIO VALET</td>-->
                                </tr>
                                <tr style="background-color: #ddd;">
                                    <td align="center" style="width: 5%;font-size:9px;">ORDEN</td>
                                    <td align="center" style="width: 5%;font-size:9px;">ARC.OASIS</td>
                                    <td align="center" style="width: 5%;font-size:9px;">MULTIPUNTO</td>
                                    <td align="center" style="width: 5%;font-size:9px;">PRESUPUESTO</td>
                                    <td align="center" style="width: 5%;font-size:9px;">VOZ CLIENTE</td>
                                    <!--<td align="center" style="width: 5%;font-size:9px;">SERVICIO VALET</td>-->
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($registros)): ?>
                                    <?php for ($index = 0; $index < count($registros); $index++): ?>
                                        <tr style="font-size:11px; background-color: <?= (($registros[$index]["id_tipo_orden"] == '6' ) ? '#fadbd8' : '#fdfefe') ?>;">
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registros[$index]["identificador"]."-".$registros[$index]["id_cita"]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registros[$index]["folio_externo"]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php 
                                                    $fecha = new DateTime($registros[$index]["fecha_recepcion"].' '.$registros[$index]["hora_recepcion"]);
                                                    echo $fecha->format('d-m-Y');
                                                 ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registros[$index]["vehiculo"]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registros[$index]["placas"]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registros[$index]["asesor"]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registros[$index]["tecnico"]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registros[$index]["cliente"]; ?>
                                            </td>
                                            <!-- Orden de servicio (Vista rapida) -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php 
                                                    $id = (double)$registros[$index]["id_cita"] * CONST_ENCRYPT;
                                                    $url_id = base64_encode($id);
                                                    $url = str_replace("=", "" ,$url_id);
                                                ?>
                                                <a href="<?=base_url().'OrdenServicio_Revision/'.$url;?>" class="btn btn-info" target="_blank" style="font-size: 10px;">ORDEN</a>
                                            </td>

                                            <!-- Archivos Oasis -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($registros[$index]["oasis"] != ""): ?>
                                                    <?php 
                                                        $indice = 1; 
                                                        $oasis = explode("|",$registros[$index]["oasis"]);
                                                    ?>
                                                    <?php for ($i = 0; $i < count($oasis); $i++): ?>
                                                        <?php if ($oasis[$i] != ""): ?>
                                                            <a href="<?=base_url().$oasis[$i];?>" class="btn btn-primary" target="_blank" style="font-size: 10px;margin-top:5px;">
                                                                ARC. OASIS (<?= $indice++; ?>)
                                                            </a>
                                                            <br>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>
                                                <?php else: ?>
                                                    <button type="button" class="btn btn-default" name="" style="font-size: 10px;">SIN <br> ARCHIVO</button>
                                                <?php endif; ?> 
                                            </td>
                                            
                                            <!-- Hoja Multipunto (Vista rapida), con indicador de lavado y firma jefe de tallar -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($registros[$index]["multipunto"] == "1"): ?>
                                                    <a href="<?=base_url().'Multipunto_Revision/'.$url;?>" class="btn <?= (($registros[$index]['firma_jefetaller'] == "1") ? 'btn-success' : 'btn-danger') ?>" target="_blank" style="font-size: 10px;">MULTIPUNTOS</a>
                                                <?php endif; ?>
                                                
                                                <label style="color:blue;font-weight: bold;">
                                                    <?= ((isset($registros[$index]["id_lavador_cambio_estatus"])) ? (($registros[$index]["id_lavador_cambio_estatus"] != "") ? 'TERMINADO' : '') : '') ?>
                                                </label>
                                            </td>
                                            
                                            <!-- Presupuestos multipunto (solo nuevos formatos) -->
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($registros[$index]["presupuesto"] == "1"): ?>
                                                    <a href="<?=base_url().'Alta_PresupuestoHYP/'.$url.'/0';?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">PRESUPUESTO</a>
                                                <?php endif; ?>
                                            </td> 
                                            
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($registros[$index]["voz_cliente"] == "1"): ?>
                                                    <a href="<?=base_url().'VCliente_Revision/'.$url;?>" class="btn btn-dark" target="_blank" style="font-size: 10px;color:white;height:100%;">VOZ CLIENTE</a>
                                                    
                                                    <br>
                                                    <?php if ($registros[$index]["evidencia_voz_cliente"] != ""): ?>
                                                        <a class="AudioModal" data-value="<?= $registros[$index]["evidencia_voz_cliente"]; ?>" data-orden="<?= $registros[$index]["id_cita"]; ?>" data-target="#muestraAudio" data-toggle="modal" style="color:blue;font-size:24px;">
                                                            <i class="far fa-play-circle" style="font-size:24px;"></i>
                                                        </a>.
                                                    <?php else: ?>
                                                        <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>

                                            <!--<td align="center" style="vertical-align: middle;">
                                                <?php if ($servicio_valet[$index] == "1"): ?>
                                                    <a href="<?=base_url().'Servicio_valet_PDF/'.$id_cita_url[$index];?>" class="btn btn-default" target="_blank" style="font-size: 10px;background-color:  #a569bd; color:white;">VALET</a>
                                                <?php endif; ?>
                                            </td>-->
                                        </tr>
                                    <?php endfor; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="14" style="width:100%;" align="center">
                                            <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                                <h4>Sin registros que mostrar</h4>
                                            <?php else: ?>
                                                <h4>Sesión Expirada</h4>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="pOrigen" id="pOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                        <input type="hidden" id="origen_lista" value="Asesor">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" autoplay style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>

<script>
    var conf_tabla = $('#bootstrap-data-table2');
    conf_tabla.DataTable({
        "language": {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": ",",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
              }
        },
        "responsive": true,
        "ordering": false,
        "searching": true,
        //"bPaginate": false,
        //"paging": false,
        //"bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        //"bAutoWidth": false,
        //"processing": true,
        //"serverSide": true
    });
</script>