<div style="margin:10px;back">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2" style="padding-top: 20px;">
                <?php if ($this->session->userdata('rolIniciado')): ?>
                    <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($this->session->userdata('rolIniciado') == "ASETEC"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_AsesorBody";?>';">
                            Regresar
                        </button>
                    <?php elseif ($this->session->userdata('rolIniciado') == "PCO"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Principal_CierreOrden/5";?>';">
                            Cerrar Sesión
                        </button>
                    <?php elseif ($this->session->userdata('rolIniciado') == "GARANTIA"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Garantias_Panel/5";?>';">
                            Cerrar Sesión
                        </button>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-10" align="right">
                <h3 style="margin-right: 10px;text-align: right;">Ordenes de servicio por estatus</h3>
            </div>
        </div>

        <div class="col-md-12">
            <br>
            <div class="panel panel-default">
                <div class="panel-body panel-flotante">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <table <?php if ($contenido != NULL) echo 'id="bootstrap-data-table2"'; ?> class="table table-striped table-bordered table-responsive" style="width: 100%;">
                        <thead>
                            <tr style="background-color: #eee;">
                                <td align="center" style="vertical-align: middle;width: 8%;"><strong>ORDEN</strong></td>
                                <td align="center" style="vertical-align: middle;width: 10%;"><strong>FECHA APERTURA</strong></td>
                                <td align="center" style="vertical-align: middle;width: 12%;"><strong>VEHÍCULO</strong></td>
                                <td align="center" style="vertical-align: middle;width: 12%;"><strong>PLACAS</strong></td>
                                <td align="center" style="vertical-align: middle;width: 17%;"><strong>ASESOR</strong></td>
                                <td align="center" style="vertical-align: middle;width: 17%;"><strong>TÉCNICO</strong></td>
                                <td align="center" style="vertical-align: middle;width: 16%;"><strong>CLIENTE</strong></td>
                                <td align="center" style="vertical-align: middle;width: 15%;"><strong>ESTATUS</strong></td>
                            </tr>
                        </thead>
                        <tbody id="imprimir_busqueda">
                            <?php if ($contenido != NULL): ?>
                                <?php foreach ($contenido as $i => $registro): ?>
                                    <tr style="font-size:12px;">
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->orden_cita ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php 
                                                $fecha = new DateTime($registro->fecha_recepcion);
                                                echo $fecha->format('d/m/Y');;
                                             ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->vehiculo_modelo ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->vehiculo_placas ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->asesor ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->tecnico ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->datos_nombres." ".$registro->datos_apellido_paterno." ".$registro->datos_apellido_materno ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;color:white;background-color: <?=$registro->color; ?>;">
                                            <?php echo strtoupper($registro->status); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="8" style="width:100%;" align="center">
                                        <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                        <h4>Sin registros que mostrar</h4>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>

<script>
    var conf_tabla = $('#bootstrap-data-table2');
    conf_tabla.DataTable({
        "language": {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": ",",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
              }
        },
        "responsive": true,
        "ordering": false,
        "searching": false,
        //"bPaginate": false,
        //"paging": false,
        //"bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        //"bAutoWidth": false,
        //"processing": true,
        //"serverSide": true
    });
</script>