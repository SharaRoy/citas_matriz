<div class="row">
	<div class="col-sm-12" align="center" style="">
		<h2 style="font-size: 30px; color: #716c6c;">BIENVENIDOS A:</h2>
		<img src="<?php echo base_url(); ?>assets/imgs/logos/LOGO-SOHEX.png" alt="" class="logo" style="width:330px;">
	</div>
</div>

<section class="content" style="margin: 20px;">
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<div class="panel panel-primary">
				<div class="panel-heading" align="center">
					<h3 class="panel-title">PANEL DE <?php if(isset($panel)) echo $panel; ?></h3>
				</div>

				<form name="" method="post" action="" id="login_soporte"> 
					<div class="panel-body" align="center">
						<div class="form-group" style="margin-top:10px;font-size:16px;" align="left">
							<input type="text" class="form-control" name="inputUsuario" placeholder="Usuario" value="" style="width:100%;">
							<span class="error" id='error_usuario'></span>
						</div>
						<div class="form-group" style="margin-top:10px;font-size:16px;" align="left">
							<input type="password" class="form-control" name="inputPassword" placeholder="Contraseña"  style="width:100%;">
							<span class="error" id='error_pass'></span>
						</div>
					</div>

					<div class="panel-body" align="center">
						<i class="fas fa-spinner cargaIcono"></i>
	                    <br>
	                    <h5 class="error" id="error_formulario"></h5>
						<input type="button" name="ingreso" id="ingreso_login" onclick="verificacion()" class="btn btn-success" value="Iniciar Sesión">
					</div>
				</form>
			</div>
		</div>
		<div class="col-sm-2"></div>
	</div>

	<div class="row">
		<div class="col-md-12" align="center" style="">
			<img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="max-height: 2cm;">
		</div>
	</div>
</section>


<script>
	//$("#ingreso_login").on('click', function (e){
	function verificacion(){
	    var usuario = $("input[name='inputUsuario']").val();
	    var pass = $("input[name='inputPassword']").val();

	    if ((usuario != "")&&(pass != "")) {
	        $(".cargaIcono").css("display","inline-block");
	        var base = $("#basePeticion").val();

	        $.ajax({
	            url: base+"mod_asetec/Login_asetec/verifica_login", 
	            method: 'post',
	            data: {
	                usuario : usuario,
	                pass : pass,
	            },
	            success:function(resp){
	                console.log(resp);
	                if (resp.indexOf("handler         </p>")<1) {
	                    if (resp == "OK") {
	                        $("#error_formulario").text("INICIANDO....");
	                        location.href = base+"Panel_AsesorBody";
	                    } else {
	                        $("#error_formulario").text("EL USUARIO Y/O CONTRASEÑA SON INCORRECTAS");
	                    }
	                }else{
	                    $("#error_formulario").text("SE PRODUJO UN ERROR");
	                }

	                //Si ya se termino de cargar la tabla, ocultamos el icono de carga
	                $(".cargaIcono").css("display","none");
	            //Cierre de success
	            },
	              error:function(error){
	                console.log(error);
	                $(".cargaIcono").css("display","none");
	                $("#error_formulario").text("SE PRODUJO UN ERROR**");
	            }
	        });
	        //document.forms[0].submit();
	    } else {
	        $("#error_formulario").text("FALTA INDICAR ALGUN CAMPO");
	    }
	}
	//});
</script>