<div style="margin: 10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">LISTA VOZ CLIENTE</h3>

            <br>
            <div class="panel panel-default panel-flotante">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <br><br>
                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                        Regresar
                                    </button>
                                <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                        Regresar
                                    </button>
                                <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                        Regresar
                                    </button>
                                <?php elseif ($this->session->userdata('rolIniciado') == "ASETEC"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_AsesorBody";?>';">
                                        Regresar
                                    </button>
                                <?php elseif ($this->session->userdata('rolIniciado') == "PCO"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Principal_CierreOrden/5";?>';">
                                        Cerrar Sesión
                                    </button>
                                <?php elseif ($this->session->userdata('rolIniciado') == "GARANTIA"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Garantias_Panel/5";?>';">
                                        Cerrar Sesión
                                    </button>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>

                        <div class="col-md-2"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-4" hidden>
                            <h5>Busqueda No. Orden / Serie / Placas:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-2" hidden>
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_vozcliente" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                                <!--Limpiar Busqueda-->
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono" id="carga_tabla"></i>
                        <table <?php if ($listado != NULL) echo 'id="bootstrap-data-table2"'; ?> class="table table-striped table-bordered table-responsive" style="width: 100%;">
                            <thead> 
                                <tr style="font-size:14px;background-color: #5cb39a;color:white;">
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>ORDEN</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>APERTURA ORDEN</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>TÉCNICO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 40%;vertical-align:middle;"><strong>DETALLE FALLA</strong></td>                                    
                                    <td align="center" style="width: 10%;"><strong><br></strong></td>
                                    <td align="center" style="width: 10%;"><strong><br></strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if ($listado != NULL): ?>
                                    <?php foreach ($listado as $index => $registro): ?>
                                        <?php 
                                            if (isset($registro->id_vc)) {
                                                $diagnostico = "Diagnóstico(s) Guardado(s): ".$registro->n_diagnosticos;
                                                $audio = $registro->audio;
                                                $tipo = 2;
                                            } else {
                                                $diagnostico = $registro->definicionFalla;
                                                $audio = $registro->evidencia;
                                                $tipo = 1;
                                            }
                                         ?>

                                        <tr style="font-size:12px; color: black; background-color: <?= (($audio == "") ? '#f4f4f4' : '#ffffff') ?>">
                                            <td align="center" style="width:15%;vertical-align:middle;">
                                                <?= $registro->identificador."-".$registro->id_cita ?>
                                                <input type="hidden" id="orden_<?= $index+1; ?>" value="<?= $registro->id_cita ?>">
                                                
                                                <input type="hidden" id="tipo_<?= $index+1; ?>" value="<?= $tipo ?>">
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?php 
                                                    $fecha = new DateTime($registro->fecha_recepcion.' 00:00:00');
                                                    echo $fecha->format('d-m-Y');
                                                ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?= strtoupper($registro->tecnico); ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?= strtoupper($registro->asesor); ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?= strtoupper($registro->cliente); ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?= $diagnostico; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($audio != ""): ?>
                                                    <a class="AudioModal" data-value="<?= $audio; ?>" data-orden="<?= $registro->id_cita ?>" data-target="#muestraAudio" data-toggle="modal" style="color:blue;font-size:24px;">
                                                        <i class="far fa-play-circle" style="font-size:24px;"></i>
                                                    </a>
                                                <?php else: ?>
                                                    <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php if ($tipo == 1): ?>
                                                    <a class="btn btn-primary comentario" style="color:white;" data-id="<?= $index+1; ?>" data-orden="<?= $registro->id_cita ?>" data-target="#pregunta"  data-toggle="modal">
                                                        Agregar <br> comentario
                                                    </a>
                                                <?php else: ?>  
                                                    <a class="btn btn-primary comentario2" style="color:white;" data-id="<?= $index+1; ?>" data-orden="<?= $registro->id_cita ?>" data-target="#pregunta2"  data-toggle="modal">
                                                        Agregar <br> comentario
                                                    </a>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="8" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pregunta2" tabindex="-1" style="margin-top:40px;" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" align="center">
                <br><br>
                <h3>Información del registro:</h3>
                <p align="justify" style="text-align: justify; font-size:12px;margin-left: 30px;" >
                    <strong style="font-size:14px;color:darkblue;">
                        No. de Orden: 
                    </strong> 
                    <label style="font-size:13px;" id="info_reg_1"></label>

                    <br>
                    <strong style="font-size:13px;color:darkblue;">
                        Cliente: 
                    </strong> 
                    <label style="font-size:13px;" id="info_reg_2"></label>

                    <br>
                    <strong style="font-size:13px;color:darkblue;">
                        Asesor: 
                    </strong> 
                    <label style="font-size:13px;" id="info_reg_3"></label>

                    <br>
                    <strong style="font-size:13px;color:darkblue;">
                        Diagnóstico: 
                    </strong> 
                    <label style="font-size:12px;" id="info_reg_4"></label>
                </p>

                <br><br>
                <div class="row">
                    <div class="col-sm-3" align="right">
                        <label style="font-size:12px;" for="vc_id_cita">No. de orden:</label>
                    </div>
                    <div class="col-sm-6" align="left">
                        <input type="text" class="form-control" name="vc_id_cita" id="vc_id_cita" style="width:100%;" disabled="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono" id="carga_2"></i>
                        <br>
                        <label id="alertaconsulta" class="error"></label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <label for="">Diagnóstico(s):</label>
                        <br>
                        <select class="form-control" id="vaciado_diag" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-2"></div>
                </div>

                <div class="row" style="font-size:13px;">
                    <div class="col-sm-6">
                        <h5>Comentarios:</h5>
                        <textarea rows='2' id='comentario_2' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                    <div class="col-sm-6">
                        <h5>Sistema:</h5>
                        <textarea rows='2' id='sistema_2' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                </div>

                <div class="row" style="font-size:13px;">
                    <div class="col-sm-6">
                        <h5>Componetes:</h5>
                        <textarea rows='2' id='componente_2' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                    <div class="col-sm-6">
                        <h5>Causa Raíz:</h5>
                        <textarea rows='2' id='causa_2' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>

                    </div>
                </div>

                <br>
                <div class="row" align="left">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono" id="carga_3"></i>
                        <br>
                        <h5 class="error" id="error_formato_vc"></h5>
                        <input type="hidden" id="registro_diag" value="">

                        <input type="button" onclick="envioComentario2()" class="btn btn-success" value="Guardar">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelar" name="cancelar">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pregunta" tabindex="-1" style="margin-top:40px;" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" align="center">
                <br><br>
                <h3>Información del registro:</h3>
                <p align="justify" style="text-align: justify; font-size:12px;margin-left: 30px;" >
                     <strong style="font-size:14px;color:darkblue;">
                        No. de Orden: 
                    </strong> 
                    <label style="font-size:14px;" id="infoRegistroScita1"></label>

                    <br>
                     <strong style="font-size:14px;color:darkblue;">
                        Cliente: 
                    </strong> 
                    <label style="font-size:14px;" id="infoRegistroScita3"></label>

                    <br>
                     <strong style="font-size:14px;color:darkblue;">
                        Asesor: 
                    </strong> 
                    <label style="font-size:14px;" id="infoRegistroScita4"></label>

                    <br>
                </p>

                <br><br>
                <div class="row">
                    <div class="col-sm-3" align="right">
                        <label for="idOrden">No. de orden a asignar:</label>
                    </div>
                    <div class="col-sm-6" align="left">
                        <input type="text" class="form-control" name="idOrden" id="idOrden" style="width:100%;">
                    </div>
                </div>

                <div class="row" style="font-size:13px;">
                    <div class="col-sm-6">
                        <h5>Comentarios:</h5>
                        <textarea rows='2' id='comentario' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                    <div class="col-sm-6">
                        <h5>Sistema:</h5>
                        <textarea rows='2' id='sistema' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                </div>

                <div class="row" style="font-size:13px;">
                    <div class="col-sm-6">
                        <h5>Componetes:</h5>
                        <textarea rows='2' id='componente' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                    <div class="col-sm-6">
                        <h5>Causa Raíz:</h5>
                        <textarea rows='2' id='causa' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>

                    </div>
                </div>

                <br>
                <div class="row" align="left">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono" id="carga_1"></i>
                        <br>
                        <h5 class="error" id="errorIdCitaComentarioVC"></h5>

                        <input type="button" onclick="envioComentario()" class="btn btn-success" value="Guardar">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelar" name="cancelar">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para mostrar el audio de la voz cliente-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<canvas id="diagramaFallas" width="280" height="390" hidden></canvas>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>

<script>
    var conf_tabla = $('#bootstrap-data-table2');
    conf_tabla.DataTable({
        "language": {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": ",",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
              }
        },
        "responsive": true,
        "ordering": false,
        "searching": true,
        //"bPaginate": false,
        //"paging": false,
        //"bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        //"bAutoWidth": false,
        //"processing": true,
        //"serverSide": true
    });
</script>