<!DOCTYPE html>
<html lang="en">
    <head>
      	<meta charset="UTF-8">
      	<title>Correo Electrónico</title>
    </head>
    <body>
        <br>
        <img src="<?= base_url().$this->config->item('logo') ?>" alt="" style="width:400px;">
        <br><br>
        <h1>
            Solicitud de firma en documentación de autorización por ausencia.
        </h1>
        <h2><?php if(isset($formularioEnvio)) echo $formularioEnvio; else echo "Diagnóstico Por Ausencia"; ?></h2>
        <br>
        <h3>
            Documentación correspondiente al vehículo <?php if(isset($serie)) echo $serie; else echo "Sin serie"; ?> el día <?php echo $dia.", ".date("d")."/".$mes."/".date("Y"); ?>
        </h3>

        <h3>
            Puede ver el documento en el siguiente link: 
            <a href="<?php if(isset($url)) echo $url; else echo ''; ?>" target="_blank">Orden de Servicio</a>
        </h3>
      	<p>
      		  O comuniquese con su asesor de servicio ‭<a href="tel:+52<?=SUCURSAL_TEL?>">Click para llamar <?=SUCURSAL_TEL?></a> 
      	</p>

    </body>
</html>
