<section style="margin: 20px;">
    <div class="panel-flotante">
        <div class="row">
            <div class="col-md-7" align="center">
                <h3 class="h2_title_blue" style="text-align: center;">Identificación de necesidades de servicio.</h3>
            </div>

            <div class="col-md-5" align="right">
                <br>
                <h5 style="color: black;font-weight: bold;">
                    NO. ORDEN DE SERVICIO:
                    &nbsp;&nbsp; <?php if (isset($id_cita)) echo $id_cita; else echo "0"; ?>
                </h5>

                <hr>
                <h5 style="color:  #a04000 ;">
                    <?= ((isset($audio_eleccion)) ? $audio_eleccion : "") ?>
                </h5>
            </div>
        </div>
    </div>

    <br>
    <?php if (isset($imgFallas)): ?>
        <?php for ($i = 0; $i < count($sistema); $i++): ?>
            <div class="panel-flotante">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            <!-- Primera parte lado izquierdo -->
                            <div class="col-md-6 table-responsive" style="width: 100%; margin-top: 12px;">
                                <h5 style="color:darkblue;">Definición de falla.</h5>
                                <textarea class="form-control" name="falla" rows="3" style="text-align:left;width:100%;" disabled><?= $definicionFalla_txt[$i]; ?></textarea>

                                <br><br>
                                <table style="width:99%;">
                                    <tr>
                                        <td align="center">
                                            <img src="<?php echo base_url().'/assets/imgs/vista.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                        </td>
                                        <td align="center">
                                            <img src="<?php echo base_url().'/assets/imgs/tacto.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                        </td>
                                        <td align="center">
                                            <img src="<?php echo base_url().'/assets/imgs/oido.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                        </td>
                                        <td align="center">
                                            <img src="<?php echo base_url().'/assets/imgs/olfato.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <?php if (isset($sentidosFalla[$i])): ?>
                                                <?php if (in_array("Vista",$sentidosFalla[$i])): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($sentidosFalla[$i])): ?>
                                                <?php if (in_array("Tacto",$sentidosFalla[$i])): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($sentidosFalla[$i])): ?>
                                                <?php if (in_array("Oido",$sentidosFalla[$i])): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if (isset($sentidosFalla[$i])): ?>
                                                <?php if (in_array("Olfalto",$sentidosFalla[$i])): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>

                                <br>
                                <div class="row">
                                    <div class="col-md-6" style="width: 100%; " align="center">
                                        <?php if (isset($imgFallas[$i])): ?>
                                            <?php if ($imgFallas[$i] != ""): ?>
                                                <img src="<?php echo base_url().$imgFallas[$i]; ?>" alt="" style="width:260px;height: 390px;">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:260px;height: 390px;">
                                            <?php endif ?>
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:260px;height: 390px;">
                                        <?php endif; ?>
                                    </div>

                                    <div class="col-md-6" style="width: 100%; ">
                                        <table style="border: 1px solid #ddd; background-color:#ffffff;width: 99%;font-size: 14px;">
                                            <tr>
                                                <td colspan="2" style="border-bottom:1px solid #ddd;">
                                                    La falla se presenta cuando:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Arranca el vehículo.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Arranque",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Inicia movimiento.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd;border-bottom:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Inicio Movimiento",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Disminuye la velocidad.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Menos Velocidad",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Da vuelta a la izquierda.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Vuelta Izquierda",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Da vuelta a la derecha.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Vuelta Derecha",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Pasa un tope.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Tope",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Pasa un bache.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Bache",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Cambia la velocidad.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Cambio Velocidad",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Está en movimiento.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Movimiento",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Constantemente.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Constantemente",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    Esporádicamente.
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("Esporádicamente",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                                    NO APLICA
                                                </td>
                                                <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                    <?php if (isset($presentaFalla[$i])): ?>
                                                        <?php if (in_array("NA",$presentaFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        </table>

                                        <br><br>
                                        <table style="width:99%;border: 1px solid #ddd;">
                                            <tr>
                                                <td colspan="4" style="border-bottom:1px solid #ddd;">La falla se percibe en:</td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;">
                                                    Volante.
                                                </td>
                                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                                                    <?php if (isset($percibeFalla[$i])): ?>
                                                        <?php if (in_array("Volante",$percibeFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;">
                                                    Cofre.
                                                </td>
                                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                                                    <?php if (isset($percibeFalla[$i])): ?>
                                                        <?php if (in_array("Cofre",$percibeFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                                    Asiento.
                                                </td>
                                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                                    <?php if (isset($percibeFalla[$i])): ?>
                                                        <?php if (in_array("Asiento",$percibeFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                                    Cajuela.
                                                </td>
                                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                                    <?php if (isset($percibeFalla[$i])): ?>
                                                        <?php if (in_array("Cajuela",$percibeFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                                    Cristales.
                                                </td>
                                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                                    <?php if (isset($percibeFalla[$i])): ?>
                                                        <?php if (in_array("Cristales",$percibeFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                                    Toldo.
                                                </td>
                                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                                    <?php if (isset($percibeFalla[$i])): ?>
                                                        <?php if (in_array("Toldo",$percibeFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                                    Carrocería.
                                                </td>
                                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                                    <?php if (isset($percibeFalla[$i])): ?>
                                                        <?php if (in_array("Carrocería",$percibeFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                                    Debajo del vehículo.
                                                </td>
                                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                                    <?php if (isset($percibeFalla[$i])): ?>
                                                        <?php if (in_array("Debajo",$percibeFalla[$i])): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="4" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                                    Estancado:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table style="width:99%;">
                                                        <tr>
                                                            <td align="center" style="width:25%;">
                                                                Dentro.<br>
                                                                <?php if (isset($percibeFalla[$i])): ?>
                                                                    <?php if (in_array("Dentro",$percibeFalla[$i])): ?>
                                                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                                Fuera.<br>
                                                                <?php if (isset($percibeFalla[$i])): ?>
                                                                    <?php if (in_array("Fuera",$percibeFalla[$i])): ?>
                                                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                                Frente.<br>
                                                                <?php if (isset($percibeFalla[$i])): ?>
                                                                    <?php if (in_array("Frente",$percibeFalla[$i])): ?>
                                                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                                Detrás.<br>
                                                                <?php if (isset($percibeFalla[$i])): ?>
                                                                    <?php if (in_array("Detrás",$percibeFalla[$i])): ?>
                                                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                            <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                                NO APLICA<br>
                                                                <?php if (isset($percibeFalla[$i])): ?>
                                                                    <?php if (in_array("NA",$percibeFalla[$i])): ?>
                                                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                                    <?php endif; ?>
                                                                <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                                <br><br>
                            </div>

                            <!-- Primera parte lado derecho -->
                            <div class="col-md-6 table-responsive" style="width: 100%; padding: 12px 0px 0px 0px;">
                                <table class="" style="border: 1px solid black; width: 99%;">
                                    <tr style="border-bottom: 1px solid black;">
                                        <td align="center">
                                            <h4 style="font-weight:bold;">Condiciones ambientales.</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logTempBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logTempBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/metrix2.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logHumBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logHumBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/metrix_humedad2.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logVientoBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logVientoBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/metrix_viento2.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>

                                <br>
                                <table class="" style="border: 1px solid black; width: 99%;">
                                    <tr style="border-bottom: 1px solid black;">
                                        <td align="center">
                                            <h4 style="font-weight:bold;">Condiciones operativas.</h4>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logVelBarra[$i])): ?> 
                                                <img src="<?php echo base_url().$logVelBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/velocidad3.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if ($ind_transmision[$i] != "NO APLICA"): ?>
                                                <img src="<?php echo base_url().$transmision_2[$i]; ?>" style="width:10%;height:35px;margin-top:5px;" class="imgFondo">
                                                <img src="<?php echo base_url().$logCam1Barra[$i]; ?>" alt="" style="width:55%;height:35px;margin-top:5px;" class="imgFondo">
                                                <img src="<?php echo base_url().$logCam2Barra[$i]; ?>" alt="" style="width:26%;height:35px;margin-top:5px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?=base_url().'assets/imgs/reducidos/condiciones/nc/cambio3_c_nc.jpg' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logRMPBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logRMPBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/rpm3.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if ($remolque[$i] == "1"): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" class="centradoBarra" style="width:15px;margin-left:87%;margin-top: 0px;margin-bottom: -10px;" alt="">
                                            <?php endif; ?>

                                            <?php if (isset($logCargaBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logCargaBarra[$i]; ?>" alt="" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/carga3.png' ?>" alt="" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo">
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if ($ind_pasajero[$i] != "NO APLICA"): ?>
                                                <img src="<?php echo base_url().$logPasajeBarra[$i]; ?>" style="width:75%;height:35px;margin-top:5px;" class="imgFondo">
                                                <img src="<?php echo base_url().$logCajuelaBarra[$i]; ?>" style="width:24%;height:35px;margin-top:5px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?= base_url().'assets/imgs/reducidos/condiciones/nc/pasajeros3_nc.jpg' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><br></td>
                                    </tr>
                                </table>

                                <br>
                                <table class="" style="border: 1px solid black; width: 99%;">
                                    <tr style="border-bottom: 1px solid black;">
                                        <td align="center">
                                            <h4 style="font-weight:bold;">Condiciones del camino.</h4>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logEstrBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logEstrBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/estructura3.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logCamBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logCamBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/camino3.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logPenBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logPenBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/pendiente3.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr style="">
                                        <td align="left" style="padding: 0px 10px 0px 10px;">
                                            <?php if (isset($logSupeBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logSupeBarra[$i] ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/superficie3.png' ?>" style="min-width:370px;height:35px;width: 100%;margin-top:5px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label style="color:red;font-weight: bold;" align="center">
                                                N/A = NO APLCA
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <!-- Parte de abajo de la hoja -->
            <div class="panel-flotante">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12" align="right">
                                <table style="width:99%;">
                                    <tr>
                                        <td style="padding-top: 0px;padding-bottom: 0px; width:33%;">
                                          <h4 style="color:darkblue;"><u>SISTEMA:</u></h4>
                                          <textarea class="form-control" name="falla" rows="3" style="text-align:left;width:90%;" disabled><?php if(isset($sistema[$i])) echo $sistema[$i]; ?></textarea>
                                        </td>
                                        <td style="">
                                            <h4 style="color:darkblue;"><u>COMPONENTE:</u></h4>
                                            <textarea class="form-control" name="falla" rows="3" style="text-align:left;width:90%;" disabled><?php if(isset($componente[$i])) echo $componente[$i]; ?></textarea>
                                        </td>
                                        <td style="padding-top: 0px;padding-bottom: 0px; width:33%;">
                                            <h4 style="color:darkblue;"><u>CAUSA RAÍZ:</u></h4>
                                            <textarea class="form-control" name="falla" rows="3" style="text-align:left;width:90%;" disabled><?php if(isset($causaRaiz[$i])) echo $causaRaiz[$i]; ?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <br><br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h5 style="color:darkblue;">COMENTARIO:</h5>
                                            <textarea class="form-control" name="falla" rows="3" style="text-align:left;width:100%;" disabled><?php if(isset($comentarios[$i])) echo $comentarios[$i]; ?></textarea>
                                        </td>
                                        <td>
                                            <br>
                                        </td>
                                    </tr>
                                </table>

                                <br>
                                <h5 style="color: darkred;font-weight: bold;">
                                    <?= ($i+1) ." / ". count($comentarios) ?>
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br><br>
        <?php endfor ?>
    <?php endif ?>
            
    <br>
    <div class="panel-flotante">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="" style="width:100%">
                            <tr>
                                <td style="width:5%;" rowspan="2">
                                    <br>
                                    <canvas id="diagramaFallas" width="280" height="390" hidden></canvas>
                                </td>
                                <td align="center"style="color:#337ab7;width:40%;">
                                    <?php if (isset($firmaAsesor)): ?>
                                        <?php if ($firmaAsesor != ""): ?>
                                            <img src="<?php echo base_url().$firmaAsesor; ?>" style="width: 150px;">    
                                        <?php endif ?>
                                    <?php endif; ?>
                                    <br>
                                    <label for="">
                                        <?php if (isset($nombreAsesor)): ?>
                                            <?php if (strlen($nombreAsesor)<40): ?>
                                                <?php echo $nombreAsesor; ?>
                                            <?php else: ?>
                                                <?php echo substr($nombreAsesor, 0, 40) ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </label>
                                </td>
                                <td style="width:5%;"></td>
                                <td align="center" style="color:#337ab7;width:40%;">
                                    <?php if (isset($firmaCliente)): ?>
                                        <?php if ($firmaCliente != ""): ?>
                                            <img src="<?php echo base_url().$firmaCliente; ?>" style="width: 150px;">    
                                        <?php endif ?>
                                    <?php endif; ?>
                                    <br>
                                    <label for="">
                                        <?php if (isset($nombreCliente)): ?>
                                            <?php if (strlen($nombreCliente)<40): ?>
                                                <?php echo $nombreCliente; ?>
                                            <?php else: ?>
                                                <?php echo substr($nombreCliente, 0, 40) ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </label>
                                    <!-- <label for=""><?php if(isset($nombreCliente)) echo $nombreCliente; ?></label> -->
                                </td>
                            </tr>
                            <tr>
                                <!-- <td style="width:15%;"></td> -->
                                <td align="center" style="border-top: 1px solid #337ab7;">
                                    <label for="">Nombre y firma del asesor.</label><br>
                                </td>
                                <td style="width:5%;"></td>
                                <td align="center" style="border-top: 1px solid #337ab7;">
                                    <label for="">Nombre y firma del cliente.</label><br>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="" id="cargaFormulario" value="2">
    <input type="hidden" name="" id="formulario" value="Revisión">
</section>