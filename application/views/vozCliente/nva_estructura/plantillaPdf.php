<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
     <head>
        <title>VOZ CLIENTE <?php if(isset($id_cita)) echo $id_cita;?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="estilos.css" />
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <style>
            @page { 
                sheet-size: A4;
                size: auto; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 5mm; 
                /*margin-top: 5mm; /* <any of the usual CSS values for margins> */
                /*margin-left: 5mm;*/
                /*margin-right: 5mm;*/
                /*margin-bottom: 10mm;*/
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 5mm; /* <any of the usual CSS values for margins> */
                margin-footer: 5mm; /* <any of the usual CSS values for margins> */
            }
        </style>

        <style media="screen"> 
            td,tr{
                padding: 0px 5px 5px 0px !important;
                font-size: 9px;
                color:#337ab7;
            }

            .centradoBarra{
                position: relative;
                max-height: 4px;
                /* margin-top: -12px; */
                margin-left: -30px;
                top: 1px;;
                left: 9px;
                /* transform: translate(-50%, -50%); */
            }

            .contenedorImgFondo{
                position: relative;
                display: inline-block;
                text-align: center;
            }

            .imgFondo{
                /* width: 300px; */
                /* height: 60px; */
                margin-top: -15px;
            }
        </style>
    </head>
    <body>
        <div>
            <?php if (isset($imgFallas)): ?>
                <?php for ($i = 0; $i < count($sistema); $i++): ?>
                    <table style="font-size:10px;margin-top:-13px;">
                        <tr>
                            <!-- Primera parte lado izquierdo -->
                            <td>
                                <table style="width: 95%:">
                                    <tr>
                                        <td colspan="4">
                                            <h5 style="color:#337ab7;font-size: 16px;">
                                                Identificación de necesidades de servicio.
                                                <br>
                                                No. de Orden: <?php if(isset($id_cita)) echo $id_cita;?>
                                            </h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="border: 1px solid gray;height:110px;width: 300px; background-color:#ffffff;">
                                            <span style="color:#337ab7;font-size: 13px;">Definición de falla.</span>
                                            <p style="text-align: justify; font-size:12px;margin-bottom: 0px;color:black;">
                                                <?= $definicionFalla_txt[$i]; ?>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="background-color:#ffffff;" align="center">
                                            <table>
                                                <tr>
                                                    <td align="center" style="width: 76px;">
                                                        <img src="<?php echo base_url().'assets/imgs/reducidos/vista-min.png' ?>" style="height: 38px;margin-top:6px;">
                                                    </td>
                                                    <td align="center" style="width: 76px;">
                                                        <img src="<?php echo base_url().'assets/imgs/reducidos/tacto-min.png' ?>" style="height: 38px;margin-top:6px;">
                                                    </td>
                                                    <td align="center" style="width: 76px;">
                                                        <img src="<?php echo base_url().'assets/imgs/reducidos/oido-min.png' ?>" style="height: 38px;margin-top:6px;">
                                                    </td>
                                                    <td align="center" style="width: 76px;">
                                                        <img src="<?php echo base_url().'assets/imgs/reducidos/olfato-min.png' ?>" style="height: 38px;margin-top:6px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <?php if (isset($sentidosFalla[$i])): ?>
                                                            <?php if (in_array("Vista",$sentidosFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($sentidosFalla[$i])): ?>
                                                            <?php if (in_array("Tacto",$sentidosFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($sentidosFalla[$i])): ?>
                                                            <?php if (in_array("Oido",$sentidosFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center">
                                                        <?php if (isset($sentidosFalla[$i])): ?>
                                                            <?php if (in_array("Olfalto",$sentidosFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="background-color:#ffffff;" align="center">
                                            <?php if (isset($imgFallas[$i])): ?>
                                                <?php if ($imgFallas[$i] != ""): ?>
                                                    <img src="<?php echo base_url().$imgFallas[$i]; ?>" alt="" style="width:180px;height: 270px;">
                                                <?php else: ?>
                                                    <?php if (file_exists("assets/imgs/cuadricula.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:180px;">
                                                    <?php endif ?>                                    
                                                <?php endif ?>
                                            <?php else: ?>
                                                <?php if (file_exists("assets/imgs/cuadricula.png")): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:180px;">
                                                <?php endif ?>
                                            <?php endif; ?>
                                        </td>
                                        <td colspan="2">
                                            <table style="border: 1px solid #ddd; width:170px; background-color:#ffffff;">
                                                <tr>
                                                    <td colspan="2" style="border-bottom:1px solid #ddd;font-size: 13px;color:#337ab7;">
                                                        La falla se presenta cuando:
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Arranca el vehículo.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Arranque",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Inicia movimiento.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd;border-bottom:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Inicio Movimiento",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Disminuye la velocidad.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Menos Velocidad",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Da vuelta a la izquierda.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Vuelta Izquierda",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Da vuelta a la derecha.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Vuelta Derecha",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Pasa un tope.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Tope",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Pasa un bache.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Bache",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Cambia la velocidad.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Cambio Velocidad",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Está en movimiento.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Movimiento",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Constantemente.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Constantemente",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        Esporádicamente.
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("Esporádicamente",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:60%; border-bottom:1px solid #ddd;font-size: 10px;"  align="left">
                                                        NO APLICA
                                                    </td>
                                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                        <?php if (isset($presentaFalla[$i])): ?>
                                                            <?php if (in_array("NA",$presentaFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>

                                <br>
                                <table style="width:100%;border: 1px solid #ddd;">
                                    <tr>
                                        <td colspan="4" style="border-bottom:1px solid #ddd;font-size: 14px;color:#337ab7;">
                                            La falla se percibe en:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;font-size: 10px;">
                                            Volante.
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                                            <?php if (isset($percibeFalla[$i])): ?>
                                                <?php if (in_array("Volante",$percibeFalla[$i])): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;font-size: 10px;">
                                            Cofre.
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                                            <?php if (isset($percibeFalla[$i])): ?>
                                                <?php if (in_array("Cofre",$percibeFalla[$i])): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 10px;">
                                            Asiento.
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                            <?php if (isset($percibeFalla[$i])): ?>
                                                <?php if (in_array("Asiento",$percibeFalla[$i])): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 10px;">
                                            Cajuela.
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                            <?php if (isset($percibeFalla[$i])): ?>
                                                <?php if (in_array("Cajuela",$percibeFalla[$i])): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 10px;">
                                            Cristales.
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                            <?php if (isset($percibeFalla[$i])): ?>
                                                <?php if (in_array("Cristales",$percibeFalla[$i])): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 10px;">
                                            Toldo.
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                            <?php if (isset($percibeFalla[$i])): ?>
                                                <?php if (in_array("Toldo",$percibeFalla[$i])): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 10px;">
                                            Carrocería.
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                            <?php if (isset($percibeFalla[$i])): ?>
                                                <?php if (in_array("Carrocería",$percibeFalla[$i])): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 10px;">
                                            Debajo del vehículo.
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                            <?php if (isset($percibeFalla[$i])): ?>
                                                <?php if (in_array("Debajo",$percibeFalla[$i])): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="4" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 10px;">
                                            Estancado:
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td align="center" style="width:25%;">
                                                        Dentro.<br>
                                                        <?php if (isset($percibeFalla[$i])): ?>
                                                            <?php if (in_array("Dentro",$percibeFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                        Fuera.<br>
                                                        <?php if (isset($percibeFalla[$i])): ?>
                                                            <?php if (in_array("Fuera",$percibeFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                        Frente.<br>
                                                        <?php if (isset($percibeFalla[$i])): ?>
                                                            <?php if (in_array("Frente",$percibeFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                        Detrás.<br>
                                                        <?php if (isset($percibeFalla[$i])): ?>
                                                            <?php if (in_array("Detrás",$percibeFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                    <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                        NO APLICA<br>
                                                        <?php if (isset($percibeFalla[$i])): ?>
                                                            <?php if (in_array("NA",$percibeFalla[$i])): ?>
                                                                <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;height: 15px;">
                                                                <?php endif ?>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                            <!-- Primera parte lado derecho -->
                            <td align="center">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="border-bottom: 1px solid gray;">
                                            <br>
                                            <span style="color:#337ab7;font-size: 13px;font-weight: bold;">
                                                CONDICIONES AMBIENTALES.
                                            </labspanel>

                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;

                                            <span style="color:red; font-weight: bold;font-size: 10px;">
                                                N/A = NO APLCA
                                            </laspanbel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="contenedorImgFondo">
                                            <?php if (isset($logTempBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logTempBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/metrix2.png' ?>" style="width:390px;height:32px;" class="imgFondo" />
                                            <?php endif; ?>

                                            <?php if (isset($logHumBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logHumBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/metrix_humedad2.png' ?>" style="width:390px;height:32px;" class="imgFondo" />
                                            <?php endif; ?>

                                            <?php if (isset($logVientoBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logVientoBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo" />
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/metrix_viento2.png' ?>" style="width:390px;height:32px;" class="imgFondo" />
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>

                                <br><br>
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="border-bottom: 1px solid gray;">
                                            <br>
                                            <span style="color:#337ab7;font-size: 13px;font-weight: bold;">
                                                CONDICIONES OPERATIVAS.
                                            </labspanel>

                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;

                                            <span style="color:red; font-weight: bold;font-size: 10px;">
                                                N/A = NO APLCA
                                            </laspanbel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="border-bottom: 1px solid gray;">
                                            <label style="color:red;font-weight: bold;" align="right">
                                                N/A = NO APLCA
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="contenedorImgFondo">
                                            <?php if (isset($logVelBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logVelBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/velocidad3.png' ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif; ?>

                                            <?php if ($ind_transmision[$i] != "NO APLICA"): ?>
                                                <img src="<?php echo base_url().$transmision_2[$i]; ?>" style="width:55px;height:32px;" class="imgFondo">
                                                <img src="<?php echo base_url().$logCam1Barra[$i]; ?>" alt="" style="width:215px;height:32px;" class="imgFondo">
                                                <img src="<?php echo base_url().$logCam2Barra[$i]; ?>" alt="" style="width:100px;height:32px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?=base_url().'assets/imgs/reducidos/condiciones/nc/cambio3_c_nc.jpg' ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif ?>

                                            <?php if (isset($logRMPBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logRMPBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/rpm3.png' ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif; ?>

                                            <?php if (isset($remolque[$i])): ?>
                                                <?php if ($remolque[$i] == "1"): ?>
                                                    <?php if (file_exists("assets/imgs/Check.png")): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" class="centradoBarra" style="width:15px;margin-left:358px;margin-top: 0px;margin-bottom: 0px;" alt="">
                                                    <?php endif ?>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                            <?php if (isset($logCargaBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logCargaBarra[$i]; ?>" alt="" style="width:390px;height:32px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/carga3.png' ?>" alt="" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif ?>

                                            <?php if ($ind_pasajero[$i] != "NO APLICA"): ?>
                                                <img src="<?php echo base_url().$logPasajeBarra[$i]; ?>" style="width:290px;height:35px;" class="imgFondo">
                                                <img src="<?php echo base_url().$logCajuelaBarra[$i]; ?>" style="width:95px;height:35px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/reducidos/condiciones/nc/pasajeros3_nc.jpg' ?>" alt="" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                </table>

                                <br><br>
                                <table style="width:100%;">
                                    <tr style="border-bottom: 1px solid gray;">
                                        <td align="left" style="border-bottom: 1px solid gray;">
                                            <br>
                                            <span style="color:#337ab7;font-size: 13px;font-weight: bold;">
                                                CONDICIONES DEL CAMINO.
                                            </labspanel>

                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;
                                            &nbsp;&nbsp;&nbsp;

                                            <span style="color:red; font-weight: bold;font-size: 10px;">
                                                N/A = NO APLCA
                                            </laspanbel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="border-bottom: 1px solid gray;">
                                            <label style="color:red;font-weight: bold;" align="right">
                                                N/A = NO APLCA
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="contenedorImgFondo">
                                            <?php if (isset($logEstrBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logEstrBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/estructura3.png' ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif; ?>

                                            <?php if (isset($logCamBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logCamBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/camino3.png' ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif; ?>

                                            <?php if (isset($logPenBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logPenBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/pendiente3.png' ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif; ?>

                                            <?php if (isset($logSupeBarra[$i])): ?>
                                                <img src="<?php echo base_url().$logSupeBarra[$i] ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php else: ?>
                                                <img src="<?php echo base_url().'assets/imgs/superficie3.png' ?>" style="width:390px;height:32px;" class="imgFondo">
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- Parte de abajo de la hoja -->
                        <tr>
                            <td colspan="2">
                                <table style="width:100%;border:1px solid gray;">
                                    <tr>
                                        <td style="width:34%;height: 80px;" align="justify">
                                            <span style="font-size:13px;color:#337ab7;">Sistema:</span>
                                            <p style="font-size:10px;color:black;text-align: justify;">
                                                <?php if(isset($sistema[$i])) echo $sistema[$i]; ?>
                                            </p>
                                        </td>
                                        <td style="width:34%;height: 80px;" align="justify">
                                            <span style="font-size:13px;color:#337ab7;">Componente:</span>
                                            <p style="font-size:10px;color:black;text-align: justify;">
                                                <?php if(isset($componente[$i])) echo $componente[$i]; ?>
                                            </p>
                                        </td>
                                        <td style="width:33%;height: 80px;" align="justify">
                                            <span style="font-size:13px;color:#337ab7;">Causa raíz:</span>
                                            <p style="font-size:10px;color:black;text-align: justify;">
                                                <?php if(isset($causaRaiz[$i])) echo $causaRaiz[$i]; ?>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <span style="font-size:13px;color:#337ab7;">Comentarios:</span>
                                            <p style="font-size:10px;color:black;text-align: justify;">
                                                <?php if($comentarios[$i] != "") echo $comentarios[$i]; else echo "Sin comentarios"; ?>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <h5 style="font-size:12px;color:black;">
                                    Diag. [<?= ($i+1) ." / ". count($comentarios) ?>]
                                </h5>
                            </td>
                        </tr>
                    </table>

                    <?php if ($i == (count($comentarios) -1)): ?>
                        <table class="table table-bordered" style="width:98%">
                            <tr>
                                <td align="center" style="color:black;font-size:9px;width: 40%;">
                                    <?php if (isset($firmaAsesor)): ?>
                                        <?php if ($firmaAsesor != ""): ?>
                                            <img src="<?php echo base_url().$firmaAsesor; ?>" style="width: 70px;height: 27px;">
                                        <?php endif ?>
                                    <?php endif; ?>
                                    <br>
                                    <p align="justify" style="text-align: center; font-size:10px;color:black;">
                                        <?php if (isset($nombreAsesor)): ?>
                                            <?php echo $nombreAsesor; ?>
                                        <?php endif; ?>
                                    </p>
                                </td>
                                <td style="width:5%;"></td>
                                <td align="center" style="color:black;font-size:9px;width: 40%;">
                                    <?php if (isset($firmaCliente)): ?>
                                        <?php if ($firmaCliente != ""): ?>
                                            <img src="<?php echo base_url().$firmaCliente; ?>" style="width: 70px;height: 27px;">
                                        <?php endif ?>
                                    <?php endif; ?>
                                    <br>
                                    <p align="justify" style="text-align: center; font-size:10px;color:black;">
                                        <?php if (isset($nombreCliente)): ?>
                                            <?php echo $nombreCliente; ?>
                                        <?php endif; ?>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="border-top: 1px solid #337ab7;font-size:12px;">
                                    Nombre y firma del asesor.<br>
                                </td>
                                <td style="width:5%;"></td>
                                <td align="center" style="border-top: 1px solid #337ab7;font-size:12px;">
                                    Nombre y firma del cliente.<br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="right">
                                    <br>
                                    <label style="color:  #a04000 ;font-size: 12px;">
                                        <?= ((isset($audio_eleccion)) ? $audio_eleccion : "") ?>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    <?php else: ?>
                        <div style="page-break-after: always"></div>
                    <?php endif ?>
                <?php endfor ?>
            <?php endif ?>    
        </div>
    </body>
</html>

<?php 
    try {
        $html = ob_get_clean(); 
        ob_clean();

        $mpdf = new \mPDF('utf-8', 'A4-P');
        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','debug' => TRUE]);
        
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        
        //$mpdf->AddPage();
        //$hoja_2 = '<br>';
        //$mpdf->WriteHTML($hoja_2);
        
        $mpdf->Output();

    //} catch (Html2PdfException $e) {
    } catch (Exception $e) {
        //$html2pdf->clean();
        //$formatter = new ExceptionFormatter($e);
        //echo $formatter->getHtmlMessage(); 
        echo "No se pudo cargar el PDF";
    }

 ?>