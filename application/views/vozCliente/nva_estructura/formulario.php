<div style="margin:30px;">
    <form id="formulario_2" method="post" autocomplete="on" enctype="multipart/form-data">
        <div class="panel-flotante">
            <div class="row">
                <div class="col-sm-2" style="padding-left: 16px;">
                    <?php if ($this->session->userdata('rolIniciado')): ?>
                        <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "ASETEC"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_AsesorBody";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "PCO"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Principal_CierreOrden/5";?>';">
                                Cerrar Sesión
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "GARANTIA"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Garantias_Panel/5";?>';">
                                Cerrar Sesión
                            </button>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>

                <div class="col-md-8" align="center">
                    <h3 class="h2_title_blue" style="text-align: center;">Identificación de necesidades de servicio.</h3>
                </div>

                <div class="col-md-2" align="right" id="contenedorAudio"> 
                    <input type="hidden" name="blockAudio_1" id="blockAudio_1" value="<?= set_value('blockAudio_1');?>">
                    <input type="hidden" name="blockAudio_2" id="blockAudio_2" value="<?= set_value('blockAudio_2');?>">
                    <input type="hidden" name="blockAudio_3" id="blockAudio_3" value="<?= set_value('blockAudio_3');?>">
                    <input type="hidden" name="blockAudio_4" id="blockAudio_4" value="<?= set_value('blockAudio_4');?>">
                    <input type="hidden" name="blockAudio_5" id="blockAudio_5" value="<?= set_value('blockAudio_5');?>">
                    <input type="hidden" name="blockAudio_6" id="blockAudio_6" value="<?= set_value('blockAudio_6');?>">
                    <input type="hidden" name="blockAudio_7" id="blockAudio_7" value="<?= set_value('blockAudio_7');?>">
                    <input type="hidden" name="blockAudio_8" id="blockAudio_8" value="<?= set_value('blockAudio_8');?>">
                    <input type="hidden" name="blockAudio_9" id="blockAudio_9" value="<?= set_value('blockAudio_9');?>">
                    <button type="button" class="btn btn-primary" style="margin-top: 20px;" id="iniciar" disabled>Iniciar</button> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6" align="center">
                    <br>
                </div>
                <div class="col-md-6" align="right">
                    <h6 style="text-align: right;color:darkblue;">
                        Indique si el cliente acepta o no que se grabe el audio del diagnóstico.
                    </h6>
                    <table style="width: 80%;">
                        <tr>
                            <td style="color:darkgreen;" align="center">
                                El cliente acepta que  <br> se grabe el audio
                            </td>
                            <td style="width: 0.5cm;" rowspan="2">
                                <br>
                            </td>
                            <td style="color:darkred;" align="center">
                                El cliente NO acepta que  <br> se grabe el audio
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <input type="radio" class="opcion_audio" style="transform: scale(1.5);" value="1" name="acepta_audio">
                            </td>
                            <td align="center">
                                <input type="radio" class="opcion_audio" style="transform: scale(1.5);" value="0" name="acepta_audio">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <h5 align="right" style="text-align:right;" class="error" id="textAudio"></h5>
        </div>
    </form>

    <br>
    <form id="formulario_3" method="post" autocomplete="on" enctype="multipart/form-data">
        <div class="panel-flotante">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                    </div>
                    <div class="alert alert-danger" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                    </div>
                    <div class="alert alert-warning" align="center" style="display:none;">
                        <strong style="font-size:20px !important;">Campos incompletos.</strong>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-1"><br></div>
                                <!-- Lado izquierdo de la pantalla -->
                                <div class="col-sm-10 table-responsive" style="width:100%;">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p>
                                                <h5>No.Orden Servicio:</h5>
                                                <input type="text" class="form-control" id="noOrden" name="idPivote" onblur="precarga()" value="<?php echo set_value('idPivote');?>"><br>

                                                <label for="" id="errorConsultaServer" class="error"></label>
                                                <span class="error" id="idPivote_error"></span>
                                            </p>

                                            <!--<br>
                                            Folio Intelisis:&nbsp;
                                            <label style="color:darkblue;">
                                                <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                                            </label>-->
                                        </div>
                                        <div class="col-sm-6">
                                            <p align="justify" style="text-align: justify;color:darkorange;font-size:13px;">
                                                * Los diagnósticos se guardaran individualmente con sus respectivos indicadores. Para finalizar el formulario, se deberan llenar las firmas y enviar el formulario.
                                            </p>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>Definición de falla.</h5>
                                            <textarea class="form-control" name="falla" rows="3" style="text-align:left;width:100%;"><?php echo set_value('falla');?></textarea>
                                            <span class="error" id="falla_error"></span>
                                        </div>
                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table style="width:100%;border: 1px solid gray;">
                                                <tr>
                                                    <td align="center">
                                                        <img src="<?php echo base_url().'/assets/imgs/vista.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                                    </td>
                                                    <td align="center">
                                                        <img src="<?php echo base_url().'/assets/imgs/tacto.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                                    </td>
                                                    <td align="center">
                                                        <img src="<?php echo base_url().'/assets/imgs/oido.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                                    </td>
                                                    <td align="center">
                                                        <img src="<?php echo base_url().'/assets/imgs/olfato.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <input type="checkbox" class="sentidos" style="transform: scale(1.5);" value="Vista" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Vista'); ?>>
                                                    </td>
                                                    <td align="center">
                                                        <input type="checkbox" class="sentidos" style="transform: scale(1.5);" value="Tacto" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Tacto'); ?>>
                                                    </td>
                                                    <td align="center">
                                                        <input type="checkbox" class="sentidos" style="transform: scale(1.5);" value="Oido" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Oido'); ?>>
                                                    </td>
                                                    <td align="center">
                                                        <input type="checkbox" class="sentidos" style="transform: scale(1.5);" value="Olfalto" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Olfalto'); ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <span class="error" id="sentidosFalla_error"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="width:50%;" align="center">
                                                        <br>
                                                        <!-- CANVAS -->
                                                        <canvas id="diagramaFallas" width="280" height="390" style="display:none;"></canvas>
                                                        <img id="diagramaTemp" src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:280px;height: 390px;">
                                                        <!-- CANVAS -->
                                                        <input type="hidden" name="fallasImg" id="fallasImg" value="<?= set_value('fallasImg');?>">
                                                        <input type="hidden" id="direccionFondo" value="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>">

                                                        <br>
                                                        <button type="button" class="btn btn-primary" id="resetoeDiagrama" style="margin-left: 20px;">
                                                            <i class="fa fa-refresh" aria-hidden="true"></i>
                                                            Restaurar diagrama
                                                        </button>
                                                    </td>
                                                    <td colspan="2">
                                                        <table style="border: 1px solid #ddd; width:90%;">
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td colspan="2" style="padding:5px 5px 5px 5px;">
                                                                    La falla se presenta cuando:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <span class="error" id="presentaFalla_error"></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Arranca el vehículo.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Arranque" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Arranque'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Inicia movimiento.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Inicio Movimiento" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Inicio Movimiento'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Disminuye la velocidad.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Menos Velocidad" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Menos Velocidad'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Da vuelta a la izquierda.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Vuelta Izquierda" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Vuelta Izquierda'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Da vuelta a la derecha.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Vuelta Derecha" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Vuelta Derecha'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Pasa un tope.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Tope" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Tope'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Pasa un bache.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Bache" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Bache'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Cambia la velocidad.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Cambio Velocidad" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Cambio Velocidad'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Está en movimiento.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Movimiento" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Movimiento'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Constantemente.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Constantemente" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Constantemente'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    Esporádicamente.
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="Esporádicamente" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Esporádicamente'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                    NO APLICA
                                                                </td>
                                                                <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                    <input type="checkbox" class="presenta" style="transform: scale(1.5);" value="NA" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'NA'); ?>>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4"><br><br></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <table style="width:100%;">
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td colspan="4" style="padding:5px 5px 5px 5px;">
                                                                    La falla se percibe en:
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" style="padding:5px 5px 5px 5px;">
                                                                    <span class="error" id="percibeFalla_error"></span>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Volante.
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Volante" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Volante'); ?>>
                                                                </td>
                                                                <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Cofre.
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Cofre" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cofre'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Asiento.
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Asiento" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Asiento'); ?>>
                                                                </td>
                                                                <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Cajuela.
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Cajuela" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cajuela'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Cristales.
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Cristales" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cristales'); ?>>
                                                                </td>
                                                                <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Toldo.
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Toldo" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Toldo'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Carrocería.
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Carrocería" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Carrocería'); ?>>
                                                                </td>
                                                                <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Debajo del vehículo.
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Debajo" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Debajo'); ?>>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td align="left" colspan="5" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Estancado:
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 1px solid #ddd;">
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Dentro.<br>
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Dentro" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Dentro'); ?>>
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Fuera.<br>
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Fuera" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Fuera'); ?>>
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Frente.<br>
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Frente" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Frente'); ?>>
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    Detrás.<br>
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="Detrás" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Detrás'); ?>>
                                                                </td>
                                                                <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                    NO APLICA<br>
                                                                    <input type="checkbox" class="percibe" style="transform: scale(1.5);" value="NA" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'NA'); ?>>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-1"><br></div>
                                <!-- Lado derecho de la pantalla -->
                                <div class="col-sm-10" style="width:100%;">
                                    <br>
                                    <table class="table-responsive" style="border: 1px solid black; width:100%;">
                                        <tr style="border-bottom: 1px solid black;">
                                            <td colspan="2" align="left">
                                                <h4 style="font-weight:bold;">Condiciones ambientales.</h4>
                                            </td>
                                            <td style="vertical-align: middle;" align="center">
                                                NO APLICA
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                                Temperatura<br>ambiente.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/metrix.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="temperatura_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td class="">
                                                <div id="medidor_1" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="tempAmbiente" id="tempAmbiente" value="-10">
                                                <br>
                                                <span class="error" id="temperatura_error"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                                Humedad.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/metrix_humedad.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="humedad_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td class="">
                                                <div id="medidor_2" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="humedad" id="humedad" value="<?php echo set_value("humedad"); ?>">
                                                <input type="hidden" name="humedadVal" id="humedadVal" value="">
                                                <br>
                                                <span class="error" id="humedad_error"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                                Viento.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/metrix_viento.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="viento_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_3" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="viento" id="viento" value="<?php echo set_value("viento"); ?>">
                                                <input type="hidden" name="vientoVal" id="vientoVal" value="">
                                                <br>
                                                <span class="error" id="viento_error"></span>
                                            </td>
                                        </tr>
                                    </table>

                                    <br>
                                    <table class="table-responsive" style="border: 1px solid black; width:100%;">
                                        <tr style="border-bottom: 1px solid black;">
                                            <td colspan="2" align="left">
                                                <h4 style="font-weight:bold;">Condiciones operativas.</h4>
                                            </td>
                                            <td style="vertical-align: middle;" align="center">
                                                NO APLICA
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                                Velocidad<br>frenado.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/velocidad2.png' ?>" alt="" style="width:50vw;height:40px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="frenado_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_4" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="frenoMedidor" id="frenado" value="">
                                                <br>
                                                <span class="error" id="frenado_error"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                                Cambio<br>
                                                Transmision.<br>
                                                4X2&nbsp;<input type="radio" value="2" name="transmision" <?php echo set_checkbox('transmision', '2'); ?>><br>
                                                4X4&nbsp;<input type="radio" value="4" name="transmision" <?php echo set_checkbox('transmision', '4'); ?>>
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/cambio2.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="transmision_1_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_5" class="centradoBarra" style="width:27.6vw;float: left;"></div>
                                                <div id="medidor_5_A" class="centradoBarra" style="width:17vw;float: left;margin-left: 5vw;"></div>
                                                <input type="hidden" name="cambio" id="cambio" value="<?php echo set_value("cambio"); ?>">
                                                <input type="hidden" name="cambioVal" id="cambioVal" value="">
                                                <input type="hidden" name="cambio_A" id="cambio_A" value="">
                                                <input type="hidden" name="cambio_AVal" id="cambio_AVal" value="">
                                                <br>
                                                <span class="error" id="transmision_error"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                                RPM x 1000
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/rpm2.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="rpm_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_6" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="rpm" id="rpm" value="">
                                                <input type="hidden" name="rpmVal" id="rpmVal" value="<?php echo set_value("rpmVal"); ?>">
                                                <br>
                                                <span class="error" id="rpm_error"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                                Carga.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/carga2.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="carga_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_7" class="centradoBarra" style="width:31vw;float: left;margin-top:8px;"></div>
                                                <input type="checkbox" value="1" name="remolque" style="float: left;margin-left: 11.7vw;transform: scale(1.5);" <?php echo set_checkbox('remolque', '1'); ?>>
                                                <input type="hidden" name="carga" id="carga" value="0">
                                                <br>
                                                <span class="error" id="carga_error"></span>
                                                <br>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                                Pasajeros.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/pasajeros2.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="pasajeros_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_8" class="centradoBarra" style="width:31.5vw;float: left;"></div>
                                                <div id="medidor_8_A" class="centradoBarra" style="width:14vw;float: left;margin-left: 4vw;"></div>
                                                <input type="hidden" name="pasajeros" id="pasajeros_2" value="<?php echo set_value("pasajeros"); ?>">
                                                <input type="hidden" name="pasajeros_2Val" id="pasajeros_2Val" value="<?php echo set_value("pasajeros_2Val"); ?>">
                                                <input type="hidden" name="cajuela_2" id="cajuela_2" value="<?php echo set_value("cajuela_2"); ?>">
                                                <br>
                                                <span class="error" id="pasajeros_error"></span>
                                            </td>
                                        </tr>
                                    </table>

                                    <br>
                                    <table class="table-responsive" style="border: 1px solid black; width:100%;">
                                        <tr style="border-bottom: 1px solid black;">
                                            <td colspan="2" align="left">
                                                <h4 style="font-weight:bold;">Condiciones del camino.</h4>
                                            </td>
                                            <td style="vertical-align: middle;" align="center">
                                                NO APLICA
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                                Estructura.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/estructura2.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="estructura_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_9" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="estructura" id="estructura" value="">
                                                <input type="hidden" name="estructuraVal" id="estructuraVal" value="">
                                                <br>
                                                <span class="error" id="estructura_error"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                                Camino.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/camino2.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="camino_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_10" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="camino" id="camino" value="">
                                                <input type="hidden" name="caminoVal" id="caminoVal" value="">
                                                <br>
                                                <span class="error" id="camino_error"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                                Pendiente.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/pendiente2.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="pendiente_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_11" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="pendiente" id="pendiente" value="">
                                                <input type="hidden" name="pendienteVal" id="pendienteVal" value="">
                                                <br>
                                                <span class="error" id="pendiente_error"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                                Superficie.
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/superficie2.png' ?>" alt="" style="width:50vw;height:30px;">
                                            </td>
                                            <td rowspan="2" style="vertical-align: middle;" align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" value="1" name="superficie_check">
                                            </td>
                                        </tr>
                                        <tr style="border-bottom: 1px solid black;">
                                            <td>
                                                <div id="medidor_12" class="centradoBarra" style="width:50vw;"></div>
                                                <input type="hidden" name="superficie" id="superficie" value="">
                                                <input type="hidden" name="superficieVal" id="superficieVal" value="">
                                                <br>
                                                <span class="error" id="superficie_error"></span>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <table style="width:100%;">
                                    <tr>
                                        <td colspan="3" align="center">
                                            <h3 class="h2_title_blue">Diagnóstico</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;" align="cneter">
                                            Sistema:<br>
                                            <textarea class="form-control" name="sistema" rows="4" style="text-align:left;width:100%;"><?php echo set_value('sistema');?></textarea>
                                            <span class="error" id="sistema_error"></span>
                                        </td>
                                        <td style="padding-left:20px;" align="cneter">
                                            Componente:<br>
                                            <textarea class="form-control" name="componente" rows="4" style="text-align:left;width:100%;"><?php echo set_value('componente');?></textarea>
                                            <span class="error" id="componente_error"></span>
                                        </td>
                                        <td style="padding-left:20px;" align="cneter">
                                            Causa raíz:<br>
                                            <textarea class="form-control" name="causa" rows="4" style="text-align:left;width:100%;"><?php echo set_value('causa');?></textarea>
                                            <span class="error" id="causa_error"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left:10px;" colspan="2">
                                            <br><br>
                                            Comentarios del diagnóstico:<br>
                                            <textarea class="form-control" name="comentario_diagnostico" rows="4" style="text-align:left;width:100%;"><?php echo set_value('comentario_diagnostico');?></textarea>
                                        </td>
                                        <td style="padding-left:20px;" align="cneter">
                                            <br>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" align="center">
                <div class="col-md-12" align="center">
                    <i class="fas fa-spinner cargaIcono" id="carga_1"></i>
                    <br>
                    <h4 id="formulario_error_diag"></h4>

                    <input type="button" class="btn btn-success" id="diagnostico" value="Guardar diagnostico">

                    <br>
                    <h5 class="error" style="text-align: left;">
                        **Nota: Para finalizar el formulario primero debe guardarse al menos un diagnóstico
                    </h5>
                </div>
            </div>
        </div>
    </form>

    <br>
    <form id="formulario" method="post" autocomplete="on" enctype="multipart/form-data">
        <div class="panel-flotante">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <table style="width:100%">
                                    <tr>
                                        <td align="center">
                                            Nombre y firma del asesor.<br>
                                            <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                                            <img class="marcoImg" src="<?= set_value('rutaFirmaAsesor');?>" id="firmaFirmaAsesor" alt="">
                                            <br><br>
                                            <input type="text" class="form-control asesorname" name="nombreAsesor" style="width:100%;" value="<?= set_value('nombreAsesor');?>">

                                            <span class="error" id="rutaFirmaAsesor_error"></span>
                                            <span class="error" id="nombreAsesor_error"></span>
                                        </td>
                                        <td rowspan="2" style="width: 1cm;">
                                            <br>
                                        </td>
                                        <td align="center">
                                            Nombre y firma del cliente.<br>
                                            <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?= set_value('rutaFirmaAcepta');?>">
                                            <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">
                                            <br><br>
                                            <input type="text" class="form-control nombreCliente" name="consumidor1Nombre" style="width:100%;" value="<?= set_value('consumidor1Nombre');?>">
                                            <span class="error" id="rutaFirmaAcepta_error"></span>
                                            <span class="error" id="consumidor1Nombre_error"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <br>
                                            <a class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                        </td>
                                        <td align="center">
                                            <br>
                                            <a class="cuadroFirma btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="row" align="center">
                <div class="col-md-12" align="center">
                    <i class="fas fa-spinner cargaIcono" id="carga_2"></i>
                    <br>
                    <h5 class="error" id="formulario_error"></h5>

                    <br>
                    <input type="button" class="btn btn-success" name="enviar" id="enviar" value="Guardar servicio" hidden>

                    <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="1">
                    <input type="hidden" name="id_cita" id="id_cita" value="">

                    <input type="hidden" name="asesorLogin" id="asesorLogin" value="<?php if(isset($asesor)) echo $asesor; ?>">
                    <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    <button type="button" class="btn btn-primary" id="terminar" >Detener grabación</button>
                </div>
            </div>
            <br>
        </div>
    </form>
</div>


<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                    <!-- <p id="limpiar">limpiar canvas</p> -->
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>