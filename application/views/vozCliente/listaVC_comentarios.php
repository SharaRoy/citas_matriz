<div style="margin: 20px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">VOZ CLIENTE</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-2" style="padding-top: 15px;">
                            <?php if ($this->session->userdata('rolIniciado')): ?>
                                <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                        Regresar
                                    </button>
                                <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                        Regresar
                                    </button>
                                <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                        Regresar
                                    </button>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>

                        <div class="col-md-5"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-5">
                            <!--<label for="" style="color:blue;font-size:14px;">Buscar voz cliente por fecha con el formato "dd-mm-aaaa"</label>-->
                            <br>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead> 
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <!--<td align="center" style="width: 10%;vertical-align:middle;"></td>-->
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>ORDEN</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>APERTURA ORDEN</strong></td>
                                    <td align="center" style="width: 20%;vertical-align:middle;"><strong>DETALLE FALLA</strong></td>
                                    <td align="center" style="width: 20%;vertical-align:middle;"><strong>COMENTARIO</strong></td>
                                    <td align="center" style="width: 20%;vertical-align:middle;"><strong>SISTEMA</strong></td>
                                    <td align="center" style="width: 20%;vertical-align:middle;"><strong>COMPONENTE</strong></td>
                                    <td align="center" style="width: 20%;vertical-align:middle;"><strong>CAUSA RAÍZ</strong></td>                                    
                                    <td align="center" style="width: 10%;" colspan="2"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idRegistro)): ?>
                                    <?php foreach ($idRegistro as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <!--<td align="center" style="width:10%;vertical-align:middle;">
                                                <?php echo $index+1; ?>                                                
                                            </td>-->
                                            <td align="center" style="width:15%;vertical-align:middle;">
                                                <?php echo $idOrden[$index]; ?>
                                                <input type="hidden" id="orden_<?php echo $index+1; ?>" value="<?php echo $idOrden[$index]; ?>">
                                                <input type="hidden" id="id_<?php echo $index+1; ?>" value="<?php echo $idRegistro[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?php echo $fechaCaptura[$index]; ?>
                                                <input type="hidden" id="fecha_<?php echo $index+1; ?>" value="<?php echo $fechaCaptura[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?php echo $definicionFalla[$index]; ?>
                                                <input type="hidden" id="falla_<?php echo $index+1; ?>" value="<?php echo $definicionFalla[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:10%;vertical-align:middle;">
                                                <?php echo $comentario[$index]; ?>
                                                <input type="hidden" id="comentario_<?php echo $index+1; ?>" value="<?php echo $comentario[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:15%;vertical-align:middle;">
                                                <?php echo $sistema[$index]; ?>
                                                <input type="hidden" id="sistema_<?php echo $index+1; ?>" value="<?php echo $sistema[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:15%;vertical-align:middle;">
                                                <?php echo $componente[$index]; ?>
                                                <input type="hidden" id="componente_<?php echo $index+1; ?>" value="<?php echo $componente[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:15%;vertical-align:middle;">
                                                <?php echo $causaRaiz[$index]; ?>
                                                <input type="hidden" id="causaRaiz2_<?php echo $index+1; ?>" value="<?php echo $causaRaiz[$index]; ?>">
                                                <input type="hidden" id="cliente_<?php echo $index+1; ?>" value="<?php echo $nombreCliente[$index]; ?>">
                                                <input type="hidden" id="asesor_<?php echo $index+1; ?>" value="<?php echo $nombreAsesor[$index]; ?>">
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($evidencia[$index] != ""): ?>
                                                    <a class="AudioModal" data-value="<?php echo $evidencia[$index]; ?>" data-orden="<?php echo $idOrden[$index]; ?>" data-target="#muestraAudio" data-toggle="modal" style="color:blue;font-size:24px;">
                                                        <i class="far fa-play-circle" style="font-size:24px;"></i>
                                                    </a>
                                                <?php else: ?>
                                                    <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <a class="btn btn-primary comentario" style="color:white;" data-id="<?php echo $index+1; ?>" data-target="#pregunta"  data-toggle="modal">
                                                    <!--Agregar <br> comentario-->
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            </td>

                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <!-- Si no existen registros que mostrar de ninguno de las dos tablas -->
                                <?php if ((!isset($idRegistro))): ?>
                                    <tr>
                                        <td colspan="8" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="pregunta" tabindex="-1" style="margin-top:40px;" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" align="center">
                <br><br>
                <h3>Información del registro:</h3>
                <p align="justify" style="text-align: justify; font-size:12px;margin-left: 30px;" >
                     <strong style="font-size:13px;">OS: </strong> <label style="font-size:11px;" id="infoRegistroScita1"></label><br>
                     <strong style="font-size:13px;">Comentario: </strong> <label style="font-size:11px;" id="infoRegistroScita2"></label><br>
                     <strong style="font-size:13px;">Cliente: </strong> <label style="font-size:11px;" id="infoRegistroScita3"></label><br>
                     <strong style="font-size:13px;">Asesor: </strong> <label style="font-size:11px;" id="infoRegistroScita4"></label><br>
                </p>

                <br><br>
                <div class="row">
                    <div class="col-sm-3" align="right">
                        <label for="idOrden">No. de orden a asignar:</label>
                    </div>
                    <div class="col-sm-6" align="left">
                        <input type="text" class="form-control" name="idOrden" id="idOrden" style="width:100%;">
                    </div>
                </div>

                <div class="row" style="font-size:13px;">
                    <div class="col-sm-6">
                        <h5>Comentarios:</h5>
                        <textarea rows='2' id='comentario' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                    <div class="col-sm-6">
                        <h5>Sistema:</h5>
                        <textarea rows='2' id='sistema' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                </div>

                <div class="row" style="font-size:13px;">
                    <div class="col-sm-6">
                        <h5>Componetes:</h5>
                        <textarea rows='2' id='componente' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                    <div class="col-sm-6">
                        <h5>Causa Raíz:</h5>
                        <textarea rows='2' id='causa' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>

                    </div>
                </div>

                <br>
                <div class="row" align="left">
                    <div class="col-sm-12" align="center">
                        <input type="button" onclick="envioComentario()" class="btn btn-success" value="Guardar">
                        <button type="button" onclick="actualiza()" class="btn btn-info" data-dismiss="modal" id="cancelar" name="cancelar">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para mostrar el audio de la voz cliente-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>
