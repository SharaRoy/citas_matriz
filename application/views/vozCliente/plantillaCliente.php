<section style="margin: 1cm;">
    <div class="row">
        <div class="col-sm-12">
            <table style="width: 100%;margin-left: 20px;">
                <tr>
                    <td width="20%">
                        <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="height:45px;"> 
                    </td>
                    <td style="width:60%;" colspan="2" align="center">
                        <strong>
                            <span class="color-blue" style="font-size:12px;color: #337ab7;" align="center">
                                <?= SUCURSAL ?>, S.A. DE C.V.
                            </span>
                        </strong>

                        <p class="justify" style="font-size:8px;" align="center">
                            <?=$this->config->item('encabezados_txt')?>
                        </p>
                    </td>
                    <td width="20%" align="" style="">
                        <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:100px;height:35px;"><br>
                    </td>
                </tr>
            </table>

            <br><br>
            <table style="width:100%;border: 1.5px solid darkblue;border-radius: 4px;">
                <tr style="border-bottom: 1.5px solid darkblue;color: #337ab7;">
                    <td colspan="6" style="vertical-align: middle;">
                        <h4 style="text-align: center;font-weight: bold;">
                           DATOS GENERALES DE LA ORDEN
                        </h4>
                        <div style="font-size: 16px;color:darkorange;width: 100%;" align="center">
                            Evidencia de audio: &nbsp;&nbsp;&nbsp;
                            <?php if (isset($evidencia)): ?>
                                <?php if ($evidencia != ""): ?>
                                    <a data-target="#muestraAudio" data-toggle="modal" style="color:darkorange;font-size:24px;">
                                        <i class="far fa-play-circle" style="font-size:24px;"></i>
                                    </a>
                                <?php else: ?>
                                    <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                                <?php endif; ?>
                            <?php else: ?>
                                <i class="fas fa-volume-mute" style="font-size:24px;color:red;"></i>
                            <?php endif ?>
                        </div>
                    </td>
                </tr>
                <tr style="border-bottom: 1.5px solid darkblue;">
                    <td style="width: 15%;padding-left: 6px;">
                        <label style="font-size: 13px;"><strong>NO. ORDEN: </strong></label>
                        <br>
                        <label style="font-size: 11px;color: darkblue;" class="formato">
                            <?php if(isset($id_cita)) echo $id_cita; else echo ""; ?>
                        </label>
                    </td>

                    <td style="width: 20%;">
                        <label style="font-size: 13px;"><strong>SERIE: </strong></label>
                        <br>
                        <label style="font-size: 11px;color: darkblue;" class="formato">
                            <?php if(isset($serie)) echo strtoupper($serie); else echo ""; ?>
                        </label>
                    </td>

                    <td style="width: 15%;">
                        <label style="font-size: 13px;"><strong>PLACAS: </strong></label>
                        <br>
                        <label style="font-size: 11px;color: darkblue;" class="formato">
                            <?php if(isset($placas)) echo strtoupper($placas); else echo ""; ?>
                        </label>
                    </td>

                    <td>
                        <label style="font-size: 13px;"><strong>UNIDAD: </strong></label>
                        <br>
                        <label style="font-size: 11px;color: darkblue;" class="formato">
                            <?php if(isset($categoria)) echo strtoupper($categoria); else echo ""; ?>
                        </label>
                    </td>

                    <td style="width: 30%;">
                        <label style="font-size: 13px;"><strong>MODELO: </strong></label>
                        <br>
                        <label style="font-size: 11px;color: darkblue;" class="formato">
                            <?php if(isset($modelo)) echo strtoupper($modelo); else echo ""; ?>
                        </label>
                    </td>

                    <td>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left: 6px;">
                        <label style="font-size: 13px;"><strong>TÉCNICO: </strong></label>
                        <br>
                        <label style="font-size: 11px;color: darkblue;" class="formato">
                            <?php if(isset($tecnico)) echo strtoupper($tecnico); else echo ""; ?>
                        </label>
                    </td>
   
                    <td colspan="2">
                        <label style="font-size: 13px;"><strong>ASESOR: </strong></label>
                        <br>
                        <label style="font-size: 11px;color: darkblue;" class="formato">
                            <?php if(isset($asesor)) echo strtoupper($asesor); else echo ""; ?>
                        </label>
                    </td>

                    <td colspan="2">
                        <label style="font-size: 13px;"><strong>CLIENTE: </strong></label>
                        <br>
                        <label style="font-size: 11px;color: darkblue;" class="formato">
                            <?php if(isset($nombreCliente)) echo strtoupper($nombreCliente); else echo ""; ?>
                        </label>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <!-- Primera parte lado izquierdo -->
        <div class="col-sm-6" style="border: 1.5px solid #337ab7; margin-top: 12px;">
            <h3 style="color:#337ab7;">Identificación de necesidades de servicio.</h3>

            <h5>Definición de falla.</h5>
            <p align="justify" style="text-align: justify; color:#337ab7;border: 1px solid gray;background-color:#ffffff;">
                <?php if (isset($definicionFalla)): ?>
                    <?php for ($x = 0; $x < count($definicionFalla); $x++): ?>
                        <?php echo $definicionFalla[$x]; ?>
                        <?php if ((($x+1) % 9) == 0): ?>
                            <br>
                        <?php endif; ?>
                    <?php endfor; ?>
                <?php endif; ?>
                <br>
            </p>

            <br>
            <table style="width:100%;">
                <tr>
                    <td align="center">
                        <img src="<?php echo base_url().'/assets/imgs/vista.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                    </td>
                    <td align="center">
                        <img src="<?php echo base_url().'/assets/imgs/tacto.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                    </td>
                    <td align="center">
                        <img src="<?php echo base_url().'/assets/imgs/oido.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                    </td>
                    <td align="center">
                        <img src="<?php echo base_url().'/assets/imgs/olfato.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <?php if (isset($sentidosFalla)): ?>
                            <?php if (in_array("Vista",$sentidosFalla)): ?>
                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center">
                        <?php if (isset($sentidosFalla)): ?>
                            <?php if (in_array("Tacto",$sentidosFalla)): ?>
                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center">
                        <?php if (isset($sentidosFalla)): ?>
                            <?php if (in_array("Oido",$sentidosFalla)): ?>
                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td align="center">
                        <?php if (isset($sentidosFalla)): ?>
                            <?php if (in_array("Olfalto",$sentidosFalla)): ?>
                                <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>

            <br>
            <div class="row">
                <div class="col-sm-6 table-responsive" align="center">
                    <?php if (isset($imgFallas)): ?>
                        <?php if ($imgFallas != ""): ?>
                            <img src="<?php echo base_url().$imgFallas; ?>" alt="" style="width:260px;height: 390px;">
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:260px;height: 390px;">
                        <?php endif ?>
                    <?php else: ?>
                        <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:260px;height: 390px;">
                    <?php endif; ?>
                </div>

                <div class="col-sm-6 table-responsive">
                    <table style="border: 1px solid #ddd; background-color:#ffffff;width: 100%;">
                        <tr>
                            <td colspan="2" style="border-bottom:1px solid #ddd;">
                                La falla se presenta cuando:
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Arranca el vehículo.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Arranque",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Inicia movimiento.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd;border-bottom:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Inicio Movimiento",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Disminuye la velocidad.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Menos Velocidad",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Da vuelta a la izquierda.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Vuelta Izquierda",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Da vuelta a la derecha.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Vuelta Derecha",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Pasa un tope.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Tope",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Pasa un bache.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Bache",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Cambia la velocidad.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Cambio Velocidad",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Está en movimiento.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Movimiento",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Constantemente.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Constantemente",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                Esporádicamente.
                            </td>
                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                <?php if (isset($presentaFalla)): ?>
                                    <?php if (in_array("Esporádicamente",$presentaFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </table>

                    <br><br>
                    <table style="width:100%;border: 1px solid #ddd;">
                        <tr>
                            <td colspan="4" style="border-bottom:1px solid #ddd;">La falla se percibe en:</td>
                        </tr>
                        <tr>
                            <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;">
                                Volante.
                            </td>
                            <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                                <?php if (isset($percibeFalla)): ?>
                                    <?php if (in_array("Volante",$percibeFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;">
                                Cofre.
                            </td>
                            <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                                <?php if (isset($percibeFalla)): ?>
                                    <?php if (in_array("Cofre",$percibeFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                Asiento.
                            </td>
                            <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                <?php if (isset($percibeFalla)): ?>
                                    <?php if (in_array("Asiento",$percibeFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                Cajuela.
                            </td>
                            <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                <?php if (isset($percibeFalla)): ?>
                                    <?php if (in_array("Cajuela",$percibeFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                Cristales.
                            </td>
                            <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                <?php if (isset($percibeFalla)): ?>
                                    <?php if (in_array("Cristales",$percibeFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                Toldo.
                            </td>
                            <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                <?php if (isset($percibeFalla)): ?>
                                    <?php if (in_array("Toldo",$percibeFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                Carrocería.
                            </td>
                            <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                <?php if (isset($percibeFalla)): ?>
                                    <?php if (in_array("Carrocería",$percibeFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                Debajo del vehículo.
                            </td>
                            <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                <?php if (isset($percibeFalla)): ?>
                                    <?php if (in_array("Debajo",$percibeFalla)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                                Estancado:
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="center" style="width:25%;">
                                            Dentro.<br>
                                            <?php if (isset($percibeFalla)): ?>
                                                <?php if (in_array("Dentro",$percibeFalla)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                            Fuera.<br>
                                            <?php if (isset($percibeFalla)): ?>
                                                <?php if (in_array("Fuera",$percibeFalla)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                            Frente.<br>
                                            <?php if (isset($percibeFalla)): ?>
                                                <?php if (in_array("Frente",$percibeFalla)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                            Detrás.<br>
                                            <?php if (isset($percibeFalla)): ?>
                                                <?php if (in_array("Detrás",$percibeFalla)): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" style="width:15px;" alt="">
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <br><br>
        </div>

        <!-- Primera parte lado derecho -->
        <div class="col-sm-6" style="padding: 12px 0px 0px 0px;">
            <table class="" style="border: 1px solid black; width: 100%;">
                <tr style="border-bottom: 1px solid black;">
                    <td align="center">
                        <h4 style="font-weight:bold;">Condiciones ambientales.</h4>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        Temperatura ambiente.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/metrix.png' ?>" alt="" style="height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td class="" style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_1" class="centradoBarra" style="width: 99%;"></div>
                        <input type="hidden" name="tempAmbiente" id="tempAmbiente" value="<?php if(set_value('tempAmbiente') != "") echo set_value('tempAmbiente'); else if(isset($logTempBarra)) echo $logTempBarra; ?>">
                        <?php echo form_error('tempAmbiente', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        Humedad.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/metrix_humedad.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td class="" style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_2" class="centradoBarra" style="min-width:370px;width: 99%;"></div>
                        <input type="hidden" name="humedad" id="humedad" value="<?php if(set_value('humedad') != "") echo set_value('humedad'); else if(isset($humedad)) echo $humedad; ?>">
                        <input type="hidden" name="humedadVal" id="humedadVal" value="<?php if(set_value('humedadVal') != "") echo set_value('humedadVal'); else if(isset($logHumBarra)) echo $logHumBarra; ?>">
                        <?php echo form_error('humedad', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        Viento.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/metrix_viento.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_3" class="centradoBarra" style="min-width:370px;width: 99%;"></div>
                        <input type="hidden" name="viento" id="viento" value="<?php if(set_value('viento') != "") echo set_value('viento'); else if(isset($viento)) echo $viento; ?>">
                        <input type="hidden" name="vientoVal" id="vientoVal" value="<?php if(set_value('vientoVal') != "") echo set_value('vientoVal'); else if(isset($logVientoBarra)) echo $logVientoBarra; ?>">
                        <?php echo form_error('viento', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
            </table>

            <br>
            <table class="" style="border: 1px solid black; width: 100%;">
                <tr style="border-bottom: 1px solid black;">
                    <td align="center">
                        <h4 style="font-weight:bold;">Condiciones operativas.</h4>
                    </td>
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        Velocidad frenado.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/velocidad2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_4" class="centradoBarra" style="width:99%;"></div>
                        <input type="hidden" name="frenoMedidor" id="frenado" value="<?php if(set_value('frenoMedidor') != "") echo set_value('frenoMedidor'); else if(isset($logVelBarra)) echo $logVelBarra; ?>">
                        <?php echo form_error('frenoMedidor', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        Cambio Transmisión: &nbsp;&nbsp;&nbsp;
                        4X2&nbsp;[<?php  if(isset($transmision)) if($transmision == "2") echo "<img src='".base_url().'assets/imgs/Check.png'."' style='width:15px;'>"; else echo '&nbsp;'; ?>]&nbsp;&nbsp;
                        4X4&nbsp;[<?php  if(isset($transmision)) if($transmision == "4") echo "<img src='".base_url().'assets/imgs/Check.png'."' style='width:15px;'>"; else echo '&nbsp;'; ?>]
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/cambio2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_5" class="centradoBarra" style="width:56%;float: left;"></div>
                        <div id="medidor_5_A" class="centradoBarra" style="width:33%;float: left;margin-left: 10%;"></div>

                        <input type="hidden" name="cambio" id="cambio" value="<?php if(set_value('cambio') != "") echo set_value('cambio'); else if(isset($cambio_A)) echo $cambio_A; ?>">
                        <input type="hidden" name="cambioVal" id="cambioVal" value="<?php if(set_value('cambioVal') != "") echo set_value('cambioVal'); else if(isset($logCam1Barra)) echo $logCam1Barra; ?>">

                        <input type="hidden" name="cambio_A" id="cambio_A" value="<?php if(set_value('cambio_A') != "") echo set_value('cambio_A'); else if(isset($operativa)) echo $operativa; ?>">
                        <input type="hidden" name="cambio_AVal" id="cambio_AVal" value="<?php if(set_value('cambio_AVal') != "") echo set_value('cambio_AVal'); else if(isset($logCam2Barra)) echo $logCam2Barra; ?>">

                        <?php echo form_error('cambio', '<span class="error">', '</span>'); ?>
                        <?php echo form_error('cambio_A', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        RPM x 1000
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/rpm2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_6" class="centradoBarra" style="width:99%;"></div>
                        <input type="hidden" name="rpm" id="rpm" value="<?php if(set_value('rpm') != "") echo set_value('rpm'); else if(isset($rmp)) echo $rmp; ?>">
                        <input type="hidden" name="rpmVal" id="rpmVal" value="<?php if(set_value('rpmVal') != "") echo set_value('rpmVal'); else if(isset($logRMPBarra)) echo $logRMPBarra; ?>">
                        <?php echo form_error('rpm', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        Carga.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/carga2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_7" class="centradoBarra" style="width:62%;float: left;margin-top:8px;"></div>
                        <input type="checkbox" value="1" name="remolque" style="float: left;margin-left: 23%;transform: scale(1.5);" <?php echo set_checkbox('remolque', '1'); ?> <?php  if(isset($remolque)) if($remolque == "1") echo "checked"; ?> disabled>
                        <input type="hidden" name="carga" id="carga" value="<?php if(set_value('carga') != "") echo set_value('carga'); else if(isset($logCargaBarra)) echo $logCargaBarra; ?>">
                        <?php echo form_error('carga', '<span class="error">', '</span>'); ?>
                        <br><br>
                    </td style="padding: 0px 10px 0px 10px;">
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        Pasajeros.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/pasajeros2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_8" class="centradoBarra" style="width:62%;float: left;"></div>
                        <div id="medidor_8_A" class="centradoBarra" style="width:28%;float: left;margin-left: 9%;"></div>

                        <input type="hidden" name="pasajeros" id="pasajeros_2" value="<?php if(set_value('pasajeros') != "") echo set_value('pasajeros'); else if(isset($pasajeros)) echo $pasajeros; ?>">
                        <input type="hidden" name="pasajeros_2Val" id="pasajeros_2Val" value="<?php if(set_value('pasajeros_2Val') != "") echo set_value('pasajeros_2Val'); else if(isset($logPasajeBarra)) echo $logPasajeBarra; ?>">

                        <input type="hidden" name="cajuela_2" id="cajuela_2" value="<?php if(set_value('cajuela_2') != "") echo set_value('cajuela_2'); else if(isset($logCajuelaBarra)) echo $logCajuelaBarra; ?>">
                        <?php echo form_error('pasajeros', '<span class="error">', '</span>'); ?>
                        <?php echo form_error('cajuela_2', '<span class="error">', '</span>'); ?>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
            </table>

            <br>
            <table class="" style="border: 1px solid black; width: 100%;">
                <tr style="border-bottom: 1px solid black;">
                    <td align="center">
                        <h4 style="font-weight:bold;">Condiciones del camino.</h4>
                    </td>
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        Estructura.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/estructura2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_9" class="centradoBarra" style="width:99%;"></div>
                        <input type="hidden" name="estructura" id="estructura" value="<?php if(set_value('estructura') != "") echo set_value('estructura'); else if(isset($estructura)) echo $estructura; ?>">
                        <input type="hidden" name="estructuraVal" id="estructuraVal" value="<?php if(set_value('estructuraVal') != "") echo set_value('estructuraVal'); else if(isset($logEstrBarra)) echo $logEstrBarra; ?>">
                        <?php echo form_error('estructura', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        Camino.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/camino2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_10" class="centradoBarra" style="width:99%;"></div>
                        <input type="hidden" name="camino" id="camino" value="<?php if(set_value('camino') != "") echo set_value('camino'); else if(isset($camino)) echo $camino; ?>">
                        <input type="hidden" name="caminoVal" id="caminoVal" value="<?php if(set_value('caminoVal') != "") echo set_value('caminoVal'); else if(isset($logCamBarra)) echo $logCamBarra; ?>">
                        <?php echo form_error('camino', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        Pendiente.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/pendiente2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_11" class="centradoBarra" style="width:99%;"></div>
                        <input type="hidden" name="pendiente" id="pendiente" value="<?php if(set_value('pendiente') != "") echo set_value('pendiente'); else if(isset($pendiente)) echo $pendiente; ?>">
                        <input type="hidden" name="pendienteVal" id="pendienteVal" value="<?php if(set_value('pendienteVal') != "") echo set_value('pendienteVal'); else if(isset($logPenBarra)) echo $logPenBarra; ?>">
                        <?php echo form_error('pendiente', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
                <tr style="">
                    <td align="left" style="padding: 0px 10px 0px 10px;">
                        Superficie.
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 10px 0px 10px;">
                        <img src="<?php echo base_url().'assets/imgs/superficie2.png' ?>" alt="" style="min-width:370px;height:30px;width: 100%;">
                    </td>
                </tr>
                <tr style="border-bottom: 1px solid black;">
                    <td style="padding: 0px 10px 0px 10px;">
                        <div id="medidor_12" class="centradoBarra" style="width:99%;"></div>
                        <input type="hidden" name="superficie" id="superficie" value="<?php if(set_value('superficie') != "") echo set_value('superficie'); else if(isset($superficie)) echo $superficie; ?>">
                        <input type="hidden" name="superficieVal" id="superficieVal" value="<?php if(set_value('superficieVal') != "") echo set_value('superficieVal'); else if(isset($logSupeBarra)) echo $logSupeBarra; ?>">
                        <?php echo form_error('superficie', '<span class="error">', '</span>'); ?>
                        <br>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br>
    <!-- Parte de abajo de la hoja -->
    <div class="row">
        <div class="col-sm-12">
            <table style="width: 100%;">
                <tr>
                    <td colspan="3" style="">
                        <br>
                        <h4 style="">Diagnóstico</h4>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table style="width:100%;border:1px solid gray;">
                            <tr>
                                <td style="padding-top: 0px;padding-bottom: 0px; width:33%;">
                                  <label style=""><u>Sistema:</u></label>
                                  <br>
                                  <p style="color:#337ab7;">
                                      <?php if (isset($sistema)): ?>
                                          <?php $sis = explode(" ",$sistema) ?>
                                          <?php for ($i = 0; $i < count($sis) ; $i++): ?>
                                              <?php if (($i % 8) == 0): ?>
                                                  <?php echo "<br>"; ?>
                                              <?php endif; ?>
                                              <?php echo $sis[$i]." "; ?>
                                          <?php endfor; ?>
                                      <?php endif; ?>
                                  </p>
                                </td>
                                <td style="">
                                    <label style=""><u>Componente:</u></label>
                                    <br>
                                    <p style="color:#337ab7;">
                                        <?php if (isset($componente)): ?>
                                            <?php $comp = explode(" ",$componente) ?>
                                            <?php for ($i = 0; $i < count($comp) ; $i++): ?>
                                                <?php if (($i % 8) == 0): ?>
                                                    <?php echo "<br>"; ?>
                                                <?php endif; ?>
                                                <?php echo $comp[$i]." "; ?>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                    </p>
                                </td>
                                <td style="padding-top: 0px;padding-bottom: 0px; width:33%;">
                                    <label style=""><u>Causa raíz:</u></label>
                                    <br>
                                    <p style="color:#337ab7;">
                                        <?php if (isset($causaRaiz)): ?>
                                            <?php $caus = explode(" ",$causaRaiz) ?>
                                            <?php for ($i = 0; $i < count($caus) ; $i++): ?>
                                                <?php if (($i % 8) == 0): ?>
                                                    <?php echo "<br>"; ?>
                                                <?php endif; ?>
                                                <?php echo $caus[$i]." "; ?>
                                            <?php endfor; ?>
                                        <?php endif; ?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12">
            <table style="width: 100%;">
                <tr>
                    <td colspan="6" style="border-bottom:1px solid gray;">
                        <h4 style="">Trabajos realizados.</h4>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                        Descripción de la operación/reparación.
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Cantidad.
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Costo unitario($).
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Costo refacciones($).
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Costo mano obra($).
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                        Subtotal
                    </td>
                </tr>
                <?php if (isset($trabajos)): ?>
                    <!-- Imprimimos los renglones guardados -->
                    <?php foreach ($trabajos as $index => $value): ?>
                        <tr>
                            <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;">
                                <?php echo $trabajos[$index][0]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <?php echo $trabajos[$index][1]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <?php echo "$ ".number_format($trabajos[$index][2],2); ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <?php echo "$ ".number_format($trabajos[$index][3],2); ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <?php echo "$ ".number_format($trabajos[$index][4],2); ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;border-right: 1px solid gray;" align="center">
                                <?php echo "$ ".number_format($trabajos[$index][5],2); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <!-- Completamos los renglones de la tabla (si hacen falta) -->
                    <?php if (count($trabajos) < 5): ?>
                        <?php for ($i = count($trabajos) ;$i < 5 ; $i++): ?>
                            <tr>
                                <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;border-right: 1px solid gray;" align="center">
                                    <br>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php for ($i = 0 ;$i < 5 ; $i++): ?>
                        <tr>
                            <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;border-right: 1px solid gray;" align="center">
                                <br>
                            </td>
                        </tr>
                    <?php endfor; ?>
                <?php endif; ?>
                <tr>
                    <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
                    <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                        SubTotal
                    </td>
                    <td align="center" style="width:14%;border:1px solid gray;color:#337ab7;">
                        <?php if (isset($subtoal)): ?>
                            <?php echo "$ ".number_format($subtoal,2); ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
                    <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                        I.V.A
                    </td>
                    <td align="center" style="width:14%;border:1px solid gray;color:#337ab7;">
                        <?php if (isset($iva)): ?>
                            <?php echo "$ ".number_format($iva,2); ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
                    <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                        Total
                    </td>
                    <td align="center" style="width:14%;border:1px solid gray;color:#337ab7;">
                        <?php if (isset($total)): ?>
                            <?php echo "$ ".number_format($total,2); ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="6"><br></td>
                </tr>
                <tr>
                    <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-top:1px solid gray;"></td>
                    <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-top:1px solid gray;" align="right">
                        Presupuesto inicial
                    </td>
                    <td align="center" style="width:14%;border:1px solid gray;color:#337ab7;">
                        <?php if (isset($presupuestos)): ?>
                            <?php if ($presupuestos[0] != ""): ?>
                                <?php echo "$ ".number_format($presupuestos[0],2); ?>
                            <?php else: ?>
                                <?php echo "$ 0" ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;"></td>
                    <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="right">
                        Adicionales
                    </td>
                    <td align="center" style="width:14%;border:1px solid gray;color:#337ab7;">
                        <?php if (isset($presupuestos)): ?>
                            <?php if ($presupuestos[1] != ""): ?>
                                <?php echo "$ ".number_format($presupuestos[1],2); ?>
                            <?php else: ?>
                                <?php echo "$ 0" ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;"></td>
                    <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="right">
                        Total autorizado
                    </td>
                    <td align="center" style="width:14%;border:1px solid gray;color:#337ab7;">
                        <?php if (isset($presupuestos)): ?>
                            <?php if ($presupuestos[2] != ""): ?>
                                <?php echo "$ ".number_format($presupuestos[2],2); ?>
                            <?php else: ?>
                                <?php echo "$ 0" ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12">
            <table style="width:100%;">
                <tr>
                    <td colspan="6" style="padding-top: 0px;padding-bottom: 0px;border-bottom:1px solid gray;">
                        <h4>Avisos / Notificaciones / Autorizaciones.</h4>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                        Asunto
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Persona contactada
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Fecha
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Hora
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Presupuesto/<br>Cambio de hora
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                        Autorizo(si/no)/<br>Acuerdo con el cliente
                    </td>
                </tr>
                <?php if (isset($avisos)): ?>
                    <!-- Imprimimos los renglones guardados -->
                    <?php foreach ($avisos as $index => $value): ?>
                        <tr>
                            <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;">
                                <?php echo $avisos[$index][0]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <?php echo $avisos[$index][1]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <?php echo $avisos[$index][2]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <?php echo $avisos[$index][3]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <?php echo $avisos[$index][4]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;border-right: 1px solid gray;" align="center">
                                <?php if ($avisos[$index][5] == "1"): ?>
                                    <?php echo "Si"; ?>
                                <?php else: ?>
                                    <?php echo "No"; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (count($avisos)<4): ?>
                        <?php for ($i = 0 ;$i < 3 ; $i++): ?>
                            <tr>
                                <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                    <br>
                                </td>
                                <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;border-right: 1px solid gray;" align="center">
                                    <br>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php for ($i = 0 ;$i < 3 ; $i++): ?>
                        <tr>
                            <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;" align="center">
                                <br>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:#337ab7;border-right: 1px solid gray;" align="center">
                                <br>
                            </td>
                        </tr>
                    <?php endfor; ?>
                <?php endif; ?>
            </table>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-sm-12">
            <table class="" style="width:98%">
                <tr>
                    <td style="width:25%;" rowspan="2">
                        <label for="">COMENTARIO:</label><br>
                        <p align="justify" style="text-align: justify; color:#337ab7;">
                            <?php if(isset($comentario)) echo $comentario; ?>
                        </p>
                    </td>
                    <td align="center"style="color:#337ab7;">
                        <?php if (isset($firmaAsesor)): ?>
                            <?php if ($firmaAsesor != ""): ?>
                                <img src="<?php echo base_url().$firmaAsesor; ?>" style='width:130px;height:40px;'>
                            <?php endif ?>
                        <?php endif; ?>
                        <br>
                        <label for="">
                            <?php if (isset($nombreAsesor)): ?>
                                <?= $nombreAsesor; ?>
                            <?php endif; ?>
                        </label>
                    </td>
                    <td style="width:5%;"></td>
                    <td align="center" style="color:#337ab7;">
                        <?php if (isset($firmaCliente)): ?>
                            <?php if ($firmaCliente != ""): ?>
                                <img class='marcoImg' src='<?= base_url() ?><?php if(isset($firmaCliente)) {if($firmaCliente != "") echo $firmaCliente; else echo 'assets/imgs/fondo_bco.jpeg';} else echo 'assets/imgs/fondo_bco.jpeg'; ?>' id='' style='width:130px;height:40px;'>
                            <?php else: ?>
                                <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">

                                <br>
                                <a class="cuadroFirma btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif; ?>
                        <?php else: ?>
                            <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">

                            <br>
                            <a class="cuadroFirma btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                        <?php endif; ?>

                        <br>
                        <label for="">
                            <?php if (isset($nombreCliente)): ?>
                                <?= $nombreCliente; ?>
                            <?php endif; ?>
                        </label>
                    </td>
                </tr>
                <tr>
                    <!-- <td style="width:15%;"></td> -->
                    <td align="center" style="border-top: 1px solid #337ab7;width:200px;">
                        <label for="">Nombre y firma del asesor.</label><br>
                    </td>
                    <td style="width:5%;"></td>
                    <td align="center" style="border-top: 1px solid #337ab7;width:200px;">
                        <label for="">Nombre y firma del cliente.</label><br>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <br><br>
    <?php if (isset($firmaCliente)): ?>
        <?php if ($firmaCliente == ""): ?>
            <form id="formulario_cliente" method="post" action="" autocomplete="on" enctype="multipart/form-data">
                <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?= set_value('rutaFirmaAcepta');?>">

                <input type="hidden" name="id_cita" id="" value="<?php if(isset($id_cita)) echo $id_cita; ?>">

                <br>
                <div class="row" align="center">
                    <div class="col-md-12"  align="center">
                        <h5>Comentarios:</h5>
                        <textarea rows='2' name='comentario' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>

                        <i class="fas fa-spinner cargaIcono"></i>
                        <span class="error" id ="formulario_error"></span>

                        <br>
                        <input type="button" class="btn btn-success" id="envio_diag" value="Guardar Firma">
                    </div>
                </div>
            </form>
        <?php endif ?>
    <?php endif ?>

    <input type="hidden" name="" id="cargaFormulario" value="1">
    <input type="hidden" name="" id="formulario" value="Revisión">

</section>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                    <!-- <p id="limpiar">limpiar canvas</p> -->
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para mostrar el audio de la voz cliente-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="<?php if(isset($evidencia)) echo base_url().$evidencia; ?>" controls="controls" type="audio/*" preload="preload" style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>