<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
 
    <head>
        <title>VOZ CLIENTE <?php if(isset($id_cita)) echo $id_cita;?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="estilos.css" />
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <style>
            @page { 
                sheet-size: A4;
                size: auto; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 5mm; 
                /*margin-top: 5mm; /* <any of the usual CSS values for margins> */
                /*margin-left: 5mm;*/
                /*margin-right: 5mm;*/
                /*margin-bottom: 10mm;*/
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 5mm; /* <any of the usual CSS values for margins> */
                margin-footer: 5mm; /* <any of the usual CSS values for margins> */
            }
        </style>

        <style media="screen">
            td,tr{
                padding: 0px 5px 5px 0px !important;
                font-size: 9px;
                color:#337ab7;
            }

            .centradoBarra{
                position: relative;
                max-height: 4px;
                /* margin-top: -12px; */
                margin-left: -30px;
                top: 1px;;
                left: 9px;
                /* transform: translate(-50%, -50%); */
            }

            .contenedorImgFondo{
                position: relative;
                display: inline-block;
                text-align: center;
            }

            .imgFondo{
                /* width: 300px; */
                /* height: 60px; */
                margin-top: -15px;
            }
        </style>
    </head>
    <body>
        <div>
            <table style="font-size:10px;margin-top:-13px;">
                <tr>
                    <!-- Primera parte lado izquierdo -->
                    <td>
                        <table style="">
                            <tr>
                                <td colspan="4">
                                    <h5 style="color:#337ab7;font-size: 16px;">
                                        Identificación de necesidades de servicio.
                                        <br>
                                        No. de Orden: <?php if(isset($id_cita)) echo $id_cita;?>
                                    </h5>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border: 1px solid gray;min-height:50px;height:110px;max-width: 200px; background-color:#ffffff;">
                                    <h6 style="color:#337ab7;font-size: 12px;">Definición de falla.</h6>
                                    <p align="justify" style="text-align: justify; font-size:12px;margin-bottom: 0px;color:black;">
                                        <!--<?php if (isset($definicionFalla)): ?>
                                            <?php echo $definicionFalla_txt; ?> 
                                        <?php endif; ?>-->
                                        <?php if (isset($definicionFalla_txt)): ?>
                                            <?php $diag = 1; ?>
                                            <?php $renglones = explode("DIAG",strtoupper($definicionFalla_txt)) ?>
                                                <?php for ($i = 0; $i < count($renglones); $i++): ?>
                                                    <?php if ($renglones[$i] != ""): ?>
                                                        <?php echo ($diag).".- DIAG".$renglones[$i]; ?>
                                                        <?php $diag++; ?>
                                                        <br>
                                                    <?php endif ?>
                                                <?php endfor ?> 
                                                <?php //echo str_replace("DIAG", "<br>DIAG", $definicionFalla_larga); ?>
                                        <?php endif; ?>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="background-color:#ffffff;" align="center">
                                    <table>
                                        <tr>
                                            <td align="center" style="width: 90px;">
                                                <?php if (file_exists("assets/imgs/vista.png")): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/vista.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">           
                                                <?php endif ?>                                  
                                            </td>
                                            <td align="center" style="width: 90px;">
                                                <?php if (file_exists("assets/imgs/tacto.png")): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/tacto.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">           
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="width: 90px;">
                                                <?php if (file_exists("assets/imgs/oido.png")): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/oido.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">           
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="width: 90px;">
                                                <?php if (file_exists("assets/imgs/olfato.png")): ?>
                                                    <img src="<?php echo base_url().'assets/imgs/olfato.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">           
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <?php if (isset($sentidosFalla)): ?>
                                                    <?php if (in_array("Vista",$sentidosFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center">
                                                <?php if (isset($sentidosFalla)): ?>
                                                    <?php if (in_array("Tacto",$sentidosFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center">
                                                <?php if (isset($sentidosFalla)): ?>
                                                    <?php if (in_array("Oido",$sentidosFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center">
                                                <?php if (isset($sentidosFalla)): ?>
                                                    <?php if (in_array("Olfalto",$sentidosFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <!--<td style="background-color:#ffffff;">
                                    <br>
                                </td> -->
                            </tr>
                            <tr>
                                <td colspan="2" style="background-color:#ffffff;" align="center">
                                    <?php if (isset($imgFallas)): ?>
                                        <?php if ($imgFallas != ""): ?>
                                            <img src="<?php echo base_url().$imgFallas; ?>" alt="" style="width:200px;height: 270px;">
                                        <?php else: ?>
                                            <?php if (file_exists("assets/imgs/cuadricula.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:200px;height: 270px;">
                                            <?php endif ?>                                    
                                        <?php endif ?>
                                    <?php else: ?>
                                        <?php if (file_exists("assets/imgs/cuadricula.png")): ?>
                                            <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:200px;height: 270px;">
                                        <?php endif ?>
                                    <?php endif; ?>
                                </td>
                                <td colspan="2">
                                    <table style="border: 1px solid #ddd; width:170px; background-color:#ffffff;">
                                        <tr>
                                            <td colspan="2" style="border-bottom:1px solid #ddd;font-size: 13px;color:#337ab7;">
                                                La falla se presenta cuando:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Arranca el vehículo.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Arranque",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Inicia movimiento.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd;border-bottom:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Inicio Movimiento",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Disminuye la velocidad.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Menos Velocidad",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Da vuelta a la izquierda.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Vuelta Izquierda",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Da vuelta a la derecha.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Vuelta Derecha",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Pasa un tope.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Tope",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Pasa un bache.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Bache",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Cambia la velocidad.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Cambio Velocidad",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Está en movimiento.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Movimiento",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Constantemente.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Constantemente",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:60%; border-bottom:1px solid #ddd;font-size: 11px;"  align="left">
                                                Esporádicamente.
                                            </td>
                                            <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                                <?php if (isset($presentaFalla)): ?>
                                                    <?php if (in_array("Esporádicamente",$presentaFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table style="width:100%;border: 1px solid #ddd;">
                            <tr>
                                <td colspan="4" style="border-bottom:1px solid #ddd;font-size: 14px;color:#337ab7;">
                                    La falla se percibe en:
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;font-size: 11px;">
                                    Volante.
                                </td>
                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                                    <?php if (isset($percibeFalla)): ?>
                                        <?php if (in_array("Volante",$percibeFalla)): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;font-size: 11px;">
                                    Cofre.
                                </td>
                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                                    <?php if (isset($percibeFalla)): ?>
                                        <?php if (in_array("Cofre",$percibeFalla)): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 11px;">
                                    Asiento.
                                </td>
                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                    <?php if (isset($percibeFalla)): ?>
                                        <?php if (in_array("Asiento",$percibeFalla)): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 11px;">
                                    Cajuela.
                                </td>
                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                    <?php if (isset($percibeFalla)): ?>
                                        <?php if (in_array("Cajuela",$percibeFalla)): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 11px;">
                                    Cristales.
                                </td>
                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                    <?php if (isset($percibeFalla)): ?>
                                        <?php if (in_array("Cristales",$percibeFalla)): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 11px;">
                                    Toldo.
                                </td>
                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                    <?php if (isset($percibeFalla)): ?>
                                        <?php if (in_array("Toldo",$percibeFalla)): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 11px;">
                                    Carrocería.
                                </td>
                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                    <?php if (isset($percibeFalla)): ?>
                                        <?php if (in_array("Carrocería",$percibeFalla)): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 11px;">
                                    Debajo del vehículo.
                                </td>
                                <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                                    <?php if (isset($percibeFalla)): ?>
                                        <?php if (in_array("Debajo",$percibeFalla)): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" colspan="4" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;font-size: 11px;">
                                    Estancado:
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table style="width:100%;">
                                        <tr>
                                            <td align="center" style="width:25%;">
                                                Dentro.<br>
                                                <?php if (isset($percibeFalla)): ?>
                                                    <?php if (in_array("Dentro",$percibeFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                Fuera.<br>
                                                <?php if (isset($percibeFalla)): ?>
                                                    <?php if (in_array("Fuera",$percibeFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                Frente.<br>
                                                <?php if (isset($percibeFalla)): ?>
                                                    <?php if (in_array("Frente",$percibeFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                                Detrás.<br>
                                                <?php if (isset($percibeFalla)): ?>
                                                    <?php if (in_array("Detrás",$percibeFalla)): ?>
                                                        <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                            <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" style="width:15px;height: 15px;">
                                                        <?php endif ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <!-- Primera parte lado derecho -->
                    <td colspan="2" align="center">
                        <table style="border: 1px solid gray; width:100%;">
                            <tr style="border-bottom: 1px solid gray;">
                                <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                                    <h5 style="color:#337ab7;font-size: 16px;">
                                        Condiciones ambientales.
                                    </h5>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logTempBarra)): ?>
                                        <img src="<?php echo base_url().$logTempBarra ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/metrix2.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 10px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logHumBarra)): ?>
                                        <img src="<?php echo base_url().$logHumBarra ?>" alt="" style="width:410px;height:35px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/metrix_humedad2.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logVientoBarra)): ?>
                                        <img src="<?php echo base_url().$logVientoBarra ?>" alt="" style="width:410px;height:35px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/metrix_viento2.png' ?>" alt="" style="width:410px;height:35px;" class="imgFondo">
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>

                        <table style="border: 1px solid gray; width:100%;">
                            <tr style="border-bottom: 1px solid gray;">
                                <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                                    <h5 style="color:#337ab7;font-size: 16px;">
                                        Condiciones operativas.
                                    </h5>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 10px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logVelBarra)): ?>
                                        <img src="<?php echo base_url().$logVelBarra ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/velocidad3.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 5px;border-bottom: 1px solid gray;" align="left" class="">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <?php if (isset($transmision_2)): ?>
                                                    <img src="<?php echo base_url().$transmision_2; ?>" alt="" style="width:55px;height:40px;">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url().'assets/imgs/reducidos/condiciones/cambio3_a.png'; ?>" alt="" style="width:55px;height:40px;float: left" class="">
                                                <?php endif ?>
                                            </td>
                                            <td>
                                                <?php if (isset($logCam1Barra)): ?>
                                                    <img src="<?php echo base_url().$logCam1Barra; ?>" alt="" style="width:230px;height:40px;">
                                                    <img src="<?php echo base_url().$logCam2Barra; ?>" alt="" style="width:105px;height:40px;">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url().'assets/imgs/reducidos/condiciones/cambio3_b.png'; ?>" alt="" style="width:230px;height:40px;float: left" class="">
                                                    <img src="<?php echo base_url().'assets/imgs/reducidos/condiciones/cambio3_c.png'; ?>" alt="" style="width:105px;height:40px;float: left" class="">
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 10px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logRMPBarra)): ?>
                                        <img src="<?php echo base_url().$logRMPBarra; ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/rpm3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($remolque)): ?>
                                        <?php if ($remolque == "1"): ?>
                                            <?php if (file_exists("assets/imgs/Check_mini.png")): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check_mini.png'; ?>" class="centradoBarra" style="width:15px;margin-left:358px;margin-top: 0px;margin-bottom: 0px;" alt="">
                                            <?php endif ?>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                    <?php if (isset($logCargaBarra)): ?>
                                        <img src="<?php echo base_url().$logCargaBarra; ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/carga3.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                                    <?php endif ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                                    <table>
                                        <tr>
                                            <td>
                                                <?php if (isset($logPasajeBarra)): ?>
                                                    <img src="<?php echo base_url().$logPasajeBarra; ?>" alt=""  style="width:300px;height:30px;" class="imgFondo">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url().'assets/imgs/reducidos/condiciones/pasajeros3_a.png' ?>" alt="" style="width:300px;height:30px;" class="imgFondo">
                                                <?php endif ?>
                                            </td>
                                            <td>
                                                <?php if (isset($logCajuelaBarra)): ?>
                                                    <img src="<?php echo base_url().$logCajuelaBarra; ?>" alt=""  style="width:110px;height:34px;position:absolute;" class="imgFondo">
                                                <?php else: ?>
                                                    <img src="<?php echo base_url().'assets/imgs/reducidos/condiciones/pasajeros3_b.png' ?>" alt="" style="width:110px;height:34px;position:absolute;" class="imgFondo">
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <table style="border: 1px solid gray; width:100%;">
                            <tr style="border-bottom: 1px solid gray;">
                                <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                                    <h5 style="color:#337ab7;font-size: 16px;">
                                        Condiciones del camino.
                                    </h5>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 10px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logEstrBarra)): ?>
                                        <img src="<?php echo base_url().$logEstrBarra ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/estructura3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 10px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logCamBarra)): ?>
                                        <img src="<?php echo base_url().$logCamBarra ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/camino3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 10px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logPenBarra)): ?>
                                        <img src="<?php echo base_url().$logPenBarra ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/pendiente3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                                    <?php if (isset($logSupeBarra)): ?>
                                        <img src="<?php echo base_url().$logSupeBarra ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/superficie3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Parte de abajo de la hoja -->
                <tr>
                    <td colspan="3" style="padding-top: 0px;padding-bottom: 0px;">
                        <h4 style="margin-top:-5px;margin-bottom:-10px;font-size: 16px;color:#337ab7;">
                            Diagnóstico
                        </h4>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table style="width:100%;border:1px solid gray;">
                            <tr>
                                <td style="width:34%;height: 80px;" align="justify">
                                    <h5 style="font-size:13px;color:#337ab7;">Sistema:</h5>
                                    <p style="font-size:11px;color:black;text-align: justify;">
                                        <?php if (isset($sistema)): ?>
                                            <?php echo $sistema ?>
                                        <?php endif; ?>
                                    </p>
                                </td>
                                <td style="width:34%;height: 80px;" align="justify">
                                    <h5 style="font-size:13px;color:#337ab7;">Componente:</h5>
                                    <p style="font-size:11px;color:black;text-align: justify;">
                                        <?php if (isset($componente)): ?>
                                            <?php echo $componente ?>
                                        <?php endif; ?>
                                    </p>
                                </td>
                                <td style="width:33%;height: 80px;" align="justify">
                                    <h5 style="font-size:13px;color:#337ab7;">Causa raíz:</h5>
                                    <p style="font-size:11px;color:black;text-align: justify;">
                                        <?php if (isset($causaRaiz)): ?>
                                            <?php echo $causaRaiz ?>
                                        <?php endif; ?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table style="width:100%;">
                <tr>
                    <td colspan="6" style="padding-top: 0px;padding-bottom: 0px;border-bottom:1px solid gray;">
                        <h4 style="margin-top:-10px;margin-bottom:-10px;font-size:14px;color:#337ab7;">Trabajos realizados.</h4>
                    </td>
                </tr>
                <tr>
                    <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;font-size:10px;color:#337ab7;">
                        Descripción de la operación/reparación.
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;font-size:10px;color:#337ab7;" align="center">
                        Cantidad.
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;font-size:10px;color:#337ab7;" align="center">
                        Costo unitario($).
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;font-size:10px;color:#337ab7;" align="center">
                        Costo refacciones($).
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;font-size:10px;color:#337ab7;" align="center">
                        Costo mano obra($).
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;font-size:10px;color:#337ab7;" align="center">
                        Subtotal
                    </td>
                </tr>
                <?php if (isset($trabajos)): ?>
                    <!-- Imprimimos los renglones guardados -->
                    <?php foreach ($trabajos as $index => $value): ?>
                        <tr>
                            <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;font-size: 9px;">
                                <?php echo $trabajos[$index][0]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;font-size: 9px;" align="center">
                                <?php echo $trabajos[$index][1]; ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;font-size: 9px;" align="center">
                                <?php echo "$ ".number_format($trabajos[$index][2],2); ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;font-size: 9px;" align="center">
                                <?php echo "$ ".number_format($trabajos[$index][3],2); ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;font-size: 9px;" align="center">
                                <?php echo "$ ".number_format($trabajos[$index][4],2); ?>
                            </td>
                            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;border-right: 1px solid gray;font-size: 9px;" align="center">
                                <?php echo "$ ".number_format($trabajos[$index][5],2); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <!-- Completamos los renglones de la tabla (si hacen falta) -->
                    <?php if (count($trabajos) < 5): ?>
                        <?php for ($i = count($trabajos) ;$i < 3 ; $i++): ?>
                            <tr>
                                <td style="font-size: 9px;padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;">
                                    <br>
                                </td>
                                <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                    <br>
                                </td>
                                <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                    <br>
                                </td>
                                <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                    <br>
                                </td>
                                <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                    <br>
                                </td>
                                <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;border-right: 1px solid gray;" align="center">
                                    <br>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php for ($i = 0 ;$i < 3 ; $i++): ?>
                        <tr>
                            <td style="font-size: 9px;padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;">
                                <br>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <br>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <br>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <br>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <br>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;border-right: 1px solid gray;" align="center">
                                <br>
                            </td>
                        </tr>
                    <?php endfor; ?>
                <?php endif; ?>
                <tr>
                    <td colspan="3" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
                    <td colspan="2" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                        SubTotal
                    </td>
                    <td align="center" style="font-size: 9px;width:14%;border:1px solid gray;color:black;">
                        <?php if (isset($subtoal)): ?>
                            <?php echo "$ ".number_format($subtoal,2); ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
                    <td colspan="2" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                        I.V.A
                    </td>
                    <td align="center" style="font-size: 9px;width:14%;border:1px solid gray;color:black;">
                        <?php if (isset($iva)): ?>
                            <?php echo "$ ".number_format($iva,2); ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
                    <td colspan="2" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                        Total
                    </td>
                    <td align="center" style="font-size: 9px;width:14%;border:1px solid gray;color:black;">
                        <?php if (isset($total)): ?>
                            <?php echo "$ ".number_format($total,2); ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="font-size: 5px;"><br></td>
                </tr>
                <tr>
                    <td colspan="3" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-top:1px solid gray;"></td>
                    <td colspan="2" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-top:1px solid gray;" align="right">
                        Presupuesto inicial
                    </td>
                    <td align="center" style="font-size: 9px;width:14%;border:1px solid gray;color:black;">
                        <?php if (isset($presupuestos)): ?>
                            <?php if ($presupuestos[0] != ""): ?>
                                <?php echo "$ ".number_format($presupuestos[0],2); ?>
                            <?php else: ?>
                                <?php echo "$ 0" ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;"></td>
                    <td colspan="2" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="right">
                        Adicionales
                    </td>
                    <td align="center" style="font-size: 9px;width:14%;border:1px solid gray;color:black;">
                        <?php if (isset($presupuestos)): ?>
                            <?php if ($presupuestos[1] != ""): ?>
                                <?php echo "$ ".number_format($presupuestos[1],2); ?>
                            <?php else: ?>
                                <?php echo "$ 0" ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;"></td>
                    <td colspan="2" style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="right">
                        Total autorizado
                    </td>
                    <td align="center" style="font-size: 9px;width:14%;border:1px solid gray;color:black;">
                        <?php if (isset($presupuestos)): ?>
                            <?php if ($presupuestos[2] != ""): ?>
                                <?php echo "$ ".number_format($presupuestos[2],2); ?>
                            <?php else: ?>
                                <?php echo "$ 0" ?>
                            <?php endif; ?>
                        <?php else: ?>
                            <?php echo "$ 0" ?>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>

            <!--<table style="font-size:12px;width:98%;">
                <tr>
                    <td colspan="6" style="padding-top: 0px;padding-bottom: 0px;border-bottom:1px solid gray;">
                        <h4 style="font-size:14px;color:#337ab7;">Avisos / Notificaciones / Autorizaciones.</h4>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:10px;color:#337ab7;padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                        Asunto
                    </td>
                    <td style="font-size:10px;color:#337ab7;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Persona contactada
                    </td>
                    <td style="font-size:10px;color:#337ab7;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Fecha
                    </td>
                    <td style="font-size:10px;color:#337ab7;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Hora
                    </td>
                    <td style="font-size:10px;color:#337ab7;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        Presupuesto/<br>Cambio de hora
                    </td>
                    <td style="font-size:10px;color:#337ab7;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                        Autorizo(si/no)/<br>Acuerdo con el cliente
                    </td>
                </tr>
                <?php if (isset($avisos)): ?>
                    <?php foreach ($avisos as $index => $value): ?>
                        <tr>
                            <td style="font-size: 9px;padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;">
                                <?php echo $avisos[$index][0]; ?>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <?php echo $avisos[$index][1]; ?>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <?php echo $avisos[$index][2]; ?>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <?php echo $avisos[$index][3]; ?>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <?php echo $avisos[$index][4]; ?>
                            </td>
                            <td style="font-size: 9px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;border-right: 1px solid gray;" align="center">
                                <?php if ($avisos[$index][5] == "1"): ?>
                                    <?php echo "Si"; ?>
                                <?php else: ?>
                                    <?php echo "No"; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (count($avisos)<4): ?>
                        <?php for ($i = 0 ;$i < 2 ; $i++): ?>
                            <tr>
                                <td style="font-size: 8px;padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;">
                                    <br>
                                </td>
                                <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                    <br>
                                </td>
                                <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                    <br>
                                </td>
                                <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                    <br>
                                </td>
                                <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                    <br>
                                </td>
                                <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;border-right: 1px solid gray;" align="center">
                                    <br>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php for ($i = 0 ;$i < 2 ; $i++): ?>
                        <tr>
                            <td style="font-size: 8px;padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;">
                                <br>
                            </td>
                            <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <br>
                            </td>
                            <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <br>
                            </td>
                            <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <br>
                            </td>
                            <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;" align="center">
                                <br>
                            </td>
                            <td style="font-size: 8px;width:14%;border-left:1px solid gray;border-bottom:1px solid gray;color:black;border-right: 1px solid gray;" align="center">
                                <br>
                            </td>
                        </tr>
                    <?php endfor; ?>
                <?php endif; ?>
            </table>-->

            <br>
            <table class="table table-bordered" style="width:98%">
                <tr>
                    <td style="width:25%;" rowspan="2" align="justify">
                        <h5 style="font-size:14px;color:#337ab7;">COMENTARIO:</h5>
                        <p align="justify" style="text-align: justify; font-size:10px;color:black;">
                            <?php if(isset($comentario)) echo $comentario; ?>
                        </p>
                    </td>
                    <td align="center" style="color:black;font-size:9px;">
                        <?php if (isset($firmaAsesor)): ?>
                            <?php if ($firmaAsesor != ""): ?>
                                <img src="<?php echo base_url().$firmaAsesor; ?>" alt="" width="70" height="20">    
                            <?php endif ?>
                        <?php endif; ?>
                        <br>
                        <p align="justify" style="text-align: justify; font-size:10px;color:black;">
                            <?php if (isset($nombreAsesor)): ?>
                                <?php echo $nombreAsesor; ?>
                            <?php endif; ?>
                        </p>
                    </td>
                    <td style="width:5%;"></td>
                    <td align="center" style="color:black;font-size:9px;">
                        <?php if (isset($firmaCliente)): ?>
                            <?php if ($firmaCliente != ""): ?>
                                <img src="<?php echo base_url().$firmaCliente; ?>" alt="" width="70" height="20">    
                            <?php endif ?>
                        <?php endif; ?>
                        <br>
                        <p align="justify" style="text-align: justify; font-size:10px;color:black;">
                            <?php if (isset($nombreCliente)): ?>
                                <?php echo $nombreCliente; ?>
                            <?php endif; ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <!-- <td style="width:15%;"></td> -->
                    <td align="center" style="border-top: 1px solid #337ab7;width:200px;font-size:9px;">
                        <label for="">Nombre y firma del asesor.</label><br>
                    </td>
                    <td style="width:5%;"></td>
                    <td align="center" style="border-top: 1px solid #337ab7;width:200px;font-size:9px;">
                        <label for="">Nombre y firma del cliente.</label><br>
                    </td>
                </tr>
            </table>
        </div>

    </body>
</html>

<?php 
    try {
        $html = ob_get_clean(); 
        ob_clean();

        $mpdf = new \mPDF('utf-8', 'A4-P');
        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','debug' => TRUE]);
        
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        
        //$mpdf->AddPage();
        //$hoja_2 = '<br>';
        //$mpdf->WriteHTML($hoja_2);
        
        $mpdf->Output();

    //} catch (Html2PdfException $e) {
    } catch (Exception $e) {
        //$html2pdf->clean();
        //$formatter = new ExceptionFormatter($e);
        //echo $formatter->getHtmlMessage(); 
        echo "No se pudo cargar el PDF";
    }

 ?>