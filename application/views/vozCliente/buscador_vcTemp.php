<div style="margin:20px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">REGISTROS VOZ CLIENTE SIN ORDEN PREVIA</h3>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-5">
                            <label for="" style="color:blue;font-size:14px;">Buscar en la tabla actual</label>
                            <br>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #eee;">
                                    <td align="center" style="width: 10%;"></td>
                                    <td align="center" style="width: 10%;"><strong>FOLIO</strong></td>
                                    <td align="center" style="width: 10%;"><strong>FECHA REGISTRO</strong></td>
                                    <td align="center" style="width: 20%;"><strong>DETALLE FALLA</strong></td>
                                    <td align="center" style="width: 20%;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 20%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idRegistro)): ?>
                                    <?php foreach ($idRegistro as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="width:10%;">
                                                <?php echo $index+1; ?>
                                                <input type="hidden" id="id_<?php echo $index+1; ?>" value="<?php echo $idRegistro[$index]; ?>">
                                                <input type="hidden" id="hora_<?php echo $index+1; ?>" value="<?php echo $limiteHoraReg[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $folio[$index]; ?>
                                                <input type="hidden" id="folio_<?php echo $index+1; ?>" value="<?php echo $folio[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $limite[$index]; ?>
                                                <input type="hidden" id="fecha_<?php echo $index+1; ?>" value="<?php echo $limite[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $falla[$index]; ?>
                                                <input type="hidden" id="vehiculo_<?php echo $index+1; ?>" value="<?php echo $falla[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $nombreConsumido1[$index]; ?>
                                                <input type="hidden" id="cliente_<?php echo $index+1; ?>" value="<?php echo $nombreConsumido1[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $nombreAsesor[$index]; ?>
                                                <input type="hidden" id="asesor_<?php echo $index+1; ?>" value="<?php echo $nombreAsesor[$index]; ?>">
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <a class="btn btn-primary asignar" style="color:white;" data-id="<?php echo $index+1; ?>" data-target="#pregunta"  data-toggle="modal">
                                                    Asignar
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <!-- Si no existen registros que mostrar de ninguno de las dos tablas -->
                                <?php if ((!isset($idRegistro))): ?>
                                    <tr>
                                        <td colspan="8" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar">
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="pregunta" tabindex="-1" style="margin-top:100px;" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" align="center">
                <br><br>
                <form name="" id="formulario" method="post" action="<?=base_url()."vozCliente/BuscadorVC/validateForm"?>">
                    <h3>Información del registro:</h3>
                    <p align="justify" style="text-align: justify; font-size:12px;margin-left: 30px;" >
                         <strong style="font-size:13px;">Folio: </strong> <label style="font-size:11px;" id="infoRegistroScita1"></label><br>
                         <strong style="font-size:13px;">Falla identificada: </strong> <label style="font-size:11px;" id="infoRegistroScita2"></label><br>
                         <strong style="font-size:13px;">Cliente: </strong> <label style="font-size:11px;" id="infoRegistroScita3"></label><br>
                         <strong style="font-size:13px;">Asesor: </strong> <label style="font-size:11px;" id="infoRegistroScita4"></label><br>
                         <strong style="font-size:13px;">Fecha de registro: </strong> <label style="font-size:11px;" id="infoRegistroScita5"></label><br>
                    </p>
                    <br>

                    <input type="hidden" name="idRegistro" id="idRegistro" value="">
                    <input type="hidden" name="folioTem" id="folioTem" value="">
                    <input type="hidden" name="fechReg" id="fechReg" value="">

                    <!-- <input type="hidden" name="horaLimite" id="horaLimite" value="<?php if(isset($limiteHora)) echo $limiteHora; ?>"> -->
                    <input type="hidden" name="horaRegistro" id="horaRegistro" value="">
                    <div class="row">
                        <div class="col-sm-6" align="right">
                            <label for="idOrden">No. de orden a asignar:</label>
                        </div>
                        <div class="col-sm-6" align="left">
                            <input type="text" class="form-control" name="idOrden" id="idOrden" style="width:100%;">
                        </div>
                    </div>

                    <br><br>
                    <div class="row" align="left">
                        <div class="col-sm-12" align="center">
                            <button type="submit" class="btn btn-info"  name="enviar">Guardar</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal" id="cancelar" name="cancelar">Cerrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
