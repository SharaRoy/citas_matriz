<div style="margin:1cm;">
    <form name="" id="formulario" method="post" action="<?=base_url()."vozCliente/VozCliente/validateForm"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-8" align="center">
                <h3 class="h2_title_blue">Identificación de necesidades de servicio.</h3>
            </div>

            <div class="col-md-2" align="right" id="contenedorAudio">
                <button type="button" class="btn btn-primary" id="iniciar" >Iniciar</button>
                <!-- <input type="hidden" name="blockAudio[]" id="blockAudio" value="<?php echo set_value("blockAudio"); ?>"> -->
                <input type="hidden" name="blockAudio_1" id="blockAudio_1" value="<?= set_value('blockAudio_1');?>">
                <input type="hidden" name="blockAudio_2" id="blockAudio_2" value="<?= set_value('blockAudio_2');?>">
                <input type="hidden" name="blockAudio_3" id="blockAudio_3" value="<?= set_value('blockAudio_3');?>">
                <input type="hidden" name="blockAudio_4" id="blockAudio_4" value="<?= set_value('blockAudio_4');?>">
                <input type="hidden" name="blockAudio_5" id="blockAudio_5" value="<?= set_value('blockAudio_5');?>">
                <input type="hidden" name="blockAudio_6" id="blockAudio_6" value="<?= set_value('blockAudio_6');?>">
                <input type="hidden" name="blockAudio_7" id="blockAudio_7" value="<?= set_value('blockAudio_7');?>">
                <input type="hidden" name="blockAudio_8" id="blockAudio_8" value="<?= set_value('blockAudio_8');?>">
            </div>

            <div class="col-sm-2">
                <?php if (isset($pOrigen)): ?>
                    <?php if ($pOrigen == "ASE"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($pOrigen == "TEC"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($pOrigen == "JDT"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                            Regresar
                        </button>
                    <?php else: ?>
                        <!--<button type="button" class="btn btn-dark" >
                            Sesión Expirada
                        </button>-->
                    <?php endif; ?>
                <?php else: ?>
                    <!--<button type="button" class="btn btn-dark" >
                        Sesión Expirada
                    </button>-->
                <?php endif; ?>
            </div>
        </div>

        <br>
            <h5 align="right" style="text-align:right;" class="error" id="textAudio"></h5>
        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="alert alert-warning" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Campos incompletos.</strong>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            <!-- Lado izquierdo de la pantalla -->
                            <div class="col-sm-6 table-responsive" style="width:100%;">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>
                                            <b>No.Orden Servicio:</b>
                                            <input type="text" class="input_field" id="noOrden" name="idPivote" onblur="precarga()" value="<?php echo set_value('idPivote');?>"><br>
                                            <label for="" id="errorConsultaServer" class="error"></label>
                                            <?php echo form_error('idPivote', '<br><span class="error">', '</span>'); ?>
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p align="justify" style="text-align: justify;color:blue;font-size:9px;">
                                            <strong>*</strong> Para llenar un formulario de Voz Cliente SIN Orden de Servicio previo, dejar el este campo en "0".
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6>Definición de falla.</h6>
                                        <textarea class="form-control" name="falla" rows="3" style="text-align:left;width:100%;"><?php echo set_value('falla');?></textarea>
                                        <?php echo form_error('falla', '<span class="error">', '</span>'); ?>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:5px;">
                                    <div class="col-md-12">
                                        <table style="width:100%;border: 1px solid gray;">
                                            <tr>
                                                <td align="center">
                                                    <img src="<?php echo base_url().'/assets/imgs/vista.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url().'/assets/imgs/tacto.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url().'/assets/imgs/oido.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url().'/assets/imgs/olfato.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <input type="checkbox" style="transform: scale(1.5);" value="Vista" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Vista'); ?>>
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" style="transform: scale(1.5);" value="Tacto" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Tacto'); ?>>
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" style="transform: scale(1.5);" value="Oido" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Oido'); ?>>
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" style="transform: scale(1.5);" value="Olfalto" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Olfalto'); ?>>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="width:50%;">
                                                    <br>
                                                    <!-- CANVAS -->
                                                    <canvas id="diagramaFallas" width="240" height="390" style="display:none;"></canvas>
                                                    <img id="diagramaTemp" src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:240px;height: 390px;">
                                                    <!-- CANVAS -->
                                                    <input type="hidden" name="fallasImg" id="fallasImg" value="<?= set_value('fallasImg');?>">
                                                    <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>">
                                                </td>
                                                <td colspan="2">
                                                    <table style="border: 1px solid #ddd; width:200px;">
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td colspan="2" style="padding:5px 5px 5px 5px;">
                                                                La falla se presenta cuando:
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Arranca el vehículo.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Arranque" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Arranque'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Inicia movimiento.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Inicio Movimiento" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Inicio Movimiento'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Disminuye la velocidad.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Menos Velocidad" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Menos Velocidad'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Da vuelta a la izquierda.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Vuelta Izquierda" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Vuelta Izquierda'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Da vuelta a la derecha.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Vuelta Derecha" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Vuelta Derecha'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Pasa un tope.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Tope" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Tope'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Pasa un bache.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Bache" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Bache'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Cambia la velocidad.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Cambio Velocidad" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Cambio Velocidad'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Está en movimiento.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Movimiento" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Movimiento'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Constantemente.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Constantemente" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Constantemente'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td style="width:60%;padding:5px 5px 5px 5px;" align="left">
                                                                Esporádicamente.
                                                            </td>
                                                            <td style="width:30%;border-left:1px solid #ddd;" align="center">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Esporádicamente" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Esporádicamente'); ?>>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><br><br></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <table style="width:100%;">
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td colspan="4" style="padding:5px 5px 5px 5px;">La falla se percibe en:</td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Volante.
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Volante" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Volante'); ?>>
                                                            </td>
                                                            <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Cofre.
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Cofre" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cofre'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Asiento.
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Asiento" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Asiento'); ?>>
                                                            </td>
                                                            <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Cajuela.
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Cajuela" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cajuela'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Cristales.
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Cristales" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cristales'); ?>>
                                                            </td>
                                                            <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Toldo.
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Toldo" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Toldo'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Carrocería.
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Carrocería" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Carrocería'); ?>>
                                                            </td>
                                                            <td align="left" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Debajo del vehículo.
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Debajo" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Debajo'); ?>>
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td align="left" colspan="4" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Estancado:
                                                            </td>
                                                        </tr>
                                                        <tr style="border: 1px solid #ddd;">
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Dentro.<br>
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Dentro" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Dentro'); ?>>
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Fuera.<br>
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Fuera" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Fuera'); ?>>
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Frente.<br>
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Frente" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Frente'); ?>>
                                                            </td>
                                                            <td align="center" style="padding:5px 5px 5px 5px;border-left:1px solid #ddd;">
                                                                Detrás.<br>
                                                                <input type="checkbox" style="transform: scale(1.5);" value="Detrás" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Detrás'); ?>>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- Lado derecho de la pantalla -->
                            <div class="col-sm-6" style="width:100%;">
                                <br>
                                <table class="table-responsive" style="border: 1px solid black; width:100%;">
                                    <tr style="border-bottom: 1px solid black;">
                                        <td colspan="2" align="left">
                                            <h4 style="font-weight:bold;">Condiciones ambientales.</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                            Temperatura<br>ambiente.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/metrix.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td class="">
                                            <div id="medidor_1" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="tempAmbiente" id="tempAmbiente" value="<?php echo set_value("tempAmbiente"); ?>">
                                            <?php echo form_error('tempAmbiente', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                            Humedad.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/metrix_humedad.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td class="">
                                            <div id="medidor_2" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="humedad" id="humedad" value="<?php echo set_value("humedad"); ?>">
                                            <input type="hidden" name="humedadVal" id="humedadVal" value="<?php echo set_value("humedadVal"); ?>">
                                            <?php echo form_error('humedad', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                            Viento.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/metrix_viento.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_3" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="viento" id="viento" value="<?php echo set_value("viento"); ?>">
                                            <input type="hidden" name="vientoVal" id="vientoVal" value="<?php echo set_value("vientoVal"); ?>">
                                            <?php echo form_error('viento', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                </table>

                                <br>
                                <table class="table-responsive" style="border: 1px solid black; width:100%;">
                                    <tr style="border-bottom: 1px solid black;">
                                        <td colspan="2" align="left">
                                            <h4 style="font-weight:bold;">Condiciones operativas.</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                            Velocidad<br>frenado.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/velocidad2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_4" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="frenoMedidor" id="frenado" value="<?php echo set_value("frenoMedidor"); ?>">
                                            <?php echo form_error('frenoMedidor', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                            Cambio<br>
                                            Transmision.<br>
                                            4X2&nbsp;<input type="radio" value="2" name="transmision" <?php echo set_checkbox('transmision', '2'); ?>><br>
                                            4X4&nbsp;<input type="radio" value="4" name="transmision" <?php echo set_checkbox('transmision', '4'); ?>>
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/cambio2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_5" class="centradoBarra" style="width:20vw;float: left;"></div>
                                            <div id="medidor_5_A" class="centradoBarra" style="width:11.5vw;float: left;margin-left: 4vw;"></div>
                                            <input type="hidden" name="cambio" id="cambio" value="<?php echo set_value("cambio"); ?>">
                                            <input type="hidden" name="cambioVal" id="cambioVal" value="<?php echo set_value("cambioVal"); ?>">
                                            <input type="hidden" name="cambio_A" id="cambio_A" value="<?php echo set_value("cambio_A"); ?>">
                                            <input type="hidden" name="cambio_AVal" id="cambio_AVal" value="<?php echo set_value("cambio_AVal"); ?>">
                                            <?php echo form_error('cambio', '<span class="error">', '</span>'); ?>
                                            <?php echo form_error('cambio_A', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                            RPM x 1000
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/rpm2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_6" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="rpm" id="rpm" value="<?php echo set_value("rpm"); ?>">
                                            <input type="hidden" name="rpmVal" id="rpmVal" value="<?php echo set_value("rpmVal"); ?>">
                                            <?php echo form_error('rpm', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                            Carga.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/carga2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_7" class="centradoBarra" style="width:22.5vw;float: left;margin-top:8px;"></div>
                                            <input type="checkbox" style="transform: scale(1.5);" value="1" name="remolque" style="float: left;margin-left: 8vw;" <?php echo set_checkbox('remolque', '1'); ?>>
                                            <input type="hidden" name="carga" id="carga" value="<?php echo set_value("carga"); ?>">
                                            <?php echo form_error('carga', '<span class="error">', '</span>'); ?>
                                            <br><br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                            Pasajeros.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/pasajeros2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_8" class="centradoBarra" style="width:22.7vw;float: left;"></div>
                                            <div id="medidor_8_A" class="centradoBarra" style="width:10vw;float: left;margin-left: 3vw;"></div>
                                            <input type="hidden" name="pasajeros" id="pasajeros_2" value="<?php echo set_value("pasajeros"); ?>">
                                            <input type="hidden" name="pasajeros_2Val" id="pasajeros_2Val" value="<?php echo set_value("pasajeros_2Val"); ?>">
                                            <input type="hidden" name="cajuela_2" id="cajuela_2" value="<?php echo set_value("cajuela_2"); ?>">
                                            <?php echo form_error('pasajeros', '<span class="error">', '</span>'); ?>
                                            <?php echo form_error('cajuela_2', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                </table>

                                <br>
                                <table class="table-responsive" style="border: 1px solid black; width:100%;">
                                    <tr style="border-bottom: 1px solid black;">
                                        <td colspan="2" align="left">
                                            <h4 style="font-weight:bold;">Condiciones del camino.</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                            Estructura.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/estructura2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_9" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="estructura" id="estructura" value="<?php echo set_value("estructura"); ?>">
                                            <input type="hidden" name="estructuraVal" id="estructuraVal" value="<?php echo set_value("estructuraVal"); ?>">
                                            <?php echo form_error('estructura', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                            Camino.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/camino2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_10" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="camino" id="camino" value="<?php echo set_value("camino"); ?>">
                                            <input type="hidden" name="caminoVal" id="caminoVal" value="<?php echo set_value("caminoVal"); ?>">
                                            <?php echo form_error('camino', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                            Pendiente.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/pendiente2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_11" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="pendiente" id="pendiente" value="<?php echo set_value("pendiente"); ?>">
                                            <input type="hidden" name="pendienteVal" id="pendienteVal" value="<?php echo set_value("pendienteVal"); ?>">
                                            <?php echo form_error('pendiente', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                            Superficie.
                                        </td>
                                        <td>
                                            <img src="<?php echo base_url().'assets/imgs/superficie2.png' ?>" alt="" style="width:36vw;height:30px;">
                                        </td>
                                    </tr>
                                    <tr style="border-bottom: 1px solid black;">
                                        <td>
                                            <div id="medidor_12" class="centradoBarra" style="width:36vw;"></div>
                                            <input type="hidden" name="superficie" id="superficie" value="<?php echo set_value("superficie"); ?>">
                                            <input type="hidden" name="superficieVal" id="superficieVal" value="<?php echo set_value("superficieVal"); ?>">
                                            <?php echo form_error('superficie', '<span class="error">', '</span>'); ?>
                                            <br>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <table style="width:100%;">
                                <tr>
                                    <td colspan="3" align="center">
                                        <h3 class="h2_title_blue">Diagnóstico</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px;" align="cneter">
                                        Sistema:<br>
                                        <textarea class="form-control" name="sistema" rows="4" style="text-align:left;width:100%;"><?php echo set_value('sistema');?></textarea>
                                        <?php echo form_error('sistema', '<span class="error">', '</span>'); ?>
                                    </td>
                                    <td style="padding-left:20px;" align="cneter">
                                        Componente:<br>
                                        <textarea class="form-control" name="componente" rows="4" style="text-align:left;width:100%;"><?php echo set_value('componente');?></textarea>
                                        <?php echo form_error('componente', '<span class="error">', '</span>'); ?>
                                    </td>
                                    <td style="padding-left:20px;" align="cneter">
                                        Causa raíz:<br>
                                        <textarea class="form-control" name="causa" rows="4" style="text-align:left;width:100%;"><?php echo set_value('causa');?></textarea>
                                        <?php echo form_error('causa', '<span class="error">', '</span>'); ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <!-- <div class="panel-heading" align="center">
                        <h4>Trabajos realizados.</h4>
                    </div> -->
                    <div class="panel-body">
                        <div class="col-md-12">
                            <table class="table table-bordered table-responsive" style="width:100%;">
                                <thead>
                                    <tr>
                                        <td colspan="2">
                                            <div class="panel-heading" align="center">
                                                <h5>Trabajos realizados.</h5>
                                            </div>
                                        </td>
                                        <td colspan="2" align="right">
                                            <label for="" class="error"> <strong>*</strong>Para grabar un registro, presione el boton de "+" </label>
                                            <br>
                                            <label for="" class="error"> <strong>*</strong>Para eliminar un registro, presione el boton de "-" </label>
                                        </td>
                                        <td align="center">
                                            <a id="agregar_btn" class="btn btn-success" style="color:white;">
                                                <i class="fas fa-plus"></i>
                                            </a>
                                            <!-- <input type="hidden" id="valoresD1" name="" value=""> -->
                                        </td>
                                        <td align="center">
                                            <a id="eliminar_btn" class="btn btn-danger" style="color:white; width: 1cm;display:none">
                                                <i class="fas fa-minus"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right:10px;width:50%;">
                                            Descripción de la operación/reparación.
                                        </td>
                                        <td style="width:10%;">
                                            Cantidad.
                                        </td>
                                        <td style="width:10%;">
                                            Costo unitario($).
                                        </td>
                                        <td style="width:10%;">
                                            Costo refacciones($).
                                        </td>
                                        <td style="width:10%;">
                                            Costo mano obra($).
                                        </td>
                                        <td style="width:10%;">
                                            Subtotal
                                            <input type="hidden" id="indiceProv" value="1">
                                        </td>
                                    </tr>
                                </thead>
                                <tbody id="trabajoLista">
                                    <tr id="fila_trabajo_1">
                                        <td>
                                            <input type='text' class='input_field' id='valorDefaultA_1' style='width:100%;text-align:left;'>
                                        </td>
                                        <td>
                                            <input type='number' class='input_field' id='valorDefaultB_1' min="1" onblur="cantidad()" value="1" style='width:100%;'>
                                        </td>
                                        <td>
                                            <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='valorDefaultC_1' value="0" onblur="costoSuma()" min="0" style='width:100%;'>
                                        </td>
                                        <td>
                                            <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='valorDefaultD_1' value="0" onblur="costoRefacc()" min="0" style='width:100%;'>
                                        </td>
                                        <td>
                                            <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='valorDefaultE_1' value="0" onblur="costoObra()" min="0" style='width:100%;'>
                                            <input type='hidden' id='valoresDD_1' name='valores[]'>
                                        </td>
                                        <td align="center">
                                            <label id='subtotalDD_1'>$ 0</label>
                                            <input type="hidden" name="" id="inputSubTotal_1" value="0">
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" align="right">
                                            SubTotal
                                        </td>
                                        <td align="center">
                                            <label id='subTotalGral'>$ 0</label>
                                            <input type="hidden" name="subTotalGralValue" id="subTotalGralImput" value="0">
                                        </td>
                                        <!-- <td colspan="1"></td> -->
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" align="right">
                                            I.V.A
                                        </td>
                                        <td align="center">
                                            <label id='ivaGral'>$ 0</label>
                                            <input type="hidden" name="ivaGralValue" id="ivaGralImput" value="0">
                                        </td>
                                        <!-- <td colspan="1"></td> -->
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" align="right">
                                            Total
                                        </td>
                                        <td align="center">
                                            <label id='totalGral'>$ 0</label>
                                            <input type="hidden" name="totalGralValue" id="totalGralImput" value="0">
                                        </td>
                                        <!-- <td colspan="1"></td> -->
                                    </tr>
                                    <tr>
                                        <td colspan="6"><br></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" align="right">
                                            Presupuesto inicial
                                        </td>
                                        <td align="center">
                                            <label id='subTotalGral'>$ 0</label>
                                            <input type="hidden" name="presupuestoIni" id="presupuestoIni" value="">
                                        </td>
                                        <!-- <td colspan="1"></td> -->
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" align="right">
                                            Adicionales
                                        </td>
                                        <td align="center">
                                            <label id='ivaGral'>$ 0</label>
                                            <input type="hidden" name="adicionales" id="adicionales" value="">
                                        </td>
                                        <!-- <td colspan="1"></td> -->
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                        <td colspan="2" align="right">
                                            Total autorizado
                                        </td>
                                        <td align="center">
                                            <label id='totalGral'>$ 0</label>
                                            <input type="hidden" name="totalAutorizado" id="totalAutorizado" value="">
                                        </td>
                                        <!-- <td colspan="1"></td> -->
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <table class="table table-bordered table-responsive" style="width:100%;">
                                <thead>
                                    <tr>
                                        <td colspan="3" align="center">
                                          <div class="panel-heading" align="center">
                                              <h5>Avisos / Notificaciones / Autorizaciones.</h5>
                                          </div>
                                        </td>
                                        <td colspan="2" align="right">
                                            <label for="" class="error"> <strong>*</strong>Para grabar un registro, presione el boton de "+" </label>
                                            <br>
                                            <label for="" class="error"> <strong>*</strong>Para eliminar un registro, presione el boton de "-" </label>
                                        </td>
                                        <td align="center" style="vertical-align:middle;">
                                            <a id="agregar" class="btn btn-success" style="color:white;">
                                                <i class="fas fa-plus"></i>
                                            </a>
                                            <!-- <input type="hidden" id="valoresD1" name="" value=""> -->
                                        </td>
                                        <td align="center" style="vertical-align:middle;">
                                            <a id="eliminar" class="btn btn-danger" style="color:white; width: 1cm;display:none">
                                                <i class="fas fa-minus"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="width:20%;">
                                            Asunto
                                        </td>
                                        <td align="center" style="width:20%;">
                                            Persona contactada
                                        </td>
                                        <td align="center" style="width:20%;">
                                            Fecha
                                        </td>
                                        <td align="center" style="width:20%;">
                                            Hora
                                        </td>
                                        <td align="center" style="width:20%;">
                                            Presupuesto/<br>Cambio de hora
                                        </td>
                                        <td align="center" colspan="2" style="width:10%;">
                                            Autorizo(si/no)/<br>Acuerdo con el cliente
                                            <input type="hidden" name="" id="indiceAvisos" value="1">
                                        </td>
                                    </tr>
                                </thead>
                                <tbody id="avisos_tabla">
                                    <tr id="fila_avisos_1">
                                        <td align="">
                                            <input type='text' class='input_field' id='asunto_1' style='width:100%;text-align:left;'>
                                        </td>
                                        <td align="">
                                            <input type='text' class='input_field' id='contacto_1' style='width:100%;text-align:left;'>
                                        </td>
                                        <td align="">
                                            <input type='date' class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" id='fecha_1' style='width:100%;text-align:left;'>
                                        </td>
                                        <td align="">
                                            <input type='time' class='input_field' id='hora_1' value="<?php if(isset($hora)) echo $hora; else echo "12:00"; ?>" style='width:100%;text-align:left;'>
                                        </td>
                                        <td align="">
                                            <input type='text' class='input_field' id='presupuesto_1' style='width:100%;text-align:left;'>
                                            <input type="hidden" name="avisosValues[]" id="avisosValores_1" value="">
                                        </td>
                                        <td align="center">
                                            Si  <input type="radio" value="1" name="autoriza_1" >
                                            <label for="" class="error" id="predet_1"></label>
                                        </td>
                                        <td align="center">
                                            No  <input type="radio" value="0" name="autoriza_1">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-12">
                            <table class="table table-bordered" style="width:100%">
                                <tr>
                                    <td align="center">
                                        Nombre y firma del asesor.<br>
                                        <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaAsesor');?>" id="firmaFirmaAsesor" alt="">
                                        <br><br>
                                        <input type="text" class="input_field asesorname" name="nombreAsesor" style="width:100%;" value="<?= set_value('nombreAsesor');?>">
                                        <?php echo form_error('rutaFirmaAsesor', '<span class="error">', '</span>'); ?><br>
                                        <?php echo form_error('nombreAsesor', '<span class="error">', '</span>'); ?><br>
                                    </td>
                                    <td align="center">
                                        Nombre y firma del cliente.<br>
                                        <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?= set_value('rutaFirmaAcepta');?>">
                                        <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">
                                        <br><br>
                                        <input type="text" class="input_field nombreCliente" name="consumidor1Nombre" style="width:100%;" value="<?= set_value('consumidor1Nombre');?>">
                                        <?php echo form_error('rutaFirmaAcepta', '<span class="error">', '</span>'); ?><br>
                                        <?php echo form_error('consumidor1Nombre', '<span class="error">', '</span>'); ?><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <br><br>
                                        <a class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                    </td>
                                    <td align="center">
                                        <br><br>
                                        <a class="cuadroFirma btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <div class="row" align="center">
            <div class="col-md-12" align="center">
                <input type="button" class="btn btn-success" name="enviar" id="enviar" value="Guardar servicio">
                <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="1">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="asesorLogin" id="asesorLogin" value="<?php if(set_value('asesorLogin') != "") echo set_value('asesorLogin'); elseif (isset($asesor)) echo $asesor;?>">
                <button type="button" class="btn btn-primary" id="terminar" >Detener grabación</button>
            </div>
        </div>
        <br>

    </form>
</div>


<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                    <!-- <p id="limpiar">limpiar canvas</p> -->
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
