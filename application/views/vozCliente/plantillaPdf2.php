<style media="screen">
    td,tr{
        padding: 0px 5px 5px 0px !important;
        font-size: 8px;
        color:#337ab7;
    }

    .centradoBarra{
        position: relative;
        height: 4px;
        /* margin-top: -12px; */
        margin-left: -30px;
        top: 1px;;
        left: 9px;
        /* transform: translate(-50%, -50%); */
    }

    .contenedorImgFondo{
        position: relative;
        display: inline-block;
        text-align: center;
    }

    .imgFondo{
        /* width: 300px; */
        /* height: 60px; */
        margin-top: -15px;
    }

</style>
<page backtop="5mm" backbottom="5mm" backleft="5mm" backright="5mm">
    <table style="font-size:8px;margin-top:-1cm;">
        <tr>
            <!-- Primera parte lado izquierdo -->
            <td>
                <table style="">
                    <tr>
                        <td colspan="4">
                            <h4> <strong>Identificación de necesidades <br>de servicio.</strong> </h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border: 1px solid gray;height:50px;background-color:#ffffff;">
                            <h6>Definición de falla.</h6>
                            <?php if (isset($definicionFalla)): ?>
                                <u><?php echo $definicionFalla ?></u>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color:#ffffff;">
                            <br><br>
                            <table>
                                <tr>
                                    <td align="center">
                                        <img src="<?php echo base_url().'/assets/imgs/vista.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">
                                    </td>
                                    <td align="center">
                                        <img src="<?php echo base_url().'/assets/imgs/tacto.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">
                                    </td>
                                    <td align="center">
                                        <img src="<?php echo base_url().'/assets/imgs/oido.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">
                                    </td>
                                    <td align="center">
                                        <img src="<?php echo base_url().'/assets/imgs/olfato.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <?php if (isset($sentidosFalla)): ?>
                                            <?php if (in_array("Vista",$sentidosFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($sentidosFalla)): ?>
                                            <?php if (in_array("Tacto",$sentidosFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($sentidosFalla)): ?>
                                            <?php if (in_array("Oido",$sentidosFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($sentidosFalla)): ?>
                                            <?php if (in_array("Olfalto",$sentidosFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color:#ffffff;"><br><br></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background-color:#ffffff;">
                            <br>
                            <?php if (isset($imgFallas)): ?>
                                <img src="<?php echo base_url().$imgFallas; ?>" alt="" style="width:130px;height: 210px;">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:130px;height: 210px;">
                            <?php endif; ?>
                        </td>
                        <td colspan="2">
                            <br>
                            <table style="border: 1px solid #ddd; width:160px; background-color:#ffffff;">
                                <tr>
                                    <td colspan="2" style="border-bottom:1px solid #ddd;">
                                        La falla se presenta cuando:
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Arranca el vehículo.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Arranque",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Inicia movimiento.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd;border-bottom:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Inicio Movimiento",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Disminuye la velocidad.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Menos Velocidad",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Da vuelta a la izquierda.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Vuelta Izquierda",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Da vuelta a la derecha.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Vuelta Derecha",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Pasa un tope.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Tope",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Pasa un bache.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Bache",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Cambia la velocidad.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Cambio Velocidad",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Está en movimiento.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Movimiento",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Constantemente.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Constantemente",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #ddd;"  align="left">
                                        Esporádicamente.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #ddd; border-bottom:1px solid #ddd;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Esporádicamente",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="width:280px;border: 1px solid #ddd;">
                    <tr>
                        <td colspan="4" style="border-bottom:1px solid #ddd;">La falla se percibe en:</td>
                    </tr>
                    <tr>
                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;">
                            Volante.
                        </td>
                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Volante",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;width:40%;">
                            Cofre.
                        </td>
                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;width:10%;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Cofre",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                            Asiento.
                        </td>
                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Asiento",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                            Cajuela.
                        </td>
                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Cajuela",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                            Cristales.
                        </td>
                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Cristales",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                            Toldo.
                        </td>
                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Toldo",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                            Carrocería.
                        </td>
                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Carrocería",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="left" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                            Debajo del vehículo.
                        </td>
                        <td align="center" style="border-left:1px solid #ddd; border-bottom:1px solid #ddd;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Debajo",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="border-left:1px solid #ddd;border-bottom:1px solid #ddd;">
                            Estancado:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                    <td align="center" style="width:25%;">
                                        Dentro.<br>
                                        <?php if (isset($percibeFalla)): ?>
                                            <?php if (in_array("Dentro",$percibeFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                        Fuera.<br>
                                        <?php if (isset($percibeFalla)): ?>
                                            <?php if (in_array("Fuera",$percibeFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                        Frente.<br>
                                        <?php if (isset($percibeFalla)): ?>
                                            <?php if (in_array("Frente",$percibeFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center" style="border-left:1px solid #ddd;width:25%;">
                                        Detrás.<br>
                                        <?php if (isset($percibeFalla)): ?>
                                            <?php if (in_array("Detrás",$percibeFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>

            <!-- Primera parte lado derecho -->
            <td colspan="2" align="center">
                <table style="border: 1px solid gray; width:100%;">
                    <tr style="border-bottom: 1px solid gray;">
                        <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                            <h5>Condiciones ambientales.</h5>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/metrix2.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                            <br>
                            <?php if (isset($logTempBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logTempBarra.'px;'; ?>;margin-left:50px;margin-top: -12px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/metrix_humedad2.png' ?>" alt="" style="width:410px;height:35px;" class="imgFondo">
                            <br>
                            <?php if (isset($logHumBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logHumBarra.'px;'; ?>;margin-left:50px;margin-top: -9px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/metrix_viento2.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logVientoBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logVientoBarra.'px;'; ?>;margin-left:50px;margin-top: -4px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="border: 1px solid gray; width:100%;">
                    <tr style="border-bottom: 1px solid gray;">
                        <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                            <h4>Condiciones operativas.</h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/velocidad3.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                            <br>
                            <?php if (isset($logVelBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logVelBarra.'px;'; ?>;margin-left:50px;margin-top: -15px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/cambio3.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                            <br>
                            <?php if (isset($logCam1Barra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logCam1Barra.'px;'; ?>;margin-left:50px;margin-top: -15px;">
                            <?php endif; ?>
                            <?php if (isset($logCam2Barra)): ?>
                                <br>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logCam2Barra.'px;'; ?>;margin-left:278px;margin-top: -10px;">
                            <?php endif; ?>
                            <?php if (isset($transmision)): ?>
                                <br>
                                <?php if ($transmision == "2"): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" class="centradoBarra" style="width:10px;height: 10px!important;margin-left:3px;margin-top: 5px;" alt="">
                                <?php elseif ($transmision == "4"): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" class="centradoBarra" style="width:10px;height: 10px!important;margin-left:30px;margin-top: 5px;" alt="">
                                    <!-- <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" class="centradoBarra" style="width:10px;height: 10px!important;margin-left:30px;margin-top: 5px;" alt=""> -->
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/rpm3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logRMPBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logRMPBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/carga3.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                            <br>
                            <?php if (isset($logCargaBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logCargaBarra.'px;'; ?>;margin-left:50px;margin-top: -15px;">
                            <?php endif; ?>
                            <?php if (isset($remolque)): ?>
                                <?php if ($remolque == "1"): ?>
                                    <br>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" class="centradoBarra" style="width:15px;height: 15px!important;margin-left:345px;margin-top: -12px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/pasajeros3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logPasajeBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" style="width:<?php echo $logPasajeBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                            <?php if (isset($logCajuelaBarra)): ?>
                                <br>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" style="width:<?php echo $logCajuelaBarra.'px;'; ?>;margin-left:295px;margin-top: -8px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="border: 1px solid gray; width:100%;">
                    <tr style="border-bottom: 1px solid gray;">
                        <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                            <h4>Condiciones del camino.</h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/estructura3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logEstrBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logEstrBarra.'px;'; ?>;margin-left:45px;margin-top: -8px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/camino3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logCamBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logCamBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 1px solid gray;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/pendiente3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logPenBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logPenBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/superficie3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logSupeBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logSupeBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- Parte de abajo de la hoja -->
        <tr>
            <td colspan="3" style="padding-top: 0px;padding-bottom: 0px;">
                <br>
                <h4 style="margin-top:-5px;margin-bottom:-10px;">Diagnóstico</h4>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table style="width:100%;border:1px solid gray;">
                    <tr>
                        <td style="padding-top: 0px;padding-bottom: 0px; width:33%;">
                            <h5 style="margin-top:-5px;margin-bottom:3px;height:60px;">Sistema:</h5>
                            <p style="font-size:8px;width:200px;">
                                <?php if (isset($sistema)): ?>
                                    <?php echo $sistema ?>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td style="padding-top: 0px;padding-bottom: 0px; width:33%;">
                            <h5 style="margin-top:-5px;margin-bottom:3px;height:60px;">Componente:</h5>
                            <p style="font-size:8px;width:200px;">
                                <?php if (isset($componente)): ?>
                                    <?php echo $componente ?>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td style="padding-top: 0px;padding-bottom: 0px; width:33%;">
                            <h5 style="margin-top:-5px;margin-bottom:3px;height:60px;">Causa raíz:</h5>
                            <p style="font-size:8px;width:200px;">
                                <?php if (isset($causaRaiz)): ?>
                                    <?php echo $causaRaiz ?>
                                <?php endif; ?>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>

    <br>
    <table style="font-size:10px;width:98%;">
        <tr>
            <td colspan="6" style="padding-top: 0px;padding-bottom: 0px;border-bottom:1px solid gray;">
                <h4 style="margin-top:-10px;margin-bottom:-10px;">Trabajos realizados.</h4>
            </td>
        </tr>
        <tr>
            <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                Descripción de la operación/reparación.
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                Cantidad.
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                Costo unitario($).
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                Costo refacciones($).
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                Costo mano obra($).
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                Subtotal
            </td>
        </tr>
        <?php if (isset($trabajos)): ?>
            <!-- Imprimimos los renglones guardados -->
            <?php foreach ($trabajos as $index => $value): ?>
                <tr>
                    <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                        <?php echo $trabajos[$index][0]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <?php echo $trabajos[$index][1]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <?php echo "$ ".$trabajos[$index][2]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <?php echo "$ ".$trabajos[$index][3]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <?php echo "$ ".$trabajos[$index][4]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                        <?php echo "$ ".$trabajos[$index][5]; ?>
                    </td>
                </tr>
            <?php endforeach; ?>

            <!-- Completamos los renglones de la tabla (si hacen falta) -->
            <!-- <?php if (count($trabajos) < 5): ?>
                <?php for ($i = count($trabajos) ;$i < 5 ; $i++): ?>
                    <tr>
                        <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                            <br>
                        </td>
                        <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                            <br>
                        </td>
                        <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                            <br>
                        </td>
                        <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                            <br>
                        </td>
                        <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                            <br>
                        </td>
                        <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                            <br>
                        </td>
                    </tr>
                <?php endfor; ?>
            <?php endif; ?> -->
        <?php else: ?>
            <?php for ($i = 0 ;$i < 5 ; $i++): ?>
                <tr>
                    <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                        <br>
                    </td>
                </tr>
            <?php endfor; ?>
        <?php endif; ?>
        <tr>
            <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
            <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                SubTotal
            </td>
            <td align="center" style="width:14%;border:1px solid gray;">
                <?php if (isset($subtoal)): ?>
                    <?php echo "$ ".$subtoal ?>
                <?php else: ?>
                    <?php echo "$ 0" ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
            <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                I.V.A
            </td>
            <td align="center" style="width:14%;border:1px solid gray;">
                <?php if (isset($iva)): ?>
                    <?php echo "$ ".$iva ?>
                <?php else: ?>
                    <?php echo "$ 0" ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;"></td>
            <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-left:1px solid gray;" align="right">
                Total
            </td>
            <td align="center" style="width:14%;border:1px solid gray;">
                <?php if (isset($total)): ?>
                    <?php echo "$ ".$total ?>
                <?php else: ?>
                    <?php echo "$ 0" ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td colspan="6"><br></td>
        </tr>
        <tr>
            <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-top:1px solid gray;"></td>
            <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-top:1px solid gray;" align="right">
                Presupuesto inicial
            </td>
            <td align="center" style="width:14%;border:1px solid gray;">
                <?php if (isset($presupuestos)): ?>
                    <?php if ($presupuestos[0] != ""): ?>
                        <?php echo "$ ".$presupuestos[0] ?>
                    <?php else: ?>
                        <?php echo "$ 0" ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php echo "$ 0" ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;"></td>
            <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="right">
                Adicionales
            </td>
            <td align="center" style="width:14%;border:1px solid gray;">
                <?php if (isset($presupuestos)): ?>
                    <?php if ($presupuestos[1] != ""): ?>
                        <?php echo "$ ".$presupuestos[1] ?>
                    <?php else: ?>
                        <?php echo "$ 0" ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php echo "$ 0" ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;"></td>
            <td colspan="2" style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="right">
                Total autorizado
            </td>
            <td align="center" style="width:14%;border:1px solid gray;">
                <?php if (isset($presupuestos)): ?>
                    <?php if ($presupuestos[2] != ""): ?>
                        <?php echo "$ ".$presupuestos[2] ?>
                    <?php else: ?>
                        <?php echo "$ 0" ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php echo "$ 0" ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>

    <br>
    <table style="font-size:10px;width:98%;">
        <tr>
            <td colspan="6" style="padding-top: 0px;padding-bottom: 0px;border-bottom:1px solid gray;" align="center">
                <label style="font-size:10px;">Avisos / Notificaciones / Autorizaciones.</label>
            </td>
        </tr>
        <tr>
            <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                Asunto
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                Persona contactada
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                Fecha
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                Hora
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                Presupuesto/<br>Cambio de hora
            </td>
            <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                Autorizo(si/no)/<br>Acuerdo con el cliente
            </td>
        </tr>
        <?php if (isset($avisos)): ?>
            <!-- Imprimimos los renglones guardados -->
            <?php foreach ($avisos as $index => $value): ?>
                <tr>
                    <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                        <?php echo $avisos[$index][0]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <?php echo $avisos[$index][1]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <?php echo $avisos[$index][2]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <?php echo $avisos[$index][3]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <?php echo $avisos[$index][4]; ?>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                        <?php if ($avisos[$index][5] == "1"): ?>
                            <?php echo "Si"; ?>
                        <?php else: ?>
                            <?php echo "No"; ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <?php for ($i = 0 ;$i < 3 ; $i++): ?>
                <tr>
                    <td style="padding-right:10px;width:30%;border-left:1px solid gray;border-bottom:1px solid gray;">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;" align="center">
                        <br>
                    </td>
                    <td style="width:14%;border-left:1px solid gray;border-bottom:1px solid gray;border-right: 1px solid gray;" align="center">
                        <br>
                    </td>
                </tr>
            <?php endfor; ?>
        <?php endif; ?>
    </table>

    <br>
    <table class="table table-bordered" style="width:98%">
        <tr>
            <td style="width:15%;"></td>
            <td align="center" >
                <?php if (isset($firmaAsesor)): ?>
                    <?php if ($firmaAsesor != ""): ?>
                        <img src="<?php echo base_url().$firmaAsesor; ?>" alt="" width="80" height="30">
                    <?php endif; ?>
                <?php endif; ?>
                <br>
                <label for=""><?php if(isset($nombreAsesor)) echo $nombreAsesor; ?></label>
            </td>
            <td style="width:15%;"></td>
            <td align="center">
              <?php if (isset($firmaCliente)): ?>
                  <?php if ($firmaCliente != ""): ?>
                      <img src="<?php echo base_url().$firmaCliente; ?>" alt="" width="80" height="30">
                  <?php endif; ?>
              <?php endif; ?>
              <br>
              <label for=""><?php if(isset($nombreCliente)) echo $nombreCliente; ?></label>
            </td>
        </tr>
        <tr>
            <td style="width:15%;"></td>
            <td align="center" style="border-top: 1px solid #337ab7;width:200px;">
                <label for="">Nombre y firma del asesor.</label><br>
            </td>
            <td style="width:15%;"></td>
            <td align="center" style="border-top: 1px solid #337ab7;width:200px;">
              <label for="">Nombre y firma del cliente.</label><br>
            </td>
        </tr>

    </table>

</page>
