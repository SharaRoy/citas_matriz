<style type="text/css">
    .titulo-1a{
        font-size: 17px;
        color: darkblue;
    }

    .titulo-1b{
        font-size: 12px;
        color: black;
        text-transform: uppercase;
        padding-right: 10px;
        vertical-align: middle;
        background-color: lavender;
    }

    .titulo-1c{
        font-size: 11px;
        color: darkblue;
        padding-left: 10px;
        vertical-align: middle;
        text-transform: uppercase;
    }

    table{
        width: 100%!important;
    }

    td{
        height: 30px;
        vertical-align: middle;
    }

    .cuadro_d{
        border-top: 1px solid darkblue;
        border-left: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_i{
        border-top: 1px solid darkblue;
        border-right: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_titulo{
        border: 2px solid darkblue;
        padding: 10px;
    }

    .cuadro_titulo_txt{
        border: 1px solid #98989A;
        text-align: justify;
        font-size: 13px;
    }

    .espacio_div{
        margin-top: 15px;
    }

    .cuadro_info{
        font-size: 13px; 
        font-weight: bold; 
        text-transform: uppercase;
        padding: 10px;
        background-color: lemonchiffon;
    }
</style>

<div style="margin:40px;">
    <div class="row espacio_div">
        <div class="col-sm-4" style="padding-left: 20px;">
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_soporte/".((isset($id_cita_url)) ? $id_cita_url : "");?>';">
                Regresar
            </button>
        </div>
    </div>

    <div class="espacio_div cuadro_titulo">
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="form-control" name="id_cita" id="id_cita" value="<?= ((isset($id_cita)) ? $id_cita : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="form-control" name="folio_externo" id="folio_externo" value="<?= ((isset($folio_externo)) ? $folio_externo : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="form-control" type="text" name="noSerie" id="noSerieText" value="<?= ((isset($serie)) ? $serie : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-sm-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="form-control" type="text" name="modelo" id="txtmodelo" value="<?= ((isset($vehiculo)) ? $vehiculo : '') ?>" style="width:100%;" disabled>
            </div>
        </div>
        
        <div class="row espacio_div">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="form-control" name="placas" id="placas" value="<?= ((isset($placas)) ? $placas : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-4">
                <label for="">Unidad</label>&nbsp;
                <input class="form-control" type="text" name="uen" id="uen" value="<?= ((isset($unidad)) ? $unidad : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-5">
                <label for="">Técnico</label>&nbsp;
                <input class="form-control" type="text" name="tecnico" id="tecnico" value="<?= ((isset($tecnico)) ? $tecnico : '') ?>" style="width:100%;" disabled>
            </div>
        </div>

        <div class="row espacio_div">
            <div class="col-md-4">
                <label for="">Asesor</label>&nbsp;
                <input class="form-control" type="text" name="asesors" id="asesors" value="<?= ((isset($asesor)) ? $asesor : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-8">
                <label for="">Cliente</label>&nbsp;
                <input class="form-control" type="text" name="cliente" id="cliente" value="<?= ((isset($cliente)) ? $cliente : '') ?>" style="width:100%;" disabled>
            </div>
        </div>
    </div>
        
    <hr>
    <?php if ($tipo_vista == "1"): ?>
        <div class="row espacio_div">
            <div class="col-sm-2"></div>
            <div class="col-sm-7">
                <h4 style="text-align: center;color: #800ab1; font-weight: bold;">
                    Reasignar diagnóstico a nuevo número de orden
                </h4>
            </div>
            <div class="col-sm-3" align="right">
                <a href="<?= base_url().'OrdenServicio_Revision/'.((isset($id_cita_url)) ? $id_cita_url : '0'); ?>" class='btn btn-warning' target="_blank" style='font-weight: bold;font-size: 13px;'>
                    Ver Orden de Servicio
                </a>
            </div>
        </div>
        <hr>
        <hr>

        <div class="row espacio_div">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 cuadro_titulo table-responsive">
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Nvo. Número de Orden
                        </td>
                        <td class="titulo-1c">
                            <input type="number" class="form-control" name="nvo_num_orden">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c">
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c">
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:100%;text-align:left;'></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Evidencias (opcional):
                        </td>
                        <td class="titulo-1c">
                            <input type="file" id="evidencias" class="form-control" name="archivo[]" accept="image/*" multiple style="width:80%;margin:20px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="error_envio_f"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <?php if ($existe_contrato == 0): ?>
                                <hr>
                                <label style="color: #ff8100;font-size: 12px;">
                                    <b>NOTA :</b> NO SE ENCONTRÓ UN CONTRATO PARA REASIGNAR, EL PROCESO SE ABORTO, VERIFIQUE LA EXISTENCIA DEL CONTRATO O RECUPERARLO EN LA OPCIÓN  <u>'Recuperar contrato y/o firmas'</u> PARA PODER CONTINUAR
                                </label>
                                <hr>
                            <?php endif ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="proceso_edicion_dig" type="button" class="btn btn-success">
                                REASIGNAR DIAGNÓSTICO
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php elseif ($tipo_vista == "2"): ?>
        <div class="row espacio_div">
            <div class="col-sm-2"></div>
            <div class="col-sm-7">
                <h4 style="text-align: center;color: #800ab1; font-weight: bold;">
                    Recuperar contrato y/o firmas del contrato
                </h4>
            </div>
            <div class="col-sm-3" align="right">
                <a href="<?= base_url().'OrdenServicio_Revision/'.((isset($id_cita_url)) ? $id_cita_url : '0'); ?>" class='btn btn-warning' target="_blank" style='font-weight: bold;font-size: 13px;'>
                    Ver Orden de Servicio
                </a>
            </div>
        </div>
        <hr>

        <div class="row espacio_div">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 cuadro_titulo table-responsive">
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c">
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c">
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:100%;text-align:left;'></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Evidencias (opcional):
                        </td>
                        <td class="titulo-1c">
                            <input type="file" id="evidencias" class="form-control" name="archivo[]" accept="image/*" multiple style="width:80%;margin:20px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="error_envio_f"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="proceso_edicion_dig" type="button" class="btn btn-success">
                                RECUPERAR CONTRATO
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    <?php elseif ($tipo_vista == "3"): ?>
        <div class="row espacio_div">
            <div class="col-sm-2"></div>
            <div class="col-sm-7">
                <h4 style="text-align: center;color: #800ab1; font-weight: bold;">
                    Eliminar Diagnóstico
                </h4>
            </div>
            <div class="col-sm-3" align="right">
                <a href="<?= base_url().'OrdenServicio_Revision/'.((isset($id_cita_url)) ? $id_cita_url : '0'); ?>" class='btn btn-warning' target="_blank" style='font-weight: bold;font-size: 13px;'>
                    Ver Orden de Servicio
                </a>
            </div>
        </div>
        <hr>
        
        <div class="row espacio_div">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 cuadro_titulo table-responsive">
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c">
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c">
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:100%;text-align:left;'></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Evidencias (opcional):
                        </td>
                        <td class="titulo-1c">
                            <input type="file" id="evidencias" class="form-control" name="archivo[]" accept="image/*" multiple style="width:80%;margin:20px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <hr>
                            <label style="color: #ff8100;font-size: 12px;">
                                [EL FORMULARIO NO SE ELIMINARA SOLO SE REASIGNARÁ UN NÚMERO DE ORDEN CON LA ABREVIATURA "DGE" POR POSIBLES ACLARACIONES]
                            </label>
                            <hr>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="error_envio_f"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="proceso_edicion_dig" type="button" class="btn btn-success">
                                ELIMINAR DIAGNÓSTICO
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php endif ?>

    <div class="row espacio_div">
        <div class="col-sm-4">
            <br>
        </div>
        <div class="col-sm-4"align="center">
            <i class="fas fa-spinner cargaIcono"></i>
            <br>
            <label style="font-size: 11px; text-transform: uppercase;" id="resultado_proceso"></label>

            <input type="hidden" name="origne_firma" value="">
            <input type="hidden" id="tipo_vista" value="<?= ((isset($tipo_vista)) ? $tipo_vista : '') ?>">
            <input type="hidden" id="orden_abierta" value="<?= ((isset($orden_abierta)) ? $orden_abierta : '') ?>">
            <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
            <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">
        </div>
        <div class="col-sm-4">
            <br>
        </div>
    </div>
</div>