<style type="text/css">
    .titulo-1a{
        font-size: 17px;
        color: darkblue;
    }

    .titulo-1b{
        font-size: 12px;
        color: black;
        text-transform: uppercase;
        padding-right: 10px;
        vertical-align: middle;
        background-color: lavender;
    }

    .titulo-1c{
        font-size: 11px;
        color: darkblue;
        padding-left: 10px;
        vertical-align: middle;
        text-transform: uppercase;
    }

    table{
        width: 100%!important;
    }

    td{
        height: 30px;
        vertical-align: middle;
    }

    .cuadro_d{
        border-top: 1px solid darkblue;
        border-left: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_i{
        border-top: 1px solid darkblue;
        border-right: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_titulo{
        border: 2px solid darkblue;
    }

    .cuadro_titulo_txt{
        border: 1px solid #98989A;
        text-align: justify;
        font-size: 13px;
    }

    .espacio_div{
        margin-top: 15px;
    }

    .cuadro_info{
        font-size: 13px; 
        font-weight: bold; 
        text-transform: uppercase;
        padding: 10px;
        background-color: lemonchiffon;
    }
</style>

<div style="margin:30px;">
    <div class="row espacio_div">
        <div class="col-sm-4" style="padding-left: 20px;">
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_soporte/".((isset($id_cita_url)) ? $id_cita_url : "");?>';">
                Regresar
            </button>
        </div>
    </div>

    <div class="espacio_div row">
        <div class="col-md-3">
            <label for="">No. Orden : </label>&nbsp;
            <input type="text" class="input_field" name="id_cita" id="id_cita" value="<?= ((isset($id_cita)) ? $id_cita : '') ?>" style="width:100%;">
        </div>
        <div class="col-md-3">
            <label for="">Orden Intelisis : </label>&nbsp;
            <input type="text" class="input_field" name="folio_externo" id="folio_externo" value="<?= ((isset($folio_externo)) ? $folio_externo : '') ?>" style="width:100%;">
        </div>
        <div class="col-md-3">
            <label for="">No.Serie (VIN):</label>&nbsp;
            <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?= ((isset($serie)) ? $serie : '') ?>" style="width:100%;" disabled>
        </div>
        <div class="col-sm-3">
            <label for="">Modelo:</label>&nbsp;
            <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?= ((isset($vehiculo)) ? $vehiculo : '') ?>" style="width:100%;">
        </div>
    </div>
    
    <div class="row espacio_div">
        <div class="col-md-3">
            <label for="">Placas</label>&nbsp;
            <input type="text" class="input_field" name="placas" id="placas" value="<?= ((isset($placas)) ? $placas : '') ?>" style="width:100%;">
        </div>
        <div class="col-md-3">
            <label for="">Unidad</label>&nbsp;
            <input class="input_field" type="text" name="uen" id="uen" value="<?= ((isset($unidad)) ? $unidad : '') ?>" style="width:100%;">
        </div>
        <div class="col-md-3">
            <label for="">Técnico</label>&nbsp;
            <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?= ((isset($tecnico)) ? $tecnico : '') ?>" style="width:100%;">
        </div>
        <div class="col-md-3">
            <label for="">Asesor</label>&nbsp;
            <input class="input_field" type="text" name="asesors" id="asesors" value="<?= ((isset($asesor)) ? $asesor : '') ?>" style="width:100%;">
        </div>
    </div>

    <div class="row table-responsive espacio_div">
        <div class="col-sm-1" align="right"></div>
        <div class="col-sm-10" align="right">
            <?php if (isset($ubicacion_proceso)): ?>
                <div class="alert alert-info" align="center">
                    <strong style="font-size:20px !important;">
                        <?php echo $ubicacion_proceso; ?>
                    </strong>
                </div>
            <?php else: ?>
                <div class="alert alert-info" align="center">
                    <strong style="font-size:20px !important;">
                        No se encontro el presupuesto
                    </strong>
                </div>
            <?php endif ?>
        </div>
        <div class="col-sm-1" align="right"></div>
    </div>

    <div class="row espacio_div">
        <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
            <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                <thead>
                    <tr style="background-color: #eee;">
                        <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            REP.
                        </td>
                        <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            CANTIDAD
                        </td>
                        <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            D E S C R I P C I Ó N
                        </td>
                        <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            NUM. PIEZA
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 10px;" align="center" colspan="3">
                            EXISTE
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            PRECIO REFACCIÓN
                        </td>
                        <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            HORAS
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            COSTO MO
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            TOTAL DE REPARACÓN
                        </td>
                        <td style=" display:none;width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            PRIORIDAD <br> REFACCION
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            PZA. GARANTÍA
                            <input type="hidden" name="indiceTablaMateria" id="indiceTablaMateria" value="1">
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            APRUEBA JEFE DE TALLER
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            APRUEBA ASESOR
                        </td>
                    </tr>
                    <tr style="background-color: #ddd;">
                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                            SI
                        </td>
                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                            NO
                        </td>
                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                            PLANTA
                        </td>
                    </tr>
                </thead>
                <tbody id="cuerpoMateriales">
                    <!-- Verificamos si existe la informacion -->
                    <?php if (isset($renglon)): ?>
                        <!-- Comprobamos que haya registros guardados -->
                        <?php if (count($renglon)>0): ?>
                            <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                <!-- Comprobamos que el renglon a mostrar tenga la aprovacion del jefe de taller -->
                                <tr id='fila_<?php echo $index+1; ?>'>
                                    <!-- REP -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <label style="color:darkblue;"><?php echo $referencia[$index]; ?></label>
                                    </td>

                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $cantidad[$index]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>

                                        <!-- Indice interno de la recaccion -->
                                        <input type="hidden" name="id_refaccion" id="id_<?php echo $index+1; ?>" value="<?php if(isset($id_refaccion[$index])) echo $id_refaccion[$index]; else echo ""; ?>">
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' id='descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $descripcion[$index]; ?></textarea>
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' id='pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $num_pieza[$index]; ?></textarea>
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaSi_<?php echo $index+1; ?>' class='input_field' name='existe_<?php echo $index+1; ?>' value="SI" <?php if($existe[$index] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaNo_<?php echo $index+1; ?>' class='input_field' name='existe_<?php echo $index+1; ?>' value="NO" <?php if($existe[$index] == "NO") echo "checked"; ?> style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaPlanta_<?php echo $index+1; ?>' class='input_field' name='existe_<?php echo $index+1; ?>' value="PLANTA" <?php if($existe[$index] == "PLANTA") echo "checked"; ?> style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        $<input type='number' step='any' min='0' class='input_field' id='costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_pieza[$index]; ?>' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' id='horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $horas_mo[$index]; ?>' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        $<input type='number' step='any' min='0' id='totalReng_<?php echo $index+1; ?>' class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_mo[$index]; ?>' style='width:90%;text-align:left;'>

                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <label id="total_renglon_label_<?php echo $index+1; ?>" style="color:darkblue;">$<?php echo number_format($total_refacion[$index],2); ?></label>
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;display: none;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' id='prioridad_<?php echo $index+1; ?>' name='prioridad_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $prioridad[$index]; ?>' style='width:70%;text-align:center;'>
                                    </td>
                                    <td style='border:1px solid #337ab7;background-color: <?= (($pza_garantia[$index] == "1") ? "#a2d9ce" : "#f5b7b1") ?>;' align='center'>
                                        <?php if ($pza_garantia[$index] == "1"): ?>
                                            SI
                                        <?php else: ?>
                                            NO
                                        <?php endif ?>

                                    </td>
                                    <td style='border:1px solid #337ab7; background-color: <?= (($autoriza_jefeTaller[$index] == "1") ? "#a2d9ce" : "#f5b7b1") ?>;' align='center'>
                                        <?php if ($autoriza_jefeTaller[$index] == "1"): ?>
                                            SI
                                        <?php else: ?>
                                            NO
                                        <?php endif ?>
                                    </td>
                                    <td style='border:1px solid #337ab7; background-color: <?= (($autoriza_asesor[$index] == "1") ? "#a2d9ce" : "#f5b7b1") ?>;' align='center'>
                                        <?php if ($autoriza_asesor[$index] == "1"): ?>
                                            SI
                                        <?php else: ?>
                                            NO
                                        <?php endif ?>

                                        <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $renglon[$index]; ?>">
                                    </td>
                                </tr>
                            <?php endfor; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="14" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="14" align="center">
                                No se cargaron los datos
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" align="right">
                            <label>SUB-TOTAL</label>
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="subTotalMaterialLabel"><?php if(isset($subtotal)) echo number_format($subtotal,2); else echo "0"; ?></label>
                            <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subtotal)) echo $subtotal; else echo "0"; ?>">
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="7" rowspan="3">
                            <?= ((isset($sin_aprobar_jdt)) ? (($sin_aprobar_jdt > 0) ? "Faltaron ". $sin_aprobar_jdt ." refaccione(s) por aprobar de la revisión del jefe de taller" : "") : "") ?>
                        </td>
                        <td colspan="3" align="right" class="error">
                            <label>CANCELADO</label>
                        </td>
                        <td colspan="1" align="center" class="error">
                            $<label id="canceladoMaterialLabel"><?php if(isset($cancelado)) echo number_format($cancelado,2); else echo "0"; ?></label>
                            <input type="hidden" name="canceladoMaterial" id="canceladoMaterial" value="<?php if(isset($cancelado)) echo $cancelado; else echo "0"; ?>">
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            <label>I.V.A.</label>
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="ivaMaterialLabel"><?php if(isset($iva)) echo number_format($iva,2); else echo "0"; ?></label>
                            <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($iva)) echo $iva; else echo "0"; ?>">
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="3" align="right">
                            <label>PRESUPUESTO TOTAL</label>
                        </td>
                        <td colspan="1" align="center">
                            $<label id="presupuestoMaterialLabel"><?php if(isset($subtotal)) echo number_format((($subtotal-$cancelado)*1.16),2); else echo "0"; ?></label>
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr style="border-top:1px solid #337ab7;">
                        <td colspan="8" rowspan="2">
                            <label for="" id="anticipoNota" style="color:blue;">Nota anticipo: <?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                        </td>
                        <td colspan="2" align="right">
                            <label>ANTICIPO</label>
                        </td>
                        <td align="center">
                            $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                            <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <label>TOTAL</label>
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="totalMaterialLabel"><?php if(isset($total)) echo number_format($total,2); else echo "0"; ?></label>
                            <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($total)) echo $total; else echo "0"; ?>">
                        </td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="10" align="right">
                            FIRMA DEL ASESOR QUE APRUEBA
                        </td>
                        <td align="center" colspan="3">
                            <img class='marcoImg' src='<?php echo base_url(); ?><?php if(isset($firma_asesor)) { echo (($firma_asesor != "") ?  $firma_asesor : "assets/imgs/fondo_bco.jpeg"); } else { echo "assets/imgs/fondo_bco.jpeg"; } ?>' id='' style='width:80px;height:30px;'>
                            <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php if(isset($firma_asesor)) echo $firma_asesor ?>'>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    <!-- Comentarios del asesor -->
    <div class="row espacio_div">
        <div class="col-sm-5">
            <h4>Notas del asesor:</h4>
            <!-- Verificamos si es una vista del asesor -->
            <label for="" style="font-size:12px;"><?php if(isset($nota_asesor)){ if($nota_asesor != "") echo $nota_asesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
        </div>
        <div class="col-sm-7" align="right">
            <h4>Archivo(s) de la cotización:</h4>
            <br>

            <?php if (isset($archivos_presupuesto)): ?>
                <?php $contador = 1; ?>
                <?php for ($i = 0; $i < count($archivos_presupuesto); $i++): ?>
                    <?php if ($archivos_presupuesto[$i] != ""): ?>
                        <a href="<?php echo base_url().$archivos_presupuesto[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
                            DOC. (<?php echo $contador; ?>)
                        </a>

                        <?php if ((($i+1)% 4) == 0): ?>
                            <br>
                        <?php endif; ?>
                        
                        <?php $contador++; ?>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if (count($archivos_presupuesto) == 0): ?>
                    <h4 style="text-align: right;">Sin archivos</h4>
                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                <?php endif; ?>
            <?php endif; ?>

            <hr>
            <br>
        </div>
    </div>

    <hr>
    <!-- Comentarios del técnico -->
    <div class="row espacio_div">
        <div class="col-md-6">
            <h4>Comentario del técnico para Refacciones:</h4>
            <label for="" style="font-size:12px;"><?php if(isset($comentario_tecnico)){ if($comentario_tecnico != "") echo $comentario_tecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
        </div>

        <div class="col-sm-6" align="right">
            <h4>Archivo(s) del técnico para Refacciones:</h4>

            <?php if (isset($archivo_tecnico)): ?>
                <?php $contador = 1; ?>
                <?php for ($i = 0; $i < count($archivo_tecnico); $i++): ?>
                    <?php if ($archivo_tecnico[$i] != ""): ?>
                        <a href="<?php echo base_url().$archivo_tecnico[$i]; ?>" class="btn" style="background-color: #5cb8a5;border-color: #4c8bae;color:white;font-size: 9px;font-weight: bold;" target="_blank">
                            DOC. (<?php echo $contador; ?>)
                        </a>

                        <?php if (($contador % 4) == 0): ?>
                            <br>
                        <?php endif; ?>

                        <?php $contador++; ?>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if (count($archivo_tecnico) == 0): ?>
                    <h4 style="text-align: right;">Sin archivos</h4>
                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <hr>
    <!-- comentarios de ventanilla -->
    <div class="row espacio_div">
        <div class="col-sm-6" align="left">
            <h4>Comentarios de Ventanilla:</h4>
            <label for="" style="font-size:12px;"><?php if(isset($comentario_ventanilla)){ if($comentario_ventanilla != "") echo $comentario_ventanilla; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
        </div>
        <div class="col-sm-6" align="right"></div>
    </div>

    <hr>
    <!-- comentarios del jefe de taller -->
    <div class="row espacio_div">
        <div class="col-sm-6" align="left">
            <h4>Comentarios del Jefe de Taller:</h4>
            <label for="" style="font-size:12px;"><?php if(isset($comentario_jdt)){ if($comentario_jdt != "") echo $comentario_jdt; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
        </div>
        <div class="col-sm-6" align="right">
            <h4>Archivo(s) del Jefe de Taller:</h4>

            <?php if (isset($archivo_jdt)): ?>
                <?php $contador = 1; ?>
                <?php for ($i = 0; $i < count($archivo_jdt); $i++): ?>
                    <?php if ($archivo_jdt[$i] != ""): ?>
                        <a href="<?php echo base_url().$archivo_jdt[$i]; ?>" class="btn btn-primary" style="font-size: 9px;font-weight: bold;" target="_blank">
                            DOC. (<?php echo $contador; ?>)
                        </a>
                        
                        <?php if (($contador % 4) == 0): ?>
                            <br>
                        <?php endif; ?>
                        
                        <?php $contador++; ?>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if (count($archivo_jdt) == 0): ?>
                    <h4 style="text-align: right;">Sin archivos</h4>
                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
    
    <hr>
    <?php if ($tipo_vista == "1"): ?>
        <div class="row espacio_div">
            <div class="col-sm-6">
                <div class="cuadro_info" align="center">
                    <?php if (isset($acepta_cliente)): ?>
                        <?php if ($acepta_cliente == "Si"): ?>
                            <label style="color: darkgreen;font-size: 13px;">
                                Presupuesto Autorizado Totalmente
                            </label>
                        <?php elseif ($acepta_cliente == "No"): ?>
                            <label style="color: red ;font-size: 13px;">
                                Presupuesto Rechazado Totalmente
                            </label>
                        <?php elseif ($acepta_cliente == "Val"): ?>
                            <label style="color: darkorange;font-size: 13px;">
                                Presupuesto Autorizado Parcialmente 
                            </label>
                        <?php else: ?>
                            <label style="color: black;font-size: 13px;">
                                Presupuesto sin Afectar
                            </label>
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($acepta_cliente != "") ? "(Confirmar con el asesor que se regresara aunque ya este afectada por el asesor)" : "") ?>
                        </label>

                        <?php 
                            if ($acepta_cliente != "No") {
                                if ($estado_refacciones == "0") {
                                    $color = "background-color:#f5acaa;";
                                } elseif ($estado_refacciones == "1") {
                                    $color = "background-color:#f5ff51;";
                                } elseif ($estado_refacciones == "2") {
                                    $color = "background-color:#5bc0de;";
                                }else{
                                    $color = "background-color:#c7ecc7;";
                                }
                            } else {
                                $color = "background-color:red;color:white;";
                            }
                         ?>
                    <?php endif ?>
                </div>

                <br>
                <div style="<?= $color ?>font-size: 13px;text-transform: uppercase;padding: 10px;" align="center">
                    <?php if (isset($estado_refacciones)): ?>
                        <?php if ($estado_refacciones == "1"): ?>
                            REFACCIONES SOLICIATADAS
                        <?php elseif ($estado_refacciones == "2"): ?>
                            REFACCIONES RECIBIDAS
                        <?php elseif ($estado_refacciones == "3"): ?>
                            REFACCIONES ENTREGADAS
                        <?php else: ?>
                            REFACCIONES SIN SOLICITAR
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($estado_refacciones != 0) ? "(No se puede regresar al asesor, actualmente ya esta siendo procesado por ventanilla, se tendra que reiniciar todo el presupuesto si se quiere editar)" : "") ?>
                        </label>
                    <?php endif ?>
                </div>

                <br>
                <div class="cuadro_info" align="center">
                    <?php if (isset($firma_asesor)): ?>
                        <?php if ($firma_asesor != ""): ?>
                            Presupuesto firmado por el asesor el <?= $fecha_fasesor ?>
                        <?php else: ?>
                            No se ha firmado por el asesor
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>

            <div class="col-sm-6">
                <br>
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c" >
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c" >
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:90%;text-align:left;'></textarea>
                            <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
                            <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <i class="fas fa-spinner cargaIcono"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="resultado_proceso"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="btn_editar_pre" type="button" class="btn btn-success" onclick="regresar_presupuesto('asesor')">
                                ELIMINAR FIRMA DEL ASESOR
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php elseif ($tipo_vista == "2"): ?>
        <div class="row espacio_div">
            <div class="col-sm-6">
                <div class="cuadro_info" align="center">
                    <?php if (isset($acepta_cliente)): ?>
                        <?php if ($acepta_cliente == "Si"): ?>
                            <label style="color: darkgreen;font-size: 13px;">
                                Presupuesto Autorizado Totalmente
                            </label>
                        <?php elseif ($acepta_cliente == "No"): ?>
                            <label style="color: red ;font-size: 13px;">
                                Presupuesto Rechazado Totalmente
                            </label>
                        <?php elseif ($acepta_cliente == "Val"): ?>
                            <label style="color: darkorange;font-size: 13px;">
                                Presupuesto Autorizado Parcialmente 
                            </label>
                        <?php else: ?>
                            <label style="color: black;font-size: 13px;">
                                Presupuesto sin Afectar
                            </label>
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($acepta_cliente != "") ? "(Confirmar con el asesor que se regresara aunque ya este afectada por el asesor)" : "") ?>
                        </label>

                        <?php 
                            if ($acepta_cliente != "No") {
                                if ($estado_refacciones == "0") {
                                    $color = "background-color:#f5acaa;";
                                } elseif ($estado_refacciones == "1") {
                                    $color = "background-color:#f5ff51;";
                                } elseif ($estado_refacciones == "2") {
                                    $color = "background-color:#5bc0de;";
                                }else{
                                    $color = "background-color:#c7ecc7;";
                                }
                            } else {
                                $color = "background-color:red;color:white;";
                            }
                         ?>
                    <?php endif ?>
                </div>

                <br>
                <div style="<?= $color ?>font-size: 13px;text-transform: uppercase;padding: 10px;" align="center">
                    <?php if (isset($estado_refacciones)): ?>
                        <?php if ($estado_refacciones == "1"): ?>
                            REFACCIONES SOLICIATADAS
                        <?php elseif ($estado_refacciones == "2"): ?>
                            REFACCIONES RECIBIDAS
                        <?php elseif ($estado_refacciones == "3"): ?>
                            REFACCIONES ENTREGADAS
                        <?php else: ?>
                            REFACCIONES SIN SOLICITAR
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($estado_refacciones != 0) ? "(No se puede regresar al asesor, actualmente ya esta siendo procesado por ventanilla, se tendra que reiniciar todo el presupuesto si se quiere editar)" : "") ?>
                        </label>
                    <?php endif ?>
                </div>

                <br>
                <div class="cuadro_info" align="center">
                    <?php if (isset($firma_asesor)): ?>
                        <?php if ($firma_asesor != ""): ?>
                            Presupuesto firmado por el asesor el <?= $fecha_fasesor ?>
                        <?php else: ?>
                            No se ha firmado por el asesor
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($firma_asesor != "") ? "(No se puede regresar al jefe de taller, ya fue firmada por el asesor, se requiere elimnar la firma del asesor primero)" : "") ?>
                        </label>
                    <?php endif ?>
                </div>

                <br>
                <div class="cuadro_info" align="center">
                    <?php if (isset($firma_jdt)): ?>
                        <?php if ($firma_jdt != ""): ?>
                            Presupuesto firmado por el jefe de taller el <?= $fecha_fjdt ?>
                        <?php else: ?>
                            No se ha firmado por el jefe de taller
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>

            <div class="col-sm-6">
                <br>
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c" >
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c" >
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:90%;text-align:left;'></textarea>
                            <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
                            <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <i class="fas fa-spinner cargaIcono"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="resultado_proceso"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="btn_editar_pre" type="button" class="btn btn-success" onclick="regresar_presupuesto('jtaller')">
                                ELIMINAR FIRMA DEL JEFE DE TALLER
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php elseif ($tipo_vista == "3"): ?>
        <div class="row espacio_div">
            <div class="col-sm-6">
                <div class="cuadro_info" align="center">
                    <?php if (isset($acepta_cliente)): ?>
                        <?php if ($acepta_cliente == "Si"): ?>
                            <label style="color: darkgreen;font-size: 13px;">
                                Presupuesto Autorizado Totalmente
                            </label>
                        <?php elseif ($acepta_cliente == "No"): ?>
                            <label style="color: red ;font-size: 13px;">
                                Presupuesto Rechazado Totalmente
                            </label>
                        <?php elseif ($acepta_cliente == "Val"): ?>
                            <label style="color: darkorange;font-size: 13px;">
                                Presupuesto Autorizado Parcialmente 
                            </label>
                        <?php else: ?>
                            <label style="color: black;font-size: 13px;">
                                Presupuesto sin Afectar
                            </label>
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($acepta_cliente != "") ? "(Confirmar con el asesor que se regresara aunque ya este afectada por el asesor)" : "") ?>
                        </label>

                        <?php 
                            if ($acepta_cliente != "No") {
                                if ($estado_refacciones == "0") {
                                    $color = "background-color:#f5acaa;";
                                } elseif ($estado_refacciones == "1") {
                                    $color = "background-color:#f5ff51;";
                                } elseif ($estado_refacciones == "2") {
                                    $color = "background-color:#5bc0de;";
                                }else{
                                    $color = "background-color:#c7ecc7;";
                                }
                            } else {
                                $color = "background-color:red;color:white;";
                            }
                         ?>
                    <?php endif ?>
                </div>

                <br>
                <div class="cuadro_info" align="center">
                    <?php if (isset($firma_asesor)): ?>
                        <?php if ($firma_asesor != ""): ?>
                            Presupuesto firmado por el asesor el <?= $fecha_fasesor ?>
                        <?php else: ?>
                            No se ha firmado por el asesor
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($firma_asesor != "") ? "(No se puede regresar a ventanilla, ya fue firmada por el asesor, se requiere elimnar la firma del asesor primero)" : "") ?>
                        </label>
                    <?php endif ?>
                </div>

                <br>
                <div class="cuadro_info" align="center">
                    <?php if (isset($firma_jdt)): ?>
                        <?php if ($firma_jdt != ""): ?>
                            Presupuesto firmado por el jefe de taller el <?= $fecha_fjdt ?>
                        <?php else: ?>
                            No se ha firmado por el jefe de taller
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($firma_jdt != "") ? "(No se puede regresar a ventanilla, ya fue firmada por el jefe de taller, se requiere elimnar la firma del jefe de taller primero)" : "") ?>
                        </label>
                    <?php endif ?>
                </div>
            </div>

            <div class="col-sm-6">
                <br>
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c" >
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c" >
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:90%;text-align:left;'></textarea>
                            <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
                            <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <i class="fas fa-spinner cargaIcono"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="resultado_proceso"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="btn_editar_pre" type="button" class="btn btn-success" onclick="regresar_presupuesto('ventanilla')">
                                REGRESAR A VENTANILLA
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php elseif ($tipo_vista == "4"): ?>
        <div class="row espacio_div">
            <div class="col-sm-12 table-responsive">
                <table style="width: 100%!important;" class="table table-striped table-bordered ">
                    <tr>
                        <td class="titulo-1b" style="width: 10%;" align="center">
                            ID CITA
                        </td>
                        <td class="titulo-1b"  style="width: 15%;" align="center">
                            PANEL ORIGEN / FORMULARIO
                        </td>
                        <td class="titulo-1b"  style="width: 30%;" align="center">
                            MOVIMINENTO
                        </td>
                        <td class="titulo-1b"  style="width: 15%;" align="center">
                            USUARIO
                        </td>
                        <td class="titulo-1b"  style="width: 15%;" align="center">
                            FECHA DE MOVIMIENTO
                        </td>
                    </tr>
                    <?php for ($x = 0; $x < count($historial_presupuesto); $x++): ?>
                        <tr>
                            <td class="titulo-1c" align="center">
                                <?= $historial_presupuesto[$x]["id_cita"] ?>
                            </td>
                            <td class="titulo-1c" align="center">
                                <?= $historial_presupuesto[$x]["origen_movimiento"] ?>
                            </td>
                            <td class="titulo-1c" align="center">
                                <?= $historial_presupuesto[$x]["movimiento"] ?>
                            </td>
                            <td class="titulo-1c" align="center">
                                <?= $historial_presupuesto[$x]["usuario"] ?>
                            </td>
                            <td class="titulo-1c" align="center">
                                <?php 
                                    $fecha = new DateTime($historial_presupuesto[$x]["fecha_registro"]);
                                    echo $fecha->format('d-m-Y H:i:s');
                                ?>
                            </td>
                        </tr>
                    <?php endfor; ?>
                </table>
            </div>
        </div>

        <input type="hidden" id="vista_informativa" value="1">
    <?php elseif ($tipo_vista == "5"): ?>
        <div class="row espacio_div">
            <div class="col-sm-6">
                <div class="cuadro_info" align="center">
                    <?php if (isset($acepta_cliente)): ?>
                        <?php if ($acepta_cliente == "Si"): ?>
                            <label style="color: darkgreen;font-size: 13px;">
                                Presupuesto Autorizado Totalmente
                            </label>
                        <?php elseif ($acepta_cliente == "No"): ?>
                            <label style="color: red ;font-size: 13px;">
                                Presupuesto Rechazado Totalmente
                            </label>
                        <?php elseif ($acepta_cliente == "Val"): ?>
                            <label style="color: darkorange;font-size: 13px;">
                                Presupuesto Autorizado Parcialmente 
                            </label>
                        <?php else: ?>
                            <label style="color: black;font-size: 13px;">
                                Presupuesto sin Afectar
                            </label>
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($acepta_cliente != "") ? "(Confirmar con el asesor que se regresara aunque ya este afectada por el asesor)" : "") ?>
                        </label>

                        <?php 
                            if ($acepta_cliente != "No") {
                                if ($estado_refacciones == "0") {
                                    $color = "background-color:#f5acaa;";
                                } elseif ($estado_refacciones == "1") {
                                    $color = "background-color:#f5ff51;";
                                } elseif ($estado_refacciones == "2") {
                                    $color = "background-color:#5bc0de;";
                                }else{
                                    $color = "background-color:#c7ecc7;";
                                }
                            } else {
                                $color = "background-color:red;color:white;";
                            }
                         ?>
                    <?php endif ?>
                </div>

                <br>
                <div style="<?= $color ?>font-size: 13px;text-transform: uppercase;padding: 10px;" align="center">
                    <?php if (isset($estado_refacciones)): ?>
                        <?php if ($estado_refacciones == "1"): ?>
                            REFACCIONES SOLICIATADAS
                        <?php elseif ($estado_refacciones == "2"): ?>
                            REFACCIONES RECIBIDAS
                        <?php elseif ($estado_refacciones == "3"): ?>
                            REFACCIONES ENTREGADAS
                        <?php else: ?>
                            REFACCIONES SIN SOLICITAR
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($estado_refacciones != 0) ? "(No se puede regresar al asesor, actualmente ya esta siendo procesado por ventanilla, se tendra que reiniciar todo el presupuesto si se quiere editar)" : "") ?>
                        </label>
                    <?php endif ?>
                </div>

                <br>
                <div class="cuadro_info" align="center">
                    <?php if (isset($firma_asesor)): ?>
                        <?php if ($firma_asesor != ""): ?>
                            Presupuesto firmado por el asesor el <?= $fecha_fasesor ?>
                        <?php else: ?>
                            No se ha firmado por el asesor
                        <?php endif ?>

                        <br>
                        <label style="color: red;font-size: 10px; text-transform: uppercase;text-align: justify;">
                            <?= (($firma_asesor != "") ? "(No se puede regresar a garantias, ya fue firmada por el asesor, se requiere elimnar la firma del asesor)" : "") ?>
                        </label>
                    <?php endif ?>
                </div>

                <br>
                <div class="cuadro_info" align="center">
                    <?php if (isset($firma_garantia)): ?>
                        <?php if ($firma_garantia != ""): ?>
                            Presupuesto firmado por garantias el <?= $fecha_fgarantias ?>
                        <?php else: ?>
                             <?php if ($ref_garantias > 0): ?>
                                No se ha firmado por garantias
                            <?php else: ?>
                                El presupuesto no cuenta con piezas con garantia
                            <?php endif ?>
                        <?php endif ?>
                    <?php endif ?>
                </div>
            </div>

            <div class="col-sm-6">
                <br>
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c" >
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c" >
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:90%;text-align:left;'></textarea>
                            <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
                            <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <i class="fas fa-spinner cargaIcono"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="resultado_proceso"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="btn_editar_pre" type="button" class="btn btn-success" onclick="regresar_presupuesto('garantias')">
                                ELIMINAR FIRMA DE GARANTÍAS
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php endif ?>

    <input type="hidden" id="firma_asesor" name="firma_asesor" value="<?= ((isset($firma_asesor)) ? (($firma_asesor != '') ? 1 : '') : '') ?>">
    <input type="hidden" id="firma_jefetaller" name="firma_jefetaller" value="<?= ((isset($firma_jdt)) ? (($firma_jdt != '') ? 1 : '') : '') ?>">
    <input type="hidden" id="firma_garantia" name="firma_garantia" value="<?= ((isset($firma_garantia)) ? (($firma_garantia != '') ? 1 : '') : '') ?>">
    <input type="hidden" id="afecta_cliente" name="afecta_cliente" value="<?= ((isset($acepta_cliente)) ? $acepta_cliente : '') ?>">
    <input type="hidden" id="edo_ref" name="edo_ref" value="<?= ((isset($estado_refacciones)) ? $estado_refacciones : '') ?>">
    <input type="hidden" name="tipo_vista" value="<?= ((isset($tipo_vista)) ? $tipo_vista : '') ?>">
</div>
