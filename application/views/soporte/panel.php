<style type="text/css">
    .titulo-1a{
        font-size: 17px;
        color: darkblue;
    }

    .titulo-1b{
        font-size: 12px;
        color: black;
        text-transform: uppercase;
        padding-right: 10px;
        vertical-align: middle;
        background-color: lavender;
    }

    .titulo-1c{
        font-size: 11px;
        color: darkblue;
        padding-left: 10px;
        vertical-align: middle;
    }

    table{
        width: 100%!important;
    }

    td{
        height: 40px;
        vertical-align: middle;
    }

    .cuadro_d{
        border-top: 1px solid darkblue;
        border-left: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_i{
        border-top: 1px solid darkblue;
        border-right: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_titulo{
        border: 2px solid darkblue;
    }

    .cuadro_titulo_txt{
        border: 1px solid #98989A;
        text-align: justify;
        font-size: 13px;
    }

    .espacio_div{
        margin-top: 15px;
    }
</style>

<section class="content" style="margin: 20px;">
    <div class="row">
        <div class="col-sm-2">
            
        </div>
        <div class="col-sm-7">
            <h3 align="center" style="color:#340f7b;">
                Panel de Soporte (Formularios)
                <input type="hidden" id="tipo_vista" value="<?= ((isset($tipo_vista)) ? $tipo_vista : 'Panel') ?>">
            </h3>
        </div>
        <div class="col-sm-2" style="padding-top: 15px;" align="right">
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Login_soporte";?>';">
                Cerrar Sesión
            </button>
        </div>
    </div>
    
    <hr>
    <br>
    
    <div class="row table-responsive">
        <div class="col-sm-7">
            <table>
                <tr>
                    <td style="width: 8%;">
                        <br>
                    </td>
                    <td class="cuadro_d" style="width: 70%;padding: 0px 0px 20px 20px;">
                        <h5>No. de Orden:</h5>
                        <div class="input-group">
                            <input type="text" class="form-control" id="busqueda_gral" value="<?= ((isset($id_cita)) ? $id_cita : '') ?>" style="width: 90%;">
                        </div>
                        <h5 class="error" id="error_busqueda"></h5>
                        <h5 style="color:darkgreen;" id="actualizacion_orden"></h5>
                    </td>
                    <td class="cuadro_i" style="width: 10%;">
                        <a class="btn btn-secondary" id="buscar" style="margin-top: 0px;">
                            <i class="fa fa-search"></i>
                        </a>
                    </td>
                    <td style="width: 5%;">
                        <br>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-1" align="center" style="padding-top: 10px;">
            <i class="fas fa-spinner cargaIcono"></i>
        </div>
        <div class="col-sm-3" align="right">
            <a id="historial_soporte" href="<?= base_url().'Soporte_Historial/'.((isset($id_cita_url)) ? $id_cita_url : '0'); ?>" class='btn' style='color: #fff; background-color: #9b7430; border-color: #eea236;font-size: 13px; margin-top: 10px;'>
                Ver Historial de Soporte
            </a>
            <button id="reenvio_orden" onclick="reenvio_orden()" class='btn' style='color: #fff; background-color: #6d0c9d; border-color: #1e0c9d;font-size: 13px; margin-top: 15px; display: none;'>
                Reenviar orden a Intelisis / DMS
            </button>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-sm-12">
            <table>
                <tr>
                    <td style="width: 15%;">
                        <br>
                    </td>
                    <td style="width: 33%;">
                        <button id="btn_doc_alta" type="button" class="btn btn-success" onclick="actualizar_doc(1)">
                            REGISTRAR DOCUMENTACIÓN
                        </button>
                    </td>
                    <td style="width: 4%;">
                        <br>
                    </td>
                    <td style="width: 33%;">
                        <button id="btn_doc_act" type="button" class="btn btn-warning" onclick="actualizar_doc(2)">
                            ACTUALIZAR DOCUMENTACIÓN
                        </button>
                    </td>
                    <td style="width: 15%;" align="left">
                        <br>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <hr> 
    <div class="row">
        <div class="col-sm-12">
            <table class="cuadro_titulo">
                <tr>
                    <td class="titulo-1b" style="width: 10%;">
                        No. Orden:
                    </td>
                    <td class="titulo-1c" style="width: 15%;">
                        <label id="lb_no_orden"></label>
                    </td>
                    <td class="titulo-1b" style="width: 10%;" align="center">
                        Folio:
                    </td>
                    <td class="titulo-1c" style="width: 15%;">
                        <label id="lb_folio"></label>
                    </td>
                    <td class="titulo-1b" style="width: 10%;" align="center">
                        Serie:
                    </td>
                    <td class="titulo-1c" style="width: 15%;">
                        <label id="lb_serie"></label>
                    </td>
                    <td class="titulo-1b" style="width: 10%;" align="center">
                        Placas:
                    </td>
                    <td class="titulo-1c" style="width: 15%;">
                        <label id="lb_placas"></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <table>
                            <tr>
                                <td class="titulo-1b" style="width: 10%;">
                                    Modelo:
                                </td>
                                <td class="titulo-1c" style="width: 20%;">
                                    <label id="lb_modelo"></label>
                                </td>
                                <td class="titulo-1b" style="width: 14%;" align="center">
                                    Fecha recepción:
                                </td>
                                <td class="titulo-1c" style="width: 20%;">
                                    <label id="lb_frecepcion"></label>
                                </td>
                                <td class="titulo-1b" style="width: 14%;" align="center">
                                    Fecha promesa:
                                </td>
                                <td class="titulo-1c" style="width: 20%;">
                                    <label id="lb_fpromesa"></label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <table>
                            <tr>
                                <td class="titulo-1b" style="width: 10%;">
                                    Asesor:
                                </td>
                                <td class="titulo-1c" style="width: 23%;">
                                    <label id="lb_asesor"></label>
                                </td>
                                <td class="titulo-1b" style="width: 10%;" align="center">
                                    Técnico:
                                </td>
                                <td class="titulo-1c" style="width: 20%;">
                                    <label id="lb_tecnico"></label>
                                </td>
                                <td class="titulo-1b" style="width: 10%;" align="center">
                                    Cliente:
                                </td>
                                <td class="titulo-1c" style="width: 26%;">
                                    <label id="lb_cliente"></label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <hr> 
    <div class="row">
        <div class="col-sm-12 table-responsive">
            <table style="width: 100%!important;" class="table table-striped table-bordered ">
                <tr>
                    <td class="cuadro_titulo titulo-1b" style="width: 48%;padding: 15px;">
                        <div class="row">
                            <div class="col-sm-7 titulo-1b">
                                HOJA MULTIPUNTO
                            </div>
                            <div class="col-sm-5" align="right">
                                <a id="btn-doc-hm" target="_blank" href="" class='btn btn-success' style='font-size: 11px;font-weight: bold;'>
                                    VER HOJA MULTIPUNTO
                                </a>
                            </div>
                        </div>
                    </td>
                    <td style="width: 4%;">
                        <br>
                    </td>
                    <td class="cuadro_titulo titulo-1b" style="width: 48%;padding: 15px;">
                        <div class="row">
                            <div class="col-sm-7 titulo-1b">
                                PRESUPUESTO MULTIPUNTO
                            </div>
                            <div class="col-sm-5" align="right">
                                <a id="btn-doc-pm" target="_blank" href="" class='btn btn-success' style='font-size: 11px;font-weight: bold;'>
                                    VER PRESUPUESTO
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="opciones_hm">
                            <tr>
                                <td class="titulo-1c" style="width: 70%;">
                                    Eliminar firma del técnico / jefe de taller
                                </td>
                                <td class="titulo-1c" style="width: 30%;" align="center">
                                    <a id="btn1-hm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Eliminar formulario multipunto
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn2-hm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Ajustar firmas (cuando se crea conflicto en el tablero para cerrar una orden y no reconoce una firma que si existe)
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn3-hm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Cambiar indicadores
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn4-hm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Reasignar hoja multipunto a nuevo número de orden
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn5-hm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <br>
                    </td>
                    <td></td>
                    <td>
                        <table id="opciones_pm">
                            <tr>
                                <td class="titulo-1c" style="width: 70%;">
                                    Ver presupuesto e historial de movimientos
                                </td>
                                <td class="titulo-1c" style="width: 30%;" align="center">
                                    <a id="btn1-pm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Regresar presupuesto al asesor <br>
                                    (siempre y cuando no se hayan solicitado las piezas en ventanilla)

                                    <br>
                                    <label id="label2-pm" class="error"></label>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn2-pm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Regresar presupuesto al jefe de taller <br>
                                    (siempre y cuando no se hayan solicitado las piezas en ventanilla)

                                    <br>
                                    <label id="label3-pm" class="error"></label>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn3-pm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Regresar presupuesto a ventanilla <br>
                                    (siempre y cuando no se hayan solicitado las piezas en ventanilla)

                                    <br>
                                    <label id="label4-pm" class="error"></label>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn4-pm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Regresar presupuesto a garantias <br>
                                    (siempre y cuando no se hayan solicitado las piezas en ventanilla)

                                    <br>
                                    <label id="label5-pm" class="error"></label>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn5-pm" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td class="cuadro_titulo titulo-1b">
                        <div class="row">
                            <div class="col-sm-7 titulo-1b">
                                DIAGNÓSTICO
                            </div>
                            <div class="col-sm-5" align="right">
                                <a id="btn-doc-diag" target="_blank" href="" class='btn btn-success' style='font-size: 11px;font-weight: bold;'>
                                    VER DIAGNÓSTICO
                                </a>
                            </div>
                        </div>
                    </td>
                    <td style="width: 4%;">
                        <br>
                    </td>
                    <td class="cuadro_titulo titulo-1b">
                        <div class="row">
                            <div class="col-sm-7 titulo-1b">
                                VOZ CLIENTE
                            </div>
                            <div class="col-sm-5" align="right">
                                <a id="btn-doc-vc" target="_blank" href="" class='btn btn-success' style='font-size: 11px;font-weight: bold;'>
                                    VER VOZ CLIENTE
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="opciones_diag">
                            <tr>
                                <td class="titulo-1c" style="width: 70%;">
                                    Reasignar diagnóstico a nuevo número de orden
                                </td>
                                <td class="titulo-1c" style="width: 30%;" align="center">
                                    <a id="btn1-diag" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Recuperar contrato y/o firmas del contrato
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn2-diag" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="titulo-1c">
                                    Eliminar Diagnóstico
                                </td>
                                <td class="titulo-1c" align="center">
                                    <a id="btn3-diag" href="" class='btn btn-primary' style='font-size: 10px;font-weight: bold;'>
                                        IR
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <br>
                    </td>
                    <td></td>
                    <td>
                        <table id="opciones_vc">
                            <tr>
                                <td class="titulo-1c" style="width: 70%;" align="center">
                                    <audio src="" controls="controls" type="audio/*" preload="preload" autoplay style="width:100%;" id="reproductor">
                                      Your browser does not support the audio element.
                                    </audio>
                                </td>
                                <td class="titulo-1c" style="width: 30%;" align="center">
                                    <br>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>
