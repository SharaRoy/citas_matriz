<style type="text/css">
    .titulo-1a{
        font-size: 17px;
        color: darkblue;
    }

    .titulo-1b{
        font-size: 12px;
        color: black;
        text-transform: uppercase;
        padding-right: 10px;
        vertical-align: middle;
        background-color: lavender;
    }

    .titulo-1c{
        font-size: 11px;
        color: darkblue;
        padding-left: 10px;
        vertical-align: middle;
        text-transform: uppercase;
    }

    table{
        width: 100%!important;
    }

    td{
        height: 30px;
        vertical-align: middle;
    }

    .cuadro_d{
        border-top: 1px solid darkblue;
        border-left: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_i{
        border-top: 1px solid darkblue;
        border-right: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_titulo{
        border: 2px solid darkblue;
        padding: 10px;
    }

    .cuadro_titulo_txt{
        border: 1px solid #98989A;
        text-align: justify;
        font-size: 13px;
    }

    .espacio_div{
        margin-top: 15px;
    }

    .cuadro_info{
        font-size: 13px; 
        font-weight: bold; 
        text-transform: uppercase;
        padding: 10px;
        background-color: lemonchiffon;
    }
</style>

<div style="margin:40px;">
    <div class="row espacio_div">
        <div class="col-sm-4" style="padding-left: 20px;">
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_soporte/".((isset($id_cita_url)) ? $id_cita_url : "");?>';">
                Regresar
            </button>
        </div>
    </div>

    <div class="espacio_div cuadro_titulo">
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="form-control" name="id_cita" id="id_cita" value="<?= ((isset($id_cita)) ? $id_cita : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="form-control" name="folio_externo" id="folio_externo" value="<?= ((isset($folio_externo)) ? $folio_externo : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="form-control" type="text" name="noSerie" id="noSerieText" value="<?= ((isset($serie)) ? $serie : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-sm-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="form-control" type="text" name="modelo" id="txtmodelo" value="<?= ((isset($vehiculo)) ? $vehiculo : '') ?>" style="width:100%;" disabled>
            </div>
        </div>
        
        <div class="row espacio_div">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="form-control" name="placas" id="placas" value="<?= ((isset($placas)) ? $placas : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="form-control" type="text" name="uen" id="uen" value="<?= ((isset($unidad)) ? $unidad : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="form-control" type="text" name="tecnico" id="tecnico" value="<?= ((isset($tecnico)) ? $tecnico : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="form-control" type="text" name="asesors" id="asesors" value="<?= ((isset($asesor)) ? $asesor : '') ?>" style="width:100%;" disabled>
            </div>
        </div>
    </div>
        
    <hr>
    <?php if ($tipo_vista == "2"): ?>
        <div class="row espacio_div">
            <div class="col-sm-2"></div>
            <div class="col-sm-7">
                <h4 style="text-align: center;color: #800ab1; font-weight: bold;">
                    Eliminar formulario multipunto
                </h4>
            </div>
            <div class="col-sm-3" align="right">
                <a href="<?= base_url().'Multipunto_Revision/'.((isset($id_cita_url)) ? $id_cita_url : '0'); ?>" class='btn btn-warning' target="_blank" style='font-weight: bold;font-size: 13px;'>
                    Ver Hoja Multipunto
                </a>
            </div>
        </div>
        <hr>

        <div class="row espacio_div">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 cuadro_titulo table-responsive">
                <br>
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c">
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c">
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:90%;text-align:left;'></textarea>
                            <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
                            <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Evidencias (opcional):
                        </td>
                        <td class="titulo-1c">
                            <input type="file" id="evidencias" class="form-control" name="archivo[]" accept="image/*" multiple style="width:80%;margin:20px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <label style="color: red;font-size: 10px; display: none;">
                                [EL FORMULARIO NO SE ELIMINARA SOLO SE REASIGNARÁ UN NÚMERO DE ORDEN CON TERMINACIÓN "HME" POR POSIBLES ACLARACIONES]
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="error_envio_f"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="proceso_edicion_hm" type="button" class="btn btn-success">
                                ELIMINAR HOJA MULTIPUNTO
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    <?php elseif ($tipo_vista == "3"): ?>
        <div class="row espacio_div">
            <div class="col-sm-12 table-responsive">
                <table style="width: 100%!important;" class="table table-striped table-bordered ">
                    <tr>
                        <td class="titulo-1b" style="width: 15%;" align="center">
                            # REGISTRO
                        </td>
                        <td class="titulo-1b" style="width: 15%;" align="center">
                            ID CITA
                        </td>
                        <td class="titulo-1b"  style="width: 15%;" align="center">
                            FIRMA ASESOR
                        </td>
                        <td class="titulo-1b"  style="width: 15%;" align="center">
                            FIRMA TÉCNICO
                        </td>
                        <td class="titulo-1b"  style="width: 15%;" align="center">
                            FIRMA JEFE TALLER
                        </td>
                        <td class="titulo-1b"  style="width: 15%;" align="center">
                            FIRMA CLIENTE
                        </td>
                        <td class="titulo-1b"  style="width: 10%;" align="center">
                            ELIMINAR
                        </td>
                    </tr>
                    <?php for ($x = 0; $x < count($multipunto_reg); $x++): ?>
                        <tr>
                            <td class="titulo-1c" align="center">
                                <?= $multipunto_reg[$x]["id"] ?>
                            </td>
                            <td class="titulo-1c" align="center">
                                <?= $multipunto_reg[$x]["orden"] ?>
                            </td>
                            <td class="titulo-1c" align="center" style="background-color: <?= (($multipunto_reg[$x]["firmaAsesor"] != "") ? '#a2d9ce' : '#f5b7b1') ?>;">
                                <?= (($multipunto_reg[$x]["firmaAsesor"] != "") ? "SI" : "NO") ?>
                            </td>
                            <td class="titulo-1c" align="center" style="background-color: <?= (($multipunto_reg[$x]["firmaTecnico"] != "") ? '#a2d9ce' : '#f5b7b1') ?>;">
                                <?= (($multipunto_reg[$x]["firmaTecnico"] != "") ? "SI" : "NO") ?>
                            </td>
                            <td class="titulo-1c" align="center" style="background-color: <?= (($multipunto_reg[$x]["firmaJefeTaller"] != "") ? '#a2d9ce' : '#f5b7b1') ?>;">
                                <?= (($multipunto_reg[$x]["firmaJefeTaller"] != "") ? "SI" : "NO") ?>
                            </td>
                            <td class="titulo-1c" align="center" style="background-color: <?= (($multipunto_reg[$x]["firmaCliente"] != "") ? '#a2d9ce' : '#f5b7b1') ?>;">
                                <?= (($multipunto_reg[$x]["firmaCliente"] != "") ? "SI" : "NO") ?>
                            </td>
                            <td class="titulo-1c" align="center">
                                <a class="btn btn-danger" title="Eliminar Registro" onclick="eliminar_reg(<?= $multipunto_reg[$x]["id"] ?>)">
                                    <i class="fa fa-trash" style="font-size: 13px; color:white;"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endfor; ?>
                </table>

                <br>
                <label style="color: red;font-size: 11px;">
                    [EL FORMULARIO NO SE ELIMINARA SOLO SE REASIGNARÁ UN NÚMERO DE ORDEN CON TERMINACIÓN "HME" POR POSIBLES ACLARACIONES]
                </label>

                <label style="color: red;font-size: 11px; ">
                    NOTA: ELIMINE EL REGISTRO CON MENOS FIRMAS REGISTRADAS
                </label>
            </div>
        </div>

        <hr>
        <div class="row espacio_div">
            <div class="col-sm-2"></div>
            <div class="col-sm-7">
                <h4 style="text-align: center;color: #800ab1; font-weight: bold;">
                    Ajustar firmas (cuando se crea conflicto en el tablero para cerrar una orden y no reconoce una firma que si existe)
                </h4>
            </div>
            <div class="col-sm-3" align="right">
                <a href="<?= base_url().'Multipunto_Revision/'.((isset($id_cita_url)) ? $id_cita_url : '0'); ?>" class='btn btn-warning' target="_blank" style='font-weight: bold;font-size: 13px;'>
                    Ver Hoja Multipunto
                </a>
            </div>
        </div>
        <hr>

        <div class="row espacio_div">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 cuadro_titulo table-responsive">
                <br>
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c">
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c">
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:90%;text-align:left;'></textarea>
                            <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
                            <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Evidencias (opcional):
                        </td>
                        <td class="titulo-1c">
                            <input type="file" id="evidencias" class="form-control" name="archivo[]" accept="image/*" multiple style="width:80%;margin:20px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <label style="color: red;font-size: 10px;">
                                [EL FORMULARIO NO SE ELIMINARA SOLO SE REASIGNARÁ UN NÚMERO DE ORDEN CON TERMINACIÓN "HME" POR POSIBLES ACLARACIONES]
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            REGISTRO A ELIMINAR :
                        </td>
                        <td>
                            <label style="color: darkblue;font-size: 15px; text-transform: uppercase; padding-left: 10px;" id="reg_elim_lb"></label>
                            <input type="hidden" name="id_reg_eliminar" id="id_reg_eliminar" value="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="error_envio_f"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="proceso_edicion_hm" type="button" class="btn btn-success">
                                ELIMINAR HOJA MULTIPUNTO
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

    <?php elseif ($tipo_vista == "5"): ?>
        <hr>
        <div class="row espacio_div">
            <div class="col-sm-2"></div>
            <div class="col-sm-7">
                <h4 style="text-align: center;color: #800ab1; font-weight: bold;">
                    Reasignar hoja multipunto a nuevo número de orden
                </h4>
            </div>
            <div class="col-sm-3" align="right">
                <a href="<?= base_url().'Multipunto_Revision/'.((isset($id_cita_url)) ? $id_cita_url : '0'); ?>" class='btn btn-warning' target="_blank" style='font-weight: bold;font-size: 13px;'>
                    Ver Hoja Multipunto
                </a>
            </div>
        </div>
        <hr>

        <div class="row espacio_div">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 cuadro_titulo table-responsive">
                <table>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;width: 30%;">
                            Nvo. Número de Orden
                        </td>
                        <td class="titulo-1c">
                            <input type="number" class="form-control" name="nvo_num_orden">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Nombre de quien realiza la edición:
                        </td>
                        <td class="titulo-1c">
                            <input type="text" class="form-control" name="nombre_edicion">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Motivo de la edición:
                        </td>
                        <td class="titulo-1c">
                            <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:100%;text-align:left;'></textarea>
                            <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
                            <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="titulo-1c" style="vertical-align: middle;">
                            Evidencias (opcional):
                        </td>
                        <td class="titulo-1c">
                            <input type="file" id="evidencias" class="form-control" name="archivo[]" accept="image/*" multiple style="width:80%;margin:20px;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <label style="color: #ff8100;font-size: 10px; display: none;">
                                [EL FORMULARIO NO SE ELIMINARA SOLO SE REASIGNARÁ UN NÚMERO DE ORDEN CON TERMINACIÓN "HME" POR POSIBLES ACLARACIONES]
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td>
                            <label style="color: red;font-size: 11px; text-transform: uppercase; text-align: justify;" id="error_envio_f"></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button id="proceso_edicion_hm" type="button" class="btn btn-success">
                                REASIGNAR HOJA MULTIPUNTO
                            </button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php endif ?>

    <div class="row espacio_div">
        <div class="col-sm-4">
            <br>
        </div>
        <div class="col-sm-4"align="center">
            <i class="fas fa-spinner cargaIcono"></i>
            <br>
            <label style="font-size: 11px; text-transform: uppercase;" id="resultado_proceso"></label>

            <input type="hidden" id="firma_asesor" name="firma_asesor" value="<?= ((isset($firmaAsesor)) ? (($firmaAsesor != '') ? 1 : 2) : '') ?>">
            <input type="hidden" id="firma_tecnico" name="firma_tecnico" value="<?= ((isset($firmaTecnico)) ? (($firmaTecnico != '') ? 1 : 2) : '') ?>">
            <input type="hidden" id="firma_jefetaller" name="firma_jefetaller" value="<?= ((isset($firmaJefeTaller)) ? (($firmaJefeTaller != '') ? 1 : 2) : '') ?>">
            <input type="hidden" id="firma_cliente" name="firma_cliente" value="<?= ((isset($firmaCliente)) ? (($firmaCliente != '') ? 1 : 2) : '') ?>">

            <input type="hidden" name="ref_gral" value="<?= ((isset($id_reg_gral)) ? $id_reg_gral : '0') ?>">
            <input type="hidden" name="ref_gral2" value="<?= ((isset($id_reg_gral2)) ? $id_reg_gral2 : '0') ?>">
            <input type="hidden" name="ref_fi" value="<?= ((isset($id_reg_fti)) ? $id_reg_fti : '0') ?>">
            <input type="hidden" name="ref_ti" value="<?= ((isset($id_reg_tri)) ? $id_reg_tri : '0') ?>">
            <input type="hidden" name="ref_fd" value="<?= ((isset($id_reg_ftd)) ? $id_reg_ftd : '0') ?>">
            <input type="hidden" name="ref_td" value="<?= ((isset($id_reg_trd)) ? $id_reg_trd : '0') ?>">
            <input type="hidden" name="ref_gral3" value="<?= ((isset($id_reg_gral3)) ? $id_reg_gral3 : '0') ?>">

            <input type="hidden" name="origne_firma" value="">
            <input type="hidden" id="tipo_vista" value="<?= ((isset($tipo_vista)) ? $tipo_vista : '') ?>">
            <!--<input type="hidden" name="orden" id="id_cita" value="<?= ((isset($idCita)) ? $idCita : '') ?>">-->
        </div>
        <div class="col-sm-4">
            <br>
        </div>
    </div>
</div>