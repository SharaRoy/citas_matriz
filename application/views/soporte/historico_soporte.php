<style type="text/css">
    .titulo-1a{
        font-size: 17px;
        color: darkblue;
    }

    .titulo-1b{
        font-size: 12px;
        color: black;
        text-transform: uppercase;
        padding-right: 10px;
        vertical-align: middle;
        background-color: lavender;
    }

    .titulo-1c{
        font-size: 11px;
        color: darkblue;
        padding-left: 10px;
        vertical-align: middle;
        text-transform: uppercase;
    }

    table{
        width: 100%!important;
    }

    td{
        height: 30px;
        vertical-align: middle;
    }

    .cuadro_d{
        border-top: 1px solid darkblue;
        border-left: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_i{
        border-top: 1px solid darkblue;
        border-right: 1px solid darkblue;
        border-bottom: 1px solid darkblue;
    }

    .cuadro_titulo{
        border: 2px solid darkblue;
        padding: 10px;
    }

    .cuadro_titulo_txt{
        border: 1px solid #98989A;
        text-align: justify;
        font-size: 13px;
    }

    .espacio_div{
        margin-top: 15px;
    }

    .cuadro_info{
        font-size: 13px; 
        font-weight: bold; 
        text-transform: uppercase;
        padding: 10px;
        background-color: lemonchiffon;
    }
</style>

<div style="margin:40px;">
    <div class="row espacio_div">
        <div class="col-sm-4" style="padding-left: 20px;">
            <a class="btn btn-dark" href="<?=base_url()."Panel_soporte/".((isset($id_cita_url)) ? $id_cita_url : "");?>">
                Regresar
            </a>
        </div>
    </div>

    <div class="espacio_div cuadro_titulo">
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="form-control" name="id_cita" id="id_cita" value="<?= ((isset($id_cita)) ? $id_cita : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="form-control" name="folio_externo" id="folio_externo" value="<?= ((isset($folio_externo)) ? $folio_externo : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="form-control" type="text" name="noSerie" id="noSerieText" value="<?= ((isset($serie)) ? $serie : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-sm-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="form-control" type="text" name="modelo" id="txtmodelo" value="<?= ((isset($vehiculo)) ? $vehiculo : '') ?>" style="width:100%;" disabled>
            </div>
        </div>
        
        <div class="row espacio_div">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="form-control" name="placas" id="placas" value="<?= ((isset($placas)) ? $placas : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-4">
                <label for="">Unidad</label>&nbsp;
                <input class="form-control" type="text" name="uen" id="uen" value="<?= ((isset($unidad)) ? $unidad : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-5">
                <label for="">Técnico</label>&nbsp;
                <input class="form-control" type="text" name="tecnico" id="tecnico" value="<?= ((isset($tecnico)) ? $tecnico : '') ?>" style="width:100%;" disabled>
            </div>
        </div>

        <div class="row espacio_div">
            <div class="col-md-4">
                <label for="">Asesor</label>&nbsp;
                <input class="form-control" type="text" name="asesors" id="asesors" value="<?= ((isset($asesor)) ? $asesor : '') ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-8">
                <label for="">Cliente</label>&nbsp;
                <input class="form-control" type="text" name="cliente" id="cliente" value="<?= ((isset($cliente)) ? $cliente : '') ?>" style="width:100%;" disabled>
            </div>
        </div>
    </div>

    <hr>
    <div class="row espacio_div">
        <div class="col-sm-12 table-responsive">
            <table <?php if (count($historico) > 0) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered table-responsive" style="width: 100%;">
                <thead>
                    <tr>
                        <td class="titulo-1b" style="width: 5%;" align="center">
                            ID CITA
                        </td>
                        <td class="titulo-1b"  style="width: 10%;" align="center">
                            USUARIO EDITA
                        </td>
                        <td class="titulo-1b"  style="width: 20%;" align="center">
                            MOTIVO DE EDICIÓN
                        </td>
                        <td class="titulo-1b"  style="width: 15%;" align="center">
                            FORMULARIO
                        </td>
                        <td class="titulo-1b"  style="width: 20%;" align="center">
                            DATOS DE RESPALDO
                        </td>
                        <td class="titulo-1b"  style="width: 10%;" align="center">
                            FECHA DE MOVIMIENTO
                        </td>
                        <td class="titulo-1b"  style="width: 20%;" align="center">
                            EVIDENCIAS
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($historico) > 0): ?>
                        <?php for ($x = 0; $x < count($historico); $x++): ?>
                            <tr>
                                <td class="titulo-1c" align="center">
                                    <?= $historico[$x]["id_cita"] ?>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <?= $historico[$x]["usuario_edita"] ?>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <?= $historico[$x]["motivo_edicion"] ?>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <?= $historico[$x]["formulario"] ?>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <?= $historico[$x]["respaldo_edicion"] ?>
                                </td>
                                <td class="titulo-1c" align="center">
                                    <?php 
                                        $fecha = new DateTime($historico[$x]["fecha_registro"]);
                                        echo $fecha->format('d-m-Y H:i:s');
                                    ?>
                                </td>
                                <td align="center">
                                    <?php  
                                        //Recuperamos el indice para buscar tiempos
                                        $indice = array_search($historico[$x]["id"], $evidencias_inx);
                                        //Verificamos si tiene evidencias
                                        $evidencia = count($evidencias[$indice]["url"]);
                                        $lista = $evidencias[$indice]["url"];
                                    ?>
                                    <?php if ($evidencia > 0): ?>
                                        <?php for ($y = 0; $y < $evidencia; $y++): ?>
                                            <a href="<?=base_url().$lista[$y]["url"];?>" class='btn btn-primary' style='font-size: 10px;font-weight: bold;' target='_blank'>
                                                Ver Evidencia <?= $y+1 ?>
                                            </a>
                                        <?php endfor; ?>
                                    <?php else: ?>
                                        <h5 style="text-align: center;">
                                            Sin evidencias que mostrar
                                        </h5>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="7" style="width:100%;" align="center">
                                <h4>Sin registros que mostrar</h4>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
                        
            </table>
        </div>
    </div>
</div>


<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>

<script>
    var conf_tabla = $('#bootstrap-data-table2');
    conf_tabla.DataTable({
        "language": {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": ",",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
              }
        },
        "responsive": true,
        "ordering": false,
        "searching": false,
        //"bPaginate": false,
        //"paging": false,
        //"bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        //"bAutoWidth": false,
        //"processing": true,
        //"serverSide": true
    });
</script>