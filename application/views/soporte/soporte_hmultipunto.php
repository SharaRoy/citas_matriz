<div style="margin: 20px;" >
	<div class="row espacio_div">
        <div class="col-sm-4" style="padding-left: 20px;">
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_soporte/".((isset($id_cita_url)) ? $id_cita_url : "");?>';">
                Regresar
            </button>
        </div>
    </div>

    <br>
    <!-- Primera hoja de formulario -->
    <div class="row">
        <div class="col-sm-5">
            <h3 class="h2_title_blue">
                HOJA MULTIPUNTOS
            </h3>
        </div>
        <div class="col-sm-7" align="right">
            <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" style="max-width:6cm;margin-top:10px;">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="vertical_asesor">
                    USO EXCLUSIVO ASESOR
                </div>
                <div class="vertical_tecnico_1">
                    USO EXCLUSIVO TÉCNICO
                </div>
                <div class="panel-heading" style="text-align:center;">
                    INSPECCIÓN DE VEHÍCULO
                </div>

                <div class="panel-body">
                	<div class="row" style="padding-left: 13px;padding-right: 13px;">
	                    <!-- Lado izquierdo de la pantalla -->
	                    <div class="col-md-6">
	                        <!-- panel 1 -->
	                        <div class="panel panel-default">
	                            <div class="panel-body" style="border:2px solid black;">
	                                <div class="form-group" style="width:100%;">
	                                    <div class="row" style="width:100%;">
	                                        <div class="col-md-6">
	                                          <label for="">Fecha:</label>
	                                          <label style="color: darkblue;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($fechaCaptura)) echo $fechaCaptura; ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>
	                                          <br>
	                                        </div>
	                                        <div class="col-md-6" align="left">
	                                            <label for="">OR:</label>
	                                            <label style="color: darkblue;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($id_cita)) echo $id_cita;?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>

	                                            <br>
                                                Folio Intelisis:&nbsp;
                                                <label style="color:darkblue;">
                                                    <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                                                </label>
	                                        </div>
	                                    </div>
	                                    <div class="row" style="margin-top:3px;">
	                                        <div class="col-md-12">
	                                            <label for="">No.Serie (VIN):</label>
	                                            <label style="color: darkblue;"><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($noSerie)) echo $noSerie;?>&nbsp;&nbsp;&nbsp;&nbsp;</u></label>

	                                            <br>
	                                            <strong><label for="" id="nombreCamapanias"><?php if(isset($campania)) {if($campania != "[]") echo "Campaña: ".$campania; else  echo "Campaña no registrada"; } else  echo "Campaña no registrada"; ?></label></strong>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- panel 1 -->

	                        <!-- panel 2 -->
	                        <div class="panel panel-default">
	                            <div class="panel-body" style="border:2px solid black;">
	                                <table class="table-responsive" style="width:100%;">
	                                    <tbody>
	                                        <tr>
	                                            <td style="width: 75%;">
	                                                <label for="" style="width: 100%;">Modelo:<strong style="color: darkblue;"><u>&nbsp;&nbsp;<?php if(isset($nvoModelo)) echo $nvoModelo;?>&nbsp;&nbsp;</u></strong>
	                                            </td>
	                                            <td style="">
	                                                <label for="">No. de torre:</label>&nbsp;
	                                                <label style="color: darkblue;"><u>&nbsp;&nbsp;<?php if(isset($torre)) echo $torre;?>&nbsp;&nbsp;</u></label>
	                                            </td>
	                                        </tr>
	                                        <tr>
	                                            <td colspan="2">
	                                                <label for="">Nombre:</label>&nbsp;
	                                                <label style="color: darkblue;"><u>&nbsp;&nbsp;<?php if(isset($nombreDueno)) echo $nombreDueno;?>&nbsp;&nbsp;</u></label>
	                                            </td>
	                                        </tr>
	                                        <tr>
	                                            <td colspan="2">
	                                                <label for="">Correo electrónico:</label>&nbsp;
	                                                <label style="color: black;min-width: 60%;"><u>&nbsp;&nbsp;<?php if(isset($email)) echo $email;?>&nbsp;&nbsp;</u></label>
	                                                <label for="">Correo compañía:</label>&nbsp;
	                                                <label style="color: black;min-width: 60%;"><u>&nbsp;&nbsp;<?php if(isset($emailComp)) echo $emailComp;?>&nbsp;&nbsp;</u></label>
	                                            </td>
	                                        </tr>
	                                    </tbody>
	                                </table>
	                            </div>
	                        </div>
	                        <!-- panel 2 -->

	                        <!-- panel 3 -->
	                        <div class="panel panel-default">
	                            <div class="panel-body" style="border:2px solid black">
	                                <div class="table-responsive">
	                                    <table class="" style="width:100%" >
	                                        <tr class="headers_table">
	                                            <td style="text-align: center;background-color:#340f7b; color:white;width:80%">NIVELES DE FLUIDOS</td>
	                                            <td style="width:20%;text-align: center;">CAMBIADO</td>
	                                        </tr>
	                                        <tr>
	                                            <td  style="text-align: left;width:80%">
	                                                <label for="">&nbsp;Perdida de fluido y/o aceite &nbsp;&nbsp;</label>
	                                                Si&nbsp;
	                                                (&nbsp;<?php if (isset($perdidaAceite)): ?>
					                                    <?php if (strtoupper($perdidaAceite) == "SI"): ?>
					                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
					                                    <?php endif; ?>
					                                <?php endif; ?>&nbsp;)
	                                                No&nbsp;
	                                                (&nbsp;<?php if (isset($perdidaAceite)): ?>
					                                    <?php if (strtoupper($perdidaAceite) == "NO"): ?>
					                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
					                                    <?php endif; ?>
					                                <?php endif; ?>&nbsp;)
	                                            </td>
	                                            <td style="width:20%;text-align: center;" align="center">
	                                                <div class="rev_divCheck" style="margin-left: 36%;">
						                                <?php if (isset($cambioAceite)): ?>
						                                    <?php if ($cambioAceite == "1"): ?>
						                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
						                                    <?php endif; ?>
						                                <?php endif; ?>
						                            </div>
	                                            </td>
	                                        </tr>
	                                    </table>

	                                    <table class="" style="width:100%;border-top: 1px solid black;" >
	                                        <tr>
			                                    <td>Bien</td>
			                                    <td>Llenar</td>
			                                    <td></td>
			                                    <td>Bien</td>
			                                    <td>Llenar</td>
			                                    <td></td>
			                                    <td>Bien</td>
			                                    <td>Llenar</td>
			                                    <td></td>
			                                </tr>
			                                <tr>
			                                    <td align="center">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($aceiteMotor)): ?>
			                                                <?php if (strtoupper($aceiteMotor) == "BIEN"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td align="center">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($aceiteMotor)): ?>
			                                                <?php if (strtoupper($aceiteMotor) == "LLENA"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td>Aceite de motor</td>
			                                    <td align="center" style="padding-top:0px!important;">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($direccionHidraulica)): ?>
			                                                <?php if (strtoupper($direccionHidraulica) == "BIEN"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td align="center" style="padding:0px;">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($direccionHidraulica)): ?>
			                                                <?php if (strtoupper($direccionHidraulica) == "LLENA"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td><label style="font-size:8px;">Dirección hidráulica</label></td>
			                                    <td align="center" style="padding:0px;">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($transmision)): ?>
			                                                <?php if (strtoupper($transmision) == "BIEN"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td align="center" style="padding:0px;">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($transmision)): ?>
			                                                <?php if (strtoupper($transmision) == "LLENA"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td>Transmisión (si está equipada con bayoneta de medición)</td>
			                                </tr>
			                                <tr>
			                                    <td align="center" style="padding:0px;">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($fluidoFrenos)): ?>
			                                                <?php if (strtoupper($fluidoFrenos) == "BIEN"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td align="center" style="padding:0px;">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($fluidoFrenos)): ?>
			                                                <?php if (strtoupper($fluidoFrenos) == "LLENA"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td>Deposito fluido frenos</td>
			                                    <td align="center" style="padding:0px;">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($limpiaparabrisas)): ?>
			                                                <?php if (strtoupper($limpiaparabrisas) == "BIEN"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td align="center" style="padding:0px;">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($limpiaparabrisas)): ?>
			                                                <?php if (strtoupper($limpiaparabrisas) == "LLENA"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td>Limpiaparabrisas</td>
			                                    <td align="center">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($refrigerante)): ?>
			                                                <?php if (strtoupper($refrigerante) == "BIEN"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td align="center">
			                                        <div class="rev_divCheck" style="float:left;">
			                                            <?php if (isset($refrigerante)): ?>
			                                                <?php if (strtoupper($refrigerante) == "LLENA"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
			                                        </div>
			                                    </td>
			                                    <td>Deposito recuperación refrigerante</td>
			                                </tr>
	                                    </table>

	                                    <br>
	                                    <table style="width:100%">
	                                    	<?php if ($tipo_vista == "4"): ?>
					            				<tr>
		                                            <td colspan="2" style="text-align: center;"><br></td>
		                                            <td style="width:20%;text-align: center;">
		                                            	<a class="btn btn-info" title="Cambiar Indicador" onclick="cambiar_indicador('1')" data-target="#cambiar_indicador" data-toggle="modal">
							                                <i class="fas fa-pen-fancy" style="font-size: 13px; color:white;"></i>
							                            </a>
		                                            </td>
		                                        </tr>
											<?php endif; ?>
		                                    	
	                                        <tr class="headers_table">
	                                            <td colspan="2" style="text-align: center;background-color:#340f7b; color:white;" >PLUMAS LIMPIAPARABRISAS</td>
	                                            <td style="width:20%;text-align: center;">CAMBIADO</td>
	                                        </tr>
	                                        <tr>
	                                            <td style="text-align: left;">
	                                                <label for="">Prueba de Limpiaparabrisas <br>realizada&nbsp;</label><br>
	                                                Si&nbsp;
	                                                (&nbsp;<?php if (isset($plumasPruebas)): ?>
					                                    <?php if (strtoupper($plumasPruebas) == "SI"): ?>
					                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;">
					                                    <?php endif; ?>
					                                <?php endif; ?>&nbsp;)
	                                                No&nbsp;
	                                                (&nbsp;<?php if (isset($plumasPruebas)): ?>
					                                    <?php if (strtoupper($plumasPruebas) == "NO"): ?>
					                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;">
					                                    <?php endif; ?>
					                                <?php endif; ?>&nbsp;)
	                                            </td>
	                                            <td>
	                                                <div class="rev_divAprobado" style="float:left;"> 
	                                                    <?php if (isset($plumas)): ?>
			                                                <?php if (strtoupper($plumas) == "SI"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divInmediato" style="float:left;"> 
	                                                    <?php if (isset($plumas)): ?>
			                                                <?php if (strtoupper($plumas) == "NO"): ?>
			                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
			                                                <?php endif; ?>
			                                            <?php endif; ?>
	                                                </div>
	                                                <label for="" style="float:left;">&nbsp; Plumas <br>Limpiaparabrisas</label>
	                                                <br>
	                                                <?php echo form_error('plumas', '<span class="error">', '</span>'); ?>
	                                            </td>
	                                            <td style="text-align: center;">
	                                                <div class="rev_divCheck" style="margin-left: 36%;">
						                                <?php if (isset($plumasCambio)): ?>
						                                    <?php if ($plumasCambio == "1"): ?>
						                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
						                                    <?php endif; ?>
						                                <?php endif; ?>
						                            </div>
	                                            </td>
	                                        </tr>
	                                        <tr>
	                                            <td colspan="3">
	                                                <!-- SISTEMAS Y COMPONENTES -->
	                                                <div class="" style="text-align:center; background-color: #340f7b ; color:white;">
	                                                    SISTEMAS / COMPONENTES
	                                                </div>
	                                                <div class="panel">
	                                                    <div class="panel-body">
	                                                        <table style="width:100%;">
	                                                            <tr class="headers_table">
	                                                                <td style="text-align: center;background-color:slategrey; color:white;" >LUCES / PARABRISAS</td>
	                                                                <td style="width:20%;text-align: center;">CAMBIADO</td>
	                                                            </tr>
	                                                            <tr>
	                                                                <td>
	                                                                    <div class="row">
	                                                                        <div class="col-md-3">
	                                                                            <div class="rev_divAprobado" style="float:left;"> 
	                                                                                <?php if (isset($lucesClaxon)): ?>
										                                                <?php if ($lucesClaxon == "Si"): ?>
										                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
										                                                <?php endif; ?>
										                                            <?php endif; ?>
	                                                                            </div>
	                                                                            <div class="rev_divInmediato" style="float:left;"> 
	                                                                                <?php if (isset($lucesClaxon)): ?>
										                                                <?php if ($lucesClaxon == "No"): ?>
										                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
										                                                <?php endif; ?>
										                                            <?php endif; ?>
	                                                                            </div>
	                                                                        </div>
	                                                                        <div class="col-md-9">
	                                                                            Funcionamiento de claxon, luces interiores, luces exteriores luces de giro, luces de emergencia y freno.
	                                                                        </div>
	                                                                    </div>
	                                                                </td>
	                                                                <td style="text-align:center;">
	                                                                    <div class="rev_divCheck" style="margin-left: 36%;">
											                                <?php if (isset($lucesClaxonCambio)): ?>
											                                    <?php if ($lucesClaxonCambio == "1"): ?>
											                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
											                                    <?php endif; ?>
											                                <?php endif; ?>
											                            </div>
	                                                                </td>
	                                                            </tr>
	                                                            <tr style="padding-top:10px;">
	                                                                <td>
	                                                                    <div class="row">
	                                                                        <div class="col-md-3">
	                                                                            <div class="rev_divAprobado" style="float:left;"> 
	                                                                                <?php if (isset($grietasParabrisas)): ?>
										                                                <?php if ($grietasParabrisas == "Si"): ?>
										                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
										                                                <?php endif; ?>
										                                            <?php endif; ?>
	                                                                            </div>
	                                                                            <div class="rev_divInmediato" style="float:left;"> 
	                                                                                <?php if (isset($grietasParabrisas)): ?>
										                                                <?php if ($grietasParabrisas == "No"): ?>
										                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
										                                                <?php endif; ?>
										                                            <?php endif; ?>
	                                                                            </div>
	                                                                        </div>
	                                                                        <div class="col-md-9">
	                                                                            Grietas, roturas y picaduras de parabrisas.
	                                                                        </div>
	                                                                    </div>
	                                                                </td>
	                                                                <td style="text-align:center;">
	                                                                    <div class="rev_divCheck" style="margin-left: 36%;">
											                                <?php if (isset($grietasCambio)): ?>
											                                    <?php if ($grietasCambio == "1"): ?>
											                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
											                                    <?php endif; ?>
											                                <?php endif; ?>
											                            </div>
	                                                                </td>
	                                                            </tr>
	                                                            <tr class="headers_table">
	                                                                <td style="width:70%; text-align: center;background-color:#340f7b; color:white;">BATERÍA</td>
	                                                                <td style="width:30%;text-align: center;">CAMBIADO</td>
	                                                            </tr>
	                                                            <tr style="margin-top:20px;">
	                                                                <td rowspan="2">
	                                                                    <br><br>
	                                                                    <img src="<?php echo base_url().'assets/imgs/nivel_bateria.png'; ?>" style="width:280px; height:40px;">
											                            <?php if (isset($bateriaValor)): ?>
											                                <br>
											                                <img src="<?php echo base_url().'assets/imgs/barra_vertical.png'; ?>" style="width:20px; height:25px;margin-left:<?php echo ($bateriaValor*2.7); ?>px;margin-top: -10px;" class="centradoBarra">
											                            <?php endif; ?>
	                                                                </td>
	                                                                <td align="center">
	                                                                    <div class="rev_divCheck" style="float:left;margin-left: 36%;">
	                                                                        <?php if (isset($bateriaCambio)): ?> 
	                                                                            <?php if ($bateriaCambio == "1"): ?>
	                                                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                                            <?php endif; ?> 
	                                                                        <?php endif; ?>
	                                                                    </div>
	                                                                </td>
	                                                            </tr>
	                                                            <tr>
	                                                                <td>Estado de batería</td>
	                                                            </tr>
	                                                            <tr>
	                                                                <td style="width:290px;">
	                                                                    <label style="color: darkblue;">
	                                                                        <?php if(isset($bateriaValor))  echo $bateriaValor; else echo "0"; ?>%
	                                                                    </label>
	                                                                </td>
	                                                                <td style="margin-left:10px;">
	                                                                    <div class="rev_divAprobado" style="float:left;">
	                                                                        <?php if (isset($bateriaEstado)): ?>
	                                                                            <?php if ($bateriaEstado == "Aprobado"): ?>
	                                                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                                            <?php endif; ?>
	                                                                        <?php endif; ?>
	                                                                    </div>
	                                                                    <div class="rev_divFuturo" style="float:left;">
	                                                                        <?php if (isset($bateriaEstado)): ?>
	                                                                            <?php if ($bateriaEstado == "Futuro"): ?>
	                                                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                                            <?php endif; ?>
	                                                                        <?php endif; ?>
	                                                                    </div>
	                                                                    <div class="rev_divInmediato" style="float:left;">
	                                                                        <?php if (isset($bateriaEstado)): ?>
	                                                                            <?php if ($bateriaEstado == "Inmediato"): ?>
	                                                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                                            <?php endif; ?>
	                                                                        <?php endif; ?>
	                                                                    </div>
	                                                                </td>
	                                                            </tr>
	                                                        </table>
	                                                        <!-- Corriente de arranque -->
	                                                        <table>
	                                                            <tr>
	                                                                <td style="width: 40%;">
	                                                                    Corriente de arranque en frío especificaciones de fabrica
	                                                                </td>
	                                                                <td align="left" style="width: 10%;">
	                                                                    <label style="color: darkblue;"><u>&nbsp;&nbsp;<?php if(isset($frio)) echo $frio;?>&nbsp;&nbsp;</u></label>
	                                                                    
	                                                                    <br>
	                                                                    <label>CCA</label>
	                                                                    <br>
	                                                                    
	                                                                </td>
	                                                                <td style="width: 40%;">
	                                                                    Corriente de arranque en frío real
	                                                                </td>
	                                                                <td align="center" style="width: 10%;">
	                                                                    <label style="color: darkblue;"><u>&nbsp;&nbsp;<?php if(isset($frioReal)) echo $frioReal;?>&nbsp;&nbsp;</u></label>
	                                                                    
	                                                                    <br>
	                                                                    <label>CCA</label>
	                                                                    <br>
	                                                                    
	                                                                </td>
	                                                            </tr>
	                                                        </table>
	                                                    </div>
	                                                </div>
	                                            </td>
	                                        </tr>
	                                    </table>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- panel 3 -->
	                    </div>

	                    <!-- Lado derecho de la pantalla -->
	                    <div class="col-md-6">
	                        <table class="table-responsive">
	                            <tr>
	                                <td colspan="6">
	                                    <h4>SÍMBOLO</h4>
	                                    <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">&nbsp;
	                                    Puede contribuir a la eficiencia del vehículo y la protección del medio ambiente.
	                                </td>
	                            </tr>
	                            <tr>
	                                <td style="padding-right:3px;">
	                                    <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #1f661f;"></div>
	                                </td>
	                                <td>&nbsp;Verificado y aprobado</td>
	                                <td style="padding-right:3px;">
	                                    <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #e6b019;"></div>
	                                </td>
	                                <td>&nbsp;Puede requerir atención en el futuro</td>
	                                <td style="padding-right:3px;">
	                                    <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #e01a14;"></div>
	                                </td>
	                                <td>&nbsp;Requiere atención inmediata</td>
	                            </tr>
	                        </table>

	                        <table class="table-responsive" style="width:100%;border: 1px solid black;">
	                            <tbody>
	                            	<?php if ($tipo_vista == "4"): ?>
					            		<tr>
                                            <td style="text-align: center;"><br></td>
                                            <td style="width:20%;text-align: center;">
                                            	<a class="btn btn-info" title="Cambiar Indicador" onclick="cambiar_indicador('2')" data-target="#cambiar_indicador" data-toggle="modal">
					                                <i class="fas fa-pen-fancy" style="font-size: 13px; color:white;"></i>
					                            </a>
                                            </td>
                                        </tr>
									<?php endif; ?>
	                                <tr class="headers_table" align="center">
	                                    <td style="background-color:gray;color:white;">BANDAS / MANGUERAS</td>
	                                    <td align="center">CAMBIADO</td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($sistemaCalefaccion)): ?>
	                                                        <?php if ($sistemaCalefaccion == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divFuturo" style="float:left;">
	                                                    <?php if (isset($sistemaCalefaccion)): ?>
	                                                        <?php if ($sistemaCalefaccion == "Futuro"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($sistemaCalefaccion)): ?>
	                                                        <?php if ($sistemaCalefaccion == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Pérdidas y/o daños en el sistema de calefacción, ventilación y aire acondicionado y en mangueras/cables.
	                                            </div>
	                                        </div>
	                                    </td>
	                                    <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($sistemaCalefaccionCambio)): ?>
	                                                <?php if ($sistemaCalefaccionCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($sisRefrigeracion)): ?>
	                                                        <?php if ($sisRefrigeracion == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divCheckSC" style="float:left;"></div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($sisRefrigeracion)): ?>
	                                                        <?php if ($sisRefrigeracion == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Sistema de refrigeración del motor, radiador, mangueras y abrazaderas.
	                                            </div>
	                                        </div>
	                                    </td>
	                                    <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($sisRefrigeracionCambio)): ?>
	                                                <?php if ($sisRefrigeracionCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($bandas)): ?>
	                                                        <?php if ($bandas == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divCheckSC" style="float:left;"></div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($bandas)): ?>
	                                                        <?php if ($bandas == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Bandas(s).
	                                            </div>
	                                        </div>
	                                    </td>
	                                    <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($bandasCambio)): ?>
	                                                <?php if ($bandasCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr class="headers_table" align="center">
	                                    <td style="background-color:gray;color:white;">SISTEMA DE FRENOS</td>
	                                    <td align="center">CAMBIADO</td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($sisFrenos)): ?>
	                                                        <?php if ($sisFrenos == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divFuturo" style="float:left;">
	                                                    <?php if (isset($sisFrenos)): ?>
	                                                        <?php if ($sisFrenos == "Futuro"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($sisFrenos)): ?>
	                                                        <?php if ($sisFrenos == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Sistema de frenos (incluye mangueras y freno de mano) &nbsp;
	                                                <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
	                                            </div>
	                                        </div>
	                                   </td>
	                                   <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($sisFrenosCambio)): ?>
	                                                <?php if ($sisFrenosCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr class="headers_table" align="center">
	                                    <td style="background-color:gray;color:white;">DIRECCIÓN Y SUSPENSIÓN</td>
	                                    <td align="center">CAMBIADO</td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($suspension)): ?>
	                                                        <?php if ($suspension == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divFuturo" style="float:left;">
	                                                    <?php if (isset($suspension)): ?>
	                                                        <?php if ($suspension == "Futuro"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($suspension)): ?>
	                                                        <?php if ($suspension == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Pérdidas y/o daños en amortiguadores/puntales y otros
	                                                componentes de la suspensión.
	                                            </div>
	                                        </div>
	                                    </td>
	                                    <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($suspensionCambio)): ?>
	                                                <?php if ($suspensionCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($varillajeDireccion)): ?>
	                                                        <?php if ($varillajeDireccion == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divFuturo" style="float:left;">
	                                                    <?php if (isset($varillajeDireccion)): ?>
	                                                        <?php if ($varillajeDireccion == "Futuro"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($varillajeDireccion)): ?>
	                                                        <?php if ($varillajeDireccion == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Dirección, varillaje de la dirección y juntas de rótula (visual)
	                                            </div>
	                                        </div>
	                                    </td>
	                                    <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($varillajeDireccionCambio)): ?>
	                                                <?php if ($varillajeDireccionCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr class="headers_table" align="center">
	                                    <td style="background-color:gray;color:white;">SISTEMA DE ESCAPE</td>
	                                    <td align="center">CAMBIADO</td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($sisEscape)): ?>
	                                                        <?php if ($sisEscape == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divFuturo" style="float:left;">
	                                                    <?php if (isset($sisEscape)): ?>
	                                                        <?php if ($sisEscape == "Futuro"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($sisEscape)): ?>
	                                                        <?php if ($sisEscape == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Sistema de escape y escudo de calor (pérdidas, daño, piezas sueltas) &nbsp;
	                                                <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
	                                            </div>
	                                        </div>
	                                    </td>
	                                    <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($sisEscapeCambio)): ?>
	                                                <?php if ($sisEscapeCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr class="headers_table" align="center">
	                                    <td style="background-color:gray;color:white;">TREN MOTRIZ</td>
	                                    <td>CAMBIADO</td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($funEmbrague)): ?>
	                                                        <?php if ($funEmbrague == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divFuturo" style="float:left;">
	                                                    <?php if (isset($funEmbrague)): ?>
	                                                        <?php if ($funEmbrague == "Futuro"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($funEmbrague)): ?>
	                                                        <?php if ($funEmbrague == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Funcionamiento del embrague (si está equipado).
	                                            </div>
	                                        </div>
	                                    </td>
	                                    <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($funEmbragueCambio)): ?>
	                                                <?php if ($funEmbragueCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr style="border: 1px solid black;">
	                                    <td>
	                                        <div class="row">
	                                            <div class="col-md-3">
	                                                <div class="rev_divAprobado" style="float:left;">
	                                                    <?php if (isset($flechaCardan)): ?>
	                                                        <?php if ($flechaCardan == "Aprobado"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divFuturo" style="float:left;">
	                                                    <?php if (isset($flechaCardan)): ?>
	                                                        <?php if ($flechaCardan == "Futuro"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                                <div class="rev_divInmediato" style="float:left;">
	                                                    <?php if (isset($flechaCardan)): ?>
	                                                        <?php if ($flechaCardan == "Inmediato"): ?>
	                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                        <?php endif; ?>
	                                                    <?php endif; ?>
	                                                </div>
	                                            </div>
	                                            <div class="col-md-9">
	                                                Transmisión, flecha cardán y lubricación (si necesita).
	                                            </div>
	                                        </div>
	                                    </td>
	                                    <td align="center">
	                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
	                                            <?php if (isset($flechaCardanCambio)): ?>
	                                                <?php if ($flechaCardanCambio == "1"): ?>
	                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
	                                                <?php endif; ?>
	                                            <?php endif; ?>
	                                        </div>
	                                    </td>
	                                </tr>
	                                <tr style="text-align: right ;background-color:#340f7b;color:white">
	                                    <td align="center">PARTE INFERIOR DEL VEHÍCULO</td>
	                                    <td></td>
	                                </tr>
	                                <tr>
	                                    <td>
	                                        <?php if (isset($defectosImg)): ?>
	                                            <?php if ($defectosImg != ""): ?>
	                                                <img src="<?php echo base_url().$defectosImg; ?>" style="width:300px;">
	                                            <?php else: ?>
	                                                <img src="<?php echo base_url().'assets/imgs/car_ext.png'; ?>" style="width:300px;">
	                                            <?php endif; ?>
	                                        <?php else: ?>
	                                            <img src="<?php echo base_url().'assets/imgs/car_ext.png'; ?>" style="width:300px;">
	                                        <?php endif; ?>
	                                    </td>
	                                    <td>
	                                        Anote en el diagrama todos los daños o defectos detectados en la parte
	                                        inferior de la carrocería durante la revisión en el taller
	                                    </td>
	                                </tr>
	                            </tbody>
	                        </table>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Segunda hoja del formulario -->
    <div class="row">
        <div class="col-md-12">
           <table style="border:2px solid black; width:100%; table-responsive">
               <tbody>
                    <tr style="color:white; text-align:center;background-color:#340f7b;">
                        <td colspan="4">DESGASTE DE NEUMÁTICO / FRENO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td style="color:white;background-color:#340f7b;border:1px solid black;">PROFUNDIDAD DE DIBUJO</td>
                        <td style="background-color:green;color:white;">5 mm y mayor</td>
                        <td style="background-color:yellow">3 a 5 mm</td>
                        <td style="background-color:red;color:white;">2 mm y menor</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td style="color:white;background-color:#340f7b;border:1px solid black;">MEDIDA DE BALATAS</td>
                        <td style="background-color:green; color:white;">Mas de 8 mm</td>
                        <td style="background-color:yellow">4 a 6 mm</td>
                        <td style="background-color:red;color:white;"> 3 mm o menos</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">
                            <div class="row" style="padding-left: 15px;">
                                <div class="col-sm-8" style="background-color:lightgrey;">
                                    &nbsp;&nbsp;
                                    <div class="rev_divCheck" style="float:left;">
                                        <?php if (isset($medicionesFrenos)): ?>
                                            <?php if ($medicionesFrenos == "1"): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;No se tomaron mediciones de los frenos en esta visita de servicio.
                                </div>                                    
                            </div>

                            <div class="row" style="padding-left: 15px;">
                                <div class="col-sm-8" style="background-color:lightgrey;">
                                    &nbsp;&nbsp;
                                    <div class="rev_divCheck" style="float:left;">
                                        <?php if (isset($indicadorAceite)): ?>
                                            <?php if ($indicadorAceite == "1"): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;Reinicio del indicador de cambio de aceite.
                                </div>                                    
                            </div>
                        </td>
                        <td colspan="3"><br></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table>
                                <tbody>
                                    <!-- Parámetros del lado izquierdo -->
                                    <td>
                                        <table class="table-responsive">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        Comentarios:<br>
                                                        <p align=justify style="margin-top:10px;color: darkblue;">
                                                            <?php if (isset($comentarios)): ?>
                                                                <?php echo $comentarios; ?>
                                                                <br>
                                                                <?php echo $comentario; ?>
                                                            <?php endif; ?>
                                                        </p>
                                                    </td>
                                                </tr>
                                                <?php if ($tipo_vista == "4"): ?>
								            		<tr>
			                                            <td style="text-align: center;"><br></td>
			                                            <td style="text-align: right;">
			                                            	<a class="btn btn-info" title="Cambiar Indicador" onclick="cambiar_indicador('3')" data-target="#cambiar_indicador" data-toggle="modal">
								                                <i class="fas fa-pen-fancy" style="font-size: 13px; color:white;"></i>
								                            </a>
			                                            </td>
			                                        </tr>
												<?php endif; ?>
                                                <tr class="headers_table" align="center">
                                                    <td style="background-color:lightgrey; color:black;">
                                                        FRENTE IZQUIERDO &nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </td>
                                                    <td>CAMBIADO</td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($pneumaticoFI)): ?>
                                                                        <?php if ($pneumaticoFI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($pneumaticoFI)): ?>
                                                                        <?php if ($pneumaticoFI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($pneumaticoFI)): ?>
                                                                        <?php if ($pneumaticoFI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Profundidad de dibujo del neumático.
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($pneumaticoFImm)) echo $pneumaticoFImm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                                <br>
                                                                <?php echo form_error('profNeumatico', '<span class="error">', '</span>'); ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($pneumaticoFIcambio)): ?>
                                                                <?php if ($pneumaticoFIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($dNeumaticoFI)): ?>
                                                                        <?php if ($dNeumaticoFI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($dNeumaticoFI)): ?>
                                                                        <?php if ($dNeumaticoFI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($dNeumaticoFI)): ?>
                                                                        <?php if ($dNeumaticoFI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Patrón de desgaste/daño del neumático.
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($dNeumaticoFIcambio)): ?>
                                                                <?php if ($dNeumaticoFIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($presionInfladoFI)): ?>
                                                                        <?php if ($presionInfladoFI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($presionInfladoFI)): ?>
                                                                        <?php if ($presionInfladoFI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($presionInfladoFI)): ?>
                                                                        <?php if ($presionInfladoFI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Presión de inflado a PSI según recomendación del fabricante.
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($presionInfladoFIpsi)) echo $presionInfladoFIpsi;?></strong>&nbsp;&nbsp;</u></label> PSI
                                                                <br>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($presionInfladoFIcambio)): ?>
                                                                <?php if ($presionInfladoFIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($espesoBalataFI)): ?>
                                                                        <?php if ($espesoBalataFI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($espesoBalataFI)): ?>
                                                                        <?php if ($espesoBalataFI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($espesoBalataFI)): ?>
                                                                        <?php if ($espesoBalataFI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Espesor de balatas.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($espesorBalataFImm)) echo $espesorBalataFImm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($espesorBalataFIcambio)): ?>
                                                                <?php if ($espesorBalataFIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($espesorDiscoFI)): ?>
                                                                        <?php if ($espesorDiscoFI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($espesorDiscoFI)): ?>
                                                                        <?php if ($espesorDiscoFI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($espesorDiscoFI)): ?>
                                                                        <?php if ($espesorDiscoFI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Espesor de disco.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($espesorDiscoFImm)) echo $espesorDiscoFImm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($espesorDiscoFIcambio)): ?>
                                                                <?php if ($espesorDiscoFIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php if ($tipo_vista == "4"): ?>
								            		<tr>
			                                            <td style="text-align: center;"><br></td>
			                                            <td style="text-align: right;">
			                                            	<a class="btn btn-info" title="Cambiar Indicador" onclick="cambiar_indicador('4')" data-target="#cambiar_indicador" data-toggle="modal">
								                                <i class="fas fa-pen-fancy" style="font-size: 13px; color:white;"></i>
								                            </a>
			                                            </td>
			                                        </tr>
												<?php endif; ?>
                                                <tr class="headers_table" align="center">
                                                    <td style="background-color:lightgrey; color:black;">
                                                        PARTE TRASERA IZQUIERDO &nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </td>
                                                    <td>CAMBIADO</td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($pneumaticoTI)): ?>
                                                                        <?php if ($pneumaticoTI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($pneumaticoTI)): ?>
                                                                        <?php if ($pneumaticoTI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($pneumaticoTI)): ?>
                                                                        <?php if ($pneumaticoTI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Profundidad de dibujo del neumático.
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($pneumaticoTImm)) echo $pneumaticoTImm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($pneumaticoTIcambio)): ?>
                                                                <?php if ($pneumaticoTIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($dNeumaticoTI)): ?>
                                                                        <?php if ($dNeumaticoTI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($dNeumaticoTI)): ?>
                                                                        <?php if ($dNeumaticoTI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($dNeumaticoTI)): ?>
                                                                        <?php if ($dNeumaticoTI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Patrón de desgaste/daño del neumático.
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($dNeumaticoTIcambio)): ?>
                                                                <?php if ($dNeumaticoTIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($presionInfladoTI)): ?>
                                                                        <?php if ($presionInfladoTI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($presionInfladoTI)): ?>
                                                                        <?php if ($presionInfladoTI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($presionInfladoTI)): ?>
                                                                        <?php if ($presionInfladoTI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Presión de inflado a PSI según recomendación del fabricante.
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($presionInfladoTIpsi)) echo $presionInfladoTIpsi;?></strong>&nbsp;&nbsp;</u></label> PSI
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($presionInfladoTIcambio)): ?>
                                                                <?php if ($presionInfladoTIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($espesoBalataTI)): ?>
                                                                        <?php if ($espesoBalataTI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($espesoBalataTI)): ?>
                                                                        <?php if ($espesoBalataTI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($espesoBalataTI)): ?>
                                                                        <?php if ($espesoBalataTI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Espesor de balatas.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($espesorBalataTImm)) echo $espesorBalataTImm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($espesorBalataTIcambio)): ?>
                                                                <?php if ($espesorBalataTIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($espesorDiscoTI)): ?>
                                                                        <?php if ($espesorDiscoTI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($espesorDiscoTI)): ?>
                                                                        <?php if ($espesorDiscoTI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($espesorDiscoTI)): ?>
                                                                        <?php if ($espesorDiscoTI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Espesor de disco.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($espesorDiscoTImm)) echo $espesorDiscoTImm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($espesorDiscoTIcambio)): ?>
                                                                <?php if ($espesorDiscoTIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($diametroTamborTI)): ?>
                                                                        <?php if ($diametroTamborTI == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($diametroTamborTI)): ?>
                                                                        <?php if ($diametroTamborTI == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($diametroTamborTI)): ?>
                                                                        <?php if ($diametroTamborTI == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Diámetro de tambor.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($diametroTamborTImm)) echo $diametroTamborTImm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($diametroTamborTIcambio)): ?>
                                                                <?php if ($diametroTamborTIcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <!-- Parámetros del lado derecho -->
                                    <td>
                                        <table style="border:1px solid black;" class="table-responsive">
                                            <tbody>
                                            	<?php if ($tipo_vista == "4"): ?>
								            		<tr>
			                                            <td style="text-align: center;"><br></td>
			                                            <td style="text-align: right;">
			                                            	<a class="btn btn-info" title="Cambiar Indicador" onclick="cambiar_indicador('5')" data-target="#cambiar_indicador" data-toggle="modal">
								                                <i class="fas fa-pen-fancy" style="font-size: 13px; color:white;"></i>
								                            </a>
			                                            </td>
			                                        </tr>
												<?php endif; ?>
                                                <tr align="center">
                                                    <td style="background-color:lightgrey; color:black;">
                                                        FRENTE DERECHO&nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </td>
                                                    <td>CAMBIADO</td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($pneumaticoFD)): ?>
                                                                        <?php if ($pneumaticoFD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($pneumaticoFD)): ?>
                                                                        <?php if ($pneumaticoFD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($pneumaticoFD)): ?>
                                                                        <?php if ($pneumaticoFD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Profundidad de dibujo del neumático.
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($pneumaticoFDmm)) echo $pneumaticoFDmm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($pneumaticoFDcambio)): ?>
                                                                <?php if ($pneumaticoFDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($dNeumaticoFD)): ?>
                                                                        <?php if ($dNeumaticoFD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($dNeumaticoFD)): ?>
                                                                        <?php if ($dNeumaticoFD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($dNeumaticoFD)): ?>
                                                                        <?php if ($dNeumaticoFD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Patrón de desgaste/daño del neumático.
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($dNeumaticoFDcambio)): ?>
                                                                <?php if ($dNeumaticoFDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($presionInfladoFD)): ?>
                                                                        <?php if ($presionInfladoFD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($presionInfladoFD)): ?>
                                                                        <?php if ($presionInfladoFD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($presionInfladoFD)): ?>
                                                                        <?php if ($presionInfladoFD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Presión de inflado a PSI según recomendación del fabricante.
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($presionInfladoFDpsi)) echo $presionInfladoFDpsi;?></strong>&nbsp;&nbsp;</u></label> PSI
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($presionInfladoFDcambio)): ?>
                                                                <?php if ($presionInfladoFDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($espesoBalataFD)): ?>
                                                                        <?php if ($espesoBalataFD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($espesoBalataFD)): ?>
                                                                        <?php if ($espesoBalataFD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($espesoBalataFD)): ?>
                                                                        <?php if ($espesoBalataFD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Espesor de balatas.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($espesorBalataFDmm)) echo $espesorBalataFDmm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($espesorBalataFDcambio)): ?>
                                                                <?php if ($espesorBalataFDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($espesorDiscoFD)): ?>
                                                                        <?php if ($espesorDiscoFD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($espesorDiscoFD)): ?>
                                                                        <?php if ($espesorDiscoFD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($espesorDiscoFD)): ?>
                                                                        <?php if ($espesorDiscoFD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Espesor de disco.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($espesorDiscoFDmm)) echo $espesorDiscoFDmm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($espesorDiscoFDcambio)): ?>
                                                                <?php if ($espesorDiscoFDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php if ($tipo_vista == "4"): ?>
								            		<tr>
			                                            <td style="text-align: center;"><br></td>
			                                            <td style="text-align: right;">
			                                            	<a class="btn btn-info" title="Cambiar Indicador" onclick="cambiar_indicador('6')" data-target="#cambiar_indicador" data-toggle="modal">
								                                <i class="fas fa-pen-fancy" style="font-size: 13px; color:white;"></i>
								                            </a>
			                                            </td>
			                                        </tr>
												<?php endif; ?>
                                                <tr class="headers_table" align="center">
                                                    <td style="background-color:lightgrey; color:black;">
                                                        PARTE TRASERA DERECHO &nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </td>
                                                    <td>CAMBIADO</td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($pneumaticoTD)): ?>
                                                                        <?php if ($pneumaticoTD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($pneumaticoTD)): ?>
                                                                        <?php if ($pneumaticoTD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($pneumaticoTD)): ?>
                                                                        <?php if ($pneumaticoTD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Profundidad de dibujo del neumático.
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($pneumaticoTDmm)) echo $pneumaticoTDmm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($pneumaticoTDcambio)): ?>
                                                                <?php if ($pneumaticoTDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($dNeumaticoTD)): ?>
                                                                        <?php if ($dNeumaticoTD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($dNeumaticoTD)): ?>
                                                                        <?php if ($dNeumaticoTD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($dNeumaticoTD)): ?>
                                                                        <?php if ($dNeumaticoTD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Patrón de desgaste/daño del neumático.
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($dNeumaticoTDcambio)): ?>
                                                                <?php if ($dNeumaticoTDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($presionInfladoTD)): ?>
                                                                        <?php if ($presionInfladoTD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($presionInfladoTD)): ?>
                                                                        <?php if ($presionInfladoTD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($presionInfladoTD)): ?>
                                                                        <?php if ($presionInfladoTD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Presión de inflado a PSI según recomendación del fabricante.
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($presionInfladoTDpsi)) echo $presionInfladoTDpsi;?></strong>&nbsp;&nbsp;</u></label> PSI
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($presionInfladoTDcambio)): ?>
                                                                <?php if ($presionInfladoTDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($espesoBalataTD)): ?>
                                                                        <?php if ($espesoBalataTD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($espesoBalataTD)): ?>
                                                                        <?php if ($espesoBalataTD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($espesoBalataTD)): ?>
                                                                        <?php if ($espesoBalataTD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Espesor de balatas.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($espesorBalataTDmm)) echo $espesorBalataTDmm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($espesorBalataTDcambio)): ?>
                                                                <?php if ($espesorBalataTDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($espesorDiscoTD)): ?>
                                                                        <?php if ($espesorDiscoTD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($espesorDiscoTD)): ?>
                                                                        <?php if ($espesorDiscoTD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($espesorDiscoTD)): ?>
                                                                        <?php if ($espesorDiscoTD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Espesor de disco.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($espesorDiscoTDmm)) echo $espesorDiscoTDmm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($espesorDiscoTDcambio)): ?>
                                                                <?php if ($espesorDiscoTDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($diametroTamborTD)): ?>
                                                                        <?php if ($diametroTamborTD == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($diametroTamborTD)): ?>
                                                                        <?php if ($diametroTamborTD == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($diametroTamborTD)): ?>
                                                                        <?php if ($diametroTamborTD == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                                Diámetro de tambor.<br>
                                                                <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($diametroTamborTDmm)) echo $diametroTamborTDmm; else echo "0";?></strong>&nbsp;&nbsp;</u></label> mm
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($diametroTamborTDcambio)): ?>
                                                                <?php if ($diametroTamborTDcambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr class="headers_table" align="center">
                                                    <td style="background-color:lightgrey; color:black;">
                                                        NEUMÁTICO DE REFACCIÓN &nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </td>
                                                    <td>CAMBIADO</td>
                                                </tr>
                                                <tr style="border:1px solid black;">
                                                    <td>
                                                      <div class="row">
                                                            <div class="col-md-3">
                                                                <div class="rev_divAprobado" style="float:left;">
                                                                    <?php if (isset($presion)): ?>
                                                                        <?php if ($presion == "Aprobado"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divFuturo" style="float:left;">
                                                                    <?php if (isset($presion)): ?>
                                                                        <?php if ($presion == "Futuro"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                                <div class="rev_divInmediato" style="float:left;">
                                                                    <?php if (isset($presion)): ?>
                                                                        <?php if ($presion == "Inmediato"): ?>
                                                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                        <?php endif; ?>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-9">
                                                              Presión de inflado establecida en
                                                              <label style="color: darkblue;"><u><strong>&nbsp;&nbsp;<?php if(isset($presionPSI)) echo $presionPSI; else echo "0";?></strong>&nbsp;&nbsp;</u></label> PSI
                                                          </div>
                                                      </div>
                                                    </td>
                                                    <td align="center">
                                                        <div class="rev_divCheck" style="float:left;margin-left: 40%;">
                                                            <?php if (isset($presionCambio)): ?>
                                                                <?php if ($presionCambio == "1"): ?>
                                                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;margin-top: -10px;margin-left: -8px;">
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tbody>
                            </table>
                        </td>
                    </tr>
               </tbody>
           </table>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <table width="100%" style="border-bottom:2px solid black;border-left: 2px solid black; border-right: 2px solid black;">
                <thead></thead>
                <tbody>
                    <tr style="background-color:lightgrey;color:black; border: 1px solid black; text-align:center;">
                        <td colspan="">DIAGNÓSTICO</td>
                    </tr>
                    <tr>
                        <td style="padding:7px;">
                            <div class="row">
                                <div class="col-sm-3">
                                    Sistema.<br>
                                    <p align="left" style="text-align: left;width:120px;">
                                        <label style="color: darkblue;"><?php if(isset($sistema)) echo str_replace("\n", "<br>", $sistema); ?></label>
                                    </p>
                                    <br>
                                </div>
                                <div class="col-sm-3">
                                    Componente.<br>
                                    <p align="left" style="text-align: left;width:120px;">
                                        <label style="color: darkblue;"><?php if(isset($componente)) echo str_replace("\n", "<br>", $componente); ?></label>
                                    </p>
                                    <br>
                                </div>
                                <div class="col-sm-3">
                                    Causa raíz.<br>
                                    <p align="left" style="text-align: left;width:120px;">
                                        <label style="color: darkblue;"><?php if(isset($causaRaiz)) echo str_replace("\n", "<br>", $causaRaiz); ?></label>
                                    </p>
                                    <br>
                                </div>
                                <div class="col-sm-3">
                                    <p style="color:blue">DATOS DISTRIBUIDOR</p>
                                    <?=$this->config->item('base_hm_texto')?>
			                        TEL. <?=SUCURSAL_TEL?>
			                        <br><span style="color:darkblue;"><?=SUCURSAL_WEB?></span><br>
						            Un sitio para todas las necesidades de su vehículo.
                                </div>
                            </div>
                        </td>
                   </tr>
                </tbody>
            </table>
        </div>
    </div>

    <br><br>
    <div class="row">
        <div class="col-md-3" align="center">
            <table style="width: 95%;">
            	<tr>
            		<td style="width: 85%;" align="center">
            			<?php if (isset($firmaAsesor)): ?>
			                <?php if ($firmaAsesor != ""): ?>
			                    <img src="<?= base_url().$firmaAsesor; ?>" alt="" style="height:2cm;width:4cm;">
			                <?php else: ?>
			                    <img src="<?= base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:2cm;width:4cm;">
			                <?php endif; ?>
			            <?php else: ?>
			                <img src="<?= base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:2cm;width:4cm;">
			            <?php endif; ?>
            		</td>
            		<td align="center" style="padding-left: 10px;">
            			<?php if ($tipo_vista == "1"): ?>
            				<a type="button" class="btn btn-danger btnfirma" title="Eliminar firma"  onclick="eliminar_firma('asesor')" data-target="#confirmar_eliminar_firma" data-toggle="modal" hidden>
                                <i class="fa fa-trash" style="font-size: 13px; color:white;"></i>
                            </a>
						<?php endif; ?>
            		</td>
            	</tr>
            	<tr>
            		<td align="center">
            			<label style="color: darkblue;"><?php if(isset($asesor)) echo $asesor;?></label>
            		</td>
            		<td>
            			<br>
            		</td>
            	</tr>
            	<tr>
            		<td style="border-top: 1px solid darkblue;" align="center">
            			NOMBRE Y FIRMA DEL ASESOR
            		</td>
            		<td>
            			<br>
            		</td>
            	</tr>
            </table>
        </div>

        <div class="col-md-3" align="center">
            <table style="width: 95%;">
            	<tr>
            		<td style="width: 85%;" align="center">
            			<?php if (isset($firmaTecnico)): ?>
			                <?php if ($firmaTecnico != ""): ?>
			                    <img src="<?= base_url().$firmaTecnico; ?>" alt="" style="height:2cm;width:4cm;">
			                <?php else: ?>
			                    <img src="<?= base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:2cm;width:4cm;">
			                <?php endif; ?>
			            <?php else: ?>
			                <img src="<?= base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:2cm;width:4cm;">
			            <?php endif; ?>
            		</td>
            		<td align="center" style="padding-left: 10px;">
            			<?php if ($tipo_vista == "1"): ?>
            				<a type="button" class="btn btn-danger btnfirma" title="Eliminar firma" onclick="eliminar_firma('tecnico')" data-target="#confirmar_eliminar_firma" data-toggle="modal" <?= ((isset($firmaTecnico)) ? (($firmaTecnico != "") ? '' : 'hidden') : 'hidden') ?>>
                                <i class="fa fa-trash" style="font-size: 13px; color:white;"></i>
                            </a>
						<?php endif; ?>
            		</td>
            	</tr>
            	<tr>
            		<td align="center">
            			<label style="color: darkblue;"><?php if(isset($tecnico)) echo $tecnico;?></label>
            		</td>
            		<td>
            			<br>
            		</td>
            	</tr>
            	<tr>
            		<td style="border-top: 1px solid darkblue;" align="center">
            			NOMBRE Y FIRMA DEL TÉCNICO
            		</td>
            		<td>
            			<br>
            		</td>
            	</tr>
            </table>
        </div>

        <div class="col-md-3" align="center">
        	<table style="width: 95%;">
            	<tr>
            		<td style="width: 85%;" align="center">
            			<?php if (isset($firmaJefeTaller)): ?>
			                <?php if ($firmaJefeTaller != ""): ?>
			                    <img src="<?= base_url().$firmaJefeTaller; ?>" alt="" style="height:2cm;width:4cm;">
			                <?php else: ?>
			                    <img src="<?= base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:2cm;width:4cm;">
			                <?php endif; ?>
			            <?php else: ?>
			                <img src="<?= base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:2cm;width:4cm;">
			            <?php endif; ?>
            		</td>
            		<td align="center" style="padding-left: 10px;">
            			<?php if ($tipo_vista == "1"): ?>
            				<a type="button" class="btn btn-danger btnfirma" title="Eliminar firma" onclick="eliminar_firma('jefeTaller')" data-target="#confirmar_eliminar_firma" data-toggle="modal" <?= ((isset($firmaJefeTaller)) ? (($firmaJefeTaller != "") ? '' : 'hidden') : 'hidden') ?>>
                                <i class="fa fa-trash" style="font-size: 13px; color:white;"></i>
                            </a>
						<?php endif; ?>
            		</td>
            	</tr>
            	<tr>
            		<td align="center">
            			<label style="color: darkblue;"><?php if(isset($jefeTaller)) echo $jefeTaller;?></label>
            		</td>
            		<td>
            			<br>
            		</td>
            	</tr>
            	<tr>
            		<td style="border-top: 1px solid darkblue;" align="center">
            			NOMBRE Y FIRMA DEL JEFE DE TALLER
            		</td>
            		<td>
            			<br>
            		</td>
            	</tr>
            </table>
        </div>

        <div class="col-md-3" align="center">
            <table style="width: 95%;">
            	<tr>
            		<td style="width: 85%;" align="center">
            			<?php if (isset($firmaCliente)): ?>
			                <?php if ($firmaCliente != ""): ?>
			                    <img src="<?= base_url().$firmaCliente; ?>" alt="" style="height:2cm;width:4cm;">
			                <?php else: ?>
			                    <img src="<?= base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:2cm;width:4cm;">
			                <?php endif; ?>
			            <?php else: ?>
			                <img src="<?= base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="height:2cm;width:4cm;">
			            <?php endif; ?>
            		</td>
            		<td align="center" style="padding-left: 10px;">
            			<?php if ($tipo_vista == "1"): ?>
            				<a type="button" class="btn btn-danger btnfirma" title="Eliminar firma" onclick="eliminar_firma('cliente')" data-target="#confirmar_eliminar_firma" data-toggle="modal" hidden>
                                <i class="fa fa-trash" style="font-size: 13px; color:white;"></i>
                            </a>
						<?php endif; ?>
            		</td>
            	</tr>
            	<tr>
            		<td align="center">
            			<label style="color: darkblue;"><?php if(isset($nombreCliente)) echo $nombreCliente;?></label>
            		</td>
            		<td>
            			<br>
            		</td>
            	</tr>
            	<tr>
            		<td style="border-top: 1px solid darkblue;" align="center">
            			NOMBRE Y FIRMA DEL CLIENTE
            		</td>
            		<td>
            			<br>
            		</td>
            	</tr>
            </table>
        </div>
    </div>

    <hr>
    <br>
    <div class="row">
    	<div class="col-sm-4">
    		<br>
    	</div>
    	<div class="col-sm-4"align="center">
            <input type="hidden" id="firma_asesor" name="firma_asesor" value="<?= ((isset($firmaAsesor)) ? (($firmaAsesor != '') ? 1 : 2) : '') ?>">
		    <input type="hidden" id="firma_tecnico" name="firma_tecnico" value="<?= ((isset($firmaTecnico)) ? (($firmaTecnico != '') ? 1 : 2) : '') ?>">
		    <input type="hidden" id="firma_jefetaller" name="firma_jefetaller" value="<?= ((isset($firmaJefeTaller)) ? (($firmaJefeTaller != '') ? 1 : 2) : '') ?>">
		    <input type="hidden" id="firma_cliente" name="firma_cliente" value="<?= ((isset($firmaCliente)) ? (($firmaCliente != '') ? 1 : 2) : '') ?>">

		    <input type="hidden" name="ref_gral" value="<?= ((isset($id_reg_gral)) ? $id_reg_gral : '0') ?>">
		    <input type="hidden" name="ref_gral2" value="<?= ((isset($id_reg_gral2)) ? $id_reg_gral2 : '0') ?>">
		  	<input type="hidden" name="ref_fi" value="<?= ((isset($id_reg_fti)) ? $id_reg_fti : '0') ?>">
		    <input type="hidden" name="ref_ti" value="<?= ((isset($id_reg_tri)) ? $id_reg_tri : '0') ?>">
		    <input type="hidden" name="ref_fd" value="<?= ((isset($id_reg_ftd)) ? $id_reg_ftd : '0') ?>">
		    <input type="hidden" name="ref_td" value="<?= ((isset($id_reg_trd)) ? $id_reg_trd : '0') ?>">
		    <input type="hidden" name="ref_gral3" value="<?= ((isset($id_reg_gral3)) ? $id_reg_gral3 : '0') ?>">

		    <input type="hidden" name="origne_firma" value="">
		    <input type="hidden" id="tipo_vista" value="<?= ((isset($tipo_vista)) ? $tipo_vista : '') ?>">
		    <input type="hidden" name="orden" id="id_cita" value="<?= ((isset($id_cita)) ? $id_cita : '') ?>">
    	</div>
    	<div class="col-sm-4">
    		<br>
    	</div>
    </div>
</div>


<?php if ($tipo_vista == "1"): ?>
	<div class="modal fade" id="confirmar_eliminar_firma" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h3 class="modal-title" align="center" id="titulo_firma">
	                	¿Esta seguro de que quiere eliminar esta firma?
	                </h3>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-sm-12">
	                        <label for="" style="font-size: 16px;font-weight: initial;">
	                        	Nombre de quien realiza la edición:
	                        </label>
	                        <br>
	                        <input type="text" class="form-control" name="nombre_edicion">

	                        <br>
	                        <label for="" style="font-size: 16px;font-weight: initial;">
	                        	Motivo de la edición:
	                        </label>
	                        <br>
	                        <textarea rows='2' class='form-control' name='motivo_edicion' id='motivo_edicion' style='width:90%;text-align:left;'></textarea>
	                        <input type="hidden" name="no_user" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
	                        <input type="hidden" name="nom_user" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">

	                        <br>
	                        <label for="" style="font-size: 16px;font-weight: initial;">
	                        	Evidencias (opcional):
	                        </label>
	                        <br>
	                        <input type="file" id="evidencias" class="form-control" name="archivo[]" accept="image/*" multiple style="width:80%;margin:20px;">

	                        <hr>
	                        <label id="alerta_ftec" style="color: red;font-size: 10px; display: none;">
	                            [Si se elimina la firma del técnico, TODOS los indicadores se van a reiniciar pera que el técnico pueda llenar el formulario y firmar nuevamente]
	                        </label>
	                    </div>
	                </div>
	                <div class="row">
	                    <div class="col-sm-12">
	                    	<i class="fas fa-spinner cargaIcono"></i>
				            <br>
				            <label style="color: red;font-size: 11px; text-transform: uppercase;" id="resultado_proceso"></label>
				            <br>
		                     <button id="proceso_edicion_hm" type="button" class="btn btn-success btn-block">
		                     	Eliminar firma
		                     </button>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	                <h5 style="font-size: 16px;font-weight: bold;" align="center" id="error_envio_f"></h5>
	                <br>
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	            </div>
	        </div>
	    </div>
	</div>
<?php elseif ($tipo_vista == "4"): ?>
	<div class="modal fade" id="cambiar_indicador" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
	    <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h3 class="modal-title" align="center" id="titulo_firma">
	                	Cambiar indicadores multipunto
	                </h3>
	            </div>
	            <div class="modal-body">
	                <div class="row bloques_ind" id="bloque_indicador_1" style="display:none;">
	                	<div class="col-sm-12">
	                		<table style="width: 100%;">
	                			<tr>
	                				<td style="width: 100%;">
	                					<h5 for="">Inidcador: </h5>
				                        <select class="form-control" id="bloque_ind_opc_1" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="1">Plumas Limpiaparabrisas</option>
				                            <option value="2">Funcionamiento de claxon, luces interiores ...</option>
				                            <option value="3">Grietas, roturas y picaduras de parabrisas. </option>
				                            <option value="4">Estado de batería</option>
				                        </select>
	                				</td>
	                			</tr>
	                			<tr>
	                				<td>
	                					<h5 for="">Nuevo Valor: </h5>
				                        <select class="form-control" id="bloque_ind_color_1" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="Aprobado" style="background-color: #1f661f; color: white;">
				                            	Aprobado
				                            </option>
				                            <option value="Futuro" style="background-color: #e6b019; color: white;">
				                            	Requiere atención futura
				                            </option>
				                            <option value="Inmediato" style="background-color: #e01a14; color: white;">
				                            	Requiere atención inmediata
				                            </option>
				                        </select>
	                				</td>
	                			</tr>
	                		</table>
	                	</div>
	                </div>

	                <div class="row bloques_ind" id="bloque_indicador_2" style="display:none;">
	                	<div class="col-sm-12">
	                		<table style="width: 100%;">
	                			<tr>
	                				<td style="width: 100%;">
	                					<h5 for="">Inidcador: </h5>
				                        <select class="form-control" id="bloque_ind_opc_2" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="1">Pérdidas y/o daños en el sistema de calefacción, ventilación y aire acondicionado .. .</option>
				                            <option value="2">Sistema de refrigeración del motor, radiador, mangueras y abrazaderas. </option>
				                            <option value="3">Bandas(s). </option>
				                            <option value="4">Sistema de frenos (incluye mangueras y freno de mano) </option>
				                            <option value="5">Pérdidas y/o daños en amortiguadores/puntales y otros componentes de la suspensión. </option>
				                            <option value="6">Dirección, varillaje de la dirección y juntas de rótula (visual) </option>
				                            <option value="7">Sistema de escape y escudo de calor (pérdidas, daño, piezas sueltas)</option>
				                            <option value="8">Funcionamiento del embrague (si está equipado). </option>
				                            <option value="9">Transmisión, flecha cardán y lubricación (si necesita). </option>
				                        </select>
	                				</td>
	                			</tr>
	                			<tr>
	                				<td>
	                					<h5 for="">Nuevo Valor: </h5>
				                        <select class="form-control" id="bloque_ind_color_2" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="Aprobado" style="background-color: #1f661f; color: white;">
				                            	Aprobado
				                            </option>
				                            <option value="Futuro" style="background-color: #e6b019; color: white;">
				                            	Requiere atención futura
				                            </option>
				                            <option value="Inmediato" style="background-color: #e01a14; color: white;">
				                            	Requiere atención inmediata
				                            </option>
				                        </select>
	                				</td>
	                			</tr>
	                		</table>
	                	</div>
	                </div>

	                <div class="row bloques_ind" id="bloque_indicador_3" style="display:none;">
	                	<div class="col-sm-12">
	                		<table style="width: 100%;">
	                			<tr>
	                				<td style="width: 100%;">
	                					<h5 for="">Inidcador: </h5>
				                        <select class="form-control" id="bloque_ind_opc_3" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="1">Profundidad de dibujo del neumático</option>
				                            <option value="2">Patrón de desgaste/daño del neumático. </option>
				                            <option value="3">Presión de inflado a PSI según recomendación del fabricante</option>
				                            <option value="4">Espesor de balatas.</option>
				                            <option value="5">Espesor de disco.</option>
				                        </select>
	                				</td>
	                			</tr>
	                			<tr>
	                				<td>
	                					<h5 for="">Nuevo Valor: </h5>
				                        <select class="form-control" id="bloque_ind_color_3" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="Aprobado" style="background-color: #1f661f; color: white;">
				                            	Aprobado
				                            </option>
				                            <option value="Futuro" style="background-color: #e6b019; color: white;">
				                            	Requiere atención futura
				                            </option>
				                            <option value="Inmediato" style="background-color: #e01a14; color: white;">
				                            	Requiere atención inmediata
				                            </option>
				                        </select>
	                				</td>
	                			</tr>
	                		</table>
	                	</div>
	                </div>

	                <div class="row bloques_ind" id="bloque_indicador_4" style="display:none;">
	                	<div class="col-sm-12">
	                		<table style="width: 100%;">
	                			<tr>
	                				<td style="width: 100%;">
	                					<h5 for="">Inidcador: </h5>
				                        <select class="form-control" id="bloque_ind_opc_4" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="1">Profundidad de dibujo del neumático.</option>
				                            <option value="2">Patrón de desgaste/daño del neumático. </option>
				                            <option value="3">Presión de inflado a PSI según recomendación del fabricante.</option>
				                            <option value="4">Espesor de balatas.</option>
				                            <option value="5">Espesor de disco.</option>
				                            <option value="6">Diámetro de tambor.</option>
				                        </select>
	                				</td>
	                			</tr>
	                			<tr>
	                				<td>
	                					<h5 for="">Nuevo Valor: </h5>
				                        <select class="form-control" id="bloque_ind_color_4" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="Aprobado" style="background-color: #1f661f; color: white;">
				                            	Aprobado
				                            </option>
				                            <option value="Futuro" style="background-color: #e6b019; color: white;">
				                            	Requiere atención futura
				                            </option>
				                            <option value="Inmediato" style="background-color: #e01a14; color: white;">
				                            	Requiere atención inmediata
				                            </option>
				                        </select>
	                				</td>
	                			</tr>
	                		</table>
	                	</div>
	                </div>

	                <div class="row bloques_ind" id="bloque_indicador_5" style="display:none;">
	                	<div class="col-sm-12">
	                		<table style="width: 100%;">
	                			<tr>
	                				<td style="width: 100%;">
	                					<h5 for="">Inidcador: </h5>
				                        <select class="form-control" id="bloque_ind_opc_5" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="1">Profundidad de dibujo del neumático</option>
				                            <option value="2">Patrón de desgaste/daño del neumático. </option>
				                            <option value="3">Presión de inflado a PSI según recomendación del fabricante</option>
				                            <option value="4">Espesor de balatas.</option>
				                            <option value="5">Espesor de disco.</option>
				                        </select>
	                				</td>
	                			</tr>
	                			<tr>
	                				<td>
	                					<h5 for="">Nuevo Valor: </h5>
				                        <select class="form-control" id="bloque_ind_color_5" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="Aprobado" style="background-color: #1f661f; color: white;">
				                            	Aprobado
				                            </option>
				                            <option value="Futuro" style="background-color: #e6b019; color: white;">
				                            	Requiere atención futura
				                            </option>
				                            <option value="Inmediato" style="background-color: #e01a14; color: white;">
				                            	Requiere atención inmediata
				                            </option>
				                        </select>
	                				</td>
	                			</tr>
	                		</table>
	                	</div>
	                </div>

	                <div class="row bloques_ind" id="bloque_indicador_6" style="display:none;">
	                	<div class="col-sm-12">
	                		<table style="width: 100%;">
	                			<tr>
	                				<td style="width: 100%;">
	                					<h5 for="">Inidcador: </h5>
				                        <select class="form-control" id="bloque_ind_opc_6" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="1">Profundidad de dibujo del neumático.</option>
				                            <option value="2">Patrón de desgaste/daño del neumático. </option>
				                            <option value="3">Presión de inflado a PSI según recomendación del fabricante.</option>
				                            <option value="4">Espesor de balatas.</option>
				                            <option value="5">Espesor de disco.</option>
				                            <option value="6">Diámetro de tambor.</option>
				                        </select>
	                				</td>
	                			</tr>
	                			<tr>
	                				<td>
	                					<h5 for="">Nuevo Valor: </h5>
				                        <select class="form-control" id="bloque_ind_color_6" style="font-size:12px;width:100%;">
				                            <option value="">SELECCIONE ...</option>
				                            <option value="Aprobado" style="background-color: #1f661f; color: white;">
				                            	Aprobado
				                            </option>
				                            <option value="Futuro" style="background-color: #e6b019; color: white;">
				                            	Requiere atención futura
				                            </option>
				                            <option value="Inmediato" style="background-color: #e01a14; color: white;">
				                            	Requiere atención inmediata
				                            </option>
				                        </select>
	                				</td>
	                			</tr>
	                		</table>
	                	</div>
	                </div>

	                <hr>
	                <div class="row">
	                    <div class="col-sm-12">
	                        <label for="" style="font-size: 14px;font-weight: initial;">
	                        	Nombre de quien realiza la edición:
	                        </label>
	                        <br>
	                        <input type="text" class="form-control" name="nombre_edicion_2">

	                        <br>
	                        <label for="" style="font-size: 14px;font-weight: initial;">
	                        	Motivo de la edición:
	                        </label>
	                        <br>
	                        <textarea rows='2' class='form-control' name='motivo_edicion_2' id='motivo_edicion_2' style='width:99%;text-align:left;'></textarea>
	                        <input type="hidden" name="no_user_2" value="<?= (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '') ?>">
	                        <input type="hidden" name="nom_user_2" value="<?= (($this->session->userdata('nombreUsuario')) ? $this->session->userdata('nombreUsuario') : '') ?>">

	                        <br>
	                        <label for="" style="font-size: 16px;font-weight: initial;">
	                        	Evidencias (opcional):
	                        </label>
	                        <br>
	                        <input type="file" id="evidencias" class="form-control" name="archivo[]" accept="image/*" multiple style="width:80%;margin:20px;">

	                        <hr>
	                        <label id="alerta_ftec" style="color: red;font-size: 10px; display: none;">
	                            [Si se elimina la firma del técnico, TODOS los indicadores se van a reiniciar pera que el técnico pueda llenar el formulario y firmar nuevamente]
	                        </label>
	                    </div>
	                </div>
	                <hr>
	                <div class="row">
	                    <div class="col-sm-12">
	                    	<i class="fas fa-spinner cargaIcono"></i>
				            
				            <br>
				            <label style="color: red;font-size: 11px; text-transform: uppercase;" id="resultado_proceso"></label>
				            
				            <br>
		                    <button id="proceso_edicion_hm" type="button" class="btn btn-success btn-block">
		                     	Guardar cambio
		                    </button>
	                    </div>
	                </div>
	            </div>
	            <div class="modal-footer">
	                <h5 style="font-size: 16px;font-weight: bold;" align="center" id="error_envio_f"></h5>
	                <br>
	                <input type="hidden" id="bloque_asignado" name="bloque_asignado">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
	            </div>
	        </div>
	    </div>
	</div>
<?php endif; ?>
	

	
