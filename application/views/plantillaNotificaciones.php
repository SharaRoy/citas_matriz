<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html style="width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;margin:0;">
   <head>
      <meta charset="UTF-8">
      <meta content="width=device-width, initial-scale=1" name="viewport">
      <meta name="x-apple-disable-message-reformatting">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta content="telephone=no" name="format-detection">
      <title>Plantilla Email</title>
      <!--[if (mso 16)]>
      <style type="text/css">    a {text-decoration: none;}    </style>
      <![endif]--> <!--[if gte mso 9]>
      <style>sup { font-size: 100% !important; }</style>
      <![endif]--> <!--[if !mso]>--> 
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">
      <!--<![endif]-->
      <style type="text/css">
         @media (max-width: 600px) { 
            #logo-dms{
               text-align: center;
               margin: 0 auto;
               width: 60% !important;
               display: block;
            }
         }
      </style>
   </head>
   <body style="width:100%;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;margin:0;">

    <div class="row">
        <div class="col-sm-12">
            <table cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top;">
                <thead>
                    <tr>
                        <td colspan="4" >
                            <img src="<?= base_url() ?>assets/imgs/images_correo/header.jpeg" style="width: 100%;">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><br></td>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td colspan="4"><br></td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td colspan="2" align="center">
                            <h3 style="font-weight: bold;text-align: center">Sistema de Notificaciones Automático</h3>

                            <img src="<?= base_url().$this->config->item('logo') ?>" alt="" style="width:60%;">
                            <br>

                            <table cellspacing="0" cellpadding="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                <tr style="border-collapse:collapse;">
                                    <td valign="top" align="center" style="padding:0;margin:0;padding-right:10px;">
                                        <a href="<?= CONST_FACEBOOK ?>" target="_blank">
                                            <img title="Facebook" src="<?= base_url() ?>assets/imgs/images_correo/facebook-logo-black.png" alt="Fb" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;">
                                        </a>
                                    </td>
                                    <td valign="top" align="center" style="padding:0;margin:0;padding-right:10px;">
                                        <a href="<?= CONST_TWITTER ?>" target="_blank">
                                            <img title="Twitter" src="<?= base_url() ?>assets/imgs/images_correo/twitter-logo-black.png" alt="Tw" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;">
                                        </a>
                                    </td>
                                    <td valign="top" align="center" style="padding:0;margin:0;padding-right:10px;">
                                        <a href="<?= CONST_INSTAGRAM ?>" target="_blank">
                                            <img title="Instagram" src="<?= base_url() ?>assets/imgs/images_correo/instagram-logo-black.png" alt="Inst" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;">
                                        </a>
                                    </td>
                                    <td valign="top" align="center" style="padding:0;margin:0;">
                                        <a href="<?= CONST_YOUTUBE ?>" target="_blank">
                                            <img title="Youtube" src="<?= base_url() ?>assets/imgs/images_correo/youtube-logo-black.png" alt="Yt" width="32" height="32" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;">
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                        <td colspan="2" align="center" style="font-weight: lighter;">
                            <?php 
                                $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","viernes","Sábado");
                                $dia = $dias[date("w")];
                                $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
                                $mes = $meses[date("n")-1];
                             ?>
                            <h2><?php if(isset($formularioEnvio)) echo $formularioEnvio; else echo "Hoja Multipuntos"; ?></h2>
                            <p style="font-size: 14px;text-align: justify;">
                                Documentación correspondiente al vehículo <?php if(isset($serie)) echo $serie; else echo "Sin serie"; ?> el día <?php echo $dia.", ".date("d")."/".$mes."/".date("Y"); ?>
                                <br>

                                Puede ver el documento en el siguiente link: <a href="<?php if(isset($url)) echo $url; else echo ''; ?>" target="_blank">Ver Archivo</a>

                                <br>
                                O comuniquese con su asesor de servicio ‭<a href="tel:+52<?=SUCURSAL_TEL?>">Click para llamar <?=SUCURSAL_TEL?></a> 
                            </p>
                        </td>
                        <td>
                            <br>
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4"><br></td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="4" >
                            <img src="<?= base_url() ?>assets/imgs/images_correo/footer.jpeg" style="width: 100%;">
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
   </body>
</html>