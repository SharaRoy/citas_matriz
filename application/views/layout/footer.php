<footer class="main-footer" style="text-align: center;">

</footer>

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>

<script>
    if ($("#respuesta").val() == "0") {
        $('.alert-warning').fadeIn(1000);
        $('.alert-warning').fadeOut(2000);

    }else if ($("#respuesta").val() == "1") {
        $('.alert-success').fadeIn(1000);
        $('.alert-success').fadeOut(2000);
    }else if ($("#respuesta").val() == "2") {
        $('.alert-danger').fadeIn(1000);
        $('.alert-danger').fadeOut(2000);
    }

    $(".AudioModal").on('click',function() {
        var orden = $(this).data("orden");
        var audio = $(this).data("value");
        var base = $("#basePeticion").val();
        // console.log(base);

        //Imprimimos en el modal la informacion
        $("#tituloAudio").text("Evidencia de audio No. de Orden: "+orden);
        $("#reproductor").attr("src",base+""+audio);

    });

    function ocultarPsni(){
        var estado = $("#estadoPsni").val();

        if (estado == "0") {
            $("#psniContenedor").css('display','block');
            $("#estadoPsni").val("1");
        } else {
            $("#psniContenedor").css('display','none');
            $("#estadoPsni").val("0");
        }
    }

    //Proceso para buscar un pdf de voz cliente
    $("#busqueda_vozCliente").on('click',function(){
        var serie = $("#busqueda_tabla_voz").val().trim();
        var base = "<?php echo base_url(); ?>";
        if ((serie != "")&&(serie != " ")) {
            $.ajax({
                  url: "<?php echo base_url(); ?>"+"vozCliente/VC_Diagnostico/existenciaVerifica", 
                  method: 'post',
                  data: {
                      serie: serie,
                  },
                  success:function(resp){

                    console.log(resp);
                    var resultados = resp.split("=");

                    if (resultados[0] == "OK") {
                        $("#linkPDFVoz").css("display","block");
                        $("#linkPDFVoz").attr("href", base+'VCliente2_Revision/'+resultados[1]);
                        $("#error_tabla_voz").text("");
                    }else if (resultados[0] == "OK1") {
                        $("#linkPDFVoz").css("display","block");
                        $("#linkPDFVoz").attr("href", base+'VCliente_Revision/'+resultados[1]);
                        $("#error_tabla_voz").text("");
                    } else {
                        $("#error_tabla_voz").text("No se encontro la voz cliente con ese no. de orden");
                    }
                  //Cierre de success
                  },
                  error:function(error){
                      console.log(error);
                  //Cierre del error
                  }
              //Cierre del ajax
              });
          }
    });

    $("#busqueda_tabla").removeAttr('disabled');

    //Interacciones para cotizacion_multipunto

</script>

<!-- Resivimos los scritp personalizados para cada vista -->
<?php if (isset($include)): ?>
    <?php foreach ($include as $index => $valor): ?>
        <?php echo "<script src='".base_url().$include[$index]."'></script><br>"; ?>
    <?php endforeach; ?>
<?php endif; ?>

<script>
  //Aun con las validaciones, si ya se entrego la unidad, bloqueamos toda edicion
  if ($("#UnidadEntrega").val() == "1") {
      console.log("Finalizo");
      $("input").attr("disabled", true);
      $("textarea").attr("disabled", true);

      $(".cuadroFirma").attr("disabled", true);
      $(".cuadroFirma").attr("data-toggle", false);

      $("#guardarInspeccion").css("display", "none");
      $("#enviarOrdenForm").css("display", "none");
      $("#aceptarCotizacion").css("display", "none");

      $("#superUsuarioA").css("display", "none");
      
      $('#btnCerrarVideo').prop("disabled",false);
      $('#enviarVideo').prop("disabled",false);
  }

</script>
