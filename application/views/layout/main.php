<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
    //Set no caching
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        
        <meta http-equiv="Expires" content="0">
        <meta http-equiv="Last-Modified" content="0">
        <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
        <meta http-equiv="Pragma" content="no-cache">

        <title><?php if(isset($titulo)) echo $titulo; ?></title>
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url()?>assets/imgs/logos/so.png"/>

        <!-- Cargamos las hojas de estilo y complementos para la vista -->
        <?php if(isset($header)): ?>
            <?php echo $header; ?>
        <?php endif; ?>

        <style>
            .panel-flotante{
                padding: 10px;
                background-color: #ffffff;
                color: #6d7277;
                box-shadow: 10px 10px 10px rgba(107, 160, 203, 0.3) !important;
            }

            .ui-slider .ui-slider-range{
                background: blue !important;
            }

            .ui-slider .ui-slider-handle{
                background: 1px solid darkorange;
            }
        </style>
    </head>
    <body class="" oncontextmenu="return false">  <!-- onkeydown="return false" -->
        <div class="wrapper">
            <div class="content-wrapper">
                <?php if(isset($contenido)): ?>
                    <?php echo $contenido; ?>
                <?php endif; ?>
            </div>
        </div>

        <input type="hidden" name="" id="basePeticion" value="<?=base_url()?>">
        <input type="hidden" name="" id="baseCitas" value="<?=BASE_CITAS?>">
        <input type="hidden" name="" id="basGarantias" value="<?=API_GARANTIA?>">
        <!-- Cargamos los script para la plantilla -->
        <?php if(isset($footer)): ?>
            <?php echo $footer; ?>
        <?php endif; ?>

        <?php clearstatcache(); ?>
    </body>
</html>
