<div class="container">
    <form name="" id="formulario" method="post" action="<?=base_url()."ordenServicio/ordenServicio/validateModalArchivo"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso concluido con exito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Ocurrio un error durante el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <h3 align="center" style="color:#340f7b;">
            Subir Arichivo
        </h3>
        <div class="row">
            <div class="col-sm-6">
                <label for="" style="font-size: 13px;font-weight: inherit;">No. de Orden:</label>
                <br>
                <input type="text" name="" class="form-control" value="<?php if(isset($idOrden)) echo $idOrden; else echo "0"; ?>" disabled>
                <?php echo form_error('idOrden', '<br><span class="error">', '</span>'); ?>
            </div>

            <div class="col-sm-6" style="margin-top: 23px;">
                <input type="file" class="btn btn-default" name="archivo[]" value="" multiple>
                <?php echo form_error('archivo', '<br><span class="error">', '</span>'); ?>
            </div>
            <input type="hidden" name="tempFile" value="<?php if(isset($archivoInventario)) echo $archivoInventario;?>">
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" align="center">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="idOrden" id="" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                <input type="submit" class="btn btn-success" id="guardarArchivo" value="<?php if (isset($archivoInventario)) if($archivoInventario != "") echo "Actualizar Archivo"; else echo "Guardar Archivo"; else echo "Guardar Archivo";?>">
            </div>
            <div class="col-sm-4"></div>
        </div>
    </form>

    <br>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6" align="center">
            <?php if (isset($archivos)): ?>
                <?php if (strlen($archivos[0]) > 10): ?>
                    <div class="alert alert-secondary" align="center">
                        <h5>Ya existen un archivos para esta orden.</h5>
                        <?php $indice = 1; ?>
                        <?php for ($i = 0; $i < count($archivos); $i++): ?>
                            <?php if ($archivos[$i] != ""): ?>
                                <a href="<?php echo $archivos[$i]; ?>" class="btn btn-secondary" target="_blank">Archivo (<?php echo $indice; ?>)</a>
                                <?php $indice++; ?>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="col-sm-3"></div>
    </div>
</div>
