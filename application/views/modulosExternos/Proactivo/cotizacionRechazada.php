<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Presupuestos no autorizados</h3>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-3">
                            <h5>Busqueda Gral.:</h5> 
                            <div class="input-group">
                                <!--<div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>-->
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_fechas_presupuesto" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <!--<i class="fa fa-trash"></i>-->
                                Limpiar Busqueda
                            </button>
                        </div>
                        <!-- formulario de busqueda -->
                    </div>
                    
                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;overflow-x: scroll;">
                            <thead>
                                <tr style="font-size:13px;background-color: #eee;">
                                    <td align="center" style="width: 5%;"></td>
                                    <td align="center" style="width: 7%;"><strong>NO. <br>ORDEN</strong></td>
                                    <td align="center" style="width: 9%;"><strong>ORDEN INTELISIS</strong></td>                             
                                    <td align="center" style="width: 9%;"><strong>FECHA DE LA ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>SERIE</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 14%;"><strong>TELEFONO CLIENTE</strong></td>
                                    <td align="center" colspan="" style=""><strong>ESTATUS</strong></td>
                                    <td align="center" colspan="4" style=""><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                           <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $ultimaFecha[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $serie[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $cliente[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php echo $telefono[$index]; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle; <?php if ($estatus[$index] == '1') echo 'background-color: #ec8d8d;'; else echo 'background-color: #ffc65e;'; ?>">
                                                <?php if ($estatus[$index] == "1"): ?>
                                                    RECHAZADA
                                                <?php else: ?>
                                                    INCOMPLETA
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if ($bandera[$index] == "1"): ?>
                                                    <?php if ($identificador[$index] == "M"): ?>
                                                        <a class="btn btn-info" target="_blank" style="color:white;" href='<?=base_url()."Cotizacion_Cliente/".$id_cita_url[$index];?>'>
                                                            VER
                                                        </a>
                                                    <?php else: ?>
                                                        <a class="btn btn-info" target="_blank" style="color:white;" href='<?=base_url()."Cotizacion_Ext_Cliente/".$id_cita_url[$index];?>'>
                                                            VER
                                                        </a>
                                                    <?php endif ?>
                                                <?php else: ?>   
                                                    <?php if ($identificador[$index] == "M"): ?>
                                                        <a class="btn btn-info" target="_blank" style="color:white;" href='<?=base_url()."Presupuesto_Cliente/".$id_cita_url[$index];?>'>
                                                            VER
                                                        </a>
                                                    <?php else: ?>
                                                        <a class="btn btn-info" target="_blank" style="color:white;" href='<?=base_url()."Cotizacion_Ext_Cliente/".$id_cita_url[$index];?>'>
                                                            VER
                                                        </a>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            </td>
                                            
                                            <td align="center" style="width: 5%;"> 
                                                <a href="javascript: void(0);" title="Agregar Cita" onclick="parent.window.location='<?= BASE_CITAS."citas/agendar_cita/".$idOrden[$index]."/0/0/0/1"; ?>'" style="font-size: 18px;color: black;">
                                                    <i class="fas fa-plus"></i>
                                                </a>
                                            </td>
                                            <td align="center" style="">
                                                <a class="addComentarioBtn" onclick="btnAddClick(<?php echo $idProactivo[$index]; ?>)" title="Agregar Comentario" data-id="<?php echo $idProactivo[$index]; ?>" data-target="#addComentario" data-toggle="modal" style="font-size: 18px;color: black;">
                                                    <i class="fas fa-comments"></i>
                                                </a>
                                            </td>
                                            <td align="center" style="">
                                                <a class="historialCometarioBtn" onclick="btnHistorialClick(<?php echo $idProactivo[$index]; ?>)" title="Historial comentarios" data-id="<?php echo $idProactivo[$index]; ?>" data-target="#historialCometario" data-toggle="modal" style="font-size: 18px;color: black;">
                                                    <i class="fas fa-info"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="11" style="width:100%;" align="center">
                                            <h4>Sin registros que mostrar</h4>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- Modal para agregar un comentario -->
<div class="modal fade" id="addComentario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Ingresar comentario</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="row">
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Titulo</h4>
                        <input type="text" class="form-control" name="" id="titulo" value="">
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Fecha</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </div>
                            <input type="date" class="form-control" id="fechaComentario" max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d"); ?>" style="padding-top: 0px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h4>Hora</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-clock"></i>
                            </div>
                            <input type="text" class="form-control" id="HoraComentario" value="<?php echo date("H:m"); ?>">
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Comentario</h4>
                        <textarea rows='3' class='form-control' name="" id='comentario' style='width:100%;text-align:left;font-size:12px;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="idMagigAdd" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnEnviarComentario">Enviar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver el historial de comentarios -->
<div class="modal fade" id="historialCometario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Historial de comentarios</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr>
                                    <td style="width:25%;"><h4 align="center">Fecha</h4></td>
                                    <td style="width:50%;"><h4 align="center">Comentario</h4></td>
                                    <td style="width:25%;"><h4 align="center">Fecha notificación</h4></td>
                                </tr>
                            </thead>
                            <tbody id="tablaComnetario">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="idMagigHistorial" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>