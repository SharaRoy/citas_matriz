<div class="panel-body"> <!--style="border:2px solid black;"-->
    <div class="row">
        <!-- formulario de busqueda por fecha-->
        <div class="col-sm-3">
            <h5>Fecha Inicio</h5>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="far fa-calendar-alt"></i>
                </div>
                <input type="date" class="form-control" id="busqueda_tabla_fecha_Ini"  placeholder="dd/mm/aaaa" max="<?php echo date("Y-m-d"); ?>" value="" style="padding-top: 0px;">
            </div>
        </div>
        <!-- /.formulario de busqueda por fecha-->
        <div class="col-sm-3">
            <h5>Fecha Fin</h5>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="far fa-calendar-alt"></i>
                </div>
                <input type="date" class="form-control" id="busqueda_tabla_fecha_Fin"  placeholder="dd/mm/aaaa" max="<?php echo date("Y-m-d"); ?>" value="" style="padding-top: 0px;">
            </div>
        </div>
        <div class="col-sm-1" align="left">
            <br><br>
            <button type="button" class="btn btn-dark" title="Buscar"  style="color:white;" id="busquedaFechas">
                <!-- Buscar -->
                <i class="fas fa-search"></i>
            </button>
        </div>
        <div class="col-sm-1" align="left">
            <br><br>
            <button type="button" class="btn btn-secondary" title="Limpiar Busqueda" style="color:white;" id="limpiarBusqueda">
                <!-- Limpiar -->
                <i class="fa fa-trash"></i>
            </button>
        </div>
        <div class="col-sm-1"></div>
        <!-- formulario de busqueda -->
        <div class="col-sm-3">
            <h5>Busqueda General</h5>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                </div>
                <!--<input type="text" class="form-control" id="busqueda_tabla" onblur="validaGral()" placeholder="Buscar...">-->
                <input type="text" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
            </div>
        </div>
        <!-- /.formulario de busqueda -->
    </div>

    <br>
    <div class="form-group" align="center">
        <i class="fas fa-spinner cargaIcono"></i>
        <table class="table table-bordered table-responsive" style="width:100%;overflow-x: scroll;">
            <thead>
                <tr style="font-size:14px;background-color: #eee;">
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>NO. <br>ORDEN</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>SERIE</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>REGISTRO</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>ASESOR</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>CLIENTE</strong></td>
                    <td align="center" rowspan="3" style=";vertical-align:middle;">CONTACTO</td>
                    <td align="center" rowspan="3" style="vertical-align:middle;font-size:8px;"><strong>BATERÍA</strong></td>
                    <td align="center" colspan="14" style="">
                        <strong>INDICADORES</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                        &nbsp;Pieza cambiada
                    </td>
                    <td align="center" rowspan="3" colspan="4" style="vertical-align:middle;"><strong></strong></td>
                </tr>
                <tr>
                    <td style="padding:0px;" colspan="3" align="center">FRENTE <br> IZQUIERDO</td>
                    <td style="padding:0px;" colspan="4" align="center">TRASERA <br> IZQUIERDO</td>
                    <td style="padding:0px;" colspan="3" align="center">FRENTE <br> DERECHO</td>
                    <td style="padding:0px;" colspan="4" align="center">TRASERA <br> DERECHO</td>
                </tr>
                <tr style="font-size:9px;">
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DAÑO <br>NEUMÁTICO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">BALATA</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DISCO</td>

                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DAÑO <br>NEUMÁTICO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">BALATA</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DISCO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">TAMBOR</td>

                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DAÑO <br>NEUMÁTICO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">BALATAS</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DISCO</td>

                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DAÑO <br>NEUMÁTICO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">BALATA</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DISCO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">TAMBOR</td>
                </tr>
            </thead>
            <tbody class="campos_buscar">
                <?php if (isset($idOrden)): ?>
                    <?php foreach ($idOrden as $index => $valor): ?>
                        <tr>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo $idOrden[$index]; ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo strtoupper($serie[$index]); ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo $ultimaFecha[$index]; ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo strtoupper($asesor[$index]); ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo strtoupper($cliente[$index]); ?>
                            </td>
                            <td align="center" style="font-weight: initial;padding:0px;vertical-align:middle;">
                                <?php echo strtoupper($contacto[$index]); ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?php if($bateria[$index] == 'V') echo '#c7ecc7;'; elseif($bateria[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($bateria[$index],0,1); ?>-->
                                <?php if ($cambioBareria[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?php if($neumaticoFI[$index] == 'V') echo '#c7ecc7;'; elseif($neumaticoFI[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($neumaticoFI[$index],0,1); ?>-->
                                <?php if ($cambioNeumaticoFI[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?php if($balatasFI[$index] == 'V') echo '#c7ecc7;'; elseif($balatasFI[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($balatasFI[$index],0,1); ?>-->
                                <?php if ($cambioBalataFI[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?php if($discoFI[$index] == 'V') echo '#c7ecc7;'; elseif($discoFI[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($discoFI[$index],0,1); ?>-->
                                <?php if ($cambioDiscoFI[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?php if($neumaticoTI[$index] == 'V') echo '#c7ecc7;'; elseif($neumaticoTI[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($neumaticoTI[$index],0,1); ?>-->
                                <?php if ($cambioNeumaticoTI[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($balatasTI[$index] == 'V') echo '#c7ecc7;'; elseif($balatasTI[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($balatasTI[$index],0,1); ?>-->
                                <?php if ($cambioBalataTI[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($discoTI[$index] == 'V') echo '#c7ecc7;'; elseif($discoTI[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($discoTI[$index],0,1); ?>-->
                                <?php if ($cambioDiscoTI[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($tamborTI[$index] == 'V') echo '#c7ecc7;'; elseif($tamborTI[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($tamborTI[$index],0,1); ?>-->
                                <?php if ($cambioTamborTI[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($neumaticoFD[$index] == 'V') echo '#c7ecc7;'; elseif($neumaticoFD[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($neumaticoFD[$index],0,1); ?>-->
                                <?php if ($cambioNeumaticoFD[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($balatasFD[$index] == 'V') echo '#c7ecc7;'; elseif($balatasFD[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($balatasFD[$index],0,1); ?>-->
                                <?php if ($cambioBalataFD[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($discoFD[$index] == 'V') echo '#c7ecc7;'; elseif($discoFD[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($discoFD[$index],0,1); ?>-->
                                <?php if ($cambioDiscoFD[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($neumaticoTD[$index] == 'V') echo '#c7ecc7;'; elseif($neumaticoTD[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($neumaticoTD[$index],0,1); ?>-->
                                <?php if ($cambioNeumaticoTD[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($balatasTD[$index] == 'V') echo '#c7ecc7;'; elseif($balatasTD[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($balatasTD[$index],0,1); ?>-->
                                <?php if ($cambioBalataTD[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($discoTD[$index] == 'V') echo '#c7ecc7;'; elseif($discoTD[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($discoTD[$index],0,1); ?>-->
                                <?php if ($cambioDiscoTD[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?php if($tamborTD[$index] == 'V') echo '#c7ecc7;'; elseif($tamborTD[$index] == 'A') echo '#f5ff51;'; else echo '#f5acaa;'; ?>" >
                                <!--<?php echo substr($tamborTD[$index],0,1); ?>-->
                                <?php if ($cambioTamborTD[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php else: ?>
                                    <!--<i class="fa fa-square" title="Pieza NO cambiada" style="font-size: 14px;font-weight: bold;color: red;"></i>-->
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;">
                                <a href="<?=base_url()."Multipunto_PDF/".$id_cita_url[$index];?>" class="btn btn-info" target="_blank" style="font-size: 11px;">PDF</a>
                            </td>
                            <td align="center" style="width: 5%;">
                                <!-- <?php echo $idProactivo[$index]; ?>   -->
                                <a href="javascript: void(0);" title="Agregar Cita" onclick="parent.window.location='<?= BASE_CITAS."citas/agendar_cita/0/0/0/".$idProactivo[$index] ?>'" style="font-size: 18px;color: black;">
                                    <i class="fas fa-plus"></i>
                                </a>
                            </td>
                            <td align="center" style="width: 5%;">
                                <a class="addComentarioBtn" onclick="btnAddClick(<?php echo $idProactivo[$index]; ?>)" title="Agregar Comentario" data-id="<?php echo $idProactivo[$index]; ?>" data-target="#addComentario" data-toggle="modal" style="font-size: 18px;color: black;">
                                    <i class="fas fa-comments"></i>
                                </a>
                            </td>
                            <td align="center" style="width: 5%;">
                                <a class="historialCometarioBtn" onclick="btnHistorialClick(<?php echo $idProactivo[$index]; ?>)" title="Historial comentarios" data-id="<?php echo $idProactivo[$index]; ?>" data-target="#historialCometario" data-toggle="modal" style="font-size: 18px;color: black;">
                                    <i class="fas fa-info"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="17">
                            No se encontraron resultados en el sistema
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>

        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
    </div>
</div>

<!-- Modal para agregar un comentario -->
<div class="modal fade" id="addComentario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Ingresar comentario</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="row">
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Titulo</h4>
                        <input type="text" class="form-control" name="" id="titulo" value="">
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Fecha</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </div>
                            <input type="date" class="form-control" id="fechaComentario" max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d"); ?>" style="padding-top: 0px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h4>Hora</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-clock"></i>
                            </div>
                            <input type="text" class="form-control" id="HoraComentario" value="<?php echo date("H:m"); ?>">
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Comentario</h4>
                        <textarea rows='3' class='form-control' name="" id='comentario' style='width:100%;text-align:left;font-size:12px;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="idMagigAdd" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnEnviarComentario">Enviar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver el historial de comentarios -->
<div class="modal fade" id="historialCometario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Historial de comentarios</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr>
                                    <td style="width:25%;"><h4 align="center">Fecha</h4></td>
                                    <td style="width:50%;"><h4 align="center">Comentario</h4></td>
                                    <td style="width:25%;"><h4 align="center">Fecha notificación</h4></td>
                                </tr>
                            </thead>
                            <tbody id="tablaComnetario">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="idMagigHistorial" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>
