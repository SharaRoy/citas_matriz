<div style="margin:10px;back">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-2">
                <br>
            </div>
            <div class="col-sm-8">
                <h3 align="center">Presupuestos no autorizados</h3>
            </div>
            <div class="col-sm-2" align="right">
                <br>
            </div>
        </div>

        <div class="col-md-12">
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <!--<div class="row">
                        <div class="col-md-5">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h5>Busqueda por Orden, Serie, Placas...:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_calidad" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>-->

                    <br>
                    <div class="row">
                        <div class="col-md-12" align="center">
                            <i class="fas fa-spinner cargaIcono"></i>
                            <h5 style="color:red;text-align: center;" id="error_act_doc"></h5>
                            <table <?php if ($presupuestos != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered table-responsive" style="width: 100%;">
                                <thead>
                                    <tr style="background-color: #eee;">
                                    <td align="center" style="width: 5%;"></td>
                                    <td align="center" style="width: 7%;"><strong>NO. <br>ORDEN</strong></td>
                                    <td align="center" style="width: 9%;"><strong>ORDEN INTELISIS</strong></td>                             
                                    <td align="center" style="width: 9%;"><strong>FECHA DE LA ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>SERIE</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 14%;"><strong>TELEFONO CLIENTE</strong></td>
                                    <td align="center" style=""><strong>ESTATUS</strong></td>
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                    <td align="center" style=""><br></td>
                                    </tr>
                                </thead>
                                <tbody id="imprimir_busqueda">
                                    <?php if ($presupuestos != NULL): ?>
                                        <?php foreach ($presupuestos as $i => $registro): ?>
                                            <tr style="font-size:12px;">
                                                <td align="center" style="width:5%;vertical-align: middle;">
                                                    <?= $i+1 ?>
                                                </td>
                                                <td align="center" style="width:5%;vertical-align: middle;">
                                                    <?= $registro->id_cita ?>
                                                </td>
                                                <td align="center" style="width:5%;vertical-align: middle;">
                                                    <?= $registro->folio_externo ?>
                                                </td>
                                                <td align="center" style="width:10%;vertical-align: middle;">
                                                    <?php 
                                                        $fecha = new DateTime($registro->fecha_recepcion);
                                                        echo $fecha->format('d-m-Y');
                                                     ?>
                                                </td>
                                                <td align="center" style="width:10%;vertical-align: middle;">
                                                    <?= $registro->serie ?>
                                                </td>
                                                <td align="center" style="width:10%;vertical-align: middle;">
                                                    <?= $registro->asesor ?>
                                                </td>
                                                <td align="center" style="width:10%;vertical-align: middle;">
                                                    <?= $registro->cliente ?>
                                                </td>
                                                <td align="center" style="width:10%;vertical-align: middle;">
                                                    <?php 
                                                        echo "Tel. 1: ".$registro->telefono_movil;
                                                        if ($registro->otro_telefono != "") {
                                                            echo "<br>Tel. 2: ".$registro->otro_telefono;
                                                        }
                                                     ?>
                                                </td>
                                                <td align="center" style="vertical-align: middle; background-color: <?= (($registro->acepta_cliente == "Val") ? '#ffc65e' : '#ec8d8d') ?>;">
                                                    <?php if ($registro->acepta_cliente == "No"): ?>
                                                        RECHAZADA TOTALMENTE
                                                    <?php else: ?>
                                                        AFECTADA PARCIALMENTE
                                                    <?php endif; ?>
                                                </td>
                                                
                                                <td align="center" style="width:10%;vertical-align: middle;">
                                                    <?php 
                                                        $id = (double)$registro->id_cita*CONST_ENCRYPT;
                                                        $url_id = base64_encode($id);
                                                        $url = str_replace("=", "" ,$url_id);
                                                    ?>

                                                    <?php if ($registro->identificador== "M"): ?>
                                                        <a class="btn btn-info" target="_blank" style="color:white;" href='<?=base_url()."Presupuesto_Cliente/".$url;?>'>
                                                            VER
                                                        </a>
                                                    <?php else: ?>
                                                        <a class="btn btn-info" target="_blank" style="color:white;" href='<?=base_url()."Cotizacion_Ext_Cliente/".$url;?>'>
                                                            VER
                                                        </a>
                                                    <?php endif ?>
                                                </td>

                                                <td align="center" style="width: 5%;"> 
                                                    <a href="javascript: void(0);" title="Agregar Cita" onclick="parent.window.location='<?= BASE_CITAS."citas/agendar_cita/".$registro->id_cita."/0/0/0/1"; ?>'" style="font-size: 18px;color: black;">
                                                        <i class="fas fa-plus"></i>
                                                    </a>
                                                </td>
                                                <td align="center" style="">
                                                    <a class="addComentarioBtn" onclick="btnAddClick(<?= '99'.$registro->id_cita ?>)" title="Agregar Comentario" data-id="<?= '99'.$registro->id_cita ?>" data-target="#addComentario" data-toggle="modal" style="font-size: 18px;color: black;">
                                                        <i class="fas fa-comments"></i>
                                                    </a>
                                                </td>
                                                <td align="center" style="">
                                                    <a class="historialCometarioBtn" onclick="btnHistorialClick(<?= '99'.$registro->id_cita ?>)" title="Historial comentarios" data-id="<?= '99'.$registro->id_cita ?>" data-target="#historialCometario" data-toggle="modal" style="font-size: 18px;color: black;">
                                                        <i class="fas fa-info"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <tr>
                                            <td colspan="13" style="width:100%;" align="center">
                                                <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                                <h4>Sin registros que mostrar</h4>
                                            </td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>

<!--<script>
    //$(document).ready(function () {
        $('#bootstrap-data-table2').DataTable({
            "ordering": false,
            "searching": false,
            "bPaginate": false,
            "paging": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false
        });

        $("#btnBusqueda_calidad").on('click', function (e){
            var campo = $('#busqueda_campo').prop('value');
            //var fecha_inicio = $('#fecha_inicio').prop('value');
            //var fecha_fin = $('#fecha_fin').prop('value');
            $('#error_act_doc').text("");
            // Evitamos que salte el enlace.
            e.preventDefault();

            //if ((fecha_inicio != "")||(campo != "")) {
            if (campo != "") {
                var paqueteDeDatos = new FormData();
                //paqueteDeDatos.append('fecha_inio', fecha_inicio);
                //paqueteDeDatos.append('fecha_fin', fecha_fin);
                paqueteDeDatos.append('campo', campo);

                var base = $("#basePeticion").val();
                $(".cargaIcono").css("display","inline-block");
                $.ajax({
                    url: base+"multipunto/AuditoriaCalidad/buscar_registro",
                    type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
                    contentType: false,
                    data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
                    processData: false,
                    cache: false,
                    success:function(resp){
                        console.log(resp);
                        var data = JSON.parse(resp);
                        //console.log(data);

                        llenar_tabla(data);

                        $(".cargaIcono").css("display","none");
                    //Cierre de success
                    },
                    error:function(error){
                        console.log(error);
                        $(".cargaIcono").css("display","none");
                        $('#error_act_doc').text("Error de carga VERIFICAR.");
                    //Cierre del error
                    }
                //Cierre del ajax
                });
            }else{

            }
                
        });

        function llenar_tabla(data){
            var tabla = $("#imprimir_busqueda");
            tabla.empty();

            var base = $("#basePeticion").val();

            if (data.length > 0) {
                for(i = 0; i < data.length; i++){
                    var ver_doc = "";
                    ver_doc = "<a href='"+base+"Auditoria_Calidad_PDF/"+data[i].id_cita_url+"' class='btn btn-primary' style='font-size: 10px; font-weight: bold;' target='_blank'>"+
                        "VER<br>PDF"+
                    "</a>";

                    //Creamos las celdas en la tabla
                    tabla.append("<tr style='font-size:11px;'>"+
                        //ORDEN
                        "<td align='center'vertical-align: middle;'>"+
                            data[i]["id_cita"]+
                        "</td>"+
                        //FOLIO INTELISIS
                        "<td align='center' style='vertical-align: middle;'>"+data[i].folio_intelisis+"</td>"+
                        //ASESOR
                        "<td align='center' style='vertical-align: middle;'>"+data[i].asesor+"</td>"+
                        //TÉCNICO
                        "<td align='center' style='vertical-align: middle;'>"+data[i].tecnico+"</td>"+
                        //MODELO
                        "<td align='center' style='vertical-align: middle;'>"+data[i].vehiculo+"</td>"+
                        //PLACAS
                        "<td align='center' style='vertical-align: middle;'>"+data[i].placas+"</td>"+
                        //FECHA PROX. SERV.
                        "<td align='center' style='vertical-align: middle;'>"+data[i].fecha_prox+"</td>"+
                        //AUDITOR
                        "<td align='center' style='vertical-align: middle;'>"+data[i].auditor+"</td>"+
                        //SERVICIO REALIZADO EL:
                        "<td align='center' style='vertical-align: middle;'>"+data[i].fecha_registro+"</td>"+
                        //ACCIONES
                        "<td align='center' style='vertical-align: middle;'>"+
                            ""+ver_doc+""+
                        "</td>"+
                    "</tr>");
                }

            } else {
                tabla.append("<tr style='font-size:14px;'>"+
                    "<td align='center' colspan='10'>No se encontraron resultados</td>"+
                "</tr>");
            }
        }
    //});  
</script> -->