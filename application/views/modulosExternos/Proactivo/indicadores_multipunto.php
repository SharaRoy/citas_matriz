<div class="panel-body">
    <div class="row">
        <div class="col-sm-3">
            <h5>Fecha Inicio</h5>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="far fa-calendar-alt"></i>
                </div>
                <input type="date" class="form-control" id="fecha_inicio"  placeholder="dd/mm/aaaa" max="<?php echo date("Y-m-d"); ?>" value="" style="padding-top: 0px;">
            </div>
        </div>
        <div class="col-sm-3">
            <h5>Fecha Fin</h5>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="far fa-calendar-alt"></i>
                </div>
                <input type="date" class="form-control" id="fecha_fin"  placeholder="dd/mm/aaaa" max="<?php echo date("Y-m-d"); ?>" value="" style="padding-top: 0px;">
            </div>
        </div>
        <div class="col-sm-1"></div>
        <div class="col-sm-3">
            <h5>Busqueda General</h5>
            <div class="input-group">
                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
            </div>
        </div>
        <div class="col-sm-1" align="left">
            <br><br>
            <button type="button" class="btn btn-dark" title="Buscar"  style="color:white;" id="busqueda">
                <!-- Buscar -->
                <i class="fas fa-search"></i>
            </button>
        </div>
        <div class="col-sm-1" align="left">
            <br><br>
            <button type="button" class="btn btn-secondary" title="Limpiar Busqueda" style="color:white;" id="limpiarBusqueda">
                <!-- Limpiar -->
                <i class="fa fa-trash"></i>
            </button>
        </div>
    </div>

    <br>
    <div class="form-group" align="center">
        <i class="fas fa-spinner cargaIcono"></i>
        <table class="table table-bordered table-responsive" style="width:100%;overflow-x: scroll;">
            <thead>
                <tr style="font-size:13px;background-color: #eee;">
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>ORDEN</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>ORDEN INTELISIS</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>FECHA RECEPCIÓN</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>SERIE</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>ASESOR</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>TÉCNICO</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>CLIENTE</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;"><strong>CONTACTO</strong></td>
                    <td align="center" rowspan="3" style="vertical-align:middle;font-size:8px;"><strong>BATERÍA</strong></td>
                    <td align="center" colspan="14" style="">
                        <strong>INDICADORES</strong>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                        &nbsp;Pieza cambiada
                    </td>
                    <td align="center" rowspan="3" colspan="4" style="vertical-align:middle;"><strong></strong></td>
                </tr>
                <tr style="font-size:11px;background-color: #ddd;">
                    <td style="padding:0px;" colspan="3" align="center">FRENTE <br> IZQUIERDO</td>
                    <td style="padding:0px;" colspan="4" align="center">TRASERA <br> IZQUIERDO</td>
                    <td style="padding:0px;" colspan="3" align="center">FRENTE <br> DERECHO</td>
                    <td style="padding:0px;" colspan="4" align="center">TRASERA <br> DERECHO</td>
                </tr>
                <tr style="font-size:9px;background-color: #ccc;">
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DAÑO <br>NEUMÁTICO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">BALATA</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DISCO</td>

                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DAÑO <br>NEUMÁTICO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">BALATA</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DISCO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">TAMBOR</td>

                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DAÑO <br>NEUMÁTICO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">BALATAS</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DISCO</td>

                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DAÑO <br>NEUMÁTICO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">BALATA</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">DISCO</td>
                    <td align="center" style="font-weight: initial;font-size:8px;padding:2px;vertical-align:middle;">TAMBOR</td>
                </tr>
            </thead>
            <tbody class="campos_buscar">
                <?php if (isset($id_cita)): ?>
                    <?php foreach ($id_cita as $index => $valor): ?>
                        <tr>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo $id_cita[$index]; ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo strtoupper($folio[$index]); ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo $fecha_recepcion[$index]; ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo strtoupper($serie[$index]); ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo strtoupper($asesor[$index]); ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo strtoupper($tecnico[$index]); ?>
                            </td>
                            <td align="center" style="font-size: 9px;font-weight: initial;vertical-align:middle;">
                                <?php echo strtoupper($cliente[$index]); ?>
                            </td>
                            <td align="center" style="font-weight: initial;padding:0px;vertical-align:middle;">
                                <?php echo strtoupper($telefono[$index]); ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?= $bateria[$index] ?>;" >
                                <?php if ($bateriaCambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?= $neumaticoFI[$index] ?>;" >
                                <?php if ($dNeumaticoFIcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?= $balatasFI[$index] ?> ;" >
                                <?php if ($espesorBalataFIcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?= $discoFI[$index] ?>;" >
                                <?php if ($espesorDiscoFIcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;font-size: 9px;background-color:<?= $neumaticoTI[$index] ?>;" >
                                <?php if ($dNeumaticoTIcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $balatasTI[$index] ?> ;" >
                                <?php if ($espesorBalataTIcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $discoTI[$index] ?> ;" >
                                <?php if ($espesorDiscoTIcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $tamborTI[$index] ?> ;" >
                                <?php if ($diametroTamborTIcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $neumaticoFD[$index] ?> ;" >
                                <?php if ($dNeumaticoFDcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $balatasFD[$index] ?> ;" >
                                <?php if ($espesorBalataFDcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $discoFD[$index] ?> ;" >
                                <?php if ($espesorDiscoFDcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $neumaticoTD[$index] ?> ;" >
                                <?php if ($dNeumaticoTDcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $balatasTD[$index] ?> ;" >
                                <?php if ($espesorBalataTDcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $discoTD[$index] ?> ;" >
                                <?php if ($espesorDiscoTDcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>
                            <td align="center" style="vertical-align:middle;font-size: 8px;background-color:<?= $tamborTD[$index] ?> ;" >
                                <?php if ($diametroTamborTDcambio[$index] == "1"): ?>
                                    <i class="fa fa-check-square" title="Pieza cambiada" style="font-size: 16px;font-weight: bold;color: green;"></i>
                                <?php endif ?>
                            </td>

                            <td align="center" style="vertical-align:middle;">
                                <a href="<?=base_url()."Multipunto_PDF/".$id_cita_url[$index];?>" class="btn btn-info" target="_blank" style="font-size: 11px;">PDF</a>
                            </td>
                            <td align="center" style="width: 5%;"> 
                                <a href="javascript: void(0);" title="Agregar Cita" onclick="parent.window.location='<?= BASE_CITAS."citas/agendar_cita/0/0/0/".$id_cita[$index] ?>'" style="font-size: 18px;color: black;">
                                    <i class="fas fa-plus"></i>
                                </a>
                            </td>
                            <td align="center" style="width: 5%;">
                                <a class="addComentarioBtn" onclick="btnAddClick(<?php echo $id_proactivo[$index]; ?>)" title="Agregar Comentario" data-id="<?php echo $id_proactivo[$index]; ?>" data-target="#addComentario" data-toggle="modal" style="font-size: 18px;color: black;">
                                    <i class="fas fa-comments"></i>
                                </a>
                            </td>
                            <td align="center" style="width: 5%;">
                                <a class="historialCometarioBtn" onclick="btnHistorialClick(<?php echo $id_proactivo[$index]; ?>)" title="Historial comentarios" data-id="<?php echo $id_proactivo[$index]; ?>" data-target="#historialCometario" data-toggle="modal" style="font-size: 18px;color: black;">
                                    <i class="fas fa-info"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="17">
                            No se encontraron resultados en el sistema
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>

         <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar"> -->

        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
    </div>
</div>

<!-- Modal para agregar un comentario -->
<div class="modal fade" id="addComentario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Ingresar comentario</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="row">
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Titulo</h4>
                        <input type="text" class="form-control" name="" id="titulo" value="">
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Fecha</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </div>
                            <input type="date" class="form-control" id="fechaComentario" max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d"); ?>" style="padding-top: 0px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h4>Hora</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-clock"></i>
                            </div>
                            <input type="text" class="form-control" id="HoraComentario" value="<?php echo date("H:m"); ?>">
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Comentario</h4>
                        <textarea rows='3' class='form-control' name="" id='comentario' style='width:100%;text-align:left;font-size:12px;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="idMagigAdd" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnEnviarComentario">Enviar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver el historial de comentarios -->
<div class="modal fade" id="historialCometario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Historial de comentarios</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr>
                                    <td style="width:25%;"><h4 align="center">Fecha</h4></td>
                                    <td style="width:50%;"><h4 align="center">Comentario</h4></td>
                                    <td style="width:25%;"><h4 align="center">Fecha notificación</h4></td>
                                </tr>
                            </thead>
                            <tbody id="tablaComnetario">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="idMagigHistorial" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>
