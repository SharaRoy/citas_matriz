<style media="screen">
    p, label{
        text-align: justify;
        font-size:10px;
        color: #337ab7;
    }

    .tituloTxt{
        font-size:9px;
        color: #337ab7;
    }
    .division{
        background-color: #ddd;
    }
    .headers_table{
        border:1px solid #337ab7;
        border-style: double;
    }
    td,tr{
        padding: 0px 0px 0px 0px !important;
        font-size: 10px;
        color:#337ab7;
    }

</style>

<?php if (isset($nombre)): ?>
    <?php foreach ($nombre as $index => $valor): ?>
        <page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm">
            <table style="width:100%;">
                <tr>
                    <td colspan="7" style="border:1px solid #337ab7;width:670px;" align="center">
                        <h4>FICHA DEL CLIENTE</h4>
                    </td>
                </tr>
                <tr>
                    <td colspan="7"><br></td>
                </tr>
                <tr>
                    <td colspan="7" align="center" style="background-color:#a1ddb9;">
                        <h5>DATOS DEL CLIENTE</h5>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;">
                        <label for=""><strong>CLIENTE:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>NOMBRE:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>APELLIDO PATERNO:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>APELLIDO MATERNO:</strong></label>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;">
                        <label for="" style="color:black;"><?php if(isset($cliente[$index])) echo $cliente[$index]; else echo ""; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($nombre[$index])) echo $nombre[$index]; else echo ""; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($ap[$index])) echo $ap[$index]; else echo ""; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($am[$index])) echo $am[$index]; else echo ""; ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <label for=""><strong>DIRECCIÓN:</strong></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" style="border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($direccion[$index])) echo $direccion[$index]; else echo "Av. siempre viva #45 XX"; ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;">
                        <label for=""><strong>TELÉFONO:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>TELÉFONO ADICIONAL:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>CONTACTO:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>RAZÓN SOCIAL:</strong></label>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($telefono[$index])) echo $telefono[$index]; else echo ""; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($celular[$index])) echo $celular[$index]; else echo ""; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($contacto[$index])) echo $contacto[$index]; else echo ""; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($rzoSocial[$index])) echo $rzoSocial[$index]; else echo ""; ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;">
                        <label for=""><strong>RFC:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td colspan="5">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:20px;"><br></td>
                                <td style="width:193px;" colspan="2">
                                    <label for=""><strong>TELÉFONO SECUNDARIO:</strong></label>
                                </td>
                                <td style="width:10px;"><br></td>
                                <td style="width:193px;" colspan="2">
                                    <label for=""><strong>CORREO:</strong></label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($rfc[$index])) echo $rfc[$index]; else echo "RFC XX"; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td colspan="5">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:20px;"><br></td>
                                <td style="width:193px;border-bottom:0.5px solid #337ab7;" colspan="2">
                                    <label for="" style="color:black;"><?php if(isset($telefonoAdi[$index])) echo $telefonoAdi[$index]; else echo "Correo propio p XX"; ?></label>
                                </td>
                                <td style="width:10px;"><br></td>
                                <td style="width:193px;border-bottom:0.5px solid #337ab7;" colspan="2">
                                    <label for="" style="color:black;"><?php if(isset($correo[$index])) echo $correo[$index]; else echo "Correo alternativo XX"; ?></label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" align="center" style="background-color:#a1ddb9;">
                        <h5>DATOS DEL VEHÍCULO</h5>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;">
                        <label for=""><strong>PLACAS:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>SERIE:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>NO. SINIESTRO:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>KILOMETRAJE</strong></label>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($placas[$index])) echo $placas[$index]; else echo "Placas XX"; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($serie[$index])) echo $serie[$index]; else echo "Modelo del vehículo XX"; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($siniestro[$index])) echo $siniestro[$index]; else echo "Color vehículo XX"; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($km_actual[$index])) echo $km_actual[$index]; else echo "Kilometraje XX"; ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" align="center" style="background-color:#a1ddb9;">
                        <h5>OTROS DATOS</h5>
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;">
                        <label for=""><strong>ASESOR:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>TORRE:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>ORDEN T:</strong></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;">
                        <label for=""><strong>FECHA DE REGISTRO:</strong></label>
                    </td>
                </tr>
                <tr>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($asesor[$index])) echo $asesor[$index]; else echo "ASESOR QUE DA DE ALTA XX"; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($torre[$index])) echo $torre[$index]; else echo "Serie XX"; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($ordent[$index])) echo $ordent[$index]; else echo "NUM DE ORDEN XX"; ?></label>
                    </td>
                    <td style="width:20px;"><br></td>
                    <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                        <label for="" style="color:black;"><?php if(isset($fecha[$index])) echo $fecha[$index]; else echo "FECHA DE REGISTRO XX"; ?></label>
                    </td>
                </tr>
            </table>
        </page>
    <?php endforeach; ?>
<?php else: ?>
    <page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm">
        <table style="width:100%;">
            <tr>
                <td colspan="7" style="border:1px solid #337ab7;width:670px;" align="center">
                    <h4>FICHA DEL CLIENTE</h4>
                </td>
            </tr>
            <tr>
                <td colspan="7"><br></td>
            </tr>
            <tr>
                <td colspan="7" align="center" style="background-color:#a1ddb9;">
                    <h5>DATOS DEL CLIENTE</h5>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                </td>
            </tr>
            <tr>
                <td style="width:123px;">
                    <label for=""><strong>CLIENTE:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>NOMBRE:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>APELLIDO PATERNO:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>APELLIDO MATERNO:</strong></label>
                </td>
            </tr>
            <tr>
                <td style="width:123px;">
                    <label for="" style="color:black;"><?php if(isset($cliente)) echo $cliente; else echo ""; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($nombre)) echo $nombre; else echo ""; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($ap)) echo $ap; else echo ""; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($am)) echo $am; else echo ""; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <label for=""><strong>DIRECCIÓN:</strong></label>
                </td>
            </tr>
            <tr>
                <td colspan="7" style="border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($direccion)) echo $direccion; else echo "Av. siempre viva #45 XX"; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                </td>
            </tr>
            <tr>
                <td style="width:123px;">
                    <label for=""><strong>TELÉFONO:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>TELÉFONO ADICIONAL:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>CONTACTO:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>RAZÓN SOCIAL:</strong></label>
                </td>
            </tr>
            <tr>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($telefono)) echo $telefono; else echo ""; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($celular)) echo $celular; else echo ""; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($contacto)) echo $contacto; else echo ""; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($rzoSocial)) echo $rzoSocial; else echo ""; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                </td>
            </tr>
            <tr>
                <td style="width:123px;">
                    <label for=""><strong>RFC:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td colspan="5">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:20px;"><br></td>
                            <td style="width:193px;" colspan="2">
                                <label for=""><strong>TELÉFONO SECUNDARIO:</strong></label>
                            </td>
                            <td style="width:10px;"><br></td>
                            <td style="width:193px;" colspan="2">
                                <label for=""><strong>CORREO:</strong></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($rfc)) echo $rfc; else echo "RFC XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td colspan="5">
                    <table style="width:100%;">
                        <tr>
                            <td style="width:20px;"><br></td>
                            <td style="width:193px;border-bottom:0.5px solid #337ab7;" colspan="2">
                                <label for="" style="color:black;"><?php if(isset($telefonoAdi)) echo $telefonoAdi; else echo "Correo propio p XX"; ?></label>
                            </td>
                            <td style="width:10px;"><br></td>
                            <td style="width:193px;border-bottom:0.5px solid #337ab7;" colspan="2">
                                <label for="" style="color:black;"><?php if(isset($correo)) echo $correo; else echo "Correo alternativo XX"; ?></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="center" style="background-color:#a1ddb9;">
                    <h5>DATOS DEL VEHÍCULO</h5>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                </td>
            </tr>
            <tr>
                <td style="width:123px;">
                    <label for=""><strong>PLACAS:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>SERIE:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>NO. SINIESTRO:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>KILOMETRAJE</strong></label>
                </td>
            </tr>
            <tr>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($placas)) echo $placas; else echo "Placas XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($serie)) echo $serie; else echo "Modelo del vehículo XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($siniestro)) echo $siniestro; else echo "Color vehículo XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($km_actual)) echo $km_actual; else echo "Kilometraje XX"; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                </td>
            </tr>
            <!-- <tr>
                <td style="width:123px;">
                    <label for=""><strong>AÑO:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>CILINDROS:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>MOTOR:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>TRANSMISIÓN</strong></label>
                </td>
            </tr> -->
            <tr>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($modelo)) echo $modelo; else echo "Año del vehículo XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($cilindros)) echo $cilindros; else echo "Cilindros del vehículo XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($motor)) echo $motor; else echo "Motor vehículo XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($transmision)) echo $transmision; else echo "Transmision XX"; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="center" style="background-color:#a1ddb9;">
                    <h5>OTROS DATOS</h5>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <br>
                </td>
            </tr>
            <tr>
                <td style="width:123px;">
                    <label for=""><strong>ASESOR:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>TORRE:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>ORDEN T:</strong></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;">
                    <label for=""><strong>FECHA DE REGISTRO:</strong></label>
                </td>
            </tr>
            <tr>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($asesor)) echo $asesor; else echo "ASESOR QUE DA DE ALTA XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($torre)) echo $torre; else echo "Serie XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($ordent)) echo $ordent; else echo "NUM DE ORDEN XX"; ?></label>
                </td>
                <td style="width:20px;"><br></td>
                <td style="width:123px;border-bottom:0.5px solid #337ab7;">
                    <label for="" style="color:black;"><?php if(isset($fecha)) echo $fecha; else echo "FECHA DE REGISTRO XX"; ?></label>
                </td>
            </tr>
        </table>
    </page>
<?php endif; ?>
