<div style="margin:40px;border:1px solid darkblue;">
    <h3 align="center" style="color:#340f7b;font-weight: 300;">
        ORDENES DE : <?php if(isset($tipo_garantia)) echo $tipo_garantia; ?>
    </h3>

    <div class="row" style="margin: 10px;">
        <div class="col-md-6" style="">
            <label for="" style="font-size: 13px;">Estatus garantia:</label>
            <select class="form-control" id="estatus" style="font-size:13px;width:80%;">
                <?php if (isset($id_estatus)): ?>
                    <?php foreach ($id_estatus as $index => $valor): ?>
                        <option value="<?php echo $id_estatus[$index]; ?>" <?php if($estatus == $id_estatus[$index]) echo "selected"; else echo "hidden"; ?>> <?php echo $estatusList[$index]; ?></option>
                    <?php endforeach ?>
                <?php endif ?>
            </select>
            <br>
        </div>
        <div class="col-md-6" style="padding-left: 10px;"></div>
    </div>

    <div class="row" id="cuadro_filtro" style="margin: 10px;">
        <div class="col-md-3" style="">
            <h5>Fecha Inicio:</h5>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                </div>
                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
            </div>
        </div>

        <div class="col-sm-3">
            <h5>Fecha Fin:</h5>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                </div>
                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
            </div>
        </div>

        <!-- formulario de busqueda -->
        <div class="col-md-3">
            <h5>TÉCNICO:</h5>
            <select class="form-control" id="tecnico" style="font-size:13px;width:100%;">
                <option value="" style="">SELECCIONAR ..</option>
                <?php if (isset($tecnico)): ?>
                    <?php foreach ($tecnico as $index => $valor): ?>
                        <option value="<?php echo $idTec[$index]; ?>" style=""><?php echo $tecnico[$index]; ?></option>
                    <?php endforeach ?>
                <?php endif ?>
            </select>
        </div>
        <!-- /.formulario de busqueda -->

        <div class="col-md-3" align="left" style="padding-top: 35px;">
            
            <button class="btn btn-secondary" type="button" name="" id="btn_buscar_garantiaV2" style="margin-right: 20px;"> 
                <i class="fa fa-search"></i>
            </button>

            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                <i class="fa fa-trash"></i>
                <!-- Limpiar Busqueda-->
            </button>
        </div>
        <br>
    </div>

    <br>
    <h3 align="center" class="error" id="Alerta_busqueda"></h3>

    <br>
    <h4 align="center" class="" style="color:darkblue;" id="titulo_busqueda_garantia"></h4>
    
    <br>
    <div class="row" style="margin-right: 10px;margin-left: 10px;">
        <!--<div class="col-md-1"></div>-->
        <div class="col-md-12" align="center">
            <i class="fas fa-spinner cargaIcono"></i>
            <br>
            <div id="chart_div" style="width: 100%; "></div>
        </div>
        <!--<div class="col-md-1"></div>-->
    </div>
</div>
