<style media="screen">
    p, label{
        text-align: justify;
        font-size:8px;
        color: #337ab7;
    }

    strong{
        font-size:8px;
        color: #337ab7;
    }

    .tituloTxt{
        font-size:7px;
        color: #337ab7;
    }
    .division{
        background-color: #ddd;
    }
    .headers_table{
        border:1px solid #337ab7;
        border-style: double;
    }
    td,tr{
        padding: 5px 8px 5px 8px !important;
        font-size: 7px;
        color:black;
    }

</style>

<page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm">
    <table style="width:750px;border: 1px solid  #337ab7;margin-left:-20px;margin-top:-10px;">
        <tr>
            <td colspan="5" style="background-color:#a1ddb9;" align="center">
                <label style="font-size:12px;">RESULTADOS DE BUSQUEDA PROACTIVO</label>
            </td>
        </tr>
        <tr>
            <td align="center" style="border-bottom: 1px solid  #337ab7;"><strong>DATOS CLIENTES</strong></td>
            <td align="center" style="border-left: 1px solid  #337ab7;border-bottom: 1px solid  #337ab7;"><strong>DATOS CONTACTO</strong></td>
            <td align="center" style="border-left: 1px solid  #337ab7;border-bottom: 1px solid  #337ab7;"><strong>DATOS VEHÍCULO</strong></td>
            <td align="center" style="border-left: 1px solid  #337ab7;border-bottom: 1px solid  #337ab7;" colspan="2"><strong>OTROS DATOS</strong></td>
        </tr>
        <?php if (isset($nombre)): ?>
            <?php $renglon = 1; ?>
            <?php foreach ($nombre as $index => $valor): ?>
                <tr <?php if(($renglon % 2) == 0) echo "style='background-color:#eee;'"; ?>>
                    <?php $renglon++; ?>
                    <td align="left" style="border-bottom: 0.5px solid  #337ab7;">
                        <strong>NOMBRE:</strong>&nbsp;&nbsp;<?php if(isset($nombre[$index])) echo $nombre[$index]." ".$ap[$index]." ".$am[$index]; else echo ""; ?><br>
                        <strong>DIRECCIÓN:</strong>&nbsp;&nbsp;<label style="font-size: 7px;"><?php if(isset($direccion_1[$index])) echo $direccion_1[$index]; else echo ""; ?><br> 
                        <?php if(isset($direccion_2[$index])) echo $direccion_2[$index]; else echo ""; ?><br>
                        <?php if(isset($direccion_3[$index])) echo $direccion_3[$index]; else echo ""; ?><br></label>
                        <strong>R.F.C.:</strong>&nbsp;&nbsp;<?php if(isset($rfc[$index])) echo $rfc[$index]; else echo ""; ?><br>
                        <strong>CONTACTO:</strong>&nbsp;&nbsp;<?php if(isset($contacto[$index])) echo $contacto[$index]; else echo ""; ?><br>
                    </td>
                    <td align="left" style="border-left: 0.5px solid  #337ab7;border-bottom: 0.5px solid  #337ab7;">
                        <strong>TEL. 1:</strong>&nbsp;&nbsp;<?php if(isset($telefono[$index])) echo $telefono[$index]; else echo ""; ?><br>
                        <strong>TEL. 2:</strong>&nbsp;&nbsp;<?php if(isset($celular[$index])) echo $celular[$index]; else echo ""; ?><br>
                        <strong>TEL. 3:</strong>&nbsp;&nbsp;<?php if(isset($telefonoAdi[$index])) echo $telefonoAdi[$index]; else echo ""; ?><br>
                        <strong>CORREO:</strong>&nbsp;&nbsp;<?php if(isset($correo[$index])) echo $correo[$index]; else echo ""; ?><br>
                    </td>
                    <td align="left" style="border-left: 0.5px solid  #337ab7;border-bottom: 0.5px solid  #337ab7;">
                        <strong>SERIE:</strong>&nbsp;&nbsp;<?php if(isset($serie[$index])) echo $serie[$index]; else echo ""; ?><br>
                        <strong>PLACAS:</strong>&nbsp;&nbsp;<?php if(isset($placas[$index])) echo $placas[$index]; else echo ""; ?><br>
                        <strong>SINIESTRO:</strong>&nbsp;&nbsp;<?php if(isset($siniestro[$index])) echo $siniestro[$index]; else echo ""; ?><br>
                        <strong>KILOMETRAJE</strong>&nbsp;&nbsp;<?php if(isset($km_actual[$index])) echo $km_actual[$index]; else echo ""; ?>
                    </td>
                    <td align="left" style="border-left: 0.5px solid  #337ab7;border-bottom: 0.5px solid  #337ab7;">
                        <strong>CLIENTE:</strong>&nbsp;&nbsp;<?php if(isset($cliente[$index])) echo $cliente[$index]; else echo ""; ?><br>
                        <strong>RAZÓN SOCIAL:</strong>&nbsp;&nbsp;<?php if(isset($rzoSocial[$index])) echo $rzoSocial[$index]; else echo ""; ?><br>
                        <strong>ASESOR:</strong>&nbsp;&nbsp;<?php if(isset($asesor[$index])) echo $asesor[$index]; else echo ""; ?><br>
                    </td>
                    <td align="left" style="border-left: 0.5px solid  #337ab7;border-bottom: 0.5px solid  #337ab7;">
                        <strong>TORRE:</strong>&nbsp;&nbsp;<?php if(isset($torre[$index])) echo $torre[$index]; else echo ""; ?><br>
                        <strong>ORDEN:</strong>&nbsp;&nbsp;<?php if(isset($ordent[$index])) echo $ordent[$index]; else echo ""; ?><br>
                        <!-- <strong>FECHA DE REGISTRO:</strong>&nbsp;&nbsp;<?php if(isset($fecha[$index])) echo $fecha[$index]; else echo ""; ?><br> -->
                        <strong>ORIGEN DATOS:</strong>&nbsp;&nbsp;<?php if(isset($origen[$index])) echo $origen[$index]; else echo ""; ?><br>
                        <strong>OBSERVACIONES:</strong>&nbsp;&nbsp;
                        <?php if (isset($observaciones[$index])): ?>
                            <?php $cadena = explode(" ",$observaciones[$index]); ?>
                            <?php for ($x = 0; $x <count($cadena); $x++): ?>
                                <?php echo $cadena[$x]; ?>
                                <?php if ((($x+1)%7) == 0): ?>
                                    <br>
                                <?php endif ?>
                            <?php endfor ?>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr>
                <td colspan="5" style="width:750px;" align="center">
                    <strong>NO SE ENCONTRARÓN RESULTADOS DE LA BUSQUEDA</strong>
                </td>
            </tr>
        <?php endif; ?>
    </table>
</page>
