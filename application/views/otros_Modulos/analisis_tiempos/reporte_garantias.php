<div style="margin:10px;back">
    <div class="panel-body">
        <div class="col-md-12 ">
            <h3 align="center">Presupuestos Multipunto Con Garantías</h3>
            <br>
            <div class="panel panel-default table-responsive">
                <div class="panel-body" style="border:2px solid black;">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <!-- <?php if ($reporte != NULL) echo 'id="bootstrap-data-table"'; ?>-->
                    <table  class="table table-striped table-bordered" >
                        <thead>
                            <tr style="background-color: #ccc;">
                                <td align="center" style="vertical-align: middle;"><strong>ORDEN</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ORDEN<br>INTELISIS</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ASESOR</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>MODELO</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>PLACAS</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>SERIE</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>PROCESO</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ULTIMA<br>ACTUALIZACIÓN</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ESTADO/CLIENTE</strong></td>
                            </tr>
                        </thead>
                        <tbody id="imprimir_busqueda">
                            <?php if ($reporte != NULL): ?>
                                <?php foreach ($reporte as $i => $registro): ?>
                                    <tr style="font-size:12px;border-top: 2px solid black;">
                                        <td align="center" style="width:5%;vertical-align: middle;">
                                            <?= $reporte[$i]["id_cita"] ?>
                                        </td>
                                        <td align="center" style="width:7%;vertical-align: middle;">
                                            <?= $reporte[$i]["folio_externo"] ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $reporte[$i]["asesor"] ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $reporte[$i]["tecnico"] ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $reporte[$i]["modelo"] ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $reporte[$i]["placas"] ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $reporte[$i]["serie"] ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= strtoupper($reporte[$i]["ubicacion_proceso"]) ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $reporte[$i]["fecha_actualiza"] ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;<?= $reporte[$i]["afecta_cliente"] ?>">
                                            <?= $reporte[$i]["txtafecta_cliente"] ?>
                                        </td>
                                    </tr>
                                    <tr style="font-size:14px;<?= $reporte[$i]["afecta_cliente"] ?>">
                                        <td>
                                            <br>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ROL</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>PROCESO</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ENCARGADO</strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong>FECHA PROCESO</strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong>TIEMPO DEL PROCESO</strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong>EVENTO DE TERMINO</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="9">
                                            <br>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>TÉCNICO</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ALTA PRESUPUESTO</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong><?= $reporte[$i]["tecnico"] ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= $reporte[$i]["historial"]["registro"]->format('d/m/Y'); ?></strong>
                                            <br>
                                            <strong><?= $reporte[$i]["historial"]["registro"]->format('H:i:s'); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <br>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            ALTA PRESUPUESTO
                                            <br>
                                            <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>VENTANILLA</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>REVISIÓN PROSUPUESTO</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["encargado_ventanilla"] != "") ? $reporte[$i]["historial"]["encargado_ventanilla"] : "VENTANILLA"); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if ($reporte[$i]["historial"]["envio_ventanilla"] != "0"): ?>
                                                <strong><?= $reporte[$i]["historial"]["termina_ventanilla"]->format('d/m/Y'); ?></strong>
                                                <br>
                                                <strong><?= $reporte[$i]["historial"]["termina_ventanilla"]->format('H:i:s'); ?></strong>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["paso_1"] != "PENDIENTE") ? $reporte[$i]["historial"]["paso_1"]." MIN" : $reporte[$i]["historial"]["paso_1"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            TERMINO DE REVISIÓN <br>
                                            <?php if ($reporte[$i]["historial"]["envio_ventanilla"] != "0"): ?>
                                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                            <?php else: ?> 
                                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>JEFE DE TALLER</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>REVISIÓN PROSUPUESTO</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>JUAN CHAVEZ</strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if (($reporte[$i]["historial"]["envio_jefeTaller"] == "1")): ?>
                                                <strong><?= $reporte[$i]["historial"]["termina_JDT"]->format('d/m/Y'); ?></strong>
                                                <br>
                                                <strong><?= $reporte[$i]["historial"]["termina_JDT"]->format('H:i:s'); ?></strong>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["paso_7"] != "PENDIENTE") ? $reporte[$i]["historial"]["paso_7"]." MIN" : $reporte[$i]["historial"]["paso_7"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            FIRMA Y ENVIO JEFE DE TALLER
                                            <br>
                                            <?php if ($reporte[$i]["historial"]["envio_jefeTaller"] == "1"): ?>
                                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                            <?php else: ?> 
                                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>GARANTÍAS</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>REVISIÓN PROSUPUESTO</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ENCARGADO GARANTÍAS</strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if (($reporte[$i]["historial"]["envio_garantia"] == "1")): ?>
                                                <strong><?= $reporte[$i]["historial"]["termino_garantias"]->format('d/m/Y'); ?></strong>
                                                <br>
                                                <strong><?= $reporte[$i]["historial"]["termino_garantias"]->format('H:i:s'); ?></strong>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["paso_8"] != "PENDIENTE") ? $reporte[$i]["historial"]["paso_8"]." MIN" : $reporte[$i]["historial"]["paso_8"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            FIRMA Y ENVIO DE GARANTÍAS
                                            <br>
                                            <?php if ($reporte[$i]["historial"]["garantia"] != ""): ?>
                                                <?php if ($reporte[$i]["historial"]["envio_garantia"] == "1"): ?>
                                                    <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                                <?php else: ?> 
                                                    <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                                <?php endif ?>
                                            <?php else: ?> 
                                                <label style="color:blue;font-weight: bold;">NO APLICA</label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ASESOR</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>FIRMA PRESUPUESTO (REVISIÓN)</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong><?= strtoupper($reporte[$i]["asesor"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if ($reporte[$i]["historial"]["firma_asesor"] != ""): ?>
                                                <strong><?= $reporte[$i]["historial"]["termino_asesor"]->format('d/m/Y'); ?></strong>
                                                <br>
                                                <strong><?= $reporte[$i]["historial"]["termino_asesor"]->format('H:i:s'); ?></strong>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["paso_2"] != "PENDIENTE") ? $reporte[$i]["historial"]["paso_2"]." MIN" : $reporte[$i]["historial"]["paso_2"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            FIRMA DE PRESUPUESTO
                                            <br>
                                            <?php if ($reporte[$i]["historial"]["firma_asesor"] != ""): ?>
                                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                            <?php else: ?> 
                                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ASESOR / CLIENTE</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>AFECTAR PRESUPUESTO</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong><?= strtoupper($reporte[$i]["asesor"]); ?> / 
                                                <br><?= strtoupper($reporte[$i]["cliente"]); ?>
                                            </strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if ($reporte[$i]["historial"]["firma_asesor"] != ""): ?>
                                                <strong><?= $reporte[$i]["historial"]["termino_cliente"]->format('d/m/Y'); ?></strong>
                                                <br>
                                                <strong><?= $reporte[$i]["historial"]["termino_cliente"]->format('H:i:s'); ?></strong>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["paso_3"] != "PENDIENTE") ? $reporte[$i]["historial"]["paso_3"]." MIN" : $reporte[$i]["historial"]["paso_3"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            AFECTAR PRESUPUESTO
                                            <br>
                                            <?php if ($reporte[$i]["historial"]["confirmacion"] != ""): ?>
                                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                            <?php else: ?> 
                                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>VENTANILLA (SOLICITUD DE REFACCIONES)</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ENTREGA DE REFACCIONES I</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["encargado_ventanilla"] != "") ? $reporte[$i]["historial"]["encargado_ventanilla"] : "VENTANILLA"); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if ((int)$reporte[$i]["historial"]["entrega_refaccion"] > 0): ?>
                                                <strong><?= $reporte[$i]["historial"]["solicita_pieza"]->format('d/m/Y'); ?></strong>
                                                <br>
                                                <strong><?= $reporte[$i]["historial"]["solicita_pieza"]->format('H:i:s'); ?></strong>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["paso_4"] != "PENDIENTE") ? $reporte[$i]["historial"]["paso_4"]." MIN" : $reporte[$i]["historial"]["paso_4"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            SOLICITAR REFACCIONES 
                                            <br>
                                            <?php if ((int)$reporte[$i]["historial"]["entrega_refaccion"] > 0): ?>
                                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                            <?php else: ?> 
                                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>VENTANILLA (RECIBIR REFACCIONES)</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ENTREGA DE REFACCIONES II</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["encargado_ventanilla"] != "") ? $reporte[$i]["historial"]["encargado_ventanilla"] : "VENTANILLA"); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if (($reporte[$i]["historial"]["entrega_refaccion"] == "2")||($reporte[$i]["historial"]["entrega_refaccion"] == "3")): ?>
                                                <strong><?= $reporte[$i]["historial"]["recibe_pieza"]->format('d/m/Y'); ?></strong>
                                                <br>
                                                <strong><?= $reporte[$i]["historial"]["recibe_pieza"]->format('H:i:s'); ?></strong>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["paso_5"] != "PENDIENTE") ? $reporte[$i]["historial"]["paso_5"]." MIN" : $reporte[$i]["historial"]["paso_5"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            RECIBIR REFACCIONES
                                            <br>
                                            <?php if (($reporte[$i]["historial"]["entrega_refaccion"] == "2")||($reporte[$i]["historial"]["entrega_refaccion"] == "3")): ?>
                                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                            <?php else: ?> 
                                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>VENTANILLA (ENTREGAR REFACCIONES)</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong>ENTREGA DE REFACCIONES III</strong>
                                        </td>
                                        <td colspan="2" align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["encargado_ventanilla"] != "") ? $reporte[$i]["historial"]["encargado_ventanilla"] : "VENTANILLA"); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if (($reporte[$i]["historial"]["entrega_refaccion"] == "2")): ?>
                                                <strong><?= $reporte[$i]["historial"]["entrega_pieza"]->format('d/m/Y'); ?></strong>
                                                <br>
                                                <strong><?= $reporte[$i]["historial"]["entrega_pieza"]->format('H:i:s'); ?></strong>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <strong><?= (($reporte[$i]["historial"]["paso_6"] != "PENDIENTE") ? $reporte[$i]["historial"]["paso_6"]." MIN" : $reporte[$i]["historial"]["paso_6"]); ?></strong>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            ENTRGA DE REFACCIONES
                                            <br>
                                            <?php if (($reporte[$i]["historial"]["entrega_refaccion"] == "2")): ?>
                                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                            <?php else: ?> 
                                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="11" style="width:100%;" align="center">
                                        <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                        <h4>Sin registros que mostrar</h4>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>
