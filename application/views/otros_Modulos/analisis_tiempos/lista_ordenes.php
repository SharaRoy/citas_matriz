<div style="margin:10px;">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <h3 align="left">Cotizaciones Multipunto</h3>
            </div>
            <!-- formulario de busqueda -->
            <div class="col-md-3">
                <h5>Busqueda No. Orden / Placas/ Serie:</h5>
                <div class="input-group">
                    <!--<div class="input-group-addon">
                        <i class="fa fa-search"></i>
                    </div>-->
                    <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                </div>
            </div>
            <!-- /.formulario de busqueda -->

            <div class="col-md-3" align="right">
                <br><br>
                <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_pGraficas" style="margin-right: 15px;"> 
                    <i class="fa fa-search"></i>
                </button>

                <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                    <!--<i class="fa fa-trash"></i>-->
                    Limpiar Busqueda
                </button>
            </div>
        </div>

        <br>
        <div class="form-group" align="center">
            <i class="fas fa-spinner cargaIcono"></i>
            <table class="table table-bordered table-responsive" style="width: 100%;overflow-y: scroll;overflow-x: scroll;">
                <thead>
                    <tr style="font-size:15px;background-color: #eee;">
                        <td align="center" style="width: 5%;vertical-align: middle;"><strong>ORDEN</strong></td>
                        <td align="center" style="width: 7%;vertical-align: middle;"><strong>ORDEN<br>INTELISIS</strong></td>
                        <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                        <td align="center" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                        <td align="center" style="width: 10%;vertical-align: middle;"><strong>MODELO</strong></td>
                        <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                        <td align="center" style="width: 10%;vertical-align: middle;"><strong>PROCESO</strong></td>
                        <td align="center" style="width: 10%;vertical-align: middle;"><strong>ULTIMA<br>ACTUALIZACIÓN</strong></td>
                        <td align="center" style="width: 10%;vertical-align: middle;"><strong>ESTADO/CLIENTE</strong></td>
                        <td align="center" style="width: 10%;vertical-align: middle;"><strong>ESTADO/REFACCIÓN</strong></td>
                        <td align="center" colspan="2" style=""><strong></strong></td>
                    </tr>
                </thead>
                <tbody id="imprimir_busqueda">
                    <?php if (isset($idOrden)): ?>
                        <?php foreach ($idOrden as $index => $valor): ?>
                            <tr style="font-size:12px;">
                                <td align="center" style="width:5%;vertical-align: middle;">
                                    <?php echo $idOrden[$index]; ?>
                                </td>
                                <td align="center" style="width:7%;vertical-align: middle;">
                                    <?php echo $idIntelisis[$index]; ?>
                                </td>
                                <td align="center" style="width:10%;vertical-align: middle;">
                                    <?php echo $asesor[$index]; ?>
                                </td>
                                <td align="center" style="width:10%;vertical-align: middle;">
                                    <?php echo $tecnico[$index]; ?>
                                </td>
                                <td align="center" style="width:10%;vertical-align: middle;">
                                    <?php echo $modelo[$index]; ?>
                                </td>
                                <td align="center" style="width:10%;vertical-align: middle;">
                                    <?php echo $placas[$index]; ?>
                                </td>
                                <td align="center" style="width:10%;vertical-align: middle;">
                                    <?php echo $proceso[$index]; ?>
                                </td>
                                <td align="center" style="width:10%;vertical-align: middle;">
                                    <!--<?php echo $actualizacion[$index]; ?>-->
                                    <?php if(isset($actualizacion[$index])) echo $actualizacion[$index]->format('d/m/Y'); ?>
                                </td>
                                <td align="center" style="width:10%;vertical-align: middle;">
                                    <?php if ($confirmacion[$index] != ""): ?>
                                        <?php if ($confirmacion[$index] == "Si"): ?>
                                            <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                        <?php elseif ($confirmacion[$index] == "Val"): ?>
                                            <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                        <?php else: ?>
                                            <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                    <?php endif; ?>
                                </td>
                                <td align="center" style="vertical-align: middle;width:10%; background-color:<?php if ($confirmacion[$index] == "No"){ echo "red;color:white;"; } else{ if($entrega_refaccion[$index] == "0") echo '#f5acaa;'; elseif ($entrega_refaccion[$index] == "1") echo '#f5ff51'; elseif ($entrega_refaccion[$index] == "3") echo '#5bc0de'; else echo '#c7ecc7;';}?>">
                                    <?php if ($entrega_refaccion[$index] == "0"): ?>
                                        <?php if ($confirmacion[$index] != "No"): ?>
                                            SIN SOLICITAR                                                        
                                        <?php endif ?>                                                    
                                    <?php elseif ($entrega_refaccion[$index] == "1"): ?>
                                        SOLICITADAS
                                    <?php elseif ($entrega_refaccion[$index] == "3"): ?>
                                        RECIBIDAS
                                    <?php else: ?>
                                        ENTREGADAS
                                    <?php endif ?>
                                </td>
                                <td align="center" style="width:10%;vertical-align: middle;">
                                    <a href="<?=base_url().'Graficas_Cotizacion/'.$id_cita_url[$index];?>" class="btn btn-primary" style="font-size: 10px;font-weight: bold;">
                                        VER<br>PROCESO
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="8" style="width:100%;" align="center">
                                <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                <h4>Sin registros que mostrar</h4>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
