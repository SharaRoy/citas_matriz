<div style="margin:10px;back">
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Presupuestos Multipunto</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <table <?php if ($presupuestos != NULL) echo 'id="bootstrap-data-table"'; ?> class="table table-striped table-bordered table-responsive" >
                        <thead>
                            <tr style="background-color: #eee;">
                                <td align="center" style="vertical-align: middle;"><strong>ORDEN</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ORDEN<br>INTELISIS</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ASESOR</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>MODELO</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>PLACAS</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>PROCESO</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ULTIMA<br>ACTUALIZACIÓN</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ESTADO/CLIENTE</strong></td>
                                <td align="center" style="vertical-align: middle;"><strong>ESTADO/REFACCIÓN</strong></td>
                                <td align="center" style=""><strong></strong></td>
                            </tr>
                        </thead>
                        <tbody id="imprimir_busqueda">
                            <?php if ($presupuestos != NULL): ?>
                                <?php foreach ($presupuestos as $i => $registro): ?>
                                    <tr style="font-size:12px;">
                                        <td align="center" style="width:5%;vertical-align: middle;">
                                            <?= $registro->id_cita ?>
                                        </td>
                                        <td align="center" style="width:7%;vertical-align: middle;">
                                            <?= $registro->folio_externo ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $registro->asesor ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $registro->tecnico ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $registro->vehiculo_modelo ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= $registro->vehiculo_placas ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?= strtoupper($registro->ubicacion_proceso) ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?php 
                                                $fecha = new DateTime($registro->fecha_actualiza);
                                                echo $fecha->format('d/m/Y');
                                             ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?php if ($registro->acepta_cliente != ""): ?>
                                                <?php if ($registro->acepta_cliente == "Si"): ?>
                                                    <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                <?php elseif ($registro->acepta_cliente == "Val"): ?>
                                                    <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                <?php else: ?>
                                                    <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;width:10%; background-color:<?php if ($registro->acepta_cliente == "No"){ echo "red;color:white;"; } else{ if($registro->estado_refacciones == "0") echo '#f5acaa;'; elseif ($registro->estado_refacciones == "1") echo '#f5ff51'; elseif ($registro->estado_refacciones == "3") echo '#5bc0de'; else echo '#c7ecc7;';}?>">
                                            <?php if ($registro->estado_refacciones == "0"): ?>
                                                <?php if ($registro->acepta_cliente != "No"): ?>
                                                    SIN SOLICITAR                                                        
                                                <?php endif ?>                                                    
                                            <?php elseif ($registro->estado_refacciones == "1"): ?>
                                                SOLICITADAS
                                            <?php elseif ($registro->estado_refacciones == "3"): ?>
                                                RECIBIDAS
                                            <?php else: ?>
                                                ENTREGADAS
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?php 
                                                $id = (double)$registro->id_cita*CONST_ENCRYPT;
                                                $url_id = base64_encode($id);
                                                $url = str_replace("=", "" ,$url_id);
                                            ?>
                                            <a href="<?=base_url().'Graficas_Cotizacion/'.$url;?>" class="btn btn-primary" style="font-size: 10px;font-weight: bold;">
                                                VER<br>PROCESO
                                            </a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="11" style="width:100%;" align="center">
                                        <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                        <h4>Sin registros que mostrar</h4>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>
