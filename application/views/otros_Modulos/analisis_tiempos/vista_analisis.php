<div style="margin: 20px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Proceso concluido con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Ocurrio un error durante el proceso.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Campos incompletos.</strong>
    </div>

    <div class="row">
        <div class="col-md-1">
            <button type="button" style="margin-top: 20px;color: white;margin-left: 20px;" class="btn btn-dark" onclick="location.href='<?=base_url()."Graficas_Lista/4";?>';">
                REGRESAR
            </button>
        </div>
        <div class="col-md-10" style="padding-right: 20px;">
            <h3 align="center" style="color:#340f7b;">
                Proceso de la cotización # <?php if(isset($idOrden)) echo $idOrden; else echo "0"; ?>
            </h3>
        </div>
    </div>

    <br>
    <div class="row">
        <div class="col-md-12" style="margin: 20px;" align="center">
            <table class="table table-responsive" style="width: 100%;overflow-y: scroll;">
                <tr style="font-weight:bold;">
                    <td style="width: 20%;">
                        <label for="" style="font-size: 13px;">ORDEN INTELISIS : </label>
                        
                        <br>
                        <label for="" style="font-size: 11px;color: blue;"><?php if(isset($idIntelisis)) if($idIntelisis != NULL) echo $idIntelisis; else echo "...";  else echo "..."; ?></label>&nbsp;
                    </td>
                    <td style="width: 20%;">
                        <label for="" style="font-size: 13px;">ORDEN :</label>
                        
                        <br>
                        <label for="" style="font-size: 11px;color: blue;"><?php if(isset($idOrden)) echo $idOrden; ?></label>&nbsp;
                    </td>
                    <td style="width: 35%;">
                        <label for="" style="font-size: 13px;">SERIE (VIN): </label>
                        
                        <br>
                        <label for="" style="font-size: 11px;color: blue;"><?php if(isset($serie)) echo $serie; ?></label>&nbsp;
                    </td>
                    <td style="width: 25%;">
                        <label for="" style="font-size: 13px;">UNIDAD:</label>
                        
                        <br>
                        <label for="" style="font-size: 11px;color: blue;"><?php if(isset($unidad)) echo $unidad; ?></label>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="" style="font-size: 13px;">MODELO : </label>
                        
                        <br>
                        <label for="" style="font-size: 11px;color: blue;"><?php if(isset($modelo)) echo $modelo; ?></label>&nbsp;
                    </td>
                    <td>
                        <label for="" style="font-size: 13px;">PLACAS : </label>
                        
                        <br>
                        <label for="" style="font-size: 11px;color: blue;"><?php if(isset($placas)) echo $placas; ?></label>&nbsp;
                    </td>
                    <td>
                        <label for="" style="font-size: 13px;">TÉCNICO : </label>
                        
                        <br>
                        <label for="" style="font-size: 11px;color: blue;"><?php if(isset($tecnico)) echo $tecnico; ?></label>&nbsp;
                    </td>
                    <td>
                        <label for="" style="font-size: 13px;">ASESOR : </label>
                        
                        <br>
                        <label for="" style="font-size: 11px;color: blue;"><?php if(isset($asesor)) echo $asesor; ?></label>&nbsp;
                    </td>
                </tr>
            </table>
            
            <br>
            <table class="table table-bordered table-responsive" style="width: 100%;overflow-y: scroll;">
                <?php if (isset($confirmacion)): ?>
                    <?php if ($confirmacion == ""): ?>
                        <tr>
                            <td colspan="6" style="vertical-align: middle;" align="center">
                                <h5 align="center" style="color: darkblue ;">PRESUPUESTO SIN CONFIRMAR</h5>
                            </td>
                        </tr>
                    <?php else: ?>
                        <?php if ($confirmacion == "Si"): ?>
                            <tr>
                                <td colspan="6" style="vertical-align: middle;background-color: #c0f99f;" align="center">
                                    <h5 align="center" style="color: darkgreen; font-weight: bold;">PRESUPUESTO ACEPTADO TOTALMENTE</h5>
                                </td>
                            </tr>
                        <?php elseif ($confirmacion == "No"): ?>
                            <tr>
                                <td colspan="6" style="vertical-align: middle;background-color: #f99fa3;" align="center">
                                    <h5 align="center" style="color: red ; font-weight: bold;">PRESUPUESTO RECHAZADO TOTALMENTE</h5>
                                </td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <td colspan="6" style="vertical-align: middle;background-color: #f9e79f;" align="center">
                                    <h5 align="center" style="color: darkorange; font-weight: bold;">PRESUPUESTO ACEPTADO PARCIALMENTE</h5>
                                </td>
                            </tr>
                        <?php endif ?>
                    <?php endif ?>
                <?php endif ?>
                <tr style="font-size:16px;background-color: #ccc;">
                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ROL</strong></td>
                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>PROCESO</strong></td>
                    <td align="center" style="width: 20%;vertical-align: middle;"><strong>ENCARGADO</strong></td>
                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA PROCESO</strong></td>
                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>TIEMPO DEL PROCESO</strong></td>
                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>EVENTO DE TERMINO</strong></td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        <strong>TÉCNICO</strong>
                    </td>
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        <strong>ALTA PRESUPUESTO</strong>
                    </td>
                    <td align="center" style="width: 20%;vertical-align: middle;">
                        <strong><?php if(isset($tecnico)) echo strtoupper($tecnico); ?></strong>
                    </td>
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        <strong><?php if(isset($registro)) echo $registro->format('d/m/Y'); ?></strong>
                        <br>
                        <strong><?php if(isset($registro)) echo $registro->format('H:i:s'); ?></strong>
                    </td>
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        
                    </td>
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        ALTA PRESUPUESTO
                        <br>
                        <label style="color:green;font-weight: bold;">FINALIZADO</label>
                    </td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        <strong>VENTANILLA</strong>
                    </td>
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        <strong>REVISIÓN PROSUPUESTO</strong>
                    </td>
                    <td align="center" style="width: 20%;vertical-align: middle;">
                        <strong><?php if(isset($encargado_ventanilla)) {echo (($encargado_ventanilla != "") ? $encargado_ventanilla : "VENTANILLA");} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        <?php if (isset($envio_ventanilla)): ?>
                            <?php if ($envio_ventanilla != "0"): ?>
                                <strong><?php if(isset($termina_ventanilla)) echo $termina_ventanilla->format('d/m/Y'); ?></strong>
                                <br>
                                <strong><?php if(isset($termina_ventanilla)) echo $termina_ventanilla->format('H:i:s'); ?></strong>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        <strong><?php if(isset($paso_1)) {echo (($paso_1 != "PENDIENTE") ? $paso_1." MIN" : $paso_1);} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;vertical-align: middle;">
                        TERMINO DE REVISIÓN <br>
                        <?php if (isset($envio_ventanilla)): ?>
                            <?php if ($envio_ventanilla != "0"): ?>
                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                            <?php else: ?> 
                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;">
                        <strong>JEFE DE TALLER</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong>REVISIÓN PROSUPUESTO</strong>
                    </td>
                    <td align="center" style="width: 20%;">
                        <strong>JUAN CHAVEZ</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <?php if (isset($envio_jefeTaller)): ?>
                            <?php if (($envio_jefeTaller == "1")): ?>
                                <strong><?php if(isset($termina_JDT)) echo $termina_JDT->format('d/m/Y'); ?></strong>
                                <br>
                                <strong><?php if(isset($termina_JDT)) echo $termina_JDT->format('H:i:s'); ?></strong>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong><?php if(isset($paso_7)) {echo (($paso_7 != "PENDIENTE") ? $paso_7." MIN" : $paso_7);} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        FIRMA Y ENVIO JEFE DE TALLER
                        <br>
                        <?php if (isset($envio_jefeTaller)): ?>
                            <?php if ($envio_jefeTaller == "1"): ?>
                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                            <?php else: ?> 
                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;">
                        <strong>GARANTÍAS</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong>REVISIÓN PROSUPUESTO</strong>
                    </td>
                    <td align="center" style="width: 20%;">
                        <strong>ENCARGADO GARANTÍAS</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <?php if (isset($envio_garantia)): ?>
                            <?php if (($envio_garantia == "1")): ?>
                                <strong><?php if(isset($termino_garantias)) echo $termino_garantias->format('d/m/Y'); ?></strong>
                                <br>
                                <strong><?php if(isset($termino_garantias)) echo $termino_garantias->format('H:i:s'); ?></strong>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong><?php if(isset($paso_8)) {echo (($paso_8 != "PENDIENTE") ? $paso_8." MIN" : $paso_8);} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        FIRMA Y ENVIO DE GARANTÍAS
                        <br>
                        <?php if (isset($envio_garantia)): ?>
                            <?php if ($garantia != ""): ?>
                                <?php if ($envio_garantia == "1"): ?>
                                    <label style="color:green;font-weight: bold;">FINALIZADO</label>
                                <?php else: ?> 
                                    <label style="color:red;font-weight: bold;">PENDIENTE</label>
                                <?php endif ?>
                            <?php else: ?> 
                                <label style="color:blue;font-weight: bold;">NO APLICA</label>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;">
                        <strong>ASESOR</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong>FIRMA PRESUPUESTO (REVISIÓN)</strong>
                    </td>
                    <td align="center" style="width: 20%;">
                        <strong><?php if(isset($asesor)) echo strtoupper($asesor); ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <?php if (isset($firma_asesor)): ?>
                            <?php if ($firma_asesor != ""): ?>
                                <strong><?php if(isset($termino_asesor)) echo $termino_asesor->format('d/m/Y'); ?></strong>
                                <br>
                                <strong><?php if(isset($termino_asesor)) echo $termino_asesor->format('H:i:s'); ?></strong>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong><?php if(isset($paso_2)) {echo (($paso_2 != "PENDIENTE") ? $paso_2." MIN" : $paso_2);} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        FIRMA DE PRESUPUESTO
                        <br>
                        <?php if (isset($firma_asesor)): ?>
                            <?php if ($firma_asesor != ""): ?>
                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                            <?php else: ?> 
                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;">
                        <strong>ASESOR / CLIENTE</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong>AFECTAR PRESUPUESTO</strong>
                    </td>
                    <td align="center" style="width: 20%;">
                        <strong><?php if(isset($asesor)) echo strtoupper($asesor); ?> / 
                            <br><?php if(isset($cliente)) echo strtoupper($cliente); ?>
                        </strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <?php if (isset($confirmacion)): ?>
                            <?php if ($confirmacion != ""): ?>
                                <strong><?php if(isset($termino_cliente)) echo $termino_cliente->format('d/m/Y'); ?></strong>
                                <br>
                                <strong><?php if(isset($termino_cliente)) echo $termino_cliente->format('H:i:s'); ?></strong>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong><?php if(isset($paso_3)) {echo (($paso_3 != "PENDIENTE") ? $paso_3." MIN" : $paso_3);} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        AFECTAR PRESUPUESTO
                        <br>
                        <?php if (isset($confirmacion)): ?>
                            <?php if ($confirmacion != ""): ?>
                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                            <?php else: ?> 
                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;">
                        <strong>VENTANILLA (SOLICITUD DE REFACCIONES)</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong>ENTREGA DE REFACCIONES I</strong>
                    </td>
                    <td align="center" style="width: 20%;">
                        <strong><?php if(isset($encargado_ventanilla)) {echo (($encargado_ventanilla != "") ? $encargado_ventanilla : "VENTANILLA");} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <?php if (isset($entrega_refaccion)): ?>
                            <?php if ((int)$entrega_refaccion > 0): ?>
                                <strong><?php if(isset($solicita_pieza)) echo $solicita_pieza->format('d/m/Y'); ?></strong>
                                <br>
                                <strong><?php if(isset($solicita_pieza)) echo $solicita_pieza->format('H:i:s'); ?></strong>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong><?php if(isset($paso_4)) {echo (($paso_4 != "PENDIENTE") ? $paso_4." MIN" : $paso_4);} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        SOLICITAR REFACCIONES 
                        <br>
                        <?php if (isset($entrega_refaccion)): ?>
                            <?php if ((int)$entrega_refaccion > 0): ?>
                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                            <?php else: ?> 
                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                            <?php endif ?>
                        <?php endif ?>                            
                    </td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;">
                        <strong>VENTANILLA (RECIBIR REFACCIONES)</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong>ENTREGA DE REFACCIONES II</strong>
                    </td>
                    <td align="center" style="width: 20%;">
                        <strong><?php if(isset($encargado_ventanilla)) {echo (($encargado_ventanilla != "") ? $encargado_ventanilla : "VENTANILLA");} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <?php if (isset($entrega_refaccion)): ?>
                            <?php if (($entrega_refaccion == "2")||($entrega_refaccion == "3")): ?>
                                <strong><?php if(isset($recibe_pieza)) echo $recibe_pieza->format('d/m/Y'); ?></strong>
                                <br>
                                <strong><?php if(isset($recibe_pieza)) echo $recibe_pieza->format('H:i:s'); ?></strong>
                            <?php endif ?>
                        <?php endif ?>
                        
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong><?php if(isset($paso_5)) {echo (($paso_5 != "PENDIENTE") ? $paso_5." MIN" : $paso_5);} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        RECIBIR REFACCIONES
                        <br>
                        <?php if (isset($entrega_refaccion)): ?>
                            <?php if (($entrega_refaccion == "2")||($entrega_refaccion == "3")): ?>
                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                            <?php else: ?> 
                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                </tr>
                <tr style="font-size: 13px;">
                    <td align="center" style="width: 10%;">
                        <strong>VENTANILLA (ENTREGAR REFACCIONES)</strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong>ENTREGA DE REFACCIONES III</strong>
                    </td>
                    <td align="center" style="width: 20%;">
                        <strong><?php if(isset($encargado_ventanilla)) {echo (($encargado_ventanilla != "") ? $encargado_ventanilla : "VENTANILLA");} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        <?php if (isset($entrega_refaccion)): ?>
                            <?php if (($entrega_refaccion == "2")): ?>
                                <strong><?php if(isset($entrega_pieza)) echo $entrega_pieza->format('d/m/Y'); ?></strong>
                                <br>
                                <strong><?php if(isset($entrega_pieza)) echo $entrega_pieza->format('H:i:s'); ?></strong>
                            <?php endif ?>
                        <?php endif ?>
                        
                    </td>
                    <td align="center" style="width: 10%;">
                        <strong><?php if(isset($paso_6)) {echo (($paso_6 != "PENDIENTE") ? $paso_6." MIN" : $paso_6);} ?></strong>
                    </td>
                    <td align="center" style="width: 10%;">
                        ENTRGA DE REFACCIONES
                        <br>
                        <?php if (isset($entrega_refaccion)): ?>
                            <?php if (($entrega_refaccion == "2")): ?>
                                <label style="color:green;font-weight: bold;">FINALIZADO</label>
                            <?php else: ?> 
                                <label style="color:red;font-weight: bold;">PENDIENTE</label>
                            <?php endif ?>
                        <?php endif ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <!--<div class="col-md-1"></div>-->
        <div class="col-md-12" align="center">
            <h4>SECUENCIA DE TIEMPOS TRANSCURRIDOS (FINALIZADOS)</h4>
            <i class="fas fa-spinner cargaIcono" id="grafica_1"></i>
            <div id="chart_div" style="width: 100%; "></div>
        </div>
        <!--<div class="col-md-1"></div>-->
    </div>

    <br>
    <div class="row">
        <!--<div class="col-md-1"></div>-->
        <div class="col-md-12" align="center">
            <h4>PROCESOS RETRASADOS</h4>
            <i class="fas fa-spinner cargaIcono" id="grafica_2"></i>
            <div id="chart_div_2" style="width: 100%; "></div>
        </div>
        <!--<div class="col-md-1"></div>-->
    </div>

    <br><br>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8" align="center">
            <h3 align="center" class="error" id="txtError"></h3>
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="idOrden" id="idOrden" value="<?php if(isset($idOrden)) echo $idOrden; else echo "0"; ?>">
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
