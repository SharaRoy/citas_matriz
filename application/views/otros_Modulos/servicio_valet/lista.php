<div style="margin:10px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Ordenes Servicio Valet</h3>
            <br>
            <div class="row">
                <div class="col-sm-9"></div>
                <div class="col-sm-3" align="right">
                    <input type="button" class="btn btn-success" onclick="location.href='<?=base_url()."Servicio_valet/0";?>';" name="" value="Nuevo registro">
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-1">
                            <br>
                            <input type="button" class="btn btn-dark" style="color:white;margin-top: 20px;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar">
                        </div>

                        <div class="col-sm-3" style="padding: 0px;padding-left: 30px;"></div>

                        <div class="col-sm-3"></div>

                        <!-- formulario de busqueda -->
                        <div class="col-sm-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_gral" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-sm-2" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" id="btnBusqueda_asig" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="form-group" align="center" style="overflow-x: scroll;overflow-y: scroll;">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <td align="center" style="width: 5%;vertical-align: middle;"><strong>#</strong></td>
                                    <td align="center" style="width: 8%;vertical-align: middle;"><strong>No ORDEN</strong></td>
                                    <td align="center" style="width: 20%;vertical-align: middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA SOLICITUD</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>SERIE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>MODELO</strong></td>
                                    <td align="center" style="width: 8%;vertical-align: middle;"><strong></strong></td>
                                    <td align="center" colspan="" style="width: 25%;vertical-align: middle;"><strong>EVIDENCIAS</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar"> 
                                <!-- Verificamos que se hayan cargado los datos para mostrar-->
                                <?php if (isset($idRegistro)): ?>
                                    <!-- Imprimimos los datos-->
                                    <?php foreach ($idRegistro as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <td align="center" style="width:5%;vertical-align: middle;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:8%;vertical-align: middle;">
                                                <?php echo $id_cita[$index]; ?>
                                            </td>
                                            <td align="center" style="width:20%;vertical-align: middle;">
                                                <?php echo $cliente[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $fecha_solicitud[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $serie[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <a href="<?=base_url().'Servicio_valet_PDF/'.$id_cita_url[$index];?>" class="btn btn-default" target="_blank" style="font-size: 10px;background-color:  #a569bd; color:white;">VER</a>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php if (count($evidencias[$index]) > 0): ?>
                                                    <?php $indice = 1; ?>
                                                    <?php for ($i = 0; $i < count($evidencias[$index]); $i++): ?>
                                                        <?php if ($evidencias[$index][$i] != ""): ?>
                                                            <a href="<?=base_url().$evidencias[$index][$i];?>" class="btn btn-primary" target="_blank" style="font-size: 10px;margin-top:5px;">
                                                                Doc. (<?php echo $indice++; ?>)
                                                            </a>
                                                        <?php endif; ?>
                                                    <?php endfor; ?>
                                                <?php else: ?>
                                                    <button type="button" class="btn btn-default" style="font-size: 10px;">SIN <br> ARCHIVO</button>
                                                <?php endif; ?> 
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <!-- Si no se cargaron los datos para mostrar -->
                                <?php else: ?>
                                    <tr>
                                        <td colspan="9" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="rolVista" value="listaAsesor">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
