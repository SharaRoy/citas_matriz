<div style="margin:10px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Ordenes Servicio Valet (sin asignar)</h3>
            <br>
            <div class="row">
                <div class="col-sm-9"></div>
                <div class="col-sm-3" align="right">
                    <input type="button" class="btn btn-success" onclick="location.href='<?=base_url()."Servicio_valet/0";?>';" name="" value="Nuevo registro">
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-1">
                            <br>
                            <input type="button" class="btn btn-dark" style="color:white;margin-top: 20px;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar">
                        </div>

                        <div class="col-sm-3" style="padding: 0px;padding-left: 30px;"></div>

                        <div class="col-sm-3"></div>

                        <!-- formulario de busqueda -->
                        <div class="col-sm-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_gral" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-sm-2" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" id="btnBusqueda" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="form-group" align="center" style="overflow-x: scroll;overflow-y: scroll;">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>#</strong></td>
                                    <td align="center" style="width: 20%;vertical-align: middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA SOLICITUD</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>SERIE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>MODELO</strong></td>
                                    <td align="center" colspan="1" style="width: 10%;vertical-align: middle;"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar"> 
                                <!-- Verificamos que se hayan cargado los datos para mostrar-->
                                <?php if (isset($idRegistro)): ?>
                                    <!-- Imprimimos los datos-->
                                    <?php foreach ($idRegistro as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:20%;vertical-align: middle;">
                                                <?php echo $cliente[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $fecha_solicitud[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $serie[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <a class='btn btn-primary' onclick='asignar_orden(<?php echo $idRegistro[$index]; ?>)' style='color:white;' data-id='<?= $idRegistro[$index]; ?>' data-target='#asignar'  data-toggle='modal'>
                                                    Asignar No. de Orden
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <!-- Si no se cargaron los datos para mostrar -->
                                <?php else: ?>
                                    <tr>
                                        <td colspan="7" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="rolVista" value="listaAsesor">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para asignar el servicio a una orden-->
<div class="modal fade" id="asignar" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="">Asignar registro a orden</h3>
            </div>
            <div class="modal-body">
                <h4 style="text-align: center;">Información del registro</h4>
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="producto_id_sat">Cliente:</label>
                            <input type="text" class="form-control" value=""  id="cliente_txt" disabled="">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="producto_id_sat">Fecha solicitud:</label>
                            <input type="text" class="form-control" value=""  id="fecha" disabled="">
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="producto_id_sat">Modelo:</label>
                            <input type="text" class="form-control" value=""  id="modelo_txt" disabled="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="producto_id_sat">Placas:</label>
                            <input type="text" class="form-control" value=""  id="placas_txt" disabled="">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="producto_id_sat">Serie:</label>
                            <input type="text" class="form-control" value=""  id="serie_txt" disabled="">
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-9">
                        <h4 style="text-align: center;">No. de Orden a asignar</h4>
                        <div class="form-group">
                            <input type="text" class="form-control" onkeypress="return event.charCode >= 46 && event.charCode <= 57" maxlength="10" value="" id="id_cita">
                            <input type="hidden" value=""  id="id_registro">
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <h5 style="text-align: center;font-weight: bold;" id="resultad_envio"></h5>
                        <br>
                    </div>
                </div>
            </div>

            <div class="modal-footer" align="center">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelar">Cancelar</button>
                <button type="button" class="btn btn-success" id="asignar_registro" style="margin-left: 30px;">Asignar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar" style="display:none;" onclick="location.reload()">Cerrar</button>
            </div>
        </div>
    </div>
</div>
