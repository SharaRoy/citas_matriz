<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
    <head>
        <title>Revisión de Aceite  <?php if (isset($id_cita)) echo $id_cita; else echo "0"; ?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="estilos.css" />
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <style>
            @page {
                sheet-size: Letter;
                size: portrait; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 15mm; /* <any of the usual CSS values for margins> */
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 20mm; /* <any of the usual CSS values for margins> */
                margin-footer: 20mm; /* <any of the usual CSS values for margins> */
            }
        </style>
    </head>
     
    <body style="font-family: helvetica; ">
        <!-- Hoja 1 -->
        <div>
            <table>
                <tr>
                    <td width="20%">
                        <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:160px;">
                    </td>
                    <td style="padding-left:10px; width:66%;" colspan="2" align="center">
                        <strong>
                            <span class="color-blue" style="font-size:10px;color: #337ab7;margin-left:30px;">
                                <?= SUCURSAL ?>, S.A. DE C.V.
                            </span>
                        </strong>

                        <p class="justify" style="font-size:9px;" align="center">
                            <?=$this->config->item('encabezados_txt')?>
                        </p>
                    </td>
                    <td width="20%" align="" style="padding-left:10px;" rowspan="2">
                        <div>
                            <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:100px;height:35px;"><br>
                        </div>
                    </td>
                </tr>
            </table>

            <br>
            <table class="" style="width:100%;">
                <tr>
                    <td style="font-size: 13px;font-weight: bold;" align="center">
                        SOLICITUD DE VALET PARA SERVICIO CON PLASENCIA MOTORS DE GUADALAJARA, S.A. DE C.V. (FORD PLASENCIA)
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10px;" align="right">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;" align="right">
                        Guadalajara, Jalisco a 
                        <u style="color:darkblue;">&nbsp;&nbsp;&nbsp; <?php if(isset($dia_solicitud)) echo $dia_solicitud; ?> &nbsp;&nbsp;&nbsp;</u> 
                        del mes de 
                        <u style="color:darkblue;">&nbsp;&nbsp;&nbsp; <?php if(isset($mes_solicitud)) echo $mes_solicitud; ?> &nbsp;&nbsp;&nbsp;</u>
                          del 
                        <u style="color:darkblue;">&nbsp;&nbsp;&nbsp; <?php if(isset($anio_solicitud)) echo $anio_solicitud; ?> &nbsp;&nbsp;&nbsp;</u>.
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 13px;" align="right">
                        Número de OS: 
                        <u style="color:darkblue;">&nbsp;&nbsp;&nbsp; <?php if(isset($id_cita)) echo $id_cita; ?> &nbsp;&nbsp;&nbsp;</u>.
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 10px;" align="right">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 12px;" align="justify">
                        <p align="justify" style="text-align: justify;">
                            (<strong>NOMBRE:</strong> <u style="color:darkblue;">&nbsp;&nbsp;&nbsp; <?php if(isset($nombre_cliente)) echo $nombre_cliente; ?> &nbsp;&nbsp;&nbsp;</u> ), para lo cual me identifico con Pasaporte/credencial para votar, misma que anexo en copia simple, por este acto manifiesto mi voluntad y deseo en contratar a Plasencia Motors de Guadalajara, S.A. de C.V. (Ford Plasencia), para que lleve a cabo el servicio de valet (traslado de mi domicilio a la agencia y de regreso), así como para el servicio de mantenimiento que se indica en el Manual de Propietario, al automotor descrito en el punto 1 siguiente, por lo tanto, acepto y reconozco: 

                            <br><br>
                            <strong>1.-</strong> Que el suscrito soy el legítimo poseedor del vehículo; <br>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td style="font-size: 10px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-MARCA: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($marca)) echo $marca; ?>
                                </td>
                                <td style="font-size: 10px;" align="right">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-MODELO: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($modelo)) echo $modelo; ?>
                                </td>
                                <td style="font-size: 10px;" align="right">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-COLOR: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($color)) echo $color; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-NÚMERO DE SERIE: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($serie)) echo $serie; ?>
                                </td>
                                <td style="font-size: 10px;" align="right">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-PLACAS: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($placas)) echo $placas; ?>
                                </td>
                                <td colspan="2">
                                    <br>
                                </td>
                            </tr>
                        </table>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 12px;" align="justify">
                        <p align="justify" style="text-align: justify;">
                            <strong>2.-</strong> El vehículo antes descrito, a la fecha cuenta con Seguro con cobertura Amplia Vigente bajo los siguientes datos: <br>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td style="font-size: 10px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-COMPAÑÍA ASEGURADORA: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($aseguradora)) echo $aseguradora; ?>
                                </td>
                                <td style="font-size: 10px;" align="right">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-NÚMERO DE PÓLIZA: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($poliza)) echo $poliza; ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 10px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-CONTRATANTE DE LA PÓLIZA: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($contratante)) echo $contratante; ?>
                                </td>
                                <td style="font-size: 10px;" align="right">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-FECHA DE VENCIMIENTO: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($fecha_vencimiento)) echo $fecha_vencimiento; ?>
                                </td>
                            </tr>
                        </table>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 12px;" align="justify">
                        <p align="justify" style="text-align: justify;">
                            <strong>3.-</strong> Otorgo mi autorización para que en caso de cualquier siniestro, Plasencia Motors de Guadalajara, S.A. de C.V., pueda hacer uso de la póliza de seguro descrita con anterioridad, en el entendido que en caso de utilizarse la póliza de seguro, cualquier cargo por deducible que se genere, será a cargo de Plasencia Motors de Guadalajara, S.A. de C.V., cuya responsabilidad se limita al pago de ese concepto. <br>

                            <br><br>
                            <strong>4.-</strong> Para efecto de llevar a cabo la comunicación referente al servicio solicitado, instruyo a Plasencia Motors de Guadalajara, S.A. de C.V., para que se realice a través de los siguientes medios electrónicos:  <br>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td style="font-size: 10px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-Correo electrónico: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($correo)) echo $correo; ?>
                                </td>
                                <td style="font-size: 10px;" align="right">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<strong>-WhatsApp: </strong>
                                </td>
                                <td style="border-bottom:0.5 solid black;color:darkblue;font-size: 11px;width: 20%;" align="center">
                                    <?php if(isset($whatsp)) echo $whatsp; ?>
                                </td>
                                <td style="width: 30%;">
                                    <br>
                                </td>
                            </tr>
                        </table>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 12px;" align="justify">
                        <p align="justify" style="text-align: justify;">
                            Por lo tanto, cualquier instrucción dada por este medio por mi parte, será válida para los fines del servicio solicitado, asumiendo todas sus consecuencias. 

                            <br><br>
                            <strong>5.-</strong> Acepto y reconozco que por la naturaleza del servicio, me comprometo a realizar el pago de la prestación solicitado, únicamente por medio de transferencia bancaria previo a la entrega del vehículo, o en su defecto, con tarjeta de débito o crédito al momento de la recepcion. 
                        </p>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size:11px;">
                        <br>
                        <strong for="">ATENTAMENTE</strong><br>

                        <img class="marcoImg" src="<?php if(isset($firma_cliente)) { if($firma_cliente != "") echo base_url().$firma_cliente;} ?>" style="width: 120px;height: 90px;">

                        <br>
                        <u style="color:darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php if(isset($nombre_cliente)) echo $nombre_cliente; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> <br>
                        
                        <strong for="">(NOMBRE Y FIRMA DEL CLIENTE)</strong><br>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>

<?php 
    try {      

        $html = ob_get_clean();
        ob_clean();

        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'LEGAL-P','debug' => TRUE]);
        $mpdf = new \mPDF('utf-8', 'Letter-P');

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        
        $mpdf->Output();

    //} catch (Html2PdfException $e) {
    } catch (Exception $e) {
    //    $html2pdf->clean();
    //    $formatter = new ExceptionFormatter($e);
    //    echo $formatter->getHtmlMessage();
        echo "NO SE PUDO GENERAR EL PDF";
    }
 ?>