<div class="" style="margin:25px;">
    <form id="servicio_valet" method="post" autocomplete="on" enctype="multipart/form-data" style="border:2px solid darkblue;">
        <!-- Alerta para proceso del registro -->
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <table style="width:100%;">
                    <tr>
                        <td style="width: 20%;vertical-align: middle;" align="center">
                            <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:100%;">
                        </td>
                        <td style="padding-left:10px; width:66%;" colspan="2" align="center">
                            <strong>
                                <span style="color: #337ab7;font-size: 13px;" style="">
                                    <?= SUCURSAL ?>, S.A. DE C.V.
                                </span>
                            </strong>

                            <p class="justify" style="text-align: center;font-size: 12px">
                                <?=$this->config->item('encabezados_txt')?>
                            </p>
                        </td>
                        <td style="width: 20%;vertical-align: middle;" align="center">
                            <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:100%;">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="3">
                            <input type="button" class="btn btn-dark" style="color:white;margin-top: 20px;" onclick="location.href='<?=base_url()."Servicio_valet_Lista/4";?>';" name="" value="Regresar">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-12">
                <h3 style="text-align: center;">
                    SOLICITUD DE VALET PARA SERVICIO CON PLASENCIA MOTORS DE GUADALAJARA, S.A. DE C.V. (FORD PLASENCIA)
                </h3>

                <br>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="2" style="font-size: 13px;" align="right">
                            Guadalajara, Jalisco a 
                            <input type="text" class="input_field" onkeypress="return event.charCode >= 46 && event.charCode <= 57" name="dia_solicitud" id="dia_solicitud" value="<?php if(set_value('dia_solicitud') != "") echo set_value('dia_solicitud'); elseif(isset($dia_solicitud)) echo $dia_solicitud; else echo date("d"); ?>" style="width:2cm;" disabled>
                            del mes de 
                            <input type="text" class="input_field" name="mes_solicitud" id="mes_solicitud" value="<?php if(set_value('mes_solicitud') != "") echo set_value('mes_solicitud'); elseif(isset($mes_solicitud)) echo $mes_solicitud;  ?>" style="width:3cm;" disabled>
                             del <?= date("Y"); ?>.
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 75%;font-size: 13px;" align="right">
                            Número de OS:
                        </td>
                        <td style="font-size: 13px;">
                            <input type="number" step="any" min="0" class="form-control" onkeypress="return event.charCode >= 46 && event.charCode <= 57" onblur='revision_valet()'  name="id_cita" id="id_cita" value="<?php if(set_value('id_cita') != "") echo set_value('id_cita'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:90%;">
                            <span class="error" id="error_id_cita"></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-md-12">
                <table style="width: 95%;margin-left: 3%;">
                    <tr>
                        <td style="font-size: 13px;">
                            (NOMBRE: <input type="text" class="input_field" onblur='cliente_nombre()'  name="nombre_cliente" id="nombre_cliente" value="<?php if(set_value('nombre_cliente') != "") echo set_value('nombre_cliente'); elseif(isset($nombre_cliente)) echo $nombre_cliente; ?>" style="width:60%;">
                            ), para lo cual me identifico con Pasaporte/credencial para votar, misma que anexo en copia simple, por este acto manifiesto mi voluntad y deseo en contratar a Plasencia Motors de Guadalajara, S.A. de C.V. (Ford Plasencia), para que lleve a cabo el servicio de valet (traslado de mi domicilio a la agencia y de regreso), así como para el servicio de mantenimiento que se indica en el Manual de Propietario, al automotor descrito en el punto 1 siguiente, por lo tanto, acepto y reconozco: 
                            
                            <br><br>
                            <strong>1.-</strong> Que el suscrito soy el legítimo poseedor del vehículo; <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-md-4" style="font-size: 13px;">
                                    <strong>-MARCA: </strong>
                                    <input type="text" class="form-control" name="marca" id="marca" value="<?php if(set_value('marca') != "") echo set_value('marca'); elseif(isset($marca)) echo $marca; ?>" style="width:100%;">
                                    <br>
                                    <span class="error" id="error_marca"></span>
                                </div>
                                <div class="col-md-4" style="font-size: 13px;">
                                    <strong>-MODELO: </strong>
                                    <input type="text" class="form-control" name="modelo" id="modelo" value="<?php if(set_value('modelo') != "") echo set_value('modelo'); elseif(isset($modelo)) echo $modelo; ?>" style="width:100%;">
                                    <br>
                                    <span class="error" id="error_modelo"></span>
                                </div>
                                <div class="col-md-4" style="font-size: 13px;">
                                    <strong>-COLOR: </strong>
                                    <input type="text" class="form-control" name="color" id="color" value="<?php if(set_value('color') != "") echo set_value('color'); elseif(isset($color)) echo $color; ?>" style="width:100%;">
                                    <br>
                                    <span class="error" id="error_color"></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <div class="row">
                                <div class="col-md-6" style="font-size: 13px;">
                                    <strong>-NÚMERO DE SERIE: </strong>
                                    <input type="text" class="form-control" onblur='rellenado()' name="serie" id="serie" value="<?php if(set_value('serie') != "") echo set_value('serie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;">
                                    <br>
                                    <span class="error" id="error_serie"></span>
                                </div>
                                <div class="col-md-4" style="font-size: 13px;">
                                    <strong>-PLACAS: </strong>
                                    <input type="text" class="form-control" onblur='rellenado()' name="placas" id="placas" value="<?php if(set_value('placas') != "") echo set_value('placas'); elseif(isset($placas)) echo $placas; ?>" style="width:100%;">
                                    <br>
                                    <span class="error" id="error_placas"></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">
                            <br>
                            <strong>2.-</strong> El vehículo antes descrito, a la fecha cuenta con Seguro con cobertura Amplia Vigente bajo los siguientes datos: 
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">
                            <br>
                            <div class="row">
                                <div class="col-md-6" style="font-size: 13px;">
                                    <strong>-COMPAÑÍA ASEGURADORA: </strong>
                                    <input type="text" class="form-control" name="aseguradora" id="aseguradora" value="<?php if(set_value('aseguradora') != "") echo set_value('aseguradora'); elseif(isset($aseguradora)) echo $aseguradora; ?>" style="width:100%;">
                                    <span class="error" id="error_aseguradora"></span>
                                </div>
                                <div class="col-md-4" style="font-size: 13px;">
                                    <strong>-NÚMERO DE PÓLIZA: </strong>
                                    <input type="text" class="form-control" name="poliza" id="poliza" value="<?php if(set_value('poliza') != "") echo set_value('poliza'); elseif(isset($poliza)) echo $poliza; ?>" style="width:100%;">
                                    <span class="error" id="error_poliza"></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">
                            <br>
                            <div class="row">
                                <div class="col-md-6" style="font-size: 13px;">
                                    <strong>-CONTRATANTE DE LA PÓLIZA: </strong>
                                    <input type="text" class="form-control" name="contratante" id="contratante" value="<?php if(set_value('contratante') != "") echo set_value('contratante'); elseif(isset($contratante)) echo $contratante; ?>" style="width:100%;">
                                    <span class="error" id="error_contratante"></span>
                                </div>
                                <div class="col-md-4" style="font-size: 13px;">
                                    <strong>-FECHA DE VENCIMIENTO: </strong>
                                    <input type="date" class="form-control" min="<?= date("Y-m-d"); ?>" name="fecha_vencimiento" id="fecha_vencimiento" value="<?php if(set_value('fecha_vencimiento') != "") echo set_value('fecha_vencimiento'); elseif(isset($fecha_vencimiento)) echo $fecha_vencimiento; ?>" style="width:100%;padding: 0px;">
                                    <span class="error" id="error_fecha_vencimiento"></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">
                            <br>
                            <strong>3.- </strong> Otorgo mi autorización para que en caso de cualquier siniestro, Plasencia Motors de Guadalajara, S.A. de C.V., pueda hacer uso de la póliza de seguro descrita con anterioridad, en el entendido que en caso de utilizarse la póliza de seguro, cualquier cargo por deducible que se genere, será a cargo de Plasencia Motors de Guadalajara, S.A. de C.V., cuya responsabilidad se limita al pago de ese concepto.   

                            <br><br>
                            <strong>4.- </strong> Para efecto de llevar a cabo la comunicación referente al servicio solicitado, instruyo a Plasencia Motors de Guadalajara, S.A. de C.V., para que se realice a través de los siguientes medios electrónicos: 
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">
                            <br>
                            <div class="row">
                                <div class="col-md-6" style="font-size: 13px;">
                                    <strong>-Correo electrónico: </strong>
                                    <input type="email" class="form-control" name="correo" id="correo" value="<?php if(set_value('correo') != "") echo set_value('correo'); elseif(isset($correo)) echo $correo; else echo 'notienecorreo@notienecorreo.com.mx'; ?>" style="width:100%;" required="required" />
                                    <span class="error" id="error_correo"></span>
                                </div>
                                <div class="col-md-4" style="font-size: 13px;">
                                    <strong>-WhatsApp:  </strong>
                                    <input type="text" class="form-control" onkeypress="return event.charCode >= 46 && event.charCode <= 57" maxlength="10" name="whatsp" id="whatsp" value="<?php if(set_value('whatsp') != "") echo set_value('whatsp'); elseif(isset($whatsp)) echo $whatsp; ?>" style="width:100%;">
                                    <span class="error" id="error_whatsp"></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;">
                            <br>
                            Por lo tanto, cualquier instrucción dada por este medio por mi parte, será válida para los fines del servicio solicitado, asumiendo todas sus consecuencias.
                            
                            <br><br>
                            5.- Acepto y reconozco que por la naturaleza del servicio, me comprometo a realizar el pago de la prestación solicitado, únicamente por medio de transferencia bancaria previo a la entrega del vehículo, o en su defecto, con tarjeta de débito o crédito al momento de la recepción.
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;" align="right">
                            <br>
                            <h5>Lado frontal</h5>
                            <input type="file" name="archivos[]" value="">
                            <h5>Lado posteior</h5>
                            <input type="file" name="archivos[]" value="">
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;" align="center">
                            <label for="">ATENTAMENTE</label><br>
                            <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?php if(isset($firma_cliente)) echo $firma_cliente; ?>">
                            <img class="marcoImg" src="<?php if(isset($firma_cliente)) { if($firma_cliente != "") echo base_url().$firma_cliente;} ?>" id="firmaFirmaConsumidor" alt="">
                            <br><br>
                            <input type="text" class="form-control" id="nombre_clienteB" name="nombre_clienteB" style="width:100%;" value="<?php if(isset($nombre_cliente)) echo $nombre_cliente; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td style="font-size: 13px;" align="center">
                            <label for="">(NOMBRE Y FIRMA DEL CLIENTE)</label><br>
                            <span class="error" id="error_rutacliente"></span><br>
                            <span class="error" id="error_nombrecliente"></span>

                            <br>
                            <?php if (isset($firma_cliente)): ?>
                                <?php if ($firma_cliente == ""): ?>
                                    <a id="Cliente_firma" class="cuadroFirma btn btn-primary" data-value="Cliente" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Cliente</a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a id="Cliente_firma" class="cuadroFirma btn btn-primary" data-value="Cliente" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Cliente</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>


        <br>
        <div class="row" align="center">
            <div class="col-md-12" align="center">
                <br>
                <i class="fas fa-spinner cargaIcono"></i>
                <span class="error" id="errorEnvio"></span>
                <br>

                <input type="hidden" name="cita_Act" id="cita_Act" value="<?php if(isset($id_cita)) echo $id_cita; ?>">
                <input type="hidden" name="tipo_formulario" id="tipo_formulario" value="1">

                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="registro" id="registro" value="<?php if(isset($idRegistro)) echo $idRegistro; else echo '0'; ?>">

                <button type="button" class="btn btn-success" name="btn_envio" id="btn_envio" style="margin-left: 25px;">
                    Guardar Formulario
                </button>

                <br><br>
            </div>
        </div>
        <br>
    </form>
</div>


<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>