<section style="margin:20px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Registro actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <br>
	<form name="" method="post" action="<?php echo base_url().'otros_Modulos/Clientes_Refacciones/validateForm';?>" autocomplete="on" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<h4 style="text-align: center; font-weight: bold;">PARTES SOLICITADAS NO INSTALADAS (CLIENTES) - ACTUALIZACIÓN</h4>
			</div>
			<div class="col-md-2">
			</div>
		</div>

		<div style="border: 1px solid darkblue;padding: 20px;">
			<h4 style="text-align: center; color: darkblue;">Datos del Generales</h4>
			<hr>

			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Orden:</label>
	                <br>
	                <input type="text" name="orden" id="ordenForm" class="form-control" value="<?php if(set_value('orden') != "") {echo set_value('orden');} else { if(isset($orden)) echo $orden; else echo '0';} ?>">
	                <?php echo form_error('orden', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Folio:</label>
	                <br>
	                <input type="text" name="folio" class="form-control" value="<?php if(set_value('folio') != "") {echo set_value('folio');} else { if(isset($folio)) echo $folio; else echo '';} ?>">
	                <?php echo form_error('folio', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha folio:</label>
	                <br>
	                <input type="date" name="fecha_folio" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?php if(set_value('fecha_folio') != "") {echo set_value('fecha_folio');} else { if(isset($fecha_folio)) echo $fecha_folio; else echo '';} ?>" style="padding-top: 0px;" disabled>
	                <?php echo form_error('fecha_folio', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Estatus:</label>
	                <br>
					<select class="form-control" name="estadoProceso">
						<option value="">SELECCIONE</option>
						<option value="DISP INSTALAR" style="background-color: #92D050;color:white;" <?php if(isset($estadoProceso)) if($estadoProceso == "DISP INSTALAR") echo 'selected'; ?> >DISP INSTALAR</option>
						<option value="NO CONTESTA" style="background-color: #FF0000;color:white;" <?php if(isset($estadoProceso)) if($estadoProceso == "NO CONTESTA") echo 'selected'; ?> >NO CONTESTA</option>
						<option value="PENDIENTE" <?php if(isset($estadoProceso)) if($estadoProceso == "PENDIENTE") echo 'selected'; ?> >PENDIENTE</option>
						<option value="NO IDENTIFICADO" style="background-color: #FFFF00;" <?php if(isset($estadoProceso)) if($estadoProceso == "NO IDENTIFICADO") echo 'selected'; ?> >NO IDENTIFICADO</option>
						<option value="POR RESERVAR" style="background-color: #00B0F0;color:white;" <?php if(isset($estadoProceso)) if($estadoProceso == "POR RESERVAR") echo 'selected'; ?> >POR RESERVAR</option>
						<option value="RESERVADO" style="background-color: #7030A0;color:white;"  <?php if(isset($estadoProceso)) if($estadoProceso == "RESERVADO") echo 'selected'; ?> >RESERVADO</option>
					</select>
	                <?php echo form_error('estadoProceso', '<br><span class="error">', '</span>'); ?>

	                <br><br>
	                <a id="historiaComentariosCliente" data-value="<?php if(isset($idRegistro)) echo $idRegistro; else echo '0'; ?>" data-orden="<?php if(isset($orden)) echo $orden; else echo '0'; ?>" data-target="#historial_comentarios" data-toggle="modal" style="color:blue;font-size:14px;cursor: pointer;">
                        Ver cambios de estatus
                    </a>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Técnico:</label>
	                <br>
	                <input type="text" name="tecnico" class="form-control" value="<?php if(set_value('tecnico') != "") {echo set_value('tecnico');} else { if(isset($tecnico)) echo $tecnico; else echo '';} ?>">
	                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Asesor:</label>
	                <br>
	                <input type="text" name="asesor" class="form-control" value="<?php if(set_value('asesor') != "") {echo set_value('asesor');} else { if(isset($asesor)) echo $asesor; else echo '';} ?>">
	                <?php echo form_error('asesor', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Notas Estatus:</label>
	                <br>
	                <textarea  class="form-control" name="notaEstatus" rows="1"><?php if(set_value('notaEstatus') != "") {echo set_value('notaEstatus');} else { if(isset($notaEstatus)) echo $notaEstatus; else echo '';} ?></textarea>
	                <?php echo form_error('notaEstatus', '<br><span class="error">', '</span>'); ?>
				</div>
			</div>

			<br>
			<h4 style="text-align: center; color: darkblue;">Datos de la unidad</h4>
			<hr>

			<div class="row">
				<div class="col-md-10">
					<label for="" style="font-size: 13px;font-weight: inherit;">Unidad en Taller:</label>
	                <br>
	                <input type="text" name="unidadTaller" class="form-control" value="<?php if(set_value('unidadTaller') != "") {echo set_value('unidadTaller');} else { if(isset($unidadTaller)) echo $unidadTaller; else echo '';} ?>">
	                <?php echo form_error('unidadTaller', '<br><span class="error">', '</span>'); ?>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">No. Unidad Inmovilizada:</label>
	                <br>
	                <input type="text" name="unidadInmo" class="form-control" value="<?php if(set_value('unidadInmo') != "") {echo set_value('unidadInmo');} else { if(isset($unidadInmo)) echo $unidadInmo; else echo '';} ?>">
	                <?php echo form_error('unidadInmo', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">Vehículo:</label>
	                <br>
	                <input type="text" name="modeloVehiculo" class="form-control" value="<?php if(set_value('modeloVehiculo') != "") {echo set_value('modeloVehiculo');} else { if(isset($modeloVehiculo)) echo $modeloVehiculo; else echo '';} ?>">
	                <?php echo form_error('modeloVehiculo', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">Serie:</label>
	                <br>
	                <input type="text" name="serie" class="form-control" value="<?php if(set_value('serie') != "") {echo set_value('serie');} else { if(isset($serie)) echo $serie; else echo '';} ?>">
	                <?php echo form_error('serie', '<br><span class="error">', '</span>'); ?>
				</div>			
			</div>

			<br>
			<h4 style="text-align: center; color: darkblue;">Datos de la refacción</h4>
			<hr>

			<div id="contenedor_Refacciones">
				<input type="hidden" name="maxRenglon" id="maxRenglon" value="<?php if(isset($descripcionCorta)) {if(count($descripcionCorta) > 1) echo count($descripcionCorta)-1; else echo "1";} else echo '1'; ?>">
				<div class="row">
					<div class='col-md-2'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Cantidad:</label>
					</div>
					<div class='col-md-3'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Número de parte:</label>
					</div>
					<div class='col-md-5'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Descripción Corta:</label>
					</div>
					<div class='col-md-1' align='right'>
						<a class='btn btn-success' style='color:white;width: 1cm;' onclick="agregarRef_cliente(0)">
                    		<i class='fa fa-plus'></i>
						</a>
					</div>
					<!--<div class='col-md-1' align='right'>
						<a class='btn btn-danger' style='color:white; width: 1cm;' onclick="quitarRef_cliente(0)">
                    		<i class='fa fa-minus'></i>
						</a>
					</div>-->				
				</div>

				<!--Verificamos que se hayan cargado los registros -->
				<?php if (isset($descripcionCorta)): ?>
					<!--Imprimimos las refacciones guardadas-->
					<?php foreach ($descripcionCorta as $index => $valor): ?>
						<!--Comprobamos que no sea un renglon vacio -->
						<?php if ($descripcionCorta[$index] != ""): ?>
							<br>
							<div class='row' id='renglon_<?php echo $index; ?>'>
								<div class='col-md-2'>
					                <input type='number' step='0.01' min='1' name='cantidad_<?php echo $index; ?>' id='cantidad_<?php echo $index; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='<?php if(set_value('cantidad_'.$index) != "") echo set_value('cantidad_'.$index); else echo $cantidad[$index]; ?>'>
					                <?php echo form_error('cantidad_'.$index, "<br><span class='error'>", '</span>'); ?>
								</div>
								<div class='col-md-3'>
					                <input type='text' name='numParte_<?php echo $index; ?>' id='numParte_<?php echo $index; ?>' class='form-control' value='<?php if(set_value('numParte_'.$index) != "") echo set_value('numParte_'.$index); else echo $numParte[$index]; ?>'>
					                <input type='hidden' name='id_refaccion_<?php echo $index; ?>' id='id_refaccion_<?php echo $index; ?>' class='form-control' value='<?php if(set_value('id_refaccion_'.$index) != "") echo set_value('id_refaccion_'.$index); else echo $id_refaccion[$index]; ?>'>
					                <?php echo form_error('numParte_'.$index, "<br><span class='error'>", '</span>'); ?>
								</div>
								<div class='col-md-6'>
					                <textarea  class='form-control' name='descripcionCorta_<?php echo $index; ?>' id='descripcionCorta_<?php echo $index; ?>' rows='2'><?php if(set_value('descripcionCorta_'.$index) != "") echo set_value('descripcionCorta_'.$index); else echo $descripcionCorta[$index]; ?></textarea>
					                <?php echo form_error('descripcionCorta_'.$index, "<br><span class='error'>", '</span>'); ?>
								</div>
								<div class='col-md-1' align='right'>
									<input type='hidden' name='' id='sub_indice_<?php echo $index; ?>' class='form-control' value='<?php echo $index; ?>'>
									<a class='btn btn-danger' style='color:white; width: 1cm;' onclick='quitarRef_cliente(<?php echo $index; ?>)'>
										<i class='fa fa-minus'></i>
									</a>
								</div>
							</div>
						<?php endif ?>
					<?php endforeach ?>
				<?php else: ?>
					<div class='row' id='renglon_0'>
						<div class='col-md-2'>
			                <input type='number' step='0.01' min='1' name='cantidad_0' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='<?php if(set_value('cantidad_0') != "") echo set_value('cantidad_0'); else echo '1';?>'>
			                <?php echo form_error('cantidad_0', "<br><span class='error'>", '</span>'); ?>
						</div>
						<div class='col-md-3'>
			                <input type='text' name='numParte_0' id='numParte_0' class='form-control' value='<?php echo set_value('numParte_0');?>'>
			                <input type='hidden' name='id_refaccion_0' id='id_refaccion_0' class='form-control' value='0'>
			                <?php echo form_error('numParte_0', "<br><span class='error'>", '</span>'); ?>
						</div>
						<div class='col-md-6'>
			                <textarea  class='form-control' name='descripcionCorta_0' id='descripcionCorta_0' rows='2'><?php echo set_value('descripcionCorta_0');?></textarea>
			                <input type='hidden' name='' id='sub_indice_0"' class='form-control' value='0'>
			                <?php echo form_error('descripcionCorta_0', "<br><span class='error'>", '</span>'); ?>
						</div>
					</div>
				<?php endif ?>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Pedido:</label>
	                <br>
	                <input type="date" name="fecha_pedido" class="form-control" value="<?php if(isset($fecha_pedido)) echo $fecha_pedido; ?>" style="padding-top: 0px;">
	                <?php echo form_error('fecha_pedido', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">No. Remisión:</label>
	                <br>
	                <input type="text" name="noRemision" class="form-control" value="<?php if(set_value('noRemision') != "") {echo set_value('noRemision');} else { if(isset($noRemision)) echo $noRemision; else echo '';} ?>">
	                <?php echo form_error('noRemision', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo:</label>
	                <br>
	                <input type="text" name="costo" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?php if(set_value('costo') != "") {echo set_value('costo');} else { if(isset($costo)) echo $costo; else echo '';} ?>">
	                <?php echo form_error('costo', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha B.O.:</label>
	                <br> 
	                <input type="date" name="fechaBO" class="form-control" style="padding-top: 0px;" max="<?php echo date('Y-m-d'); ?>" value="<?php if(set_value('fechaBO') != "") {echo set_value('fechaBO');} else { if(isset($fechaBO)) echo $fechaBO; else echo '';} ?>">
	                <?php echo form_error('fechaBO', '<br><span class="error">', '</span>'); ?>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Promesa:</label>
	                <br>
	                <input type="date" name="fecha_promesa" max="<?php echo date('Y-m-d'); ?>"  class="form-control" value="<?php if(isset($fecha_promesa)) echo $fecha_promesa; ?>" style="padding-top: 0px;">
	                <?php echo form_error('fecha_promesa', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Recibe:</label>
	                <br>
	                <input type="date" name="fecha_recibe" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?php if(isset($fecha_recibe)) echo $fecha_recibe; ?>" style="padding-top: 0px;">
	                <?php echo form_error('fecha_recibe', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Aviso de Taller:</label>
	                <br>
	                <input type="date" name="fecha_taller" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?php if(isset($fecha_taller)) echo $fecha_taller; ?>" style="padding-top: 0px;">
	                <?php echo form_error('fecha_taller', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Instalación:</label>
	                <br>
	                <input type="date" name="fechaInstalacion" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?php if(isset($fechaInstalacion)) echo $fechaInstalacion; ?>" style="padding-top: 0px;">
	                <?php echo form_error('fechaInstalacion', '<br><span class="error">', '</span>'); ?>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Refacción Revisada Por:</label>
	                <br>
	                <input type="text" name="revisadaPor" id="" class="form-control" value="<?php if(set_value('revisadaPor') != "") {echo set_value('revisadaPor');} else { if(isset($revisadaPor)) echo $revisadaPor; else echo '';} ?>">
	                <?php echo form_error('revisadaPor', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo Dist.:</label>
	                <br>
	                <input type="text" name="costoDist" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?php if(set_value('costoDist') != "") {echo set_value('costoDist');} else { if(isset($costoDist)) echo $costoDist; else echo '';} ?>">
	                <?php echo form_error('costoDist', '<br><span class="error">', '</span>'); ?>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo Total:</label>
	                <br>
	                <input type="text" name="costoTotal" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?php if(set_value('costoTotal') != "") {echo set_value('costoTotal');} else { if(isset($costoTotal)) echo $costoTotal; else echo '';} ?>">
	                <?php echo form_error('costoTotal', '<br><span class="error">', '</span>'); ?>
				</div>
			</div>

			<br>
			<div class="row">				
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Observaciones sobre la pieza:</label>
	                <br>
	                <textarea  class="form-control" name="observacionesPza" rows="1"><?php if(set_value('observacionesPza') != "") {echo set_value('observacionesPza');} else { if(isset($observacionesPza)) echo $observacionesPza; else echo '';} ?></textarea>
	                <?php echo form_error('observacionesPza', '<br><span class="error">', '</span>'); ?>
				</div>	
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Observaciones / Notas:</label>
	                <br>
	                <textarea  class="form-control" name="notas" rows="1"><?php if(set_value('notas') != "") {echo set_value('notas');} else { if(isset($notas)) echo $notas; else echo '';} ?></textarea>
	                <?php echo form_error('notas', '<br><span class="error">', '</span>'); ?>
				</div>	
			</div>

			<hr>
			<div class="row">
	            <div class="col-sm-4"></div>
	            <div class="col-sm-8" align="right">
	               	<h4>Archivo(s):</h4><br>
					<?php if (isset($archivos)): ?>
	                    <?php $contador = 1; ?>
	                    <?php for ($i = 0; $i < count($archivos); $i++): ?>
	                        <?php if ($archivos[$i] != ""): ?>
	                            <a href="<?php echo base_url().$archivos[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
	                                DOC. (<?php echo $contador; ?>)
	                            </a>
	                            
	                            <?php $contador++; ?>
	                        <?php endif; ?>
	                    <?php endfor; ?>

	                    <?php if (count($archivos) == 0): ?>
	                        <h4 style="text-align: right;">Sin archivos</h4>
	                    <?php endif; ?>
	                <?php endif; ?>

	                <hr>
	                <input type="file" multiple accept="image/*" name="uploadfiles[]"><br>
	                <input type="hidden" name="uploadfiles_resp" value="<?php if(isset($archivos_txt)) echo $archivos_txt;?>">
	            </div>
	        </div>
		</div>

		<br><br>
        <div class="col-sm-12" align="center">
            <input type="hidden" name="cargaFormulario" value="2">
            <input type="hidden" name="" id="formulario" value="clientes">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="registro" id="registro" value="<?php if(isset($idRegistro)) echo $idRegistro; else echo '0'; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."ClienteRF_Lista/5";?>';" style="margin-right: 20px;">
                Regresar
            </button>
            
            <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="">Actualizar Formulario</button>
        </div>

        <br>
	</form>	
</section>

<!-- Modal para ver el historial de comentarios -->
<div class="modal fade" id="historial_comentarios" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Historial de cambios de estatus</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="" style="width:100%;">
                            <thead>
                                <tr style="background-color: #ddd;">
                                	<td style="width:8%;"><h5 align="center">#</h5></td>
                                    <td style="width:20%;"><h5 align="center">Fecha</h5></td>
                                    <td style="width:22%;"><h5 align="center">Estatus</h5></td>
                                    <td style="width:50%;"><h5 align="center">Nota</h5></td>
                                </tr>
                            </thead>
                            <tbody id="tablaComnetario">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="identificador_historial" value="G">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>