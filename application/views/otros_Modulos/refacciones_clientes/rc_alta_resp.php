<section style="margin:20px;">
	<br>
	<form name="" id="historial_clientes_refacciones" method="post" autocomplete="on" enctype="multipart/form-data">
		<?php 
			#action="<? php echo base_url().'otros_Modulos/Clientes_Refacciones/validateForm';? >"
		 ?>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<h4 style="text-align: center;font-weight: bold;">PARTES SOLICITADAS NO INSTALADAS (CLIENTES) - REGISTRO</h4>
			</div>
			<div class="col-md-2">
			</div>
		</div>

		<div style="border: 1px solid darkblue;padding: 20px;">
			<h4 style="text-align: center; color: darkblue;">Datos del Generales</h4>
			<hr>

			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Orden: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="text" name="orden" id="ordenForm" onblur="precarga()" class="form-control" value="<?php if(set_value('orden') != "") {echo set_value('orden');} else { if(isset($orden)) echo $orden;} ?>">
	                <br><span class="error" id ="error_orden"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Folio: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="text" name="folio" id="" class="form-control" value="<?php if(set_value('folio') != "") {echo set_value('folio');} else { if(isset($folio)) echo $folio; else echo '0';} ?>">
	                <br><span class="error" id ="error_folio"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Fecha folio: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="date" name="fecha_folio" max="<?php echo date('Y-m-d'); ?>"  id="" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_folio"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Estatus: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
					<select class="form-control" name="estadoProceso" id="estadoProceso">
						<option value="">SELECCIONE</option>
						<option value="DISP INSTALAR" <?php if(set_value('estadoProceso') == 'DISP INSTALAR') echo 'selected';?> style="background-color: #92D050;color:white;">DISP INSTALAR</option>
						<option value="NO CONTESTA" <?php if(set_value('estadoProceso') == 'NO CONTESTA') echo 'selected';?> style="background-color: #FF0000;color:white;">NO CONTESTA</option>
						<option value="PENDIENTE" <?php if(set_value('estadoProceso') == 'PENDIENTE') echo 'selected';?>>PENDIENTE</option>
						<option value="NO IDENTIFICADO" <?php if(set_value('estadoProceso') == 'NO IDENTIFICADO') echo 'selected';?> style="background-color: #FFFF00;">NO IDENTIFICADO</option>
						<option value="POR RESERVAR" <?php if(set_value('estadoProceso') == 'POR RESERVAR') echo 'selected';?> style="background-color: #00B0F0;color:white;">POR RESERVAR</option>
						<option value="RESERVADO" <?php if(set_value('estadoProceso') == 'RESERVADO') echo 'selected';?> style="background-color: #7030A0;color:white;">RESERVADO</option>
					</select>
	                <br><span class="error" id ="error_estadoProceso"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Técnico:
					</label>
	                <br>
	                <input type="text" name="tecnico" id="inputTecnico" class="form-control" value="<?php echo set_value('tecnico');?>">
	                <br><span class="error" id ="error_tecnico"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Asesor: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="text" name="asesor" id="inputAsesor" class="form-control" value="<?php echo set_value('asesor');?>">
	                <br><span class="error" id ="error_asesor"></span>
				</div>
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Notas Estatus:
					</label>
	                <br>
	                <textarea  class="form-control" name="notaEstatus" rows="1"><?php echo set_value('notaEstatus');?></textarea>
	                <br><span class="error" id ="error_notaEstatus"></span>
				</div>
			</div>
				
			<br>
			<h4 style="text-align: center; color: darkblue;">Datos de la unidad</h4>
			<hr>

			<div class="row">
				<div class="col-md-10">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Unidad en Taller: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="text" name="unidadTaller" id="inputCliente" class="form-control" value="<?php echo set_value('unidadTaller');?>">
	                <br><span class="error" id ="error_unidadTaller"></span>
	                <input type="hidden" maxlength="15" id="inputTelefono" name="telefonoCliente" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php echo set_value('telefonoCliente');?>">
	                <input type="hidden" maxlength="15" id="inputTelefono_2" name="telefonoCliente_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php echo set_value('telefonoCliente_2');?>">
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						No. Unidad Inmovilizada: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="text" name="unidadInmo" id="" class="form-control" value="<?php echo set_value('unidadInmo');?>">
	                <br><span class="error" id ="error_unidadInmo"></span>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Vehículo: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="text" name="modeloVehiculo" id="inputModelo" class="form-control" value="<?php echo set_value('modeloVehiculo');?>">
	                <br><span class="error" id ="error_modeloVehiculo"></span>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Serie: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="text" name="serie" id="inputSerie" class="form-control" value="<?php echo set_value('serie');?>">
	                <br><span class="error" id ="error_serie"></span>
				</div>
			</div>

			<br>
			<h4 style="text-align: center; color: darkblue;">Datos de la refacción</h4>
			<hr>

			<div>
				<input type="hidden" name="maxRenglon" id="maxRenglon" value="1">
				<div class="row">
					<div class='col-md-2'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>
							Cantidad: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
						</label>
						<br><span class="error" id ="error_refaccion"></span>
					</div>
					<div class='col-md-3'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>
							Número de parte: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
						</label>
					</div>
					<div class='col-md-5'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>
							Descripción Corta: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
						</label>
					</div>
					<div class='col-md-1' align='right'>
						<a class='btn btn-success' style='color:white;width: 1cm;' onclick="agregarRef_cliente(0)">
                    		<i class='fa fa-plus'></i>
						</a>
					</div>
					<!--<div class='col-md-1' align='right'>
						<a class='btn btn-danger' style='color:white; width: 1cm;' onclick="quitarRef_cliente(0)">
                    		<i class='fa fa-minus'></i>
						</a>
					</div>					-->
				</div>
			</div>

			<div id="contenedor_Refacciones">
				<div class='row' id='renglon_0'>
					<div class='col-md-2'>
		                <input type='number' step='0.01' min='1' name='cantidad_0' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='<?php if(set_value('cantidad_0') != "") echo set_value('cantidad_0'); else echo '1';?>'>
		                <br><span class="error" id ="error_cantidad_0"></span>
					</div>
					<div class='col-md-3'>
		                <input type='text' name='numParte_0' id='numParte_0' class='form-control' value='<?php echo set_value('numParte_0');?>'>
		                <input type='hidden' name='id_refaccion_0' id='id_refaccion_0"' class='form-control' value='0'>
		                <br><span class="error" id ="error_numParte_0"></span>
					</div>
					<div class='col-md-6'>
		                <textarea  class='form-control' name='descripcionCorta_0' id='descripcionCorta_0' rows='2'><?php echo set_value('descripcionCorta_0');?></textarea>
		                <br><span class="error" id ="error_descripcionCorta_0"></span>
					</div>
				</div>
				<div class='col-md-1' align='right'>
					<input type='hidden' name='' id='sub_indice_0' class='form-control' value='0'>
					<!--<a class='btn btn-danger' style='color:white; width: 1cm;' onclick="quitarRef_cliente(0)">
                		<i class='fa fa-minus'></i>
					</a>-->
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Fecha Pedido: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <input type="date" name="fecha_pedido" max="<?php echo date('Y-m-d'); ?>" id="" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_pedido"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">No. Remisión:</label>
	                <br>
	                <input type="text" name="noRemision" id="" class="form-control" value="<?php echo set_value('noRemision');?>">
	                <br><span class="error" id ="error_noRemision"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo:</label>
	                <br>
	                <input type="text" name="costo" onkeypress='return event.charCode >= 46 && event.charCode <= 57' id="" class="form-control" value="<?php echo set_value('costo');?>">
	                <br><span class="error" id ="error_costo"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha B.O.:</label>
	                <br>
	                <input type="date" max="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;" name="fechaBO" class="form-control" value="<?php echo set_value('fechaBO');?>">
	                <br><span class="error" id ="error_fechaBO"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Promesa:</label>
	                <br>
	                <input type="date" name="fecha_promesa" id="inputFechaPromesa" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_promesa"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Recibe:</label>
	                <br>
	                <input type="date" name="fecha_recibe" id="inputFechaRecibe" max="<?php echo date('Y-m-d'); ?>"  class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_recibe"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Aviso de Taller:</label>
	                <br>
	                <input type="date" name="fecha_taller" max="<?php echo date('Y-m-d'); ?>" id="" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_taller"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Instalación:</label>
	                <br>
	                <input type="date" name="fechaInstalacion" max="<?php echo date('Y-m-d'); ?>" id="" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fechaInstalacion"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Refacción Revisada Por:</label>
	                <br>
	                <input type="text" name="revisadaPor" id="" class="form-control" value="<?php echo set_value('revisadaPor');?>">
	                <br><span class="error" id ="error_revisadaPor"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo Dist.:</label>
	                <br>
	                <input type="text" name="costoDist" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?php echo set_value('costoDist');?>">
	                <br><span class="error" id ="error_costoDist"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo Total:</label>
	                <br>
	                <input type="text" name="costoTotal" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?php echo set_value('costoTotal');?>">
	                <br><span class="error" id ="error_costoTotal"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Observaciones sobre la pieza:</label>
	                <br>
	                <textarea  class="form-control" name="observacionesPza" rows="1"><?php echo set_value('observacionesPza');?></textarea>
	                <br><span class="error" id ="error_observacionesPza"></span>
				</div>	
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Observaciones / Notas:</label>
	                <br>
	                <textarea  class="form-control" name="notas" rows="1"><?php echo set_value('notas');?></textarea>
	                <br><span class="error" id ="error_notas"></span>
				</div>	
			</div>

			<hr>
			<div class="row">
	            <div class="col-sm-5"></div>
	            <div class="col-sm-7" align="right">
	                <h4>Archivo(s):</h4><br>
	                <input type="file" multiple accept="image/*" name="uploadfiles[]"><br>
	                <input type="hidden" name="uploadfiles_resp" value="<?php if(isset($archivos_presupuesto_lineal)) echo $archivos_presupuesto_lineal;?>">
	            </div>
	        </div>
		</div>

		<br><br>
        <div class="col-sm-12" align="center">
            <br>
            <i class="fas fa-spinner cargaIcono" id="carga_envio"></i>
            <span class="error" id="errorEnvio"></span>
            <br>

            <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="1">
            <input type="hidden" name="" id="formulario" value="clientes">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="registro" id="registro" value="<?php if(isset($idRegistro)) echo $idRegistro; else echo '0'; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."ClienteRF_Lista/4";?>';" style="margin-right: 20px;">
                Regresar
            </button>

            <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="guardar_formulario">Guardar Formulario</button>
        </div>

        <br>
	</form>	
</section>