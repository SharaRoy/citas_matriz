<section style="margin:30px;">
	<form id="historial_clientes_refacciones" method="post" autocomplete="on" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<h4 style="text-align: center; font-weight: bold;">PARTES SOLICITADAS NO INSTALADAS (CLIENTES) - ACTUALIZACIÓN</h4>
			</div>
			<div class="col-md-2">
			</div>
		</div>

		<div class="panel-flotante">
			<h4 style="text-align: center; color: darkblue;">Datos del Generales</h4>
			<hr>

			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Orden:</label>
	                <br>
	                <input type="text" name="orden" id="ordenForm" class="form-control" value="<?= ((isset($orden)) ? $orden : '') ?>">
	                <br><span class="error" id ="error_orden"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Folio:</label>
	                <br>
	                <input type="text" name="folio" class="form-control" value="<?= ((isset($folio)) ? $folio : '') ?>">
	                <br><span class="error" id ="error_folio"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha folio:</label>
	                <br>
	                <input type="date" name="fecha_folio" class="form-control" value="<?= ((isset($fecha_folio)) ? $fecha_folio : date("Y-m-d")) ?>" style="padding-top: 0px;" disabled>
	                <br><span class="error" id ="error_fecha_folio"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Estatus:</label>
	                <br>
					<select class="form-control" name="estadoProceso">
						<option value="">SELECCIONE</option>
						<option value="DISP INSTALAR" style="background-color: #92D050;color:white;" <?= ((isset($estadoProceso)) ? (($estadoProceso == "DISP INSTALAR") ? 'selected': '') : '') ?> >DISP INSTALAR</option>
						<option value="NO CONTESTA" style="background-color: #FF0000;color:white;"<?= ((isset($estadoProceso)) ? (($estadoProceso == "NO CONTESTA") ? 'selected': '') : '') ?> >NO CONTESTA</option>
						<option value="PENDIENTE"<?= ((isset($estadoProceso)) ? (($estadoProceso == "PENDIENTE") ? 'selected': '') : '') ?>>PENDIENTE</option>
						<option value="NO IDENTIFICADO" style="background-color: #FFFF00;"<?= ((isset($estadoProceso)) ? (($estadoProceso == "NO IDENTIFICADO") ? 'selected': '') : '') ?> >NO IDENTIFICADO</option>
						<option value="POR RESERVAR" style="background-color: #00B0F0;color:white;"<?= ((isset($estadoProceso)) ? (($estadoProceso == "POR RESERVAR") ? 'selected': '') : '') ?>>POR RESERVAR</option>
						<option value="RESERVADO" style="background-color: #7030A0;color:white;"<?= ((isset($estadoProceso)) ? (($estadoProceso == "RESERVADO") ? 'selected': '') : '') ?> >RESERVADO</option>
					</select>
	                <br><span class="error" id ="error_estadoProceso"></span>

	                <br><br>
	                <a id="historiaComentariosCliente" data-value="<?php if(isset($idRegistro)) echo $idRegistro; else echo '0'; ?>" data-orden="<?php if(isset($orden)) echo $orden; else echo '0'; ?>" data-target="#historial_comentarios" data-toggle="modal" style="color:blue;font-size:14px;cursor: pointer;">
                        Ver cambios de estatus
                    </a>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Técnico:</label>
	                <br>
	                <input type="text" name="tecnico" class="form-control" value="<?= ((isset($tecnico)) ? $tecnico : '') ?> ">
	                <br><span class="error" id ="error_tecnico"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Asesor:</label>
	                <br>
	                <select class="form-control" name="asesor" id="asesor">
						<option value="">SELECCIONE</option>
						<?php if ($asesores != NULL): ?>
							<?php foreach ($asesores as $i => $ases): ?>
								<option value="<?= $ases->nombre ?>" <?php if(isset($asesor)) {if($asesor == $ases->nombre) echo 'selected';} ?>><?= $ases->nombre ?></option>
							<?php endforeach ?>
						<?php endif ?>
					</select>
	                <br><span class="error" id="error_asesor"></span>
				</div>
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Notas Estatus:</label>
	                <br>
	                <textarea  class="form-control" name="notaEstatus" rows="2"><?= ((isset($notaEstatus)) ? $notaEstatus : '') ?></textarea>
	                <br><span class="error" id ="error_notaEstatus"></span>
				</div>
			</div>

			<br>
			<h4 style="text-align: center; color: darkblue;">Datos de la unidad</h4>
			<hr>

			<div class="row">
				<div class="col-md-10">
					<label for="" style="font-size: 13px;font-weight: inherit;">Unidad en Taller:</label>
	                <br>
	                <input type="text" name="unidadTaller" class="form-control" value="<?= ((isset($unidadTaller)) ? $unidadTaller : '') ?>">
	                <br><span class="error" id ="error_unidadTaller"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">No. Unidad Inmovilizada:</label>
	                <br>
	                <input type="text" name="unidadInmo" class="form-control" value="<?= ((isset($unidadInmo)) ? $unidadInmo : '') ?>">
	                <br><span class="error" id ="error_unidadInmo"></span>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">Vehículo:</label>
	                <br>
	                <input type="text" name="modeloVehiculo" class="form-control" value="<?= ((isset($modeloVehiculo)) ? $modeloVehiculo : '') ?>">
	                <br><span class="error" id ="error_modeloVehiculo"></span>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">Serie:</label>
	                <br>
	                <input type="text" name="serie" class="form-control" value="<?= ((isset($serie)) ? $serie : '') ?>">
	                <br><span class="error" id ="error_serie"></span>
				</div>			
			</div>

			<br>
			<h4 style="text-align: center; color: darkblue;">Datos de la refacción</h4>
			<hr>

			<div id="contenedor_Refacciones">
				<input type="hidden" class="maxRenglon" name="maxRenglon" id="maxRenglon" value="<?php if(isset($refacciones)) {if(count($refacciones) > 1) echo count($refacciones); else echo "1";} else echo '1'; ?>">
				<input type="hidden" class="maxRenglon" name="maxRenglon2" id="" value="<?php if(isset($refacciones)) {if(count($refacciones) > 1) echo count($refacciones); else echo "1";} else echo '1'; ?>">
				<div class="row">
					<div class='col-md-2'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Cantidad:</label>
					</div>
					<div class='col-md-3'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Número de parte:</label>
					</div>
					<div class='col-md-5'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Descripción Corta:</label>
					</div>
					<div class='col-md-1' align='right'>
						<a class='btn btn-success' style='color:white;width: 1cm;' onclick="agregarRef_cliente(0)">
                    		<i class='fa fa-plus'></i>
						</a>
					</div>
					<div class='col-md-1' align='right'>
						<!--<a class='refaccionesBox btn btn-warning' data-target="#refacciones" data-toggle="modal" style="color:white;">
                            <i class="fa fa-search"></i>
                        </a>-->
					</div>				
				</div>

				<br>
				<!--Verificamos que se hayan cargado los registros -->
				<?php if (isset($refacciones)): ?>
					<!--Imprimimos las refacciones guardadas-->
					<?php foreach ($refacciones as $index => $valor): ?>
						<!--Comprobamos que no sea un renglon vacio -->
						<?php if (count($refacciones[$index]) > 0): ?>
							<br>
							<div class='row' id='renglon_<?=($index+1)?>'>
								<div class='col-md-2'>
					                <input type='number' step='0.01' min='1' name='cantidad_<?=($index+1)?>' id='cantidad_<?=($index+1)?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='<?= $refacciones[$index]["cantidad"]; ?>'>
					                <br><span class="error" id ="error_cantidad_<?=($index+1)?>"></span>

					                <input type="hidden" name="id_index_<?=($index+1)?>" id="id_index_<?=($index+1)?>" value='<?= $refacciones[$index]["id_refaccion"]; ?>'>
					                <input type="hidden" name="id_refaccion_<?=($index+1)?>" id="id_refaccion_<?=($index+1)?>" value='0'>
								</div>
								<div class='col-md-3'>
					                <input type='text' name='numParte_<?=($index+1)?>' id='numParte_<?=($index+1)?>' class='form-control' value='<?= $refacciones[$index]["clave_pieza"]; ?>'>
					                <br><span class="error" id ="error_numParte_<?=($index+1)?>"></span>
								</div>
								<div class='col-md-6'>
					                <textarea  class='form-control' name='descripcionCorta_<?=($index+1)?>' id='descripcionCorta_<?=($index+1)?>' rows='2'><?= $refacciones[$index]["descripcion"]; ?></textarea>
					                <br><span class="error" id ="error_descripcionCorta_<?=($index+1)?>"></span>
								</div>
								<div class='col-md-1' align='right'>
									<input type='hidden' name='' id='sub_indice_<?=($index+1)?>' class='form-control' value='<?=($index+1)?>'>
									<a class='btn btn-danger' style='color:white; width: 1cm;' onclick='quitarRef_cliente(<?=($index+1)?>)' data-target="#eliminarRefaccion" data-toggle="modal">
										<i class='fa fa-minus'></i>
									</a>
								</div>
							</div>
						<?php endif ?>
					<?php endforeach ?>
				<?php else: ?>
					<div class='row' id='renglon_1'>
						<div class='col-md-2'>
			                <input type='number' step='0.01' min='1' name='cantidad_1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='<?php if(set_value('cantidad_1') != "") echo set_value('cantidad_1'); else echo '1';?>'>
			                <input type='hidden' name='id_refaccion_1' id='id_refaccion_1' value='0'>
			                <input type='hidden' name='id_index_1' id='id_index_1' value='0'>
			                <br><span class="error" id ="error_cantidad_1"></span>
						</div>
						<div class='col-md-3'>
			                <input type='text' name='numParte_1' id='numParte_1' class='form-control' value='<?php echo set_value('numParte_1');?>'>
			                <br><span class="error" id ="error_numParte_1"></span>
						</div>
						<div class='col-md-5'>
			                <textarea  class='form-control' name='descripcionCorta_1' id='descripcionCorta_1' rows='2'><?php echo set_value('descripcionCorta_1');?></textarea>
			                <br><span class="error" id ="error_descripcionCorta_1"></span>
						</div>
					</div>
				<?php endif ?>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Pedido:</label>
	                <br>
	                <input type="date" name="fecha_pedido" class="form-control" value="<?= ((isset($fecha_pedido)) ? $fecha_pedido : '') ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_pedido"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">No. Remisión:</label>
	                <br>
	                <input type="text" name="noRemision" class="form-control" value="<?= ((isset($noRemision)) ? $noRemision : '') ?>">
	                <br><span class="error" id ="error_noRemision"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo:</label>
	                <br>
	                <input type="text" name="costo" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?= ((isset($costo)) ? $costo : '') ?>">
	                <br><span class="error" id ="error_costo"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha B.O.:</label>
	                <br> 
	                <input type="date" name="fechaBO" class="form-control" style="padding-top: 0px;" value="<?= ((isset($fechaBO)) ? $fechaBO : '') ?>">
	                <br><span class="error" id ="error_fechaBO"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Promesa:</label>
	                <br>
	                <input type="date" name="fecha_promesa" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?= ((isset($fecha_promesa)) ? $fecha_promesa : '') ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_promesa"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Recibe:</label>
	                <br>
	                <input type="date" name="fecha_recibe" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?= ((isset($fecha_recibe)) ? $fecha_recibe : '') ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_recibe"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Aviso de Taller:</label>
	                <br>
	                <input type="date" name="fecha_taller" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?= ((isset($fecha_taller)) ? $fecha_taller : '') ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_taller"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Instalación:</label>
	                <br>
	                <input type="date" name="fechaInstalacion" max="<?php echo date('Y-m-d'); ?>" class="form-control" value="<?= ((isset($fechaInstalacion)) ? $fechaInstalacion : '') ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fechaInstalacion"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Refacción Revisada Por:</label>
	                <br>
	                <input type="text" name="revisadaPor" id="" class="form-control" value="<?= ((isset($revisadaPor)) ? $revisadaPor : '') ?>">
	                <br><span class="error" id ="error_revisadaPor"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo Dist.:</label>
	                <br>
	                <input type="text" name="costoDist" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?= ((isset($costoDist)) ? $costoDist : '') ?>">
	                <br><span class="error" id ="error_costoDist"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo Total:</label>
	                <br>
	                <input type="text" name="costoTotal" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?= ((isset($costoTotal)) ? $costoTotal : '') ?>">
	                <br><span class="error" id ="error_costoTotal"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-6">
					<label for="" style="font-size: 14px;font-weight: inherit;">Tiempo Instalación Técnico:</label>
					<div class="row">
						<div class="col-md-5">
							<label for="" style="font-size: 13px;font-weight: inherit;">
								Tiempo:
							</label>
			                <br>
			                <select class="form-control" name="tiempo_tecnico" id="tiempo_tecnico">
								<option value="">SELECCIONE</option>
								<?php if (count($tiempo_int)): ?>
									<?php for ($i = 0; $i < count($tiempo_int); $i++): ?>
										<option value="<?=$tiempo_int[$i]?>" <?= ((isset($t_tec[0])) ? (($t_tec[0] == $tiempo_int[$i]) ? 'selected' : '') : '') ?>><?= $tiempo_int[$i] ?></option>
									<?php endfor ?>
								<?php endif ?>
							</select>
			                <br><span class="error" id ="error_tiempo_tecnico"></span>
						</div>

						<div class="col-md-4">
							<label for="" style="font-size: 13px;font-weight: inherit;">
								Unidad:
							</label>
			                <br>
			                <select class="form-control" name="unidad_tecnico" id="unidad_tecnico">
								<option value="Hora(s)" <?= ((isset($t_tec[1])) ? (($t_tec[1] == "Hora(s)") ? 'selected' : '') : '') ?>>Hora(s)</option>
								<option value="Día(s)" <?= ((isset($t_tec[1])) ? (($t_tec[1] == "Día(s)") ? 'selected' : '') : '') ?>>Día(s)</option>
							</select>
			                <br><span class="error" id ="error_unidad_tecnico"></span>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<label for="" style="font-size: 14px;font-weight: inherit;">Tiempo Instalación Cliente:</label>
					<div class="row">
						<div class="col-md-5">
							<label for="" style="font-size: 13px;font-weight: inherit;">
								Tiempo:
							</label>
			                <br>
			                <select class="form-control" name="tiempo_cliente" id="tiempo_cliente">
								<option value="">SELECCIONE</option>
								<?php if (count($tiempo_int)): ?>
									<?php for ($i = 0; $i < count($tiempo_int); $i++): ?>
										<option value="<?=$tiempo_int[$i]?>" <?= ((isset($t_cl[0])) ? (($t_cl[0] == $tiempo_int[$i]) ? 'selected' : '') : '') ?>><?= $tiempo_int[$i] ?></option>
									<?php endfor ?>
								<?php endif ?>
							</select>
			                <br><span class="error" id ="error_tiempo_cliente"></span>
						</div>

						<div class="col-md-4">
							<label for="" style="font-size: 13px;font-weight: inherit;">
								Unidad:
							</label>
			                <br>
			                <select class="form-control" name="unidad_cliente" id="unidad_cliente">
								<option value="Hora(s)" <?= ((isset($t_cl[1])) ? (($t_cl[1] == "Hora(s)") ? 'selected' : '') : '') ?>>Hora(s)</option>
								<option value="Día(s)" <?= ((isset($t_cl[1])) ? (($t_cl[1] == "Día(s)") ? 'selected' : '') : '') ?>>Día(s)</option>
							</select>
			                <br><span class="error" id ="error_unidad_cliente"></span>
						</div>
					</div>
				</div>	
			</div>

			<br>
			<div class="row">				
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Observaciones sobre la pieza:</label>
	                <br>
	                <textarea  class="form-control" name="observacionesPza" rows="2"><?= ((isset($observacionesPza)) ? $observacionesPza : '') ?></textarea>
	                <br><span class="error" id ="error_observacionesPza"></span>
				</div>	
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Observaciones / Notas:</label>
	                <br>
	                <textarea  class="form-control" name="notas" rows="2"><?= ((isset($notas)) ? $notas : '') ?></textarea>
	                <br><span class="error" id ="error_notas"></span>
				</div>	
			</div>

			<hr>
			<div class="row">
	            <div class="col-sm-4"></div>
	            <div class="col-sm-8" align="right">
	               	<h4 style="color:darkblue;">Archivo(s):</h4><br>
					<?php if (isset($archivos)): ?>
	                    <?php $contador = 1; ?>
	                    <?php for ($i = 0; $i < count($archivos); $i++): ?>
	                        <?php if ($archivos[$i] != ""): ?>
	                            <a href="<?php echo base_url().$archivos[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
	                                DOC. (<?php echo $contador; ?>)
	                            </a>
	                            
	                            <?php $contador++; ?>
	                        <?php endif; ?>
	                    <?php endfor; ?>

	                    <?php if (count($archivos) == 0): ?>
	                        <h4 style="text-align: right;color:black;">Sin archivos</h4>
	                    <?php endif; ?>
	                <?php endif; ?>

	                <hr>
	                <input type="file" multiple accept="image/*" name="uploadfiles[]"><br>
	                <input type="hidden" name="uploadfiles_resp" value="<?php if(isset($archivos_txt)) echo $archivos_txt;?>">
	            </div>
	        </div>
		</div>

		<br><br>
        <div class="col-sm-12" align="center">
            <br>
            <i class="fas fa-spinner cargaIcono" id="carga_envio"></i>
            <span class="error" id="errorEnvio"></span>
            <br>

            <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="2">
            <input type="hidden" name="" id="formulario" value="clientes">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="registro" id="registro" value="<?php if(isset($idRegistro)) echo $idRegistro; else echo '0'; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."ClienteRF_Lista/5";?>';" style="margin-right: 20px;">
                Regresar
            </button>
             
            <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="guardar_formulario">Actualizar Formulario</button>
        </div>

        <br>
	</form>	
</section>

<!-- Modal para ver el historial de comentarios -->
<div class="modal fade" id="historial_comentarios" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Historial de cambios de estatus</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="" style="width:100%;">
                            <thead>
                                <tr style="background-color: #ddd;">
                                	<td style="width:8%;"><h5 align="center">#</h5></td>
                                    <td style="width:20%;"><h5 align="center">Fecha</h5></td>
                                    <td style="width:22%;"><h5 align="center">Estatus</h5></td>
                                    <td style="width:50%;"><h5 align="center">Nota</h5></td>
                                </tr>
                            </thead>
                            <tbody id="tablaComnetario">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="identificador_historial" value="G">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para filtrar las refacciones-->
<div class="modal fade" id="refacciones" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Buscador de Refacciones</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <br>
                        <input type='radio' id='' class='input_field' name='filtros' value="clave" checked>
                        Buscar por clave
                        <br><br>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding:0px;border: 0px;">
                                <!-- <i class="fa fa-search"></i> -->
                                <button type="button" class="btn btn-default" id="busquedaClaveBtn"><i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control" id="claveBuscar" placeholder="Buscar..." style="width:100%;height:35px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <br>
                        <input type='radio' class='input_field' name='filtros' value="descripcion">
                        Buscar por descripción
                        <br><br>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding:0px;border: 0px;">
                                <!-- <i class="fa fa-search"></i> -->
                                <button type="button" class="btn btn-default" id="busquedaDescripBtn" ><i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control" id="descipBuscar" placeholder="Buscar..." style='width:100%;'>
                        </div>
                    </div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <br>
                        <label id="alertaConexion"></label>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <label for="">Resultados:</label>
                        <br>
                        <select class="form-control" id="resultadosVaciado" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-2"></div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Clave:</label>
                        <input type='text' class='form-control' id="claveRefaccion" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-6" style="padding-top: 20px;">
                        <input type='text' class='form-control' id="tipoBuscar" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="">Descripcion:</label>
                        <input type='text' class='form-control' id="descripcionRefacciones" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Precio:</label>
                        <input type='text' class='form-control' id="precioRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Existencia:</label>
                        <input type='text' class='form-control' id="existenciaRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Unidad:</label>
                        <input type='text' class='form-control' id="unidadRefacciones" style='width:100%;' disabled>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="indiceOperacion" value="">
                <input type="hidden" name="" id="existeRefacciones" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarOperacion">Cerrar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="bajaOperacion">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="eliminarRefaccion" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #ebccd1;font-weight: bold;">
            <div class="modal-header">
                <h5 class="modal-title" style="font-weight: bold;">ELIMINAR REFACCIÓN</h5>
            </div>
            <div class="modal-body">
                <div class="" align="center">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <br>
                    <strong style="font-size:14px;" id="contenido">
                        ¿Seguro que quiere eliminar la siguiente refacción?
                        <br><br>
                        <samp id="label_refaccion" style="color:black;"></samp>
                        <br><br>

                        <div class="" align="left">
                            <label style="color: darkred;font-size: 13px;">
                                NOTA: Esta acción no se puede deshacer, se eliminara en automático del formulario.
                            </label>
                            <br>
                        </div>
                    </strong>
                    <label id="notificacion" style="color: darkred;font-size: 13px;"></label>
                    <br>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" id="notificacion_cerrar_Envio" style="display: none;">Aceptar</button>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="id_cita" value="<?php if(isset($id_cita)) echo $id_cita; ?>">
                <input type="hidden" name="" id="id_refaccion_renglon" value="">
                <input type="hidden" name="" id="renglon_eliminar" value="">
                <button type="button" class="btn btn-warning" data-dismiss="modal" id="cerrar_Envio">Cancelar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" name="btnSign" id="btn_envio_refaccion">Eliminar</button>
            </div>
        </div>
    </div>
</div>