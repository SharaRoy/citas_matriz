<div >
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <br>
    <div class="panel-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-8">
                    <h3 align="center">RECEPCIÓN DE BODYSHOP</h3>
                </div>
                <div class="col-sm-4" align="right">
                    <?php if (!isset($modalidad)): ?>
                        <input type="button" class="btn btn-success" style="color:white; margin-top:20px;" onclick="location.href='<?=base_url()."IV_BodyShop/0";?>';" value="NUEVA RECEPCIÓN">
                    <?php endif; ?>
                </div>
            </div>

            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-sm-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;">
                                    <td align="center" style="width: 5%;vertical-align: middle;">#</td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ID</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>VIN</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;"><strong>TELEFONO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA REGISTRO</strong></td>
                                    <?php if (isset($modalidad)): ?>
                                        <td align="center"><strong>ACCIONES</strong></td>
                                    <?php else: ?>
                                        <td align="center" colspan="2"><strong>ACCIONES</strong></td>
                                    <?php endif; ?>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idRegistro)): ?>
                                    <?php foreach ($idRegistro as $index => $valor): ?> 
                                        <tr style="font-size:12px;">
                                            <td align="center" style="width:10%;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $vin[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $nombreProp[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $telefono[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $fecha[$index]; ?>
                                            </td>
                                            <?php if (isset($modalidad)): ?>
                                                <td align="center" style="width:15%;">
                                                    <!-- <input type="button" class="btn btn-primary" style="color:white;" onclick="location.href='<?=base_url()."IV_BodyShop_PDF/".$idOrden[$index];?>';" value="PDF"> -->
                                                    <a href="<?=base_url()."IV_BodyShop_PDF/".$idOrden[$index];?>" class="btn btn-primary" style="color:white;" target="_blank">PDF</a>
                                                </td>
                                            <?php else: ?>
                                                <td align="center" style="width:15%;">
                                                    <input type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."IV_BodyShop/".$idOrden[$index];?>';" value="Editar">
                                                </td>
                                                <td align="center" style="width:15%;">
                                                    <!-- <input type="button" class="btn btn-primary" style="color:white;" onclick="location.href='<?=base_url()."IV_BodyShop_PDF/".$idOrden[$index];?>';" value="PDF"> -->
                                                    <a href="<?=base_url()."IV_BodyShop_PDF/".$idOrden[$index];?>" class="btn btn-primary" style="color:white;" target="_blank">PDF</a>
                                                </td>
                                            <?php endif; ?>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td <?php if(isset($modalidad)) echo "colspan='9'"; else echo "colspan='1'0"; ?> style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
