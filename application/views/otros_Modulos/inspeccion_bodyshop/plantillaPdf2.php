<style media="screen">
    td,tr{
        /* padding: 0px 5px 5px 0px !important; */
        font-size: 8px;
        color:#337ab7;
    }

    .centradoBarra{
        position: relative;
        height: 4px;
        /* margin-top: -12px; */
        margin-left: -30px;
        top: 1px;
        left: 9px;
        /* transform: translate(-50%, -50%); */
    }

    .contenedorImgFondo{
        position: relative;
        display: inline-block;
        text-align: center;
    }

    .imgFondo{
        /* width: 300px; */
        /* height: 60px; */
        margin-top: -15px;
    }

    .divAprobado{
        width:10px;
        height:10px;
        border: 0.5px solid;
        background-color: #ffffff;
        position: relative;
        vertical-align: middle;
    }
/*
    .divCheckBG{
        position: relative;
        width:10px;
        height:10px;
        border: 0.5px solid;
        background-color: #ffffff;
        display: table-cell;
        vertical-align: middle;
    } */

</style>
<page backtop="5mm" backbottom="5mm" backleft="5mm" backright="5mm">
    <table style="margin-left:-23px;margin-top:-10px;">
        <tr>
            <td style="width:225px;" rowspan="4">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:220px;height:50px;">
            </td>
            <td colspan="3" style="width:290px;">
                <table style="width:280px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Nombre del Cliente:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:200px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($nombreCliente)) echo $nombreCliente; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:160px;">
                <table style="width:155px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Fecha:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:100px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($fecha)) echo $fecha; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="width:290px;">
                <table style="width:280px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Correo Electrónico:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:200px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($correoCliente)) echo $correoCliente; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:160px;">
                <table style="width:155px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Teléfono:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:100px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($telefonoCliente)) echo $telefonoCliente; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:120px;">
                <table style="width:115px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Vehículo:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:90px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($vehiculo)) echo $vehiculo; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:120px;">
                <table style="width:115px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Placas:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:90px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($placas)) echo $placas; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:120px;">
                <table style="width:115px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Kilometraje:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:90px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($kilometraje)) echo $kilometraje; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:120px;">
                <table style="width:115px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">ID.:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:90px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($noOrden)) echo $noOrden; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width:120px;">
                <table style="width:115px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Serie:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:90px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($serie)) echo $serie; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:120px;">
                <table style="width:115px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Color:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:90px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($color)) echo $color; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:120px;">
                <table style="width:115px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Torre:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:90px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($torre)) echo $torre; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width:120px;">
                <table style="width:115px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Tipo de Motor:&nbsp;&nbsp;</label>
                        </td>
                        <td style="border-bottom: 0.5px solid black;min-width:90px;" align="center">
                            <label for="">&nbsp;&nbsp;<?php if(isset($tipoMotor)) echo $tipoMotor; else echo "Sin información" ?></label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width:225px;background-color:#eee;padding-top:6px;">
                <label style="font-size:10.5px;font-weight:200;">INSPECCIÓN VISUAL E INVENTARIO EN RECEPCIÓN</label>
            </td>
            <td colspan="3">
                <table style="width:445px;">
                    <tr>
                        <td style="width:auto;">
                            <label for="">Tipo de Transmisión:&nbsp;&nbsp;</label>
                        </td>
                        <td style="min-width:180px;" align="center">
                            Automática &nbsp;
                            <div class="divAprobado" style="float:left;">
                                <?php if (isset($tipoTransmision)): ?>
                                    <?php if ($tipoTransmision == "Automática"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                        <td style="min-width:180px;" align="center">
                            Manual &nbsp;
                            <div class="divAprobado" style="float:left;">
                                <?php if (isset($tipoTransmision)): ?>
                                    <?php if ($tipoTransmision == "Manual"): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <!-- Parte superior (diagnostico de la orden de servicio) -->
    <table style="margin-left:-23px;">
        <tr>
            <td>
                <table style="width:170px;border:0.5px solid #337ab7;float: right;">
                    <tr>
                        <td colspan="2" style="border-bottom:0.5px solid #337ab7;">
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:60px;">
                                        <strong style="font-size:10px;">Interiores</strong>
                                    </td>
                                    <td style="width:90px;">
                                        Opera Si(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">)
                                        /No(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">)
                                        /<br>No cuenta (NC)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Póliza Garantía/Manual de Prop
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("PólizaGarantia.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("PólizaGarantia.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("PólizaGarantia.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Seguro rines.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("SeguroRines.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("SeguroRines.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("SeguroRines.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            Indicadores de falla Activados:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-bottom: 0.5px solid #337ab7;">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/imgs/05.png" style="width:20px;">
                                    </td>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/imgs/02.png" style="width:20px;">
                                    </td>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/imgs/07.png" style="width:20px;">
                                    </td>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/imgs/01.png" style="width:20px;">
                                    </td>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/imgs/06.png" style="width:20px;">
                                    </td>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/imgs/04.png" style="width:20px;">
                                    </td>
                                    <td>
                                        <img src="<?php echo base_url();?>assets/imgs/03.png" style="width:20px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <?php if (isset($interiores)): ?>
                                            <?php if (in_array("IndicadorGasolina.Si",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                            <?php elseif (in_array("IndicadorGasolina.No",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                            <?php elseif (in_array("IndicadorGasolina.NC",$interiores)): ?>
                                                <?php echo "<label style='color:black;'>NC</label>"; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($interiores)): ?>
                                            <?php if (in_array("IndicadorMantenimento.Si",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                            <?php elseif (in_array("IndicadorMantenimento.No",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                            <?php elseif (in_array("IndicadorMantenimento.NC",$interiores)): ?>
                                                <?php echo "<label style='color:black;'>NC</label>"; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($interiores)): ?>
                                            <?php if (in_array("SistemaABS.Si",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                            <?php elseif (in_array("SistemaABS.No",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                            <?php elseif (in_array("SistemaABS.NC",$interiores)): ?>
                                                <?php echo "<label style='color:black;'>NC</label>"; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($interiores)): ?>
                                            <?php if (in_array("IndicadorFrenos.Si",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                            <?php elseif (in_array("IndicadorFrenos.No",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                            <?php elseif (in_array("IndicadorFrenos.NC",$interiores)): ?>
                                                <?php echo "<label style='color:black;'>NC</label>"; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($interiores)): ?>
                                            <?php if (in_array("IndicadorBolsaAire.Si",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                            <?php elseif (in_array("IndicadorBolsaAire.No",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                            <?php elseif (in_array("IndicadorBolsaAire.NC",$interiores)): ?>
                                                <?php echo "<label style='color:black;'>NC</label>"; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($interiores)): ?>
                                            <?php if (in_array("IndicadorTPMS.Si",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                            <?php elseif (in_array("IndicadorTPMS.No",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                            <?php elseif (in_array("IndicadorTPMS.NC",$interiores)): ?>
                                                <?php echo "<label style='color:black;'>NC</label>"; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($interiores)): ?>
                                            <?php if (in_array("IndicadorBateria.Si",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                            <?php elseif (in_array("IndicadorBateria.No",$interiores)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                            <?php elseif (in_array("IndicadorBateria.NC",$interiores)): ?>
                                                <?php echo "<label style='color:black;'>NC</label>"; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Rociadores y Limpiaparabrisas.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Rociadores.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Rociadores.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Rociadores.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Claxon.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Claxon.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Claxon.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Claxon.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-bottom:0.5px solid #337ab7;">
                            Luces:
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            &nbsp;&nbsp;&nbsp;&nbsp;Delanteras.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("LucesDelanteras.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("LucesDelanteras.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("LucesDelanteras.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            &nbsp;&nbsp;&nbsp;&nbsp;Traseras.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("LucesTraseras.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("LucesTraseras.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("LucesTraseras.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            &nbsp;&nbsp;&nbsp;&nbsp;Stop.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("LucesStop.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("LucesStop.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("LucesStop.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Radio / Caratulas.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Caratulas.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Caratulas.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Caratulas.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Pantallas.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Pantallas.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Pantallas.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Pantallas.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            A/C
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("AA.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("AA.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("AA.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Encendedor.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Encendedor.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Encendedor.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Encendedor.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Vidrios.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Vidrios.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Vidrios.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Vidrios.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Espejos.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Espejos.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Espejos.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Espejos.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Seguros eléctricos.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("SegurosEléctricos.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("SegurosEléctricos.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("SegurosEléctricos.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Disco compacto.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("CD.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("CD.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("CD.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:75%;border-bottom:0.5px solid #337ab7;">
                            Asientos y vestiduras.
                        </td>
                        <td style="width:25%;border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Vestiduras.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Vestiduras.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Vestiduras.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="width:70%;">
                            Tapetes.
                        </td>
                        <td style="border-left:0.5px solid #337ab7;width:30%;" align="center">
                            <?php if (isset($interiores)): ?>
                                <?php if (in_array("Tapetes.Si",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Tapetes.No",$interiores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Tapetes.NC",$interiores)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="width:100%;">
                    <tr>
                        <td align="center" style="width:40%">
                            <img src="<?php echo base_url().'assets/imgs/gas.jpeg'; ?>" style="width:30px;" alt="">
                        </td>
                        <td style="margin-left:5px;border:0.5px solid #337ab7;border-right: 0.5px solid #ffffff;width:30%;">
                            Nivel de <br>Gasolina:
                        </td>
                        <td align="center" style="margin-left:5px;border:0.5px solid #337ab7;border-left: 0.5px solid #ffffff;width:30%;">
                            <label for=""> <strong><?php if(isset($nivelGasolina)) echo $nivelGasolina; else echo "0/0";?></strong> </label>
                        </td>
                    </tr>
                </table>
            </td>

            <td align="center">
                <table style="width:215px">
                    <tr>
                        <td>
                            <img src="<?php echo base_url().'assets/imgs/carroceria.png'; ?>" alt="" style="width:200px; height:30px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php if (isset($danosImg)): ?>
                                <img src="<?php echo base_url().$danosImg; ?>" alt="" style="width:210px;">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/car.jpeg'; ?>" alt="" style="width:210px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="border: 0.5px solid #337ab7;">
                    <tr>
                        <td colspan="3" rowspan="2" align="left">
                            <label style="font-size:10px;font-weight:bold;">Sistema de frenos</label>
                        </td>
                        <td colspan="3" align="right">
                            solo revisar 2 m.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left" style="">
                            Medida Actual
                            <br>
                        </td>
                    </tr>
                    <tr style="">
                        <td align="center" style="border: 0.5px solid #337ab7;border-top: 0.5px solid white;">
                            Ruedas
                        </td>
                        <td align="center" style="border: 0.5px solid #337ab7;border-top: 0.5px solid white;" colspan="2">
                            Balata/Zapata
                        </td>
                        <td align="center" style="border: 0.5px solid #337ab7;border-top: 0.5px solid white;" colspan="2">
                            Disco/Tambor
                        </td>
                        <td align="center" style="border: 0.5px solid #337ab7;border-top: 0.5px solid white;">
                            Neumático
                        </td>
                    </tr>
                    <tr style="">
                        <td align="left" style="width:25%;border: 0.5px solid #337ab7;">
                            Delantera Derecha
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaDD.Balata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaDD.Zapata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaDD.Disco",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaDD.Tambor",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaDD.Neumatico",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr style="">
                        <td align="left" style="width:25%;border: 0.5px solid #337ab7;">
                            Delantera Izquierda
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasDI.Balata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasDI.Zapata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasDI.Disco",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasDI.Tambor",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasDI.Neumatico",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr style="">
                        <td align="left" style="width:25%;border: 0.5px solid #337ab7;">
                            Trasera Derecha
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaTD.Balata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaTD.Zapata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaTD.Disco",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaTD.Tambor",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedaTD.Neumatico",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr style="">
                        <td align="left" style="width:25%;border: 0.5px solid #337ab7;">
                            Trasera Izquierda
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasTI.Balata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasTI.Zapata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasTI.Disco",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasTI.Tambor",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("RuedasTI.Neumatico",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr style="">
                        <td align="left" style="width:40%;border: 0.5px solid #337ab7;">
                            Refacción
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("Refaccion.Balata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("Refaccion.Zapata",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("Refaccion.Disco",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;border-right: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("Refaccion.Tambor",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="center" style="border-top: 0.5px solid #337ab7;border-bottom: 0.5px solid #337ab7;">
                            <?php if (isset($sisFrenos)): ?>
                                <?php if (in_array("Refaccion.Neumatico",$sisFrenos)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </td>

            <td style="">
                <div class="row" style="width:175px;height:60px;border:0.5px solid #337ab7;">
                    <p align=justify style="">
                        <?php if (isset($observaciones1)): ?>
                            <?php echo $observaciones1 ?>
                        <?php else: ?>
                            Observaciones:
                        <?php endif; ?>
                    </p>
                </div>

                <br>
                <table style="width:100%;border:0.5px solid #337ab7;">
                    <tr class="headers_table" align="center">
                        <td colspan="2" align="center" style="border-bottom:0.5px solid #337ab7;">
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:70px;" align="left">
                                        <strong style="font-size:10px;">Cofre</strong>
                                    </td>
                                    <td style="width:70px;" align="left">
                                        Nivel Correcto(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">)
                                        /<br>Nivel incorrecto(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">)
                                        /<br>Fuga (F)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:60%;">
                            Aceite de Motor.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($cofre)): ?>
                                <?php if (in_array("AceiteMotor.Si",$cofre)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("AceiteMotor.No",$cofre)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("AceiteMotor.F",$cofre)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Liquido de Frenos.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($cofre)): ?>
                                <?php if (in_array("LiquidoFrenos.Si",$cofre)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("LiquidoFrenos.No",$cofre)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("LiquidoFrenos.F",$cofre)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Limpiaparabrisas.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($cofre)): ?>
                                <?php if (in_array("Limpiaparabrisas.Si",$cofre)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Limpiaparabrisas.No",$cofre)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Limpiaparabrisas.F",$cofre)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Anticongelante.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($cofre)): ?>
                                <?php if (in_array("Anticongelante.Si",$cofre)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Anticongelante.No",$cofre)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Anticongelante.F",$cofre)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                            <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                                Liquido de Dirección.
                            </td>
                            <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                                <?php if (isset($cofre)): ?>
                                    <?php if (in_array("LiquidoDirección.Si",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                    <?php elseif (in_array("LiquidoDirección.No",$cofre)): ?>
                                        <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                    <?php elseif (in_array("LiquidoDirección.F",$cofre)): ?>
                                        <?php echo "<label style='color:black;'>F</label>"; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                </table>

                <br>
                <table style="width:100%;border:0.5px solid #337ab7;">
                    <tr class="headers_table" align="center">
                        <td colspan="2" align="center" style="border-bottom:0.5px solid #337ab7;">
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:60px;">
                                        <strong style="font-size:10px;">Cajuela</strong>
                                    </td>
                                    <td style="width:90px;">
                                        Opera Si(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">)
                                        /No(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">)
                                        /<br>No cuenta (NC)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Herramienta.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($cajuela)): ?>
                                <?php if (in_array("Herramienta.Si",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Herramienta.No",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Herramienta.NC",$cajuela)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:0.5px solid #337ab7;">
                            Gato / Llave.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;">
                            <?php if (isset($cajuela)): ?>
                                <?php if (in_array("eLlave.Si",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("eLlave.No",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("eLlave.NC",$cajuela)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:0.5px solid #337ab7;">
                            Reflejantes.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;">
                            <?php if (isset($cajuela)): ?>
                                <?php if (in_array("Reflejantes.Si",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Reflejantes.No",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Reflejantes.NC",$cajuela)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:0.5px solid #337ab7;">
                            Cables.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;">
                            <?php if (isset($cajuela)): ?>
                                <?php if (in_array("Cables.Si",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Cables.No",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Cables.NC",$cajuela)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:0.5px solid #337ab7;">
                            Extintor.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;">
                            <?php if (isset($cajuela)): ?>
                                <?php if (in_array("Extintor.Si",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Extintor.No",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Extintor.NC",$cajuela)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Llanta Refacción.
                        </td>
                        <td align="center" style="border-left:0.5px solid #337ab7;">
                            <?php if (isset($cajuela)): ?>
                                <?php if (in_array("LlantaRefaccion.Si",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("LlantaRefaccion.No",$cajuela)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("LlantaRefaccion.NC",$cajuela)): ?>
                                    <?php echo "<label style='color:black;'>NC</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </td>

            <td>
                <table style="width:100%;border:0.5px solid #337ab7;">
                    <tr class="headers_table" align="center">
                        <td colspan="2" align="center" style="border-bottom:0.5px solid #337ab7;">
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:60px;">
                                        <strong style="font-size:10px;">Inferior</strong>
                                    </td>
                                    <td style="width:70px;">
                                        Bien(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">)
                                        /Mal(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">)
                                        /<br>Fuga (F)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Sistema de Escape.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($inferior)): ?>
                                <?php if (in_array("SisEscape.Si",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("SisEscape.No",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("SisEscape.F",$inferior)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Amortiguadores.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($inferior)): ?>
                                <?php if (in_array("Amortiguador.Si",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Amortiguador.No",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Amortiguador.F",$inferior)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Tuberias.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($inferior)): ?>
                                <?php if (in_array("Tuberias.Si",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Tuberias.No",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Tuberias.F",$inferior)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Transeje / Transmisión.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($inferior)): ?>
                                <?php if (in_array("Transmisión.Si",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Transmisión.No",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Transmisión.F",$inferior)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Sistema de Dirección.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($inferior)): ?>
                                <?php if (in_array("SisDirección.Si",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("SisDirección.No",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("SisDirección.F",$inferior)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Chasis sucio.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($inferior)): ?>
                                <?php if (in_array("Chasis.Si",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Chasis.No",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Chasis.F",$inferior)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr align="">
                        <td style="border-bottom:0.5px solid #337ab7;width:70%;">
                            Golpes Especifico.
                        </td>
                        <td align="center" style="border-bottom:0.5px solid #337ab7;border-left:0.5px solid #337ab7;width:30%;">
                            <?php if (isset($inferior)): ?>
                                <?php if (in_array("Golpes.Si",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Golpes.No",$inferior)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php elseif (in_array("Golpes.F",$inferior)): ?>
                                    <?php echo "<label style='color:black;'>F</label>"; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <br><br>
                <table style="width:100%;border:0.5px solid #337ab7;">
                    <tr class="headers_table" align="center">
                        <td colspan="2" align="center" style="border-bottom:0.5px solid #337ab7;">
                            <table style="width:100%;">
                                <tr>
                                    <td style="width:60px;">
                                        <strong style="font-size:10px;">Exteriores</strong>
                                    </td>
                                    <td style="width:70px;">
                                        Si(<img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">)
                                        /No(<img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">)
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #337ab7;width:70%;">
                            Tapones rueda.
                        </td>
                        <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;width:30%;">
                            <?php if (isset($exteriores)): ?>
                                <?php if (in_array("TaponesRueda.Si",$exteriores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("TaponesRueda.No",$exteriores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #337ab7;width:70%;">
                            Gomas de limpiadores.
                        </td>
                        <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;width:30%;">
                            <?php if (isset($exteriores)): ?>
                                <?php if (in_array("Gotas.Si",$exteriores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Gotas.No",$exteriores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom:1px solid #337ab7;">
                            Antena.
                        </td>
                        <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                            <?php if (isset($exteriores)): ?>
                                <?php if (in_array("Antena.Si",$exteriores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("Antena.No",$exteriores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Tapón de gasolina.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7;">
                            <?php if (isset($exteriores)): ?>
                                <?php if (in_array("TaponGasolina.Si",$exteriores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:8px;" alt="">
                                <?php elseif (in_array("TaponGasolina.No",$exteriores)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:7px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <br><br>
                <div class="" style="width:150px;border:0.5px solid #337ab7;">
                    <label style="font-size:10px;">¿Deja artículos personales?</label>
                </div>
                <div class="" style="width:150px;border:0.5px solid #337ab7;">
                    <table style="width:100%;font-size:10px;">
                        <tr>
                            <td align="center">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label for="">&nbsp;Si&nbsp;</label>
                                <div class="divAprobado">
                                    <?php if (isset($articulos)): ?>
                                        <?php if (strtoupper($articulos) == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </td>
                            <td align="center">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label for="">&nbsp;No&nbsp;</label>
                                <div class="divAprobado">
                                    <?php if (isset($articulos)): ?>
                                        <?php if (strtoupper($articulos) == "0"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/cancel2.png'; ?>" style="width:10px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="" style="width:150px;border:0.5px solid #337ab7;">
                    <label style="font-size:10px;">¿Cuales?</label>
                </div>
                <div class="" style="width:150px;border:0.5px solid #337ab7;">
                    <p style="font-size:10px;width:140px;" align="justify">
                        <?php if(isset($cualesArticulos))  echo $cualesArticulos; else echo "Ninguno";?>
                    </p>
                </div>
            </td>
        </tr>
    </table>

    <!-- Parte inferior (diagnostico d ela voz cliente) -->
    <table style="margin-top:-10px;">
        <tr>
            <!-- Lado izquierdo de la parte inferior -->
            <td style="width:310px;">
                <table style="">
                    <tr>
                        <td colspan="4">
                            <h4> <strong>Identificación de necesidades <br>de servicio.</strong> </h4>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border: 1px solid #337ab7;height:50px;background-color:#ffffff;">
                            <h6>Definición de falla.</h6>
                            <?php if (isset($definicionFalla)): ?>
                                <u><?php echo $definicionFalla ?></u>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color:#ffffff;">
                            <br><br>
                            <table>
                                <tr>
                                    <td align="center">
                                        <img src="<?php echo base_url().'/assets/imgs/reducidos/vista-min.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">
                                    </td>
                                    <td align="center">
                                        <img src="<?php echo base_url().'/assets/imgs/reducidos/tacto-min.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">
                                    </td>
                                    <td align="center">
                                        <img src="<?php echo base_url().'/assets/imgs/reducidos/oido-min.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">
                                    </td>
                                    <td align="center">
                                        <img src="<?php echo base_url().'/assets/imgs/reducidos/olfato-min.png' ?>" alt="" style="width:38px;height: 38px;margin-top:6px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <?php if (isset($sentidosFalla)): ?>
                                            <?php if (in_array("Vista",$sentidosFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($sentidosFalla)): ?>
                                            <?php if (in_array("Tacto",$sentidosFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($sentidosFalla)): ?>
                                            <?php if (in_array("Oido",$sentidosFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center">
                                        <?php if (isset($sentidosFalla)): ?>
                                            <?php if (in_array("Olfalto",$sentidosFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:15px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="background-color:#ffffff;"><br><br></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="background-color:#ffffff;">
                            <br>
                            <?php if (isset($imgFallas)): ?>
                                <img src="<?php echo base_url().$imgFallas; ?>" alt="" style="width:130px;height: 210px;">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:130px;height: 210px;">
                            <?php endif; ?>
                        </td>
                        <td colspan="2">
                            <br>
                            <table style="border: 1px solid #337ab7; width:160px; background-color:#ffffff;">
                                <tr>
                                    <td colspan="2" style="border-bottom:1px solid #337ab7;">
                                        La falla se presenta cuando:
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Arranca el vehículo.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Arranque",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Inicia movimiento.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7;border-bottom:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Inicio Movimiento",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Disminuye la velocidad.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Menos Velocidad",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Da vuelta a la izquierda.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Vuelta Izquierda",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Da vuelta a la derecha.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Vuelta Derecha",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Pasa un tope.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Tope",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Pasa un bache.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Bache",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Cambia la velocidad.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Cambio Velocidad",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Está en movimiento.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Movimiento",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Constantemente.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Constantemente",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:60%; border-bottom:1px solid #337ab7;"  align="left">
                                        Esporádicamente.
                                    </td>
                                    <td style="width:30%;border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;"  align="center">
                                        <?php if (isset($presentaFalla)): ?>
                                            <?php if (in_array("Esporádicamente",$presentaFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="width:280px;border: 1px solid #337ab7;">
                    <tr>
                        <td colspan="4" style="border-bottom:1px solid #337ab7;">La falla se percibe en:</td>
                    </tr>
                    <tr>
                        <td align="left" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;width:40%;">
                            Volante.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;width:10%;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Volante",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="left" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;width:40%;">
                            Cofre.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;width:10%;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Cofre",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;">
                            Asiento.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Asiento",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="left" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;">
                            Cajuela.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Cajuela",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;">
                            Cristales.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Cristales",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="left" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;">
                            Toldo.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Toldo",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;">
                            Carrocería.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Carrocería",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                        <td align="left" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;">
                            Debajo del vehículo.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7; border-bottom:1px solid #337ab7;">
                            <?php if (isset($percibeFalla)): ?>
                                <?php if (in_array("Debajo",$percibeFalla)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;">
                            Estancado:
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="width:100%;">
                                <tr>
                                    <td align="center" style="width:25%;">
                                        Dentro.<br>
                                        <?php if (isset($percibeFalla)): ?>
                                            <?php if (in_array("Dentro",$percibeFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;width:25%;">
                                        Fuera.<br>
                                        <?php if (isset($percibeFalla)): ?>
                                            <?php if (in_array("Fuera",$percibeFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;width:25%;">
                                        Frente.<br>
                                        <?php if (isset($percibeFalla)): ?>
                                            <?php if (in_array("Frente",$percibeFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;width:25%;">
                                        Detrás.<br>
                                        <?php if (isset($percibeFalla)): ?>
                                            <?php if (in_array("Detrás",$percibeFalla)): ?>
                                                <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" style="width:10px;" alt="">
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>

            <!-- Lado derecho de la parte inferior -->
            <td>
                <table style="border: 0.5px solid #337ab7; width:100%;">
                    <tr style="border-bottom: 0.5px solid #337ab7;">
                        <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                            <h6 style="font-size:13px;font-weight:bold;">Condiciones ambientales.</h6>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/metrix2.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                            <br>
                            <?php if (isset($logTempBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logTempBarra.'px;'; ?>;margin-left:50px;margin-top: -12px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/metrix_humedad2.png' ?>" alt="" style="width:410px;height:35px;" class="imgFondo">
                            <br>
                            <?php if (isset($logHumBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logHumBarra.'px;'; ?>;margin-left:50px;margin-top: -9px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/metrix_viento2.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logVientoBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logVientoBarra.'px;'; ?>;margin-left:50px;margin-top: -4px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="border: 0.5px solid #337ab7; width:100%;">
                    <tr style="border-bottom: 0.5px solid #337ab7;">
                        <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                            <label style="font-size:13px;font-weight:bold;">Condiciones operativas.</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/velocidad3.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                            <br>
                            <?php if (isset($logVelBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logVelBarra.'px;'; ?>;margin-left:50px;margin-top: -15px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/cambio3.png' ?>" alt="" style="width:410px;height:40px;" class="imgFondo">
                            <br>
                            <?php if (isset($logCam1Barra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logCam1Barra.'px;'; ?>;margin-left:50px;margin-top: -15px;">
                            <?php endif; ?>
                            <?php if (isset($logCam2Barra)): ?>
                                <br>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logCam2Barra.'px;'; ?>;margin-left:278px;margin-top: -10px;">
                            <?php endif; ?>
                            <?php if (isset($transmision)): ?>
                                <?php if ($transmision == "2"): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" class="centradoBarra" style="width:10px;height: 10px!important;margin-left:3px;margin-top: -10px;" alt="">
                                <?php elseif ($transmision == "4"): ?>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" class="centradoBarra" style="width:10px;height: 10px!important;margin-left:30px;margin-top: -10px;" alt="">
                                    <!-- <img src="<?php echo base_url().'assets/imgs/check2.png'; ?>" class="centradoBarra" style="width:10px;height: 10px!important;margin-left:30px;margin-top: 5px;" alt=""> -->
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/rpm3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logRMPBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logRMPBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/carga3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logCargaBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logCargaBarra.'px;'; ?>;margin-left:50px;margin-top: -15px;">
                            <?php endif; ?>
                            <?php if (isset($remolque)): ?>
                                <?php if ($remolque == "1"): ?>
                                    <br>
                                    <img src="<?php echo base_url().'assets/imgs/Check.png'; ?>" class="centradoBarra" style="width:15px;height: 15px!important;margin-left:345px;margin-top: -12px;" alt="">
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/pasajeros3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logPasajeBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" style="width:<?php echo $logPasajeBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                            <?php if (isset($logCajuelaBarra)): ?>
                                <br>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" style="width:<?php echo $logCajuelaBarra.'px;'; ?>;margin-left:295px;margin-top: -8px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="border: 0.5px solid #337ab7; width:100%;">
                    <tr style="border-bottom: 0.5px solid #337ab7;">
                        <td colspan="2" align="left" style="margin:0px 0px 0px 0px; padding: 0px 0px 0px 0px;">
                            <label style="font-size:13px;font-weight:bold;">Condiciones del camino.</label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/estructura3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logEstrBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logEstrBarra.'px;'; ?>;margin-left:45px;margin-top: -8px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/camino3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logCamBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logCamBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;border-bottom: 0.5px solid #337ab7;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/pendiente3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logPenBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logPenBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top: 15px;" align="left" class="contenedorImgFondo">
                            <img src="<?php echo base_url().'assets/imgs/superficie3.png' ?>" alt="" style="width:410px;height:30px;" class="imgFondo">
                            <br>
                            <?php if (isset($logSupeBarra)): ?>
                                <img src="<?php echo base_url().'assets/imgs/barra.png' ?>" class="centradoBarra" alt="" style="width:<?php echo $logSupeBarra.'px;'; ?>;margin-left:50px;margin-top: -5px;">
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="" style="width:98%">
        <tr>
            <td rowspan="2" style="width:200px;border: 0.5px solid #337ab7;">
                <p align=justify style="">
                    <?php if (isset($observaciones1)): ?>
                        <?php echo $observaciones1 ?>
                    <?php else: ?>
                        Observaciones:
                    <?php endif; ?>
                </p>
            </td>
            <td align="center" >
                <?php if (isset($firmaAsesor)): ?>
                    <?php if ($firmaAsesor != ""): ?>
                        <img src="<?php echo base_url().$firmaAsesor; ?>" alt="" width="80" height="30">
                    <?php else: ?>
                        <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" width="80" height="30">
                    <?php endif; ?>
                <?php else: ?>
                    <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" width="80" height="30">
                <?php endif; ?>
                <br>
                <label for=""><?php if(isset($nombreAsesor)) echo $nombreAsesor; else echo "Sin información"; ?></label>
            </td>
            <td style="width:30px"></td>
            <td align="center">
              <?php if (isset($firmaCliente)): ?>
                  <?php if ($firmaCliente != ""): ?>
                      <img src="<?php echo base_url().$firmaCliente; ?>" alt="" width="80" height="30">
                  <?php else: ?>
                      <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" width="80" height="30">
                  <?php endif; ?>
              <?php else: ?>
                  <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" width="80" height="30">
              <?php endif; ?>
              <br>
              <label for=""><?php if(isset($nombreContacto)) echo $nombreContacto; else echo "Sin información"; ?></label>
            </td>
        </tr>
        <tr>
            <td align="center" style="border-top: 1px solid #337ab7;width:200px;">
                <label for="">Nombre y firma del asesor.</label><br>
            </td>
            <td style="width:30px;"></td>
            <td align="center" style="border-top: 1px solid #337ab7;width:200px;">
              <label for="">Nombre y firma del cliente.</label><br>
            </td>
        </tr>
    </table>
</page>
