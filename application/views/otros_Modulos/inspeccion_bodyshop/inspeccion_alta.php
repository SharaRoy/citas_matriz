<form name="" class="" method="post" action="<?=base_url()."otros_Modulos/InspeccionBS/validateView"?>" autocomplete="on" enctype="multipart/form-data">
    <?php ob_start("ob_gzhandler"); ?>
    <!-- Alerta para proceso del registro -->
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Fallo el proceso.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Campos incompletos.</strong>
    </div>

    <br><br>
    <div class="row">
        <div class="col-sm-4">
            <br><br>
            <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" style="width:100%;">
            <br><br><br><br>
            <div style="min-width:100%;border:1px solid black;padding-left:13px;">
                <h6 style="font-weight:100;">Subir Video</h6>
                <span class="" style="font-weight:200;color:#337ab7;">*Antes de comenzar, adjuntar/grabar video. </span>
                <input type="file" id="uploadfilesVideo" name="archivo" accept="video/*" style="width:90%;font-size:12px;">
            </div>
        </div>
        <div class="col-sm-8">
            <table style="width:100%">
                <tr style="font-size:13px;">
                    <td colspan="2">
                        <label for="">Nombre del Cliente</label><br>
                        <input type="text" id="nombreCliente" class="input_field nombreCliente" name="nombreCliente" style="width:90%;" value="<?= set_value('nombreCliente');?>">
                        <?php echo form_error('nombreCliente', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">Correo Electrónico</label><br>
                        <input type="email" id="correoCliente" class="input_field" name="correoCliente" style="width:90%;" value="<?= set_value('correoCliente');?>">
                        <?php echo form_error('correoCliente', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">Fecha</label><br>
                        <input type="date" id="fechaRegistro" class="input_field" name="fechaRegistro" style="width:90%;" value="<?= date("Y-m-d")?>" max="<?= date("Y-m-d")?>" min="<?= date("Y-m-d")?>">
                        <?php echo form_error('fechaRegistro', '<span class="error">', '</span>'); ?><br>
                    </td>
                </tr>
                <tr style="font-size:13px;">
                    <td colspan="2">
                        <label for="">Nombre Contacto</label><br>
                        <input type="text" id="contacto" class="input_field" name="contacto" onblur="duplicado()" style="width:90%;" value="<?= set_value('contacto');?>">
                        <?php echo form_error('contacto', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">Celular</label><br>
                        <input type="text" id="celular" class="input_field" name="celular" style="width:90%;" value="<?= set_value('celular');?>">
                        <?php echo form_error('celular', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">Teléfono</label><br>
                        <input type="text" id="telefonoCliente" class="input_field" name="telefonoCliente" style="width:90%;" value="<?= set_value('telefonoCliente');?>">
                        <?php echo form_error('telefonoCliente', '<span class="error">', '</span>'); ?><br>
                    </td>
                </tr>
                <tr style="font-size:13px;">
                    <td>
                        <label for="">Vehículo</label><br>
                        <input type="text" id="vin" class="input_field" name="vin" style="width:90%;" value="<?= set_value('vin');?>">
                        <?php echo form_error('vin', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">Placas</label><br>
                        <input type="text" id="placas" class="input_field" name="placas" style="width:90%;" value="<?= set_value('placas');?>">
                        <?php echo form_error('placas', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">Kilometraje</label><br>
                        <input type="text" id="kilometraje" class="input_field" name="kilometraje" style="width:90%;" value="<?= set_value('kilometraje');?>">
                        <?php echo form_error('kilometraje', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">ID.</label><br>
                        <input type="text" id="idOrden" class="input_field" onblur="revisarOrden()" name="idOrden" style="width:90%;" value="<?php if(set_value('idOrden') != "") echo set_value('idOrden'); else if(isset($idOrden)) echo $idOrden; ?>" disabled>
                        <?php echo form_error('idOrden', '<span class="error">', '</span>'); ?><br>
                        <label for="" class="error" id="idOrdenAlerta"></label>
                    </td>
                </tr>
                <tr style="font-size:13px;">
                    <td colspan="2">
                        <label for="">Serie</label><br>
                        <input type="text" id="serieVehiculo" class="input_field" name="serieVehiculo" style="width:90%;" value="<?= set_value('serieVehiculo');?>">
                        <?php echo form_error('serieVehiculo', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">Color</label><br>
                        <select name="colorVehiculo" id="colorVehiculo" class="input_select" style="width:90%;">
                            <option value="">Selecione..</option>
                            <?php if (isset($idColor)): ?>
                                <?php foreach ($idColor as $index => $valor): ?>
                                    <option value="<?php echo $idColor[$index]; ?>"><?php echo $colorLista[$index]; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <!-- <input type="text" id="colorVehiculo" class="input_field" name="colorVehiculo" style="width:90%;" value="<?= set_value('colorVehiculo');?>"> -->
                        <?php echo form_error('colorVehiculo', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td>
                        <label for="">Torre</label><br>
                        <input type="text" id="torreVehiculo" class="input_field" name="torreVehiculo" style="width:90%;" value="<?= set_value('torreVehiculo');?>">
                        <?php echo form_error('torreVehiculo', '<span class="error">', '</span>'); ?><br>
                    </td>
                </tr>
                <tr style="font-size:13px;">
                    <td colspan="2">
                        <label for="">Tipo de Motor</label><br>
                        <input type="text" id="tipoMotor" class="input_field" name="tipoMotor" style="width:90%;" value="<?= set_value('tipoMotor');?>">
                        <?php echo form_error('tipoMotor', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td style="padding-top: 7px;">
                        <label for="">Tipo de Transmisión</label>
                    </td>
                    <td>
                        Automática &nbsp;<input type="radio" value="Automática" id="tipoTrans_1" name="tipoTrans" <?php echo set_checkbox('tipoTrans', 'Automática'); ?>>&nbsp;&nbsp;
                        Manual&nbsp;<input type="radio" value="Manual" id="tipoTrans_2" name="tipoTrans" <?php echo set_checkbox('tipoTrans', 'Manual'); ?>>&nbsp;&nbsp;
                        <?php echo form_error('tipoTrans', '<span class="error">', '</span>'); ?><br>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 7px;vertical-align:middle;" id="contenedorAudio" align="left">
                        <button type="button" class="btn btn-primary" id="iniciar" >Comenzar Formulario</button>
                        <!-- Campos ocultos para guardar audios -->
                        <input type="hidden" name="blockAudio_1" id="blockAudio_1" value="<?= set_value('blockAudio_1');?>">
                        <input type="hidden" name="blockAudio_2" id="blockAudio_2" value="<?= set_value('blockAudio_2');?>">
                        <input type="hidden" name="blockAudio_3" id="blockAudio_3" value="<?= set_value('blockAudio_3');?>">
                        <input type="hidden" name="blockAudio_4" id="blockAudio_4" value="<?= set_value('blockAudio_4');?>">
                        <input type="hidden" name="blockAudio_5" id="blockAudio_5" value="<?= set_value('blockAudio_5');?>">
                        <input type="hidden" name="blockAudio_6" id="blockAudio_6" value="<?= set_value('blockAudio_6');?>">
                        <input type="hidden" name="blockAudio_7" id="blockAudio_7" value="<?= set_value('blockAudio_7');?>">
                        <input type="hidden" name="blockAudio_8" id="blockAudio_8" value="<?= set_value('blockAudio_8');?>">
                        <input type="hidden" name="blockAudio_9" id="blockAudio_9" value="<?= set_value('blockAudio_9');?>">
                    </td>
                    <td colspan="3">

                    </td
                </tr>
            </table>
        </div>
    </div>

    <br><br>
    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading" align="center">
                    <h4>INSPECCIÓN VISUAL E INVENTARIO EN RECEPCIÓN</h4>
                </div>
                <div class="panel-body">
                    <div class="col-sm-6 table-responsive">
                        <table class="" style="width:90%;">
                            <tbody>
                                <tr style="border:1px solid black;">
                                    <td align="center">
                                        <h4 style="font-weight:bold;">Interiores </h4>
                                        <?php echo form_error('interiores', '<span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center">
                                        Si
                                    </td>
                                    <td align="center">
                                        No
                                    </td>
                                    <td align="center">
                                        (No cuenta)<br>
                                        NC
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Póliza Garantía / Manual de Prop.</td>
                                    <td align="center">
                                        <input type="checkbox" class="llaveroCheck" name="interiores[]" value="PólizaGarantia.Si" <?php echo set_checkbox('interiores[]', 'PólizaGarantia.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="llaveroCheck" name="interiores[]" value="PólizaGarantia.No" <?php echo set_checkbox('interiores[]', 'PólizaGarantia.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="llaveroCheck" name="interiores[]" value="PólizaGarantia.NC" <?php echo set_checkbox('interiores[]', 'PólizaGarantia.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Seguro rines.</td>
                                    <td align="center">
                                        <input type="checkbox" class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.Si" <?php echo set_checkbox('interiores[]', 'SeguroRines.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.No" <?php echo set_checkbox('interiores[]', 'SeguroRines.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="SeguroRinesCheck" name="interiores[]" value="SeguroRines.NC" <?php echo set_checkbox('interiores[]', 'SeguroRines.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;border-bottom:1px solid white;">
                                    <td colspan="4">Indicadores de falla Activados:</td>
                                </tr>
                                <tr style="border:1px solid black;border-top:1px solid white;">
                                    <td colspan="4">
                                        <table style="width:100%;">
                                            <tr>
                                                <td align="center">
                                                    <img src="<?php echo base_url();?>assets/imgs/05.png" style="width:1.2cm;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url();?>assets/imgs/02.png" style="width:1.2cm;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url();?>assets/imgs/07.png" style="width:1.2cm;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url();?>assets/imgs/01.png" style="width:1.2cm;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url();?>assets/imgs/06.png" style="width:1.2cm;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url();?>assets/imgs/04.png" style="width:1.2cm;">
                                                </td>
                                                <td align="center">
                                                    <img src="<?php echo base_url();?>assets/imgs/03.png" style="width:1.2cm;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <input type="checkbox" class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.Si" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.Si'); ?>>
                                                    <input type="checkbox" class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.No" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.No'); ?>>
                                                    <input type="checkbox" class="IndicadorGasolinaCheck" name="interiores[]" value="IndicadorGasolina.NC" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina.NC'); ?>>
                                                    <!-- <input type="checkbox" name="interiores[]" value="IndicadorGasolina" <?php echo set_checkbox('interiores[]', 'IndicadorGasolina'); ?>> -->
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.Si" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.Si'); ?>>
                                                    <input type="checkbox" class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.No" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.No'); ?>>
                                                    <input type="checkbox" class="IndicadorMantenimentoCheck" name="interiores[]" value="IndicadorMantenimento.NC" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento.NC'); ?>>
                                                    <!-- <input type="checkbox" name="interiores[]" value="IndicadorMantenimento" <?php echo set_checkbox('interiores[]', 'IndicadorMantenimento'); ?>> -->
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" class="SistemaABSCheck" name="interiores[]" value="SistemaABS.Si" <?php echo set_checkbox('interiores[]', 'SistemaABS.Si'); ?>>
                                                    <input type="checkbox" class="SistemaABSCheck" name="interiores[]" value="SistemaABS.No" <?php echo set_checkbox('interiores[]', 'SistemaABS.No'); ?>>
                                                    <input type="checkbox" class="SistemaABSCheck" name="interiores[]" value="SistemaABS.NC" <?php echo set_checkbox('interiores[]', 'SistemaABS.NC'); ?>>
                                                    <!-- <input type="checkbox" name="interiores[]" value="SistemaABS" <?php echo set_checkbox('interiores[]', 'SistemaABS'); ?>> -->
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.Si" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.Si'); ?>>
                                                    <input type="checkbox" class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.No" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.No'); ?>>
                                                    <input type="checkbox" class="IndicadorFrenosCheck" name="interiores[]" value="IndicadorFrenos.NC" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos.NC'); ?>>
                                                    <!-- <input type="checkbox" name="interiores[]" value="IndicadorFrenos" <?php echo set_checkbox('interiores[]', 'IndicadorFrenos'); ?>> -->
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.Si" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.Si'); ?>>
                                                    <input type="checkbox" class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.No" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.No'); ?>>
                                                    <input type="checkbox" class="IndicadorBolsaAireCheck" name="interiores[]" value="IndicadorBolsaAire.NC" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire.NC'); ?>>
                                                    <!-- <input type="checkbox" name="interiores[]" value="IndicadorBolsaAire" <?php echo set_checkbox('interiores[]', 'IndicadorBolsaAire'); ?>> -->
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.Si" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.Si'); ?>>
                                                    <input type="checkbox" class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.No" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.No'); ?>>
                                                    <input type="checkbox" class="IndicadorTPMSCheck" name="interiores[]" value="IndicadorTPMS.NC" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS.NC'); ?>>
                                                    <!-- <input type="checkbox" name="interiores[]" value="IndicadorTPMS" <?php echo set_checkbox('interiores[]', 'IndicadorTPMS'); ?>> -->
                                                </td>
                                                <td align="center">
                                                    <input type="checkbox" class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.Si" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.Si'); ?>>
                                                    <input type="checkbox" class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.No" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.No'); ?>>
                                                    <input type="checkbox" class="IndicadorBateriaCheck" name="interiores[]" value="IndicadorBateria.NC" <?php echo set_checkbox('interiores[]', 'IndicadorBateria.NC'); ?>>
                                                    <!-- <input type="checkbox" name="interiores[]" value="IndicadorBateria" <?php echo set_checkbox('interiores[]', 'IndicadorBateria'); ?>> -->
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Rociadores y Limpiaparabrisas.</td>
                                    <td align="center">
                                        <input type="checkbox" class="RociadoresCheck" name="interiores[]" value="Rociadores.Si" <?php echo set_checkbox('interiores[]', 'Rociadores.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="RociadoresCheck" name="interiores[]" value="Rociadores.No" <?php echo set_checkbox('interiores[]', 'Rociadores.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="RociadoresCheck" name="interiores[]" value="Rociadores.NC" <?php echo set_checkbox('interiores[]', 'Rociadores.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Claxon.</td>
                                    <td align="center">
                                        <input type="checkbox" class="ClaxonCheck" name="interiores[]" value="Claxon.Si" <?php echo set_checkbox('interiores[]', 'Claxon.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="ClaxonCheck" name="interiores[]" value="Claxon.No" <?php echo set_checkbox('interiores[]', 'Claxon.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="ClaxonCheck" name="interiores[]" value="Claxon.NC" <?php echo set_checkbox('interiores[]', 'Claxon.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;border-bottom:1px solid white;">
                                    <td colspan="4">
                                        Luces
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;border-top: 1px solid white;">
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        Delanteras.
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.Si" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.No" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesDelanterasCheck" name="interiores[]" value="LucesDelanteras.NC" <?php echo set_checkbox('interiores[]', 'LucesDelanteras.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        Traseras.
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.Si" <?php echo set_checkbox('interiores[]', 'LucesTraseras.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.No" <?php echo set_checkbox('interiores[]', 'LucesTraseras.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesTraserasCheck" name="interiores[]" value="LucesTraseras.NC" <?php echo set_checkbox('interiores[]', 'LucesTraseras.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        Stop.
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesStopCheck" name="interiores[]" value="LucesStop.Si" <?php echo set_checkbox('interiores[]', 'LucesStop.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesStopCheck" name="interiores[]" value="LucesStop.No" <?php echo set_checkbox('interiores[]', 'LucesStop.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LucesStopCheck" name="interiores[]" value="LucesStop.NC" <?php echo set_checkbox('interiores[]', 'LucesStop.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Radio / Carátulas.</td>
                                    <td align="center">
                                        <input type="checkbox" class="CaratulasCheck" name="interiores[]" value="Caratulas.Si" <?php echo set_checkbox('interiores[]', 'Caratulas.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="CaratulasCheck" name="interiores[]" value="Caratulas.No" <?php echo set_checkbox('interiores[]', 'Caratulas.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="CaratulasCheck" name="interiores[]" value="Caratulas.NC" <?php echo set_checkbox('interiores[]', 'Caratulas.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Pantallas, FIS.</td>
                                    <td align="center">
                                        <input type="checkbox" class="PantallasCheck" name="interiores[]" value="Pantallas.Si" <?php echo set_checkbox('interiores[]', 'Pantallas.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="PantallasCheck" name="interiores[]" value="Pantallas.No" <?php echo set_checkbox('interiores[]', 'Pantallas.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="PantallasCheck" name="interiores[]" value="Pantallas.NC" <?php echo set_checkbox('interiores[]', 'Pantallas.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>A/C</td>
                                    <td align="center">
                                        <input type="checkbox" class="AACheck" name="interiores[]" value="AA.Si" <?php echo set_checkbox('interiores[]', 'AA.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="AACheck" name="interiores[]" value="AA.No" <?php echo set_checkbox('interiores[]', 'AA.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="AACheck" name="interiores[]" value="AA.NC" <?php echo set_checkbox('interiores[]', 'AA.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Encendedor.</td>
                                    <td align="center">
                                        <input type="checkbox" class="EncendedorCheck" name="interiores[]" value="Encendedor.Si" <?php echo set_checkbox('interiores[]', 'Encendedor.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="EncendedorCheck" name="interiores[]" value="Encendedor.No" <?php echo set_checkbox('interiores[]', 'Encendedor.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="EncendedorCheck" name="interiores[]" value="Encendedor.NC" <?php echo set_checkbox('interiores[]', 'Encendedor.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Vidrios.</td>
                                    <td align="center">
                                        <input type="checkbox" class="VidriosCheck" name="interiores[]" value="Vidrios.Si" <?php echo set_checkbox('interiores[]', 'Vidrios.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="VidriosCheck" name="interiores[]" value="Vidrios.No" <?php echo set_checkbox('interiores[]', 'Vidrios.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="VidriosCheck" name="interiores[]" value="Vidrios.NC" <?php echo set_checkbox('interiores[]', 'Vidrios.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Espejos.</td>
                                    <td align="center">
                                        <input type="checkbox" class="EspejosCheck" name="interiores[]" value="Espejos.Si" <?php echo set_checkbox('interiores[]', 'Espejos.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="EspejosCheck" name="interiores[]" value="Espejos.No" <?php echo set_checkbox('interiores[]', 'Espejos.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="EspejosCheck" name="interiores[]" value="Espejos.NC" <?php echo set_checkbox('interiores[]', 'Espejos.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Seguros Eléctricos.</td>
                                    <td align="center">
                                        <input type="checkbox" class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.Si" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.No" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="SegurosEléctricosCheck" name="interiores[]" value="SegurosEléctricos.NC" <?php echo set_checkbox('interiores[]', 'SegurosEléctricos.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>CD, Artículos Personales, Guantera.</td>
                                    <td align="center">
                                        <input type="checkbox" class="CDCheck" name="interiores[]" value="CD.Si" <?php echo set_checkbox('interiores[]', 'CD.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="CDCheck" name="interiores[]" value="CD.No" <?php echo set_checkbox('interiores[]', 'CD.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="CDCheck" name="interiores[]" value="CD.NC" <?php echo set_checkbox('interiores[]', 'CD.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Asientos y vestiduras.</td>
                                    <td align="center">
                                        <input type="checkbox" class="VestidurasCheck" name="interiores[]" value="Vestiduras.Si" <?php echo set_checkbox('interiores[]', 'Vestiduras.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="VestidurasCheck" name="interiores[]" value="Vestiduras.No" <?php echo set_checkbox('interiores[]', 'Vestiduras.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="VestidurasCheck" name="interiores[]" value="Vestiduras.NC" <?php echo set_checkbox('interiores[]', 'Vestiduras.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Tapetes.</td>
                                    <td align="center">
                                        <input type="checkbox" class="TapetesCheck" name="interiores[]" value="Tapetes.Si" <?php echo set_checkbox('interiores[]', 'Tapetes.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="TapetesCheck" name="interiores[]" value="Tapetes.No" <?php echo set_checkbox('interiores[]', 'Tapetes.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="TapetesCheck" name="interiores[]" value="Tapetes.NC" <?php echo set_checkbox('interiores[]', 'Tapetes.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>OASIS Actualización SYNC.</td>
                                    <td align="center">
                                        <input type="checkbox" class="OasisCheck" name="interiores[]" value="Oasis.Si" <?php echo set_checkbox('interiores[]', 'Oasis.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="OasisCheck" name="interiores[]" value="Oasis.No" <?php echo set_checkbox('interiores[]', 'Oasis.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="OasisCheck" name="interiores[]" value="Oasis.NC" <?php echo set_checkbox('interiores[]', 'Oasis.NC'); ?>>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4"><br><br></td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table class="" style="border: 1px solid black;width:100%;">
                                            <tr>
                                                <td align="center" style="border: 1px solid black; width:30%;">
                                                    <img src="<?php echo base_url().'assets/imgs/gas.jpeg'; ?>" style="width:50px;" alt="">
                                                </td>
                                                <td style="margin-left:5px;" colspan="2">
                                                    Nivel de Gasolina:&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input type="number" name="nivelGasolina_1" class="input_field" style="width:1.5cm;" min="0" value="<?= set_value('nivelGasolina_1');?>">
                                                    <img src="<?php echo base_url().'assets/imgs/reducidos/diagonal-min.jpg'; ?>" style="width:20px; height:20px;" alt="">
                                                    <input type="number" name="nivelGasolina_2" class="input_field" style="width:1.5cm;" min="0" value="<?= set_value('nivelGasolina_2');?>">
                                                    <br>
                                                    <?php echo form_error('nivelGasolina_1', '<span class="error">', '</span>'); ?>
                                                    <?php echo form_error('nivelGasolina_2', '<span class="error">', '</span>'); ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                    </div>

                    <div class="col-sm-6 table-responsive" >
                        <div class="row" style="border: 1px solid black;">
                            <div class="col-sm-12">
                                <h3 style="font-weight: bold;">Condiciones de carrocería</h3>
                                <table width="100%" class="table-bordered">
                                    <tr>
                                        <td>
                                            <label style="font-weight: bold;font-size:15px;">Daños</label>&nbsp;&nbsp;&nbsp;
                                            Si&nbsp;<input type="radio" value="1" name="danos" <?php echo set_checkbox('danos', '1'); ?>>
                                            No&nbsp;<input type="radio" value="0" name="danos" <?php echo set_checkbox('danos', '0'); ?>>
                                        </td>
                                        <td>
                                            <input type="radio" ng-model="hits" id="golpes" name="marcasRadio" checked value="hit">&nbsp;Golpes
                                            &nbsp;&nbsp;<input type="radio" id="roto" name="marcasRadio" value="broken">&nbsp;Roto / Estrellado
                                            &nbsp;&nbsp;<input type="radio" id="rayones" name="marcasRadio" value="scratch">&nbsp;Rayones
                                        </td>
                                    </tr>
                                </table>
                                <a href="#" class="" id="btn-download" download="diagramaGolpes.png">Descargar diagrama</a>
                                <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car_2.jpg'; ?>">
                                <input type="hidden" name="danosMarcas" id="danosMarcas" value="<?= set_value('danosMarcas');?>">
                                <input type="hidden" name="validaClick_1" id="validaClick_1" value="0">

                                <canvas id="canvas_3" width="420" height="200" style="display:none;">
                                    Su navegador no soporta canvas.
                                </canvas>
                                <img id="diagramaTempDanio" src="<?php echo base_url().'assets/imgs/car_2.jpg'; ?>" alt="" style="width:420px;height: 200px;">
                            </div>
                        </div>

                        <br><br>
                        <table style="border: 1px solid black;width:105%;margin-left: -15px;">
                            <tr>
                                <td colspan="2" rowspan="2" style="width:50%;">
                                    <h4 style="font-weight: bold;">Sistema de frenos</h4>
                                </td>
                                <td colspan="2" align="right" style="width:50%;font-size:13px;">
                                    solo revisar 2 m.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left" style="font-size:13px;">
                                    Medida Actual
                                    <br>
                                </td>
                            </tr>
                            <tr style="font-size:13px;">
                                <td align="center" style="width:25%;border: 1px solid black;font-size:13px;border-top: 1px solid white;">
                                    Ruedas
                                </td>
                                <td align="center" style="width:25%;border: 1px solid black;font-size:12px;border-top: 1px solid white;">
                                    Balata / Zapata
                                </td>
                                <td align="center" style="width:25%;border: 1px solid black;font-size:12px;border-top: 1px solid white;">
                                    Disco / Tambor
                                </td>
                                <td align="center" style="width:25%;border: 1px solid black;font-size:12px;border-top: 1px solid white;">
                                    Neumático
                                </td>
                            </tr>
                            <tr style="">
                                <td align="left" style="width:25%;border: 1px solid black;font-size:13px;">
                                    Delantera Derecha
                                    <?php echo form_error('ruedaDD', '<br><span class="error">', '</span>'); ?>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT1" value="RuedaDD.Balata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Balata'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT1" value="RuedaDD.Zapata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Zapata'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT2" value="RuedaDD.Disco" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Disco'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT2" value="RuedaDD.Tambor" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaDD.Tambor'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" value="RuedaDD.Neumatico" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'Ruedas.Neumatico'); ?>>
                                </td>
                            </tr>
                            <tr style="">
                                <td align="left" style="width:25%;border: 1px solid black;font-size:13px;">
                                    Delantera Izquierda
                                    <?php echo form_error('ruedaDI', '<br><span class="error">', '</span>'); ?>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT3" value="RuedasDI.Balata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Balata'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT3" value="RuedasDI.Zapata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Zapata'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT4" value="RuedasDI.Disco" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Disco'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT4" value="RuedasDI.Tambor" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Tambor'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" value="RuedasDI.Neumatico" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasDI.Neumatico'); ?>>
                                </td>
                            </tr>
                            <tr style="">
                                <td align="left" style="width:25%;border: 1px solid black;font-size:13px;">
                                    Trasera Derecha
                                    <?php echo form_error('ruedaTD', '<br><span class="error">', '</span>'); ?>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT5" value="RuedaTD.Balata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaTD.Balata'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT5" value="RuedaTD.Zapata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaTD.Zapata'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT6" value="RuedaTD.Disco" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaTD.Disco'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT6" value="RuedaTD.Tambor" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaTD.Tambor'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" value="RuedaTD.Neumatico" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedaTD.Neumatico'); ?>>
                                </td>
                            </tr>
                            <tr style="">
                                <td align="left" style="width:25%;border: 1px solid black;font-size:13px;">
                                    Trasera Izquierda
                                    <?php echo form_error('ruedaTI', '<br><span class="error">', '</span>'); ?>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT7" value="RuedasTI.Balata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Balata'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT7" value="RuedasTI.Zapata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Zapata'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT8" value="RuedasTI.Disco" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Disco'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT8" value="RuedasTI.Tambor" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Tambor'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" value="RuedasTI.Neumatico" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'RuedasTI.Neumatico'); ?>>
                                </td>
                            </tr>
                            <tr style="">
                                <td align="left" style="width:40%;border: 1px solid black;font-size:13px;">
                                    Refacción
                                    <?php echo form_error('refaccion', '<br><span class="error">', '</span>'); ?>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT9" value="Refaccion.Balata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'Refaccion.Balata'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT9" value="Refaccion.Zapata" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'Refaccion.Zapata'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" class="ruedaT10" value="Refaccion.Disco" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'Refaccion.Disco'); ?>>
                                    &nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;
                                    <input type="checkbox" class="ruedaT10" value="Refaccion.Tambor" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'Refaccion.Tambor'); ?>>
                                </td>
                                <td align="center" style="width:20%;border: 1px solid black;font-size:13px;">
                                    <input type="checkbox" value="Refaccion.Neumatico" name="sisFrenos[]" <?php echo set_checkbox('sisFrenos[]', 'Refaccion.Neumatico'); ?>>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <textarea class="form-control" name="observaciones" rows="2" placeholder="Observaciones..." style="width:100%"><?= set_value('observaciones');?></textarea>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading" align="center"><br></div>
                <div class="panel-body">
                    <div class="col-sm-4 table-responsive">
                        <table class="" style="min-width:100%;border:1px solid black;">
                            <thead>
                                <tr class="headers_table" align="center" style="border:1px solid black;">
                                    <td>
                                        <h4 style="font-weight:bold;">Cofre </h4>
                                        <?php echo form_error('cofre', '<span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center">
                                        Nivel <br>Correcto
                                    </td>
                                    <td align="center">
                                        Nivel <br>Incorrecto
                                    </td>
                                    <td align="center">
                                        Fuga (F)
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="border:1px solid black;">
                                    <td>Aceite de Motor.</td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_1" name="cofre[]" value="AceiteMotor.Si" <?php echo set_checkbox('cofre[]', 'AceiteMotor.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_1" name="cofre[]" value="AceiteMotor.No" <?php echo set_checkbox('cofre[]', 'AceiteMotor.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_1" name="cofre[]" value="AceiteMotor.F" <?php echo set_checkbox('cofre[]', 'AceiteMotor.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Liquido de Frenos.</td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_2" name="cofre[]" value="LiquidoFrenos.Si" <?php echo set_checkbox('cofre[]', 'LiquidoFrenos.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_2" name="cofre[]" value="LiquidoFrenos.No" <?php echo set_checkbox('cofre[]', 'LiquidoFrenos.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_2" name="cofre[]" value="LiquidoFrenos.F" <?php echo set_checkbox('cofre[]', 'LiquidoFrenos.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Limpiaparabrisas.</td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_3" name="cofre[]" value="Limpiaparabrisas.Si" <?php echo set_checkbox('cofre[]', 'Limpiaparabrisas.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_3" name="cofre[]" value="Limpiaparabrisas.No" <?php echo set_checkbox('cofre[]', 'Limpiaparabrisas.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_3" name="cofre[]" value="Limpiaparabrisas.F" <?php echo set_checkbox('cofre[]', 'Limpiaparabrisas.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Anticongelante.</td>
                                    <td align="center">
                                        <input type="checkbox" class="cofres_4" name="cofre[]" value="Anticongelante.Si" <?php echo set_checkbox('cofre[]', 'Anticongelante.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofres_4" name="cofre[]" value="Anticongelante.No" <?php echo set_checkbox('cofre[]', 'Anticongelante.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofres_4" name="cofre[]" value="Anticongelante.F" <?php echo set_checkbox('cofre[]', 'Anticongelante.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Liquido de Dirección.</td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_5" name="cofre[]" value="LiquidoDirección.Si" <?php echo set_checkbox('cofre[]', 'LiquidoDirección.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_5" name="cofre[]" value="LiquidoDirección.No" <?php echo set_checkbox('cofre[]', 'LiquidoDirección.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="cofre_5" name="cofre[]" value="LiquidoDirección.F" <?php echo set_checkbox('cofre[]', 'LiquidoDirección.F'); ?>>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <br>
                        <table class="" style="min-width:100%;border:1px solid black;">
                            <thead>
                                <tr class="headers_table" align="center" style="border:1px solid black;">
                                    <td>
                                        <h4 style="font-weight:bold;">Cajuela </h4>
                                        <?php echo form_error('cajuela', '<span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center">
                                        Si
                                    </td>
                                    <td align="center">
                                        No
                                    </td>
                                    <td align="center">
                                        No cuenta<br>
                                        (NC)
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="border:1px solid black;">
                                    <td>Herramienta.</td>
                                    <td align="center">
                                        <input type="checkbox" class="HerramientaCheck" name="cajuela[]" value="Herramienta.Si" <?php echo set_checkbox('cajuela[]', 'Herramienta.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="HerramientaCheck" name="cajuela[]" value="Herramienta.No" <?php echo set_checkbox('cajuela[]', 'Herramienta.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="HerramientaCheck" name="cajuela[]" value="Herramienta.NC" <?php echo set_checkbox('cajuela[]', 'Herramienta.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Gato / Llave.</td>
                                    <td align="center">
                                        <input type="checkbox" class="eLlaveCheck" name="cajuela[]" value="eLlave.Si" <?php echo set_checkbox('cajuela[]', 'eLlave.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="eLlaveCheck" name="cajuela[]" value="eLlave.No" <?php echo set_checkbox('cajuela[]', 'eLlave.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="eLlaveCheck" name="cajuela[]" value="eLlave.NC" <?php echo set_checkbox('cajuela[]', 'eLlave.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Reflejantes.</td>
                                    <td align="center">
                                        <input type="checkbox" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.Si" <?php echo set_checkbox('cajuela[]', 'Reflejantes.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.No" <?php echo set_checkbox('cajuela[]', 'Reflejantes.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="ReflejantesCheck" name="cajuela[]" value="Reflejantes.NC" <?php echo set_checkbox('cajuela[]', 'Reflejantes.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Cables.</td>
                                    <td align="center">
                                        <input type="checkbox" class="CablesCheck" name="cajuela[]" value="Cables.Si" <?php echo set_checkbox('cajuela[]', 'Cables.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="CablesCheck" name="cajuela[]" value="Cables.No" <?php echo set_checkbox('cajuela[]', 'Cables.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="CablesCheck" name="cajuela[]" value="Cables.NC" <?php echo set_checkbox('cajuela[]', 'Cables.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Extintor.</td>
                                    <td align="center">
                                        <input type="checkbox" class="ExtintorCheck" name="cajuela[]" value="Extintor.Si" <?php echo set_checkbox('cajuela[]', 'Extintor.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="ExtintorCheck" name="cajuela[]" value="Extintor.No" <?php echo set_checkbox('cajuela[]', 'Extintor.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="ExtintorCheck" name="cajuela[]" value="Extintor.NC" <?php echo set_checkbox('cajuela[]', 'Extintor.NC'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Llanta Refacción.</td>
                                    <td align="center">
                                        <input type="checkbox" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.Si" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.No" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="LlantaRefaccionCheck" name="cajuela[]" value="LlantaRefaccion.NC" <?php echo set_checkbox('cajuela[]', 'LlantaRefaccion.NC'); ?>>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-4 table-responsive">
                        <table class="" style="min-width:100%;border:1px solid black;">
                            <thead>
                                <tr class="headers_table" align="center" style="border:1px solid black;">
                                    <td>
                                        <h4 style="font-weight:bold;">Inferior </h4>
                                        <?php echo form_error('inferior', '<span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center">
                                        Bien
                                    </td>
                                    <td align="center">
                                        Mal
                                    </td>
                                    <td align="center">
                                        Fuga (F)
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="border:1px solid black;">
                                    <td>Sistema de Escape.</td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_1" name="inferior[]" value="SisEscape.Si" <?php echo set_checkbox('inferior[]', 'SisEscape.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_1" name="inferior[]" value="SisEscape.No" <?php echo set_checkbox('inferior[]', 'SisEscape.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_1" name="inferior[]" value="SisEscape.F" <?php echo set_checkbox('inferior[]', 'SisEscape.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Amortiguadores.</td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_2" name="inferior[]" value="Amortiguador.Si" <?php echo set_checkbox('inferior[]', 'Amortiguador.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_2" name="inferior[]" value="Amortiguador.No" <?php echo set_checkbox('inferior[]', 'Amortiguador.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_2" name="inferior[]" value="Amortiguador.F" <?php echo set_checkbox('inferior[]', 'Amortiguador.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Tuberias.</td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_3" name="inferior[]" value="Tuberias.Si" <?php echo set_checkbox('inferior[]', 'Tuberias.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_3" name="inferior[]" value="Tuberias.No" <?php echo set_checkbox('inferior[]', 'Tuberias.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_3" name="inferior[]" value="Tuberias.F" <?php echo set_checkbox('inferior[]', 'Tuberias.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Transeje / Transmisión.</td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_4" name="inferior[]" value="Transmisión.Si" <?php echo set_checkbox('inferior[]', 'Transmisión.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_4" name="inferior[]" value="Transmisión.No" <?php echo set_checkbox('inferior[]', 'Transmisión.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_4" name="inferior[]" value="Transmisión.F" <?php echo set_checkbox('inferior[]', 'Transmisión.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Sistema de Dirección.</td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_5" name="inferior[]" value="SisDirección.Si" <?php echo set_checkbox('inferior[]', 'SisDirección.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_5" name="inferior[]" value="SisDirección.No" <?php echo set_checkbox('inferior[]', 'SisDirección.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_5" name="inferior[]" value="SisDirección.F" <?php echo set_checkbox('inferior[]', 'SisDirección.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Chasis sucio.</td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_6" name="inferior[]" value="Chasis.Si" <?php echo set_checkbox('inferior[]', 'Chasis.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_6" name="inferior[]" value="Chasis.No" <?php echo set_checkbox('inferior[]', 'Chasis.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_6" name="inferior[]" value="Chasis.F" <?php echo set_checkbox('inferior[]', 'Chasis.F'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Golpes Especifico.</td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_7" name="inferior[]" value="Golpes.Si" <?php echo set_checkbox('inferior[]', 'Golpes.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_7" name="inferior[]" value="Golpes.No" <?php echo set_checkbox('inferior[]', 'Golpes.No'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="inferior_7" name="inferior[]" value="Golpes.F" <?php echo set_checkbox('inferior[]', 'Golpes.F'); ?>>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <br>
                        <table class="" style="min-width:100%;border:1px solid black;">
                            <thead>
                                <tr class="headers_table" align="center" style="border:1px solid black;">
                                    <td>
                                        <h4 style="font-weight:bold;">Exteriores </h4>
                                        <?php echo form_error('exteriores', '<span class="error">', '</span>'); ?>
                                    </td>
                                    <td align="center" style="width:15%;">
                                        Si
                                    </td>
                                    <td align="center" style="width:15%;">
                                        No
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="border:1px solid black;">
                                    <td>Tapones rueda.</td>
                                    <td align="center">
                                        <input type="checkbox" class="TaponesRuedaCheck" name="exteriores[]" value="TaponesRueda.Si" <?php echo set_checkbox('exteriores[]', 'TaponesRueda.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="TaponesRuedaCheck" name="exteriores[]" value="TaponesRueda.No" <?php echo set_checkbox('exteriores[]', 'TaponesRueda.No'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Gomas de limpiadores.</td>
                                    <td align="center">
                                        <input type="checkbox" class="GotasCheck" name="exteriores[]" value="Gotas.Si" <?php echo set_checkbox('exteriores[]', 'Gotas.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="GotasCheck" name="exteriores[]" value="Gotas.No" <?php echo set_checkbox('exteriores[]', 'Gotas.No'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Antena.</td>
                                    <td align="center">
                                        <input type="checkbox" class="AntenaCheck" name="exteriores[]" value="Antena.Si" <?php echo set_checkbox('exteriores[]', 'Antena.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="AntenaCheck" name="exteriores[]" value="Antena.No" <?php echo set_checkbox('exteriores[]', 'Antena.No'); ?>>
                                    </td>
                                </tr>
                                <tr style="border:1px solid black;">
                                    <td>Tapón de gasolina.</td>
                                    <td align="center">
                                        <input type="checkbox" class="TaponGasolinaCheck" name="exteriores[]" value="TaponGasolina.Si" <?php echo set_checkbox('exteriores[]', 'TaponGasolina.Si'); ?>>
                                    </td>
                                    <td align="center">
                                        <input type="checkbox" class="TaponGasolinaCheck" name="exteriores[]" value="TaponGasolina.No" <?php echo set_checkbox('exteriores[]', 'TaponGasolina.No'); ?>>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-sm-4 table-responsive">
                        <table class="" style="min-width:100%;border:1px solid black;">
                            <tr align="center" style="border:1px solid black;">
                                <td style="padding-top:10px;padding-bottom:10px;">¿Deja artículos personales?</td>
                                <td>
                                    (Si&nbsp;<input type="radio" value="1" name="articulos" <?php echo set_checkbox('articulos', '1'); ?>>
                                    No&nbsp;<input type="radio" value="0" name="articulos" <?php echo set_checkbox('articulos', '0'); ?>>)<br>
                                    <?php echo form_error('articulos', '<span class="error">', '</span>'); ?>
                                </td>
                            </tr>
                            <tr align="left" style="border:1px solid black;">
                                <td style="padding-top:10px;padding-bottom:10px;" colspan="2">¿Cuales?</td>
                            </tr>
                            <tr align="center" style="border:1px solid black;">
                                <td colspan="2" align="center">
                                    <textarea class="form-control" name="cualesArticulos" rows="2" placeholder="Articulos..." style="width:95%"><?= set_value('cualesArticulos');?></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Parte voz cliente -->
            <div class="panel panel-default">
                <div class="panel-heading" align="center">
                    <h5 style="font-weight:bold;">
                        Identificación de necesidades de servicio / Diagnóstico Final
                    </h5>
                </div>
                <div class="panel-body">
                    <div class="col-sm-5 table-responsive" style="border: 1px solid black;">
                        <h6 style="font-weight:bold;">Definición de falla.</h6>
                        <textarea class="form-control" name="falla" rows="3" style="text-align:left;width:100%;"><?php echo set_value('falla');?></textarea>
                        <?php echo form_error('falla', '<span class="error">', '</span>'); ?>

                        <br>
                        <table style="width:100%;">
                            <tr>
                                <td align="center">
                                    <img src="<?php echo base_url().'/assets/imgs/reducidos/vista-min.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                </td>
                                <td align="center">
                                    <img src="<?php echo base_url().'/assets/imgs/reducidos/tacto-min.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                </td>
                                <td align="center">
                                    <img src="<?php echo base_url().'/assets/imgs/reducidos/oido-min.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                </td>
                                <td align="center">
                                    <img src="<?php echo base_url().'/assets/imgs/reducidos/olfato-min.png' ?>" alt="" style="width:40px;height: 40px;margin-top:3px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <input type="checkbox" value="Vista" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Vista'); ?>>
                                </td>
                                <td align="center">
                                    <input type="checkbox" value="Tacto" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Tacto'); ?>>
                                </td>
                                <td align="center">
                                    <input type="checkbox" value="Oido" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Oido'); ?>>
                                </td>
                                <td align="center">
                                    <input type="checkbox" value="Olfalto" name="sentidos[]" <?php echo set_checkbox('sentidos[]', 'Olfalto'); ?>>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width:50%;" align="center">
                                    <br>
                                    <!-- CANVAS -->
                                    <canvas id="diagramaFallas" width="200" height="290" style="display:none;"></canvas>
                                    <img id="diagramaTemp" src="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>" alt="" style="width:200px;height: 290px;">
                                    <!-- CANVAS -->
                                    <input type="hidden" name="fallasImg" id="fallasImg" value="<?= set_value('fallasImg');?>">
                                    <input type="hidden" name="" id="direccionFondo_VC" value="<?php echo base_url().'assets/imgs/cuadricula.png'; ?>">
                                    <input type="hidden" name="validaClick_2" id="validaClick_2" value="0">
                                </td>
                                <td colspan="2" align="center">
                                    <table style="border: 1px solid black; width:200px;">
                                        <tr style="border: 1px solid black;">
                                            <td colspan="2" style="">
                                                La falla se presenta cuando:
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Arranca el vehículo.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Arranque" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Arranque'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Inicia movimiento.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Inicio Movimiento" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Inicio Movimiento'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Disminuye la velocidad.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Menos Velocidad" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Menos Velocidad'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Da vuelta a la izquierda.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Vuelta Izquierda" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Vuelta Izquierda'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Da vuelta a la derecha.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Vuelta Derecha" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Vuelta Derecha'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Pasa un tope.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Tope" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Tope'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Pasa un bache.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Bache" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Bache'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Cambia la velocidad.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Cambio Velocidad" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Cambio Velocidad'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Está sin movimiento.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Movimiento" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Movimiento'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Constantemente.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Constantemente" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Constantemente'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width:60%;" align="left">
                                                Esporádicamente.
                                            </td>
                                            <td style="width:30%;border-left:1px solid black;" align="center">
                                                <input type="checkbox" value="Esporádicamente" name="fallasCheck[]" <?php echo set_checkbox('fallasCheck[]', 'Esporádicamente'); ?>>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"><br><br></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <table style="width:100%;">
                                        <tr style="border: 1px solid black;">
                                            <td colspan="4" style="">La falla se percibe en:</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td align="left" style="border-left:1px solid black;">
                                                Volante.
                                            </td>
                                            <td align="center" style="border-left:1px solid black;">
                                                <input type="checkbox" value="Volante" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Volante'); ?>>
                                            </td>
                                            <td align="left" style="border-left:1px solid black;">
                                                Cofre.
                                            </td>
                                            <td align="center" style="border-left:1px solid black;">
                                                <input type="checkbox" value="Cofre" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cofre'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td align="left" style="border-left:1px solid black;">
                                                Asiento.
                                            </td>
                                            <td align="center" style="border-left:1px solid black;">
                                                <input type="checkbox" value="Asiento" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Asiento'); ?>>
                                            </td>
                                            <td align="left" style="border-left:1px solid black;">
                                                Cajuela.
                                            </td>
                                            <td align="center" style="border-left:1px solid black;">
                                                <input type="checkbox" value="Cajuela" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cajuela'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td align="left" style="border-left:1px solid black;">
                                                Cristales.
                                            </td>
                                            <td align="center" style="border-left:1px solid black;">
                                                <input type="checkbox" value="Cristales" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Cristales'); ?>>
                                            </td>
                                            <td align="left" style="border-left:1px solid black;">
                                                Toldo.
                                            </td>
                                            <td align="center" style="border-left:1px solid black;">
                                                <input type="checkbox" value="Toldo" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Toldo'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td align="left" style="border-left:1px solid black;">
                                                Carrocería.
                                            </td>
                                            <td align="center" style="border-left:1px solid black;">
                                                <input type="checkbox" value="Carrocería" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Carrocería'); ?>>
                                            </td>
                                            <td align="left" style="border-left:1px solid black;">
                                                Debajo del vehículo.
                                            </td>
                                            <td align="center" style="border-left:1px solid black;">
                                                <input type="checkbox" value="Debajo" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Debajo'); ?>>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td align="left" colspan="4" style="border-left:1px solid black;">
                                                Estando:&nbsp;&nbsp;&nbsp;&nbsp;
                                                Dentro.&nbsp;&nbsp;
                                                <input type="checkbox" value="Dentro" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Dentro'); ?>>&nbsp;&nbsp;&nbsp;&nbsp;
                                                Fuera.&nbsp;&nbsp;
                                                <input type="checkbox" value="Fuera" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Fuera'); ?>>&nbsp;&nbsp;&nbsp;&nbsp;
                                                Frente.&nbsp;&nbsp;
                                                <input type="checkbox" value="Frente" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Frente'); ?>>&nbsp;&nbsp;&nbsp;&nbsp;
                                                Detrás.&nbsp;&nbsp;
                                                <input type="checkbox" value="Detrás" name="fallaPercibe[]" <?php echo set_checkbox('fallaPercibe[]', 'Detrás'); ?>>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <br><br>
                        Observaciones:<br>
                        <textarea name="observaciones2" class="form-control" rows="4" style="text-align:left;width:100%;"><?php echo set_value('observaciones2');?></textarea>
                        <?php echo form_error('observaciones2', '<span class="error">', '</span>'); ?>
                        <br>
                    </div>

                    <div class="col-sm-7 table-responsive">
                        <table class="table-responsive" style="border: 1px solid black; width:100%;">
                            <tr style="border-bottom: 1px solid black;">
                                <td colspan="2" align="left">
                                    <h4 style="font-weight:bold;">Condiciones ambientales.</h4>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                    Temperatura<br>ambiente.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/metrix.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td class="">
                                    <div id="medidor_1" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="tempAmbiente" id="tempAmbiente" value="<?php echo set_value("tempAmbiente"); ?>">
                                    <?php echo form_error('tempAmbiente', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                    Humedad.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/metrix_humedad.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td class="">
                                    <div id="medidor_2" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="humedad" id="humedad" value="<?php echo set_value("humedad"); ?>">
                                    <input type="hidden" name="humedadVal" id="humedadVal" value="<?php echo set_value("humedadVal"); ?>">
                                    <?php echo form_error('humedad', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                    Viento.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/metrix_viento.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_3" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="viento" id="viento" value="<?php echo set_value("viento"); ?>">
                                    <input type="hidden" name="vientoVal" id="vientoVal" value="<?php echo set_value("vientoVal"); ?>">
                                    <?php echo form_error('viento', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table-responsive" style="border: 1px solid black; width:100%;">
                            <tr style="border-bottom: 1px solid black;">
                                <td colspan="2" align="left">
                                    <h4 style="font-weight:bold;">Condiciones operativas.</h4>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                    Velocidad<br>frenado.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/velocidad2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_4" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="frenoMedidor" id="frenado" value="<?php echo set_value("frenoMedidor"); ?>">
                                    <?php echo form_error('frenoMedidor', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                    Cambio<br>
                                    Transmision.<br>
                                    4X2&nbsp;<input type="radio" value="2" name="transmision" <?php echo set_checkbox('transmision', '2'); ?>><br>
                                    4X4&nbsp;<input type="radio" value="4" name="transmision" <?php echo set_checkbox('transmision', '4'); ?>>
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/cambio2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_5" class="centradoBarra" style="width:25vw;float: left;"></div>
                                    <div id="medidor_5_A" class="centradoBarra" style="width:14.3vw;float: left;margin-left: 4vw;"></div>
                                    <input type="hidden" name="cambio" id="cambio" value="<?php echo set_value("cambio"); ?>">
                                    <input type="hidden" name="cambioVal" id="cambioVal" value="<?php echo set_value("cambioVal"); ?>">
                                    <input type="hidden" name="cambio_A" id="cambio_A" value="<?php echo set_value("cambio_A"); ?>">
                                    <input type="hidden" name="cambio_AVal" id="cambio_AVal" value="<?php echo set_value("cambio_AVal"); ?>">
                                    <?php echo form_error('cambio', '<span class="error">', '</span>'); ?>
                                    <?php echo form_error('cambio_A', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                    RPM x 1000
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/rpm2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_6" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="rpm" id="rpm" value="<?php echo set_value("rpm"); ?>">
                                    <input type="hidden" name="rpmVal" id="rpmVal" value="<?php echo set_value("rpmVal"); ?>">
                                    <?php echo form_error('rpm', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                    Carga.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/carga2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_7" class="centradoBarra" style="width:27.5vw;float: left;margin-top:8px;"></div>
                                    <input type="checkbox" value="1" name="remolque" style="float: left;margin-left: 10vw;" <?php echo set_checkbox('remolque', '1'); ?>>
                                    <input type="hidden" name="carga" id="carga" value="<?php echo set_value("carga"); ?>">
                                    <?php echo form_error('carga', '<span class="error">', '</span>'); ?>
                                    <br><br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:15px;padding-left: 10px;">
                                    Pasajeros.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/pasajeros2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_8" class="centradoBarra" style="width:27.7vw;float: left;"></div>
                                    <div id="medidor_8_A" class="centradoBarra" style="width:12vw;float: left;margin-left: 4vw;"></div>
                                    <input type="hidden" name="pasajeros" id="pasajeros_2" value="<?php echo set_value("pasajeros"); ?>">
                                    <input type="hidden" name="pasajeros_2Val" id="pasajeros_2Val" value="<?php echo set_value("pasajeros_2Val"); ?>">
                                    <input type="hidden" name="cajuela_2" id="cajuela_2" value="<?php echo set_value("cajuela_2"); ?>">
                                    <?php echo form_error('pasajeros', '<span class="error">', '</span>'); ?>
                                    <?php echo form_error('cajuela_2', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                        </table>

                        <br>
                        <table class="table-responsive" style="border: 1px solid black; width:100%;">
                            <tr style="border-bottom: 1px solid black;">
                                <td colspan="2" align="left">
                                    <h4 style="font-weight:bold;">Condiciones del camino.</h4>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                    Estructura.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/estructura2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_9" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="estructura" id="estructura" value="<?php echo set_value("estructura"); ?>">
                                    <input type="hidden" name="estructuraVal" id="estructuraVal" value="<?php echo set_value("estructuraVal"); ?>">
                                    <?php echo form_error('estructura', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                    Camino.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/camino2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_10" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="camino" id="camino" value="<?php echo set_value("camino"); ?>">
                                    <input type="hidden" name="caminoVal" id="caminoVal" value="<?php echo set_value("caminoVal"); ?>">
                                    <?php echo form_error('camino', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                    Pendiente.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/pendiente2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_11" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="pendiente" id="pendiente" value="<?php echo set_value("pendiente"); ?>">
                                    <input type="hidden" name="pendienteVal" id="pendienteVal" value="<?php echo set_value("pendienteVal"); ?>">
                                    <?php echo form_error('pendiente', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="padding-right:28px;padding-left: 10px;">
                                    Superficie.
                                </td>
                                <td>
                                    <img src="<?php echo base_url().'assets/imgs/superficie2.png' ?>" alt="" style="width:44vw;height:30px;">
                                </td>
                            </tr>
                            <tr style="border-bottom: 1px solid black;">
                                <td>
                                    <div id="medidor_12" class="centradoBarra" style="width:44vw;"></div>
                                    <input type="hidden" name="superficie" id="superficie" value="<?php echo set_value("superficie"); ?>">
                                    <input type="hidden" name="superficieVal" id="superficieVal" value="<?php echo set_value("superficieVal"); ?>">
                                    <?php echo form_error('superficie', '<span class="error">', '</span>'); ?>
                                    <br>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <br><br>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <table class="table table-bordered" style="width:100%">
                <tr>
                    <td align="center">
                        Nombre y firma del asesor.<br>
                        <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                        <img class="marcoImg" src="<?= set_value('rutaFirmaAsesor');?>" id="firmaFirmaAsesor" alt="">
                        <br><br>
                        <input type="text" class="input_field" id="asesor" name="nombreAsesor" style="width:100%;" value="<?php if(set_value('nombreAsesor') != "") echo set_value('nombreAsesor'); else if(isset($asesor)) echo $asesor; ?>">
                        <?php echo form_error('rutaFirmaAsesor', '<span class="error">', '</span>'); ?><br>
                        <?php echo form_error('nombreAsesor', '<span class="error">', '</span>'); ?><br>
                    </td>
                    <td align="center">
                        Nombre y firma del cliente.<br>
                        <input type="hidden" id="rutaFirmaAcepta" name="rutaFirmaAcepta" value="<?= set_value('rutaFirmaAcepta');?>">
                        <img class="marcoImg" src="<?= set_value('rutaFirmaAcepta');?>" id="firmaImgAcepta" alt="">
                        <br><br>
                        <input type="text" class="input_field nombreCliente" id="otroClienteN" name="consumidor1Nombre" style="width:100%;" value="<?= set_value('consumidor1Nombre');?>">
                        <?php echo form_error('rutaFirmaAcepta', '<span class="error">', '</span>'); ?><br>
                        <?php echo form_error('consumidor1Nombre', '<span class="error">', '</span>'); ?><br>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <br><br>
                        <a class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                    </td>
                    <td align="center">
                        <br><br>
                        <a class="cuadroFirma btn btn-primary" data-value="Consumidor_1" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-sm-2"></div>
    </div>

    <br>
    <div class="row" align="center">
        <div class="col-md-12" align="center">
            <input type="button" class="btn btn-success" name="enviar" id="enviarOrdenForm" value="Guardar diagnóstico">
            <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="1">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
            <button type="button" class="btn btn-danger" id="terminar" style="margin-left:30px;">Detener grabación</button>
        </div>
    </div>

</form>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
