<br><br>
<section style="margin:35px;">
	<div class="panel-flotante" style="border:2px solid darkblue;padding: 20px;">
		<input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."CartaRenuncia_Lista/4";?>';" id="regreso" value="Regresar">
		<br>
		<form id="formulario" method="post" autocomplete="on" enctype="multipart/form-data">
			<div class="row">
				<div class="col-sm-7">
					<br>
					<h4 style="font-weight: bold;">
						CARTA DE RENUNCIA A BENEFICIOS 
					</h4>
				</div>
				<div class="col-sm-5" align="right">
					<img src="<?= base_url().'assets/imgs/logos/ford_protect.png'; ?>" alt="" class="logo" style="height: 1.5cm;">
				</div>
			</div>

			<br><hr>
			<div class="row" id="autorizacion" hidden>
                <div class="col-md-3"></div>
                <div class="col-md-7">
                    <table style="width: 100%;border: 1.5px solid orange;">
                        <tr>
                            <td rowspan="<?= count($motivos_ausencia)+1 ?>" style="width: 55%;vertical-align: middle;padding-left: 10px;">
                                <input type="checkbox" style="transform: scale(1.3);" name="autorizacion_ausencia" id="autorizacion_ausencia" value="1" <?php if(isset($autorizacion_ausencia)) if($autorizacion_ausencia == "1") echo "checked";?>>
                                &nbsp;&nbsp;
                                Autorización por Ausencia
                                
                            </td>
                            <td style="width: 45%;vertical-align: middle;">
                                <div class="error" id="error_autorizacion_ausencia"></div>
                            </td>
                        </tr>
                        <?php foreach ($motivos_ausencia as $x => $row): ?>
                            <tr>
                                <td style="width: 45%;vertical-align: middle;">
                                    <input type="radio" style="transform: scale(1.3);" class="mt" value="<?= $row->id ?>" name="motivo" id="motivo_<?= $row->id ?>" <?php if(isset($motivo_ausencia)) if($motivo_ausencia == $row->id) echo "checked";?>>
                                    &nbsp;&nbsp;
                                    <?= $row->nombre ?>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                </div>
            </div>

            <br><hr>
			<div class="row">
				<div class="col-sm-5" align="right">
					<label for="" style="font-size: 17px;font-weight: inherit;">
						No. Orden :
					</label>
				</div>
				<div class="col-sm-7">
					<input type="text" name="orden" id="orden" onblur="datos_llenado()" class="form-control" value="<?php if(isset($id_orden)) echo $id_orden; ?>" style="width: 60%;">
	                <div id="error_vin"></div>
				</div>				
			</div>

			<br>
			<div style="display: none;">
				<hr>
				<div class="row">
					<div class="col-md-1"></div>
		            <div class="col-md-3">
		                <label for="" style="font-size: 15px;font-weight: inherit;">Orden Intelisis : </label>&nbsp;
		                <input type="text" class="form-control" name="folio_externo" value="<?php if(isset($folioContinue)) echo $folioContinue; ?>" style="width:100%;">
		            </div>
		            <div class="col-md-3">
		                <label for="" style="font-size: 15px;font-weight: inherit;">Modelo:</label>&nbsp;
		                <input class="form-control" type="text" name="modelo" value="<?php if(isset($modelo)) echo $modelo; ?>" style="width:100%;">
		            </div>
		            <div class="col-md-3">
		                <label for="" style="font-size: 15px;font-weight: inherit;">Placas</label>&nbsp;
		                <input type="text" class="form-control" name="placas" value="<?php if(isset($placas)) echo $placas; ?>" style="width:100%;">
		            </div>
		            
		        </div>

		        <div class="row">
		        	<div class="col-md-1"></div>
		        	<div class="col-md-3">
		                <label for="" style="font-size: 15px;font-weight: inherit;">Unidad</label>&nbsp;
		                <input class="form-control" type="text" name="uen" value="<?php if(isset($unidad)) echo $unidad; ?>" style="width:100%;">
		            </div>
		            <div class="col-md-3">
		                <label for="" style="font-size: 15px;font-weight: inherit;">Técnico</label>&nbsp;
		                <input class="form-control" type="text" name="tecnico" value="<?php if(isset($tecnico)) echo $tecnico; ?>" style="width:100%;">
		            </div>
		        </div>

		        <hr>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-5" align="right">
					<label for="" style="font-size: 17px;font-weight: inherit;">
						Número de identificación del vehículo (VIN) :
					</label>
				</div>
				<div class="col-sm-7">
					<input type="text" name="serie" id="vin" maxlength="18" minlength="17" class="form-control" value="<?php if(isset($serie)) echo $serie; ?>" style="width: 80%;text-transform: uppercase;">
	                <div id="error_vin"></div>
				</div>				
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8 table-responsive">
					<h4 style="font-weight: bold;color:black;text-align: center">
						El único plan de extensión de garantía respaldado por Ford Motor 
						Company
					</h4>
					<h4 style="font-weight: bold;color:black;text-align: center">
						Protegiendo tu tranquilidad
					</h4>
					<h3 style="font-weight: bold;color:black;text-align: center">
						Lista de características y beneficios de la garantía 
						extendida
					</h3>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8 table-responsive">
                    <table class="table" style="border:1px solid black;">
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Plazo.Adicional" <?php if(isset($beneficios)) if(in_array("Plazo.Adicional",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Plazo adicional de la cobertura
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Cobertura.Nacional" <?php if(isset($beneficios)) if(in_array("Cobertura.Nacional",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Cobertura Nacional
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Cobertura.Componentes" <?php if(isset($beneficios)) if(in_array("Cobertura.Componentes",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Amplia cobertura en componentes
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Asistencia.Vial" <?php if(isset($beneficios)) if(in_array("Asistencia.Vial",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Asistencia Vial 24 horas
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Sin.Gastos" <?php if(isset($beneficios)) if(in_array("Sin.Gastos",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Sin gastos adicionales
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Transferible" <?php if(isset($beneficios)) if(in_array("Transferible",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Transferible a otro propietario
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Movilidad" <?php if(isset($beneficios)) if(in_array("Movilidad",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Beneficio de movilidad ante cualquier imprevisto
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Financiamiento" <?php if(isset($beneficios)) if(in_array("Financiamiento",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Facilidad de Financiamiento
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Sin.Intereses" <?php if(isset($beneficios)) if(in_array("Sin.Intereses",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Meses sin intereses disponibles
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Cero.Deducible" <?php if(isset($beneficios)) if(in_array("Cero.Deducible",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Cero deducible
							</td>
						</tr>
						<tr>
							<td style="width: 10%;" align="center">
								<input type="checkbox" style="transform: scale(1.5);" class="caracterirtica" name="beneficios[]" value="Reparaciones" <?php if(isset($beneficios)) if(in_array("Reparaciones",$beneficios)) echo "checked";?>>
							</td>
							<td style="font-size: 14px;">
								Tranquilidad ante reparaciones imprevistas
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<span class="error" id ="error_caracterirtica"></span>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-2"></div>	
			</div>

			<br>
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8 table-responsive">
					<h4 style="color:black;text-align: center">
						Reconozco que he revisado las características y los beneficios del plan
						de extensión de garantía de Ford Protect y he decidido que no deseo 
						adquirirlo en este momento
					</h4>
				</div>
			</div>

			<br>
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-4" align="center">
                    <img class="marcoImg" src="<?php if(isset($firma_cliente)) { echo base_url().(($firma_cliente != "") ? $firma_cliente : 'assets/imgs/fondo_bco.jpeg'); } else{ echo base_url().'assets/imgs/fondo_bco.jpeg'; } ?>" id="firma_cliente_Img"  style="max-height: 2cm;">

                    <input type="hidden" id="ruta_firma_cliente" name="ruta_firma_cliente" value="<?php if(isset($firma_cliente)) echo $firma_cliente;?>">

                    <br>
                    <input type="text" class="input_field" name="nombre_cliente" style="width:100%;text-transform: uppercase;" value="<?php if(isset($nombre_cliente)) echo $nombre_cliente;?>">

                    <div id="ruta_firma_cliente_error"></div>
                    <div id="nombre_cliente_error"></div>
                    <br>
                    NOMBRE Y FIRMA DEL CLIENTE

                    <br><br>
                    <?php if (!isset($id)): ?>
                		<a class="cuadroFirma firmaCliente btn btn-primary" data-value="Cliente_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
					<?php endif ?>
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-3">
                	<label for="" style="font-size: 14px;font-weight: inherit;">
						Fecha:
					</label>
					<br>
					<input type="date" name="fecha_fcliente" class="form-control" max="<?= date('Y-m-d'); ?>" value="<?php if(isset($fecha_fcliente)) echo $fecha_fcliente; else echo date('Y-m-d'); ?>" style="width: 100%;padding: 0px;">
                </div>
                <div class="col-sm-2"></div>
            </div>

            <br><br>
            <div class="row">
				<div class="col-sm-2"></div>
                <div class="col-sm-4" align="center">
                    <img class="marcoImg" src="<?php if(isset($firma_asesor)) { echo base_url().(($firma_asesor != "") ? $firma_asesor : 'assets/imgs/fondo_bco.jpeg'); } else{ echo base_url().'assets/imgs/fondo_bco.jpeg'; } ?>" id="firma_asesor_Img" style="max-height: 2cm;">

                    <input type="hidden" id="ruta_firma_asesor" name="ruta_firma_asesor" value="<?php if(isset($firma_asesor)) echo $firma_asesor;?>">

                    <br>
                    <input type="text" class="input_field" name="nombre_asesor" style="width:100%;text-transform: uppercase;" value="<?php if(isset($nombre_asesor)) echo $nombre_asesor;?>">

                    <div id="ruta_firma_asesor_error"></div>
                    <div id="nombre_asesor_error"></div>
                    <br>
                    NOMBRE Y FIRMA DEL DISTRIBUIDOR 

                    <br><br>
                    <?php if (!isset($id)): ?>
                		<a class="cuadroFirma btn btn-primary" data-value="Asesor_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                	<?php endif ?>
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-3">
                	<label for="" style="font-size: 14px;font-weight: inherit;">
						Fecha:
					</label>
					<br>
					<input type="date" name="fecha_fasesor" class="form-control" max="<?= date('Y-m-d'); ?>" value="<?php if(isset($fecha_fasesor)) echo $fecha_fasesor; else echo date('Y-m-d'); ?>" style="width: 100%;padding: 0px;">
                </div>
                <div class="col-sm-2"></div>
            </div>

            <br><br>
	        <div class="row">
	        	<div class="col-sm-4"></div>
	        	<div class="col-sm-4" align="center">
	        		<i class="fas fa-spinner cargaIcono"></i>
	        		<br>
	        		<h5 class="error" id="formulario_error"></h5>
	        		<input type="hidden" name="id_orden" value="<?php if(isset($id)) echo $id; else echo '0';?>">
	        		<?php if (isset($id)): ?>
	        			<input type="hidden" name="envio_formulario" value="2">
	        			<input type="button" class="btn btn-success" name="enviar_formulario" id="enviarOrdenForm" value="ACTUALIZAR">
	        		<?php else: ?>
	        			<input type="hidden" name="envio_formulario" value="1">
	        			<input type="button" class="btn btn-success" name="enviar_formulario" id="enviarOrdenForm" value="GUARDAR">
	        		<?php endif ?>
	        	</div>
	        	<div class="col-sm-4"></div>
	        </div>
		</form>
	</div>
</section>

<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <div class="signatureparent_cont_1">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>