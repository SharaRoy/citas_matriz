<!DOCTYPE html>
<html lang="en">
    <head>
      	<meta charset="UTF-8">
      	<title>Cart Renuncia</title>
    </head>
    <body>
        <br>
        <img src="<?php echo base_url().'assets/imgs/logos/logo_queretaro_2.png' ?>" alt="" style="width:400px;">
        <br><br>
        <h3>
            Documentación. <br>
            Formato de renuncia de beneficio. (FORD PROTECT)</h3>
        <br>
        <h3>
            Documentación correspondiente al vehículo: <?php if(isset($unidad)) echo $unidad; else echo "#"; ?>, <br>
            VIN: <?php if(isset($serie)) echo $serie; else echo "Sin serie"; ?> <br>
            Orden de Servicio : #<?php if(isset($id_cita)) echo $id_cita; else echo "#"; ?>
        <h3>
            Puede ver el documento en el siguiente link: 
            <a href="<?php if(isset($url)) echo $url; else echo "#"; ?>" target="_blank">Ver Archivo</a>
        </h3> 
    </body>
</html>
