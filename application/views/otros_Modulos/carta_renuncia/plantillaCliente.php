<br><br>
<section style="margin:35px;">
    <div class="panel-flotante" style="border:2px solid darkblue;padding: 20px;">
        <div class="row">
            <div class="col-sm-7">
                <br>
                <h4 style="font-weight: bold;">
                    CARTA DE RENUNCIA A BENEFICIOS 
                </h4>
            </div>
            <div class="col-sm-5" align="right">
                <img src="<?= base_url().'assets/imgs/logos/ford_protect.png'; ?>" alt="" class="logo" style="height: 1.5cm;">
            </div>
        </div>

        <br><hr>
        <div class="row">
            <div class="col-sm-6" align="right">
                <label for="" style="font-size: 17px;font-weight: inherit;">
                    No. Orden :
                </label>
            </div>
            <div class="col-sm-6">
                <h4 style="color:darkblue;"><?= ((isset($id_orden)) ? $id_orden : "") ?></h4>
            </div>              
        </div>

        <br>
        <div class="row">
            <div class="col-sm-6" align="right">
                <label for="" style="font-size: 16px;font-weight: inherit;">
                    Número de identificación <br>
                    del vehículo (VIN) :
                </label>
            </div>
            <div class="col-sm-6">
                <h4 style="color:darkblue;"><?= ((isset($serie)) ? strtoupper($serie) : "") ?></h4>
            </div>              
        </div>

        <br>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10 table-responsive">
                <h4 style="font-weight: bold;color:black;text-align: center">
                    El único plan de extensión de garantía <br> 
                    respaldado por Ford Motor Company
                </h4>
                <h4 style="font-weight: bold;color:black;text-align: center">
                    Protegiendo tu tranquilidad
                </h4>
                <h5 style="font-weight: bold;color:darkblue;text-align: center">
                    Lista de características y beneficios de la garantía 
                    extendida
                </h5>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8 table-responsive" style="border:1px solid black; padding: 10px; ">
                <table style="width: 100%;">
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Plazo.Adicional",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Plazo adicional de la cobertura
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Cobertura.Nacional",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Cobertura Nacional
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Cobertura.Componentes",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Amplia cobertura en componentes
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Asistencia.Vial",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Asistencia Vial 24 horas
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Sin.Gastos",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Sin gastos adicionales
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Transferible",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Transferible a otro propietario
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Movilidad",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Beneficio de movilidad ante cualquier imprevisto
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Financiamiento",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Facilidad de Financiamiento
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Sin.Intereses",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Meses sin intereses disponibles
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Cero.Deducible",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Cero deducible
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br></td>
                    </tr>
                    <tr>
                        <td class="listado" align="center">
                            <?php if (isset($beneficios)): ?>
                                <?php if (in_array("Reparaciones",$beneficios)): ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                                <?php else: ?>
                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                                <?php endif ?>
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        </td>
                        <td style="font-size: 16px;">
                            Tranquilidad ante reparaciones imprevistas
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-9 table-responsive">
                <table class="table" style="width:100%;">
                    <tr>
                        <td align="center" style="color:black;font-size:9px;width: 200px;">
                            <?php if (isset($firma_cliente)): ?>
                                <?php if ($firma_cliente != ""): ?>
                                    <img class='marcoImg' src='<?= base_url().$firma_cliente; ?>' style='width:150px;'>

                                <?php else: ?>
                                    <img class="marcoImg" src="" id="firma_cliente_Img" alt="">
                                    <br>

                                    <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Cliente_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <img class="marcoImg" src="" id="firma_cliente_Img" alt="">
                                <br>

                                <a class="cuadroFirma firmaCliente btn btn-primary" data-value="Cliente_2" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                            <?php endif; ?>

                            <br>
                            <div class="error" id="error_firma_2"></div>
                            <p align="center" style="text-align: center; font-size:9px;color:darkblue;">
                                <?php if (isset($nombre_cliente)): ?>
                                    <?php echo $nombre_cliente; ?>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td style="width: 10px;" rowspan="2">
                            <br>
                        </td>
                        <td style="width:60px;padding-left: 10px;" rowspan="2" align="center">
                            <h5 style="font-size:13px;color:#337ab7;">Fecha:</h5>
                            <p style="font-size:11px;color:black;text-align: justify;">
                                <?php if (isset($fecha_cliente_visual)): ?>
                                    <?php if ($firma_cliente != ""): ?>
                                        <?php echo $fecha_cliente_visual; ?>
                                    <?php else: ?>
                                        <?= date("d-m-Y")?>
                                    <?php endif ?>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td align="center" style="color:black;font-size:9px;width: 200px;">
                            <?php if (isset($firma_asesor)): ?>
                                <?php if ($firma_asesor != ""): ?>
                                    <img src="<?php echo base_url().$firma_asesor; ?>" style='width:150px;'>
                                <?php endif ?>
                            <?php endif; ?>
                            <br>
                            <p align="center" style="text-align: center; font-size:9px;color:darkblue;">
                                <?php if (isset($nombre_asesor)): ?>
                                    <?php echo $nombre_asesor; ?>
                                <?php endif; ?>
                            </p>
                        </td>
                        <td style="width: 10px;" rowspan="2">
                            <br>
                        </td>
                        <td style="width:60px;padding-left: 10px;" rowspan="2" align="center">
                            <h5 style="font-size:13px;color:#337ab7;">Fecha:</h5>
                            <p style="font-size:11px;color:black;text-align: justify;">
                                <?php if (isset($fecha_asesor_visual)): ?>
                                    <?php echo $fecha_asesor_visual ?>
                                <?php endif; ?>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="border-top: 1px solid #337ab7;width:150px;font-size:10px;">
                            NOMBRE Y FIRMA DEL CLIENTE<br>
                        </td>
                        
                        <td align="center" style="border-top: 1px solid #337ab7;width:150px;font-size:10px;">
                            NOMBRE Y FIRMA DEL DISTRIBUIDOR <br>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</section>

 <br><br>
<?php if (isset($firma_cliente)): ?>
    <?php if ($firma_cliente == ""): ?>
        <form id="formulario_cliente" method="post" action="" autocomplete="on" enctype="multipart/form-data">
            <input type="hidden" id="ruta_firma_cliente" name="ruta_firma_cliente" value="">

            <input type="hidden" name="id_cita" id="" value="<?php if(isset($id_orden)) echo $id_orden; ?>">

            <br>
            <div class="row" align="center">
                <div class="col-md-12"  align="center">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <span class="error" id ="formulario_error"></span>

                    <br>
                    <input type="button" class="btn btn-success" id="envio_diag" value="Guardar Firma">
                </div>
            </div>
        </form>
    <?php endif ?>
<?php endif ?>


<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <div class="signatureparent_cont_1">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>