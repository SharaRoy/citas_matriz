<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
    <head>
        <title>CARTA RENUNCIA <?php if(isset($id_orden)) echo $id_orden;?></title>
        <meta charset="utf-8" />

        <style media="screen">
            @page { 
                sheet-size: A4;
                size: auto; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 10mm; 
                /*margin-top: 5mm; /* <any of the usual CSS values for margins> */
                /*margin-left: 10mm;*/
                /*margin-right: 10mm;*/
                /*margin-bottom: 10mm;*/
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 10mm; /* <any of the usual CSS values for margins> */
                margin-footer: 10mm; /* <any of the usual CSS values for margins> */
            } 
        </style>

        <style>
            td,tr{
                padding: 0px !important;
                font-size: 9px;
            }

            .recuadros{
                border: 1px solid black;
                width: 11px;
                vertical-align: middle;
                font-size: 10px;
            }

            .listado{
                width: 55px;
                height: 15px;
                font-size: 10px;
            }

        </style>
    </head>
    <body>
        <div>
            <table style="border-bottom: 2px solid black;width: 100%;">
                <tr>
                    <td style="width: 70%;font-size: 17px;font-weight: bold;">
                        CARTA DE RENUNCIA A BENEFICIOS 
                    </td>
                    <td>
                        <img src="<?= base_url().'assets/imgs/logos/ford_protect.png'; ?>" alt="" class="logo" style="height: 1cm;">
                    </td>
                </tr>
            </table>
            
            <br>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 120px;font-size: 13px;">
                        Número de identificación del vehículo (VIN) :
                        <br>
                    </td>
                    <?php if (isset($serie)): ?>
                        <?php $caracteres = strlen($serie); ?>
                        <?php for ($x = 0; $x < $caracteres; $x++): ?>
                            <td class="recuadros" align="center">
                                <?php echo $serie[$x]; ?>
                            </td>
                        <?php endfor ?>
                        <?php if ($caracteres<17): ?>
                            <?php for ($x = 0; $x <  (17-$caracteres); $x++): ?>
                                <td class="recuadros" align="center">
                                    <br>
                                </td>
                            <?php endfor ?>
                        <?php endif ?>
                    <?php else: ?>
                        <?php for ($x = 0; $x < 17; $x++): ?>
                            <td class="recuadros" align="center">
                                X
                            </td>
                        <?php endfor ?>
                    <?php endif ?>
                </tr>
            </table>
        </div>

        <br>
        <div style="margin: 10px 30px 10px 30px;" align="center">
            <h3 style="font-weight: bold;color:black;text-align: center;font-size: 15px;">
                El único plan de extensión de garantía respaldado por Ford Motor <br>
                Company
            </h3>
            <h4 style="font-weight: bold;color:black;text-align: center;font-size: 18px;margin-top:-15px;">
                Protegiendo tu tranquilidad
            </h4>
            <h4 style="font-weight: bold;color:black;text-align: center;font-size: 21px;margin-top:-10px;">
                Lista de características y beneficios de la garantía <br>
                extendida
            </h4>

            <br>
            <table style="border:1px solid black;width: 100%;">
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Plazo.Adicional",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Plazo adicional de la cobertura
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Cobertura.Nacional",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Cobertura Nacional
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Cobertura.Componentes",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Amplia cobertura en componentes
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Asistencia.Vial",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Asistencia Vial 24 horas
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Sin.Gastos",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Sin gastos adicionales
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Transferible",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Transferible a otro propietario
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Movilidad",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Beneficio de movilidad ante cualquier imprevisto
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Financiamiento",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Facilidad de Financiamiento
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Sin.Intereses",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Meses sin intereses disponibles
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Cero.Deducible",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Cero deducible
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><br></td>
                </tr>
                <tr>
                    <td class="listado" align="center">
                        <?php if (isset($beneficios)): ?>
                            <?php if (in_array("Reparaciones",$beneficios)): ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="width:20px;" alt="">
                            <?php else: ?>
                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                            <?php endif ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="width:15px;" alt="">
                        <?php endif ?>
                    </td>
                    <td style="font-size: 16px;">
                        Tranquilidad ante reparaciones imprevistas
                    </td>
                </tr>
            </table>

            <br><br>
            <table class="table table-bordered" style="width:100%;">
                <tr>
                    <td align="center" style="color:black;font-size:9px;width: 200px;">
                        <?php if (isset($firma_cliente)): ?>
                            <?php if ($firma_cliente != ""): ?>
                                <img src="<?php echo base_url().$firma_cliente; ?>"  style='width:150px;'>
                            <?php endif ?>
                        <?php endif; ?>
                        <br>
                        <p align="center" style="text-align: center; font-size:9px;color:darkblue;">
                            <?php if (isset($nombre_cliente)): ?>
                                <?php echo $nombre_cliente; ?>
                            <?php endif; ?>
                        </p>
                    </td>
                    <td style="width: 10px;" rowspan="2">
                        <br>
                    </td>
                    <td style="width:60px;padding-left: 10px;" rowspan="2" align="center">
                        <h5 style="font-size:13px;color:#337ab7;">Fecha:</h5>
                        <p style="font-size:11px;color:black;text-align: justify;">
                            <?php if (isset($fecha_cliente_visual)): ?>
                                <?php echo $fecha_cliente_visual ?>
                            <?php endif; ?>
                        </p>
                    </td>
                    <td align="center" style="color:black;font-size:9px;width: 200px;">
                        <?php if (isset($firma_asesor)): ?>
                            <?php if ($firma_asesor != ""): ?>
                                <img src="<?php echo base_url().$firma_asesor; ?>" style='width:150px;'>
                            <?php endif ?>
                        <?php endif; ?>
                        <br>
                        <p align="center" style="text-align: center; font-size:9px;color:darkblue;">
                            <?php if (isset($nombre_asesor)): ?>
                                <?php echo $nombre_asesor; ?>
                            <?php endif; ?>
                        </p>
                    </td>
                    <td style="width: 10px;" rowspan="2">
                        <br>
                    </td>
                    <td style="width:60px;padding-left: 10px;" rowspan="2" align="center">
                        <h5 style="font-size:13px;color:#337ab7;">Fecha:</h5>
                        <p style="font-size:11px;color:black;text-align: justify;">
                            <?php if (isset($fecha_asesor_visual)): ?>
                                <?php echo $fecha_asesor_visual ?>
                            <?php endif; ?>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-top: 1px solid #337ab7;width:150px;font-size:10px;">
                        NOMBRE Y FIRMA DEL CLIENTE<br>
                    </td>
                    
                    <td align="center" style="border-top: 1px solid #337ab7;width:150px;font-size:10px;">
                        NOMBRE Y FIRMA DEL DISTRIBUIDOR <br>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>

<?php 
    try {      

        $html = ob_get_clean();
        ob_clean();

        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','debug' => TRUE]);
        $mpdf = new \mPDF('utf-8', 'A4-P');

        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        
        $mpdf->Output();

    //} catch (Html2PdfException $e) {
    } catch (Exception $e) {
    //    $html2pdf->clean();
    //    $formatter = new ExceptionFormatter($e);
    //    echo $formatter->getHtmlMessage();
        echo "NO SE PUDO GENERAR EL PDF";
    }
 ?>