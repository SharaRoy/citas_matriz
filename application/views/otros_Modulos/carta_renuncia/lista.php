<div style="margin:10px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Registro actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-2" align="right"></div>
                <div class="col-sm-8">
                    <h3 align="center">Listado Carta Renuncia Beneficios</h3>
                </div>
                <div class="col-sm-2" align="right">
                    <input type="button" class="btn btn-success" style="margin-top: 15px;" onclick="location.href='<?=base_url()."CartaRenuncia/0";?>';" name="" value="Nuevo registro">
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-1">
                            <br>
                            <input type="button" class="btn btn-dark" style="color:white;margin-top: 20px;" onclick="location.href='<?=base_url()."Panel_Asesor/4";?>';" name="" value="Regresar">
                        </div>

                        <div class="col-sm-3" style="padding: 0px;padding-left: 30px;"></div>

                        <div class="col-sm-3"></div>

                        <!-- formulario de busqueda -->
                        <div class="col-sm-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_gral" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-sm-2" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" id="btnBusqueda" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="form-group" align="center" style="overflow-x: scroll;overflow-y: scroll;">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <td align="center" style="width: 5%;vertical-align: middle;"><strong>#</strong></td>
                                    <td align="center" style="width: 8%;vertical-align: middle;"><strong>No ORDEN</strong></td>
                                    <!--<td align="center" style="width: 12%;vertical-align: middle;"><strong>FOLIO</strong></td>-->
                                    <td align="center" style="width: 15%;vertical-align: middle;"><strong>SERIE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;"><strong>UNIDAD</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA REGISTRO</strong></td>
                                    <td align="center" colspan="3" style="width: 8%;vertical-align: middle;"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar"> 
                                <!-- Verificamos que se hayan cargado los datos para mostrar-->
                                <?php if (isset($id_cita)): ?>
                                    <!-- Imprimimos los datos-->
                                    <?php foreach ($id_cita as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <td align="center" style="width:5%;vertical-align: middle;background-color: <?= $cl_envio[$index] ?>;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:8%;vertical-align: middle;">
                                                <?php echo $id_cita[$index]; ?>
                                            </td>
                                            <!--<td align="center" style="width:20%;vertical-align: middle;">
                                                <?php echo $folio[$index]; ?>
                                            </td>-->
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $serie[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $unidad[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $cliente[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $fecha_registro[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <a href="<?=base_url().'CartaRenuncia_PDF/'.$id_cita_url[$index];?>" class="btn btn-default" target="_blank" style="font-size: 10px;background-color:  #a569bd; color:white;">VER</a>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php if ($envio[$index] == "0"): ?>
                                                    <a onclick="enviar_correo(<?= $id_cita[$index] ?>)" class="btn btn-success"data-target='#AlertaModal' data-toggle='modal' style="font-size: 11px;color:white;">Enviar</a>
                                                    <input type="hidden" id="rcb_<?= $id_cita[$index]; ?>" value="<?= $cliente[$index].'|1'; ?>">
                                                <?php else: ?>
                                                    <a onclick="enviar_correo(<?= $id_cita[$index] ?>)" class="btn btn-warning" data-target='#AlertaModal' data-toggle='modal' style="font-size: 11px;color:white;">Reenviar</a>
                                                    <input type="hidden" id="rcb_<?= $id_cita[$index]; ?>" value="<?= $cliente[$index].'|2'; ?>">
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <a href="<?=base_url().'assets/ford_protect_plan_2019.pdf'; ?>" class="btn btn-warning" target="_blank" style="font-size: 10px;b color:white;">FORD Protect</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <!-- Si no se cargaron los datos para mostrar -->
                                <?php else: ?>
                                    <tr>
                                        <td colspan="11" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="rolVista" value="listaAsesor">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="AlertaModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="AlertaModalCuerpo">
            <div class="modal-body">
                <br><br>
                <h5 class="modal-title" id="tituloForm" align="center"></h5>
                <br>
                <div class="row">
                    <div class="col-md-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <br>
                        <h4 id="respuesta_envio">
                            
                        </h4>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12" align="CENTER">
                        <label style="font-size: 13px;color: #a93226 ;" id="errorEnvioCotizacion"></label>
                        <br>
                        <input type="hidden" id="orden" value="">
                        <input type="hidden" id="envio" value="">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="cancelar">Cancelar</button>
                        <button type="button" class="btn btn-default" style="margin-left: 20px;" id="enviar_email">Enviar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- mostramos la informacion para correo -->
<!--
<div class="modal fade" id="envio_correo" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="">Datos para enviar correo</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <h4 style="text-align: center;">
                        ENVIAR CORREO DEL PDF <br> 
                        "CARTA DE RENUNCIA A BENEFICIO"  FORD PROTECT A:
                    </h4>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Correo 1:</label>
                            <input type="text" class="form-control" value="" id="serie_txt">
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Correo 2:</label>
                            <input type="text" class="form-control" value="" id="serie_txt">
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">Correo opcional:</label>
                            <input type="text" class="form-control" value="" id="serie_txt">
                        </div>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-md-12" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <h5 style="text-align: center;font-weight: bold;" id="resultad_envio"></h5>
                        <br>
                    </div>
                </div>
            </div>

            <div class="modal-footer" align="center">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelar">Cancelar</button>
                <button type="button" class="btn btn-success" id="asignar_registro" style="margin-left: 30px;">Enviar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar" style="display:none;" onclick="location.reload()">Cerrar</button>
            </div>
        </div>
    </div>
</div> -->