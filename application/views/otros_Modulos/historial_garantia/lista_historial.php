<div style="margin: 10px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Registro actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <br>
    <div class="panel-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-2" align="right">
                    <?php if ($this->session->userdata('rolIniciado')): ?>
                        <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                Regresar
                            </button>
                        <?php else: ?>
                            <!--<button type="button" class="btn btn-dark" >
                                Sesión Expirada
                            </button>-->
                        <?php endif; ?>
                    <?php elseif ($this->session->userdata('id_usuario')): ?>
                        <button type="button" class="btn btn-dark" onclick="window.close()">
                            Regresar
                        </button>
                    <?php else: ?>
                        <button type="button" class="btn btn-dark" style="color:white; margin-top:20px;" onclick="location.href='<?=base_url()."Panel/5";?>';">
                            Regresar
                        </button> 
                    <?php endif; ?>
                </div>
                <div class="col-md-8">
                    <h3 align="center">PARTES SOLICITADAS NO INSTALADAS - GARANTÍAS</h3>
                </div>
                <div class="col-md-2" align="right">
                    <input type="button" class="btn btn-success" style="color:white; margin-top:20px;" onclick="location.href='<?=base_url()."HG_Alta/0";?>';" value="Nuevo Registro">
                </div>
            </div>

            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-9" style="padding-left: 1cm;">
                            <div class="row" style="border: 1px solid gray;padding:10px;">                                
                                <div class="col-md-4">
                                    <h5>Fecha Inicio:</h5>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                        </div>
                                        <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <h5>Fecha Fin:</h5>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                        </div>
                                        <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                                    </div>
                                </div>

                                <div class="col-md-4" align="center">
                                    <br><br>
                                    <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_hg" style="margin-right: 15px;"> 
                                        <i class="fa fa-search"></i>
                                    </button>

                                    <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                        <!--<i class="fa fa-trash"></i>-->
                                        Limpiar Busqueda
                                    </button>
                                </div>
                            </div>
                            <label class="error">*Nota: La busqueda por fecha se realiza en base a la fecha en que se abrio la orden.</label>
                        </div>

                        <!-- formulario de busqueda -->
                        <div class="col-md-3" style="padding-top: 20px;">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>  
                        <!-- /.formulario de busqueda -->
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="btn" style="background-color: #92D050;cursor: inherit;padding-top: 0px;float:left;margin-right: 2px;"> 
                                &nbsp;&nbsp;&nbsp;
                            </label>
                            <label style="font-size: 11px; font-weight: bold;">DISP<br>INSTALAR</label>
                        </div>
                        <div class="col-md-2">
                            <label class="btn" style="background-color: #FF0000;cursor: inherit;padding-top: 0px;float:left;margin-right: 2px;"> 
                                &nbsp;&nbsp;&nbsp;
                            </label>
                            <label style="font-size: 11px; font-weight: bold;">NO<br>CONTESTA</label>
                        </div>
                        <div class="col-md-2">
                            <label class="btn" style="border: 1px solid black;cursor: inherit;padding-top: 0px;float:left;margin-right: 2px;"> 
                                &nbsp;&nbsp;&nbsp;
                            </label>
                            <label style="font-size: 11px; font-weight: bold;">PENDIENTE</label>
                        </div>
                        <div class="col-md-2">
                            <label class="btn" style="background-color: #FFFF00;cursor: inherit;padding-top: 0px;float:left;margin-right: 2px;"> 
                                &nbsp;&nbsp;&nbsp;
                            </label>
                            <label style="font-size: 11px; font-weight: bold;">NO<br>IDENTIFICADO</label>
                        </div>
                        <div class="col-md-2">
                            <label class="btn" style="background-color: #00B0F0;cursor: inherit;padding-top: 0px;float:left;margin-right: 2px;"> 
                                &nbsp;&nbsp;&nbsp;
                            </label>
                            <label style="font-size: 11px; font-weight: bold;">POR<br>RESERVAR</label>
                        </div>
                        <div class="col-md-2">
                            <label class="btn" style="background-color: #7030A0;cursor: inherit;padding-top: 0px;float:left;margin-right: 2px;"> 
                                &nbsp;&nbsp;&nbsp;
                            </label>
                            <label style="font-size: 11px; font-weight: bold;">RESERVADO</label>
                        </div>
                    </div>

                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="min-width:100%;">
                            <thead>
                                <tr style="font-size:14px; background-color: #eee;">
                                    <td align="center" style="width: 5%;vertical-align: middle;">#</td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ORDEN</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>FOLIO</strong></td>
                                    <td align="center" style="width: 8%;vertical-align: middle;"><strong>SERIE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>TECNICO</strong></td>
                                    <td align="center" style="width: 15%;vertical-align: middle;"><strong>TELEFONO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA RECIBE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA PROMESA</strong></td>
                                    <td align="center" colspan="5"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($orden)): ?>
                                    <?php foreach ($orden as $index => $valor): ?> 
                                        <tr style="font-size:12px;">
                                            <td align="center" style="width:10%; background-color:<?php echo $estatusColor[$index]; ?>; color: <?php echo $colorLetra[$index]; ?>;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $orden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $folio[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $serie[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $cliente[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $tel_cliente[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $fecha_recibe[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $fecha_promesa[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <input type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."HG_Editar/".$id[$index];?>';" value="Editar">
                                            </td>
                                            <td align="center" style="width: 5%;">
                                                <?php if ($idProactivo[$index] != "0"): ?>
                                                    <a href="javascript: void(0);" title="Agregar Cita" onclick="parent.window.location='<?= BASE_CITAS."citas/agendar_cita/0/0/0/".$orden[$index] ?>'" style="font-size: 18px;color: black;">
                                                        <i class="fas fa-plus"></i>
                                                    </a>    
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="width: 5%;">
                                                <?php if ($idProactivo[$index] != "0"): ?>
                                                    <a class="addComentarioBtn" onclick="btnAddClick(<?php echo $idProactivo[$index]; ?>)" title="Agregar Comentario" data-id="<?php echo $idProactivo[$index]; ?>" data-target="#addComentario" data-toggle="modal" style="font-size: 18px;color: black;">
                                                        <i class="fas fa-comments"></i>
                                                    </a>         
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="width: 5%;">
                                                <?php if ($idProactivo[$index] != "0"): ?>
                                                    <a class="historialCometarioBtn" onclick="btnHistorialClick(<?php echo $idProactivo[$index]; ?>)" title="Historial comentarios" data-id="<?php echo $idProactivo[$index]; ?>" data-target="#historialCometario" data-toggle="modal" style="font-size: 18px;color: black;">
                                                        <i class="fas fa-info"></i>
                                                    </a>
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="width: 5%;">
                                                <a class="eliminar" onclick="btnDeleteClick(<?php echo $id[$index]; ?>)" title="Eliminar Registro" data-id="<?php echo $id[$index]; ?>" data-target="#eliminar" data-toggle="modal" style="font-size: 18px;color: black;">
                                                    <i class="fas fa-trash"></i>
                                                </a>  
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="13" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="origenRegistro" value="2">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- Modal para agregar un comentario -->
<div class="modal fade" id="addComentario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Ingresar comentario</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="row">
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Titulo</h4>
                        <input type="text" class="form-control" name="" id="titulo" value="">
                    </div>
                </div>
                <br><br>
                <h4 style="font-weight: bold;">
                    NOTIFICACIONES
                </h4>
                <div class="row">
                    <div class="col-sm-6">
                        <h4>Fecha</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-calendar-alt"></i>
                            </div>
                            <input type="date" class="form-control" id="fechaComentario" max="<?php echo date("Y-m-d"); ?>" value="<?php echo date("Y-m-d"); ?>" style="padding-top: 0px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h4>Hora</h4>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="far fa-clock"></i>
                            </div>
                            <input type="text" class="form-control" id="HoraComentario" value="<?php echo date("H:m"); ?>">
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Comentario</h4>
                        <textarea rows='3' class='form-control' name="" id='comentario' style='width:100%;text-align:left;font-size:12px;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="idMagigAdd" value="">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" id="btnEnviarComentario">Enviar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ver el historial de comentarios -->
<div class="modal fade" id="historialCometario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Historial de comentarios</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr>
                                    <td style="width:25%;"><h4 align="center">Fecha</h4></td>
                                    <td style="width:50%;"><h4 align="center">Comentario</h4></td>
                                    <td style="width:25%;"><h4 align="center">Fecha notificación</h4></td>
                                </tr>
                            </thead>
                            <tbody id="tablaComnetario">

                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="idMagigHistorial" value="">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para eliminar registro -->
<div class="modal fade" id="eliminar" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Eliminar registro de la lista</h3>
            </div>

            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult2">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult2">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>

                <div class="row">
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>

                <h4 id="datosCabecera"></h4>
                <h4 id="datosEstatus"></h4>

                <div class="row">
                    <div class="col-sm-12">
                        <textarea rows='4' class='form-control' name="" id='refacciones' style='width:100%;text-align:left;font-size:12px;' disabled></textarea>
                    </div>
                </div>
                
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Motivos Baja</h4>
                        <textarea rows='3' class='form-control' name="" id='motivoBaja' style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <label class="error" id="mensajeError"></label>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="idRegistro" value="">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" id="btnEliminar">Eliminar</button>
            </div>
        </div>
    </div>
</div>