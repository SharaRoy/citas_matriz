<section style="margin:30px;">
	<form id="historial_garantias_refacciones" name="" method="post"> 
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<h4 style="text-align: center;font-weight: bold;">PARTES SOLICITADAS NO INSTALADAS (GARANTÍAS) - REGISTRO</h4>
			</div>
			<div class="col-md-2">
			</div>
		</div>

		<div class="panel-flotante">
			<h4 style="text-align: center; color: darkblue;">Datos del Generales</h4>
			<hr>

			<div class="row">
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">Orden:</label>
	                <br>
	                <input type="text" name="orden" id="ordenForm" onblur="precarga(2)"  class="form-control" value="<?php if(set_value('orden') != "") {echo set_value('orden');} else { if(isset($orden)) echo $orden; } ?>">
	                <br><span class="error" id ="error_orden"></span>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">Folio:</label>
	                <br>
	                <input type="text" name="folio" id="" class="form-control" value="<?php if(set_value('folio') != "") {echo set_value('folio');} else { if(isset($folio)) echo $folio; } ?>">
	                <br><span class="error" id ="error_folio"></span>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha folio:</label>
	                <br>
	                <input type="date" name="fecha_folio" max="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" id="" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_folio"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Estatus:</label>
	                <br>
					<select class="form-control" name="estatuaOrden" id="estatuaOrden">
						<option value="">SELECCIONE</option>
						<option value="DISP INSTALAR" style="background-color: #92D050;">DISP INSTALAR</option>
						<option value="NO CONTESTA" style="background-color: #FF0000;color:white;">NO CONTESTA</option>
						<option value="PENDIENTE">PENDIENTE</option>
						<option value="NO IDENTIFICADO" style="background-color: #FFFF00;">NO IDENTIFICADO</option>
						<option value="POR RESERVAR" style="background-color: #00B0F0;color:white;">POR RESERVAR</option>
						<option value="RESERVADO" style="background-color: #7030A0;color:white;">RESERVADO</option>
					</select>
	                <br><span class="error" id ="error_estatuaOrden"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">
						Asesor: <b class="error" style="font-size: 13px;font-weight: bold;">*</b>
					</label>
	                <br>
	                <select class="form-control" name="asesor" id="asesor">
						<option value="">SELECCIONE</option>
						<?php if ($asesores != NULL): ?>
							<?php foreach ($asesores as $i => $ases): ?>
								<option value="<?= $ases->nombre ?>"><?= $ases->nombre ?></option>
							<?php endforeach ?>
						<?php endif ?>
					</select>
	                <br><span class="error" id="error_asesor"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Técnico:</label>
	                <br>
	                <input type="text" name="tecnico" id="inputTecnico" class="form-control" value="<?php echo set_value('tecnico');?>">
	                <br><span class="error" id ="error_tecnico"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Notas Estatus:</label>
	                <br>
	                <textarea  class="form-control" name="notaEstatus" id="notaEstatus"><?php echo set_value('notaEstatus');?></textarea>
	                <br><span class="error" id ="error_notaEstatus"></span>
				</div>
			</div>
				
			<br>
			<h4 style="text-align: center; color: darkblue;">Datos de la unidad</h4>
			<hr>

			<div class="row">
				<div class="col-md-8">
					<label for="" style="font-size: 13px;font-weight: inherit;">Cliente:</label>
	                <br>
	                <input type="text" name="cliente" id="inputCliente" class="form-control" value="<?php echo set_value('cliente');?>">
	                <br><span class="error" id ="error_cliente"></span>
				</div>
				<div class="col-md-4">
					<label for="" style="font-size: 13px;font-weight: inherit;">Teléfono:</label>
	                <br>
	                <input type="text" maxlength="15" id="inputTelefono" name="telefonoCliente" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php echo set_value('telefonoCliente');?>">
	                <input type="hidden" maxlength="15" id="inputTelefono_2" name="telefonoCliente_2" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php echo set_value('telefonoCliente_2');?>">
	                <br><span class="error" id ="error_telefonoCliente"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Vehículo:</label>
	                <br>
	                <input type="text" name="modeloVehiculo" id="inputModelo" class="form-control" value="<?php echo set_value('modeloVehiculo');?>">
	                <br><span class="error" id ="error_modeloVehiculo"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Serie:</label>
	                <br>
	                <input type="text" name="serie" id="inputSerie" class="form-control" value="<?php echo set_value('serie');?>">
	                <br><span class="error" id ="error_serie"></span>
				</div>
			</div>

			<br>
			<h4 style="text-align: center; color: darkblue;">Datos de la refacción</h4>
			<hr>

			<div id="contenedor_Refacciones">
				<input type="hidden" name="maxRenglon" id="maxRenglon" value="1">
				<div class="row">
					<div class='col-md-2'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Cantidad:</label>
						<br><span class="error" id ="error_refaccion"></span>
					</div>
					<div class='col-md-3'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Número de parte:</label>
					</div>
					<div class='col-md-5'>
						<label for='' style='font-size: 13px;font-weight: inherit;'>Descripción Corta:</label>
					</div>
					<div class='col-md-1' align='right'>
						<a class='btn btn-success' style='color:white;width: 1cm;' onclick="agregarRef(0)">
                    		<i class='fa fa-plus'></i>
						</a>
					</div>
					<div class='col-md-1' align='right'>
						<!--<a class='refaccionesBox btn btn-warning' data-target="#refacciones" data-toggle="modal" style="color:white;">
                            <i class="fa fa-search"></i>
                        </a>-->
					</div>				
				</div>

				<br>
				<div class='row' id='renglon_1'>
					<div class='col-md-2'>
		                <input type='number' step='0.01' min='1' name='cantidad_1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='form-control' value='<?php if(set_value('cantidad_1') != "") echo set_value('cantidad_1'); else echo '1';?>'>
		                <br><span class="error" id ="error_cantidad_1"></span>

		                <input type='hidden' name='id_refaccion_1' id='id_refaccion_1' value='0'>
			            <input type='hidden' name='id_index_1' id='id_index_1' value='0'>
					</div>
					<div class='col-md-3'>
		                <input type='text' name='numParte_1' id='numParte_1' class='form-control' value='<?php echo set_value('numParte_1');?>'>
		                <br><span class="error" id ="error_numParte_1"></span>
					</div>
					<div class='col-md-5'>
		                <textarea  class='form-control' name='descripcionCorta_1' id='descripcionCorta_1' rows='2'><?php echo set_value('descripcionCorta_1');?></textarea>
		                <br><span class="error" id ="error_descripcionCorta_1"></span>
					</div>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Pedido:</label>
	                <br>
	                <input type="date" name="fecha_pedido" max="<?php echo date('Y-m-d'); ?>" id="" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_pedido"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">No. Remisión:</label>
	                <br>
	                <input type="text" name="noRemision" id="" class="form-control" value="<?php echo set_value('noRemision');?>">
	                <br><span class="error" id ="error_noRemision"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha B.O.:</label>
	                <br>
	                <input type="date" name="fechaBO" max="<?php echo date('Y-m-d'); ?>" id="" class="form-control" value="<?php echo set_value('fechaBO');?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fechaBO"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">No. Unidad Inmovilizada:</label>
	                <br>
	                <input type="text" name="unidadInmo" id="" class="form-control" value="<?php echo set_value('unidadInmo');?>">
	                <br><span class="error" id ="error_unidadInmo"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Promesa:</label>
	                <br>
	                <input type="date" name="fecha_promesa" id="inputFechaPromesa" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_promesa"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Recibe:</label>
	                <br>
	                <input type="date" name="fecha_recibe" max="<?php echo date('Y-m-d'); ?>" id="inputFechaRecibe" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_recibe"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Fecha Aviso de Taller:</label>
	                <br>
	                <input type="date" name="fecha_taller" max="<?php echo date('Y-m-d'); ?>" id="" class="form-control" value="<?php echo date('Y-m-d'); ?>" style="padding-top: 0px;">
	                <br><span class="error" id ="error_fecha_taller"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Revisada por:</label>
	                <br>
	                <input type="text" name="revisadaPor" id="" class="form-control" value="<?php echo set_value('revisadaPor');?>">
	                <br><span class="error" id ="error_revisadaPor"></span>
				</div>
			</div>

			<br>
			<div class="row">
				<div class="col-md-6">
					<label for="" style="font-size: 14px;font-weight: inherit;">Tiempo Instalación Técnico:</label>
					<div class="row">
						<div class="col-md-5">
							<label for="" style="font-size: 13px;font-weight: inherit;">
								Tiempo:
							</label>
			                <br>
			                <select class="form-control" name="tiempo_tecnico" id="tiempo_tecnico">
								<option value="">SELECCIONE</option>
								<?php if (count($tiempo_int)): ?>
									<?php for ($i = 0; $i < count($tiempo_int); $i++): ?>
										<option value="<?= $tiempo_int[$i] ?>"><?= $tiempo_int[$i] ?></option>
									<?php endfor ?>
								<?php endif ?>
							</select>
			                <br><span class="error" id ="error_tiempo_tecnico"></span>
						</div>

						<div class="col-md-4">
							<label for="" style="font-size: 13px;font-weight: inherit;">
								Unidad:
							</label>
			                <br>
			                <select class="form-control" name="unidad_tecnico" id="unidad_tecnico">
								<option value="Hora(s)">Hora(s)</option>
								<option value="Día(s)">Día(s)</option>
							</select>
			                <br><span class="error" id ="error_unidad_tecnico"></span>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<label for="" style="font-size: 14px;font-weight: inherit;">Tiempo Instalación Cliente:</label>
					<div class="row">
						<div class="col-md-5">
							<label for="" style="font-size: 13px;font-weight: inherit;">
								Tiempo:
							</label>
			                <br>
			                <select class="form-control" name="tiempo_cliente" id="tiempo_cliente">
								<option value="">SELECCIONE</option>
								<?php if (count($tiempo_int)): ?>
									<?php for ($i = 0; $i < count($tiempo_int); $i++): ?>
										<option value="<?= $tiempo_int[$i] ?>"><?= $tiempo_int[$i] ?></option>
									<?php endfor ?>
								<?php endif ?>
							</select>
			                <br><span class="error" id ="error_tiempo_cliente"></span>
						</div>

						<div class="col-md-4">
							<label for="" style="font-size: 13px;font-weight: inherit;">
								Unidad:
							</label>
			                <br>
			                <select class="form-control" name="unidad_cliente" id="unidad_cliente">
								<option value="Hora(s)">Hora(s)</option>
								<option value="Día(s)">Día(s)</option>
							</select>
			                <br><span class="error" id ="error_unidad_cliente"></span>
						</div>
					</div>
				</div>	
			</div>

			<br>
			<div class="row">
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo Dist.:</label>
	                <br>
	                <input type='number' step='any' min='0' name="costoDist" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?php echo set_value('costoDist');?>">
	                <br><span class="error" id ="error_costoDist"></span>
				</div>
				<div class="col-md-3">
					<label for="" style="font-size: 13px;font-weight: inherit;">Costo Total:</label>
	                <br>
	                <input type='number' step='any' min='0' name="costoTotal" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="form-control" value="<?php echo set_value('costoTotal');?>">
	                <br><span class="error" id ="error_costoTotal"></span>
				</div>
				<div class="col-md-6">
					<label for="" style="font-size: 13px;font-weight: inherit;">Observaciones sobre la pieza:</label>
	                <br>
	                <textarea  class="form-control" name="observacionesPza"><?php echo set_value('observacionesPza');?></textarea>
	                <br><span class="error" id ="error_observacionesPza"></span>
				</div>	
			</div>
		</div>

		<br><br>
        <div class="col-sm-12" align="center">
            <br>
            <i class="fas fa-spinner cargaIcono" id="carga_envio"></i>
            <span class="error" id="errorEnvio"></span>
            <br>

            <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="1">
            <input type="hidden" name="" id="formulario" value="garantias">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="registro" id="registro" value="<?php if(isset($idRegistro)) echo $idRegistro; else echo '0'; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."HG_Lista/4";?>';" style="margin-right: 20px;">
                Regresar
            </button>

            <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="guardar_formulario">Guardar Formulario</button>
        </div>

        <br>
	</form>	
</section>

<!-- Modal para filtrar las refacciones-->
<div class="modal fade" id="refacciones" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Buscador de Refacciones</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <br>
                        <input type='radio' id='' class='input_field' name='filtros' value="clave" checked>
                        Buscar por clave
                        <br><br>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding:0px;border: 0px;">
                                <!-- <i class="fa fa-search"></i> -->
                                <button type="button" class="btn btn-default" id="busquedaClaveBtn"><i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control" id="claveBuscar" placeholder="Buscar..." style="width:100%;height:35px;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <br>
                        <input type='radio' class='input_field' name='filtros' value="descripcion">
                        Buscar por descripción
                        <br><br>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding:0px;border: 0px;">
                                <!-- <i class="fa fa-search"></i> -->
                                <button type="button" class="btn btn-default" id="busquedaDescripBtn" ><i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control" id="descipBuscar" placeholder="Buscar..." style='width:100%;'>
                        </div>
                    </div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <br>
                        <label id="alertaConexion"></label>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <label for="">Resultados:</label>
                        <br>
                        <select class="form-control" id="resultadosVaciado" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-2"></div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Clave:</label>
                        <input type='text' class='form-control' id="claveRefaccion" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-6" style="padding-top: 20px;">
                        <input type='text' class='form-control' id="tipoBuscar" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="">Descripcion:</label>
                        <input type='text' class='form-control' id="descripcionRefacciones" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Precio:</label>
                        <input type='text' class='form-control' id="precioRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Existencia:</label>
                        <input type='text' class='form-control' id="existenciaRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Unidad:</label>
                        <input type='text' class='form-control' id="unidadRefacciones" style='width:100%;' disabled>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="indiceOperacion" value="">
                <input type="hidden" name="" id="existeRefacciones" value="">
                <!-- <input type="hidden" name="" id="base_ajax" value="<?php echo base_url(); ?>"> -->
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarOperacion">Cerrar</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="bajaOperacion">Aceptar</button>
            </div> 
        </div>
    </div>
</div>