<div >
    <br><br>
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <br>
    <div class="panel-body">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-8">
                    <h3 align="center">Registros Formulario 1 - Queretaro</h3>
                </div>
                <div class="col-sm-4" align="right">
                    <input type="button" class="btn btn-success" style="color:white; margin-top:20px;" onclick="location.href='<?=base_url()."Formulario_Qro_A/0";?>';" value="Nuevo Registro">
                </div>
            </div>

            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-sm-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;">
                                    <td align="center" style="width: 5%;">#</td>
                                    <td align="center" style="width: 10%;"><strong>No ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;"><strong>TELEFONO</strong></td>
                                    <td align="center" style="width: 10%;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>FECHA CITA</strong></td>
                                    <td align="center" style="width: 10%;"><strong>REQUIERE TAXI</strong></td>
                                    <td align="center" style="width: 10%;"><strong>CONFIRMA CITA</strong></td>
                                    <td align="center" colspan="1"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($txtidOrden)): ?>
                                    <?php foreach ($txtidOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="width:10%;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $txtidOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $txtNombre[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $txtTelefono[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $txtPlaca[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $txtAsesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $txtFecha_Cita[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $txtReq_Taxi[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $txtConfirma_Cita[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <a href="<?=base_url()."Formulario_Qro_E/".$txtidOrden[$index];?>" class="btn btn-primary">EDITAR</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="10" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
