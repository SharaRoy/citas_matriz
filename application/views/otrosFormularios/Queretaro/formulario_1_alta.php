<div class="row" style="background-color:white;">
		<div class="col-md-12" align="center" style="">
				<img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:280px;height:70px;">
		</div>
</div>
<br><br><br><br><br><br>
<section class="container">
		<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
						<div class="panel panel-primary">
								<div class="panel-heading" align="center">
										<h3 class="panel-title">Formulario Alta (Prueba - Queretaro)</h3>
								</div>

								<!-- Alerta para proceso del registro -->
				        <div class="alert alert-danger" align="center" style="display:none;">
				            <strong style="font-size:16px !important;">Fallo al autentificar usuario.</strong>
				        </div>
				        <div class="alert alert-warning" align="center" style="display:none;">
				            <strong style="font-size:16px !important;">Campos incorrectos.</strong>
				        </div>

								<div class="panel-body">
										<form name="" method="post" action="<?php echo base_url()."formularios/Queretaro/Formularios_1/validateForm";?>">
												<h4>Datos cita:</h4>
												<hr>

                        <div class="row">
                            <div class="col-sm-4">
                                <label for="">No. Orden:</label>&nbsp;<br>
                                <input type="text" class="form-control" onblur="validarFormulario(1)" id="idOrdenFormulario" name="idOrden" value="<?= set_value('idOrden');?>" style="width:100%;">
                                <?php echo form_error('idOrden', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Fecha de la cita:</label>&nbsp;<br>
                                <input type="date" class="form-control" max="<?php echo $hoy ?>" id="txtFecha" name="txtFecha" value="<?php if(set_value('txtFecha') != "") echo set_value('txtFecha'); else echo $hoy;?>" style="width:100%;padding-top: 0px;">
                                <?php echo form_error('txtFecha', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Hora de la cita:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtHoraCita" name="txtHoraCita" value="<?php if(set_value('txtHoraCita') != "") echo set_value('txtHoraCita'); else echo $hora;?>" style="width:100%;">
                                <?php echo form_error('txtHoraCita', '<span class="error">', '</span>'); ?>
                            </div>
                        </div>

                        <h4>Datos cliente</h4>
												<hr>

                        <div class="row">
                            <div class="col-sm-4">
                                <label for="">Nombre:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtCliente" name="txtCliente" value="<?= set_value('txtCliente');?>" style="width:100%;">
                                <?php echo form_error('txtCliente', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Apellido Paterno:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtApe1" name="txtApe1" value="<?= set_value('txtApe1');?>" style="width:100%;">
                                <?php echo form_error('txtApe1', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Apellido Materno:</label>&nbsp;&nbsp;&nbsp;
																<input type="text" class="form-control" id="txtApe2" name="txtApe2" value="<?= set_value('txtApe2');?>" style="width:100%;">
                                <?php echo form_error('txtApe2', '<span class="error">', '</span>'); ?>
                            </div>
                        </div>

												<br>
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="">Teléfono:</label>&nbsp;<br>
                                <input type="text" class="form-control" maxlength="16" id="txtTelefono" name="txtTelefono" value="<?= set_value('txtTelefono');?>" style="width:100%;">
                                <?php echo form_error('txtTelefono', '<span class="error">', '</span>'); ?>
                            </div>
														<div class="col-sm-4">
																<label for="">Correo:</label>&nbsp;<br>
																<input type="text" class="form-control" id="txtCorreo" name="txtCorreo" value="<?= set_value('txtCorreo');?>" style="width:100%;">
																<?php echo form_error('txtCorreo', '<span class="error">', '</span>'); ?>
														</div>
                            <div class="col-sm-4">
                                <br>
                                <label for="">Requiere Taxi:</label>&nbsp;&nbsp;&nbsp;
                                <input type="checkbox" value="SI" name="txtTaxi" <?php echo set_checkbox('txtTaxi', 'SI'); ?>>
                            </div>
                        </div>

												<h4>Datos Vehículo</h4>
												<hr>

                        <div class="row">
                            <div class="col-sm-4">
                                <label for="">Placas:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtPlacas" max="9" name="txtPlacas" value="<?= set_value('txtPlacas');?>" style="width:100%;">
                                <?php echo form_error('txtPlacas', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Año de la unidad:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtAnio" max="4" name="txtAnio" value="<?= set_value('txtAnio');?>" style="width:100%;">
                                <?php echo form_error('txtAnio', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Unidad:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtUen" name="txtUen" value="<?= set_value('txtUen');?>" style="width:100%;">
                                <?php echo form_error('txtUen', '<span class="error">', '</span>'); ?>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-sm-4">
                                <label for="">Servicio:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtServicio" name="txtServicio" value="<?= set_value('txtServicio');?>" style="width:100%;">
                                <?php echo form_error('txtServicio', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Tipo de servicio:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtTipoServicio" name="txtTipoServicio" value="<?= set_value('txtTipoServicio');?>" style="width:100%;">
                                <?php echo form_error('txtTipoServicio', '<span class="error">', '</span>'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label for="">Asesor:</label>&nbsp;<br>
                                <input type="text" class="form-control" id="txtAsesor" name="txtAsesor" value="<?= set_value('txtAsesor');?>" style="width:100%;">
                                <?php echo form_error('txtAsesor', '<span class="error">', '</span>'); ?>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-sm-12" align="center">
                                <br>
                                <strong><label for="" style="font-size:14px;">Confirmo cita:</label>&nbsp;&nbsp;&nbsp;</strong>
                                <input type="checkbox" value="SI" name="txtConfirma" <?php echo set_checkbox('txtConfirma', 'SI'); ?>>
                            </div>
                        </div>

                        <br><br>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4" align="center">
																<input type="hidden" name="tipoVista" id="tipoVista" value="1">
                                <input type="hidden" name="" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                                <input type="submit" name="ingreso" class="btn btn-success" value="Guardar">
                            </div>
                            <div class="col-sm-4"></div>
                        </div>
										</form>
								</div>
						</div>
				</div>
				<div class="col-sm-1"></div>
		</div>
</section>
