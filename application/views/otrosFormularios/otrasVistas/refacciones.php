<div style="margin:20px;">
    <div class="row">
        <!--<div class="col-md-1" align="center"></div>-->
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-4">
                    <br><br><br>
                    <h4 style="text-align: center;">Refacciones y Mano de Obra</h4>
                </div>
                <div class="col-md-4">
                    <br>
                    <input type='radio' id='' class='input_field' name='filtros' value="clave" checked>
                    Buscar por clave
                    <br><br>
                    <div class="input-group">
                        <div class="input-group-addon" style="padding:0px;border: 0px;">
                            <button type="button" class="btn btn-default" id="busquedaClaveBtn"><i class="fa fa-search"></i></button>
                        </div>
                        <input type="text" class="form-control" id="claveBuscar" placeholder="Buscar..." style="width:100%;">
                    </div>
                </div>
                <div class="col-md-4">
                    <br>
                    <input type='radio' class='input_field' name='filtros' value="descripcion">
                    Buscar por descripción
                    <br><br>
                    <div class="input-group">
                        <div class="input-group-addon" style="padding:0px;border: 0px;">
                            <button type="button" class="btn btn-default" id="busquedaDescripBtn" ><i class="fa fa-search"></i></button>
                        </div>
                        <input type="text" class="form-control" id="descipBuscar" placeholder="Buscar..." style='width:100%;'>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-1" align="center"></div> -->
    </div>

    <div class="row">
        <div class="col-md-12" align="center">
            <i class="fas fa-spinner cargaIcono"></i>
            <label id="alertaConexion"></label>
            <table class="table table-bordered table-responsive" style="width: auto;overflow-y: scroll;overflow-x: scroll;">
                <thead>
                    <tr style="font-size:14px;background-color: #ddd;">
                        <td align="center" style="width: 10%;"><strong>#</strong></td>
                        <td align="center" style="width: 20%;"><strong>TIPO</strong></td>
                        <td align="center" style="width: 20%;"><strong>ARTICULO</strong></td>
                        <td align="center" style="width: 30%;"><strong>DESCRIPCIÓN</strong></td>
                        <td align="center" style="width: 15%;"><strong>PRECIO</strong></td>
                        <td align="center" style="width: 15%;"><strong>EXISTENCIA</strong></td>
                    </tr>
                </thead>
                <tbody id="tabla_refacciones">
                    <?php if (isset($tipo)): ?>
                        <?php foreach ($tipo as $index => $valor): ?>
                            <tr>
                                <td><?php echo $index+1; ?></td>
                                <td><?php echo $tipo[$index]; ?></td>
                                <td><?php echo $articulo[$index]; ?></td>
                                <td><?php echo $descripcion[$index]; ?></td>
                                <td><?php echo $precio[$index]; ?></td>
                                <td><?php echo $existencia[$index]; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
