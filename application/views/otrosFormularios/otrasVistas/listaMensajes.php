<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-8">
                    <h3 align="right">Ordenes enviadas sin exito a Intelisis</h3>
                </div>
            </div>
            
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-3">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <!--<div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>-->
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_fechas" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <!--<i class="fa fa-trash"></i>-->
                                Limpiar Busqueda
                            </button>
                        </div>
                        <!-- formulario de busqueda -->
                        <!--<div class="col-md-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!- - /.formulario de busqueda -->
                    </div>

                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width: 100%;overflow-y: scroll;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <td align="center" style="width: 5%;"></td>
                                    <td align="center" style="width: 8%;"><strong>ID ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;"><strong>FECHA RECEPCIÓN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>FECHA PROMESA</strong></td>
                                    <td align="center" style="width: 10%;"><strong>MENSAJE DE ERROR</strong></td>
                                    <td align="center" style=""><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="width:5%;vertical-align: middle;">
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:8%;vertical-align: middle;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $cliente[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $fechaAlta[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $fechaPromesa[$index]; ?>
                                            </td>
                                            <td align="center" style="width:25%;vertical-align: middle;">
                                                <label class="error"><?php echo $mensaje[$index]; ?></label> 
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <a class="btn btn-warning renvioOrden" data-orden="<?php echo $idOrden[$index]; ?>" style="color:white;">
                                                    Reenviar Orden
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="8" style="width:100%;" align="center">
                                            <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                            <h4>Sin registros que mostrar</h4>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
