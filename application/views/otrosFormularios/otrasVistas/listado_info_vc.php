<div style="margin: 20px;">
    <br>
    <div class="panel panel-default panel-flotante">
        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-sm-2" align="left" style="padding-top: 20px;">
                </div>
                <div class="col-sm-8">
                    <h3 align="right">AUDIOS VOZ CLIENTE</h3>
                </div>
                <div class="col-sm-2" align="right" style="padding-top: 20px;">
                    <button class="btn btn-success" onclick="carga_datos()">Iniciar</button>
                </div>
            </div>
            
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <br>
                    <div class="form-group table-responsive" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <h5 style="color:red;text-align: center;" id="error_act_doc"></h5>
                        
                        <table <?= (($listado != NULL) ? 'id="bootstrap-data-table"' : '') ?> class="table table-striped table-bordered" style="width: 100%;">
                            <thead>
                                <tr style="font-size:12px;background-color: #eee;">
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>NO. ORDEN</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>APERTURA ORDEN</strong></td>
                                    <!--<td align="center" style="width: 10%;vertical-align:middle;"><strong>TÉCNICO</strong></td>-->
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;"><strong>FECHA DEL AUDIO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align:middle;">
                                        <strong>DURACIÓN SEG</strong>
                                        <input type="hidden" id="limite" value="<?= (($listado != NULL) ? count($listado) : 0) ?>">
                                    </td>
                                    <td align="center" style="width: 8%;vertical-align:middle;">
                                        <strong>DURACIÓN MIN</strong>
                                    </td>
                                    <td>
                                        <br>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($listado != NULL): ?>
                                    <?php foreach ($listado as $i => $registro): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="vertical-align: middle;">
                                                <?= $registro->id_cita; ?>
                                                <?php 
                                                    if ($registro->evidencia != NULL) {
                                                        $audio = $registro->evidencia;
                                                        $tipo = 2;
                                                    }else{
                                                        $audio = $registro->audio;
                                                        $tipo = 1;
                                                    }
                                                ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php
                                                    $fecha = new DateTime($registro->fecha_recepcion.' '.$registro->hora_recepcion);
                                                    echo $fecha->format('d-m-Y H:i');
                                                 ?>
                                            </td>
                                            <!--<td align="center" style="vertical-align: middle;">
                                                <?php 
                                                    if ($registro->tecnico != NULL) {
                                                        echo $registro->tecnico;
                                                    }else{
                                                        if ($tipo == 1) {
                                                            echo $registro->tecnico;
                                                        }else{
                                                            echo $registro->tecnico;
                                                        }
                                                    }
                                                 ?>
                                            </td>-->
                                            <td align="center" style="vertical-align: middle;">
                                                <!--<?= $registro->asesor ?>-->
                                                <?php 
                                                    if ($registro->asesor != NULL) {
                                                        echo $registro->asesor;
                                                    }else{
                                                        if ($tipo == 1) {
                                                            echo $registro->nombre_asesor;
                                                        }else{
                                                            echo $registro->nombreAsesor;
                                                        }
                                                    }
                                                 ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <!--<?= $registro->cliente ?>-->
                                                <?php 
                                                    if ($registro->cliente != NULL) {
                                                        echo $registro->cliente;
                                                    }else{
                                                        if ($tipo == 1) {
                                                            echo $registro->nombre_cliente;
                                                        }else{
                                                            echo $registro->nombreCliente;
                                                        }
                                                    }
                                                 ?>
                                            </td>
                                            <td align="center" style="vertical-align: middle;">
                                                <?php
                                                    $reg = (($tipo == 2) ? $registro->registro_voz_cliente_old : $registro->registro_voz_cliente_new);
                                                    $fecha_2 = new DateTime($reg);
                                                    echo $fecha_2->format('d-m-Y H:i');
                                                 ?>
                                            </td>

                                            <td align="center" style="vertical-align: middle;">
                                                <?php 
                                                    if (file_exists($audio)) {
                                                        $audio_url = base_url().$audio;
                                                    }else{
                                                        $audio_url = $base.''.$audio;
                                                    }
                                                ?>
                                                <input type="hidden" id="url_mp3_<?= $i ?>" value="<?= $audio_url ?>">
                                                <label id="label_rp_<?= $i ?>"></label>
                                            </td>

                                            <td align="center" style="vertical-align: middle;">
                                                <label id="label_rp_min_<?= $i ?>"></label>
                                            </td>

                                            <td align="center" style="vertical-align: middle;">
                                                <audio preload="preload" id="reproductor_dl_<?= $i ?>" controls>
                                                    <source src="<?= $audio_url ?>" type="audio/ogg">
                                                  
                                                    Your browser does not support the audio element.
                                                </audio>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="8" style="width:100%;" align="center">
                                            <h4>Sin registros que mostrar</h4>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>


<script>
    function carga_datos(){
        var renglones = $("#limite").val();

        for (var i = 0; i < renglones; i++) {
            var url = $("#url_mp3_"+i).val();
            //$("#reproductor_dl").attr("src",url);

            var seg = document.getElementById("reproductor_dl_"+i).duration;
            //var ajuste = ((seg > 70) ? seg-1.8 : seg);
            //var min = Math.floor((seg / 60) % 60)  //seg/60;
            var hour = Math.floor(seg / 3600);
            hour = (hour < 10)? '0' + hour : hour;
            var minute = Math.floor((seg / 60) % 60);
            minute = (minute < 10)? '0' + minute : minute;
            var second = seg % 60;
            if (second % 1 != 0) {
                if (second < 59) {
                    second = Math.round(second);
                }else{
                    second = Math.trunc(second);
                }
                
            } 

            second = (second < 10)? '0' + second : second;
            //return hour + ':' + minute + ':' + second;

            //console.log(seg);
            //console.log(min);
            //console.log((min-0.32).toFixed(2));
            document.getElementById("label_rp_"+i).innerHTML = seg.toFixed(2);
            document.getElementById("label_rp_min_"+i).innerHTML = minute + ':' + second; 
        }
    }

    /*
    function secondsToString(seconds) {
  var hour = Math.floor(seconds / 3600);
  hour = (hour < 10)? '0' + hour : hour;
  var minute = Math.floor((seconds / 60) % 60);
  minute = (minute < 10)? '0' + minute : minute;
  var second = seconds % 60;
  second = (second < 10)? '0' + second : second;
  return hour + ':' + minute + ':' + second;
}

     */
</script>