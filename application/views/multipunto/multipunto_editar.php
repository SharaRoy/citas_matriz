<div style="margin: 30px;" >
    <form name="" id="formulario" method="post" action="<?=base_url()."multipunto/Multipunto/validateView"?>" autocomplete="on" enctype="multipart/form-data">
        <!-- Primera hoja de formulario -->
        <div class="row">
            <div class="col-sm-5">
                <h3 class="h2_title_blue">
                    HOJA MULTIPUNTOS
                    <a id="superUsuarioA" class="h2_title_blue" data-target="#superUsuario" data-toggle="modal" style="color:blue;margin-top:15px;cursor: pointer;">
                        <i class="fas fa-user"></i>
                    </a>
                </h3>
            </div>
            <div class="col-sm-7" align="right">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" style="max-width:6cm;margin-top:10px;">
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <!-- Alerta para proceso del registro -->
                <div class="alert alert-success" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="alert alert-warning" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Campos incompletos.</strong>
                </div>
                <div class="panel panel-primary">
                    <div class="vertical_asesor">
                        USO EXCLUSIVO ASESOR
                    </div>
                    <div class="vertical_tecnico_1">
                        USO EXCLUSIVO TÉCNICO
                    </div>
                    <div class="panel-heading" style="text-align:center;">
                        INSPECCIÓN DE VEHÍCULO
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-5" align="right"></div>
                            <div class="col-sm-5" align="right">
                                <?php if (isset($tipoServicio)): ?>
                                    <?php if (($tipoServicio == "1")&&($dirige == "TEC")): ?>
                                        <!--<button type="button" class="btn btn-success" id="btns-aprobado" <?php if(isset($firmaTecnico)) if($firmaTecnico != "") echo "disabled"; else echo ""; else echo ""; ?>>
                                            Autocarga
                                        </button>-->
                                        <!-- <button type="button" class="btn btn-warning" id="btns-futuro">
                                            Autocarga
                                        </button>
                                        <button type="button" class="btn btn-danger" id="btns-inmediato">
                                            Autocarga
                                        </button> -->
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-2" align="right">
                                <?php if (isset($dirige)): ?>
                                    <?php if ($dirige == "ASE"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($dirige == "TEC"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($dirige == "JDT"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                            Regresar
                                        </button>
                                    <?php else: ?>
                                        <!--<button type="button" class="btn btn-dark" >
                                            Sesión Expirada
                                        </button>-->
                                    <?php endif; ?>
                                <?php else: ?>
                                    <!--<button type="button" class="btn btn-dark" >
                                        Sesión Expirada
                                    </button>-->
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <br><br>
                        <div class="row" style="padding-left: 13px;padding-right: 13px;">

                            <!-- Lado izquierdo de la pantalla -->
                            <div class="col-md-6">
                                <!-- panel 1 -->
                                <div class="panel panel-default">
                                    <div class="panel-body" style="border:2px solid black;">
                                        <div class="form-group" style="width:100%;">
                                            <div class="row" style="width:100%;">
                                                <div class="col-sm-6">
                                                  <label for="">Fecha:</label>
                                                  <input type="date" class="input_field seccionAsesor" id="start" name="fechaCaptura" value="<?php if(set_value('fechaCaptura') != "") { echo set_value('fechaCaptura');} else {if(isset($presionPSI)) echo $fechaCaptura; else echo $hoy; } ?>" min="<?php echo $hoy; ?>" max="<?php echo $hoy; ?>">
                                                  <br>
                                                  <?php echo form_error('fechaCaptura', '<span class="error">', '</span>'); ?>
                                                </div>
                                                <div class="col-sm-6" align="left">
                                                    <label for="">OR:</label>
                                                    <input class="input_field" type="text" name="orden" id="multipuntoOrden" onblur="multipuntoConsulta()" value="<?php if(set_value("orden") != "") echo set_value("orden"); else {if(isset($idCita)){ if($idCita != "0") echo $idCita; else echo "";}} ?>" >
                                                    <?php echo form_error('orden', '<br><span class="error">', '</span>'); ?>
                                                    <label for="" class="error" id="errorConsulta"></label>
                                                    <label for="" id="errorConsultaServer" class="error"></label>

                                                    <br>
                                                    Folio Intelisis:&nbsp;
                                                    <label style="color:darkblue;">
                                                        <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:3px;">
                                                <div class="col-md-12">
                                                    <label for="">No.Serie (VIN):</label>
                                                    <input class="input_field seccionAsesor" type="text" id="noSerie" name="noSerie" style="width:70%" value="<?php if(set_value('noSerie') != "") { echo set_value('noSerie');} else { if(isset($noSerie)) echo $noSerie;}?>">
                                                    <br>
                                                    <?php echo form_error('noSerie', '<span class="error">', '</span>'); ?>
                                                    <br>
                                                    <strong><label for="" id="nombreCamapanias"><?php if(isset($campania)) {if($campania != "[]") echo "Campaña: ".$campania; else  echo "Campaña no registrada"; } else  echo "Campaña no registrada"; ?></label></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel 1 -->

                                <!-- panel 2 -->
                                <div class="panel panel-default">
                                    <div class="panel-body" style="border:2px solid black;">
                                        <table class="table-responsive" style="width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label for="">Modelo:</label>&nbsp;
                                                        <input class="input_field seccionAsesor" type="text" id="modelo" name="modelo" ng-model="model" style="width: 60%;" value="<?php if(isset($modelo)) echo $modelo;?>">
                                                        <br>
                                                        <?php echo form_error('modelo', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <label for="">No. de torre:</label>&nbsp;
                                                        <input class="input_field seccionAsesor" type="text" name="torre" id="torre" style="width: 40%;" value="<?php if(isset($torre)) echo $torre;?>">
                                                        <br>
                                                        <?php echo form_error('torre', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <label for="">Nombre:</label>&nbsp;
                                                        <input class="input_field seccionAsesor" type="text" name="nombreCarro" id="nombreCarro" style="width: 60%;" value="<?php if(isset($nombreDueno)) echo $nombreDueno;?>">
                                                        <br>
                                                        <?php echo form_error('nombreCarro', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <label for="">Correo electrónico:</label>&nbsp;
                                                        <input class="input_field seccionAsesor" type="text" name="email" id="email" style="width: 60%;" value="<?php if(isset($email)) echo $email;?>">
                                                        <br>
                                                        <label for="">Correo compañía:</label>&nbsp;
                                                        <input class="input_field" type="text" name="emailComp" id="correoCompaniaLabel" style="width: 60%;" value="<?php if(isset($emailComp)) echo $emailComp;?>">
                                                        <br>
                                                        <?php echo form_error('email', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- panel 2 -->

                                <!-- panel 3 -->
                                <div class="panel panel-default">
                                    <div class="panel-body" style="border:2px solid black">
                                        <div class="table-responsive">
                                            <table class="" style="width:100%" >
                                                <tr class="headers_table">
                                                    <td style="text-align: center;background-color:#340f7b; color:white;width:80%">NIVELES DE FLUIDOS</td>
                                                    <td style="width:20%;text-align: center;">CAMBIADO</td>
                                                </tr>
                                                <tr>
                                                    <td  style="text-align: left;width:80%">
                                                        <label for="">&nbsp;Perdida de fluido y/o aceite &nbsp;&nbsp;</label>
                                                        Si&nbsp;<input type="radio" style="transform: scale(1.5);"class="seccionAsesor" value="Si" name="perdidaAceite" id="perdidaAceite_1" <?php if(isset($perdidaAceite)) if(strtoupper($perdidaAceite) == "SI") echo "checked";?>>
                                                        No&nbsp;<input type="radio" style="transform: scale(1.5);"class="seccionAsesor" value="No" name="perdidaAceite" id="perdidaAceite_2" <?php if(isset($perdidaAceite)) if(strtoupper($perdidaAceite) == "NO") echo "checked";?>>
                                                        <?php echo form_error('perdidaAceite', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td style="width:20%;text-align: center;">
                                                        <input type="checkbox" style="transform: scale(1.5);"  class="seccionAsesor" value="1" name="fuidoCambios" id="fuidoCambios" <?php if(isset($cambioAceite)) if($cambioAceite == "1") echo "checked";?>>
                                                        <br>
                                                        <?php echo form_error('fuidoCambios', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="" style="width:100%;border-top: 1px solid black;" >
                                                <tr>
                                                    <td style="padding:2px;">Bien</td>
                                                    <td>Llenar</td>
                                                    <td></td>
                                                    <td>Bien</td>
                                                    <td>Llenar</td>
                                                    <td></td>
                                                    <td>Bien</td>
                                                    <td>Llenar</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="aceiteCheck" id="aceiteCheck_1" value="Bien" <?php if(isset($aceiteMotor)) if($aceiteMotor == "Bien") echo "checked";?> <?php echo set_checkbox('aceiteCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor"  name="aceiteCheck" id="aceiteCheck_2" value="Llena" <?php if(isset($aceiteMotor)) if($aceiteMotor == "Llena") echo "checked";?> <?php echo set_checkbox('aceiteCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Aceite de motor
                                                        <?php echo form_error('aceiteCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="direccionCheck" id="direccionChech_1" value="Bien" <?php if(isset($direccionHidraulica)) if($direccionHidraulica == "Bien") echo "checked";?> <?php echo set_checkbox('direccionCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="direccionCheck" id="direccionChech_2" value="Llena" <?php if(isset($direccionHidraulica)) if($direccionHidraulica == "Llena") echo "checked";?> <?php echo set_checkbox('direccionCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Dirección hidráulica
                                                        <?php echo form_error('direccionCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="transmisionCheck" id="transmisionCheck_1" value="Bien" <?php if(isset($transmision)) if($transmision == "Bien") echo "checked";?> <?php echo set_checkbox('transmisionCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="transmisionCheck" id="transmisionCheck_2" value="Llena" <?php if(isset($transmision)) if($transmision == "Llena") echo "checked";?> <?php echo set_checkbox('transmisionCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Transmisión<br>(si está equipada con<br> bayoneta de medición)
                                                        <?php echo form_error('transmisionCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="liquidoFrenosCheck" id="liquidoFrenosCheck_1" value="Bien" <?php if(isset($fluidoFrenos)) if($fluidoFrenos == "Bien") echo "checked";?> <?php echo set_checkbox('liquidoFrenosCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="liquidoFrenosCheck" id="liquidoFrenosCheck_2" value="Llena" <?php if(isset($fluidoFrenos)) if($fluidoFrenos == "Llena") echo "checked";?> <?php echo set_checkbox('liquidoFrenosCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Deposito fluido frenos
                                                        <?php echo form_error('liquidoFrenosCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="liquidoParabriCheck" id="liquidoParabriCheck_1" value="Bien" <?php if(isset($limpiaparabrisas)) if($limpiaparabrisas == "Bien") echo "checked";?> <?php echo set_checkbox('liquidoParabriCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="liquidoParabriCheck" id="liquidoParabriCheck_2" value="Llena" <?php if(isset($limpiaparabrisas)) if($limpiaparabrisas == "Llena") echo "checked";?> <?php echo set_checkbox('liquidoParabriCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Limpiapa-<br>rabrisas
                                                        <?php echo form_error('liquidoParabriCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="refrigeranteCheck" id="refrigeranteCheck_1" value="Bien" <?php if(isset($refrigerante)) if($refrigerante == "Bien") echo "checked";?> <?php echo set_checkbox('refrigeranteCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="refrigeranteCheck" id="refrigeranteCheck_2" value="Llena" <?php if(isset($refrigerante)) if($refrigerante == "Llena") echo "checked";?> <?php echo set_checkbox('refrigeranteCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Deposito recuperación refrigerante
                                                        <?php echo form_error('refrigeranteCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                            </table>

                                            <br>
                                            <table style="width:100%">
                                                <tr class="headers_table">
                                                    <td colspan="2" style="text-align: center;background-color:#340f7b; color:white;" >PLUMAS LIMPIAPARABRISAS</td>
                                                    <td style="width:20%;text-align: center;">CAMBIADO</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left;">
                                                        <label for="">Prueba de Limpiaparabrisas <br>realizada&nbsp;</label><br>
                                                        Si &nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="pruebaparabrisas" id="pruebaparabrisas_1" value="Si" <?php if(isset($plumasPruebas)) if($plumasPruebas == "Si") echo "checked";?> <?php echo set_checkbox('pruebaparabrisas', 'Si'); ?>>
                                                        No &nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="pruebaparabrisas" id="pruebaparabrisas_2" value="No" <?php if(isset($plumasPruebas)) if($plumasPruebas == "No") echo "checked";?> <?php echo set_checkbox('pruebaparabrisas', 'No'); ?>>
                                                        <br>
                                                        <?php echo form_error('pruebaparabrisas', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divAprobado" style="float:left;"> 
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="plumas" id="plumas_1" value="Si" <?php if(isset($plumas)) if($plumas == "Si") echo "checked";?><?php echo set_checkbox('plumas', 'Si'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;"> 
                                                            <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="plumas" id="plumas_2" value="No" <?php if(isset($plumas)) if($plumas == "No") echo "checked";?><?php echo set_checkbox('plumas', 'No'); ?>>
                                                        </div>
                                                        <label for="" style="float:left;">&nbsp; Plumas <br>Limpiaparabrisas</label>
                                                        <br>
                                                        <?php echo form_error('plumas', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <input type="checkbox" style="transform: scale(1.5);"  value="1" class="seccionAsesor" name="plumasCambio" id="plumasCambio" <?php if(isset($plumasCambio)) if($plumasCambio == "1") echo "checked";?> <?php echo set_checkbox('plumasCambio', '1'); ?>>
                                                        <br>
                                                        <?php echo form_error('plumasCambio', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <!-- SISTEMAS Y COMPONENTES -->
                                                        <div class="" style="text-align:center; background-color: #340f7b ; color:white;">
                                                            SISTEMAS / COMPONENTES
                                                        </div>
                                                        <div class="panel">
                                                            <div class="panel-body">
                                                                <table style="width:100%;">
                                                                    <tr class="headers_table">
                                                                        <td style="text-align: center;background-color:slategrey; color:white;" >LUCES / PARABRISAS</td>
                                                                        <td style="width:20%;text-align: center;">CAMBIADO</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="divAprobado" style="float:left;"> 
                                                                                        <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="luces" id="luces_1" value="Si" <?php if(isset($lucesClaxon)) if($lucesClaxon == "Si") echo "checked";?> <?php echo set_checkbox('luces', 'Si'); ?>>
                                                                                    </div>
                                                                                    <div class="divInmediato" style="float:left;"> 
                                                                                        <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="luces" id="luces_2" value="No" <?php if(isset($lucesClaxon)) if($lucesClaxon == "No") echo "checked";?> <?php echo set_checkbox('luces', 'No'); ?>>
                                                                                    </div>
                                                                                    <br>
                                                                                    <?php echo form_error('luces', '<span class="error">', '</span>'); ?>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    Funcionamiento de claxon, luces interiores, luces exteriores luces de giro, luces de emergencia y freno.
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <input type="checkbox" style="transform: scale(1.5);"  value="1" class="seccionAsesor" name="lucesCambio" id="lucesCambio" <?php if(isset($lucesClaxonCambio)) if($lucesClaxonCambio == "1") echo "checked";?> <?php echo set_checkbox('lucesCambio', '1'); ?>>
                                                                            <br>
                                                                            <?php echo form_error('lucesCambio', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="padding-top:10px;">
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="divAprobado" style="float:left;">
                                                                                        <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="parabrisas" id="parabrisas_1" value="Si" <?php if(isset($grietasParabrisas)) if($grietasParabrisas == "Si") echo "checked";?> <?php echo set_checkbox('parabrisas', 'Si'); ?>>
                                                                                    </div>
                                                                                    <div class="divInmediato" style="float:left;"> 
                                                                                        <input type="radio" style="transform: scale(1.5);"class="seccionAsesor" name="parabrisas" id="parabrisas_2" value="No" <?php if(isset($grietasParabrisas)) if($grietasParabrisas == "No") echo "checked";?> <?php echo set_checkbox('parabrisas', 'No'); ?>>
                                                                                    </div>
                                                                                    <br>
                                                                                    <?php echo form_error('parabrisas', '<span class="error">', '</span>'); ?>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    Grietas, roturas y picaduras de parabrisas.
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <input type="checkbox" style="transform: scale(1.5);"  value="1" name="parabrisasCambio" id="parabrisasCambio" class="seccionAsesor" <?php if(isset($grietasCambio)) if($grietasCambio == "1") echo "checked";?> <?php echo set_checkbox('parabrisasCambio', '1'); ?>>
                                                                            <br>
                                                                            <?php echo form_error('parabrisasCambio', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="headers_table">
                                                                        <td style="width:70%; text-align: center;background-color:#340f7b; color:white;">BATERÍA</td>
                                                                        <td style="width:30%;text-align: center;">CAMBIADO</td>
                                                                    </tr>
                                                                    <tr style="margin-top:20px;">
                                                                        <td rowspan="2">
                                                                            <br><br>
                                                                            <img src="<?php echo base_url().'/assets/imgs/nivel_bateria.png' ?>" alt="" style="height: 50px;width:290px;">
                                                                        </td>
                                                                        <td align="center">
                                                                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionAsesor" name="bateriaCambio" id="bateriaCambio" value="1" <?php if(isset($bateriaCambio)) if($bateriaCambio == "1") echo "checked";?>  <?php echo set_checkbox('bateriaCambio', '1'); ?>>
                                                                            <br>
                                                                            <?php echo form_error('bateriaCambio', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Estado de batería</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:290px;">
                                                                            <div id="slider-2" style="width:290px;"></div>
                                                                            <label for="" id="labelNB">
                                                                                <?php if(set_value('valorBateria') != "") {echo set_value('valorBateria');}else { if(isset($bateriaValor))  echo $bateriaValor; else echo "0"; } ?>%</label>
                                                                            <input type="hidden" name="valorBateria" id="valorBateria" value="<?php if(set_value('valorBateria') != "") {echo set_value('valorBateria');}else { if(isset($bateriaValor))  echo $bateriaValor; else echo "0"; } ?>">
                                                                            <br>
                                                                            <?php echo form_error('valorBateria', '<span class="error">', '</span>'); ?> 
                                                                        </td>
                                                                        <td style="margin-left:10px;">
                                                                            <div class="divAprobado" style="float:left;">
                                                                                <input type="radio" style="transform: scale(1.5);"name="bateriaRadio" id="bateriaRadio_1" value="Aprobado" class="seccionTecnico seccionAsesor" <?php if(isset($bateriaEstado)) if($bateriaEstado == "Aprobado") echo "checked";?> <?php echo set_checkbox('bateriaRadio', 'Aprobado'); ?>>
                                                                            </div>
                                                                            <div class="divFuturo" style="float:left;">
                                                                                <input type="radio" style="transform: scale(1.5);"name="bateriaRadio" id="bateriaRadio_2" value="Futuro" class="seccionTecnico seccionAsesor" <?php if(isset($bateriaEstado)) if($bateriaEstado == "Futuro") echo "checked";?> <?php echo set_checkbox('bateriaRadio', 'Futuro'); ?>>
                                                                            </div>
                                                                            <div class="divInmediato" style="float:left;"> 
                                                                                <input type="radio" style="transform: scale(1.5);"name="bateriaRadio" id="bateriaRadio_3" value="Inmediato" class="seccionTecnico seccionAsesor" <?php if(isset($bateriaEstado)) if($bateriaEstado == "Inmediato") echo "checked";?> <?php echo set_checkbox('bateriaRadio', 'Inmediato'); ?>>
                                                                            </div>
                                                                            <br>
                                                                            <?php echo form_error('bateriaRadio', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <!-- Corriente de arranque -->
                                                                <table>
                                                                    <tr>
                                                                        <td>Corriente de arranque en frío especificaciones de fabrica</td>
                                                                        <td align="center">
                                                                            <input type="text" class="input_field seccionAsesor" name="ccafabrica" id="ccafabrica" style="width:50px;" value="<?php if(set_value('ccafabrica') != "") {echo set_value('ccafabrica');} else { if(isset($frio)) echo $frio; else echo '';} ?>">
                                                                            <label>CCA</label>
                                                                            <br>
                                                                            <?php echo form_error('ccafabrica', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                        <td>Corriente de arranque en frío real</td>
                                                                        <td align="center">
                                                                            <input type="text" class="input_field seccionAsesor" style="width:50px;" name="ccareal" id="ccareal" value="<?php if(set_value('ccareal') != "") {echo set_value('ccareal');} else { if(isset($frioReal)) echo $frioReal; else echo '';} ?>">
                                                                            <label>CCA</label>
                                                                            <br>
                                                                            <?php echo form_error('ccareal', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel 3 -->
                            </div>

                            <!-- Lado derecho de la pantalla -->
                            <div class="col-md-6">
                                <table class="table-responsive">
                                    <tr>
                                        <td colspan="6">
                                            <h4>SÍMBOLO</h4>
                                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">&nbsp;
                                            Puede contribuir a la eficiencia del vehículo y la protección del medio ambiente.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right:3px;">
                                            <!--<img src="<?php echo base_url();?>assets/imgs/square_green.png" width="28px" height="28px">-->
                                            <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #1f661f;"></div>
                                        </td>
                                        <td>&nbsp;Verificado y aprobado</td>
                                        <td style="padding-right:3px;">
                                            <!--<img src="<?php echo base_url();?>assets/imgs/square_yellow.png" width="28px" height="28px">-->
                                            <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #e6b019;"></div>
                                        </td>
                                        <td>&nbsp;Puede requerir atención en el futuro</td>
                                        <td style="padding-right:3px;">
                                            <!--<img src="<?php echo base_url();?>assets/imgs/square_red.png" width="28px" height="28px">-->
                                            <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #e01a14;"></div>
                                        </td>
                                        <td>&nbsp;Requiere atención inmediata</td>
                                    </tr>
                                </table>

                                <table class="table-responsive" style="width:100%;border: 1px solid black;">
                                    <tbody>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">BANDAS / MANGUERAS</td>
                                            <td align="center">CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="sisCalefaccion" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($sistemaCalefaccion)) if($sistemaCalefaccion == "Aprobado") echo "checked"; ?> <?php echo set_checkbox('sisCalefaccion', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="sisCalefaccion" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($sistemaCalefaccion)) if($sistemaCalefaccion == "Futuro") echo "checked"; ?> <?php echo set_checkbox('sisCalefaccion', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="sisCalefaccion" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($sistemaCalefaccion)) if($sistemaCalefaccion == "Inmediato") echo "checked"; ?> <?php echo set_checkbox('sisCalefaccion', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('sisCalefaccion', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Pérdidas y/o daños en el sistema de calefacción, ventilación y aire acondicionado y en mangueras/cables.
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="sisCalefaccionCambio" value="1" <?php if(isset($sistemaCalefaccionCambio)) if($sistemaCalefaccionCambio == "1") echo "checked";?> <?php echo set_checkbox('sisCalefaccionCambio', '1'); ?>>
                                                <br>
                                                <?php echo form_error('sisCalefaccionCambio', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="radiadorRario" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($sisRefrigeracion)) if($sisRefrigeracion == "Aprobado") echo "checked";?> <?php echo set_checkbox('radiadorRario', 'Aprobado'); ?>>
                                                        </div>
                                                        <!--<label class="" style="width:40px;"></label>-->
                                                        <div class="divCheckSC" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);display:none;" class="seccionTecnico BtnAmarillo" name="radiadorRario" value="Futuro" <?php echo set_checkbox('radiadorRario', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="radiadorRario" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($sisRefrigeracion)) if($sisRefrigeracion == "Inmediato") echo "checked";?> <?php echo set_checkbox('radiadorRario', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('radiadorRario', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Sistema de refrigeración del motor, radiador, mangueras y abrazaderas.
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="radiadorCambio" value="1" <?php if(isset($sisRefrigeracionCambio)) if($sisRefrigeracionCambio == "1") echo "checked";?> <?php echo set_checkbox('radiadorCambio', '1'); ?>>
                                                <br>
                                                <?php echo form_error('radiadorCambio', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="bandasRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($bandas)) if($bandas == "Aprobado") echo "checked";?> <?php echo set_checkbox('bandasRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divCheckSC" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);display:none;" class="seccionTecnico BtnAmarillo" name="bandasRadio" value="Futuro" <?php echo set_checkbox('bandasRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="bandasRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($bandas)) if($bandas == "Inmediato") echo "checked";?> <?php echo set_checkbox('bandasRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('bandasRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Banda(s)
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="bandasCambio" value="1" <?php if(isset($bandasCambio)) if($bandasCambio == "1") echo "checked";?> <?php echo set_checkbox('bandasCambio', '1'); ?>>
                                                <br>
                                                <?php echo form_error('bandasCambio', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">SISTEMA DE FRENOS</td>
                                            <td align="center">CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="frenosRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($sisFrenos)) if($sisFrenos == "Aprobado") echo "checked";?> <?php echo set_checkbox('frenosRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="frenosRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($sisFrenos)) if($sisFrenos == "Futuro") echo "checked";?> <?php echo set_checkbox('frenosRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="frenosRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($sisFrenos)) if($sisFrenos == "Inmediato") echo "checked";?> <?php echo set_checkbox('frenosRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('frenosRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Sistema de frenos (incluye mangueras y freno de mano) &nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </div>
                                                </div>
                                           </td>
                                           <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="frenosCambio" value="1" <?php if(isset($sisFrenosCambio)) if($sisFrenosCambio == "1") echo "checked";?> <?php echo set_checkbox('frenosCambio', '1'); ?>>
                                                <br>
                                                <?php echo form_error('frenosCambio', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">DIRECCIÓN Y SUSPENSIÓN</td>
                                            <td align="center">CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="amortiguadoRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($suspension)) if($suspension == "Aprobado") echo "checked";?> <?php echo set_checkbox('amortiguadoRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="amortiguadoRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($suspension)) if($suspension == "Futuro") echo "checked";?> <?php echo set_checkbox('amortiguadoRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="amortiguadoRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($suspension)) if($suspension == "Inmediato") echo "checked";?> <?php echo set_checkbox('amortiguadoRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('amortiguadoRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Pérdidas y/o daños en amortiguadores/puntales y otros
                                                        componentes de la suspensión.
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="amortiguadorCheck" value="1" <?php if(isset($suspensionCambio)) if($suspensionCambio == "1") echo "checked";?> <?php echo set_checkbox('amortiguadorCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('amortiguadorCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="varillajeRadio" <?php if(isset($varillajeDireccion)) if($varillajeDireccion == "Aprobado") echo "checked";?> <?php echo set_checkbox('varillajeRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="varillajeRadio" <?php if(isset($varillajeDireccion)) if($varillajeDireccion == "Futuro") echo "checked";?> <?php echo set_checkbox('varillajeRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="varillajeRadio" <?php if(isset($varillajeDireccion)) if($varillajeDireccion == "Inmediato") echo "checked";?> <?php echo set_checkbox('varillajeRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('varillajeRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Dirección, varillaje de la dirección y juntas de rótula (visual)
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="varillajeCheck" value="1" <?php if(isset($varillajeDireccionCambio)) if($varillajeDireccionCambio == "1") echo "checked";?> <?php echo set_checkbox('varillajeCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('varillajeCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">SISTEMA DE ESCAPE</td>
                                            <td align="center">CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="escapeRadio" <?php if(isset($sisEscape)) if($sisEscape == "Aprobado") echo "checked";?> <?php echo set_checkbox('escapeRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="escapeRadio" <?php if(isset($sisEscape)) if($sisEscape == "Futuro") echo "checked";?> <?php echo set_checkbox('escapeRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="escapeRadio" <?php if(isset($sisEscape)) if($sisEscape == "Inmediato") echo "checked";?> <?php echo set_checkbox('escapeRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('escapeRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Sistema de escape y escudo de calor (pérdidas, daño, piezas sueltas) &nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="escapeCheck" value="1" <?php if(isset($sisEscapeCambio)) if($sisEscapeCambio == "1") echo "checked";?> <?php echo set_checkbox('escapeCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('escapeCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">TREN MOTRIZ</td>
                                            <td>CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="embragueRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($funEmbrague)) if($funEmbrague == "Aprobado") echo "checked";?> <?php echo set_checkbox('embragueRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="embragueRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($funEmbrague)) if($funEmbrague == "Futuro") echo "checked";?> <?php echo set_checkbox('embragueRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="embragueRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($funEmbrague)) if($funEmbrague == "Inmediato") echo "checked";?> <?php echo set_checkbox('embragueRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('embragueRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Funcionamiento del embrague (si está equipado).
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="embragueCheck" value="1" <?php if(isset($funEmbragueCambio)) if($funEmbragueCambio == "1") echo "checked";?> <?php echo set_checkbox('embragueCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('embragueCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="cardanRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($flechaCardan)) if($flechaCardan == "Aprobado") echo "checked";?> <?php echo set_checkbox('cardanRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="cardanRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($flechaCardan)) if($flechaCardan == "Futuro") echo "checked";?> <?php echo set_checkbox('cardanRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"name="cardanRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($flechaCardan)) if($flechaCardan == "Inmediato") echo "checked";?> <?php echo set_checkbox('cardanRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('cardanRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Transmisión, flecha cardán y lubricación (si necesita).
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="cardanCheck" value="1" <?php if(isset($flechaCardanCambio)) if($flechaCardanCambio == "1") echo "checked";?> <?php echo set_checkbox('cardanCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('cardanCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="text-align: right ;background-color:#340f7b;color:white">
                                            <td align="center">PARTE INFERIOR DEL VEHÍCULO</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <?php if (isset($defectosImg)): ?>
                                                    <?php if ($defectosImg != ""): ?>
                                                        <img src="<?php echo base_url().$defectosImg; ?>" style="width:380px;">
                                                        <canvas  id="diagramaInferior" width="380" height="200" hidden></canvas>
                                                        <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car_ext.png'; ?>">
                                                        <input type="hidden" name="danosInferior" id="danosInferior" value="<?php echo $defectosImg; ?>">
                                                    <?php else: ?>
                                                        <!-- CANVAS -->
                                                        <canvas  id="diagramaInferior" width="380" height="200"></canvas>
                                                        <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car_ext.png'; ?>">
                                                        <input type="hidden" name="danosInferior" id="danosInferior" value="<?php echo set_value('danosInferior'); ?>">
                                                        <!-- CANVAS -->
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <!-- CANVAS -->
                                                    <canvas  id="diagramaInferior" width="380" height="200"></canvas>
                                                    <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car_ext.png'; ?>">
                                                    <input type="hidden" name="danosInferior" id="danosInferior" value="<?php echo set_value('danosInferior'); ?>">
                                                    <!-- CANVAS -->
                                                <?php endif; ?>

                                            </td>
                                            <td>
                                                Anote en el diagrama todos los daños o defectos detectados en la parte
                                                inferior de la carrocería durante la revisión en el taller
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Segunda hoja del formulario -->
        <div class="row">
            <div class="col-md-12">
                <table style="border:2px solid black; width:100%;">
                    <tr style="color:white; text-align:center;background-color:#340f7b;">
                            <td colspan="4">DESGASTE DE NEUMÁTICO / FRENO</td>
                        </tr>
                        <tr style="border:1px solid black;">
                            <td style="color:white;background-color:#340f7b;border:1px solid black;">PROFUNDIDAD DE DIBUJO</td>
                            <td style="background-color:green;color:white;" align="center">5 mm y mayor</td>
                            <td style="background-color:yellow" align="center">3 a 5 mm</td>
                            <td style="background-color:red;color:white;" align="center">2 mm y menor</td>
                        </tr>
                        <tr style="border:1px solid black;">
                            <td style="color:white;background-color:#340f7b;border:1px solid black;">MEDIDA DE BALATAS</td>
                            <td style="background-color:green; color:white;" align="center">Mas de 8 mm</td>
                            <td style="background-color:yellow" align="center">4 a 6 mm</td>
                            <td style="background-color:red;color:white;" align="center"> 3 mm o menos</td>
                        </tr>
                </table>
            </div>
        </div>

        <div class="row" style="padding-left: 15px;padding-right: 15px;">
            <!--Lado izquierdo de la segunda hoja -->
            <div class="col-md-6" style="border:2px solid black; width:100%; padding: 0px;">
                <table style="width:100%;">
                    <tr>
                        <td style="background-color:lightgrey;">
                            &nbsp;&nbsp;<input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="medicionfrenos" value="1" <?php if(isset($medicionesFrenos)) if($medicionesFrenos == "1") echo "checked";?> <?php echo set_checkbox('medicionfrenos', '1'); ?>>
                            &nbsp;&nbsp;&nbsp;&nbsp;No se tomaron mediciones de los frenos en esta visita de servicio.
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:lightgrey;">
                            &nbsp;&nbsp;<input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="reinicioindicador" value="1" <?php if(isset($indicadorAceite)) if($indicadorAceite == "1") echo "checked";?> <?php echo set_checkbox('reinicioindicador', '1'); ?>>
                            &nbsp;&nbsp;&nbsp;&nbsp;Reinicio del indicador de cambio de aceite.
                            <br>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="width:100%;">
                    <tr class="headers_table" align="center">
                        <td style="background-color:lightgrey; color:black;">
                            FRENTE IZQUIERDO &nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="profNeumaticoRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($pneumaticoFI)) if($pneumaticoFI == "Aprobado") echo "checked";?> <?php echo set_checkbox('profNeumaticoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="profNeumaticoRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($pneumaticoFI)) if($pneumaticoFI == "Futuro") echo "checked";?> <?php echo set_checkbox('profNeumaticoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="profNeumaticoRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($pneumaticoFI)) if($pneumaticoFI == "Inmediato") echo "checked";?> <?php echo set_checkbox('profNeumaticoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('profNeumaticoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Profundidad de dibujo del neumático.
                                    <input type="text" class="input_field" name="profNeumatico" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;" value="<?php if(set_value('profNeumatico') != "") { echo set_value('profNeumatico');} else {if(isset($pneumaticoFImm)) echo $pneumaticoFImm; }  ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('profNeumatico', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="profNeumaticoCheck" value="1" <?php if(isset($pneumaticoFIcambio)) if($pneumaticoFIcambio == "1") echo "checked";?>  <?php echo set_checkbox('profNeumaticoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('profNeumaticoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="patronDegasteRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($dNeumaticoFI)) if($dNeumaticoFI == "Aprobado") echo "checked";?> <?php echo set_checkbox('patronDegasteRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="patronDegasteRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($dNeumaticoFI)) if($dNeumaticoFI == "Futuro") echo "checked";?> <?php echo set_checkbox('patronDegasteRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="patronDegasteRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($dNeumaticoFI)) if($dNeumaticoFI == "Inmediato") echo "checked";?> <?php echo set_checkbox('patronDegasteRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('patronDegasteRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Patrón de desgaste/daño del neumático.
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="patronDegasteCheck" value="1" <?php if(isset($dNeumaticoFIcambio)) if($dNeumaticoFIcambio == "1") echo "checked";?> <?php echo set_checkbox('patronDegasteCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('patronDegasteCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="presionRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($presionInfladoFI)) if($presionInfladoFI == "Aprobado") echo "checked";?> <?php echo set_checkbox('presionRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="presionRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($presionInfladoFI)) if($presionInfladoFI == "Futuro") echo "checked";?> <?php echo set_checkbox('presionRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="presionRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($presionInfladoFI)) if($presionInfladoFI == "Inmediato") echo "checked";?> <?php echo set_checkbox('presionRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('presionRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Presión de inflado a PSI según recomendación del fabricante.
                                    <input type="text" class="input_field" name="presionPSIFI" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;" value="<?php if(set_value('presionPSIFI') != "") { echo set_value('presionPSIFI');} else {if(isset($presionInfladoFIpsi)) echo $presionInfladoFIpsi; }  ?>">
                                    PSI
                                    <br>
                                    <?php echo form_error('presionPSIFI', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="presionCheck" value="1" <?php if(isset($presionInfladoFIcambio)) if($presionInfladoFIcambio == "1") echo "checked";?> <?php echo set_checkbox('presionCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('presionCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="balatasRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($espesoBalataFI)) if($espesoBalataFI == "Aprobado") echo "checked";?> <?php echo set_checkbox('balatasRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="balatasRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($espesoBalataFI)) if($espesoBalataFI == "Futuro") echo "checked";?> <?php echo set_checkbox('balatasRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="balatasRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($espesoBalataFI)) if($espesoBalataFI == "Inmediato") echo "checked";?> <?php echo set_checkbox('balatasRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('balatasRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de balatas.<br>
                                    <input type="text" class="input_field" onkeypress='return event.charCode >= 46 && event.charCode <= 57' name="balatas" style="width:2cm;" value="<?php if(set_value('balatas') != "") { echo set_value('balatas');} else {if(isset($espesorBalataFImm)) echo $espesorBalataFImm; }  ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('balatas', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="balatasCheck" value="1" <?php if(isset($espesorBalataFIcambio)) if($espesorBalataFIcambio == "1") echo "checked";?> <?php echo set_checkbox('balatasCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('balatasCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="discoRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($espesorDiscoFI)) if($espesorDiscoFI == "Aprobado") echo "checked";?> <?php echo set_checkbox('discoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="discoRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($espesorDiscoFI)) if($espesorDiscoFI == "Futuro") echo "checked";?> <?php echo set_checkbox('discoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="discoRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($espesorDiscoFI)) if($espesorDiscoFI == "Inmediato") echo "checked";?> <?php echo set_checkbox('discoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('discoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de disco.<br>
                                    <input type="text" class="input_field" onkeypress='return event.charCode >= 46 && event.charCode <= 57' name="discoI" style="width:2cm;" value="<?php if(set_value('discoI') != "") { echo set_value('discoI');} else {if(isset($espesorDiscoFImm)) echo $espesorDiscoFImm; }  ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('discoI', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="discoCheck" value="1" <?php if(isset($espesorDiscoFIcambio)) if($espesorDiscoFIcambio == "1") echo "checked";?> <?php echo set_checkbox('discoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('discoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr class="headers_table" align="center">
                        <td style="background-color:lightgrey; color:black;">
                            PARTE TRASERA IZQUIERDO &nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="profundidadDibujoRadio" value="Aprobado" class="seccionTecnico BtnVerde" <?php if(isset($pneumaticoTI)) if($pneumaticoTI == "Aprobado") echo "checked";?> <?php echo set_checkbox('profundidadDibujoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="profundidadDibujoRadio" value="Futuro" class="seccionTecnico BtnAmarillo" <?php if(isset($pneumaticoTI)) if($pneumaticoTI == "Futuro") echo "checked";?> <?php echo set_checkbox('profundidadDibujoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"name="profundidadDibujoRadio" value="Inmediato" class="seccionTecnico BtnRojo" <?php if(isset($pneumaticoTI)) if($pneumaticoTI == "Inmediato") echo "checked";?> <?php echo set_checkbox('profundidadDibujoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('profundidadDibujoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Profundidad de dibujo del neumático.
                                    <input type="text" class="input_field" onkeypress='return event.charCode >= 46 && event.charCode <= 57' name="profundidadDibujo" style="width:2cm;" value="<?php if(set_value('profundidadDibujo') != "") { echo set_value('profundidadDibujo');} else {if(isset($pneumaticoTImm)) echo $pneumaticoTImm; }  ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('profundidadDibujo', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="profundidadDibujoCheck" value="1" <?php if(isset($pneumaticoTIcambio)) if($pneumaticoTIcambio == "1") echo "checked";?> <?php echo set_checkbox('profundidadDibujoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('profundidadDibujoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                  <div class="divAprobado" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="desgasteRadio" <?php if(isset($dNeumaticoTI)) if($dNeumaticoTI == "Aprobado") echo "checked";?> <?php echo set_checkbox('desgasteRadio', 'Aprobado'); ?>>
                                  </div>
                                  <div class="divFuturo" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="desgasteRadio" <?php if(isset($dNeumaticoTI)) if($dNeumaticoTI == "Futuro") echo "checked";?> <?php echo set_checkbox('desgasteRadio', 'Futuro'); ?>>
                                  </div>
                                  <div class="divInmediato" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="desgasteRadio" <?php if(isset($dNeumaticoTI)) if($dNeumaticoTI == "Inmediato") echo "checked";?> <?php echo set_checkbox('desgasteRadio', 'Inmediato'); ?>>
                                  </div>
                                  <br>
                                  <?php echo form_error('desgasteRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Patrón de desgaste/daño del neumático.
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="desgasteCheck" value="1" <?php if(isset($dNeumaticoTIcambio)) if($dNeumaticoTIcambio == "1") echo "checked";?> <?php echo set_checkbox('desgasteCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('desgasteCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                          <div class="row">
                              <div class="col-md-4">
                                  <div class="divAprobado" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="infladoRadio" <?php if(isset($presionInfladoTI)) if($presionInfladoTI == "Aprobado") echo "checked";?> <?php echo set_checkbox('infladoRadio', 'Aprobado'); ?>>
                                  </div>
                                  <div class="divFuturo" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="infladoRadio" <?php if(isset($presionInfladoTI)) if($presionInfladoTI == "Futuro") echo "checked";?> <?php echo set_checkbox('infladoRadio', 'Futuro'); ?>>
                                  </div>
                                  <div class="divInmediato" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="infladoRadio" <?php if(isset($presionInfladoTI)) if($presionInfladoTI == "Inmediato") echo "checked";?> <?php echo set_checkbox('infladoRadio', 'Inmediato'); ?>>
                                  </div>
                                  <br>
                                  <?php echo form_error('infladoRadio', '<span class="error">', '</span>'); ?>
                              </div>
                              <div class="col-md-8">
                                  Presión de inflado a PSI según recomendación del fabricante.
                                  <input type="text" class="input_field" name="presionPSITI" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;" value="<?php if(set_value('presionPSITI') != "") { echo set_value('presionPSITI');} else {if(isset($presionInfladoTIpsi)) echo $presionInfladoTIpsi; }  ?>">
                                  PSI
                                  <br>
                                  <?php echo form_error('presionPSITI', '<span class="error">', '</span>'); ?>
                              </div>
                          </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="infladoCheck" value="1" <?php if(isset($presionInfladoTIcambio)) if($presionInfladoTIcambio == "1") echo "checked";?> <?php echo set_checkbox('infladoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('infladoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="espesorBalataRadio" <?php if(isset($espesoBalataTI)) if($espesoBalataTI == "Aprobado") echo "checked";?> <?php echo set_checkbox('espesorBalataRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="espesorBalataRadio" <?php if(isset($espesoBalataTI)) if($espesoBalataTI == "Futuro") echo "checked";?> <?php echo set_checkbox('espesorBalataRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="espesorBalataRadio" <?php if(isset($espesoBalataTI)) if($espesoBalataTI == "Inmediato") echo "checked";?> <?php echo set_checkbox('espesorBalataRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('espesorBalataRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de balatas.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="espesorBalata" style="width:2cm;" value="<?php if(set_value('espesorBalata') != "") { echo set_value('espesorBalata');} else {if(isset($espesorBalataTImm)) echo $espesorBalataTImm; }  ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('espesorBalata', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="espesorBalataCheck" value="1" <?php if(isset($espesorBalataTIcambio)) if($espesorBalataTIcambio == "1") echo "checked";?> <?php echo set_checkbox('espesorBalataCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('espesorBalataCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="espesorDiscoRadio" <?php if(isset($espesorDiscoTI)) if($espesorDiscoTI == "Aprobado") echo "checked";?> <?php echo set_checkbox('espesorDiscoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="espesorDiscoRadio" <?php if(isset($espesorDiscoTI)) if($espesorDiscoTI == "Futuro") echo "checked";?> <?php echo set_checkbox('espesorDiscoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="espesorDiscoRadio" <?php if(isset($espesorDiscoTI)) if($espesorDiscoTI == "Inmediato") echo "checked";?> <?php echo set_checkbox('espesorDiscoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('espesorDiscoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de disco.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="espesorDisco" style="width:2cm;" value="<?php if(set_value('espesorDisco') != "") { echo set_value('espesorDisco');} else {if(isset($espesorDiscoTImm)) echo $espesorDiscoTImm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('espesorDisco', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="espesorDiscoCheck" value="1" <?php if(isset($espesorDiscoTIcambio)) if($espesorDiscoTIcambio == "1") echo "checked";?> <?php echo set_checkbox('espesorDiscoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('espesorDiscoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="diametroRadio" <?php if(isset($diametroTamborTI)) if($diametroTamborTI == "Aprobado") echo "checked";?> <?php echo set_checkbox('diametroRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="diametroRadio" <?php if(isset($diametroTamborTI)) if($diametroTamborTI == "Futuro") echo "checked";?> <?php echo set_checkbox('diametroRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="diametroRadio" <?php if(isset($diametroTamborTI)) if($diametroTamborTI == "Inmediato") echo "checked";?> <?php echo set_checkbox('diametroRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('diametroRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Diámetro de tambor.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="diametro" style="width:2cm;" value="<?php if(set_value('diametro') != "") { echo set_value('diametro');} else {if(isset($diametroTamborTImm)) echo $diametroTamborTImm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('diametro', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="diametroCheck" value="1" <?php if(isset($diametroTamborTIcambio)) if($diametroTamborTIcambio == "1") echo "checked";?> <?php echo set_checkbox('diametroCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('diametroCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
            </div>

            <!--Lado derecho de la segunda hoja -->
            <div class="col-md-6" style="border:2px solid black; width:100%; padding: 0px;">
                <table style="width:100%;">
                    <tr>
                        <td colspan="2">
                            Comentarios:<br>
                            <textarea class="form-control SeccionTecnico" name="comentarios" rows="1" style="width: 100%;"><?php if(set_value('comentarios') != "") { echo set_value('comentarios');} else {if(isset($comentarios)) echo $comentarios." ".$comentario; } ?></textarea>
                            <br>
                        </td>
                    </tr>
                    <tr align="center">
                        <td style="background-color:lightgrey; color:black;">
                            FRENTE DERECHO&nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="profNeumaticoDerechoRadio" <?php if(isset($pneumaticoFD)) if($pneumaticoFD == "Aprobado") echo "checked";?> <?php echo set_checkbox('profNeumaticoDerechoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="profNeumaticoDerechoRadio" <?php if(isset($pneumaticoFD)) if($pneumaticoFD == "Futuro") echo "checked";?> <?php echo set_checkbox('profNeumaticoDerechoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="profNeumaticoDerechoRadio" <?php if(isset($pneumaticoFD)) if($pneumaticoFD == "Inmediato") echo "checked";?> <?php echo set_checkbox('profNeumaticoDerechoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('profNeumaticoDerechoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Profundidad de dibujo del neumático.
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="profNeumaticoDerecho" style="width:2cm;" value="<?php if(set_value('profNeumaticoDerecho') != "") { echo set_value('profNeumaticoDerecho');} else {if(isset($pneumaticoFDmm)) echo $pneumaticoFDmm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('profNeumaticoDerecho', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="profNeumaticoDerechoCheck" value="1" <?php if(isset($pneumaticoFDcambio)) if($pneumaticoFDcambio == "1") echo "checked";?> <?php echo set_checkbox('profNeumaticoDerechoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('profNeumaticoDerechoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="desgasteDerRadio" <?php if(isset($dNeumaticoFD)) if($dNeumaticoFD == "Aprobado") echo "checked";?> <?php echo set_checkbox('desgasteDerRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="desgasteDerRadio" <?php if(isset($dNeumaticoFD)) if($dNeumaticoFD == "Futuro") echo "checked";?> <?php echo set_checkbox('desgasteDerRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="desgasteDerRadio" <?php if(isset($dNeumaticoFD)) if($dNeumaticoFD == "Inmediato") echo "checked";?> <?php echo set_checkbox('desgasteDerRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('desgasteDerRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Patrón de desgaste/daño del neumático.
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="desgasteDerCheck" value="1" <?php if(isset($dNeumaticoFDcambio)) if($dNeumaticoFDcambio == "1") echo "checked";?> <?php echo set_checkbox('desgasteDerCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('desgasteDerCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="infladoDerRadio" <?php if(isset($presionInfladoFD)) if($presionInfladoFD == "Aprobado") echo "checked";?> <?php echo set_checkbox('infladoDerRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="infladoDerRadio" <?php if(isset($presionInfladoFD)) if($presionInfladoFD == "Futuro") echo "checked";?> <?php echo set_checkbox('infladoDerRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="infladoDerRadio" <?php if(isset($presionInfladoFD)) if($presionInfladoFD == "Inmediato") echo "checked";?> <?php echo set_checkbox('infladoDerRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('infladoDerRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Presión de inflado a PSI según recomendación del fabricante.
                                    <input type="text" class="input_field" name="presionPSIFD" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;" value="<?php if(set_value('presionPSIFD') != "") { echo set_value('presionPSIFD');} else {if(isset($presionInfladoFDpsi)) echo $presionInfladoFDpsi; }  ?>">
                                    PSI
                                    <br>
                                    <?php echo form_error('presionPSIFD', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="infladoDerCheck" value="1" <?php if(isset($presionInfladoFDcambio)) if($presionInfladoFDcambio == "1") echo "checked";?> <?php echo set_checkbox('infladoDerCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('infladoDerCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="balatasDerRadio" <?php if(isset($espesoBalataFD)) if($espesoBalataFD == "Aprobado") echo "checked";?> <?php echo set_checkbox('balatasDerRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="balatasDerRadio" <?php if(isset($espesoBalataFD)) if($espesoBalataFD == "Futuro") echo "checked";?> <?php echo set_checkbox('balatasDerRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="balatasDerRadio" <?php if(isset($espesoBalataFD)) if($espesoBalataFD == "Inmediato") echo "checked";?> <?php echo set_checkbox('balatasDerRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('balatasDerRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de balatas.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="balatasDerc" style="width:2cm;" value="<?php if(set_value('balatasDerc') != "") { echo set_value('balatasDerc');} else {if(isset($espesorBalataFDmm)) echo $espesorBalataFDmm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('balatasDerc', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="balatasDercCheck" value="1" <?php if(isset($espesorBalataFDcambio)) if($espesorBalataFDcambio == "1") echo "checked";?> <?php echo set_checkbox('balatasDercCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('balatasDercCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="discoDercRadio" <?php if(isset($espesorDiscoFD)) if($espesorDiscoFD == "Aprobado") echo "checked";?> <?php echo set_checkbox('discoDercRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="discoDercRadio" <?php if(isset($espesorDiscoFD)) if($espesorDiscoFD == "Futuro") echo "checked";?> <?php echo set_checkbox('discoDercRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="discoDercRadio" <?php if(isset($espesorDiscoFD)) if($espesorDiscoFD == "Inmediato") echo "checked";?> <?php echo set_checkbox('discoDercRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('discoDercRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de disco.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="discoDerch" style="width:2cm;" value="<?php if(set_value('discoDerch') != "") { echo set_value('discoDerch');} else {if(isset($espesorDiscoFDmm)) echo $espesorDiscoFDmm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('discoDerch', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="discoDercCheck" value="1" <?php if(isset($espesorDiscoFDcambio)) if($espesorDiscoFDcambio == "1") echo "checked";?> <?php echo set_checkbox('discoDercCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('discoDercCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr class="headers_table" align="center">
                        <td style="background-color:lightgrey; color:black;">
                            PARTE TRASERA DERECHO &nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="dibujoDerecRadio" <?php if(isset($pneumaticoTD)) if($pneumaticoTD == "Aprobado") echo "checked";?> <?php echo set_checkbox('dibujoDerecRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="dibujoDerecRadio" <?php if(isset($pneumaticoTD)) if($pneumaticoTD == "Futuro") echo "checked";?> <?php echo set_checkbox('dibujoDerecRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="dibujoDerecRadio" <?php if(isset($pneumaticoTD)) if($pneumaticoTD == "Inmediato") echo "checked";?> <?php echo set_checkbox('dibujoDerecRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('dibujoDerecRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Profundidad de dibujo del neumático.
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="dibujoDerech" style="width:2cm;" value="<?php if(set_value('dibujoDerech') != "") { echo set_value('dibujoDerech');} else {if(isset($pneumaticoTDmm)) echo $pneumaticoTDmm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('dibujoDerech', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="dibujoDerechCheck" value="1" <?php if(isset($pneumaticoTDcambio)) if($pneumaticoTDcambio == "1") echo "checked";?> <?php echo set_checkbox('dibujoDerechCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('dibujoDerechCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="patronDesgDerchRadio" <?php if(isset($dNeumaticoTD)) if($dNeumaticoTD == "Aprobado") echo "checked";?> <?php echo set_checkbox('patronDesgDerchRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="patronDesgDerchRadio" <?php if(isset($dNeumaticoTD)) if($dNeumaticoTD == "Futuro") echo "checked";?> <?php echo set_checkbox('patronDesgDerchRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="patronDesgDerchRadio" <?php if(isset($dNeumaticoTD)) if($dNeumaticoTD == "Inmediato") echo "checked";?> <?php echo set_checkbox('patronDesgDerchRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('patronDesgDerchRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Patrón de desgaste/daño del neumático.
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="patronDesgDerchCheck" value="1" <?php if(isset($dNeumaticoTDcambio)) if($dNeumaticoTDcambio == "1") echo "checked";?> <?php echo set_checkbox('patronDesgDerchCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('patronDesgDerchCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="presionDerchRadio" <?php if(isset($presionInfladoTD)) if($presionInfladoTD == "Aprobado") echo "checked";?> <?php echo set_checkbox('presionDerchRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="presionDerchRadio" <?php if(isset($presionInfladoTD)) if($presionInfladoTD == "Futuro") echo "checked";?> <?php echo set_checkbox('presionDerchRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="presionDerchRadio" <?php if(isset($presionInfladoTD)) if($presionInfladoTD == "Inmediato") echo "checked";?> <?php echo set_checkbox('presionDerchRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('presionDerchRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Presión de inflado a PSI según recomendación del fabricante.
                                    <input type="text" class="input_field" name="presionPSITD" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;" value="<?php if(set_value('presionPSITD') != "") { echo set_value('presionPSITD');} else {if(isset($presionInfladoTDpsi)) echo $presionInfladoTDpsi; }  ?>">
                                    PSI
                                    <br>
                                    <?php echo form_error('presionPSITD', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="presionDerchCheck" value="1" <?php if(isset($presionInfladoTDcambio)) if($presionInfladoTDcambio == "1") echo "checked";?> <?php echo set_checkbox('presionDerchCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('presionDerchCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="balatasEspesorRadio" <?php if(isset($espesoBalataTD)) if($espesoBalataTD == "Aprobado") echo "checked";?> <?php echo set_checkbox('balatasEspesorRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="balatasEspesorRadio" <?php if(isset($espesoBalataTD)) if($espesoBalataTD == "Futuro") echo "checked";?> <?php echo set_checkbox('balatasEspesorRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="balatasEspesorRadio" <?php if(isset($espesoBalataTD)) if($espesoBalataTD == "Inmediato") echo "checked";?> <?php echo set_checkbox('balatasEspesorRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('balatasEspesorRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de balatas.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="balatasEspesor" style="width:2cm;" value="<?php if(set_value('balatasEspesor') != "") { echo set_value('balatasEspesor');} else {if(isset($espesorBalataTDmm)) echo $espesorBalataTDmm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('balatasEspesor', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="balatasEspesorCheck" value="1" <?php if(isset($espesorBalataTDcambio)) if($espesorBalataTDcambio == "1") echo "checked";?> <?php echo set_checkbox('balatasEspesorCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('balatasEspesorCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="espesorDisco2Radio" <?php if(isset($espesorDiscoTD)) if($espesorDiscoTD == "Aprobado") echo "checked";?> <?php echo set_checkbox('espesorDisco2Radio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="espesorDisco2Radio" <?php if(isset($espesorDiscoTD)) if($espesorDiscoTD == "Futuro") echo "checked";?> <?php echo set_checkbox('espesorDisco2Radio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="espesorDisco2Radio" <?php if(isset($espesorDiscoTD)) if($espesorDiscoTD == "Inmediato") echo "checked";?> <?php echo set_checkbox('espesorDisco2Radio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('espesorDisco2Radio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de disco.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="espesorDisco2" style="width:2cm;" value="<?php if(set_value('espesorDisco2') != "") { echo set_value('espesorDisco2');} else {if(isset($espesorDiscoTDmm)) echo $espesorDiscoTDmm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('espesorDisco2', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="espesorDisco2Check" value="1" <?php if(isset($espesorDiscoTDcambio)) if($espesorDiscoTDcambio == "1") echo "checked";?> <?php echo set_checkbox('espesorDisco2Check', '1'); ?>>
                            <br>
                            <?php echo form_error('espesorDisco2Check', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="diametroTamborRadio" <?php if(isset($diametroTamborTD)) if($diametroTamborTD == "Aprobado") echo "checked";?> <?php echo set_checkbox('diametroTamborRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="diametroTamborRadio" <?php if(isset($diametroTamborTD)) if($diametroTamborTD == "Futuro") echo "checked";?> <?php echo set_checkbox('diametroTamborRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="diametroTamborRadio" <?php if(isset($diametroTamborTD)) if($diametroTamborTD == "Inmediato") echo "checked";?> <?php echo set_checkbox('diametroTamborRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('diametroTamborRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Diámetro de tambor.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="diametroTambor" style="width:2cm;" value="<?php if(set_value('diametroTambor') != "") { echo set_value('diametroTambor');} else {if(isset($diametroTamborTDmm)) echo $diametroTamborTDmm; } ?>">
                                    mm
                                    <br>
                                    <?php echo form_error('diametroTambor', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="diametroTamborCheck" value="1" <?php if(isset($diametroTamborTDcambio)) if($diametroTamborTDcambio == "1") echo "checked";?> <?php echo set_checkbox('diametroTamborCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('diametroTamborCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr class="headers_table" align="center">
                        <td style="background-color:lightgrey; color:black;">
                            NEUMÁTICO DE REFACCIÓN &nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                          <div class="row">
                              <div class="col-md-4">
                                  <div class="divAprobado" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Aprobado" class="seccionTecnico BtnVerde" name="infladoPresionRadio" <?php if(isset($presion)) if($presion == "Aprobado") echo "checked";?> <?php echo set_checkbox('infladoPresionRadio', 'Aprobado'); ?>>
                                  </div>
                                  <div class="divFuturo" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Futuro" class="seccionTecnico BtnAmarillo" name="infladoPresionRadio" <?php if(isset($presion)) if($presion == "Futuro") echo "checked";?> <?php echo set_checkbox('infladoPresionRadio', 'Futuro'); ?>>
                                  </div>
                                  <div class="divInmediato" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);"value="Inmediato" class="seccionTecnico BtnRojo" name="infladoPresionRadio" <?php if(isset($presion)) if($presion == "Inmediato") echo "checked";?> <?php echo set_checkbox('infladoPresionRadio', 'Inmediato'); ?>>
                                  </div>
                                  <br>
                                  <?php echo form_error('infladoPresionRadio', '<span class="error">', '</span>'); ?>
                              </div>
                              <div class="col-md-8">
                                  Presión de inflado establecida en
                                  <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field" name="infladoPresion" value="<?php if(set_value('infladoPresion') != "") { echo set_value('infladoPresion');} else {if(isset($presionPSI)) echo $presionPSI; } ?>">
                                  PSI
                                  <br>
                                  <?php echo form_error('infladoPresion', '<span class="error">', '</span>'); ?>
                              </div>
                          </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);"  class="seccionTecnico" name="infladoPresionCheck" value="1" <?php if(isset($presionCambio)) if($presionCambio == "1") echo "checked";?> <?php echo set_checkbox('infladoPresionCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('infladoPresionCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <table width="100%" style="border-bottom:2px solid black;border-left: 2px solid black; border-right: 2px solid black;">
                    <thead></thead>
                    <tbody>
                        <tr style="background-color:lightgrey;color:black; border: 1px solid black; text-align:center;">
                            <td colspan="">DIAGNÓSTICO</td>
                        </tr>
                        <tr>
                            <td style="padding:7px;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Sistema.<br>
                                        <textarea type="text" class="input_field_lg" name="system" rows="5"><?php if(set_value('system') != "") { echo set_value('system');} else {if(isset($sistema)) echo $sistema; } ?></textarea>
                                        <br>
                                        <?php echo form_error('system', '<span class="error">', '</span>'); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        Componente.<br>
                                        <textarea type="text" class="input_field_lg" name="component" rows="5"><?php if(set_value('component') != "") { echo set_value('component');} else {if(isset($componente)) echo $componente; } ?></textarea>
                                        <br>
                                        <?php echo form_error('component', '<span class="error">', '</span>'); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        Causa raíz.<br>
                                        <textarea type="text" class="input_field_lg" name="raiz" rows="5"><?php if(set_value('raiz') != "") { echo set_value('raiz');} else {if(isset($causaRaiz)) echo $causaRaiz; } ?></textarea>
                                        <br>
                                        <?php echo form_error('raiz', '<span class="error">', '</span>'); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <p style="color:blue">DATOS DISTRIBUIDOR</p>
                                        <?=$this->config->item('base_hm_texto')?>
                                        TEL. <?=SUCURSAL_TEL?>
                                        <br><span style="color:darkblue;"><?=SUCURSAL_WEB?></span><br>
                                    </div>
                                </div>
                            </td>
                       </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12" align="right">
                <br>
                <!-- UPLOAD FILES -->
                <input type="file" id="archivos" multiple name="uploadfiles[]"><br>
                <label for=""id="nombrearchivo"></label>
                <br>
                <?php if (isset($archivo)): ?>
                    <?php for ($i = 0; $i < count($archivo); $i++): ?>
                      <?php if ($archivo[$i] != ""): ?>
                          <a href="<?php echo base_url().$archivo[$i]; ?>" class="btn btn-info" target="_blank">Ver documento (<?php echo $i+1; ?>)</a>
                          <?php if ((($i+1)% 2) == 0): ?>
                              <br>
                          <?php endif; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                <?php endif; ?>
                <input type="hidden" name="tempFile" value="<?php if(isset($archivoTemp)) echo $archivoTemp;?>">
                <br>
                <!-- <a class="btn btn-dark" id="cotizaExt" data-target="#cotizacionModal" data-toggle="modal" style="color:white;">
                    <i class="fas fa-calculator"></i> Cotización
                </a> -->
                <button type="button" class="btn btn-dark" id="cotizaExt" style="color:white;">
                    <i id="iconCoti" class="far fa-eye" style="font-size: 14px;"></i> Cotización
                </button>
                <br>
                <input type="hidden" id="divCotizacion" value="0">
                <label for="" id="MsmModal"></label>
            </div>
        </div>

        <br>
        <!-- Mostramos seccion para dar de alta una cotización -->
        <div class="row" style="border: 2px solid black;display:none;height:750px;min-width:100%;" id="espacioCoti">
            <!-- <div class="col-sm-12" style="height:250px;min-width:100%;"> -->
            <iframe class="embed-responsive-item" src="<?php echo base_url().'Alta_Presupuesto/'.((isset($idCita)) ? $idCita : '0'); ?>" frameborder="0" id="ventanaOculta" style="height:100%;width:100%;"></iframe>
            <!-- </div> -->
        </div>

        <div class="row">
            <div class="col-sm-3" align="center">
                NOMBRE Y FIRMA DEL ASESOR
                <br>
                <?php if (isset($firmaAsesor)): ?>
                    <?php if ($firmaAsesor == ""): ?>
                        <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                        <img class="marcoImg" src="<?php if(set_value('rutaFirmaAsesor') != "") echo set_value('rutaFirmaAsesor');  ?>" id="firmaFirmaAsesor" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <!-- <input type="text" class="input_field" name="asesorNombre" id="asesorNombre" style="width:100%;" value="<?php if(isset($asesor)) echo $asesor;?>"> -->
                    <?php else: ?>
                        <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?php if(isset($firmaAsesor)) echo $firmaAsesor;?>" disabled>
                        <img class="marcoImg" src="<?php if(isset($firmaAsesor)) echo base_url().$firmaAsesor;?>" id="firmaFirmaAsesor" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <!-- <input type="text" class="input_field" name="asesorNombre" id="asesorNombre" style="width:100%;" value="<?php if(isset($asesor)) echo $asesor;?>"> -->
                    <?php endif; ?>
                <?php else: ?>
                    <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                    <img class="marcoImg" src="<?php if(set_value('rutaFirmaAsesor') != "") echo set_value('rutaFirmaAsesor');  ?>" id="firmaFirmaAsesor" alt="" style="height:2cm;width:4cm;">
                    <br>
                    <!-- <input type="text" class="input_field" name="asesorNombre" id="asesorNombre" style="width:100%;" value="<?= set_value('asesorNombre');?>"> -->
                <?php endif; ?>
                <input type="text" class="input_field" name="asesorNombre" id="asesorNombre" style="width:100%;" value="<?php if(isset($asesor)) echo $asesor;?>">
                <?php echo form_error('rutaFirmaAsesor', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('asesorNombre', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>

            <div class="col-sm-3" align="center">
                NOMBRE Y FIRMA DEL TÉCNICO
                <br>
                <?php if (isset($firmaTecnico)): ?>
                    <?php if ($firmaTecnico == ""): ?>
                        <input type="hidden" id="rutaFirmaTecnico" name="rutaFirmaTecnico" value="<?= set_value('rutaFirmaTecnico');?>">
                        <img class="marcoImg" src="<?php if(set_value('rutaFirmaTecnico') != "") echo set_value('rutaFirmaTecnico');  ?>" id="firmaFirmaTecnico" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <!-- <input type="text" class="input_field" name="nombreTecnico" id="nombreTecnico" style="width:100%;" value="<?= set_value('nombreTecnico');?>"> -->
                    <?php else: ?>
                        <input type="hidden" id="rutaFirmaTecnico" name="rutaFirmaTecnico" value="<?php if(isset($firmaTecnico)) echo $firmaTecnico;?>">
                        <img class="marcoImg" src="<?php if(isset($firmaTecnico)) echo base_url().$firmaTecnico;?>" id="firmaFirmaTecnico" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <!-- <input type="text" class="input_field" name="nombreTecnico" id="nombreTecnico" style="width:100%;" value="<?php if(isset($tecnico)) echo $tecnico;?>" disabled> -->
                    <?php endif; ?>
                <?php else: ?>
                    <input type="hidden" id="rutaFirmaTecnico" name="rutaFirmaTecnico" value="<?= set_value('rutaFirmaTecnico');?>">
                    <img class="marcoImg" src="<?php if(set_value('rutaFirmaTecnico') != "") echo set_value('rutaFirmaTecnico');  ?>" id="firmaFirmaTecnico" alt="" style="height:2cm;width:4cm;">
                    <br>
                    <!-- <input type="text" class="input_field" name="nombreTecnico" id="nombreTecnico" style="width:100%;" value="<?= set_value('nombreTecnico');?>"> -->
                <?php endif; ?>
                <input type="text" class="input_field" name="nombreTecnico" id="nombreTecnico" style="width:100%;" value="<?php if(isset($tecnico)) echo $tecnico;?>" disabled>
                <?php echo form_error('rutaFirmaTecnico', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('nombreTecnico', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>
            <div class="col-sm-3" align="center">
                NOMBRE Y FIRMA DEL JEFE DE TALLER
                <br>
                <?php if (isset($firmaJefeTaller)): ?>
                    <?php if ($firmaJefeTaller == ""): ?>
                        <input type="hidden" id="rutaFirmaJefeTaller" name="rutaFirmaJefeTaller" value="<?= set_value('rutaFirmaJefeTaller');?>">
                        <img class="marcoImg" src="<?php if(set_value('rutaFirmaJefeTaller') != "") echo set_value('rutaFirmaJefeTaller');  ?>" id="firmaFirmaJefeTaller" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <!-- <input type="text" class="input_field" name="JefeTallerNombre" id="JefeTallerNombre" style="width:100%;" value="<?= set_value('JefeTallerNombre');?>"> -->
                    <?php else: ?>
                        <input type="hidden" id="rutaFirmaJefeTaller" name="rutaFirmaJefeTaller" value="<?php if(isset($firmaJefeTaller)) echo $firmaJefeTaller;?>">
                        <img class="marcoImg" src="<?php if(isset($firmaJefeTaller)) echo base_url().$firmaJefeTaller;?>" id="firmaFirmaJefeTaller" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <!-- <input type="text" class="input_field" name="JefeTallerNombre" id="JefeTallerNombre" style="width:100%;" value="<?php if(isset($jefeTaller)) echo $jefeTaller;?>" disabled> -->
                    <?php endif; ?>
                <?php else: ?>
                    <input type="hidden" id="rutaFirmaJefeTaller" name="rutaFirmaJefeTaller" value="<?= set_value('rutaFirmaJefeTaller');?>">
                    <img class="marcoImg" src="<?php if(set_value('rutaFirmaJefeTaller') != "") echo set_value('rutaFirmaJefeTaller');  ?>" id="firmaFirmaJefeTaller" alt="" style="height:2cm;width:4cm;">
                    <br>
                    <!-- <input type="text" class="input_field" name="JefeTallerNombre" id="JefeTallerNombre" style="width:100%;" value="<?= set_value('JefeTallerNombre');?>"> -->
                <?php endif; ?>
                <input type="text" class="input_field" name="JefeTallerNombre" id="JefeTallerNombre" style="width:100%;" value="<?php if(isset($jefeTaller)) echo $jefeTaller;?>" disabled>
                <?php echo form_error('rutaFirmaJefeTaller', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('JefeTallerNombre', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>

            <div class="col-sm-3" align="center">
                NOMBRE Y FIRMA DEL CLIENTE
                <br>
                <?php if (isset($firmaCliente)): ?>
                    <?php if ($firmaCliente == ""): ?>
                        <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?= set_value('rutaFirmaConsumidor');?>">
                        <img class="marcoImg" src="<?php if(set_value('rutaFirmaConsumidor') != "") echo set_value('rutaFirmaConsumidor');  ?>" id="firmaFirmaConsumidor" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <!-- <input type="text" class="input_field nombreCliente" name="nombreConsumidor" id="nombreConsumidor" style="width:100%;" value="<?= set_value('nombreConsumidor');?>"> -->
                    <?php else: ?>
                        <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?php if(isset($firmaCliente)) echo $firmaCliente;?>">
                        <img class="marcoImg" src="<?php if(isset($firmaCliente)) echo base_url().$firmaCliente;?>" id="firmaFirmaConsumidor" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <!-- <input type="text" class="input_field nombreCliente" name="nombreConsumidor" id="nombreConsumidor" style="width:100%;" value="<?php if(isset($nombreCliente)) echo $nombreCliente;?>" disabled> -->
                    <?php endif; ?>
                <?php else: ?>
                    <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?= set_value('rutaFirmaConsumidor');?>">
                    <img class="marcoImg" src="<?php if(set_value('rutaFirmaConsumidor') != "") echo set_value('rutaFirmaConsumidor');  ?>" id="firmaFirmaConsumidor" alt="" style="height:2cm;width:4cm;">
                    <br>
                    <!-- <input type="text" class="input_field nombreCliente" name="nombreConsumidor" id="nombreConsumidor" style="width:100%;" value="<?= set_value('nombreConsumidor');?>"> -->
                <?php endif; ?>
                <input type="text" class="input_field nombreCliente" name="nombreConsumidor" id="nombreConsumidor" style="width:100%;" value="<?php if(isset($nombreCliente)) echo $nombreCliente;?>" disabled>
                <?php echo form_error('rutaFirmaConsumidor', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('nombreConsumidor', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3" align="center">
                <?php if (isset($firmaAsesor)): ?>
                    <?php if ($firmaAsesor == ""): ?>
                        <a id="Asesor_firma" class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white; display:none;"> Firma Asesor</a>
                    <?php endif; ?>
                <?php else: ?>
                    <a id="Asesor_firma" class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white; display:none;"> Firma Asesor </a>
                <?php endif; ?>
            </div>
            <div class="col-sm-3" align="center">
                <?php if (isset($firmaTecnico)): ?>
                    <?php if ($firmaTecnico == ""): ?>
                        <a id="Tecnico_firma" class="cuadroFirma btn btn-primary" data-value="Tecnico" data-target="#firmaDigital" data-toggle="modal" style="color:white; display:none;"> Firma Técnico </a>
                    <?php endif; ?>
                <?php else: ?>
                    <a id="Tecnico_firma" class="cuadroFirma btn btn-primary" data-value="Tecnico" data-target="#firmaDigital" data-toggle="modal" style="color:white; display:none;"> Firma Técnico </a>
                <?php endif; ?> 
            </div>
            <div class="col-sm-3" align="center">
                <?php if (isset($firmaJefeTaller)): ?>
                    <?php if ($firmaJefeTaller == ""): ?>
                        <a id="JefeTaller_firma" class="cuadroFirma btn btn-primary" data-value="JefeTaller" data-target="#firmaDigital" data-toggle="modal" style="color:white; display:none;"> Firma J. de Taller</a>
                    <?php endif; ?>
                <?php else: ?>
                    <a id="JefeTaller_firma" class="cuadroFirma btn btn-primary" data-value="JefeTaller" data-target="#firmaDigital" data-toggle="modal" style="color:white; display:none;"> Firma J. de Taller</a>
                <?php endif; ?>
            </div>
            <div class="col-sm-3" align="center">
                <?php if (isset($firmaCliente)): ?>
                    <?php if ($firmaCliente == ""): ?>
                        <a id="Cliente_firma" class="cuadroFirma btn btn-primary" data-value="Cliente" data-target="#firmaDigital" data-toggle="modal" style="color:white; display:none;"> Firma Cliente</a>
                    <?php endif; ?>
                <?php else: ?>
                    <a id="Cliente_firma" class="cuadroFirma btn btn-primary" data-value="Cliente" data-target="#firmaDigital" data-toggle="modal" style="color:white; display:none;"> Firma Cliente</a>
                <?php endif; ?>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4" align="center">
                <i class="fas fa-spinner cargaIcono"></i>
                <h5 style="color:darkblue;text-align: center;font-weight: bold;" id="msm_envio"></h5>
                <br>
                
                <!-- <input type="button" class="btn btn-primary" name="enviar" id="guardarInspeccion" value="Actualizar inspección" style="display:none;"> -->
                <button type="button" class="btn btn-success" id="guardarInspeccion"  name="button">Actualizar Hoja Multipunto</button>

                <!-- Variable de redirección -->
                <input type="hidden" name="cargaFormulario" id="estatusSlider" value="<?php if(isset($modalidadFormulario)) echo $modalidadFormulario; else echo "3";  ?>">
                <input type="hidden" name="idCita" id="idCita" value="<?php if(isset($idCita)) echo $idCita; else echo "0";  ?>">

                <!-- Para el tecnico no se puede editar si la unidad no se esta trabajando  -->                
                <input type="hidden" name="" id="unidadTrabajando" value="<?php if(isset($unidadTrabajando)) echo $unidadTrabajando; else echo "FALSE";  ?>">

                <input type="hidden" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
                <input type="hidden" id="firma_diagnostico" value="<?php if(isset($firma_diagnostico)) echo $firma_diagnostico; else echo '1'; ?>">
                <input type="hidden" name="idOrden" id="idOrden" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                <input type="hidden" id="formularioHoja" value="MultipuntoHoja">
                <input type="hidden" name="completoFormulario" id="completoFormulario" value="<?php if(isset($completoFormulario)) echo $completoFormulario; else echo "0"; ?>">

                <!-- Panel desde el cual se invoco -->
                <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>">

                <!-- Validamos firmas -->
                <input type="hidden" id="Asesor_firma_2" name="Asesor_firma_2" value="<?php if(isset($firmaAsesor)) if($firmaAsesor != "") echo "1"; else echo "0"; else echo "0"; ?>">
                <input type="hidden" id="Tecnico_firma_2" name="Tecnico_firma_2" value="<?php if(isset($firmaTecnico)) if($firmaTecnico != "") echo "1"; else echo "0"; else echo "0"; ?>">
                <input type="hidden" id="JefeTaller_firma_2" name="JefeTaller_firma_2" value="<?php if(isset($firmaJefeTaller)) if($firmaJefeTaller != "") echo "1"; else echo "0"; else echo "0"; ?>">
                <input type="hidden" id="Cliente_firma_2" name="Cliente_firma_2" value="<?php if(isset($firmaCliente)) if($firmaCliente != "") echo "1"; else echo "0"; else echo "0"; ?>">
            </div>
            <div class="col-md-4" ></div>
        </div>
    </form>
    <br><br>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para cotizacion externa -->
<!-- <div class="modal fade " id="cotizacionModal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Cotización.</h5>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" frameborder="0" id="ventanaOculta" style="z-index:1000;overflow-y: scroll;height:100%;"></iframe>
                </div>
                <br><br>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div> -->

<!-- Formulario de superusuario -->
<div class="modal fade " id="superUsuario" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Ingreso.</h5>
            </div>
            <div class="modal-body" style="font-size:12px;">
                <label for="" style="font-size: 14px;font-weight: initial;">Usuario:</label>
                <br>
                <input type="text" class="input_field" type="text" id="superUsuarioName" value="" style="width:100%;">
                <br><br>
                <label for="" style="font-size: 14px;font-weight: initial;">Password:</label>
                <br>
                <input type="password" class="input_field" type="text" id="superUsuarioPass" value="" style="width:100%;">
                <br><br><br>
                <h4 class="error" style="font-size: 16px;font-weight: initial;" align="center" id="superUsuarioError">:</h4>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="superUsuarioEnvio">Validar</button>
            </div>
        </div>
    </div>
</div>
