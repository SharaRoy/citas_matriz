<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
 
    <head>
        <title>HOJA MULTIPUNTO <?php if(isset($idCita)) echo $idCita;?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="estilos.css" />
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <style>
            @page { 
                sheet-size: A4;
                size: auto; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 5mm; 
                /*margin-top: 5mm; /* <any of the usual CSS values for margins> */
                /*margin-left: 5mm;*/
                /*margin-right: 5mm;*/
                /*margin-bottom: 10mm;*/
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 5mm; /* <any of the usual CSS values for margins> */
                margin-footer: 5mm; /* <any of the usual CSS values for margins> */
            }
        </style>
    </head>
     
    <body>
        <!-- Hoja 1 -->
        <!-- Primera tabla de la hoja-->
        <div>
            <table style="">
                <tr>
                    <td>
                        <h3>HOJA MULTIPUNTOS</h3>
                    </td>
                    <td align="right">
                        <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" style="height:45px;margin-top:0px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="banda" align="center" style="text-align: center;background-color:#340f7b;color:white;">
                        INSPECCIÓN DE VEHÍCULO
                    </td>
                </tr>
                <tr>
                    <!-- Lado derecho -->
                    <td style="max-width:400px !important;font-size:8px;">
                        <table class="encabezado">
                            <tr class="recuadro">
                                <td rowspan="2" style="border-right:2px solid;">
                                    <img src="<?php echo base_url();?>assets/imgs/obligatorio.png" style="max-height:65px;">
                                </td>
                                <td colspan="2" style="border-top: 2px solid;font-size: 12px;">
                                    Fecha <u style="color:darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($fechaCaptura)) echo $fechaCaptura;?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                                <td colspan="2" align="left" style="border-top: 2px solid;border-right: 2px solid;font-size: 12px;">
                                    OR: <u style="color:darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($idCita)) echo $idCita;?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-bottom: 2px solid;font-size: 12px;">
                                    No.Serie (VIN): <u style="color:darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($noSerie)) echo $noSerie;?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                                <td colspan="2" style="border-bottom: 2px solid;border-right: 2px solid;font-size: 12px;">
                                    Orden Intelisis: <u style="color:darkblue;">&nbsp;&nbsp;<?php if(isset($idIntelisis)) echo $idIntelisis;?>&nbsp;&nbsp;</u>
                                </td>
                            </tr>

                            <!-- Separación de recuadros -->
                            <tr>
                                <td colspan="5"> </td>
                            </tr>
                            <tr>
                                <td rowspan="3" style="border-right:2px solid;">
                                    <img src="<?php echo base_url();?>assets/imgs/opcional2.png" style="max-height:65px;">
                                </td>
                                <td colspan="2" style="border-top: 2px solid;font-size: 12px;">
                                    Modelo: <u style="color:darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($modelo)) echo $modelo;?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                                <td colspan="2" style="border-top: 2px solid;border-right: 2px solid;font-size: 12px;" align="left">
                                    No. de torre: <u style="color:darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($torre)) echo $torre;?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-right: 2px solid;font-size: 12px;">
                                    Nombre:<u style="color:darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($nombreDueno)) echo $nombreDueno;?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-bottom: 2px solid;border-right: 2px solid;font-size: 12px;">
                                    Correo electrónico:<u style="color:darkblue;">&nbsp;&nbsp;&nbsp;&nbsp;<?php if(isset($email)) echo $email;?>&nbsp;&nbsp;&nbsp;&nbsp;</u>
                                </td>
                            </tr>

                            <!-- Separación de recuadros -->
                            <tr>
                                <td colspan="5"> </td>
                            </tr>
                            <tr>
                                <td rowspan="14" style="border-right:2px solid;">
                                    <img src="<?php echo base_url();?>assets/imgs/titulo1.png" style="width:17px;">
                                </td>
                                <td colspan="3" align="center" class="banda" style="border-top: 2px solid;text-align: center;background-color:#340f7b;color:white;">
                                    NIVELES DE FLUIDOS
                                </td>
                                <td align="center" style="width:auto;border-top: 2px solid;border-right: 2px solid; border-bottom: 1px groove;font-size: 13px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td style="" style="border-bottom: 1px solid;font-size: 12px;">
                                    &nbsp;Perdida de fluido y/o aceite &nbsp;&nbsp;
                                </td>
                                <td align="left" style="border-bottom: 1px solid;font-size: 12px;">
                                    &nbsp;Si&nbsp;
                                    <?php if (isset($perdidaAceite)): ?>
                                        <?php if (strtoupper($perdidaAceite) == "SI"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td align="left" style="border-bottom: 1px solid;font-size: 12px;">
                                    &nbsp;No&nbsp;
                                    <?php if (isset($perdidaAceite)): ?>
                                        <?php if (strtoupper($perdidaAceite) == "NO"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td align="center" style="border-left:  1px solid;border-bottom: 1px solid;border-right:  1px solid;">
                                    <?php if (isset($cambioAceite)): ?>
                                        <?php if ($cambioAceite == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-right: 2px solid;">
                                    <table>
                                        <tr>
                                            <td style="font-size: 11px;font-weight: bold;">Bien</td>
                                            <td style="font-size: 11px;font-weight: bold;">Llenar</td>
                                            <td style="font-size: 11px;font-weight: bold;"></td>
                                            <td style="font-size: 11px;font-weight: bold;">Bien</td>
                                            <td style="font-size: 11px;font-weight: bold;">Llenar</td>
                                            <td style="font-size: 11px;font-weight: bold;"></td>
                                            <td style="font-size: 11px;font-weight: bold;">Bien</td>
                                            <td style="font-size: 11px;font-weight: bold;">Llenar</td>
                                            <td style="font-size: 11px;font-weight: bold;"></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <?php if (isset($aceiteMotor)): ?>
                                                    <?php if (strtoupper($aceiteMotor) == "BIEN"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center">
                                                <?php if (isset($aceiteMotor)): ?>
                                                    <?php if (strtoupper($aceiteMotor) == "LLENA"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 11px;">
                                                Aceite de motor
                                            </td>
                                            <td align="center" style="padding-top:0px!important;">
                                                <?php if (isset($direccionHidraulica)): ?>
                                                    <?php if (strtoupper($direccionHidraulica) == "BIEN"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="padding:0px;">
                                                <?php if (isset($direccionHidraulica)): ?>
                                                    <?php if (strtoupper($direccionHidraulica) == "LLENA"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Dirección <br> hidráulica
                                            </td>
                                            <td align="center" style="padding:0px;">
                                                <?php if (isset($transmision)): ?>
                                                    <?php if (strtoupper($transmision) == "BIEN"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="padding:0px;">
                                                <?php if (isset($transmision)): ?>
                                                    <?php if (strtoupper($transmision) == "LLENA"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 11px;">
                                                Transmisión (si está equipada con bayoneta de medición)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding:0px;">
                                                <?php if (isset($fluidoFrenos)): ?>
                                                    <?php if (strtoupper($fluidoFrenos) == "BIEN"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="padding:0px;">
                                                <?php if (isset($fluidoFrenos)): ?>
                                                    <?php if (strtoupper($fluidoFrenos) == "LLENA"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 11px;">
                                                Deposito fluido<br> frenos
                                            </td>
                                            <td align="center" style="padding:0px;">
                                                <?php if (isset($limpiaparabrisas)): ?>
                                                    <?php if (strtoupper($limpiaparabrisas) == "BIEN"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="padding:0px;">
                                                <?php if (isset($limpiaparabrisas)): ?>
                                                    <?php if (strtoupper($limpiaparabrisas) == "LLENA"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 11px;">
                                                Limpiaparabrisas
                                            </td>
                                            <td align="center">
                                                <?php if (isset($refrigerante)): ?>
                                                    <?php if (strtoupper($refrigerante) == "BIEN"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center">
                                                <?php if (isset($refrigerante)): ?>
                                                    <?php if (strtoupper($refrigerante) == "LLENA"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_gris.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 11px;">
                                                Deposito recuperación refrigerante
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="banda" style="text-align: center;background-color:#340f7b;color:white;">PLUMAS LIMPIAPARABRISAS</td>
                                <td align="center" style="border-right: 2px solid;border-top: 1px solid;border-bottom: 1px solid;font-size: 13px;">CAMBIADO</td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="font-size: 12px;border:0.5px solid white;font-size: 12px;">
                                                Prueba de Limpiaparabrisas<br> realizada:&nbsp;&nbsp;
                                            </td>
                                            <td style="font-size: 12px;border:0.5px solid white;">
                                                Si&nbsp;
                                                <?php if (isset($plumasPruebas)): ?>
                                                    <?php if (strtoupper($plumasPruebas) == "SI"): ?> 
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;border:0.5px solid white;">
                                                No&nbsp;
                                                <?php if (isset($plumasPruebas)): ?>
                                                    <?php if (strtoupper($plumasPruebas) == "NO"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;border:0.5px solid white;">
                                                <?php if (isset($plumas)): ?>
                                                    <?php if (strtoupper($plumas) == "SI"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;border:0.5px solid white;">
                                                <?php if (isset($plumas)): ?>
                                                    <?php if (strtoupper($plumas) == "NO"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?> 
                                            </td>
                                            <td style="font-size: 12px;border:0.5px solid white;font-size: 12px;">
                                                Plumas <br>Limpiaparabrisas
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-right: 2px solid;border-left: 1px solid;border-bottom: 1px solid;">
                                    <?php if (isset($plumasCambio)): ?>
                                        <?php if ($plumasCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="banda" style="border-right: 2px solid;text-align: center;background-color:#340f7b;color:white;">SISTEMAS / COMPONENTES</td>
                            </tr>
                            <tr>
                                <td colspan="3" class="subtema" align="center" style="text-align: center;background-color:#8d97a1;color:white;font-size: 13px;">LUCES / PARABRISAS</td>
                                <td align="center" style="border-right: 2px solid black;border-bottom: 1px solid black;font-size: 13px;">CAMBIADO</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="border-bottom:  1px solid;">
                                    <table style="border:  0.5px solid white; width: 100%;">
                                        <tr>
                                            <td style="font-size: 12px;">
                                                Si&nbsp;
                                                <?php if (isset($lucesClaxon)): ?>
                                                    <?php if ($lucesClaxon == "Si"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                &nbsp;No
                                                <?php if (isset($lucesClaxon)): ?>
                                                    <?php if ($lucesClaxon == "No"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                &nbsp;Funcionamiento de claxon, luces interiores, luces <br>exteriores luces de giro, luces de emergencia y freno.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-right: 2px solid;border-left:  1px solid;border-bottom:  1px solid;">
                                    <?php if (isset($lucesClaxonCambio)): ?>
                                        <?php if ($lucesClaxonCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="">
                                    <table style="border:  0.5px solid white; width: 100%;">
                                        <tr>
                                            <td style="font-size: 12px;">
                                                Si&nbsp;
                                                <?php if (isset($grietasParabrisas)): ?>
                                                    <?php if ($grietasParabrisas == "Si"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                &nbsp;No
                                                <?php if (isset($grietasParabrisas)): ?>
                                                    <?php if ($grietasParabrisas == "No"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                &nbsp;Grietas, roturas y picaduras de parabrisas.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-right: 2px solid;border-left:  1px solid;border-bottom:  1px solid;border-top:  1px solid;">
                                    <?php if (isset($grietasCambio)): ?>
                                        <?php if ($grietasCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="banda" style="text-align: center;background-color:#340f7b;color:white;font-size: 13px;">BATERÍA</td>
                                <td align="center" style="border-right: 2px solid;border-bottom:  1px solid;font-size: 13px;">CAMBIADO</td>
                            </tr>
                            <tr>
                                <td colspan="2" rowspan="2" align="center" class="contenedorImgFondo">
                                    <img src="<?php echo base_url().'assets/imgs/nivel_bateria.png'; ?>" style="width:255px; height:50px;">
                                    <?php if (isset($bateria)): ?>
                                        <br>
                                        <img src="<?php echo base_url().'assets/imgs/img_icon/barra_vertical.png'; ?>" style="width:15px; height:20px;margin-left:<?php echo $bateria; ?>;margin-top: -5px;" class="centradoBarra">
                                    <?php endif; ?>
                                </td>
                                <td style="font-size: 14px;" align="center">
                                    Estado de la batería
                                </td>
                                <td align="center" style="border-right: 2px solid;border-left:  1px solid;border-top:  1px solid;border-bottom:  1px solid;">
                                    <?php if (isset($bateriaCambio)): ?>
                                        <?php if ($bateriaCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:23px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="width: 60%;">
                                        <tr>
                                            <td style="vertical-align: middle;" align="right">
                                                <?php if (isset($bateriaEstado)): ?>
                                                    <?php if ($bateriaEstado == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:25px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="vertical-align: middle;" align="right">
                                                <?php if (isset($bateriaEstado)): ?>
                                                    <?php if ($bateriaEstado == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:25px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="vertical-align: middle;" align="right">
                                                <?php if (isset($bateriaEstado)): ?>
                                                    <?php if ($bateriaEstado == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:25px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-right: 2px solid;"></td>
                            </tr>
                            <tr>
                                <td rowspan="2" style="border-bottom: 2px solid;font-size: 12px;">
                                    Corriente de arranque en frío<br> especificaciones de fabrica.
                                </td>
                                <td align="center" style="border-bottom: 0.5px solid;font-size: 12px;color: darkblue;">
                                    <?php if(isset($frio)) echo $frio; else echo "0";?>
                                </td>
                                <td rowspan="2" style="border-bottom: 2px solid;font-size: 12px;">
                                    Corriente de arranque en<br> frío real
                                </td>
                                <td align="center" style="border-right: 2px solid;border-bottom: 0.5px solid;font-size: 12px;color: darkblue;">
                                    <?php if(isset($frioReal)) echo $frioReal; else echo "0";?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="border-bottom: 2px solid;font-size: 13px;">
                                    <label>CCA</label>
                                </td>
                                <td align="center" style="border-right: 2px solid;border-bottom: 2px solid;font-size: 13px;">
                                    <label>CCA</label>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <!-- Lado izquierdo -->
                    <td style="width:50%;font-size:8px;">
                        <table style="margin-left:5px;width: 100%;">
                            <tr>
                                <td colspan="4">
                                    <table class="" style="width: 100%;">
                                        <tr>
                                            <td style="font-size: 14px;">SÍMBOLO</td>
                                            <td colspan="5" style="font-size: 12px;">
                                                <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" style="width:15px;">&nbsp;
                                                Puede contribuir a la eficiencia del vehículo y la protección del medio<br>ambiente.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:25px;" alt="">
                                            </td>
                                            <td style="font-size: 12px;">
                                                &nbsp;Verificado y <br>aprobado
                                            </td>
                                            <td align="center">
                                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:25px;" alt="">
                                            </td>
                                            <td style="font-size: 12px;">
                                                &nbsp;Puede requerir <br>atención en el futuro
                                            </td>
                                            <td align="center">
                                                <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:25px;" alt="">
                                            </td>
                                            <td style="font-size: 12px;">
                                                &nbsp;Requiere atención <br>inmediata
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-top: 2px solid;"></td>
                                <td rowspan="17" align="right" style="border-left: 2px solid;">
                                  <img src="<?php echo base_url();?>assets/imgs/titulo2.png" style="width:15px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="subtema" align="center" style="border-left:2px solid;text-align: center;background-color:#8d97a1;color:white;font-size: 13px; width: 200px;">
                                    BANDAS / MANGUERAS
                                </td>
                                <td align="center" style="border-bottom:0.5px solid;font-size: 13px;width: 20px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;border-bottom:0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <?php if (isset($sistemaCalefaccion)): ?>
                                                    <?php if ($sistemaCalefaccion == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($sistemaCalefaccion)): ?>
                                                    <?php if ($sistemaCalefaccion == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($sistemaCalefaccion)): ?>
                                                    <?php if ($sistemaCalefaccion == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Pérdidas y/o daños en el sistema de calefacción, <br>ventilación y aire acondicionado y en mangueras/cables.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left:0.5px solid;border-bottom:0.5px solid;">
                                    <?php if (isset($sistemaCalefaccionCambio)): ?>
                                        <?php if ($sistemaCalefaccionCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;border-bottom:0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <?php if (isset($sisRefrigeracion)): ?>
                                                    <?php if ($sisRefrigeracion == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                            </td>
                                            <td>
                                                <?php if (isset($sisRefrigeracion)): ?>
                                                    <?php if ($sisRefrigeracion == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Sistema de refrigeración del motor, radiador, mangueras<br>y abrazaderas.<br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left:0.5px solid;border-bottom:0.5px solid;">
                                    <?php if (isset($sisRefrigeracionCambio)): ?>
                                        <?php if ($sisRefrigeracionCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($bandas)): ?>
                                                    <?php if ($bandas == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($bandas)): ?>
                                                    <?php if ($bandas == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Bandas(s).<br>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left:0.5px solid;">
                                    <?php if (isset($bandasCambio)): ?>
                                        <?php if ($bandasCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="subtema" align="center" style="border-left:2px solid;text-align: center;background-color:#8d97a1;color:white;font-size: 13px;">
                                    SISTEMA DE FRENOS
                                </td>
                                <td align="center" style="border-bottom:0.5px solid;border-top:0.5px solid;font-size: 13px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <?php if (isset($sisFrenos)): ?>
                                                    <?php if ($sisFrenos == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($sisFrenos)): ?>
                                                    <?php if ($sisFrenos == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($sisFrenos)): ?>
                                                    <?php if ($sisFrenos == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Sistema de frenos (incluye mangueras y freno de mano) &nbsp;
                                                <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" class="leaf" style="width:13px;float:left;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left:0.5px solid;">
                                    <?php if (isset($sisFrenosCambio)): ?>
                                        <?php if ($sisFrenosCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="subtema" align="center" style="border-left: 2px solid;text-align: center;background-color:#8d97a1;color:white;font-size: 13px;">
                                    DIRECCIÓN Y SUSPENSIÓN
                                </td>
                                <td align="center" style="border-bottom:0.5px solid;border-top:0.5px solid;font-size: 13px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;border-bottom:0.5px solid;">
                                    <table>
                                        <tr>
                                            <td>
                                                <?php if (isset($suspension)): ?>
                                                    <?php if ($suspension == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($suspension)): ?>
                                                    <?php if ($suspension == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($suspension)): ?>
                                                    <?php if ($suspension == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Pérdidas y/o daños en amortiguadores/puntales y otros<br>componentes de la suspensión.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-bottom:0.5px solid;border-left:0.5px solid;">
                                    <?php if (isset($suspensionCambio)): ?>
                                        <?php if ($suspensionCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;border-bottom:0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <?php if (isset($varillajeDireccion)): ?>
                                                    <?php if ($varillajeDireccion == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($varillajeDireccion)): ?>
                                                    <?php if ($varillajeDireccion == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($varillajeDireccion)): ?>
                                                    <?php if ($varillajeDireccion == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Dirección, varillaje de la dirección y juntas de rótula (visual).
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-bottom:0.5px solid;border-left:0.5px solid;">
                                    <?php if (isset($varillajeDireccionCambio)): ?>
                                        <?php if ($varillajeDireccionCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <?php if (isset($sisEscape)): ?>
                                                    <?php if ($sisEscape == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($sisEscape)): ?>
                                                    <?php if ($sisEscape == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($sisEscape)): ?>
                                                    <?php if ($sisEscape == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Sistema de escape y escudo de calor (pérdidas, daño, piezas <br>sueltas).&nbsp;
                                                <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" class="leaf" style="width:13px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left:0.5px solid;">
                                    <?php if (isset($sisEscapeCambio)): ?>
                                        <?php if ($sisEscapeCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="subtema" align="center" style="border-left:2px solid;text-align: center;background-color:#8d97a1;color:white;font-size: 13px;">
                                    TREN MOTRIZ
                                </td>
                                <td align="center" style="border-bottom:0.5px solid;border-top:0.5px solid;font-size: 13px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;border-bottom:0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <?php if (isset($funEmbrague)): ?>
                                                    <?php if ($funEmbrague == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($funEmbrague)): ?>
                                                    <?php if ($funEmbrague == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($funEmbrague)): ?>
                                                    <?php if ($funEmbrague == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Funcionamiento del embrague (si está equipado).
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left:0.5px solid;">
                                    <?php if (isset($funEmbragueCambio)): ?>
                                        <?php if ($funEmbragueCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="" style="border-left:2px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td>
                                                <?php if (isset($flechaCardan)): ?>
                                                    <?php if ($flechaCardan == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($flechaCardan)): ?>
                                                    <?php if ($flechaCardan == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <?php if (isset($flechaCardan)): ?>
                                                    <?php if ($flechaCardan == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 12px;">
                                                Transmisión, flecha cardán y lubricación (si necesita).
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;">
                                    <?php if (isset($flechaCardanCambio)): ?>
                                        <?php if ($flechaCardanCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="banda" align="center" style="border-left: 2px solid;text-align: center;background-color:#340f7b;color:white;">
                                    PARTE INFERIOR DEL VEHÍCULO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" style="border-bottom: 2px solid;border-left: 2px solid;">
                                    <?php if (isset($defectosImg)): ?>
                                        <?php if ($defectosImg != ""): ?>
                                            <img src="<?php echo base_url().$defectosImg; ?>" style="width:200px;">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/car_ext.png'; ?>" style="width:200px;">
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <img src="<?php echo base_url().'assets/imgs/car_ext.png'; ?>" style="width:200px;">
                                    <?php endif; ?>
                                </td>
                                <td colspan="2" style="border-bottom: 2px solid;">
                                    Anote en el diagrama <br>todos los daños o defectos<br>detectados en la parte<br>
                                    inferior de la carrocería<br>durante la revisión en el taller.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
            <!-- Segunda tabla de la hoja-->
            <table style="">
                <tr>
                    <td colspan="5" class="banda" align="center" style="border-top: 2px solid; border-left: 2px solid;text-align: center;background-color:#340f7b;color:white;font-size: 11px;">
                        DESGASTE DE NEUMÁTICO / FRENO
                    </td>
                    <td rowspan="4" align="right" style="border-left: 2px solid;">
                        <img src="<?php echo base_url();?>assets/imgs/titulo2.png" style="width:15px;">
                    </td>
                </tr>
                <tr>
                    <td align="left" class="banda" style="border-left: 2px solid;font-size: 10px;text-align: center;background-color:#340f7b;color:white;">PROFUNDIDAD DE DIBUJO</td>
                    <td align="center" style="background-color:#1f661f;color:white;font-size: 10px;" colspan="2">5 mm y mayor</td>
                    <td align="center" style="background-color:#e6b019;font-size: 10px;">3 a 5 mm</td>
                    <td align="center" style="background-color:#e01a14;color:white;font-size: 10px;">2 mm y menor</td>
                </tr>
                <tr>
                    <td align="left" class="banda" style="border-left: 2px solid;font-size: 10px;text-align: center;background-color:#340f7b;color:white;">MEDIDA DE BALATAS</td>
                    <td align="center" style="background-color:#1f661f; color:white;font-size: 10px;" colspan="2">Mas de 8 mm</td>
                    <td align="center" style="background-color:#e6b019;font-size: 10px;">4 a 6 mm</td>
                    <td align="center" style="background-color:#e01a14;color:white;font-size: 10px;"> 3 mm o menos</td>
                </tr>
                <tr>
                    <!-- Primera columna -->
                    <td rowspan="2" style="border-left: 2px solid;border-bottom: 2px solid;width: 200px;">
                        <table style="position:relative; padding-top:0px;padding-left:0px;width: 200px; ">
                            <tr style="background-color:#8d97a1;">
                                <td style="width: 20px;">
                                    <?php if (isset($medicionesFrenos)): ?>
                                        <?php if ($medicionesFrenos == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="font-size: 11px;">
                                    No se tomaron mediciones de los<br>frenos en esta visita de servicio.
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td style="width: 20px;">
                                    <?php if (isset($indicadorAceite)): ?>
                                        <?php if ($indicadorAceite == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                <td style="font-size: 11px;">
                                    Reinicio del indicador<br>de cambio de aceite.
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size: 11px;">
                                    Comentarios:<br><br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size: 11px;height: 300px;color: darkblue;">
                                    <?php if (isset($comentarios)): ?>
                                        <?php echo $comentarios; ?>
                                    <?php endif; ?>
                                    <?php if (isset($comentario)): ?>
                                        <?php echo "<br>".$comentario; ?>
                                    <?php endif; ?>
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="subtema" align="center" style="text-align: center;background-color:#8d97a1;color:white;">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width:120px;font-size: 11px;">
                                    Sistema.<br>
                                    <p align="justify" style="text-align: justify;color: darkblue;">
                                        <?php if(isset($sistema)) echo $sistema;?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <!-- Segunda columna -->
                    <td colspan="2" rowspan="2" style="font-size:9px;border-bottom: 2px solid; width: 350px;">
                        <table style="">
                            <tr>
                                <td colspan="2" class="subtema" style="text-align: center;background-color:#8d97a1;color:white;font-size: 10px;">
                                    FRENTE IZQUIERDO &nbsp;
                                </td>
                                <td colspan="2" style="border-bottom:0.5px solid;border-top:0.5px solid;">
                                    <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" class="leaf" style="width:13px;">
                                </td>
                                <td align="center" style="border:0.5px solid;font-size: 10px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left:0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoFI)): ?>
                                                    <?php if ($pneumaticoFI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoFI)): ?>
                                                    <?php if ($pneumaticoFI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoFI)): ?>
                                                    <?php if ($pneumaticoFI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Profundidad de dibujo del neumático.
                                                <u style="color:darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($pneumaticoFImm)) echo $pneumaticoFImm; else echo "0";?></strong>&nbsp;&nbsp;</u> mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left:0.5px solid;border-right: 0.5px solid;width: 20px;">
                                    <?php if (isset($pneumaticoFIcambio)): ?>
                                        <?php if ($pneumaticoFIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left:0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoFI)): ?>
                                                    <?php if ($dNeumaticoFI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoFI)): ?>
                                                    <?php if ($dNeumaticoFI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoFI)): ?>
                                                    <?php if ($dNeumaticoFI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Patrón de desgaste/daño del neumático.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left:0.5px solid;border-right: 0.5px solid;width: 20px;">
                                    <?php if (isset($dNeumaticoFIcambio)): ?>
                                        <?php if ($dNeumaticoFIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoFI)): ?>
                                                    <?php if ($presionInfladoFI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoFI)): ?>
                                                    <?php if ($presionInfladoFI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoFI)): ?>
                                                    <?php if ($presionInfladoFI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Presión de inflado a PSI según recomendación del fabricante.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($presionInfladoFIpsi)) echo $presionInfladoFIpsi;?></strong>&nbsp;&nbsp;</u> PSI
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border:0.5px solid;border-right: 0.5px solid;width: 20px;">
                                    <?php if (isset($presionInfladoFIcambio)): ?>
                                        <?php if ($presionInfladoFIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td colspan="4">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataFI)): ?>
                                                    <?php if ($espesoBalataFI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataFI)): ?>
                                                    <?php if ($espesoBalataFI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataFI)): ?>
                                                    <?php if ($espesoBalataFI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Espesor de balatas.
                                                <u style="color:darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($espesorBalataFImm)) echo $espesorBalataFImm; else echo "0";?></strong>&nbsp;&nbsp;</u> mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <?php if (isset($espesorBalataFIcambio)): ?>
                                        <?php if ($espesorBalataFIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td colspan="4">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoFI)): ?>
                                                    <?php if ($espesorDiscoFI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoFI)): ?>
                                                    <?php if ($espesorDiscoFI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoFI)): ?>
                                                    <?php if ($espesorDiscoFI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Espesor de disco.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($espesorDiscoFImm)) echo $espesorDiscoFImm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <?php if (isset($espesorDiscoFIcambio)): ?>
                                        <?php if ($espesorDiscoFIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="subtema" align="center" style="border: 0.5px solid;text-align: center;background-color:#8d97a1;color:white;font-size: 10px;">
                                    PARTE TRASERA IZQUIERDO &nbsp;
                                </td>
                                <td colspan="2" style="border: 0.5px solid;">
                                    <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" class="leaf" style="width:13px;">
                                </td>
                                <td align="center" style="border: 0.5px solid;border-right: 0.5px solid;font-size: 10px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoTI)): ?>
                                                    <?php if ($pneumaticoTI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoTI)): ?>
                                                    <?php if ($pneumaticoTI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoTI)): ?>
                                                    <?php if ($pneumaticoTI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Profundidad de dibujo del neumático.
                                                <u style="color:darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($pneumaticoTImm)) echo $pneumaticoTImm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;border-right: 0.5px solid;">
                                    <?php if (isset($pneumaticoTIcambio)): ?>
                                        <?php if ($pneumaticoTIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoTI)): ?>
                                                    <?php if ($dNeumaticoTI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoTI)): ?>
                                                    <?php if ($dNeumaticoTI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoTI)): ?>
                                                    <?php if ($dNeumaticoTI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Patrón de desgaste/daño del neumático.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;border-right: 0.5px solid;">
                                    <?php if (isset($dNeumaticoTIcambio)): ?>
                                        <?php if ($dNeumaticoTIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;border-top: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoTI)): ?>
                                                    <?php if ($presionInfladoTI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoTI)): ?>
                                                    <?php if ($presionInfladoTI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoTI)): ?>
                                                    <?php if ($presionInfladoTI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Presión de inflado a PSI según recomendación del fabricante.
                                                <u style="color:darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($presionInfladoTIpsi)) echo $presionInfladoTIpsi;?></strong>&nbsp;&nbsp;</u>PSI
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;border-top: 0.5px solid;border-right: 0.5px solid;">
                                    <?php if (isset($presionInfladoTIcambio)): ?>
                                        <?php if ($presionInfladoTIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td colspan="4">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataTI)): ?>
                                                    <?php if ($espesoBalataTI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataTI)): ?>
                                                    <?php if ($espesoBalataTI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataTI)): ?>
                                                    <?php if ($espesoBalataTI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Espesor de balatas.
                                                <u style="color:darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($espesorBalataTImm)) echo $espesorBalataTImm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <?php if (isset($espesorBalataTIcambio)): ?>
                                        <?php if ($espesorBalataTIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td colspan="4">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoTI)): ?>
                                                    <?php if ($espesorDiscoTI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoTI)): ?>
                                                    <?php if ($espesorDiscoTI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoTI)): ?>
                                                    <?php if ($espesorDiscoTI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Espesor de disco.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($espesorDiscoTImm)) echo $espesorDiscoTImm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <?php if (isset($espesorDiscoTIcambio)): ?>
                                        <?php if ($espesorDiscoTIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($diametroTamborTI)): ?>
                                                    <?php if ($diametroTamborTI == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($diametroTamborTI)): ?>
                                                    <?php if ($diametroTamborTI == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($diametroTamborTI)): ?>
                                                    <?php if ($diametroTamborTI == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Diámetro de tambor.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($diametroTamborTImm)) echo $diametroTamborTImm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border: 0.5px solid;border-right: 0.5px solid;">
                                    <?php if (isset($diametroTamborTIcambio)): ?>
                                        <?php if ($diametroTamborTIcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" class="subtema" align="left" style="text-align: center;background-color:#8d97a1;color:white;font-size: 13px;font-weight: bold;">
                                    DIAGNÓSTICO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="font-size: 11px;">
                                    Componente.<br>
                                    <p align="justify" style="text-align: justify;color: darkblue;">
                                        <?php if(isset($componente)) echo $componente;?>
                                    </p>
                                    <br>
                                </td>
                                <td colspan="3" style="font-size: 11px;">
                                    Causa raíz.<br>
                                    <p align="justify" style="text-align: justify;color: darkblue;">
                                        <?php if(isset($causaRaiz)) echo $causaRaiz;?>
                                    </p>
                                    <br>
                                </td>
                            </tr>
                        </table>
                    </td>

                    <!-- Tercera columana -->
                    <td colspan="2" style="width:50%;font-size:9px;padding-left:5px; width: 350px;">
                        <table style="width: 100%;">
                            <tr>
                                <td colspan="2" class="subtema" style="text-align: center;background-color:#8d97a1;color:white;font-size: 10px;">
                                    FRENTE DERECHO &nbsp;
                                </td>
                                <td colspan="2" style="border-top: 0.5px solid;border-bottom: 0.5px solid;">
                                    <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" class="leaf" style="width:13px;">
                                </td>
                                <td align="center" style="border-top: 0.5px solid;border-bottom: 0.5px solid;border-left: 0.5px solid;font-size: 10px;width: 20px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoFD)): ?>
                                                    <?php if ($pneumaticoFD == "Aprobado"): ?>
                                                         <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoFD)): ?>
                                                    <?php if ($pneumaticoFD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoFD)): ?>
                                                    <?php if ($pneumaticoFD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Profundidad de dibujo del neumático.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($pneumaticoFDmm)) echo $pneumaticoFDmm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;">
                                    <?php if (isset($pneumaticoFDcambio)): ?>
                                        <?php if ($pneumaticoFDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoFD)): ?>
                                                    <?php if ($dNeumaticoFD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoFD)): ?>
                                                    <?php if ($dNeumaticoFD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoFD)): ?>
                                                    <?php if ($dNeumaticoFD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Patrón de desgaste/daño del neumático.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;">
                                    <?php if (isset($dNeumaticoFDcambio)): ?>
                                        <?php if ($dNeumaticoFDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoFD)): ?>
                                                    <?php if ($presionInfladoFD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoFD)): ?>
                                                    <?php if ($presionInfladoFD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoFD)): ?>
                                                    <?php if ($presionInfladoFD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Presión de inflado a PSI según recomendación del fabricante.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($presionInfladoFDpsi)) echo $presionInfladoFDpsi;?></strong>&nbsp;&nbsp;</u>PSI
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border: 0.5px solid;">
                                    <?php if (isset($presionInfladoFDcambio)): ?>
                                        <?php if ($presionInfladoFDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td colspan="4">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataFD)): ?>
                                                    <?php if ($espesoBalataFD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataFD)): ?>
                                                    <?php if ($espesoBalataFD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataFD)): ?>
                                                    <?php if ($espesoBalataFD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Espesor de balatas.
                                                <u style="color:darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($espesorBalataFDmm)) echo $espesorBalataFDmm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <?php if (isset($espesorBalataFDcambio)): ?>
                                        <?php if ($espesorBalataFDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td colspan="4">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoFD)): ?>
                                                    <?php if ($espesorDiscoFD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoFD)): ?>
                                                    <?php if ($espesorDiscoFD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoFD)): ?>
                                                    <?php if ($espesorDiscoFD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Espesor de disco.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($espesorDiscoFDmm)) echo $espesorDiscoFDmm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <?php if (isset($espesorDiscoFDcambio)): ?>
                                        <?php if ($espesorDiscoFDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="subtema" align="center" style="text-align: center;background-color:#8d97a1;color:white;font-size: 10px;">
                                    PARTE TRASERA DERECHO &nbsp;
                                </td>
                                <td colspan="2" style="border-top: 0.5px solid;border-bottom: 0.5px solid;">
                                    <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" class="leaf" style="width:13px;">
                                </td>
                                <td align="center" style="border-top: 0.5px solid;border-bottom: 0.5px solid;border-left: 0.5px solid;font-size: 10px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoTD)): ?>
                                                    <?php if ($pneumaticoTD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoTD)): ?>
                                                    <?php if ($pneumaticoTD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($pneumaticoTD)): ?>
                                                    <?php if ($pneumaticoTD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Profundidad de dibujo del<br>neumático.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($pneumaticoTDmm)) echo $pneumaticoTDmm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;">
                                    <?php if (isset($pneumaticoTDcambio)): ?>
                                        <?php if ($pneumaticoTDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoTD)): ?>
                                                    <?php if ($dNeumaticoTD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoTD)): ?>
                                                    <?php if ($dNeumaticoTD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($dNeumaticoTD)): ?>
                                                    <?php if ($dNeumaticoTD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Patrón de desgaste/daño del neumático.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;">
                                    <?php if (isset($dNeumaticoTDcambio)): ?>
                                        <?php if ($dNeumaticoTDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoTD)): ?>
                                                    <?php if ($presionInfladoTD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoTD)): ?>
                                                    <?php if ($presionInfladoTD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presionInfladoTD)): ?>
                                                    <?php if ($presionInfladoTD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Presión de inflado a PSI según recomendación del fabricante.
                                                <u style="color:darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($presionInfladoTDpsi)) echo $presionInfladoTDpsi;?></strong>&nbsp;&nbsp;</u>PSI
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border: 0.5px solid;">
                                    <?php if (isset($presionInfladoTDcambio)): ?>
                                        <?php if ($presionInfladoTDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td colspan="4">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataTD)): ?>
                                                    <?php if ($espesoBalataTD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataTD)): ?>
                                                    <?php if ($espesoBalataTD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesoBalataTD)): ?>
                                                    <?php if ($espesoBalataTD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Espesor de balatas.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($espesorBalataTDmm)) echo $espesorBalataTDmm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <?php if (isset($espesorBalataTDcambio)): ?>
                                        <?php if ($espesorBalataTDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr style="background-color:#8d97a1;">
                                <td colspan="4">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoTD)): ?>
                                                    <?php if ($espesorDiscoTD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoTD)): ?>
                                                    <?php if ($espesorDiscoTD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($espesorDiscoTD)): ?>
                                                    <?php if ($espesorDiscoTD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Espesor de disco.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($espesorDiscoTDmm)) echo $espesorDiscoTDmm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center">
                                    <?php if (isset($espesorDiscoTDcambio)): ?>
                                        <?php if ($espesorDiscoTDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_gris.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($diametroTamborTD)): ?>
                                                    <?php if ($diametroTamborTD == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($diametroTamborTD)): ?>
                                                    <?php if ($diametroTamborTD == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($diametroTamborTD)): ?>
                                                    <?php if ($diametroTamborTD == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Diámetro de tambor.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($diametroTamborTDmm)) echo $diametroTamborTDmm; else echo "0";?></strong>&nbsp;&nbsp;</u>mm
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;">
                                    <?php if (isset($diametroTamborTDcambio)): ?>
                                        <?php if ($diametroTamborTDcambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="subtema" align="center" style="text-align: center;background-color:#8d97a1;color:white;font-size: 10px;">
                                    NEUMÁTICO DE REFACCIÓN &nbsp;
                                </td>
                                <td colspan="2" style="border-top:0.5px solid;border-bottom:0.5px solid; ">
                                    <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" class="leaf" style="width:13px;">
                                </td>
                                <td align="center" style="border-top:0.5px solid;border-bottom:0.5px solid;border-left:0.5px solid; font-size: 10px;">
                                    CAMBIADO
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border-left: 0.5px solid;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 20px;">
                                                <?php if (isset($presion)): ?>
                                                    <?php if ($presion == "Aprobado"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_verde.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_verde.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presion)): ?>
                                                    <?php if ($presion == "Futuro"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_amarillo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_amarillo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="width: 20px;">
                                                <?php if (isset($presion)): ?>
                                                    <?php if ($presion == "Inmediato"): ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_rojo.png'; ?>" style="float:left;width:20px;" alt="">
                                                    <?php else: ?>
                                                        <img src="<?php echo base_url().'assets/imgs/img_icon/check_bg_rojo.png'; ?>" style="float:left;width:15px;" alt="">
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td style="font-size: 10px;">
                                                Presión de inflado establecida en.
                                                <u style="color: darkblue;"><strong>&nbsp;&nbsp;<?php if(isset($presionPSI)) echo $presionPSI; else echo "0";?></strong>&nbsp;&nbsp;</u>PSI
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="border-left: 0.5px solid;">
                                    <?php if (isset($presionCambio)): ?>
                                        <?php if ($presionCambio == "1"): ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check_bco.png'; ?>" style="float:left;width:20px;" alt="">
                                        <?php else: ?>
                                            <img src="<?php echo base_url().'assets/imgs/img_icon/check.png'; ?>" style="float:left;width:15px;" alt="">
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                            </tr> 
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="border-top: 2px solid;border-left: 2px solid;font-size: 11px;">
                        <?=$this->config->item('base_hm_texto')?>
                        TEL. <?=SUCURSAL_TEL?>
                        <br><span style="color:darkblue;"><?=SUCURSAL_WEB?></span><br>
                        Un sitio para todas las necesidades de su vehículo.
                    </td>
                </tr>
            </table>
        </div>
        
        <!-- Apartado de firmas -->
        <div>
            <table style="width:100%; font-size: 9px;">
                <tr>
                    <td style="width:200px;margin-left:10px; font-size:10px;color: darkblue;" align="center">
                        <?php if (isset($firmaAsesor)): ?>
                            <?php if ($firmaAsesor != ""): ?>
                                <img src="<?php echo base_url().$firmaAsesor; ?>" alt="" style="width:90px;height:40px;">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:90px;height:40px;">
                        <?php endif; ?>
                        <br>
                        <?php if(isset($asesor)) echo $asesor;?>
                    </td>
                    <td style="width:200px;margin-left:10px; font-size:10px;color: darkblue;" align="center">
                        <?php if (isset($firmaTecnico)): ?>
                            <?php if ($firmaTecnico != ""): ?>
                                <img src="<?php echo base_url().$firmaTecnico; ?>" alt="" style="width:90px;height:40px;">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:90px;height:40px;">
                        <?php endif; ?>
                        <br>
                        <?php if(isset($tecnico)) echo $tecnico;?>
                    </td>
                    <td style="width:200px;margin-left:10px; font-size:10px;color: darkblue;" align="center">
                        <?php if (isset($firmaJefeTaller)): ?>
                            <?php if ($firmaJefeTaller != ""): ?>
                                <img src="<?php echo base_url().$firmaJefeTaller; ?>" alt="" style="width:90px;height:40px;">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:90px;height:40px;">
                        <?php endif; ?>
                        <br>
                        <?php if(isset($jefeTaller)) echo $jefeTaller;?>
                    </td>
                    <td style="width:200px;margin-left:10px; font-size:10px;color: darkblue;" align="center">
                        <?php if (isset($firmaCliente)): ?>
                            <?php if ($firmaCliente != ""): ?>
                                <img src="<?php echo base_url().$firmaCliente; ?>" alt="" style="width:90px;height:40px;">
                            <?php endif; ?>
                        <?php else: ?>
                            <img src="<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" alt="" style="width:90px;height:40px;">
                        <?php endif; ?>
                        <br>
                        <?php if(isset($nombreCliente)) echo $nombreCliente;?>
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 0.5px solid #8d97a1; font-size:10px;" align="center">
                        NOMBRE Y FIRMA DEL ASESOR
                    </td>
                    <td style="border-top: 0.5px solid #8d97a1; font-size:10px;" align="center">
                        NOMBRE Y FIRMA DEL TÉCNICO
                    </td>
                    <td style="border-top: 0.5px solid #8d97a1; font-size:10px;" align="center">
                        NOMBRE Y FIRMA DEL JEFE DE TALLER
                    </td>
                    <td style="border-top: 0.5px solid #8d97a1; font-size:10px;" align="center">
                        NOMBRE Y FIRMA DEL CLIENTE
                    </td>
                </tr>
            </table>
        </div>
        <br>
        
        <!-- Hoja 2 -->
        <br>
        <div style="margin-top: 25px;">
            <table style="margin-bottom: 60px; margin-left:65px;margin-top:60px;border: 0.5px solid #5bc0de;padding-left:20px;padding-right:20px;">
                <tr>
                    <td style="width:560px;" align="center">
                        <h4> <strong> Instrucciones para completar la Hoja Multipuntos </strong> </h4>
                    </td>
                </tr>
                <tr>
                    <td style="width:560px;font-size:13px;">
                        <strong>La importancia de los códigos de estado rojo, amarillo y verde.</strong>
                        <p align="justify" style="text-align: justify;">
                            Al momento de la inspección, se debe identiﬁcar el estado inicial de todos los sistemas/componentes de la
                            Hoja Multipuntos y actualizarlo una vez que se realizan las tareas de mantenimiento, incluidas las solicitudes
                            de mantenimiento adicionales, vendidas y completadas durante la visita. Las Órdenes de Reparación (OR)
                            deben codificarse para indicar el estado del vehículo al ﬁnalizar el servicio o reparación, mediante las
                            instrucciones que se detallan a continuación:<br><br><br>
                            Los siguientes códigos deben introducirse en la OR como parte de la operación de trabajo cuando se cierra la OR:
                        </p>
                        <ul>
                            <li>99P para indicar que se completó la revisión total del vehículo según la Hoja Multipuntos.</li>
                            <li>El código apropiado para cada estado rojo o amarillo que no haya recibido mantenimiento.</li>
                            <li>El código apropiado para cada estado verde de freno, neumático y batería.</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td style="width:560px;font-size:13px;">
                        <p align="justify" style="text-align: justify;">
                            <strong>Nota Importante:</strong> Registre el valor <strong>más bajo</strong> asignado para cualquier medida o estado. Por ejemplo, si
                            hubiese dos medidas de freno <strong>verdes</strong> y dos <strong>amarillas</strong>, registre el valor general más bajo, <strong>amarillo</strong> (FRA).
                            <br><br><br>
                            Se considera que los artículos en la sección de sistemas o componentes de la Hoja Multipuntos, que  no se
                            indican como <strong>rojo</strong> o <strong>amarillo</strong>, son <strong>verdes</strong> y no requieren reparación ni reemplazo.
                            <br><br><br>
                            <strong>El personal del Distribuidor a cargo del registro de los códigos de estado al cierre de las OR, debe
                            consultar la guía provista de la hoja de inspección del vehículo.</strong>
                            <br><br><br>
                            <strong>¿Qué ocurre si se repara/remplaza un componente?</strong>
                            Cuando la inspección indica un estado rojo o amarillo, debe registrarse en la Hoja Multipuntos. <strong>El Asesor
                            de Servicio, debe comunicarse con el cliente para informarle y obtener su aprobación, para realizar
                            la reparación o reemplazo.</strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="width:560px;font-size:13px;">
                        <p align="justify" style="text-align: justify;">
                            En caso de que el Distribuidor obtenga permiso para completar la reparación o reemplazo, el asesor de
                            servicio debe actualizar la Hoja Multipuntos, marcando las casillas <strong>‘Cambiado’</strong>. Esta casilla indica que se
                            realizó el trabajo durante el servicio o reparación y por lo tanto deberá reﬂejar el código de estado <strong>verde</strong> al
                            cierre de la OR.
                            <br><br><br>
                            Si el cliente rechaza el mantenimiento requerido identiﬁcado durante la inspección, deberá registrarse el
                            estado amarillo o rojo en la HM y codiﬁcarse en la OR.
                            <br><br><br>
                            <strong>NO SÓLO REGRISTRE LOS RESULTADOS INICIALES DE LA INSPECCION, SE DEBE REFLEJAR EL
                            ESTADO DEL VEHÍCULO AL MOMENTO QUE SALE DEL DISTRIBUIDOR EN LA HOJA MULTIPUNTOS
                            DEL VEHÍCULO Y DEBE CODIFICARSE EN LA ORDEN DE REPARACIÓN.</strong>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="width:560px;font-size:9px;">
                        <br><br><br><br>
                        <br><br><br><br>
                        <p align="justify" style="text-align: justify;">
                            <img src="<?php echo base_url();?>assets/imgs/img_icon/hoja2.png" class="leaf" style="width:13px;">&nbsp;&nbsp;
                            El ícono de la “hoja verde” designa a los artículos que pueden contribuir  a mejorar la eﬁciencia del vehículo y promover el ambiente verde, según el folleto verde de la
                            Asociación Nacional de Distribuidores de Automóviles de los Estados Unidos de América (NADA por sus siglas en inglés). Consulte www.nada.org/green en caso de requerir
                            mayor información.
                        </p>
                        <br><br><br><br>
                    </td>
                </tr>
            </table>
        </div>

        <!-- Hoja 3 -->
        <br>
        <div style="margin-top: 10px;">
            <br>
            <table style="margin-left:55px;margin-top:70px;border: 0.5px solid #5bc0de;">
                <tr>
                    <td style="width:620px;" align="center">
                        <img src="<?php echo base_url();?>/assets/imgs/grafico_car.jpg" alt="" style="width:620px;">
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>

<?php 
    //try {
        $html = ob_get_clean();
        ob_clean();

        $mpdf = new \mPDF('utf-8', 'A4-P');
        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','debug' => TRUE]);
        
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        
        //$mpdf->AddPage();
        //$hoja_2 = '<br>';
        //$mpdf->WriteHTML($hoja_2);
        
        $mpdf->Output();

    //} catch (Html2PdfException $e) {
    //    $html2pdf->clean();
    //    $formatter = new ExceptionFormatter($e);
    //    echo $formatter->getHtmlMessage();
    //}

 ?>