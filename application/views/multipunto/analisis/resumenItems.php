<div class="container" ng-controller="multiCtrl">
    <br><br>
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <br>
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Ordenes de servicio</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>
                    <br><br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;">
                                    <td align="center" style="width: 10%;" rowspan="2">#</td>
                                    <td align="center" style="width: 10%;" rowspan="2"><strong>NO. ORDEN</strong></td>
                                    <td align="center" style="width: 10%;" rowspan="2"><strong>FECHA REGISTRO</strong></td>
                                    <td align="center" style="width: 10%;" colspan="4"><strong>SELECCIÓN</strong></td>
                                    <td align="center" style="width: 10%;" rowspan="2">TOTAL ITEMS</td>
                                    <!-- <td align="center" colspan="3" style="width: 10%;" rowspan="2"><strong>ACCIONES</strong></td> -->
                                </tr>
                                <tr style="font-size:14px;">
                                    <td align="center" style="width: 10%;background-color:#5cb85c;"><strong>APROBADO</strong></td>
                                    <td align="center" style="width: 10%;background-color:#f0ad4e;"><strong>FUTURO</strong></td>
                                    <td align="center" style="width: 10%;background-color:#d9534f;"><strong>INMEDIATO</strong></td>
                                    <td align="center" style="width: 10%;background-color:#eee;"><strong>CAMBIOS</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align="center" style="width:10%;">
                                                <!-- <?php echo count($idOrden) - $index; ?> -->
                                                <?php echo $index+1; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $fechaCaptura[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $aprobado[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $futuro[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $inmediato[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $cambioParte[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $totalItems[$index]; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="6" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <!-- <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar"> -->

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
