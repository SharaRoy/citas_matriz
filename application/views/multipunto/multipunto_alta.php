<div style="margin: 30px;" >
    <form name="" id="formulario" method="post" action="<?=base_url()."multipunto/Multipunto/validateView"?>" autocomplete="on" enctype="multipart/form-data">
        <!-- Primera hoja de formulario -->
        <div class="row">
            <div class="col-sm-5">
                <h3 class="h2_title_blue">HOJA MULTIPUNTOS</h3>
            </div>
            <div class="col-sm-7" align="right">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" style="max-width:6cm;margin-top:10px;">
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="alert alert-warning" align="center" style="display:none;">
                    <strong style="font-size:20px !important;">Campos incompletos.</strong>
                </div>
                <div class="panel panel-primary">
                    <div class="vertical_asesor">
                        USO EXCLUSIVO ASESOR
                    </div>
                    <div class="vertical_tecnico_1">
                        USO EXCLUSIVO TÉCNICO
                    </div>
                    <div class="panel-heading" style="text-align:center;">
                        INSPECCIÓN DE VEHÍCULO
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-5" align="right"></div>
                            <div class="col-md-5" align="right">
                                <!--<?php if (isset($tipoServicio)): ?>
                                    <?php if (($tipoServicio == "1")&&($dirige == "TEC")): ?>
                                        <!- -<button type="button" class="btn btn-success" id="btns-aprobado" <?php if(isset($firmaTecnico)) if($firmaTecnico != "") echo "disabled"; else echo ""; else echo ""; ?>>
                                            Autocarga
                                        </button>- ->
                                        <!- - <button type="button" class="btn btn-warning" id="btns-futuro">
                                            Autocarga
                                        </button>
                                        <button type="button" class="btn btn-danger" id="btns-inmediato">
                                            Autocarga
                                        </button> - ->
                                    <?php endif; ?> 
                                <?php endif; ?>-->
                            </div>
                            <div class="col-md-2" align="right">
                                <?php if (isset($dirige)): ?>
                                    <?php if ($dirige == "ASE"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($dirige == "TEC"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                            Regresar
                                        </button>
                                    <?php elseif ($dirige == "JDT"): ?>
                                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                            Regresar
                                        </button>
                                    <?php else: ?>
                                        <!--<button type="button" class="btn btn-dark" >
                                            Sesión Expirada
                                        </button>-->
                                    <?php endif; ?>
                                <?php else: ?>
                                    <!--<button type="button" class="btn btn-dark" >
                                        Sesión Expirada
                                    </button>-->
                                <?php endif; ?>
                            </div>
                        </div>
                        
                        <br><br>
                        <div class="row" style="padding-left: 13px;padding-right: 13px;">
                        
                            <!-- Lado izquierdo de la pantalla -->
                            <div class="col-md-6">
                                <!-- panel 1 -->
                                <div class="panel panel-default">
                                    <div class="panel-body" style="border:2px solid black;">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6" style="width:50%;">
                                                    <label for="">Fecha:</label>
                                                    <input type="date" class="input_field" id="start" name="fechaCaptura" value="<?php echo $hoy; ?>" min="<?php echo $hoy; ?>" max="<?php echo $hoy; ?>">
                                                    <br>
                                                    <?php echo form_error('fechaCaptura', '<span class="error">', '</span>'); ?>
                                                </div>
                                                <div class="col-sm-6" style="width:50%;">
                                                    <label for="">OR:</label>
                                                    <input class="input_field" type="text" name="orden" id="noOrden" onblur="precarga()" value="<?php if(set_value("orden") != "") echo set_value("orden"); else {if(isset($idCita)){ if($idCita != "0") echo $idCita; else echo "";}}  ?>"><br>
                                                    <label for="" id="errorConsultaServer" class="error"></label>
                                                    <?php echo form_error('orden', '<br><span class="error">', '</span>'); ?>

                                                    <br>
                                                    Folio Intelisis:&nbsp;
                                                    <label style="color:darkblue;">
                                                        <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top:5px;">
                                                <div class="col-md-12">
                                                    <label for="">No.Serie (VIN):</label>
                                                    <input class="input_field" type="text" name="noSerie" id="noSerieText" style="width:70%" value="<?php echo set_value('noSerie');?>">
                                                    <br>
                                                    <?php echo form_error('noSerie', '<span class="error">', '</span>'); ?>
                                                    <br>
                                                    <strong><label for="" id="nombreCamapanias"></label></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel 1 -->

                                <!-- panel 2 -->
                                <div class="panel panel-default">
                                    <div class="panel-body" style="border:2px solid black;">
                                        <table class="table-responsive" style="width:100%;">
                                            <tbody>
                                                <tr>
                                                    <td style="width:60%;">
                                                        <label for="">Modelo:</label>&nbsp;
                                                        <input class="input_field" type="text" name="modelo" id="txtmodelo" style="width: 60%;" value="<?php if(isset($vModelo)) echo $vModelo; else echo set_value('modelo');?>">
                                                        <br>
                                                        <?php echo form_error('modelo', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td style="width:40%;">
                                                        <label for="">No. de torre:</label>&nbsp;
                                                        <input class="input_field" type="text" name="torre" id="txttorre" style="width: 40%;" value="<?php if(isset($noTorrev)) echo $noTorrev; else echo set_value('torre');?>">
                                                        <br>
                                                        <?php echo form_error('torre', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <label for="">Nombre:</label>&nbsp;
                                                        <input class="input_field" type="text" name="nombreCarro" id="txtnombreCarro" style="width: 60%;" value="<?php if(isset($nombresv)) echo $nombresv; else echo set_value('nombreCarro');?>">
                                                        <br>
                                                        <?php echo form_error('nombreCarro', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <label for="">Correo electrónico:</label>&nbsp;
                                                        <input class="input_field" type="text" name="email" id="txtemail" style="width: 60%;" value="<?php if(isset($vEmail)) echo $vEmail; else echo set_value('email');?>">
                                                        <br>
                                                        <label for="">Correo compañía:</label>&nbsp;
                                                        <input class="input_field" type="text" name="emailComp" id="correoCompaniaLabel" style="width: 60%;" value="<?php echo set_value('emailComp');?>">
                                                        <br>
                                                        <?php echo form_error('email', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- panel 2 -->

                                <!-- panel 3 -->
                                <div class="panel panel-default">
                                    <div class="panel-body" style="border:2px solid black">
                                        <div class="table-responsive">
                                            <table class="" style="width:100%" >
                                                <tr class="headers_table">
                                                    <td style="text-align: center;background-color:#340f7b; color:white;width:80%">NIVELES DE FLUIDOS</td>
                                                    <td style="width:20%;text-align: center;">CAMBIADO</td>
                                                </tr>
                                                <tr>
                                                    <td  style="text-align: left;width:80%">
                                                        <label for="">&nbsp;Perdida de fluido y/o aceite &nbsp;&nbsp;</label>
                                                        <!-- <input type="checkbox" style="transform: scale(1.5);" value="" ng-model="lostfluid">Si/No -->
                                                        Si&nbsp;<input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" value="Si" name="perdidaAceite" <?php echo set_checkbox('perdidaAceite', 'Si'); ?>>
                                                        No&nbsp;<input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" value="No" name="perdidaAceite" <?php echo set_checkbox('perdidaAceite', 'No'); ?>>
                                                        <?php echo form_error('perdidaAceite', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td style="width:20%;text-align: center;">
                                                        <input type="checkbox" style="transform: scale(1.5);" value="1" style="transform: scale(1.5);" name="fuidoCambios" <?php echo set_checkbox('fuidoCambios', '1'); ?>>
                                                        <br>
                                                        <?php echo form_error('fuidoCambios', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table class="" style="width:100%;border-top: 1px solid black;" >
                                                <tr>
                                                    <td style="padding:2px;">Bien</td>
                                                    <td>Llenar</td>
                                                    <td></td>
                                                    <td>Bien</td>
                                                    <td>Llenar</td>
                                                    <td></td>
                                                    <td>Bien</td>
                                                    <td>Llenar</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">           
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="aceiteCheck" value="Bien" <?php echo set_checkbox('aceiteCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="aceiteCheck" value="Llena" <?php echo set_checkbox('aceiteCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Aceite de motor
                                                        <?php echo form_error('aceiteCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="direccionCheck" value="Bien" <?php echo set_checkbox('direccionCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="direccionCheck" value="Llena" <?php echo set_checkbox('direccionCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Dirección hidráulica
                                                        <?php echo form_error('direccionCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="transmisionCheck" value="Bien" <?php echo set_checkbox('transmisionCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="transmisionCheck" value="Llena" <?php echo set_checkbox('transmisionCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Transmisión<br>(si está equipada con<br> bayoneta de medición)
                                                        <?php echo form_error('transmisionCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="liquidoFrenosCheck" value="Bien" <?php echo set_checkbox('liquidoFrenosCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="liquidoFrenosCheck" value="Llena" <?php echo set_checkbox('liquidoFrenosCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Deposito fluido frenos
                                                        <?php echo form_error('liquidoFrenosCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="liquidoParabriCheck" value="Bien" <?php echo set_checkbox('liquidoParabriCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="liquidoParabriCheck" value="Llena" <?php echo set_checkbox('liquidoParabriCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Limpiapa-<br>rabrisas
                                                        <?php echo form_error('liquidoParabriCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="refrigeranteCheck" value="Bien" <?php echo set_checkbox('refrigeranteCheck', 'Bien'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="divCheck" style="float:left;">  
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="refrigeranteCheck" value="Llena" <?php echo set_checkbox('refrigeranteCheck', 'Llena'); ?>>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        Deposito recuperación refrigerante
                                                        <?php echo form_error('refrigeranteCheck', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                            </table>

                                            <br>
                                            <table style="width:100%">
                                                <tr class="headers_table">
                                                    <td colspan="2" style="text-align: center;background-color:#340f7b; color:white;" >PLUMAS LIMPIAPARABRISAS</td>
                                                    <td style="width:20%;text-align: center;">CAMBIADO</td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left;">
                                                        <label for="">Prueba de Limpiaparabrisas <br>realizada&nbsp;</label><br>
                                                        Si &nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);" name="pruebaparabrisas" style="transform: scale(1.5);" value="Si" <?php echo set_checkbox('pruebaparabrisas', 'Si'); ?>>
                                                        No &nbsp;&nbsp;<input type="radio" style="transform: scale(1.5);" name="pruebaparabrisas" style="transform: scale(1.5);" value="No" <?php echo set_checkbox('pruebaparabrisas', 'No'); ?>>
                                                        <br>
                                                        <?php echo form_error('pruebaparabrisas', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td>
                                                        <div class="divAprobado" style="float:left;"> 
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="plumas" value="Si" <?php echo set_checkbox('plumas', 'Si'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;"> 
                                                            <input type="radio" style="transform: scale(1.5);" style="transform: scale(1.5);" name="plumas" value="No" <?php echo set_checkbox('plumas', 'No'); ?>>
                                                        </div>
                                                        <label for="" style="float:left;">&nbsp; Plumas <br>Limpiaparabrisas</label>
                                                        <br>
                                                        <?php echo form_error('plumas', '<span class="error">', '</span>'); ?>
                                                        
                                                        <?php echo form_error('plumas', '<br><span class="error">', '</span>'); ?>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <input type="checkbox" style="transform: scale(1.5);" style="transform: scale(1.5);" name="plumasCambio" value="1" <?php echo set_checkbox('plumasCambio', '1'); ?>>
                                                        <br>
                                                        <?php echo form_error('plumasCambio', '<span class="error">', '</span>'); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <!-- SISTEMAS Y COMPONENTES -->
                                                        <div class="" style="text-align:center; background-color: #340f7b ; color:white;">
                                                            SISTEMAS / COMPONENTES
                                                        </div>
                                                        <div class="panel">
                                                            <div class="panel-body">
                                                                <table style="width:100%;">
                                                                    <tr class="headers_table">
                                                                        <td style="text-align: center;background-color:slategrey; color:white;" >LUCES / PARABRISAS</td>
                                                                        <td style="width:20%;text-align: center;">CAMBIADO</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="divAprobado" style="float:left;"> 
                                                                                        <input type="radio" style="transform: scale(1.5);" name="luces" value="Si" <?php echo set_checkbox('luces', 'Si'); ?>>
                                                                                    </div>
                                                                                    <div class="divInmediato" style="float:left;"> 
                                                                                        <input type="radio" style="transform: scale(1.5);" name="luces" value="No" <?php echo set_checkbox('luces', 'No'); ?>>
                                                                                    </div>
                                                                                    <br>
                                                                                    <?php echo form_error('luces', '<span class="error">', '</span>'); ?>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    Funcionamiento de claxon, luces interiores, luces exteriores luces de giro, luces de emergencia y freno.
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <input type="checkbox" style="transform: scale(1.5);" value="1" name="lucesCambio" <?php echo set_checkbox('lucesCambio', '1'); ?>>
                                                                            <br>
                                                                            <?php echo form_error('lucesCambio', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="padding-top:10px;">
                                                                        <td>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <div class="divAprobado" style="float:left;"> 
                                                                                        <input type="radio" style="transform: scale(1.5);" name="parabrisas" value="Si" <?php echo set_checkbox('parabrisas', 'Si'); ?>>
                                                                                    </div>
                                                                                    <div class="divInmediato" style="float:left;"> 
                                                                                        <input type="radio" style="transform: scale(1.5);" name="parabrisas" value="No" <?php echo set_checkbox('parabrisas', 'No'); ?>>
                                                                                    </div>
                                                                                    <br>
                                                                                    <?php echo form_error('parabrisas', '<span class="error">', '</span>'); ?>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    Grietas, roturas y picaduras de parabrisas.
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td style="text-align:center;">
                                                                            <input type="checkbox" style="transform: scale(1.5);" value="1" name="parabrisasCambio" <?php echo set_checkbox('parabrisasCambio', '1'); ?>>
                                                                            <br>
                                                                            <?php echo form_error('parabrisasCambio', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="headers_table">
                                                                        <td style="width:70%; text-align: center;background-color:#340f7b; color:white;">BATERÍA</td>
                                                                        <td style="width:30%;text-align: center;">CAMBIADO</td>
                                                                    </tr>
                                                                    <tr style="margin-top:20px;">
                                                                        <td rowspan="2">
                                                                            <br><br>
                                                                            <img src="<?php echo base_url().'/assets/imgs/nivel_bateria.png' ?>" alt="" style="height: 50px;width:290px;">
                                                                        </td>
                                                                        <td align="center">
                                                                            <input type="checkbox" style="transform: scale(1.5);" name="bateriaCambio" value="1" <?php echo set_checkbox('bateriaCambio', '1'); ?>>
                                                                            <br>
                                                                            <?php echo form_error('bateriaCambio', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Estado de batería</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:290px;">
                                                                            <div id="slider-2"style="width:290px;"></div>
                                                                            <label for="" id="labelNB"><?php if(set_value('valorBateria') != "") echo  set_value('valorBateria'); else echo "0";?>%</label>
                                                                            <input type="hidden" name="valorBateria" id="valorBateria" value="<?php if(set_value('valorBateria') != "") echo  set_value('valorBateria'); else echo "0";?>">
                                                                            <br>
                                                                            <?php echo form_error('valorBateria', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                        <td style="margin-left:10px;">
                                                                            <div class="divAprobado" style="float:left;"> 
                                                                                <input type="radio" style="transform: scale(1.5);" name="bateriaRadio" value="Aprobado" <?php echo set_checkbox('bateriaRadio', 'Aprobado'); ?>>
                                                                            </div>
                                                                            <div class="divFuturo" style="float:left;">
                                                                                <input type="radio" style="transform: scale(1.5);" name="bateriaRadio" value="Futuro" <?php echo set_checkbox('bateriaRadio', 'Futuro'); ?>>
                                                                            </div>
                                                                            <div class="divInmediato" style="float:left;"> 
                                                                                <input type="radio" style="transform: scale(1.5);" name="bateriaRadio" value="Inmediato" <?php echo set_checkbox('bateriaRadio', 'Inmediato'); ?>>
                                                                            </div>
                                                                            <br>
                                                                            <?php echo form_error('bateriaRadio', '<span class="error">', '</span>'); ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>

                                                        <!-- Corriente de arranque -->
                                                        <table>
                                                            <tr>
                                                                <td>Corriente de arranque en frío especificaciones de fabrica</td>
                                                                <td align="center">
                                                                    <input type="text" class="input_field" name="ccafabrica" style="width:50px;" value="<?php echo set_value('ccafabrica');?>">
                                                                    <label>CCA</label>
                                                                    <br>
                                                                    <?php echo form_error('ccafabrica', '<span class="error">', '</span>'); ?>
                                                                </td>
                                                                <td>Corriente de arranque en frío real</td>
                                                                <td align="center">
                                                                    <input type="text" class="input_field" style="width:50px;" name="ccareal" value="<?php echo set_value('ccareal');?>">
                                                                    <label>CCA</label>
                                                                    <br>
                                                                    <?php echo form_error('ccareal', '<span class="error">', '</span>'); ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div> <!-- class table responsive -->
                                    </div>
                                </div>
                                <!-- panel 3 -->
                            </div>

                            <!-- Lado derecho de la pantalla -->
                            <div class="col-md-6">
                                <table class="table-responsive">
                                    <tr>
                                        <td colspan="6">
                                            <h4>SÍMBOLO</h4>
                                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">&nbsp;
                                            Puede contribuir a la eficiencia del vehículo y la protección del medio ambiente.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right:3px;">
                                            <!--<img src="<?php echo base_url();?>assets/imgs/square_green.png" width="28px" height="28px">-->
                                            <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #1f661f;"></div>
                                        </td>
                                        <td>&nbsp;Verificado y aprobado</td>
                                        <td style="padding-right:3px;">
                                            <!--<img src="<?php echo base_url();?>assets/imgs/square_yellow.png" width="28px" height="28px">-->
                                            <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #e6b019;"></div>
                                        </td>
                                        <td>&nbsp;Puede requerir atención en el futuro</td>
                                        <td style="padding-right:3px;">
                                            <!--<img src="<?php echo base_url();?>assets/imgs/square_red.png" width="28px" height="28px">-->
                                            <div class="" style="float:left;width: 25px;height: 25px;border-radius: 5px;background-color: #e01a14;"></div>
                                        </td>
                                        <td>&nbsp;Requiere atención inmediata</td>
                                    </tr>
                                </table>

                                <table class="table-responsive" style="width:100%;border: 1px solid black;">
                                    <tbody>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;width: 80%;">BANDAS / MANGUERAS</td>
                                            <td align="center" style="width: 20%;">CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td style="width: 80%;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="sisCalefaccion" value="Aprobado" <?php echo set_checkbox('sisCalefaccion', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="sisCalefaccion" value="Futuro" <?php echo set_checkbox('sisCalefaccion', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="sisCalefaccion" value="Inmediato" <?php echo set_checkbox('sisCalefaccion', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('sisCalefaccion', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Pérdidas y/o daños en el sistema de calefacción, ventilación y aire acondicionado y en mangueras/cables.
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center" style="width: 20%;">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="sisCalefaccionCambio" value="1" <?php echo set_checkbox('sisCalefaccionCambio', '1'); ?>>
                                                <br>
                                                <?php echo form_error('sisCalefaccionCambio', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="radiadorRario" value="Aprobado" <?php echo set_checkbox('radiadorRario', 'Aprobado'); ?>>
                                                        </div>
                                                        <!--<label class="" style="width:40px;"></label>-->
                                                        <div class="divCheckSC" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);display:none;" class="seccionTecnico BtnAmarillo" name="radiadorRario" value="Futuro" <?php echo set_checkbox('radiadorRario', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="radiadorRario" value="Inmediato" <?php echo set_checkbox('radiadorRario', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('radiadorRario', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Sistema de refrigeración del motor, radiador, mangueras y abrazaderas.
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="radiadorCambio" value="1" <?php echo set_checkbox('radiadorCambio', '1'); ?>>
                                                <br>
                                                <?php echo form_error('radiadorCambio', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="bandasRadio" value="Aprobado" <?php echo set_checkbox('bandasRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divCheckSC" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);display:none;" class="seccionTecnico BtnAmarillo" name="bandasRadio" value="Futuro" <?php echo set_checkbox('bandasRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="bandasRadio" value="Inmediato" <?php echo set_checkbox('bandasRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('bandasRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Banda(s)
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="bandasCambio" value="1" <?php echo set_checkbox('bandasCambio', '1'); ?>>
                                                <br>
                                                <?php echo form_error('bandasCambio', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">SISTEMA DE FRENOS</td>
                                            <td align="center">CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="frenosRadio" value="Aprobado" <?php echo set_checkbox('frenosRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="frenosRadio" value="Futuro" <?php echo set_checkbox('frenosRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="frenosRadio" value="Inmediato" <?php echo set_checkbox('frenosRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('frenosRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Sistema de frenos (incluye mangueras y freno de mano) &nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </div>
                                                </div>
                                           </td>
                                           <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="frenosCambio" value="1" <?php echo set_checkbox('frenosCambio', '1'); ?>>
                                                <br>
                                                <?php echo form_error('frenosCambio', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">DIRECCIÓN Y SUSPENSIÓN</td>
                                            <td align="center">CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="amortiguadoRadio" value="Aprobado" <?php echo set_checkbox('amortiguadoRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="amortiguadoRadio" value="Futuro" <?php echo set_checkbox('amortiguadoRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="amortiguadoRadio" value="Inmediato" <?php echo set_checkbox('amortiguadoRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('amortiguadoRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Pérdidas y/o daños en amortiguadores/puntales y otros
                                                        componentes de la suspensión.
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="amortiguadorCheck" value="1" <?php echo set_checkbox('amortiguadorCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('amortiguadorCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"  class="seccionTecnico BtnVerde" value="Aprobado" name="varillajeRadio" <?php echo set_checkbox('varillajeRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"  class="seccionTecnico BtnAmarillo" value="Futuro" name="varillajeRadio" <?php echo set_checkbox('varillajeRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);"  class="seccionTecnico BtnRojo" value="Inmediato" name="varillajeRadio" <?php echo set_checkbox('varillajeRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('varillajeRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Dirección, varillaje de la dirección y juntas de rótula (visual)
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="varillajeCheck" value="1" <?php echo set_checkbox('varillajeCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('varillajeCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">SISTEMA DE ESCAPE</td>
                                            <td align="center">CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="escapeRadio" <?php echo set_checkbox('escapeRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="escapeRadio" <?php echo set_checkbox('escapeRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="escapeRadio" <?php echo set_checkbox('escapeRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('escapeRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Sistema de escape y escudo de calor (pérdidas, daño, piezas sueltas) &nbsp;
                                                        <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="escapeCheck" value="1" <?php echo set_checkbox('escapeCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('escapeCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr class="headers_table" align="center">
                                            <td style="background-color:gray;color:white;">TREN MOTRIZ</td>
                                            <td>CAMBIADO</td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="embragueRadio" value="Aprobado" <?php echo set_checkbox('embragueRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="embragueRadio" value="Futuro" <?php echo set_checkbox('embragueRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="embragueRadio" value="Inmediato" <?php echo set_checkbox('embragueRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('embragueRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Funcionamiento del embrague (si está equipado).
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="embragueCheck" value="1" <?php echo set_checkbox('embragueCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('embragueCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="border: 1px solid black;">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="divAprobado" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="cardanRadio" value="Aprobado"  <?php echo set_checkbox('cardanRadio', 'Aprobado'); ?>>
                                                        </div>
                                                        <div class="divFuturo" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="cardanRadio" value="Futuro"  <?php echo set_checkbox('cardanRadio', 'Futuro'); ?>>
                                                        </div>
                                                        <div class="divInmediato" style="float:left;">
                                                            <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="cardanRadio" value="Inmediato"  <?php echo set_checkbox('cardanRadio', 'Inmediato'); ?>>
                                                        </div>
                                                        <br>
                                                        <?php echo form_error('cardanRadio', '<span class="error">', '</span>'); ?>
                                                    </div>
                                                    <div class="col-md-8">
                                                        Transmisión, flecha cardán y lubricación (si necesita).
                                                    </div>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="cardanCheck" value="1"  <?php echo set_checkbox('cardanCheck', '1'); ?>>
                                                <br>
                                                <?php echo form_error('cardanCheck', '<span class="error">', '</span>'); ?>
                                            </td>
                                        </tr>
                                        <tr style="text-align: right ;background-color:#340f7b;color:white">
                                            <td align="center">PARTE INFERIOR DEL VEHÍCULO</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <!-- CANVAS -->
                                                <?php if (isset($modalidadFormulario)): ?>
                                                    <!-- Si existe la variable hay que comprobar que sea el asesor quien entro a crear al multiputo -->
                                                    <?php if ($modalidadFormulario == "1A"): ?>
                                                        <canvas  id="diagramaInferior" width="380" height="200"></canvas>
                                                    <?php else: ?>
                                                        <canvas  id="diagramaInferior" width="380" height="200" style="display:none;"></canvas>
                                                        <img src="<?php echo base_url().'assets/imgs/car_ext.png'; ?>" alt="" width="380" height="200" style="">
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <!-- Si no existe la variable quiere decir que quien crea la multipunto es un asesor -->
                                                    <canvas  id="diagramaInferior" width="380" height="200" style="display:none;"></canvas>
                                                    <img src="<?php echo base_url().'assets/imgs/car_ext.png'; ?>" alt="" width="380" height="200" style="">
                                                <?php endif; ?>
                                                <!-- CANVAS -->
                                                <input type="hidden" name="danosInferior" id="danosInferior" value="<?php echo set_value('danosInferior'); ?>">
                                                <input type="hidden" name="" id="direccionFondo" value="<?php echo base_url().'assets/imgs/car_ext.png'; ?>">
                                                <!-- coordenadas de los puntos a remarcar -->
                                                <!-- <div class="row" id="coleccionCoordenadas"></div> -->
                                            </td>
                                            <td>
                                                Anote en el diagrama todos los daños o defectos detectados en la parte
                                                inferior de la carrocería durante la revisión en el taller
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

                <!-- Segunda hoja del formulario -->
        <div class="row">
            <div class="col-md-12">
                <table style="border:2px solid black; width:100%;">
                    <tr style="color:white; text-align:center;background-color:#340f7b;">
                            <td colspan="4">DESGASTE DE NEUMÁTICO / FRENO</td>
                        </tr>
                        <tr style="border:1px solid black;">
                            <td style="color:white;background-color:#340f7b;border:1px solid black;">PROFUNDIDAD DE DIBUJO</td>
                            <td style="background-color:green;color:white;" align="center">5 mm y mayor</td>
                            <td style="background-color:yellow" align="center">3 a 5 mm</td>
                            <td style="background-color:red;color:white;" align="center">2 mm y menor</td>
                        </tr>
                        <tr style="border:1px solid black;">
                            <td style="color:white;background-color:#340f7b;border:1px solid black;">MEDIDA DE BALATAS</td>
                            <td style="background-color:green; color:white;" align="center">Mas de 8 mm</td>
                            <td style="background-color:yellow" align="center">4 a 6 mm</td>
                            <td style="background-color:red;color:white;" align="center"> 3 mm o menos</td>
                        </tr>
                </table>
            </div>
        </div>

        <div class="row" style="padding-left: 15px;padding-right: 15px;">
            <!--Lado izquierdo de la segunda hoja -->
            <div class="col-md-6" style="border:2px solid black; width:100%; padding: 0px;">
                <table style="width:100%;">
                    <tr>
                        <td style="background-color:lightgrey;">
                            &nbsp;&nbsp;<input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="medicionfrenos" value="1" <?php echo set_checkbox('medicionfrenos', '1'); ?>>
                            &nbsp;&nbsp;&nbsp;No se tomaron mediciones de los frenos en esta visita de servicio.
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color:lightgrey;">
                            &nbsp;&nbsp;<input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="reinicioindicador" value="1" <?php echo set_checkbox('reinicioindicador', '1'); ?>>
                            &nbsp;&nbsp;&nbsp;Reinicio del indicador de cambio de aceite.
                            <br>
                        </td>
                    </tr>
                </table>

                <br>
                <table style="width:100%;">
                    <tr class="headers_table" align="center">
                        <td style="background-color:lightgrey; color:black;">
                            FRENTE IZQUIERDO &nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="profNeumaticoRadio" value="Aprobado" <?php echo set_checkbox('profNeumaticoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="profNeumaticoRadio" value="Futuro" <?php echo set_checkbox('profNeumaticoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="profNeumaticoRadio" value="Inmediato" <?php echo set_checkbox('profNeumaticoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('profNeumaticoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Profundidad de dibujo del neumático.
                                    <input type="text" class="input_field seccionTecnico"  name="profNeumatico" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;"value="<?php echo set_value('profNeumatico');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('profNeumatico', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="profNeumaticoCheck" value="1"  <?php echo set_checkbox('profNeumaticoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('profNeumaticoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="patronDegasteRadio" value="Aprobado" <?php echo set_checkbox('patronDegasteRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="patronDegasteRadio" value="Futuro" <?php echo set_checkbox('patronDegasteRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="patronDegasteRadio" value="Inmediato" <?php echo set_checkbox('patronDegasteRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('patronDegasteRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Patrón de desgaste/daño del neumático.
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="patronDegasteCheck" value="1"  <?php echo set_checkbox('patronDegasteCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('patronDegasteCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="presionRadio" value="Aprobado" <?php echo set_checkbox('presionRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="presionRadio" value="Futuro" <?php echo set_checkbox('presionRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="presionRadio" value="Inmediato" <?php echo set_checkbox('presionRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('presionRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Presión de inflado a PSI según recomendación del fabricante.
                                    <input type="text" class="input_field seccionTecnico"  name="presionPSIFI" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;"value="<?php echo set_value('presionPSIFI');?>">
                                    PSI
                                    <br>
                                    <?php echo form_error('presionPSIFI', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="presionCheck" value="1" <?php echo set_checkbox('presionCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('presionCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="balatasRadio" value="Aprobado" <?php echo set_checkbox('balatasRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="balatasRadio" value="Futuro" <?php echo set_checkbox('balatasRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="balatasRadio" value="Inmediato" <?php echo set_checkbox('balatasRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('balatasRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de balatas.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="balatas" style="width:2cm;" value="<?php echo  set_value('balatas');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('balatas', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="balatasCheck" value="1" <?php echo set_checkbox('balatasCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('balatasCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="discoRadio" value="Aprobado" <?php echo set_checkbox('discoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="discoRadio" value="Futuro" <?php echo set_checkbox('discoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="discoRadio" value="Inmediato" <?php echo set_checkbox('discoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('discoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de disco.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico" name="discoI" style="width:2cm;" value="<?php echo set_value('discoI');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('discoI', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="discoCheck" value="1" <?php echo set_checkbox('discoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('discoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr class="headers_table" align="center">
                        <td style="background-color:lightgrey; color:black;">
                            PARTE TRASERA IZQUIERDO &nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" name="profundidadDibujoRadio" value="Aprobado" <?php echo set_checkbox('profundidadDibujoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" name="profundidadDibujoRadio" value="Futuro" <?php echo set_checkbox('profundidadDibujoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" name="profundidadDibujoRadio" value="Inmediato" <?php echo set_checkbox('profundidadDibujoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('profundidadDibujoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Profundidad de dibujo del neumático.
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico" name="profundidadDibujo" style="width:2cm;" value="<?php echo  set_value('profundidadDibujo');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('profundidadDibujo', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="profundidadDibujoCheck" value="1" <?php echo set_checkbox('profundidadDibujoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('profundidadDibujoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                  <div class="divAprobado" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="desgasteRadio" <?php echo set_checkbox('desgasteRadio', 'Aprobado'); ?>>
                                  </div>
                                  <div class="divFuturo" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="desgasteRadio" <?php echo set_checkbox('desgasteRadio', 'Futuro'); ?>>
                                  </div>
                                  <div class="divInmediato" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="desgasteRadio" <?php echo set_checkbox('desgasteRadio', 'Inmediato'); ?>>
                                  </div>
                                  <br>
                                  <?php echo form_error('desgasteRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Patrón de desgaste/daño del neumático.
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="desgasteCheck" value="1" <?php echo set_checkbox('desgasteCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('desgasteCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                          <div class="row">
                              <div class="col-md-4">
                                  <div class="divAprobado" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="infladoRadio" <?php echo set_checkbox('infladoRadio', 'Aprobado'); ?>>
                                  </div>
                                  <div class="divFuturo" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="infladoRadio" <?php echo set_checkbox('infladoRadio', 'Futuro'); ?>>
                                  </div>
                                  <div class="divInmediato" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="infladoRadio" <?php echo set_checkbox('infladoRadio', 'Inmediato'); ?>>
                                  </div>
                                  <br>
                                  <?php echo form_error('infladoRadio', '<span class="error">', '</span>'); ?>
                              </div>
                              <div class="col-md-8">
                                  Presión de inflado a PSI según recomendación del fabricante.
                                  <input type="text" class="input_field seccionTecnico"  name="presionPSITI" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;"value="<?php echo set_value('presionPSITI');?>">
                                  PSI
                                  <br>
                                  <?php echo form_error('presionPSITI', '<span class="error">', '</span>'); ?>
                              </div>
                          </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="infladoCheck" value="1" <?php echo set_checkbox('infladoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('infladoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="espesorBalataRadio" <?php echo set_checkbox('espesorBalataRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="espesorBalataRadio" <?php echo set_checkbox('espesorBalataRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="espesorBalataRadio" <?php echo set_checkbox('espesorBalataRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('espesorBalataRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de balatas.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="espesorBalata" style="width:2cm;" value="<?php echo  set_value('espesorBalata');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('espesorBalata', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="espesorBalataCheck" value="1" <?php echo set_checkbox('espesorBalataCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('espesorBalataCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="espesorDiscoRadio" <?php echo set_checkbox('espesorDiscoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="espesorDiscoRadio" <?php echo set_checkbox('espesorDiscoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="espesorDiscoRadio" <?php echo set_checkbox('espesorDiscoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('espesorDiscoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de disco.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="espesorDisco" style="width:2cm;" value="<?php echo  set_value('espesorDisco');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('espesorDisco', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="espesorDiscoCheck" value="1" <?php echo set_checkbox('espesorDiscoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('espesorDiscoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="diametroRadio" <?php echo set_checkbox('diametroRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="diametroRadio" <?php echo set_checkbox('diametroRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="diametroRadio" <?php echo set_checkbox('diametroRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('diametroRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Diámetro de tambor.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="diametro" style="width:2cm;" value="<?php echo set_value('diametro');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('diametro', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="diametroCheck" value="1" <?php echo set_checkbox('diametroCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('diametroCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
            </div>

            <!--Lado derecho de la segunda hoja -->
            <div class="col-md-6" style="border:2px solid black; width:100%; padding: 0px;">
                <table style="width:100%;">
                    <tr>
                        <td colspan="2">
                            Comentarios:<br>
                            <textarea class="form-control SeccionTecnico" name="comentarios" rows="1" style="width: 100%;"><?php echo  set_value('comentarios');?></textarea>
                            <br>
                        </td>
                    </tr>
                    <tr align="center">
                        <td style="background-color:lightgrey; color:black;">
                            FRENTE DERECHO&nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="profNeumaticoDerechoRadio" <?php echo set_checkbox('profNeumaticoDerechoRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="profNeumaticoDerechoRadio" <?php echo set_checkbox('profNeumaticoDerechoRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="profNeumaticoDerechoRadio" <?php echo set_checkbox('profNeumaticoDerechoRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('profNeumaticoDerechoRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Profundidad de dibujo del neumático.
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="profNeumaticoDerecho" style="width:2cm;" value="<?php echo set_value('profNeumaticoDerecho');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('profNeumaticoDerecho', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="profNeumaticoDerechoCheck" value="1" <?php echo set_checkbox('profNeumaticoDerechoCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('profNeumaticoDerechoCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="desgasteDerRadio" <?php echo set_checkbox('desgasteDerRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="desgasteDerRadio" <?php echo set_checkbox('desgasteDerRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="desgasteDerRadio" <?php echo set_checkbox('desgasteDerRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('desgasteDerRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Patrón de desgaste/daño del neumático.
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="desgasteDerCheck" value="1" <?php echo set_checkbox('desgasteDerCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('desgasteDerCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="infladoDerRadio" <?php echo set_checkbox('infladoDerRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="infladoDerRadio" <?php echo set_checkbox('infladoDerRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="infladoDerRadio" <?php echo set_checkbox('infladoDerRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('infladoDerRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Presión de inflado a PSI según recomendación del fabricante.
                                    <input type="text" class="input_field seccionTecnico"  name="presionPSIFD" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;"value="<?php echo set_value('presionPSIFD');?>">
                                    PSI
                                    <br>
                                    <?php echo form_error('presionPSIFD', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="infladoDerCheck" value="1" <?php echo set_checkbox('infladoDerCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('infladoDerCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="balatasDerRadio" <?php echo set_checkbox('balatasDerRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="balatasDerRadio" <?php echo set_checkbox('balatasDerRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="balatasDerRadio" <?php echo set_checkbox('balatasDerRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('balatasDerRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de balatas.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="balatasDerc" style="width:2cm;"value="<?php echo set_value('balatasDerc');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('balatasDerc', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="balatasDercCheck" value="1" <?php echo set_checkbox('balatasDercCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('balatasDercCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="discoDercRadio" <?php echo set_checkbox('discoDercRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="discoDercRadio" <?php echo set_checkbox('discoDercRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="discoDercRadio" <?php echo set_checkbox('discoDercRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('discoDercRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de disco.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico" name="discoDerch" style="width:2cm;" style="width:2cm;"value="<?php echo set_value('discoDerch');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('discoDerch', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="discoDercCheck" value="1" <?php echo set_checkbox('discoDercCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('discoDercCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr class="headers_table" align="center">
                        <td style="background-color:lightgrey; color:black;">
                            PARTE TRASERA DERECHO &nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="dibujoDerecRadio" <?php echo set_checkbox('dibujoDerecRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="dibujoDerecRadio" <?php echo set_checkbox('dibujoDerecRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="dibujoDerecRadio" <?php echo set_checkbox('dibujoDerecRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('dibujoDerecRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Profundidad de dibujo del neumático.
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="dibujoDerech" style="width:2cm;"value="<?php echo set_value('dibujoDerech');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('dibujoDerech', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="dibujoDerechCheck" value="1" <?php echo set_checkbox('dibujoDerechCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('dibujoDerechCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="patronDesgDerchRadio" <?php echo set_checkbox('patronDesgDerchRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="patronDesgDerchRadio" <?php echo set_checkbox('patronDesgDerchRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="patronDesgDerchRadio" <?php echo set_checkbox('patronDesgDerchRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('patronDesgDerchRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Patrón de desgaste/daño del neumático.
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="patronDesgDerchCheck" value="1" <?php echo set_checkbox('patronDesgDerchCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('patronDesgDerchCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="presionDerchRadio" <?php echo set_checkbox('presionDerchRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="presionDerchRadio" <?php echo set_checkbox('presionDerchRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="presionDerchRadio" <?php echo set_checkbox('presionDerchRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('presionDerchRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Presión de inflado a PSI según recomendación del fabricante.
                                    <input type="text" class="input_field seccionTecnico"  name="presionPSITD" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style="width:2cm;"value="<?php echo set_value('presionPSITD');?>">
                                    PSI
                                    <br>
                                    <?php echo form_error('presionPSITD', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="presionDerchCheck" value="1" <?php echo set_checkbox('presionDerchCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('presionDerchCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="balatasEspesorRadio" <?php echo set_checkbox('balatasEspesorRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="balatasEspesorRadio" <?php echo set_checkbox('balatasEspesorRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="balatasEspesorRadio" <?php echo set_checkbox('balatasEspesorRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('balatasEspesorRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de balatas.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="balatasEspesor" style="width:2cm;"value="<?php echo set_value('balatasEspesor');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('balatasEspesor', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="balatasEspesorCheck" value="1" <?php echo set_checkbox('balatasEspesorCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('balatasEspesorCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="espesorDisco2Radio" <?php echo set_checkbox('espesorDisco2Radio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="espesorDisco2Radio" <?php echo set_checkbox('espesorDisco2Radio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="espesorDisco2Radio" <?php echo set_checkbox('espesorDisco2Radio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('espesorDisco2Radio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Espesor de disco.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="espesorDisco2" style="width:2cm;"value="<?php echo set_value('espesorDisco2');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('espesorDisco2', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="espesorDisco2Check" value="1" <?php echo set_checkbox('espesorDisco2Check', '1'); ?>>
                            <br>
                            <?php echo form_error('espesorDisco2Check', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="divAprobado" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="diametroTamborRadio" <?php echo set_checkbox('diametroTamborRadio', 'Aprobado'); ?>>
                                    </div>
                                    <div class="divFuturo" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="diametroTamborRadio" <?php echo set_checkbox('diametroTamborRadio', 'Futuro'); ?>>
                                    </div>
                                    <div class="divInmediato" style="float:left;">
                                        <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="diametroTamborRadio" <?php echo set_checkbox('diametroTamborRadio', 'Inmediato'); ?>>
                                    </div>
                                    <br>
                                    <?php echo form_error('diametroTamborRadio', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="col-md-8">
                                    Diámetro de tambor.<br>
                                    <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="diametroTambor" style="width:2cm;" value="<?php echo  set_value('diametroTambor');?>">
                                    mm
                                    <br>
                                    <?php echo form_error('diametroTambor', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="diametroTamborCheck" value="1" <?php echo set_checkbox('diametroTamborCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('diametroTamborCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                    <tr class="headers_table" align="center">
                        <td style="background-color:lightgrey; color:black;">
                            NEUMÁTICO DE REFACCIÓN &nbsp;
                            <img src="<?php echo base_url();?>assets/imgs/hoja.png" class="leaf">
                        </td>
                        <td>CAMBIADO</td>
                    </tr>
                    <tr style="border:1px solid black;">
                        <td>
                          <div class="row">
                              <div class="col-md-4">
                                  <div class="divAprobado" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnVerde" value="Aprobado" name="infladoPresionRadio" <?php echo set_checkbox('infladoPresionRadio', 'Aprobado'); ?>>
                                  </div>
                                  <div class="divFuturo" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnAmarillo" value="Futuro" name="infladoPresionRadio" <?php echo set_checkbox('infladoPresionRadio', 'Futuro'); ?>>
                                  </div>
                                  <div class="divInmediato" style="float:left;">
                                      <input type="radio" style="transform: scale(1.5);" class="seccionTecnico BtnRojo" value="Inmediato" name="infladoPresionRadio" <?php echo set_checkbox('infladoPresionRadio', 'Inmediato'); ?>>
                                  </div>
                                  <br>
                                  <?php echo form_error('infladoPresionRadio', '<span class="error">', '</span>'); ?>
                              </div>
                              <div class="col-md-8">
                                  Presión de inflado establecida en
                                  <input type="text" onkeypress='return event.charCode >= 46 && event.charCode <= 57' class="input_field seccionTecnico"  name="infladoPresion" value="<?php echo set_value('infladoPresion');?>">
                                  PSI
                                  <br>
                                  <?php echo form_error('infladoPresion', '<span class="error">', '</span>'); ?>
                              </div>
                          </div>
                        </td>
                        <td align="center">
                            <input type="checkbox" style="transform: scale(1.5);" class="seccionTecnico" name="infladoPresionCheck" value="1" <?php echo set_checkbox('infladoPresionCheck', '1'); ?>>
                            <br>
                            <?php echo form_error('infladoPresionCheck', '<span class="error">', '</span>'); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table width="100%" style="border-bottom:2px solid black;border-left: 2px solid black; border-right: 2px solid black;">
                    <tdead></thead>
                    <tbody>
                        <tr style="background-color:lightgrey;color:black; border: 1px solid black; text-align:center;">
                            <td colspan="">DIAGNÓSTICO</td>
                        </tr>
                        <tr>
                            <td style="padding:7px;">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Sistema.<br>
                                        <textarea  class="input_field_lg seccionTecnico" name="system" rows="5"><?php echo set_value('system');?></textarea>
                                        <br>
                                        <?php echo form_error('system', '<span class="error">', '</span>'); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        Componente.<br>
                                        <textarea  class="input_field_lg seccionTecnico" name="component" rows="5"><?php echo set_value('component');?></textarea>
                                        <br>
                                        <?php echo form_error('component', '<span class="error">', '</span>'); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        Causa raíz.<br>
                                        <textarea  class="input_field_lg seccionTecnico" name="raiz" rows="5"><?php echo set_value('raiz');?></textarea>
                                        <br>
                                        <?php echo form_error('raiz', '<span class="error">', '</span>'); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <p style="color:blue">DATOS DISTRIBUIDOR</p>
                                        <?=$this->config->item('base_hm_texto')?>
                                        TEL. <?=SUCURSAL_TEL?>
                                        <br><span style="color:darkblue;"><?=SUCURSAL_WEB?></span><br>
                                    </div>
                                </div>
                            </td>
                       </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12" align="right">
                <br>
                <input type="file" multiple name="uploadfiles[]" value="<?php echo set_value('uploadfiles');?>"><br>
                <!-- <input type="button" value="Subir" id='upload' ng-click="upload()"> -->
                <label for=""id="nombrearchivo"></label>
                <input type="hidden" name="tempFile" value="<?php if(isset($archivoTemp)) echo $archivoTemp;?>">
                <br>
                <button type="button" class="btn btn-dark" id="cotizaExt" style="color:white;">
                    <i id="iconCoti" class="far fa-eye" style="font-size: 14px;"></i> Cotización
                </button>
                <br>
                <input type="hidden" id="divCotizacion" value="0">
                <label for="" id="MsmModal"></label>
            </div>
        </div>

        <br>
        <!-- Mostramos seccion para dar de alta una cotización -->
        <div class="row" style="border: 2px solid black;display:none;height:750px;min-width:100%;" id="espacioCoti">
            <!-- <div class="col-md-12" style="height:250px;min-width:100%;"> -->
            <iframe class="embed-responsive-item" src="<?php echo base_url().'Alta_Presupuesto/'.((isset($idCita)) ? $idCita : '0'); ?>" frameborder="0" id="ventanaOculta" style="height:100%;width:100%;"></iframe>
            <!-- </div> -->
            <br><br>
        </div>

        <div class="row">
            <div class="col-sm-3" align="center">
                NOMBRE Y FIRMA DEL ASESOR
                <br>
                <input type="hidden" id="rutaFirmaAsesor" name="rutaFirmaAsesor" value="<?= set_value('rutaFirmaAsesor');?>">
                <img class="marcoImg" src="<?php if(set_value('rutaFirmaAsesor') != "") echo set_value('rutaFirmaAsesor');  ?>" id="firmaFirmaAsesor" alt="" style="height:2cm;width:4cm;">
                <br>
                <input type="text" class="input_field asesorname" id="asesorname" name="asesorNombre" value="<?php echo set_value('asesorNombre'); ?>"><br>
                <?php echo form_error('rutaFirmaAsesor', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('asesorNombre', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>

            <div class="col-sm-3" align="center">
                NOMBRE Y FIRMA DEL TÉCNICO
                <br>
                <input type="hidden" id="rutaFirmaTecnico" name="rutaFirmaTecnico" value="<?= set_value('rutaFirmaTecnico');?>" style="background-color:#ddd;">
                <img class="marcoImg" src="<?php if(set_value('rutaFirmaTecnico') != "") echo set_value('rutaFirmaTecnico');  ?>" id="firmaFirmaTecnico" alt="" style="height:2cm;width:4cm;">
                <br>
                <!-- <input type="text" class="input_field" name="" id="tecniconame" style="width:100%;" value="<?= set_value('nombreTecnico');?>" style="background-color:#ddd;"> -->
                <input type="text" class="input_field" name="nombreTecnico" id="tecniconame" style="width:100%;" value="<?= set_value('nombreTecnico');?>">
                <?php echo form_error('rutaFirmaTecnico', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('nombreTecnico', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>
            <div class="col-sm-3" align="center">
                NOMBRE Y FIRMA DEL JEFE DE TALLER
                <br>
                <input type="hidden" id="rutaFirmaJefeTaller" name="rutaFirmaJefeTaller" value="<?= set_value('rutaFirmaJefeTaller');?>" style="background-color:#ddd;">
                <img class="marcoImg" src="<?php if(set_value('rutaFirmaJefeTaller') != "") echo set_value('rutaFirmaJefeTaller');  ?>" id="firmaFirmaJefeTaller" alt="" style="height:2cm;width:4cm;">
                <br>
                <input type="text" class="input_field" name="JefeTallerNombre" id="JefeTallerNombre" style="width:100%;" value="JUAN CHAVEZ" style="background-color:#ddd;">
                <?php echo form_error('rutaFirmaJefeTaller', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('JefeTallerNombre', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>

            <div class="col-sm-3" align="center">
                NOMBRE Y FIRMA DEL CLIENTE
                <br>
                <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?= set_value('rutaFirmaConsumidor');?>" style="background-color:#ddd;">
                <img class="marcoImg" src="<?php if(set_value('rutaFirmaConsumidor') != "") echo set_value('rutaFirmaConsumidor');  ?>" id="firmaFirmaConsumidor" alt="" style="height:2cm;width:4cm;">
                <br>
                <input type="text" class="input_field nombreCliente" name="nombreConsumidor" id="nombreConsumidor" style="width:100%;" value="<?= set_value('nombreConsumidor');?>" style="background-color:#ddd;">
                <?php echo form_error('rutaFirmaConsumidor', '<span class="error">', '</span>'); ?><br>
                <?php echo form_error('nombreConsumidor', '<span class="error">', '</span>'); ?><br>
                <br><br>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3" align="center" style="margin-bottom: 10px;">
                <a id="Asesor_firma" class="cuadroFirma btn btn-primary" data-value="Asesor" data-target="#firmaDigital" data-toggle="modal" style="color:white;">Firma Asesor</a>
                <br>
            </div>
            <div class="col-md-3" align="center" style="margin-bottom: 10px;">
                <a id="Tecnico_firma" class="cuadroFirma btn btn-primary" data-value="Tecnico" style="color:white;" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Técnico</a>
                <br>
            </div>
            <div class="col-md-3" align="center" style="margin-bottom: 10px;">
                <a id="JefeTaller_firma" class="cuadroFirma btn btn-primary" data-value="JefeTaller" style="color:white;" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Jefe de Taller </a>
                <br>
            </div>
            <div class="col-md-3" align="center">
                <a id="Cliente_firma" class="cuadroFirma btn btn-primary" data-value="Cliente" style="color:white;" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Cliente</a>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-md-12" align="center">
                <i class="fas fa-spinner cargaIcono"></i>
                <h5 style="color:darkblue;text-align: center;font-weight: bold;" id="msm_envio"></h5>
                <br>
                
                <input type="button" class="btn btn-success" name="enviar" id="guardarInspeccion" value="Guardar Hoja Multipunto">
                <!-- <button class="btn btn-primary" ng-click="save()" value="">Guardar Orden</button> -->
                <input type="hidden" name="idCita" id="idCita" value="<?php if(isset($idCita)) echo $idCita; else echo "0";  ?>">
                
                <!-- Para el tecnico no se puede editar si la unidad no se esta trabajando  -->                
                <input type="hidden" name="" id="unidadTrabajando" value="<?php if(isset($unidadTrabajando)) echo $unidadTrabajando; else echo "FALSE";  ?>">

                <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; else echo "GEN"; ?>">
                <!-- <input type="hidden" name="cargaFormulario" id="estatusSlider" value="1"> -->
                <input type="hidden" name="cargaFormulario" id="estatusSlider" value="<?php if(isset($modalidadFormulario)) echo $modalidadFormulario; else echo "1";  ?>">
                <input type="hidden" name="asesorLogin" id="asesorLogin" value="<?php if(set_value('asesorLogin') != "") echo set_value('asesorLogin'); elseif (isset($asesor)) echo $asesor;?>">
                <input type="hidden" name="" id="formularioHoja" value="MultipuntoHoja">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            </div>
        </div>
    </form>
    <br><br>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para cotizacion externa -->
<!-- <div class="modal fade " id="cotizacionModal" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Cotización.</h5>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" frameborder="0" id="ventanaOculta" style="z-index:1000;overflow-y: scroll;height:100%;"></iframe>
                </div>
                <br><br>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div> -->
