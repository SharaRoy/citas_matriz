<div style="margin:5px;">
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">LISTADO HOJA MULTIPUNTO</h3>
            <br>

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-3">
                            <h5>Busqueda No. Orden:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_campo" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-md-3">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_HM" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                        <!-- formulario de busqueda -->
                    </div>

                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ccc;">
                                    <td align="center" style="width: 10%;"><strong>OR.</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" style="width: 20%;"><strong>CLIENTE</strong></td>
                                    <td align="center" style="width: 20%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 20%;"><strong>TÉCNICO</strong></td>
                                    <td align="center" style="width: 20%;"><strong>SISTEMA</strong></td>
                                    <td align="center" style="width: 20%;"><strong>COMPONENTE</strong></td>
                                    <td align="center" style="width: 20%;"><strong>CAUSA RAÍZ</strong></td>
                                    <td align="center" style="width: 20%;"><strong>COMENTARIO</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <td align='center' style='width:10%;vertical-align:middle;'>
                                                <?php echo $idOrden[$index]; ?>
                                                <input type='hidden' id='orden_<?php echo $index+1; ?>' value='<?php echo $idOrden[$index]; ?>'>
                                            </td>
                                            <td align='center' style='width:10%;vertical-align:middle;'>
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align='center' style='width:15%;vertical-align:middle;'>
                                                <?php echo $nombreCliente[$index]; ?>
                                                <input type='hidden' id='nombreCliente_<?php echo $index+1; ?>' value='<?php echo $nombreCliente[$index]; ?>'>
                                            </td>
                                            <td align='center' style='width:15%;vertical-align:middle;'>
                                                <?php echo $asesor[$index]; ?>
                                                <input type='hidden' id='asesor_<?php echo $index+1; ?>' value='<?php echo $asesor[$index]; ?>'>
                                            </td>
                                            <td align='center' style='width:15%;vertical-align:middle;'>
                                                <?php echo $tecnico[$index]; ?>
                                                <input type='hidden' id='tecnico_<?php echo $index+1; ?>' value='<?php echo $tecnico[$index]; ?>'>
                                            </td>
                                            <td align='center' style='width:10%;vertical-align:middle;'>
                                                <?php echo $sistema[$index]; ?>
                                                <input type='hidden' id='fechaCaptura_<?php echo $index+1; ?>' value='<?php echo $fechaCaptura[$index]; ?>'>
                                                <input type='hidden' id='sistema_<?php echo $index+1; ?>' value='<?php echo $sistema[$index]; ?>'>
                                            </td>
                                            <td align='center' style='width:10%;vertical-align:middle;'>
                                                <?php echo $componente[$index]; ?>
                                                <input type='hidden' id='componente_<?php echo $index+1; ?>' value='<?php echo $componente[$index]; ?>'>
                                            </td>
                                            <td align='center' style='width:15%;vertical-align:middle;'>
                                                <?php echo $causaRaiz[$index]; ?>
                                                <input type='hidden' id='causaRaiz_<?php echo $index+1; ?>' value='<?php echo $causaRaiz[$index]; ?>'>
                                            </td>
                                            <td align='center' style='width:15%;vertical-align:middle;'>
                                                <?php echo $comentario[$index]; ?>
                                                <input type='hidden' id='comentario_<?php echo $index+1; ?>' value='<?php if($comentario != '') echo $comentario[$index]; else echo 'Sin comentarios'; ?>'>
                                            </td>
                                            <td align='center' style='width:15%;'>
                                                <a class='btn btn-primary comentarioHM' style='color:white;' data-id='<?php echo $index+1; ?>' data-target='#pregunta'  data-toggle='modal'>
                                                    Agregar <br> comentario
                                                </a>
                                            </td>

                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <!-- Si no existen registros que mostrar de ninguno de las dos tablas -->
                                <?php if ((!isset($idOrden))): ?>
                                    <tr>
                                        <td colspan="8" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';" name="" value="Regresar">
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="pregunta" tabindex="-1" style="" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" align="center">
                <br>
                <h3>Información del registro:</h3>

                <div class="row">
                    <div class="col-sm-4" align="left">
                        <label style="font-size:13px;color:darkblue;">CLIENTE:</label>
                        <br>
                        <label style="font-size:13px;" id="infoRegistroScita3"></label>
                    </div>
                    <div class="col-sm-4" align="left">
                        <label style="font-size:13px;color:darkblue;">ASESOR:</label>
                        <br>
                        <label style="font-size:13px;" id="infoRegistroScita4"></label>
                    </div>
                    <div class="col-sm-4" align="left">
                        <label style="font-size:13px;color:darkblue;">TÉCNICO:</label>
                        <br>
                        <label style="font-size:13px;" id="infoRegistroScita5"></label>
                    </div>
                </div>

                <hr>
                <div class="row" style="font-size:13px;">
                    <div class="col-sm-4" align="right">
                        <h4 for="idOrden">OS:</h4>
                    </div>
                    <div class="col-sm-4" align="left">
                        <input type="text" class="form-control" name="idOrden" id="idOrden" style="width:100%;" disabled>
                    </div>
                </div>

                <div class="row" style="font-size:13px;">
                    <div class="col-sm-6">
                        <h5>Comentarios:</h5>
                        <textarea rows='2' id='comentario' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                    <div class="col-sm-6">
                        <h5>Sistema:</h5>
                        <textarea rows='2' id='sistema' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                </div>

                <div class="row" style="font-size:13px;">
                    <div class="col-sm-6">
                        <h5>Componetes:</h5>
                        <textarea rows='2' id='componente' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>
                        <br><br>
                    </div>
                    <div class="col-sm-6">
                        <h5>Causa Raíz:</h5>
                        <textarea rows='2' id='causa' class="form-control" style='width:100%;text-align:left;font-size:12px;'></textarea>

                    </div>
                </div>
                <br><br>
                <h4 id="errorIdCitaComentarioHM" align="center"></h4>
            </div>
            <div class="modal-footer" align="center">
                <input type="button" onclick="envioComentario()" id="guardarEvento" class="btn btn-success" value="Guardar">
                <button type="button" onclick="javascript:window.location.reload();" class="btn btn-info" data-dismiss="modal" id="cancelar" name="cancelar">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para mostrar el audio de la voz cliente-->
<div class="modal fade" id="muestraAudio" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="tituloAudio">Evidencia de audio</h3>
            </div>
            <div class="modal-body" align="center">
                <audio src="" controls="controls" type="audio/*" preload="preload" style="width:100%;" id="reproductor">
                  Your browser does not support the audio element.
                </audio>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>
