<style media="screen">
    #encabezado {
        color: #fff;
        background-color: #337ab7;
        border-color: #337ab7;
        border-radius: 4px;
    }

    p, label{
        text-align: justify;
        font-size:8px;
        color: black;
    }

    .tituloTxt{
        font-size:9px;
        color: #337ab7;
    }
    .division{
        background-color: #ddd;
    }
    .headers_table{
        border:1px solid black;
        border-style: double;
    }
    td,tr{
        padding: 0px 0px 0px 0px !important;
        font-size: 8px;
        color:black;
    }
</style>

<page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm">
    <h5 align="center" style="">
        <strong>REQUISICIÓN : <?php if(isset($requisicion)) echo $requisicion; ?></strong>
    </h5>
    <table style="width:100%;border-radius: 4px;">
        <tr>
            <td colspan="6">
                <br><br>
                PÚBLICO
                <br><br>
            </td>
            <td colspan="4" align="center">
                <br><br>
                <?php if(isset($vendedor)) echo $vendedor; else echo "<br>"; ?>
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br><br>
                <?php if(isset($ordenT)) echo $ordenT; else echo "<br>"; ?>
                <br><br><br>
            </td>
            <td>
                <br>
            </td>
            <td colspan="3">
                <br><br>
                <?php if(isset($nombreCliente)) echo $nombreCliente; else echo "<br>"; ?>
                <br><br><br>
            </td>
            <td colspan="4" align="right">
                <br><br>
                <?php if(isset($dia)) echo $dia." de ".$mes." del ".$anio." ".$diaSemeana." ".$hora; else echo "<br>"; ?>
                <br><br><br>
            </td>
        </tr>
        <!-- <tr>
            <td style="width:7%;border-bottom:1px solid #337ab7;" align="center"><strong>CANTIDAD</strong></td>
            <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>  D E S C R I P C I Ó N</strong></td>
            <td style="width:26%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>NUM. PIEZA</strong></td>
            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO PZA</strong></td>
            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>HORAS</strong></td>
            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO MO</strong></td>
            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>AUTORIZÓ</strong></td>
        </tr> -->
        <?php if (isset($cantidad)): ?>
            <?php foreach ($cantidad as $index => $value): ?>
                <tr>
                    <td style="width:7%;" align="center">
                        <label for=""><?php echo $cantidad[$index]; ?></label>
                    </td>
                    <td style="width:7%;" align="center">
                        <label for=""><?php echo $gpo[$index]; ?></label>
                    </td>
                    <td style="width:7%;" align="center">
                        <label for=""><?php echo $prefijo[$index]; ?></label>
                    </td>
                    <td style="width:8%;" align="center">
                        <label for=""><?php echo $basico[$index]; ?></label>
                    </td>
                    <td style="width:8%;" align="center">
                        <label for=""><?php echo $sufijo[$index]; ?></label>
                    </td>
                    <td style="width:35%;" align="center">
                        <label for=""><?php echo $descripcion[$index]; ?></label>
                    </td>
                    <td style="width:7%;" align="center">
                        <label for=""><?php echo $ubicacion[$index]; ?></label>
                    </td>
                    <td style="width:7%;" align="center">
                        <label for=""><?php echo $renglon[$index]; ?></label>
                    </td>
                    <td style="width:7%;" align="center">
                        <label for=""><?php echo number_format($precio_lista[$index],2); ?></label>
                    </td>
                    <td style="width:5%;" align="center">
                        <label for=""><?php echo number_format($precio[$index],2); ?></label>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>
</page>
