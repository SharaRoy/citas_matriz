<div class="" style="margin:20px;" align="center">
    <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:320px;height:100px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."multipunto/Cotizaciones/validateForm"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <?php if (isset($aceptoTermino)): ?>
            <?php if ($aceptoTermino == ""): ?>
                <div class="alert alert-primary" role="alert">
                    Visualiza la cotización y da clic en "Aceptar la cotización", "Rechazar la cotización" o puedes seleccionar una a una las refacciones.
                </div>
            <?php elseif ($aceptoTermino == "Si"): ?>
                <div class="alert alert-success" role="alert" align="center">
                    Cotización Aceptada Completa.
                </div>
            <?php elseif ($aceptoTermino == "Val"): ?>
                <div class="alert alert-warning" role="alert" align="center">
                    Cotización Afectada por Detalle.
                </div>
            <?php else: ?>
                <div class="alert alert-danger" role="alert" align="center">
                    Cotización Rechazada Completa.
                </div>
            <?php endif; ?>
        <?php else: ?>
            <div class="alert alert-primary" role="alert">
                No se cargo correctamente la información.
            </div>
        <?php endif; ?>

        <table style="width:100%;border:1px solid #337ab7;border-radius: 4px;">
            <tr>
                <td style="width:7%;border-bottom:1px solid #337ab7;" align="center"><strong>CANTIDAD</strong></td>
                <td style="width:42%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>  D E S C R I P C I Ó N</strong></td>
                <td style="width:18%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>NUM. PIEZA</strong></td>
                <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO PZA</strong></td>
                <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>HORAS</strong></td>
                <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO MO</strong></td>
                <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO TOTAL</strong></td>
                <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                    <strong>REVISADO <br> (ASESOR)</strong>
                    <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($registrosControl)) if(count($registrosControl) > 0) echo count($registrosControl)+1; else echo "1"; else echo "1"; ?>">
                </td>
                <?php if (isset($registrosControl)): ?>
                    <td style="width:20%;border-bottom:1px solid #337ab7;" colspan="2" align="center"><strong>ELECCIÓN</strong></td>
                <?php endif; ?>
            </tr>
            <?php if (isset($registrosControl)): ?>
                <?php if (count($registrosControl)>0): ?>
                    <?php foreach ($registrosControl as $index => $value): ?>
                        <?php if ($aceptoTermino != "Val"): ?>
                            <?php if ($aceptoTermino == "Si"): ?>
                                <tr id="fila_indicador_<?php echo $index+1; ?>"style='background-color:#c7ecc7;'>
                            <?php elseif ($aceptoTermino == "No"): ?>
                                <tr id="fila_indicador_<?php echo $index+1; ?>" style='background-color:#f5acaa;'>
                            <?php else: ?>
                                <tr id="fila_indicador_<?php echo $index+1; ?>">
                            <?php endif; ?>
                        <?php else: ?>
                            <tr id="fila_indicador_<?php echo $index+1; ?>"  <?php if(count($registrosControl[$index])>9) if($registrosControl[$index][9] == "Aceptada") echo "style='background-color:#c7ecc7'"; else echo "style='background-color:#f5acaa'"; ?>>
                        <?php endif; ?>

                            <td style="border-bottom:1px solid #337ab7;height:20px;" align="center">
                                <label for=""><?php echo $registrosControl[$index][0]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][1]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align='center'>
                                <label for=""><?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][2]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][3]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][4]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <?php $conteo1 = (int)$registrosControl[$index][0] * (float)$registrosControl[$index][2]; ?>  
                                <?php $conteo2 = (float)$registrosControl[$index][4] * (float)$registrosControl[$index][3]; ?>
                                <label for="" style="color:blue;">$<?php echo number_format(($conteo1+$conteo2),2); ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                  <label for=""><?php echo $registrosControl[$index][5]; ?></label>
                            </td>
                            <td style="width: 10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <a id="interruptorA_<?php echo $index+1; ?>" class="btn-success btn multipleEleccion" title="Aceptar" onclick="aceptarItem(<?php echo $index+1; ?>)" style="color:white;margin:5px;" hidden>
                                    <i class="fas fa-check"></i>
                                </a>
                            </td>
                            <td style="width: 10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <a id="interruptorB_<?php echo $index+1; ?>" class="btn-danger btn multipleEleccion" title="Rechazar" onclick="rechazarItem(<?php echo $index+1; ?>)" style="color:white;margin:5px;" hidden>
                                    <i class="fas fa-times"></i>
                                </a>
                                <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $registrosControlRenglon[$index]; ?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php else: ?>
                <tr>
                    <td colspan="7" align="center" style="border-bottom:1px solid #337ab7;height:20px;background-color: #ddd;">
                        No se registro ninguna cotización.
                    </td>
                    <td style="width:20%;border:1px solid #337ab7;border-left:1px solid #337ab7;background-color: #ddd;" align="center"></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    SUB-TOTAL
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                    <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subTotalMaterial)) echo $subTotalMaterial; else echo "0"; ?>">
                </td>
            </tr>
            <tr>
                <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;color:red;">
                    CANCELADO
                </td>
                <td align="center" style="border:1px solid #337ab7;color:red;">
                    $<label for="" id="canceladoMaterialLabel"><?php if(isset($canceladoMaterial)) echo $canceladoMaterial; else echo "0"; ?></label>
                    <input type="hidden" id="canceladoMaterial" name="canceladoMaterial" value="0">
                </td>
            </tr>
            <tr>
                <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    I.V.A.
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                    <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($ivaMaterial)) echo $ivaMaterial; else echo "0"; ?>">
                </td>
            </tr>
            <tr>
                <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    PRESUPUESTO TOTAL
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label id="presupuestoMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format(($subTotalMaterial+$ivaMaterial),2); else echo "0"; ?></label>
                </td>
                <td colspan="2"></td>
            </tr>
            <tr style="border-top:1px solid #337ab7;">
                <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    ANTICIPO
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                    <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                </td>
                <td colspan="3">
                    <label for="" id="anticipoNota" style="color:blue;"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    TOTAL
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                    <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($totalMaterial)) echo $totalMaterial; else echo "0"; ?>">
                </td>
            </tr>
            <!-- <tr>
                <td colspan="5" align="right">
                    FIRMA DEL ASESOR QUE APRUEBA
                </td>
                <td align="center">
                    <?php if (isset($firmaAsesor)): ?>
                        <?php if ($firmaAsesor != ""): ?>
                            <img class='marcoImg' src='<?php echo base_url().$firmaAsesor ?>' id='' style='width:100px;height:40px;;'>
                        <?php else: ?>
                            <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' style='width:100px;height:40px;;'>
                        <?php endif; ?>
                    <?php else: ?>
                        <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' style='width:100px;height:40px;;'>
                    <?php endif; ?>
              </td>
            </tr> -->
        </table>

        <!-- <br><br>
        <div class="row">
            <div class="col-sm-12" align="right">
                <?php if (isset($archivo)): ?>
                    <?php for ($i = 0; $i < count($archivo); $i++): ?>
                        <?php if ($archivo[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo[$i]; ?>" class="btn btn-info" target="_blank">Ver documento (<?php echo $i+1; ?>)</a>
                            <?php if ((($i+1)% 2) == 0): ?>
                                <br>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endfor; ?>
                <?php else: ?>
                    <a  class="btn btn-info" onclick="event.preventDefault()">Sin documentos</a>
                <?php endif; ?>
            </div>
        </div> -->

        <br><br>
        <?php if (isset($aceptoTermino)): ?>
            <?php if ($aceptoTermino == ""): ?>
                <div class="row">
                    <div class="col-sm-1" align="center"></div>
                  	<div class="col-sm-3" align="center">
                    		<a id="aceptar" class="btn btn-success multipleEleccionBtn" data-value="Aceptar" data-target="#AlertaModal" data-toggle="modal" style="color:white;margin-top:20px;">Aceptar todo</a>
                  	</div>
                    <div class="col-sm-3" align="center">
                    		<a id="rechazar" class="btn btn-danger multipleEleccionBtn" data-value="Rechazar" data-target="#AlertaModal" data-toggle="modal" style="color:white;margin-top:20px;">Rechazar todo</a>
                        <button type="button" id="seleccionarTodo" class="btn btn-danger eleccionBtn" style="color:white;margin-top:20px;" hidden>Cancelar</button>
                  	</div>
                    <div class="col-sm-3" align="center">
                        <button type="button" id="seleccionarFilas" class="btn btn-primary multipleEleccionBtn" style="color:white;margin-top:20px;">Seleccionar individualmente</button>
                    		<button type="submit" class="btn btn-success" id="envioFormulario" style="color:white;margin-top:20px;" hidden> Guardar Cotización Adicional</button>
                  	</div>
                    <div class="col-sm-1" align="center">
                        <input type="hidden" id="limite" value="<?php if (isset($registrosControl)) if(count($registrosControl) > 0) echo count($registrosControl); else echo "1"; else echo "1"; ?>">
                        <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="5">
                        <input type="hidden" name="modoGuardado" id="modoGuardado" value="Actualizar">
                        <input type="hidden" name="" id="bandera" value="0">
                        <input type="hidden" name="idOrdenTemp" id="idOrdenTemp" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
                    </div>
                </div>
            <?php else: ?>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <?php if ($aceptoTermino == "Si"): ?>
                            <a class="btn btn-success" style="color:white;">Cotización Aceptada</a>
                        <?php elseif ($aceptoTermino == "Val"): ?>
                            <a class="btn btn-warning" style="color:white;">Cotización revisada por artículo</a>
                        <?php else: ?>
                            <a class="btn btn-danger" style="color:white;">Cotización Rechazada</a>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </form>
</div>

<!-- Modal para la envio de decision-->
<div class="modal fade" id="AlertaModal" role="dialog" data-backdrop="static" data-keyboard="false" style="background-color:">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="AlertaModalCuerpo">
            <div class="modal-body">
                <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;display:none;'>
                    Su navegador no soporta canvas
                </canvas>
                <form class="" id="formularioAceptaCoti" action="" method="post">
                    <br><br>
                    <h5 class="modal-title" id="tituloForm" align="center"></h5>
                    <br>
                    <div class="row">
                        <div class="col-md-12"  align="center">
                            <i class="fas fa-spinner cargaIcono"></i>
                            <br>
                            <label for="" class="error" id="errorEnvioCotizacion"></label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12" align="right">
                            <input type="hidden" name="desicionCoti" id="desicionCoti" value="">
                            <input type="hidden" name="idOrden" id="idOrden" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelarCoti">Cancelar</button>
                            <button type="button" class="btn btn-primary" id="enviarCotizacionApro">Confirmar</button>
                        </div>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
