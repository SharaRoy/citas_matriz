<div style="margin:20px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."multipunto/CotizacionesExternas/validateForm"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row table-responsive">
            <div class="col-sm-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="form-control" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(isset($idOrden)) echo $idOrden; ?>" style="width:100%;">
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <!-- <div class="col-sm-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php echo set_value('noSerie');?>" style="width:100%;">
                <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-sm-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;">
                <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-sm-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;">
                <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div> -->
        </div>

        <!-- <br>
        <div class="row table-responsive">
            <div class="col-sm-4">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;">
                <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-sm-4">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;">
                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-sm-4">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;">
                <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div> -->

        <div class="row table-responsive">
            <!-- <div class="col-sm-4"> -->
                <!-- No. Orden : <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTemp" value="<?php if(isset($idOrden)) echo $idOrden; ?>" style="width:70%;">
                <br>
                <?php echo form_error('idOrdenTemp', '<span class="error">', '</span>'); ?> -->
            <!-- </div> -->
            <div class="col-sm-10" align="right">
                <label for="" class="error"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                <br>
                <label for="" class="error"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
            </div>
            <div class="col-sm-1" align="right">
                <a id="agregarCM" class="btn btn-success" style="color:white;">
                    <i class="fas fa-plus"></i>
                </a>
            </div>
            <div class="col-sm-1" align="right">
                <a id="eliminarCM" class="btn btn-danger" style="color:white; width: 1cm;" >
                    <i class="fas fa-minus"></i>
                </a>
            </div>
        </div>

        <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
            <thead>
                <tr>
                    <!-- <td style="width:11%;border-bottom:1px solid #337ab7;height:30px;" align="center">
                        REQ. No.
                    </td> -->
                    <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        CANTIDAD
                    </td>
                    <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        D E S C R I P C I Ó N
                    </td>
                    <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        <!-- OPERARIO -->
                        NUM. PIEZA
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center" colspan="2">
                        EXISTE
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        <!-- DEPTO. -->
                        COSTO PZA
                    </td>
                    <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        <!-- OPERARIO -->
                        HORAS
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        <!-- FECHA -->
                        COSTO MO
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        REVISADO <br> (ASESOR)
                        <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($registrosControl)) if(count($registrosControl) > 0) echo count($registrosControl)+1; else echo "1"; else echo "1"; ?>">
                    </td>
                    <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">

                    </td>
                </tr>
                <tr>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        SI
                    </td>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        NO
                    </td>
                </tr>
            </thead>
            <tbody id="cuerpoMateriales">
                <!-- Verificamos si existe la informacion -->
                <?php if (isset($registrosControl)): ?>
                    <!-- Comprobamos que haya registros guardados -->
                    <?php if (count($registrosControl)>0): ?>
                        <?php foreach ($registrosControl as $index => $valor): ?>
                            <tr id='fila_<?php echo $index+1; ?>'>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='text' class='input_field' onblur='cantidadCollet(<?php echo $index+1; ?>)' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $registrosControl[$index][0]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <!-- <input type='text' class='input_field' onblur='descripcionCollect(<?php echo $index+1; ?>)' id='descripcion_<?php echo $index+1; ?>' value="<?php echo $registrosControl[$index][1]; ?>" style='width:90%;text-align:left;' disabled> -->
                                    <textarea rows='1' class='input_field_lg' onblur='descripcionCollect(<?php echo $index+1; ?>)' id='descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $registrosControl[$index][1]; ?></textarea>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <!-- <input type='text' class='input_field' onblur='piezaCollet(<?php echo $index+1; ?>)' id='pieza_<?php echo $index+1; ?>'  value='<?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?>' style='width:90%;text-align:left;' disabled> -->
                                    <textarea rows='1' class='input_field_lg' onblur='piezaCollet(<?php echo $index+1; ?>)' id='pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?></textarea>
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='radio' id='apuntaFocus_<?php echo $index+1; ?>' class='input_field' onclick='existeCollet(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="SI" <?php if(isset($registrosControl[$index][8])) if($registrosControl[$index][8] == "SI") echo "checked"; ?>>
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='radio' class='input_field' onclick='existeCollet(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="NO" <?php if(isset($registrosControl[$index][8])) if($registrosControl[$index][8] == "NO") echo "checked"; ?>>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    $<input type='text' class='input_field' onblur='costoCollet(<?php echo $index+1; ?>)' id='costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][2]; ?>' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='text' class='input_field' onblur='horasCollet(<?php echo $index+1; ?>)' id='horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][3]; ?>' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    $<input type='text' id='totalReng_<?php echo $index+1; ?>' onblur="autosuma(<?php echo $index+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;'>
                                    <input type='hidden' id='totalOperacion_<?php echo $index+1; ?>' value='<?php if($registrosControl[$index][4] == "0") {echo "0";} else {if(isset($registrosControl[$index][7])) echo $registrosControl[$index][7]; else echo "0";} ?>'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(<?php echo $index+1; ?>)' value="<?php echo $index+1; ?>" id='autorizo_<?php echo $index+1; ?>' <?php if($registrosControl[$index][5] == "SI") echo "checked"; ?>>
                                    <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $registrosControlRenglon[$index]; ?>">
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <!-- <a id='refaccionBusqueda_<?php echo $index+1 ?>' class='refaccionesBox btn btn-warning' data-value="<?php echo $index+1; ?>" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                        <i class="fa fa-search"></i>
                                    </a> -->
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <!-- Creamos una linea vacia para que puedna ingresar -->
                        <tr id='fila_<?php echo count($registrosControl)+1 ?>'>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='text' class='input_field' onblur='cantidadCollet(<?php echo count($registrosControl)+1; ?>)' id='cantidad_<?php echo count($registrosControl)+1 ?>' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <!-- <input type='text' class='input_field' id='descripcion_<?php echo count($registrosControl)+1; ?>' onblur='descripcionCollect(<?php echo count($registrosControl)+1; ?>)' style='width:90%;text-align:left;'> -->
                                <textarea rows='1' class='input_field_lg' id='descripcion_<?php echo count($registrosControl)+1; ?>' onblur='descripcionCollect(<?php echo count($registrosControl)+1; ?>)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <!-- <input type='text' class='input_field' id='pieza_<?php echo count($registrosControl)+1; ?>' onblur='piezaCollet(<?php echo count($registrosControl)+1; ?>)' value='' style='width:90%;text-align:left;'> -->
                                <textarea rows='1' class='input_field_lg' id='pieza_<?php echo count($registrosControl)+1; ?>' onblur='piezaCollet(<?php echo count($registrosControl)+1; ?>)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaFocus_<?php echo count($registrosControl)+1; ?>' class='input_field' onclick='existeCollet(<?php echo count($registrosControl)+1; ?>)' name='existe_<?php echo count($registrosControl)+1; ?>' value="SI">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' class='input_field' onclick='existeCollet(<?php echo count($registrosControl)+1; ?>)' name='existe_<?php echo count($registrosControl)+1; ?>' value="NO">
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='text' class='input_field' onblur='costoCollet(<?php echo count($registrosControl)+1; ?>)' id='costoCM_<?php echo count($registrosControl)+1 ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='text' class='input_field' onblur='horasCollet(<?php echo count($registrosControl)+1; ?>)' id='horas_<?php echo count($registrosControl)+1 ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='text' id='totalReng_<?php echo count($registrosControl)+1 ?>' onblur="autosuma(<?php echo count($registrosControl)+1 ?>)" class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                <input type='hidden' id='totalOperacion_<?php echo count($registrosControl)+1 ?>' value='0'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='checkbox' onclick='autorizaColet(<?php echo count($registrosControl)+1; ?>)' value="<?php echo count($registrosControl)+1 ?>" class='input_field autorizaCheck' id='autorizo_<?php echo count($registrosControl)+1 ?>' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                <input type='hidden' id='valoresCM_<?php echo count($registrosControl)+1 ?>' name='registros[]'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <!-- <a id='refaccionBusqueda_<?php echo count($registrosControl)+1; ?>' class='refaccionesBox btn btn-warning' data-value="<?php echo count($registrosControl)+1; ?>" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                    <i class="fa fa-search"></i>
                                </a> -->
                            </td>
                        </tr>
                    <?php else: ?>
                        <tr id='fila_1'>
                            <td align='center'>
                                <input type='text' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <!-- <input type='text' class='input_field' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'> -->
                                <textarea rows='1' class='input_field_lg' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <!-- <input type='text' class='input_field' id='pieza_1' onblur='piezaCollet(1)' value='' style='width:90%;text-align:left;'> -->
                                <textarea rows='1' class='input_field_lg' id='pieza_1' onblur='piezaCollet(1)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaFocus_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="SI">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' class='input_field' onclick='existeCollet(1)' name='existe_1' value="NO">
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='text' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='text' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='text' id='totalReng_1' class='input_field'  onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                <input type='hidden' id='totalOperacion_1' value='0'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                <input type='hidden' id='valoresCM_1' name='registros[]'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <!-- <a id='refaccionBusqueda_1' class='refaccionesBox btn btn-warning' data-value="1" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                    <i class="fa fa-search"></i>
                                </a> -->
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php else: ?>
                    <tr id='fila_1'>
                        <td align='center'>
                            <input type='text' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <!-- <input type='text' class='input_field' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'> -->
                            <textarea rows='1' class='input_field_lg' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'></textarea>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <!-- <input type='text' class='input_field' id='pieza_1' onblur='piezaCollet(1)' value='' style='width:90%;text-align:left;'> -->
                            <textarea rows='1' class='input_field_lg' id='pieza_1' onblur='piezaCollet(1)' style='width:90%;text-align:left;'></textarea>
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <input type='radio' id='apuntaFocus_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="SI">
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <input type='radio' class='input_field' onclick='existeCollet(1)' name='existe_1' value="NO">
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            $<input type='text' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <input type='text' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            $<input type='text' id='totalReng_1' class='input_field' onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                            <input type='hidden' id='totalOperacion_1' value='0'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                            <input type='hidden' id='valoresCM_1' name='registros[]'>
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <!-- <a id='refaccionBusqueda_1' class='refaccionesBox btn btn-warning' data-value="1" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                <i class="fa fa-search"></i>
                            </a> -->
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" align="right">
                        SUB-TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subTotalMaterial)) echo $subTotalMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        I.V.A.
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($ivaMaterial)) echo $ivaMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        PRESUPUESTO TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label id="presupuestoMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format(($subTotalMaterial+$ivaMaterial),2); else echo "0"; ?></label>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr style="border-top:1px solid #337ab7;">
                    <td colspan="7" align="right">
                        ANTICIPO
                    </td>
                    <td align="center">
                        $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                        <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                    </td>
                    <td colspan="2">
                        <label for="" id="anticipoNota" style="color:blue;"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($totalMaterial)) echo $totalMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <!-- <tr>
                    <td colspan="7" align="right">
                        FIRMA DEL ASESOR QUE APRUEBA
                    </td>
                    <td align="center" colspan="3">
                      <?php if (isset($firmaAsesor)): ?>
                          <?php if ($firmaAsesor != ""): ?>
                              <img class='marcoImg' src='<?php echo base_url().$firmaAsesor ?>' id='' style='width:80px;height:30px;'>
                              <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php echo $firmaAsesor ?>'>
                          <?php else: ?>
                              <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value=''>
                              <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                              <a id="AsesorCostos" class='cuadroFirma btn btn-primary' data-value="AsesorCostos" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                  <i class='fas fa-pen-fancy'></i>
                              </a>
                          <?php endif; ?>
                      <?php else: ?>
                          <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value=''>
                          <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                          <a id="AsesorCostos" class='cuadroFirma btn btn-primary' data-value="AsesorCostos" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                              <i class='fas fa-pen-fancy'></i>
                          </a>
                      <?php endif; ?>
                    </td>
                </tr> -->
            </tfoot>
        </table>

        <br><br>
        <!-- <div class="row" <?php if(isset($dirige)) if($dirige == "ASE") echo "hidden"; else echo ""; else echo "hidden"; ?>>
            <div class="col-sm-6">
                <h4>Comentario del técnico para Refacciones:</h4><br>
                <?php if (isset($dirige)): ?>
                    <?php if ($dirige == "TEC"): ?>
                        <textarea id="comentarioTecnico" name="comentarioTecnico" rows="2" style="width:100%;border-radius:4px;" placeholder="Clave de la refacción..."><?php if(isset($comentarioTecnico)) echo $comentarioTecnico;?></textarea>
                    <?php else: ?>
                        <label for="" style="font-size:12px;"><?php if(isset($comentarioTecnico)){ if($comentarioTecnico != "") echo $comentarioTecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="font-size:12px;"><?php if(isset($comentarioTecnico)){ if($comentarioTecnico != "") echo $comentarioTecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                <?php endif; ?>
            </div>
            <div class="col-sm-6" align="right">
                <?php if (isset($archivoTecnico)): ?>
                    <?php for ($i = 0; $i < count($archivoTecnico); $i++): ?>
                      <?php if ($archivoTecnico[$i] != ""): ?>
                          <a href="<?php echo base_url().$archivoTecnico[$i]; ?>" class="btn btn-primary" target="_blank">Adj. (<?php echo $i+1; ?>)</a>
                          <?php if ((($i+1)% 3) == 0): ?>
                              <br>
                          <?php endif; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                    <?php if (count($archivoTecnico) == 0): ?>
                        <a href="" class="btn btn-primary" target="_blank">Sin archivos</a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="row" <?php if(isset($dirige)) if($dirige != "TEC") echo "hidden"; else echo ""; else echo "hidden"; ?>>
            <div class="col-sm-12" align="right">
                <label for="">Archivo(s) para Refacciones:</label><br>
                <input id="uploadfilesTec" type="file" accept="image/*" multiple name="uploadfilesTec[]"><br>
            </div>
        </div>

        <input type="hidden" name="tempFileTecnico" value="<?php if(isset($archivoImgTecnico)) echo $archivoImgTecnico;?>">

        <br>
        <div class="row" style="border-top:1px solid black;">
            <div class="col-sm-6">
                <h4>Comentarios del Jefe de Taller:</h4>
                <?php if (isset($dirige)): ?>
                    <?php if ($dirige == "JDT"): ?>
                        <textarea rows='2' name="comentariosExtra" id='comentario' style='width:100%;text-align:left;font-size:12px;'><?php if(set_value('comentariosExtra') != "") { echo set_value('comentariosExtra');} else { if(isset($comentario)) echo $comentario;}?></textarea>
                    <?php else: ?>
                        <label for="" style="font-size:12px;"><?php if(isset($comentario)){ if($comentario != "") echo $comentario; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="font-size:12px;"><?php if(isset($comentario)){ if($comentario != "") echo $comentario; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                <?php endif; ?>
            </div>
            <div class="col-sm-6" align="right" style="padding-top:10px;">
                <?php if (isset($archivo)): ?>
                    <?php $indice = 1; ?>
                    <?php for ($i = 0; $i < count($archivo); $i++): ?>
                      <?php if ($archivo[$i] != ""): ?>
                          <a href="<?php echo base_url().$archivo[$i]; ?>" class="btn btn-info" target="_blank">Doc. (<?php echo $indice; ?>)</a>
                          <?php $indice++; ?>
                          <?php if ((($i+1)% 3) == 0): ?>
                              <br>
                          <?php endif; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                <?php endif; ?>
                <input type="hidden" name="tempFile" value="<?php if(isset($archivoTxt)) echo $archivoTxt;?>">
                <br>
                <input type="file" multiple name="uploadfiles[]"><br>
            </div>
        </div> -->

        <div class="col-sm-12" align="center">
            <!-- <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>"> -->
            <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="2">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
            <input type="hidden" name="" id="envioRefacciones" value="<?php if(isset($refacciones)) echo $refacciones; else echo "0"; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
            <input type="hidden" name="tipoRegistro" id="tipoRegistro" value="<?php if(isset($tipoRegistro)) echo $tipoRegistro; ?>">
            <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="aceptarCotizacion" >Actualizar Cotización Adicional</button>
        </div>
    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Formulario de superusuario -->
<div class="modal fade " id="superUsuario" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Ingreso.</h5>
            </div>
            <div class="modal-body" style="font-size:12px;">
                <label for="" style="font-size: 14px;font-weight: initial;">Usuario:</label>
                <br>
                <input type="text" class="input_field" type="text" id="superUsuarioName" value="" style="width:100%;">
                <br><br>
                <label for="" style="font-size: 14px;font-weight: initial;">Password:</label>
                <br>
                <input type="password" class="input_field" type="text" id="superUsuarioPass" value="" style="width:100%;">
                <br><br><br>
                <h4 class="error" style="font-size: 16px;font-weight: initial;" align="center" id="superUsuarioError">:</h4>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="superUsuarioEnvio">Validar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para filtrar las refacciones-->
<div class="modal fade" id="refacciones" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel">Buscador de refacciones</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Gpo.</label>
                        <select class="input_field" id="grupoRefaccion" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                            <?php if (isset($grupo)): ?>
                                <?php foreach ($grupo as $index => $valor): ?>
                                    <option value="<?php echo $grupo[$index]; ?>"><?php echo $grupo[$index]; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Prefijo</label>
                        <select class="input_field" id="prefijoRefaccion" style="font-size:12px;width:100%;">
                            <option value=" ">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Básico</label>
                        <select class="input_field" id="basicoRefaccion" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Sufijo.</label>
                        <select class="input_field" id="sufijoRefaccion" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Descripción</label>
                        <textarea class="input_field-lg" id="descripcionRefacciones" style='width:100%;' disabled></textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="">F.U.E.</label>
                        <input type='text' class='input_field' id="fueRefacciones" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-3">
                        <label for="">Cla.</label>
                        <input type='text' class='input_field' id="claRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-3">
                        <label for="">P. Lista</label>
                        <input type='text' class='input_field' id="precioRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Cantidad</label>
                        <input type='number' min="0" max="" class='input_field' id="cantidadRefacciones" value="1" style='width:100%;'>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Ubicación</label>
                        <input type='text' class='input_field' id="ubicacionRefacciones" style='width:100%;' disabled>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="indiceOperacion" value="">
                <input type="hidden" name="" id="existeRefacciones" value="">
                <input type="hidden" name="" id="claveRefaccion" value="">
                <input type="hidden" name="" id="base_ajax" value="<?php echo base_url(); ?>">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarOperacion">Cerrar</button>
                <button type="button" class="btn btn-primary" name="" id="bajaOperacion" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
