<div style="margin:20px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."multipunto/CotizacionesExternas/validateForm"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row table-responsive">
            <div class="col-sm-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="form-control" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(isset($idOrden)) echo $idOrden; ?>" style="width:100%;">
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <!-- <div class="col-sm-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php echo set_value('noSerie');?>" style="width:100%;">
                <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-sm-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;">
                <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-sm-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;">
                <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div> -->
        </div>
        <!-- <br>
        <div class="row table-responsive">
            <div class="col-sm-4">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;">
                <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-sm-4">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;">
                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-sm-4">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;">
                <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div> -->
        <br>

        <div class="row table-responsive">
            <!-- <div class="col-sm-4"> -->
                <!-- No. Orden : <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTemp" value="<?php if(isset($idOrden)) echo $idOrden; ?>" style="width:70%;">
                <br>
                <?php echo form_error('idOrdenTemp', '<span class="error">', '</span>'); ?> -->
            <!-- </div> -->
            <div class="col-sm-10" align="right">
                <label for="" class="error"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                <br>
                <label for="" class="error"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
            </div>
            <div class="col-sm-1" align="right">
                <a id="agregarCM" class="btn btn-success" style="color:white;">
                    <i class="fas fa-plus"></i>
                </a>
            </div>
            <div class="col-sm-1" align="right">
                <a id="eliminarCM" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                    <i class="fas fa-minus"></i>
                </a>
            </div>
        </div>

        <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
            <thead>
                <tr>
                    <!-- <td style="width:11%;border-bottom:1px solid #337ab7;height:30px;" align="center">
                        REQ. No.
                    </td> -->
                    <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        CANTIDAD
                    </td>
                    <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        D E S C R I P C I Ó N
                    </td>
                    <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        <!-- OPERARIO -->
                        NUM. PIEZA
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center" colspan="2">
                        EXISTE
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        <!-- DEPTO. -->
                        COSTO PZA
                    </td>
                    <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        <!-- OPERARIO -->
                        HORAS
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        <!-- FECHA -->
                        COSTO MO
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        REVISADO <br> (ASESOR)
                        <input type="hidden" name="" id="indiceTablaMateria" value="1">
                    </td>
                    <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">

                    </td>
                </tr>
                <tr>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        SI
                    </td>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        NO
                    </td>
                </tr>
            </thead>
            <tbody id="cuerpoMateriales">
                <tr id='fila_1'>
                    <!-- <td style='width:11%;border-bottom:1px solid #337ab7;height:30px;' align='center'>
                        <input type='text' class='input_field' id='req_1' style='width:100%;text-align:left;'>
                    </td> -->
                    <td align='center' style="border:1px solid #337ab7;vertical-align: middle;">
                        <input type='text' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 48 && event.charCode <= 57' style='width:90%;'>
                    </td>
                    <td style='border:1px solid #337ab7;' align='center'>
                        <!-- <input type='text' class='input_field' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'> -->
                        <textarea rows='1' class='input_field_lg' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'></textarea>
                    </td>
                    <td style='border:1px solid #337ab7;' align='center'>
                        <!-- <input type='text' class='input_field' id='pieza_1' onblur='piezaCollet(1)' value='' style='width:90%;text-align:left;'> -->
                        <textarea rows='1' class='input_field_lg' id='pieza_1' onblur='piezaCollet(1)' style='width:90%;text-align:left;'></textarea>
                    </td>
                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                        <input type='radio' id='apuntaSi_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="SI">
                    </td>
                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                        <input type='radio' id='apuntaNo_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="NO">
                    </td>
                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                        $<input type='text' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                    </td>
                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                        <input type='text' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                    </td>
                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                        $<input type='text' id='totalReng_1' class='input_field ' onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                        <input type='hidden' id='totalOperacion_1' value='0'>
                    </td>
                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                        <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                        <input type='hidden' id='valoresCM_1' name='registros[]'>
                        <br>
                        <?php echo form_error('registros', '<span class="error">', '</span>'); ?>
                    </td>
                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                        <!-- <a id='refaccionBusqueda_1' class='refaccionesBox btn btn-warning' data-value="1" data-target="#refacciones" data-toggle="modal" style="color:white;">
                            <i class="fa fa-search"></i>
                        </a> -->
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" align="right">
                        SUB-TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subTotalMaterial)) echo $subTotalMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        I.V.A.
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($ivaMaterial)) echo $ivaMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        PRESUPUESTO TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label id="presupuestoMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format(($subTotalMaterial+$ivaMaterial),2); else echo "0"; ?></label>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr style="border-top:1px solid #337ab7;">
                    <td colspan="7" align="right">
                        ANTICIPO
                    </td>
                    <td align="center">
                        $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                        <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                    </td>
                    <td colspan="2">
                        <label for="" id="anticipoNota" style="color:blue;"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($totalMaterial)) echo $totalMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <!-- <tr>
                    <td colspan="7" align="right">
                        FIRMA DEL ASESOR QUE APRUEBA
                    </td>
                    <td align="center" colspan="2">
                      <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php echo set_value("rutaFirmaAsesorCostos"); ?>'>
                      <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                      <a id="AsesorCostos" class='cuadroFirma btn btn-primary' data-value="AsesorCostos" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                          <i class='fas fa-pen-fancy'></i>
                      </a>
                </tr> -->
            </tfoot>
        </table>

        <br><br>
        <!-- <div class="row">
            <div class="col-sm-12" align="right">
                <input type="file" multiple name="uploadfiles[]"><br>
            </div>
        </div>

        <br><br>
        <div class="row" style="border-top: 1px solid black;" <?php if(isset($dirige)) if($dirige != "TEC") echo "hidden"; else echo ""; else echo "hidden"; ?>>
            <div class="col-sm-5">
                <h4>Comentario del técnico para Refacciones:</h4><br>
                <textarea id="comentarioTecnico" name="comentarioTecnico" rows="2" style="width:100%;border-radius:4px;" placeholder="Clave de la refacción..."><?php echo set_value("comentarioTecnico"); ?></textarea>
            </div>
            <div class="col-sm-7">
                <label for="">Archivo(s) para Refacciones:</label><br>
                <input id="uploadfilesTec" type="file" accept="image/*" multiple name="uploadfilesTec[]" ><br>
            </div>
        </div>
        <input type="hidden" name="tempFileTecnico" value="<?php if(isset($archivoImgTecnico)) echo $archivoImgTecnico;?>"> -->

        <div class="col-sm-12" align="center">
            <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>">
            <input type="hidden" name="modoVistaFormulario" id="" value="1">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
            <input type="hidden" name="tipoRegistro" id="tipoRegistro" value="<?php if(isset($tipoRegistro)) echo $tipoRegistro; ?>">
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button> -->
            <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="aceptarCotizacion" disabled>Guardar Cotizacion Adicional</button>
        </div>
    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para filtrar las refacciones-->
<div class="modal fade" id="refacciones" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel">Buscador de refacciones</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Gpo.</label>
                        <select class="input_field" id="grupoRefaccion" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                            <?php if (isset($grupo)): ?>
                                <?php foreach ($grupo as $index => $valor): ?>
                                    <option value="<?php echo $grupo[$index]; ?>"><?php echo $grupo[$index]; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Prefijo</label>
                        <select class="input_field" id="prefijoRefaccion" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Básico</label>
                        <select class="input_field" id="basicoRefaccion" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Sufijo.</label>
                        <select class="input_field" id="sufijoRefaccion" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="">Descripción</label>
                        <textarea class="input_field-lg" id="descripcionRefacciones" style='width:100%;' disabled></textarea>
                    </div>
                    <div class="col-sm-4">
                        <label for="">F.U.E.</label>
                        <input type='text' class='input_field' id="fueRefacciones" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-3">
                        <label for="">Cla.</label>
                        <input type='text' class='input_field' id="claRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-3">
                        <label for="">P. Lista</label>
                        <input type='text' class='input_field' id="precioRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Cantidad</label>
                        <input type='number' min="0" max="" class='input_field' id="cantidadRefacciones" value="1" style='width:100%;'>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Ubicación</label>
                        <input type='text' class='input_field' id="ubicacionRefacciones" style='width:100%;' disabled>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="indiceOperacion" value="">
                <input type="hidden" name="" id="existeRefacciones" value="">
                <input type="hidden" name="" id="claveRefaccion" value="">
                <input type="hidden" name="" id="base_ajax" value="<?php echo base_url(); ?>">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarOperacion">Cerrar</button>
                <button type="button" class="btn btn-primary" name="" id="bajaOperacion" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
