
<div class="container">
    <form name="" id="formulario" method="post" action="<?=base_url()."multipunto/Cotizaciones/validateForm"?>" autocomplete="on" enctype="multipart/form-data">
        <h3 align="center" style="color:#340f7b;">
            COTIZACIÓN
        </h3>
        <br>
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row table-responsive">
            <div class="col-sm-12">
                No. Orden : <input type="text" class="input_field" value="<?php if(isset($idOrden)) echo $idOrden; ?>" style="width:70%;" readonly>
                    <input type="hidden" name="idOrdenTemp" id="" value="<?php if(isset($idOrden)) echo $idOrden; ?>" style="width:70%;">
                <br>
                <?php echo form_error('idOrdenTemp', '<span class="error">', '</span>'); ?>
            </div>
        </div>
        <br>

        <table style="width:100%;border:1px solid #337ab7;border-radius: 4px;">
            <tr>
                <td style="width:7%;border-bottom:1px solid #337ab7;" align="center"><strong>CANTIDAD</strong></td>
                <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>  D E S C R I P C I Ó N</strong></td>
                <td style="width:26%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>NUM. PIEZA</strong></td>
                <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO PZA</strong></td>
                <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>HORAS</strong></td>
                <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO MO</strong></td>
                <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO TOTAL</strong></td>       
                <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>REVISADO <br> (ASESOR)</strong></td>
            </tr>
            <?php if (isset($registrosControl)): ?>
                <?php if (count($registrosControl)>0): ?>
                    <?php foreach ($registrosControl as $index => $value): ?>
                        <tr>
                            <td style="width:10%;border-bottom:1px solid #337ab7;height:20px;" align="center">
                                <label for=""><?php echo $registrosControl[$index][0]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][1]; ?></label>
                            </td>
                            <td style="width:35%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align='center'>
                                <label for=""><?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?></label>
                            </td>
                            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][2]; ?></label>
                            </td>
                            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][3]; ?></label>
                            </td>
                            <td style="width:15%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][4]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                    <?php $conteo1 = (int)$registrosControl[$index][0] * (float)$registrosControl[$index][2]; ?>  
                                    <?php $conteo2 = (float)$registrosControl[$index][4] * (float)$registrosControl[$index][3]; ?>
                                  <label for="" style="color:blue;">$<?php echo number_format(($conteo1+$conteo2),2); ?></label>
                            </td>
                            <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                  <label for=""><?php echo $registrosControl[$index][5]; ?></label>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php else: ?>
                <tr>
                    <td colspan=7" align="center" style="border-bottom:1px solid #337ab7;height:20px;background-color: #ddd;">
                        No se registro ninguna cotización.
                    </td>
                    <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;background-color: #ddd;" align="center"></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td colspan="7" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    SUB-TOTAL
                </td>
                <td align="center" >
                    $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    I.V.A.
                </td>
                <td align="center">
                    $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="right" style="border-left:1px solid #337ab7;">
                    PRESUPUESTO TOTAL
                </td>
                <td colspan="1" align="center">
                    $<label id="presupuestoMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format(($subTotalMaterial+$ivaMaterial),2); else echo "0"; ?></label>
                </td>
            </tr>
            <tr style="border-top:1px solid #337ab7;">
                <td colspan="7" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    ANTICIPO
                </td>
                <td align="center">
                    $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                    <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                </td>
                <td colspan="3">
                    <label for="" id="anticipoNota" style="color:blue;"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    TOTAL
                </td>
                <td align="center">
                    $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                </td>
            </tr>
        </table>

        <br><br>
        <div class="row">
            <div class="col-sm-12" align="right">
                <?php if (isset($archivo)): ?>
                    <?php for ($i = 0; $i < count($archivo); $i++): ?>
                        <?php if ($archivo[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo[$i]; ?>" class="btn btn-info" target="_blank">Ver documento (<?php echo $i+1; ?>)</a>
                            <?php if ((($i+1)% 2) == 0): ?>
                                <br>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endfor; ?>
                <?php else: ?>
                    <a  class="btn btn-info" onclick="event.preventDefault()">Sin documentos</a>
                <?php endif; ?>
                <!-- <input type="hidden" name="tempFile" value="<?php if(isset($archivoTxt)) echo $archivoTxt;?>"> -->
                <!-- <br> -->
                <!-- <input type="file" multiple name="uploadfiles[]"><br> -->
            </div>
        </div>
        <br><br>

        Acepto (Si&nbsp;<input type="radio" value="Si" name="aceptoTermino" <?php echo set_checkbox('aceptoTermino', 'Si'); ?> <?php if(isset($aceptoTermino)) if($aceptoTermino == "Si") echo "checked disabled";?> <?php if (!isset($firmaCliente)) echo "disabled"; ?>>&nbsp;&nbsp;
        No&nbsp;<input type="radio" value="No" name="aceptoTermino" <?php echo set_checkbox('aceptoTermino', 'No'); ?> <?php if(isset($aceptoTermino)) if($aceptoTermino == "No") echo "checked disabled";?> <?php if (!isset($firmaCliente)) echo "disabled"; ?>>)&nbsp;&nbsp; la cotizacion aqui mostrada
        <br>
        <?php echo form_error('aceptoTermino', '<span class="error">', '</span>'); ?><br>
        <br><br>

        <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
            <tr>
                <td style="width:50%;" align="center"></td>
                <td style="width:50%;" align="center" colspan="2"></td>
            </tr>
            <tr>
                <td style="width:50%;" align="center">
                    FIRMA DEL ASESOR QUE APRUEBA
                    <br>
                    <?php if (isset($firmaAsesor)): ?>
                        <?php if ($firmaAsesor != ""): ?>
                            <img class='marcoImg' src='<?php echo base_url().$firmaAsesor ?>' id='' style='width:4cm;height:2cm;'>
                        <?php else: ?>
                            <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' style='width:4cm;height:2cm;'>
                        <?php endif; ?>
                    <?php else: ?>
                        <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' style='width:4cm;height:2cm;'>
                    <?php endif; ?>
                    <br><br><br><br><br>
                </td>
                <td style="width:50%;" align="center">
                    NOMBRE Y FIRMA DEL CLIENTE
                    <br>
                    <?php if (isset($firmaCliente)): ?>
                        <?php if ($firmaCliente == ""): ?>
                            <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?= set_value('rutaFirmaConsumidor');?>">
                            <img class="marcoImg" src="<?php if(set_value('rutaFirmaConsumidor') != "") echo set_value('rutaFirmaConsumidor'); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaFirmaConsumidor" alt="" style="height:2cm;width:4cm;">
                            <br>
                            <input type="text" class="input_field nombreCliente" name="nombreConsumidor" id="nombreConsumidor" style="width:100%;" value="<?= set_value('nombreConsumidor');?>" <?php if (!isset($firmaCliente)) echo "disabled"; ?>>
                        <?php else: ?>
                            <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?php if(isset($firmaCliente)) echo $firmaCliente;?>">
                            <img class="marcoImg" src="<?php if(isset($firmaCliente)) echo base_url().$firmaCliente;?>" id="firmaFirmaConsumidor" alt="">
                            <br>
                            <input type="text" class="input_field nombreCliente" name="" id="nombreConsumidor" style="width:100%;" value="<?php if(isset($nombreCliente)) echo $nombreCliente;?>" disabled>
                            <input type="hidden" class="input_field nombreCliente" name="nombreConsumidor" id="" style="width:100%;" value="<?php if(isset($nombreCliente)) echo $nombreCliente;?>" >
                        <?php endif; ?>
                    <?php else: ?>
                        <input type="hidden" id="rutaFirmaConsumidor" name="rutaFirmaConsumidor" value="<?= set_value('rutaFirmaConsumidor');?>">
                        <img class="marcoImg" src="<?php if(set_value('rutaFirmaConsumidor') != "") echo set_value('rutaFirmaConsumidor'); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaFirmaConsumidor" alt="" style="height:2cm;width:4cm;">
                        <br>
                        <input type="text" class="input_field nombreCliente" name="nombreConsumidor" id="nombreConsumidor" style="width:100%;" value="<?= set_value('nombreConsumidor');?>" <?php if (!isset($firmaCliente)) echo "disabled"; ?>>
                    <?php endif; ?>
                    <br>
                    <?php echo form_error('rutaFirmaConsumidor', '<span class="error">', '</span>'); ?><br>
                    <?php echo form_error('nombreConsumidor', '<span class="error">', '</span>'); ?>
                    <br><br>
                </td>
                <td>
                    <?php if (isset($firmaCliente)): ?>
                        <?php if ($firmaCliente == ""): ?>
                            <a id="Cliente_firma" class="cuadroFirma btn btn-primary" data-value="Cliente" style="color:white;" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                <i class='fas fa-pen-fancy'></i>
                            </a>
                        <?php else: ?>
                            <br>
                        <?php endif; ?>
                    <?php else: ?>
                        <a id="Cliente_firma" class="cuadroFirma btn btn-primary" data-value="Cliente" style="color:white;" data-target="#" style="color:white;" disabled>
                            <i class='fas fa-pen-fancy'></i>
                        </a>
                    <?php endif; ?>
                </td>
            </tr>
        </table>

        <br>
        <div class="col-sm-12" align="center">
            <input type="hidden" name="modoVistaFormulario" id="" value="3">
            <input type="hidden" name="" id="base_ajax" value="<?php echo base_url(); ?>">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="tipoRegistro" id="tipoRegistro" value="<?php if(isset($tipoRegistro)) echo $tipoRegistro; ?>">

            <?php if (isset($firmaAsesor)): ?>
                <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="" >Actualizar Cotización Multipunto</button>
            <?php else: ?>
                <button type="button" class="btn btn-success" name="" id="" disabled>Actualizar Cotización Multipunto</button>
            <?php endif; ?>
        </div>

    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <!-- <canvas id="canvas" class="canvas_box" width="430" height="200" style='border: 1px solid #CCC;'> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
