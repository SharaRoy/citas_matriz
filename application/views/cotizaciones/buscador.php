<div style="margin:20px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Presupuestos Multipunto</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <?php if (isset($dirige)): ?>
                        <?php if ($dirige == "ASE"): ?>
                            <div class="row">
                                <div class="col-md-5"></div>
                                <!-- formulario de busqueda -->
                                <div class="col-md-3">
                                    <h5>Busqueda Gral.:</h5>
                                    <div class="input-group">
                                        <!--<div class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                        </div>-->
                                        <input type="text" class="form-control" id="busqueda_campo_coti" placeholder="Buscar...">
                                    </div>
                                </div>
                                <!-- /.formulario de busqueda -->

                                <div class="col-md-3">
                                    <br><br>
                                    <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_fechas_coti" style="margin-right: 15px;"> 
                                        <i class="fa fa-search"></i>
                                    </button>

                                    <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                        <!--<i class="fa fa-trash"></i>-->
                                        Limpiar Busqueda
                                    </button>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-3"></div>
                                <!-- formulario de busqueda -->
                                <div class="col-md-5">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-search"></i>
                                        </div>
                                        <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                                    </div>
                                </div>
                                <!-- /.formulario de busqueda -->
                            </div>
                        <?php endif ?>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-3"></div>
                            <!-- formulario de busqueda -->
                            <div class="col-md-5">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-search"></i>
                                    </div>
                                    <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                                </div>
                            </div>
                            <!-- /.formulario de busqueda -->
                        </div>
                    <?php endif ?>
                    
                    <br>
                    <div class="form-group" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <td align="center" style="width: 10%;"></td>
                                    <td align="center" style="width: 10%;"><strong>NO. ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" style="width: 10%;"><strong>FECHA REGISTRO</strong></td>
                                    <td align="center" style="width: 10%;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" style="width: 10%;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>TÉCNICO</strong></td>
                                    <td align="center" style="width: 20%;"><strong>FIRMA ASESOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>APRUEBA CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;"><strong>EDO. REFACCIONES</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <!-- Verificamos que se hayan cargado los datos para mostrar-->
                                <?php if (isset($idOrden)): ?>
                                    <!-- Imprimimos los datos-->
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <!--Para el lista de los asesores, validamos si ya fue revisada por el jefe de taller-->
                                        <tr style="font-size:12px;" <?php if(($dirige == "ASE") && ($envioRef[$index] == "0")) echo 'hidden'; ?> <?php if(($dirige == "ASE") && ($garantias[$index] == "0")) echo 'hidden'; ?>>
                                            <!-- Verificamos si la vista es para los de refacciones -->
                                            <?php if ($indicador == "REF"): ?>
                                                <!-- Verificamos si ya se envio esta cotizacion al asesor y si ya firmo el asesor -->
                                                <?php if ($aprobacionRef[$index] == "1"): ?>
                                                    <td align="center" style="width:10%;background-color:#68ce68;">
                                                        <?php echo count($idOrden) - $index; ?>
                                                    </td>
                                                <?php elseif ($aprobacionRef[$index] == "2"): ?>
                                                    <td align="center" style="width:10%;background-color:#b776e6;color:white;">
                                                        <?php echo count($idOrden) - $index; ?>
                                                    </td>
                                                <?php else: ?>
                                                    <td align="center" style="width:10%;">
                                                        <?php echo count($idOrden) - $index; ?>
                                                    </td>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <td align="center" style="width:10%;">
                                                    <?php echo count($idOrden) - $index; ?>
                                                </td>
                                            <?php endif; ?>
                                            <td align="center" style="width:15%;">
                                                <?php echo $idOrden[$index]; ?>
                                                <?php if ($aprobacionRef[$index] == "2"): ?>
                                                    <p style="font-size:9px;color:blue;">Se envio al asesor</p>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $fechaCaptura[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $firmaAsesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php if ($aceptoTermino[$index] != ""): ?>
                                                    <?php if ($aceptoTermino[$index] == "Si"): ?>
                                                        <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                    <?php elseif ($aceptoTermino[$index] == "Val"): ?>
                                                        <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                    <?php else: ?>
                                                        <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="width:15%; background-color:<?php if ($aceptoTermino[$index] == "No"){ echo "red;color:white;"; } else{ if($statusRef[$index] == "0") echo '#f5acaa;'; elseif ($statusRef[$index] == "1") echo '#f5ff51'; elseif ($statusRef[$index] == "3") echo '#5bc0de'; else echo '#c7ecc7;';}?>">
                                                <?php if ($statusRef[$index] == "0"): ?>
                                                    <?php if ($aceptoTermino[$index] != "No"): ?>
                                                        SIN SOLICITAR                                                   
                                                    <?php endif ?>                                                    
                                                <?php elseif ($statusRef[$index] == "1"): ?>
                                                    SOLICIATADAS
                                                <?php elseif ($statusRef[$index] == "3"): ?>
                                                    RECIBIDAS
                                                <?php else: ?>
                                                    ENTREGADAS
                                                <?php endif ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php if (isset($destino)): ?>
                                                    <?php if ($destino == "OT"): ?>
                                                        <!-- Verificamos si ta esta completa la cotizacion -->
                                                        <?php if ($aceptoTermino[$index] != ""): ?>
                                                            <?php if ($statusRef[$index] != "2"): ?>
                                                                <?php if ($aceptoTermino[$index] != "No"): ?>
                                                                    <button type="button" class="btn btn-secondary" style="color:white;" onclick="location.href='<?=base_url()."Cotizacion_Refacciones/".$id_cita_url[$index];?>';">Soliciar/Entregar refacción</button>
                                                                <?php else: ?>
                                                                    <button type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Cotizacion_Refacciones/".$id_cita_url[$index];?>';">Ver</button>
                                                                <?php endif ?>
                                                            <?php else: ?>
                                                                <button type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Multipunto_Cotizacion/".$id_cita_url[$index];?>';">Ver</button>
                                                            <?php endif ?>

                                                        <!-- Si ya salio la cotizacion de refacciones y es la lista de refacciones -->
                                                        <?php elseif ($aprobacionRef[$index] != "0"): ?>
                                                            <button type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Cotizacion_Edita/".$id_cita_url[$index];?>';">Ver</button>
                                                        <?php else: ?>
                                                            <button type="button" class="btn btn-primary" style="color:white;" onclick="location.href='<?=base_url()."Cotizacion_Edita/".$id_cita_url[$index];?>';">Editar</button>
                                                        <?php endif; ?>
                                                    <?php else: ?>

                                                        <!-- Verificamos si el asesor ya firmo (se asume que ya se le notifico al cliente ) darle permiso al asesor de aceptar/rechazar la cotización -->
                                                        <!-- Si el asesor ya firmo y el cliente no ha confirmado/rechazado la cotización -->
                                                        <?php if (($firmaAsesor[$index] == "SI") && ($aceptoTermino[$index] == "")): ?>
                                                            <button type="button" class="btn btn-primary" style="color:white;" onclick="location.href='<?=base_url()."Cotizacion_Cliente/".$id_cita_url[$index];?>';">Aprobar/Rechazar</button>
                                                        <!-- Si el asesor no a firmado y/o el cliente ya confirmo/rechazo la cotización -->
                                                        <?php else: ?>
                                                            <button type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Multipunto_Cotizacion/".$id_cita_url[$index];?>';">Ver</button>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                      <!-- <input type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Multipunto_Cotizacion/".$idOrden[$index];?>';" value="Ver"> -->
                                                      <button type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Multipunto_Cotizacion/".$id_cita_url[$index];?>';">Ver</button>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <!-- Si no se cargaron los datos para mostrar -->
                                <?php else: ?>
                                    <tr>
                                        <td colspan="9" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <!-- <input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar"> -->

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="indicador" value="<?php if(isset($indicador)) echo $indicador; ?>">
                        <input type="hidden" name="" id="rolVista" value="lista">
                        <input type="hidden" name="" id="rolBusqueda" value="<?php if(isset($dirige)) echo $dirige; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
