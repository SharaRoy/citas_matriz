<!--Vista para editar de refacciones-->

<div class="" style="margin:20px;">
    <form name="formularioPresupuesto" id="formularioPresupuesto" method="post" action="<?=base_url()."multipunto/Cotizaciones/validateForm"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row">
            <div class="col-md-2">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); else{ if(isset($idOrden)) echo $idOrden;} ?>" style="width:100%;">
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-2">
                <label for="">Folio Intelisis:</label>&nbsp;<br>
                <label class="" style="color:darkblue;"><?php if(isset($idIntelisis)) echo $idIntelisis;  ?></label>
            </div>
            <div class="col-md-2">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php echo set_value('noSerie');?>" style="width:100%;">
                <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;">
                <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3" align="right">
                <?php if (isset($UnidadEntrega)): ?>
                    <?php if ($UnidadEntrega != 1): ?>
                        <a class='btn btn-dark' data-target="#modalAnticipo" data-toggle="modal" style="color:white;">
                            <i class="fas fa-donate" style=""></i>&nbsp;&nbsp;ANTICIPO
                        </a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp; 
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;">
                <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;">
                <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;">
                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;">
                <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        <br><br>

        <div class="row table-responsive">
            <div class="col-sm-10" align="right">
                <label for="" class="error"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                <br>
                <label for="" class="error"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
            </div>
            <div class="col-sm-1" align="right">
                <a id="agregarCM" class="btn btn-success" style="color:white;">
                    <i class="fas fa-plus"></i>
                </a>
            </div>
            <div class="col-sm-1" align="right">
                <a id="eliminarCM" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                    <i class="fas fa-minus"></i>
                </a>
            </div>
        </div>

        <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
            <thead>
                <tr>
                    <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        CANTIDAD
                    </td>
                    <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        D E S C R I P C I Ó N
                    </td>
                    <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        NUM. PIEZA
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center" colspan="3">
                        EXISTE
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        COSTO PZA
                    </td>
                    <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        HORAS
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        COSTO MO
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        REVISADO <br> (ASESOR)
                        <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($registrosControl)) if(count($registrosControl) > 0) echo count($registrosControl)+1; else echo "1"; else echo "1"; ?>">
                        <input type="hidden" name="refAutorizadas" id="refAutorizadas" value="<?php if (isset($pzsAprobadasJDT_2)) echo $pzsAprobadasJDT_2; else echo ''?>">
                    </td>
                    <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        PZA. C/GARANTÍA
                    </td>
                    <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">

                    </td>
                </tr>
                <tr>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        SI
                    </td>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        NO
                    </td>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        PLANTA
                    </td>
                </tr>
            </thead>
            <tbody id="cuerpoMateriales">
                <!-- Verificamos si existe la informacion -->
                <?php if (isset($registrosControl)): ?>
                    <!-- Comprobamos que haya registros guardados -->
                    <?php if (count($registrosControl)>0): ?>
                        <?php foreach ($registrosControl as $index => $valor): ?>
                            <!-- Comprobamos que el renglon a mostrar tenga la aprovacion del jefe de taller -->
                            <!-- <?php if (!in_array(($index+1),$pzsAprobadasJDT)&&($pzsAprobadasJDT_2 != "")) echo 'hidden'; ?> -->
                            <tr id='fila_<?php echo $index+1; ?>'  >
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='number' step='any' min='0' class='input_field' onblur='cantidadCollet(<?php echo $index+1; ?>)' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $registrosControl[$index][0]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;' disabled>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <textarea rows='3' class='input_field_lg' onblur='descripcionCollect(<?php echo $index+1; ?>)' id='descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $registrosControl[$index][1]; ?></textarea>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <textarea rows='1' class='input_field_lg' onblur='piezaCollet(<?php echo $index+1; ?>)' id='pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?></textarea>
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='radio' id='apuntaSi_<?php echo $index+1; ?>' class='input_field' onclick='existeCollet(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="SI" <?php if(isset($registrosControl[$index][8])) if($registrosControl[$index][8] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='radio' id='apuntaNo_<?php echo $index+1; ?>' class='input_field' onclick='existeCollet(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="NO" <?php if(isset($registrosControl[$index][8])) if($registrosControl[$index][8] == "NO") echo "checked"; ?> style="transform: scale(1.5);">
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='radio' id='apuntaPlanta_<?php echo $index+1; ?>' class='input_field' onclick='existeCollet(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="PLANTA" <?php if(isset($registrosControl[$index][8])) if($registrosControl[$index][8] == "PLANTA") echo "checked"; ?> style="transform: scale(1.5);">
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    $<input type='number' step='any' min='0' class='input_field' onblur='costoCollet(<?php echo $index+1; ?>)' id='costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][2]; ?>' style='width:90%;text-align:left;' disabled>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='number' step='any' min='0' class='input_field' onblur='horasCollet(<?php echo $index+1; ?>)' id='horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][3]; ?>' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    $<input type='number' step='any' min='0' id='totalReng_<?php echo $index+1; ?>' onblur="autosuma(<?php echo $index+1; ?>)"  class='input_field blockCampo' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;' disabled>
                                    <input type='hidden' id='totalOperacion_<?php echo $index+1; ?>' value='<?php if($registrosControl[$index][4] == "0") {echo "0";} else {if(isset($registrosControl[$index][7])) echo $registrosControl[$index][7]; else echo "0";} ?>'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(<?php echo $index+1; ?>)' value="<?php echo $index+1; ?>" id='autorizo_<?php echo $index+1; ?>' <?php if($registrosControl[$index][5] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                    <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $registrosControlRenglon[$index]; ?>">
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <?php if (in_array(($index+1),$pzaGarantia)): ?>
                                        SI
                                    <?php else: ?>
                                        NO
                                    <?php endif ?>
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <a id='refaccionBusqueda_<?php echo $index+1; ?>' class='refaccionesBox btn btn-warning' data-id="<?php echo $index+1; ?>" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                        <i class="fa fa-search"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        <!-- Creamos una linea vacia para que puedna ingresar -->
                        <!--<tr id='fila_<?php echo count($registrosControl)+1 ?>'>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='number' step='any' min='0' class='input_field' onblur='cantidadCollet(<?php echo count($registrosControl)+1; ?>)' id='cantidad_<?php echo count($registrosControl)+1 ?>' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <textarea rows='1' class='input_field_lg' id='descripcion_<?php echo count($registrosControl)+1; ?>' onblur='descripcionCollect(<?php echo count($registrosControl)+1; ?>)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <textarea rows='1' class='input_field_lg' id='pieza_<?php echo count($registrosControl)+1; ?>' onblur='piezaCollet(<?php echo count($registrosControl)+1; ?>)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaSi_<?php echo count($registrosControl)+1; ?>' class='input_field' onclick='existeCollet(<?php echo count($registrosControl)+1; ?>)' name='existe_<?php echo count($registrosControl)+1; ?>' value="SI" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaNo_<?php echo count($registrosControl)+1; ?>' class='input_field' onclick='existeCollet(<?php echo count($registrosControl)+1; ?>)' name='existe_<?php echo count($registrosControl)+1; ?>' value="NO" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaPlanta_<?php echo count($registrosControl)+1; ?>' class='input_field' onclick='existeCollet(<?php echo count($registrosControl)+1; ?>)' name='existe_<?php echo count($registrosControl)+1; ?>' value="PLANTA" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='number' step='any' min='0' class='input_field' onblur='costoCollet(<?php echo count($registrosControl)+1; ?>)' id='costoCM_<?php echo count($registrosControl)+1 ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='number' step='any' min='0' class='input_field' onblur='horasCollet(<?php echo count($registrosControl)+1; ?>)' id='horas_<?php echo count($registrosControl)+1 ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='number' step='any' min='0' id='totalReng_<?php echo count($registrosControl)+1 ?>' onblur="autosuma(<?php echo count($registrosControl)+1 ?>)" class='input_field blockCampo' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;' disabled>
                                <input type='hidden' id='totalOperacion_<?php echo count($registrosControl)+1 ?>' value='0'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='checkbox' onclick='autorizaColet(<?php echo count($registrosControl)+1; ?>)' value="<?php echo count($registrosControl)+1 ?>" class='input_field autorizaCheck' id='autorizo_<?php echo count($registrosControl)+1 ?>' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?> style="transform: scale(1.5);">
                                <input type='hidden' id='valoresCM_<?php echo count($registrosControl)+1 ?>' name='registros[]'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                NO
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <!- - <a id='refaccionBusqueda_<?php echo count($registrosControl)+1; ?>' class='refaccionesBox btn btn-warning' data-id="<?php echo count($registrosControl)+1 ?>" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                    <i class="fa fa-search"></i>
                                </a> - ->
                            </td>
                        </tr>-->
                    <?php else: ?>
                        <tr id='fila_1'>
                            <td align='center'>
                                <input type='number' step='any' min='0' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <textarea rows='1' class='input_field_lg' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <textarea rows='1' class='input_field_lg' id='pieza_1' onblur='piezaCollet(1)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaSi_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="SI" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaNo_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="NO" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaPlanta_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="PLANTA" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='number' step='any' min='0' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='number' step='any' min='0' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='number' step='any' min='0' id='totalReng_1' class='input_field'  onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;' disabled>
                                <input type='hidden' id='totalOperacion_1' value='0'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?> style="transform: scale(1.5);">
                                <input type='hidden' id='valoresCM_1' name='registros[]'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                NO
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <!-- <a id='refaccionBusqueda_1' class='refaccionesBox btn btn-warning' data-id="1" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                    <i class="fa fa-search"></i>
                                </a> -->
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php else: ?>
                    <tr id='fila_1'>
                        <td align='center'>
                            <input type='number' step='any' min='0' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <textarea rows='1' class='input_field_lg' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'></textarea>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <textarea rows='1' class='input_field_lg' id='pieza_1' onblur='piezaCollet(1)' style='width:90%;text-align:left;'></textarea>
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <input type='radio' id='apuntaSi_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="SI" style="transform: scale(1.5);">
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <input type='radio' id='apuntaNo_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="NO" style="transform: scale(1.5);">
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <input type='radio' id='apuntaPlanta_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="PLANTA" style="transform: scale(1.5);">
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            $<input type='number' step='any' min='0' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <input type='number' step='any' min='0' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            $<input type='number' step='any' min='0' id='totalReng_1' class='input_field' onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;' disabled>
                            <input type='hidden' id='totalOperacion_1' value='0'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?> style="transform: scale(1.5);">
                            <input type='hidden' id='valoresCM_1' name='registros[]'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            NO
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                           <!--  <a id='refaccionBusqueda_1' class='refaccionesBox btn btn-warning' data-id="1" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                <i class="fa fa-search"></i>
                            </a> -->
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7" align="right">
                        SUB-TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subTotalMaterial)) echo $subTotalMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        I.V.A.
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($ivaMaterial)) echo $ivaMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        PRESUPUESTO TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label id="presupuestoMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format(($subTotalMaterial+$ivaMaterial),2); else echo "0"; ?></label>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr style="border-top:1px solid #337ab7;">
                    <td colspan="7" align="right">
                        ANTICIPO
                    </td>
                    <td align="center">
                        $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                        <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                    </td>
                    <td colspan="2">
                        <label for="" id="anticipoNota" style="color:blue;"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($totalMaterial)) echo $totalMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        FIRMA DEL ASESOR QUE APRUEBA
                    </td>
                    <td align="center" colspan="3">
                      <?php if (isset($firmaAsesor)): ?>
                          <?php if ($firmaAsesor != ""): ?>
                              <img class='marcoIm
                              <td colspan="2"></td>g' src='<?php echo base_url().$firmaAsesor ?>' id='' style='width:80px;height:30px;'>
                              <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php echo $firmaAsesor ?>'>
                          <?php else: ?>
                              <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value=''>
                              <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                          <?php endif; ?>
                      <?php else: ?>
                          <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value=''>
                          <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                      <?php endif; ?>
                    </td>
                </tr>
            </tfoot>
        </table>

        <!-- Creamos segunda tabla para las piezas con garantias ( solo para refacciones) -->
        <?php if (isset($tipoRegistro)): ?>
            <?php if ($tipoRegistro == "Otros"): ?>
                <br><br>
                <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
                    <thead>
                        <tr>
                            <td colspan="7" style="border-bottom:1px solid #337ab7;">
                                <h3 style="text-align: center;">Piezas con garantías</h3>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                                CANTIDAD
                            </td>
                            <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                                COSTO PZA FORD
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                                COSTO MO GARANTÍAS
                            </td>
                            <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                                TOTAL REFACCIÓN
                                <input type="hidden" name="" id="renglonesGarantias" value="<?php if(isset($tablaPzsGarantias)) echo count($tablaPzsGarantias); else echo '0'; ?> ">
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($tablaPzsGarantias)): ?>
                            <!-- Comprobamos que haya registros guardados ya editados -->
                            <?php if (count($tablaPzsGarantias)>0): ?>
                                <!-- creamos variable para totales con garantias-->
                                <?php $totalGarantia = 0; ?>
                                <?php foreach ($tablaPzsGarantias as $index => $valor): ?>
                                    <!-- Comprobamos que el renglon a mostrar tenga la aprovacion del jefe de taller -->
                                    <tr id='fila_<?php echo $index+1; ?>'  <?php if (!in_array($tablaPzsGarantias[$index][0],$pzsAprobadasJDT)&&($pzsAprobadasJDT_2 != "")) echo 'hidden'; ?>>
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur='cantidadCollet_tabla2(<?php echo $index+1; ?>)' id='tb2_cantidad_<?php echo $index+1; ?>' value='<?php echo $tablaPzsGarantias[$index][1]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;' disabled>

                                            <!-- Creamos un renglon pivote -->
                                            <input type="hidden" name="" id="renglonGarantia_<?php echo $tablaPzsGarantias[$index][0]; ?>" value="<?php echo $index+1; ?>">
                                        </td>
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='3' class='input_field_lg' onblur='descripcionCollect_tabla2(<?php echo $index+1; ?>)' id='tb2_descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $tablaPzsGarantias[$index][2]; ?></textarea>
                                        </td>
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' onblur='piezaCollet_tabla2(<?php echo $index+1; ?>)' id='tb2_pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php if(isset($tablaPzsGarantias[$index][3])) echo $tablaPzsGarantias[$index][3]; else echo ""; ?></textarea>
                                        </td>
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            $<input type='number' step='any' min='0' class='input_field' onblur='costoCollet_tabla2(<?php echo $index+1; ?>)' id='tb2_costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $tablaPzsGarantias[$index][4]; ?>' style='width:90%;text-align:left;' disabled>
                                        </td>
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur='horasCollet_tabla2(<?php echo $index+1; ?>)' id='tb2_horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $tablaPzsGarantias[$index][5]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            $<input type='number' step='any' min='0' id='tb2_totalReng_<?php echo $index+1; ?>' onblur="autosuma_tabla2(<?php echo $index+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $tablaPzsGarantias[$index][4]; ?>' style='width:90%;text-align:left;'>
                                            <input type='hidden' id='totalOperacion_<?php echo $index+1; ?>' value='<?php if($tablaPzsGarantias[$index][4] == "0") {echo "0";} else {if(isset($tablaPzsGarantias[$index][7])) echo $tablaPzsGarantias[$index][7]; else echo "0";} ?>'>
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <?php $conteo1 = (int)$tablaPzsGarantias[$index][1] * (float)$tablaPzsGarantias[$index][3]; ?>  
                                            <?php $conteo2 = (float)$tablaPzsGarantias[$index][4] * (float)$tablaPzsGarantias[$index][5]; ?>
                                            <label for="" id="totalRenglonGarantia_<?php echo $index+1; ?>" style="color:blue;">$<?php echo number_format(($conteo1+$conteo2),2); ?></label>
                                            <?php $totalGarantia += ($conteo1+$conteo2); ?>
                                            <input type='hidden' id='tablaPzsGarantias_<?php echo $index+1; ?>' name='tablaPzsGarantias[]' value="<?php echo $tablaPzsGarantias_rg[$index]; ?>">
                                            <input type="hidden" name="" id="totalPzsGarantias" value="<?php echo $totalGarantia; ?>">
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="7" style="border-bottom:1px solid #337ab7;">
                                        <h5 style="text-align: center">Sin refacciones para mostrar</h5>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7" style="border-bottom:1px solid #337ab7;">
                                    <h5 style="text-align: center">Sin refacciones para mostrar</h5>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    <?php if (isset($totalGarantia)): ?>
                        <tfoot>
                            <tr>
                                <td colspan="6" align="right">
                                    SUB-TOTAL
                                </td>
                                <td colspan="1" align="center">
                                    $<label for="" id="subTotalMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo $totalGarantia; ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">
                                    I.V.A.
                                </td>
                                <td colspan="1" align="center">
                                    $<label for="" id="ivaMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo number_format(($totalGarantia*0.16)); ?></label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">
                                    TOTAL
                                </td>
                                <td colspan="1" align="center">
                                    $<label for="" id="totalMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo number_format(($totalGarantia*1.16)); ?></label>
                                </td>
                            </tr>
                        </tfoot>
                    <?php endif ?>
                </table>
            <?php endif ?>    
        <?php endif ?>  


        <br><br>
        <div class="row">
            <div class="col-sm-5">
                <h4>Notas del asesor:</h4>
                <!-- Verificamos si es una vista del asesor -->
                <?php if (isset($dirige)): ?>
                    <?php if ($dirige == "ASE"): ?>
                        <textarea id="notaAsesor" name="notaAsesor" rows="2" style="width:100%;border-radius:4px;" placeholder="Notas del asesor..."><?php if(isset($notaAsesor)) echo $notaAsesor;?></textarea>
                    <?php else: ?>
                        <label for="" style="font-size:12px;"><?php if(isset($notaAsesor)){ if($notaAsesor != "") echo $notaAsesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="font-size:12px;"><?php if(isset($notaAsesor)){ if($notaAsesor != "") echo $notaAsesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                <?php endif; ?>
            </div>
            <div class="col-sm-7" align="right">
                <h5>Archivo(s) de la cotización:</h5><br>
                <?php if (isset($archivo)): ?>
                    <?php $indice = 1; ?>
                    <?php for ($i = 0; $i < count($archivo); $i++): ?>
                      <?php if ($archivo[$i] != ""): ?>
                          <a href="<?php echo base_url().$archivo[$i]; ?>" class="btn btn-info" target="_blank">Doc. (<?php echo $indice; ?>)</a>
                          <?php $indice++; ?>
                          <?php if ((($i+1)% 3) == 0): ?>
                              <br>
                          <?php endif; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                <?php endif; ?>
                <input type="hidden" name="tempFile" value="<?php if(isset($archivoTxt)) echo $archivoTxt;?>">
                <br>
                <input type="file" multiple name="uploadfiles[]"><br>
            </div>
        </div>

        <br><br>
        <div class="row" <?php if(isset($dirige)) if($dirige == "ASE") echo "hidden"; else echo ""; else echo "hidden"; ?>>
            <div class="col-md-6" style="">
                <h4>Comentario del técnico para Refacciones:</h4>
                <?php if (isset($dirige)): ?>
                    <?php if ($dirige == "TEC"): ?>
                        <textarea id="comentarioTecnico" name="comentarioTecnico" rows="2" style="width:100%;border-radius:4px;" placeholder="Clave de la refacción..."><?php if(isset($comentarioTecnico)) echo $comentarioTecnico;?></textarea>
                    <?php else: ?>
                        <label for="" style="font-size:12px;"><?php if(isset($comentarioTecnico)){ if($comentarioTecnico != "") echo $comentarioTecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="font-size:12px;"><?php if(isset($comentarioTecnico)){ if($comentarioTecnico != "") echo $comentarioTecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                <?php endif; ?>

                <br>
                <h4>Archivo(s) para Refacciones:</h4>
                <?php if (isset($archivoTecnico)): ?>
                    <?php for ($i = 0; $i < count($archivoTecnico); $i++): ?>
                      <?php if ($archivoTecnico[$i] != ""): ?>
                          <a href="<?php echo base_url().$archivoTecnico[$i]; ?>" class="btn btn-primary" target="_blank">Adj. (<?php echo $i+1; ?>)</a>
                          <?php if ((($i+1)% 3) == 0): ?>
                              <br>
                          <?php endif; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                    <?php if (count($archivoTecnico) == 0): ?>
                        <a href="" class="btn btn-primary" target="_blank">Sin archivos</a>
                    <?php endif; ?>
                <?php endif; ?>

                <br>
                <input id="uploadfilesTec" type="file" accept="image/*" multiple name="uploadfilesTec[]" hidden><br>
                <input type="hidden" name="tempFileTecnico" value="<?php if(isset($archivoImgTecnico)) echo $archivoImgTecnico;?>">
            </div>
            <div class="col-md-6" align="right" style="border-left: 1px solid black; ">
                <h4>Comentarios para el Jefe de Taller:</h4>
                <textarea rows='2' name="comentariosExtra" id='comentario' style='width:100%;text-align:left;font-size:12px;'><?php if(set_value('comentariosExtra') != "") { echo set_value('comentariosExtra');} else { if(isset($comentario)) echo $comentario;}?></textarea>
            </div>
        </div>

        <br><br>
        <div class="col-sm-3" align="right"></div>
        <div class="col-sm-3" align="center">
            <button type="submit" class="btn btn-success" name="" id="terminarCotizacion" >Actualizar Cotizacion Multipunto</button>
        </div>
        <div class="col-sm-3" align="center">
            <!-- <button type="submit" class="btn btn-primary" name="aceptarCotizacion" id="aceptarCotizacion" >Actualizar</button> -->
            <button type="submit" class="btn btn-success" name="" id="terminarCotizacion2" >Terminar Revisión</button>
            <button type="button" id="regresarBtn" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Ventanilla/JDT";?>';" style="display:none;">
                Regresar
            </button>
        </div>

        <div class="col-sm-3" align="right">
            <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>">
            <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="4">
            <input type="hidden" name="modoGuardado" id="modoGuardado" value="Actualizar">
            <input type="hidden" name="" id="formularioHoja" value="CotizacionMultipunto">
            <input type="hidden" name="" id="envioRefacciones" value="<?php if(isset($refacciones)) echo $refacciones; else echo "0"; ?>">
            <input type="hidden" name="envioJDT" id="envioJDT" value="<?php if(isset($envioJDT)) echo $envioJDT; else echo "0"; ?>">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
            <input type="hidden" name="tipoRegistro" id="tipoRegistro" value="<?php if(isset($tipoRegistro)) echo $tipoRegistro; ?>">
        </div>
    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para filtrar las refacciones-->
<div class="modal fade" id="refacciones" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel">Buscador de Refacciones/Mano de Obra</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <br>
                        <input type='radio' id='' class='input_field' name='filtros' value="clave" checked>
                        Buscar por clave
                        <br><br>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding:0px;border: 0px;">
                                <!-- <i class="fa fa-search"></i> -->
                                <button type="button" class="btn btn-default" id="busquedaClaveBtn"><i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control" id="claveBuscar" placeholder="Buscar..." style="width:100%;height:100%;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <br>
                        <input type='radio' class='input_field' name='filtros' value="descripcion">
                        Buscar por descripción
                        <br><br>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding:0px;border: 0px;">
                                <!-- <i class="fa fa-search"></i> -->
                                <button type="button" class="btn btn-default" id="busquedaDescripBtn" ><i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control" id="descipBuscar" placeholder="Buscar..." style='width:100%;'>
                        </div>
                    </div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <br>
                        <label id="alertaConexion"></label>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <label for="">Resultados:</label>
                        <br>
                        <select class="form-control" id="resultadosVaciado" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-2"></div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Clave:</label>
                        <input type='text' class='form-control' id="claveRefaccion" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-6" style="padding-top: 20px;">
                        <input type='text' class='form-control' id="tipoBuscar" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="">Descripcion:</label>
                        <input type='text' class='form-control' id="descripcionRefacciones" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-3">
                        <label for="">Precio:</label>
                        <input type='text' class='form-control' id="precioRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Existencia:</label>
                        <input type='text' class='form-control' id="existenciaRefacciones" style='width:100%;' disabled>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="indiceOperacion" value="">
                <input type="hidden" name="" id="existeRefacciones" value="">
                <input type="hidden" name="" id="claveRefaccion" value="">
                <!-- <input type="hidden" name="" id="base_ajax" value="<?php echo base_url(); ?>"> -->
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarOperacion">Cerrar</button>
                <button type="button" class="btn btn-primary" name="" id="bajaOperacion" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ingresar anticipo-->
<div class="modal fade" id="modalAnticipo" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="center">Anticipo de refacciones</h3>
            </div>
            <div class="modal-body">
                <div class="row" id="credencialesAnticipo">
                    <div class="col-sm-12">
                        <label for="" style="font-size: 16px;font-weight: initial;">Usuario:</label>
                        <br>
                        <input type="text" class="form-control" type="text" id="usuarioAnticipo" value="" style="width:100%;">
                        <br>
                        <label for="" style="font-size: 16px;font-weight: initial;">Password:</label>
                        <br>
                        <input type="password" class="form-control" type="text" id="passAnticipo" value="" style="width:100%;">
                        <br><br>
                        <button type="button" class="btn btn-primary" id="btnCredencialAnticipo">Validar</button>
                    </div>
                </div>
                <div class="row" id="addAnticipo" style="display:none;">
                    <div class="col-sm-12">
                      <label for="" style="font-size: 16px;font-weight: initial;">Cantidad:</label>
                      <br>
                      <div class="input-group">
                          <div class="input-group-addon">
                              <i class="fa fa-hand-holding-usd"></i>
                          </div>
                          <input type="text" class="form-control" id="cantidadAnticipo" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value="0" style="width:100%;">
                      </div>
                      <br>
                      <label for="" style="font-size: 16px;font-weight: initial;">Comentario:</label>
                      <br>
                      <textarea rows='2' id='comentarioAnticipo' style='width:100%;text-align:left;font-size:16px;'></textarea>
                      <br><br>
                      <button type="button" class="btn btn-success btn-block" id="btnAddAnticipo">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <h5 class="error" style="font-size: 16px;font-weight: bold;" align="center" id="AnticipoError"></h5>
                <br>
                <input type="hidden" id="idCotizacion" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>
