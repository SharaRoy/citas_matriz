<div style="margin:20px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <h4 align="center">Presupuesto de Piezas con Garantías</h4>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-3"></div>
                        <!-- formulario de busqueda -->
                        <div class="col-md-5">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" name="q" class="form-control" id="busqueda_tabla" placeholder="Buscar...">
                            </div>
                        </div>
                        <!-- /.formulario de busqueda -->
                    </div>

                    <br>
                    <div class="form-group" align="center">
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <td align="center" style="width: 8%;"></td>
                                    <td align="center" style="width: 10%;"><strong>NO. ORDEN</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ORDEN<br>INTELISIS</strong></td>
                                    <td align="center" style="width: 10%;"><strong>APERTURA <br> ORDEN</strong></td>
                                    <!--<td align="center" style="width: 10%;"><strong>ULTIMA <br>ACTUALIZACIÓN</strong></td>-->
                                    <td align="center" style="width: 10%;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" style="width: 10%;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;"><strong>TÉCNICO</strong></td>       
                                    <!--<td align="center" style="width: 10%;"><strong>SUBTOTAL</strong></td>
                                    <td align="center" style="width: 10%;"><strong>IVA</strong></td>
                                    <td align="center" style="width: 10%;"><strong>TOTAL</strong></td>-->
                                    <td align="center" style="width: 20%;"><strong>PIEZAS CON<br>GARANTÍAS</strong></td>
                                    <td align="center" style="width: 10%;"><strong>APRUEBA<br> CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar">
                                <?php if (isset($idOrden)): ?>
                                    <?php foreach ($idOrden as $index => $valor): ?>
                                        <tr style="font-size:12px;">
                                            <!-- Verificamos si la vista es para los de refacciones -->
                                            <td align="center" style="width:8%; <?php if($envioGarantia[$index] == "1") echo 'background-color:#68ce68;'; ?>">
                                                    <?php echo count($idOrden) - $index; ?>
                                            </td>   
                                            <td align="center" style="width:10%;">
                                                <?php echo $idOrden[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $fechaCaptura[$index]; ?>
                                            </td>
                                            <!--<td align="center" style="width:15%;">
                                                <?php echo $fechaActualiza[$index]; ?>
                                            </td>-->
                                            <td align="center" style="width:15%;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <!--<td align="center" style="width:15%;">
                                                $ <?php echo number_format($subTotalMaterial[$index],2); ?>
                                            </td>
                                            <td align="center" style="width:10%;">
                                                $ <?php echo number_format($ivaMaterial[$index],2); ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                $ <?php echo number_format($totalMaterial[$index],2); ?>
                                            </td>-->
                                            <td align="center" style="width:15%;">
                                                <?php echo count($pzaGarantia[$index])-1; ?>
                                            </td>
                                            <td align="center" style="width:15%;"> 
                                                <?php if ($aceptoTermino[$index] != ""): ?>
                                                    <?php if ($aceptoTermino[$index] == "Si"): ?>
                                                        <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                    <?php elseif ($aceptoTermino[$index] == "Val"): ?>
                                                        <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                    <?php else: ?>
                                                        <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="width:15%;">
                                                <button type="button" class="btn <?php if ($envioGarantia[$index] == '0') echo 'btn-info'; else echo 'btn-primary' ?> "; style="color:white;" onclick="location.href='<?=base_url()."Cotizacion_Edita_Garantias/".$id_cita_url[$index];?>';">
                                                    <!--Verificamos si aun no a sido atendido por garantias -->
                                                    <?php if ($envioGarantia[$index] == "0"): ?>
                                                        REVISAR

                                                    <!-- Si ya fue revisado por garantias ..-->
                                                    <?php else: ?>
                                                        VER
                                                    <?php endif ?>
                                            </button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="9" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="rolVista" value="lista">
                        <input type="hidden" name="" id="indicador" value="<?php if(isset($indicador)) echo $indicador; ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
