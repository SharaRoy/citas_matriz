<!-- Vista para editar garantias-->

<div style="margin:20px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."multipunto/Cotizaciones/validateForm"?>" autocomplete="on" enctype="multipart/form-data">

        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(isset($idOrden)) echo $idOrden; ?>" style="width:100%;">
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Folio Intelisis:</label>&nbsp;<br>
                <label class="" style="color:darkblue;"><?php if(isset($idIntelisis)) echo $idIntelisis;  ?></label>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php echo set_value('noSerie');?>" style="width:100%;">
                <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;">
                <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;">
                <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;">
                <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;">
                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;">
                <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>

        <!--<div class="row table-responsive">
            <div class="col-sm-10" align="right">
                <label for="" class="error"> <strong>*</strong>Para grabar un registro, presione el boton de [<i class="fas fa-plus"></i>] </label>
                <br>
                <label for="" class="error"> <strong>*</strong>Para eliminar un registro, presione el boton de [<i class="fas fa-minus"></i>] </label>
            </div>
            <div class="col-sm-1" align="right">
                <a id="agregarCM" class="btn btn-success" style="color:white;">
                    <i class="fas fa-plus"></i>
                </a>
            </div>
            <div class="col-sm-1" align="right">
                <a id="eliminarCM" class="btn btn-danger" style="color:white; width: 1cm;" >
                    <i class="fas fa-minus"></i>
                </a>
            </div>
        </div>-->
        
        <br>
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="alert alert-warning" align="left">
                    <strong style="font-size:13px !important; font-weight: 700;">
                        NOTA: Las piezas con garantías que no sean marcadas con el "check" de "<u>AUTORIZAR GARANTÍA</u>"se considadaran como piezas no autorizadas. <br>
                        Para guardar el presupuesto y enviarlo al asesor, primero se debera firmar el presupuesto en la parte inferior.
                    </strong>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
        
        <br>
        <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
            <thead>
                <tr>
                    <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        CANTIDAD
                    </td>
                    <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        D E S C R I P C I Ó N
                    </td>
                    <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        NUM. PIEZA
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center" colspan="3">
                        EXISTE
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        COSTO PZA
                    </td>
                    <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        HORAS
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        COSTO MO
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        PZA. C/GARANTÍA
                        <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($registrosControl)) if(count($registrosControl) > 0) echo count($registrosControl)+1; else echo "1"; else echo "1"; ?>">
                        <input type="hidden" name="refAutorizadas" id="refAutorizadas" value="<?php if (isset($pzsAprobadasJDT_2)) echo $pzsAprobadasJDT_2; else echo ''?>">
                    </td> 
                    <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                        AUTORIZAR GARANTÍA
                    </td>
                </tr>
                <tr>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        SI
                    </td>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        NO
                    </td>
                    <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                        PLANTA
                    </td>
                </tr>
            </thead>
            <tbody id="cuerpoMateriales">
                <!-- Verificamos si existe la informacion -->
                <?php if (isset($registrosControl)): ?>
                    <!-- Comprobamos que haya registros guardados -->
                    <?php if (count($registrosControl)>0): ?>
                        <?php foreach ($registrosControl as $index => $valor): ?>
                            <!-- Comprobamos que el renglon a mostrar tenga la aprovacion del jefe de taller -->
                            <!--<tr id='fila_<?php echo $index+1; ?>'  <?php if (!in_array(($index+1),$pzsAprobadasJDT)&&($pzsAprobadasJDT_2 != "")) echo 'hidden'; ?>>-->
                            <tr id='fila_<?php echo $index+1; ?>'>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='number' step='any' min='0' class='input_field' onblur='cantidadCollet(<?php echo $index+1; ?>)' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $registrosControl[$index][0]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <textarea rows='3' class='input_field_lg' onblur='descripcionCollect(<?php echo $index+1; ?>)' id='descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $registrosControl[$index][1]; ?></textarea>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <textarea rows='1' class='input_field_lg' onblur='piezaCollet(<?php echo $index+1; ?>)' id='pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?></textarea>
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='radio' id='apuntaSi_<?php echo $index+1; ?>' class='input_field' onclick='existeCollet(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="SI" <?php if(isset($registrosControl[$index][8])) if($registrosControl[$index][8] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='radio' id='apuntaNo_<?php echo $index+1; ?>' class='input_field' onclick='existeCollet(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="NO" <?php if(isset($registrosControl[$index][8])) if($registrosControl[$index][8] == "NO") echo "checked"; ?> style="transform: scale(1.5);">
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='radio' id='apuntaPlanta_<?php echo $index+1; ?>' class='input_field' onclick='existeCollet(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="PLANTA" <?php if(isset($registrosControl[$index][8])) if($registrosControl[$index][8] == "PLANTA") echo "checked"; ?> style="transform: scale(1.5);">
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    $<input type='number' step='any' min='0' class='input_field' onblur='costoCollet(<?php echo $index+1; ?>)' id='costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][2]; ?>' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='number' step='any' min='0' class='input_field' onblur='horasCollet(<?php echo $index+1; ?>)' id='horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][3]; ?>' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    $<input type='number' step='any' min='0' id='totalReng_<?php echo $index+1; ?>' onblur="autosuma(<?php echo $index+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;'>
                                    <input type='hidden' id='totalOperacion_<?php echo $index+1; ?>' value='<?php if($registrosControl[$index][4] == "0") {echo "0";} else {if(isset($registrosControl[$index][7])) echo $registrosControl[$index][7]; else echo "0";} ?>'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(<?php echo $index+1; ?>)' value="<?php echo $index+1; ?>" id='autorizo_<?php echo $index+1; ?>' <?php if($registrosControl[$index][5] == "SI") echo "checked"; ?> style="display: none;">
                                    <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $registrosControlRenglon[$index]; ?>">

                                    <?php if (in_array(($index+1),$pzaGarantia)): ?>
                                        SI
                                    <?php else: ?>
                                        NO
                                    <?php endif ?>
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <input type='checkbox' class='input_field' value="<?php echo $index+1; ?>" name='autorizaPzaGarantia[]' style='transform: scale(1.5);' <?php if (!in_array(($index+1),$pzaGarantia)) echo 'disabled'; ?> <?php if (in_array(($index+1),$autorizaPzaGarantia)) echo 'checked'; ?>>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr id='fila_1'>
                            <td align='center'>
                                <input type='number' step='any' min='1' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <textarea rows='1' class='input_field_lg' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <textarea rows='1' class='input_field_lg' id='pieza_1' onblur='piezaCollet(1)' style='width:90%;text-align:left;'></textarea>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaSi_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="SI" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaNo_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="NO" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <input type='radio' id='apuntaPlanta_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="PLANTA" style="transform: scale(1.5);">
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                $<input type='number' step='any' min='1' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;' align='center'>
                                <input type='number' step='any' min='1' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                $<input type='number' step='any' min='1' id='totalReng_1' class='input_field ' onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;'> 
                                <input type='hidden' id='totalOperacion_1' value='0'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <!--<input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?> style="transform: scale(1.5);"> -->
                                <input type='checkbox' class='input_field' value="1" name='autorizaPzaGarantia[]' style='transform: scale(1.5);'>
                                <input type='hidden' id='valoresCM_1' name='registros[]'>
                                <br>
                                <?php echo form_error('registros', '<span class="error">', '</span>'); ?>
                            </td>
                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                <!-- <a id='refaccionBusqueda_1' class='refaccionesBox btn btn-warning' data-value="1" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                    <i class="fa fa-search"></i>
                                </a> -->
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php else: ?>
                    <tr id='fila_1'>
                        <td align='center'>
                            <input type='number' step='any' min='1' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <textarea rows='1' class='input_field_lg' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'></textarea>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <textarea rows='1' class='input_field_lg' id='pieza_1' onblur='piezaCollet(1)' style='width:90%;text-align:left;'></textarea>
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <input type='radio' id='apuntaSi_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="SI" style="transform: scale(1.5);">
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <input type='radio' id='apuntaNo_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="NO" style="transform: scale(1.5);">
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <input type='radio' id='apuntaPlanta_1' class='input_field' onclick='existeCollet(1)' name='existe_1' value="PLANTA" style="transform: scale(1.5);">
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            $<input type='number' step='any' min='1' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                        </td>
                        <td style='border:1px solid #337ab7;' align='center'>
                            <input type='number' step='any' min='1' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            $<input type='number' step='any' min='1' id='totalReng_1' class='input_field ' onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='860' style='width:90%;text-align:left;'> 
                            <input type='hidden' id='totalOperacion_1' value='0'>
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <!--<input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?> style="transform: scale(1.5);"> -->
                            <input type='checkbox' class='input_field' value="1" name='autorizaPzaGarantia[]' style='transform: scale(1.5);'>
                            <input type='hidden' id='valoresCM_1' name='registros[]'>
                            <br>
                            <?php echo form_error('registros', '<span class="error">', '</span>'); ?>
                        </td>
                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                            <!-- <a id='refaccionBusqueda_1' class='refaccionesBox btn btn-warning' data-value="1" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                <i class="fa fa-search"></i>
                            </a> -->
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8" align="right">
                        SUB-TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subTotalMaterial)) echo $subTotalMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="8" align="right">
                        I.V.A.
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($ivaMaterial)) echo $ivaMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="8" align="right">
                        PRESUPUESTO TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label id="presupuestoMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format(($subTotalMaterial+$ivaMaterial),2); else echo "0"; ?></label>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr style="border-top:1px solid #337ab7;">
                    <td colspan="8" align="right">
                        ANTICIPO
                    </td>
                    <td align="center">
                        $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                        <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                    </td>
                    <td colspan="2">
                        <label for="" id="anticipoNota" style="color:blue;"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" align="right">
                        TOTAL
                    </td>
                    <td colspan="1" align="center">
                        $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                        <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($totalMaterial)) echo $totalMaterial; else echo "0"; ?>">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="7" align="right">
                        FIRMA DEL ASESOR QUE APRUEBA
                    </td>
                    <td align="center" colspan="3">
                      <?php if (isset($firmaAsesor)): ?>
                          <?php if ($firmaAsesor != ""): ?>
                              <img class='marcoImg' src='<?php echo base_url().$firmaAsesor ?>' id='' style='width:80px;height:30px;'>
                              <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php echo $firmaAsesor ?>'>
                          <?php else: ?>
                              <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value=''>
                              <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                          <?php endif; ?>
                      <?php else: ?>
                          <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value=''>
                          <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                      <?php endif; ?>
                    </td>
                </tr>
            </tfoot>
        </table>

        <br><br>
        <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
            <thead>
                <tr>
                    <td colspan="7" style="border-bottom:1px solid #337ab7;">
                        <h3 style="text-align: center;">Piezas con garantías</h3>
                    </td>
                </tr>
                <tr>
                    <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                        CANTIDAD
                    </td>
                    <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                        D E S C R I P C I Ó N
                    </td>
                    <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                        NUM. PIEZA
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                        COSTO PZA FORD
                    </td>
                    <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                        HORAS
                    </td>
                    <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                        COSTO MO GARANTÍAS
                    </td>
                    <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center">
                        TOTAL REFACCIÓN
                        <input type="hidden" name="" id="renglonesGarantias" value="<?php if(isset($tablaPzsGarantias)) echo count($tablaPzsGarantias); else echo '0'; ?> ">
                    </td>
                </tr>
            </thead>
            <tbody id="cuerpoMateriales">
                <!-- Verificamos si existe la informacion -->
                <?php if (isset($tablaPzsGarantias)): ?>
                    <!-- Comprobamos que haya registros guardados ya editados -->
                    <?php if (count($tablaPzsGarantias)>0): ?>
                        <!-- creamos variable para totales con garantias-->
                        <?php $totalGarantia = 0; ?>
                        <?php foreach ($tablaPzsGarantias as $index => $valor): ?>
                            <!-- Comprobamos que el renglon a mostrar tenga la aprovacion del jefe de taller -->
                            <!--<tr id='fila_<?php echo $index+1; ?>'  <?php if (!in_array($tablaPzsGarantias[$index][0],$pzsAprobadasJDT)&&($pzsAprobadasJDT_2 != "")) echo 'hidden'; ?>>-->
                            <tr id='fila_<?php echo $index+1; ?>'>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='number' step='any' min='1' class='input_field' onblur='cantidadCollet_tabla2(<?php echo $index+1; ?>)' id='tb2_cantidad_<?php echo $index+1; ?>' value='<?php echo $tablaPzsGarantias[$index][1]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>

                                    <!-- Creamos un renglon pivote -->
                                    <input type="hidden" name="" id="renglonGarantia_<?php echo $tablaPzsGarantias[$index][0]; ?>" value="<?php echo $index+1; ?>">
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <textarea rows='3' class='input_field_lg' onblur='descripcionCollect_tabla2(<?php echo $index+1; ?>)' id='tb2_descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $tablaPzsGarantias[$index][2]; ?></textarea>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <textarea rows='1' class='input_field_lg' onblur='piezaCollet_tabla2(<?php echo $index+1; ?>)' id='tb2_pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php if(isset($tablaPzsGarantias[$index][3])) echo $tablaPzsGarantias[$index][3]; else echo ""; ?></textarea>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    $<input type='number' step='any' min='1' class='input_field' onblur='costoCollet_tabla2(<?php echo $index+1; ?>)' id='tb2_costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $tablaPzsGarantias[$index][4]; ?>' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    <input type='number' step='any' min='1' class='input_field' onblur='horasCollet_tabla2(<?php echo $index+1; ?>)' id='tb2_horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $tablaPzsGarantias[$index][5]; ?>' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border:1px solid #337ab7;' align='center'>
                                    $<input type='number' step='any' min='1' id='tb2_totalReng_<?php echo $index+1; ?>' onblur="autosuma_tabla2(<?php echo $index+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $tablaPzsGarantias[$index][4]; ?>' style='width:90%;text-align:left;'>
                                    <input type='hidden' id='totalOperacion_<?php echo $index+1; ?>' value='<?php if($tablaPzsGarantias[$index][4] == "0") {echo "0";} else {if(isset($tablaPzsGarantias[$index][7])) echo $tablaPzsGarantias[$index][7]; else echo "0";} ?>'>
                                </td>
                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                    <?php $conteo1 = (int)$tablaPzsGarantias[$index][1] * (float)$tablaPzsGarantias[$index][3]; ?>  
                                    <?php $conteo2 = (float)$tablaPzsGarantias[$index][4] * (float)$tablaPzsGarantias[$index][5]; ?>
                                    <label for="" id="totalRenglonGarantia_<?php echo $index+1; ?>" style="color:blue;">$<?php echo number_format(($conteo1+$conteo2),2); ?></label>
                                    <?php $totalGarantia += ($conteo1+$conteo2); ?>
                                    <input type='hidden' id='tablaPzsGarantias_<?php echo $index+1; ?>' name='tablaPzsGarantias[]' value="<?php echo $tablaPzsGarantias_rg[$index]; ?>">
                                    <input type="hidden" name="" id="totalPzsGarantias" value="<?php echo $totalGarantia; ?>">
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="7" style="border-bottom:1px solid #337ab7;">
                                <h5 style="text-align: center">Sin refacciones para mostrar</h5>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="7" style="border-bottom:1px solid #337ab7;">
                            <h5 style="text-align: center">Sin refacciones para mostrar</h5>
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
            <?php if (isset($totalGarantia)): ?>
                <tfoot>
                    <tr>
                        <td colspan="6" align="right">
                            SUB-TOTAL
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="subTotalMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo $totalGarantia; ?></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="right">
                            I.V.A.
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="ivaMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo number_format(($totalGarantia*0.16)); ?></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="right">
                            TOTAL
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="totalMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo number_format(($totalGarantia*1.16)); ?></label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" style="border-top:1px solid #337ab7;">
                            <br>
                        </td>
                    </tr>                
                    <tr>
                        <td colspan="5" align="right">
                            CLIENTE PAGA &nbsp;&nbsp;$&nbsp;
                        </td>
                        <td colspan="2" align="center">
                            <input type='number' step='any' min='1' class='form-control' id='clientePaga' name="clientePaga" value='<?php if(set_value('clientePaga') != '') {echo set_value('clientePaga');} else {if(isset($clientePaga)) echo $clientePaga; else echo '0';} ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right">
                            FORD PAGA &nbsp;&nbsp;$&nbsp;
                        </td>
                        <td colspan="2" align="center">
                            <input type='number' step='any' min='1' class='form-control' id='fordPaga' name="fordPaga" value="<?php if(set_value('fordPaga') != '') {echo set_value('fordPaga');} else {if(isset($fordPaga)) echo $fordPaga; else echo '0';} ?>" onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right">
                            TOTAL REPARACIÓN &nbsp;&nbsp;$&nbsp;
                        </td>
                        <td colspan="2" align="center">
                            <input type='number' step='any' min='1' class='form-control' id='totalReparacion' name="totalReparacion" value='<?php if(set_value('totalReparacion') != '') {echo set_value('totalReparacion');} else {if(isset($totalReparacion)) echo $totalReparacion; else echo '0';} ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                        </td>
                    </tr>
                </tfoot>
            <?php endif ?>
        </table>

        <br><br>
        <div class="row">
            <div class="col-sm-5">
                <h4>Notas del asesor:</h4>
                <!-- Verificamos si es una vista del asesor -->
                <?php if (isset($dirige)): ?>
                    <?php if ($dirige == "ASE"): ?>
                        <textarea id="notaAsesor" name="notaAsesor" rows="2" style="width:100%;border-radius:4px;" placeholder="Notas del asesor..."><?php if(isset($notaAsesor)) echo $notaAsesor;?></textarea>
                    <?php else: ?>
                        <label for="" style="font-size:12px;"><?php if(isset($notaAsesor)){ if($notaAsesor != "") echo $notaAsesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="font-size:12px;"><?php if(isset($notaAsesor)){ if($notaAsesor != "") echo $notaAsesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                <?php endif; ?>
            </div>
            <div class="col-sm-7" align="right">
                <h5>Archivo(s) de la cotización:</h5><br>
                <?php if (isset($archivo)): ?>
                    <?php $indice = 1; ?>
                    <?php for ($i = 0; $i < count($archivo); $i++): ?>
                      <?php if ($archivo[$i] != ""): ?>
                          <a href="<?php echo base_url().$archivo[$i]; ?>" class="btn btn-info" target="_blank">Doc. (<?php echo $indice; ?>)</a>
                          <?php $indice++; ?>
                          <?php if ((($i+1)% 3) == 0): ?>
                              <br>
                          <?php endif; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                <?php endif; ?>
                <input type="hidden" name="tempFile" value="<?php if(isset($archivoTxt)) echo $archivoTxt;?>">
                <br>
                <input type="file" multiple name="uploadfiles[]"><br>
            </div>
        </div>

        <br>
        <div class="row" <?php if(isset($dirige)) if($dirige == "ASE") echo "hidden"; else echo ""; else echo "hidden"; ?>>
            <div class="col-md-6">
                <h4>Comentario del técnico para Refacciones:</h4>
                <?php if (isset($dirige)): ?>
                    <?php if ($dirige == "TEC"): ?>
                        <textarea id="comentarioTecnico" name="comentarioTecnico" rows="2" style="width:100%;border-radius:4px;" placeholder="Clave de la refacción..."><?php if(isset($comentarioTecnico)) echo $comentarioTecnico;?></textarea>
                    <?php else: ?>
                        <label for="" style="font-size:12px;"><?php if(isset($comentarioTecnico)){ if($comentarioTecnico != "") echo $comentarioTecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="font-size:12px;"><?php if(isset($comentarioTecnico)){ if($comentarioTecnico != "") echo $comentarioTecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                <?php endif; ?>

                <br>
                <h5>Archivo(s) para Refacciones:</h5>
                <?php if (isset($archivoTecnico)): ?>
                    <?php for ($i = 0; $i < count($archivoTecnico); $i++): ?>
                      <?php if ($archivoTecnico[$i] != ""): ?>
                          <a href="<?php echo base_url().$archivoTecnico[$i]; ?>" class="btn btn-primary" target="_blank">Adj. (<?php echo $i+1; ?>)</a>
                          <?php if ((($i+1)% 3) == 0): ?>
                              <br>
                          <?php endif; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                    <?php if (count($archivoTecnico) == 0): ?>
                        <a href="" class="btn btn-primary" target="_blank">Sin archivos</a>
                    <?php endif; ?>
                <?php endif; ?>

                <br>
                <input id="uploadfilesTec" type="file" accept="image/*" multiple name="uploadfilesTec[]"><br>
                <input type="hidden" name="tempFileTecnico" value="<?php if(isset($archivoImgTecnico)) echo $archivoImgTecnico;?>">
            </div>

            <div class="col-md-6" align="right" style="border-left: 1px solid black;">
                <h4>Comentarios del Jefe de Taller:</h4>
                <?php if (isset($dirige)): ?>
                    <?php if ($dirige == "JDT"): ?>
                        <textarea rows='2' name="comentariosExtra" id='comentario' style='width:100%;text-align:left;font-size:12px;'><?php if(set_value('comentariosExtra') != "") { echo set_value('comentariosExtra');} else { if(isset($comentario)) echo $comentario;}?></textarea>
                    <?php else: ?>
                        <label for="" style="font-size:12px;"><?php if(isset($comentario)){ if($comentario != "") echo $comentario; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="font-size:12px;"><?php if(isset($comentario)){ if($comentario != "") echo $comentario; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                <?php endif; ?>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6" align="center" style="border: 0.5px solid darkblue;">
                <table style="width: 90%;margin-left: 10px;margin-right: 10px;">
                    <tr>
                        <td>
                            <h4 align="center">Garantías</h4>
                            Comentarios:
                             <textarea id="notaGarantias" name="notaGarantias" rows="2" style="width:100%;border-radius:4px;" placeholder="Notas del encargado de garantias..."><?php if(set_value('notaGarantias') != "") { echo set_value('notaGarantias');} else {if(isset($notaGarantias)) echo $notaGarantias; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <?php if (isset($firmaGarantias)): ?>
                                <?php if ($firmaGarantias == ""): ?>
                                    <input type="hidden" id="rutaFirmaGarantia" name="rutaFirmaGarantia" value="<?= set_value('rutaFirmaGarantia');?>">
                                    <img class="marcoImg" src="<?php if(set_value('rutaFirmaGarantia') != "") echo set_value('rutaFirmaGarantia');  ?>" id="firmaFirmaGarantia" alt="" style="height:2cm;width:4cm;">
                                    <br>
                                <?php else: ?>
                                    <input type="hidden" id="rutaFirmaGarantia" name="rutaFirmaGarantia" value="<?php if(isset($firmaGarantias)) echo $firmaGarantias;?>" disabled>
                                    <img class="marcoImg" src="<?php if(isset($firmaGarantias)) echo base_url().$firmaGarantias;?>" id="firmaFirmaGarantia" alt="" style="height:2cm;width:4cm;">
                                    <br>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaFirmaGarantia" name="rutaFirmaGarantia" value="<?= set_value('rutaFirmaGarantia');?>">
                                <img class="marcoImg" src="<?php if(set_value('rutaFirmaGarantia') != "") echo set_value('rutaFirmaGarantia');  ?>" id="firmaFirmaGarantia" alt="" style="height:2cm;width:4cm;">
                                <br>
                            <?php endif; ?>
                            <?php echo form_error('rutaFirmaGarantia', '<span class="error">', '</span>'); ?><br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <?php if (isset($firmaGarantias)): ?>
                                <?php if ($firmaGarantias == ""): ?>
                                    <a id="Garantia_firma" class="cuadroFirma btn btn-primary" data-value="Garantias" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Garantías</a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a id="Garantia_firma" class="cuadroFirma btn btn-primary" data-value="Garantias" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Garantías </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-3"></div>
        </div>

        <br><br>
        <div class="col-sm-12" align="center">
            <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="3A">
            <input type="hidden" name="" id="origen" value="Cotizacion Multipunto">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
            <input type="hidden" name="" id="envioRefacciones" value="<?php if(isset($refacciones)) echo $refacciones; else echo "0"; ?>">
            
            <input type="hidden" name="envioGarantia" id="envioGarantia" value="<?php if(isset($envioGarantia)) echo $envioGarantia; else echo "0"; ?>">
            <input type="hidden" name="pzaGarantia" id="pzaGarantia" value="<?php if(isset($pzaGarantia_Og)) echo $pzaGarantia_Og; ?>">
            <input type="hidden" name="pzaAutorizaGarantia" id="pzaAutorizaGarantia" value="<?php if(isset($pzaAutorizada_Og)) echo $pzaAutorizada_Og; ?>">

            <input type="hidden" name="envioJDT" id="envioJDT" value="<?php if(isset($envioJDT)) echo $envioJDT; else echo "0"; ?>">

            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
            <input type="hidden" name="tipoRegistro" id="tipoRegistro" value="<?php if(isset($tipoRegistro)) echo $tipoRegistro; ?>">
            
            <button type="submit" class="btn btn-success" name="aceptarCotizacion" id="aceptarCotizacion" disabled>Enviar Cotización al Asesor</button>

            <button type="button" id="regresarBtn" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Garantias/4";?>';" >
                Regresar
            </button>
        </div>
    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Formulario de superusuario -->
<div class="modal fade " id="superUsuario" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Ingreso.</h5>
            </div>
            <div class="modal-body" style="font-size:12px;">
                <label for="" style="font-size: 14px;font-weight: initial;">Usuario:</label>
                <br>
                <input type="text" class="input_field" type="text" id="superUsuarioName" value="" style="width:100%;">
                <br><br>
                <label for="" style="font-size: 14px;font-weight: initial;">Password:</label>
                <br>
                <input type="password" class="input_field" type="text" id="superUsuarioPass" value="" style="width:100%;">
                <br><br><br>
                <h4 class="error" style="font-size: 16px;font-weight: initial;" align="center" id="superUsuarioError">:</h4>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="superUsuarioEnvio">Validar</button>
            </div>
        </div>
    </div>
</div>

