<div class="" style="margin:20px;">
    <h3 class="text-center"><?= SUCURSAL ?>, S.A. DE C.V.</h3> 
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row">
            <div class="col-md-2">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" name="" readonly="" disabled="" id="idOrdenTempCotiza" value="<?php if(isset($idOrden)) echo $idOrden; ?>" style="width:100%;" >
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Folio Intelisis:</label>&nbsp;<br>
                <label class="" style="color:darkblue;"><?php if(isset($idIntelisis)) echo $idIntelisis;  ?></label>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="" id="noSerieText" value="<?php echo set_value('noSerie');?>" style="width:100%;" readonly="" disabled="">
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;" readonly="" disabled="">
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp; 
                <input type="text" class="input_field" name="" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;" readonly="" disabled="">
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;" readonly="" disabled="">
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;" readonly="" disabled="">
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;" readonly="" disabled="">
            </div>
        </div>

        <?php if (isset($aceptoTermino)): ?>
            <?php if ($aceptoTermino == ""): ?>
                <div class="alert alert-primary" role="alert">
                    Visualiza la cotización y da clic en "Aceptar la cotización", "Rechazar la cotización" o puedes seleccionar una a una las refacciones.
                </div>
            <?php elseif ($aceptoTermino == "Si"): ?>
                <div class="alert alert-success" role="alert" align="center">
                    Cotización Aceptada Completa.
                </div>
            <?php elseif ($aceptoTermino == "Val"): ?>
                <div class="alert alert-warning" role="alert" align="center">
                    Cotización Afectada por Detalle.
                </div>
            <?php else: ?>
                <div class="alert alert-danger" role="alert" align="center">
                    Cotización Rechazada Completa.
                </div>
            <?php endif; ?>
        <?php else: ?>
            <div class="alert alert-primary" role="alert">
                No se cargo correctamente la información.
            </div>
        <?php endif; ?>

        <table style="width:100%;border:1px solid #337ab7;border-radius: 4px;">
            <tr>
                <td style="width:7%;border-bottom:1px solid #337ab7;" align="center"><strong>CANTIDAD</strong></td>
                <td style="width:42%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>  D E S C R I P C I Ó N</strong></td>
                <td style="width:18%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>NUM. PIEZA</strong></td>
                <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO PZA.</strong></td>
                <td style="width:5%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>HORAS</strong></td>
                <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO MO</strong></td>
                <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                    <strong>REVISADO <br> (ASESOR)</strong>
                    <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($registrosControl)) if(count($registrosControl) > 0) echo count($registrosControl)+1; else echo "1"; else echo "1"; ?>">
                </td>
                <?php if (isset($registrosControl)): ?>
                    <td style="width:20%;border-bottom:1px solid #337ab7;" colspan="2" align="center"><strong>ELECCIÓN</strong></td>
                <?php endif; ?>
            </tr>
            <?php if (isset($registrosControl)): ?>
                <?php if (count($registrosControl)>0): ?>
                    <?php foreach ($registrosControl as $index => $value): ?>
                        <?php if ($aceptoTermino != "Val"): ?>
                            <?php if ($aceptoTermino == "Si"): ?>
                                <tr id="fila_indicador_<?php echo $index+1; ?>"style='background-color:#c7ecc7;'>
                            <?php elseif ($aceptoTermino == "No"): ?>
                                <tr id="fila_indicador_<?php echo $index+1; ?>" style='background-color:#f5acaa;'>
                            <?php else: ?>
                                <tr id="fila_indicador_<?php echo $index+1; ?>">
                            <?php endif; ?>
                        <?php else: ?>
                            <tr id="fila_indicador_<?php echo $index+1; ?>"  <?php if(count($registrosControl[$index])>9) if($registrosControl[$index][9] == "Aceptada") echo "style='background-color:#c7ecc7'"; else echo "style='background-color:#f5acaa'"; ?>>
                        <?php endif; ?>
                            <td style="border-bottom:1px solid #337ab7;height:20px;" align="center">
                                <label for=""><?php echo $registrosControl[$index][0]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][1]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align='center'>
                                <label for=""><?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][2]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][3]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <label for=""><?php echo $registrosControl[$index][4]; ?></label>
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                  <label for=""><?php echo $registrosControl[$index][5]; ?></label>
                            </td>
                            <td style="width: 10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <a id="interruptorA_<?php echo $index+1; ?>" class="btn-success btn multipleEleccion" title="Aceptar" onclick="aceptarItem(<?php echo $index+1; ?>)" style="color:white;margin:5px;" hidden>
                                    <i class="fas fa-check"></i>
                                </a>
                            </td>
                            <td style="width: 10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <a id="interruptorB_<?php echo $index+1; ?>" class="btn-danger btn multipleEleccion" title="Rechazar" onclick="rechazarItem(<?php echo $index+1; ?>)" style="color:white;margin:5px;" hidden>
                                    <i class="fas fa-times"></i>
                                </a>
                                <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $registrosControlRenglon[$index]; ?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php else: ?>
                <tr>
                    <td colspan="6" align="center" style="border-bottom:1px solid #337ab7;height:20px;background-color: #ddd;">
                        No se registro ninguna cotización.
                    </td>
                    <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;background-color: #ddd;" align="center"></td>
                </tr>
            <?php endif; ?>
            <tr>
                <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    SUB-TOTAL
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                    <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subTotalMaterial)) echo $subTotalMaterial; else echo "0"; ?>">
                </td>
            </tr>
            <tr>
                <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;color:red;">
                    CANCELADO
                </td>
                <td align="center" style="border:1px solid #337ab7;color:red;">
                    $<label for="" id="canceladoMaterialLabel"><?php if(isset($canceladoMaterial)) echo $canceladoMaterial; else echo "0"; ?></label>
                    <input type="hidden" id="canceladoMaterial" name="canceladoMaterial" value="0">
                </td>
            </tr>
            <tr>
                <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    I.V.A.
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                    <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($ivaMaterial)) echo $ivaMaterial; else echo "0"; ?>">
                </td>
            </tr>
            <tr>
                <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    PRESUPUESTO TOTAL
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label id="presupuestoMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format(($subTotalMaterial+$ivaMaterial),2); else echo "0"; ?></label>
                </td>
                <td colspan="2"></td>
            </tr>
            <tr style="border-top:1px solid #337ab7;">
                <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    ANTICIPO
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                    <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                </td>
                <td colspan="2">
                    <label for="" id="anticipoNota" style="color:blue;"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                </td>
            </tr>
            <tr>
                <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                    TOTAL
                </td>
                <td align="center" style="border:1px solid #337ab7;">
                    $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                    <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($totalMaterial)) echo $totalMaterial; else echo "0"; ?>">
                </td>
            </tr>
            <tr>
                <td colspan="5" align="right">
                    FIRMA DEL ASESOR QUE APRUEBA
                </td>
                <td align="center" colspan="2">
                    <?php if (isset($firmaAsesor)): ?>
                        <?php if ($firmaAsesor != ""): ?>
                            <img class='marcoImg' src='<?php echo base_url().$firmaAsesor ?>' id='' style='width:100px;height:40px;;'>
                        <?php else: ?>
                            <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' style='width:100px;height:40px;;'>
                        <?php endif; ?>
                    <?php else: ?>
                        <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' style='width:100px;height:40px;;'>
                    <?php endif; ?>
              </td>
            </tr>
        </table>

        <div class="row">
            <div class="col-sm-12" align="right">
                <?php if (isset($archivo)): ?>
                    <?php for ($i = 0; $i < count($archivo); $i++): ?>
                        <?php if ($archivo[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo[$i]; ?>" class="btn btn-info" target="_blank">Ver documento (<?php echo $i+1; ?>)</a>
                            <?php if ((($i+1)% 2) == 0): ?>
                                <br>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endfor; ?>
                <?php else: ?>
                    <a  class="btn btn-info" onclick="event.preventDefault()">Sin documentos</a>
                <?php endif; ?>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-5">
                <h4>Notas del asesor:</h4>
                <!-- Verificamos si es una vista del asesor -->
                <?php if (isset($dirige)): ?>
                    <?php if ($dirige == "ASE"): ?>
                        <textarea id="notaAsesor" name="notaAsesor" rows="2" style="width:100%;border-radius:4px;" placeholder="Notas del asesor..."><?php if(isset($notaAsesor)) echo $notaAsesor;?></textarea>
                    <?php else: ?>
                        <label for="" style="font-size:12px;"><?php if(isset($notaAsesor)){ if($notaAsesor != "") echo $notaAsesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="font-size:12px;"><?php if(isset($notaAsesor)){ if($notaAsesor != "") echo $notaAsesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                <?php endif; ?>
            </div>
            <div class="col-sm-7" align="right">
                <h5>Archivo(s) de la cotización:</h5><br>
                <?php if (isset($archivo)): ?>
                    <?php $indice = 1; ?>
                    <?php for ($i = 0; $i < count($archivo); $i++): ?>
                      <?php if ($archivo[$i] != ""): ?>
                          <a href="<?php echo base_url().$archivo[$i]; ?>" class="btn btn-info" target="_blank">Doc. (<?php echo $indice; ?>)</a>
                          <?php $indice++; ?>
                          <?php if ((($i+1)% 3) == 0): ?>
                              <br>
                          <?php endif; ?>
                      <?php endif; ?>
                    <?php endfor; ?>
                <?php endif; ?>
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-12" align="center">
                <button type="button" id="regresarBtn2" class="btn btn-dark" onclick="location.href='<?=base_url()."Presupuesto_Ventanilla/4";?>';" style="display:none;">
                Regresar
                </button> 
            </div>
        </div>

        <br><br>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6" align="center">
                <button type="button" class="btn btn-info" id="pedirRefacciones" style="display: none;">Solicitar Refacciones</button>
                <button type="button" class="btn btn-warning" id="recibirRefacciones" style="display: none;">Recibir Refacciones</button>
                <button type="button" class="btn btn-success" id="entregarRefacciones" style="display: none;">Entregar Refacciones</button>
                
                <br>
                <input type="hidden" id="idCitaRef" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                <input type="hidden" id="estadoRef" value="<?php if(isset($statusRef)) echo $statusRef; else echo ""; ?>">
                <input type="hidden" id="aceptoTermino" value="<?php if(isset($aceptoTermino)) echo $aceptoTermino; else echo "No"; ?>">
                <label style="color:red;font-size: 12px;" id="errorRefacciones"></label>
            </div>
            <div class="col-sm-3"></div>
        </div>

</div>

<!-- Modal para la envio de decision-->
<div class="modal fade" id="AlertaModal" role="dialog" data-backdrop="static" data-keyboard="false" style="background-color:">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="AlertaModalCuerpo">
            <div class="modal-body">
                <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;display:none;'>
                    Su navegador no soporta canvas
                </canvas>
                <form class="" id="formularioAceptaCoti" action="" method="post">
                    <br><br>
                    <h5 class="modal-title" id="tituloForm" align="center"></h5>
                    <br>
                    <div class="row">
                        <div class="col-md-12"  align="center">
                            <i class="fas fa-spinner cargaIcono"></i>
                            <br>
                            <label for="" class="error" id="errorEnvioCotizacion"></label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12" align="right">
                            <input type="hidden" name="desicionCoti" id="desicionCoti" value="">
                            <input type="hidden" name="idOrden" id="idOrden" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancelarCoti">Cancelar</button>
                            <button type="button" class="btn btn-primary" id="enviarCotizacionApro">Confirmar</button>
                        </div>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
