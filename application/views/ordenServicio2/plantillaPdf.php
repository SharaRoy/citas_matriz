<?php 
    // Eliminamos cache
    //la pagina expira en una fecha pasada
    header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT"); 
    //ultima actualizacion ahora cuando la cargamos
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
    //no guardar en CACHE
    header ("Cache-Control: no-cache, must-revalidate"); 
    header ("Pragma: no-cache");

    ob_start();

?>

<style media="screen">
    #encabezado {
        color: #fff;
        background-color: #337ab7;
        border-color: #337ab7;
        border-radius: 4px;
    }

    p, label{
        text-align: justify;
        font-size:8px;
        color: #337ab7;
    }

    .tituloTxt{
        font-size:9px;
        color: #337ab7;
    }
    .division{
        background-color: #ddd;
    }
    .headers_table{
        border:1px solid black;
        border-style: double;
    }
    td,tr{
        padding: 0px 0px 0px 0px !important;
        font-size: 8px;
        color:#337ab7;
    }

    .verticaltext {
        padding: 5px;
        width: 50px;
        text-align:center;
        /* transform: rotate(270deg); */
        writing-mode: vertical-lr;
        transform: rotate(180deg);
    }
</style>

<page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm">
    <table style="width: 100%;">
        <tr>
            <td width="20%">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="width:150px;height:40px;">
            </td>
            <td style="width:60%;" colspan="2" align="center">
                <strong>
                    <span class="color-blue" style="font-size:12px;color: #337ab7;" align="center">
                        <?= SUCURSAL ?>, S.A. DE C.V.
                    </span>
                </strong>

                <p class="justify" style="font-size:8px;" align="center">
                    <?=$this->config->item('encabezados_txt')?>
                </p>
            </td>
            <td width="20%" align="" style="">
                <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:100px;height:35px;"><br>
            </td>
        </tr>
    </table>
    
    <br>
    <h6 align="center" style="color:#340f7b;font-size:10px;border-radius:4px;">
        <strong>CONTROL DE MATERIALES</strong>
    </h6>

    <table style="width:100%;border:1px solid #337ab7;border-radius: 4px;margin-top:-10px;">
        <tr>
            <td style="width:10%;border-bottom:1px solid #337ab7;" align="center"><strong>CANTIDAD</strong></td>
            <td style="width:40%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>  D E S C R I P C I Ó N</strong></td>
            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>NUM. PIEZA</strong></td>
            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO</strong></td>
            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>HORAS</strong></td>
            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>TOTAL</strong></td>
            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>AUTORIZÓ</strong></td>
        </tr>
        <?php if (isset($registrosControl)): ?>
            <?php if (count($registrosControl)>0): ?>
                <?php foreach ($registrosControl as $index => $value): ?>
                    <tr>
                        <td style="width:10%;border-bottom:1px solid #337ab7;height:20px;" align="center">
                            <label for="" style="color:black;"><?php echo $registrosControl[$index][0]; ?></label>
                        </td>
                        <td style="width:40%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label for="" style="color:black;"><?php echo $registrosControl[$index][1]; ?></label>
                        </td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label for="" style="color:black;"><?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?></label>
                        </td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label for="" style="color:black;"><?php echo $registrosControl[$index][2]; ?></label>
                        </td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label for="" style="color:black;"><?php echo $registrosControl[$index][3]; ?></label>
                        </td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label for="" style="color:black;"><?php echo $registrosControl[$index][4]; ?></label>
                        </td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                              <label for="" style="color:black;"><?php echo $registrosControl[$index][5]; ?></label>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php for ($i = 0; $i<(15-count($registrosControl)) ; $i++): ?>
                    <tr>
                        <td style="width:10%;border-bottom:1px solid #337ab7;height:20px;" align="center"></td>
                        <td style="width:40%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    </tr>
                <?php endfor; ?>
            <?php else: ?>
                <?php for ($i = 0; $i<15 ; $i++): ?>
                    <tr>
                        <td style="width:10%;border-bottom:1px solid #337ab7;height:20px;" align="center"></td>
                        <td style="width:40%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    </tr>
                <?php endfor; ?>
            <?php endif; ?>
        <?php else: ?>
            <?php for ($i = 0; $i<15 ; $i++): ?>
                <tr>
                    <td style="width:10%;border-bottom:1px solid #337ab7;height:20px;" align="center"></td>
                    <td style="width:40%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                 </tr>
            <?php endfor; ?>
        <?php endif; ?>
        <tr>
            <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                SUB-TOTAL
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                I.V.A.
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                TOTAL
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;">
                $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right" style="height:35px;">
                FIRMA DEL ASESOR QUE APRUEBA
            </td>
            <td align="center">
              <?php if (isset($firmaAsesor)): ?>
                  <?php if ($firmaAsesor != ""): ?>
                      <img class='marcoImg' src='<?php echo base_url().$firmaAsesor ?>' id='' style='width:80px;height:30px;'>
                  <?php endif; ?>
              <?php endif; ?>
            </td>
        </tr>
    </table>

    <br>
    <table style="width:100%;border:1px solid #337ab7;border-radius:4px;">
        <tr>
            <td align="center" style="border-bottom:1px solid #337ab7;width:4%;">
                DÍA
            </td>
            <?php for ($i = 0; $i<31 ; $i++): ?>
              <td align="center" class="verticaltext" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;width:3.1%;">
                  <label for="" style="color:black;"><?php echo $i+1; ?></label>
              </td>
            <?php endfor; ?>
        </tr>
        <tr>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                PROG
            </td>
            <?php if (isset($diaProg)): ?>
                <!-- Verificamos que se haya cargado la información -->
                <?php for ($i = 0; $i<count($diaProg)-1 ; $i++): ?>
                  <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;height:25px;">
                      <label for="" style="color:black;"><?php echo $diaProg[$i]; ?></label>
                  </td>
                <?php endfor; ?>
            <?php else: ?>
                <!-- Si no se cargo la variable -->
                <?php for ($i = 0; $i<31 ; $i++): ?>
                    <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;height:25px;"></td>
                <?php endfor; ?>
            <?php endif; ?>
        </tr>
        <tr>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                REAL
            </td>
            <?php if (isset($diaReal)): ?>
                <!-- Verificamos que se haya cargado la información -->
                <?php for ($i = 0; $i<count($diaReal)-1 ; $i++): ?>
                  <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;height:25px;">
                      <label for="" style="color:black;"><?php echo $diaReal[$i]; ?></label>
                  </td>
                <?php endfor; ?>
            <?php else: ?>
                <!-- Si no se cargo la variable -->
                <?php for ($i = 0; $i<31 ; $i++): ?>
                    <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;height:25px;"></td>
                <?php endfor; ?>
            <?php endif; ?>
        </tr>
        <tr>
            <td colspan="32" align="center" style="border-bottom:1px solid #337ab7;height:15px;">
                A = X AUTORIZAR, M= MECÁNICA, L= LAMINADO, PR = PREPARACIÓN, P = PINTURA, AR=ARMADO, D = DETALLADO
            </td>
        </tr>
        <tr>
            <td colspan="32" align="center" style="height:15px;">
                Lo,ARo= GRUPO ORO; Lp,ARp=GRUPO PLATA; Pn,PRn,Dn=GRUPO NARANJA; Pb=GRUPO BLANCO
            </td>
        </tr>
    </table>

    <h6 align="center" style="color:#340f7b;font-size:10px;border-radius:4px;">
        <strong>AUTOCERTIFICACIÓN Y PAGO</strong>
    </h6>
    <table style="width:100%;">
        <tr>
            <td align="center" style="height:40px;" colspan="2">
            </td>
            <td align="center">
                <strong>FECHA DE RECEPCIÓN</strong>
            </td>
            <td align="center" colspan="3">
                <strong>FECHA Y FIRMA OPERARIO</strong>
            </td>
            <td align="center" colspan="3">
                <strong>FECHA Y FIRMA LÍDER</strong>
            </td>
            <td align="center" colspan="3">
                <strong>FECHA Y FIRMA COORDINADOR</strong>
            </td>
        </tr>
        <tr>
            <td align="left" style="height:20px;width:8%;">
                <strong>MECÁNICA</strong>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_mecaOpera)): ?>
                    <?php if (($firma_mecaOpera != "")||($firma_mecaLider != "")||($firma_mecaCoord != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_recepMeca)) echo $fech_recepMeca; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_mecaOpera)): ?>
                    <?php if (($firma_mecaOpera != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_mecaOpera)) echo $fech_mecaOpera; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_mecaOpera)): ?>
                    <?php if ($firma_mecaOpera != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_mecaOpera; ?>" id="" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <!-- <input type='date' name="fechamMecaLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'> -->
                <?php if (isset($firma_mecaLider)): ?>
                    <?php if (($firma_mecaLider != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_mecaLider)) echo $fech_mecaLider; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_mecaLider)): ?>
                    <?php if ($firma_mecaLider != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_mecaLider; ?>" id="" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <!-- <input type='date' name="fechaMecaCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'> -->
                <?php if (isset($firma_mecaCoord)): ?>
                    <?php if (($firma_mecaCoord != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_mecaCoord)) echo $fech_mecaCoord; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
              <?php if (isset($firma_mecaCoord)): ?>
                  <?php if ($firma_mecaCoord != ""): ?>
                      <img class="marcoImg" src="<?php echo base_url().$firma_mecaCoord; ?>" id="" alt="" style="width:70px;height:20px;">
                  <?php endif; ?>
              <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="height:20px;">
                <strong>LAMINADO</strong>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_lamiOpera)): ?>
                    <?php if (($firma_lamiOpera != "")||($firma_lamiLider != "")||($firma_lamiCoord != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_recepLami)) echo $fech_recepLami; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_lamiOpera)): ?>
                    <?php if ($firma_lamiOpera != ""): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_lamiOpera)) echo $fech_lamiOpera; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_lamiOpera)): ?>
                    <?php if (($firma_lamiOpera != "")): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_lamiOpera; ?>" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_lamiLider)): ?>
                    <?php if ($firma_lamiLider != ""): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_lamiLider)) echo $fech_lamiLider; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_lamiLider)): ?>
                    <?php if ($firma_lamiLider != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_lamiLider; ?>" id="" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_lamiCoord)): ?>
                    <?php if ($firma_lamiCoord != ""): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_lamiCoord)) echo $fech_lamiCoord; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;width:10%;">
                <?php if (isset($firma_lamiCoord)): ?>
                    <?php if ($firma_lamiCoord != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_lamiCoord; ?>" id="" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="height:20px;">
                <strong>PREPARACIÓN</strong>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_prepOpera)): ?>
                    <?php if (($firma_prepOpera != "")||($firma_prepLider != "")||($firma_prepCoord != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_recepPrep)) echo $fech_recepPrep; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_prepOpera)): ?>
                    <?php if ($firma_prepOpera != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_prepOpera; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_prepOpera)): ?>
                    <?php if ($firma_prepOpera != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_prepOpera; ?>" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_prepLider)): ?>
                    <?php if ($firma_prepLider != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_prepLider; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_prepLider)): ?>
                    <?php if ($firma_prepLider != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_prepLider; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_prepCoord)): ?>
                    <?php if ($firma_prepCoord != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_prepCoord; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_prepCoord)): ?>
                    <?php if ($firma_prepCoord != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_prepCoord; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="height:20px;">
                <strong>PINTURA</strong>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_pintOpera)): ?>
                    <?php if (($firma_pintOpera != "")||($firma_pintLider != "")||($firma_pintCoord != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_recepPint)) echo $fech_recepPint; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_pintOpera)): ?>
                    <?php if ($firma_pintOpera != ""): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_pintOpera)) echo $fech_pintOpera ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_pintOpera)): ?>
                    <?php if ($firma_pintOpera != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_pintOpera; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_pintLider)): ?>
                    <?php if ($firma_pintLider != ""): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_pintLider)) echo $fech_pintLider; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_pintLider)): ?>
                    <?php if ($firma_pintLider != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_pintLider; ?>" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_pintCoord)): ?>
                    <?php if ($firma_pintCoord != ""): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_pintCoord)) echo $fech_pintCoord; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_pintCoord)): ?>
                    <?php if ($firma_pintCoord != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_pintCoord; ?>" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="height:20px;">
              <strong>  ARMADO</strong>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_armaOpera)): ?>
                    <?php if (($firma_armaOpera != "")||($firma_armaLider != "")||($firma_armaCoord != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_recepArma)) echo $fech_recepArma; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_armaOpera)): ?>
                    <?php if ($firma_armaOpera != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_armaOpera; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_armaOpera)): ?>
                    <?php if ($firma_armaOpera != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_armaOpera; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_armaLider)): ?>
                    <?php if ($firma_armaLider != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_armaLider; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_armaLider)): ?>
                    <?php if ($firma_armaLider != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_armaLider; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_armaCoord)): ?>
                    <?php if ($firma_armaCoord != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_armaCoord; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_armaCoord)): ?>
                    <?php if ($firma_armaCoord != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_armaCoord; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="height:20px;">
                <strong>DETALLADO</strong>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_detallOpera)): ?>
                    <?php if (($firma_detallOpera != "")||($firma_detallLider != "")||($firma_detallCoord != "")): ?>
                        <label for="" style="color:black;"><?php if(isset($fech_recepDetall)) echo $fech_recepDetall; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_detallOpera)): ?>
                    <?php if ($firma_detallOpera != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_detallOpera; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_detallOpera)): ?>
                    <?php if ($firma_detallOpera != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_detallOpera; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_detallLider)): ?>
                    <?php if ($firma_detallLider != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_detallLider; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_detallLider)): ?>
                    <?php if ($firma_detallLider != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_detallLider; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_detallCoord)): ?>
                    <?php if ($firma_detallCoord != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_detallCoord; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_detallCoord)): ?>
                    <?php if ($firma_detallCoord != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_detallCoord; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td align="left" style="height:20px;">
                <strong>C. DE CALIDAD</strong>
            </td>
            <td style="width:3%;"></td>
            <td align="center"></td>
            <td style="width:3%;"></td>
            <td align="center" colspan="2"></td>
            <td style="width:3%;"></td>
            <td align="center" colspan="2"></td>
            <td style="width:3%;"></td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_calidCoord)): ?>
                    <?php if ($firma_calidCoord != ""): ?>
                        <label for="" style="color:black;"><?php echo $fech_calidCoord; ?></label>
                    <?php else: ?>
                        <label for="" style="color:black;">Sin registro</label>
                    <?php endif; ?>
                <?php else: ?>
                    <label for="" style="color:black;">Sin registro</label>
                <?php endif; ?>
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;">
                <?php if (isset($firma_calidCoord)): ?>
                    <?php if ($firma_calidCoord != ""): ?>
                        <img class="marcoImg" src="<?php echo base_url().$firma_calidCoord; ?>" alt="" style="width:70px;height:20px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
    </table>

    <br>
    <table style="width:100%;">
        <tr>
            <td style="width:48%;">
                <table style="border:1px solid #337ab7; width:100%;border-radius:4px;">
                    <tr>
                        <td align="center" style="border-bottom:1px solid #337ab7;width:10%;height:15px;">
                            O.C.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;width:50%;height:15px;">
                            GASOLINA Y LUBRICANTE
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;width:20%;height:15px;">
                            COSTO
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;width:20%;height:15px;">
                            VENTA
                        </td>
                    </tr>
                    <?php if (isset($costos)): ?>
                        <?php if (count($costos)>0): ?>
                            <?php foreach ($costos as $index => $valor): ?>
                                <tr>
                                    <td align="center" style="border-bottom:1px solid #337ab7;height:15px;">
                                        <label for="" style="color:black;"><?php echo $costos[$index][0]; ?></label>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;">
                                        <label for="" style="color:black;"><?php echo $costos[$index][1]; ?></label>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;">
                                        <label for="" style="color:black;"><?php echo number_format($costos[$index][2],2); ?></label>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;">
                                        <label for="" style="color:black;"><?php echo number_format($costos[$index][3],2); ?></label disabled>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php for ($i = 0; $i<(12-count($costos)-1) ; $i++): ?>
                                <tr>
                                    <td align="center" style="border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                </tr>
                            <?php endfor; ?>

                        <?php else: ?>
                            <?php for ($i = 0; $i<12 ; $i++): ?>
                                <tr>
                                    <td align="center" style="border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php for ($i = 0; $i<12 ; $i++): ?>
                            <tr>
                                <td align="center" style="border-bottom:1px solid #337ab7;height:15px;"></td>
                                <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif; ?>
                    <tr>
                        <td colspan="3" align="right">
                            TOTALES
                        </td>
                        <td colspan="1" align="center">
                            <label for="" style="color:black;"><?php if(isset($totalesGasolina)) echo number_format($totalesGasolina,2); else echo "$0.00"; ?></label>
                        </td>
                    </tr>
                </table>
            </td>

            <td style="width:49%;">
                <table style="border:1px solid #337ab7; width:100%;border-radius:4px;margin-left:10px;">
                    <tr>
                        <td align="center" style="border-bottom:1px solid #337ab7;width:10%;height:15px;">
                            O.C.
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;width:50%;height:15px;">
                            TRABAJO FUERA TALLER
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;width:20%;height:15px;">
                            COSTO
                        </td>
                        <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;width:20%;height:15px;">
                            VENTA
                        </td>
                    </tr>
                    <?php if (isset($venta_taller)): ?>
                        <?php if (count($venta_taller)>0): ?>
                            <?php foreach ($venta_taller as $index => $valor): ?>
                                <tr>
                                    <td align="center" style="border-bottom:1px solid #337ab7;height:15px;">
                                        <label for="" style="color:black;"><?php echo $venta_taller[$index][0]; ?></label>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;">
                                        <label for="" style="color:black;"><?php echo $venta_taller[$index][1]; ?></label>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;">
                                        <label for="" style="color:black;"><?php echo number_format($venta_taller[$index][2],2); ?></label>
                                    </td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;">
                                        <label for="" style="color:black;"><?php echo number_format($venta_taller[$index][3],2); ?></label disabled>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php for ($i = 0; $i<(11-count($venta_taller)-1) ; $i++): ?>
                                <tr>
                                    <td align="center" style="border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                </tr>
                            <?php endfor; ?>

                        <?php else: ?>
                            <?php for ($i = 0; $i<11 ; $i++): ?>
                                <tr>
                                    <td align="center" style="border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                    <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                </tr>
                            <?php endfor; ?>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php for ($i = 0; $i<11 ; $i++): ?>
                            <tr>
                                <td align="center" style="border-bottom:1px solid #337ab7;height:15px;"></td>
                                <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                                <td align="center" style="border-left:1px solid #337ab7;border-bottom:1px solid #337ab7;height:15px;"></td>
                            </tr>
                        <?php endfor; ?>
                    <?php endif; ?>
                    <tr>
                        <td colspan="3" align="right">
                            TOTALES
                        </td>
                        <td colspan="1" align="center">
                            <label for="" style="color:black;"><?php if(isset($totalTaller)) echo number_format($totalTaller,2); else echo "$0.00"; ?></label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</page>

<?php 
    use Spipu\Html2Pdf\Html2Pdf;

    //Orientación  de la hoja P->Vertical   L->Horizontal
    $html2pdf = new Html2Pdf('P', 'LEGAL', 'en',TRUE,'UTF-8',array(5,5,5,5));
    
    try {

        $html = ob_get_clean();

        ob_clean();
        /* Limpiamos la salida del búfer y lo desactivamos */
        //ob_end_clean();

        $html2pdf->setDefaultFont('Helvetica');     //Helvetica
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($html);
        $html2pdf->setTestTdInOnePage(FALSE);
        $html2pdf->Output();

    } catch (Html2PdfException $e) {
        $html2pdf->clean();
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }

?>