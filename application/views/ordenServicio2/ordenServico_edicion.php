<div class="container" style="color:#01018f;">
    <form name="" class="formulario" id="formulario" method="post" action="<?=base_url()."ordenServicio/OrdenServicioP2/validateForm"?>" autocomplete="on" enctype="multipart/form-data">

        <!-- Alerta para proceso del registro -->
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>

        <div class="row" style="margin-top:30px;">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-8">
                        <label style="font-size:12px;">No. Orden de Servicio: </label>
                        <input type="text" name="idOrden" id="ordenMaterial" onblur="ordenMaterialF()" class='input_field' style="font-size:12px;" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                        <br>

                        <br>
                        Folio Intelisis:&nbsp;
                        <label style="color:darkblue;">
                            <?php if(isset($idIntelisis)) echo $idIntelisis;  ?>
                        </label>
                    </div>
                    <div class="col-sm-4" align="right">
                        <?php if (isset($pOrigen)): ?>
                            <?php if ($pOrigen == "ASE"): ?>
                                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                    Regresar
                                </button>
                            <?php elseif ($pOrigen == "TEC"): ?>
                                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                    Regresar
                                </button>
                            <?php elseif ($pOrigen == "JDT"): ?>
                                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                    Regresar
                                </button>
                            <?php else: ?>
                                <button type="button" class="btn btn-dark" >
                                    Sesión Expirada
                                </button>
                            <?php endif; ?>
                        <?php else: ?>
                            <button type="button" class="btn btn-dark" >
                                Sesión Expirada
                            </button>
                        <?php endif; ?>
                    </div>
                </div>
                <br>

                <h4 style="color:#337ab7;" align="center">
                    CONTROL DE MATERIALES
                    <a id="superUsuarioA" class="h2_title_blue" data-target="#superUsuario" data-toggle="modal" style="color:blue;margin-top:15px;cursor: pointer;">
                        <i class="fas fa-user"></i>
                    </a>
                </h4>
                <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
                    <thead>
                        <tr>
                            <!-- <td style="width:11%;border-bottom:1px solid #337ab7;height:30px;" align="center">
                                REQ. No.
                            </td> -->
                            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                CANTIDAD
                            </td>
                            <td style="width:35%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <!-- OPERARIO -->
                                NUM. PIEZA
                            </td>
                            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <!-- DEPTO. -->
                                COSTO
                            </td>
                            <td style="width:10%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <!-- OPERARIO -->
                                HORAS
                            </td>
                            <td style="width:15%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                <!-- FECHA -->
                                TOTAL
                            </td>
                            <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                                AUTORIZÓ
                                <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($registrosControl)) if(count($registrosControl) > 0) echo count($registrosControl)+1; else echo "1"; else echo "1"; ?>">
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($registrosControl)): ?>
                            <!-- Comprobamos que haya registros guardados -->
                            <?php if (count($registrosControl)>0): ?>
                                <?php foreach ($registrosControl as $index => $valor): ?>
                                    <tr id='fila_<?php echo $index+1; ?>'>
                                        <!-- <td style='width:11%;border-bottom:1px solid #337ab7;height:30px;' align='center'>
                                            <input type='text' class='input_field' id='req_1' style='width:100%;text-align:left;'>
                                        </td> -->
                                        <td align='center'>
                                            <input type='text' class='input_field' onblur='cantidadCollet(<?php echo $index+1; ?>)' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $registrosControl[$index][0]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;' disabled>
                                        </td>
                                        <td style='border-left:1px solid #337ab7;' align='center'>
                                            <input type='text' class='input_field' onblur='descripcionCollect(<?php echo $index+1; ?>)' id='descripcion_<?php echo $index+1; ?>' value="<?php echo $registrosControl[$index][1]; ?>" style='width:90%;text-align:left;' disabled>
                                        </td>
                                        <td style='border-left:1px solid #337ab7;' align='center'>
                                            <input type='text' class='input_field' onblur='piezaCollet(<?php echo $index+1; ?>)' id='pieza_<?php echo $index+1; ?>'  value='<?php if(isset($registrosControl[$index][6])) echo $registrosControl[$index][6]; else echo ""; ?>' style='width:90%;text-align:left;' disabled>
                                        </td>
                                        <td style='border-left:1px solid #337ab7;' align='center'>
                                            $<input type='text' class='input_field' onblur='costoCollet(<?php echo $index+1; ?>)' id='costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][2]; ?>' style='width:90%;text-align:left;' disabled>
                                        </td>
                                        <td style='border-left:1px solid #337ab7;' align='center'>
                                            <input type='text' class='input_field' onblur='horasCollet(<?php echo $index+1; ?>)' id='horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][3]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <td style='border-left:1px solid #337ab7;' align='center'>
                                            $<input type='text' id='totalReng_<?php echo $index+1; ?>' onblur="autosuma(<?php echo $index+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $registrosControl[$index][4]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <td style='border-left:1px solid #337ab7;' align='center'>
                                            <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(<?php echo $index+1; ?>)' value="<?php echo $index+1; ?>" id='autorizo_<?php echo $index+1; ?>' <?php if($registrosControl[$index][5] == "SI") echo "checked"; ?>>
                                            <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $registrosControl[$index][0]."_".$registrosControl[$index][1]."_".$registrosControl[$index][2]."_".$registrosControl[$index][3]."_".$registrosControl[$index][4]."_".$registrosControl[$index][5]; ?>">
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <!-- Creamos una linea vacia para que puedna ingresar -->
                                <tr id='fila_<?php echo count($registrosControl)+1 ?>'>
                                    <!-- <td style='width:11%;border-bottom:1px solid #337ab7;height:30px;' align='center'>
                                        <input type='text' class='input_field' id='req_1' style='width:100%;text-align:left;'>
                                    </td> -->
                                    <td align='center'>
                                        <input type='text' class='input_field' onblur='cantidadCollet(<?php echo count($registrosControl)+1; ?>)' id='cantidad_<?php echo count($registrosControl)+1 ?>' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        <input type='text' class='input_field' onblur='descripcionCollect(<?php echo count($registrosControl)+1; ?>)' id='descripcion_<?php echo count($registrosControl)+1 ?>' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        <input type='text' class='input_field' onblur='piezaCollet(<?php echo count($registrosControl)+1; ?>)' id='pieza_<?php echo count($registrosControl)+1 ?>' onblur='piezaCollet(<?php echo count($registrosControl)+1 ?>)' value='' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        $<input type='text' class='input_field' onblur='costoCollet(<?php echo count($registrosControl)+1; ?>)' id='costoCM_<?php echo count($registrosControl)+1 ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        <input type='text' class='input_field' onblur='horasCollet(<?php echo count($registrosControl)+1; ?>)' id='horas_<?php echo count($registrosControl)+1 ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' onblur="autosuma(<?php echo count($registrosControl)+1; ?>)" align='center'>
                                        $<input type='text' id='totalReng_<?php echo count($registrosControl)+1 ?>' onblur="autosuma(<?php echo count($registrosControl)+1 ?>)" class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        <input type='checkbox' onclick='autorizaColet(<?php echo count($registrosControl)+1; ?>)' value="<?php echo count($registrosControl)+1 ?>" class='input_field autorizaCheck' id='autorizo_<?php echo count($registrosControl)+1 ?>' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                        <input type='hidden' id='valoresCM_<?php echo count($registrosControl)+1 ?>' name='registros[]'>
                                    </td>
                                </tr>
                            <?php else: ?>
                                <tr id='fila_1'>
                                    <!-- <td style='width:11%;border-bottom:1px solid #337ab7;height:30px;' align='center'>
                                        <input type='text' class='input_field' id='req_1' style='width:100%;text-align:left;'>
                                    </td> -->
                                    <td align='center'>
                                        <input type='text' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        <input type='text' class='input_field' id='descripcion_1' onblur='descripcionCollect(1)' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        <input type='text' class='input_field' id='pieza_1'  onblur='piezaCollet(1)' value='' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        $<input type='text' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        <input type='text' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        $<input type='text' id='totalReng_1' class='input_field'  onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;' align='center'>
                                        <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                        <input type='hidden' id='valoresCM_1' name='registros[]'>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr id='fila_1'>
                                <!-- <td style='width:11%;border-bottom:1px solid #337ab7;height:30px;' align='center'>
                                    <input type='text' class='input_field' id='req_1' style='width:100%;text-align:left;'>
                                </td> -->
                                <td align='center'>
                                    <input type='text' class='input_field' id='cantidad_1' onblur='cantidadCollet(1)' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                </td>
                                <td style='border-left:1px solid #337ab7;' align='center'>
                                    <input type='text' class='input_field' id='pieza_1' onblur='descripcionCollect(1)' value='' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border-left:1px solid #337ab7;' align='center'>
                                    <input type='text' class='input_field' id='descripcion_1' onblur='piezaCollet(1)' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border-left:1px solid #337ab7;' align='center'>
                                    $<input type='text' class='input_field' id='costoCM_1' onblur='costoCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border-left:1px solid #337ab7;' align='center'>
                                    <input type='text' class='input_field' id='horas_1' onblur='horasCollet(1)' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                </td>
                                <td style='border-left:1px solid #337ab7;' align='center'>
                                    $<input type='text' id='totalReng_1' class='input_field' onblur="autosuma(1)" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                </td>
                                <td style='border-left:1px solid #337ab7;' align='center'>
                                    <input type='checkbox' class='input_field autorizaCheck' onclick='autorizaColet(1)' value="1" id='autorizo_1' <?php if(isset($tipoRegistro)) if($tipoRegistro == "Tecnico") echo "disabled"; ?>>
                                    <input type='hidden' id='valoresCM_1' name='registros[]'>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6" align="right">
                                SUB-TOTAL
                            </td>
                            <td align="center">
                                $<label for="" id="subTotalMaterialLabel"><?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?></label>
                                <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subTotalMaterial)) echo number_format($subTotalMaterial,2); else echo "0"; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="right">
                                I.V.A.
                            </td>
                            <td align="center">
                                $<label for="" id="ivaMaterialLabel"><?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?></label>
                                <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($ivaMaterial)) echo number_format($ivaMaterial,2); else echo "0"; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="right">
                                TOTAL
                            </td>
                            <td align="center">
                                $<label for="" id="totalMaterialLabel"><?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?></label>
                                <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($totalMaterial)) echo number_format($totalMaterial,2); else echo "0"; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="right">
                                FIRMA DEL ASESOR QUE APRUEBA
                            </td>
                            <td align="center">
                              <?php if (isset($firmaAsesor)): ?>
                                  <?php if ($firmaAsesor != ""): ?>
                                      <img class='marcoImg' src='<?php echo base_url().$firmaAsesor ?>' id='' style='width:80px;height:30px;'>
                                      <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php echo $firmaAsesor ?>'>
                                  <?php else: ?>
                                      <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php echo set_value("rutaFirmaConsumidor"); ?>'>
                                      <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                                      <a class='cuadroFirma btn btn-primary' data-value="AsesorCostos" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                          <i class='fas fa-pen-fancy'></i>
                                      </a>
                                  <?php endif; ?>
                              <?php else: ?>
                                  <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php echo set_value("rutaFirmaConsumidor"); ?>'>
                                  <img class='marcoImg' src='<?php echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>' id='firmaAsesorCostos' style='width:80px;height:30px;'>
                                  <a class='cuadroFirma btn btn-primary' data-value="AsesorCostos" data-target="#firmaDigital" data-toggle="modal" style="color:white;">
                                      <i class='fas fa-pen-fancy'></i>
                                  </a>
                              <?php endif; ?>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <br>
        <div class="row" style="margin-top:30px;">
            <div class="col-sm-12">
                <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;">
                    <tr>
                        <td align="center" style="border-bottom:1px solid #337ab7;">
                            DÍA
                        </td>
                        <?php for ($i = 0; $i<31 ; $i++): ?>
                          <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;">
                              <?php echo $i+1; ?>
                          </td>
                        <?php endfor; ?>
                    </tr>
                    <tr>
                        <td align="center" style="border-bottom:1px solid #337ab7;">
                            PROG
                        </td>
                        <?php if (isset($diaProg)): ?>
                            <!-- Verificamos que se haya cargado la información -->
                            <?php for ($i = 0; $i<count($diaProg)-1 ; $i++): ?>
                              <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;">
                                  <input type="text" style="width:1cm;" name="prog_[]" value="<?php echo $diaProg[$i]; ?>">
                              </td>
                            <?php endfor; ?>
                        <?php else: ?>
                            <!-- Si no se cargo la variable -->
                            <?php for ($i = 0; $i<31 ; $i++): ?>
                              <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;">
                                  <input type="text" style="width:1cm;" name="prog_[]" value="">
                              </td>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td align="center" style="border-bottom:1px solid #337ab7;">
                            REAL
                        </td>
                        <?php if (isset($diaReal)): ?>
                            <!-- Verificamos que se haya cargado la información -->
                            <?php for ($i = 0; $i<count($diaReal)-1 ; $i++): ?>
                              <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;">
                                  <input type="text" style="width:1cm;" name="real_[]" value="<?php echo $diaReal[$i]; ?>">
                              </td>
                            <?php endfor; ?>
                        <?php else: ?>
                            <!-- Si no se cargo la variable -->
                            <?php for ($i = 0; $i<31 ; $i++): ?>
                              <td align="center" style="border-bottom:1px solid #337ab7;border-left: 1px solid #337ab7;">
                                  <input type="text" style="width:1cm;" name="real_[]" value="">
                              </td>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td colspan="32" align="center" style="border-bottom:1px solid #337ab7;">
                            A = X AUTORIZAR, M= MECÁNICA, L= LAMINADO, PR = PREPARACIÓN, P = PINTURA, AR=ARMADO, D = DETALLADO
                        </td>
                    </tr>
                    <tr>
                        <td colspan="32" align="center">
                            Lo,ARo= GRUPO ORO; Lp,ARp=GRUPO PLATA; Pn,PRn,Dn=GRUPO NARANJA; Pb=GRUPO BLANCO
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row" style="margin-top:30px;">
            <div class="col-sm-12">
                <h4 style="color:#337ab7;" align="center">
                    AUTOCERTIFICACIÓN Y PAGO
                </h4>
                <table style="width:100%;border-radius: 4px;">
                    <tr>
                        <td align="center"style="height:40px;">
                        </td>
                        <td align="center">
                            FECHA DE RECEPCIÓN
                        </td>
                        <td align="center" colspan="2">
                            FECHA Y FIRMA OPERARIO
                        </td>
                        <td align="center" colspan="2">
                            FECHA Y FIRMA LÍDER
                        </td>
                        <td align="center" colspan="2">
                            FECHA Y FIRMA COORDINADOR
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height:30px;">
                            MECÁNICA
                        </td>
                        <td align="center">
                            <?php if (isset($firma_mecaOpera)): ?>
                                <?php if (($firma_mecaOpera != "")||($firma_mecaLider != "")||($firma_mecaCoord != "")): ?>
                                    <label for=""><?php if(isset($fech_recepMeca)) echo $fech_recepMeca; ?></label>
                                    <input type="hidden" name="recepMeca" value="<?php if(isset($fech_recepMecaInput)) echo $fech_recepMecaInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="recepMeca" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php echo $fech_recepMeca; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="recepMeca" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>

                        <td align="center">
                            <?php if (isset($firma_mecaOpera)): ?>
                                <?php if (($firma_mecaOpera != "")): ?>
                                    <label for=""><?php if(isset($fech_mecaOpera)) echo $fech_mecaOpera; ?></label>
                                    <input type="hidden" name="fechaMecaOperario" value="<?php if(isset($fech_mecaOperaInput)) echo $fech_mecaOperaInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaMecaOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php echo $fech_mecaOpera; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaMecaOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_mecaOpera)): ?>
                                <?php if ($firma_mecaOpera != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_mecaOpera; ?>" id="" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaMecaOperario" name="rutaMecaOperario" value="<?php echo $firma_mecaOpera; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaMecaOperario" name="rutaMecaOperario" value="<?php echo set_value("rutaMecaOperario") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaMecaOperario") != "") echo set_value("rutaMecaOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaMecaOperario" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="MecaOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaMecaOperario" name="rutaMecaOperario" value="<?php echo set_value("rutaMecaOperario") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaMecaOperario") != "") echo set_value("rutaMecaOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaMecaOperario" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="MecaOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>

                        <td align="center">
                            <!-- <input type='date' name="fechamMecaLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'> -->
                            <?php if (isset($firma_mecaLider)): ?>
                                <?php if (($firma_mecaLider != "")): ?>
                                    <label for=""><?php if(isset($fech_mecaLider)) echo $fech_mecaLider; ?></label>
                                    <input type="hidden" name="fechamMecaLider" value="<?php if(isset($fech_mecaLiderInput)) echo $fech_mecaLiderInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechamMecaLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechamMecaLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_mecaLider)): ?>
                                <?php if ($firma_mecaLider != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_mecaLider; ?>" id="" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaMecaLider" name="rutaMecaLider" value="<?php echo $firma_mecaLider; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaMecaLider" name="rutaMecaLider" value="<?php echo set_value("rutaMecaLider") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaMecaLider") != "") echo set_value("rutaMecaLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaMecaLider" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="MecaLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaMecaLider" name="rutaMecaLider" value="<?php echo set_value("rutaMecaLider") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaMecaLider") != "") echo set_value("rutaMecaLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaMecaLider" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="MecaLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>

                        <td align="center">
                            <!-- <input type='date' name="fechaMecaCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'> -->
                            <?php if (isset($firma_mecaCoord)): ?>
                                <?php if (($firma_mecaCoord != "")): ?>
                                    <label for=""><?php if(isset($fech_mecaCoord)) echo $fech_mecaCoord; ?></label>
                                    <input type="hidden" name="fechaMecaCoord" value="<?php echo $fech_mecaCoordInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaMecaCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaMecaCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                          <?php if (isset($firma_mecaCoord)): ?>
                              <?php if ($firma_mecaCoord != ""): ?>
                                  <img class="marcoImg" src="<?php echo base_url().$firma_mecaCoord; ?>" id="" alt="" style="width:80px;height:30px;">
                                  <input type="hidden" id="rutaMecaCoord" name="rutaMecaCoord" value="<?php echo $firma_mecaCoord; ?>">
                              <?php else: ?>
                                  <input type="hidden" id="rutaMecaCoord" name="rutaMecaCoord" value="<?php echo set_value("rutaMecaCoord") ?>">
                                  <img class="marcoImg" src="<?php if(set_value("rutaMecaCoord") != "") echo set_value("rutaMecaCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaMecaCoord" alt="" style="width:80px;height:30px;">
                                  <a class="cuadroFirma btn btn-info" data-value="MecaCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                              <?php endif; ?>
                          <?php else: ?>
                              <input type="hidden" id="rutaMecaCoord" name="rutaMecaCoord" value="<?php echo set_value("rutaMecaCoord") ?>">
                              <img class="marcoImg" src="<?php if(set_value("rutaMecaCoord") != "") echo set_value("rutaMecaCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaMecaCoord" alt="" style="width:80px;height:30px;">
                              <a class="cuadroFirma btn btn-info" data-value="MecaCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                          <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height:30px;">
                            LAMINADO
                        </td>
                        <td align="center">
                            <?php if (isset($firma_lamiOpera)): ?>
                                <?php if (($firma_lamiOpera != "")||($firma_lamiLider != "")||($firma_lamiCoord != "")): ?>
                                    <label for=""><?php if(isset($fech_recepLami)) echo $fech_recepLami; ?></label>
                                    <input type="hidden" name="recepLami" value="<?php echo $fech_recepLamiInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="recepLami" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="recepLami" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                              <?php if (isset($firma_lamiOpera)): ?>
                                  <?php if ($firma_lamiOpera != ""): ?>
                                      <label for=""><?php if(isset($fech_lamiOpera)) echo $fech_lamiOpera; ?></label>
                                      <input type="hidden" name="fechaLamiOperario" value="<?php echo $fech_lamiOperaInput; ?>">
                                  <?php else: ?>
                                      <input type='date' name="fechaLamiOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                  <?php endif; ?>
                              <?php else: ?>
                                  <input type='date' name="fechaLamiOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                              <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_lamiOpera)): ?>
                                <?php if (($firma_lamiOpera != "")): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_lamiOpera; ?>" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaLamiOperario" name="rutaLamiOperario" value="<?php echo $firma_lamiOpera; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaLamiOperario" name="rutaLamiOperario" value="<?php echo set_value("rutaLamiOperario") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaLamiOperario") != "") echo set_value("rutaLamiOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaLamiOperario" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="LamiOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaLamiOperario" name="rutaLamiOperario" value="<?php echo set_value("rutaLamiOperario") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaLamiOperario") != "") echo set_value("rutaLamiOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaLamiOperario" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="LamiOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_lamiLider)): ?>
                                <?php if ($firma_lamiLider != ""): ?>
                                    <label for=""><?php if(isset($fech_lamiLider)) echo $fech_lamiLider; ?></label>
                                    <input type="hidden" name="fechaLamiLider" value="<?php echo $fech_lamiLiderInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaLamiLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaLamiLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_lamiLider)): ?>
                                <?php if ($firma_lamiLider != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_lamiLider; ?>" id="" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaLamiLider" name="rutaLamiLider" value="<?php echo $firma_lamiLider; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaLamiLider" name="rutaLamiLider" value="<?php echo set_value("rutaLamiLider") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaLamiLider") != "") echo set_value("rutaLamiLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaLamiLider" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="LamiLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaLamiLider" name="rutaLamiLider" value="<?php echo set_value("rutaLamiLider") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaLamiLider") != "") echo set_value("rutaLamiLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaLamiLider" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="LamiLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>

                        <td align="center">
                            <?php if (isset($firma_lamiCoord)): ?>
                                <?php if ($firma_lamiCoord != ""): ?>
                                    <label for=""><?php if(isset($fech_lamiCoord)) echo $fech_lamiCoord; ?></label>
                                    <input type="hidden" name="fechaLamiCoord" value="<?php echo $fech_lamiCoordInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaLamiCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaLamiCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_lamiCoord)): ?>
                                <?php if ($firma_lamiCoord != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_lamiCoord; ?>" id="" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaLamiCoord" name="rutaLamiCoord" value="<?php echo $firma_lamiCoord; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaLamiCoord" name="rutaLamiCoord" value="<?php echo set_value("rutaLamiCoord") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaLamiCoord") != "") echo set_value("rutaLamiCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaLamiCoord" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="LamiCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaLamiCoord" name="rutaLamiCoord" value="<?php echo set_value("rutaLamiCoord") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaLamiCoord") != "") echo set_value("rutaLamiCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaLamiCoord" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="LamiCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height:30px;">
                            PREPARACIÓN
                        </td>
                        <td align="center">
                            <?php if (isset($firma_prepOpera)): ?>
                                <?php if (($firma_prepOpera != "")||($firma_prepLider != "")||($firma_prepCoord != "")): ?>
                                    <label for=""><?php if(isset($fech_recepPrep)) echo $fech_recepPrep; ?></label>
                                    <input type="hidden" name="recepPrep" value="<?php echo $fech_recepPrepInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="recepPrep" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="recepPrep" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_prepOpera)): ?>
                                <?php if ($firma_prepOpera != ""): ?>
                                    <label for=""><?php echo $fech_prepOpera; ?></label>
                                    <input type="hidden" name="fechaPrepOperario" value="<?php echo $fech_prepOperaInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaPrepOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaPrepOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_prepOpera)): ?>
                                <?php if ($firma_prepOpera != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_prepOpera; ?>" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaPrepOperario" name="rutaPrepOperario" value="<?php echo $firma_prepOpera; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaPrepOperario" name="rutaPrepOperario" value="<?php echo set_value("rutaPrepOperario") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaPrepOperario") != "") echo set_value("rutaPrepOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPrepOperario" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="PrepOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaPrepOperario" name="rutaPrepOperario" value="<?php echo set_value("rutaPrepOperario") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaPrepOperario") != "") echo set_value("rutaPrepOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPrepOperario" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="PrepOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_prepLider)): ?>
                                <?php if ($firma_prepLider != ""): ?>
                                    <label for=""><?php echo $fech_prepLider; ?></label>
                                    <input type="hidden" name="fechaPrepLider" value="<?php echo $fech_prepLiderInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaPrepLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaPrepLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_prepLider)): ?>
                                <?php if ($firma_prepLider != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_prepLider; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaPrepLider" name="rutaPrepLider" value="<?php echo $firma_prepLider; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaPrepLider" name="rutaPrepLider" value="<?php echo set_value("rutaPrepLider") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaPrepLider") != "") echo set_value("rutaPrepLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPrepLider" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="PrepLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaPrepLider" name="rutaPrepLider" value="<?php echo set_value("rutaPrepLider") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaPrepLider") != "") echo set_value("rutaPrepLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPrepLider" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="PrepLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_prepCoord)): ?>
                                <?php if ($firma_prepCoord != ""): ?>
                                    <label for=""><?php echo $fech_prepCoord; ?></label>
                                    <input type="hidden" name="fechaPrepCoord" value="<?php echo $fech_prepCoordInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaPrepCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaPrepCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_prepCoord)): ?>
                                <?php if ($firma_prepCoord != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_prepCoord; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaPrepCoord" name="rutaPrepCoord" value="<?php echo $firma_prepCoord; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaPrepCoord" name="rutaPrepCoord" value="<?php echo set_value("rutaPrepCoord") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaPrepCoord") != "") echo set_value("rutaPrepCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPrepCoord" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="PrepCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaPrepCoord" name="rutaPrepCoord" value="<?php echo set_value("rutaPrepCoord") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaPrepCoord") != "") echo set_value("rutaPrepCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPrepCoord" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="PrepCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height:30px;">
                            PINTURA
                        </td>
                        <td align="center">
                            <?php if (isset($firma_pintOpera)): ?>
                                <?php if (($firma_pintOpera != "")||($firma_pintLider != "")||($firma_pintCoord != "")): ?>
                                    <label for=""><?php if(isset($fech_recepPint)) echo $fech_recepPint; ?></label>
                                    <input type="hidden" name="recepPint" value="<?php echo $fech_recepPintInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="recepPint" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="recepPint" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_pintOpera)): ?>
                                <?php if ($firma_pintOpera != ""): ?>
                                    <label for=""><?php if(isset($fech_pintOpera)) echo $fech_pintOpera ?></label>
                                    <input type="hidden" name="fechaPintOperario" value="<?php echo $fech_pintOperaInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaPintOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaPintOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_pintOpera)): ?>
                                <?php if ($firma_pintOpera != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_pintOpera; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaPintOperario" name="rutaPintOperario" value="<?php echo $firma_pintOpera; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaPintOperario" name="rutaPintOperario" value="<?php echo set_value("rutaPintOperario") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaPintOperario") != "") echo set_value("rutaPintOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPintOperario" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="PintOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaPintOperario" name="rutaPintOperario" value="<?php echo set_value("rutaPintOperario") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaPintOperario") != "") echo set_value("rutaPintOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPintOperario" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="PintOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_pintLider)): ?>
                                <?php if ($firma_pintLider != ""): ?>
                                    <label for=""><?php if(isset($fech_pintLider)) echo $fech_pintLider; ?></label>
                                    <input type="hidden" name="fechaPintLider" value="<?php echo $fech_pintLiderInput ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaPintLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaPintLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_pintLider)): ?>
                                <?php if ($firma_pintLider != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_pintLider; ?>" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaPintLider" name="rutaPintLider" value="<?php echo $firma_pintLider; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaPintLider" name="rutaPintLider" value="<?php echo set_value("rutaPintLider") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaPintLider") != "") echo set_value("rutaPintLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPintLider" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="PintLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaPintLider" name="rutaPintLider" value="">
                                <img class="marcoImg" src="<?php if(set_value("rutaPintLider") != "") echo set_value("rutaPintLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPintLider" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="PintLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_pintCoord)): ?>
                                <?php if ($firma_pintCoord != ""): ?>
                                    <label for=""><?php if(isset($fech_pintCoord)) echo $fech_pintCoord; ?></label>
                                    <input type="hidden" name="fechaPintCoord" value="<?php echo $fech_pintCoordInput ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaPintCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaPintCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_pintCoord)): ?>
                                <?php if ($firma_pintCoord != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_pintCoord; ?>" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaPintCoord" name="rutaPintCoord" value="<?php echo $firma_pintCoord; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaPintCoord" name="rutaPintCoord" value="<?php echo set_value("rutaPintCoord") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaPintCoord") != "") echo set_value("rutaPintCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPintCoord" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="PintCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaPintCoord" name="rutaPintCoord" value="<?php echo set_value("rutaPintCoord") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaPintCoord") != "") echo set_value("rutaPintCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaPintCoord" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="PintCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height:30px;">
                            ARMADO
                        </td>
                        <td align="center">
                            <?php if (isset($firma_armaOpera)): ?>
                                <?php if (($firma_armaOpera != "")||($firma_armaLider != "")||($firma_armaCoord != "")): ?>
                                    <label for=""><?php if(isset($fech_recepArma)) echo $fech_recepArma; ?></label>
                                    <input type="hidden" name="recepArma" value="<?php echo $fech_recepArmaInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="recepArma" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="recepArma" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_armaOpera)): ?>
                                <?php if ($firma_armaOpera != ""): ?>
                                    <label for=""><?php echo $fech_armaOpera; ?></label>
                                    <input type="hidden" name="fechaArmaOperario" value="<?php echo $fech_armaOperaInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaArmaOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaArmaOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_armaOpera)): ?>
                                <?php if ($firma_armaOpera != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_armaOpera; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaArmaOperario" name="rutaArmaOperario" value="<?php echo $firma_armaOpera; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaArmaOperario" name="rutaArmaOperario" value="<?php echo set_value("rutaArmaOperario") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaArmaOperario") != "") echo set_value("rutaArmaOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaArmaOperario" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="ArmaOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaArmaOperario" name="rutaArmaOperario" value="<?php echo set_value("rutaArmaOperario") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaArmaOperario") != "") echo set_value("rutaArmaOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaArmaOperario" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="ArmaOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_armaLider)): ?>
                                <?php if ($firma_armaLider != ""): ?>
                                    <label for=""><?php echo $fech_armaLider; ?></label>
                                    <input type="hidden" name="fechaArmaLider" value="<?php echo $fech_armaLiderInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaArmaLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaArmaLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_armaLider)): ?>
                                <?php if ($firma_armaLider != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_armaLider; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaArmaLider" name="rutaArmaLider" value="<?php echo $firma_armaLider; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaArmaLider" name="rutaArmaLider" value="<?php echo set_value("rutaArmaLider") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaArmaLider") != "") echo set_value("rutaArmaLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaArmaLider" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="ArmaLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaArmaLider" name="rutaArmaLider" value="<?php echo set_value("rutaArmaLider") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaArmaLider") != "") echo set_value("rutaArmaLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaArmaLider" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="ArmaLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_armaCoord)): ?>
                                <?php if ($firma_armaCoord != ""): ?>
                                    <label for=""><?php echo $fech_armaCoord; ?></label>
                                    <input type="hidden" name="fechaArmaCoord" value="<?php echo $fech_armaCoordInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaArmaCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaArmaCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_armaCoord)): ?>
                                <?php if ($firma_armaCoord != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_armaCoord; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaArmaCoord" name="rutaArmaCoord" value="<?php echo $firma_armaCoord; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaArmaCoord" name="rutaArmaCoord" value="<?php echo set_value("rutaArmaCoord") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaArmaCoord") != "") echo set_value("rutaArmaCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaArmaCoord" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="ArmaCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaArmaCoord" name="rutaArmaCoord" value="<?php echo set_value("rutaArmaCoord") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaArmaCoord") != "") echo set_value("rutaArmaCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaArmaCoord" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="ArmaCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height:30px;">
                            DETALLADO
                        </td>
                        <td align="center">
                            <?php if (isset($firma_detallOpera)): ?>
                                <?php if (($firma_detallOpera != "")||($firma_detallLider != "")||($firma_detallCoord != "")): ?>
                                    <label for=""><?php if(isset($fech_recepDetall)) echo $fech_recepDetall; ?></label>
                                    <input type="hidden" name="recepDetall" value="<?php echo $fech_recepDetallInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="recepDetall" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="recepDetall" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_detallOpera)): ?>
                                <?php if ($firma_detallOpera != ""): ?>
                                    <label for=""><?php echo $fech_detallOpera; ?></label>
                                    <input type="hidden" name="fechaDetallOperario" value="<?php echo $fech_detallOperaInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaDetallOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaDetallOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_detallOpera)): ?>
                                <?php if ($firma_detallOpera != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_detallOpera; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaDetallOperario" name="rutaDetallOperario" value="<?php echo $firma_detallOpera; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaDetallOperario" name="rutaDetallOperario" value="<?php echo set_value("rutaDetallOperario") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaDetallOperario") != "") echo set_value("rutaDetallOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaDetallOperario" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="DetallOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaDetallOperario" name="rutaDetallOperario" value="<?php echo set_value("rutaDetallOperario") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaDetallOperario") != "") echo set_value("rutaDetallOperario"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaDetallOperario" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="DetallOperario" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_detallLider)): ?>
                                <?php if ($firma_detallLider != ""): ?>
                                    <label for=""><?php echo $fech_detallLider; ?></label>
                                    <input type="hidden" name="fechaDetallLider" value="<?php echo $fech_detallLiderInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaDetallLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaDetallLider" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_detallLider)): ?>
                                <?php if ($firma_detallLider != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_detallLider; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaDetallLider" name="rutaDetallLider" value="<?php echo $firma_detallLider; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaDetallLider" name="rutaDetallLider" value="<?php echo set_value("rutaDetallLider") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaDetallLider") != "") echo set_value("rutaDetallLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaDetallLider" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="DetallLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaDetallLider" name="rutaDetallLider" value="<?php echo set_value("rutaDetallLider") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaDetallLider") != "") echo set_value("rutaDetallLider"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaDetallLider" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="DetallLider" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_detallCoord)): ?>
                                <?php if ($firma_detallCoord != ""): ?>
                                  <label for=""><?php echo $fech_detallCoord; ?></label>
                                  <input type="hidden" name="fechaDetallCoord" value="<?php echo $fech_detallCoordInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaDetallCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaDetallCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_detallCoord)): ?>
                                <?php if ($firma_detallCoord != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_detallCoord; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaDetallCoord" name="rutaDetallCoord" value="<?php echo $firma_detallCoord; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaDetallCoord" name="rutaDetallCoord" value="<?php echo set_value("rutaDetallCoord") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaDetallCoord") != "") echo set_value("rutaDetallCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaDetallCoord" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="DetallCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaDetallCoord" name="rutaDetallCoord" value="<?php echo set_value("rutaDetallCoord") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaDetallCoord") != "") echo set_value("rutaDetallCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaDetallCoord" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="DetallCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height:30px;">
                            C. DE CALIDAD
                        </td>
                        <td align="center">
                            <!-- <input type='date' name="recepCali" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'> -->
                        </td>
                        <td align="center" colspan="2">
                            <!-- <input type='date' name="fechaCaliOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'> -->
                        </td>
                        <td align="center" colspan="2">
                            <!-- <input type='date' name="fechaCaliOperario" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'> -->
                        </td>
                        <td align="center">
                            <?php if (isset($firma_calidCoord)): ?>
                                <?php if ($firma_calidCoord != ""): ?>
                                    <label for=""><?php echo $fech_calidCoord; ?></label>
                                    <input type="hidden" name="fechaCaliCoord" value="<?php echo $fech_calidCoordInput; ?>">
                                <?php else: ?>
                                    <input type='date' name="fechaCaliCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type='date' name="fechaCaliCoord" class='input_field' min='<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>' value="<?php if(isset($hoy)) echo $hoy; else echo "2019-01-01"; ?>" style='width:90%;border-bottom:1px solid #337ab7;'>
                            <?php endif; ?>
                        </td>
                        <td align="center">
                            <?php if (isset($firma_calidCoord)): ?>
                                <?php if ($firma_calidCoord != ""): ?>
                                    <img class="marcoImg" src="<?php echo base_url().$firma_calidCoord; ?>" alt="" style="width:80px;height:30px;">
                                    <input type="hidden" id="rutaCaliCoord" name="rutaCaliCoord" value="<?php echo $firma_calidCoord; ?>">
                                <?php else: ?>
                                    <input type="hidden" id="rutaCaliCoord" name="rutaCaliCoord" value="<?php echo set_value("rutaCaliCoord") ?>">
                                    <img class="marcoImg" src="<?php if(set_value("rutaCaliCoord") != "") echo set_value("rutaCaliCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaCaliCoord" alt="" style="width:80px;height:30px;">
                                    <a class="cuadroFirma btn btn-info" data-value="CaliCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaCaliCoord" name="rutaCaliCoord" value="<?php echo set_value("rutaCaliCoord") ?>">
                                <img class="marcoImg" src="<?php if(set_value("rutaCaliCoord") != "") echo set_value("rutaCaliCoord"); else echo base_url().'assets/imgs/fondo_bco.jpeg'; ?>" id="firmaCaliCoord" alt="" style="width:80px;height:30px;">
                                <a class="cuadroFirma btn btn-info" data-value="CaliCoord" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> <i class="fas fa-pen-fancy"></i> </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <br>
        <div class="row" style="margin-top:30px;">
            <div class="col-md-6">
                <table class="table-responsive" style="border: 1px solid #337ab7;border-radius: 4px;">
                    <thead>
                        <tr style="border:1px solid #337ab7;">
                            <td align="center">
                                O.C.
                            </td>
                            <td align="center" style="border-left:1px solid #337ab7;">
                                GASOLINA Y LUBRICANTE
                            </td>
                            <td align="center" style="border-left:1px solid #337ab7;">
                                COSTO
                            </td>
                            <td align="center" style="border-left:1px solid #337ab7;">
                                VENTA
                                <input type="hidden" id="indiceGasolina" value="<?php if(isset($costos)) echo count($costos)+1; else echo "1"; ?>">
                            </td>
                            <td align="center">
                                <a id="agregarGL" class="btn btn-success" style="color:white;">
                                    +
                                </a>
                            </td>
                            <td align="center">
                                <a id="eliminarGL" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                                    -
                                </a>
                            </td>
                        </tr>
                    </thead>
                    <tbody id="tabla_gasolina">
                        <?php if (isset($costos)): ?>
                            <?php if (count($costos)>0): ?>
                                <?php foreach ($costos as $index => $valor): ?>
                                    <tr id="fila_Gasolina_<?php echo $index+1; ?>">
                                        <td align="center">
                                            <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_<?php echo $index+1; ?>' value='<?php echo $costos[$index][0]; ?>' style='width:1cm;' disabled>
                                        </td>
                                        <td style='border-left:1px solid #337ab7;width:50%;'>
                                            <input type='text' class='input_field' id='gasolina_<?php echo $index+1; ?>' value='<?php echo $costos[$index][1]; ?>' style='width:100%;' disabled>
                                        </td>
                                        <td align="center" style='border-left:1px solid #337ab7;'>
                                            <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_<?php echo $index+1; ?>' value='<?php echo number_format($costos[$index][2],2); ?>' style='width:2cm;' disabled>
                                        </td>
                                        <td align="center" colspan='3' style='border-left:1px solid #337ab7;'>
                                            <label for="" id='venta_<?php echo $index+1; ?>'><?php echo number_format($costos[$index][3],2); ?></label disabled>
                                            <input type='hidden' class='input_field' id='ventaValor_<?php echo $index+1; ?>' name='costos[]' value="<?php echo $costos[$index][0]."_".$costos[$index][1]."_".$costos[$index][2]."_".$costos[$index][3]; ?>">
                                            <input type='hidden' id='totalRenglon_<?php echo $index+1; ?>' value='<?php echo $costos[$index][3]; ?>'>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr id="fila_Gasolina_<?php echo count($costos)+1; ?>">
                                    <td align="center">
                                        <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_<?php echo count($costos)+1; ?>' value='1' style='width:1cm;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;width:50%;'>
                                        <input type='text' class='input_field' id='gasolina_<?php echo count($costos)+1; ?>' value='' style='width:100%;'>
                                    </td>
                                    <td align="center" style='border-left:1px solid #337ab7;'>
                                        <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_<?php echo count($costos)+1; ?>' value='0' style='width:2cm;'>
                                    </td>
                                    <td align="center" colspan='3' style='border-left:1px solid #337ab7;'>
                                        <label for=""id='venta_<?php echo count($costos)+1; ?>'>$ 0</label>
                                        <input type='hidden' class='input_field' id='ventaValor_<?php echo count($costos)+1; ?>' name='costos[]'>
                                        <input type='hidden' id='totalRenglon_<?php echo count($costos)+1; ?>' value='0'>
                                    </td>
                                </tr>
                            <?php else: ?>
                                <tr id="fila_Gasolina_1">
                                    <td align="center">
                                        <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_1' value='1' style='width:1cm;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;width:50%;'>
                                        <input type='text' class='input_field' id='gasolina_1' value='' style='width:100%;'>
                                    </td>
                                    <td align="center" style='border-left:1px solid #337ab7;'>
                                        <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_1' value='0' style='width:2cm;'>
                                    </td>
                                    <td align="center" colspan='3' style='border-left:1px solid #337ab7;'>
                                        <label for=""id='venta_1'>$ 0</label>
                                        <input type='hidden' class='input_field' id='ventaValor_1' name='costos[]'>
                                        <input type='hidden' id='totalRenglon_1' value='0'>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr id="fila_Gasolina_1">
                                <td align="center">
                                    <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_1' value='1' style='width:1cm;'>
                                </td>
                                <td style='border-left:1px solid #337ab7;width:50%;'>
                                    <input type='text' class='input_field' id='gasolina_1' value='' style='width:100%;'>
                                </td>
                                <td align="center" style='border-left:1px solid #337ab7;'>
                                    <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_1' value='0' style='width:2cm;'>
                                </td>
                                <td align="center" colspan='3' style='border-left:1px solid #337ab7;'>
                                    <label for=""id='venta_1'>$ 0</label>
                                    <input type='hidden' class='input_field' id='ventaValor_1' name='costos[]'>
                                    <input type='hidden' id='totalRenglon_1' value='0'>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2" align="right">
                                TOTALES
                            </td>
                            <td colspan="2" align="center">
                                <label for="" id="totalesGasolinaLabel"><?php if(isset($totalesGasolina)) echo number_format($totalesGasolina,2); else echo "$0.00"; ?></label>
                                <input type="hidden" name="totalesGasolina" id="totalesGasolinaGral" value="<?php if(isset($totalesGasolina)) echo $totalesGasolina; else echo "0.00"; ?>">
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="col-md-6">
                <table class="table-responsive" style="border: 1px solid #337ab7;border-radius: 4px;">
                    <thead>
                        <tr style="border:1px solid #337ab7;">
                            <td align="center">
                                O.C.
                            </td>
                            <td align="center" style="border-left:1px solid #337ab7;">
                                TRABAJO FUERA TALLER
                            </td>
                            <td align="center" style="border-left:1px solid #337ab7;">
                                COSTO
                            </td>
                            <td align="center" style="border-left:1px solid #337ab7;">
                                VENTA
                                <input type="hidden" id="indiceTaller" value="<?php if(isset($venta_taller)) echo count($venta_taller)+1; else echo "1"; ?>">
                            </td>
                            <td align="center">
                                <a id="agregarFDT" class="btn btn-success" style="color:white;">
                                    +
                                </a>
                            </td>
                            <td align="center">
                                <a id="eliminarFDT" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                                    -
                                </a>
                            </td>
                        </tr>
                    </thead>
                    <tbody id="tabla_taller">
                        <?php if (isset($venta_taller)): ?>
                            <?php if (count($venta_taller)>0): ?>
                                <?php foreach ($venta_taller as $index => $valor): ?>
                                    <tr id="fila_taller_<?php echo $index+1; ?>">
                                        <td align="center">
                                            <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_taller_<?php echo $index+1; ?>' value='<?php echo $venta_taller[$index][0]; ?>' style='width:1cm;'>
                                        </td>
                                        <td style='border-left:1px solid #337ab7;width:50%;'>
                                            <input type='text' class='input_field' id='taller_<?php echo $index+1; ?>' value='<?php echo $venta_taller[$index][1]; ?>' style='width:100%;'>
                                        </td>
                                        <td align="center" style='border-left:1px solid #337ab7;'>
                                            <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_taller_<?php echo $index+1; ?>' value='<?php echo number_format($venta_taller[$index][2],2); ?>' style='width:2cm;'>
                                        </td>
                                        <td align="center" colspan='3' style='border-left:1px solid #337ab7;'>
                                            <label for=""id='venta_taller_<?php echo $index+1; ?>'><?php echo number_format($venta_taller[$index][3],2); ?></label>
                                            <input type='hidden' class='input_field' id='ventaValor_taller_<?php echo $index+1; ?>' name='venta_taller[]' value="<?php echo $venta_taller[$index][0]."_".$venta_taller[$index][1]."_".$venta_taller[$index][2]."_".$venta_taller[$index][3]; ?>">
                                            <input type='hidden' id='totalRenglon_taller_<?php echo $index+1; ?>' value='<?php echo $venta_taller[$index][3]; ?>'>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr id="fila_taller_<?php echo count($venta_taller)+1; ?>">
                                    <td align="center">
                                        <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_taller_<?php echo count($venta_taller)+1; ?>' value='1' style='width:1cm;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;width:50%;'>
                                        <input type='text' class='input_field' id='taller_<?php echo count($venta_taller)+1; ?>' value='' style='width:100%;'>
                                    </td>
                                    <td align="center" style='border-left:1px solid #337ab7;'>
                                        <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_taller_<?php echo count($venta_taller)+1; ?>' value='0' style='width:2cm;'>
                                    </td>
                                    <td align="center" colspan='3' style='border-left:1px solid #337ab7;'>
                                        <label for=""id='venta_taller_<?php echo count($venta_taller)+1; ?>'>$ 0</label>
                                        <input type='hidden' class='input_field' id='ventaValor_taller_<?php echo count($venta_taller)+1; ?>' name='venta_taller[]'>
                                        <input type='hidden' id='totalRenglon_taller_<?php echo count($venta_taller)+1; ?>' value='0'>
                                    </td>
                                </tr>
                            <?php else: ?>
                                <tr id="fila_taller_1">
                                    <td align="center">
                                        <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_taller_1' value='1' style='width:1cm;'>
                                    </td>
                                    <td style='border-left:1px solid #337ab7;width:50%;'>
                                        <input type='text' class='input_field' id='taller_1' value='' style='width:100%;'>
                                    </td>
                                    <td align="center" style='border-left:1px solid #337ab7;'>
                                        <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_taller_1' value='0' style='width:2cm;'>
                                    </td>
                                    <td align="center" colspan='3' style='border-left:1px solid #337ab7;'>
                                        <label for=""id='venta_taller_1'>$ 0</label>
                                        <input type='hidden' class='input_field' id='ventaValor_taller_1' name='venta_taller[]'>
                                        <input type='hidden' id='totalRenglon_taller_1' value='0'>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr id="fila_taller_1">
                                <td align="center">
                                    <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='oc_taller_1' value='1' style='width:1cm;'>
                                </td>
                                <td style='border-left:1px solid #337ab7;width:50%;'>
                                    <input type='text' class='input_field' id='taller_1' value='' style='width:100%;'>
                                </td>
                                <td align="center" style='border-left:1px solid #337ab7;'>
                                    <input type='text' onkeypress='return event.charCode >= 46 && event.charCode <= 57' class='input_field' id='costo_taller_1' value='0' style='width:2cm;'>
                                </td>
                                <td align="center" colspan='3' style='border-left:1px solid #337ab7;'>
                                    <label for=""id='venta_taller_1'>$ 0</label>
                                    <input type='hidden' class='input_field' id='ventaValor_taller_1' name='venta_taller[]'>
                                    <input type='hidden' id='totalRenglon_taller_1' value='0'>
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2" align="right">
                                TOTALES
                            </td>
                            <td colspan="2" align="center">
                                <label for="" id="totalesTallerLabel"><?php if(isset($totalTaller)) echo number_format($totalTaller,2); else echo "$0.00"; ?></label>
                                <input type="hidden" name="totalesTaller" id="totalesTaller" value="<?php if(isset($totalTaller)) echo $totalTaller; else echo "0.00"; ?>">
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <br>
        <div class="row" align="center">
            <div class="col-md-12" align="center">
                <input type="submit" class="btn btn-primary" name="enviar" id="enviarOrdenForm" value="Actualizar">
                <input type="hidden" name="cargaFormulario" id="cargaFormulario" value="2">
                <input type="hidden" name="panelOrigen" id="panelOrigen" value="<?php if(isset($pOrigen)) echo $pOrigen; ?>">
                <input type="hidden" name="completoFormulario" id="completoFormulario" value="<?php if(isset($completoFormulario)) echo $completoFormulario; else echo "0"; ?>">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">
                <input type="hidden" name="idOrden" value="<?php if(isset($idOrden)) echo $idOrden; else echo "0"; ?>">
                <input type="hidden" name="idMaterial" value="<?php if(isset($idMaterial)) echo $idMaterial; else echo "0"; ?>">
            </div>
        </div>
        <br>
    </form>
</div>


<!-- Modal para la firma del quien elaboro el diagnóstico-->
<div class="modal fade" id="firmaDigital" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1">
                    <!-- <div id="signature" ></div> -->
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                    <!-- <p id="limpiar">limpiar canvas</p> -->
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>


<!-- Formulario de superusuario -->
<div class="modal fade " id="superUsuario" role="dialog" data-backdrop="static" data-keyboard="false" style="overflow-y: scroll;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 100%;margin-left: -30px;">
            <div class="modal-header" style="background-color:#eee;">
                <h5 class="modal-title" id="">Ingreso.</h5>
            </div>
            <div class="modal-body" style="font-size:12px;">
                <label for="" style="font-size: 14px;font-weight: initial;">Usuario:</label>
                <br>
                <input type="text" class="input_field" type="text" id="superUsuarioName" value="" style="width:100%;">
                <br><br>
                <label for="" style="font-size: 14px;font-weight: initial;">Password:</label>
                <br>
                <input type="password" class="input_field" type="text" id="superUsuarioPass" value="" style="width:100%;">
                <br><br><br>
                <h4 class="error" style="font-size: 16px;font-weight: initial;" align="center" id="superUsuarioError">:</h4>
            </div>
            <div class="modal-footer" align="right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="superUsuarioEnvio">Validar</button>
            </div>
        </div>
    </div>
</div>
