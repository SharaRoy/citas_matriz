<div style="margin:10px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."presupuestos/Presupuestos/validateFormR2"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:100%;" disbled>
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="input_field" name="folio_externo" id="folio_externo" value="<?php if(set_value('folio_externo') != "") echo set_value('folio_externo'); elseif(isset($folio_externo)) echo $folio_externo; ?>" style="width:100%;">
                <?php echo form_error('folio_externo', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php if(set_value('noSerie') != "") echo set_value('noSerie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;" disabled>
                <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;" disbled>
                <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;" disbled>
                <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;" disbled>
                <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;" disbled>
                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;" disbled>
                <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>

        <br>
        <!-- identificamos el procedimiento que se realiza-->
            <?php if ($dirige == "Entrega_T"): ?>
                <?php if (isset($estado_refacciones)): ?>
                    <?php if ($estado_refacciones == "1"): ?>
                        <div class="alert alert-primary" align="center">
                            <strong style="font-size:13px !important;color:darkblue;">
                                REFACCIONES SOLICIATADAS
                            </strong>
                        </div>
                    <?php elseif ($estado_refacciones == "2"): ?>
                        <div class="alert alert-primary" align="center">
                            <strong style="font-size:13px !important;color:darkblue;">
                                REFACCIONES RECIBIDAS
                            </strong>
                        </div>
                    <?php elseif ($estado_refacciones == "3"): ?>
                        <div class="alert alert-primary" align="center">
                            <strong style="font-size:13px !important;color:darkblue;">
                                REFACCIONES ENTREGADAS
                            </strong>
                        </div>
                    <?php endif ?>
                <?php endif ?>
            <?php elseif ($dirige == "Entrega_G"): ?>
                <?php if (isset($estado_refacciones_garantias)): ?>
                    <?php if ($estado_refacciones_garantias == "1"): ?>
                        <div class="alert alert-primary" align="center">
                            <strong style="font-size:13px !important;color:darkblue;">
                                REFACCIONES SOLICIATADAS
                            </strong>
                        </div>
                    <?php elseif ($estado_refacciones_garantias == "2"): ?>
                        <div class="alert alert-primary" align="center">
                            <strong style="font-size:13px !important;color:darkblue;">
                                REFACCIONES RECIBIDAS
                            </strong>
                        </div>
                    <?php elseif ($estado_refacciones_garantias == "3"): ?>
                        <div class="alert alert-primary" align="center">
                            <strong style="font-size:13px !important;color:darkblue;">
                                REFACCIONES ENTREGADAS
                            </strong>
                        </div>
                    <?php endif ?>
                <?php endif ?>
            <?php endif ?>
        
        <br>
        <div class="row">
            <!-- tabla meramente informativa -->
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:95%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REP.
                            </td>
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                CANTIDAD
                            </td>
                            <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:11%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                EXISTE
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                FECHA TENTATIVA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                PRECIO REFACCIÓN
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                COSTO MO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                TOTAL DE REPARACÓN
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                EDO. REFACCIÓN
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($renglon)): ?>
                            <!-- Comprobamos que haya registros guardados -->
                            <?php if (count($renglon)>0): ?>
                                <?php $total = 0; ?>
                                <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                    <!--Verificamso que el asesor haya marcado la refaccion como revisada -->
                                    <?php if ($autoriza_asesor[$index] == "1"): ?> 
                                        <!-- Comprobamos si ya se afecto el presupuesto por el cliente -->
                                        <?php if ($acepta_cliente != ""): ?>
                                            <!-- Si es una garantia no autorizada -->
                                            <?php if ($pza_garantia[$index] == "1"): ?>
                                                <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #97e2ee;">
                                            <!-- Si el presupuesto fue autorizado parcialmente -->
                                            <?php else: ?>
                                                <?php if ($autoriza_cliente[$index] == "1"): ?> 
                                                    <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #c7ecc7;">
                                                <?php else: ?>
                                                    <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #f5acaa;">
                                                <?php endif ?> 
                                            <?php endif ?>
                                        <!-- de lo contrario es una refaccion limpia -->
                                        <?php else: ?>
                                            <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>'>
                                        <?php endif ?>

                                            <!-- REP -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $referencia[$index]; ?></label>
                                            </td>
                                            <!-- CANTIDAD -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $cantidad[$index]; ?></label>
                                            </td>
                                            <!-- D E S C R I P C I Ó N -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $descripcion[$index]; ?></label>
                                            </td>
                                            <!-- NUM. PIEZA -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $num_pieza[$index]; ?></label>
                                            </td>
                                            <!-- EXISTE -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $existe[$index]; ?></label>
                                            </td>
                                            <!-- FECHA TENTATIVA -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;">
                                                    <?php 
                                                        if($fecha_planta[$index] != "0000-00-00"){
                                                            $fecha  = new DateTime($fecha_planta[$index]);
                                                            echo $fecha->format('d/m/Y');
                                                        }
                                                    ?>
                                                </label>
                                            </td>
                                            <?php if ($pza_garantia[$index] == "1"): ?>
                                                <td colspan="4" style='border:1px solid #337ab7;' align='center'>
                                                    Refaccion con garantía  (<?php echo (($autoriza_garantia[$index] == "1") ? "AUTORIZADA" : "NO AUTORIZADA"); ?>)
                                                    <?php $total_renglon = ((float)$cantidad[$index] *(float)$costo_pieza[$index] ) + ((float)$horas_mo[$index] *(float)$costo_mo[$index] ); ?>
                                                </td>
                                            <?php else: ?>
                                                <!-- PRECIO REFACCIÓN -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($costo_pieza[$index],2); ?></label>
                                                </td>
                                                <!-- HORAS -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($horas_mo[$index],2); ?></label>
                                                </td>
                                                <!-- COSTO MO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($costo_mo[$index],2); ?></label>
                                                    
                                                    <?php $total_renglon = ((float)$cantidad[$index] *(float)$costo_pieza[$index] ) + ((float)$horas_mo[$index] *(float)$costo_mo[$index] ); ?>
                                                </td>
                                                <!-- TOTAL DE REPARACÓN -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($total_renglon); ?></label>
                                                </td>
                                            <?php endif ?>

                                            <?php 
                                                if ($acepta_cliente != "No") {
                                                    if ($estado_entrega[$index] == "0") {
                                                        $color = "background-color:#f5acaa;";
                                                    } elseif ($estado_entrega[$index] == "1") {
                                                        $color = "background-color:#f5ff51;";
                                                    } elseif ($estado_entrega[$index] == "2") {
                                                        $color = "background-color:#5bc0de;";
                                                    }else{
                                                        $color = "background-color:#c7ecc7;";
                                                    }
                                                } else {
                                                    $color = "background-color:red;color:white;";
                                                }
                                            ?>
                                            
                                            <!-- ESTADO DE LA REFACCION -->
                                            <td style='border:1px solid #337ab7;<?= $color ?>' align='center'>
                                                <?php if (($autoriza_cliente[$index] == "1")||($autoriza_garantia[$index] == "1")): ?>
                                                    <?php if ($estado_entrega[$index] == "0"): ?>
                                                        SIN SOLICITAR
                                                    <?php elseif ($estado_entrega[$index] == "1"): ?>
                                                        SOLICITADA
                                                    <?php elseif ($estado_entrega[$index] == "2"): ?>
                                                        RECIBIDA
                                                    <?php else: ?>
                                                        ENTREGADA
                                                    <?php endif ?>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <?php $total += $total_renglon; ?>
                                    <?php endif ?>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="16" align="center">
                                        No se cargaron los datos
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="16" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php if (isset($dirige)): ?>
            <?php if ($dirige == "Entrega_G"): ?>
                <hr>
                <!-- comentarios de ventanilla -->
                <div class="row">
                    <div class="col-sm-6" align="left">
                        <h4>Comentarios de Garantías:</h4>
                        <label for="" style="font-size:12px;"><?php if(isset($comentario_garantia)){ if($comentario_garantia != "") echo $comentario_garantia; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    </div>
                    <div class="col-sm-6" align="right"></div>
                </div>
            <?php endif ?>
        <?php endif ?>
        
        <br>
        <div class="col-sm-12" align="center">
            <br>
            <i class="fas fa-spinner cargaIcono"></i>
            <br>
            <label for="" class="error" id="errorEnvioCotizacion"></label>
            <br>

            <!-- Datos basicos -->
            <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>"> 
            <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="6">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <!-- validamos los envios de cada rol -->
            <input type="hidden" name="envioRefacciones" id="envioRefacciones" value="<?php if(isset($envia_ventanilla)) echo $envia_ventanilla; else echo "0"; ?>">
            <input type="hidden" name="" id="envioJefeTaller" value="<?php if(isset($envio_jdt)) echo $envio_jdt; else echo "0"; ?>">
            <input type="hidden" name="" id="envioGarantias" value="<?php if(isset($envio_garantia)) echo $envio_garantia; else echo "0"; ?>">
            
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Ventanilla/4";?>';">
                Regresar
            </button>

            <!-- identificamos el procedimiento que se realiza-->
            <?php if ($dirige == "Entrega_T"): ?>
                <?php if (isset($estado_refacciones)): ?>
                    <?php if ($estado_refacciones == "0"): ?>
                        <button type="button" class="btn proceso_solicitud" style="background-color: #f5ff51; margin-left: 30px;">
                            SOLICITAR REFACCIÓN
                        </button>
                        <input type="hidden" id="valorEvento" value="1">
                    <?php elseif ($estado_refacciones == "1"): ?>
                        <button type="button" class="btn proceso_solicitud" style="background-color: #c7ecc7; margin-left: 30px;">
                            RECIBIR REFACCIÓN
                        </button>
                        <input type="hidden" id="valorEvento" value="2">
                    <?php elseif ($estado_refacciones == "2"): ?>
                        <button type="button" class="btn proceso_solicitud" style="background-color: #5bc0de; margin-left: 30px;">
                            ENTREGAR REFACCIÓN
                        </button>
                        <input type="hidden" id="valorEvento" value="3">
                    <?php else: ?>

                    <?php endif ?>
                    <input type="hidden" id="evento" value="T">
                <?php endif ?>

            <?php elseif ($dirige == "Entrega_G"): ?>
                <?php if (isset($estado_refacciones_garantias)): ?>
                    <?php if ($estado_refacciones_garantias == "0"): ?>
                        <button type="button" class="btn proceso_solicitud" style="background-color: #f5ff51; margin-left: 30px;">
                            SOLICITAR REFACCIÓN
                        </button>
                        <input type="hidden" id="valorEvento" value="1">
                    <?php elseif ($estado_refacciones_garantias == "1"): ?>
                        <button type="button" class="btn proceso_solicitud" style="background-color: #c7ecc7; margin-left: 30px;">
                            RECIBIR REFACCIÓN
                        </button>
                        <input type="hidden" id="valorEvento" value="2">
                    <?php elseif ($estado_refacciones_garantias == "2"): ?>
                        <button type="button" class="btn proceso_solicitud" style="background-color: #5bc0de; margin-left: 30px;">
                            ENTREGAR REFACCIÓN
                        </button>
                        <input type="hidden" id="valorEvento" value="3">
                    <?php else: ?>

                    <?php endif ?>
                    <input type="hidden" id="evento" value="G">
                <?php endif ?>
            <?php else: ?>

            <?php endif ?>

            <br>
        </div>
    </form>
</div>