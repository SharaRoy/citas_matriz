<div style="margin:10px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."presupuestos/Presupuestos/validateFormR2"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:100%;">
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="input_field" name="folio_externo" id="folio_externo" value="<?php if(set_value('folio_externo') != "") echo set_value('folio_externo'); elseif(isset($folio_externo)) echo $folio_externo; ?>" style="width:100%;">
                <?php echo form_error('folio_externo', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php if(set_value('noSerie') != "") echo set_value('noSerie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;" disabled>
                <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;">
                <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;">
                <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;">
                <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;">
                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;">
                <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <div class="row table-responsive" align="right" id="panel_refacciones">
            <div class="col-md-4" align="right"></div>
            <div class="col-md-8" align="right">
                <div class="row">
                    <div class="col-md-9" align="right">
                        <label for="" class="error"> <strong>*</strong> Guardar renglon [<i class="fas fa-plus"></i>] </label>
                        <br>
                        <label for="" class="error"> <strong>*</strong> Eliminar renglon [<i class="fas fa-minus"></i>] </label>
                    </div>
                    <div class="col-md-1" align="right">
                        <a id="agregarRefaccionRef" class="btn btn-success" style="color:white;">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="col-md-1" align="right">
                        <a id="eliminarRefaccionRef" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                            <i class="fas fa-minus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <?php if (isset($envia_ventanilla)): ?>
            <?php if ($envia_ventanilla != "0"): ?>
                <div class="alert alert-danger" align="center">
                    <strong style="font-size:13px !important;color:darkblue;">
                        Proceso del presupuesto: <?php echo $ubicacion_proceso; ?>
                        <br>
                        <!--Proceso del presupuesto con garantías: <?php echo $ubicacion_proceso_garantia; ?>-->
                    </strong>
                    <br>
                    <strong style="font-size:13px !important;">
                        Vigencia: Un mes a partir de la fecha de emisión del presente presupuesto. (Fecha de vencimiento: <?php if(isset($fecha_limite)) echo $fecha_limite; ?>)
                    </strong>
                </div>
            <?php endif ?>
        <?php endif ?>

        <div class="row">
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REP.
                            </td>
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                CANTIDAD
                            </td>
                            <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" colspan="5">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 10px;" align="center" colspan="3">
                                EXISTE
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                                FECHA<br>TENTATIVA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PRECIO REFACCIÓN
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                COSTO MO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                TOTAL DE REPARACÓN
                            </td>
                            <!--<td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PRIORIDAD <br> REFACCION
                            </td>-->
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PZA. GARANTÍA
                                <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($renglon)) echo count($renglon)+1; else echo "1"; ?>">
                                <input type="hidden" name="" id="indice_limite" value="<?php if (isset($renglon)) echo count($renglon)+1; else echo "1"; ?>">
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REPARACIÓN ADICIONAL
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                
                            </td>
                        </tr>
                        <tr style="background-color: #ddd;">
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                GPO.
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PREFIJO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                BÁSICO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                SUFIJO
                            </td>
                            <td style="width:15%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                CLAVE
                            </td>

                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                SI
                            </td>
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                NO
                            </td>
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                PLANTA
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($renglon)): ?>
                            <!-- Comprobamos que haya registros guardados -->
                            <?php if (count($renglon)>0): ?>
                                <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                    <tr id='fila_<?php echo $index+1; ?>'>
                                        <!-- REP. -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' id='ref_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $referencia[$index]; ?></textarea>
                                        </td>
                                        <!-- CANTIDAD -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo $index+1; ?>)' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $cantidad[$index]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                        </td>
                                        <!-- D E S C R I P C I Ó N -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?php echo $index+1; ?>)' id='descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $descripcion[$index]; ?></textarea>
                                        </td>
                                        <!-- GRUPO -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <?php 
                                                if ($basico[$index] != "") {
                                                    $pre_1 = (($prefijo[$index] != ".") ? $prefijo[$index] : "");
                                                    $basi_1 = (($basico[$index] != ".") ? $basico[$index] : "");
                                                    $sufi_1 = (($sufijo[$index] != ".") ? $sufijo[$index] : "");
                                                    $protocolo = strlen($pre_1."".$basi_1."".$sufi_1);
                                                    $numero_pieza = strlen(trim($num_pieza[$index]));
                                                    $diferencia = $numero_pieza - $protocolo;

                                                    if (($diferencia <= 0)||($diferencia == $numero_pieza)) {
                                                        $grupo = "";
                                                    } else {
                                                        $grupo = substr($num_pieza[$index],0,$diferencia);
                                                    }
                                                    
                                                }else{
                                                    $grupo = "";
                                                }
                                            ?>
                                            <textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes(<?php echo $index+1; ?>)' id='grupo_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?= $grupo; ?></textarea>
                                        </td>
                                        <!-- PREFIJO -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes(<?php echo $index+1; ?>)' id='prefijo_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $prefijo[$index]; ?></textarea>
                                        </td>
                                        <!-- BASICO -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes(<?php echo $index+1; ?>)' id='basico_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $basico[$index]; ?></textarea>
                                        </td>
                                        <!-- SUFIJO -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes(<?php echo $index+1; ?>)' id='sufijo_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $sufijo[$index]; ?></textarea>
                                        </td>
                                        <!-- NUM. PIEZA -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?php echo $index+1; ?>)' id='pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $num_pieza[$index]; ?></textarea>
                                        </td>
                                        <!-- EXISTE -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaSi_<?php echo $index+1; ?>' class='input_field' onclick='actualizaValores(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="SI" <?php if($existe[$index] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaNo_<?php echo $index+1; ?>' class='input_field' onclick='actualizaValores(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="NO" <?php if($existe[$index] == "NO") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaPlanta_<?php echo $index+1; ?>' class='input_field' onclick='actualizaValores(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="PLANTA" <?php if($existe[$index] == "PLANTA") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <!-- FECHA TENTANTIVA - ENTREGA -->
                                        <td style='border:1px solid #337ab7;' align='center'> 
                                            <input type="date" class='input_field' id="fecha_tentativa_<?php echo $index+1; ?>" value="<?php if($fecha_planta[$index] != "0000-00-00") echo $fecha_planta[$index]; else echo date('Y-m-d'); ?>" <?php if($fecha_planta[$index] == "0000-00-00") echo "style='display: none;'";?>> 
                                        </td>
                                        <!-- PRECIO REFACCIÓN -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            $<input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo $index+1; ?>)' id='costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_pieza[$index]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <!-- HORAS -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo $index+1; ?>)' id='horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $horas_mo[$index]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <!-- COSTO MO -->
                                        <td style='border:1px solid #337ab7;' align='center'> 
                                            $<input type='number' step='any' min='0' id='totalReng_<?php echo $index+1; ?>' onblur="actualizaValores(<?php echo $index+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_mo[$index]; ?>' style='width:90%;text-align:left;' disabled>
                                            
                                            <!-- total de la refaccion -->
                                            <input type='hidden' id='totalOperacion_<?php echo $index+1; ?>' value='<?php echo $total_refacion[$index]; ?>'>

                                            <!-- Indice interno de la recaccion -->
                                            <input type='hidden' id='id_refaccion_<?php echo $index+1; ?>' name='id_registros[]' value="<?php echo $id_refaccion[$index]; ?>">
                                            <!-- Informacion del renglon -->
                                            <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $renglon[$index]; ?>">
                                            <!-- Informacion de datos de garantia de la refaccion (si aplica) -->
                                            <input type='hidden' id='valoresGarantias_<?php echo $index+1; ?>' name='registrosGarantias[]' value="<?php echo $renglonGarantia[$index]; ?>">

                                            <!-- Informacion del aprueba jefe de taller -->
                                            <input type='hidden' id='aprueba_jdt_<?php echo $index+1; ?>' value="<?php echo $autoriza_jefeTaller[$index]; ?>">
                                            <!-- Informacion pieza con garantia -->
                                            <input type='hidden' name="garantia[]" id='garantia_<?php echo $index+1; ?>' value="<?php echo $pza_garantia[$index]; ?>">
                                            <!-- Informacion de prioridad de la refaccion -->
                                            <input type="hidden" name="prioridad_<?php echo $index+1; ?>" id="prioridad_<?php echo $index+1; ?>" value="<?php if(isset($prioridad[$index])) echo $prioridad[$index]; else echo "1"; ?>">
                                        </td>
                                        <!-- TOTAL DE REPARACÓN -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <label id="total_renglon_label_<?php echo $index+1; ?>" style="color:darkblue;">$<?php echo number_format($total_refacion[$index],2); ?></label>
                                        </td>
                                        <!-- PRIORIDAD <br> REFACCION -->
                                        <!--<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' id='prioridad_<?php echo $index+1; ?>' name='prioridad_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $prioridad[$index]; ?>' style='width:70%;text-align:center;'>
                                        </td>-->
                                        <!-- PZA. GARANTÍA -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='checkbox' class='input_field' onclick='refaccionGarantia(<?php echo $index+1; ?>)' name="pzaGarantia[]" value="<?php echo $index+1; ?>" id='garantia_check_<?php echo $index+1; ?>' <?php if($pza_garantia[$index] == "1") echo "checked"; ?> style="transform: scale(1.5);" disabled>
                                        </td>
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <?php if ($rep_adicional[$index] == "1"): ?>
                                                SI
                                            <?php else: ?>
                                                NO
                                            <?php endif ?>
                                        </td>
                                        <!-- BUSCAR REFACCION EN INVENTARIO -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <a id='refaccionBusqueda_<?php echo $index+1; ?>' class='refaccionesBox btn btn-warning' data-id="<?php echo $index+1; ?>" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php endfor; ?>

                                <!-- Creamos un renglon vacio por si se va a agregar algun item -->
                                <tr id='fila_<?php echo count($renglon)+1; ?>'>
                                    <!-- REP. -->
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?= count($renglon)+1; ?>)' id='ref_<?= count($renglon)+1; ?>' name="ref_<?= count($renglon)+1; ?>" style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- CANTIDAD -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo count($renglon)+1; ?>)' id='cantidad_<?php echo count($renglon)+1; ?>' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                    </td>
                                    <!-- D E S C R I P C I Ó N -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?php echo count($renglon)+1; ?>)' id='descripcion_<?php echo count($renglon)+1; ?>' style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- GRUPO -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes(<?= count($renglon)+1; ?>)' id='grupo_<?= count($renglon)+1; ?>' style='width:90%;text-align:left;'>FM</textarea>
                                    </td>
                                    <!-- PREFIJO -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes(<?= count($renglon)+1; ?>)' id='prefijo_<?= count($renglon)+1; ?>' style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- BASICO -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes(<?= count($renglon)+1; ?>)' id='basico_<?= count($renglon)+1; ?>' style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- SUFIJO -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValorespartes(<?= count($renglon)+1; ?>)' id='sufijo_<?= count($renglon)+1; ?>' style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- NUM. PIEZA -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValores(<?php echo count($renglon)+1; ?>)' id='pieza_<?php echo count($renglon)+1; ?>' style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- EXISTE -->
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaSi_<?php echo count($renglon)+1; ?>' class='input_field' onclick='actualizaValores(<?php echo count($renglon)+1; ?>)' name='existe_<?php echo count($renglon)+1; ?>' value="SI" style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaNo_<?php echo count($renglon)+1; ?>' class='input_field' onclick='actualizaValores(<?php echo count($renglon)+1; ?>)' name='existe_<?php echo count($renglon)+1; ?>' value="NO" style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaPlanta_<?php echo count($renglon)+1; ?>' class='input_field' onclick='actualizaValores(<?php echo count($renglon)+1; ?>)' name='existe_<?php echo count($renglon)+1; ?>' value="PLANTA" style="transform: scale(1.5);">
                                    </td>
                                    <!-- FECHA TENTANTIVA - ENTREGA -->
                                    <td style='border:1px solid #337ab7;' align='center'> 
                                        <input type="date" class='input_field' min="<?php echo date('Y-m-d'); ?>" id="fecha_tentativa_<?php echo count($renglon)+1; ?>" value="<?php echo date('Y-m-d'); ?>" name="" style="display: none;"> 
                                    </td>
                                    <!-- PRECIO REFACCIÓN -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        $<input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo count($renglon)+1; ?>)' id='costoCM_<?php echo count($renglon)+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                    </td>
                                    <!-- HORAS -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' onblur='actualizaValores(<?php echo count($renglon)+1; ?>)' id='horas_<?php echo count($renglon)+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                    </td>
                                    <!-- COSTO MO -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        $<input type='number' step='any' min='0' id='totalReng_<?php echo $index+1; ?>' onblur="actualizaValores(<?php echo count($renglon)+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='990' style='width:90%;text-align:left;' disabled>
                                        
                                        <!-- total de la refaccion -->
                                        <input type='hidden' id='totalOperacion_<?php echo count($renglon)+1; ?>' value='0'>

                                        <!-- Indice interno de la recaccion -->
                                        <input type='hidden' id='id_refaccion_<?php echo count($renglon)+1; ?>' name='id_registros[]' value="0">
                                        <!-- Informacion del renglon -->
                                        <input type='hidden' id='valoresCM_<?php echo count($renglon)+1; ?>' name='registros[]' value="">
                                        <!-- Informacion de datos de garantia de la refaccion (si aplica) -->
                                        <input type='hidden' id='valoresGarantias_<?php echo count($renglon)+1; ?>' name='registrosGarantias[]' value="0_0_0">

                                        <!-- Informacion del aprueba jefe de taller -->
                                        <input type='hidden' id='aprueba_jdt_<?php echo count($renglon)+1; ?>' value="0">
                                        <!-- Informacion pieza con garantia -->
                                        <input type='hidden' name="garantia[]" id='garantia_<?php echo count($renglon)+1; ?>' value="0">
                                        <!-- Informacion de prioridad de la refaccion -->
                                        <input type="hidden" name="prioridad_<?php echo count($renglon)+1; ?>" id="prioridad_<?php echo count($renglon)+1; ?>" value="2">
                                    </td>
                                    <!-- TOTAL DE REPARACÓN -->
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <label id="total_renglon_label_<?php echo count($renglon)+1; ?>" style="color:darkblue;">$0.00</label>
                                    </td>
                                    <!-- PRIORIDAD <br> REFACCION -->
                                    <!--<td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' id='prioridad_<?php echo count($renglon)+1; ?>' name='prioridad_<?php echo count($renglon)+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='2' style='width:70%;text-align:center;'>
                                    </td>-->
                                    <!-- PZA. GARANTÍA -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='checkbox' class='input_field' name='pzaGarantia[]' onclick='refaccionGarantia(<?php echo count($renglon)+1; ?>)' value='<?php echo count($renglon)+1; ?>' id='garantia_check_<?php echo count($renglon)+1; ?>' style='transform: scale(1.5);' >
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        NO
                                    </td>
                                    <!-- BUSCAR REFACCION EN INVENTARIO -->
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <a id='refaccionBusqueda_<?php echo count($renglon)+1; ?>' class='refaccionesBox btn btn-warning' data-id="<?php echo count($renglon)+1; ?>" data-target="#refacciones" data-toggle="modal" style="color:white;">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td colspan="18" align="center">
                                        No se cargaron los datos
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="18" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="15" align="right">
                                <label>SUB-TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="subTotalMaterialLabel"><?php if(isset($subtotal)) echo number_format($subtotal,2); else echo "0"; ?></label>
                                <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subtotal)) echo $subtotal; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="15" align="right" class="error">
                                <label>CANCELADO</label>
                            </td>
                            <td colspan="1" align="center" class="error">
                                $<label id="canceladoMaterialLabel"><?php if(isset($cancelado)) echo number_format($cancelado,2); else echo "0"; ?></label>
                                <input type="hidden" name="canceladoMaterial" id="canceladoMaterial" value="<?php if(isset($cancelado)) echo $cancelado; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="15" align="right">
                                <label>I.V.A.</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="ivaMaterialLabel"><?php if(isset($iva)) echo number_format($iva,2); else echo "0"; ?></label>
                                <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($iva)) echo $iva; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="15" align="right">
                                <label>PRESUPUESTO TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label id="presupuestoMaterialLabel"><?php if(isset($subtotal)) echo number_format((($subtotal-$cancelado)*1.16),2); else echo "0"; ?></label>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr style="border-top:1px solid #337ab7;">
                            <td colspan="13" rowspan="2">
                                <label for="" id="anticipoNota" style="color:blue;">Nota anticipo: <?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                                <input type="hidden" name="anticipoNota" value="<?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?>">
                            </td>
                            <td colspan="2" align="right">
                                <label>ANTICIPO</label>
                            </td>
                            <td align="center">
                                $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                                <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <label>TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="totalMaterialLabel"><?php if(isset($total)) echo number_format($total,2); else echo "0"; ?></label>
                                <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($total)) echo $total; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="15" align="right">
                                FIRMA DEL ASESOR QUE APRUEBA
                            </td>
                            <td align="center" colspan="3">
                                <img class='marcoImg' src='<?php echo base_url(); ?><?php if(isset($firma_asesor)) { echo (($firma_asesor != "") ?  $firma_asesor : "assets/imgs/fondo_bco.jpeg"); } else { echo "assets/imgs/fondo_bco.jpeg"; } ?>' id='' style='width:80px;height:30px;'>
                                <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php if(isset($firma_asesor)) echo $firma_asesor ?>'>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!--<div class="col-sm-12">
                <div class="alert alert-success" align="left">
                    <strong style="font-size:12px !important;font-weight: bold;">
                        Prioridad de refacciones:<br>
                        0 = Sin prioridad<br>
                        1 = Prioridad Baja <br>
                        2 = Prioridad Media <br>
                        3 = Prioridad Alta
                    </strong>
                </div>
            </div>-->
        </div>

        <!-- Tabla de refacciones con garantias -->
        <div class="row">
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;border-bottom: 1px solid darkblue;">
                            <td colspan="13" align="center">
                                <h5 style="text-align: center;">REFACCIONES CON GARANTIAS</h5>
                            </td>
                        </tr>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" >
                                CANTIDAD
                            </td>
                            <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" >
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" >
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" >
                                COSTO PZA FORD
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" >
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" >
                                COSTO MO GARANTÍAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" >
                                TOTAL DE REPARACÓN
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" >
                                EDITAR COSTOS
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMaterialesGarantias">
                        <!-- Verificamos que haya informacion cargada -->
                        <?php if (isset($renglon)): ?>
                            <!-- Verififamos que haya refacciones con garantias -->
                            <?php if ($registrosGarantias > 0): ?>
                                <?php $totalGarantia = 0; ?>
                                <!-- Recorremos los registros para imprimir los que lleven refacciones -->
                                <?php for ($i  = 0; $i < count($renglon); $i++): ?>
                                    <!-- Comprobamos que la refaccion tenga garnatia -->
                                    <?php if ($pza_garantia[$i] == "1"): ?>
                                        <tr id='tb2_fila_<?php echo $i+1; ?>'>
                                            <!-- CANTIDAD -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias(<?php echo $i+1; ?>)' id='tb2_cantidad_<?php echo $i+1; ?>' value='<?php echo $cantidad[$i]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;' disabled>
                                            </td>
                                            <!-- D E S C R I P C I Ó N -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <textarea rows='1' class='input_field_lg' onblur='actualizaValoresGarantias(<?php echo $i+1; ?>)' id='tb2_descripcion_<?php echo $i+1; ?>' style='width:90%;text-align:left;' disabled><?php echo $descripcion[$i]; ?></textarea>
                                            </td>
                                            <!-- NUM. PIEZA -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <textarea rows='1' class='input_field_lg' onblur='actualizaValoresGarantias(<?php echo $i+1; ?>)' id='tb2_pieza_<?php echo $i+1; ?>' style='width:90%;text-align:left;' disabled><?php echo $num_pieza[$i]; ?></textarea>
                                            </td>
                                            <!-- COSTO PZA FORD -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                $<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias(<?php echo $i+1; ?>)' id='tb2_costoCM_<?php echo $i+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_ford[$i]; ?>' style='width:90%;text-align:left;'>
                                            </td>
                                            <!-- HORAS -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresGarantias(<?php echo $i+1; ?>)' id='tb2_horas_<?php echo $i+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $horas_mo_garantias[$i]; ?>' style='width:90%;text-align:left;'>
                                            </td>
                                            <!-- COSTO MO GARANTÍAS -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                $<input type='number' step='any' min='0' id='tb2_totalReng_<?php echo $i+1; ?>' onblur="actualizaValoresGarantias(<?php echo $i+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_mo_garantias[$i]; ?>' style='width:90%;text-align:left;'>

                                            </td>
                                            <!-- TOTAL DE REPARACÓN -->
                                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                <!-- Calculamos el total para visualizar -->
                                                <?php $total = ((float)$cantidad[$i]*(float)$costo_ford[$i]) + ((float)$horas_mo_garantias[$i]*(float)$costo_mo_garantias[$i]); ?>
                                                <?php $totalGarantia += $total; ?>
                                                <label id="tb2_renglon_label_<?php echo $i+1; ?>" style="color:darkblue;">$<?php echo number_format($total,2); ?></label>
                                            </td>
                                            <!-- EDITAR COSTOS -->
                                            <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                <a id='costos_g_<?= $i+1; ?>' class='btn btn-warning' onclick='subir_costos(<?= $i+1 ?>)' data-id="<?= $i+1 ?>" data-target="#editar_costos" data-toggle="modal" style="color:white;" <?= (($envia_ventanilla == "0") ? 'hidden' : '') ?>>
                                                    <i class='fas fa-pen-fancy'></i>
                                                    
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endif ?>
                                <?php endfor ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="7" style="border-bottom:1px solid #337ab7;">
                                        <h5 style="text-align: center">Sin refacciones para mostrar</h5>
                                    </td>
                                </tr>
                            <?php endif ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7" style="border-bottom:1px solid #337ab7;">
                                    <h5 style="text-align: center">Sin refacciones para mostrar</h5>
                                </td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6" align="right">
                                SUB-TOTAL
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="subTotalMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo $totalGarantia; ?></label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="right">
                                I.V.A.
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="ivaMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo number_format(($totalGarantia*0.16)); ?></label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="right">
                                TOTAL
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="totalMaterialLabel_tabla2"><?php if(isset($totalGarantia)) echo number_format(($totalGarantia*1.16)); ?></label>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <br>
        <!-- Comentarios del asesor -->
        <div class="row">
            <div class="col-sm-5">
                <h4>Notas del asesor:</h4>
                <!-- Verificamos si es una vista del asesor -->
                <label for="" style="font-size:12px;"><?php if(isset($nota_asesor)){ if($nota_asesor != "") echo $nota_asesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-7" align="right">
                <h4>Archivo(s) de la cotización:</h4>
                <br>

                <?php if (isset($archivos_presupuesto)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivos_presupuesto); $i++): ?>
                        <?php if ($archivos_presupuesto[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivos_presupuesto[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>

                            <?php if ((($i+1)% 4) == 0): ?>
                                <br>
                            <?php endif; ?>
                            
                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivos_presupuesto) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>

                <hr>
                <!-- Para agregar mas documentos -->
                <input type="file" multiple name="uploadfiles[]">
                <input type="hidden" name="uploadfiles_resp" value="<?php if(isset($archivos_presupuesto_lineal)) echo $archivos_presupuesto_lineal;?>">
                <br>
            </div>
        </div>

        <hr>
        <!-- Comentarios del técnico -->
        <div class="row">
            <div class="col-md-6">
                <h4>Comentario del técnico para Refacciones:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_tecnico)){ if($comentario_tecnico != "") echo $comentario_tecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>

            <div class="col-sm-6" align="right">
                <h4>Archivo(s) del técnico para Refacciones:</h4>

                <?php if (isset($archivo_tecnico)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivo_tecnico); $i++): ?>
                        <?php if ($archivo_tecnico[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo_tecnico[$i]; ?>" class="btn" style="background-color: #5cb8a5;border-color: #4c8bae;color:white;font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>

                            <?php if (($contador % 4) == 0): ?>
                                <br>
                            <?php endif; ?>

                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivo_tecnico) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <hr>
        <!-- comentarios de ventanilla -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios de Ventanilla:</h4>
                <textarea id="comentario_ventanilla" name="comentario_ventanilla" rows="2" style="width:100%;border-radius:4px;" placeholder="Notas de ventanilla..."><?php if(isset($comentario_ventanilla)) echo $comentario_ventanilla;?></textarea>
            </div>
            <div class="col-sm-6" align="right"></div>
        </div>

        <hr>
        <!-- comentarios del jefe de taller -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios del Jefe de Taller:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_jdt)){ if($comentario_jdt != "") echo $comentario_jdt; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right">
                <h4>Archivo(s) del Jefe de Taller:</h4>

                <?php if (isset($archivo_jdt)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivo_jdt); $i++): ?>
                        <?php if ($archivo_jdt[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo_jdt[$i]; ?>" class="btn btn-primary" style="font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>
                            
                            <?php if (($contador % 4) == 0): ?>
                                <br>
                            <?php endif; ?>
                            
                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivo_jdt) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <hr>
        <!-- comentarios de ventanilla -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios de Garantías:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_garantia)){ if($comentario_garantia != "") echo $comentario_garantia; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right"></div>
        </div>
        
        <br>
        <div class="col-sm-12" align="center">
            <!-- Datos basicos -->
            <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>"> 
            <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="3">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <!-- validamos los envios de cada rol -->
            <input type="hidden" name="envioRefacciones" id="envioRefacciones" value="<?php if(isset($envia_ventanilla)) echo $envia_ventanilla; else echo "0"; ?>">
            <input type="hidden" name="" id="envioJefeTaller" value="<?php if(isset($envio_jdt)) echo $envio_jdt; else echo "0"; ?>">
            <input type="hidden" name="" id="envioGarantias" value="<?php if(isset($envio_garantia)) echo $envio_garantia; else echo "0"; ?>">
            
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Ventanilla/4";?>';">
                Regresar
            </button>

            <!--<button type="submit" class="btn btn-success" name="" id="terminarCotizacion" >Actualizar Cotizacion Multipunto</button>
            <button type="submit" class="btn btn-success" name="" id="terminarCotizacion2" >Termina Revisión</button> -->
            <input type="submit" class="btn btn-success terminarCotizacion" style="margin-left: 20px;" name="aceptarCotizacion" value="Actualiar Presupuesto">
            <input type="submit" class="btn terminarCotizacion" style="background-color: #1f711f;color: white;margin-left: 20px;" name="aceptarCotizacion" value="Enviar Presupuesto">

        </div>
    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para filtrar las refacciones-->
<div class="modal fade" id="refacciones" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel">Buscador de Refacciones/Mano de Obra</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <br>
                        <input type='radio' id='' class='input_field' name='filtros' value="clave" checked>
                        Buscar por clave
                        <br><br>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding:0px;border: 0px;">
                                <!-- <i class="fa fa-search"></i> -->
                                <button type="button" class="btn btn-default" id="busquedaClaveBtn"><i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control" id="claveBuscar" placeholder="Buscar..." style="width:100%;height:100%;">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <br>
                        <input type='radio' class='input_field' name='filtros' value="descripcion">
                        Buscar por descripción
                        <br><br>
                        <div class="input-group">
                            <div class="input-group-addon" style="padding:0px;border: 0px;">
                                <!-- <i class="fa fa-search"></i> -->
                                <button type="button" class="btn btn-default" id="busquedaDescripBtn" ><i class="fa fa-search"></i></button>
                            </div>
                            <input type="text" class="form-control" id="descipBuscar" placeholder="Buscar..." style='width:100%;'>
                        </div>
                    </div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <br>
                        <label id="alertaConexion"></label>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <label for="">Resultados:</label>
                        <br>
                        <select class="form-control" id="resultadosVaciado" style="font-size:12px;width:100%;">
                            <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="col-sm-2"></div>
                </div>

                <br><br>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Clave:</label>
                        <input type='text' class='form-control' id="claveRefaccion" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-6" style="padding-top: 20px;">
                        <input type='text' class='form-control' id="tipoBuscar" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <label for="">Descripcion:</label>
                        <input type='text' class='form-control' id="descripcionRefacciones" style='width:100%;' disabled>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-3">
                        <label for="">Precio:</label>
                        <input type='text' class='form-control' id="precioRefacciones" style='width:100%;' disabled>
                    </div>
                    <div class="col-sm-3">
                        <label for="">Existencia:</label>
                        <input type='text' class='form-control' id="existenciaRefacciones" style='width:100%;' disabled>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="indiceOperacion" value="">
                <input type="hidden" name="" id="existeRefacciones" value="">
                <input type="hidden" name="" id="claveRefaccion" value="">
                <!-- <input type="hidden" name="" id="base_ajax" value="<?php echo base_url(); ?>"> -->
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarOperacion">Cerrar</button>
                <button type="button" class="btn btn-primary" name="" id="bajaOperacion" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para ingresar anticipo-->
<div class="modal fade" id="modalAnticipo" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="center">Anticipo de refacciones</h3>
            </div>
            <div class="modal-body">
                <div class="row" id="credencialesAnticipo">
                    <div class="col-sm-12">
                        <label for="" style="font-size: 16px;font-weight: initial;">Usuario:</label>
                        <br>
                        <input type="text" class="form-control" type="text" id="usuarioAnticipo" value="" style="width:100%;">
                        <br>
                        <label for="" style="font-size: 16px;font-weight: initial;">Password:</label>
                        <br>
                        <input type="password" class="form-control" type="text" id="passAnticipo" value="" style="width:100%;">
                        <br><br>
                        <button type="button" class="btn btn-primary" id="btnCredencialAnticipo">Validar</button>
                    </div>
                </div>
                <div class="row" id="addAnticipo" style="display:none;">
                    <div class="col-sm-12">
                      <label for="" style="font-size: 16px;font-weight: initial;">Cantidad:</label>
                      <br>
                      <div class="input-group">
                          <div class="input-group-addon">
                              <i class="fa fa-hand-holding-usd"></i>
                          </div>
                          <input type="text" class="form-control" id="cantidadAnticipo" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value="0" style="width:100%;">
                      </div>
                      <br>
                      <label for="" style="font-size: 16px;font-weight: initial;">Comentario:</label>
                      <br>
                      <textarea rows='2' id='comentarioAnticipo' style='width:100%;text-align:left;font-size:16px;'></textarea>
                      <br><br>
                      <button type="button" class="btn btn-success btn-block" id="btnAddAnticipo">Guardar</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <h5 class="error" style="font-size: 16px;font-weight: bold;" align="center" id="AnticipoError"></h5>
                <br>
                <input type="hidden" id="idCotizacion" value="<?php if(isset($idOrden)) echo $idOrden; ?>">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal para editar los costos de garantias-->
<div class="modal fade" id="editar_costos" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel">REFACCIÓN CON GARANTÍA</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8">
                        <h5 style="color: darkblue; font-weight: bold;">D E S C R I P C I Ó N</h5>
                        <label id="descripcion_g" style="color: darkred; font-size: 14px;"></label>
                    </div>
                    <div class="col-sm-4">
                        <h5 style="color: darkblue; font-weight: bold;">NUM. PIEZA</h5>
                        <label id="pieza_g" style="color: darkred; font-size: 14px;"></label>
                    </div>
                </div>
                
                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <h5 style="color: darkblue; font-weight: bold;">COSTO MO</h5>
                        <input type="text" class="form-control" id="precio_mo_g" onblur="actualiza_costo_g()" onkeypress='return event.charCode >= 46 && event.charCode <= 57'>
                    </div>
                    <div class="col-sm-6">
                        <h5 style="color: darkblue; font-weight: bold;">HORAS MO</h5>
                        <input type="text" class="form-control" id="horas_mo_g" onblur="actualiza_costo_g()" onkeypress='return event.charCode >= 46 && event.charCode <= 57'>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <h5 style="color: darkblue; font-weight: bold;">COSTO PZA FORD</h5>
                        <input type="text" class="form-control" id="precio_pieza_g" onblur="actualiza_costo_g()" onkeypress='return event.charCode >= 46 && event.charCode <= 57'>
                    </div>
                    <div class="col-sm-6">
                        <h5 style="color: darkblue; font-weight: bold;">TOTAL</h5>
                        <input type="text" class="form-control" id="total_g" onkeypress='return event.charCode >= 46 && event.charCode <= 57' disabled>
                    </div>
                </div>
            </div>
            <div class="modal-footer" align="center">
                <i class="fas fa-spinner cargaIcono" id="envio_costo_g"></i>
                <h4 class="error" id="errorEnvio"></h4>

                <br>
                <input type="hidden" id="refaccion_cg" value="">
                <input type="hidden" id="index" value="">
                <input type="hidden" id="cantidad_cg" value="">
                <input type="hidden" id="panel" value="Ventanilla">
                <button class="btn btn-warning" onclick="cerrar_costos_g()" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-success" id="enviar_costos_g" style="margin-left: 40px;">Actualizar</button>
            </div>
        </div>
    </div>
</div>