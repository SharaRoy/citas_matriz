<?php 
    // Eliminamos cache
    //la pagina expira en una fecha pasada
    header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT"); 
    //ultima actualizacion ahora cuando la cargamos
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
    //no guardar en CACHE
    header ("Cache-Control: no-cache, must-revalidate"); 
    header ("Pragma: no-cache");

    ob_start();

?>

<style media="screen">
    #encabezado {
        color: #fff;
        background-color: #337ab7;
        border-color: #337ab7;
        border-radius: 4px;
    }

    p, label{
        text-align: justify;
        font-size:8px;
        color: black;
    }

    .tituloTxt{
        font-size:9px;
        color: #337ab7;
    }
    .division{
        background-color: #ddd;
    }
    .headers_table{
        border:1px solid black;
        border-style: double;
    }
    td,tr{
        padding: 0px 0px 0px 0px !important;
        font-size: 8px;
        color:black;
    }
</style>

<page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm">
    <table style="width:98%;border-radius: 4px;">
        <tr>
            <td colspan="10" style="font-weight: bold; font-size:16px;" align="center">
                <?= SUCURSAL ?>, S.A. DE C.V.
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="10" style="font-weight: bold; font-size:13px;" align="center">
                REQUISICIÓN DE REFACCIONES
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="font-weight: bold; font-size:12px;">
                Orden Servicio
                <br><br>
            </td>
            <td colspan="6" align="left" style="font-weight: bold; font-size:14px;">
                <?php if(isset($orden_foliointelsis)) echo $orden_foliointelsis; else echo "<br>"; ?>
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="1" style="font-weight: bold; font-size:11px;">
                Día
            </td>
            <td colspan="3" align="left" style=" font-size:11px;">
                <?php if(isset($dia)) echo $diaSemeana.", ".$dia." de ".$mes." del ".$anio." "; else echo "<br>"; ?>
            </td>

            <td colspan="1" style="font-weight: bold; font-size:11px;">
                Hora
            </td>
            <td colspan="1" align="left" style=" font-size:11px;">
                <?php if(isset($dia)) echo $hora; else echo "<br>"; ?>
            </td>

            <td colspan="2" style="font-weight: bold; font-size:11px;" align="right">
                REQUISICIÓN NO.
            </td>
            <td colspan="2" align="left" style=" font-size:12px;">
                <?php if(isset($req_no)) echo $req_no; else echo "M13739"; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; font-size:12px;">
                Usuario
                <br><br>
            </td>
            <td colspan="4" align="left" style=" font-size:11px;">
                <?php if(isset($usuario)) echo $usuario; else echo "AGOMEZ"; ?>
                <br><br>
            </td>

            <td colspan="2" style="font-weight: bold; font-size:11px;" align="right">
                Empresa
                <br><br>
            </td>
            <td colspan="2" align="left" style=" font-size:11px;">
                <?php if(isset($empresa)) echo $empresa; else echo "FORGU"; ?>
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="10" style="border-top: 0.5px solid black;">
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="1" style="font-weight: bold; font-size:12px;">
                Dirección:
            </td>
            <td colspan="9" align="left" style="font-weight: bold; font-size:10px;">
                <?php if(isset($orden_domicilio)) echo $orden_domicilio.",<br> C.P. ".$orden_cp. ", ".$orden_municipio.", ".$orden_estado; else echo "<br>"; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; font-size:12px;">
                OBSERVACIONES:
            </td>
            <td colspan="8" align="left" style=" font-size:11px;">
                <?php if(isset($observaciones)) echo $observaciones; else echo "<br>"; ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; font-size:12px;">
                Teléfono:
                <br><br>
            </td>
            <td colspan="8" align="left" style=" font-size:11px;">
                <?php if(isset($orden_telefono_movil)) echo $orden_telefono_movil; else echo "<br>"; ?>
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold; font-size:12px;">
                Número de Transito:
            </td>
            <td colspan="1" align="left" style=" font-size:11px;">
                <?php if(isset($transito)) echo $transito; else echo "M12499"; ?>
            </td>

            <td colspan="1" style="font-weight: bold; font-size:12px;">
                Almacen: <?php if(isset($almacen)) echo $almacen; else echo "R"; ?>
            </td>

            <td colspan="2" style="font-weight: bold; font-size:12px;">
                Destino: <?php if(isset($destino)) echo $destino; else echo "S"; ?>
            </td>

            <td colspan="3" style="font-weight: bold; font-size:12px;">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="9" style="border-bottom: 0.5px solid black;">
                <br><br>
            </td>
            <td>
                <br>
            </td>
        </tr>
        <tr>
            <td style="width:15%;font-weight: bold;font-size:11;" align="center">
                Código
            </td>
            <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                Localización
            </td>
            <td style="width:30%;font-weight: bold;font-size:11;" align="center" colspan="2">
                Descripción
            </td>
            <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                Ctd.
            </td>
            <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                Precio
            </td>
            <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                Importe
            </td>
            <td style="width:15%;font-weight: bold;font-size:11;" align="center" colspan="2">
                Total
            </td>
            <td>
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="9" style="border-top: 0.5px solid black;">
                <br><br>
            </td>
            <td>
                <br>
            </td>
        </tr>
        <?php if (isset($cantidad)): ?>
            <?php foreach ($cantidad as $index => $value): ?>
                <tr>
                    <td style="width:15%;font-size:10;" align="center">
                        <label for=""><?php echo $clave[$index]; ?></label>
                    </td>
                    <td style="width:10%;font-size:10;" align="center">
                        <br>
                    </td>
                    <td style="width:30%;font-size:10;" align="center" colspan="2">
                        <label for=""><?php echo $descripcion[$index]; ?></label>
                    </td>
                    
                    <td style="width:10%;font-size:10;" align="center">
                        <label for=""><?php echo $cantidad[$index]; ?></label>
                    </td>
                    <td style="width:10%;font-size:10;" align="center">
                        <label for=""><?php echo number_format($precio_lista[$index],2); ?></label>
                    </td>
                    <td style="width:10%;font-size:10;" align="center">
                        <label for=""><?php echo number_format($importe[$index],2); ?></label>
                    </td>
                    <td style="width:15%;font-size:10;" align="center" colspan="2">
                        <label for=""><?php echo number_format($total[$index],2); ?></label>
                    </td>
                    <td>
                <br>
            </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        <tr>
           <td colspan="2"><br></td>
           <td colspan="6" style="font-size:11;" align="right">
                <br>
               Subtotal: &nbsp;<?= ((isset($subtotal)) ? number_format($subtotal,2) : "0.00") ?> &nbsp;&nbsp;&nbsp;
               Iva: &nbsp;<?= ((isset($iva)) ? number_format($iva,2) : "0.00") ?> &nbsp;&nbsp;&nbsp;
               Total: &nbsp;<?= ((isset($cta_total)) ? number_format($cta_total,2) : "0.00") ?> 
               <br>
           </td>
           <td colspan="2"><br></td>
        </tr>
        <tr>
           <td colspan="10">
               <br><br><br><br>
           </td> 
        </tr>
        <tr>
            <td colspan="1" style="font-size:11;" align="left">
                Cliente:
            </td>
            <td colspan="6" align="left" style="font-size:11;">
                <?= ((isset($nombreCliente)) ? $nombreCliente : "<br>") ?>
            </td>
            <td colspan="3">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="1" style="font-size:11;" align="left">
                Vehículo:
            </td>
            <td colspan="6" align="left" style="font-size:11;">
                <?= ((isset($orden_vehiculo_marca)) ? $orden_vehiculo_marca : "<br>") ?>
            </td>
            <td colspan="3">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="1" style="font-size:11;" align="left">
                VIN:
            </td>
            <td colspan="6" align="left" style="font-size:11;">
                <?= ((isset($orden_vehiculo_identificacion)) ? $orden_vehiculo_identificacion : "<br>") ?>
            </td>
            <td colspan="3">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="1" style="font-size:11;" align="left">
                Placas:
            </td>
            <td colspan="6" align="left" style="font-size:11;">
                <?= ((isset($orden_vehiculo_placas)) ? $orden_vehiculo_placas : "<br>") ?>
            </td>
            <td colspan="3">
                <br>
            </td>
        </tr>
        <tr>
           <td colspan="10">
               <br><br><br><br>
               <br><br><br><br>
           </td> 
        </tr>
        <tr>
            <td colspan="3">
                <br>
            </td>
            <td colspan="3" align="center">
                <?php if (isset($firma_requisicion)): ?>
                    <?php if ($firma_requisicion_url != ""): ?>
                        <img class="marcoImg" src="<?php if(isset($firma_requisicion_url)) echo base_url().$firma_requisicion_url;?>" style="height:50px;width:80px;">
                    <?php endif; ?>
                <?php endif; ?>
            </td>
            <td colspan="4">
                <br>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br>
            </td>
            <td colspan="3" align="center" style="border-top: 0.5px solid black;">
                <?= ((isset($tecnico)) ? $tecnico : "") ?>
            </td>
            <td colspan="4">
                <br>
            </td>
        </tr>
    </table>
</page>

<?php 
    use Spipu\Html2Pdf\Html2Pdf;

    //Orientación  de la hoja P->Vertical   L->Horizontal
    $html2pdf = new Html2Pdf('P', 'A4', 'en',TRUE,'UTF-8',NULL);
    
    try {

        $html = ob_get_clean();

        ob_clean();
        /* Limpiamos la salida del búfer y lo desactivamos */
        //ob_end_clean();

        $html2pdf->setDefaultFont('Helvetica');     //Helvetica
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($html);
        $html2pdf->setTestTdInOnePage(FALSE);
        $html2pdf->Output();

    } catch (Html2PdfException $e) {
        $html2pdf->clean();
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }

?>