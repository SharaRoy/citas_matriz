<?php 
    // eliminamos cache
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP 1.1.
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");  // HTTP 1.0.

    ob_start();
    
?>

<html lang="es">
 
    <head>
        <title>RECEPCIÓN DE PIEZAS <?php if(isset($ordenT)) echo $ordenT;?></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="estilos.css" />
        <link rel="shortcut icon" href="/favicon.ico" />
        
        <style>
            @page { 
                sheet-size: A4;
                size: auto; /* <length>{1,2} | auto | portrait | landscape */
                      /* 'em' 'ex' and % are not allowed; length values are width height */
                margin: 5mm; 
                /*margin-top: 5mm; /* <any of the usual CSS values for margins> */
                /*margin-left: 5mm;*/
                /*margin-right: 5mm;*/
                /*margin-bottom: 10mm;*/
                             /*(% of page-box width for LR, of height for TB) */
                margin-header: 5mm; /* <any of the usual CSS values for margins> */
                margin-footer: 5mm; /* <any of the usual CSS values for margins> */
            }
        </style>

        <style media="screen">
            td,tr{
                padding: 0px 5px 5px 0px !important;
                font-size: 9px;
                color:#337ab7;
            }

            .centradoBarra{
                position: relative;
                max-height: 4px;
                /* margin-top: -12px; */
                margin-left: -30px;
                top: 1px;;
                left: 9px;
                /* transform: translate(-50%, -50%); */
            }

            .contenedorImgFondo{
                position: relative;
                display: inline-block;
                text-align: center;
            }

            .imgFondo{
                /* width: 300px; */
                /* height: 60px; */
                margin-top: -15px;
            }
        </style>
    </head>
    <body>
        <div>
            <br>
            <table style="width:100%;">
                <tr>
                    <td colspan="11" style="font-weight: bold; font-size:16px;" align="center">
                        <?= SUCURSAL ?>, S.A. DE C.V.
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="11" style="font-weight: bold; font-size:13px;" align="center">
                        RECEPCIÓN DE PIEZAS
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" style="font-weight: bold; font-size:12px;">
                        Orden Servicio
                        <br><br>
                    </td>
                    <td colspan="3" align="left" style="font-weight: bold; font-size:14px;">
                        <?php if(isset($ordenT)) echo $ordenT; else echo "<br>"; ?>
                        <br><br>
                    </td>
                    <td colspan="2" style="font-weight: bold; font-size:12px;">
                        Folio Intelisis
                        <br><br>
                    </td>
                    <td colspan="3" align="left" style="font-weight: bold; font-size:14px;">
                        <?php if(isset($orden_foliointelsis)) echo $orden_foliointelsis; else echo "<br>"; ?>
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" style="font-weight: bold; font-size:11px;">
                        Día
                    </td>
                    <td colspan="3" align="left" style=" font-size:11px;">
                        <?php if(isset($dia)) echo $diaSemeana.", ".$dia." de ".$mes." del ".$anio." "; else echo "<br>"; ?>
                    </td>

                    <td colspan="1" style="font-weight: bold; font-size:11px;">
                        Hora
                    </td>
                    <td colspan="1" align="left" style=" font-size:11px;">
                        <?php if(isset($dia)) echo $hora; else echo "<br>"; ?>
                    </td>

                    <td colspan="2" style="font-weight: bold; font-size:11px;" align="right">
                        REQUISICIÓN NO.
                    </td>
                    <td colspan="3" align="left" style=" font-size:12px;">
                        <?php if(isset($req_no)) echo $req_no; else echo "M13739"; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="11" style="border-bottom: 0.5px solid black;">
                        <br><br>
                    </td>
                </tr>
                <tr>
                    <td style="width:15%;font-weight: bold;font-size:11;" align="center">
                        Código
                    </td>
                    <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                        Localización
                    </td>
                    <td style="width:30%;font-weight: bold;font-size:11;" align="center" colspan="3">
                        Descripción
                    </td>
                    <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                        Cantidad.
                    </td>
                    <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                        <!--Precio-->
                    </td>
                    <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                        <!--Importe-->
                    </td>
                    <td style="width:15%;font-weight: bold;font-size:11;" align="center" colspan="2">
                        <!--Total-->
                    </td>
                    <td style="width:10%;font-weight: bold;font-size:11;" align="center">
                        Recibidas
                    </td>
                </tr>
                <tr>
                    <td colspan="11" style="border-top: 0.5px solid black;">
                        <br><br>
                    </td>
                </tr>
                <?php if (isset($cantidad)): ?>
                    <?php foreach ($cantidad as $index => $value): ?>
                        <tr>
                            <td style="width:15%;font-size:10;" align="center">
                                <label for=""><?php echo $clave[$index]; ?></label>
                            </td>
                            <td style="width:10%;font-size:10;" align="center">
                                <br>
                            </td>
                            <td style="width:30%;font-size:10;" align="center" colspan="3">
                                <label for=""><?php echo $descripcion[$index]; ?></label>
                            </td>
                            
                            <td style="width:10%;font-size:10;" align="center">
                                <label for=""><?php echo $cantidad[$index]; ?></label>
                            </td>
                            <td style="width:10%;font-size:10;" align="center">
                                <!--<label for=""><?php echo "$".number_format($precio_lista[$index],2); ?></label>-->
                            </td>
                            <td style="width:10%;font-size:10;" align="center">
                                <!--<label for=""><?php echo "$".number_format($importe[$index],2); ?></label>-->
                            </td>
                            <td style="width:15%;font-size:10;" align="center" colspan="2">
                                <!--<label for=""><?php echo "$".number_format($total[$index],2); ?></label>-->
                            </td>
                            <td style="width:10%;font-size:9;" align="center">
                                <?php if ($estado_entrega[$index] == "3"): ?>
                                    <label for=""><?= $fecha_entrega[$index] ?></label>
                                <?php else: ?>
                                    <label for="" style="font-size:8;">PENDIENTE DE ENTREGA</label>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <tr>
                   <td colspan="2"><br></td>
                   <td colspan="6" style="font-size:10;" align="right">
                       <br>
                       <!--Subtotal: &nbsp;<?= "$".((isset($subtotal)) ? number_format($subtotal,2) : "0.00") ?> &nbsp;&nbsp;&nbsp;
                       Iva: &nbsp;<?= "$".((isset($iva)) ? number_format($iva,2) : "0.00") ?> &nbsp;&nbsp;&nbsp;
                       Total: &nbsp;<?= "$".((isset($cta_total)) ? number_format($cta_total,2) : "0.00") ?> -->
                       <br>
                   </td>
                   <td colspan="3"><br></td>
                </tr>
                <tr>
                   <td colspan="11">
                       <br><br><br><br>
                   </td> 
                </tr>
                <tr>
                    <td colspan="1" style="font-size:11;" align="left">
                        Cliente:
                    </td>
                    <td colspan="6" align="left" style="font-size:10;">
                        <?= ((isset($nombreCliente)) ? $nombreCliente : "<br>") ?>
                    </td>
                    <td colspan="4">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" style="font-size:11;" align="left">
                        Vehículo:
                    </td>
                    <td colspan="6" align="left" style="font-size:10;">
                        <?= ((isset($orden_vehiculo_marca)) ? $orden_vehiculo_marca : "<br>") ?>
                    </td>
                    <td colspan="4">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" style="font-size:11;" align="left">
                        VIN:
                    </td>
                    <td colspan="6" align="left" style="font-size:10;">
                        <?= ((isset($orden_vehiculo_identificacion)) ? $orden_vehiculo_identificacion : "<br>") ?>
                    </td>
                    <td colspan="4">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" style="font-size:11;" align="left">
                        Placas:
                    </td>
                    <td colspan="6" align="left" style="font-size:10;">
                        <?= ((isset($orden_vehiculo_placas)) ? $orden_vehiculo_placas : "<br>") ?>
                    </td>
                    <td colspan="4">
                        <br>
                    </td>
                </tr>
                <tr>
                   <td colspan="11">
                       <br><br>
                   </td> 
                </tr>
                <tr>
                    <td colspan="4">
                        <br>
                    </td>
                    <td colspan="7" align="right">
                        <table style="width:100%; font-size: 10px;" id="comentarios_registrados">
                            <?php if (isset($valida_uso)): ?>
                                <?php if (count($valida_uso)>0): ?>
                                    <?php $validar = 0; ?>
                                    <?php for ($index  = 0; $index < count($valida_uso); $index++): ?>
                                        <?php if ($valida_uso[$index] != ""): ?>
                                            <tr>
                                                <td style='width: 60%;font-size: 10px;color: black;text-align: justify;' rowspan='2'>
                                                    <?= $index+1 ?> ) Se inspeccionaron las partes reemplazadas <span style='color:darkblue;'><?= $req_pza_parte[$index] ?></span> en la acción de servicio #<span style='color:darkblue;'><?= $valida_uso[$index] ?></span> y se verificó que fueran reemplazadas.
                                                </td>
                                                <td style='width: 40%;border-bottom: 0.5px solid black;font-size: 11px;color: black;'  align="center">
                                                    <img class="marcoImg" src="<?= base_url().$req_pza_firma[$index] ?>" style="height:1cm;width:2cm;">

                                                    <br>
                                                    <span style='color:darkblue;'><?= $req_pza_gerente[$index] ?></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style='font-size: 10px;color: black;' align="center">
                                                    Gerente de servicio
                                                </td>
                                            </tr>
                                            <?php $validar = 1; ?>
                                        <?php endif ?>
                                    <?php endfor; ?>

                                    <?php if ($validar == 0): ?>
                                        <tr>
                                            <td colspan="2" align="center">
                                                NO SE CARGARON COMENTARIOS DE INSTALACIÓN
                                            </td>
                                        </tr>
                                    <?php endif ?>
                                <?php else: ?>
                                    <tr>
                                        <td colspan="2" align="center">
                                            NO SE CARGARON COMENTARIOS DE INSTALACIÓN
                                        </td>
                                    </tr>
                                <?php endif ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="2" align="center">
                                        NO SE CARGARON COMENTARIOS DE INSTALACIÓN
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <br>
                    </td>
                    <td colspan="3" align="center">
                        <br>
                    </td>
                    <td colspan="2" rowspan="2" align="center" style="font-size: 9px;">
                        <br>
                    </td>
                    <td colspan="3" align="center">
                        <?php if (isset($firma_requisicion_garantias)): ?>
                            <?php if (file_exists($firma_requisicion_garantias)): ?> 
                                <img class="marcoImg" src="<?= base_url().$firma_requisicion_garantias_url;?>" style="height:50px;width:80px;">
                            <?php endif; ?>
                        <?php endif; ?>
                    </td>
                    <td colspan="2" rowspan="2" align="center" style="font-size: 9px;">
                        FIRMA DE GARANTÍAS: <br>
                        <?= ((isset($termina_requisicion_garantias)) ? $termina_requisicion_garantias : "") ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <br>
                    </td>
                    <td colspan="3" align="center" style="font-size: 11px;">
                        <br>
                    </td>
                    
                    <td colspan="3" align="center" style="border-top: 0.5px solid black;font-size: 11px;">
                        <?= ((isset($garantias_nombre)) ? $garantias_nombre : "Diosselim Neptali Galindo Jimenez") ?>
                    </td>
                    
                </tr>
                <tr>
                   <td colspan="11">
                       <br>
                   </td> 
                </tr>
            </table>
        </div>
    </body>
</html>

<?php 
    try {
        $html = ob_get_clean(); 
        ob_clean();

        $mpdf = new \mPDF('utf-8', 'A4-P');
        //$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-P','debug' => TRUE]);
        
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->WriteHTML($html);
        
        //$mpdf->AddPage();
        //$hoja_2 = '<br>';
        //$mpdf->WriteHTML($hoja_2);
        
        $mpdf->Output();

    //} catch (Html2PdfException $e) {
    } catch (Exception $e) {
        //$html2pdf->clean();
        //$formatter = new ExceptionFormatter($e);
        //echo $formatter->getHtmlMessage(); 
        echo "No se pudo cargar el PDF";
        echo $e;
    }

 ?>