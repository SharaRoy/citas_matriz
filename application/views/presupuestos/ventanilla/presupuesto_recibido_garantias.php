<div style="margin:10px;">
    <!--<form id="formulario" method="post" action="<?=base_url()."presupuestos/Presupuestos/validateFormR2"?>" autocomplete="on" enctype="multipart/form-data">-->
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:100%;">
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="input_field" name="folio_externo" id="folio_externo" value="<?php if(set_value('folio_externo') != "") echo set_value('folio_externo'); elseif(isset($folio_externo)) echo $folio_externo; ?>" style="width:100%;">
                <?php echo form_error('folio_externo', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php if(set_value('noSerie') != "") echo set_value('noSerie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;" disabled>
                <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;">
                <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;">
                <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;">
                <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;">
                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;">
                <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <div class="row">
            <!-- tabla meramente informativa -->
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:95%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REP.
                            </td>
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                CANTIDAD
                            </td>
                            <td style="width:47%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:21%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                PRECIO PÚBLICO
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                COSTO MO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                TOTAL REFACCIÓN
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                ENTREGADA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                FECHA ENTREGA
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($renglon)): ?>
                            <!-- Comprobamos que haya registros guardados -->
                            <?php if (count($renglon)>0): ?>
                                <?php $total = 0; ?>
                                <?php if ($dirige == "Requisicion_T"): ?>
                                    <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                        <!-- Imprimimos solo las refacciones que no tienen garantia -->
                                        <?php if (($pza_garantia[$index] == "0")&&($autoriza_cliente[$index] == "1")): ?>
                                            <tr id='fila_<?php echo $index+1; ?>' style="background-color: <?= (($autoriza_cliente[$index] == '1') ? '#c7ecc7' : 'f5acaa') ?>; ">
                                                <!-- REP -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $referencia[$index]; ?></label>
                                                </td>
                                                    <!-- CANTIDAD -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $cantidad[$index]; ?></label>
                                                </td>
                                                <!-- D E S C R I P C I Ó N -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $descripcion[$index]; ?></label>
                                                </td>
                                                <!-- NUM. PIEZA -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $num_pieza[$index]; ?></label>
                                                </td>
                                                <!-- PRECIO PÚBLICO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($costo_pieza[$index],2); ?></label>
                                                </td>
                                                <!-- HORAS -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($horas_mo[$index],2); ?></label>
                                                </td>
                                                <!-- COSTO MO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($costo_mo[$index],2); ?></label>
                                                    
                                                    <?php $total_renglon = ((float)$cantidad[$index] *(float)$costo_pieza[$index] ) + ((float)$horas_mo[$index] *(float)$costo_mo[$index] ); ?>
                                                </td>
                                                <!-- TOTAL REFACCIÓN -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($total_renglon); ?></label>
                                                </td>
                                                <!-- DATOS ENTREGA REFACCION -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">
                                                        <?php if ($estado_entrega[$index] == "3"): ?>
                                                            SI
                                                        <?php else: ?>
                                                            NO
                                                        <?php endif ?>
                                                    </label>
                                                </td>
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">
                                                         <?php if ($estado_entrega[$index] == "3"): ?>
                                                            <?= $fecha_entrega[$index]; ?>
                                                        <?php else: ?>
                                                            <br>
                                                        <?php endif ?>
                                                    </label>
                                                </td>
                                            </tr>
                                            <?php $total += $total_renglon; ?> 

                                        <?php elseif (($pza_garantia[$index] == "1")&&($autoriza_garantia[$index] == "1")): ?>
                                            <tr id='fila_<?php echo $index+1; ?>' style="background-color: #97e2ee;" >
                                                <!-- REP -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $referencia[$index]; ?></label>
                                                </td>
                                                <!-- CANTIDAD -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $cantidad[$index]; ?></label>
                                                </td>
                                                <!-- D E S C R I P C I Ó N -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $descripcion[$index]; ?></label>
                                                </td>
                                                <!-- NUM. PIEZA -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $num_pieza[$index]; ?></label>
                                                </td>
                                                <!-- PRECIO PÚBLICO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$<?php echo number_format($costo_pieza[$index],2); ?></label>
                                                </td>
                                                <!-- HORAS -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$<?php echo number_format($horas_mo[$index],2); ?></label>
                                                </td>
                                                <!-- COSTO MO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$<?php echo number_format($costo_mo[$index],2); ?></label>
                                                    
                                                    <?php $total_renglon = ((float)$cantidad[$index] *(float)$costo_pieza[$index] ) + ((float)$horas_mo[$index] *(float)$costo_mo[$index] ); ?>
                                                </td>
                                                <!-- TOTAL REFACCIÓN -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">$<?php echo $costo_pieza[$index]; ?></label>
                                                </td>
                                                <!-- DATOS ENTREGA REFACCION -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">
                                                        <?php if ($estado_entrega[$index] == "3"): ?>
                                                            SI
                                                        <?php else: ?>
                                                            NO
                                                        <?php endif ?>
                                                    </label>
                                                </td>
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">
                                                         <?php if ($estado_entrega[$index] == "3"): ?>
                                                            <?= $fecha_entrega[$index]; ?>
                                                        <?php else: ?>
                                                            <br>
                                                        <?php endif ?>
                                                    </label>
                                                </td>
                                            </tr>
                                            <?php $total += $total_renglon; ?>                                         
                                        <?php endif ?>
                                    <?php endfor; ?>
                                <?php else: ?>
                                    <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                        <?php if (($pza_garantia[$index] == "1")&&($autoriza_garantia[$index] == "1")): ?>
                                            <tr id='fila_<?php echo $index+1; ?>' style="background-color: #97e2ee;" >
                                                <!-- REP -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $referencia[$index]; ?></label>
                                                </td>
                                                <!-- CANTIDAD -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $cantidad[$index]; ?></label>
                                                </td>
                                                <!-- D E S C R I P C I Ó N -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $descripcion[$index]; ?></label>
                                                </td>
                                                <!-- NUM. PIEZA -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;"><?php echo $num_pieza[$index]; ?></label>
                                                </td>
                                                <!-- PRECIO PÚBLICO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$<?php echo number_format($costo_pieza[$index],2); ?></label>
                                                </td>
                                                <!-- HORAS -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$<?php echo number_format($horas_mo[$index],2); ?></label>
                                                </td>
                                                <!-- COSTO MO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$<?php echo number_format($costo_mo[$index],2); ?></label>
                                                    
                                                    <?php $total_renglon = ((float)$cantidad[$index] *(float)$costo_pieza[$index] ) + ((float)$horas_mo[$index] *(float)$costo_mo[$index] ); ?>
                                                </td>
                                                <!-- TOTAL REFACCIÓN -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">$<?php echo $costo_pieza[$index]; ?></label>
                                                </td>
                                                <!-- DATOS ENTREGA REFACCION -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">
                                                        <?php if ($estado_entrega[$index] == "3"): ?>
                                                            SI
                                                        <?php else: ?>
                                                            NO
                                                        <?php endif ?>
                                                    </label>
                                                </td>
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">
                                                         <?php if ($estado_entrega[$index] == "3"): ?>
                                                            <?= $fecha_entrega[$index]; ?>
                                                        <?php else: ?>
                                                            <br>
                                                        <?php endif ?>
                                                    </label>
                                                </td>
                                            </tr>
                                            <?php $total += $total_renglon; ?>                                         
                                        <?php endif ?>
                                    <?php endfor; ?>
                                <?php endif ?>

                                    
                            <?php else: ?>
                                <tr>
                                    <td colspan="15" align="center">
                                        No se cargaron los datos
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="15" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php if (isset($dirige)): ?>
            <hr>
            <!-- comentarios de ventanilla -->
            <div class="row">
                <div class="col-sm-6" align="left">
                    <h4>Notas Garantías:</h4>
                    <label for="" style="font-size:12px;"><?php if(isset($comentario_garantia)){ if($comentario_garantia != "") echo $comentario_garantia; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                </div>
                <div class="col-sm-6" align="right">
                    <table style="width:100%; font-size: 14px;" id="comentarios_registrados">
                        <?php if (isset($valida_uso)): ?>
                            <?php if (count($valida_uso)>0): ?>
                                <?php $validar = 0; ?>
                                <?php for ($index  = 0; $index < count($valida_uso); $index++): ?>
                                    <?php if ($valida_uso[$index] != ""): ?>
                                        <tr>
                                            <td style='width: 60%;font-size: 12px;color: black;text-align: justify;' rowspan='2'>
                                                <?= $index+1 ?> ) Se inspeccionaron las partes reemplazadas <span style='color:darkblue;'><?= $req_pza_parte[$index] ?></span> en la acción de servicio #<span style='color:darkblue;'><?= $valida_uso[$index] ?></span> y se verificó que fueran reemplazadas.
                                            </td>
                                            <td style='width: 40%;border-bottom: 0.5px solid black;font-size: 11px;color: black;'  align="center">
                                                <img class="marcoImg" src="<?= base_url().$req_pza_firma[$index] ?>" style="height:1cm;width:2cm;">

                                                <br>
                                                <span style='color:darkblue;'><?= $req_pza_gerente[$index] ?></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='font-size: 10px;color: black;' align="center">
                                                Gerente de servicio
                                            </td>
                                        </tr>
                                        <?php $validar = 1; ?>
                                    <?php endif ?>
                                <?php endfor; ?>

                                <?php if ($validar == 0): ?>
                                    <tr>
                                        <td colspan="2" align="center">
                                            NO SE CARGARON COMENTARIOS DE INSTALACIÓN
                                        </td>
                                    </tr>
                                <?php endif ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="2" align="center">
                                        NO SE CARGARON COMENTARIOS DE INSTALACIÓN
                                    </td>
                                </tr>
                            <?php endif ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="2" align="center">
                                    NO SE CARGARON COMENTARIOS DE INSTALACIÓN
                                </td>
                            </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        <?php endif ?>

        <!-- identificamos el procedimiento que se realiza-->
        <?php if ($dirige == "Recibido_G"): ?>
            <?php if (isset($firma_requisicion_garantias)): ?>
                <hr>
                <div class="row">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4" align="center" style="border: 0.5px solid darkblue;">
                        <table style="width: 90%;margin-left: 10px;margin-right: 10px;">
                            <tr>
                                <td align="center">
                                    <h4>Firma de garantía</h4>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <?php if (isset($firma_requisicion_garantias)): ?>
                                        <?php if ($firma_requisicion_garantias_url == ""): ?>
                                            <img class="marcoImg" src="<?php if(set_value('rutaFirmaReq') != "") echo set_value('rutaFirmaReq');  ?>" id="firmaFirmaReq" alt="" style="height:2cm;width:4cm;">
                                            <br>
                                        <?php else: ?>
                                            <img class="marcoImg" src="<?php if(isset($firma_requisicion_garantias)) echo base_url().$firma_requisicion_garantias_url;?>" id="firmaFirmaReq" alt="" style="height:2cm;width:4cm;">
                                            <br>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <img class="marcoImg" src="<?php if(set_value('rutaFirmaReq') != "") echo set_value('rutaFirmaReq');  ?>" id="firmaFirmaReq" alt="" style="height:2cm;width:4cm;">
                                        <br>
                                    <?php endif; ?>

                                    <input type="text" class="input_field" name="nombre_encargado" value="Diosselim Neptali Galindo Jimenez" style="width:100%;">
                                    <?php echo form_error('rutaFirmaReq', '<span class="error">', '</span>'); ?><br>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <?php if (isset($firma_requisicion_garantias)): ?>
                                        <?php if ($firma_requisicion_garantias_url == ""): ?>
                                            <a id="Req_firma" class="cuadroFirma btn btn-primary" data-value="Req" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma</a>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <a id="Req_firma" class="cuadroFirma btn btn-primary" data-value="Req" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma </a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-4"></div>
                </div>
            <?php endif ?>
        <?php endif ?>
        
    <form id="firma_Requisicion" method="post" autocomplete="on" enctype="multipart/form-data">
        <br>
        <div class="row">
            <div class="col-sm-12" align="center">
                <i class="fas fa-spinner cargaIcono"></i>
                <h4 class="error" id="errorEnvio"></h4>

                <!-- Datos basicos -->
                <input type="hidden" name="destino_vista" value="G">
                <input type="hidden" name="ruta_destino" value="2">
                <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>"> 
                <input type="hidden" name="id_cita" id="id_cita" value="<?php if(isset($id_cita)) echo $id_cita; ?>"> 
                <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="9">
                <input type="hidden" name="" id="origen" value="Presupuesto">
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
                <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

                <input type="hidden" id="rutaFirmaReq" name="rutaFirmaReq" value="<?php if(isset($firma_requisicion)) echo $firma_requisicion;?>" >

                <!-- validamos los envios de cada rol -->
                <input type="hidden" name="envioRefacciones" id="envioRefacciones" value="<?php if(isset($envia_ventanilla)) echo $envia_ventanilla; else echo "0"; ?>">
                <input type="hidden" name="" id="envioJefeTaller" value="<?php if(isset($envio_jdt)) echo $envio_jdt; else echo "0"; ?>">
                <input type="hidden" name="" id="envioGarantias" value="<?php if(isset($envio_garantia)) echo $envio_garantia; else echo "0"; ?>">

                <input type="hidden" name="" id="validaFirma_2" value="<?php if(isset($firma_requisicion)) {echo $firma_requisicion; } ?>">
                <input type="hidden" name="" id="validaFirma_1" value="<?php if(isset($firma_requisicion_garantias)) {echo $firma_requisicion_garantias; } ?>">

                <?php if ($dirige == "Requisicion_T"): ?>
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Tecnico/4";?>';">
                        Regresar
                    </button>
                <?php endif ?>

                <!--<?php if (isset($firma_requisicion)): ?>
                    <?php if ($firma_requisicion == "0"): ?>
                        <input type="button" id="envio_firma_req" class="btn btn-success terminarCotizacion12" style="margin-left: 20px;" name="aceptarCotizacion" value="Guardar Firma">
                    <?php elseif ($dirige == "Requisicion_G"): ?>
                        <input type="button" id="envio_firma_req" class="btn btn-success terminarCotizacion12" style="margin-left: 20px;" name="aceptarCotizacion" value="Finalizar y generar PDF">
                    <?php else: ?>
                        <input type="button" id="envio_firma_req" class="btn btn-success terminarCotizacion12" style="margin-left: 20px;" name="aceptarCotizacion" value="Guardar Firma++">
                    <?php endif ?>
                <?php endif ?>-->

                <?php if (isset($firma_requisicion_garantias)): ?>
                    <?php if ($firma_requisicion_garantias == "0"): ?>
                        <input type="button" id="envio_firma_req" class="btn btn-success terminarCotizacion12" style="margin-left: 20px;" name="aceptarCotizacion" value="Guardar Firma">
                    <?php elseif (($dirige == "Recibido_G")&&($firma_requisicion_garantias == "1")): ?>
                        <input type="button" id="envio_firma_req" class="btn btn-success terminarCotizacion12" style="margin-left: 20px;" name="aceptarCotizacion" value="Finalizar y generar PDF">
                    <?php else: ?>
                        Firmado y generado
                    <?php endif ?>
                <?php endif ?>
                <br><br><br>
            </div>
        </div>
            
    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="inspeccion_piezas" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" align="center">
                <br><br>
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <h4>Refacciones por comentar:</h4>
                        <br>
                        <select class="form-control" id="vaciado_refacciones" style="font-size:12px;width:100%;">
                            <option value="0">Seleccionar.. </option>
                            <?php if (isset($renglon)): ?>
                                <?php if (count($renglon)>0): ?>
                                    <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                        <?php if (($pza_garantia[$index] == "1")&&($autoriza_garantia[$index] == "1")&&($valida_uso[$index] == "")): ?>
                                            <option value="<?= $id_refaccion[$index].'||'.$num_pieza[$index] ?>"><?= "[".$num_pieza[$index]."] ".$descripcion[$index] ?></option>
                                        <?php endif ?>
                                    <?php endfor; ?>
                                <?php endif ?>
                            <?php endif; ?>
                        </select>
                        <input type="hidden" id="parte_id_refaccion"  value="" >
                    </div>
                    <div class="col-sm-2"></div>
                </div>

                <br>
                <hr>
                <div class="row" style="font-size:13px;">
                    <div class="col-sm-1"></div>

                    <div class="col-sm-10">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 100%; font-size: 14px; text-align: justify;">
                                    Comentario: <br>
                                    Se inspeccionaron las partes reemplazadas : <input type="text" class="input_field" id="comentario_pieza" value="" style="width:3.5cm;"> en la acción de servicio : <input type="text" class="input_field" id="comentario_asc" value="" style="width:4.5cm;"> y se verificó que fueran reemplazadas.
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <hr>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <h4>Firma gerente de servicio</h4>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img class="marcoImg" src="" id="firma_parte_req" alt="" style="height:2cm;width:4cm;">
                                    <input type="hidden" id="ruta_parte_req"  value="" >
                                    
                                    <br><br>

                                    <input type="text" class="input_field" id="nombre_gerente" value="Jorge muñoz serrano" style="width:80%;">
                                    <br>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <br>
                                    <a id="Req_firma_gerente" class="cuadroFirma btn btn-primary" data-value="Req_firma_gerente" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firmar </a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-sm-1"></div>
                </div>

                <br>
                <div class="row" align="left">
                    <div class="col-sm-12" align="center">
                        <i class="fas fa-spinner cargaIcono" id="carga_3"></i>
                        <br>
                        <h5 class="error" id="error_formato"></h5>
                        <input type="hidden" id="registro_diag" value="">

                        <input type="button" id="enviar_comentario_req" class="btn btn-success" value="Guardar">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" id="cancelar" name="cancelar">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>