<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Presupuestos Multipunto</h3>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel/5";?>';">
                        Regresar
                    </button>
                </div>
            </div>

            <br>
            <div class="panel panel-default" align="center">
                <div class="panel-body" style="border:2px solid black;">
                    <h4 style="color:darkblue;text-align: left;">Busqueda por campos</h4>
                    <div class="row" style="border: 1px solid gray;padding:10px;">
                        <div class="col-sm-3" align="left">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>

                        <div class="col-sm-3" align="left">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>

                        <div class="col-sm-3" align="left">
                            <h5>Busqueda Campo:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_gral" placeholder="Buscar...">
                            </div>
                            <input type="hidden" name="" id="indicador" value="JefeTaller">
                        </div>

                        <div class="col-sm-2" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_asesor_table" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <h4 style="color:darkblue;text-align: left;">Busqueda por estatus de refacciones</h4>
                    <div class="row" style="border: 1px solid gray;padding:10px;">
                        <div class="col-md-4"><br></div>
                        <div class="col-md-4" align="left">
                            <h5 for="">Estado ventanilla: </h5>
                            <select class="form-control" id="filtro_ventanillas" style="font-size:12px;width:100%;">
                                <option value="">SELECCIONE ...</option>
                                <option value="0" style="">SIN SOLICITAR</option>
                                <option value="1" style="background-color: #f5ff51;">SOLICIATADAS</option>
                                <option value="2" style="background-color: #5bc0de;">RECIBIDAS</option>
                                <option value="3" style="background-color: #c7ecc7;">ENTREGADAS</option>
                            </select>
                        </div>

                        <div class="col-md-4" align="right">
                            <br><br>
                            <button class="btn btn-dark" type="button" id="btnBusqueda_exportar_ventanilla" style="margin-right: 8px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-dark" type="button" id="btnLimpiar" onclick="location.reload()" style="margin-right: 8px;">
                                <i class="fa fa-trash"></i>
                            </button>

                            <a href="" class="btn btn-dark" title="Exportar Resultado" id="exportar" target="_blank" hidden>
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a> 
                        </div>
                    </div>

                    <br>
                    <i class="fas fa-spinner cargaIcono"></i>

                    <table <?php if ($presupuestos != NULL) echo 'id="bootstrap-data-table2"'; ?> class="table table-striped table-bordered table-responsive" style="width: 100%;">
                        <thead>
                            <tr style="background-color: #eee;">
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>NO. ORDEN</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>TIPO PRESUPUESTO</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>ORDEN INTELISIS</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA RECEPCIÓN</strong></td>
                                <!--<td align="center" style="width: 10%;vertical-align: middle;"><strong>SERIE</strong></td>-->
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>VEHÍCULO</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>APRUEBA CLIENTE</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>EDO. REFACCIONES</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                            </tr>
                        </thead>
                        <tbody id="imprimir_busqueda">
                            <?php if ($presupuestos != NULL): ?>
                                <?php foreach ($presupuestos as $i => $registro): ?>
                                    <tr style="font-size:12px;">
                                        <td align="center" style="vertical-align: middle;background-color: <?= (($registro->envia_ventanilla == '1') ? "#68ce68;" : (($registro->envia_ventanilla == '2') ? "#b776e6;color:white;" : "#fffff;")) ?>">
                                            <?= $registro->id_cita ?>
                                            <?= (($registro->envia_ventanilla == '2') ? '<p style="font-size:9px;color:blue;">Se envio al asesor</p>' : '' ) ?>
                                            <?= (($registro->envia_ventanilla == '3') ? '<p style="font-size:9px;color:blue;">Refacción agregada de garantías</p>' : '' ) ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;background-color: <?= (($registro->solicita_psni != '0') ? "#7fb3d5;" : "#fffff;") ?>">
                                            <?= (($registro->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")  ?>
                                            <?= (($registro->solicita_psni != '0') ? '<br>Solicitud PSNI' : '' ) ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->folio_externo ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php 
                                                $fecha = new DateTime($registro->fecha_recepcion.' 00:00:00');
                                                echo $fecha->format('d-m-Y');
                                             ?>
                                        </td>
                                        <!--<td align="center" style="vertical-align: middle;">
                                            <?= $registro->serie ?>
                                        </td>-->
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->vehiculo ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->placas ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->asesor ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->tecnico ?>
                                        </td>

                                        <td align="center" style="width:10%;vertical-align: middle;">
                                            <?php if ($registro->acepta_cliente != ""): ?>
                                                <?php if ($registro->acepta_cliente == "Si"): ?>
                                                    <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                <?php elseif ($registro->acepta_cliente == "Val"): ?>
                                                    <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                <?php else: ?>
                                                    <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                            <?php endif; ?>

                                            <?php 
                                                if ($registro->acepta_cliente != "No") {
                                                    if ($registro->estado_refacciones == "0") {
                                                        $color = "background-color:#f5acaa;";
                                                    } elseif ($registro->estado_refacciones == "1") {
                                                        $color = "background-color:#f5ff51;";
                                                    } elseif ($registro->estado_refacciones == "2") {
                                                        $color = "background-color:#5bc0de;";
                                                    }else{
                                                        $color = "background-color:#c7ecc7;";
                                                    }
                                                } else {
                                                    $color = "background-color:red;color:white;";
                                                }
                                             ?>
                                        </td>

                                        <td align="center" style="vertical-align: middle; <?= $color ?>">
                                                <?php if (($registro->estado_refacciones == "0")&&($registro->acepta_cliente != "No")): ?>
                                                    SIN SOLICITAR
                                                <?php elseif ($registro->acepta_cliente == "No"): ?>
                                                    RECHAZADA
                                                <?php elseif ($registro->estado_refacciones == "1"): ?>
                                                    SOLICITADAS
                                                <?php elseif ($registro->estado_refacciones == "2"): ?>
                                                    RECIBIDAS
                                                <?php elseif ($registro->estado_refacciones == "3"): ?>
                                                    ENTREGADAS
                                                <?php else: ?>
                                                    SIN SOLICITAR *
                                                <?php endif ?>
                                            </td>

                                        <td align="center" style="width:15%;vertical-align: middle;">
                                            <?php 
                                                $id = (double)$registro->id_cita*CONST_ENCRYPT;
                                                $url_id = base64_encode($id);
                                                $url = str_replace("=", "" ,$url_id);
                                            ?>

                                            <a href="<?=base_url().'OrdenServicio_Revision/'.$url;?>" class="btn btn-warning" target="_blank" style="font-size: 12px;">ORDEN</a>
                                        </td>

                                        <td align="center" style="vertical-align: middle;">
                                            <?php if (($registro->envia_ventanilla != '0')&&($registro->envia_ventanilla != '3')): ?>
                                                <a href="<?=base_url()."Presupuesto_Ventanilla/".$url;?>" class="btn btn-info" style="color:white;font-size: 12px;">VER</a>
                                            <?php else: ?>
                                                <a href="<?=base_url()."Presupuesto_Ventanilla/".$url;?>" class="btn btn-primary" style="color:white;font-size: 12px;">REVISAR</a>
                                            <?php endif ?>
                                        </td>

                                        <td align="center" style="vertical-align: middle;">
                                            <?php if (($registro->acepta_cliente != "")&&($registro->acepta_cliente != "No")&&($registro->envia_ventanilla != '3')): ?>
                                                <?php if ($registro->estado_refacciones == "0"): ?>
                                                    <button type="button" class="btn" style="background-color: #f5ff51;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$url;?>';">
                                                        SOLICITAR<br>REFACCIÓN
                                                    </button>
                                                <?php elseif ($registro->estado_refacciones == "1"): ?>
                                                    <button type="button" class="btn" style="background-color: #c7ecc7;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$url;?>';">
                                                        RECIBIR<br>REFACCIÓN
                                                    </button>
                                                <?php elseif ($registro->estado_refacciones == "2"): ?>
                                                    <button type="button" class="btn" style="background-color: #5bc0de;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$url;?>';">
                                                        ENTREGAR<br>REFACCIÓN
                                                    </button>
                                                <?php else: ?>
                                                    <button type="button" class="btn btn-success" style="font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$url;?>';">
                                                        FINALIZADO
                                                    </button>
                                                <?php endif ?>
                                            <?php endif ?>
                                        </td>

                                        <td align="center" style="width:15%;vertical-align: middle;">
                                            <?php if (($registro->acepta_cliente != "")&&($registro->acepta_cliente != "No")): ?>
                                                <?php if ($registro->firma_requisicion != ''): ?>
                                                    <a href="<?=base_url()."Requisicion_T/".$url;?>" class="btn btn-success" style="color:white;font-size: 10px;" target="_blank">REQUISICIÓN</a>
                                                <?php else: ?>
                                                    <a href="<?=base_url()."Requisicion_T/".$url;?>" class="btn btn-danger" style="color:white;font-size: 10px;" target="_blank">REQUISICIÓN<br> PENDIENTE</a>
                                                <?php endif ?>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="14" style="width:100%;" align="center">
                                        <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                        <h4>Sin registros que mostrar</h4>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>


<script>
    $('#bootstrap-data-table2').DataTable({
        "ordering": false,
        "searching": false,
        "bPaginate": false,
        "paging": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false
    });


    $("#btnBusqueda_exportar_ventanilla").on('click', function (e){
        var campo = $('#busqueda_gral').prop('value');
        var estado_ref = $('#filtro_ventanillas').prop('value');
        var fecha_inicio = $('#fecha_inicio').prop('value');
        var fecha_fin = $('#fecha_fin').prop('value');

        // Evitamos que salte el enlace.
        e.preventDefault();

        if (estado_ref != "") {
            var paqueteDeDatos = new FormData();
            paqueteDeDatos.append('fecha_inio', fecha_inicio);
            paqueteDeDatos.append('fecha_fin', fecha_fin);
            paqueteDeDatos.append('campo', campo);
            paqueteDeDatos.append('estado', estado_ref);

            var base = $("#basePeticion").val();
            $(".cargaIcono").css("display","inline-block");

            $.ajax({
                url: base+"presupuestos/PresupuestosBusquedas/buscador_ventanilla_estatus",
                type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
                contentType: false,
                data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
                processData: false,
                cache: false,
                success:function(resp){
                    //console.log(resp);
                    var data = JSON.parse(resp);
                    //console.log(data);

                    llenar_tabla(data);

                    if (estado_ref != "") {
                        $("#exportar").attr('href',base+'Exportar_Refacciones/'+estado_ref+"."+fecha_inicio+"."+fecha_fin);
                        $("#exportar").attr('hidden',false);
                    }else{
                        $("#exportar").attr('hidden',true);
                    }

                    $(".cargaIcono").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $(".cargaIcono").css("display","none");
                    $('#errorVideo').text("Error de carga VERIFICAR.");
                //Cierre del error
                }
            //Cierre del ajax
            });
        }else{

        }
    });

    $("#btnBusqueda_asesor_table").on('click', function (e){
        var campo = $('#busqueda_gral').prop('value');
        var fecha_inicio = $('#fecha_inicio').prop('value');
        var fecha_fin = $('#fecha_fin').prop('value');

        // Evitamos que salte el enlace.
        e.preventDefault();

        if ((fecha_inicio != "")||(campo != "")) {
            var paqueteDeDatos = new FormData();
            paqueteDeDatos.append('fecha_inio', fecha_inicio);
            paqueteDeDatos.append('fecha_fin', fecha_fin);
            paqueteDeDatos.append('campo', campo);

            var base = $("#basePeticion").val();
            $(".cargaIcono").css("display","inline-block");
            $.ajax({
                url: base+"presupuestos/PresupuestosBusquedas/buscador_ventanilla",
                type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
                contentType: false,
                data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
                processData: false,
                cache: false,
                success:function(resp){
                    //console.log(resp);
                    var data = JSON.parse(resp);
                    //console.log(data);

                    llenar_tabla(data);

                    $(".cargaIcono").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $(".cargaIcono").css("display","none");
                    $('#errorVideo').text("Error de carga VERIFICAR.");
                //Cierre del error
                }
            //Cierre del ajax
            });
        }else{

        }
    });

    function llenar_tabla(data){
        var tabla = $("#imprimir_busqueda");
        tabla.empty();

        var base = $("#basePeticion").val();

        if (data.length > 0) {
            for(i = 0; i < data.length; i++){
                var revisar = "";
                if ((data[i]["acepta_cliente"] != "")&&(data[i]["acepta_cliente"] != "No")) {
                    if (data[i]["estado_refacciones"] == "0") {
                        revisar = "<a class='btn' style='background-color: #f5acaa;font-size: 10px;' href='"+base+"Presupuesto_Tradicional/"+data[i].id_cita_url+"'>"+
                                    "SOLICITAR<br>REFACCIÓN"+
                                "</a>";
                    }else if(data[i]["estado_refacciones"] == "1"){
                        revisar = "<a class='btn' style='background-color: #f5ff51;font-size: 10px;' href='"+base+"Presupuesto_Tradicional/"+data[i].id_cita_url+"'>"+
                                    "RECIBIR<br>REFACCIÓN"+
                                "</a>";
                    }else if(data[i]["estado_refacciones"] == "2"){
                        revisar = "<a class='btn' style='background-color: #c7ecc7;font-size: 10px;' href='"+base+"Presupuesto_Tradicional/"+data[i].id_cita_url+"'>"+
                                    "ENTREGAR<br>REFACCIÓN"+
                                "</a>";
                    }else{
                        revisar = "<a class='btn btn-success' style='font-size: 10px;' href='"+base+"Presupuesto_Cliente/"+data[i].id_cita_url+"'>"+
                                    "FINALIZADO"+
                                "</a>";
                    }
                }

                var editar = "";
                if ((data[i]["envia_ventanilla"] != "0")&&(data[i]["envia_ventanilla"] != "3")) {
                    editar = "<a class='btn btn-info' style='color:white;font-size:10px;' href='"+base+"Presupuesto_Ventanilla/"+data[i].id_cita_url+"'>VER</a>";
                } else {
                    editar = "<a class='btn btn-primary' style='color:white;font-size:10px;' href='"+base+"Presupuesto_Ventanilla/"+data[i].id_cita_url+"'>REVISAR</a>";
                }

                var apruebaCliente = '';
                if (data[i]["acepta_cliente"] != "") {
                    if (data[i]["acepta_cliente"] == 'Si') {
                        apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                    } else if(data[i]["acepta_cliente"] == 'Val'){
                        apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                    } else{
                        apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                    }
                } else{
                    apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                }

                var requisicion = "";
                if ((data[i]["acepta_cliente"] != "")&&(data[i]["acepta_cliente"] != "No")) {
                    //Verificamos si sera la firma o el pdf
                    if (data[i]["requisicion"] != "") {
                        requisicion = "<a class='btn btn-success' style='color:white;font-size: 10px;' href='"+base+"Requisicion_T/"+data[i].id_cita_url+"' target='_blank'>REQUISICIÓN</a>";
                    } else {
                        requisicion = "<a class='btn btn-danger' style='color:white;font-size: 10px;' href='"+base+"Requisicion_T/"+data[i].id_cita_url+"'  target='_blank'>REQUISICIÓN<br>FIRMA</a>";
                    }
                }

                //Creamos las celdas en la tabla
                tabla.append("<tr style='font-size:11px;'>"+
                    //NO. ORDEN
                    "<td align='center' style='width:10%;background-color:"+((data[i].envia_ventanilla == "0") ? '#ffffff' : ((data[i].envia_ventanilla == '1') ? '#68ce68' : '#b776e6;color:white'))+";vertical-align: middle;width:10%;'>"+
                        data[i]["id_cita"]+
                        ((data[i].envia_ventanilla == '2') ? "<p style='font-size:9px;color:blue;'>Se envio al asesor</p>" : "")+ "" +
                        ((data[i].envia_ventanilla == '3') ? "<p style='font-size:9px;color:blue;'>Refacción agregada de garantías</p>" : "")+ "" +
                    "</td>"+
                    //TIPO PRESUPUESTO   #7fb3d5;" : "#fffff;"
                    "<td align='center' style='vertical-align: middle;background-color:"+((data[i].psni == "0") ? '#ffffff' : '#7fb3d5')+"'>"+
                        data[i].tipo_presupuesto+
                        ((data[i].psni == "0") ? '' : '<br>Solicitud PSNI')+""+
                    "</td>"+
                    //ORDEN INTELISIS
                    "<td align='center' style='vertical-align: middle;'>"+data[i].folio_intelisis+"</td>"+
                    //FECHA RECEPCION
                    "<td align='center' style='vertical-align: middle;width:10%;'>"+data[i].fecha_formato+"</td>"+
                    //SERIE
                    //"<td align='center' style='vertical-align: middle;width:15%;'>"+data[i].serie+"</td>"+
                    //VEHÍCULO
                    "<td align='center' style='vertical-align: middle;width:15%;'>"+data[i].vehiculo+"</td>"+
                    //PLACAS
                    "<td align='center' style='vertical-align: middle;width:15%;'>"+data[i].placas+"</td>"+
                    //ASESOR
                    "<td align='center' style='vertical-align: middle;width:10%;'>"+data[i].asesor+"</td>"+
                    //TÉCNICO
                    "<td align='center' style='vertical-align: middle;width:15%;'>"+data[i].tecnico+"</td>"+
                    //APRUEBA CLIENTE
                    "<td align='center' style='vertical-align: middle;width:15%;'>"+apruebaCliente+"</td>"+
                    //EDO. REFACCIONES
                    "<td align='center' style='vertical-align: middle;width:15%;"+data[i].color_bg+"'>"+data[i].edo_entrega+"</td>"+
                    //ACCIONES
                    //VER ORDEN DE SERVICIO
                    "<td align='center' style='width:15%;vertical-align: middle;'>"+
                        "<a href='"+base+"OrdenServicio_Revision/"+data[i].id_cita_url+"' class='btn btn-warning' target='_blank' style='font-size: 10px;'>ORDEN</a>"+
                    "</td>"+
                    //ACCIONES
                    "<td align='center' style='vertical-align: middle;width:15%;'>"+
                        ""+editar+""+
                    "</td>"+
                    "<td align='center' style='vertical-align: middle;width:15%;'>"+
                        ""+revisar+""+
                    "</td>"+
                    "<td align='center' style='vertical-align: middle;width:15%;'>"+
                        ""+requisicion+""+
                    "</td>"+
                "</tr>");
            }

        } else {
            tabla.append("<tr style='font-size:14px;'>"+
                "<td align='center' colspan='11'>No se encontraron resultados</td>"+
            "</tr>");
        }
    }
</script>