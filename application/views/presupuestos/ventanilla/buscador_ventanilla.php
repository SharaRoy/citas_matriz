<div style="margin:10px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Presupuestos Multipunto</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <h4 style="color:darkblue;">Busqueda por estatus de refacciones</h4>
                    <div class="row" style="border: 1px solid gray;padding:10px;">
                        <div class="col-md-3">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <h5 for="">Estado ventanilla: </h5>
                            <select class="form-control" id="filtro_ventanillas" style="font-size:12px;width:100%;">
                                <option value="">SELECCIONE ...</option>
                                <option value="0" style="">SIN SOLICITAR</option>
                                <option value="1" style="background-color: #f5ff51;">SOLICIATADAS</option>
                                <option value="2" style="background-color: #5bc0de;">RECIBIDAS</option>
                                <option value="3" style="background-color: #c7ecc7;">ENTREGADAS</option>
                            </select>
                        </div>

                        <div class="col-md-3" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" id="btnBusqueda_exportar" style="margin-right: 8px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" id="btnLimpiar" onclick="location.reload()" style="margin-right: 8px;">
                                <i class="fa fa-trash"></i>
                            </button>

                            <a href="" class="btn btn-dark" title="Exportar Resultado" id="exportar" target="_blank" hidden>
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i>
                            </a> 
                        </div>
                    </div>

                    <h4 style="color:darkblue;">Busqueda por campos</h4>
                    <div class="row" style="border: 1px solid gray;padding:10px;">
                        <div class="col-md-1">
                            <!--<input type="button" class="btn btn-dark" style="color:white;" onclick="location.href='<?=base_url()."Panel/5";?>';" name="" value="Regresar">-->
                        </div>
                        <!-- Si la vista es de refacciones, mostramos los filtros por estatus del cliente -->
                        <div class="col-md-5">
                            <!--<h5 for="">Afectados por el cliente: </h5>
                            <select class="form-control" id="filtro_cliente" style="font-size:12px;width:100%;">
                                <option value="">SELECCIONE ...</option>
                                <option value="" style="">SIN CONFIRMAR</option>
                                <option value="Si" style="color: darkgreen; font-weight: bold;">CONFIRMADA</option>
                                <option value="No" style="color: red ; font-weight: bold;">RECHAZADA</option>
                                <option value="Val" style="color: darkorange; font-weight: bold;">DETALLADA</option>
                            </select>-->
                        </div>

                        <div class="col-md-4">
                            <h5>Busqueda No. Orden:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_ventanilla" placeholder="Buscar...">
                            </div>
                            <input type="hidden" name="" id="indicador" value="Ventanilla">
                        </div>

                        <div class="col-md-2" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_venatnilla" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="form-group" align="center" style="overflow-x: scroll;overflow-y: scroll;">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <!--<td align="center" style="width: 10%;"></td>-->
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>NO. ORDEN</strong></td>
                                    <!--<td align="center" style="width: 10%;vertical-align: middle;"><strong>ORDEN DMS</strong></td>-->
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>TIPO PRESUPUESTO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ORDEN INTELISIS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                    <!--<td align="center" style="width: 20%;"><strong>FIRMA ASESOR</strong></td>-->
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>APRUEBA CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>EDO. REFACCIONES</strong></td>
                                    <td align="center" colspan="4" style="width: 10%;vertical-align: middle;"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar"> 
                                <!-- Verificamos que se hayan cargado los datos para mostrar-->
                                <?php if (isset($id_cita)): ?>
                                    <!-- Imprimimos los datos-->
                                    <?php foreach ($id_cita as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <td align="center" style="width:10%;vertical-align: middle;background-color: <?php if ($envia_ventanilla[$index] == "1") echo "#68ce68"; elseif($envia_ventanilla[$index] == "2") echo "#b776e6;color:white"; else echo "#fffff"; ?>;">
                                                <?php echo $id_cita[$index]; ?>
                                                <?php if ($envia_ventanilla[$index] == "2"): ?>
                                                    <p style="font-size:9px;color:blue;">Se envio al asesor</p>
                                                <?php endif; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;background-color: <?php if ($peticion_psni[$index] == "1") echo "#7fb3d5"; else echo "#fffff"; ?>;">
                                                <?php echo $tipo[$index]; ?>
                                                <?php if ($peticion_psni[$index] == "1") echo "<br>Solicitud PSNI"; else echo ""; ?>
                                            </td> 
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <!--<td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $firma_asesor[$index]; ?>
                                            </td>-->

                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php if ($acepta_cliente[$index] != ""): ?>
                                                    <?php if ($acepta_cliente[$index] == "Si"): ?>
                                                        <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                    <?php elseif ($acepta_cliente[$index] == "Val"): ?>
                                                        <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                    <?php else: ?>
                                                        <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                                <?php endif; ?>
                                            </td>

                                            <td align="center" style="width:15%;vertical-align: middle; <?php echo $color[$index];?>">
                                                <?php if (($estado_refacciones[$index] == "0")&&($acepta_cliente[$index] != "No")): ?>
                                                    SIN SOLICITAR
                                                <?php elseif ($acepta_cliente[$index] == "No"): ?>
                                                    RECHAZADA
                                                <?php elseif ($estado_refacciones[$index] == "1"): ?>
                                                    SOLICITADAS
                                                <?php elseif ($estado_refacciones[$index] == "2"): ?>
                                                    RECIBIDAS
                                                <?php elseif ($estado_refacciones[$index] == "3"): ?>
                                                    ENTREGADAS
                                                <?php else: ?>
                                                    SIN SOLICITAR *
                                                <?php endif ?>
                                            </td>

                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <a href="<?=base_url().'OrdenServicio_Revision/'.$id_cita_url[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 10px;">ORDEN</a>
                                            </td>

                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <!-- si ya se edita por ventanilla -->
                                                <?php if ($envia_ventanilla[$index] != "0"): ?>
                                                    <button type="button" class="btn btn-info" style="color:white;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Ventanilla/".$id_cita_url[$index];?>';">
                                                        VER
                                                    </button>
                                                <!-- si no se ha liberado de ventanilla -->
                                                <?php else: ?>
                                                    <button type="button" class="btn btn-primary" style="color:white;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Ventanilla/".$id_cita_url[$index];?>';">
                                                        EDITAR
                                                    </button>
                                                <?php endif; ?>
                                            </td>


                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <!-- Si la cotizacion ya fue afectada y no fue rechazada (presupuesto tradicional) -->
                                                <?php if (($acepta_cliente[$index] != "")&&($acepta_cliente[$index] != "No")&&($ruta[$index] == "1")): ?>
                                                    <?php if ($estado_refacciones[$index] == "0"): ?>
                                                        <button type="button" class="btn" style="background-color: #f5ff51;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$id_cita_url[$index];?>';">
                                                            SOLICITAR<br>REFACCIÓN
                                                        </button>
                                                    <?php elseif ($estado_refacciones[$index] == "1"): ?>
                                                        <button type="button" class="btn" style="background-color: #c7ecc7;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$id_cita_url[$index];?>';">
                                                            RECIBIR<br>REFACCIÓN
                                                        </button>
                                                    <?php elseif ($estado_refacciones[$index] == "2"): ?>
                                                        <button type="button" class="btn" style="background-color: #5bc0de;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$id_cita_url[$index];?>';">
                                                            ENTREGAR<br>REFACCIÓN
                                                        </button>
                                                    <?php else: ?>
                                                        <button type="button" class="btn btn-success" style="font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Tradicional/".$id_cita_url[$index];?>';">
                                                            VER
                                                        </button>
                                                    <?php endif ?>
                                                <!-- Si es una cotizacion con garantias y no se han entregado las refacciones (liberada de garantias) -->
                                                <?php elseif ($ruta[$index] == "2"): ?>
                                                    <?php if ($estado_refacciones[$index] == "0"): ?>
                                                        <button type="button" class="btn" style="background-color: #f5ff51;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Garantias/".$id_cita_url[$index];?>';">
                                                            SOLICITAR<br>REFACCIÓN
                                                        </button>
                                                    <?php elseif ($estado_refacciones[$index] == "1"): ?>
                                                        <button type="button" class="btn" style="background-color: #c7ecc7;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Garantias/".$id_cita_url[$index];?>';">
                                                            RECIBIR<br>REFACCIÓN
                                                        </button>
                                                    <?php elseif ($estado_refacciones[$index] == "2"): ?>
                                                        <button type="button" class="btn" style="background-color: #5bc0de;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Garantias/".$id_cita_url[$index];?>';">
                                                            ENTREGAR<br>REFACCIÓN
                                                        </button>
                                                    <?php else: ?>
                                                        <button type="button" class="btn btn-success" style="font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Garantias/".$id_cita_url[$index];?>';">
                                                            VER
                                                        </button>
                                                    <?php endif ?>
                                                <!-- Si es una cotizacion viejita que solo se visualizara -->
                                                <?php elseif ($ruta[$index] == "3"): ?>

                                                    <?php if (($estado_refacciones[$index] != "3")&&($acepta_cliente[$index] != "")&&($acepta_cliente[$index] != "No")): ?>
                                                        <button type="button" class="btn btn-secondary" style="color:white;" onclick="location.href='<?=base_url()."Cotizacion_Refacciones/".$id_cita_url[$index];?>';">Soliciar/Entregar refacción</button>
                                                    <?php else: ?>
                                                        <button type="button" class="btn btn-info" style="font-size: 10px;" onclick="location.href='<?=base_url()."Cotizacion_Edita/".$id_cita_url[$index];?>';">
                                                            VER
                                                        </button>
                                                    <?php endif ?> 

                                                <!-- si ya se afecto por el cliente -->
                                                <?php elseif (($acepta_cliente[$index] != "")&&($ruta[$index] == "1")): ?>
                                                    <button type="button" class="btn btn-danger" style="color:white;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Cliente/".$id_cita_url[$index];?>';">
                                                        VER
                                                    </button>  
                                                <!-- si ya se edita por ventanilla -->
                                                <?php elseif ($envia_ventanilla[$index] != "0"): ?>
                                                    <button type="button" class="btn btn-info" style="color:white;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Ventanilla/".$id_cita_url[$index];?>';">
                                                        VER
                                                    </button>
                                                <!-- si no se ha liberado de ventanilla -->
                                                <?php else: ?>
                                                    <button type="button" class="btn btn-primary" style="color:white;font-size: 10px;" onclick="location.href='<?=base_url()."Presupuesto_Ventanilla/".$id_cita_url[$index];?>';">
                                                        EDITAR
                                                    </button>
                                                <?php endif; ?>
                                            </td>
    

                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <!--Requisicion para presupuestos tradicionales -->
                                                <?php if (($acepta_cliente[$index] != "")&&($acepta_cliente[$index] != "No")): ?>
                                                    <?php if ($firma_requisicion[$index] != ""): ?>
                                                        <a href="<?=base_url()."Requisicion_T/".$id_cita_url[$index];?>" class="btn btn-secondary" style="color:white;font-size: 10px;" target="_blank">REQUISICIÓN</a>
                                                    <?php else: ?>
                                                        <a href="<?=base_url()."Requisicion_Firma_T/".$id_cita_url[$index];?>" class="btn btn-success" style="color:white;font-size: 10px; display:none;">REQUISICIÓN<br>FIRMA</a>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <!-- Si no se cargaron los datos para mostrar -->
                                <?php else: ?>
                                    <tr>
                                        <td colspan="9" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="rolVista" value="listaVentanilla">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

