<div style="margin:10px;">
    <form id="formulario" method="post" autocomplete="on" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:100%;" disbled>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="input_field" name="folio_externo" id="folio_externo" value="<?php if(set_value('folio_externo') != "") echo set_value('folio_externo'); elseif(isset($folio_externo)) echo $folio_externo; ?>" style="width:100%;">
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php if(set_value('noSerie') != "") echo set_value('noSerie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;" disbled>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;" disbled>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;" disbled>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;" disbled>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;" disbled>
            </div>
        </div>

        <br>
        <!-- identificamos el procedimiento que se realiza-->
        <?php if ($dirige == "Entrega_T"): ?>
            <?php if (isset($estado_refacciones)): ?>
                <?php if ($estado_refacciones == "1"): ?>
                    <div class="alert alert-primary" align="center">
                        <strong style="font-size:13px !important;color:darkblue;">
                            REFACCIONES SOLICIATADAS
                        </strong>
                    </div>
                <?php elseif ($estado_refacciones == "2"): ?>
                    <div class="alert alert-primary" align="center">
                        <strong style="font-size:13px !important;color:darkblue;">
                            REFACCIONES RECIBIDAS
                        </strong>
                    </div>
                <?php elseif ($estado_refacciones == "3"): ?>
                    <div class="alert alert-primary" align="center">
                        <strong style="font-size:13px !important;color:darkblue;">
                            REFACCIONES ENTREGADAS
                        </strong>
                    </div>
                <?php endif ?>
            <?php endif ?>
        <?php elseif ($dirige == "Entrega_G"): ?>
            <?php if (isset($estado_refacciones_garantias)): ?>
                <?php if ($estado_refacciones_garantias == "1"): ?>
                    <div class="alert alert-primary" align="center">
                        <strong style="font-size:13px !important;color:darkblue;">
                            REFACCIONES SOLICIATADAS
                        </strong>
                    </div>
                <?php elseif ($estado_refacciones_garantias == "2"): ?>
                    <div class="alert alert-primary" align="center">
                        <strong style="font-size:13px !important;color:darkblue;">
                            REFACCIONES RECIBIDAS
                        </strong>
                    </div>
                <?php elseif ($estado_refacciones_garantias == "3"): ?>
                    <div class="alert alert-primary" align="center">
                        <strong style="font-size:13px !important;color:darkblue;">
                            REFACCIONES ENTREGADAS
                        </strong>
                    </div>
                <?php endif ?>
            <?php endif ?>
        <?php endif ?>
        
        <br>
        <div class="row">
            <!-- tabla meramente informativa -->
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:95%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REP.
                            </td>
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                CANTIDAD
                            </td>
                            <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:11%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                EXISTE
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                FECHA TENTATIVA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                PRECIO REFACCIÓN
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                COSTO MO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                TOTAL DE REPARACÓN
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                EDO. REFACCIÓN
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                ACCIÓN
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                FIRMA
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($renglon)): ?>
                            <!-- Comprobamos que haya registros guardados -->
                            <?php if (count($renglon)>0): ?>
                                <?php $total = 0; ?>
                                <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                    <!--Verificamso que el asesor haya marcado la refaccion como revisada -->
                                    <?php if ($autoriza_asesor[$index] == "1"): ?> 
                                        <!-- Comprobamos si ya se afecto el presupuesto por el cliente -->
                                        <?php if ($acepta_cliente != ""): ?>
                                            <!-- Si es una garantia no autorizada -->
                                            <?php if ($pza_garantia[$index] == "1"): ?>
                                                <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #97e2ee;">
                                            <!-- Si el presupuesto fue autorizado parcialmente -->
                                            <?php else: ?>
                                                <?php if ($autoriza_cliente[$index] == "1"): ?> 
                                                    <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #c7ecc7;">
                                                <?php else: ?>
                                                    <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #f5acaa;">
                                                <?php endif ?> 
                                            <?php endif ?>
                                        <!-- de lo contrario es una refaccion limpia -->
                                        <?php else: ?>
                                            <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>'>
                                        <?php endif ?>

                                            <!-- REP -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $referencia[$index]; ?></label>
                                            </td>
                                            <!-- CANTIDAD -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $cantidad[$index]; ?></label>
                                            </td>
                                            <!-- D E S C R I P C I Ó N -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $descripcion[$index]; ?></label>
                                                <input type="hidden" id="info_ref_<?= $id_refaccion[$index] ?>" value="<?= '['.$num_pieza[$index].'] '.$descripcion[$index].' '.(($pza_garantia[$index] == '1') ? '(pza. con garantia)' : ''); ?>">
                                            </td>
                                            <!-- NUM. PIEZA -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $num_pieza[$index]; ?></label>
                                            </td>
                                            <!-- EXISTE -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;"><?php echo $existe[$index]; ?></label>
                                            </td>
                                            <!-- FECHA TENTATIVA -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <label style="color:darkblue;">
                                                    <?php 
                                                        if($fecha_planta[$index] != "0000-00-00"){
                                                            $fecha  = new DateTime($fecha_planta[$index]);
                                                            echo $fecha->format('d/m/Y');
                                                        }
                                                    ?>
                                                </label>
                                            </td>
                                            <?php if ($pza_garantia[$index] == "1"): ?>
                                                <td colspan="4" style='border:1px solid #337ab7;' align='center'>
                                                    Refaccion con garantía  (<?php echo (($autoriza_garantia[$index] == "1") ? "AUTORIZADA" : "<label class='error'>NO AUTORIZADA</label>"); ?>)
                                                    <?php $total_renglon = ((float)$cantidad[$index] *(float)$costo_pieza[$index] ) + ((float)$horas_mo[$index] *(float)$costo_mo[$index] ); ?>
                                                </td>
                                            <?php else: ?>
                                                <!-- PRECIO REFACCIÓN -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($costo_pieza[$index],2); ?></label>
                                                </td>
                                                <!-- HORAS -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($horas_mo[$index],2); ?></label>
                                                </td>
                                                <!-- COSTO MO -->
                                                <td style='border:1px solid #337ab7;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($costo_mo[$index],2); ?></label>
                                                    
                                                    <?php $total_renglon = ((float)$cantidad[$index] *(float)$costo_pieza[$index] ) + ((float)$horas_mo[$index] *(float)$costo_mo[$index] ); ?>
                                                </td>
                                                <!-- TOTAL DE REPARACÓN -->
                                                <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                                    <label style="color:darkblue;">$ <?php echo number_format($total_renglon); ?></label>
                                                </td>
                                            <?php endif ?>

                                            <?php 
                                                if ($acepta_cliente != "No") {
                                                    if ($estado_entrega[$index] == "0") {
                                                        $color = "background-color:#f5acaa;";
                                                    } elseif ($estado_entrega[$index] == "1") {
                                                        $color = "background-color:#f5ff51;";
                                                    } elseif ($estado_entrega[$index] == "2") {
                                                        $color = "background-color:#5bc0de;";
                                                    }else{
                                                        $color = "background-color:#c7ecc7;";
                                                    }
                                                } else {
                                                    $color = "background-color:red;color:white;";
                                                }
                                            ?>
                                            
                                            <!-- ESTADO DE LA REFACCION -->
                                            <td style='border:1px solid #337ab7;<?= $color ?>' align='center'>
                                                <?php if (($autoriza_cliente[$index] == "1")||($autoriza_garantia[$index] == "1")): ?>
                                                    <?php if ($estado_entrega[$index] == "0"): ?>
                                                        SIN SOLICITAR
                                                    <?php elseif ($estado_entrega[$index] == "1"): ?>
                                                        SOLICITADA
                                                    <?php elseif ($estado_entrega[$index] == "2"): ?>
                                                        RECIBIDA
                                                    <?php else: ?>
                                                        ENTREGADA
                                                    <?php endif ?>
                                                <?php else: ?>
                                                    <br>
                                                <?php endif ?>
                                            </td>

                                            <!-- ACCION INDIVIDUAL -->
                                            <td style='border:1px solid #337ab7;' align='center'>
                                                <?php if (($autoriza_cliente[$index] == "1")||($autoriza_garantia[$index] == "1")): ?>
                                                    <?php if ($estado_entrega[$index] == "2"): ?>
                                                        <a onclick="alerta_entrega(<?= $id_refaccion[$index] ?>)" class="btn" style="background-color:#b7950b; color:white ;font-size: 9px;margin: 10px;" data-target="#alerta_entrega" data-toggle="modal">
                                                            ENTREGAR
                                                        </a>
                                                    <?php elseif ($estado_entrega[$index] == "1"): ?>
                                                        <a onclick="alerta_recibir(<?= $id_refaccion[$index] ?>)" class="btn" style="background-color:#272bff; color: white;font-size: 9px;margin: 10px;" data-target="#alerta_recibir" data-toggle="modal">
                                                            RECIBIR
                                                        </a>
                                                    <?php else: ?>
                                                        <br><br>
                                                    <?php endif ?>
                                                <?php else: ?>
                                                    <br>
                                                <?php endif ?>
                                            </td>
                                            <td style='border:1px solid #337ab7;<?= (($firma_ventanilla[$index] != "") ? "background-color: white;" : "") ?>' align='center'>
                                                <?php if (($autoriza_cliente[$index] == "1")||($autoriza_garantia[$index] == "1")): ?>
                                                    <?php if ($firma_ventanilla[$index] != ""): ?>
                                                        <img class='marcoImg' src='<?= base_url().$firma_ventanilla[$index] ?>' id='' style='width:80px;height:30px;'>
                                                    <?php else: ?>
                                                        <label style="color:darkblue;">POR FIRMAR</label>
                                                    <?php endif ?>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <?php $total += $total_renglon; ?>
                                    <?php endif ?>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="16" align="center">
                                        No se cargaron los datos
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="16" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php if (isset($dirige)): ?>
            <?php if ($dirige == "Entrega_G"): ?>
                <hr>
                <!-- comentarios de ventanilla -->
                <div class="row">
                    <div class="col-sm-6" align="left">
                        <h4>Comentarios de Garantías:</h4>
                        <label for="" style="font-size:12px;"><?php if(isset($comentario_garantia)){ if($comentario_garantia != "") echo $comentario_garantia; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
                    </div>
                    <div class="col-sm-6" align="right"></div>
                </div>
            <?php endif ?>
        <?php endif ?>
        
        <br>
        <div class="col-sm-12" align="center">
            <br>
            <i class="fas fa-spinner cargaIcono"></i>
            <br>
            <label for="" class="error" id="errorEnvioCotizacion"></label>
            <br>

            <input type="hidden" id="ref_aprobadas" value="<?= ((isset($pzs_aprobadas)) ? $pzs_aprobadas : '0') ?>">
            <input type="hidden" id="id_cita" value="<?php if(isset($id_cita)) echo $id_cita; ?>">
            <!-- Datos basicos -->
            <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>">
            <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="6">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <!-- validamos los envios de cada rol -->
            <input type="hidden" name="envioRefacciones" id="envioRefacciones" value="<?php if(isset($envia_ventanilla)) echo $envia_ventanilla; else echo "0"; ?>">
            <input type="hidden" name="" id="envioJefeTaller" value="<?php if(isset($envio_jdt)) echo $envio_jdt; else echo "0"; ?>">
            <input type="hidden" name="" id="envioGarantias" value="<?php if(isset($envio_garantia)) echo $envio_garantia; else echo "0"; ?>">
            
            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Ventanilla/4";?>';">
                Regresar
            </button>
            <br>
        </div>
    </form>
</div>

<div class="modal fade" id="alerta_entrega" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background-color:#c7ecc7;font-weight: bold;">
            <div class="modal-header">
                <h5 class="modal-title" style="font-weight: bold;">Entregar refacción al técnico</h5>
            </div>
            <div class="modal-body">
                <div class="" align="center">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <br>
                    <strong style="font-size:14px;" id="contenido">
                        ¿Seguro que quiere marcar esta refacción como entregada?
                        <br><br>
                        <samp id="label_refaccion" style="color:black;">1 ejemplo</samp>
                        <br><br>

                        <div class="row" align="center">
                            <div class="col-sm-4">
                                <br>
                            </div>
                            <div class="col-sm-3" align="center" style="background-color: white;">
                                <label style="color: darkblue;font-size: 14px;">
                                    FIRMA DE VENTANILLA
                                </label>
                                <br>
                                <img class="marcoImg" src="" id="firma_parte_req" alt="" style="height:2cm;width:4cm;">
                                <input type="hidden" id="ruta_parte_req"  value="" >
                                <br><br>

                            </div>
                            <div class="col-sm-4">
                                <br>
                            </div>
                        </div>

                        <br><br>
                        <div class="" align="left">
                            <label style="color: darkred;font-size: 13px;">
                                NOTA: Esta acción no se puede deshacer.
                            </label>
                            <br>
                        </div>
                    </strong>
                    <label id="notificacion" style="color: darkred;font-size: 13px;"></label>
                    <br>
                    
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="id_refaccion" value="">
                <input type="hidden" id="ref_entregadas" value="<?= ((isset($pzs_entregadas)) ? $pzs_entregadas : '0') ?>">
                <button type="button" class="btn btn-warning" data-dismiss="modal" id="cerrar_Envio" style="color:white;margin-left: 10px;">Cancelar</button>
                <a id="Req_firma_gerente" class="cuadroFirma btn btn-primary" data-value="Req_firma_gerente" data-target="#firmaDigital" data-toggle="modal" style="color:white;margin-left: 10px;"> Firmar </a>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="btn_entrega_f" onclick="guardar_entrega()" hidden>Entregar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:5000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" onclick="btn_entrega()" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="alerta_recibir" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color:#5bc0de;font-weight: bold;">
            <div class="modal-header">
                <h5 class="modal-title" style="font-weight: bold;">Recibir refacción</h5>
            </div>
            <div class="modal-body">
                <div class="" align="center">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <br>
                    <strong style="font-size:14px;" id="contenido_2">
                        ¿Seguro que quiere marcar esta refacción como recida?
                        <br><br>
                        <samp id="label_refaccion_2" style="color:black;">2 ejemplo</samp>
                        <br><br>

                        <div class="" align="left">
                            <label style="color: darkred;font-size: 13px;">
                                NOTA: Esta acción no se puede deshacer.
                            </label>
                            <br>
                        </div>
                    </strong>
                    <label id="notificacion" style="color: darkred;font-size: 13px;"></label>
                    <br>
                    
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="id_refaccion_2" value="">
                <input type="hidden" id="ref_recbidas" value="<?= ((isset($pzs_recibidas)) ? $pzs_recibidas : '0') ?>">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="guardar_recibida()">Recibir</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function alerta_recibir(refaccion){
        var info = $("#info_ref_"+refaccion).val();
        $("#label_refaccion_2").text(info);
        $("#id_refaccion_2").val(refaccion);
    }

    function guardar_recibida(){
        var aprobadas = $("#ref_aprobadas").val();
        var recibidas = $("#ref_recbidas").val();
        var ref = $("#id_refaccion_2").val();
        var id_cita = $("#idOrdenTempCotiza").val();

        $(".cargaIcono").css("display","inline-block");

        var base = $("#basePeticion").val().trim();
        $.ajax({
            url: base+"presupuestos/Presupuestos/solicituar_refaccion_individual",
            method: 'post',
            data: {
                refaccion_aprobadas: aprobadas,
                refaccion_recibida: recibidas,
                refaccion: ref,
                id_cita: id_cita
            },
            success:function(resp){
                if (resp.indexOf("handler           </p>")<1) {
                    if (resp == "OK") {
                        location.reload();
                    }else {
                        $("#notificacion").text(resp+".");
                    }
                }else{
                    $("#notificacion").text("ERROR AL ENVIAR LA INFORMACÓN.");
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }

    function btn_entrega(){
        $("#btn_entrega_f").attr('hidden', false);
        $("#Req_firma_gerente").attr('hidden', true);
    }

    function alerta_entrega(refaccion){
        var info = $("#info_ref_"+refaccion).val();
        $("#label_refaccion").text(info);
        $("#id_refaccion").val(refaccion);
    }

    function guardar_entrega(){
        var aprobadas = $("#ref_aprobadas").val();
        var entregadas = $("#ref_entregadas").val();
        var ref = $("#id_refaccion").val();
        var id_cita = $("#idOrdenTempCotiza").val();

        $(".cargaIcono").css("display","inline-block");

        var base = $("#basePeticion").val().trim();
        /*$.ajax({
            url: base+"presupuestos/Presupuestos/entrega_refaccion_individual",
            method: 'post',
            data: {
                refaccion_aprobadas: aprobadas,
                refaccion_entregadas: entregadas,
                refaccion: ref,
                id_cita: id_cita
            },*/
        var paqueteCotizacion = new FormData();
        paqueteCotizacion.append('refaccion_aprobadas', aprobadas); 
        paqueteCotizacion.append('refaccion_entregadas', entregadas); 
        paqueteCotizacion.append('refaccion', ref); 
        paqueteCotizacion.append('id_cita', id_cita); 
        paqueteCotizacion.append('rutaFirmaVentanilla', $("#ruta_parte_req").prop('value')); 

        /* Se envia el paquete de datos por ajax. */
        var base = $("#basePeticion").val().trim();

        $.ajax({
            url: base+"presupuestos/Presupuestos/entrega_refaccion_individual",
            type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
            contentType: false,
            data: paqueteCotizacion, // Al atributo data se le asigna el objeto FormData.
            processData: false,
            cache: false,
            success:function(resp){
                if (resp.indexOf("handler           </p>")<1) {
                    if (resp == "OK") {
                        location.reload();
                    }else {
                        $("#notificacion").text(resp+".");
                    }
                }else{
                    $("#notificacion").text("ERROR AL ENVIAR LA INFORMACÓN.");
                }
            //Cierre de success
            },
            error:function(error){
                console.log(error);
            //Cierre del error
            }
        //Cierre del ajax
        });
    }
</script>