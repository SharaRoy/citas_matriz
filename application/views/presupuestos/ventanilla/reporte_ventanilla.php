<table>
	<tr>
	    <th> No Orden </th>
	    <th> Folio </th>
	    <th> Cliente </th>
	    <th> Asesor </th>
	    <th> Tecnico </th>
	    <th> Vehiculo </th>
	    <th> Serie </th>
	    <th> Placas </th>
	    <th> Estatus. Presupuesto </th>
	</tr>
	<?php for ($i=0; $i < count($contenido) ; $i++): ?>
		<?php if (count($contenido[$i]['refacciones'])>0): ?>
			<tr _excel-styles='{"fill":{"fillType":"PhpSpreadsheet_Style_Fill::FILL_SOLID","color":{"argb":"e1c7ec"}}}'>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}'><?= $contenido[$i]['id_cita'] ?></td>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}'><?= $contenido[$i]['folio'] ?></td>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}'><?= $contenido[$i]['cliente'] ?></td>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}'><?= $contenido[$i]['tecnico'] ?></td>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}'><?= $contenido[$i]['asesor'] ?></td>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}'><?= $contenido[$i]['vehiculo_modelo'] ?></td>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}'><?= $contenido[$i]['serie'] ?></td>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}'><?= $contenido[$i]['vehiculo_placas'] ?></td>
	            <td _excel-dimensions='{"column":{"autoSize":"true"}}' _excel-styles='{"font":{"color":{"rgb":"<?= $contenido[$i]['color'] ?>"}}}'><?= $contenido[$i]['status_refaccion'] ?></td>
	        </tr>
	        <tr>
	        	<td></td>
	        	<td>
	        		Cantidad
	        	</td>
	        	<td>
	        		Descripcion
	        	</td>
	        	<td>
	        		No. Pieza
	        	</td>
	        	<td>
	        		Estatus Individual
	        	</td>
	        </tr>
	        <?php for ($j=0; $j < count($contenido[$i]['refacciones']) ; $j++): ?>
	        	<tr>
		        	<td></td>
		        	<td>
		        		<?= $contenido[$i]['refacciones'][$j]['cantidad'] ?>
		        	</td>
		        	<td>
		        		<?= $contenido[$i]['refacciones'][$j]['descripcion'] ?>
		        	</td>
		        	<td>
		        		<?= $contenido[$i]['refacciones'][$j]['num_pieza'] ?>
		        	</td>
		        	<td _excel-styles='{"fill":{"fillType":"PhpSpreadsheet_Style_Fill::FILL_SOLID","color":{"argb":"<?= $contenido[$i]['refacciones'][$j]['color'] ?>"}}}'>
		        		<?= $contenido[$i]['refacciones'][$j]['status_refaccion'] ?>
		        	</td>
		        </tr>
	    	<?php endfor; ?>			
		<?php endif ?>
          	
    <?php endfor; ?>
</table>