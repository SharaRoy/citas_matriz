<div style="margin:20px;">
    <!--<form name="" id="formulario" method="post" action="<?=base_url()."presupuestos/Presupuestos/validateFormR2"?>" autocomplete="on" enctype="multipart/form-data">-->
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="form-control" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:100%;font-size:12px;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="form-control" name="folio_externo" id="folio_externo" value="<?php if(set_value('folio_externo') != "") echo set_value('folio_externo'); elseif(isset($folio_externo)) echo $folio_externo; ?>" style="width:100%;font-size:12px;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="form-control" type="text" name="noSerie" id="noSerieText" value="<?php if(set_value('noSerie') != "") echo set_value('noSerie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;font-size:12px;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="form-control" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;font-size:12px;" disabled>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="form-control" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;font-size:12px;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="form-control" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;font-size:12px;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="form-control" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;font-size:12px;" disabled>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="form-control" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;font-size:12px;" disabled>
            </div>
        </div>
        
        <br><br>
        <div class="row">
            <!-- tabla meramente informativa -->
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:95%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                REP.
                            </td>
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                CANTIDAD
                            </td>
                            <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:21%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                PRECIO REFACCIÓN
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                COSTO MO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                TOTAL DE REPARACÓN
                            </td>
                            <td style="display: none;width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                PRIORIDAD
                                <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($renglon)) echo count($renglon); else echo "1"; ?>">
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center">
                                TIPO DE PIEZA
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <?php $total_op = 0; ?>
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($id_refaccion)): ?>
                            <!-- Comprobamos que haya registros guardados -->
                            <?php if (count($id_refaccion)>0): ?>
                                <?php for ($index  = 0; $index < count($id_refaccion); $index++): ?>
                                    <!-- Comprobamos si ya se afecto el presupuesto por el cliente -->
                                    <?php if ($acepta_cliente != ""): ?>
                                        <!-- Si el presupuesto fue autorizado totalmente -->
                                        <?php if ($acepta_cliente == "Si"): ?>
                                            <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #c7ecc7;">
                                        <!-- Si el presupuesto fue rechazado totalmente -->
                                        <?php elseif ($acepta_cliente == "No"): ?>
                                            <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #f5acaa;">
                                        <!-- Si el presupuesto fue autorizado parcialmente -->
                                        <?php else: ?>
                                            <?php if ($autoriza_cliente[$index] == "1"): ?>
                                                <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #c7ecc7;">
                                            <?php else: ?>
                                                <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>' style="background-color: #f5acaa;">
                                            <?php endif ?>
                                        <?php endif ?>
                                    <!-- de lo contrario es una refaccion limpia -->
                                    <?php else: ?>
                                        <tr class="fila" id='fila_indicador_<?php echo $index+1; ?>'>
                                    <?php endif ?>
                                        <!-- REP -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <label style="color:darkblue;"><?php echo $referencia[$index]; ?></label>
                                        </td>

                                        <!-- CANTIDAD -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <label style="color:darkblue;"><?php echo $cantidad[$index]; ?></label>
                                        </td>
                                        <!-- D E S C R I P C I Ó N -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <label style="color:darkblue;"><?php echo $descripcion[$index]; ?></label>
                                        </td>
                                        <!-- NUM. PIEZA -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <label style="color:darkblue;"><?php echo $num_pieza[$index]; ?></label>
                                        </td>
                                        <!-- PRECIO REFACCIÓN -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <label style="color:darkblue;">$
                                                <?php 
                                                    $costo_pz = (($pza_garantia[$index] == 0) ? $costo_pieza[$index] : (($autoriza_garantia[$index] == 1) ? $costo_ford[$index] : $costo_pieza[$index])); 
                                                    $thoras_mo = (($pza_garantia[$index] == 0) ? $horas_mo[$index] : (($autoriza_garantia[$index] == 1) ? $horas_mo_garantias[$index] : $horas_mo[$index]));
                                                    $precio_mo = (($pza_garantia[$index] == 0) ? $costo_mo[$index] : (($autoriza_garantia[$index] == 1) ? $costo_mo_garantias[$index] : $costo_mo[$index]));
                                                    $tipo_pza = (($pza_garantia[$index] == 0) ? "Púbico" : "Garantía");
                                                ?>
                                                <?= number_format($costo_pz,2); ?></label>
                                        </td>
                                        <!-- HORAS -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <label style="color:darkblue;">$<?= number_format($thoras_mo,2); ?></label>
                                        </td>
                                        <!-- COSTO MO -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <label style="color:darkblue;">$<?= number_format($precio_mo,2); ?></label>
                                            
                                            <?php 
                                                $total_renglon = ((float)$cantidad[$index] * (float)$costo_pz ) + ((float)$thoras_mo * (float)$precio_mo); 

                                                $total_op += $total_renglon;
                                            ?>
                                        </td>
                                        <!-- TOTAL DE REPARACÓN -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <label style="color:darkblue;">$<?php echo number_format($total_renglon,2); ?></label>
                                        </td>
                                        <!-- PRIORIDAD <br> REFACCION -->
                                        <td style='display: none;border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <?php if ($prioridad[$index] == "3"): ?>
                                                ALTA
                                            <?php elseif ($prioridad[$index] == "2"): ?>
                                                MEDIA
                                            <?php elseif ($prioridad[$index] == "1"): ?>
                                                BAJA
                                            <?php else: ?>
                                                SIN PRIORIDAD
                                            <?php endif ?>
                                            <!-- Indice interno de la recaccion -->
                                            <input type='hidden' id='id_refaccion_<?php echo $index+1; ?>' name='id_registros[]' value="<?php echo $id_refaccion[$index]; ?>">
                                            <!-- indicador del renglon -->
                                            <input type='hidden' id='decision_<?php echo $index+1; ?>' value="0">
                                            <!-- bandera del renglon -->
                                            <input type='hidden' id='bandera_decision_<?php echo $index+1; ?>' value="0">
                                            <!-- total del renglon -->
                                            <input type='hidden' id='total_renglon_<?php echo $index+1; ?>' value="<?php echo $total_renglon; ?>">
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <label style="color:darkblue;">
                                                <?= $tipo_pza ?>
                                            </label>
                                            <?php if (($pza_garantia[$index] == 1)&&($autoriza_garantia[$index] == 1)): ?>
                                                <label style="font-size: 9px;font-weight: bold;color:darkgreen;">
                                                    (Autorizado)
                                                </label>
                                            <?php elseif (($pza_garantia[$index] == 1)&&($autoriza_garantia[$index] == 0)): ?>
                                                <label style="font-size: 9px;font-weight: bold;color:red;">
                                                    (No autorizado)
                                                </label>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="13" align="center">
                                        No se cargaron los datos
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="13" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7" align="right">
                                <label>SUBTOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="subTotalMaterialLabel"><?php if(isset($subtotal)) echo number_format($subtotal,2); else echo "0"; ?></label>
                                <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subtotal)) echo $subtotal; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="right" class="error">
                                <label>CANCELADO</label>
                            </td>
                            <td colspan="1" align="center" class="error">
                                $<label id="canceladoMaterialLabel"><?php if(isset($cancelado)) echo number_format($cancelado,2); else echo "0"; ?></label>
                                <input type="hidden" name="canceladoMaterial" id="canceladoMaterial" value="<?php if(isset($cancelado)) echo $cancelado; else echo "0"; ?>">
                                <input type="hidden" name="" id="canceladoMaterial_temp" value="<?php if(isset($cancelado)) echo $cancelado; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="right">
                                <label>PRESUPUESTO</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label id="presupuestoTemptoMaterialLabel"><?php if(isset($cancelado)) echo number_format(($subtotal-$cancelado),2); else echo "0"; ?></label>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="right">
                                <label>I.V.A.</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="ivaMaterialLabel"><?php if(isset($iva)) echo number_format($iva,2); else echo "0"; ?></label>
                                <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($iva)) echo $iva; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="right">
                                <label>PRESUPUESTO TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label id="presupuestoMaterialLabel"><?php if(isset($subtotal)) echo number_format((($subtotal-$cancelado)*1.16),2); else echo "0"; ?></label>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr style="border-top:1px solid #337ab7;">
                            <td colspan="6" rowspan="2">
                                <label for="" id="anticipoNota" style="color:blue;">Nota anticipo: <?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                                <input type="hidden" name="anticipoNota" value="<?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?>">
                            </td>
                            <td colsan="2" align="right">
                                <label>ANTICIPO</label>
                            </td>
                            <td align="center">
                                $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                                <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colsan="2" align="right">
                                <label>TOTAL</label>
                            </td>
                            <td align="center">
                                $<label for="" id="totalMaterialLabel"><?php if(isset($total)) echo number_format($total,2); else echo "0"; ?></label>
                                <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($total)) echo $total; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="7" align="right">
                                FIRMA DEL ASESOR QUE APRUEBA
                            </td>
                            <td align="center" colspan="3">
                                <img class='marcoImg' src='<?php echo base_url(); ?><?php if(isset($firma_asesor)) { echo (($firma_asesor != "") ?  $firma_asesor : "assets/imgs/fondo_bco.jpeg"); } else { echo "assets/imgs/fondo_bco.jpeg"; } ?>' id='' style='width:80px;height:30px;'>
                                <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php if(isset($firma_asesor)) echo $firma_asesor ?>'>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <br>
        <!-- Comentarios del asesor -->
        <div class="row">
            <div class="col-sm-5">
                <h4>Notas del asesor:</h4>
                <!-- Verificamos si es una vista del asesor -->
                <label for="" style="font-size:12px;"><?php if(isset($nota_asesor)){ if($nota_asesor != "") echo $nota_asesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-7" align="right">
                <h4>Archivo(s) de la cotización:</h4>
                <br>

                <?php if (isset($archivos_presupuesto)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivos_presupuesto); $i++): ?>
                        <?php if ($archivos_presupuesto[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivos_presupuesto[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>

                            <?php if ((($i+1)% 4) == 0): ?>
                                <br>
                            <?php endif; ?>
                            
                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivos_presupuesto) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>

                <hr>
            </div>
        </div>

        <hr>
        <!-- Comentarios del técnico -->
        <div class="row">
            <div class="col-md-6">
                <h4>Comentario del técnico para Refacciones:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_tecnico)){ if($comentario_tecnico != "") echo $comentario_tecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>

            <div class="col-sm-6" align="right">
                <h4>Archivo(s) del técnico para Refacciones:</h4>

                <?php if (isset($archivo_tecnico)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivo_tecnico); $i++): ?>
                        <?php if ($archivo_tecnico[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo_tecnico[$i]; ?>" class="btn" style="background-color: #5cb8a5;border-color: #4c8bae;color:white;font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>

                            <?php if (($contador % 4) == 0): ?>
                                <br>
                            <?php endif; ?>

                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivo_tecnico) == 0): ?>
                        <h4 >Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <hr>
        <!-- comentarios de ventanilla -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios de Ventanilla:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_ventanilla)){ if($comentario_ventanilla != "") echo $comentario_ventanilla; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right"></div>
        </div>

        <hr>
        <!-- comentarios del jefe de taller -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios del Jefe de Taller:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_jdt)){ if($comentario_jdt != "") echo $comentario_jdt; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right">
                <h4>Archivo(s) del Jefe de Taller:</h4>

                <?php if (isset($archivo_jdt)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivo_jdt); $i++): ?>
                        <?php if ($archivo_jdt[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo_jdt[$i]; ?>" class="btn btn-primary" style="font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>
                            
                            <?php if (($contador % 4) == 0): ?>
                                <br>
                            <?php endif; ?>
                            
                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivo_jdt) == 0): ?>
                        <h4>Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        
        <br>
        <div class="col-sm-12" align="center">
            <br>
            <i class="fas fa-spinner cargaIcono"></i>
            <br>
            <label for="" class="error" id="errorEnvioCotizacion"></label>
            <br>

            <!-- Datos basicos -->
            <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>"> 
            <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="10">
            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <!-- validamos los envios de cada rol -->
            <input type="hidden" name="envioRefacciones" id="envioRefacciones" value="<?php if(isset($envia_ventanilla)) echo $envia_ventanilla; else echo "0"; ?>">
            <input type="hidden" name="" id="envioJefeTaller" value="<?php if(isset($envio_jdt)) echo $envio_jdt; else echo "0"; ?>">
            <input type="hidden" name="" id="envioGarantias" value="<?php if(isset($envio_garantia)) echo $envio_garantia; else echo "0"; ?>">
        </div>
    <!--</form>-->
</div>

<!-- Modal para la envio de decision-->
<div class="modal fade" id="AlertaModal" role="dialog" data-backdrop="static" data-keyboard="false" style="background-color:">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="AlertaModalCuerpo">
            <div class="modal-body">
                <br><br>
                <h5 class="modal-title" id="tituloForm" align="center"></h5>
                <br>
                <div class="row">
                    <div class="col-md-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <br>
                        <label for="" class="error" id="errorEnvioCotizacion"></label>
                        <button class="btn" type="button" id="recargar_presupuesto" onclick="location.reload()" hidden>
                                FINALIZAR!!
                        </button>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12" align="right">
                        <input type="hidden" name="" id="desicion" value="">
                        <input type="hidden" name="" id="desicion_cancelado" value="">
                        <input type="hidden" name="" id="desicion_subtotal" value="">
                        <input type="hidden" name="aprobacion_refacciones" id="aprobacion_refacciones" value="">
                        <input type="hidden" name="id_cita" id="id_cita" value="<?php if(isset($id_cita)) echo $id_cita; ?>">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="cancelarCoti">Cancelar</button>
                        <button type="button" class="btn btn-default" id="enviar_presupuesto">Confirmar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
