<div style="margin:10px;">
    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Presupuestos Multipunto</h3>

            <br>
            <div class="row">
                <div class="col-sm-12">
                    <?php if ($this->session->userdata('rolIniciado')): ?>
                        <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "TEC"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_Tec/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "JDT"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Panel_JefeTaller/5";?>';">
                                Regresar
                            </button>
                        <?php elseif ($this->session->userdata('rolIniciado') == "PCO"): ?>
                            <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Principal_CierreOrden/5";?>';">
                                Cerrar Sesión
                            </button>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

            <br>
            <div class="panel panel-default" align="center">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-3" align="left">
                            <h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>

                        <div class="col-sm-3" align="left">
                            <h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>
                        </div>

                        <div class="col-sm-3" align="left">
                            <h5>Busqueda Campo:</h5>
                            <div class="input-group">
                                <input type="text" class="form-control" id="busqueda_gral" placeholder="Buscar...">
                            </div>
                            <input type="hidden" name="" id="indicador" value="JefeTaller">
                        </div>

                        <div class="col-sm-2" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_asesor_table" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>
                    </div>

                    <br>
                    <i class="fas fa-spinner cargaIcono"></i>
                    
                    <table <?php if ($presupuestos != NULL) echo 'id="bootstrap-data-table2"'; ?> class="table table-striped table-bordered table-responsive" style="width: 100%;">
                        <thead>
                            <tr style="background-color: #eee;">
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>NO. ORDEN</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>ORDEN INTELISIS</strong></td>
                                <!--<td align="center" style="width: 10%;vertical-align: middle;"><strong>TIPO PRESUPUESTO</strong></td>-->
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>FECHA RECEPCIÓN</strong></td>
                                <!--<td align="center" style="width: 10%;vertical-align: middle;"><strong>SERIE</strong></td>-->
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>VEHÍCULO</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>APRUEBA CLIENTE</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong>EDO. REFACCIONES</strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                                <td align="center" style="width: 10%;vertical-align: middle;"><strong><br></strong></td>
                            </tr>
                        </thead>
                        <tbody id="imprimir_busqueda">
                            <?php if ($presupuestos != NULL): ?>
                                <?php foreach ($presupuestos as $i => $registro): ?>
                                    <tr style="font-size:12px;">
                                        <td align="center" style="vertical-align: middle;background-color: <?= (($registro->envio_garantia == '1') ? "#68ce68" : "#fffff") ?>;">
                                            <?= $registro->id_cita ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->folio_externo ?>
                                        </td>
                                        <!--<td align="center" style="vertical-align: middle;">
                                            <?= (($registro->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")  ?>
                                        </td>-->
                                        <td align="center" style="vertical-align: middle;">
                                            <?php 
                                                $fecha = new DateTime($registro->fecha_recepcion.' 00:00:00');
                                                echo $fecha->format('d-m-Y');
                                             ?>
                                        </td>
                                        <!--<td align="center" style="vertical-align: middle;">
                                            <?= $registro->serie ?>
                                        </td>-->
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->vehiculo ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->placas ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->asesor ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?= $registro->tecnico ?>
                                        </td>

                                        <td align="center" style="vertical-align: middle;">
                                            <?php if ($registro->acepta_cliente != ""): ?>
                                                <?php if ($registro->acepta_cliente == "Si"): ?>
                                                    <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                <?php elseif ($registro->acepta_cliente == "Val"): ?>
                                                    <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                <?php else: ?>
                                                    <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                            <?php endif; ?>

                                            <?php 
                                                if ($registro->acepta_cliente != "No") {
                                                    if ($registro->estado_refacciones == "0") {
                                                        $color = "background-color:#f5acaa;";
                                                    } elseif ($registro->estado_refacciones == "1") {
                                                        $color = "background-color:#f5ff51;";
                                                    } elseif ($registro->estado_refacciones == "2") {
                                                        $color = "background-color:#5bc0de;";
                                                    }else{
                                                        $color = "background-color:#c7ecc7;";
                                                    }
                                                } else {
                                                    $color = "background-color:red;color:white;";
                                                }
                                             ?>
                                        </td>

                                        <td align="center" style="vertical-align: middle; <?= $color ?>">
                                            <?php if (($registro->estado_refacciones == "0")&&($registro->acepta_cliente != "No")): ?>
                                                SIN SOLICITAR
                                            <?php elseif ($registro->acepta_cliente == "No"): ?>
                                                RECHAZADA
                                            <?php elseif ($registro->estado_refacciones == "1"): ?>
                                                SOLICITADAS
                                            <?php elseif ($registro->estado_refacciones == "2"): ?>
                                                RECIBIDAS
                                            <?php elseif ($registro->estado_refacciones == "3"): ?>
                                                ENTREGADAS
                                            <?php else: ?>
                                                SIN SOLICITAR *
                                            <?php endif ?>
                                        </td>

                                        <td align="center" style="vertical-align: middle;">
                                            <?php 
                                                $id = (double)$registro->id_cita*CONST_ENCRYPT;
                                                $url_id = base64_encode($id);
                                                $url = str_replace("=", "" ,$url_id);
                                            ?>

                                            <a href="<?=base_url().'OrdenServicio_Revision/'.$url;?>" class="btn btn-warning" target="_blank" style="font-size: 12px;">ORDEN</a>
                                        </td>    

                                        <td align="center" style="vertical-align: middle;">
                                            <!-- Verificamos si se va a editar o se va visualizar el presupuesto -->
                                            <?php if ($registro->envio_garantia == "1"): ?>
                                                <a href="<?=base_url()."Presupuesto_Garantia/".$url;?>" class="btn btn-info" style="color:white;font-size: 12px;">VER</a>
                                            <?php else: ?>
                                                <a href="<?=base_url()."Presupuesto_Garantia/".$url;?>" class="btn btn-primary" style="color:white;font-size: 12px;">REVISAR</a>
                                            <?php endif ?>
                                        </td>
                                        <td align="center" style="vertical-align: middle;">
                                            <?php if ($registro->firma_requisicion != ''): ?>
                                                <?php if ($registro->firma_requisicion_garantias != ''): ?>
                                                    <a href="<?=base_url()."Requisicion_G/".$url;?>" class="btn btn-secondary" style="color:white;font-size: 10px;" target="_blank">REQUISICIÓN</a>
                                                <?php else: ?>
                                                    <a href="<?=base_url()."Requisicion_Firma_G/".$url;?>" class="btn btn-success" style="color:white;font-size: 10px;">REQUISICIÓN<br>FIRMA</a>
                                                <?php endif ?>
                                            <?php endif ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="11" style="width:100%;" align="center">
                                        <!-- Comprobamos si expiro la sesion o simplemente no hay registros -->
                                        <h4>Sin registros que mostrar</h4>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>assets/js/data-table/datatables.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/dataTables.buttons.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/jszip.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/pdfmake.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/vfs_fonts.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.html5.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.print.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/buttons.colVis.min.js"></script>
<script src="<?= base_url() ?>assets/js/data-table/datatables-init.js"></script>


<script>
    $('#bootstrap-data-table2').DataTable({
        "ordering": false,
        "searching": false,
        "bPaginate": false,
        "paging": false,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "bAutoWidth": false
    });

    $("#btnBusqueda_asesor_table").on('click', function (e){
        var campo = $('#busqueda_gral').prop('value');
        var fecha_inicio = $('#fecha_inicio').prop('value');
        var fecha_fin = $('#fecha_fin').prop('value');

        // Evitamos que salte el enlace.
        e.preventDefault();

        if ((fecha_inicio != "")||(campo != "")) {
            var paqueteDeDatos = new FormData();
            paqueteDeDatos.append('fecha_inio', fecha_inicio);
            paqueteDeDatos.append('fecha_fin', fecha_fin);
            paqueteDeDatos.append('campo', campo);

            var base = $("#basePeticion").val();
            $(".cargaIcono").css("display","inline-block");
            $.ajax({
                url: base+"presupuestos/PresupuestosBusquedas/buscador_garantias",
                type: 'post', // Siempre que se envíen ficheros, por POST, no por GET.
                contentType: false,
                data: paqueteDeDatos, // Al atributo data se le asigna el objeto FormData.
                processData: false,
                cache: false,
                success:function(resp){
                    //console.log(resp);
                    var data = JSON.parse(resp);
                    //console.log(data);

                    llenar_tabla(data);

                    $(".cargaIcono").css("display","none");
                //Cierre de success
                },
                error:function(error){
                    console.log(error);
                    $(".cargaIcono").css("display","none");
                    $('#errorVideo').text("Error de carga VERIFICAR.");
                //Cierre del error
                }
            //Cierre del ajax
            });
        }else{

        }
            
    });

    function llenar_tabla(data){
        var tabla = $("#imprimir_busqueda");
        tabla.empty();

        var base = $("#basePeticion").val();

        if (data.length > 0) {
            for(i = 0; i < data.length; i++){
                var revisar = "";
                if (data[i]["envio_garantia"] == "1") {
                    revisar = "<a class='btn btn-info' style='color:white;font-size:10px;' href='"+base+"Presupuesto_Garantia/"+data[i].id_cita_url+"'>VER</a>";
                } else {
                    revisar = "<a class='btn btn-primary' style='color:white;font-size:10px;' href='"+base+"Presupuesto_Garantia/"+data[i].id_cita_url+"'>REVISAR</a>";
                }

                var apruebaCliente = '';
                if (data[i]["acepta_cliente"] != "") {
                    if (data[i]["acepta_cliente"] == 'Si') {
                        apruebaCliente = "<label style='color: darkgreen; font-weight: bold;'>CONFIRMADA</label>";
                    } else if(data[i]["acepta_cliente"] == 'Val'){
                        apruebaCliente = "<label style='color: darkorange; font-weight: bold;'>DETALLADA</label>";
                    } else{
                        apruebaCliente = "<label style='color: red ; font-weight: bold;'>RECHAZADA</label>";
                    }
                } else{
                    apruebaCliente = "<label style='color: black; font-weight: bold;'>SIN CONFIRMAR</label>";
                }

                var requisicion = "";
                if (data[i]["firma_requisicion"] == "1") {
                    if (data[i]["firma_requisicion_garantias"] == "1") {
                        requisicion = "<a class='btn btn-secondary' style='color:white;font-size: 10px;' href='"+base+"Requisicion_G/"+data[i].id_cita_url+"'>REQUISICIÓN</a>";
                    } else {
                        requisicion = "<a class='btn btn-success' style='color:white;font-size: 10px;' href='"+base+"Requisicion_Firma_G/"+data[i].id_cita_url+"'>REQUISICIÓN<br>FIRMA</a>";
                    }
                }

                //Creamos las celdas en la tabla
                tabla.append("<tr style='font-size:11px;'>"+
                    //NO. ORDEN
                    "<td align='center' style='background-color:"+((data[i].envio_garantia == "0") ? '#ffffff' : '#68ce68')+";vertical-align: middle;'>"+
                        data[i]["id_cita"]+
                    "</td>"+
                    //ORDEN INTELISIS
                    "<td align='center' style='vertical-align: middle;'>"+data[i].folio_intelisis+"</td>"+
                    //TIPO PRESUPUESTO
                    //"<td align='center' style='vertical-align: middle;'>"+data[i].tipo_presupuesto+"</td>"+
                    //FECHA RECEPCION
                    "<td align='center' style='vertical-align: middle;'>"+data[i].fecha_formato+"</td>"+
                    //SERIE
                    //"<td align='center' style='vertical-align: middle;wid;'>"+data[i].serie+"</td>"+
                    //VEHÍCULO
                    "<td align='center' style='vertical-align: middle;'>"+data[i].vehiculo+"</td>"+
                    //PLACAS
                    "<td align='center' style='vertical-align: middle;'>"+data[i].placas+"</td>"+
                    //ASESOR
                    "<td align='center' style='vertical-align: middle;'>"+data[i].asesor+"</td>"+
                    //TÉCNICO
                    "<td align='center' style='vertical-align: middle;'>"+data[i].tecnico+"</td>"+
                    //APRUEBA CLIENTE
                    "<td align='center' style='vertical-align: middle;'>"+apruebaCliente+"</td>"+
                    //EDO. REFACCIONES
                    "<td align='center' style='vertical-align: middle;"+data[i].color_bg+"'>"+data[i].edo_entrega+"</td>"+
                    //ACCIONES
                    //VER ORDEN DE SERVICIO
                    "<td align='center' style='vertical-align: middle;'>"+
                        "<a href='"+base+"OrdenServicio_Revision/"+data[i].id_cita_url+"' class='btn btn-warning' target='_blank' style='font-size: 10px;'>ORDEN</a>"+
                    "</td>"+
                    "<td align='center' style='vertical-align: middle;'>"+
                        ""+revisar+""+
                    "</td>"+
                    "<td align='center' style='vertical-align: middle;'>"+
                        ""+requisicion+""+
                    "</td>"+
                "</tr>");
            }

        } else {
            tabla.append("<tr style='font-size:14px;'>"+
                "<td align='center' colspan='11'>No se encontraron resultados</td>"+
            "</tr>");
        }
    }
</script>