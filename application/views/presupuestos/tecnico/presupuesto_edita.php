<div style="margin:10px;">
    <form id="form_edita_tec" method="post" action="" autocomplete="on" enctype="multipart/form-data">        
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:100%;">
                <?=  form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="input_field" name="folio_externo" id="folio_externo" value="<?php if(set_value('folio_externo') != "") echo set_value('folio_externo'); elseif(isset($folio_externo)) echo $folio_externo; ?>" style="width:100%;">
                <?=  form_error('folio_externo', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php if(set_value('noSerie') != "") echo set_value('noSerie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;" disabled>
                <?=  form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?=  set_value('modelo');?>" style="width:100%;">
                <?=  form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?=  set_value('placas');?>" style="width:100%;">
                <?=  form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?=  set_value('uen');?>" style="width:100%;">
                <?=  form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?=  set_value('tecnico');?>" style="width:100%;">
                <?=  form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?=  set_value('asesors');?>" style="width:100%;">
                <?=  form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>

        <br><br>
        <div class="alert alert-warning" align="center">
            <strong style="font-size:13px !important;color:darkblue;">
                Proceso del presupuesto: <?=  $ubicacion_proceso; ?>
                <br>
                <!--Proceso del presupuesto con garantías: <?=  $ubicacion_proceso_garantia; ?>-->
            </strong>
            <br>
            <strong style="font-size:13px !important;">
                Vigencia: Un mes a partir de la fecha de emisión del presente presupuesto. (Fecha de vencimiento: <?php if(isset($fecha_limite)) echo $fecha_limite; ?>)
            </strong>
        </div>

        <div class="row">
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REP.
                            </td>
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                CANTIDAD
                            </td>
                            <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 10px;" align="center" colspan="3">
                                EXISTE
                            </td>
                            <!--<td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                                FECHA<br>TENTATIVA
                            </td>-->
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PRECIO REFACCIÓN
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                COSTO MO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                TOTAL DE REPARACÓN
                            </td>
                            <td style="display: none;width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PRIORIDAD <br> REFACCION
                            </td>
                            <td style="min-width: 30px;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PZA. GARANTÍA
                                <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($renglon)) echo count($renglon)+1; else echo "1"; ?>">
                                <input type="hidden" name="" id="indice_limite" value="<?php if (isset($renglon)) echo count($renglon)+1; else echo "1"; ?>">
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REPARACIÓN ADICIONAL
                            </td>
                            <td style="min-width: 30px;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                ELIMINAR <br> REFACCIÓN
                            </td>
                        </tr>
                        <tr style="background-color: #ddd;">
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                SI
                            </td>
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                NO
                            </td>
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                PLANTA
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($renglon)): ?>
                            <!-- Comprobamos que haya registros guardados -->
                            <?php if (count($renglon)>0): ?>
                                <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                    <tr id='fila_<?=  $index+1; ?>'>
                                        <!-- REP. -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' id='ref_<?=  $index+1; ?>' onblur='actualizaValoresTecnico(<?=  $index+1; ?>)' style='width:90%;text-align:left;'><?=  $referencia[$index]; ?></textarea>
                                        </td>
                                        <!-- CANTIDAD -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico(<?=  $index+1; ?>)' id='cantidad_<?=  $index+1; ?>' value='<?=  $cantidad[$index]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                        </td>
                                        <!-- D E S C R I P C I Ó N -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='2' class='input_field_lg' onblur='actualizaValoresTecnico(<?=  $index+1; ?>)' id='descripcion_<?=  $index+1; ?>' style='width:90%;text-align:left;'><?=  $descripcion[$index]; ?></textarea>
                                        </td>
                                        <!-- NUM. PIEZA -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='2' class='input_field_lg' onblur='actualizaValoresTecnico(<?=  $index+1; ?>)' id='pieza_<?=  $index+1; ?>' style='width:90%;text-align:left;'><?=  $num_pieza[$index]; ?></textarea>
                                        </td>
                                        <!-- EXISTE -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaSi_<?=  $index+1; ?>' class='input_field' onclick='actualizaValoresTecnico(<?=  $index+1; ?>)' name='existe_<?=  $index+1; ?>' value="SI" <?php if($existe[$index] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaNo_<?=  $index+1; ?>' class='input_field' onclick='actualizaValoresTecnico(<?=  $index+1; ?>)' name='existe_<?=  $index+1; ?>' value="NO" <?php if($existe[$index] == "NO") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaPlanta_<?=  $index+1; ?>' class='input_field' onclick='actualizaValoresTecnico(<?=  $index+1; ?>)' name='existe_<?=  $index+1; ?>' value="PLANTA" <?php if($existe[$index] == "PLANTA") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <!-- FECHA TENTANTIVA - ENTREGA -->
                                        <td style='border:1px solid #337ab7;display: none;' align='center'> 
                                            <input type="date" class='input_field' id="fecha_tentativa_<?=  $index+1; ?>" value="<?php if($fecha_planta[$index] != "0000-00-00") echo $fecha_planta[$index]; else echo date('Y-m-d'); ?>" <?php if($fecha_planta[$index] == "0000-00-00") echo "style='display: none;'";?> disabled> 
                                        </td>
                                        <!-- PRECIO REFACCIÓN -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            $<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico(<?=  $index+1; ?>)' id='costoCM_<?=  $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?=  $costo_pieza[$index]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <!-- HORAS -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico(<?=  $index+1; ?>)' id='horas_<?=  $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?=  $horas_mo[$index]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <!-- COSTO MO -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            $<input type='number' step='any' min='0' id='totalReng_<?=  $index+1; ?>' onblur="actualizaValoresTecnico(<?=  $index+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?=  $costo_mo[$index]; ?>' style='width:90%;text-align:left;' disabled>
                                            
                                            <!-- total de la refaccion -->
                                            <input type='hidden' id='totalOperacion_<?=  $index+1; ?>' value='<?=  $total_refacion[$index]; ?>'>

                                            <!-- Indice interno de la recaccion -->
                                            <input type='hidden' id='id_refaccion_<?=  $index+1; ?>' name='id_registros[]' value="<?=  $id_refaccion[$index]; ?>">
                                            <!-- Informacion del renglon -->
                                            <input type='hidden' id='valoresCM_<?=  $index+1; ?>' name='registros[]' value="<?=  $renglon[$index]; ?>">
                                            <!-- Informacion de datos de garantia de la refaccion (si aplica) -->
                                            <input type='hidden' id='valoresGarantias_<?=  $index+1; ?>' name='registrosGarantias[]' value="<?=  $renglonGarantia[$index]; ?>">

                                            <!-- Informacion del aprueba jefe de taller -->
                                            <input type='hidden' id='aprueba_jdt_<?=  $index+1; ?>' value="<?=  $autoriza_jefeTaller[$index]; ?>">
                                            <!-- Informacion pieza con garantia -->
                                            <input type='hidden' name="garantia[]" id='garantia_<?=  $index+1; ?>' value="<?=  $pza_garantia[$index]; ?>">
                                            <!-- Informacion de prioridad de la refaccion -->
                                            <!--<input type="hidden" name="prioridad_<?=  $index+1; ?>" id="prioridad_<?=  $index+1; ?>" value="<?php if(isset($prioridad[$index])) echo $prioridad[$index]; else echo "1"; ?>">-->
                                        </td>
                                        <!-- TOTAL DE REPARACÓN -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <label id="total_renglon_label_<?=  $index+1; ?>" style="color:darkblue;">$<?=  number_format($total_refacion[$index],2); ?></label>
                                        </td>
                                        <!-- PRIORIDAD <br> REFACCION -->
                                        <td style='display: none;border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur="actualizaValoresTecnico(<?=  $index+1; ?>)" id='prioridad_<?=  $index+1; ?>' name='prioridad_<?=  $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?=  $prioridad[$index]; ?>' style='width:70%;text-align:center;'>
                                        </td>
                                        <!-- PZA. GARANTÍA -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='checkbox' class='input_field' onclick='habilitar_rep(<?=  $index+1; ?>)' name="pzaGarantia[]" value="<?=  $index+1; ?>" id='check_g_<?=  $index+1; ?>' <?php if($pza_garantia[$index] == "1") echo "checked"; ?> style="transform: scale(1.5);" >
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input id='rep_add_<?=  $index+1; ?>' type='checkbox' class='input_field' onclick='actualiza_fila(<?=  $index+1; ?>)' value='<?=  $index+1; ?>' name='rep_add[]' style='transform: scale(1.5);' <?= (($pza_garantia[$index] == "0") ? 'disabled' : '') ?> <?= (($rep_adicional[$index] == "1") ? 'checked' : '') ?> >
                                        </td>
                                        <!-- Eliminar renglon -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <!--<a id='refaccion_baja_<?=  $index+1; ?>' class='refaccion_baja btn btn-danger' data-id="<?=  $index+1; ?>" data-target="#eliminarRefaccion" data-toggle="modal" style="color:white;">
                                                <i class="fa fa-trash"></i>
                                            </a>-->
                                        </td>
                                    </tr>
                                <?php endfor; ?>

                                <tr>
                                    <td colspan="8" align="center" style="font-size: 14px;background-color: #eeee;">
                                        Agregar refacciones
                                    </td>
                                    <td colspan="3" style="background-color: #eeee;">
                                        <label for="" class="error"> <strong>*</strong> Guardar renglon [<i class="fas fa-plus"></i>] </label>
                                        <br>
                                        <label for="" class="error"> <strong>*</strong> Eliminar renglon [<i class="fas fa-minus"></i>] </label>
                                    </td>
                                    <td colspan="1" style="background-color: #eeee;">
                                        <a id="agregarRefaccionActTec" class="btn btn-success" style="color:white;">
                                            <i class="fas fa-plus"></i>
                                        </a>
                                        
                                    </td>
                                    <td colspan="1" style="background-color: #eeee;">
                                        <a id="eliminarRefaccionActTec" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </td>
                                </tr>

                                <!-- Creamos un renglon vacio por si se va a agregar algun item -->
                                <tr id='fila_<?=  count($renglon)+1; ?>'>
                                    <!-- REP. -->
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <textarea rows='1' class='input_field_lg' onblur='actualizaValoresTecnico(<?= count($renglon)+1; ?>)' id='ref_<?= count($renglon)+1; ?>' name="ref_<?= count($renglon)+1; ?>" style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- CANTIDAD -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico(<?=  count($renglon)+1; ?>)' id='cantidad_<?=  count($renglon)+1; ?>' value='1' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                    </td>
                                    <!-- D E S C R I P C I Ó N -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='2' class='input_field_lg' onblur='actualizaValoresTecnico(<?=  count($renglon)+1; ?>)' id='descripcion_<?=  count($renglon)+1; ?>' style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- NUM. PIEZA -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='2' class='input_field_lg' onblur='actualizaValoresTecnico(<?=  count($renglon)+1; ?>)' id='pieza_<?=  count($renglon)+1; ?>' style='width:90%;text-align:left;'></textarea>
                                    </td>
                                    <!-- EXISTE -->
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaSi_<?=  count($renglon)+1; ?>' class='input_field' onclick='actualizaValoresTecnico(<?=  count($renglon)+1; ?>)' name='existe_<?=  count($renglon)+1; ?>' value="SI" style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaNo_<?=  count($renglon)+1; ?>' class='input_field' onclick='actualizaValoresTecnico(<?=  count($renglon)+1; ?>)' name='existe_<?=  count($renglon)+1; ?>' value="NO" style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaPlanta_<?=  count($renglon)+1; ?>' class='input_field' onclick='actualizaValoresTecnico(<?=  count($renglon)+1; ?>)' name='existe_<?=  count($renglon)+1; ?>' value="PLANTA" style="transform: scale(1.5);">
                                    </td>
                                    <!-- FECHA TENTANTIVA - ENTREGA -->
                                    <td style='border:1px solid #337ab7;display: none;' align='center'> 
                                        <input type="date" class='input_field' min="<?=  date('Y-m-d'); ?>" id="fecha_tentativa_<?=  count($renglon)+1; ?>" value="<?=  date('Y-m-d'); ?>" name="" style="display: none;"> 
                                    </td>
                                    <!-- PRECIO REFACCIÓN -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        $<input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico(<?=  count($renglon)+1; ?>)' id='costoCM_<?=  count($renglon)+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                    </td>
                                    <!-- HORAS -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' onblur='actualizaValoresTecnico(<?=  count($renglon)+1; ?>)' id='horas_<?=  count($renglon)+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='0' style='width:90%;text-align:left;'>
                                    </td>
                                    <!-- COSTO MO -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        $<input type='number' step='any' min='0' id='totalReng_<?=  count($renglon)+1; ?>' onblur="actualizaValoresTecnico(<?=  count($renglon)+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='990' style='width:90%;text-align:left;' disabled>
                                        
                                        <!-- total de la refaccion -->
                                        <input type='hidden' id='totalOperacion_<?=  count($renglon)+1; ?>' value='0'>

                                        <!-- Indice interno de la recaccion -->
                                        <input type='hidden' id='id_refaccion_<?=  count($renglon)+1; ?>' name='id_registros[]' value="0">
                                        <!-- Informacion del renglon -->
                                        <input type='hidden' id='valoresCM_<?=  count($renglon)+1; ?>' name='registros[]' value="">
                                        <!-- Informacion de datos de garantia de la refaccion (si aplica) -->
                                        <input type='hidden' id='valoresGarantias_<?=  count($renglon)+1; ?>' name='registrosGarantias[]' value="0_0_0">

                                        <!-- Informacion del aprueba jefe de taller -->
                                        <input type='hidden' id='aprueba_jdt_<?=  count($renglon)+1; ?>' value="0">
                                        <!-- Informacion pieza con garantia -->
                                        <input type='hidden' name="garantia[]" id='garantia_<?=  count($renglon)+1; ?>' value="0">
                                        <!-- Informacion de prioridad de la refaccion -->
                                        <!--<input type="hidden" name="prioridad_<?=  count($renglon)+1; ?>" id="prioridad_<?=  count($renglon)+1; ?>" value="2">-->
                                    </td>
                                    <!-- TOTAL DE REPARACÓN -->
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <label id="total_renglon_label_<?=  count($renglon)+1; ?>" style="color:darkblue;">$0.00</label>
                                    </td>
                                    <!-- PRIORIDAD <br> REFACCION -->
                                    <td style='display: none;border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' onblur="actualizaValoresTecnico(<?=  count($renglon)+1; ?>)" id='prioridad_<?=  count($renglon)+1; ?>' name='prioridad_<?=  count($renglon)+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='2' style='width:70%;text-align:center;'>
                                    </td>
                                    <!-- PZA. GARANTÍA -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='checkbox' class='input_field' name='pzaGarantia[]' value='<?=  count($renglon)+1; ?>' onclick='habilitar_rep(<?= count($renglon)+1; ?>)' id='check_g_<?= count($renglon)+1; ?>' style='transform: scale(1.5);'>
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input id='rep_add_<?= count($renglon)+1; ?>' type='checkbox' class='input_field' onclick='actualiza_fila(<?= count($renglon)+1; ?>)' value='<?= count($renglon)+1; ?>' name='rep_add[]' style='transform: scale(1.5);' disabled>
                                    </td>
                                    <!-- Eliminar renglon -->
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <!--<a id='refaccionBusqueda_<?=  count($renglon)+1; ?>' class='refaccionesBox btn btn-danger' data-id="<?=  count($renglon)+1; ?>" data-target="#eliminarRefaccion" data-toggle="modal" style="color:white;">
                                            <i class="fa fa-trash"></i>
                                        </a>-->
                                    </td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <td colspan="13" align="center">
                                        No se cargaron los datos
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="13" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="9" align="right">
                                <label>SUB-TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="subTotalMaterialLabel"><?php if(isset($subtotal)) echo number_format($subtotal,2); else echo "0"; ?></label>
                                <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subtotal)) echo $subtotal; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="9" align="right" class="error">
                                <label>CANCELADO</label>
                            </td>
                            <td colspan="1" align="center" class="error">
                                <!--$<label id="canceladoMaterialLabel"><?php if(isset($cancelado)) echo number_format($cancelado,2); else echo "0"; ?></label>-->
                                $<label id="canceladoMaterialLabel">0.00</label>
                                <input type="hidden" name="canceladoMaterial" id="canceladoMaterial" value="<?php if(isset($cancelado)) echo $cancelado; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="9" align="right">
                                <label>I.V.A.</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="ivaMaterialLabel"><?php if(isset($iva)) echo number_format($iva,2); else echo "0"; ?></label>
                                <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($iva)) echo $iva; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="9" align="right">
                                <label>PRESUPUESTO TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label id="presupuestoMaterialLabel"><?php if(isset($subtotal)) echo number_format((($subtotal-$cancelado)*1.16),2); else echo "0"; ?></label>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr style="border-top:1px solid #337ab7;">
                            <td colspan="7" rowspan="2">
                                <label for="" id="anticipoNota" style="color:blue;">Nota anticipo: <?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                                <input type="hidden" name="anticipoNota" value="<?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?>">
                            </td>
                            <td colspan="2" align="right">
                                <label>ANTICIPO</label>
                            </td>
                            <td align="center">
                                $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                                <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <label>TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="totalMaterialLabel"><?php if(isset($total)) echo number_format($total,2); else echo "0"; ?></label>
                                <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($total)) echo $total; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="9" align="right">
                                FIRMA DEL ASESOR QUE APRUEBA
                            </td>
                            <td align="center" colspan="3">
                                <img class='marcoImg' src='<?=  base_url(); ?><?php if(isset($firma_asesor)) { echo (($firma_asesor != "") ?  $firma_asesor : "assets/imgs/fondo_bco.jpeg"); } else { echo "assets/imgs/fondo_bco.jpeg"; } ?>' id='' style='width:80px;height:30px;'>
                                <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php if(isset($firma_asesor)) echo $firma_asesor ?>'>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-sm-12" hidden>
                <div class="alert alert-success" align="left">
                    <strong style="font-size:12px !important;font-weight: bold;">
                        Prioridad de refacciones:<br>
                        0 = Sin prioridad<br>
                        1 = Prioridad Baja <br>
                        2 = Prioridad Media <br>
                        3 = Prioridad Alta
                    </strong>
                </div>
            </div>
        </div>

        <br>
        <!-- Comentarios del asesor -->
        <div class="row">
            <div class="col-sm-5">
                <h4>Notas del asesor:</h4>
                <!-- Verificamos si es una vista del asesor -->
                <label for="" style="font-size:12px;"><?php if(isset($nota_asesor)){ if($nota_asesor != "") echo $nota_asesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-7" align="right">
                <h4>Archivo(s) de la cotización:</h4>
                <br>

                <?php if (isset($archivos_presupuesto)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivos_presupuesto); $i++): ?>
                        <?php if ($archivos_presupuesto[$i] != ""): ?>
                            <a href="<?=  base_url().$archivos_presupuesto[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?=  $contador; ?>)
                            </a>

                            <?php if ((($i+1)% 4) == 0): ?>
                                <br>
                            <?php endif; ?>
                            
                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivos_presupuesto) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>

                <hr>
                <!-- Para agregar mas documentos -->
                <input type="file" multiple name="uploadfiles[]">
                <input type="hidden" name="uploadfiles_resp" value="<?php if(isset($archivos_presupuesto_lineal)) echo $archivos_presupuesto_lineal;?>">
                <br>
            </div>
        </div>

        <hr>
        <!-- Comentarios del técnico -->
        <div class="row">
            <div class="col-md-6">
                <h4>Comentario del técnico para Refacciones:</h4>
                <textarea id="comentarioTecnico" name="comentarioTecnico" rows="2" style="width:100%;border-radius:4px;" placeholder="Notas del teécnico..."><?php if(isset($comentario_tecnico)) echo $comentario_tecnico;?></textarea>
            </div>

            <div class="col-sm-6" align="right">
                <h4>Archivo(s) del técnico para Refacciones:</h4>

                <?php if (isset($archivo_tecnico)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivo_tecnico); $i++): ?>
                        <?php if ($archivo_tecnico[$i] != ""): ?>
                            <a href="<?=  base_url().$archivo_tecnico[$i]; ?>" class="btn" style="background-color: #5cb8a5;border-color: #4c8bae;color:white;font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?=  $contador; ?>)
                            </a>

                            <?php if (($contador % 4) == 0): ?>
                                <br>
                            <?php endif; ?>

                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivo_tecnico) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>
                <!-- Para agregar mas documentos -->
                <input type="file" multiple name="uploadfilesTec[]"> 
                <input type="hidden" name="tempFileTecnico" value="<?php if(isset($archivo_tecnico_lineal)) echo $archivo_tecnico_lineal;?>">
                <br>
            </div>
        </div>

        <hr>
        <!-- comentarios de ventanilla -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios de Ventanilla:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_ventanilla)){ if($comentario_ventanilla != "") echo $comentario_ventanilla; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right"></div>
        </div>

        <hr>
        <!-- comentarios del jefe de taller -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios del Jefe de Taller:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_jdt)){ if($comentario_jdt != "") echo $comentario_jdt; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right">
                <h4>Archivo(s) del Jefe de Taller:</h4>

                <?php if (isset($archivo_jdt)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivo_jdt); $i++): ?>
                        <?php if ($archivo_jdt[$i] != ""): ?>
                            <a href="<?=  base_url().$archivo_jdt[$i]; ?>" class="btn btn-primary" style="font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?=  $contador; ?>)
                            </a>
                            
                            <?php if (($contador % 4) == 0): ?>
                                <br>
                            <?php endif; ?>
                            
                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivo_jdt) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <hr>
        <!-- comentarios de ventanilla -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios de Garantías:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_garantia)){ if($comentario_garantia != "") echo $comentario_garantia; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right"></div>
        </div>
        
        <br>
        <div>
            <div class="col-sm-12" align="center">
                <i class="fas fa-spinner cargaIcono"></i>
                <h4 class="error" id="errorEnvio"></h4>

                <!-- Datos basicos -->
                <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>"> 
                <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="8"> 
                <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
                <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

                <!-- validamos los envios de cada rol -->
                <input type="hidden" name="envioRefacciones" id="envioRefacciones" value="<?php if(isset($envia_ventanilla)) echo $envia_ventanilla; else echo "0"; ?>">
                <input type="hidden" name="" id="envioJefeTaller" value="<?php if(isset($envio_jdt)) echo $envio_jdt; else echo "0"; ?>">
                <input type="hidden" name="" id="envioGarantias" value="<?php if(isset($envio_garantia)) echo $envio_garantia; else echo "0"; ?>">

                <input type="hidden" id="vista" value="<?php if(isset($vista)) echo $vista; ?>">
                <input type="hidden" id="ordenT" value="<?php if(isset($tipo_orden)) echo $tipo_orden; ?>">
                <input type="hidden" id="curl_2" value="<?php if(isset($id_cita_url)) echo $id_cita_url; ?>">
                
                <?php if ($this->session->userdata('rolIniciado')): ?>
                    <?php if ($this->session->userdata('rolIniciado') == "TEC"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Tecnico/5";?>';">
                            Regresar
                        </button>
                    <?php elseif ($this->session->userdata('rolIniciado') == "ASETEC"): ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_AsesorTecnicoLC/5";?>';">
                            Regresar
                        </button>
                    <?php else: ?>
                        <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Tecnico/4";?>';">
                            Regresar
                        </button>
                    <?php endif; ?>
                <?php else: ?>
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Tecnico/4";?>';">
                        Regresar
                    </button>
                <?php endif; ?>

                <!--<input type="submit" class="btn btn-success terminarCotizacion" style="margin-left: 20px;" name="" value="Actualiar Presupuesto">-->
                <button type="submit" class="btn btn-success terminarCotizacion_tec" id="actualiza_tecncio_btn" style="margin-left: 20px;">
                    Actualizar Presupuesto
                </button>
            </div>
        </div>
            
    </form>
</div>

<div class="modal fade" id="eliminarRefaccion" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #ebccd1;font-weight: bold;">
            <div class="modal-header">
                <h5 class="modal-title" style="font-weight: bold;">ELIMINAR REFACCIÓN</h5>
            </div>
            <div class="modal-body">
                <div class="" align="center">
                    <i class="fas fa-spinner cargaIcono"></i>
                    <br>
                    <strong style="font-size:14px;" id="contenido">
                        ¿Seguro que quiere eliminar la siguiente refacción?
                        <br><br>
                        <samp id="label_refaccion" style="color:black;">1 ejemplo</samp>
                        <br><br>

                        <div class="" align="left">
                            <label style="color: darkred;font-size: 13px;">
                                NOTA: Esta acción no se puede deshacer, se eliminara en automático del presupuesto.
                            </label>
                            <br>
                        </div>
                    </strong>
                    <label id="notificacion" style="color: darkred;font-size: 13px;"></label>
                    <br>
                    <button type="button" class="btn btn-warning" data-dismiss="modal" id="notificacion_cerrar_Envio" style="display: none;">Aceptar</button>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="id_cita" value="<?php if(isset($id_cita)) echo $id_cita; ?>">
                <input type="hidden" name="" id="id_refaccion" value="">
                <input type="hidden" name="" id="renglon_eliminar" value="">
                <button type="button" class="btn btn-warning" data-dismiss="modal" id="cerrar_Envio">Cancelar</button>
                <button type="button" class="btn btn-danger" name="btnSign" id="btn_envio_refaccion">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script>
    function habilitar_rep(indice){
        if( $('#check_g_'+indice).is(':checked') ) {
            console.log('refaccion con garantia');
            $("#rep_add_"+indice).attr('disabled', false);
        } else{
            console.log('refaccion sin garantia');
            document.querySelector("#rep_add_"+indice).checked = false;
            //document.getElementById("rep_add_"+indice).focus();
            $("#rep_add_"+indice).attr('disabled', true);
        }

        actualiza_fila(indice);
    }

    function actualiza_fila(indice){
        var tipo_orden = "";
        if ($("#ordenT").length) {
            tipo_orden = $("#ordenT").val();
        }

        //Recuperamos los valores de toda la fila
        var fila = "";
        //0 Cantidad
        fila += parseFloat($("#cantidad_"+indice).val())+"_";
        //1 Descripcion
        fila += $("#descripcion_"+indice).val().replace('_', '-')+"_";
        //2 NUM. PIEZA
        fila += $("#pieza_"+indice).val().replace('_', '-')+"_";
        //3 EXISTE
        if ($("input[name='existe_"+indice+"']:radio").is(':checked')) {
            fila += $("input[name='existe_"+indice+"']:checked").val()+"_";
        }else {
            fila += ""+"_";
        }
        //4 PRECIO / P
        fila += parseFloat($("#costoCM_"+indice).val())+"_";
        //5 HORAS
        fila += parseFloat($("#horas_"+indice).val())+"_";
        //6 COSTO MO
        fila += parseFloat($("#totalReng_"+indice).val())+"_";

        var refaccion = parseFloat($("#cantidad_"+indice).val()) * parseFloat($("#costoCM_"+indice).val());
        var mo = parseFloat($("#horas_"+indice).val()) * parseFloat($("#totalReng_"+indice).val());
        
        //7 TOTAL RENGLON
        var total = refaccion+mo;

        fila += total.toFixed(2)+"_";

        //8 PRIORIDAD DE LA REFACCION
        fila += $("#prioridad_"+indice).val()+"_";

        //9 REPARACION
        fila += $("#ref_"+indice).val()+"_";

        //Si es una orden de bodyshop agregamos un campo
        if (tipo_orden == "3") {
            //Costo Aseguradora
            fila += $("#costoaseg_"+indice).val()+"_";
        }else{
            fila += "_";
        }

        if( $('#rep_add_'+indice).is(':checked') ) {
            fila += "1_";
        } else{
            fila += "0_";
        }

        $("#total_renglon_label_"+indice).text(total.toFixed(2));
        //Vaciamos lo recolectado en la variable
        $("#valoresCM_"+indice).val(fila);
        console.log(fila.split("_"));

        //Recuperamos la operacion para ir haciendo la autosuma
        var subtotal = parseFloat($("#subTotalMaterial").val());

        //Calculamos los nuevos valores
        var nvo_subtotal = subtotal + total;
        var iva = nvo_subtotal * 0.16;
        var presupuesto = nvo_subtotal * 1.16;

        //Imprimimos los valores
        $("#subTotalMaterialLabel").text(nvo_subtotal.toFixed(2));
        $("#subTotalMaterial").val(nvo_subtotal);

        $("#ivaMaterialLabel").text(iva.toFixed(2));
        $("#ivaMaterial").val(iva);

        $("#totalMaterialLabel").text(presupuesto.toFixed(2));
        $("#totalMaterial").val(presupuesto);

        $("#presupuestoMaterialLabel").text(presupuesto.toFixed(2));
    }

</script>