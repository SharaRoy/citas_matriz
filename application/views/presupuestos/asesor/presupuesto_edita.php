<div style="margin:10px;">
    <form name="" id="formulario" method="post" action="<?=base_url()."presupuestos/Presupuestos/validateFormR2"?>" autocomplete="on" enctype="multipart/form-data">
        <div class="alert alert-success" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
        </div>
        <div class="alert alert-danger" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Fallo el proceso.</strong>
        </div>
        <div class="alert alert-warning" align="center" style="display:none;">
            <strong style="font-size:20px !important;">Campos incompletos.</strong>
        </div>
        
        <div class="row">
            <div class="col-md-3">
                <label for="">No. Orden : </label>&nbsp;
                <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:100%;">
                <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="input_field" name="folio_externo" id="folio_externo" value="<?php if(set_value('folio_externo') != "") echo set_value('folio_externo'); elseif(isset($folio_externo)) echo $folio_externo; ?>" style="width:100%;">
                <?php echo form_error('folio_externo', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">No.Serie (VIN):</label>&nbsp;
                <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php if(set_value('noSerie') != "") echo set_value('noSerie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;" disabled>
                <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Modelo:</label>&nbsp;
                <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;">
                <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <div class="row">
            <div class="col-md-3">
                <label for="">Placas</label>&nbsp;
                <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;">
                <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Unidad</label>&nbsp;
                <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;">
                <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Técnico</label>&nbsp;
                <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;">
                <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
            </div>
            <div class="col-md-3">
                <label for="">Asesor</label>&nbsp;
                <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;">
                <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
            </div>
        </div>
        
        <br>
        <!--<div class="row table-responsive" align="right" id="panel_refacciones">
            <div class="col-md-4" align="right"></div>
            <div class="col-md-8" align="right">
                <div class="row">
                    <div class="col-md-9" align="right">
                        <label for="" class="error"> <strong>*</strong> Guardar renglon [<i class="fas fa-plus"></i>] </label>
                        <br>
                        <label for="" class="error"> <strong>*</strong> Eliminar renglon [<i class="fas fa-minus"></i>] </label>
                    </div>
                    <div class="col-md-1" align="right">
                        <a id="agregarRefaccionASE" class="btn btn-success" style="color:white;">
                            <i class="fas fa-plus"></i>
                        </a>
                    </div>
                    <div class="col-md-1" align="right">
                        <a id="eliminarRefaccionASE" class="btn btn-danger" style="color:white; width: 1cm;" disabled>
                            <i class="fas fa-minus"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>-->

        <?php if (isset($firma_asesor)): ?>
            <?php if ($firma_asesor != ""): ?>
                <div class="alert alert-danger" align="center">
                    <strong style="font-size:13px !important;color:darkblue;">
                        Proceso del presupuesto: <?php echo $ubicacion_proceso; ?>
                        <br>
                        <!--Proceso del presupuesto con garantías: <?php echo $ubicacion_proceso_garantia; ?>-->
                    </strong>
                    <br>
                    <strong style="font-size:13px !important;">
                        Vigencia: Un mes a partir de la fecha de emisión del presente presupuesto. (Fecha de vencimiento: <?php if(isset($fecha_limite)) echo $fecha_limite; ?>)
                    </strong>
                </div>
            <?php endif ?>
        <?php endif ?>

        <div class="row">
            <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
                <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                    <thead>
                        <tr style="background-color: #eee;">
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REP.
                            </td>
                            <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                CANTIDAD
                            </td>
                            <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                D E S C R I P C I Ó N
                            </td>
                            <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                NUM. PIEZA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 10px;" align="center" colspan="3">
                                EXISTE
                            </td>
                            <td style="display:none;width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;" align="center" rowspan="2">
                                FECHA<br>TENTATIVA
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PRECIO REFACCIÓN
                            </td>
                            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                HORAS
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                COSTO MO
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                TOTAL DE REPARACÓN
                            </td>
                            <td style="display: none;width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PRIORIDAD <br> REFACCION
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                PZA. GARANTÍA
                                <input type="hidden" name="" id="indiceTablaMateria" value="<?php if (isset($renglon)) echo count($renglon)+1; else echo "1"; ?>">
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                REPARACIÓN ADICIONAL
                            </td>
                            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                                APRUEBA<br>REFACCIÓN
                            </td>
                        </tr>
                        <tr style="background-color: #ddd;">
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                SI
                            </td>
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                NO
                            </td>
                            <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                                PLANTA
                            </td>
                        </tr>
                    </thead>
                    <tbody id="cuerpoMateriales">
                        <!-- Verificamos si existe la informacion -->
                        <?php if (isset($renglon)): ?>
                            <!-- Comprobamos que haya registros guardados -->
                            <?php if (count($renglon)>0): ?>
                                <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                    <!-- si hay refacciones con garantias, las remarcamos -->
                                    <tr id='fila_<?php echo $index+1; ?>' <?php if($pza_garantia[$index] == "1") echo "style='background-color:#faebcc;'"; ?>>
                                        <!-- REP. -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='1' class='input_field_lg' id='ref_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $referencia[$index]; ?></textarea>
                                        </td>
                                        <!-- CANTIDAD -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur='actualizaSumaFinal(<?php echo $index+1; ?>)' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $cantidad[$index]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>
                                        </td>
                                        <!-- D E S C R I P C I Ó N -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='2' class='input_field_lg' onblur='actualizaSumaFinal(<?php echo $index+1; ?>)' id='descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $descripcion[$index]; ?></textarea>
                                        </td>
                                        <!-- NUM. PIEZA -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <textarea rows='2' class='input_field_lg' onblur='actualizaSumaFinal(<?php echo $index+1; ?>)' id='pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $num_pieza[$index]; ?></textarea>
                                        </td>
                                        <!-- EXISTE -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaSi_<?php echo $index+1; ?>' class='input_field' onclick='actualizaSumaFinal(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="SI" <?php if($existe[$index] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaNo_<?php echo $index+1; ?>' class='input_field' onclick='actualizaSumaFinal(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="NO" <?php if($existe[$index] == "NO") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='radio' id='apuntaPlanta_<?php echo $index+1; ?>' class='input_field' onclick='actualizaSumaFinal(<?php echo $index+1; ?>)' name='existe_<?php echo $index+1; ?>' value="PLANTA" <?php if($existe[$index] == "PLANTA") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                        <!-- FECHA TENTANTIVA - ENTREGA -->
                                        <td style='display:none;border:1px solid #337ab7;' align='center'> 
                                            <input type="date" class='input_field' disabled id="fecha_tentativa_<?php echo $index+1; ?>" value="<?php if($fecha_planta[$index] != "0000-00-00") echo $fecha_planta[$index]; else echo date('Y-m-d'); ?>" <?php if($fecha_planta[$index] == "0000-00-00") echo "style='display: none;'";?>>  
                                        </td>
                                        <!-- PRECIO REFACCIÓN -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            $<input type='number' step='any' min='0' class='input_field' onblur='actualizaSumaFinal(<?php echo $index+1; ?>)' id='costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_pieza[$index]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <!-- HORAS -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <input type='number' step='any' min='0' class='input_field' onblur='actualizaSumaFinal(<?php echo $index+1; ?>)' id='horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $horas_mo[$index]; ?>' style='width:90%;text-align:left;'>
                                        </td>
                                        <!-- COSTO MO -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            $<input type='number' step='any' min='0' disabled id='totalReng_<?php echo $index+1; ?>' onblur="actualizaSumaFinal(<?php echo $index+1; ?>)"  class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_mo[$index]; ?>' style='width:90%;text-align:left;'>
                                            
                                            <!-- total de la refaccion -->
                                            <input type='hidden' id='totalOperacion_<?php echo $index+1; ?>' value='<?php echo $total_refacion[$index]; ?>'>

                                            <!-- Indice interno de la recaccion -->
                                            <input type='hidden' id='id_refaccion_<?php echo $index+1; ?>' name='id_registros[]' value="<?php echo $id_refaccion[$index]; ?>">
                                            <!-- Informacion del renglon -->
                                            <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $renglon[$index]; ?>">
                                            <!-- Informacion de datos de garantia de la refaccion (si aplica) -->
                                            <input type='hidden' id='valoresGarantias_<?php echo $index+1; ?>' name='registrosGarantias[]' value="<?php echo $renglonGarantia[$index]; ?>">

                                            <!-- Informacion del aprueba jefe de taller -->
                                            <input type='hidden' id='aprueba_jdt_<?php echo $index+1; ?>' value="<?php echo $autoriza_jefeTaller[$index]; ?>">
                                            <!-- Informacion pieza con garantia -->
                                            <input type='hidden' name="garantia[]" id='garantia_<?php echo $index+1; ?>' value="<?php echo $pza_garantia[$index]; ?>">
                                            <!-- Informacion de prioridad de la refaccion -->
                                            <input type="hidden" name="prioridad_<?php echo $index+1; ?>" id="prioridad_<?php echo $index+1; ?>" value="<?php if(isset($prioridad[$index])) echo $prioridad[$index]; else echo "1"; ?>">
                                        </td>
                                        <!-- TOTAL DE REPARACÓN -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <!--<?php if ($pza_garantia[$index] == "1"): ?>
                                                <label id="total_renglon_label_<?php echo $index+1; ?>" style="color:darkblue;">$ 0.00</label>
                                            <?php else: ?>
                                                <label id="total_renglon_label_<?php echo $index+1; ?>" style="color:darkblue;">$ <?php echo number_format($total_refacion[$index],2); ?></label>    
                                            <?php endif ?>-->
                                            <label id="total_renglon_label_<?php echo $index+1; ?>" style="color:darkblue;">$ <?php echo number_format($total_refacion[$index],2); ?></label>    
                                        </td>
                                        <!-- PRIORIDAD <br> REFACCION -->
                                        <td style='display: none;border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <!--<input type='number' step='any' min='0' class='input_field' id='prioridad_<?php echo $index+1; ?>' name='prioridad_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $prioridad[$index]; ?>' onblur="actualizaSumaFinal(<?php echo $index+1; ?>)" style='width:70%;text-align:center;'>-->
                                        </td>
                                        <!-- PZA. GARANTÍA -->
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <?php if ($pza_garantia[$index] == "1"): ?>
                                                SI
                                                <br>
                                                <?php if ($autoriza_garantia[$index] == "1"): ?>
                                                    <label style="font-size: 9px;font-weight: bold;color:darkgreen;">(Autorizado)</label>
                                                <?php else: ?>
                                                    <label style="font-size: 9px;font-weight: bold;color:red;">(No autorizado)</label>
                                                <?php endif ?>
                                            <?php else: ?>
                                                NO
                                            <?php endif ?>
                                        </td>
                                        <td style='border:1px solid #337ab7;' align='center'>
                                            <?php if ($rep_adicional[$index] == "1"): ?>
                                                SI
                                            <?php else: ?>
                                                NO
                                            <?php endif ?>
                                        </td>
                                        <!-- APROBAR REFACCION -->
                                        <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                            <input type='checkbox' class='input_field' id="asesor_check_<?php echo $index+1; ?>" name="autoriza_asesor[]" value="<?php echo $id_refaccion[$index]; ?>" onclick='actualizaSumaFinal(<?php echo $index+1; ?>)' <?php if($autoriza_asesor[$index] == "1") echo "checked"; ?> style="transform: scale(1.5);">
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="13" align="center">
                                        No se cargaron los datos
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="13" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <td colspan="10" align="center">
                            </td>
                            <td colspan="3" align="center">
                                <?php echo form_error('autoriza_asesor', '<br><span class="error">', '</span><br>'); ?>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7" rowspan="2" style="font-size:14px; vertical-align: middle;color: red;font-weight: bold;" align="left">
                                <input type="hidden" value="<?= ((isset($sin_aprobar_jdt)) ? $sin_aprobar_jdt : '') ?>">
                                <?= ((isset($sin_aprobar_jdt)) ? (($sin_aprobar_jdt > 0) ? "Faltaron ". $sin_aprobar_jdt ." refaccione(s) por aprobar de la revisión del jefe de taller" : "") : "") ?>
                            </td>
                            <td colspan="3" align="right">
                                <label>SUB-TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="subTotalMaterialLabel"><?php if(isset($subtotal)) echo number_format($subtotal,2); else echo "0"; ?></label>
                                <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subtotal)) echo $subtotal; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="3" align="right" class="error">
                                <label>CANCELADO</label>
                            </td>
                            <td colspan="1" align="center" class="error">
                                $<label id="canceladoMaterialLabel"><?php if(isset($cancelado)) echo number_format($cancelado,2); else echo "0"; ?></label>
                                <input type="hidden" name="canceladoMaterial" id="canceladoMaterial" value="<?php if(isset($cancelado)) echo $cancelado; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="10" align="right">
                                <label>I.V.A.</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="ivaMaterialLabel"><?php if(isset($iva)) echo number_format($iva,2); else echo "0"; ?></label>
                                <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($iva)) echo $iva; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="10" align="right">
                                <label>PRESUPUESTO TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label id="presupuestoMaterialLabel"><?php if(isset($subtotal)) echo number_format((($subtotal-$cancelado)*1.16),2); else echo "0"; ?></label>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                        <tr style="border-top:1px solid #337ab7;">
                            <td colspan="8" rowspan="2">
                                <label for="" id="anticipoNota" style="color:blue;">Nota anticipo: <?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                                <input type="hidden" name="anticipoNota" value="<?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?>">
                            </td>
                            <td colspan="2" align="right">
                                <label>ANTICIPO</label>
                            </td>
                            <td align="center">
                                $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                                <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="right">
                                <label>TOTAL</label>
                            </td>
                            <td colspan="1" align="center">
                                $<label for="" id="totalMaterialLabel"><?php if(isset($total)) echo number_format($total,2); else echo "0"; ?></label>
                                <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($total)) echo $total; else echo "0"; ?>">
                            </td>
                            <td colspan="2"></td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="col-sm-12">
                <div class="alert alert-warning" align="left">
                    <strong style="font-size:14px !important;font-weight: bold;">
                        **Los costos de las refacciones con garantías no serán visibles para el cliente, tampoco se sumaran al presupuesto final.
                    </strong>
                </div>
            </div>

            <div class="col-sm-12" hidden>
                <div class="alert alert-success" align="left">
                    <strong style="font-size:14px !important;font-weight: bold;">
                        Prioridad de refacciones:<br>
                        0 = Sin prioridad<br>
                        1 = Prioridad Baja <br>
                        2 = Prioridad Media <br>
                        3 = Prioridad Alta
                    </strong>
                </div>
            </div>
        </div>

        <br>
        <!-- Comentarios del asesor -->
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-8" align="right">
                <h4>Archivo(s) de la cotización:</h4>
                <br>

                <?php if (isset($archivos_presupuesto)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivos_presupuesto); $i++): ?>
                        <?php if ($archivos_presupuesto[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivos_presupuesto[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>

                            <?php if ((($i+1)% 4) == 0): ?>
                                <br>
                            <?php endif; ?>
                            
                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivos_presupuesto) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>

                <hr>
                <!-- Para agregar mas refacciones -->
                <input type="file" multiple name="uploadfiles[]">
                <input type="hidden" name="uploadfiles_resp" value="<?php if(isset($archivos_presupuesto_lineal)) echo $archivos_presupuesto_lineal;?>">
                <br>
            </div>
        </div>

        <hr>
        <!-- Comentarios del técnico -->
        <div class="row">
            <div class="col-md-6">
                <h4>Comentario del técnico para Refacciones:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_tecnico)){ if($comentario_tecnico != "") echo $comentario_tecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>

            <div class="col-sm-6" align="right">
                <h4>Archivo(s) del técnico para Refacciones:</h4>

                <?php if (isset($archivo_tecnico)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivo_tecnico); $i++): ?>
                        <?php if ($archivo_tecnico[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo_tecnico[$i]; ?>" class="btn" style="background-color: #5cb8a5;border-color: #4c8bae;color:white;font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>

                            <?php if (($contador % 4) == 0): ?>
                                <br>
                            <?php endif; ?>

                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivo_tecnico) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <hr>
        <!-- comentarios de ventanilla -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios de Ventanilla:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_ventanilla)){ if($comentario_ventanilla != "") echo $comentario_ventanilla; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right"></div>
        </div>

        <hr>
        <!-- comentarios del jefe de taller -->
        <div class="row">
            <div class="col-sm-6" align="left">
                <h4>Comentarios del Jefe de Taller:</h4>
                <label for="" style="font-size:12px;"><?php if(isset($comentario_jdt)){ if($comentario_jdt != "") echo $comentario_jdt; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
            </div>
            <div class="col-sm-6" align="right">
                <h4>Archivo(s) del Jefe de Taller:</h4>

                <?php if (isset($archivo_jdt)): ?>
                    <?php $contador = 1; ?>
                    <?php for ($i = 0; $i < count($archivo_jdt); $i++): ?>
                        <?php if ($archivo_jdt[$i] != ""): ?>
                            <a href="<?php echo base_url().$archivo_jdt[$i]; ?>" class="btn btn-primary" style="font-size: 9px;font-weight: bold;" target="_blank">
                                DOC. (<?php echo $contador; ?>)
                            </a>
                            
                            <?php if (($contador % 4) == 0): ?>
                                <br>
                            <?php endif; ?>
                            
                            <?php $contador++; ?>
                        <?php endif; ?>
                    <?php endfor; ?>

                    <?php if (count($archivo_jdt) == 0): ?>
                        <h4 style="text-align: right;">Sin archivos</h4>
                        <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6" align="center" style="border: 0.5px solid darkblue;">
                <table style="width: 90%;margin-left: 10px;margin-right: 10px;">
                    <tr>
                        <td align="center">
                            <h4>Notas del asesor:</h4>
                            <textarea rows='2' name="nota_asesor" id='nota_asesor' placeholder="Sin comentarios" style='width:100%;text-align:left;font-size:12px;'><?php if(set_value('nota_asesor') != "") { echo set_value('nota_asesor');} else { if(isset($nota_asesor)){ if($nota_asesor != "") echo $nota_asesor;}}?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <?php if (isset($firma_asesor)): ?>
                                <?php if ($firma_asesor == ""): ?>
                                    <input type="hidden" id="rutaFirmaAsesorCostos" name="rutaFirmaAsesorCostos" value="<?= set_value('rutaFirmaAsesorCostos');?>">
                                    <img class="marcoImg" src="<?php if(set_value('rutaFirmaAsesorCostos') != "") echo set_value('rutaFirmaAsesorCostos');  ?>" id="firmaAsesorCostos" alt="" style="height:2cm;width:4cm;">
                                    <br>
                                <?php else: ?>
                                    <input type="hidden" id="rutaFirmaAsesorCostos" name="rutaFirmaAsesorCostos" value="<?php if(isset($firma_asesor)) echo $firma_asesor;?>" >
                                    <img class="marcoImg" src="<?php if(isset($firma_asesor)) echo base_url().$firma_asesor;?>" id="firmaAsesorCostos" alt="" style="height:2cm;width:4cm;">
                                    <br>
                                <?php endif; ?>
                            <?php else: ?>
                                <input type="hidden" id="rutaFirmaAsesorCostos" name="rutaFirmaAsesorCostos" value="<?= set_value('rutaFirmaAsesorCostos');?>">
                                <img class="marcoImg" src="<?php if(set_value('rutaFirmaAsesorCostos') != "") echo set_value('rutaFirmaAsesorCostos');  ?>" id="firmaAsesorCostos" alt="" style="height:2cm;width:4cm;">
                                <br>
                            <?php endif; ?>
                            <?php echo form_error('rutaFirmaAsesorCostos', '<span class="error">', '</span>'); ?><br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <?php if (isset($firma_asesor)): ?>
                                <?php if ($firma_asesor == ""): ?>
                                    <a id="AsesorCostos" class="cuadroFirma btn btn-primary" data-value="AsesorCostos" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Asesor</a>
                                <?php endif; ?>
                            <?php else: ?>
                                <a id="AsesorCostos" class="cuadroFirma btn btn-primary" data-value="AsesorCostos" data-target="#firmaDigital" data-toggle="modal" style="color:white;"> Firma Asesor </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-sm-3"></div>
        </div>
        
        <br>
        <div class="col-sm-12" align="center">
            <!-- Datos basicos -->
            <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>"> 
            <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="2">
            <input type="hidden" name="" id="origen" value="Presupuesto">

            <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
            <input type="hidden" name="" id="usuarioActivo" value="<?php if ($this->session->userdata('usuario')) echo $this->session->userdata('usuario'); else echo "Sin sesión"; ?>">
            <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

            <!-- validamos los envios de cada rol -->
            <input type="hidden" name="envioRefacciones" id="envioRefacciones" value="<?php if(isset($envia_ventanilla)) echo $envia_ventanilla; else echo "0"; ?>">
            <input type="hidden" name="" id="envioJefeTaller" value="<?php if(isset($envio_jdt)) echo $envio_jdt; else echo "0"; ?>">
            <input type="hidden" name="" id="envioGarantias" value="<?php if(isset($envio_garantia)) echo $envio_garantia; else echo "0"; ?>">
            <input type="hidden" name="" id="finalizaAsesor" value="<?php if(isset($firma_asesor)) {if($firma_asesor != "") echo "1"; else "0";} else echo "0"; ?>">
            
            <?php if ($this->session->userdata('rolIniciado')): ?>
                <?php if ($this->session->userdata('rolIniciado') == "ASE"): ?>
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Asesor/5";?>';">
                        Regresar
                    </button>
                <?php elseif ($this->session->userdata('rolIniciado') == "ASETEC"): ?>
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_AsesorTecnico/5";?>';">
                        Regresar
                    </button>
                <?php else: ?>
                    <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Asesor/4";?>';">
                        Regresar
                    </button>
                <?php endif; ?>
            <?php else: ?>
                <button type="button" class="btn btn-dark" onclick="location.href='<?=base_url()."Listar_Asesor/4";?>';">
                    Regresar
                </button>
            <?php endif; ?>

            <input type="submit" class="btn btn-success terminarCotizacion" style="margin-left: 20px;" name="aceptarCotizacion" value="Actualizar Presupuesto">

        </div>
    </form>
</div>

<!-- Modal para la firma eléctronica-->
<div class="modal fade" id="firmaDigital" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="firmaDigitalLabel"></h5>
            </div>
            <div class="modal-body">
                <!-- signature -->
                <div class="signatureparent_cont_1" align="center">
                    <canvas id="canvas" width="430" height="200" style='border: 1px solid #CCC;'>
                        Su navegador no soporta canvas
                    </canvas>
                </div>
                <!-- signature -->
            </div>
            <div class="modal-footer">
                <input type="hidden" name="" id="destinoFirma" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="cerrarCuadroFirma">Cerrar</button>
                <button type="button" class="btn btn-info" id="limpiar">Limpiar firma</button>
                <button type="button" class="btn btn-primary" name="btnSign" id="btnSign" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>

