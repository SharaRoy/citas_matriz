<div style="margin:10px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Inventario actualizado con exito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Se supero el tiempo de espera.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">No se actualizo el registro.</strong>
    </div>

    <div class="panel-body">
        <div class="col-md-12">
            <h3 align="center">Presupuestos Multipunto</h3>
            <br>
            <div class="panel panel-default">
                <div class="panel-body" style="border:2px solid black;">
                    <div class="row">
                        <div class="col-sm-1">
                            <br>
                            <input type="button" class="btn btn-dark" style="color:white;margin-top: 20px;" onclick="location.href='<?=base_url()."Panel_Asesor/5";?>';" name="" value="Regresar">
                        </div>

                        <div class="col-sm-3" style="padding: 0px;padding-left: 30px;">
                            <!--<h5>Fecha Inicio:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_inicio" style="padding-top: 0px;" max="<?php echo date("Y-m-d"); ?>" value="">
                            </div>-->
                        </div>

                        <div class="col-sm-3">
                            <!--<h5>Fecha Fin:</h5>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                </div>
                                <input type="date" class="form-control" id="fecha_fin" style="padding-top: 0px;" max = "<?php echo date("Y-m-d"); ?>" value="">
                            </div>-->
                        </div>

                        <!-- formulario de busqueda -->
                        <div class="col-sm-3">
                            <h5>Busqueda Gral.:</h5>
                            <div class="input-group">
                                <!--<div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>-->
                                <input type="text" class="form-control" id="busqueda_gral" placeholder="Buscar...">
                            </div>
                            <input type="hidden" name="" id="indicador" value="JefeTaller">
                        </div>
                        <!-- /.formulario de busqueda -->

                        <div class="col-sm-2" align="right">
                            <br><br>
                            <button class="btn btn-secondary" type="button" name="" id="btnBusqueda_asesor" style="margin-right: 15px;"> 
                                <i class="fa fa-search"></i>
                            </button>

                            <button class="btn btn-secondary" type="button" name="" id="btnLimpiar" onclick="location.reload()">
                                <i class="fa fa-trash"></i>
                                <!-- Limpiar Busqueda-->
                            </button>
                        </div>
                    </div>

                    <br>
                    <div class="form-group" align="center" style="overflow-x: scroll;overflow-y: scroll;">
                        <i class="fas fa-spinner cargaIcono"></i>
                        <table class="table table-bordered table-responsive" style="width:100%;">
                            <thead>
                                <tr style="font-size:14px;background-color: #ddd;">
                                    <!--<td align="center" style="width: 10%;"></td>-->
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>NO. ORDEN</strong></td>
                                    <!--<td align="center" style="width: 10%;vertical-align: middle;"><strong>ORDEN DMS</strong></td>-->
                                    <!--<td align="center" style="width: 10%;vertical-align: middle;"><strong>TIPO PRESUPUESTO</strong></td>-->
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ORDEN INTELISIS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>VEHÍCULO</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>PLACAS</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>ASESOR</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>TÉCNICO</strong></td>
                                    <!--<td align="center" style="width: 10%;vertical-align: middle;"><strong>FIRMA ASESOR</strong></td>-->
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>APRUEBA CLIENTE</strong></td>
                                    <td align="center" style="width: 10%;vertical-align: middle;"><strong>EDO. REFACCIONES</strong></td>
                                    <td align="center" colspan="3" style="width: 10%;vertical-align: middle;"><strong>ACCIONES</strong></td>
                                </tr>
                            </thead>
                            <tbody class="campos_buscar"> 
                                <!-- Verificamos que se hayan cargado los datos para mostrar-->
                                <?php if (isset($id_cita)): ?>
                                    <!-- Imprimimos los datos-->
                                    <?php foreach ($id_cita as $index => $valor): ?>
                                        <tr style="font-size:11px;">
                                            <!--<td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo count($idOrden) - $index; ?>
                                            </td>-->
                                            <!--<td align="center" style="width:10%;vertical-align: middle;background-color: <?php if ($firma_asesor[$index] == "SI") echo "#68ce68"; else echo "#fffff"; ?>;">-->
                                            <td align="center" style="width:10%;vertical-align: middle;background-color: <?php if ($acepta_cliente[$index] != "") echo "#68ce68"; else echo "#fffff"; ?>;">
                                                <?php echo $id_cita[$index]; ?>
                                            </td>
                                            <!--<td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $orden_dms[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $tipo[$index]; ?>
                                            </td>-->
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $idIntelisis[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $modelo[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $placas[$index]; ?>
                                            </td>
                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php echo $asesor[$index]; ?>
                                            </td>
                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $tecnico[$index]; ?>
                                            </td>
                                            <!--<td align="center" style="width:15%;vertical-align: middle;">
                                                <?php echo $firma_asesor[$index]; ?>
                                            </td>-->

                                            <td align="center" style="width:10%;vertical-align: middle;">
                                                <?php if ($acepta_cliente[$index] != ""): ?>
                                                    <?php if ($acepta_cliente[$index] == "Si"): ?>
                                                        <label style="color: darkgreen; font-weight: bold;">CONFIRMADA</label>
                                                    <?php elseif ($acepta_cliente[$index] == "Val"): ?>
                                                        <label style="color: darkorange; font-weight: bold;">DETALLADA</label>
                                                    <?php else: ?>
                                                        <label style="color: red ; font-weight: bold;">RECHAZADA</label>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    <label style="color: black; font-weight: bold;">SIN CONFIRMAR</label>
                                                <?php endif; ?>
                                            </td>

                                            <td align="center" style="width:15%;vertical-align: middle; <?php echo $color[$index];?>">
                                                <?php if (($estado_refacciones[$index] == "0")&&($acepta_cliente[$index] != "No")): ?>
                                                    SIN SOLICITAR
                                                <?php elseif ($acepta_cliente[$index] == "No"): ?>
                                                    RECHAZADA
                                                <?php elseif ($estado_refacciones[$index] == "1"): ?>
                                                    SOLICITADAS
                                                <?php elseif ($estado_refacciones[$index] == "2"): ?>
                                                    RECIBIDAS
                                                <?php elseif ($estado_refacciones[$index] == "3"): ?>
                                                    ENTREGADAS
                                                <?php else: ?>
                                                    SIN SOLICITAR *
                                                <?php endif ?>
                                            </td>

                                            <!--<td align="center" style="width:15%;vertical-align: middle;">
                                                <a href="<?=base_url().'OrdenServicio_Revision/'.$id_cita[$index];?>" class="btn btn-warning" target="_blank" style="font-size: 12px;">ORDEN</a>
                                            </td>    -->

                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <!-- Nuevos presupuestos -->
                                                <?php if ($tipo[$index] != "TRADICIONAL *"): ?>
                                                    <!-- Verificamos si se va a editar o se va visualizar el presupuesto -->
                                                    <?php if ($firma_asesor[$index] == "SI"): ?>
                                                        <a href="<?=base_url()."Presupuesto_Asesor/".$id_cita_url[$index];?>" class="btn btn-info" style="color:white;font-size: 10px;">VER</a>
                                                    <?php else: ?>
                                                        <a href="<?=base_url()."Presupuesto_Asesor/".$id_cita_url[$index];?>" class="btn btn-primary" style="color:white;font-size: 10px;">REVISAR</a>
                                                    <?php endif ?>
                                                <?php else: ?>
                                                    <!-- Verificamos si se va a editar o se va visualizar el presupuesto -->
                                                    <?php if ($firma_asesor[$index] == "SI"): ?>
                                                        <a href="<?=base_url()."Multipunto_Cotizacion/".$id_cita_url[$index];?>" class="btn btn-info" style="color:white;font-size: 10px;">VER</a>
                                                    <?php else: ?>
                                                        <a href="<?=base_url()."Multipunto_Cotizacion/".$id_cita_url[$index];?>" class="btn btn-primary" style="color:white;font-size: 10px;">REVISAR</a>
                                                    <?php endif ?>
                                                <?php endif ?>
                                                
                                            </td>

                                            <td align="center" style="width:15%;vertical-align: middle;">
                                                <!-- Nuevos presupuestos -->
                                                <?php if ($tipo[$index] != "TRADICIONAL *"): ?>
                                                    <!--Si ya firmo el asesor y el cliente aun no toma una decision, dejamos afectar al asesor-->
                                                    <?php if (($firma_asesor[$index] == "SI")&&($acepta_cliente[$index] == "")): ?>
                                                        <a href="<?=base_url()."Presupuesto_Cliente/".$id_cita_url[$index];?>" class="btn btn-warning" style="color:white;font-size: 10px;">AFECTAR<br>PRESUPUESTO</a>
                                                    <!--Si ya firmo el asesor y el cliente ya toma una decisionmostramos cotizacion final-->
                                                    <?php elseif (($firma_asesor[$index] == "SI")&&($acepta_cliente[$index] != "")): ?>
                                                        <a href="<?=base_url()."Presupuesto_Cliente/".$id_cita_url[$index];?>" class="btn btn-success" style="color:white;font-size: 10px;">PRESUPUESTO<br>FINAL</a>
                                                    <?php endif ?>
                                                <?php else: ?>
                                                    <!-- Si el asesor ya firmo y el cliente no ha confirmado/rechazado la cotización -->
                                                    <?php if (($firma_asesor[$index] == "SI") && ($acepta_cliente[$index] == "")): ?> 
                                                        <button type="button" class="btn btn-secondary" style="color:white;" onclick="location.href='<?=base_url()."Cotizacion_Cliente/".$id_cita_url[$index];?>';">Aprobar/Rechazar</button>
                                                    <!-- Si el asesor no a firmado y/o el cliente ya confirmo/rechazo la cotización -->
                                                    <?php elseif (($firma_asesor[$index] == "SI")&&($acepta_cliente[$index] != "")): ?>
                                                        <button type="button" class="btn btn-info" style="color:white;" onclick="location.href='<?=base_url()."Multipunto_Cotizacion/".$id_cita_url[$index];?>';">Ver</button>
                                                    <?php endif; ?>
                                                <?php endif ?>
                                            </td>
                                            <!-- Agregar un comentario sin entrar el presupuesto -->
                                            <!--<td align="center" style="width:15%;vertical-align: middle;">
                                                <a class="addComentarioBtn" onclick="btnAddClick(<?php echo $ID_CITA[$index]; ?>)" title="Agregar Comentario" data-id="<?php echo $ID_CITA[$index]; ?>" data-target="#addComentario" data-toggle="modal" style="font-size: 18px;color: black;">
                                                    <! -- <i class="fas fa-comments"></i> - ->
                                                    AGREGAR<br>COMENTARIO
                                                </a>
                                            </td> -->
                                        </tr>
                                    <?php endforeach; ?>
                                <!-- Si no se cargaron los datos para mostrar -->
                                <?php else: ?>
                                    <tr>
                                        <td colspan="8" style="width:100%;" align="center">Sin registros que mostrar</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>

                        <input type="hidden" name="respuesta" id="respuesta" value="<?php if(isset($mensaje)) echo $mensaje; ?>">
                        <input type="hidden" name="" id="rolVista" value="listaAsesor">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal para agregar un comentario -->
<div class="modal fade" id="addComentario" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:3000;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" align="right">Ingresar comentario</h3>
            </div>
            <div class="modal-body">
                <div class="alert alert-success" align="center" style="display:none;" id="OkResult">
                    <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
                </div>
                <div class="alert alert-danger" align="center" style="display:none;" id="errorResult">
                    <strong style="font-size:20px !important;">Fallo el proceso.</strong>
                </div>
                <div class="row">
                    <div class="col-sm-12"  align="center">
                        <i class="fas fa-spinner cargaIcono"></i>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Comentario</h4>
                        <textarea rows='3' class='form-control' name="" id='comentario' style='width:100%;text-align:left;font-size:12px;'></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" class='form-control' name="" id="idMagigAdd" value="">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnEnviarComentario">Enviar</button>
            </div>
        </div>
    </div>
</div>