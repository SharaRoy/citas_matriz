<?php 
    // Eliminamos cache
    //la pagina expira en una fecha pasada
    header ("Expires: Thu, 27 Mar 1980 23:59:00 GMT"); 
    //ultima actualizacion ahora cuando la cargamos
    header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); 
    //no guardar en CACHE
    header ("Cache-Control: no-cache, must-revalidate"); 
    header ("Pragma: no-cache");

    ob_start();

?>

<style media="screen">
    .formato{
        color:black;
    }

    #encabezado {
        color: #fff;
        background-color: #337ab7;
        border-color: #337ab7;
        border-radius: 4px;
    }

    p, label{
        text-align: justify;
        font-size:8px;
        /*color: #337ab7;*/
    }

    .tituloTxt{
        font-size:9px;
        color: #337ab7;
    }
    .division{
        background-color: #ddd;
    }
    .headers_table{
        border:1px solid black;
        border-style: double;
    }
    td,tr{
        padding: 0px 0px 0px 0px !important;
        font-size: 8px;
        color:#337ab7;
    }
</style>

<page backtop="7mm" backbottom="7mm" backleft="5mm" backright="5mm">
    <table style="width: 100%;margin-left: 20px;">
        <tr>
            <td width="20%">
                <img src="<?= base_url().$this->config->item('logo'); ?>" alt="" class="logo" style="height:45px;"> 
            </td>
            <td style="width:60%;" colspan="2" align="center">
                <strong>
                    <span class="color-blue" style="font-size:12px;color: #337ab7;" align="center">
                        <?= SUCURSAL ?>, S.A. DE C.V.
                    </span>
                </strong>

                <p class="justify" style="font-size:8px;" align="center">
                    <?=$this->config->item('encabezados_txt')?>
                </p>
            </td>
            <td width="20%" align="" style="">
                <img src="<?php echo base_url(); ?>assets/imgs/logo.png" alt="" class="logo" style="width:100px;height:35px;"><br>
            </td>
        </tr>
    </table>

    <br><br>
    <table style="width:100%;border:0.5px solid #337ab7;border-radius: 4px;margin-left: 10px;">
        <tr>
            <td style="width: 15%;">
                <label style="font-size: 10px;"><strong>NO. ORDEN: </strong></label>
            </td>
            <td style="width: 10%;border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($id_cita)) echo $id_cita; else echo ""; ?>
                </label>
            </td>
            <td style="width: 15%;" align="right">
                <label style="font-size: 10px;"><strong>ORDEN INTELISIS: </strong></label>
            </td>
            <td style="width: 10%;border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($folio_externo)) echo $folio_externo; else echo ""; ?>
                </label>
            </td>
            <td style="width: 10%;" align="right">
                <label style="font-size: 10px;"><strong>SERIE: </strong></label>
            </td>
            <td style="width: 15%;border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($serie)) echo strtoupper($serie); else echo ""; ?>
                </label>
            </td>
            <td style="width: 10%;" align="right">
                <label style="font-size: 10px;"><strong>MODELO: </strong></label>
            </td>
            <td style="width: 10%;border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($modelo)) echo strtoupper($modelo); else echo ""; ?>
                </label>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <label style="font-size: 10px;"><strong>PLACAS: </strong></label>
            </td>
            <td style="border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($placas)) echo strtoupper($placas); else echo ""; ?>
                </label>
            </td>
            <td align="right">
                <label style="font-size: 10px;"><strong>UNIDAD: </strong></label>
            </td>
            <td colspan="2" style="border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($categoria)) echo strtoupper($categoria); else echo ""; ?>
                </label>
            </td>
            <td align="right">
                <label style="font-size: 10px;"><strong>TÉCNICO: </strong></label>
            </td>
            <td colspan="2" style="border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($tecnico)) echo strtoupper($tecnico); else echo ""; ?>
                </label>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <label style="font-size: 10px;"><strong>ASESOR: </strong></label>
            </td>
            <td colspan="3" style="border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($asesor)) echo strtoupper($asesor); else echo ""; ?>
                </label>
            </td>
            <td align="right">
                <label style="font-size: 10px;"><strong>CLIENTE: </strong></label>
            </td>
            <td colspan="3" style="border-bottom: 0.5px solid darkblue;">
                <label style="font-size: 10px;" class="formato">
                    <?php if(isset($cliente)) echo strtoupper($cliente); else echo ""; ?>
                </label>
            </td>
        </tr>
    </table>

    <br>

    <br>
    <h4 align="center" style="color:#340f7b;border-radius:4px;">
        <strong>COTIZACIÓN  <?php if(isset($tipoOrigen)) echo " - ".$tipoOrigen; else echo ""; ?></strong>
    </h4>
    
    <br>
    <table style="width:100%;border:1px solid #337ab7;border-radius: 4px;margin-top:-10px;">
        <tr>
            <td style="width:7%;border-bottom:1px solid #337ab7;" align="center"><strong>CANTIDAD</strong></td>
            <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>  D E S C R I P C I Ó N</strong></td>
            <td style="width:22%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>NUM. PIEZA</strong></td>
            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>COSTO</strong></td>
            <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>HORAS</strong></td>
            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>TOTAL</strong></td>
            <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"><strong>TOTAL REFACCIÓN</strong></td>
        </tr>
        <!-- Verificamos si existe la informacion -->
        <?php if (isset($id_refaccion)): ?>
            <!-- Comprobamos que haya registros guardados -->
            <?php if (count($id_refaccion)>0): ?>
                <?php for ($index  = 0; $index < count($id_refaccion); $index++): ?>
                    <tr> 
                        <td style="width:7%;border-bottom:1px solid #337ab7;height:20px;" align="center">
                            <label class="formato"><?php echo $cantidad[$index]; ?></label>
                        </td>
                        <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label class="formato"><?php echo $descripcion[$index]; ?></label>
                        </td>
                        <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label class="formato"><?php echo $num_pieza[$index]; ?></label>
                        </td>
                        <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label class="formato">$<?php echo number_format($costo_pieza[$index],2); ?></label>
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                            <label class="formato">$<?php echo number_format($horas_mo[$index],2); ?></label>
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center">
                              <label class="formato">$<?php echo number_format($costo_mo[$index],2); ?></label>
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align='center'>
                            <?php $total_renglon = ((float)$cantidad[$index] *(float)$costo_pieza[$index] ) + ((float)$horas_mo[$index] *(float)$costo_mo[$index] ); ?>
                            <label class="formato">$<?php echo number_format($total_renglon,2); ?></label>
                        </td>
                    </tr>                     
                <?php endfor; ?>
                <?php for ($i = 0; $i<(18-count($cantidad)) ; $i++): ?>
                    <tr>
                        <td style="width:7%;border-bottom:1px solid #337ab7;height:20px;" align="center"></td>
                        <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:22%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    </tr>
                <?php endfor; ?>
            <?php else: ?>
                <?php for ($i = 0; $i<18 ; $i++): ?>
                    <tr>
                        <td style="width:7%;border-bottom:1px solid #337ab7;height:20px;" align="center"></td>
                        <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:22%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                    </tr>
                <?php endfor; ?>
            <?php endif; ?>
        <?php else: ?>
              <?php for ($i = 0; $i<18 ; $i++): ?>
                  <tr>
                      <td style="width:7%;border-bottom:1px solid #337ab7;height:20px;" align="center"></td>
                      <td style="width:37%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                      <td style="width:22%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                      <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                      <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                      <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                      <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" align="center"></td>
                  </tr>
              <?php endfor; ?>
        <?php endif; ?>
        <tr>
            <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                SUB-TOTAL
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" colspan="2">
                <label class="formato">$ <?php if(isset($subtotal)) echo number_format($subtotal,2); else echo "0"; ?></label>
                
            </td>
        </tr>
        <tr>
            <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                CANCELADO
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" colspan="2">
                <label class="formato">$ <?php if(isset($cancelado)) echo $cancelado; else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                PRESUPUESTO PARCIAL
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" colspan="2">
                <label class="formato">$ <?php if(isset($cancelado)) echo number_format(($subtotal-$cancelado),2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                I.V.A.
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" colspan="2">
                <label class="formato">$ <?php if(isset($iva)) echo number_format($iva,2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                PRESUPUESTO TOTAL
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" colspan="2">
                <label class="formato">$ <?php if(isset($subtotal)) echo number_format((($subtotal-$cancelado)*1.16),2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr style="border-top:1px solid #337ab7;">
            <td colspan="4" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                <label  class="formato"><?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
            </td>
            <td align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                ANTICIPO
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" colspan="2">
                <label class="formato">$ <?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
            </td>
        </tr>
        <tr>
            <td colspan="5" align="right" style="border-bottom:1px solid #337ab7;height:20px;">
                TOTAL
            </td>
            <td align="center" style="border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;" colspan="2">
                <label class="formato">$ <?php if(isset($total)) echo number_format($total,2); else echo "0"; ?></label>
            </td>
        </tr>
        <!-- <tr>
            <td colspan="6">
                <br><br>
            </td>
        </tr> -->
        <tr>
            <td colspan="6" align="right" style="height:35px;">
                FIRMA DEL ASESOR QUE APRUEBA
            </td>
            <td align="center">
              <?php if (isset($firma_asesor)): ?>
                  <?php if ($firma_asesor != ""): ?>
                      <img class='marcoImg' src='<?php echo base_url().$firma_asesor ?>' id='' style='width:80px;height:30px;'>
                  <?php endif; ?>
              <?php endif; ?>
            </td>
        </tr>
    </table>
</page>

<?php 
    use Spipu\Html2Pdf\Html2Pdf;

    //Orientación  de la hoja P->Vertical   L->Horizontal
    $html2pdf = new Html2Pdf('P', 'A4', 'en',TRUE,'UTF-8',NULL);
    
    try {

        $html = ob_get_clean();

        ob_clean();
        /* Limpiamos la salida del búfer y lo desactivamos */
        //ob_end_clean();

        $html2pdf->setDefaultFont('Helvetica');     //Helvetica
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($html);
        $html2pdf->setTestTdInOnePage(FALSE);
        $html2pdf->Output();

    } catch (Html2PdfException $e) {
        $html2pdf->clean();
        $formatter = new ExceptionFormatter($e);
        echo $formatter->getHtmlMessage();
    }

?>