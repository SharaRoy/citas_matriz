<!-- vista para editar asesor y tecnico -->

<div style="margin:10px;">
    <div class="alert alert-success" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Proceso completado con éxito.</strong>
    </div>
    <div class="alert alert-danger" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Fallo el proceso.</strong>
    </div>
    <div class="alert alert-warning" align="center" style="display:none;">
        <strong style="font-size:20px !important;">Campos incompletos.</strong>
    </div>

    <div class="row">
        <div class="col-md-3">
            <label for="">No. Orden : </label>&nbsp;
            <input type="text" class="input_field" onblur="validarEstado(1)" name="idOrdenTemp" id="idOrdenTempCotiza" value="<?php if(set_value('idOrdenTemp') != "") echo set_value('idOrdenTemp'); elseif(isset($id_cita)) echo $id_cita; ?>" style="width:100%;">
            <?php echo form_error('idOrdenTemp', '<br><span class="error">', '</span>'); ?>
        </div>
        <div class="col-md-3">
            <label for="">Orden Intelisis : </label>&nbsp;
                <input type="text" class="input_field" name="folio_externo" id="folio_externo" value="<?php if(set_value('folio_externo') != "") echo set_value('folio_externo'); elseif(isset($folio_externo)) echo $folio_externo; ?>" style="width:100%;">
                <?php echo form_error('folio_externo', '<br><span class="error">', '</span>'); ?>
        </div>
        <div class="col-md-3">
            <label for="">No.Serie (VIN):</label>&nbsp;
            <input class="input_field" type="text" name="noSerie" id="noSerieText" value="<?php if(set_value('noSerie') != "") echo set_value('noSerie'); elseif(isset($serie)) echo $serie; ?>" style="width:100%;" disabled>
            <?php echo form_error('noSerie', '<br><span class="error">', '</span>'); ?>
        </div>
        <div class="col-sm-3">
            <label for="">Modelo:</label>&nbsp;
            <input class="input_field" type="text" name="modelo" id="txtmodelo" value="<?php echo set_value('modelo');?>" style="width:100%;">
            <?php echo form_error('modelo', '<br><span class="error">', '</span>'); ?>
        </div>
    </div>
    
    <br>
    <div class="row">
        <div class="col-md-3">
            <label for="">Placas</label>&nbsp;
            <input type="text" class="input_field" name="placas" id="placas" value="<?php echo set_value('placas');?>" style="width:100%;">
            <?php echo form_error('placas', '<br><span class="error">', '</span>'); ?>
        </div>
        <div class="col-md-3">
            <label for="">Unidad</label>&nbsp;
            <input class="input_field" type="text" name="uen" id="uen" value="<?php echo set_value('uen');?>" style="width:100%;">
            <?php echo form_error('uen', '<br><span class="error">', '</span>'); ?>
        </div>
        <div class="col-md-3">
            <label for="">Técnico</label>&nbsp;
            <input class="input_field" type="text" name="tecnico" id="tecnico" value="<?php echo set_value('tecnico');?>" style="width:100%;">
            <?php echo form_error('tecnico', '<br><span class="error">', '</span>'); ?>
        </div>
        <div class="col-md-3">
            <label for="">Asesor</label>&nbsp;
            <input class="input_field" type="text" name="asesors" id="asesors" value="<?php echo set_value('asesors');?>" style="width:100%;">
            <?php echo form_error('asesors', '<br><span class="error">', '</span>'); ?>
        </div>
    </div>

    <br>
    <div class="row table-responsive">
        <div class="col-sm-1" align="right"></div>
        <div class="col-sm-10" align="right">
            <?php if (isset($ubicacion_proceso)): ?>
                <div class="alert alert-info" align="center">
                    <strong style="font-size:20px !important;">
                        <?php echo $ubicacion_proceso; ?>
                    </strong>
                </div>
            <?php else: ?>
                <div class="alert alert-info" align="center">
                    <strong style="font-size:20px !important;">
                        No se encontro el presupuesto
                    </strong>
                </div>
            <?php endif ?>
        </div>
        <div class="col-sm-1" align="right"></div>
    </div>

    <div class="row">
        <div class="col-md-12 table-responsive" style="overflow-x: scroll;width: auto;">
            <table class="table-responsive" style="border:1px solid #337ab7;width:100%;border-radius: 4px;min-width: 833px;margin:10px;">
                <thead>
                    <tr style="background-color: #eee;">
                        <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            REP.
                        </td>
                        <td style="width:7%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            CANTIDAD
                        </td>
                        <td style="width:30%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            D E S C R I P C I Ó N
                        </td>
                        <td style="width:20%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            NUM. PIEZA
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 10px;" align="center" colspan="3">
                            EXISTE
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            PRECIO REFACCIÓN
                        </td>
                        <td style="width:6%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            HORAS
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            COSTO MO
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            TOTAL DE REPARACÓN
                        </td>
                        <td style=" display:none;width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            PRIORIDAD <br> REFACCION
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            PZA. GARANTÍA
                            <input type="hidden" name="indiceTablaMateria" id="indiceTablaMateria" value="1">
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            APRUEBA JEFE DE TALLER
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            APRUEBA ASESOR
                        </td>
                        <td style="width:8%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;vertical-align: middle;font-size: 10px;" align="center" rowspan="2">
                            REPARACIÓN ADICIONAL
                        </td>
                    </tr>
                    <tr style="background-color: #ddd;">
                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                            SI
                        </td>
                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                            NO
                        </td>
                        <td style="width:4%;border-bottom:1px solid #337ab7;border-left:1px solid #337ab7;font-size: 9px;" align="center">
                            PLANTA
                        </td>
                    </tr>
                </thead>
                <tbody id="cuerpoMateriales">
                    <!-- Verificamos si existe la informacion -->
                    <?php if (isset($renglon)): ?>
                        <!-- Comprobamos que haya registros guardados -->
                        <?php if (count($renglon)>0): ?>
                            <?php for ($index  = 0; $index < count($renglon); $index++): ?>
                                <!-- Comprobamos que el renglon a mostrar tenga la aprovacion del jefe de taller -->
                                <!-- <?php if ($autoriza_jefeTaller != "1") echo 'hidden'; ?> -->
                                <tr id='fila_<?php echo $index+1; ?>'>
                                    <!-- REP -->
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <label style="color:darkblue;"><?php echo $referencia[$index]; ?></label>
                                    </td>

                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' id='cantidad_<?php echo $index+1; ?>' value='<?php echo $cantidad[$index]; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' style='width:90%;'>

                                        <!-- Indice interno de la recaccion -->
                                        <input type="hidden" name="id_refaccion" id="id_<?php echo $index+1; ?>" value="<?php if(isset($id_refaccion[$index])) echo $id_refaccion[$index]; else echo ""; ?>">
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' id='descripcion_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $descripcion[$index]; ?></textarea>
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <textarea rows='1' class='input_field_lg' id='pieza_<?php echo $index+1; ?>' style='width:90%;text-align:left;'><?php echo $num_pieza[$index]; ?></textarea>
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaSi_<?php echo $index+1; ?>' class='input_field' name='existe_<?php echo $index+1; ?>' value="SI" <?php if($existe[$index] == "SI") echo "checked"; ?> style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaNo_<?php echo $index+1; ?>' class='input_field' name='existe_<?php echo $index+1; ?>' value="NO" <?php if($existe[$index] == "NO") echo "checked"; ?> style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <input type='radio' id='apuntaPlanta_<?php echo $index+1; ?>' class='input_field' name='existe_<?php echo $index+1; ?>' value="PLANTA" <?php if($existe[$index] == "PLANTA") echo "checked"; ?> style="transform: scale(1.5);">
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        $<input type='number' step='any' min='0' class='input_field' id='costoCM_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_pieza[$index]; ?>' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' id='horas_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $horas_mo[$index]; ?>' style='width:90%;text-align:left;'>
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        $<input type='number' step='any' min='0' id='totalReng_<?php echo $index+1; ?>' class='input_field' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $costo_mo[$index]; ?>' style='width:90%;text-align:left;'>

                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;' align='center'>
                                        <label id="total_renglon_label_<?php echo $index+1; ?>" style="color:darkblue;">$<?php echo number_format($total_refacion[$index],2); ?></label>
                                    </td>
                                    <td style='border:1px solid #337ab7;vertical-align: middle;display: none;' align='center'>
                                        <input type='number' step='any' min='0' class='input_field' id='prioridad_<?php echo $index+1; ?>' name='prioridad_<?php echo $index+1; ?>' onkeypress='return event.charCode >= 46 && event.charCode <= 57' value='<?php echo $prioridad[$index]; ?>' style='width:70%;text-align:center;'>
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <!--<input type='checkbox' class='input_field' name="pzaGarantia[]" value="<?php echo $index+1; ?>" id='autorizo_<?php echo $index+1; ?>' <?php if($pza_garantia[$index] == "1") echo "checked"; ?> style="transform: scale(1.5);">-->
                                        <?php if ($pza_garantia[$index] == "1"): ?>
                                            SI
                                        <?php else: ?>
                                            NO
                                        <?php endif ?>

                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <?php if ($autoriza_jefeTaller[$index] == "1"): ?>
                                            SI
                                        <?php else: ?>
                                            NO
                                        <?php endif ?>
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <!--<input type='checkbox' class='input_field autorizaCheck' name="autorizo_asesor[]" value="<?php echo $index+1; ?>" id='autorizo_<?php echo $index+1; ?>' <?php if($autoriza_asesor[$index] == "1") echo "checked"; ?> style="transform: scale(1.5);">-->
                                        <?php if ($autoriza_asesor[$index] == "1"): ?>
                                            SI
                                        <?php else: ?>
                                            NO
                                        <?php endif ?>

                                        <input type='hidden' id='valoresCM_<?php echo $index+1; ?>' name='registros[]' value="<?php echo $renglon[$index]; ?>">
                                    </td>
                                    <td style='border:1px solid #337ab7;' align='center'>
                                        <?php if ($rep_adicional[$index] == "1"): ?>
                                            SI
                                        <?php else: ?>
                                            NO
                                        <?php endif ?>
                                    </td>
                                </tr>
                            <?php endfor; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="15" align="center">
                                    No se cargaron los datos
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="15" align="center">
                                No se cargaron los datos
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" align="right">
                            <label>SUB-TOTAL</label>
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="subTotalMaterialLabel"><?php if(isset($subtotal)) echo number_format($subtotal,2); else echo "0"; ?></label>
                            <input type="hidden" name="subTotalMaterial" id="subTotalMaterial" value="<?php if(isset($subtotal)) echo $subtotal; else echo "0"; ?>">
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="10" align="right" class="error">
                            <label>CANCELADO</label>
                        </td>
                        <td colspan="1" align="center" class="error">
                            $<label id="canceladoMaterialLabel"><?php if(isset($cancelado)) echo number_format($cancelado,2); else echo "0"; ?></label>
                            <input type="hidden" name="canceladoMaterial" id="canceladoMaterial" value="<?php if(isset($cancelado)) echo $cancelado; else echo "0"; ?>">
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="10" align="right">
                            <label>I.V.A.</label>
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="ivaMaterialLabel"><?php if(isset($iva)) echo number_format($iva,2); else echo "0"; ?></label>
                            <input type="hidden" name="ivaMaterial" id="ivaMaterial" value="<?php if(isset($iva)) echo $iva; else echo "0"; ?>">
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="10" align="right">
                            <label>PRESUPUESTO TOTAL</label>
                        </td>
                        <td colspan="1" align="center">
                            $<label id="presupuestoMaterialLabel"><?php if(isset($subtotal)) echo number_format((($subtotal-$cancelado)*1.16),2); else echo "0"; ?></label>
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr style="border-top:1px solid #337ab7;">
                        <td colspan="8" rowspan="2">
                            <label for="" id="anticipoNota" style="color:blue;">Nota anticipo: <?php if(isset($notaAnticipo)) echo $notaAnticipo; else echo ""; ?></label>
                        </td>
                        <td colspan="2" align="right">
                            <label>ANTICIPO</label>
                        </td>
                        <td align="center" colspan="2">
                            $<label for="" id="anticipoMaterialLabel"><?php if(isset($anticipo)) echo number_format($anticipo,2); else echo "0"; ?></label>
                            <input type="hidden" name="anticipoMaterial" id="anticipoMaterial" value="<?php if(isset($anticipo)) echo $anticipo; else echo "0"; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <label>TOTAL</label>
                        </td>
                        <td colspan="1" align="center">
                            $<label for="" id="totalMaterialLabel"><?php if(isset($total)) echo number_format($total,2); else echo "0"; ?></label>
                            <input type="hidden" name="totalMaterial" id="totalMaterial" value="<?php if(isset($total)) echo $total; else echo "0"; ?>">
                        </td>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="10" align="right">
                            FIRMA DEL ASESOR QUE APRUEBA
                        </td>
                        <td align="center" colspan="4">
                            <img class='marcoImg' src='<?php echo base_url(); ?><?php if(isset($firma_asesor)) { echo (($firma_asesor != "") ?  $firma_asesor : "assets/imgs/fondo_bco.jpeg"); } else { echo "assets/imgs/fondo_bco.jpeg"; } ?>' id='' style='width:80px;height:30px;'>
                            <input type='hidden' id='rutaFirmaAsesorCostos' name='rutaFirmaAsesorCostos' value='<?php if(isset($firma_asesor)) echo $firma_asesor ?>'>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="col-sm-12" hidden>
                <div class="alert alert-success" align="left">
                    <strong style="font-size:12px !important;font-weight: bold;">
                        Prioridad de refacciones:<br>
                        0 = Sin prioridad<br>
                        1 = Prioridad Baja <br>
                        2 = Prioridad Media <br>
                        3 = Prioridad Alta
                    </strong>
                </div>
            </div>
    </div>

    <br>
    <!-- Comentarios del asesor -->
    <div class="row">
        <div class="col-sm-5">
            <h4>Notas del asesor:</h4>
            <!-- Verificamos si es una vista del asesor -->
            <label for="" style="font-size:12px;"><?php if(isset($nota_asesor)){ if($nota_asesor != "") echo $nota_asesor; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
        </div>
        <div class="col-sm-7" align="right">
            <h4>Archivo(s) de la cotización:</h4>
            <br>

            <?php if (isset($archivos_presupuesto)): ?>
                <?php $contador = 1; ?>
                <?php for ($i = 0; $i < count($archivos_presupuesto); $i++): ?>
                    <?php if ($archivos_presupuesto[$i] != ""): ?>
                        <a href="<?php echo base_url().$archivos_presupuesto[$i]; ?>" class="btn btn-info" style="font-size: 9px;font-weight: bold;" target="_blank">
                            DOC. (<?php echo $contador; ?>)
                        </a>

                        <?php if ((($i+1)% 4) == 0): ?>
                            <br>
                        <?php endif; ?>
                        
                        <?php $contador++; ?>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if (count($archivos_presupuesto) == 0): ?>
                    <h4 style="text-align: right;">Sin archivos</h4>
                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                <?php endif; ?>
            <?php endif; ?>

            <hr>
            <!-- Para agregar mas refacciones -->
            <input type="file" multiple name="uploadfiles[]">
            <input type="hidden" name="uploadfiles_resp" value="<?php if(isset($archivos_presupuesto_lineal)) echo $archivos_presupuesto_lineal;?>">
            <br>
        </div>
    </div>

    <hr>
    <!-- Comentarios del técnico -->
    <div class="row">
        <div class="col-md-6">
            <h4>Comentario del técnico para Refacciones:</h4>
            <label for="" style="font-size:12px;"><?php if(isset($comentario_tecnico)){ if($comentario_tecnico != "") echo $comentario_tecnico; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
        </div>

        <div class="col-sm-6" align="right">
            <h4>Archivo(s) del técnico para Refacciones:</h4>

            <?php if (isset($archivo_tecnico)): ?>
                <?php $contador = 1; ?>
                <?php for ($i = 0; $i < count($archivo_tecnico); $i++): ?>
                    <?php if ($archivo_tecnico[$i] != ""): ?>
                        <a href="<?php echo base_url().$archivo_tecnico[$i]; ?>" class="btn" style="background-color: #5cb8a5;border-color: #4c8bae;color:white;font-size: 9px;font-weight: bold;" target="_blank">
                            DOC. (<?php echo $contador; ?>)
                        </a>

                        <?php if (($contador % 4) == 0): ?>
                            <br>
                        <?php endif; ?>

                        <?php $contador++; ?>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if (count($archivo_tecnico) == 0): ?>
                    <h4 style="text-align: right;">Sin archivos</h4>
                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <hr>
    <!-- comentarios de ventanilla -->
    <div class="row">
        <div class="col-sm-6" align="left">
            <h4>Comentarios de Ventanilla:</h4>
            <textarea id="comentario_ventanilla" name="comentario_ventanilla" rows="2" style="width:100%;border-radius:4px;" placeholder="Notas de ventanilla..."><?php if(isset($comentario_ventanilla)) echo $comentario_ventanilla;?></textarea>
        </div>
        <div class="col-sm-6" align="right"></div>
    </div>

    <hr>
    <!-- comentarios del jefe de taller -->
    <div class="row">
        <div class="col-sm-6" align="left">
            <h4>Comentarios del Jefe de Taller:</h4>
            <label for="" style="font-size:12px;"><?php if(isset($comentario_jdt)){ if($comentario_jdt != "") echo $comentario_jdt; else echo "Sin comentarios";}else echo "Sin comentarios"; ?></label>
        </div>
        <div class="col-sm-6" align="right">
            <h4>Archivo(s) del Jefe de Taller:</h4>

            <?php if (isset($archivo_jdt)): ?>
                <?php $contador = 1; ?>
                <?php for ($i = 0; $i < count($archivo_jdt); $i++): ?>
                    <?php if ($archivo_jdt[$i] != ""): ?>
                        <a href="<?php echo base_url().$archivo_jdt[$i]; ?>" class="btn btn-primary" style="font-size: 9px;font-weight: bold;" target="_blank">
                            DOC. (<?php echo $contador; ?>)
                        </a>
                        
                        <?php if (($contador % 4) == 0): ?>
                            <br>
                        <?php endif; ?>
                        
                        <?php $contador++; ?>
                    <?php endif; ?>
                <?php endfor; ?>

                <?php if (count($archivo_jdt) == 0): ?>
                    <h4 style="text-align: right;">Sin archivos</h4>
                    <!--<a href="" class="btn btn-primary" target="_blank">Sin archivos</a>-->
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
    
    <br>
    <div class="col-sm-12" align="center">
        <input type="hidden" name="dirige" id="dirige" value="<?php if(isset($dirige)) echo $dirige; ?>"> 
        <input type="hidden" name="modoVistaFormulario" id="modoVistaFormulario" value="<?php if(isset($vista)) echo $vista; else echo "2A"; ?>">
        <input type="hidden" name="UnidadEntrega" id="UnidadEntrega" value="<?php if(isset($UnidadEntrega)) echo $UnidadEntrega; else echo "0";?>">

        <input type="hidden" name="tipoRegistro" id="tipoRegistro" value="<?php if(isset($tipoRegistro)) echo $tipoRegistro; ?>">
    </div>
</div>
