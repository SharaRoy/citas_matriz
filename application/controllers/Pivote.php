<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pivote extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->database();
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

  	public function index($destino = "ADMIN",$resp = NULL)
  	{
        //Verificamos si hay alguna sesion iniciada
        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') != "SOPORTE") {
                $this->session->sess_destroy();
            }
            
            //Eliminamos la cookie
            delete_cookie('panel_origen');
        }

        $datos["mensaje"] = $resp;
        //Cargamos la vista principal dependiendo la ruta
        if ($destino == "PT") {
            //Pantalla principal para tecnico (solo la multipunto)
            $datos["loginOrigen"] = "TEC";
            $datos["panel"] = " TÉCNICOS";
            $this->loadAllView($datos,'verificar','TÉCNICO');
        } elseif ($destino == "PA") {
            //Pantalla principal para asesor (todos los formularios de alta)
            $datos["loginOrigen"] = "ASE";
            $datos["panel"] = " ASESORES";
            $this->loadAllView($datos,'verificar','ASESOR');
        } elseif ($destino == "JT") {
            //Pantalla principal para jefe de taller (solo los formularios de multipunto (llenar) y voz cliente (alta))
            $datos["loginOrigen"] = "JDT";
            $datos["panel"] = " JEFE DE TALLER";
            $this->loadAllView($datos,'verificar','JEFE DE TALLER');
        }elseif ($destino == "OT") {
            //Pantalla principal para otros usuarios (solo editar cotizacion de la multipunto) 
            $datos["loginOrigen"] = "OT";
            $datos["panel"] = " VENTANILLA";
            $this->loadAllView($datos,'verificar','Ventanilla');
        }elseif ($destino == "PCO") {
            //Pantalla principal para otros usuarios (solo editar cotizacion de la multipunto)  
            $datos["loginOrigen"] = "PCO";
            $datos["panel"] = " CIERRE DE ORDEN";
            $this->loadAllView($datos,'verificar','CIERRE DE ORDEN');
        } elseif ($destino == "PAT") {
            //Pantalla principal para otros usuarios (solo editar cotizacion de la multipunto)  
            $datos["loginOrigen"] = "ASETEC";
            $datos["panel"] = " ASESOR TÉCNICO";
            $this->loadAllView($datos,'verificar','ASESOR TÉCNICO');
        } else {
            redirect('Principal_Asesor/4', 'refresh');
        }
  	}

    public function panel($destino = "ADMIN",$resp = NULL)
  	{
        $datos["mensaje"] = $resp;
        // echo '<pre>';
        // print_r($this->session->userdata());

        //Validamos si existe la sesion activa
        if ($this->session->userdata('rolIniciado')) {
            $datos["sesionActiva"] = "1";
        }else {
            $datos["sesionActiva"] = "0";
        }

        //Cargamos la vista principal dependiendo la ruta
        if ($destino == "TEC") {
            //Pantalla principal para tecnico (solo la multipunto)
            $this->loadAllView($datos,'principales/tecnico/principal_tec','TÉCNICO');
        } elseif ($destino == "ASE") {
            //Pantalla principal para asesor (todos los formularios de edicion)
            $this->loadAllView($datos,'principales/asesor/principal_ases','ASESOR');
        } elseif ($destino == "JDT") {
            //Pantalla principal para asesor (multipunto y voz cliente)
            $this->loadAllView($datos,'principales/taller/principal_taller','JEFE DE TALLER');
        }elseif ($destino == "OT") {
            //Limpiamos las variables de sesion
            if ($this->session->userdata('rolIniciado')) {
                //$this->session->sess_destroy();
                if ($this->session->userdata('rolIniciado') == 'OT') {
                    $this->loadAllView($datos,'principales/otros/principal_otro','PANEL');
                } else {
                    redirect('Principal_Otro/4', 'refresh');
                }
                
            }else{
                redirect('Principal_Otro/4', 'refresh');
            }
        } elseif ($destino == "ASETEC") {
            //Pantalla principal para asesor (orden y voz cliente)
            $this->loadAllView($datos,'principales/asesor/principal_asestec','ASESOR TÉCNICO');
            
        } else {
            //Pantalla principal para pruebas
            $this->loadAllView($datos,'principales/principal_2','Pruebas');
        }
  	}

    public function loadAllView($datos = NULL,$vista = "",$titulo = "")
    {
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", '', TRUE),
            "titulo" => $titulo
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
