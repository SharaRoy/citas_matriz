<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formularios_1 extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($x = 0,$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");
        $this->loadAllView($datos,'otrosFormularios/Queretaro/formulario_1_alta');
    }

    //Alta formulario limpio
    public function limpiar($x = 0,$datos = NULL)
    {
        $datos["mensaje"] = "1";
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");
        $this->loadAllView($datos,'otrosFormularios/Queretaro/formulario_1_alta');
    }

    //Recuperamos formulario y validamos
    public function validateForm()
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");

        $datos["tipoVista"] = $this->input->post("tipoVista");
        $datos["idOrden"] = $this->input->post("idOrden");

        //Validamos campos
        if ($datos["tipoVista"] == "1") {
            $this->form_validation->set_rules('idOrden', '', 'required|callback_existencia');
        }else {
            $this->form_validation->set_rules('idOrden', '', 'required');
        }

        $this->form_validation->set_rules('txtHoraCita', '', 'required');
        $this->form_validation->set_rules('txtCliente', '', 'required');
        $this->form_validation->set_rules('txtApe1', '', 'required');
        $this->form_validation->set_rules('txtPlacas', '', 'required');
        $this->form_validation->set_rules('txtAnio', '', 'required');
        $this->form_validation->set_rules('txtUen', '', 'required');
        $this->form_validation->set_rules('txtAsesor', '', 'required');
        $this->form_validation->set_rules('txtCorreo', 'Correo electrónico', 'valid_email');

        $this->form_validation->set_message('required','Campo requerido');
        $this->form_validation->set_message('valid_email','Proporcione un correo electrónico valido');
        $this->form_validation->set_message('existencia','Este No. de Orden ya esta registrado. Favor de verificar.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";
            //Identificamos a que vista va a retornar el error
            if ($datos["tipoVista"] == "1") {
                $this->loadAllView($datos,'otrosFormularios/Queretaro/formulario_1_alta');
            } else {
                $this->setData($datos["idOrden"],$datos);
            }


        }
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idOrden");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"formulario_quere1");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }


        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }


    //Recuperamos los datos y los almacenamos
    public function getDataForm($datos = NULL)
    {
        $registro = date("Y-m-d H:i");

        if ($datos["tipoVista"] == "1") {
            //Creamos el array para guardar el registro
            $dataPrueba = array(
                "idRegistro" => NULL,
                "indiceConsecutivo" => $this->indice(),
                "idOrden" => $datos["idOrden"],
            );
        }

        $dataPrueba["fechaCita"] = $this->input->post("txtFecha");
        $dataPrueba["horaCita"] = $this->input->post("txtHoraCita");
        $dataPrueba["cliente"] = $this->input->post("txtCliente");
        $dataPrueba["ape_pa"] = $this->input->post("txtApe1");
        $dataPrueba["ape_ma"] = $this->input->post("txtApe2");
        $dataPrueba["telefono"] = $this->input->post("txtTelefono");
        $dataPrueba["correo"] = $this->input->post("txtCorreo");
        $dataPrueba["placa"] = $this->input->post("txtPlacas");
        $dataPrueba["anioUnidad"] = $this->input->post("txtAnio");
        $dataPrueba["unidad"] = $this->input->post("txtUen");
        $dataPrueba["servicio"] = $this->input->post("txtServicio");
        $dataPrueba["tipoServicio"] = $this->input->post("txtTipoServicio");
        $dataPrueba["taxi"] = $this->validateRadioCheck($this->input->post("txtTaxi"),"NO");
        $dataPrueba["asesor"] = $this->input->post("txtAsesor");
        $dataPrueba["confirma"] = $this->validateRadioCheck($this->input->post("txtConfirma"),"NO");

        if ($datos["tipoVista"] == "1") {
            $dataPrueba["fecha_registro"] = $registro;
        }

        $dataPrueba["fecha_actualiza"] = $registro;


        //Verificamos que tipo de formulario se va a guardar
        if ($datos["tipoVista"] == "1") {
            $consulta = $this->mConsultas->save_register('formulario_quere1',$dataPrueba);
            if ($consulta != 0) {
                $datos["mensaje"] = "1";
                // $this->index($dataPrueba["idOrden"],$datos);
                redirect('Formulario_Qro_L/_', 'refresh');
            }else {
                $datos["mensaje"] = "2";
                $this->index("0",$datos);
            }
        }else {
            $actualizar = $this->mConsultas->update_table_row('formulario_quere1',$dataPrueba,'idOrden',$datos["idOrden"]);
            if ($actualizar) {
                //Guardamos el movimiento

                $datos["mensaje"]="1";
                // echo "Actualizado";
                redirect('Formulario_Qro_T/1', 'refresh');
            }else {
                $datos["mensaje"]="2";
                $this->setData($datos["idOrden"],$datos);
            }
        }
    }

    //Validar valor del checkbox
    public function validateRadioCheck($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Cargamos la informacion del registro
    public function setData($idOrden = 0,$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"formulario_quere1");

        foreach ($query as $row) {
            $datos["txtCita"] = $row->indiceConsecutivo ;
            $datos["txtidOrden"] = $row->idOrden ;
            $datos["txtFecha_Cita"] = $row->fechaCita ;
            $datos["txtHora_Cita"] = $row->horaCita ;
            $datos["txtNombre"] = $row->cliente ;
            $datos["txtApellido_pa"] = $row->ape_pa ;
            $datos["txtApellido_ma"] = $row->ape_ma ;
            $datos["txtTelefono"] = $row->telefono ;
            $datos["txtCorreo"] = $row->correo ;
            $datos["txtPlaca"] = $row->placa ;
            $datos["txtAnioUnidad"] = $row->anioUnidad ;
            $datos["txtUnidad"] = $row->unidad ;
            $datos["txtServicio"] = $row->servicio ;
            $datos["txtTipo_Servicio"] = $row->tipoServicio ;
            $datos["txtAsesor"] = $row->asesor ;
            $datos["txtReq_Taxi"] = $row->taxi ;
            $datos["txtConfirma_Cita"] = $row->confirma ;
        }

        $this->loadAllView($datos,'otrosFormularios/Queretaro/formulario_1_editar');
    }

    //Listamos todos los registro
    public function listData($x = '',$datos = NULL)
    {
        $query = $this->mConsultas->get_table('formulario_quere1');
        foreach ($query as $row) {
            $datos["txtidOrden"][] = $row->idOrden ;
            $datos["txtFecha_Cita"][] = $row->fechaCita ;
            $datos["txtNombre"][] = $row->cliente." ".$row->ape_pa." ".$row->ape_ma;
            $datos["txtTelefono"][] = $row->telefono ;
            // $datos["txtCorreo"][] = $row->correo ;
            $datos["txtPlaca"][] = $row->placa ;
            $datos["txtAsesor"][] = $row->asesor ;
            $datos["txtReq_Taxi"][] = $row->taxi ;
            $datos["txtConfirma_Cita"][] = $row->confirma ;
        }

        $this->loadAllView($datos,'otrosFormularios/Queretaro/formulario_1_lista');
    }

    //recuperamos el ultimo id y lo creamos consecutivo
    public function indice()
    {
        $base = 23185;
        $registro = $this->mConsultas->last_registre();
        $row = $registro->row();
        if (isset($row)){
          $indexFolio = $registro->row(0)->idRegistro;
        }else {
          $indexFolio = 0;
        }

        $nvoFolio = $indexFolio + $base;

        return $nvoFolio;
    }

    //Carga de datos mediante de ajax
    public function loadData()
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);
        // ini_set('max_input_time',"20M");

        //Recuperamos el id de la orden
        $idOrden = $_POST['idOrden'];
        $cadena = "";

        $precarga = $this->mConsultas->precargaConsulta($idOrden);
        foreach ($precarga as $row){
            $cadena .= $row->fecha_entrega."|";
            $cadena .= $row->hora_entrega."|";
            $cadena .= $row->datos_nombres."|";
            $cadena .= $row->datos_apellido_paterno."|";
            $cadena .= $row->datos_apellido_materno."|";
            $cadena .= $row->datos_telefono."|";
            $cadena .= $row->datos_email."|";
            $cadena .= $row->vehiculo_placas."|";
            $cadena .= $row->vehiculo_anio."|";
            $cadena .= $row->vehiculo_modelo."|";

            if ($row->id_uen != "") {
                $query_temp_2 = $this->mConsultas->get_result("id",$row->id_uen,"cat_uen");
                foreach ($query_temp_2 as $row_2){
                    $dato_2 = $row_2->uen;
                }
                //Comprobamos que se haya hecho la consulta
                if (isset($dato_2)) {
                    $cadena .= $dato_2."|";
                }else {
                    $cadena .= "|";
                }
            }else {
                $cadena .= "|";
            }

            $cadena .= $row->asesor."|";

            if ($row->id_tipo_operacion != "") {
                $query_temp_3 = $this->mConsultas->get_result("id",$row->id_tipo_operacion,"cat_tipo_operacion");
                foreach ($query_temp_3 as $row_3){
                    $dato_3 = $row_3->tipo_operacion;
                }
                //Comprobamos que se haya hecho la consulta
                if (isset($dato_3)) {
                    $cadena .= $dato_3."|";
                }else {
                    $cadena .= "|";
                }
            }else {
                $cadena .= "|";
            }


            if ($row->id_concepto != "") {
                $query_temp_4 = $this->mConsultas->get_result("id",$row->id_concepto,"cat_concepto");
                foreach ($query_temp_4 as $row_4){
                    $dato_4 = $row_4->concepto;
                }
                //Comprobamos que se haya hecho la consulta
                if (isset($dato_4)) {
                    $cadena .= $dato_4."|";
                }else {
                    $cadena .= "|";
                }
            }else {
                $cadena .= "|";
            }


        }

        echo $cadena;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                // 'assets/js/firmaCliente.js',
                "assets/js/otrosFormularios/form_1_qro.js",
                "assets/js/otrosFormularios/formulario.js",
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Formulario"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }



}
