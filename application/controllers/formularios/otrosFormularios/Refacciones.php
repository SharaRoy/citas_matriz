<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Refacciones extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        //Cargamos la libreria pagination
        $this->load->library('pagination');

    }

    public function index($dato = 0,$datos = NULL)
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        $jsonData["Login"] = array(
            "usuario" => "user",
            "password" => "1234567"
        );

        $jsonData["Art"] = "";
        $jsonData["Descripcion"] = ""; 
        $jsonData["Sucursal"] = "1";
        $jsonData["Empresa"] = "G";

        //Precargamos algunos datos
        $url_2 = 'plafordtepvtadb.hobsca.net/api_plasencia/api/articulo'; 
        //Creamos el objeto json para enviar por url
        $jsonDataEncoded = json_encode($jsonData);
        //Inicializamos el objeto CUrl
        $ch = curl_init($url_2);
        //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Adjuntamos el json a nuestra petición
        curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
        //Agregamos los encabezados del contenido
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        //Obtenemos la respuesta
        $response = curl_exec($ch);
        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);
        //Enviamos la respuesta para limpiar la respuesta del servidor
        $respuesta = $this->clearString($response,"Ref");
        $renglones = explode("|",$respuesta);
        $regitro = ( count($renglones) > 1) ? count($renglones) -1 : count($renglones);
        // var_dump($response);
        for ($i=0; $i < $regitro ; $i++) {
            $campos = explode("_",$renglones[$i]);
            if ($campos[2] == "Servicio") {
                $tipo = "Mano de Obra";
            }elseif ($campos[2] == "Normal") {
                $tipo = "Refacción";
            }else {
                $tipo = $campos[2];
            }

            $datos["tipo"][] = $tipo;
            $datos["articulo"][] = $campos[0];
            $datos["descripcion"][] = $campos[1];
            $datos["precio"][] = $campos[3];
            $datos["existencia"][] = $campos[4];
        }

        //Tratamos como json la respuesta
        $result = json_decode($response,true);

        if (isset($result["Articulo"])) {
            foreach ($result["Articulo"] as $index => $valor) {
                if ($result["Tipo"][$index] == "Servicio") {
                    $tipo = "Mano de Obra";
                }elseif ($result["Tipo"][$index] == "Normal") {
                    $tipo = "Refacción";
                }else {
                    $tipo = $result["Tipo"][$index];
                }

                $datos["tipo"][] = $tipo;
                $datos["articulo"][] = $result["Articulo"][$index];
                $datos["descripcion"][] = $result["DescripcionExtra"][$index];
                $datos["precio"][] = $result["Articulo"][$index];
                $datos["existencia"][] = $result["Articulo"][$index];
            }

            $jsonDatos = json_encode($datos);
            echo $jsonDatos;
        }

        $this->loadAllView($datos,'otrosFormularios/otrasVistas/refacciones');

    }

    //Recibimos una busqueda por ajax para un campo
    public function filtrosDescripcion()
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);

        $respuesta = "";
        $valorFiltro = $_POST['valor_1'];
        $tipoBusqueda = $_POST['dato_1'];

        $jsonData["Login"] = array(
            "usuario" => "user",
            "password" => "1234567"
        );

        if ($tipoBusqueda == "clave") {
            $jsonData["Art"] = $valorFiltro;
            $jsonData["Descripcion"] = "";
        } else {
            $jsonData["Art"] = "";
            $jsonData["Descripcion"] = $valorFiltro;
        }
        
        $jsonData["Sucursal"] = "1";
        $jsonData["Empresa"] = "G";

        //Creamos el objeto json para enviar por url
        $jsonDataEncoded = json_encode($jsonData);

        $url_1 = 'plafordtepvtadb.hobsca.net/api_plasencia/api/mo';
        $url_2 = 'plafordtepvtadb.hobsca.net/api_plasencia/api/articulo';

        //Buscamos las refacciones
        //Inicializamos el objeto CUrl
        $ch = curl_init($url_2);
        //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Adjuntamos el json a nuestra petición
        curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
        //Agregamos los encabezados del contenido
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        //Obtenemos la respuesta
        $response = curl_exec($ch);
        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);
        //Enviamos la respuesta para limpiar la respuesta del servidor
        $respuesta .= $this->clearString($response,"Ref");
        // echo $response;

        //Buscamos las mano de obra
        //Inicializamos el objeto CUrl
        $ch = curl_init($url_1);
        //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Adjuntamos el json a nuestra petición
        curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
        //Agregamos los encabezados del contenido
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        //Obtenemos la respuesta
        $response = curl_exec($ch);
        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);
        //Enviamos la respuesta para limpiar la respuesta del servidor
        $respuesta .= $this->clearString($response,"MO");
        echo $respuesta;
    }

    //Limpiar respuesta del servidor y retornar
    public function clearString($result = "",$ubicacion = "")
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);

        $temporal = "";
        $signo = array(",","}","{",":","[","]");
        $puntas = array('[{"','"}]');
        $respuesta = str_replace($puntas, "",$result);
        $respuesta = str_replace($signo, "",$result);
        $respuesta = str_replace('""', '"',$respuesta);
        $respuesta = str_replace('NULL', "",$respuesta);
        $cadenas = explode('"',$respuesta);

        //Restructuramos la cadena
        if ((count($cadenas) % 2) == 0) {
            $renglones = count($cadenas);
        }else {
            $renglones = count($cadenas) - 1;
        }
        for ($i=2; $i <$renglones ; $i+=2) {
            if ($ubicacion == "MO") {
                if (($i % 8) == 0) {
                    $temporal .= $cadenas[$i]."_ _".$ubicacion."|";
                }else{
                    $temporal .= $cadenas[$i]."_";
                }
            } else {
                if (($i % 10) == 0) {
                    $temporal .= $cadenas[$i]."_".$ubicacion."|";
                }else{
                    $temporal .= $cadenas[$i]."_";
                }
            }
        }
        // echo $respuesta;
        return $temporal;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrosFormularios/buscadorRefacciones.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Refacciones"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
