<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Concentrado_VC extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'consulta', TRUE);
        //$this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        $this->load->library('MP3File');
    }

    public function index($dato = 0,$datos = NULL)
    {
        $datos["listado"] = $this->consulta->recuperar_audios_vc();
        $datos["base"] = "https://www.planificadorempresarial.mx/cs/citas_ford/horarios/citas/citas_matriz/";

        $this->loadAllView($datos,"otrosFormularios/otrasVistas/listado_info_vc");
    }

    public function index_2()
    {
        echo "Total VC: ".($this->consulta->get_total('voz_cliente') + $this->consulta->get_total('vc_registro'));
        echo "<br>[2019] Total VC con audio: ".($this->consulta->recuperar_totales_vc_caudio_old(2019) + $this->consulta->recuperar_totales_vc_caudio_new(2019));
        echo "<br>[2019] otal VC sin audio: ".($this->consulta->recuperar_totales_vc_saudio_old(2019) + $this->consulta->recuperar_totales_vc_saudio_new(2019));

        echo "<br>----------------------------<br>";

        echo "<br>[2020] Total VC con audio: ".($this->consulta->recuperar_totales_vc_caudio_old(2020) + $this->consulta->recuperar_totales_vc_caudio_new(2020));
        echo "<br>[2020] otal VC sin audio: ".($this->consulta->recuperar_totales_vc_saudio_old(2020) + $this->consulta->recuperar_totales_vc_saudio_new(2020));

        echo "<br>----------------------------<br>";

        echo "<br>[2021] Total VC con audio: ".($this->consulta->recuperar_totales_vc_caudio_old(2021) + $this->consulta->recuperar_totales_vc_caudio_new(2021));
        echo "<br>[2021] otal VC sin audio: ".($this->consulta->recuperar_totales_vc_saudio_old(2021) + $this->consulta->recuperar_totales_vc_saudio_new(2021));

        echo "<br>----------------------------<br>";
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                //'assets/js/otrosFormularios/buscadorRefacciones.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Audio VC"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
