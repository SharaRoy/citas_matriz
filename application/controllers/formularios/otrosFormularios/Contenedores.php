<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Contenedores extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',3000);
    }

    public function index($x = 0,$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");
    }


    //Campañas
    public function campanias($x = "")
    {
        $this->load->view("otrosFormularios/otrasVistas/campania");
        // $this->loadAllView(NULL,"otrosFormularios/otrasVistas/campania","Ford Mazatlan");
    }

    //Recibimos el array de id de Busqueda
    public function idsBusquedaMagic($busqueda = "",$datos = NULL)
    {
        // try {
            $ids = explode("_",$busqueda);
            //Campo por fila
            $filtros1 = $ids[0];
            //Fechas
            $filtros2 = $ids[1];
            $filtros3 = $ids[2];

            //Filtro por campo general
            if ($filtros1 != "") {
                //Busqueda de campos y fechas
                if ($filtros2 != "") { 
                    if ($filtros3 != "") {
                        $consultaMagic = $this->mConsultas->busquedaMagicTodo($filtros1,$filtros2,$filtros3);
                        $consultaOrdenes = $this->mConsultas->busquedaMagic_CitasTodo($filtros1,$filtros2." 00:00:00",$filtros3." 23:00:00");
                    }else{
                        $consultaMagic = $this->mConsultas->busquedaMagicTodo($filtros1,$filtros2,$filtros2);
                        $consultaOrdenes = $this->mConsultas->busquedaMagic_CitasTodo($filtros1,$filtros2." 00:00:00",$filtros2." 23:00:00");
                    }

                    $consultaMagic = $this->mConsultas->busquedaMagicTodo($filtros1,$filtros2,$filtros3);
                    $consultaOrdenes = $this->mConsultas->busquedaMagic_CitasTodo($filtros1,$filtros2." 00:00:00",$filtros3." 23:00:00");
                //Busqueda solo por campos
                }else {
                    $consultaMagic = $this->mConsultas->busquedaMagicCampo($filtros1);
                    $consultaOrdenes = $this->mConsultas->busquedaMagic_CitasCampo($filtros1);
                }
            //Filtro por fechas
            }else {
                if ($filtros3 == "") {
                    $consultaMagic = $this->mConsultas->busquedaMagicFecha($filtros2,$filtros2);
                    $consultaOrdenes = $this->mConsultas->busquedaMagic_CitasFecha($filtros2." 00:00:00",$filtros2." 23:00:00");
                }else{
                    $consultaMagic = $this->mConsultas->busquedaMagicFecha($filtros2,$filtros3);
                    $consultaOrdenes = $this->mConsultas->busquedaMagic_CitasFecha($filtros2." 00:00:00",$filtros3." 23:00:00");
                }
                
            }

            $idBusqueda = [];
            $idBusquedaOrden = [];

            //Recuperamos los id de la consulta con las ordenes
            if (isset($consultaOrdenes)) {
                foreach ($consultaOrdenes as $row) {
                    $idBusquedaOrden[] = $row->id_cita;
                }
            }

            $limitante = count($idBusquedaOrden);
            for ($i=0; $i < count($idBusquedaOrden) ; $i++) {
              //Recuperamos la informacion del cliente
                $perfil = $this->mConsultas->api_orden($idBusquedaOrden[$i]);
                foreach ($perfil as $row) {
                    $datos["nombre"][] = strtoupper($row->datos_nombres);
                    $datos["ap"][] = strtoupper($row->datos_apellido_paterno);
                    $datos["am"][] = strtoupper($row->datos_apellido_materno);

                    $datos["direccion_1"][] = strtoupper($row->calle." ".$row->noexterior." ".$row->nointerior.", ");
                    $datos["direccion_2"][] = strtoupper($row->colonia.", Cp.".$row->cp);
                    $datos["direccion_3"][] = strtoupper($row->municipio.", ".$row->estado.",MX.");

                    $datos["telefono"][] = $row->telefono_movil;
                    $datos["celular"][] = $row->otro_telefono;
                    $datos["contacto"][] = strtoupper($row->nombre_contacto_compania);

                    $datos["rfc"][] = strtoupper($row->rfc);
                    $datos["correo"][] = strtoupper($row->datos_email);
                    $datos["correo2"][] = strtoupper($row->correo_compania);

                    $datos["observaciones"][] = strtoupper($row->comentarios_servicio);

                    $datos["asesor"][] = strtoupper($row->asesor);

                    $datos["placas"][] = strtoupper($row->vehiculo_placas);
                    $datos["auto"][] = strtoupper($row->vehiculo_anio);

                    $queryColor  = $this->mConsultas->get_result('id',$row->id_color,'cat_colores');
                    foreach ($queryColor as $row2) {
                        $datos["color"][] = $row2->color;
                    }

                    // $datos["color"][] = strtoupper($row->color);
                    $datos["kilometraje"][] = strtoupper($row->vehiculo_kilometraje);

                    $datos["cilindros"][] = strtoupper($row->cilindros);
                    $datos["modelo"][] = strtoupper($row->categoria);
                    $datos["motor"][] = strtoupper($row->motor);
                    $datos["transmision"][] = strtoupper($row->transmision);

                    $identificador = $this->mConsultas->getTipoOrdenIdentificacion($row->id_tipo_orden);
                    $datos["ordent"][] = strtoupper($row->id_cita."".$identificador);
                    $datos["id"][] = $row->id;
                    $datos["serie"][] = strtoupper((($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion));
                    if ($row->fecha != NULL) {
                        $fecha = explode("-",$row->fecha_recepcion);
                        $datos["fecha"][] = $fecha[2]."/".$fecha[1]."/".$fecha[0];
                    }else {
                        $datos["fecha"][] = "00/00/0000";
                    }

                    $datos["origen"][] = "ORDEN";
                }
            }

            //Recuperamos los id de la consulta de magic
            if (isset($consultaMagic)) {
                foreach ($consultaMagic as $row) {
                    $idBusqueda[] = $row->id;
                }
            }

            //Verificamos no se haya superado el limite de registros a imprimir
            if ($limitante < 41) {
                for ($i=0; $i < count($idBusqueda) ; $i++) {
                    //Recuperamos la informacion del cliente
                    $perfil = $this->mConsultas->idFichaPerfil($idBusqueda[$i]);
                    foreach ($perfil as $row) {
                        $datos["nombre"][] = strtoupper($row->nombre);
                        $datos["ap"][] = strtoupper($row->ap);
                        $datos["am"][] = strtoupper($row->am);
                        $datos["cliente"][] = strtoupper($row->codigoCte);
                        $datos["rzoSocial"][] = strtoupper($row->razon_social);

                        $direccion = $row->calle." ".$row->numero_exterior.",".$row->colonia.", Cp. ".$row->cp;
                        $datos["direccion"][] = strtoupper($direccion.", ".$row->municipio.", ".$row->estado.",MX.");

                        $datos["direccion_1"][] = strtoupper($row->calle." ".$row->numero_exterior." ".$row->numero_interior.",".$row->colonia.",");
                        $datos["direccion_2"][] = strtoupper(" Cp. ".$row->cp.", ".$row->municipio.", ".$row->estado.",MX.");

                        $datos["telefono"][] = $row->tel_principal;
                        $datos["celular"][] = $row->tel_adicional;
                        $datos["contacto"][] = strtoupper($row->dirigirse_con);

                        $datos["rfc"][] = strtoupper($row->rfc);
                        $datos["correo"][] = strtoupper($row->correo);
                        $datos["telefonoAdi"][] = strtoupper($row->tel_secundario);

                        $datos["torre"][] = strtoupper($row->notorre);
                        $datos["km_actual"][] = strtoupper($row->km_actual);
                        $datos["asesor"][] = $this->mConsultas->nombreOperador($row->noasesor);

                        $datos["placas"][] = strtoupper($row->placas);
                        $datos["siniestro"][] = strtoupper($row->no_siniestro);

                        $datos["ordent"][] = strtoupper($row->no_orden);
                        // $datos["id"][] = $row->id;
                        $datos["serie"][] = strtoupper($row->serie_larga);
                        if ($row->fecha_recepcion != NULL) {
                            $fecha = explode("-",$row->fecha_recepcion);
                            $datos["fecha"][] = $fecha[2]."/".$fecha[1]."/".$fecha[0];
                        }else {
                            $datos["fecha"][] = "00/00/0000";
                        }

                        $datos["origen"][] = "MAGIC";

                        //Verificamos el limitador
                        $limitante++;
                        if ($limitante > 42) {
                            break;
                        }
                    }
                }
            }
            $datos["orientacion"] = "L";

            // $paquete["respuesta"] = $datos;
            $this->generatePDF($datos,"modulosExternos/busquedaMagic");

        // } catch (\Exception $e) {
            // $paquete["respuesta"] = "Error";
            // echo "ERROR";
        // }

          // $respuesta = json_encode($paquete);
          // echo $respuesta;
    }

    //PDF resultante de busqueda
    public function pdfBusqueda($idMagic = 0,$datos = NULL)
    {
        $datos["orientacion"] = "P";
        //Recuperamos la informacion del cliente
        $perfil = $this->mConsultas->idFichaPerfil($idMagic);
        foreach ($perfil as $row) {
            $datos["nombre"][] = strtoupper($row->nombre);
            $datos["ap"][] = strtoupper($row->ap);
            $datos["am"][] = strtoupper($row->am);
            $datos["cliente"][] = strtoupper($row->codigoCte);
            $datos["rzoSocial"][] = strtoupper($row->razon_social);

            $direccion = $row->calle." ".$row->numero_exterior." ".$row->numero_interior.",".$row->colonia.", Cp. ".$row->cp;
            $datos["direccion"][] = strtoupper($direccion.", ".$row->municipio.", ".$row->estado.",MX.");

            $datos["telefono"][] = $row->tel_principal;
            $datos["celular"][] = $row->tel_adicional;
            $datos["contacto"][] = strtoupper($row->dirigirse_con);

            $datos["rfc"][] = strtoupper($row->rfc);
            $datos["correo"][] = strtoupper($row->correo);
            $datos["telefonoAdi"][] = strtoupper($row->tel_secundario);

            $datos["torre"][] = strtoupper($row->notorre);
            $datos["km_actual"][] = strtoupper($row->km_actual);
            $datos["asesor"][] = $this->mConsultas->nombreOperador($row->noasesor);

            $datos["placas"][] = strtoupper($row->placas);
            $datos["siniestro"][] = strtoupper($row->no_siniestro);

            $datos["ordent"][] = strtoupper($row->no_orden);
            // $datos["id"][] = $row->id;
            $datos["serie"][] = strtoupper($row->serie_larga);
            if ($row->fecha_recepcion != NULL) {
                $fecha = explode("-",$row->fecha_recepcion);
                $datos["fecha"][] = $fecha[2]."/".$fecha[1]."/".$fecha[0];
            }else {
                $datos["fecha"][] = "00/00/0000";
            }
        }
        $this->generatePDF($datos,"modulosExternos/fichaMagic");

    }

    //Generamos pdf
    public function generatePDF($datos = NULL,$vista = "")
    {
        ini_set('max_execution_time',3000);
        //Orientación  de la hoja P->Vertical   L->Horizontal
        $html2pdf = new Html2Pdf($datos["orientacion"], 'A4', 'en',TRUE,'UTF-8',array(10,10,10,10));
        try {
            //Creamos las hojas del pdf por registro

            $html = $this->load->view($vista, $datos, TRUE);
            // $html = $this->load->view("ordenServicio/plantillaPdfPita", $datos, TRUE);
            $html2pdf->setDefaultFont('Arial');     //Helvetica
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->WriteHTML($html);
            $html2pdf->setTestTdInOnePage(FALSE);
            $html2pdf->Output();
        } catch (Html2PdfException $e) {
            $html2pdf->clean();
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "",$titulo = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                // 'assets/js/firmaCliente.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => $titulo
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }



}
