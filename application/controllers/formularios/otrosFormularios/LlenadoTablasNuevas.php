<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LlenadoTablasNuevas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($x = 0,$datos = NULL)
    {
        
    }

    //Creamos los registros para pruebas de documentacion
    public function crear_regsitros_documentacion($x = NULL)
    {
        $documentacion = [];
        $registro = date("Y-m-d H:i:s");

        $campos = $this->mConsultas->recuperar_documentaciones();
        //$campos = $this->mConsultas->recuperar_documentaciones_fecha(date("Y-m-d"));

        foreach ($campos as $row) {
            //Verificamos si hay algun presupuesto
            if (($row->idCotizacion != "")|| ($row->id_presupuesto != "")) {
                //Si existe un presupuesto identificamos si es nuevo o viejo
                // (si se encuentra un id en los nuevos presupuesto, se supone que es nuevo formato)
                if ($row->id_presupuesto != "") {
                    $presupuesto = $row->id_presupuesto;
                    $afecta_cliente = $row->acepta_cliente;
                    $origen = "1";
                //De lo contrario se deduce que es un presupuesto viejito
                } else {
                    $presupuesto = $row->idCotizacion;
                    $afecta_cliente = $row->aceptoTermino;
                    $origen = "2";
                }
            
            //De lo contrario, no hay ningun presupuesto hecho
            } else {
                $presupuesto = "";
                $afecta_cliente = "";
                $origen = "0";
            }
            
            $contededor = array(
                "id" => NULL,
                "id_cita" => $row->orden_cita,
                "folio_externo" => $row->folioIntelisis,
                "fecha_recepcion" => $row->fecha_recepcion,
                "vehiculo" => $row->vehiculo_modelo,
                "placas" => $row->vehiculo_placas,
                "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                "asesor" => $row->asesor,
                "tecnico" => (($row->tecnico != NULL) ? $row->tecnico : "T"),
                "id_tecnico" => $row->id_tecnico,
                "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                "oasis" => $row->archivoOasis,
                "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                "presupuesto" => (($presupuesto != "") ? "1" : "0"),
                "presupuesto_afectado" => $afecta_cliente,
                "presupuesto_origen" => $origen,
                "voz_cliente" => ((($row->idVoz != "")||($row->vc_id != "")) ? "1" : "0"),
                "evidencia_voz_cliente" => (($row->evidencia !="") ? $row->evidencia : (($row->audio != "") ? $row->audio : "")),
                "servicio_valet" => (($row->idValet != "") ? "1" : "0"),
                "fechas_creacion" => $registro,
            );

            $documentacion[] = $contededor;
        }

        $total_reg = count($documentacion) - 1;
        for ($i=0; $i <= $total_reg  ; $i++) { 
            if ($documentacion[($total_reg - $i)]["id_cita"] != NULL) {
                $guardado = $this->mConsultas->save_register('documentacion_ordenes',$documentacion[($total_reg - $i)]);
                if ($guardado != 0) {
                    echo $documentacion[($total_reg - $i)]["id_cita"]." - ".$i."<br>";
                }else{
                    echo "0 - ".$i."<br>";
                }
            }
        }   
    }

    public function actualizar_regsitros_documentacion($fecha = "")
    {
        $documentacion = [];
        $id_citas = [];

        $documentacion_nvo = [];
        $id_citas_nvo = [];

        $registro = date("Y-m-d H:i:s");

        if (strlen($fecha) <= 6) {
            $fecha = date("Y-m-d");
        }

        //$campos = $this->mConsultas->recuperar_documentaciones();
        $campos = $this->mConsultas->recuperar_documentaciones_fecha($fecha);

        foreach ($campos as $row) {
            //Verificamos si hay algun presupuesto
            if (($row->idCotizacion != "")|| ($row->id_presupuesto != "")) {
                //Si existe un presupuesto identificamos si es nuevo o viejo
                // (si se encuentra un id en los nuevos presupuesto, se supone que es nuevo formato)
                if ($row->id_presupuesto != "") {
                    $presupuesto = $row->id_presupuesto;
                    $afecta_cliente = $row->acepta_cliente;
                    $origen = "1";
                //De lo contrario se deduce que es un presupuesto viejito
                } else {
                    $presupuesto = $row->idCotizacion;
                    $afecta_cliente = $row->aceptoTermino;
                    $origen = "2";
                }
            
            //De lo contrario, no hay ningun presupuesto hecho
            } else {
                $presupuesto = "";
                $afecta_cliente = "";
                $origen = "0";
            }

            $revision = $this->mConsultas->get_result("id_cita",$row->orden_cita,"documentacion_ordenes");
            foreach ($revision as $row2) {
                $verificacion = $row2->id;
            }

            //Si existe el registro lo creamos
            if (isset($verificacion)) {
                $documentacion[] = array(
                    //"id" => NULL,
                    //"id_cita" => $row->orden_cita,
                    "folio_externo" => $row->folioIntelisis,
                    "fecha_recepcion" => $row->fecha_recepcion,
                    "vehiculo" => $row->vehiculo_modelo,
                    "placas" => $row->vehiculo_placas,
                    "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                    "asesor" => $row->asesor,
                    "tecnico" => $row->tecnico,
                    "id_tecnico" => $row->id_tecnico,
                    "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                    "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                    "oasis" => $row->archivoOasis,
                    "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                    "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                    "presupuesto" => (($presupuesto != "") ? "1" : "0"),
                    "presupuesto_afectado" => $afecta_cliente,
                    "presupuesto_origen" => $origen,
                    "voz_cliente" => ((($row->idVoz != "")||($row->vc_id != "")) ? "1" : "0"),
                    "evidencia_voz_cliente" => (($row->evidencia !="") ? $row->evidencia : (($row->audio != "") ? $row->audio : "")),
                    "servicio_valet" => (($row->idValet != "") ? "1" : "0"),
                    //"fechas_creacion" => $registro,
                );
                $id_citas[] = $row->orden_cita;
            }else{
                $documentacion_nvo[] = array(
                    "id" => NULL,
                    "id_cita" => $row->orden_cita,
                    "folio_externo" => $row->folioIntelisis,
                    "fecha_recepcion" => $row->fecha_recepcion,
                    "vehiculo" => $row->vehiculo_modelo,
                    "placas" => $row->vehiculo_placas,
                    "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                    "asesor" => $row->asesor,
                    "tecnico" => $row->tecnico,
                    "id_tecnico" => $row->id_tecnico,
                    "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                    "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                    "oasis" => $row->archivoOasis,
                    "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                    "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                    "presupuesto" => (($presupuesto != "") ? "1" : "0"),
                    "presupuesto_afectado" => $afecta_cliente,
                    "presupuesto_origen" => $origen,
                    "voz_cliente" => ((($row->idVoz != "")||($row->vc_id != "")) ? "1" : "0"),
                    "evidencia_voz_cliente" => (($row->evidencia !="") ? $row->evidencia : (($row->audio != "") ? $row->audio : "")),
                    "servicio_valet" => (($row->idValet != "") ? "1" : "0"),
                    "fechas_creacion" => $registro,
                );

                $id_citas_nvo[] = $row->orden_cita;
            }
        }

        $total_reg = count($documentacion) - 1;
        $total_reg_nvo = count($documentacion_nvo) - 1;

        for ($i=0; $i <= $total_reg  ; $i++) { 
            $guardado = $this->mConsultas->update_table_row('documentacion_ordenes',$documentacion[($total_reg - $i)],'id_cita',$id_citas[($total_reg - $i)]);
            if ($guardado) {
                echo $id_citas[($total_reg - $i)]." - ".$i."<br>";
            }else{
                echo $id_citas[($total_reg - $i)]."*0 - ".$i."<br>";
            }
        }

        for ($i=0; $i <= $total_reg_nvo ; $i++) {
            $guardado = $this->mConsultas->save_register('documentacion_ordenes',$documentacion[($total_reg_nvo - $i)]);
            if ($guardado != 0) {
                echo "ALTA: ".$documentacion[($total_reg_nvo - $i)]["id_cita"]." - ".$i."<br>";
            }else{
                echo "ALTA: 0 - ".$i."<br>";
            }
        }
    }

    //Llenar tabla de indicadores multipunto
    public function llenado_indicadores_multipunto($value='',$datos = NULL)
    {
        $query = $this->mConsultas->recopilacion_multipuntos();

        $registro_indicador = [];

        $registro = date("Y-m-d H:i:s");
        $marcador = FALSE;
        foreach ($query as $row){
            if ($row->bateriaEstado == "Aprobado") {
                $bateria = "V";
            }elseif ($row->bateriaEstado == "Futuro") {
                $bateria = "A";
                $marcador = TRUE;
            }else {
                $bateria = "R";
                $marcador = TRUE;
            }

            //Frente izquierdo
            if ($row->dNeumaticoFI == "Aprobado") {
                $neumaticoFI = "V";
            }elseif ($row->dNeumaticoFI == "Futuro") {
                $neumaticoFI = "A";
                $marcador = TRUE;
            }else {
                $neumaticoFI = "R";
                $marcador = TRUE;
            }

            if ($row->espesoBalataFI == "Aprobado") {
                $balatasFI = "V";
            }elseif ($row->espesoBalataFI == "Futuro") {
                $balatasFI = "A";
                $marcador = TRUE;
            }else {
                $balatasFI = "R";
                $marcador = TRUE;
            }

            if ($row->espesorDiscoFI == "Aprobado") {
                $discoFI = "V";
            }elseif ($row->espesorDiscoFI == "Futuro") {
                $discoFI = "A";
                $marcador = TRUE;
            }else {
                $discoFI = "R";
                $marcador = TRUE;
            }

            //Parte trasera izquierda
            if ($row->dNeumaticoTI == "Aprobado") {
                $neumaticoTI = "V";
            }elseif ($row->dNeumaticoTI == "Futuro") {
                $neumaticoTI = "A";
                $marcador = TRUE;
            }else {
                $neumaticoTI = "R";
                $marcador = TRUE;
            }

            if ($row->espesoBalataTI == "Aprobado") {
                $balatasTI = "V";
            }elseif ($row->espesoBalataTI == "Futuro") {
                $balatasTI = "A";
                $marcador = TRUE;
            }else {
                $balatasTI = "R";
                $marcador = TRUE;
            }

            if ($row->espesorDiscoTI == "Aprobado") {
                $discoTI = "V";
            }elseif ($row->espesorDiscoTI == "Futuro") {
                $discoTI = "A";
                $marcador = TRUE;
            }else {
                $discoTI = "R";
                $marcador = TRUE;
            }

            if ($row->diametroTamborTI == "Aprobado") {
                $tamborTI = "V";
            }elseif ($row->diametroTamborTI == "Futuro") {
                $tamborTI = "A";
                $marcador = TRUE;
            }else {
                $tamborTI = "R";
                $marcador = TRUE;
            }

            //Frente derecho
            if ($row->dNeumaticoFD == "Aprobado") {
                $neumaticoFD = "V";
            }elseif ($row->dNeumaticoFD == "Futuro") {
                $neumaticoFD = "A";
                $marcador = TRUE;
            }else {
                $neumaticoFD = "R";
                $marcador = TRUE;
            }

            if ($row->espesoBalataFD == "Aprobado") {
                $balatasFD = "V";
            }elseif ($row->espesoBalataFD == "Futuro") {
                $balatasFD = "A";
                $marcador = TRUE;
            }else {
                $balatasFD = "R";
                $marcador = TRUE;
            }

            if ($row->espesorDiscoFD == "Aprobado") {
                $discoFD = "V";
            }elseif ($row->espesorDiscoFD == "Futuro") {
                $discoFD = "A";
                $marcador = TRUE;
            }else {
                $discoFD = "R";
                $marcador = TRUE;
            }

            //Parte Trasera derecha
            if ($row->dNeumaticoTD == "Aprobado") {
                $neumaticoTD = "V";
            }elseif ($row->dNeumaticoTD == "Futuro") {
                $neumaticoTD = "A";
                $marcador = TRUE;
            }else {
                $neumaticoTD = "R";
                $marcador = TRUE;
            }

            if ($row->espesoBalataTD == "Aprobado") {
                $balatasTD = "V";
            }elseif ($row->espesoBalataTD == "Futuro") {
                $balatasTD = "A";
                $marcador = TRUE;
            }else {
                $balatasTD = "R";
                $marcador = TRUE;
            }

            if ($row->espesorDiscoTD == "Aprobado") {
                $discoTD = "V";
            }elseif ($row->espesorDiscoTD == "Futuro") {
                $discoTD = "A";
                $marcador = TRUE;
            }else {
                $discoTD = "R";
                $marcador = TRUE;
            }

            if ($row->diametroTamborTD == "Aprobado") {
                $tamborTD = "V";
            }elseif ($row->diametroTamborTD == "Futuro") {
                $tamborTD = "A";
                $marcador = TRUE;
            }else {
                $tamborTD = "R";
                $marcador = TRUE;
            }

            if ($marcador) {
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                //$proactivo = $this->mConsultas->idHistorialProactivo($serie);

                $indicador = array(
                    "id" => NULL,
                    "id_cita" => $row->id_cita,
                    "id_proactivo" => "909".$row->id_cita, //(($proactivo != "0") ? $proactivo : "909".$row->id_cita),
                    "folio" => $row->folioIntelisis,
                    "fecha_recepcion" => $row->fecha_recepcion." ".$row->hora_recepcion,
                    "cliente" => $row->nombreCliente,
                    "telefono" => "Tel. ".$row->telefono_movil."<br>Tel 2. ".$row->otro_telefono,
                    "asesor" => $row->asesor,
                    "tecnico" => $row->tecnico,
                    "modelo" => $row->vehiculo_modelo,
                    "placas" => $row->vehiculo_placas,
                    "serie" => $serie,

                    "bateriaEstado" => $bateria,
                    "bateriaCambio" => $row->bateriaCambio,
                    "dNeumaticoFI" => $neumaticoFI,
                    "dNeumaticoFIcambio" => $row->dNeumaticoFIcambio,
                    "espesoBalataFI" => $balatasFI,
                    "espesorBalataFIcambio" => $row->espesorBalataFIcambio,
                    "espesorDiscoFI" => $discoFI,
                    "espesorDiscoFIcambio" => $row->espesorDiscoFIcambio,
                    "dNeumaticoTI" => $neumaticoTI,
                    "dNeumaticoTIcambio" => $row->dNeumaticoTIcambio,
                    "espesoBalataTI" => $balatasTI,
                    "espesorBalataTIcambio" => $row->espesorBalataTIcambio,
                    "espesorDiscoTI" => $discoTI,
                    "espesorDiscoTIcambio" => $row->espesorDiscoTIcambio,
                    "diametroTamborTI" => $tamborTI,
                    "diametroTamborTIcambio" => $row->diametroTamborTIcambio,
                    "dNeumaticoFD" =>  $neumaticoFD,
                    "dNeumaticoFDcambio" => $row->dNeumaticoFDcambio,
                    "espesoBalataFD" =>  $balatasFD,
                    "espesorBalataFDcambio" => $row->espesorBalataFDcambio,
                    "espesorDiscoFD" =>  $discoFD,
                    "espesorDiscoFDcambio" => $row->espesorDiscoFDcambio,
                    "dNeumaticoTD" =>  $neumaticoTD,
                    "dNeumaticoTDcambio" => $row->dNeumaticoTDcambio,
                    "espesoBalataTD" =>  $balatasTD,
                    "espesorBalataTDcambio" => $row->espesorBalataTDcambio,
                    "espesorDiscoTD" =>  $discoTD,
                    "espesorDiscoTDcambio" => $row->espesorDiscoTDcambio,
                    "diametroTamborTD" => $tamborTD,
                    "diametroTamborTDcambio" => $row->diametroTamborTDcambio,

                    "fecha_alta" => $registro,
                    "fecha_actualiza" => $registro,
                );

                //var_dump($indicador);
                //echo "<br>---<br>";
                $registro_indicador[] = $indicador;
            }

            $marcador = FALSE;
        }

        $total_reg = count($registro_indicador) - 1;
        for ($i=0; $i <= $total_reg  ; $i++) { 
            $guardado = $this->mConsultas->save_register('indicadores_multipunto',$registro_indicador[($total_reg - $i)]);
            if ($guardado != 0) {
                echo $guardado." - ".$i."<br>";
            }else{
                echo "0 - ".$i."<br>";
            }
        }
    }

    //Llenar tabla de indicadores multipunto
    public function actualizar_indicadores_multipunto($fecha='',$datos = NULL)
    {
        $registro_indicador = [];
        $id_citas = [];
        $registro = date("Y-m-d H:i:s");

        if (strlen($fecha) <= 6) {
            $fecha = date("Y-m-d");
        }

        $query = $this->mConsultas->recopilacion_multipuntos_fecha($fecha);

        $marcador = FALSE;
        foreach ($query as $row){
            if ($row->bateriaEstado == "Aprobado") {
                $bateria = "V";
            }elseif ($row->bateriaEstado == "Futuro") {
                $bateria = "A";
                $marcador = TRUE;
            }else {
                $bateria = "R";
                $marcador = TRUE;
            }

            //Frente izquierdo
            if ($row->dNeumaticoFI == "Aprobado") {
                $neumaticoFI = "V";
            }elseif ($row->dNeumaticoFI == "Futuro") {
                $neumaticoFI = "A";
                $marcador = TRUE;
            }else {
                $neumaticoFI = "R";
                $marcador = TRUE;
            }

            if ($row->espesoBalataFI == "Aprobado") {
                $balatasFI = "V";
            }elseif ($row->espesoBalataFI == "Futuro") {
                $balatasFI = "A";
                $marcador = TRUE;
            }else {
                $balatasFI = "R";
                $marcador = TRUE;
            }

            if ($row->espesorDiscoFI == "Aprobado") {
                $discoFI = "V";
            }elseif ($row->espesorDiscoFI == "Futuro") {
                $discoFI = "A";
                $marcador = TRUE;
            }else {
                $discoFI = "R";
                $marcador = TRUE;
            }

            //Parte trasera izquierda
            if ($row->dNeumaticoTI == "Aprobado") {
                $neumaticoTI = "V";
            }elseif ($row->dNeumaticoTI == "Futuro") {
                $neumaticoTI = "A";
                $marcador = TRUE;
            }else {
                $neumaticoTI = "R";
                $marcador = TRUE;
            }

            if ($row->espesoBalataTI == "Aprobado") {
                $balatasTI = "V";
            }elseif ($row->espesoBalataTI == "Futuro") {
                $balatasTI = "A";
                $marcador = TRUE;
            }else {
                $balatasTI = "R";
                $marcador = TRUE;
            }

            if ($row->espesorDiscoTI == "Aprobado") {
                $discoTI = "V";
            }elseif ($row->espesorDiscoTI == "Futuro") {
                $discoTI = "A";
                $marcador = TRUE;
            }else {
                $discoTI = "R";
                $marcador = TRUE;
            }

            if ($row->diametroTamborTI == "Aprobado") {
                $tamborTI = "V";
            }elseif ($row->diametroTamborTI == "Futuro") {
                $tamborTI = "A";
                $marcador = TRUE;
            }else {
                $tamborTI = "R";
                $marcador = TRUE;
            }

            //Frente derecho
            if ($row->dNeumaticoFD == "Aprobado") {
                $neumaticoFD = "V";
            }elseif ($row->dNeumaticoFD == "Futuro") {
                $neumaticoFD = "A";
                $marcador = TRUE;
            }else {
                $neumaticoFD = "R";
                $marcador = TRUE;
            }

            if ($row->espesoBalataFD == "Aprobado") {
                $balatasFD = "V";
            }elseif ($row->espesoBalataFD == "Futuro") {
                $balatasFD = "A";
                $marcador = TRUE;
            }else {
                $balatasFD = "R";
                $marcador = TRUE;
            }

            if ($row->espesorDiscoFD == "Aprobado") {
                $discoFD = "V";
            }elseif ($row->espesorDiscoFD == "Futuro") {
                $discoFD = "A";
                $marcador = TRUE;
            }else {
                $discoFD = "R";
                $marcador = TRUE;
            }

            //Parte Trasera derecha
            if ($row->dNeumaticoTD == "Aprobado") {
                $neumaticoTD = "V";
            }elseif ($row->dNeumaticoTD == "Futuro") {
                $neumaticoTD = "A";
                $marcador = TRUE;
            }else {
                $neumaticoTD = "R";
                $marcador = TRUE;
            }

            if ($row->espesoBalataTD == "Aprobado") {
                $balatasTD = "V";
            }elseif ($row->espesoBalataTD == "Futuro") {
                $balatasTD = "A";
                $marcador = TRUE;
            }else {
                $balatasTD = "R";
                $marcador = TRUE;
            }

            if ($row->espesorDiscoTD == "Aprobado") {
                $discoTD = "V";
            }elseif ($row->espesorDiscoTD == "Futuro") {
                $discoTD = "A";
                $marcador = TRUE;
            }else {
                $discoTD = "R";
                $marcador = TRUE;
            }

            if ($row->diametroTamborTD == "Aprobado") {
                $tamborTD = "V";
            }elseif ($row->diametroTamborTD == "Futuro") {
                $tamborTD = "A";
                $marcador = TRUE;
            }else {
                $tamborTD = "R";
                $marcador = TRUE;
            }

            if ($marcador) {
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                //$proactivo = $this->mConsultas->idHistorialProactivo($serie);
                $revision = $this->mConsultas->get_result("id_cita",$row->id_cita,"indicadores_multipunto");
                foreach ($revision as $row2) {
                    $verificacion = $row2->id;
                }

                //Si existe el registro lo creamos
                if (isset($verificacion)) {
                    $indicador = array(
                        //"id" => NULL,
                        //"id_cita" => $row->id_cita,
                        //"id_proactivo" => "909".$row->id_cita, //(($proactivo != "0") ? $proactivo : "909".$row->id_cita),
                        "folio" => $row->folioIntelisis,
                        //"fecha_recepcion" => $row->fecha_recepcion." ".$row->hora_recepcion,
                        "cliente" => $row->nombreCliente,
                        "telefono" => "Cel: ".$row->telefono_movil."<br>Tel: ".$row->otro_telefono,
                        "asesor" => $row->asesor,
                        "tecnico" => $row->tecnico,
                        "modelo" => $row->vehiculo_modelo,
                        "placas" => $row->vehiculo_placas,
                        "serie" => $serie,

                        "bateriaEstado" => $bateria,
                        "bateriaCambio" => $row->bateriaCambio,
                        "dNeumaticoFI" => $neumaticoFI,
                        "dNeumaticoFIcambio" => $row->dNeumaticoFIcambio,
                        "espesoBalataFI" => $balatasFI,
                        "espesorBalataFIcambio" => $row->espesorBalataFIcambio,
                        "espesorDiscoFI" => $discoFI,
                        "espesorDiscoFIcambio" => $row->espesorDiscoFIcambio,
                        "dNeumaticoTI" => $neumaticoTI,
                        "dNeumaticoTIcambio" => $row->dNeumaticoTIcambio,
                        "espesoBalataTI" => $balatasTI,
                        "espesorBalataTIcambio" => $row->espesorBalataTIcambio,
                        "espesorDiscoTI" => $discoTI,
                        "espesorDiscoTIcambio" => $row->espesorDiscoTIcambio,
                        "diametroTamborTI" => $tamborTI,
                        "diametroTamborTIcambio" => $row->diametroTamborTIcambio,
                        "dNeumaticoFD" =>  $neumaticoFD,
                        "dNeumaticoFDcambio" => $row->dNeumaticoFDcambio,
                        "espesoBalataFD" =>  $balatasFD,
                        "espesorBalataFDcambio" => $row->espesorBalataFDcambio,
                        "espesorDiscoFD" =>  $discoFD,
                        "espesorDiscoFDcambio" => $row->espesorDiscoFDcambio,
                        "dNeumaticoTD" =>  $neumaticoTD,
                        "dNeumaticoTDcambio" => $row->dNeumaticoTDcambio,
                        "espesoBalataTD" =>  $balatasTD,
                        "espesorBalataTDcambio" => $row->espesorBalataTDcambio,
                        "espesorDiscoTD" =>  $discoTD,
                        "espesorDiscoTDcambio" => $row->espesorDiscoTDcambio,
                        "diametroTamborTD" => $tamborTD,
                        "diametroTamborTDcambio" => $row->diametroTamborTDcambio,

                        //"fecha_alta" => $registro,
                        "fecha_actualiza" => $registro,
                    );

                    $id_cita[] = $row->id_cita;
                    $registro_indicador[] = $indicador;

                }else{
                    $indicador = array(
                        "id" => NULL,
                        "id_cita" => $row->id_cita,
                        "id_proactivo" => "909".$row->id_cita, //(($proactivo != "0") ? $proactivo : "909".$row->id_cita),
                        "folio" => (($row->folioIntelisis != NULL) ? $row->folioIntelisis : ''),
                        "fecha_recepcion" => $row->fecha_recepcion." ".$row->hora_recepcion,
                        "cliente" => $row->nombreCliente,
                        "telefono" => "Tel. ".$row->telefono_movil."<br>Tel 2. ".$row->otro_telefono,
                        "asesor" => $row->asesor,
                        "tecnico" => $row->tecnico,
                        "modelo" => $row->vehiculo_modelo,
                        "placas" => $row->vehiculo_placas,
                        "serie" => $serie,

                        "bateriaEstado" => $bateria,
                        "bateriaCambio" => $row->bateriaCambio,
                        "dNeumaticoFI" => $neumaticoFI,
                        "dNeumaticoFIcambio" => $row->dNeumaticoFIcambio,
                        "espesoBalataFI" => $balatasFI,
                        "espesorBalataFIcambio" => $row->espesorBalataFIcambio,
                        "espesorDiscoFI" => $discoFI,
                        "espesorDiscoFIcambio" => $row->espesorDiscoFIcambio,
                        "dNeumaticoTI" => $neumaticoTI,
                        "dNeumaticoTIcambio" => $row->dNeumaticoTIcambio,
                        "espesoBalataTI" => $balatasTI,
                        "espesorBalataTIcambio" => $row->espesorBalataTIcambio,
                        "espesorDiscoTI" => $discoTI,
                        "espesorDiscoTIcambio" => $row->espesorDiscoTIcambio,
                        "diametroTamborTI" => $tamborTI,
                        "diametroTamborTIcambio" => $row->diametroTamborTIcambio,
                        "dNeumaticoFD" =>  $neumaticoFD,
                        "dNeumaticoFDcambio" => $row->dNeumaticoFDcambio,
                        "espesoBalataFD" =>  $balatasFD,
                        "espesorBalataFDcambio" => $row->espesorBalataFDcambio,
                        "espesorDiscoFD" =>  $discoFD,
                        "espesorDiscoFDcambio" => $row->espesorDiscoFDcambio,
                        "dNeumaticoTD" =>  $neumaticoTD,
                        "dNeumaticoTDcambio" => $row->dNeumaticoTDcambio,
                        "espesoBalataTD" =>  $balatasTD,
                        "espesorBalataTDcambio" => $row->espesorBalataTDcambio,
                        "espesorDiscoTD" =>  $discoTD,
                        "espesorDiscoTDcambio" => $row->espesorDiscoTDcambio,
                        "diametroTamborTD" => $tamborTD,
                        "diametroTamborTDcambio" => $row->diametroTamborTDcambio,

                        "fecha_alta" => $registro,
                        "fecha_actualiza" => $registro,
                    );

                    $id_cita_alta[] = $row->id_cita;
                    $registro_indicador_alta[] = $indicador;
                }
            }

            $marcador = FALSE;
        }

        $total_reg = count($registro_indicador) - 1;
        $total_alta = count($registro_indicador_alta) - 1;

        for ($i=0; $i <= $total_reg  ; $i++) { 
            $guardado = $this->mConsultas->update_table_row('indicadores_multipunto',$registro_indicador[($total_reg - $i)],'id_cita',$id_cita[($total_reg - $i)]);
            if ($guardado) {
                echo $id_cita[($total_reg - $i)]." - ".$i."<br>";
            }else{
                echo $id_cita[($total_reg - $i)]."*0 - ".$i."<br>";
            }
        }

        for ($i=0; $i <= $total_alta  ; $i++) { 
            $guardado = $this->mConsultas->save_register('indicadores_multipunto',$registro_indicador_alta[($total_alta - $i)]);
            if ($guardado != 0) {
                echo "Alta: ".$id_cita_alta[($total_alta - $i)]." - ".$i."<br>";
            }else{
                echo "Alta: ".$id_cita_alta[($total_alta - $i)]."*0 - ".$i."<br>";
            }
        }
    }


    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array('')
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Formulario"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }



}
