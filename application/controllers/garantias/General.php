<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class General extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($x = 0,$datos = NULL)
    {
        $this->loadAllView($datos,'garantias/generales/panel');
    }

    //Cargamos las ordenes de servicio para que la visualicen
    public function listaOrdenesHistoria($var = 0,$datos = NULL)
    {
        $datos["idOrden"] = [];
        //Verificamos y cargamos las ordenes de servicio sin cita
        // $campos = $this->mConsultas->get_table_desc_2();
        $campos = $this->mConsultas->listaOrdenes(); 
        foreach ($campos as $row) {
            if (!in_array($row->orden_cita,$datos["idOrden"])) {
                $corte_b = explode("-",$row->fecha_recepcion);
                if (count($corte_b)>1) {
                    $datos["fechaDiag"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0];
                }else{
                    $datos["fechaDiag"][] = "";
                }

                $datos["idOrden"][] = $row->orden_cita;
                $datos["id_cita_url"][] = $this->encrypt($row->orden_cita);
                $datos["idIntelisis"][] = $row->folioIntelisis;
                
                $datos["tecnico"][] = $row->tecnico;
                $datos["placas"][] = $row->vehiculo_placas;
                $datos["modelo"][] = $row->vehiculo_modelo;

                $datos["nombreAsesor"][] = $row->asesor;
                $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;

                $datos["cotizacion"][] = $row->idCotizacion ;
                $datos["vozAudio"][] = ((file_exists($row->evidencia)) ? $row->evidencia : "") ;
                $datos["vozCliente"][] = $row->idVoz ;
                $datos["multipunto"][] = $row->idMultipunto ;

                $datos["archivoOasis"][] = explode("|",$row->archivoOasis);
                
                $datos["lavado"][] = (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : '');
                $datos["firmasMultipunto"][] = $row->firmaJefeTaller ;
                $datos["cotizacionFinaliza"][] = $row->aceptoTermino ;
            }
        }

        if ($this->session->userdata('rolIniciado')) {
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
            //$datos["asesor"] = $this->session->userdata('nombreUsuario');
        }else{
            // $datos["dirige"] = "GENERICO";
            $datos["pOrigen"] = "";
        }

        //Enviamos la informacion a la vista
        $this->loadAllView($datos,'garantias/generales/listaHistorialOrden');
    }

    //Cargamos las cotizaciones que tienen piezas con garantías
    public function listaCotizaciones($x = "",$datos = NULL)
    {
        //Recuperamos todas las cotizaciones que se han generado en la multipunto
        //$query = $this->mConsultas->get_table_desc("cotizacion_multipunto");
        $query = $this->mConsultas->listaCotizacionDatosGarantia(); 
        $datos["idOrden"] = []; 

        foreach ($query as $row){
            //if ($row->pzaGarantia != "") {
                //Comprobamos que sea una cotizacion de la multipunto
                $datos["idOrden"][] = $row->idOrden;
                $datos["idIntelisis"][] = $row->folioIntelisis;

                $datos["placas"][] = $row->vehiculo_placas;
                $datos["tecnico"][] = $row->tecnico;
                $datos["modelo"][] = $row->vehiculo_modelo;
                $datos["asesor"][] = $row->asesor;

                $fecha = explode(" ",$row->fech_registro);
                $fechaFracc = explode("-",$row->fecha_recepcion);
                if (count($fechaFracc)>1) {
                    
                    $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
                }else{
                    $datos["fechaCaptura"][] = "00/00/0000";
                }

                //$fecha_2 = explode(" ",$row->fech_actualiza);
                //$fechaFracc_2 = explode("-",$fecha_2[0]);
                //$datos["fechaActualiza"][] = $fechaFracc_2[2]."/".$fechaFracc_2[1]."/".$fechaFracc_2[0];

                //$datos["subTotalMaterial"][] = $row->subTotal;
                //$datos["ivaMaterial"][] = $row->iva;
                //$datos["anticipo"][] = $row->anticipo;
                //$datos["totalMaterial"][] = $row->total;
                //$datos["canceladoMaterial"][] = $row->cancelado;
                $datos["firmaAsesor"][] = ($row->firmaAsesor != "") ? "SI" : "NO";
                $datos["aceptoTermino"][] = $row->aceptoTermino;
                $datos["aprobacionRef"][] = $row->aprobacionRefaccion;

                //Otros valores
                $datos["identificador"][] = $row->identificador;
                $datos["pzaGarantia"][] = explode("_", $row->pzaGarantia);
                $datos["envioGarantia"][] = $row->envioGarantia;
            //}
        }

        $this->loadAllView($datos,'cotizaciones/procesosExtras/listaCotiGarantia');
    }

    //Verificamos que exista la cotizacion para mostrarla
    public function validateCotiResp($idOrden = "0")
    {
        $respuesta = "0";
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            //Comprobamos que sea una cotizacion de la multipunto
            if ($row->identificador == "M") {
                $dato = $row->aceptoTermino;
            }
        }
        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = $dato;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "";
        }

        return $respuesta;
    }

    //Verificamos que exista la cotizacion para mostrarla
    public function validateCoti($idOrden = "0")
    {
        $respuesta = "0";
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == "M") {
                $dato = $row->idCotizacion;
            }
        }
        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "1";
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "0";
        }

        return $respuesta;
    }

    //Verificamos que exista la voz cliente para mostrarla
    public function validateVC($idOrden = "0")
    {
        $respuesta = "0";
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idPivote",$idOrden,"voz_cliente");
        foreach ($query as $row){
            $dato = $row->idPivote;
            $audio = $row->evidencia;
        }
        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = $audio."|1";
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "0|0";
        }

        return $respuesta;
    }

    //Verificamos que exista la multipunto para mostrarla
    public function validateMulti($idOrden = "0")
    {
        $respuesta = "0";
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("orden",$idOrden,"multipunto_general");
        foreach ($query as $row){
            $dato = $row->id;
        }
        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "1";
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "0";
        }

        return $respuesta;
    }

    //Validamos las firmas regsitradas en la multipunto  
    public function validateFirmaMulti($idOrden = "0")
    {
        //Respuesta con firmas nular
        $respuesta = ['0','0','0','0'];

        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("orden",$idOrden,"multipunto_general");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, recuperamos las firmas 
            $query2 = $this->mConsultas->get_result("idOrden",$dato,"multipunto_general_3");
            foreach ($query2 as $row2){
                $respuesta[0] = ($row2->firmaAsesor != "") ? "1" : "0";  //$row2->firmaAsesor;
                $respuesta[1] = ($row2->firmaTecnico != "") ? "1" : "0";  //$row2->firmaTecnico;                
                $respuesta[2] = ($row2->firmaJefeTaller != "") ? "1" : "0";  //$row2->firmaJefeTaller;
                $respuesta[3] = ($row2->firmaCliente != "") ? "1" : "0";  //$row2->firmaCliente;
            }
        }

        return $respuesta;
    }

    //Recuperamos las garantias que no se han firmado
    public function listaPorFirmar($x = "",$datos = NULL)
    {
        //$query = $this->mConsultas->get_table("garantias");
        $query = $this->mConsultas->get_table_list("garantias","idRegistro");
        foreach ($query as $row){
            //Comprobamos que las garantias que se muestren esten firmadas
            $temp = $this->validatePartTwo($row->idOrden);
            if (($row->firmaCliente == "")||($row->firmaTecnico == "")||($temp == "3")) {
                $datos["idRegistro"][] = $row->idRegistro;
                $datos["folio"][] = $row->folio;
                $datos["idOrden"][] = $row->idOrden;
                $datos["id_cita_url"][] = $this->encrypt($row->idOrden);
                
                $datos["noTorre"][] = $row->noTorre;

                $datos["firmaVal"][] = $row->firmaCliente;
                $datos["firmaVal2"][] = $row->firmaTecnico;

                $datos["idAsesor"][] = $row->idAsesor;
                $datos["placas"][] = $row->placas;
                $datos["telefono"][] = $row->telefono;
                $datos["nombreProp"][] = $row->nombreProp;

                $datos["fechaRecibe"][] = $row->fechaRecibe;
                $datos["fechaEntrega"][] = $row->fechaEntrega;
                $datos["noDistribuidor"][] = $row->noDistribuidor;

                $datos["garantia2"][] = $this->validatePartTwo($row->idOrden);
            }
        }

        //Enviamos la informacion a la vista
        $this->loadAllView($datos,'garantias/generales/porFirmar');
    }

    //Validar si existe la segunda parte de las garantias
    public function validatePartTwo($idOrden = 0)
    {
        $respuesta = "0";

        //Verificamos si existe la segunda parte
        $revision = $this->mConsultas->get_result("idOrden",$idOrden,"garantia_2");
        foreach ($revision as $row2) {
            $idGarantia = $row2->idRegistro;
            $firma1 = $row2->firma1;
            $firma2 = $row2->firma2;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idGarantia)) {
            // $respuesta = "2";
            if (($firma1 == "")||($firma2 == "")) {
                $respuesta = "3";
            }else {
                $respuesta = "2";
            }
        } else {
            $respuesta = "1";
        }

        return $respuesta;
    }

    //Cargamos la informacion del primer formulario
    //Recuperamos los campos de la base de datos
    public function setDataP1($idOrden = 0,$datos = NULL)
    {
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"garantias");
        foreach ($query as $row){
            $datos["idRegistro"] = $row->idRegistro;
            $datos["folio"] = $row->folio;
            $datos["idOrden"] = $row->idOrden;
            $datos["folioContinua"] = $row->folioContinua;
            $datos["noTorre"] = $row->noTorre;
            $datos["repGastosMisc"] = explode("_",$row->repGastosMisc);
            $datos["diasGM"] = $row->diasGM;
            $datos["precioGM"] = $row->precioGM;
            $datos["pagoClienteGM"] = $row->pagoClienteGM;
            $datos["gruaGM"] = $row->gruaGM;
            $datos["otrosGM"] = $row->otrosGM;
            $datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : "";
            $datos["firmaGerente"] = (file_exists($row->firmaGerente)) ? $row->firmaGerente : "";
            $datos["idAsesor"] = $row->idAsesor;
            $datos["placas"] = $row->placas;
            $datos["anio"] = $row->anio;
            $datos["tipo"] = $row->tipo;
            $datos["horaRecibo"] = $row->horaRecibo;
            $datos["horaPromedio"] = $row->horaPromedio;
            $datos["telefono"] = $row->telefono;
            $datos["nombreProp"] = $row->nombreProp;
            $datos["clienteVisita"] = $row->clienteVisita;
            $datos["domicilio"] = $row->domicilio;
            $datos["estado"] = $row->estado;
            $datos["cp"] = $row->cp;
            $datos["idVehiculo"] = explode("_",$row->idVehiculo);
            $datos["fechaRecibe"] = explode("-",$row->fechaRecibe);
            $datos["kmRecibe"] = explode("_",$row->kmRecibe);
            $datos["fechaEntrega"] = explode("-",$row->fechaEntrega);
            $datos["kmEntrega"] = explode("_",$row->kmEntrega);
            $datos["fechaGarantia"] = explode("-",$row->fechaGarantia);
            $datos["noDistribuidor"] = $row->noDistribuidor;
            $datos["fechaReparacion"] = explode("-",$row->fechaReparacion);
            $datos["oasis"] = $row->oasis;
            $datos["fechaVta"] = explode("-",$row->fechaVta);
            $datos["kmsVta"] = $row->kmsVta;
            $datos["noRefDoc"] = $row->noRefDoc;

            $datos["contenedorColeccion"] = explode("|",$row->contenedor);
            $datos["contenedor"] = $this->createCells(explode("|",$row->contenedor));

            $datos["totalPartes"] = $row->totalPartes;
            $datos["totalObra"] = $row->totalObra;
            $datos["totalMisc"] = $row->totalMisc;
            $datos["totalIva"] = $row->totalIva;
            $datos["totalCliente"] = $row->totalCliente;
            $datos["totalDistribuidor"] = $row->totalDistribuidor;
            $datos["totalReparacion"] = $row->totalReparacion;

            $datos["firmaTecnico"] = (file_exists($row->firmaTecnico)) ? $row->firmaTecnico : "";
            $datos["nombreTecnico"] = $row->nombreTecnico;
            $datos["firmaProv"] = (file_exists($row->firmaProv)) ? $row->firmaProv : "";
            $datos["nombreProv"] = $row->nombreProv;

            $datos["contenidoPartesColeccion"] = explode("|",$row->contenidoPartes);
            $datos["contenidoPartes"] = $this->createCells(explode("|",$row->contenidoPartes));
        }

        $datos["idOrden"] = $idOrden;

        //Verificamos donde se imprimira la información
        $this->loadAllView($datos,'garantias/generales/firmasP1');
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Validamos las firmas del primer formulario
    public function validateFormP1()
    {
        $datos["idOrden"] = $this->input->post("idOrden");
        $datos["tipoGuardado"] = $this->input->post("modoVistaFormulario");

        $this->form_validation->set_rules('rutaFirmaCliente', 'La firma del cliente es requerida.', 'required');
        $this->form_validation->set_rules('rutaFirmaGerente', 'La firma del gerente es requerida.', 'required');
        $this->form_validation->set_rules('rutaFirmaTecnico', 'La firma del técnico es requerida.', 'required');
        $this->form_validation->set_rules('rutaFirmaProv', 'La firma es requerida.', 'required');

        $this->form_validation->set_message('required','%s');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataFirmsP1($datos);
        }else{
            $datos["mensaje"]="0";
            $this->setDataP1($datos["idOrden"],$datos);
        }

    }

    //Guardamos las firmas del primer formulario
    public function getDataFirmsP1($datos = NULL)
    {
        $registro =  date("Y-m-d H:i");

        //Firma del cliente
        $dataSA["firmaCliente"] = $this->base64ToImage($this->input->post("rutaFirmaCliente"),"firmas");
        //Firma del gerente de servicio
        $dataSA["firmaGerente"] = $this->base64ToImage($this->input->post("rutaFirmaGerente"),"firmas");
        //Firma del tecnico
        $dataSA["firmaTecnico"] = $this->base64ToImage($this->input->post("rutaFirmaTecnico"),"firmas");
        //Firma aun sin personaje obligado
        $dataSA["firmaProv"] = $this->base64ToImage($this->input->post("rutaFirmaProv"),"firmas");

        $dataSA["fechaActualiza"] = $registro;

        $actualizar = $this->mConsultas->update_table_row('garantias',$dataSA,'idOrden',$datos["idOrden"]);
        if ($actualizar) {
            //Guardamos el movimiento
            // $this->loadHistory($datos["idOrden"],"Actualizacion firmas","Garantías");
            $datos["mensaje"]="1";
        }else {
            $datos["mensaje"]="0";
        }

        $this->setDataP1($datos["idOrden"],$datos);
    }

    //Recuperamos la información del registro
    public function setDataP2($idOrden = 0, $datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");

        //Consultamos si ya se entrego la unidad
        // $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idOrden);

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"garantia_2");
        foreach ($query as $row){
            $datos["xrep"] = $this->createCells(explode("|",$row->repN));
            $datos["xluzFalla"] = $this->createCells(explode("|",$row->luzFalla));
            $datos["xkoeo"] = $this->createCells(explode("|",$row->koeo));
            $datos["xkoec"] = $this->createCells(explode("|",$row->koec));
            $datos["xkoer"] = $this->createCells(explode("|",$row->koer));
            $datos["xcarroceria"] = $this->createCells(explode("|",$row->carroceria));
            $datos["xchassis"] = $this->createCells(explode("|",$row->chassis));
            $datos["xindefinido"] = $this->createCells(explode("|",$row->indefinido));
            $datos["xotro"] = $this->createCells(explode("|",$row->otro));

            $datos["xcontenidoRenglon"] = explode("|",$row->contenidoLabor);
            $datos["xcontenidoLabor"] = $this->createCells(explode("|",$row->contenidoLabor));

            $datos["firma1"] = $row->firma1;
            $datos["nombre1"] = $row->nombre1;
            $datos["firma2"] = $row->firma2;
            $datos["nombre2"] = $row->nombre2;
        }

        $datos["idOrden"] = $idOrden;

        //Verificamos donde se imprimira la información
        $this->loadAllView($datos,'garantias/generales/firmasP2');
    }

    //Validamos las firmas del primer formulario
    public function validateFormP2()
    {
        $datos["idOrden"] = $this->input->post("idOrden");
        $datos["tipoGuardado"] = $this->input->post("modoVistaFormulario");

        $this->form_validation->set_rules('rutaFirma1', 'La firma es requerida.', 'required');
        $this->form_validation->set_rules('rutaFirma2', 'La firma es requerida.', 'required');

        $this->form_validation->set_message('required','%s');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataFirmsP2($datos);
        }else{
            $datos["mensaje"]="0";
            $this->setDataP2($datos["idOrden"],$datos);
        }

    }

    //Guardamos las firmas del primer formulario
    public function getDataFirmsP2($datos = NULL)
    {
        $registro =  date("Y-m-d H:i");

        //Firma del cliente
        $dataGarant["firma1"] = $this->base64ToImage($this->input->post("rutaFirma1"),"firmas");
        //Firma del gerente de servicio
        $dataGarant["firma2"] = $this->base64ToImage($this->input->post("rutaFirma2"),"firmas");

        $dataGarant["fechaActualizacion"] = $registro;

        $actualizar = $this->mConsultas->update_table_row('garantia_2',$dataGarant,'idOrden',$datos["idOrden"]);
        if ($actualizar) {
            //Guardamos el movimiento
            // $this->loadHistory($datos["idOrden"],"Actualizacion firmas","Garantías");
            $datos["mensaje"]="1";
        }else {
            $datos["mensaje"]="0";
        }

        $this->setDataP2($datos["idOrden"],$datos);
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }
        $data = explode(',', $imgBase64);

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }
        }
        return $base;
    }

    function encrypt($data){
        /*$id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;*/
        return $data;
    }

    function decrypt($data){
        /*$url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;*/
        return $data;
    }

    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/superUsuario.js',
                'assets/js/garantias/firmas_gral.js',
                'assets/js/otrasVistas/buscadorVarios.js',
                'assets/js/diagnostico/filtrosListaOrden.js',
                'assets/js/diagnostico/buscarOrden.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Garantías"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }



}
