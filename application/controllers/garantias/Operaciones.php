<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Operaciones extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

  	public function index($idOrden = 0,$datos = NULL)
  	{
        $datos["dia"] = date("d");
        $datos["mes"] = date("m");
        $datos["anio"] = date("Y");
        $datos["hora"] = date("H:i");

        $id_cita = (($idOrden == "0") ? "0" : $this->decrypt($idOrden));

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        //Identificamos si la orden se servicio ya ha generado alguna cotizacion
        $revision = $this->mConsultas->get_result("idOrden",$id_cita,"garantias");
        foreach ($revision as $row) {
            $idGarantia = $row->idRegistro;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idGarantia)) {
            //Verificamos si la cotización ya fue aceptada/rechazada por el cliente
            $this->setData($id_cita,$datos);
        } else {
            $datos['idOrden'] = $id_cita;
            $this->loadAllView($datos,'garantias/operaciones_alta');
        }

  	}

    //Validamos los campos del formulario
    public function validateForm()
    {
        $datos["folio"] = $this->input->post("folio");
        $datos["idOrden"] = $this->input->post("idOrden");
        $datos["tipoGuardado"] = $this->input->post("modoVistaFormulario");
        $datos["idRegistro"] = $this->input->post("idRegistro");
        $datos["hora"] = date("H:i");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idOrden"]);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        if ($datos["tipoGuardado"] == 1) {
            $this->form_validation->set_rules('folio', 'El Folio es requerido.', 'required');
            // $this->form_validation->set_rules('folio', 'El Folio es requerido.', 'required');
            $this->form_validation->set_rules('idOrden', 'El No. Orden es requerido.', 'required|callback_existencia');
            $this->form_validation->set_rules('nombreTecnico', 'Falta completar.', 'required');
            $this->form_validation->set_rules('nombreProv', 'Falta completar.', 'required');
            //Valiamos firmas
            // $this->form_validation->set_rules('rutaFirmaCliente', 'La firma del cliente es requerida.', 'required');
            // $this->form_validation->set_rules('rutaFirmaGerente', 'La firma del gerente es requerida.', 'required');
        }elseif ($datos["tipoGuardado"] == 3) {
            $this->form_validation->set_rules('rutaFirmaCliente', 'La firma del cliente es requerida.', 'required');
            $this->form_validation->set_rules('rutaFirmaGerente', 'La firma del gerente es requerida.', 'required');
        }else {
            $this->form_validation->set_rules('folio', 'El Folio es requerido.', 'required');
            $this->form_validation->set_rules('idOrden', 'El No. Orden es requerido.', 'required');
            $this->form_validation->set_rules('nombreTecnico', 'Falta completar.', 'required');
            $this->form_validation->set_rules('nombreProv', 'Falta completar.', 'required');
        }

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('existencia','El campo ya existe. Favor de verificar este campo.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="2";
            $this->index('0',$datos);
        }
    }

    //Validamos si existe un registro con ese numero de orden / folio
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idOrden");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"garantias");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Recuperamos los campos del formulario
    public function getDataForm($datos = NULL)
    {
        $registro =  date("Y-m-d H:i");

        if ($datos["tipoGuardado"] == "1") {
            $dataSA = array(
                'idRegistro' => NULL,
                'folio' => $this->input->post("folio"),
                'idOrden' => $this->input->post("idOrden"),
            );
        }

        //Tabla estatica 4
        $dataSA["folioContinua"] = $this->input->post("folioContinue");
        $dataSA["noTorre"] = $this->input->post("noTorre");

        //Tabla estatica 1
        $resp = $this->input->post("repPrestamo")."_".$this->input->post("repPagoAlCliente")."_".$this->input->post("repGrua")."_".$this->input->post("repOtros");
        $dataSA["repGastosMisc"] = $resp;
        $dataSA["diasGM"] = $this->input->post("dias");
        $dataSA["precioGM"] = $this->input->post("precio");
        // $dataSA[""] = $this->input->post("repPagoAlCliente");
        $dataSA["pagoClienteGM"] = $this->input->post("pagoAlCliente");
        // $dataSA[""] = $this->input->post("repGrua");
        $dataSA["gruaGM"] = $this->input->post("grua");
        // $dataSA[""] = $this->input->post("repOtros");
        $dataSA["otrosGM"] = $this->input->post("otros");

        // if ($datos["tipoGuardado"] == "1") {
            $dataSA["firmaCliente"] = $this->base64ToImage($this->input->post("rutaFirmaCliente"),"firmas");   //Firma del cliente
            $dataSA["firmaGerente"] = $this->base64ToImage($this->input->post("rutaFirmaGerente"),"firmas");   //Firma del gerente de servicio
        // }

        //Tabla estatica 2
        $dataSA["idAsesor"] = $this->input->post("idAsesor");
        $dataSA["placas"] = $this->input->post("placas");
        $dataSA["anio"] = $this->input->post("anio");
        $dataSA["tipo"] = $this->input->post("tipo");
        $dataSA["horaRecibo"] = $this->input->post("horaRecibo");
        $dataSA["horaPromedio"] = $this->input->post("horaPromedio");
        $dataSA["telefono"] = $this->input->post("telefono");

        //Tabla estatica 3
        $dataSA["nombreProp"] = $this->input->post("nombreProp");
        $dataSA["clienteVisita"] = $this->input->post("clienteVisita");
        $dataSA["domicilio"] = $this->input->post("direccion");
        $dataSA["estado"] = $this->input->post("estado");
        $dataSA["cp"] = $this->input->post("cPostal");
        $dataSA["idVehiculo"] = $this->multipleInput("idVehiculo_",17);    // del 1 - 17

        //Tabla estatica 5
        $fecha = $this->input->post("diaRecibe")."-".$this->input->post("mesRecibe")."-".$this->input->post("anioRecibe");
        $dataSA["fechaRecibe"] = $fecha;
        $dataSA["kmRecibe"] = $this->multipleInput("kmRecibe_",6);   // del 1 - 6

        $fecha_2 = $this->input->post("diaEntrega")."-".$this->input->post("mesEntrega")."-".$this->input->post("anioEntrega");
        $dataSA["fechaEntrega"] = $fecha_2;
        $dataSA["kmEntrega"] = $this->multipleInput("kmEntrega_",6);   // del 1 - 6

        $fecha_3 = $this->input->post("diaGarantia")."-".$this->input->post("mesGarantia")."-".$this->input->post("anioGarantia");
        $dataSA["fechaGarantia"] = $fecha_3;
        $dataSA["noDistribuidor"] = $this->input->post("noDistribuidor");
        $fecha_4 = $this->input->post("diaReparacion")."-".$this->input->post("mesReparacion")."-".$this->input->post("anioReparacion");
        $dataSA["fechaReparacion"] = $fecha_4;

        //Tabla estatica 6
        $dataSA["oasis"] = $this->input->post("oasis");
        $fecha_5 = $this->input->post("diaVta")."-".$this->input->post("mesVta")."-".$this->input->post("anioVta");
        $dataSA["fechaVta"] = $fecha_5;
        $dataSA["kmsVta"] = $this->input->post("totKms");
        $dataSA["noRefDoc"] = $this->input->post("noRefDoc");

        //Tabla dinamica 1
        $dataSA["contenedor"] = $this->validateTable($this->input->post("contenedor"),"");     //Valores de la tabla (multiple)
        $dataSA["totalPartes"] = $this->input->post("TotalPartes");
        $dataSA["totalObra"] = $this->input->post("TotalObra");
        $dataSA["totalMisc"] = $this->input->post("TotalMisc");
        $dataSA["totalIva"] = $this->input->post("TotalIva");
        $dataSA["totalCliente"] = $this->input->post("TotalCliente");
        $dataSA["totalDistribuidor"] = $this->input->post("TotalDistribuidor");
        $dataSA["totalReparacion"] = $this->input->post("TotalReparacion"); 

        //Tabla dinamica 2
        $dataSA["contenidoPartes"] = $this->validateTable($this->input->post("contenido"),"");    //Información de los registro

        $dataSA["estatus"] = "1";

        $registro = date("Y-m-d H:i:s");
        //Fechas de registro
        if ($datos["tipoGuardado"] == "1") {
            $dataSA["fechaRegistro"] = $registro;
        }
        $dataSA["fechaActualiza"] = $registro;

        //Verificamos si se va a guardar o se va a actualizar
        if ($datos["tipoGuardado"] == 1) {
            $idRegistro = $this->mConsultas->save_register('garantias',$dataSA);
            if ($idRegistro != 0) {
                //Guardamos los historicos del cambio de estatus
                $contenedor_estatus = array(
                    "id" => NULL,
                    "id_cita" => $dataSA["idOrden"],
                    "estatus" => "1",
                    "usuario" => $usuarioReg,
                    "comentario" => "",
                    "fecha_registro" => $registro,
                );

                $registro_estatus = $this->mConsultas->save_register('garantias_historico',$contenedor_estatus);

                $datos["mensaje"] = "1";
                $this->index($datos["idOrden"],$datos);
            }else {
                $datos["mensaje"] = "2";
                $this->index("0",$datos);
            }
        }else {
            $actualizar = $this->mConsultas->update_table_row('garantias',$dataSA,'idOrden',$datos["idOrden"]);
            if ($actualizar) {
                //Guardamos el movimiento
                // $this->loadHistory($datos["idOrden"],"Actualizacion","Cotización");
                $datos["mensaje"]="1";
            }else {
                $datos["mensaje"]="0";
            }
            $this->index($datos["idOrden"],$datos);
        }
    }

    //Validar multiples input
    public function multipleInput($campo = NULL,$longitud = 0)
    {
        if ($campo == NULL) {
            $respuesta = "";
        }else {
            $respuesta = "";
            //Recuperamos los valores
            for ($i=0; $i < $longitud ; $i++) {
                $temp = $this->input->post($campo."".($i+1));
                $respuesta .= $temp."_";
            }
        }
        return $respuesta;
    }

    //Validar multiple check
    public function validateTable($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."|";
                }
            }
        }
        return $respuesta;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }
        }
        return $base;
    }

    //Entrada para el PDF
    public function setDataPDF($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $this->setData($this->decrypt($idOrden),$datos);
    }

    //Enviamos a la vista para cargar formulario sin edicion y firmar
    public function setDataFirmsPart1($idOrden = 0)
    {
        $datos["destinoVista"] = "FIRMAS";
        $this->setData($this->decrypt($idOrden),$datos);
    }

    //Recuperamos los campos de la base de datos
    public function setData($idOrden = 0,$datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"garantias");
        foreach ($query as $row){
            $datos["idRegistro"] = $row->idRegistro;
            $datos["folio"] = $row->folio;
            $datos["idOrden"] = $row->idOrden;
            $datos["folioContinua"] = $row->folioContinua;
            $datos["noTorre"] = $row->noTorre;
            $datos["repGastosMisc"] = explode("_",$row->repGastosMisc);
            $datos["diasGM"] = $row->diasGM;
            $datos["precioGM"] = $row->precioGM;
            $datos["pagoClienteGM"] = $row->pagoClienteGM;
            $datos["gruaGM"] = $row->gruaGM;
            $datos["otrosGM"] = $row->otrosGM;
            $datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : "";
            $datos["firmaGerente"] = (file_exists($row->firmaGerente)) ? $row->firmaGerente : "";
            $datos["idAsesor"] = $row->idAsesor;
            $datos["placas"] = $row->placas;
            $datos["anio"] = $row->anio;
            $datos["tipo"] = $row->tipo;
            $datos["horaRecibo"] = $row->horaRecibo;
            $datos["horaPromedio"] = $row->horaPromedio;
            $datos["telefono"] = $row->telefono;
            $datos["nombreProp"] = $row->nombreProp;
            $datos["clienteVisita"] = $row->clienteVisita;
            $datos["domicilio"] = $row->domicilio;
            $datos["estado"] = $row->estado;
            $datos["cp"] = $row->cp;
            $datos["idVehiculo"] = explode("_",$row->idVehiculo);
            $datos["fechaRecibe"] = explode("-",$row->fechaRecibe);
            $datos["kmRecibe"] = explode("_",$row->kmRecibe);
            $datos["fechaEntrega"] = explode("-",$row->fechaEntrega);
            $datos["kmEntrega"] = explode("_",$row->kmEntrega);
            $datos["fechaGarantia"] = explode("-",$row->fechaGarantia);
            $datos["noDistribuidor"] = $row->noDistribuidor;
            $datos["fechaReparacion"] = explode("-",$row->fechaReparacion);
            $datos["oasis"] = $row->oasis;
            $datos["fechaVta"] = explode("-",$row->fechaVta);
            $datos["kmsVta"] = $row->kmsVta;
            $datos["noRefDoc"] = $row->noRefDoc;

            $datos["contenedorColeccion"] = explode("|",$row->contenedor);
            $datos["contenedor"] = $this->createCells(explode("|",$row->contenedor));

            $datos["totalPartes"] = $row->totalPartes;
            $datos["totalObra"] = $row->totalObra;
            $datos["totalMisc"] = $row->totalMisc;
            $datos["totalIva"] = $row->totalIva;
            $datos["totalCliente"] = $row->totalCliente;
            $datos["totalDistribuidor"] = $row->totalDistribuidor;
            $datos["totalReparacion"] = $row->totalReparacion;

            $datos["firmaTecnico"] = (file_exists($row->firmaTecnico)) ? $row->firmaTecnico : "";
            $datos["nombreTecnico"] = $row->nombreTecnico;
            $datos["firmaProv"] = (file_exists($row->firmaProv)) ? $row->firmaProv : "";
            $datos["nombreProv"] = $row->nombreProv;

            $datos["contenidoPartesColeccion"] = explode("|",$row->contenidoPartes);
            $datos["contenidoPartes"] = $this->createCells(explode("|",$row->contenidoPartes));
        }

        $datos["idOrden"] = $idOrden;

        //Verificamos donde se imprimira la información
        if ($datos["destinoVista"] == "PDF") {
            // $this->generatePDF($datos);
            $this->setDataView2($datos["idOrden"],$datos);
        }elseif ($datos["destinoVista"] == "FIRMAS") {
            $this->loadAllView($datos,'garantias/generales/firmasP1');
        }else {
            $this->loadAllView($datos,'garantias/operaciones_editar');
        }
    }

    //Cargamos los datos de la segunda hoja
    public function setDataView2($idOrden = 0,$datos = NULL)
    {
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"garantia_2");
        foreach ($query as $row){
            $datos["xrep"] = $this->createCells(explode("|",$row->repN));
            $datos["xluzFalla"] = $this->createCells(explode("|",$row->luzFalla));
            $datos["xkoeo"] = $this->createCells(explode("|",$row->koeo));
            $datos["xkoec"] = $this->createCells(explode("|",$row->koec));
            $datos["xkoer"] = $this->createCells(explode("|",$row->koer));
            $datos["xcarroceria"] = $this->createCells(explode("|",$row->carroceria));
            $datos["xchassis"] = $this->createCells(explode("|",$row->chassis));
            $datos["xindefinido"] = $this->createCells(explode("|",$row->indefinido));
            $datos["xotro"] = $this->createCells(explode("|",$row->otro));

            $datos["xcontenidoRenglon"] = explode("|",$row->contenidoLabor);
            $datos["xcontenidoLabor"] = $this->createCells(explode("|",$row->contenidoLabor));

            $datos["firma1"] = (file_exists($row->firma1)) ? $row->firma1 : "";
            $datos["nombre1"] = $row->nombre1;
            $datos["firma2"] = (file_exists($row->firma2)) ? $row->firma2 : "";
            $datos["nombre2"] = $row->nombre2;
        }

        $this->generatePDF($datos);
    }

    //Recuperamos los elementos de la tabla
    public function listData($x = "",$datos = NULL)
    {
        $query = $this->mConsultas->get_table_garantias();
        foreach ($query as $row){
            //Comprobamos que las garantias que se muestren esten firmadas
            $temp = $this->validatePartTwo($row->idOrden);
            if (($row->firmaCliente != "")&&($row->firmaTecnico != "")&&($temp != "3")) {
                $datos["idRegistro"][] = $row->idRegistro;
                $datos["folio"][] = $row->folio;
                $datos["idOrden"][] = $row->idOrden;
                $datos["id_cita_url"][] = $this->encrypt($row->idOrden);

                $datos["noTorre"][] = $row->noTorre;

                $datos["idAsesor"][] = $row->idAsesor;
                $datos["placas"][] = $row->placas;
                $datos["telefono"][] = $row->telefono;
                $datos["nombreProp"][] = $row->nombreProp;

                $datos["fechaRecibe"][] = $row->fechaRecibe;
                $datos["fechaEntrega"][] = $row->fechaEntrega;
                $datos["noDistribuidor"][] = $row->noDistribuidor;

                $datos["estatus"][] = $row->estatus_actual;

                $datos["garantia2"][] = $temp;
            }
        }

        //Recuperamos el usuario que da de alta
        if ($this->session->userdata('rolIniciado')) {
            $datos["usuarioReg"] = $this->session->userdata('nombreUsuario');
        }elseif ($this->session->userdata('id_usuario')) {
            $datos["usuarioReg"] = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $datos["usuarioReg"] = "ADMIN*";
        }

        //Recuperamos los tipo de operacion
        $estatus = $this->mConsultas->get_table("cat_estatus_garantia");

        //Recuperar el listado de estatus
        foreach ($estatus as $row){
            $datos["id_estatus"][] = $row->id;
            $datos["estatusList"][] = $row->estatus;
        }

        $this->loadAllView($datos,'garantias/lista');
    }

    //Validar si existe la segunda parte de las garantias
    public function validatePartTwo($idOrden = 0)
    {
        $respuesta = "0";

        //Verificamos si existe la segunda parte
        $revision = $this->mConsultas->get_result("idOrden",$idOrden,"garantia_2");
        foreach ($revision as $row2) {
            $idGarantia = $row2->idRegistro;
            $firma1 = $row2->firma1;
            $firma2 = $row2->firma2;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idGarantia)) {
            // $respuesta = "2";
            if (($firma1 == "")||($firma2 == "")) {
                $respuesta = "3";
            }else {
                $respuesta = "2";
            }
        } else {
            $respuesta = "1";
        }

        return $respuesta;
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Jalamos datos para el prellenado del formulario
    public function loadDataForm()
    {
        //Recuperamos el id de la orden
        $idOrden = $_POST['idOrden'];
        $cadena = "";

        //Consultamos si existe alguna garantia con ese numero de orden
        $revision = $this->mConsultas->get_result("idOrden",$idOrden,"garantias");
        foreach ($revision as $row) {
            $idGarantia = $row->idRegistro;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idGarantia)) {
            $cadena .= "EXISTE_";
        //Si no existe una garantia para esa orden, cargamos los datos
        } else {
            $cadena .= "NO EXISTE_";
            $precarga = $this->mConsultas->precargaConsulta($idOrden);
            foreach ($precarga as $row){
                $cadena .= $row->asesor."_";
                $cadena .= $row->vehiculo_placas."_";
                $cadena .= $row->vehiculo_anio."_";
                //campo "tipo"
                $cadena .= $row->categoria."_";
                //---
                $cadena .= $row->telefono_asesor."_";
                //--
                $cadena .= $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno."_";
                $cadena .= "_";
                //--
                $cadena .= $row->calle." ".$row->nointerior." #".$row->noexterior.", ".$row->colonia.", ".$row->municipio."_";
                $cadena .= $row->estado."_";
                $cadena .= $row->cp."_";
                //----
                $cadena .= (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion)."_";
                //----
                $cadena .= $row->numero_interno."-".$row->color."_";
                //----
                $cadena .= $row->vehiculo_kilometraje."_";
                //----
                $cadena .= $row->tecnico."_";
            }
        }
        echo $cadena;
    }

    //Recuperamos la actualizacion de estatus
    public function actulizaEstatus()
    {
        $estatus  = $_POST["estatus"];
        $id_cita  = $_POST["id_cita"];
        $usuario  = $_POST["usuario"];
        $comentario  = $_POST["comentario"];

        $registro = date("Y-m-d H:i:s");
        $respuesta = "";

        //Actualizamos el estatus en la garantia
        $garantia = array(
            "estatus" => $estatus,
            "fechaActualiza" => $registro,
        );

        $actualizar = $this->mConsultas->update_table_row('garantias',$garantia,'idOrden',$id_cita);
        if ($actualizar) {
            //Guardamos los historicos del cambio de estatus
            $contenedor_estatus = array(
                "id" => NULL,
                "id_cita" => $id_cita,
                "estatus" => $estatus,
                "usuario" => $usuario,
                "comentario" => $comentario,
                "fecha_registro" => $registro,
            );

            $registro_estatus = $this->mConsultas->save_register('garantias_historico',$contenedor_estatus);

            $respuesta = "OK";
        }else {
            $respuesta = "ERROR AL GUARDAR EL REGISTRO";
        }
        echo $respuesta;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("garantias/plantillaPdf2", $datos);
    }

    function encrypt($data){
        /*$id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;*/
        return $data;
    }

    function decrypt($data){
        /*$url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;*/
        return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/garantias/autollenado.js',
                'assets/js/garantias/firmas_SA.js',
                'assets/js/garantias/tablas.js',
                'assets/js/garantias/autosumas.js',
                'assets/js/garantias/autoGuardadoT1.js',
                'assets/js/garantias/autoGuardadoT2.js',
                'assets/js/superUsuario.js',
                'assets/js/garantias/cambio_estatus.js',
            )
        );

        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Garantía"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
