<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Operaciones2 extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

  	public function index($idOrden = 0,$datos = NULL)
  	{
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");

        $id_cita = (($idOrden == "0") ? "0" : $this->decrypt($idOrden));

        //Identificamos si la orden se servicio ya ha generado alguna cotizacion
        $revision = $this->mConsultas->get_result("idOrden",$id_cita,"garantia_2");
        foreach ($revision as $row) {
            $idGarantia = $row->idRegistro;
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idGarantia)) {
            //Verificamos si la cotización ya fue aceptada/rechazada por el cliente
            $this->setData($id_cita,$datos);
        } else {
            $datos['idOrden'] = $id_cita;
            $this->loadAllView($datos,'garantias/F1863/alta');
        }
  	}

    //Validamos los campos de la vista
    public function validateForm()
    {
        $datos["tipoGuardado"] = $this->input->post("modoVistaFormulario");
        $datos["idOrden"] = $this->input->post("idOrden");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idOrden"]);

        if ($datos["tipoGuardado"] == 1) {
            $this->form_validation->set_rules('idOrden', 'El No. de Orden es requerido.', 'required|callback_existencia');
            $this->form_validation->set_rules('contenedor', '', 'callback_registros');

            //Valiamos firmas
            // $this->form_validation->set_rules('rutaFirma1', 'La firma es requerida.', 'required');
            $this->form_validation->set_rules('nombre1', 'Debe ingresar el nombre de quien firma.', 'required');
            // $this->form_validation->set_rules('rutaFirma2', 'La firma es requerida.', 'required');
            $this->form_validation->set_rules('nombre2', 'Debe ingresar el nombre de quien firma.', 'required');
        }else {
            $this->form_validation->set_rules('idOrden', 'El Folio es requerido.', 'required');
            $this->form_validation->set_rules('contenedor', '', 'callback_registros');
        }

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('registros','No se ha guardado ningun elemento');
        $this->form_validation->set_message('existencia','Este No. de Orden ya esta registrado. Favor de verificar.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getData($datos);
        }else{
            $datos["mensaje"]="2";
            $this->index('0',$datos);
        }
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idOrden");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"garantia_2");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Validar requisitos minimos P1
    public function registros()
    {
        $return = FALSE;
        if ($this->input->post("contenedor")) {
            if (count($this->input->post("contenedor")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Recuperamos los datos del formulario
    public function getData($datos = NULL)
    {
        $registro = date("Y-m-d H:i");
        //Creamos el array para guardado
        if ($datos["tipoGuardado"] == "1") {
            $dataGarant = array(
                'idRegistro' => NULL,
                'idOrden' => $this->input->post("idOrden"),
            );
        }

        //Primera tabla lado izquierdo
        $rep_1 = $this->multipleInput("rep_1_",6);
        $rep_2 = $this->multipleInput("rep_2_",6);
        $dataGarant["repN"] = $rep_1."|".$rep_2;
        $dataGarant["luzFalla"] = $this->multipleInput("luz_falla_",6);

        //Primera tabla lado derecho
        $dataGarant["koeo"] = $this->multipleInput("koeo_",36);
        $dataGarant["koec"] = $this->multipleInput("koec_",36);
        $dataGarant["koer"] = $this->multipleInput("koer_",36);
        $dataGarant["carroceria"] = $this->multipleInput("carroceria_",36);
        $dataGarant["chassis"] = $this->multipleInput("chassis_",36);
        $dataGarant["indefinido"] = $this->multipleInput("indefinido_",36);
        $dataGarant["otro"] = $this->multipleInput("otro_",36);


        //Tabla dinamica
        $dataGarant["contenidoLabor"] = $this->validateTable($this->input->post("contenedor"),"");

        //Verificamos si se guardara la imagen
        $firma_1 = $this->input->post("rutaFirma1");
        if (substr($firma_1, 0, 22) == "data:image/png;base64,") {
            $dataGarant["firma1"] = $this->base64ToImage($firma_1,"firmas");
        }else {
            $dataGarant["firma1"] = $firma_1;
        }
        $dataGarant["nombre1"] = $this->input->post("nombre1");

        $firma_2 = $this->input->post("rutaFirma2");
        if (substr($firma_2, 0, 22) == "data:image/png;base64,") {
            $dataGarant["firma2"] = $this->base64ToImage($firma_2,"firmas");
        }else {
            $dataGarant["firma2"] = $firma_2;
        }
        $dataGarant["nombre2"] = $this->input->post("nombre2");

        if ($datos["tipoGuardado"] == "1") {
            $dataGarant["fechaRegistro"] = $registro;
        }
        $dataGarant["fechaActualizacion"] = $registro;

        //Verificamos si se va a guardar o se va a actualizar
        if ($datos["tipoGuardado"] == 1) {
            $idRegistro = $this->mConsultas->save_register('garantia_2',$dataGarant);
            if ($idRegistro != 0) {
                $datos["mensaje"] = "1";
                $this->index($datos["idOrden"],$datos);
                //Regresamos al listado

            }else {
                $datos["mensaje"] = "2";
                $this->index("0",$datos);
            }
        }else {
            $actualizar = $this->mConsultas->update_table_row('garantia_2',$dataGarant,'idOrden',$datos["idOrden"]);
            if ($actualizar) {
                //Guardamos el movimiento
                // $this->loadHistory($datos["idOrden"],"Actualizacion","Cotización");
                $datos["mensaje"]="1";
            }else {
                $datos["mensaje"]="0";
            }
            $this->index($datos["idOrden"],$datos);
        }
    }

    //Validar multiples input
    public function multipleInput($campo = NULL,$longitud = 0)
    {
        if ($campo == NULL) {
            $respuesta = "";
        }else {
            $respuesta = "";
            //Recuperamos los valores
            for ($i=0; $i < $longitud ; $i++) {
                $temp = $this->input->post($campo."".($i+1));
                $respuesta .= $temp."_";
            }
        }
        return $respuesta;
    }

    //Validar multiple check
    public function validateTable($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."|";
                }
            }
        }
        return $respuesta;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }
        }
        return $base;
    }

    //Entrada para el PDF
    public function setDataPDF($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $this->setData($this->decrypt($idOrden),$datos);
    }

    //Recuperamos la información del registro
    public function setData($idOrden = 0, $datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"garantia_2");
        foreach ($query as $row){
            $datos["xrep"] = $this->createCells(explode("|",$row->repN));
            $datos["xluzFalla"] = $this->createCells(explode("|",$row->luzFalla));
            $datos["xkoeo"] = $this->createCells(explode("|",$row->koeo));
            $datos["xkoec"] = $this->createCells(explode("|",$row->koec));
            $datos["xkoer"] = $this->createCells(explode("|",$row->koer));
            $datos["xcarroceria"] = $this->createCells(explode("|",$row->carroceria));
            $datos["xchassis"] = $this->createCells(explode("|",$row->chassis));
            $datos["xindefinido"] = $this->createCells(explode("|",$row->indefinido));
            $datos["xotro"] = $this->createCells(explode("|",$row->otro));

            $datos["xcontenidoRenglon"] = explode("|",$row->contenidoLabor);
            $datos["xcontenidoLabor"] = $this->createCells(explode("|",$row->contenidoLabor));

            $datos["firma1"] = (file_exists($row->firma1)) ? $row->firma1 : "";
            $datos["nombre1"] = $row->nombre1;
            $datos["firma2"] = (file_exists($row->firma2)) ? $row->firma2 : "";
            $datos["nombre2"] = $row->nombre2;
        }

        $datos["idOrden"] = $idOrden;

        //Verificamos donde se imprimira la información
        if ($datos["destinoVista"] == "PDF") {
            $this->generatePDF($datos);
        }else {
            $this->loadAllView($datos,'garantias/F1863/editar');
        }
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("garantias/F1863/plantillaPdf", $datos);
    }

    function encrypt($data){
        /*$id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;*/
        return $data;
    }

    function decrypt($data){
        /*$url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;*/
        return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/garantias/firmas_G2.js',
                'assets/js/garantias/tablaF1.js',
                'assets/js/garantias/autoGuardadoT3F1.js',
                'assets/js/superUsuario.js'
            )
        );

        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Garantía F1863"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
