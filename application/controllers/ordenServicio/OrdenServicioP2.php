<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

// Control de materiales
class OrdenServicioP2 extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($idOrden = 0,$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["tipoRegistro"] = "Asesor";
        $datos["idOrden"] = $idOrden;

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        $this->loadAllView($datos,'ordenServicio2/ordenServico_alta');
    }

    //Validamos los campos
    public function validateForm()
    {
        $datos["idOrden"] = $this->input->post("idOrden");
        $datos["tipoGuardado"] = $this->input->post("cargaFormulario");
        $datos["tipoRegistro"] = $this->input->post("tipoRegistro");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idOrden"]);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        if ($datos["tipoGuardado"] == 1) {
            $this->form_validation->set_rules('idOrden', 'El Número de orden es requerido.', 'required|callback_existencia|callback_ordenPrevia');
        }else {
            $this->form_validation->set_rules('idOrden', 'El Número de orden es requerido.', 'required');
        }

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar.');
        $this->form_validation->set_message('ordenPrevia','No existe una orden de servicio previa con este No. de Orden.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="2";
            if ($this->session->userdata('rolIniciado')) {
                // $campos = $this->session->userdata('dataUser');
                $datos["pOrigen"] = $this->session->userdata('rolIniciado');
            }
            $this->index('0',$datos);
        }
    }

    //Validamos si existe un una orden de servicio previa
    public function ordenPrevia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idOrden");

        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("id_cita",$idOrden,"ordenservicio");
        foreach ($query as $row){
            $validar = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($validar)) {
            //Si existe el dato, entonces ya existe la orden se servicio
            $respuesta = TRUE;
        }else {
            //Si no existe el dato, entonces no existe la orden de servicio
            $respuesta = FALSE;
        }
        return $respuesta;
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idOrden");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"control_material_os");
        foreach ($query as $row){
            $dato = $row->idMaterial;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Recuperamos los campos del formulario
    public function getDataForm($datos = NULL)
    {
        $registro =  date("Y-m-d H:i");

        if ($datos["tipoGuardado"] == "1") {
            $dataControl = array(
                "idMaterial" => NULL,
                "idOrden" => $this->input->post("idOrden"),
            );
        }

        //Registros tabla 1
        $dataControl["registrosControl"] = $this->validateCheck($this->input->post("registros"),"");
        $dataControl["subTotalMaterial"] = $this->input->post("subTotalMaterial");
        $dataControl["ivaMaterial"] = $this->input->post("ivaMaterial");
        $dataControl["totalMaterial"] = $this->input->post("totalMaterial");
        //Verificamos si se guardara la imagen
        $firma_1 = $this->input->post("rutaFirmaAsesorCostos");
        if (substr($firma_1, 0, 22) == "data:image/png;base64,") {
            $dataControl["firmaAsesor"] = $this->base64ToImage($firma_1,"firmas");
        }else {
            $dataControl["firmaAsesor"] = $firma_1;
        }
        $dataControl["nombreAsesor"] = "";

        //Registros tabla 2
        $dataControl["diaProg"] = $this->validateProgram($this->input->post("prog_"));
        $dataControl["diaReal"] = $this->validateProgram($this->input->post("real_"));

        //Registros tabla 3
        $dataControl["fech_recepMeca"] = $this->input->post("recepMeca");
        $dataControl["fech_mecaOpera"] = $this->input->post("fechaMecaOperario");

        //Verificamos si se guardara la imagen
        $firma_1 = $this->input->post("rutaMecaOperario");
        if (substr($firma_1, 0, 22) == "data:image/png;base64,") {
          $dataControl["firma_mecaOpera"] = $this->base64ToImage($firma_1,"firmas");
        }else {
          $dataControl["firma_mecaOpera"] = $firma_1;
        }

        $dataControl["fech_mecaLider"] = $this->input->post("fechamMecaLider");
        //Verificamos si se guardara la imagen
        $firma_2 = $this->input->post("rutaMecaLider");
        if (substr($firma_2, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_mecaLider"] = $this->base64ToImage($firma_2,"firmas");
        }else {
            $dataControl["firma_mecaLider"] = $firma_2;
        }

        $dataControl["fech_mecaCoord"] = $this->input->post("fechaMecaCoord");
        //Verificamos si se guardara la imagen
        $firma_3 = $this->input->post("rutaMecaCoord");
        if (substr($firma_3, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_mecaCoord"] = $this->base64ToImage($firma_3,"firmas");
        }else {
            $dataControl["firma_mecaCoord"] = $firma_3;
        }

        $dataControl["fech_recepLami"] = $this->input->post("recepLami");
        $dataControl["fech_lamiOpera"] = $this->input->post("fechaLamiOperario");
        //Verificamos si se guardara la imagen
        $firma_4 = $this->input->post("rutaLamiOperario");
        if (substr($firma_4, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_lamiOpera"] = $this->base64ToImage($firma_4,"firmas");
        }else {
            $dataControl["firma_lamiOpera"] = $firma_4;
        }

        $dataControl["fech_lamiLider"] = $this->input->post("fechaLamiLider");
        //Verificamos si se guardara la imagen
        $firma_5 = $this->input->post("rutaLamiLider");
        if (substr($firma_5, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_lamiLider"] = $this->base64ToImage($firma_5,"firmas");
        }else {
            $dataControl["firma_lamiLider"] = $firma_5;
        }

        $dataControl["fech_lamiCoord"] = $this->input->post("fechaLamiCoord");
        //Verificamos si se guardara la imagen
        $firma_6 = $this->input->post("rutaLamiCoord");
        if (substr($firma_6, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_lamiCoord"] = $this->base64ToImage($firma_6,"firmas");
        }else {
            $dataControl["firma_lamiCoord"] = $firma_6;
        }

        $dataControl["fech_recepPrep"] = $this->input->post("recepPrep");
        $dataControl["fech_prepOpera"] = $this->input->post("fechaPrepOperario");
        //Verificamos si se guardara la imagen
        $firma_7 = $this->input->post("rutaPrepOperario");
        if (substr($firma_7, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_prepOpera"] = $this->base64ToImage($firma_7,"firmas");
        }else {
            $dataControl["firma_prepOpera"] = $firma_7;
        }

        $dataControl["fech_prepLider"] = $this->input->post("fechaPrepLider");
        //Verificamos si se guardara la imagen
        $firma_8 = $this->input->post("rutaPrepLider");
        if (substr($firma_8, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_prepLider"] = $this->base64ToImage($firma_8,"firmas");
        }else {
            $dataControl["firma_prepLider"] = $firma_8;
        }

        $dataControl["fech_prepCoord"] = $this->input->post("fechaPrepCoord");
        //Verificamos si se guardara la imagen
        $firma_9 = $this->input->post("rutaPrepCoord");
        if (substr($firma_9, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_prepCoord"] = $this->base64ToImage($firma_9,"firmas");
        }else {
            $dataControl["firma_prepCoord"] = $firma_9;
        }

        $dataControl["fech_recepPint"] = $this->input->post("recepPint");
        $dataControl["fech_pintOpera"] = $this->input->post("fechaPintOperario");
        //Verificamos si se guardara la imagen
        $firma_10 = $this->input->post("rutaPintOperario");
        if (substr($firma_10, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_pintOpera"] = $this->base64ToImage($firma_10,"firmas");
        }else {
            $dataControl["firma_pintOpera"] = $firma_10;
        }

        $dataControl["fech_pintLider"] = $this->input->post("fechaPintLider");
        //Verificamos si se guardara la imagen
        $firma_11 = $this->input->post("rutaPintLider");
        if (substr($firma_11, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_pintLider"] = $this->base64ToImage($firma_11,"firmas");
        }else {
            $dataControl["firma_pintLider"] = $firma_11;
        }

        $dataControl["fech_pintCoord"] = $this->input->post("fechaPintCoord");
        //Verificamos si se guardara la imagen
        $firma_12 = $this->input->post("rutaPintCoord");
        if (substr($firma_12, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_pintCoord"] = $this->base64ToImage($firma_12,"firmas");
        }else {
            $dataControl["firma_pintCoord"] = $firma_12;
        }

        $dataControl["fech_recepArma"] = $this->input->post("recepArma");
        $dataControl["fech_armaOpera"] = $this->input->post("fechaArmaOperario");
        //Verificamos si se guardara la imagen
        $firma_13 = $this->input->post("rutaArmaOperario");
        if (substr($firma_13, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_armaOpera"] = $this->base64ToImage($firma_13,"firmas");
        }else {
            $dataControl["firma_armaOpera"] = $firma_13;
        }

        $dataControl["fech_armaLider"] = $this->input->post("fechaArmaLider");
        //Verificamos si se guardara la imagen
        $firma_14 = $this->input->post("rutaArmaLider");
        if (substr($firma_14, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_armaLider"] = $this->base64ToImage($firma_14,"firmas");
        }else {
            $dataControl["firma_armaLider"] = $firma_14;
        }

        $dataControl["fech_armaCoord"] = $this->input->post("fechaArmaCoord");
        //Verificamos si se guardara la imagen
        $firma_15 = $this->input->post("rutaArmaCoord");
        if (substr($firma_15, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_armaCoord"] = $this->base64ToImage($firma_15,"firmas");
        }else {
            $dataControl["firma_armaCoord"] = $firma_15;
        }

        $dataControl["fech_recepDetall"] = $this->input->post("recepDetall");
        $dataControl["fech_detallOpera"] = $this->input->post("fechaDetallOperario");
        //Verificamos si se guardara la imagen
        $firma_16 = $this->input->post("rutaDetallOperario");
        if (substr($firma_16, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_detallOpera"] = $this->base64ToImage($firma_16,"firmas");
        }else {
            $dataControl["firma_detallOpera"] = $firma_16;
        }

        $dataControl["fech_detallLider"] = $this->input->post("fechaDetallLider");
        //Verificamos si se guardara la imagen
        $firma_17 = $this->input->post("rutaDetallLider");
        if (substr($firma_17, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_detallLider"] = $this->base64ToImage($firma_17,"firmas");
        }else {
            $dataControl["firma_detallLider"] = $firma_17;
        }

        $dataControl["fech_detallCoord"] = $this->input->post("fechaDetallCoord");
        //Verificamos si se guardara la imagen
        $firma_18 = $this->input->post("rutaDetallCoord");
        if (substr($firma_18, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_detallCoord"] = $this->base64ToImage($firma_18,"firmas");
        }else {
            $dataControl["firma_detallCoord"] = $firma_18;
        }

        $dataControl["fech_calidCoord"] = $this->input->post("fechaCaliCoord");
        //Verificamos si se guardara la imagen
        $firma_19 = $this->input->post("rutaCaliCoord");
        if (substr($firma_19, 0, 22) == "data:image/png;base64,") {
            $dataControl["firma_calidCoord"] = $this->base64ToImage($firma_19,"firmas");
        }else {
            $dataControl["firma_calidCoord"] = $firma_19;
        }

        //Registros tabla 3
        $dataControl["costos"] = $this->validateCheck($this->input->post("costos"),"");
        $dataControl["totalesGasolina"] = $this->input->post("totalesGasolina");

        //Registros tabla 4
        $dataControl["venta_taller"] = $this->validateCheck($this->input->post("venta_taller"),"");
        $dataControl["totalTaller"] = $this->input->post("totalesTaller");

        if ($datos["tipoGuardado"] == "1") {
            $dataControl["registro"] = $registro;
        }

        // var_dump($dataControl);
        if ($datos["tipoGuardado"] == "1") {
            $control = $this->mConsultas->save_register('control_material_os',$dataControl);
            if ($control != 0) {
                $this->loadHistory($datos["idOrden"],"Registro","Control de Materiales");

                $datos["mensaje"] = "1";
                redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
            }else {
                $datos["mensaje"] = "2";
                $this->index("0",$datos);
            }

        } else {
            //Recuperamos el id del formulario
            $idMaterial = $this->input->post("idMaterial");
            $actualizar = $this->mConsultas->update_table_row('control_material_os',$dataControl,'idMaterial',$idMaterial);
            if ($actualizar) {
                $this->loadHistory($datos["idOrden"],"Actualización","Control de Materiales");

                $datos["mensaje"]="1";
                $this->setDataControl($datos["idOrden"],$datos);
            }else {
                $datos["mensaje"]="2";
                $this->setDataControl($datos["idOrden"],$datos);
            }

        }
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Validamos la tabla de programacion
    public function validateProgram($campo = NULL)
    {
        $retorno = "";

        if ($campo != NULL) {
            for ($i=0; $i < count($campo) ; $i++) {
                $retorno .= $campo[$i]."|";
            }
        }

        return $retorno;
    }

    //Validar multiple check
    public function validateCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >1){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."|";
                }
            }
        }
        return $respuesta;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        $urlNombre = date("YmdHi")."_";

        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        // $urlNombre = "";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        //Creamos la ruta donde se guardara la firma y su extensión
        if (strlen($imgBase64)>1) {
            $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
            $data = explode(',', $imgBase64);
            $Base64Img = base64_decode($data[1]);
            file_put_contents($base, $Base64Img);
        }else {
            $base = "";
        }

        return $base;
    }

    //Comprobamos que al final se genere el pdf
    public function setDataPDFControl($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $this->setDataControl($idOrden,$datos);
    }

    //Recuperamos la información para editar
    public function setDataControl($idOrden = 0,$datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        $datos["hoy"] = date("Y-m-d");
        $datos["idOrden"] = $idOrden;
        //Realizamos la consulta
        $query = $this->mConsultas->get_result("idOrden",$datos["idOrden"],"control_material_os");
        foreach ($query as $row) {
            $datos["idMaterial"] = $row->idMaterial;

            //Registros tabla 1
            $registro = explode("|",$row->registrosControl);
            $datos["registrosControl"] = $this->divideFields($registro);
            $datos["subTotalMaterial"] = $row->subTotalMaterial;
            $datos["ivaMaterial"] = $row->ivaMaterial;
            $datos["totalMaterial"] = $row->totalMaterial;
            $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : ""; //$row->firmaAsesor;
            $datos["nombreAsesor"] = $row->nombreAsesor;

            //Registros tabla 2
            $datos["diaProg"] = explode("|",$row->diaProg);
            $datos["diaReal"] = explode("|",$row->diaReal);

            //Registros tabla 3
            //Firmas de la sección
            $datos["firma_mecaOpera"] = (file_exists($row->firma_mecaOpera)) ? $row->firma_mecaOpera : "";
            $datos["firma_mecaLider"] = (file_exists($row->firma_mecaLider)) ? $row->firma_mecaLider : "";
            $datos["firma_mecaCoord"] = (file_exists($row->firma_mecaCoord)) ? $row->firma_mecaCoord : "";
            //Validamos si se podra editar la fecha
            if (($datos["firma_mecaOpera"] != "")||($datos["firma_mecaLider"] != "")||($datos["firma_mecaCoord"] != "")) {
                $datos["fech_recepMeca"] = $this->resetFormatDate($row->fech_recepMeca,"LABEL");
                $datos["fech_recepMecaInput"] = $this->resetFormatDate($row->fech_recepMeca,"");
            }else {
                $datos["fech_recepMeca"] = $this->resetFormatDate($row->fech_recepMeca,"");
                $datos["fech_recepMecaInput"] = $this->resetFormatDate($row->fech_recepMeca,"");
            }
            $datos["fech_mecaOpera"] = $this->resetFormatDate($row->fech_mecaOpera,$datos["firma_mecaOpera"]);
            $datos["fech_mecaOperaInput"] = $this->resetFormatDate($row->fech_mecaOpera,"");
            $datos["fech_mecaLider"] = $this->resetFormatDate($row->fech_mecaLider,$datos["firma_mecaLider"]);
            $datos["fech_mecaLiderInput"] = $this->resetFormatDate($row->fech_mecaLider,"");
            $datos["fech_mecaCoord"] = $this->resetFormatDate($row->fech_mecaCoord,$datos["firma_mecaCoord"]);
            $datos["fech_mecaCoordInput"] = $this->resetFormatDate($row->fech_mecaCoord,"");

            //Firmas de la sección
            $datos["firma_lamiOpera"] = (file_exists($row->firma_lamiOpera)) ? $row->firma_lamiOpera : "";
            $datos["firma_lamiLider"] = (file_exists($row->firma_lamiLider)) ? $row->firma_lamiLider : "";
            $datos["firma_lamiCoord"] = (file_exists($row->firma_lamiCoord)) ? $row->firma_lamiCoord : "";
            //Validamos si se podra editar la fecha
            if (($datos["firma_lamiOpera"] != "")||($datos["firma_lamiLider"] != "")||($datos["firma_lamiCoord"] != "")) {
                $datos["fech_recepLami"] = $this->resetFormatDate($row->fech_recepLami,"LABEL");
                $datos["fech_recepLamiInput"] = $this->resetFormatDate($row->fech_recepLami,"");
            }else {
                $datos["fech_recepLami"] = $this->resetFormatDate($row->fech_recepLami,"");
                $datos["fech_recepLamiInput"] = $this->resetFormatDate($row->fech_recepLami,"");
            }
            $datos["fech_lamiOpera"] = $this->resetFormatDate($row->fech_lamiOpera,$datos["firma_lamiOpera"]);
            $datos["fech_lamiOperaInput"] = $this->resetFormatDate($row->fech_lamiOpera,"");
            $datos["fech_lamiLider"] = $this->resetFormatDate($row->fech_lamiLider,$datos["firma_lamiLider"]);
            $datos["fech_lamiLiderInput"] = $this->resetFormatDate($row->fech_lamiLider,"");
            $datos["fech_lamiCoord"] = $this->resetFormatDate($row->fech_lamiCoord,$datos["firma_lamiCoord"]);
            $datos["fech_lamiCoordInput"] = $this->resetFormatDate($row->fech_lamiCoord,"");

            //Firmas de la sección
            $datos["firma_prepOpera"] = (file_exists($row->firma_prepOpera)) ? $row->firma_prepOpera : "";
            $datos["firma_prepLider"] = (file_exists($row->firma_prepLider)) ? $row->firma_prepLider : "";
            $datos["firma_prepCoord"] = (file_exists($row->firma_prepCoord)) ? $row->firma_prepCoord : "";
            //Validamos si se podra editar la fecha
            if (($datos["firma_prepOpera"] != "")||($datos["firma_prepLider"] != "")||($datos["firma_prepCoord"] != "")) {
                $datos["fech_recepPrep"] = $this->resetFormatDate($row->fech_recepPrep,"LABEL");
                $datos["fech_recepPrepInput"] = $this->resetFormatDate($row->fech_recepPrep,"");
            }else {
                $datos["fech_recepPrep"] = $this->resetFormatDate($row->fech_recepPrep,"");
                $datos["fech_recepPrepInput"] = $this->resetFormatDate($row->fech_recepPrep,"");
            }
            $datos["fech_prepOpera"] = $this->resetFormatDate($row->fech_prepOpera,$datos["firma_prepOpera"]);
            $datos["fech_prepOperaInput"] = $this->resetFormatDate($row->fech_prepOpera,"");
            $datos["fech_prepLider"] = $this->resetFormatDate($row->fech_prepLider,$datos["firma_prepLider"]);
            $datos["fech_prepLiderInput"] = $this->resetFormatDate($row->fech_prepLider,"");
            $datos["fech_prepCoord"] = $this->resetFormatDate($row->fech_prepCoord,$datos["firma_prepCoord"]);
            $datos["fech_prepCoordInput"] = $this->resetFormatDate($row->fech_prepCoord,"");

            //Firmas de la sección
            $datos["firma_pintOpera"] = (file_exists($row->firma_pintOpera)) ? $row->firma_pintOpera : "";
            $datos["firma_pintLider"] = (file_exists($row->firma_pintLider)) ? $row->firma_pintLider : "";
            $datos["firma_pintCoord"] = (file_exists($row->firma_pintCoord)) ? $row->firma_pintCoord : "";
            //Validamos si se podra editar la fecha
            if (($datos["firma_pintOpera"] != "")||($datos["firma_pintLider"] != "")||($datos["firma_pintCoord"] != "")) {
                $datos["fech_recepPint"] = $this->resetFormatDate($row->fech_recepPint,"LABEL");
                $datos["fech_recepPintInput"] = $this->resetFormatDate($row->fech_recepPint,"");
            }else {
                $datos["fech_recepPint"] = $this->resetFormatDate($row->fech_recepPint,"");
                $datos["fech_recepPintInput"] = $this->resetFormatDate($row->fech_recepPint,"");
            }
            $datos["fech_pintOpera"] = $this->resetFormatDate($row->fech_pintOpera,$datos["firma_pintOpera"]);
            $datos["fech_pintOperaInput"] = $this->resetFormatDate($row->fech_pintOpera,"");
            $datos["fech_pintLider"] = $this->resetFormatDate($row->fech_pintLider,$datos["firma_pintLider"]);
            $datos["fech_pintLiderInput"] = $this->resetFormatDate($row->fech_pintLider,"");
            $datos["fech_pintCoord"] = $this->resetFormatDate($row->fech_pintCoord,$datos["firma_pintCoord"]);
            $datos["fech_pintCoordInput"] = $this->resetFormatDate($row->fech_pintCoord,"");

            //Firmas de la sección
            $datos["firma_armaOpera"] = (file_exists($row->firma_armaOpera)) ? $row->firma_armaOpera : "";
            $datos["firma_armaLider"] = (file_exists($row->firma_armaLider)) ? $row->firma_armaLider : "";
            $datos["firma_armaCoord"] = (file_exists($row->firma_armaCoord)) ? $row->firma_armaCoord : "";
            //Validamos si se podra editar la fecha
            if (($datos["firma_armaOpera"] != "")||($datos["firma_armaLider"] != "")||($datos["firma_armaCoord"] != "")) {
                $datos["fech_recepArma"] = $this->resetFormatDate($row->fech_recepArma,"LABEL");
                $datos["fech_recepArmaInput"] = $this->resetFormatDate($row->fech_recepArma,"");
            }else {
                $datos["fech_recepArma"] = $this->resetFormatDate($row->fech_recepArma,"");
                $datos["fech_recepArmaInput"] = $this->resetFormatDate($row->fech_recepArma,"");
            }
            $datos["fech_armaOpera"] = $this->resetFormatDate($row->fech_armaOpera,$datos["firma_armaOpera"]);
            $datos["fech_armaOperaInput"] = $this->resetFormatDate($row->fech_armaOpera,"");
            $datos["fech_armaLider"] = $this->resetFormatDate($row->fech_armaLider,$datos["firma_armaLider"]);
            $datos["fech_armaLiderInput"] = $this->resetFormatDate($row->fech_armaLider,"");
            $datos["fech_armaCoord"] = $this->resetFormatDate($row->fech_armaCoord,$datos["firma_armaCoord"]);
            $datos["fech_armaCoordInput"] = $this->resetFormatDate($row->fech_armaCoord,"");

            //Firmas de la sección
            $datos["firma_detallOpera"] = (file_exists($row->firma_detallOpera)) ? $row->firma_detallOpera : "";
            $datos["firma_detallLider"] = (file_exists($row->firma_detallLider)) ? $row->firma_detallLider : "";
            $datos["firma_detallCoord"] = (file_exists($row->firma_detallCoord)) ? $row->firma_detallCoord : "";
            //Validamos si se podra editar la fecha
            if (($datos["firma_detallOpera"] != "")||($datos["firma_detallLider"] != "")||($datos["firma_detallCoord"] != "")) {
                $datos["fech_recepDetall"] = $this->resetFormatDate($row->fech_recepDetall,"LABEL");
                $datos["fech_recepDetallInput"] = $this->resetFormatDate($row->fech_recepDetall,"");
            }else {
                $datos["fech_recepDetall"] = $this->resetFormatDate($row->fech_recepDetall,"");
                $datos["fech_recepDetallInput"] = $this->resetFormatDate($row->fech_recepDetall,"");
            }
            $datos["fech_detallOpera"] = $this->resetFormatDate($row->fech_detallOpera,$datos["firma_detallOpera"]);
            $datos["fech_detallOperaInput"] = $this->resetFormatDate($row->fech_detallOpera,"");
            $datos["fech_detallLider"] = $this->resetFormatDate($row->fech_detallLider,$datos["firma_detallLider"]);
            $datos["fech_detallLiderInput"] = $this->resetFormatDate($row->fech_detallLider,"");
            $datos["fech_detallCoord"] = $this->resetFormatDate($row->fech_detallCoord,$datos["firma_detallCoord"]);
            $datos["fech_detallCoordInput"] = $this->resetFormatDate($row->fech_detallCoord,"");

            $datos["firma_calidCoord"] = $row->firma_calidCoord;
            $datos["fech_calidCoord"] = $this->resetFormatDate($row->fech_calidCoord,$datos["firma_calidCoord"]);
            $datos["fech_calidCoordInput"] = $this->resetFormatDate($row->fech_calidCoord,"");

            //Registros tabla 3
            $registro_2 = explode("|",$row->costos);
            $datos["costos"] = $this->divideFields($registro_2);
            $datos["totalesGasolina"] = $row->totalesGasolina;
            //Registros tabla 4
            $registro_3 = explode("|",$row->venta_taller);
            $datos["venta_taller"] = $this->divideFields($registro_3);
            $datos["totalTaller"] = $row->totalTaller;
        }


        //Verificamos donde se imprimira la información
        if ($datos["destinoVista"] == "PDF") {
            $this->generatePDF($datos);
        } else {
            $this->loadAllView($datos,'ordenServicio2/ordenServico_edicion');
        }
    }

    //Separar los campos de las tablas
    public function divideFields($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Acomodamos el formato de la fecha
    public function resetFormatDate($fecha = "",$validador = "")
    {
        $respuesta = "";
        if ($validador == "") {
            $fecha = explode(" ",$fecha);
            $respuesta = $fecha[0];
        }else {
            $fecha = explode(" ",$fecha);
            $fechaFracc = explode("-",$fecha[0]);
            $respuesta = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
        }
        return $respuesta;
    }

    //Validacion de campos para alta/Edicion (ajax)
    public function searchOrden()
    {
        $dato = $_POST['orden'];
        $cadena = "";
        $query = $this->mConsultas->get_result("idOrden",$dato,"control_material_os");
        foreach ($query as $row){
            $validar = $row->idMaterial;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($validar)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "EDITAR";
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "ALTA";
        }
        echo $respuesta;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("ordenServicio2/plantillaPdf", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_CM.js',
                // 'assets/js/firmasMoviles.js',
                'assets/js/materiales/ctolMateriales.js',
                'assets/js/materiales/materiales2.js',
                'assets/js/materiales/autosuma.js',
                'assets/js/reglasBloqueo.js',
                'assets/js/superUsuario.js',
                'assets/js/Multipunto/precarga_form.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Control de Materiales"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}
