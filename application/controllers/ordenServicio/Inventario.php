<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Inventario extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
    }

    public function index($idOrden = 0,$datos = NULL)
  	{
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));

        $datos["idOrden"] = $id_cita;

        //Recuperamos el id orden_intelisis
        //$datos["orden_intelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        //Comprobamos que exista el registro
        $revision = $this->mConsultas->get_result("idOrden",$id_cita,"inventario_or");
        foreach ($revision as $row) {
            $verificacion = $row->id;
        }

        //Si existe el registro cargamos los datos
        if (isset($verificacion)) {
            $this->setData($id_cita,$datos);

        //De lo contrario, consultamos los datos para mostrar
        } else {
            //Recuperamos la informacion de la orden
            if (!isset($datos["serie"])) {
                $precarga = $this->mConsultas->precargaConsulta($id_cita);
                foreach ($precarga as $row){
                    $datos["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                    $datos["modelo"] = $row->subcategoria;
                    //$datos["color"] = $this->mConsultas->getColorAuto($idOrden);
                    //Recuperamos el color
                    $queryColor  = $this->mConsultas->get_result('id',$row->id_color,'cat_colores');
                    foreach ($queryColor as $row_2) {
                        $datos["color"] = $row_2->color;
                    }

                    $datos["torre"] = $row->numero_interno."-".$row->color;
                    $datos["cliente"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
                    $datos["km"] = $row->vehiculo_kilometraje;
                    $datos["placas"] = $row->vehiculo_placas;
                    $datos["motor"] = $row->motor;
                    $datos["asesor"] = $row->asesor;

                    $datos["idIntelisis"] = $row->folioIntelisis;

                    //$cadena .= $row->datos_email."|";
                    //$correo_2 = $row->correo_compania;
                }
            }

            //Verificamos si sera un formulario con prellenado o un formulario en limpio
            $revision = $this->mConsultas->get_result("noServicio",$id_cita,"diagnostico");
            foreach ($revision as $row) {
                $verificacion_2 = $row->idDiagnostico;
            }

            //Si existe un registro de inventario , enviamos a precargar los datos
            if (isset($verificacion_2)) {
                $this->comodinPrellenado($id_cita,$datos);
            //Si no existe, solo cargamos los datos base
            } else {

                $this->loadAllView($datos,'inventario/inventario_alta');    
            }
        }
  	}

    //Recuperamos la informacion de la orden 
    public function comodinPrellenado($idOrden = '', $datos = NULL)
    {
        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("noServicio",$idOrden,"diagnostico");
        foreach ($query as $row){
            $datos["firmaAsesor"] = (file_exists($row->firmaDiagnostico)) ? $row->firmaDiagnostico : "";
            //$datos["firmaCliente"] = (file_exists($row->firmaConsumidor1)) ? $row->firmaConsumidor1 : "";

            $datos["danos"] = $row->danos;
            $datos["danosImg"] = (file_exists($row->danosImg)) ? $row->danosImg : "";

            $datos["interiores"] = explode("_",$row->interiores);
            $datos["cajuela"] = explode("_",$row->cajuela);
            $datos["exteriores"] = explode("_",$row->exteriores);
            $datos["documentacion"] = explode("_",$row->documentacion);

            $datos["articulos"] = $row->articulos;
            $datos["cualesArticulos"] = $row->cualesArticulos;
            $datos["nivelGasolina"] = $row->nivelGasolina;
            $datos["nivelGasolinaDiv"] = explode("/",$row->nivelGasolina);
            $datos["asesor_2"] = $row->nombreDiagnostico;
            $datos["cliente_2"] = $row->nombreConsumido1;
        }

        //Consultamos si ya se entrego la unidad
        $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idOrden);

        $this->loadAllView($datos,'inventario/inventario_alta');     
    }

    //Validamos la infromacion recibida de la vista
    public function validateView()
    {
        $datos["tipoRegistro"] = $this->input->post("cargaFormulario");
        $datos["idOrden"] = $this->input->post("orderService");
        $datos["color"] = $this->input->post("color");
        $datos["torre"] = $this->input->post("torre");
        $datos["cliente"] = $this->input->post("cliente");
        $datos["modelo"] = $this->input->post("modelo");
        $datos["km"] = $this->input->post("km");
        $datos["placas"] = $this->input->post("placas");
        $datos["fecha"] = $this->input->post("fecha");
        $datos["motor"] = $this->input->post("motor");
        $datos["serie"] = $this->input->post("serie");
        $datos["asesor"] = $this->input->post("asesor");
        $datos["pOrigen"] = $this->input->post("pOrigen");

        if ($datos["tipoRegistro"] == "1") {
            $this->form_validation->set_rules('orderService', 'La orden de servicio es requerida.', 'required|callback_existencia');
            //Validamos las firmas
            $this->form_validation->set_rules('rutaFirmaAsesor', 'Firma requerida.', 'required');
            $this->form_validation->set_rules('rutaFirmaAcepta', 'Firma requerida.', 'required');

            // $this->form_validation->set_rules('nivelGasolina', 'Falta indicar.', 'required');
            //$this->form_validation->set_rules('nivelGasolina_1', 'Falta indicar nivel de gasolina.', 'required');
            //$this->form_validation->set_rules('nivelGasolina_2', '<br>Falta indicar nivel de gasolina.', 'required');
        }else{
            $this->form_validation->set_rules('orderService', 'Campo Requerido.', 'required');
        }

        //$this->form_validation->set_rules('color', 'Campo Requerido.', 'required');
        //$this->form_validation->set_rules('torre', 'Campo Requerido.', 'required');
        $this->form_validation->set_rules('cliente', 'Campo Requerido.', 'required');
        $this->form_validation->set_rules('modelo', 'Campo Requerido.', 'required');
        $this->form_validation->set_rules('km', 'Campo Requerido.', 'required');
        $this->form_validation->set_rules('placas', 'Campo Requerido.', 'required');
        $this->form_validation->set_rules('fecha', 'Campo Requerido.', 'required');
        //$this->form_validation->set_rules('motor', 'Campo Requerido.', 'required');
        $this->form_validation->set_rules('serie', 'Campo Requerido.', 'required');

        //$this->form_validation->set_rules('interiores', '<br>Las Características interiores son requeridas.', 'callback_interiores');
        //$this->form_validation->set_rules('cajuela', '<br>Falta opciones.', 'callback_cajuela');
        // $this->form_validation->set_rules('exteriores', 'Falta indicar.', 'required');
        // $this->form_validation->set_rules('documentacion', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('articulos', 'Falta indicar.', 'required');

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('interiores','Faltan características.');
        $this->form_validation->set_message('cajuela','Faltan características de cajuela.');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar el No. de Orden.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if(($this->form_validation->run()!=FALSE)||($this->form_validation->run()=='')){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";
            // var_dump(form_error());
            //var_dump($this->form_validation->run());
            $this->index($datos["idOrden"],$datos);
        }
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("orderService");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"inventario_or");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Validar requisitos minimos P1
    public function interiores()
    {
        $return = FALSE;
        if ($this->input->post("interiores")) {
            if (count($this->input->post("interiores")) >10){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar requisitos minimos P1
    public function cajuela()
    {
        $return = FALSE;
        if ($this->input->post("cajuela")) {
            if (count($this->input->post("cajuela")) >3){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Empaquetamos los datos y enviamos al servidor
    public function getDataForm($datos = NULL)
    {
        $datos["registro"] = date("Y-m-d H:i:s");

        //Armamos el array para contener los valores
        if ($datos["tipoRegistro"] == "1") {
            $dataForm = array(
              "id" => NULL,
              "idOrden" => $datos["idOrden"],
              "folio" => '',
            );
        }

        $dataForm["color"] = $this->input->post("color");
        $dataForm["torre"] = $this->input->post("torre");
        $dataForm["cliente"] = $this->input->post("cliente");
        $dataForm["placas"] = $this->input->post("placas");
        $dataForm["fecha"] = $this->input->post("fecha");
        $dataForm["motor"] = $this->input->post("motor");
        $dataForm["serie"] = $this->input->post("serie");
        $dataForm["modelo"] = $this->input->post("modelo");
        $dataForm["km"] = $this->input->post("km");

        $dataForm["danos"] = $this->validateRadio($this->input->post("danos"),"0");
        $dataForm["danosImg"] = $this->base64ToImage($this->input->post("danosMarcas"),"diagramas");

        //Valores de tablas (indicadores) Parte - Diagnostico
        $dataForm["interiores"] = $this->validateCheck($this->input->post("interiores"),"S/R");
        $dataForm["cajuela"] = $this->validateCheck($this->input->post("cajuela"),"S/R");
        $dataForm["exteriores"] = $this->validateCheck($this->input->post("exteriores"),"S/R");
        $dataForm["cofre"] = $this->validateCheck($this->input->post("cofre"),"S/R");
        $dataForm["inferior"] = $this->validateCheck($this->input->post("inferior"),"S/R");
        $dataForm["sis_frenos"] = $this->validateCheck($this->input->post("sisFrenos"),"S/R");

        $dataForm["gasolina"] = $this->input->post("nivelGasolina_1")."/".$this->input->post("nivelGasolina_2");

        $dataForm["articulos"] = $this->validateRadio($this->input->post("articulos"),"0");
        $dataForm["cualesArticulos"] = $this->input->post("cualesArticulos");

        $dataForm["asesor"] = $this->input->post("asesor");

        //if ($datos["tipoRegistro"] == "1") {
            $dataForm["firmaCliente"] = $this->base64ToImage($this->input->post("rutaFirmaAcepta"),"firmas");
            $dataForm["firmaAsesor"] = $this->base64ToImage($this->input->post("rutaFirmaAsesor"),"firmas");
        //}

        $dataForm["observaciones"] = $this->input->post("observaciones");
         
        if ($datos["tipoRegistro"] == "1") {   
            $dataForm["fecha_registro"] = $datos["registro"];
        }

        $dataForm["fecha_actualiza"] = $datos["registro"];
        //Guardamos el Registro
        if ($datos["tipoRegistro"] == "1") {
            // //Comprobamos si es un diagnostico con cita o sin citas
            // if ($datos["servicioId"] != "0") {
                //Con cita
                $servicio = $this->mConsultas->save_register('inventario_or',$dataForm);
                if ($servicio != 0) {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idOrden"],"Registro","Orden de Servicio (Inventario)");

                    $datos["mensaje"] = "1";
                    //$this->listData($datos["idOrden"],$datos);
                    redirect(base_url().'Inventario_Lista/'.$datos["mensaje"]);
                }else {
                    $datos["mensaje"] = "2";
                    $this->index($datos["idOrden"],$datos);
                }
                
            // }else {
            //     //Sin cita
            //     $folio = $this->generateFolio();
            //     $dataForm["noServicio"] = $folio;
            //     $datos["servicioId"] = $folio;
            //     $dataForm["fechaRegistro"] = date("Y-m-d H:i:s");
            //     //Sin cita
            //     $servicio = $this->mConsultas->save_register('diagnostico_sc',$dataForm);
            //     if ($servicio != 0) {
            //         //Guardamos el movimiento
            //         $this->loadHistory($datos["servicioId"],"Registro temporal","Orden de Servicio (temporal)");

            //         $datos["mensaje"] = "1";
            //         $this->saveContract($datos);
            //     }else {
            //         $datos["mensaje"] = "2";
            //         $this->index("0",$datos);
            //     }
            // }
        //Si vamos a actualizar el registro
        } else {
            $actualizar = $this->mConsultas->update_table_row('inventario_or',$dataForm,'idOrden',$datos["idOrden"]);
            if ($actualizar) {
                //Guardamos el movimiento
                $this->loadHistory($datos["idOrden"],"Actualización","Orden de Servicio (Inventario)");

                $datos["mensaje"]="1";
                $this->setData($datos["idOrden"],$datos);
            }else {
                $datos["mensaje"]="2";
                $this->setData($datos["idOrden"],$datos);
            }

        }
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Funcion para generar el folio de la venta
    public function generateFolio()
    {
        //Recuperamos el indice para el folio
        $fecha = date("Ymd");
        $registro = $this->mConsultas->last_id_table();
        $row = $registro->row();
        if (isset($row)){
          $indexFolio = $registro->row(0)->idDiagnostico;
        }else {
          $indexFolio = 0;
        }

        $nvoFolio = $indexFolio + 1;
        if ($nvoFolio < 10) {
            $folio = "00".$nvoFolio."-".$fecha;
        } elseif(($nvoFolio > 9) && ($nvoFolio < 100)) {
            $folio = "0".$nvoFolio."-".$fecha;
        }else {
            $folio = $nvoFolio."-".$fecha;
        }
        return $folio;
    }

    //Recuperamos los registros para listarlos
    public function listData($x = "",$datos = NULL)
    {
        $campos = $this->mConsultas->listaDiagnosticoDatos(); 
        foreach ($campos as $row) {
            $corte_b = explode("-",$row->fecha_recepcion);
            $datos["fechaDiag"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0]."";

            //$datos['idRegistro'][] = $row->idDiagnostico;
            $datos["idOrden"][] = $row->orden_cita;
            $datos["id_cita_url"][] = $this->encrypt($row->orden_cita);
            $datos["idIntelisis"][] = $row->folioIntelisis;
            
            $datos["tecnico"][] = $row->tecnico;
            $datos["vehiculo"][] = $row->vehiculo_modelo;
            $datos["placas"][] = $row->vehiculo_placas;

            $datos["nombreAsesor"][] = $row->asesor;
            $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;

            $datos["diagnostico"][] = $this->validaDiagnostico($row->orden_cita);
            $datos["inventario"][] = $this->validaInventario($row->orden_cita);

        }

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        $this->loadAllView($datos,'inventario/lista_inventario');
    }

    //Verificamos que exista la cotizacion para mostrarla
    public function validaDiagnostico($idOrden = "0")
    {
        $respuesta = "0";
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("noServicio",$idOrden,"diagnostico");
        foreach ($query as $row){
            $dato = $row->idDiagnostico;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "1";
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "0";
        }

        return $respuesta;
    }

    //Verificamos que exista la cotizacion para mostrarla
    public function validaInventario($idOrden = "0")
    {
        $respuesta = "0";
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"inventario_or");
        foreach ($query as $row){
            $dato = $row->id;
        }
        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "1";
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "0";
        }

        return $respuesta;
    }


    //Comprobamos que al final se genere el pdf
    public function setDataPDF($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Comprobamos que al final se genera la vista de revision
    public function setDataRevision($idOrden = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData($idOrden = 0,$datos = NULL)
    {
        //Recuperamos el id orden_intelisis
        //$datos["orden_intelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["idOrden"])) {
            $datos["idOrden"] = $idOrden;
        }

        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"inventario_or");
        foreach ($query as $row){
            //Recuperamos valores para formulario (Exclusivo)
            if ($datos["destinoVista"] == "Formulario") {
                $datos["fecha"] = $row->fecha;
            }else {
                $fechaFracc = explode("-",$row->fecha);
                $datos["fecha"] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
            }

            $datos["color"] = $row->color;
            $datos["torre"] = $row->torre;
            $datos["cliente"] = $row->cliente;
            $datos["placas"] = $row->placas;
            $datos["fecha"] = $row->fecha;
            $datos["motor"] = $row->motor;
            $datos["serie"] = $row->serie;
            $datos["modelo"] = $row->modelo;
            $datos["km"] = $row->km;
            
            $datos["danos"] = $row->danos;
            $datos["danosImg"] = (file_exists($row->danosImg)) ? $row->danosImg : "";

            $datos["interiores"] = explode("_",$row->interiores);
            $datos["cajuela"] = explode("_",$row->cajuela);
            $datos["exteriores"] = explode("_",$row->exteriores);
            $datos["cofre"] = explode("_",$row->cofre);
            $datos["inferior"] = explode("_",$row->inferior);
            $datos["sisFrenos"] = explode("_",$row->sis_frenos);

            $datos["articulos"] = $row->articulos;
            $datos["cualesArticulos"] = $row->cualesArticulos;
            $datos["observaciones"] = $row->observaciones;

            //$datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : "";
            $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";

            $datos["nivelGasolina"] = $row->gasolina;
            $datos["nivelGasolinaDiv"] = explode("/",$row->gasolina);

            $datos["asesor"] = $row->asesor;
        }

        //Consultamos si ya se entrego la unidad
        $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idOrden);

        if ($datos["destinoVista"] == "PDF") {
              $this->generatePDF($datos);
        } elseif ($datos["destinoVista"] == "Revisión") {
            $this->loadAllView($datos,'inventario/plantillaRevision');
        } else {
            $this->loadAllView($datos,'inventario/inventario_editar');            
        }
    }

    //Validar Radio
    public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Validar multiple check
    public function validateCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >1){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."_";
                }
            }
        }
        return $respuesta;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_IN";
        for($i=0; $i<=6; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }
        
        $data = explode(',', $imgBase64);

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("inventario/plantillaPdf", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_VC.js',
                'assets/js/inventario/interaccionCheck.js',
                'assets/js/inventario/interaccionesDiagramas.js',
                'assets/js/superUsuario.js'
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Inventario"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}
