<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Documentacion_Ordenes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
    }

	//Entrada para cargar lista de documentos (asesores) Nuevo listado
    public function documentacion_tb_asesor($entrada = "",$datos = NULL)
    {
        $datos["destinoVista"] = "ASESOR";
        $datos["tipo_vista"] = "LISTA";
        $this->lista_registros($entrada,$datos);
    }

    //Entrada para cargar lista de documentos (técnicos) Nuevo listado
    public function documentacion_tb_tecnico($entrada = "",$datos = NULL)
    {
        $datos["destinoVista"] = "TECNICO";
        $datos["tipo_vista"] = "LISTA";

        $id = '';
        
        /** Hacemos proceso de listado aqui mismo  **/
        $mes_actual = date("m");
        
        if ($mes_actual <= 1) {
            $fecha_busqueda = (date("Y")-1)."-"."12-20";
        } else {
            $fecha_busqueda = date("Y")."-".((($mes_actual - 1) < 10) ? "0" : "").($mes_actual - 1)."-20";
        }

        if ($this->session->userdata('rolIniciado')) {
            //Filtramos dependiendo del rol 
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {

                $id = $this->session->userdata('idUsuario');
                //$registros = $this->mConsultas->doc_tb_ordenes_tec($id,$fecha_busqueda);
            }else{
                //$id = '28';
            }
        }else{
            //$id = '28';
        }

        if ($id == '') {
            $this->lista_registros($entrada,$datos);
        } else {
            $id_cita_concetrado = [];
            //Recuperamos ordenes del tecnico
            $listado_ordenes = $this->mConsultas->ordenes_tec_principal($id, $fecha_busqueda);
            for($i = 0; $i < count($listado_ordenes); $i++ ){
                $id_cita_concetrado[] = $listado_ordenes[$i]["id_cita"];
            }

            $listado_operaciones_tec = $this->mConsultas->ordenes_tec_operacion($id, $fecha_busqueda);

            //Recuperamos todas las ordenes asociadas al tecnico
            for($j = 0; $j < count($listado_operaciones_tec); $j++ ){
                if (!in_array($listado_operaciones_tec[$j]["id_cita"], $id_cita_concetrado)) {
                    $id_cita_concetrado[] = $listado_operaciones_tec[$j]["id_cita"];
                }
            }

            //Generamos la documentacion
            for($x = 0; $x < count($id_cita_concetrado); $x++ ){
                if ($id_cita_concetrado[$x] != "") {
                    $info = $this->mConsultas->doc_tb_orden_tec_cita($id_cita_concetrado[$x]);

                    if ($info != NULL) {
                        if (($info["identificador"] == "T")||(($info["identificador"] == "G")&&($info["diagnostico"] == "0"))) {
                            $conteo_fg = $this->conteo_registro($info["id_cita"]);
                            $formatos_g = $conteo_fg[0];
                            $formatos_gfirma = $conteo_fg[1];
                        } else {
                            $formatos_g = 0;
                            $formatos_gfirma = 0;
                        }
                        
                        $fecha = new DateTime($info["fecha_recepcion"].' 00:00:00');
                        $datos["fecha_recepcion"][] = $fecha->format('d-m-Y');

                        $datos["id_cita"][] = $info["id_cita"];
                        $datos["tipo_orden"][] = $info["identificador"];
                        $datos["id_orden"][] = $this->encrypt($info["id_orden"]);
                        $datos["id_cita_url"][] = $this->encrypt($info["id_cita"]);
                        $datos["folio_externo"][] = $info["folio_externo"];

                        $datos["placas"][] = $info["placas"];
                        $datos["modelo"][] = $info["vehiculo"];

                        $datos["nombre_asesor"][] = $info["asesor"];
                        $datos["tecnico"][] = $info["tecnico"];
                        $datos["cliente"][] = $info["cliente"];
                        $datos["kilometraje"][] = $info["vehiculo_kilometraje"];

                        $datos["presupuesto"][] = $info["presupuesto"];
                        $datos["presupuesto_afecta"][] = $info["presupuesto_afectado"] ;
                        $datos["presupuesto_origen"][] = $info["presupuesto_origen"];

                        $datos["vc_udio"][] = ((file_exists($info["evidencia_voz_cliente"])) ? $info["evidencia_voz_cliente"] : "") ;
                        $datos["voz_cliente"][] = $info["voz_cliente"] ;
                        
                        $datos["multipunto"][] = $info["multipunto"] ;
                        $datos["firma_jefetaller"][] = ((($info["firma_jefetaller"] != "") &&($info["firma_jefetaller"] != "0")) ? "1" : "0");

                        $datos["oasis"][] = str_replace("|", "",$info["oasis"]);
                        $datos["diagnostico"][] = $info["diagnostico"];
                        $datos["doc_poliza"][] = $info["archivo_poliza"]; //(($row->archivo_poliza == "1") ? $this->recuperar_polizas($row->id_cita) : []);
                        
                        $datos["lavado"][] = (($info["id_lavador_cambio_estatus"] != "") ? 'TERMINADO' : '');

                        $datos["servicio_valet"][] = $info["servicio_valet"];
                        $datos["bg_tr"][] = (($info["id_tipo_orden"] == "6") ? '#fadbd8' : '#fdfefe');
                        $datos["tipo_servicio_orden"][] = $info["id_status_color"];

                        //Verificamos si es el panel del tecnico para ver validaciones
                        if (($datos["destinoVista"] == "TECNICO") || ($datos["destinoVista"] == "JEFE_TALLER")) {
                            $datos["formatos_g"][] = $formatos_g; 
                            $datos["formatos_gfirma"][] = $formatos_gfirma;
                            //(($row->identificador == "T") ? $this->conteo_registro($row->id_cita) : "-1");
                        }elseif ($datos["destinoVista"] == "CIERRE_ORDEN") {
                            $datos["formatos_g"][] = 0;
                            $datos["formatos_gfirma"][] = "no";
                        }

                        if (($datos["destinoVista"] != "GARANTIAS") && ($datos["destinoVista"] != "CIERRE_ORDEN")) { 
                            $id_asociado = $this->mConsultas->publica_asociada($info["id_cita"]);
                            $id_cita_asociada = (($id_asociado != NULL) ? $id_asociado : (($info["id_tipo_orden"] == "2") ? $info["id_cita"] : NULL));

                            $garantias_id_cita = (($id_cita_asociada != NULL) ? $id_cita_asociada : $info["id_cita"]);
                            $datos["garantias_id_cita"][] = $garantias_id_cita;

                            $existe_garantias = (($id_cita_asociada != NULL) ? $this->validar_garantias($id_cita_asociada) : "");
                            $datos["garantias"][] = $existe_garantias;

                            if ($existe_garantias == 1) {
                                $datos["gpza_firmas"][] = $this->validar_firmas_pzag($garantias_id_cita);
                            }else{
                                $datos["gpza_firmas"][] = 0;
                            }
                        
                        }elseif ($datos["destinoVista"] == "CIERRE_ORDEN") {
                            $datos["garantias_id_cita"][] = NULL;
                            $datos["garantias"][] = "0";
                        }
                    }
                }
            }

            $this->loadAllView($datos,'ordenServicio/listas/documentos_tecnico');
        }
    
        //$this->lista_registros($entrada,$datos);
    }

    //Entrada para cargar lista de documentos (jefe de taller) Nuevo listado
    public function documentacion_tb_jdt($entrada = "",$datos = NULL)
    {
        $datos["destinoVista"] = "JEFE_TALLER";
        $datos["tipo_vista"] = "LISTA";
        $this->lista_registros($entrada,$datos);
    }

    //Entrada para cargar lista de documentos (proactivo - cierre de orden) Nuevo listado
    public function documentacion_tb_cierre($entrada = "",$datos = NULL)
    {
        $datos["destinoVista"] = "CIERRE_ORDEN";
        $datos["tipo_vista"] = "LISTA";
        $this->lista_registros($entrada,$datos);
    }

    //Entrada para cargar lista de documentos (proactivo - cierre de orden) Nuevo listado
    public function documentacion_tb_garantia($entrada = "",$datos = NULL)
    {
        $datos["destinoVista"] = "GARANTIAS";
        $datos["tipo_vista"] = "LISTA";
        $this->lista_registros($entrada,$datos);
    }

    //Entrada para cargar lista de documentos (jefe de taller) Nuevo listado
    public function documentacion_tb_ventanilla($entrada = "",$datos = NULL)
    {
        $datos["destinoVista"] = "VENTANILLA";
        $datos["tipo_vista"] = "LISTA";
        $this->lista_registros($entrada,$datos);
    }

    //Entrada para cargar lista ordenes  para polizas
    public function ordenes_tb_poliza($entrada = "",$datos = NULL)
    {
        $datos["destinoVista"] = "POLIZA";
        $datos["tipo_vista"] = "LISTA";
        $this->lista_registros($entrada,$datos);
    }

    //Cargamos la nueva forma de listar la documentacion
    public function lista_registros($entrada = '',$datos = NULL)
    {
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "GENERAL";
        }

        if (!isset($datos["tipo_vista"])) {
            $datos["tipo_vista"] = "LISTA";
        }

        $mes_actual = date("m");
        
        if ($mes_actual <= 1) {
            $fecha_busqueda = (date("Y")-1)."-"."12-20";
        } else {
            $fecha_busqueda = date("Y")."-".((($mes_actual - 1) < 10) ? "0" : "").($mes_actual - 1)."-20";
        }

        //Comprobamos el tipo de usuario que accede a la lista
        if (($datos["destinoVista"] == "VENTANILLA")||($datos["destinoVista"] == "GARANTIAS")) {
            //$registros = $this->mConsultas->doc_tb_ordenes($fecha_busqueda);
            $registros = $this->mConsultas->doc_tb_ordenes_mes($fecha_busqueda);

            //Almacenamos los resultados
            foreach ($registros as $row) {
                $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');
                $datos["fecha_recepcion"][] = $fecha->format('d-m-Y');

                $datos["id_cita"][] = $row->id_cita;
                $datos["tipo_orden"][] = $row->identificador;
                $datos["id_orden"][] = $this->encrypt($row->id_orden);
                $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                $datos["folio_externo"][] = $row->folio_externo;

                $datos["placas"][] = $row->placas;
                $datos["modelo"][] = $row->vehiculo;

                $datos["nombre_asesor"][] = $row->asesor;
                $datos["tecnico"][] = $row->tecnico;
                $datos["cliente"][] = $row->cliente;
                $datos["kilometraje"][] = $row->vehiculo_kilometraje;

                $datos["presupuesto"][] = $row->presupuesto;
                $datos["presupuesto_afecta"][] = $row->presupuesto_afectado ;
                $datos["presupuesto_origen"][] = $row->presupuesto_origen;

                $datos["vc_udio"][] = ((file_exists($row->evidencia_voz_cliente)) ? $row->evidencia_voz_cliente : "") ;
                $datos["voz_cliente"][] = $row->voz_cliente ;
                
                $datos["multipunto"][] = $row->multipunto ;
                $datos["firma_jefetaller"][] = $row->firma_jefetaller ;

                $datos["oasis"][] = str_replace("|", "",$row->oasis);
                $datos["diagnostico"][] = $row->diagnostico;
                $datos["doc_poliza"][] = $row->archivo_poliza; 
                
                $datos["lavado"][] = (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : '');

                $datos["servicio_valet"][] = $row->servicio_valet;
                $datos["bg_tr"][] = (($row->id_tipo_orden == "6") ? '#fadbd8' : '#fdfefe');
                $datos["tipo_servicio_orden"][] = $row->id_status_color;

                $id_cita_asociada = $this->mConsultas->publica_asociada($row->id_cita);
                
                //$datos["garantias"][] = (($id_cita_asociada != NULL) ? $this->validar_garantias($id_cita_asociada) : "");
                $existe_garantias = (($id_cita_asociada != NULL) ? $this->validar_garantias($id_cita_asociada) : "");
                $datos["garantias"][] = $existe_garantias;
                $datos["gpza_firmas"][] = 0;
                /*if ($existe_garantias == 1) {
                    $datos["gpza_firmas"][] = $this->validar_firmas_pzag($id_cita_asociada);
                }else{
                    $datos["gpza_firmas"][] = 0;
                }*/

                $datos["garantias_id_cita"][] = (($id_cita_asociada != NULL) ? $id_cita_asociada : $row->id_cita);
            }
        }elseif ($this->session->userdata('rolIniciado')) {
        	//Filtramos dependiendo del rol 
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {

                $id = $this->session->userdata('idUsuario');
                $registros = $this->mConsultas->doc_tb_ordenes_tec($id,$fecha_busqueda);
            }else{
                $registros = $this->mConsultas->doc_tb_ordenes($fecha_busqueda);
            }

            //Almacenamos los resultados
            foreach ($registros as $row) {

                if (($row->identificador == "T")||(($row->identificador == "G")&&($row->diagnostico == "0")) || ($row->identificador == "I") || ($row->identificador == "R")) {
                    $conteo_fg = $this->conteo_registro($row->id_cita);
                    $formatos_g = $conteo_fg[0];
                    $formatos_gfirma = $conteo_fg[1];
                } else {
                    $formatos_g = 0;
                    $formatos_gfirma = 0;
                }
                
                //if (!in_array($row->orden_cita,$datos["idOrden"])) {
                    $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');
                    $datos["fecha_recepcion"][] = $fecha->format('d-m-Y');

                    $datos["id_cita"][] = $row->id_cita;
                    $datos["tipo_orden"][] = $row->identificador;
                    $datos["id_orden"][] = $this->encrypt($row->id_orden);
                    $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                    $datos["folio_externo"][] = $row->folio_externo;

                    $datos["placas"][] = $row->placas;
                    $datos["modelo"][] = $row->vehiculo;

                    $datos["nombre_asesor"][] = $row->asesor;
                    $datos["tecnico"][] = $row->tecnico;
                    $datos["cliente"][] = $row->cliente;
                    $datos["kilometraje"][] = $row->vehiculo_kilometraje;

                    $datos["presupuesto"][] = $row->presupuesto;
                    $datos["presupuesto_afecta"][] = $row->presupuesto_afectado ;
                    $datos["presupuesto_origen"][] = $row->presupuesto_origen;

                    $datos["vc_udio"][] = ((file_exists($row->evidencia_voz_cliente)) ? $row->evidencia_voz_cliente : "") ;
                    $datos["voz_cliente"][] = $row->voz_cliente ;
                    
                    $datos["multipunto"][] = $row->multipunto ;
                    $datos["firma_jefetaller"][] = ((($row->firma_jefetaller != "") &&($row->firma_jefetaller != "0")) ? "1" : "0");

                    $datos["oasis"][] = str_replace("|", "",$row->oasis);
                    $datos["diagnostico"][] = $row->diagnostico;
                    $datos["doc_poliza"][] = $row->archivo_poliza; //(($row->archivo_poliza == "1") ? $this->recuperar_polizas($row->id_cita) : []);
                    
                    $datos["lavado"][] = (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : '');

                    //Verificamos si hay comentarios en la orden (solo vistas autorizados)
                    if (($datos["destinoVista"] == "JEFE_TALLER") || $datos["destinoVista"] == "CIERRE_ORDEN") {
                        $datos["comentarios"][] = $this->mConsultas->validarComentarioCO($row->id_cita);
                    }

                    $datos["servicio_valet"][] = $row->servicio_valet;
                    $datos["bg_tr"][] = (($row->id_tipo_orden == "6") ? '#fadbd8' : '#fdfefe');
                    $datos["tipo_servicio_orden"][] = $row->id_status_color;

                    //Verificamos si es el panel del tecnico para ver validaciones
                    if (($datos["destinoVista"] == "TECNICO") || ($datos["destinoVista"] == "JEFE_TALLER")) {
                        $datos["formatos_g"][] = $formatos_g; 
                        $datos["formatos_gfirma"][] = $formatos_gfirma;
                        //(($row->identificador == "T") ? $this->conteo_registro($row->id_cita) : "-1");
                    }elseif ($datos["destinoVista"] == "CIERRE_ORDEN") {
                        $datos["formatos_g"][] = 0;
                        $datos["formatos_gfirma"][] = "no";
                    }

                    if ($datos["destinoVista"] == "JEFE_TALLER") {
                        //Comparamos operaciones asignadas con reparaciones de garantias
                        
                    }

                    if (($datos["destinoVista"] != "GARANTIAS") && ($datos["destinoVista"] != "CIERRE_ORDEN")) { 
                        $id_asociado = $this->mConsultas->publica_asociada($row->id_cita);
                        $id_cita_asociada = (($id_asociado != NULL) ? $id_asociado : (($row->id_tipo_orden == "2") ? $row->id_cita : NULL));

                        $garantias_id_cita = (($id_cita_asociada != NULL) ? $id_cita_asociada : $row->id_cita);
                        $datos["garantias_id_cita"][] = $garantias_id_cita;

                        $existe_garantias = (($id_cita_asociada != NULL) ? $this->validar_garantias($id_cita_asociada) : "");
                        $datos["garantias"][] = $existe_garantias;

                        if ($existe_garantias == 1) {
                            $datos["gpza_firmas"][] = $this->validar_firmas_pzag($garantias_id_cita);
                        }else{
                            $datos["gpza_firmas"][] = 0;
                        }
                    
                    }elseif ($datos["destinoVista"] == "CIERRE_ORDEN") {
                        $datos["garantias_id_cita"][] = NULL;
                        $datos["garantias"][] = "0";
                    }
                //}
            }
        }

        if ($this->session->userdata('rolIniciado')) {
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }else{
            $datos["pOrigen"] = "GENERAL";
        }

        //Identificamos la lista donde se desplegaran los datos
        if ($datos["destinoVista"] == "ASESOR") {
            $this->loadAllView($datos,'ordenServicio/listas/documentos_asesor');
        } elseif ($datos["destinoVista"] == "TECNICO") {
        	$this->loadAllView($datos,'ordenServicio/listas/documentos_tecnico');
        } elseif (($datos["destinoVista"] == "JEFE_TALLER") || $datos["destinoVista"] == "CIERRE_ORDEN") {
        	$this->loadAllView($datos,'ordenServicio/listas/documentos_comentarios');
        } elseif ($datos["destinoVista"] == "GARANTIAS") {
            $this->loadAllView($datos,'ordenServicio/listas/documentos_garantias');
        } elseif ($datos["destinoVista"] == "VENTANILLA") {
            $this->loadAllView($datos,'ordenServicio/listas/documentos_ventanilla');
        } elseif ($datos["destinoVista"] == "POLIZA") {
            $this->loadAllView($datos,'ordenServicio/listas/documentos_poliza_garantias');
        } else {
            echo "Sin ruta";
        }
    }

    public function recuperar_polizas($id_cita='')
    {
        $respuesta = [];
        $archivos = $this->mConsultas->get_result_field("id_cita",$id_cita,"activo","1","archivos_poliza_garantias");
        foreach ($archivos as $row) {
            $respuesta[] = $row->url_archivo;
        }

        return $respuesta;
    }

    public function busqueda_documentacion()
    {
        $fecha_ini = $_POST["fecha_inicio"];
        $fecha_fin = $_POST["fecha_fin"];
        $campo = $_POST["campo"];

        $origen_lista = $_POST["listado"];

        if ($this->session->userdata('rolIniciado')) {
            $sesion_activa = $this->session->userdata('rolIniciado');
        }else{
            $sesion_activa = "";
        }

        //Si no se envia ninguna fecha
        if (($fecha_fin == "")&&($fecha_ini == "")) {
            $listado = $this->mConsultas->filtro_doc_campo($campo); 
        //Si se envian ambas fechas
        }elseif (($fecha_fin != "")&&($fecha_ini != "")) {
            $listado = $this->mConsultas->filtro_doc_campo_fecha1($campo,$fecha_ini,$fecha_fin);
        //Si solo se envia una fecha
        } else {
            if ($fecha_fin == "") {
                $listado = $this->mConsultas->filtro_doc_campo_fecha($campo,$fecha_ini);
            } else {
                $listado = $this->mConsultas->filtro_doc_campo_fecha($campo,$fecha_fin);
            }
        }
        
        $cadena = "";
        $contenedor = [];
        foreach ($listado as $row){
            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

            if (($origen_lista == "Tecnico")||($sesion_activa == "JDT")||($origen_lista == "Ventanilla")||($origen_lista == "Asesor")) {
                //$id_cita_asociada = $this->mConsultas->publica_asociada($row->id_cita);
                $id_asociado = $this->mConsultas->publica_asociada($row->id_cita);
                $id_cita_asociada = (($id_asociado != NULL) ? $id_asociado : (($row->id_tipo_orden == "2") ? $row->id_cita : NULL));

                $garantias = (($id_cita_asociada != NULL) ? $this->validar_garantias($id_cita_asociada) : (($sesion_activa == "JDT") ? "0" : "/"));
                $id_cita = (($id_cita_asociada != NULL) ? $id_cita_asociada : $row->id_cita);

                if (($origen_lista == "Tecnico")||($sesion_activa == "JDT")) {
                    $color_gpza = $this->validar_firmas_pzag($id_cita);

                    $conteo_fg = $this->conteo_registro($row->id_cita);
                    $formatos_g = $conteo_fg[0];
                    $formatos_gfirma = $conteo_fg[1];
                }else{
                    $color_gpza = 0;
                    $formatos_g = 0;
                    $formatos_gfirma = 0;
                }
            }else{
                $color_gpza = 0;
                $formatos_g = 0;
                $formatos_gfirma = 0;
            }

            $contenedor[] = array(
                "fecha_recepcion" => $fecha->format('d-m-Y'),
                "id_cita" => $row->id_cita,
                "identificador" => $row->identificador,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "id_orden" => $this->encrypt($row->id_orden),
                "folio_externo" =>  $row->folio_externo,
                "placas" =>  $row->placas,
                "vehiculo" =>  $row->vehiculo,
                "asesor" =>  $row->asesor,
                "tecnico" =>  $row->tecnico,
                "cliente" =>  $row->cliente,
                "km" => number_format($row->vehiculo_kilometraje,0)." km",
                "presupuesto" =>  $row->presupuesto,
                "presupuesto_afectado" =>  $row->presupuesto_afectado ,
                "presupuesto_origen" =>  $row->presupuesto_origen,
                "audio_vc" => ((file_exists($row->evidencia_voz_cliente)) ? $row->evidencia_voz_cliente : ""),
                "voz_cliente" => $row->voz_cliente,
                "multipunto" => $row->multipunto,
                "firma_jefetaller" => ((($row->firma_jefetaller != "") &&($row->firma_jefetaller != "0")) ? "1" : "0") ,
                "servicio_valet" => $row->servicio_valet,
                "diagnostico" => $row->diagnostico,
                "oasis" => str_replace("|", "~", $row->oasis),
                "lavado" => (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : ''),
                "tipo_orden" => (($row->id_tipo_orden == "6") ? '#fadbd8' : '#fdfefe'),
                "status_color" => $row->id_status_color,
                "listado" => $origen_lista,
                "registros_garantias" => $formatos_g,
                //((($origen_lista == "Tecnico")||($sesion_activa == "JDT")) ? $this->conteo_registro($row->id_cita): "/"),
                "folio_garantia" => ((isset($garantias)) ? $garantias : (($sesion_activa == "JDT") ? "0" : "/")),
                "garantias_id_cita" => ((isset($id_cita)) ? $id_cita : $row->id_cita),
                "gid_cita_url" => $this->encrypt((isset($id_cita)) ? $id_cita : $row->id_cita),
                "comentarios" => ((($sesion_activa == "PCO")||($sesion_activa == "JDT")) ? $this->mConsultas->validarComentarioCO($row->id_cita): "/"),
                "sesion" => $sesion_activa,
                "doc_poliza" => (($row->archivo_poliza != NULL) ? $row->archivo_poliza : "0"), 
                //(($row->archivo_poliza == "1") ? $this->recuperar_polizas($row->id_cita) : []),
                "cl_btng" => $color_gpza,
                "cl_btnfg" => $formatos_gfirma,

            );
        } //Garantias

        echo json_encode( $contenedor );
    }

    //Vista exclusiva para ordenes con "autorizacion por ausencia"
    public function documentos_cliente($idOrden='')
    {
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));

        //Recuperamos la informacion general
        $precarga = $this->mConsultas->precargaConsulta($id_cita);
        foreach ($precarga as $row){
            $datos["modelo"] = $row->vehiculo_anio;
            $datos["categoria"] = $row->vehiculo_modelo;

            $datos["autorizacion_ausencia"] = $row->autorizacion_ausencia;
            $datos["firma_diagnostico"] = $row->firma_diagnostico;
            $datos["firma_vc"] = $row->firma_vc;
            $datos["firma_protect"] = $row->firma_protect;
        }

        //Revisamos la carta renuncia de beneficios
        $revision_2 = $this->mConsultas->get_result("id_cita",$id_cita,"carta_renuncia");
        foreach ($revision_2 as $row) {
            $verificar = $row->id;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($verificar)) {
            $datos["carta_renuncia"] = "1";
        } else {
            $datos["carta_renuncia"] = "0";
        }

        //Si la orden tiene firmas por ausencia
        $validar = 0;
        //Validamos la firma del diagnostico
        if ($datos["firma_diagnostico"] == "1") {
            $validar++;
        }

        //Validamos la firma de la voz cliente
        if ($datos["firma_vc"] == "1") {
            $validar++;
        } else{
            //Verificamos si existe la voz cliente
            $query = $this->mConsultas->get_result("idPivote",$id_cita,"voz_cliente");
            foreach ($query as $row){
                $dato = $row->idRegistro;
            }

            if (!isset($dato)) {
                $validar++;
            }
        }

        //Validamos la firma de la carta renuncia
        if ($datos["firma_protect"] == "1") {
            $validar++;
        } else{
            if ($datos["carta_renuncia"] == "0") {
                $validar++;
            }
        }

        //Verificamos todas las firmas
        if ($validar >= 3) {
            $datos["liquidar_firmas"] = "1";
        } else {
            $datos["liquidar_firmas"] = "0";
        }

        //Comprobamos que exista el registro
        $revision = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($revision as $row) {
            $datos["id_cita"] = $row->id_cita;
            $datos["id_cita_enc"] = $this->encrypt($row->id_cita);
            $datos["fecha_recepcion"] =$row->fecha_recepcion;
            $datos["vehiculo"] = $row->vehiculo;
            $datos["placas"] = $row->placas;
            $datos["serie"] = $row->serie;
            $datos["asesor"] = $row->asesor;
            $datos["tecnico"] = $row->tecnico;
            $datos["cliente"] = $row->cliente;

            $datos["diagnostico"] = $row->diagnostico;
            $datos["voz_cliente"] = $row->voz_cliente;
        }

        $this->loadAllView($datos,'vista_doc_clientes');
    }

    //Vista exclusiva para ordenes con "autorizacion por ausencia"
    public function documentos_cliente_asesor()
    {
        $datos = NULL;
        //$id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        //Recuperamos los formularios pendientes por firmar
        $llenado_firmas = $this->mConsultas->doc_tb_ordenes_firmas();
        foreach ($llenado_firmas as $row){
            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');
            $datos["fecha_recepcion"][] = $fecha->format('d-m-Y');

            $datos["id_cita"][] = $row->id_cita;
            $datos["id_cita_en"][] = $this->encrypt($row->id_cita);
            $datos["folio_externo"][] = $row->folio_externo;

            $datos["placas"][] = $row->placas;
            $datos["modelo"][] = $row->vehiculo;

            $datos["nombre_asesor"][] = $row->asesor;
            $datos["tecnico"][] = $row->tecnico;
            $datos["cliente"][] = $row->cliente;
            $datos["vc_udio"][] = ((file_exists($row->evidencia_voz_cliente)) ? $row->evidencia_voz_cliente : "") ;
            $datos["voz_cliente"][] = $row->voz_cliente ;

            //Indicadores de firmas
            $datos["firma_diag"][] = $row->firma_diagnostico;
            $datos["firma_vc"][] = $row->firma_vc;
            $datos["carta_renuncia"][] = $row->id_renuncia;
            $datos["firma_cr"][] = $row->firma_protect;
        }

        $this->loadAllView($datos,'ordenServicio/listas/documentos_cliente_sfirma');
    }

    public function actualizar_documentacion($fecha = "")
    {
        $fecha = $_POST["fecha"];

        $documentacion = [];
        $id_citas = [];

        $documentacion_nvo = [];
        $id_citas_nvo = [];

        //$campos = $this->mConsultas->recuperar_documentaciones();
        $campos = $this->mConsultas->recuperar_documentaciones_fecha($fecha);

        foreach ($campos as $row) {
            //Verificamos si hay algun presupuesto
            if (($row->idCotizacion != "")|| ($row->id_presupuesto != "")) {
                //Si existe un presupuesto identificamos si es nuevo o viejo
                // (si se encuentra un id en los nuevos presupuesto, se supone que es nuevo formato)
                if ($row->id_presupuesto != "") {
                    $presupuesto = $row->id_presupuesto;
                    $afecta_cliente = $row->acepta_cliente;
                    $origen = "1";
                //De lo contrario se deduce que es un presupuesto viejito
                } else {
                    $presupuesto = $row->idCotizacion;
                    $afecta_cliente = $row->aceptoTermino;
                    $origen = "2";
                }
            
            //De lo contrario, no hay ningun presupuesto hecho
            } else {
                $presupuesto = "";
                $afecta_cliente = "";
                $origen = "0";
            }

            $revision = $this->mConsultas->get_result("id_cita",$row->orden_cita,"documentacion_ordenes");
            foreach ($revision as $row2) {
                $verificacion = $row2->id;
            }

            //Si existe el registro lo creamos
            if (isset($verificacion)) {
                $documentacion[] = array(
                    //"id" => NULL,
                    //"id_cita" => $row->orden_cita,
                    "folio_externo" => $row->folioIntelisis,
                    "fecha_recepcion" => $row->fecha_recepcion,
                    "vehiculo" => $row->vehiculo_modelo,
                    "placas" => $row->vehiculo_placas,
                    "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                    "asesor" => $row->asesor,
                    "tecnico" => $row->tecnico,
                    "id_tecnico" => $row->id_tecnico,
                    "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                    "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                    "oasis" => $row->archivoOasis,
                    "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                    "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                    "presupuesto" => (($presupuesto != "") ? "1" : "0"),
                    "presupuesto_afectado" => $afecta_cliente,
                    "presupuesto_origen" => $origen,
                    "voz_cliente" => ((($row->idVoz != "")||($row->vc_id != "")) ? "1" : "0"),
                    "evidencia_voz_cliente" => (($row->evidencia !="") ? $row->evidencia : (($row->audio != "") ? $row->audio : "")),
                    "servicio_valet" => "0", //(($row->idValet != "") ? "1" : "0"),
                    //"fechas_creacion" => $registro,
                );
                $id_citas[] = $row->orden_cita;
            }else{
                $documentacion_nvo[] = array(
                    "id" => NULL,
                    "id_cita" => $row->orden_cita,
                    "folio_externo" => $row->folioIntelisis,
                    "fecha_recepcion" => $row->fecha_recepcion,
                    "vehiculo" => $row->vehiculo_modelo,
                    "placas" => $row->vehiculo_placas,
                    "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                    "asesor" => $row->asesor,
                    "tecnico" => $row->tecnico,
                    "id_tecnico" => $row->id_tecnico,
                    "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                    "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                    "oasis" => $row->archivoOasis,
                    "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                    "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                    "presupuesto" => (($presupuesto != "") ? "1" : "0"),
                    "presupuesto_afectado" => $afecta_cliente,
                    "presupuesto_origen" => $origen,
                    "voz_cliente" => ((($row->idVoz != "")||($row->vc_id != "")) ? "1" : "0"),
                    "evidencia_voz_cliente" => (($row->evidencia !="") ? $row->evidencia : (($row->audio != "") ? $row->audio : "")),
                    "servicio_valet" => "0", //(($row->idValet != "") ? "1" : "0"),
                    "fechas_creacion" => date("Y-m-d H:i:s"),
                );

                $id_citas_nvo[] = $row->orden_cita;
            }
        }

        $total_reg = count($documentacion) - 1;
        $total_reg_nvo = count($documentacion_nvo) - 1;
        $respuesta = "";

        for ($i=0; $i <= $total_reg  ; $i++) { 
            $guardado = $this->mConsultas->update_table_row('documentacion_ordenes',$documentacion[($total_reg - $i)],'id_cita',$id_citas[($total_reg - $i)]);
            if ($guardado) {
                $respuesta = "OK";
            }else{
                $respuesta = "ERROR";
            }
        }

        /*for ($i=0; $i <= $total_reg_nvo ; $i++) {
            $guardado = $this->mConsultas->save_register('documentacion_ordenes',$documentacion[($total_reg_nvo - $i)]);
            if ($guardado != 0) {
                $respuesta = "OK";
            }else{
                $respuesta = "ERROR";
            }
        }*/

        echo $respuesta;
    }

    public function conteo_registro($id_cita='')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_GARANTIA."garantias/api/numero_formatos_guardados?no_orden=".$id_cita); 
        // Receive server response ...
        //curl_setopt($ch, CURLOPT_POSTFIELDS,"");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        /*curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Cache-Control": "no-cache",
                "Postman-Token": "6900f9a3-bbd5-4558-af94-c9ac3830948f"
            ));
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_HEADER, 0);*/

        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        $dataregistro = json_decode($response, TRUE);

        if (isset($dataregistro["numero_formatos"])) {
            $formatos = $dataregistro["numero_formatos"];
            $firma = (($dataregistro["firma_jefe_taller_completo"] == "si") ? 1 : 0);
        }else{
            $formatos = 0;
            $firma = 0;
        }

        return [$formatos,$firma];
    }

    public function validar_garantias($id_cita='')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_GARANTIA."index.php/apis/verifica_existencia_folio?numero_orden=".$id_cita); 
        // Receive server response ...
        //curl_setopt($ch, CURLOPT_POSTFIELDS,"");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        /*curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Cache-Control": "no-cache",
                "Postman-Token": "6900f9a3-bbd5-4558-af94-c9ac3830948f"
            ));
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_HEADER, 0);*/

        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        $dataregistro = json_decode($response, TRUE);

        if (isset($dataregistro["existe"])) {
            $garantia = (($dataregistro["existe"]) ? 1 : 0);
        }else{
            $garantia = 0;
        }

        return $garantia;
    }

    public function mostrat_formatos_garantias($id_cita_url='')
    {
        $id_cita = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_GARANTIA."garantias/api/url_formatos?perfil=".$this->session->userdata('rolIniciado')."&no_orden=".$id_cita);
        // Receive server response ...
        //curl_setopt($ch, CURLOPT_POSTFIELDS,"");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        /*curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Cache-Control": "no-cache",
                "Postman-Token": "6900f9a3-bbd5-4558-af94-c9ac3830948f"
            ));
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_HEADER, 0);*/

        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        $dataregistro = json_decode($response, TRUE);

        $datos["id_cita"] = $id_cita;
        //$datos["ligas"] = ((isset($dataregistro["data"]["url"])) ? $dataregistro["data"]["url"] : NULL);
        $datos["ligas"] = ((isset($dataregistro["data"]["documentacion"])) ? $dataregistro["data"]["documentacion"] : NULL);

        $this->loadAllView($datos,'ordenServicio/listas/formatos_garantias'); 
    }

    public function operaciones_orden_folio($id_orden = '',$datos = NULL)
    {
        $orden = ((ctype_digit($id_orden)) ? $id_orden : $this->decrypt($id_orden));
        $id_cita = $this->mConsultas->recuperar_id_cita($orden);
        //Recuperamos los folios de las garantias
        $datos["folios"] = $this->folios_asociados_garantias($id_cita);
        $datos["reparaciones"] = $this->reparaciones_garantias($id_cita);
        
        //Recuperamos las operaciones
        $datos["operaciones"] = $this->mConsultas->articulosTitulos($orden);

        //Datos de cabecera
        $cabecera = $this->mConsultas->notificacionTecnicoCita($id_cita);
        foreach ($cabecera as $row) {
            $datos["cabecera"] = array(
                "id_cita" => $row->id_cita,
                "folio_externo" => $row->folioIntelisis,
                "serie" => $row->vehiculo_identificacion,
                "placas" => $row->vehiculo_placas,
                "modelo" => $row->vehiculo_modelo,
                "anio" => $row->vehiculo_anio,
                "tecnico" => $row->tecnico,
                "asesor" => $row->asesor,
            );
        } 
        
        $this->loadAllView($datos,'ordenServicio/listas/folio_operaciones');
    }

    public function folios_asociados_garantias($id_cita = '')
    {
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_URL,API_GARANTIA."index.php/garantias/api/folios_asociados_get");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_URL,API_GARANTIA."index.php/garantias/F1863/partes_operaciones_byorden");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,'no_orden='.$id_cita);
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        
        curl_close($ch);

        $dataregistro = json_decode($response, TRUE);
        
        if (isset($dataregistro["data"])) {
            $folios = $dataregistro["data"];
        }else{
            $folios = NULL;
        }

        return $folios;
    }

    public function reparaciones_garantias($id_cita = '')
    {
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_URL,API_GARANTIA."index.php/garantias/api/folios_asociados_get");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_URL,API_GARANTIA."garantias/api/garantias_api_getList");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,'numero_orden_publica='.$id_cita);
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        
        curl_close($ch);

        $dataregistro = json_decode($response, TRUE);
        
        if (isset($dataregistro["data"])) {
            $folios = $dataregistro["data"];
        }else{
            $folios = NULL;
        }

        return $folios;
    }

    public function asociar_folios()
    {
        //$this->mConsultas->update_table_row('citas',array('folio_asociado'=> $_POST["folio"]),'id_cita',$_POST['id_cita']);
        //$this->mConsultas->update_table_row('citas',array('folio_asociado'=> $_POST["folio"]),'id_cita',$_POST['orden_asociada']);

        //Guardamos en articulos orden la asociadion
        $operacion = array(
            "folio_asociado" => $_POST['folio'],
            "n_reparacion_garantia" => $_POST['reparacion'],
        );
        $this->mConsultas->update_table_row('articulos_orden',$operacion,'id',$_POST['renglon']);

        //$this->mConsultas->update_table_row('articulos_orden',array('folio_asociado'=> $_POST["folio"]),'id',$_POST['renglon']);

        //Guardamos la relacion con la reparacion
        $historico = array(
            "id" => NULL,
            "id_articulo_orden" => $_POST['renglon'],
            "folio_asociado" => $_POST['folio'],
            "reparacion" => $_POST['reparacion'],
            "fecha_alta" => date("Y-m-d H:i:s")
        );

        $this->mConsultas->save_register('reparaciones_garantias',$historico);

        //Enviamos notifcacion a garantias
        $query = $this->mConsultas->notificacionTecnicoCita($_POST["id_cita"]); 
        foreach ($query as $row) {
            $tecnico = $row->tecnico;
            $modelo = $row->vehiculo_modelo;
            $placas = $row->vehiculo_placas;
            $asesor = $row->asesor;
        }

        if (isset($tecnico)) {
            $texto = SUCURSAL.". Se asocio una operacion de la Orden: ".$_POST["id_cita"]." al folio de garantía ".$_POST["folio"]." no. de reparacion ".$_POST["reparacion"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
            //Garantias
            $this->sms_general('3316725075',$texto);
            
            //$celular_sms = $this->mConsultas->telefono_asesor($_POST["id_cita"]);
            //$this->sms_general($celular_sms,$texto);

            //$celular = $this->mConsultas->telefono_tecnico($_POST["id_cita"]);
            //$this->sms_general($celular,$texto);
        }

        $this->loadHistory($_POST["id_cita"],"Se asocia la operacion con el id : ".$_POST['renglon']." al folio de garantía ".$_POST["folio"]." no. de reparacion ".$_POST["reparacion"],"Externo");
        echo 1;
        exit();
    }

    public function validar_firmas_pzag($id_cita = ''){
        $total_firmadas = 0;
        $total_pzas = 0;

        $revision = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $idCotizacion = $row->id;
            $acepta_cliente = $row->acepta_cliente;
        }

        if (isset($acepta_cliente)) {
            if (($acepta_cliente == "Si")||($acepta_cliente == "Val")) {
                //Recuperamos las refacciones
                $query_presupuesto = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_multipunto");

                foreach ($query_presupuesto as $row_p){
                    if ($row_p->activo == "1") {
                        //Solo las refacciones aceptadas poe el cliente
                        if (($row_p->autoriza_cliente == "1")&&($row_p->autoriza_jefeTaller == "1")&&($row_p->autoriza_asesor == "1")&&($row_p->pza_garantia  == "0")) {
                            /*
                            if ($row_p->firma_jefetaller_garantia != "") {
                                $total_firmadas++;
                            }
                            $total_pzas++;
                            */
                            
                        }elseif (($row_p->pza_garantia  == "1")&&($row_p->autoriza_garantia == "1")) {
                            if ($row_p->firma_jefetaller_garantia != "") {
                                $total_firmadas++;
                            }
                            $total_pzas++;
                        }
                    }
                }
            }
        } else{
            //Verificamos si no hay presupuesto, probamos otro numero de orden
            $orden_publica = $this->mConsultas->garantia_asociada($id_cita);

            $revision = $this->mConsultas->get_result_field("id_cita",$orden_publica,"identificador","M","presupuesto_registro");
            foreach ($revision as $row) {
                $idCotizacion = $row->id;
                $acepta_cliente = $row->acepta_cliente;
            }

            if (isset($acepta_cliente)) {
                if (($acepta_cliente == "Si")||($acepta_cliente == "Val")) {
                    //Recuperamos las refacciones
                    $query_presupuesto = $this->mConsultas->get_result_field("id_cita",$orden_publica,"identificador","M","presupuesto_multipunto");

                    foreach ($query_presupuesto as $row_p){
                        if ($row_p->activo == "1") {
                            //Solo las refacciones aceptadas poe el cliente
                            if (($row_p->autoriza_cliente == "1")&&($row_p->autoriza_jefeTaller == "1")&&($row_p->autoriza_asesor == "1")&&($row_p->pza_garantia  == "0")) {
                                /*
                                if ($row_p->firma_jefetaller_garantia != "") {
                                    $total_firmadas++;
                                }
                                $total_pzas++;
                                */
                                
                            }elseif (($row_p->pza_garantia  == "1")&&($row_p->autoriza_garantia == "1")) {
                                if ($row_p->firma_jefetaller_garantia != "") {
                                    $total_firmadas++;
                                }
                                $total_pzas++;
                            }
                        }
                    }
                }            
            }
        }

        //echo "<br>ID CITA: ".$id_cita."[ PZA : ".$total_pzas."] [ PZA-F : ".$total_firmadas."] ";
        return (($total_pzas == $total_firmadas) ? 1 : 0);
    }

    public function sms_general($celular_sms='',$mensaje_sms=''){
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        $archivosJs = array(
            "include" => array(
                'assets/js/diagnostico/filtros_ordenes.js',
                'assets/js/diagnostico/filtros_ordenes_cgarantias.js',
				'assets/js/diagnostico/comentariosCO.js',
            )
        );
        
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Documentación de Ordenes"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}
