<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrdenServicio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //Cargamos la libreria de email
        $this->load->library('email');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
    }

    public function index($idCita = 0,$datos = NULL)
  	{
        $id_cita = ((ctype_digit($idCita)) ? $idCita : $this->decrypt($idCita));
        $datos["id_cita"] = $id_cita;
        $datos["empresa"] = SUCURSAL;
        
        //Comprobamos que exista el registro
        $revision = $this->mConsultas->get_result("noServicio",$id_cita,"diagnostico");
        foreach ($revision as $row) {
            $verificacion = $row->idDiagnostico;
        }

        //Si existe el registro cargamos los datos
        if (isset($verificacion)) {
            $this->setData($id_cita,$datos);

        //De lo contrario, consultamos los datos para mostrar
        } else {
            $datos["hoy"] = date("Y-m-d");
            $datos["limiteAno"] = date("Y");
            $datos["diaValue"] = date("d");
            $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
            $datos["mesValue"] = $meses[date("n")-1];
            $datos["hora"] = date("H:i");
            $datos["id_cita"] = $id_cita;
            $datos["tipo_form"] = "1";

            $datos["tipo_servicio"] = $this->mConsultas->servicio_orden($id_cita);
            $datos["motivos_ausencia"] = $this->mConsultas->get_table("cat_autorizacion_ausencia");

            if ($this->session->userdata('rolIniciado')) {
                // $campos = $this->session->userdata('dataUser');
                $datos["pOrigen"] = $this->session->userdata('rolIniciado');
                $datos["asesor"] = $this->session->userdata('nombreUsuario');
            }

            //Revisar si se lleno la carta renuncia con datos
            $ausencia = $this->mConsultas->get_result("id_cita",$id_cita,"carta_renuncia");
            foreach ($ausencia as $row) {
                $datos["autorizacion_ausencia"] = $row->autorizacion_ausencia;
                $datos["motivo"] = $row->motivo_ausencia;
            }

            $this->loadAllView($datos,'ordenServicio/os_fomulario');
        }
  	}
    
// -------------------------------------------------------------------------------------------------------
    public function guardar_formulario($datos = NULL)
    {
        $datos["registro"] = date("Y-m-d H:i:s");
        $respuesta = "";
        //Armamos el array para contener los valores
        if ($_POST["cargaFormulario"] == "1") {
            $dataForm = array(
              "idDiagnostico" => NULL,
              "noServicio" => trim($_POST["orderService"])
            );
        }

        if ((strtoupper($_POST["vehiculo"]) == "NA")||($_POST["vehiculo"] == "")) {
            $dataForm["vehiculo"] = $this->mConsultas->servicio_orden($_POST["orderService"]);
        }else{
            $dataForm["vehiculo"] = $_POST["vehiculo"];
        }
        
        $dataForm["dias"] = $_POST["dias"];
        $dataForm["terminos"] = $_POST["aceptoTermino"];
        if ($_POST["cargaFormulario"] == "1") {
            $dataForm["fecha_diagnostico"] = $_POST["trip_start"];
            $dataForm["fecha_aceptacion"] = $_POST["trip_start_1"];
        }
        $dataForm["firmaDiagnostico"] = $this->base64ToImage($_POST["rutaFirmaDiagnostico"],"firmas");

        $dataForm["firmaConsumidor1"] = $this->base64ToImage($_POST["rutaFirmaAcepta"],"firmas");
        $dataForm["danos"] = ((isset($_POST["danos"])) ? $_POST["danos"] : "0");
        $dataForm["danosImg"] = $this->base64ToImage($_POST["danosMarcas"],"diagramas");

        $dataForm["interiores"] = $this->validateCheck($_POST["interiores"],"S/R");
        $dataForm["cajuela"] = $this->validateCheck($_POST["cajuela"],"S/R");
        $dataForm["exteriores"] = $this->validateCheck($_POST["exteriores"],"S/R");
        $dataForm["documentacion"] = $this->validateCheck($_POST["documentacion"],"S/R");
        $dataForm["articulos"] = ((isset($_POST["articulos"])) ? $_POST["articulos"] : "0");
        $dataForm["cualesArticulos"] = $_POST["cualesArticulos"];
        $dataForm["reporte"] = $_POST["reporte"];
        $dataForm["costo"] = $_POST["costo"];
        $dataForm["diaValue"] = $_POST["diaValue"];
        $dataForm["mesValue"] = $_POST["mesValue"];
        $dataForm["anioValue"] = $_POST["anioValue"];
        
        $dataForm["firmaAsesor"] = $this->base64ToImage($_POST["rutaFirmaAsesor"],"firmas");
        $dataForm["firmaConsumidor2"] = $this->base64ToImage($_POST["rutaFirmaConsumidor"],"firmas");
        $dataForm["nivelGasolina"] = $_POST["nivelGasolina_1"]."/".$_POST["nivelGasolina_2"];
        $dataForm["nombreDiagnostico"] = $_POST["nombreDiagnostico"];
        $dataForm["nombreConsumido1"] = $_POST["consumidor1Nombre"];
        $dataForm["nombreAsesor"] = $_POST["asesorNombre"];
        $dataForm["nombreConsumidor2"] = $_POST["nombreConsumidor2"];

        if ($_POST["cargaFormulario"] == "1") {
            $dataForm["archivoOasis"] = "";
            $dataForm["fechaRegistro"] = date("Y-m-d H:i:s");
            $dataForm["existePrev"] = $this->registrationFirstPart($_POST["orderService"]);
            $dataForm["autorizacion_ausencia"] = ((isset($_POST["autorizacion_ausencia"])) ? "1" : "0");
            $dataForm["motivo_ausencia"] = ((isset($_POST["motivo"])) ? $_POST["motivo"] : "0");
        }

        $dataForm["fecha_actualiza"] = $datos["registro"];

        //Guardamos/Actualizamos el registro
        if ($_POST["cargaFormulario"] == "1") {
            $dataForm["inicio_formulario"] = $_POST["inicio_formulario"];

            //Revisar si ya se guardo el diagnostico
            $revision = $this->mConsultas->get_result("noServicio",$_POST["orderService"],"diagnostico");
            foreach ($revision as $row) {
                $verificacion = $row->idDiagnostico;
            }

            //Si existe el registro cargamos los datos
            if (!isset($verificacion)) {
                $servicio = $this->mConsultas->save_register('diagnostico',$dataForm);
                if ($servicio != 0) {
                    $this->registro_documentacion($_POST["orderService"]);

                    //Si se lleno el diagnostico por ausencia, se actualiza en la orden de servicio
                    //if (isset($_POST["autorizacion_ausencia"])) {
                    if ($dataForm["firmaConsumidor2"] == "") {
                        $contededor["autorizacion_ausencia"] = "1";
                        //Guardamos el registro
                        $actualiza = $this->mConsultas->update_table_row('ordenservicio',$contededor,'id_cita',$_POST["orderService"]);

                        //Guardamos el movimiento
                        $this->loadHistory($_POST["orderService"],"Registro con autorizacion por ausensia","Orden de Servicio (Diagnóstico)");

                        //$this->envio_correo($_POST["orderService"]);
                    //De lo contrario, actualizamos la firma
                    }else{
                        $contededor["firma_diagnostico"] = "1";
                        //Guardamos el registro
                        $actualiza = $this->mConsultas->update_table_row('ordenservicio',$contededor,'id_cita',$_POST["orderService"]);

                        //Guardamos el movimiento
                        $this->loadHistory($_POST["orderService"],"Registro completo","Orden de Servicio (Diagnóstico)");
                    }

                    $this->saveContract($datos);

                }else {
                    $respuesta = "La ocurrio un problema al intentar guardar la orden=".$this->encrypt($_POST["orderService"]);
                    echo $respuesta;
                }
            }else{
                //$respuesta = "YA EXISTE UN DIAGNOSTICO CON ESTE NUMERO DE ORDEN=".$this->encrypt($_POST["orderService"]);
                $respuesta = "A ocurrio un problema al intentar guardar la orden (pre-contrato)=".$this->encrypt($_POST["orderService"]);
                
                $revision = $this->mConsultas->get_result("identificador",$_POST["orderService"],"contrato");
                foreach ($revision as $row) {
                    $veri2 = $row->idContracto;
                }
                if (!isset($veri2)) {
                    $this->saveContract($datos);
                } else {
                    echo $respuesta;
                }
            }
        } else {
            $actualizar = $this->mConsultas->update_table_row('diagnostico',$dataForm,'noServicio',$_POST["orderService"]);
            if ($actualizar) {
                //Guardamos el movimiento
                $this->loadHistory($_POST["orderService"],"Actualización","Orden de Servicio (Diagnóstico)");

                $respuesta = "OK=".$this->encrypt($_POST["orderService"]);
            }else {
                $respuesta = "La ocurrio un problema al intentar actualizar la orden=".$this->encrypt($_POST["orderService"]);
            }

            echo $respuesta;
        }
    }

    //Guardamos el contrato
    public function saveContract($datos = NULL)
    {
        $dataContract = array(
            "idContracto" => NULL, 
            
            "identificador"=> trim($_POST["orderService"]),
            "folio" => $_POST["orderService"], //$_POST["folio"],
            "fecha" => $_POST["fecha"],
            "hora" => $_POST["hora"],
            "empresa" => SUCURSAL,
            "pagoConsumidor" => ((isset($_POST["cantidadAu"])) ? $_POST["cantidadAu"] : 'Por definir'), 
            "condicionalInfo" => ((isset($_POST["ceder"])) ? $_POST["ceder"] : '1'), 
            "condicionalPublicidad" => ((isset($_POST["publicar"])) ? $_POST["publicar"] : '0'),
            "firmaCliente" => $this->base64ToImage($_POST["rutaFirmaConsumidor_2"],"firmas"),
            "nombreDistribuidor " => $_POST["nombreDistribuidor_3"],
            "firmaDistribuidor" => $this->base64ToImage($_POST["rutaFirmaDistribuidor_3"],"firmas"),
            "nombreConsumidor" => $_POST["nombreConsumidor_3"],
            "firmaConsumidor" => $this->base64ToImage($_POST["rutaFirmaConsumidor_3"],"firmas"),
            "aceptaTerminos" => ((isset($_POST["aceptar"])) ? $_POST["aceptar"] : '0'), 
            "noRegistro" => $_POST["numeroR"],
            "noExpediente" => $_POST["expediente"],
            "fechDia" => $_POST["diaN"],
            "fechMes" => $_POST["mesN"],
            "fechAnio" => $_POST["anoN"],
            "terminos_contrato" => ((isset($_POST["terminos_contrato"])) ? "1" : "0"),
            "fechaRegistro" => $datos["registro"],
        );

        $contrato = $this->mConsultas->save_register('contrato',$dataContract);
        if ($contrato != 0) {
            $respuesta = "OK=".$this->encrypt($_POST["orderService"]);
        }else {
            $respuesta = "La ocurrio un problema al intentar guardar la orden=".$this->encrypt($_POST["orderService"]);
        }

        echo $respuesta;
    }
//  ------------------------------------------------------------------------------------------------------
    public function guardar_firmas()
    {
        $datos["registro"] = date("Y-m-d H:i:s");
        //Guardamos las firmas del diagnostico
        $dataForm["firmaConsumidor1"] = $this->base64ToImage($_POST["rutaFirmaAcepta"],"firmas");
        $dataForm["firmaConsumidor2"] = $this->base64ToImage($_POST["rutaFirmaConsumidor"],"firmas");
        $dataForm["fecha_actualiza"] = $datos["registro"];

        $actualizar = $this->mConsultas->update_table_row('diagnostico',$dataForm,'noServicio',$_POST["id_cita"]);
        if ($actualizar) {
            $dataForm_2["firmaCliente"] = $this->base64ToImage($_POST["rutaFirmaConsumidor_2"],"firmas");
            $dataForm_2["firmaConsumidor"] = $this->base64ToImage($_POST["rutaFirmaConsumidor_3"],"firmas");
            $dataForm_2["terminos_contrato"] = ((isset($_POST["terminos_contrato"])) ? "1" : "0");

            $actualiza2 = $this->mConsultas->update_table_row('contrato',$dataForm_2,'identificador',$_POST["id_cita"]);
            if ($actualiza2) {
                //Guardamos el movimiento
                $this->loadHistory($_POST["id_cita"],"Actualizacion","Orden de Servicio (Contrato)");

                $contededor["firma_diagnostico"] = "1";
                //Guardamos el registro
                $actualiza = $this->mConsultas->update_table_row('ordenservicio',$contededor,'id_cita',$_POST["id_cita"]);

                //Guardamos el movimiento
                $this->loadHistory($_POST["id_cita"],"Se firma el diagnóstico por parte del cliente","Orden de Servicio (Diagnóstico)");

                $respuesta = "OK=".$this->encrypt($_POST["id_cita"]);
            } else {
                $respuesta = "La ocurrio un problema al intentar guardar la orden=".$this->encrypt($_POST["id_cita"]);
            }
            
        } else {
            $respuesta = "La ocurrio un problema al intentar actualizar la orden=".$this->encrypt($_POST["id_cita"]);
        }
        echo $respuesta;
    }
//  ------------------------------------------------------------------------------------------------------

    //Creamos el registro para el listado de la documentacion de ordenes
    public function registro_documentacion($id_cita = '')
    {
        //Recuperamos la informacion general
        $informacion_gral = $this->mConsultas->precargaConsulta($id_cita);
        foreach ($informacion_gral as $row){
            //$id_cita = $row->orden_cita;
            $folio_externo = $row->folioIntelisis;
            $fecha_recepcion = $row->fecha_recepcion;
            $vehiculo = $row->vehiculo_modelo;
            $placas = $row->vehiculo_placas;
            $serie = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
            $asesor = $row->asesor;
            $tecnico = $row->tecnico;
            $id_tecnico = $row->id_tecnico;
            $cliente = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
        }

        //Verificamos que la informacion se haya cargado
        if (isset($serie)) {
            $registro = date("Y-m-d H:i:s");

            //Comprobamos que exista el registro
            $revision = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
            foreach ($revision as $row) {
                $verificacion = $row->id;
            }

            //Si existe el registro cargamos los datos
            if (!isset($verificacion)) {
                $contededor = array(
                    "id" => NULL,
                    "id_cita" => $id_cita,
                    "folio_externo" => $folio_externo,
                    "tipo_vita" => 1,
                    "fecha_recepcion" => $fecha_recepcion,
                    "vehiculo" => $vehiculo,
                    "placas" => $placas,
                    "serie" => $serie,
                    "asesor" => $asesor,
                    "tecnico" => $tecnico,
                    "id_tecnico" => $id_tecnico,
                    "cliente" => $cliente,
                    "diagnostico" => "1",
                    "oasis" => "",
                    "multipunto" => "0",
                    "firma_jefetaller" => "0",
                    "presupuesto" => "0",
                    "presupuesto_afectado" => "",
                    "presupuesto_origen" => "0",
                    "voz_cliente" => "0",
                    "evidencia_voz_cliente" => "",
                    "servicio_valet" => "0",
                    "fechas_creacion" => $registro,
                );
                //Guardamos el registro
                $guardado = $this->mConsultas->save_register('documentacion_ordenes',$contededor);
            
            } else {
                $contededor = array(
                    "folio_externo" => $folio_externo,
                    "fecha_recepcion" => $fecha_recepcion,
                    "vehiculo" => $vehiculo,
                    "placas" => $placas,
                    "serie" => $serie,
                    "asesor" => $asesor,
                    "tecnico" => $tecnico,
                    "id_tecnico" => $id_tecnico,
                    "cliente" => $cliente,
                    "diagnostico" => "1",
                );

                $actualizar = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id',$verificacion);
            }
        }
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Verificamos si existe la primera parte de la orden
    public function registrationFirstPart($idOrden = 0)
    {
        $respuesta = "0";
        //Consultamos en la tabla de orden de servicio si existe el registro
        $query = $this->mConsultas->get_result("id_cita",$idOrden,"ordenservicio");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe la primera parte de la orden
            $respuesta = "1";
        }else {
            //Si no existe el dato, entonces no existe la primera parte de la orden
            $respuesta = "0";
        }
        return $respuesta;
    }

    //Cargamos de manera informativa las ordenes y los estatus de estos
    public function listDataStatusOrder($x = "",$datos = NULL)
    {
        //Comprobamos el tipo de usuario que accede a la lista
        if ($this->session->userdata('rolIniciado')) {
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {
                $id = $this->session->userdata('idUsuario');
                $campos = $this->mConsultas->tecnicosOrdenesEstatus($id);
                foreach ($campos as $row) {
                    $date = new DateTime($row->fecha_recepcion);
                    $datos["fechaDiag"][] = $date->format('d-m-Y');

                    // $datos['idRegistro'][] = $row->idDiagnostico;
                    $datos["idOrden"][] = $row->orden_cita;
                    $datos["idIntelisis"][] = $row->folioIntelisis;

                    $datos["tecnico"][] = $row->tecnico;
                    $datos["placas"][] = $row->vehiculo_placas;
                    $datos["modelo"][] = $row->vehiculo_modelo;

                    $datos["status"][] = $row->status;
                    $datos["color"][] = $row->color;

                    $datos["nombreAsesor"][] = $row->asesor;
                    $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
                }            
            } else {
                $campos = $this->mConsultas->listaOrdenesEstatus(); 
                foreach ($campos as $row) {
                    $date = new DateTime($row->fecha_recepcion);
                    $datos["fechaDiag"][] = $date->format('d-m-Y');

                    // $datos['idRegistro'][] = $row->idDiagnostico;
                    $datos["idOrden"][] = $row->orden_cita;
                    $datos["idIntelisis"][] = $row->folioIntelisis;

                    $datos["tecnico"][] = $row->tecnico;
                    $datos["placas"][] = $row->vehiculo_placas;
                    $datos["modelo"][] = $row->vehiculo_modelo;

                    $datos["status"][] = $row->status;
                    $datos["color"][] = $row->color;

                    $datos["nombreAsesor"][] = $row->asesor;
                    $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
                }
            }
        }

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }
        //var_dump($this->session->userdata());

        $this->loadAllView($datos,'ordenServicio/listaOrdenesEstatus');
    }

    //Entrada para firma individual del cliente
    public function setDataClienteFirma($idServicio = 0)
    {
        $datos["destinoVista"] = "FIRMA";
        $datos["tipo_form"] = "4";
        $id_cita = ((ctype_digit($idServicio)) ? $idServicio : $this->decrypt($idServicio));
        $this->setData($id_cita,$datos);
    }

    //Entrada para firma individual del cliente
    public function setDataFormulario($idServicio = 0)
    {
        $datos["destinoVista"] = "Formulario";
        $datos["tipo_form"] = "2";
        $id_cita = ((ctype_digit($idServicio)) ? $idServicio : $this->decrypt($idServicio));
        $this->setData($id_cita,$datos);
    }

    //Comprobamos que al final se genere el pdf
    public function setDataPDF($idServicio = 0)
    {
        $datos["destinoVista"] = "PDF";
        $datos["tipo_form"] = "3";
        $id_cita = ((ctype_digit($idServicio)) ? $idServicio : $this->decrypt($idServicio));
        $this->setData($id_cita,$datos);
    }

    //Comprobamos que al final se genera la vista de revision
    public function setDataRevision($idServicio = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $datos["tipo_form"] = "3";
        $id_cita = ((ctype_digit($idServicio)) ? $idServicio : $this->decrypt($idServicio));
        $this->setData($id_cita,$datos);
    }

    public function setDataVerificar($idServicio='',$datos = NULL)
    {
        $id_cita = ((ctype_digit($idServicio)) ? $idServicio : $this->decrypt($idServicio));
        $this->setData($id_cita,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData($idServicio = 0,$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["id_cita"] = $idServicio;
        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        //Verificamos si existe el registro en la tabla de estados terminados
        $query2 = $this->mConsultas->get_result("idOrden",$idServicio,"estados_pdf");
        foreach ($query2 as $row){
            $datos["completoFormulario"] = $row->ordenServicioPDF_2;
        }

        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
            $datos["tipo_form"] = "2";
        }

        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("noServicio",$idServicio,"diagnostico");
        foreach ($query as $row){
            $fecha = explode(" ",$row->fecha_diagnostico);
            $datos["fecha_diagnostico"] = $fecha[0];
            $fecha_2 = explode(" ",$row->fecha_aceptacion);
            $datos["fecha_aceptacion"] = $fecha_2[0];

            $datos["id_cita"] = $row->noServicio;
            $datos["tipo_servicio"] = $row->vehiculo;
            $datos["dias"] = $row->dias;
            $datos["terminos"] = $row->terminos;
            $datos["firmaDiagnostico"] = (file_exists($row->firmaDiagnostico)) ? $row->firmaDiagnostico : "";
            $datos["firmaConsumidor1"] = (file_exists($row->firmaConsumidor1)) ? $row->firmaConsumidor1 : "";
            $datos["danos"] = $row->danos;
            $datos["danosImg"] = (file_exists($row->danosImg)) ? $row->danosImg : "";

            $datos["interiores"] = explode("_",$row->interiores);
            $datos["cajuela"] = explode("_",$row->cajuela);
            $datos["exteriores"] = explode("_",$row->exteriores);
            $datos["documentacion"] = explode("_",$row->documentacion);

            $datos["articulos"] = $row->articulos;
            $datos["cualesArticulos"] = $row->cualesArticulos;
            $datos["reporte"] = $row->reporte;
            $datos["costo"] = $row->costo;
            $datos["diaValue"] = $row->diaValue;
            $datos["mesValue"] = $row->mesValue;
            $datos["anioValue"] = $row->anioValue;
            $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";
            $datos["firmaConsumidor2"] = (file_exists($row->firmaConsumidor2)) ? $row->firmaConsumidor2 : "";
            $datos["nivelGasolina"] = $row->nivelGasolina;
            $datos["nivelGasolinaDiv"] = explode("/",$row->nivelGasolina);
            $datos["nombreDiagnostico"] = $row->nombreDiagnostico;
            $datos["nombreConsumido1"] = $row->nombreConsumido1;
            $datos["nombreAsesor"] = $row->nombreAsesor;
            $datos["nombreConsumidor2"] = $row->nombreConsumidor2;

            $datos["autorizacion_ausencia"] = $row->autorizacion_ausencia;
            $datos["motivo_ausencia"] = $row->motivo_ausencia;

            $datos["idServicio"] = $row->idDiagnostico;
        }

        $datos["limiteAno"] = date("Y");

        //Consultamos si ya se entrego la unidad
        $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idServicio);

        $this->loadDataContrac($idServicio,$datos);
    }

    //Recuperamos la informacion del contrato
    public function loadDataContrac($idServicio = 0,$datos = NULL)
    {
        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("identificador",$idServicio,"contrato");
        foreach ($query as $row){
            $fecha = new DateTime($row->fecha);
            $datos["fecha"] = $fecha->format('d/m/Y');
            $datos["fecha_f"] = $row->fecha;

            $datos["folio"] = $row->folio;
            $datos["hora"] = $row->hora;
            $datos["empresa"] = $row->empresa;
            $datos["pagoConsumidor"] = $row->pagoConsumidor;
            $datos["condicionalInfo"] = $row->condicionalInfo;
            $datos["condicionalPublicidad"] = $row->condicionalPublicidad;
            $datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : "";
            $datos["nombreDistribuidor"] = $row->nombreDistribuidor;
            $datos["firmaDistribuidor"] = (file_exists($row->firmaDistribuidor)) ? $row->firmaDistribuidor : "";
            $datos["nombreConsumidor"] = $row->nombreConsumidor;
            $datos["firmaConsumidor"] = (file_exists($row->firmaConsumidor)) ? $row->firmaConsumidor : "";
            $datos["aceptaTerminos"] = $row->aceptaTerminos;
            $datos["noRegistro"] = $row->noRegistro;
            $datos["noExpediente"] = $row->noExpediente;
            $datos["fechDia"] = $row->fechDia;
            $datos["fechMes"] = $row->fechMes;
            $datos["fechAnio"] = $row->fechAnio;
            $datos["checkImg"] = base_url().'assets/imgs/Check.png';
            $datos["checkImgA"] = base_url().'assets/imgs/check2.png';
            $datos["terminos_contrato"] = $row->terminos_contrato;
        }

        //Verificamos donde se imprimira la información
        if ($datos["destinoVista"] != "Formulario"){
            $this->loadDataPart1($idServicio,$datos);
            // $this->generatePDF($datos);
        } else {
            $this->loadAllView($datos,'ordenServicio/os_fomulario');
        }
    }

    //Recuperamos la parte de pita
    public function loadDataPart1($idServicio = 0,$datos = NULL)
    {
        //Recuperamos el id de la orden de servicio
        $precarga = $this->mConsultas->get_result('id_cita',$idServicio,'ordenservicio'); 
        foreach ($precarga as $row){
            $idOrden = $row->id;
            $iva = $row->iva/100;
            $datos["orden_id_tipo_orden"] = $row->id_tipo_orden;
            $datos["idIntelisis"] = $row->folioIntelisis;
            $idCategoria = $row->idcategoria;
        }

        $datos["totalRenglones"] = 0;
        $datos["total_items"] = 0;
        $datos["subtotal_items"] = 0;
        $datos["iva_items"] = 0;
        $datos["presupuesto_items"] = 0;
        $datos["orden_anticipo"] = 0;

        if (isset($idOrden)) {
            //Recuperamos el id de la categoria
            $Categoria = $this->mConsultas->get_result('id',$idCategoria,'categorias');
            foreach ($Categoria as $row){
                $datos["orden_categoria"] = $row->categoria;
            }

            //Recuperamos la información de la cita
            $infoCita = $this->mConsultas->citas($idOrden);
            foreach ($infoCita as $row) {
                $datos["orden_id_cita"] = $row->id_cita;
                //Recuperamos todas las campañas del vehiculo
                $campanias = $this->mConsultas->get_result('vin',$row->vehiculo_numero_serie,'campanias');
                $datos["nombreCampania"] = "[";
                foreach ($campanias as $row_1) {
                    $datos["nombreCampania"] .= $row_1->tipo.",";
                }

                $datos["nombreCampania"] .= "]";

                $datos["orden_campania"] = $row->campania;
                $datos["orden_asesor"] = $row->asesor;
                $datos["orden_telefono_asesor"] = $row->telefono_asesor;
                $datos["orden_extension_asesor"] = $row->extension_asesor;
                //torre
                $datos["orden_numero_interno"] = $row->numero_interno."-".$row->color;
                $datos["orden_fecha_recepcion"] = $row->fecha_recepcion;
                $datos["orden_hora_recepcion"] = $row->hora_recepcion;
                $datos["orden_fecha_entrega"] = $row->fecha_entrega;
                $datos["orden_hora_entrega"] = $row->hora_entrega;
                $datos["orden_datos_nombres"] = $row->datos_nombres;
                $datos["orden_datos_apellido_paterno"] = $row->datos_apellido_paterno;
                $datos["orden_datos_apellido_materno"] = $row->datos_apellido_materno;
                $datos["orden_nombre_compania"] = $row->nombre_compania;
                $datos["orden_nombre_contacto_compania"] = $row->nombre_contacto_compania;
                $datos["orden_ap_contacto"] = $row->ap_contacto;
                $datos["orden_am_contacto"] = $row->am_contacto;

                $datos["orden_datos_email"] = $row->datos_email;
                $datos["orden_rfc"] = $row->rfc;
                $datos["orden_correo_compania"] = $row->correo_compania;

                $datos["orden_domicilio"] = $row->calle." ".$row->noexterior." ".$row->nointerior.", ".$row->colonia;
                $datos["orden_municipio"] = $row->municipio;
                $datos["orden_cp"] = $row->cp;
                $datos["orden_estado"] = $row->estado;
                $datos["orden_telefono_movil"] = $row->telefono_movil;
                $datos["orden_otro_telefono"] = $row->otro_telefono;
                $datos["orden_vehiculo_placas"] = $row->vehiculo_placas;
                // $datos["orden_vehiculo_identificacion"] = $row->vehiculo_numero_serie;
                $datos["orden_vehiculo_identificacion"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : "Sin serie";
                $datos["orden_vehiculo_kilometraje"] = $row->km;
                $datos["orden_vehiculo_marca"] =  $row->vehiculo_marca; 
                $datos["orden_vehiculo_anio"] = $row->vehiculo_anio;

                //Otras variables
                $datos["orden_anticipo"] += (float)$row->anticipo;
                $datos["orden_confirmada"] = $row->cotizacion_confirmada;
                $idColor = $row->id_color;

            }

            if (isset($datos["orden_confirmada"])) {
                //Recuperamos el estatus de la cotización
                $datos["orden_confirmada"] = $this->mConsultas->EstatusCotizacionUser($idOrden);
                $items = $this->mConsultas->articulosTitulos($idOrden); 

                // $datos["subtotal_items"] = 0;
                // $datos["total_items"] = 0;

                foreach ($items as $row) {
                    if ($datos["orden_confirmada"] == "1") {
                        $datos["item_descripcion"][] = $row->descripcion;

                        if ($row->tipo == 3) {
                            $datos["gp_items"][] = "GEN.";
                            $datos["op_items"][] = "";

                            $datos["item_precio_unitario"][] = number_format(($row->total/1.16),2);
                        }elseif ($row->tipo == 5) {
                            $datos["gp_items"][] = $row->grupo;
                            $datos["op_items"][] = $row->descripcion;

                            $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                        }elseif ($row->tipo == 4) {
                            $datos["gp_items"][] = $row->grupo;
                            $datos["op_items"][] = $row->tipo_operacion;

                            $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                        }else {
                            $datos["gp_items"][] = $row->grupoDes;
                            $datos["op_items"][] = $row->operacionDes;

                            $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                        }

                        //$datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                        $datos["item_cantidad"][] = number_format($row->cantidad,2);
                        $datos["item_descuento"][] = number_format($row->descuento,2);
                        $datos["item_operacion"][] = ($row->operacion!='') ? 'Si': 'No';
                        $datos["item_total"][] = number_format($row->total,2);

                        // --$subtotal_linea = $row->cantidad * $row->precio_unitario;
                        //-- //$datos["subtotal_items"] += $subtotal_linea;
                        // $datos["subtotal_items"] += $row->total;
                        // $datos["iva_items"] = $datos["subtotal_items"] * 0.16;
                        // $datos["total_items"] = $datos["subtotal_items"] + $datos["iva_items"];
                        // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];

                        //Nvo procedimiento
                        $datos["total_items"] += $row->total;
                        $datos["subtotal_items"] = $datos["total_items"] / 1.16;
                        $datos["iva_items"] = $datos["total_items"] - $datos["subtotal_items"];
                        // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];
                    } else {
                        //Si la cotizacion no esta confirmada, solo lo tipo 1
                        if(($row->tipo == 1)||($row->tipo == 3)||($row->tipo == 5)||($row->tipo == 4)){
                            $datos["item_descripcion"][] = $row->descripcion;

                            if ($row->tipo == 3) {
                                $datos["gp_items"][] = "GEN.";
                                $datos["op_items"][] = "";

                                $datos["item_precio_unitario"][] = number_format(($row->total/1.16),2);
                            }elseif ($row->tipo == 5) {
                                $datos["gp_items"][] = $row->grupo;
                                $datos["op_items"][] = $row->descripcion;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }elseif ($row->tipo == 4) {
                                $datos["gp_items"][] = $row->grupo;
                                $datos["op_items"][] = $row->tipo_operacion;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }else {
                                $datos["gp_items"][] = $row->grupoDes;
                                $datos["op_items"][] = $row->operacionDes;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }

                            //$datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            $datos["item_cantidad"][] = number_format($row->cantidad,2);
                            $datos["item_descuento"][] = number_format($row->descuento,2);
                            $datos["item_operacion"][] = ($row->operacion!='') ? 'Si': 'No';
                            $datos["item_total"][] = number_format($row->total,2);

                            // //$subtotal_linea = $row->cantidad * $row->precio_unitario;
                            // //$datos["subtotal_items"] += $subtotal_linea;
                            // $datos["subtotal_items"] += $row->total;
                            // $datos["iva_items"] = $datos["subtotal_items"] * 0.16;
                            // $datos["total_items"] = $datos["subtotal_items"] + $datos["iva_items"];
                            // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];

                            //Nvo procedimiento
                            $datos["total_items"] += $row->total;
                            $datos["subtotal_items"] = $datos["total_items"] / 1.16;
                            $datos["iva_items"] = $datos["total_items"] - $datos["subtotal_items"];
                            // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];
                        }
                    }
                }

                if (isset($datos["item_total"])) {
                    $datos["totalRenglones"] = count($datos["item_total"]);
                }
            }

            $datos["Coti_1"] = $datos["totalRenglones"];

            //Comprobamos que se cargaran los datos
            if (isset($datos["orden_id_cita"])) {
                //Recuperamos el color
                $queryColor  = $this->mConsultas->get_result('id',$idColor,'cat_colores');
                foreach ($queryColor as $row) {
                    $datos["color"] = $row->color;
                }

                // $datos["color"] = $this->mConsultas->getColorCitaAuto($datos["orden_id_tipo_orden"]);

                //Recuperamos los tecnicos
                $datos["tecnicos"] = $this->mConsultas->tecnicos($datos["orden_id_cita"]);

                //Recuperamos el tipo de orden
                $datos["orden_tipo_orden"] = $this->mConsultas->getTipoOrden($datos["orden_id_tipo_orden"]);
                $datos["orden_identificador"] = $this->mConsultas->getTipoOrdenIdentificacion($datos["orden_id_tipo_orden"]);

                //Recuperamos el dato del anticipo
                // $datos["orden_anticipo"] = $this->mConsultas->getAnticipoOrden($idOrden);
            }
        }

        //Recuperamos la información de la cotizacion Mutlipunto
        $revision = $this->mConsultas->get_result("idOrden",$idServicio,"cotizacion_multipunto");
        foreach ($revision as $row) {
            if ($row->identificador == "M") {
                $idCotizacion = $row->idCotizacion;
                $acepta = $row->aceptoTermino;
                $cuerpo = $this->createCells(explode("|",$row->contenido));
            }
        }

        //Comprobamos que se haya cargado la información
        if (isset($idCotizacion)) {
            //Verificamos la respuesta afirmativa
            if (($acepta == "Si")||($acepta == "Val")) {
                $datos["subtotal_HM"] = 0;
                $datos["presupuesto_HM"] = 0;
                $datos["iva_HM"] = 0;
                $datos["anticipo_HM"] = (float)$row->anticipo;
                $datos["pieza_HM"] = [];

                //Comprobamos si la cotizacion se acepto completa o por partes
                if ($acepta == "Si") {
                    //Recuperamos los elementos de la cotización
                    foreach ($cuerpo as $index => $valor) {
                        if ($cuerpo[$index][1] != "") {
                            //Columnas de la tabla
                            $datos["gp_op_HM"][] = "Refacción";

                            $datos["pieza_HM"][] = $cuerpo[$index][6];
                            $datos["descripcion_HM"][] = $cuerpo[$index][1];
                            // $datos["mo_hm"][] = number_format($cuerpo[$index][3],2);
                            // $datos["precio_mo_hm"][] = number_format($cuerpo[$index][4],2);
                            $datos["mo_hm"][] = $cuerpo[$index][3] * $cuerpo[$index][4];

                            $datos["precio_HM"][] = number_format($cuerpo[$index][2],2);
                            $datos["cantidad_HM"][] = number_format($cuerpo[$index][0],2);

                            $precarga_C = ((float)$cuerpo[$index][0]*(float)$cuerpo[$index][2]) + ((float)$cuerpo[$index][3]*(float)$cuerpo[$index][4]);
                            $datos["total_fila_HM"][] = $precarga_C;
                            //Subtotal sin iva
                            $datos["subtotal_HM"] += $precarga_C;
                            //Obetenemos el iva
                            $datos["iva_HM"] = $datos["subtotal_HM"] * 0.16;
                            //Sacamos el acumulativo final (subtotal + iva)
                            $datos["presupuesto_HM"] = $datos["subtotal_HM"] * 1.16;

                            //$datos["presupuesto_HM"] += $precarga_C;
                            //$datos["iva_HM"] = $datos["presupuesto_HM"] * 0.16;
                            //$datos["subtotal_HM"] = $datos["presupuesto_HM"] - $datos["iva_HM"];
                        }
                    }
                }else {
                    //Recuperamos los elementos de la cotización
                    foreach ($cuerpo as $index => $valor) {
                        if (isset($cuerpo[$index][9])) {
                            if ($cuerpo[$index][9] == "Aceptada") {
                                //Columnas de la tabla
                                $datos["gp_op_HM"][] = "Refacción";

                                $datos["pieza_HM"][] = $cuerpo[$index][6];
                                $datos["descripcion_HM"][] = $cuerpo[$index][1];
                                // $datos["mo_hm"][] = number_format($cuerpo[$index][3],2);
                                // $datos["precio_mo_hm"][] = number_format($cuerpo[$index][4],2);
                                $datos["mo_hm"][] = $cuerpo[$index][3] * $cuerpo[$index][4];

                                $datos["precio_HM"][] = number_format($cuerpo[$index][2],2);
                                $datos["cantidad_HM"][] = number_format($cuerpo[$index][0],2);

                                $precarga_C = ((float)$cuerpo[$index][0]*(float)$cuerpo[$index][2]) + ((float)$cuerpo[$index][3]*(float)$cuerpo[$index][4]);

                                $datos["total_fila_HM"][] = $precarga_C;
                                //Subtotal sin iva
                                $datos["subtotal_HM"] += $precarga_C;
                                //Obetenemos el iva
                                $datos["iva_HM"] = $datos["subtotal_HM"] * 0.16;
                                //Sacamos el acumulativo final (subtotal + iva)
                                $datos["presupuesto_HM"] = $datos["subtotal_HM"] * 1.16;

                                //$datos["presupuesto_HM"] += $precarga_C;
                                //$datos["iva_HM"] = $datos["presupuesto_HM"] * 0.16;
                                //$datos["subtotal_HM"] = $datos["presupuesto_HM"] - $datos["iva_HM"];
                            }
                        }     
                    }
                }

                if (isset($datos["gp_op_HM"])) {
                    //Totales
                    $datos["subtotal_items"] = $datos["subtotal_items"] + $datos["subtotal_HM"];
                    $datos["iva_items"] = $datos["iva_items"] + $datos["iva_HM"];
                    $datos["total_items"] = $datos["total_items"] + $datos["presupuesto_HM"];
                    $datos["orden_anticipo"] += $datos["anticipo_HM"];
                    $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];
                    $datos["totalRenglones"] = $datos["totalRenglones"] + count($datos["gp_op_HM"]);
                }
            }
        }

        //Si no existe el presupuesto, se busca en el nuevo proceso
        //Consultamos su ya se ha generado alguna cotizacion para ese presupuesto
        $revision = $this->mConsultas->get_result_field("id_cita",$idServicio,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $idCotizacion = $row->id;
            $acepta_cliente = $row->acepta_cliente;
            $datos["anticipo_HM"] = (float)$row->anticipo;
        }

        if (isset($acepta_cliente)) {
            $datos["subtotal_HM"] = 0;
            $datos["presupuesto_HM"] = 0;
            $datos["iva_HM"] = 0;
            $datos["pieza_HM"] = [];
            
            if (($acepta_cliente == "Si")||($acepta_cliente == "Val")) {
                //Recuperamos las refacciones
                $query_presupuesto = $this->mConsultas->get_result_field("id_cita",$idServicio,"identificador","M","presupuesto_multipunto");
                foreach ($query_presupuesto as $row_presupuesto){
                    if ($row_presupuesto->activo == "1") {
                        //Solo las refacciones aceptadas poe el cliente
                        if (($row_presupuesto->autoriza_cliente == "1")&&($row_presupuesto->autoriza_jefeTaller == "1")&&($row_presupuesto->autoriza_asesor == "1")) {
                            //Columnas de la tabla
                            $datos["gp_op_HM"][] = "Refacción";

                            $datos["pieza_HM"][] = $row_presupuesto->num_pieza;
                            $datos["descripcion_HM"][] = $row_presupuesto->descripcion;
                            $datos["mo_hm"][] = ($row_presupuesto->horas_mo * $row_presupuesto->costo_mo);

                            $datos["precio_HM"][] = $row_presupuesto->costo_pieza;
                            $datos["cantidad_HM"][] = $row_presupuesto->cantidad;

                            $precarga_C = ((float)$row_presupuesto->cantidad*(float)$row_presupuesto->costo_pieza) + ((float)$row_presupuesto->horas_mo*(float)$row_presupuesto->costo_mo);

                            $datos["total_fila_HM"][] = $precarga_C;
                            //Subtotal sin iva
                            $datos["subtotal_HM"] += $precarga_C;
                            //Obetenemos el iva
                            $datos["iva_HM"] = $datos["subtotal_HM"] * 0.16;
                            //Sacamos el acumulativo final (subtotal + iva)
                            $datos["presupuesto_HM"] = $datos["subtotal_HM"] * 1.16;

                            //$datos["presupuesto_HM"] += $precarga_C;
                            //$datos["iva_HM"] = $datos["presupuesto_HM"] * 0.16;
                            //$datos["subtotal_HM"] = $datos["presupuesto_HM"] - $datos["iva_HM"];
                        } 
                    }
                }

                //Totales
                $datos["subtotal_items"] = $datos["subtotal_items"] + $datos["subtotal_HM"];
                $datos["iva_items"] = $datos["iva_items"] + $datos["iva_HM"];
                $datos["total_items"] = $datos["total_items"] + $datos["presupuesto_HM"];
                $datos["orden_anticipo"] += $datos["anticipo_HM"];
                $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];

                $datos["totalRenglones"] = $datos["totalRenglones"] + count($datos["pieza_HM"]);
            }            
        }

        if ($datos["destinoVista"] == "PDF") {
              $this->generatePDF($datos);
        } elseif ($datos["destinoVista"] == "FIRMA") {
            $this->loadAllView($datos,'ordenServicio/plantillaCliente');
        }else {
            $this->loadAllView($datos,'ordenServicio/plantillaRevision');
        }
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Separar los campos de las tablas
    /*public function divideFields($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }*/

    //Acomodamos el formato de la fecha
    /*public function resetFormatDate($fecha = "",$validador = "")
    {
        $respuesta = "";
        if ($validador == "") {
            $fecha = explode(" ",$fecha);
            $respuesta = $fecha[0];
        }else {
            $fecha = explode(" ",$fecha);
            $fechaFracc = explode("-",$fecha[0]);
            $respuesta = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
        }
        return $respuesta;
    }*/

    //Validar Radio
    /*public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }*/

    //Validar multiple check
    public function validateCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."_";
                }
            }
        }
        return $respuesta;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_DI";
        for($i=0; $i<=6; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }
        }

        return $base;
    }

    //Recibimos video por ajax
    public function videoAjax()
    {
        $respuesta = "";
        $idCita = $_POST["idCita"];
        $comentario = $_POST["comentario"];
        $tipo = $_POST["tipoArchivo"];

        if ($tipo == "videos") {
            $tipoDB = 1;
        }else {
            $tipoDB = 2;
        }

        $archivo = $this->validateDoc("videos");

        if (($idCita != "")&&($archivo != "")) {
            $datos = array(
                'id' => NULL,
                'id_cita' => $idCita,
                'tipo' => $tipoDB,
                'video' => $archivo,
                'comentario' => $comentario,
                'fecha_creacion'=>date('Y-m-d H:i:s')
            );

           $idVideo = $this->mConsultas->save_register('evidencia',$datos);

           if ($idVideo != 0) {
                $respuesta = "OK";
           }else {
                $respuesta = "ERROR";
           }
        }else {
            $respuesta = $archivo;
        }


       echo $respuesta;
    }

    //Validacion de campos para alta/Edicion (ajax)
    public function searchOrden()
    {
        $dato = $_POST['orden'];
        $cadena = "";

        //Verificamos que exista la orden parte 1 abierta
        $query = $this->mConsultas->get_result("id_cita",$dato,"ordenservicio");
        foreach ($query as $row){
            $id_cita = $row->id;
            $tipo_orden = $row->id_tipo_orden;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($id_cita)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = TRUE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = FALSE;
        }

        $query = $this->mConsultas->get_result("noServicio",$dato,"diagnostico");
        foreach ($query as $row){
            $validar = $row->idDiagnostico;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($validar)) {
            $respuesta = "EDITAR=".$this->encrypt($dato)."=";
        }else {
            $complemento = "";
            //Verificamos datos
            /*if (isset($tipo_orden)) {
                $usuario = $this->session->userdata('usuario');
                $txt_tipo_or = $this->consulta->tipo_orden_valida($dato);

                if (count($txt_tipo_or) > 0) {
                    $txt = explode("(",$txt_tipo_or[0]);
                    $txt_or = $txt[0];
                } else {
                    $txt_or = "";
                }
                
                //Verificamos el tipo de orden es interna u holateria
                if (($tipo_orden == 3) || ($tipo_orden == 4)) {
                    //Verificamos si el asesor es valido para el tipo de orden
                    if ((in_array($usuario, $this->config->item('usuarios_admin'))) || (in_array($id_usuario, $this->config->item('user_asetec')))) {
                        $complemento = "";

                    //De lo contrario, no se puede inicar el inventario
                    } else{
                        $complemento = "El usuario no esta permitido para llenar el inventario de tipo: ".$txt_or;
                    }
                } else {
                    //Verificamos si el asesor es valido para el tipo de orden
                    if ((in_array($id_usuario, $this->config->item('usuarios_admin'))) || (!in_array($id_usuario, $this->config->item('user_asetec')))) {
                        $complemento = "";

                    //De lo contrario, no se puede inicar el inventario
                    } else{
                        $complemento = "El usuario no esta permitido para llenar el inventario de tipo: ".$txt_or;
                    }
                }
            }*/
            
            $respuesta = "ALTA=".$this->encrypt($dato)."=".$complemento;
            /*if ($respuesta) {
                $respuesta = "ALTA=".$this->encrypt($dato)."=";
            } else {
                $respuesta = "ALTA=".$this->encrypt($dato)."=No se ha abierto una orden previamente.";
            }*/
        }
        echo $respuesta;
    }

    //Validamos y guardamos los documentos
    public function validateDoc($direcctorio = "")
    {
        $path = "";
        //300 segundos  = 10 minutos
        ini_set('max_execution_time',600);
        //Revisamos que la imagen se envio
        if ($_FILES["archivo"]['name'] != "") {

           $urlDoc = $direcctorio;
           //Generamos un nombre random
           $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
           $urlNombre = date("YmdHi")."_DI";
           for($j=0; $j<=6; $j++ ){
              $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
           }

           //Validamos que exista la carpeta destino   base_url()
           if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
           }

           //Configuramos las propiedades permitidas para la imagen
           $config['upload_path'] = $urlDoc;
           $config['file_name'] = $urlNombre;
           $config['allowed_types'] = "*";
           $config['max_size'] = 0;

           //Cargamos la libreria y la inicializamos
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('archivo')) {
                $data['uploadError'] = $this->upload->display_errors();
                $path .= $this->upload->display_errors();
                // echo $this->upload->display_errors();
                $path .= "";
           }else{
                //Si se pudo cargar la imagen la guardamos
                $fileData = $this->upload->data();
                $ext = explode(".",$_FILES['archivo']['name']);
                if ($urlDoc == "videos") {
                    $path = base_url().$urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                }else {
                    $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                }
           }
        }else{
            $path = "";
        }
        return $path;
    }

    //Llamamos a la vista para cargar el modal de archivo
    public function modalArchivo($idOrden = 0,$datos = NULL)
    {
        $datos["idOrden"] = $idOrden;

        //Comprobamos si ya existe un documento para ese archivo
        $query = $this->mConsultas->get_result("id_cita",$datos["idOrden"],"ordenservicio");
        foreach ($query as $row){
            $datos["archivoInventario"] = $row->archivoInventario;
            $datos["archivos"] = explode("|",$row->archivoInventario);
        }

        $this->loadAllView($datos,'modulosExternos/archivoOSP1');
    }

    //Recuperamos el formulario, lo validamos y guardamos
    public function validateModalArchivo()
    {
        $datos["idOrden"] = $this->input->post("idOrden");

        $this->form_validation->set_rules('archivo', '<br>No se cargo el archivo correctamente.', 'callback_archivo');
        $this->form_validation->set_rules('idOrden', 'Falta indicar.', 'required');

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('archivo','Faltan características.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Guardamos el archivo (s)
            if($this->input->post("tempFile") != "") {
                $archivos_2 = $this->validateDocOasis(2);
                $dataArchivo["archivoInventario"] = $this->input->post("tempFile")."|".$archivos_2;
            }else {
                $dataArchivo["archivoInventario"] = $this->validateDocOasis(2);
            }

            if ($dataArchivo["archivoInventario"] != "") {
                $actualiza = $this->mConsultas->update_table_row('ordenservicio',$dataArchivo,'id_cita',$datos["idOrden"]);

                if ($actualiza) {
                    $datos["mensaje"] = "1";
                }else {
                    $datos["mensaje"] = "2";
                }
            }else {
                $datos["mensaje"] = "0";
            }
            // $this->modalOasis($datos["idOrden"],$datos);
        }else{
            $datos["mensaje"] = "0";
            // $this->modalOasis($datos["idOrden"],$datos);
        }

        $this->modalArchivo($datos["idOrden"],$datos);
    }

    //Llamamos a la vista modal para cargar el archivo de oasis ford
    public function modalOasis($idOrden = 0,$datos = NULL)
    {
        $idOrden = trim($idOrden);
        $datos["idOrden"] = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));

        //Comprobamos si ya existe un documento para ese archivo
        $query = $this->mConsultas->get_result("noServicio",$datos["idOrden"],"diagnostico");
        foreach ($query as $row){
            $datos["archivoOasisL"] = $row->archivoOasis;
            $datos["archivos"] = explode("|",$row->archivoOasis);
        }

        //Verificamos que exista el diagnostico correspondiente
        if (isset($datos["archivoOasisL"])) {
            $tipo_orden = $this->mConsultas->tipo_servicio($datos["idOrden"]);

            //Revisamos si la orden requiere voz cliente
            if (($tipo_orden == "1")||(($tipo_orden == "5"))) {
                //Verificamos si existe la voz cliente
                $validar_vc = $this->validar_registro($datos["idOrden"]);

                //Interpretamos la respuesta de la consulta
                if ($validar_vc != 1) {
                    $datos["existe"] = 1;
                }else {
                    $datos["existe"] = 3;
                }
            }else{
                $datos["existe"] = 1;
            }
        }else {
            $datos["existe"] = 0;
        }

        $this->loadAllView($datos,'ordenServicio/archivoOasis');
    }

    public function validar_registro($id_cita='')
    {
        $respuesta = 1;
        $query = $this->mConsultas->get_result("idPivote",$id_cita,"voz_cliente");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        if (isset($dato)) {
            $respuesta = 3;
        }else{
            $query = $this->mConsultas->get_result("id_cita",$id_cita,"vc_registro");
            foreach ($query as $row){
                $dato2 = $row->id;
            }

            if (isset($dato2)) {
                $respuesta = 2;
            }else{
                $respuesta = 1;
            }
        }

        return $respuesta;
    }

    //Recuperamos el formulario, lo validamos y guardamos
    public function validateModalOasis()
    {
        $datos["idOrden"] = $this->input->post("idOrden");

        $this->form_validation->set_rules('archivo', '<br>No se cargo el archivo correctamente.', 'callback_archivo');
        $this->form_validation->set_rules('idOrden', 'Falta indicar.', 'required');

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('archivo','Faltan características.');
        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Guardamos el archivo (s)
            if($this->input->post("tempFile") != "") {
                $archivos_2 = $this->validateDocOasis(1);
                $dataArchivo["archivoOasis"] = $this->input->post("tempFile")."|".$archivos_2;
            }else {
                $dataArchivo["archivoOasis"] = $this->validateDocOasis(1);
            }

            if ($dataArchivo["archivoOasis"] != "") {

                $actualiza = $this->mConsultas->update_table_row('diagnostico',$dataArchivo,'noServicio',$datos["idOrden"]);

                if ($actualiza) {
                    //Actualizamos la información para el historial de ordenes
                    $this->actualiza_documentacion($datos["idOrden"],$dataArchivo["archivoOasis"]);

                    $datos["mensaje"] = "1";
                }else {
                    $datos["mensaje"] = "2";
                }
            }else {
                $datos["mensaje"] = "0";
            }
            // $this->modalOasis($datos["idOrden"],$datos);
        }else{
            $datos["mensaje"] = "0";
            // $this->modalOasis($datos["idOrden"],$datos);
        }
        $this->modalOasis($datos["idOrden"],$datos);
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion($id_cita = '',$archivo = "")
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "oasis" => $archivo
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        
        //Si no existe el registro, lo creamos
        }else{
            //Recuperamos la informacion general
            $informacion_gral = $this->mConsultas->precargaConsulta($id_cita);
            foreach ($informacion_gral as $row){
                //$id_cita = $row->orden_cita;
                $folio_externo = $row->folioIntelisis;
                $fecha_recepcion = $row->fecha_recepcion;
                $vehiculo = $row->vehiculo_modelo;
                $placas = $row->vehiculo_placas;
                $serie = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
                $asesor = $row->asesor;
                $tecnico = $row->tecnico;
                $id_tecnico = $row->id_tecnico;
                $cliente = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
                //Recuperamos el tipo de orden
                $tipo_orden = $row->id_tipo_orden;
            }

            $contededor = array(
                "id" => NULL,
                "id_cita" => $id_cita,
                "folio_externo" => $folio_externo,
                "tipo_vita" => ((($tipo_orden == 4) || ($tipo_orden == 3)) ? 2 : 1),
                "fecha_recepcion" => $fecha_recepcion,
                "vehiculo" => $vehiculo,
                "placas" => $placas,
                "serie" => $serie,
                "asesor" => $asesor,
                "tecnico" => $tecnico,
                "id_tecnico" => $id_tecnico,
                "cliente" => $cliente,
                "diagnostico" => "1",
                "oasis" => $archivo,
                "multipunto" => "0",
                "firma_jefetaller" => "0",
                "presupuesto" => "0",
                "presupuesto_afectado" => "",
                "presupuesto_origen" => "0",
                "voz_cliente" => "0",
                "evidencia_voz_cliente" => "",
                "servicio_valet" => "0",
                "fechas_creacion" => date("Y-m-d H:i:s"),
            );
            //Guardamos el registro
            $guardado = $this->mConsultas->save_register('documentacion_ordenes',$contededor);
        }
    }

    //Verificamos la existencia del archivo
    public function archivo()
    {
        if ($_FILES['archivo']['name'] != NULL) {
            $retorno = TRUE;
        }else {
            $retorno = FALSE;
        }

        return $retorno;
    }

    //Validamos y guardamos los documentos del oasis
    public function validateDocOasis($anexo = 1)
    {
        $path = "";
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
        for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES['archivo']['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES['archivo']['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES['archivo']['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES['archivo']['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docOasis";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_OA";
            for($j=0; $j<=6; $j++ ){
                $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
                mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 if ($anexo == 2) {
                     $path .= base_url().$urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 }else {
                     $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 }
                 // $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }

    //Busqueda de la orden se servicio que se llenara
    public function validateOrderRegistre()
    {
        $idOrden = $_POST['orden'];
        $cadena = "";
        $query = $this->mConsultas->get_result("id_cita",$idOrden,"ordenservicio");
        foreach ($query as $row){
            $validar = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($validar)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "1";
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "0";
        }
        echo $respuesta;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("ordenServicio/plantillaPdf", $datos); 
    } 

    /*public function envio_correo($idOrden = 0)
    {
        //Definimos la fecha de envio
        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","viernes","Sábado");
        $datosEmail["dia"] = $dias[date("w")];
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $datosEmail["mes"] = $meses[date("n")-1];

        //Recuperamos la informacion a enviar
        $query = $this->mConsultas->precargaConsulta($idOrden);
        foreach ($query as $row){
            $datosEmail["correo1"] = $row->datos_email;
            $datosEmail["correo2"] = $row->correo_compania;
            $datosEmail["nombre"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            $datosEmail["companiaNombre"] = $row->nombre_compania;
            $datosEmail["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
        }

        //Url del archivo
        $datosEmail["url"] = base_url()."Documentacion_PorFirmar/".$this->encrypt($idOrden);
        $datosEmail["formularioEnvio"] = "Orden de Servicio - Diagnóstico";
        //Cargamos la informacion en la plnatilla
        $htmlContent = $this->load->view("plantillaEmailAusencia", $datosEmail, TRUE);

        $configGmail = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => EMAIL_SERVER,
            'smtp_pass' => EMAIL_PASS,
            'mailtype' => 'html',
            'smtp_crypto'=> 'tls',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );

        //Establecemos esta configuración
        $this->email->initialize($configGmail);
        //Ponemos la dirección de correo que enviará el email y un nombre
        $this->email->from(EMAIL_SERVER, SUCURSAL);
        //Ponemos la direccion de aquien va dirigido
        //$this->email->to($datosEmail["correo1"],$datosEmail["nombre"]);
        //$this->email->to($datosEmail["correo2"],$datosEmail["nombre"]);
        $this->email->to('desarrollo@sohex.mx','Tester');

        //Multiples destinos
        // $this->email->to($correo1, $correo2, $correo3);
        //Con copia a:
        // $this->email->cc($correo);
        //Con copia oculta a:
        // $this->email->bcc($correo);
        //$this->email->bcc('info@planificadorempresarial.mx','Angel');
        $this->email->bcc('info@sohex.mx','Angel');
        //Colocamos el asunto del correo
        $this->email->subject('Diagnóstico Autorizado por Ausencia ()');
        //Cargamos la plantilla del correo
        $this->email->message($htmlContent);
        //Enviar archivo adjunto
        // $this->email->attach('files/attachment.pdf');
        //Enviamos el correo
        if($this->email->send()){
            $envio = 1;
        }else{
            $envio = 2;
        }

        return $envio;
    }*/

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/diagnostico/envio_formulario.js',
                'assets/js/diagnostico/interracciones.js',
                'assets/js/firmasMoviles/firmas_VC.js',
                'assets/js/diagnostico/cargaVideo.js',
                'assets/js/ordenServicioJs.js',

                //'assets/js/diagnostico/filtrosListaOrden.js',
                'assets/js/interaccionCheckOS.js',
                'assets/js/reglasBloqueo.js',
                'assets/js/superUsuario.js'
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Orden de servicio"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}
