<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class OrdenServicio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
    }

    public function index($idCita = 0,$datos = NULL)
  	{
        $datos["hoy"] = date("Y-m-d");
        $datos["limiteAno"] = date("Y");
        $datos["dia"] = date("d");
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $datos["mes"] = $meses[date("n")-1];
        $datos["hora"] = date("H:i");

        $id_cita = (($idCita == "0") ? "0" : $this->decrypt($idCita));
        $datos["idCita"] = $id_cita;

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
            $datos["asesor"] = $this->session->userdata('nombreUsuario');
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

  		$this->loadAllView($datos,'ordenServicio/ordenServicio');
  	}

    //Validamos la infromacion recibida de la vista
    public function validateView()
    {
        $datos["tipoRegistro"] = $this->input->post("cargaFormulario");
        $datos["idServicio"] = $this->input->post("idServicio");
        $datos["servicioId"] = $this->input->post("orderService");
        $datos["pOrigen"] = $this->input->post("pOrigen");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["servicioId"]);

        if ($datos["tipoRegistro"] == "1") {
            $this->form_validation->set_rules('orderService', 'La orden de servicio es requerida.', 'required|callback_existencia|callback_ordenprevia');
            //Validamos las firmas
            $this->form_validation->set_rules('rutaFirmaDiagnostico', 'Firma requerida.', 'required');
            $this->form_validation->set_rules('rutaFirmaAcepta', 'Firma requerida.', 'required');
            $this->form_validation->set_rules('rutaFirmaAsesor', 'Firma requerida.', 'required');
            $this->form_validation->set_rules('rutaFirmaConsumidor', 'Firma requerida.', 'required');
            //Validacion de nombres de quienes firman
            $this->form_validation->set_rules('nombreDiagnostico', 'Nombre de quien elaboro el diagnóstico requerido', 'required');
            $this->form_validation->set_rules('consumidor1Nombre', 'Nombre del consumidor requerido', 'required');
            $this->form_validation->set_rules('asesorNombre', 'Nombre del asesor requerido', 'required');
            $this->form_validation->set_rules('nombreConsumidor2', 'Nombre del consumidor requerido', 'required');
            // $this->form_validation->set_rules('nivelGasolina', 'Falta indicar.', 'required');
            $this->form_validation->set_rules('nivelGasolina_1', 'Falta indicar nivel de gasolina.', 'required');
            $this->form_validation->set_rules('nivelGasolina_2', '<br>Falta indicar nivel de gasolina.', 'required');
            $this->form_validation->set_rules('trip_start', 'Falta indicar.', 'required');
            $this->form_validation->set_rules('trip_start_1', 'Falta indicar.', 'required');

            //Validamos los campos del contrato
            $this->form_validation->set_rules('folio', 'El folio es requerido.', 'required');
            $this->form_validation->set_rules('aceptar', 'Debe aceptar los terminos', 'required');
            $this->form_validation->set_rules('rutaFirmaConsumidor_2', 'Firma requerida.', 'required');
            $this->form_validation->set_rules('nombreDistribuidor_3', 'Nombre del distribuidor requerido.', 'required');
            $this->form_validation->set_rules('rutaFirmaDistribuidor_3', 'Firma requerida.', 'required');
            $this->form_validation->set_rules('nombreConsumidor_3', 'Nombre del consumidor requerido.', 'required');
            $this->form_validation->set_rules('rutaFirmaConsumidor_3', 'Firma requerida.', 'required');
        }

        $this->form_validation->set_rules('interiores', '<br>Las Características interiores son requeridas.', 'callback_interiores');
        $this->form_validation->set_rules('cajuela', '<br>Falta opciones.', 'callback_cajuela');
        // $this->form_validation->set_rules('exteriores', 'Falta indicar.', 'required');
        // $this->form_validation->set_rules('documentacion', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('articulos', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('costo', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('diaValue', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('mesValue', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('anioValue', 'Falta indicar.', 'required');

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('interiores','Faltan características.');
        $this->form_validation->set_message('cajuela','Faltan características de cajuela.');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar el No. de Orden.');
        $this->form_validation->set_message('ordenprevia','No se ha abierto una orden previamente.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";
            $this->index('0',$datos);
        }
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia() 
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("orderService");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("noServicio",$id_cita,"diagnostico");
        foreach ($query as $row){
            $dato = $row->idDiagnostico;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Validamos si existe un registro con ese numero de orden
    public function ordenprevia() 
    {
        $respuesta = TRUE;
        $id_cita = $this->input->post("orderService");
        //Verificamos que exista la orden parte 1 abierta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"ordenservicio");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = TRUE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = FALSE;
        }
        return $respuesta;
    }

    //Validar requisitos minimos P1
    public function interiores()
    {
        $return = FALSE;
        if ($this->input->post("interiores")) {
            if (count($this->input->post("interiores")) >20){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar requisitos minimos P1
    public function cajuela()
    {
        $return = FALSE;
        if ($this->input->post("cajuela")) {
            if (count($this->input->post("cajuela")) >3){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Empaquetamos los datos y enviamos al servidor
    public function getDataForm($datos = NULL)
    {
        $datos["registro"] = date("Y-m-d H:i:s");

        //Armamos el array para contener los valores
        if ($datos["tipoRegistro"] == "1") {
            $dataForm = array(
              "idDiagnostico" => NULL,
              "noServicio" => $datos["servicioId"],
            );
        }

        $dataForm["vehiculo"] = $this->input->post("vehiculo");
        $dataForm["dias"] = $this->input->post("dias");
        $dataForm["terminos"] = $this->validateRadio($this->input->post("aceptoTermino"),"0");

        if ($datos["tipoRegistro"] == "1") {
            $dataForm["fecha_diagnostico"] = $this->input->post("trip_start");
            $dataForm["fecha_aceptacion"] = $this->input->post("trip_start_1");
            $dataForm["firmaDiagnostico"] = $this->base64ToImage($this->input->post("rutaFirmaDiagnostico"),"firmas");
            // $dataForm["firmaDiagnosticoBase"] = $this->input->post("rutaFirmaDiagnostico");
            $dataForm["firmaConsumidor1"] = $this->base64ToImage($this->input->post("rutaFirmaAcepta"),"firmas");
            // $dataForm["firmaConsumidorBase1"] = $this->input->post("rutaFirmaAcepta");
            $dataForm["danos"] = $this->validateRadio($this->input->post("danos"),"0");
            $dataForm["danosImg"] = $this->base64ToImage($this->input->post("danosMarcas"),"diagramas");
        }

        // $dataForm["danosImgBase"] = $this->input->post("danosMarcas");
        $dataForm["interiores"] = $this->validateCheck($this->input->post("interiores"),"S/R");
        // $dataForm["cajuelaValida"] = $this->validateRadio($this->input->post("cajuReq"),"0");
        $dataForm["cajuela"] = $this->validateCheck($this->input->post("cajuela"),"S/R");
        // $dataForm["exteriorValida"] = $this->validateRadio($this->input->post("exteReq"),"0");
        $dataForm["exteriores"] = $this->validateCheck($this->input->post("exteriores"),"S/R");
        // $dataForm["docuValida"] = $this->validateRadio($this->input->post("docReq"),"0");
        $dataForm["documentacion"] = $this->validateCheck($this->input->post("documentacion"),"S/R");
        $dataForm["articulos"] = $this->validateRadio($this->input->post("articulos"),"0");
        $dataForm["cualesArticulos"] = $this->input->post("cualesArticulos");
        $dataForm["reporte"] = $this->input->post("reporte");
        $dataForm["costo"] = $this->input->post("costo");
        $dataForm["diaValue"] = $this->input->post("diaValue");
        $dataForm["mesValue"] = $this->input->post("mesValue");
        $dataForm["anioValue"] = $this->input->post("anioValue");

        if ($datos["tipoRegistro"] == "1") {
            $dataForm["firmaAsesor"] = $this->base64ToImage($this->input->post("rutaFirmaAsesor"),"firmas");
            // $dataForm["firmaAsesorBase"] = $this->input->post("rutaFirmaAsesor");
            $dataForm["firmaConsumidor2"] = $this->base64ToImage($this->input->post("rutaFirmaConsumidor"),"firmas");
            // $dataForm["firmaConsumidorBase2"] = $this->input->post("rutaFirmaConsumidor");
            $dataForm["nivelGasolina"] = $this->input->post("nivelGasolina_1")."/".$this->input->post("nivelGasolina_2");

            $dataForm["nombreDiagnostico"] = $this->input->post("nombreDiagnostico");
            $dataForm["nombreConsumido1"] = $this->input->post("consumidor1Nombre");
            $dataForm["nombreAsesor"] = $this->input->post("asesorNombre");
            $dataForm["nombreConsumidor2"] = $this->input->post("nombreConsumidor2");
            $dataForm["archivoOasis"] = "";
            $dataForm["fechaRegistro"] = $datos["registro"];
            $dataForm["fecha_actualiza"] = $datos["registro"];
            $dataForm["existePrev"] = $this->registrationFirstPart($datos["servicioId"]);
        }

        //Guardamos/Actualizamos el registro
        if ($datos["tipoRegistro"] == "1") {
            //Comprobamos si es un diagnostico con cita o sin citas
            if ($datos["servicioId"] != "0") {
                //Con cita
                $servicio = $this->mConsultas->save_register('diagnostico',$dataForm);
                if ($servicio != 0) {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["servicioId"],"Registro","Orden de Servicio (Diagnostico)");

                    //Si el diagnostico se guardo, creamos el registro para la documentacion
                    $this->registro_documentacion($dataForm["noServicio"]);

                    $datos["mensaje"] = "1";

                    //Verificamos si ya se firmoaron todos los campos
                    $this->chekingFirm($datos["idServicio"],$datos["servicioId"]);

                    $this->saveContract($datos);
                }else {
                    $datos["mensaje"] = "2";
                    $this->index("0",$datos);
                }
            }else {
                //Sin cita
                $folio = $this->generateFolio();
                $dataForm["noServicio"] = $folio;
                $datos["servicioId"] = $folio;
                //$dataForm["fechaRegistro"] = $datos["registro"];
                $dataForm["fecha_actualiza"] = $datos["registro"];
                //Sin cita
                $servicio = $this->mConsultas->save_register('diagnostico_sc',$dataForm);
                if ($servicio != 0) {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["servicioId"],"Registro temporal","Orden de Servicio (temporal)");

                    $datos["mensaje"] = "1";
                    $this->saveContract($datos);
                }else {
                    $datos["mensaje"] = "2";
                    $this->index("0",$datos);
                }
            }
        } else {
            $actualizar = $this->mConsultas->update_table_row('diagnostico',$dataForm,'idDiagnostico',$datos["idServicio"]);
            if ($actualizar) {
                //Guardamos el movimiento
                $this->loadHistory($datos["servicioId"],"Actualización","Orden de Servicio (Diagnostico)");

                //Verificamos si ya se firmoaron todos los campos
                $this->chekingFirm($datos["servicioId"],$datos["servicioId"]);

                $datos["mensaje"]="1";
                $this->setData($datos["servicioId"],$datos);
            }else {
                $datos["mensaje"]="2";
                $this->setData($datos["servicioId"],$datos);
            }
        }
    }

    //Creamos el registro para el listado de la documentacion de ordenes
    public function registro_documentacion($id_cita = '')
    {
        //Recuperamos la informacion general
        $informacion_gral = $this->mConsultas->precargaConsulta($id_cita);
        foreach ($informacion_gral as $row){
            //$id_cita = $row->orden_cita;
            $folio_externo = $row->folioIntelisis;
            $fecha_recepcion = $row->fecha_recepcion;
            $vehiculo = $row->vehiculo_modelo;
            $placas = $row->vehiculo_placas;
            $serie = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
            $asesor = $row->asesor;
            $tecnico = $row->tecnico;
            $id_tecnico = $row->id_tecnico;
            $cliente = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
        }

        //Verificamos que la informacion se haya cargado
        if (isset($serie)) {
            $registro = date("Y-m-d H:i:s");

            $contededor = array(
                "id" => NULL,
                "id_cita" => $id_cita,
                "folio_externo" => $folio_externo,
                "fecha_recepcion" => $fecha_recepcion,
                "vehiculo" => $vehiculo,
                "placas" => $placas,
                "serie" => $serie,
                "asesor" => $asesor,
                "tecnico" => $tecnico,
                "id_tecnico" => $id_tecnico,
                "cliente" => $cliente,
                "diagnostico" => "1",
                "oasis" => "",
                "multipunto" => "0",
                "firma_jefetaller" => "0",
                "presupuesto" => "0",
                "presupuesto_afectado" => "",
                "presupuesto_origen" => "0",
                "voz_cliente" => "0",
                "evidencia_voz_cliente" => "",
                "servicio_valet" => "0",
                "fechas_creacion" => $registro,
            );

            //Guardamos el registro
            $guardado = $this->mConsultas->save_register('documentacion_ordenes',$contededor);
        }
    }

    //Revisamos si ya se firmaron todos los campos
    public function chekingFirm($id = 0,$idOrden = 0)
    {
        $registro = date("Y-m-d H:i:s");
        //Recuperamos los campos necesarios
        $query = $this->mConsultas->get_result("noServicio",$id,"diagnostico");
        foreach ($query as $row){
            $firmaDiagnostico = $row->firmaDiagnostico;
            $firmaConsumidor1 = $row->firmaConsumidor1;
            $firmaAsesor = $row->firmaAsesor;
            $firmaConsumidor2 = $row->firmaConsumidor2;
        }

        if (isset($firmaAsesor)) {
            //Verificamos si existe el registro en la tabla
            $query2 = $this->mConsultas->get_result("idOrden",$idOrden,"estados_pdf");
            foreach ($query2 as $row){
                $multipunto = $row->multipuntoPDF;
                $idPdf = $row->idRegistro;
            }

            if (isset($multipunto)) {
                //Comprobamos los estados validos (actualizar)
                if (($firmaDiagnostico != "")&&($firmaConsumidor1 != "")&&($firmaAsesor != "")&&($firmaConsumidor2 != "")) {
                    $data = array(
                        "ordenServicioPDF_2" => 1,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                  $data = array(
                      "ordenServicioPDF_2" => 0,
                      "fecha_actualizacion" => $registro,
                  );
                }
                $this->mConsultas->update_table_row('estados_pdf',$data,'idRegistro',$idPdf);
            //Si no existe el registro se cres
            }else {
                //Comprobamos los estados validos (registro)
                if (($firmaDiagnostico != "")&&($firmaConsumidor1 != "")&&($firmaAsesor != "")&&($firmaConsumidor2 != "")) {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 1,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 0,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }
                $this->mConsultas->save_register('estados_pdf',$data);
            }
        }

        return FALSE;
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Verificamos si existe la primera parte de la orden
    public function registrationFirstPart($idOrden = 0)
    {
        $respuesta = "0";
        //Consultamos en la tabla de orden de servicio si existe el registro
        $query = $this->mConsultas->get_result("id_cita",$idOrden,"ordenservicio");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe la primera parte de la orden
            $respuesta = "1";
        }else {
            //Si no existe el dato, entonces no existe la primera parte de la orden
            $respuesta = "0";
        }
        return $respuesta;
    }

    //Funcion para generar el folio de la venta
    public function generateFolio()
    {
        //Recuperamos el indice para el folio
        $fecha = date("Ymd");
        $registro = $this->mConsultas->last_id_table();
        $row = $registro->row();
        if (isset($row)){
          $indexFolio = $registro->row(0)->idDiagnostico;
        }else {
          $indexFolio = 0;
        }

        $nvoFolio = $indexFolio + 1;
        if ($nvoFolio < 10) {
            $folio = "00".$nvoFolio."-".$fecha;
        } elseif(($nvoFolio > 9) && ($nvoFolio < 100)) {
            $folio = "0".$nvoFolio."-".$fecha;
        }else {
            $folio = $nvoFolio."-".$fecha;
        }
        return $folio;
    }

    //Guardamos el contrato
    public function saveContract($datos = NULL)
    {
        $dataContract = array(
            "idContracto" => NULL,
            // "fax"=>$this->input->post("fax"),
            "identificador"=> $datos["servicioId"],
            "folio" => $this->input->post("folio"),
            "fecha" => $this->input->post("fecha"),
            "hora" => $this->input->post("hora"),
            "empresa" => $this->input->post("empresa"),
            "pagoConsumidor" => $this->input->post("cantidadAu"),
            "condicionalInfo" => $this->validateRadio($this->input->post("ceder"),"0"),
            "condicionalPublicidad" => $this->validateRadio($this->input->post("publicar"),"0"),
            "firmaCliente" => $this->base64ToImage($this->input->post("rutaFirmaConsumidor_2"),"firmas"),
            // "firmaClienteBase" => $this->input->post("rutaFirmaConsumidor_2"),
            "nombreDistribuidor " => $this->input->post("nombreDistribuidor_3"),
            "firmaDistribuidor" => $this->base64ToImage($this->input->post("rutaFirmaDistribuidor_3"),"firmas"),
            // "firmaDistribuidorBase" => $this->input->post("rutaFirmaDistribuidor"),
            "nombreConsumidor" => $this->input->post("nombreConsumidor_3"),
            "firmaConsumidor" => $this->base64ToImage($this->input->post("rutaFirmaConsumidor_3"),"firmas"),
            // "firmaConsumidorBase" => $this->input->post("rutaFirmaConsumidor"),
            "aceptaTerminos" => $this->validateRadio($this->input->post("aceptar"),"0"),
            "noRegistro" => $this->input->post("numeroR"),
            "noExpediente" => $this->input->post("expediente"),
            "fechDia" => $this->input->post("diaN"),
            "fechMes" => $this->input->post("mesN"),
            "fechAnio" => $this->input->post("anoN"),
            "fechaRegistro" => $datos["registro"],
        );

        $contrato = $this->mConsultas->save_register('contrato',$dataContract);
        if ($contrato != 0) {
            $datos["mensaje"] = "1";
            redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
        }else {
            $datos["mensaje"] = "2";
            $this->index("0",$datos);
        }
    }

    //Recuperamos los registros para listarlos
    public function listData($x = "",$datos = NULL) 
    {
        $datos["idOrden"] = [];

        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "GENERAL";
        }

        if (!isset($datos["tipo_vista"])) {
            $datos["tipo_vista"] = "LISTA";
        }

        //Comprobamos el tipo de usuario que accede a la lista
        if ($this->session->userdata('rolIniciado')) {
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {
                $id = $this->session->userdata('idUsuario');
                $campos = $this->mConsultas->tecnicosOrdenes($id);
                foreach ($campos as $row) {
                    //if (!in_array($row->orden_cita,$datos["idOrden"])) {

                        /*$corte_b = explode("-",$row->fecha_recepcion);
                        if (count($corte_b)>1) {
                            $datos["fechaDiag"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0];
                        }else{
                            $datos["fechaDiag"][] = "";
                        } */

                        $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');
                        $datos["fechaDiag"][] = $fecha->format('d-m-Y');
                        
                        $datos["idOrden"][] = $row->orden_cita;
                        $datos["id_cita_url"][] = $this->encrypt($row->orden_cita);
                        $datos["idIntelisis"][] = $row->folioIntelisis;

                        $datos["tecnico"][] = $row->tecnico;
                        $datos["placas"][] = $row->vehiculo_placas;
                        $datos["modelo"][] = $row->vehiculo_modelo;

                        $datos["nombreAsesor"][] = $row->asesor;
                        $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;

                        //Comprobamos si se cargara una cotizacion vieja o un nuevo presupuesto
                        $datos["cotizacion"][] = ""; //$row->idCotizacion;
                        $datos["cotizacionFinaliza"][] = ""; //$row->aceptoTermino ;

                        $datos["presupuesto"][] = $row->id_presupuesto;
                        $datos["presupuestoFinaliza"][] = $row->acepta_cliente ;

                        $datos["vozAudio"][] = ((file_exists($row->evidencia)) ? $row->evidencia : "") ;
                        $datos["vozCliente"][] = $row->idVoz ;
                        $datos["multipunto"][] = $row->idMultipunto ;

                        $datos["archivoOasis"][] = explode("|",$row->archivoOasis);
                        
                        $datos["lavado"][] = (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : '');
                        $datos["firmasMultipunto"][] = $row->firmaJefeTaller ;

                        //Verificamos si hay comentarios en la orden (solo vistas autorizados)
                        if (($datos["destinoVista"] == "JDT") || ($datos["destinoVista"] == "CIERRE")) {
                            $datos["comentarios"][] = $this->mConsultas->validarComentarioCO($row->orden_cita);
                        }

                        $datos["valet"][] = $row->idValet;
                    //}
                }
            } else {
                //Verificamos y cargamos las ordenes de servicio sin cita
                $campos = $this->mConsultas->listaOrdenes();
                foreach ($campos as $row) {
                    //if (!in_array($row->orden_cita,$datos["idOrden"])) {
                        /*$corte_b= explode("-",$row->fecha_recepcion);
                        if (count($corte_b)>1) {
                            $datos["fechaDiag"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0];
                        }else{
                            $datos["fechaDiag"][] = "";
                        }*/

                        $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');
                        $datos["fechaDiag"][] = $fecha->format('d-m-Y');

                        $datos["idOrden"][] = $row->orden_cita;
                        $datos["id_cita_url"][] = $this->encrypt($row->orden_cita);
                        $datos["idIntelisis"][] = $row->folioIntelisis;

                        $datos["tecnico"][] = $row->tecnico;
                        $datos["placas"][] = $row->vehiculo_placas;
                        $datos["modelo"][] = $row->vehiculo_modelo;

                        $datos["nombreAsesor"][] = $row->asesor;
                        $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;

                        //Comprobamos si se cargara una cotizacion vieja o un nuevo presupuesto
                        $datos["cotizacion"][] = ""; //$row->idCotizacion;
                        $datos["cotizacionFinaliza"][] = ""; //$row->aceptoTermino ;

                        $datos["presupuesto"][] = $row->id_presupuesto;
                        $datos["presupuestoFinaliza"][] = $row->acepta_cliente ;

                        $datos["vozAudio"][] = ((file_exists($row->evidencia)) ? $row->evidencia : "") ;
                        $datos["vozCliente"][] = $row->idVoz ;
                        $datos["multipunto"][] = $row->idMultipunto ;

                        $datos["archivoOasis"][] = explode("|",$row->archivoOasis);
                        
                        $datos["lavado"][] = (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : '');
                        $datos["firmasMultipunto"][] = $row->firmaJefeTaller ;

                        //Verificamos si hay comentarios en la orden (solo vistas autorizados)
                        if (($datos["destinoVista"] == "JDT") || ($datos["destinoVista"] == "CIERRE")) {
                            $datos["comentarios"][] = $this->mConsultas->validarComentarioCO($row->orden_cita);
                        }

                        $datos["valet"][] = $row->idValet;
                    //}
                }
            }
        }

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        //Identificamos la lista donde se desplegaran los datos
        if ($datos["destinoVista"] == "ASESOR") {
            $this->loadAllView($datos,'ordenServicio/lista_documentos_asesor');
        } else {
            $this->loadAllView($datos,'ordenServicio/listaOrdenes');
        }
    }

    //Cargamos de manera informativa las ordenes y los estatus de estos
    public function listDataStatusOrder($x = "",$datos = NULL)
    {
        $datos["idOrden"] = [];

        //Comprobamos el tipo de usuario que accede a la lista
        if ($this->session->userdata('rolIniciado')) {
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {
                $id = $this->session->userdata('idUsuario');
                $campos = $this->mConsultas->tecnicosOrdenesEstatus($id);
                foreach ($campos as $row) {
                    if (!in_array($row->orden_cita,$datos["idOrden"])) {
                        $corte_b = explode("-",$row->fecha_recepcion);
                        $datos["fechaDiag"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0];

                        // $datos['idRegistro'][] = $row->idDiagnostico;
                        $datos["idOrden"][] = $row->orden_cita;
                        $datos["id_cita_url"][] = $this->encrypt($row->orden_cita);
                        $datos["tecnico"][] = $row->tecnico;
                        $datos["placas"][] = $row->vehiculo_placas;
                        $datos["modelo"][] = $row->vehiculo_modelo;

                        $datos["status"][] = $row->status;
                        $datos["color"][] = $row->color;

                        $datos["nombreAsesor"][] = $row->asesor;
                        $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
                    }
                }            
            } else {
                $campos = $this->mConsultas->listaOrdenesEstatus(); 
                foreach ($campos as $row) {
                    if (!in_array($row->orden_cita,$datos["idOrden"])) {
                        $corte_b = explode("-",$row->fecha_recepcion);
                        $datos["fechaDiag"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0];

                        // $datos['idRegistro'][] = $row->idDiagnostico;
                        $datos["idOrden"][] = $row->orden_cita;
                        $datos["id_cita_url"][] = $this->encrypt($row->orden_cita);
                        $datos["idIntelisis"][] = $row->folioIntelisis;
                        
                        $datos["tecnico"][] = $row->tecnico;
                        $datos["placas"][] = $row->vehiculo_placas;
                        $datos["modelo"][] = $row->vehiculo_modelo;

                        $datos["status"][] = $row->status;
                        $datos["color"][] = $row->color;

                        $datos["nombreAsesor"][] = $row->asesor;
                        $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
                    }
                }
            }
        }else{
            $campos = $this->mConsultas->listaOrdenesEstatus(); 
            foreach ($campos as $row) {
                if (!in_array($row->orden_cita,$datos["idOrden"])) {
                    $corte_b = explode("-",$row->fecha_recepcion);
                    $datos["fechaDiag"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0];

                    // $datos['idRegistro'][] = $row->idDiagnostico;
                    $datos["idOrden"][] = $row->orden_cita;
                    $datos["id_cita_url"][] = $this->encrypt($row->orden_cita);
                    $datos["idIntelisis"][] = $row->folioIntelisis;

                    $datos["tecnico"][] = $row->tecnico;
                    $datos["placas"][] = $row->vehiculo_placas;
                    $datos["modelo"][] = $row->vehiculo_modelo;

                    $datos["status"][] = $row->status;
                    $datos["color"][] = $row->color;

                    $datos["nombreAsesor"][] = $row->asesor;
                    $datos["nombreConsumidor2"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
                }
            }
        } 

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }
        //var_dump($this->session->userdata());

        $this->loadAllView($datos,'ordenServicio/listaOrdenesEstatus');
    }

    //Comprobamos que al final se genere el pdf
    public function setDataPDF($idServicio = 0)
    {
        $datos["destinoVista"] = "PDF";
        $this->setData($this->decrypt($idServicio),$datos);
    }

    //Comprobamos que al final se genera la vista de revision
    public function setDataRevision($idServicio = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $this->setData($this->decrypt($idServicio),$datos);
    }

    //Comprobamos que al final se genere el pdf
    public function setDataVerificar($idServicio = 0)
    {
        $datos["destinoVista"] = "Formulario";
        $this->setData($this->decrypt($idServicio),$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData($idServicio = 0,$datos = NULL)
    {
        //$idServicio = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idServicio);

        //Verificamos si existe el registro en la tabla de estados terminados
        $query2 = $this->mConsultas->get_result("idOrden",$idServicio,"estados_pdf");
        foreach ($query2 as $row){
            $datos["completoFormulario"] = $row->ordenServicioPDF_2;
        }

        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("noServicio",$idServicio,"diagnostico");
        foreach ($query as $row){
            $fecha = explode(" ",$row->fecha_diagnostico);
            $datos["fecha_diagnostico"] = $fecha[0];
            $fecha_2 = explode(" ",$row->fecha_aceptacion);
            $datos["fecha_aceptacion"] = $fecha_2[0];

            $datos["noServicio"] = $row->noServicio;
            $datos["vehiculo"] = $row->vehiculo;
            $datos["dias"] = $row->dias;
            $datos["terminos"] = $row->terminos;
            $datos["firmaDiagnostico"] = (file_exists($row->firmaDiagnostico)) ? $row->firmaDiagnostico : "";
            // $datos["firmaDiagnosticoBase"] = $row->firmaDiagnosticoBase;
            $datos["firmaConsumidor1"] = (file_exists($row->firmaConsumidor1)) ? $row->firmaConsumidor1 : "";
            // $datos["firmaConsumidorBase1"] = $row->firmaConsumidorBase1;
            $datos["danos"] = $row->danos;
            $datos["danosImg"] = (file_exists($row->danosImg)) ? $row->danosImg : "";
            // $datos["danosImgBase"] = $row->danosImgBase;

            $datos["interiores"] = explode("_",$row->interiores);
            // $datos["cajuelaValida"] = $row->cajuelaValida;
            $datos["cajuela"] = explode("_",$row->cajuela);
            // $datos["exteriorValida"] = $row->exteriorValida;
            $datos["exteriores"] = explode("_",$row->exteriores);
            // $datos["docuValida"] = $row->docuValida;
            $datos["documentacion"] = explode("_",$row->documentacion);

            $datos["articulos"] = $row->articulos;
            $datos["cualesArticulos"] = $row->cualesArticulos;
            $datos["reporte"] = $row->reporte;
            $datos["costo"] = $row->costo;
            $datos["diaValue"] = $row->diaValue;
            $datos["mesValue"] = $row->mesValue;
            $datos["anioValue"] = $row->anioValue;
            $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";
            // $datos["firmaAsesorBase"] = $row->firmaAsesorBase;
            $datos["firmaConsumidor2"] = (file_exists($row->firmaConsumidor2)) ? $row->firmaConsumidor2 : "";
            // $datos["firmaConsumidorBase2"] = $row->firmaConsumidorBase2;
            $datos["nivelGasolina"] = $row->nivelGasolina;
            $datos["nivelGasolinaDiv"] = explode("/",$row->nivelGasolina);
            $datos["nombreDiagnostico"] = $row->nombreDiagnostico;
            $datos["nombreConsumido1"] = $row->nombreConsumido1;
            $datos["nombreAsesor"] = $row->nombreAsesor;
            $datos["nombreConsumidor2"] = $row->nombreConsumidor2;
            $datos["idServicio"] = $row->idDiagnostico;
        }

        $datos["limiteAno"] = date("Y");

        //Consultamos si ya se entrego la unidad
        // $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idServicio);

        $this->loadDataContrac($idServicio,$datos);
    }

    //Recuperamos la informacion del contrato
    public function loadDataContrac($idServicio = 0,$datos = NULL)
    {
        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("identificador",$idServicio,"contrato");
        foreach ($query as $row){
            $fecha = explode(" ",$row->fecha);
            $datos["fecha"] = $fecha[0];

            $datos["folio"] = $row->folio;
            $datos["hora"] = $row->hora;
            $datos["empresa"] = $row->empresa;
            $datos["pagoConsumidor"] = $row->pagoConsumidor;
            $datos["condicionalInfo"] = $row->condicionalInfo;
            $datos["condicionalPublicidad"] = $row->condicionalPublicidad;
            $datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : "";
            // $datos["firmaClienteBase"] = $row->firmaClienteBase;
            $datos["nombreDistribuidor"] = $row->nombreDistribuidor;
            $datos["firmaDistribuidor"] = (file_exists($row->firmaDistribuidor)) ? $row->firmaDistribuidor : "";
            // $datos["firmaDistribuidorBase"] = $row->firmaDistribuidorBase;
            $datos["nombreConsumidor"] = $row->nombreConsumidor;
            $datos["firmaConsumidor"] = (file_exists($row->firmaConsumidor)) ? $row->firmaConsumidor : "";
            // $datos["firmaConsumidorBase"] = $row->firmaConsumidorBase;
            $datos["aceptaTerminos"] = $row->aceptaTerminos;
            $datos["noRegistro"] = $row->noRegistro;
            $datos["noExpediente"] = $row->noExpediente;
            $datos["fechDia"] = $row->fechDia;
            $datos["fechMes"] = $row->fechMes;
            $datos["fechAnio"] = $row->fechAnio;
        }

        // $this->generatePDF($datos);

        
        if (($datos["destinoVista"] == "PDF")||($datos["destinoVista"] == "Revisión")) {
            $this->loadDataPart1($idServicio,$datos);
            // $this->generatePDF($datos);
        } else {
            $this->loadAllView($datos,'ordenServicio/ordenServicio_edit');
        }
    }

    //Recuperamos la parte de pita
    public function loadDataPart1($idServicio = 0,$datos = NULL)
    {
        //Recuperamos el id de la orden de servicio
        $precarga = $this->mConsultas->get_result('id_cita',$idServicio,'ordenservicio'); 
        foreach ($precarga as $row){
            $idOrden = $row->id;
            $iva = $row->iva/100;
            $datos["orden_id_tipo_orden"] = $row->id_tipo_orden;
            $datos["idIntelisis"] = $row->folioIntelisis;
            $idCategoria = $row->idcategoria;
        }

        $datos["totalRenglones"] = 0;
        $datos["total_items"] = 0;
        $datos["subtotal_items"] = 0;
        $datos["iva_items"] = 0;
        $datos["presupuesto_items"] = 0;
        $datos["orden_anticipo"] = 0;

        if (isset($idOrden)) {
            //Recuperamos el id de la categoria
            $Categoria = $this->mConsultas->get_result('id',$idCategoria,'categorias');
            foreach ($Categoria as $row){
                $datos["orden_categoria"] = $row->categoria;
            }

            //Recuperamos la información de la cita
            $infoCita = $this->mConsultas->citas($idOrden);
            foreach ($infoCita as $row) {
                $datos["orden_id_cita"] = $row->id_cita;
                //Recuperamos todas las campañas del vehiculo
                $campanias = $this->mConsultas->get_result('vin',$row->vehiculo_numero_serie,'campanias');
                $datos["nombreCampania"] = "[";
                foreach ($campanias as $row_1) {
                    $datos["nombreCampania"] .= $row_1->tipo.",";
                }

                $datos["nombreCampania"] .= "]";

                $datos["orden_campania"] = $row->campania;
                $datos["orden_asesor"] = $row->asesor;
                $datos["orden_telefono_asesor"] = $row->telefono_asesor;
                $datos["orden_extension_asesor"] = $row->extension_asesor;
                //torre
                $datos["orden_numero_interno"] = $row->numero_interno."-".$row->color;
                $datos["orden_fecha_recepcion"] = $row->fecha_recepcion;
                $datos["orden_hora_recepcion"] = $row->hora_recepcion;
                $datos["orden_fecha_entrega"] = $row->fecha_entrega;
                $datos["orden_hora_entrega"] = $row->hora_entrega;
                $datos["orden_datos_nombres"] = $row->datos_nombres;
                $datos["orden_datos_apellido_paterno"] = $row->datos_apellido_paterno;
                $datos["orden_datos_apellido_materno"] = $row->datos_apellido_materno;
                $datos["orden_nombre_compania"] = $row->nombre_compania;
                $datos["orden_nombre_contacto_compania"] = $row->nombre_contacto_compania;
                $datos["orden_ap_contacto"] = $row->ap_contacto;
                $datos["orden_am_contacto"] = $row->am_contacto;

                $datos["orden_datos_email"] = $row->datos_email;
                $datos["orden_rfc"] = $row->rfc;
                $datos["orden_correo_compania"] = $row->correo_compania;

                $datos["orden_domicilio"] = $row->calle." ".$row->noexterior." ".$row->nointerior.", ".$row->colonia;
                $datos["orden_municipio"] = $row->municipio;
                $datos["orden_cp"] = $row->cp;
                $datos["orden_estado"] = $row->estado;
                $datos["orden_telefono_movil"] = $row->telefono_movil;
                $datos["orden_otro_telefono"] = $row->otro_telefono;
                $datos["orden_vehiculo_placas"] = $row->vehiculo_placas;
                // $datos["orden_vehiculo_identificacion"] = $row->vehiculo_numero_serie;
                $datos["orden_vehiculo_identificacion"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : "Sin serie";
                $datos["orden_vehiculo_kilometraje"] = $row->km;
                $datos["orden_vehiculo_marca"] =  $row->vehiculo_marca; 
                $datos["orden_vehiculo_anio"] = $row->vehiculo_anio;

                //Otras variables
                $datos["orden_anticipo"] += (float)$row->anticipo;
                $datos["orden_confirmada"] = $row->cotizacion_confirmada;
                $idColor = $row->id_color;

            }

            if (isset($datos["orden_confirmada"])) {
                //Recuperamos el estatus de la cotización
                $datos["orden_confirmada"] = $this->mConsultas->EstatusCotizacionUser($idOrden);
                $items = $this->mConsultas->articulosTitulos($idOrden); 

                // $datos["subtotal_items"] = 0;
                // $datos["total_items"] = 0;

                foreach ($items as $row) {
                    if ($datos["orden_confirmada"] == "1") {
                        $datos["item_descripcion"][] = $row->descripcion;

                        if ($row->tipo == 3) {
                            $datos["gp_items"][] = "GEN.";
                            $datos["op_items"][] = "";

                            $datos["item_precio_unitario"][] = number_format(($row->total/1.16),2);
                        }elseif ($row->tipo == 5) {
                            $datos["gp_items"][] = $row->grupo;
                            $datos["op_items"][] = $row->descripcion;

                            $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                        }elseif ($row->tipo == 4) {
                            $datos["gp_items"][] = $row->grupo;
                            $datos["op_items"][] = $row->tipo_operacion;

                            $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                        }else {
                            $datos["gp_items"][] = $row->grupoDes;
                            $datos["op_items"][] = $row->operacionDes;

                            $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                        }

                        //$datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                        $datos["item_cantidad"][] = number_format($row->cantidad,2);
                        $datos["item_descuento"][] = number_format($row->descuento,2);
                        $datos["item_operacion"][] = ($row->operacion!='') ? 'Si': 'No';
                        $datos["item_total"][] = number_format($row->total,2);

                        // --$subtotal_linea = $row->cantidad * $row->precio_unitario;
                        //-- //$datos["subtotal_items"] += $subtotal_linea;
                        // $datos["subtotal_items"] += $row->total;
                        // $datos["iva_items"] = $datos["subtotal_items"] * 0.16;
                        // $datos["total_items"] = $datos["subtotal_items"] + $datos["iva_items"];
                        // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];

                        //Nvo procedimiento
                        $datos["total_items"] += $row->total;
                        $datos["subtotal_items"] = $datos["total_items"] / 1.16;
                        $datos["iva_items"] = $datos["total_items"] - $datos["subtotal_items"];
                        // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];
                    } else {
                        //Si la cotizacion no esta confirmada, solo lo tipo 1
                        if(($row->tipo == 1)||($row->tipo == 3)||($row->tipo == 5)||($row->tipo == 4)){
                            $datos["item_descripcion"][] = $row->descripcion;

                            if ($row->tipo == 3) {
                                $datos["gp_items"][] = "GEN.";
                                $datos["op_items"][] = "";

                                $datos["item_precio_unitario"][] = number_format(($row->total/1.16),2);
                            }elseif ($row->tipo == 5) {
                                $datos["gp_items"][] = $row->grupo;
                                $datos["op_items"][] = $row->descripcion;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }elseif ($row->tipo == 4) {
                                $datos["gp_items"][] = $row->grupo;
                                $datos["op_items"][] = $row->tipo_operacion;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }else {
                                $datos["gp_items"][] = $row->grupoDes;
                                $datos["op_items"][] = $row->operacionDes;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }

                            //$datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            $datos["item_cantidad"][] = number_format($row->cantidad,2);
                            $datos["item_descuento"][] = number_format($row->descuento,2);
                            $datos["item_operacion"][] = ($row->operacion!='') ? 'Si': 'No';
                            $datos["item_total"][] = number_format($row->total,2);

                            // //$subtotal_linea = $row->cantidad * $row->precio_unitario;
                            // //$datos["subtotal_items"] += $subtotal_linea;
                            // $datos["subtotal_items"] += $row->total;
                            // $datos["iva_items"] = $datos["subtotal_items"] * 0.16;
                            // $datos["total_items"] = $datos["subtotal_items"] + $datos["iva_items"];
                            // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];

                            //Nvo procedimiento
                            $datos["total_items"] += $row->total;
                             $datos["subtotal_items"] = $datos["total_items"] / 1.16;
                            $datos["iva_items"] = $datos["total_items"] - $datos["subtotal_items"];
                            // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];
                        }
                    }
                }

                if (isset($datos["item_total"])) {
                    $datos["totalRenglones"] = count($datos["item_total"]);
                }
            }

            $datos["Coti_1"] = $datos["totalRenglones"];

            //Comprobamos que se cargaran los datos
            if (isset($datos["orden_id_cita"])) {
                //Recuperamos el color
                $queryColor  = $this->mConsultas->get_result('id',$idColor,'cat_colores');
                foreach ($queryColor as $row) {
                    $datos["color"] = $row->color;
                }

                // $datos["color"] = $this->mConsultas->getColorCitaAuto($datos["orden_id_tipo_orden"]);

                //Recuperamos los tecnicos
                $datos["tecnicos"] = $this->mConsultas->tecnicos($datos["orden_id_cita"]);

                //Recuperamos el tipo de orden
                $datos["orden_tipo_orden"] = $this->mConsultas->getTipoOrden($datos["orden_id_tipo_orden"]);
                $datos["orden_identificador"] = $this->mConsultas->getTipoOrdenIdentificacion($datos["orden_id_tipo_orden"]);

                //Recuperamos el dato del anticipo
                // $datos["orden_anticipo"] = $this->mConsultas->getAnticipoOrden($idOrden);
            }
        }

        //Recuperamos la información de la cotizacion Mutlipunto
        $revision = $this->mConsultas->get_result("idOrden",$idServicio,"cotizacion_multipunto");
        foreach ($revision as $row) {
            if ($row->identificador == "M") {
                $idCotizacion = $row->idCotizacion;
                $acepta = $row->aceptoTermino;
                $cuerpo = $this->createCells(explode("|",$row->contenido));
            }
        }

        //Comprobamos que se haya cargado la información
        if (isset($idCotizacion)) {
            //Verificamos la respuesta afirmativa
            if (($acepta == "Si")||($acepta == "Val")) {
                $datos["subtotal_HM"] = 0;
                $datos["presupuesto_HM"] = 0;
                $datos["iva_HM"] = 0;
                $datos["anticipo_HM"] = (float)$row->anticipo;
                $datos["pieza_HM"] = [];

                //Comprobamos si la cotizacion se acepto completa o por partes
                if ($acepta == "Si") {
                    //Recuperamos los elementos de la cotización
                    foreach ($cuerpo as $index => $valor) {
                        if ($cuerpo[$index][1] != "") {
                            //Columnas de la tabla
                            $datos["gp_op_HM"][] = "Refacción";

                            $datos["pieza_HM"][] = $cuerpo[$index][6];
                            $datos["descripcion_HM"][] = $cuerpo[$index][1];
                            // $datos["mo_hm"][] = number_format($cuerpo[$index][3],2);
                            // $datos["precio_mo_hm"][] = number_format($cuerpo[$index][4],2);
                            $datos["mo_hm"][] = $cuerpo[$index][3] * $cuerpo[$index][4];

                            $datos["precio_HM"][] = number_format($cuerpo[$index][2],2);
                            $datos["cantidad_HM"][] = number_format($cuerpo[$index][0],2);

                            $precarga_C = ((float)$cuerpo[$index][0]*(float)$cuerpo[$index][2]) + ((float)$cuerpo[$index][3]*(float)$cuerpo[$index][4]);
                            $datos["total_fila_HM"][] = $precarga_C;
                            //Subtotal sin iva
                            $datos["subtotal_HM"] += $precarga_C;
                            //Obetenemos el iva
                            $datos["iva_HM"] = $datos["subtotal_HM"] * 0.16;
                            //Sacamos el acumulativo final (subtotal + iva)
                            $datos["presupuesto_HM"] = $datos["subtotal_HM"] * 1.16;

                            //$datos["presupuesto_HM"] += $precarga_C;
                            //$datos["iva_HM"] = $datos["presupuesto_HM"] * 0.16;
                            //$datos["subtotal_HM"] = $datos["presupuesto_HM"] - $datos["iva_HM"];
                        }
                    }
                }else {
                    //Recuperamos los elementos de la cotización
                    foreach ($cuerpo as $index => $valor) {
                        if (isset($cuerpo[$index][9])) {
                            if ($cuerpo[$index][9] == "Aceptada") {
                                //Columnas de la tabla
                                $datos["gp_op_HM"][] = "Refacción";

                                $datos["pieza_HM"][] = $cuerpo[$index][6];
                                $datos["descripcion_HM"][] = $cuerpo[$index][1];
                                // $datos["mo_hm"][] = number_format($cuerpo[$index][3],2);
                                // $datos["precio_mo_hm"][] = number_format($cuerpo[$index][4],2);
                                $datos["mo_hm"][] = $cuerpo[$index][3] * $cuerpo[$index][4];

                                $datos["precio_HM"][] = number_format($cuerpo[$index][2],2);
                                $datos["cantidad_HM"][] = number_format($cuerpo[$index][0],2);

                                $precarga_C = ((float)$cuerpo[$index][0]*(float)$cuerpo[$index][2]) + ((float)$cuerpo[$index][3]*(float)$cuerpo[$index][4]);

                                $datos["total_fila_HM"][] = $precarga_C;
                                //Subtotal sin iva
                                $datos["subtotal_HM"] += $precarga_C;
                                //Obetenemos el iva
                                $datos["iva_HM"] = $datos["subtotal_HM"] * 0.16;
                                //Sacamos el acumulativo final (subtotal + iva)
                                $datos["presupuesto_HM"] = $datos["subtotal_HM"] * 1.16;

                                //$datos["presupuesto_HM"] += $precarga_C;
                                //$datos["iva_HM"] = $datos["presupuesto_HM"] * 0.16;
                                //$datos["subtotal_HM"] = $datos["presupuesto_HM"] - $datos["iva_HM"];
                            }
                        }     
                    }
                }

                if (isset($datos["gp_op_HM"])) {
                    //Totales
                    $datos["subtotal_items"] = $datos["subtotal_items"] + $datos["subtotal_HM"];
                    $datos["iva_items"] = $datos["iva_items"] + $datos["iva_HM"];
                    $datos["total_items"] = $datos["total_items"] + $datos["presupuesto_HM"];
                    $datos["orden_anticipo"] += $datos["anticipo_HM"];
                    $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];
                    $datos["totalRenglones"] = $datos["totalRenglones"] + count($datos["gp_op_HM"]);
                }
            }
        }

        //Si no existe el presupuesto, se busca en el nuevo proceso
        //Consultamos su ya se ha generado alguna cotizacion para ese presupuesto
        $revision = $this->mConsultas->get_result_field("id_cita",$idServicio,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $idCotizacion = $row->id;
            $acepta_cliente = $row->acepta_cliente;
            $datos["anticipo_HM"] = (float)$row->anticipo;
        }

        if (isset($acepta_cliente)) {
            $datos["subtotal_HM"] = 0;
            $datos["presupuesto_HM"] = 0;
            $datos["iva_HM"] = 0;
            $datos["pieza_HM"] = [];
            
            if (($acepta_cliente == "Si")||($acepta_cliente == "Val")) {
                //Recuperamos las refacciones
                $query_presupuesto = $this->mConsultas->get_result_field("id_cita",$idServicio,"identificador","M","presupuesto_multipunto");
                foreach ($query_presupuesto as $row_presupuesto){
                    //Solo las refacciones aceptadas poe el cliente
                    if (($row_presupuesto->autoriza_cliente == "1")&&($row_presupuesto->autoriza_jefeTaller == "1")&&($row_presupuesto->autoriza_asesor == "1")) {
                        //Columnas de la tabla
                        $datos["gp_op_HM"][] = "Refacción";

                        $datos["pieza_HM"][] = $row_presupuesto->num_pieza;
                        $datos["descripcion_HM"][] = $row_presupuesto->descripcion;
                        $datos["mo_hm"][] = ($row_presupuesto->horas_mo * $row_presupuesto->costo_mo);

                        $datos["precio_HM"][] = $row_presupuesto->costo_pieza;
                        $datos["cantidad_HM"][] = $row_presupuesto->cantidad;

                        $precarga_C = ((float)$row_presupuesto->cantidad*(float)$row_presupuesto->costo_pieza) + ((float)$row_presupuesto->horas_mo*(float)$row_presupuesto->costo_mo);

                        $datos["total_fila_HM"][] = $precarga_C;
                        //Subtotal sin iva
                        $datos["subtotal_HM"] += $precarga_C;
                        //Obetenemos el iva
                        $datos["iva_HM"] = $datos["subtotal_HM"] * 0.16;
                        //Sacamos el acumulativo final (subtotal + iva)
                        $datos["presupuesto_HM"] = $datos["subtotal_HM"] * 1.16;

                        //$datos["presupuesto_HM"] += $precarga_C;
                        //$datos["iva_HM"] = $datos["presupuesto_HM"] * 0.16;
                        //$datos["subtotal_HM"] = $datos["presupuesto_HM"] - $datos["iva_HM"];
                    } 
                }

                //Totales
                $datos["subtotal_items"] = $datos["subtotal_items"] + $datos["subtotal_HM"];
                $datos["iva_items"] = $datos["iva_items"] + $datos["iva_HM"];
                $datos["total_items"] = $datos["total_items"] + $datos["presupuesto_HM"];
                $datos["orden_anticipo"] += $datos["anticipo_HM"];
                $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];

                $datos["totalRenglones"] = $datos["totalRenglones"] + count($datos["pieza_HM"]);
            }            
        }

        if ($datos["destinoVista"] == "PDF") {
              $this->generatePDF($datos);
        } else {
            //$this->load->view("ordenServicio/plantillaRevision", $datos);
            $this->loadAllView($datos,'ordenServicio/plantillaRevision');
        }
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Separar los campos de las tablas
    public function divideFields($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Acomodamos el formato de la fecha
    public function resetFormatDate($fecha = "",$validador = "")
    {
        $respuesta = "";
        if ($validador == "") {
            $fecha = explode(" ",$fecha);
            $respuesta = $fecha[0];
        }else {
            $fecha = explode(" ",$fecha);
            $fechaFracc = explode("-",$fecha[0]);
            $respuesta = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
        }
        return $respuesta;
    }

    //Validar Radio
    public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Validar multiple check
    public function validateCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >1){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."_";
                }
            }
        }
        return $respuesta;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        //Creamos la ruta donde se guardara la firma y su extensión
        if (strlen($imgBase64)>1) {
            $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
            $data = explode(',', $imgBase64);
            $Base64Img = base64_decode($data[1]);
            file_put_contents($base, $Base64Img);
        }else {
            $base = "";
        }

        return $base;
    }

    //Recibimos video por ajax
    public function videoAjax()
    {
        $respuesta = "";
        $idCita = $_POST["idCita"];
        $comentario = $_POST["comentario"];
        $tipo = $_POST["tipoArchivo"];

        if ($tipo == "videos") {
            $tipoDB = 1;
        }else {
            $tipoDB = 2;
        }

        $archivo = $this->validateDoc("videos");

        if (($idCita != "")&&($archivo != "")) {
            $datos = array(
                'id' => NULL,
                'id_cita' => $idCita,
                'tipo' => $tipoDB,
                'video' => $archivo,
                'comentario' => $comentario,
                'fecha_creacion'=>date('Y-m-d H:i:s')
            );

           $idVideo = $this->mConsultas->save_register('evidencia',$datos);

           if ($idVideo != 0) {
                $respuesta = "OK";
           }else {
                $respuesta = "ERROR";
           }
        }else {
            $respuesta = $archivo;
        }


       echo $respuesta;
    }

    //Validacion de campos para alta/Edicion (ajax)
    public function searchOrden()
    {
        $dato = $_POST['orden'];
        $cadena = "";
        $query = $this->mConsultas->get_result("noServicio",$dato,"diagnostico");
        foreach ($query as $row){
            $validar = $row->idDiagnostico;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($validar)) {
           //Si existe el dato, entonces ya existe el registro
            $respuesta = "EDITAR_".$this->encrypt($dato);
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "ALTA_".$this->encrypt($dato);
        }
        echo $respuesta;

    }

    //Validamos y guardamos los documentos
    public function validateDoc($direcctorio = "")
    {
        ini_set('max_execution_time',700);
        $path = "";
        //300 segundos  = 10 minutos
        //ini_set('max_execution_time',600);
        
        //Revisamos que la imagen se envio
        if ($_FILES["archivo"]['name'] != "") {
           // $name = date('dmyHis').'_'.str_replace(" ", "", $_FILES['archivo']['name']);
           // $path_to_save = base_url().'videos/'.date('Hms');
           // if(!file_exists($path_to_save)){
           //      mkdir($path_to_save, 0647, true);
           //  }
           //  $path = $path_to_save."/video".$name;
           //  move_uploaded_file($_FILES['archivo']['tmp_name'], $path);

           //Url donde se guardara la imagen
           // $urlDoc ="videos";
           $urlDoc = $direcctorio;
           //Generamos un nombre random
           $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
           $urlNombre = date("YmdHi")."_";
           for($j=0; $j<=8; $j++ ){
              $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
           }

          //Validamos que exista la carpeta destino   base_url()
          if(!file_exists($urlDoc)){
              mkdir($urlDoc, 0647, true);
          }

           //Configuramos las propiedades permitidas para la imagen
           $config['upload_path'] = $urlDoc;
           $config['file_name'] = $urlNombre;
           $config['allowed_types'] = "*";
           $config['max_size'] = 0;

           //Cargamos la libreria y la inicializamos
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('archivo')) {
                $data['uploadError'] = $this->upload->display_errors();
                $path .= $this->upload->display_errors();
                // echo $this->upload->display_errors();
                $path .= "";
           }else{
                //Si se pudo cargar la imagen la guardamos
                $fileData = $this->upload->data();
                $ext = explode(".",$_FILES['archivo']['name']);
                if ($urlDoc == "videos") {
                    $path = base_url().$urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                }else {
                    $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                }
           }
        }else{
            $path = "";
        }
        return $path;
    }

    //Llamamos a la vista para cargar el modal de archivo
    public function modalArchivo($idOrden = 0,$datos = NULL)
    {
        $datos["idOrden"] = $idOrden;

        //Comprobamos si ya existe un documento para ese archivo
        $query = $this->mConsultas->get_result("id_cita",$datos["idOrden"],"ordenservicio");
        foreach ($query as $row){
            $datos["archivoInventario"] = $row->archivoInventario;
            $datos["archivos"] = explode("|",$row->archivoInventario);
        }

        $this->loadAllView($datos,'modulosExternos/archivoOSP1');
    }

    //Recuperamos el formulario, lo validamos y guardamos
    public function validateModalArchivo()
    {
        $datos["idOrden"] = $this->input->post("idOrden");

        $this->form_validation->set_rules('archivo', '<br>No se cargo el archivo correctamente.', 'callback_archivo');
        $this->form_validation->set_rules('idOrden', 'Falta indicar.', 'required');

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('archivo','Faltan características.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Guardamos el archivo (s)
            if($this->input->post("tempFile") != "") {
                $archivos_2 = $this->validateDocOasis(2);
                $dataArchivo["archivoInventario"] = $this->input->post("tempFile")."|".$archivos_2;
            }else {
                $dataArchivo["archivoInventario"] = $this->validateDocOasis(2);
            }

            if ($dataArchivo["archivoInventario"] != "") {
                $actualiza = $this->mConsultas->update_table_row('ordenservicio',$dataArchivo,'id_cita',$datos["idOrden"]);

                if ($actualiza) {
                    $datos["mensaje"] = "1";
                }else {
                    $datos["mensaje"] = "2";
                }
            }else {
                $datos["mensaje"] = "0";
            }
            // $this->modalOasis($datos["idOrden"],$datos);
        }else{
            $datos["mensaje"] = "0";
            // $this->modalOasis($datos["idOrden"],$datos);
        }

        $this->modalArchivo($datos["idOrden"],$datos);
    }

    //Llamamos a la vista modal para cargar el archivo de oasis ford
    public function modalOasis($idOrden = 0,$datos = NULL)
    {
        $datos["idOrden"] = $idOrden;

        //Comprobamos si ya existe un documento para ese archivo
        $query = $this->mConsultas->get_result("noServicio",$datos["idOrden"],"diagnostico");
        foreach ($query as $row){
            $datos["archivoOasisL"] = $row->archivoOasis;
            $datos["archivos"] = explode("|",$row->archivoOasis);
        }

        //Verificamos que exista el diagnostico correspondiente
        if (isset($datos["archivoOasisL"])) {
            $datos["existe"] = 1;
        }else {
            $datos["existe"] = 0;
        }

        $this->loadAllView($datos,'ordenServicio/archivoOasis');
    }

    //Recuperamos el formulario, lo validamos y guardamos
    public function validateModalOasis()
    {
        $datos["idOrden"] = $this->input->post("idOrden");

        $this->form_validation->set_rules('archivo', '<br>No se cargo el archivo correctamente.', 'callback_archivo');
        $this->form_validation->set_rules('idOrden', 'Falta indicar.', 'required');

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('archivo','Faltan características.');
        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Guardamos el archivo (s)
            if($this->input->post("tempFile") != "") {
                $archivos_2 = $this->validateDocOasis(1);
                $dataArchivo["archivoOasis"] = $this->input->post("tempFile")."|".$archivos_2;
            }else {
                $dataArchivo["archivoOasis"] = $this->validateDocOasis(1);
            }

            if ($dataArchivo["archivoOasis"] != "") {

                $actualiza = $this->mConsultas->update_table_row('diagnostico',$dataArchivo,'noServicio',$datos["idOrden"]);

                if ($actualiza) {
                    //Actualizamos la información para el historial de ordenes
                    $this->actualiza_documentacion($datos["idOrden"],$dataArchivo["archivoOasis"]);
                    $datos["mensaje"] = "1";
                }else {
                    $datos["mensaje"] = "2";
                }
            }else {
                $datos["mensaje"] = "0";
            }
            // $this->modalOasis($datos["idOrden"],$datos);
        }else{
            $datos["mensaje"] = "0";
            // $this->modalOasis($datos["idOrden"],$datos);
        }
        $this->modalOasis($datos["idOrden"],$datos);
    }

    //Verificamos la existencia del archivo
    public function archivo()
    {
        if ($_FILES["archivo"]['name'] != "") {
            $retorno = TRUE;
        }else {
            $retorno = FALSE;
        }

        return $retorno;
    }

    //Validamos y guardamos los documentos del oasis
    public function validateDocOasis($anexo = 1)
    {
        $path = "";
        //300 segundos  = 5 minutos
        //ini_set('max_execution_time',300);
        
        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
        for ($i = 0; $i < count($_FILES['archivo']['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES['archivo']['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES['archivo']['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES['archivo']['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES['archivo']['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docOasis";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_";
            for($j=0; $j<=7; $j++ ){
                $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
                mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 if ($anexo == 2) {
                     $path .= base_url().$urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 }else {
                     $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 }
                 // $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion($id_cita = '',$archivo = "")
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "oasis" => $archivo
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Busqueda de la orden se servicio que se llenara
    public function validateOrderRegistre()
    {
        $idOrden = $_POST['orden'];
        $cadena = "";
        $query = $this->mConsultas->get_result("id_cita",$idOrden,"ordenservicio");
        foreach ($query as $row){
            $validar = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($validar)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "1";
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "0";
        }
        echo $respuesta;
    }

    //Recuperamos las fechas para relaizar los filtros
    public function filtroPorFechas()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $campo = $_POST['campo'];

        //Si no se envio un campo de consulta
        if ($campo == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->busquedaOrdenesFecha($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->busquedaOrdenesFechas_1($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->busquedaOrdenesCampo($campo);
            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->busquedaOrdenesCampo_Fecha($campo,$fecha_inicio,$fecha_fin);
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->busquedaOrdenesCampo_Fecha_1($campo,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->busquedaOrdenesCampo_Fecha_1($campo,$fecha_fin);
                }
            }
        }
        
        $cadena = "";

        if ($this->session->userdata('rolIniciado')) {
            $usuario = $this->session->userdata('rolIniciado');
        }else{
            $usuario = "";
        }

        $datos["idOrden"] = [];
        
        foreach ($precarga as $row){
            if (!in_array($row->orden_cita,$datos["idOrden"])) {
                //0
                $cadena .= $usuario."=";
                //1
                $cadena .= str_replace("|", "*",$row->orden_cita)."=";
                //2
                $corte_b = explode("-",$row->fecha_recepcion);
                $cadena .= str_replace("|", "*",$corte_b[2]."-".$corte_b[1]."-".$corte_b[0])."=";
                //3
                $cadena .= str_replace("|", "*",$row->vehiculo_modelo)."=";
                //4
                $cadena .= str_replace("|", "*",$row->vehiculo_placas)."=";
                //5
                $cadena .= str_replace("|", "*",$row->asesor)."=";
                //6
                $cadena .= str_replace("|", "*",$row->tecnico)."="; 
                //7
                $cadena .= str_replace("|", "*",$row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno)."=";
                //8
                $oasis = str_replace("|", "~", $row->archivoOasis);
                $cadena .= $oasis."=";
                //9
                $cadena .= str_replace("|", "*",$row->idMultipunto)."=";
                //10
                $cadena .= ((file_exists($row->firmaJefeTaller)) ? "1" : "0")."=";
                //11
                //$cadena .= $this->validateCotiResp($row->orden_cita)."=";
                $cadena .= str_replace("|", "*",$row->aceptoTermino)."=";
                //12
                $cadena .= str_replace("|", "*",$row->idCotizacion)."=";
                //13
                $cadena .= str_replace("|", "*",$row->idVoz)."=";
                //14
                $cadena .= ((file_exists($row->evidencia)) ? $row->evidencia : "")."=";
                //15
                $cadena .= (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : '')."=";
                //16
                $cadena .= $row->folioIntelisis."=";

                //Consultamos su ya se ha generado alguna cotizacion para ese presupuesto
                /*$revision = $this->mConsultas->get_result_field("id_cita",$row->orden_cita,"identificador","M","presupuesto_registro");
                foreach ($revision as $row_2) {
                    $idCotizacion = $row_2->id;
                    $acepta_cliente = $row_2->acepta_cliente;
                }

                //Si existe una cotizacion generada
                if (isset($idCotizacion)) {
                    //17
                    $cadena .= $idCotizacion."=";
                    //18
                    $cadena .= $acepta_cliente."=";
                }else{
                    //17
                    $cadena .= ""."=";
                    //18
                    $cadena .= ""."=";
                }*/
                //17
                $cadena .= $row->id_presupuesto."=";
                //18
                $cadena .= $row->acepta_cliente."=";
                    
                //19
                $cadena .= str_replace("|", "*",$row->idValet)."=";

                //20
                $cadena .= $this->encrypt($row->orden_cita)."=";
                
                $cadena .= "|";

                $datos["idOrden"][] = $row->orden_cita;
            }
        }

        echo $cadena;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("ordenServicio/plantillaPdf", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        if (isset($datos["tipo_vista"])) {
            $archivosJs = array(
                "include" => array(
                    'assets/js/diagnostico/filtrosListaOrden.js',
                    'assets/js/diagnostico/comentariosCO.js',
                )
            );
        } else {
            $archivosJs = array(
                "include" => array(
                    // 'assets/js/firmaCliente.js',
                    'assets/js/ordenServicioJs.js',
                    'assets/js/firmasMoviles/firmas_VC.js',
                    'assets/js/Multipunto/precarga_form.js',
                    //'assets/js/diagnostico/filtrosListaOrden.js',
                    'assets/js/diagnostico/buscarOrden.js',
                    'assets/js/diagnostico/cargaVideo.js',
                    'assets/js/diagnostico/comentariosCO.js',
                    //'assets/js/otrasVistas/buscadorVarios.js',
                    'assets/js/interaccionCheckOS.js',
                    'assets/js/reglasBloqueo.js',
                    'assets/js/superUsuario.js'
                )
            );
        }
        
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Orden de servicio"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}
