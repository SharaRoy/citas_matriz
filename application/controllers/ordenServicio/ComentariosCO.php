<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ComentariosCO extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($algo = "",$datos = NULL)
    {

    }

    public function comentarioCO()
    {
        //Recuperamos los campos del formulario
        $idHistorial = $_POST["idHistorial"];
        $titulo = $_POST["titulo"];
        $hora = $_POST["hora"];
        $fecha = $_POST["fecha"];
        $comentario = $_POST["comentario"];
        $respuesta = "";

        if (strlen($idHistorial) > 2) {
            //Guardamos la informacion en el historial
            $contenedor = array(
                'comentario' => $comentario,
                'id_cita' => $idHistorial,
                'fecha_registro' => date('Y-m-d H:i:s'),
                'titulo' => $titulo,
                'id_usuario'=> ($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : '0',
                //'fecha_notificacion' => $fecha." ".$hora,
            );

            $idComentario = $this->mConsultas->save_register('comentarios_cierre_orden',$contenedor);
            if ($idComentario != 0) {
                //Enviamos el mensaje al cierre de orden
                //Recuperamos la información para la notificación
                $query = $this->mConsultas->notificacionTecnicoCita($idHistorial); 
                foreach ($query as $row) {
                    $tecnico = $row->tecnico;
                    $modelo = $row->vehiculo_modelo;
                    $placas = $row->vehiculo_placas;
                    $asesor = $row->asesor;
                    $idIntelisis = $row->folioIntelisis;
                }

                //Armamos el mensaje
                $texto = SUCURSAL.". Se agrega comentario para cierre de orden.  TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR. ".$idHistorial.". OR Intelisis: ".$idIntelisis;

                //Numero de angel (pruebas)
                $this->sms_general('5535527547',$texto);

                //Gerente de servicio
                //$this->sms_general('3315273100',$texto);

                //Cierre de orden - Maria Elvia Garcia Tomas 
                $this->sms_general('3316723220',$texto);

                //
                $this->sms_general('3323589743',$texto); 

                $respuesta = "OK";
            }else {
                $respuesta = "ERROR";
            }
        }else {
            $respuesta = "ERROR";
        }
        
        echo $respuesta;
    }

    //Enviamos el mensaje para el asesor
    public function sms_general($celular_sms='',$mensaje_sms='')
    {
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    //Cargamos el historial de comnetarios de magic
    public function loadHistoryCierreOrden()
    {
        $idHistorial = $_POST["idHistorial"];

        $cadena = "";

        //Recuperamos la informacion de los comentarios
        $query = $this->mConsultas->get_result("id_cita",$idHistorial,"comentarios_cierre_orden");
        foreach ($query as $row){
            $cadena .=  $row->fecha_registro."_";
            $cadena .=  $row->comentario."_";
            $cadena .=  $row->titulo."_";
            $cadena .= "|";
        }

        echo $cadena;
    }


    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/diagnostico/comentariosCO.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Inventarios sin cita"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
