<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buscador extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($algo = "",$datos = NULL)
    {
        if (!isset($datos["mensaje"])) {
            $datos["mensaje"] = "5";
        }
        // Recuperamos la hora y fecha actual
        $limiteHora = date("H:i:s");
        $horaAct = strtotime($limiteHora);

        $hoy = date("Y-m-d");

        //Verificamos y cargamos las ordenes de servicio sin cita
        $registros = $this->mConsultas->get_table('diagnostico_sc');
        foreach ($registros as $row) {
            if ($row->noServicio != "0") {
                //Recuperamos la hora y fecha del registro
                $fecha = explode(" ",$row->fechaRegistro);
                $fechaCreacion = $fecha[0];
                $horaCreacion = $fecha[1];

                //Verificamos que no se haya excedido el tiempo limite de espera
                if (strcmp($hoy, $fechaCreacion) === 0) {
                    //Verificamos que el cambio este en tiempo y forma
                    $horaReg = strtotime($horaCreacion);
                    $diferencia = $horaAct-$horaReg;
                    if (($diferencia >= 0 )&&($diferencia <= 1800)) {
                        //El registro aun esta dentro de tiempo y forma
                        $datos['idRegistro'][] = $row->idDiagnostico;
                        $datos["folio"][] = $row->noServicio;

                        $corte_b = explode("-",$fecha[0]);
                        $datos["limite"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0];
                        $datos["limiteHoraReg"][] = $fecha[1];
                        $datos["vehiculo"][] = $row->vehiculo;
                        $datos["nombreConsumido1"][] = $row->nombreConsumido1;
                        $datos["nombreAsesor"][] = $row->nombreAsesor;
                    }else {
                        //Ya vencio el tiempo de espera, eliminamos el registro
                        // $eliminar = $this->deleteRegistry($row->idDiagnostico,$row->noServicio);
                    }
                }else {
                    //Ya vencio el tiempo de espera, eliminamos el registro
                    // $eliminar = $this->deleteRegistry($row->idDiagnostico,$row->noServicio);
                }
            }
        }

        //Revisamos las ordenes con cita que no tiene primera parte de la orden
        $query = $this->mConsultas->get_result("existePrev","0","diagnostico");
        foreach ($query as $row) {
            $fecha_2 = explode(" ",$row->fechaRegistro);
            $fechaCreacion_2 = $fecha_2[0];
            $horaCreacion_2 = $fecha_2[1];

            //Verificamos que no se haya excedido el tiempo limite de espera
            if (strcmp($hoy, $fechaCreacion_2) === 0) {
                $horaReg_2 = strtotime($horaCreacion_2);
                $diferencia_2 = $horaAct-$horaReg_2;
                if (($diferencia_2 >= 0 )&&($diferencia_2 <= 1800)) {
                    //El registro aun esta dentro de tiempo y forma
                    $datos["idDiagnostico"][] = $row->idDiagnostico;
                    $datos["noServicio"][] = $row->noServicio;

                    $corte_d = explode("-",$fecha_2[0]);
                    $datos["fechaDiag"][] = $corte_d[2]."-".$corte_d[1]."-".$corte_d[0];
                    $datos["vehiculoDiag"][] = $row->vehiculo;
                    $datos["nombreConsumido1Diag"][] = $row->nombreConsumido1;
                    $datos["nombreAsesorDiag"][] = $row->nombreAsesor;
                }
            }
        }

        $this->loadAllView($datos,'ordenServicio2/buscador_sc');
    }

    //Recibimos los valones necesarios para actualizar las tablas
    public function validateForm()
    {
        $this->form_validation->set_rules('idOrden', 'El Número de orden es requerido.', 'required');
        $this->form_validation->set_message('required','%s');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->transferData();
        }else{
            $datos["mensaje"]="0";
            $this->index("",$datos);
        }
    }

    //Actualizamos los campos de las tablas
    public function transferData()
    {
        $idTemp = $this->newIndex();
        $limiteHora = date("H:i:s");
        $hoy = date("d-m-Y");
        $idRegistro = $this->input->post("idRegistro");
        $horaRegistro = $this->input->post("horaRegistro");
        $idOrden = $this->input->post("idOrden");
        $folio = $this->input->post("folioTem");
        $fechaReg = $this->input->post("fechReg");

        $registro = date("Y-m-d H:i:s");

        //Verificamos que el cambio este en tiempo y forma
        $horaReg = strtotime($horaRegistro);
        $horaAct = strtotime($limiteHora);

        //Verificamos que el regsitro sea de ese dia
        if (strcmp($hoy, $fechaReg) === 0) {
            //Verificamos que no hayan pasado mas de 30 min desde el registro
            $diferencia = $horaAct-$horaReg;
            if (($diferencia >= 0 )&&($diferencia <= 1800)) {
                //Aun queda tiempo, actualizamos los campos de la tabla antes de tranferirlos
                $datosTem = array(
                    "idDiagnostico" => $idTemp,
                    "noServicio" => $idOrden,
                    "fecha_actualiza" => $registro,
                    "existePrev" => 1,
                );
                $actualizar = $this->mConsultas->update_table_row('diagnostico_sc',$datosTem,'idDiagnostico',$idRegistro);
                if ($actualizar) {
                    //Si se actualizo correctamente el registro, tranferimos la informacion
                    $transpasa = $this->mConsultas->merge_table($idTemp);
                    if ($transpasa) {
                        //Actualizamos el dato en la orden en el contrato
                        $datosTem2 = array(
                            "identificador" => $idOrden,
                        );
                        //Respaldamos orden temporal para evitar perdidas

                        $actualizar2 = $this->mConsultas->update_table_row('contrato',$datosTem2,'identificador',$folio);
                        //Si el proceso terminos con exito borramos el registro temporal
                        if ($actualizar2) {
                            //Si el diagnostico se guardo, creamos el registro para la documentacion
                            $this->registro_documentacion($idOrden);

                            // $eliminar3 = $this->mConsultas->delete_row('diagnostico_sc','idDiagnostico',$idTemp);
                            //Si el proceso terminos con exito borramos el registro temporal
                            $datosTem1 = array(
                                "idDiagnostico" => $idRegistro,
                                "noServicio" => "0",
                            );
                            $this->mConsultas->update_table_row('diagnostico_sc',$datosTem1,'idDiagnostico',$idTemp);

                            $datos["mensaje"]="1";
                        }else {
                            $datos["mensaje"]="0";
                        }

                        //Verificamos si ya se firmoaron todos los campos
                        $this->chekingFirm($idOrden);
                    }else {
                        $datos["mensaje"]="0";
                    }
                }else {
                    $datos["mensaje"]="2";
                }
            }else {
                //Ya vencio el tiempo de espera, eliminamos el registro
                // $datos["respuesta"] = $this->deleteRegistry($idRegistro,$folio);
                //Si el proceso terminos con exito borramos el registro temporal
                $datosTem1 = array(
                    "idDiagnostico" => $idRegistro,
                    "noServicio" => "0",
                );
                $this->mConsultas->update_table_row('diagnostico_sc',$datosTem1,'idDiagnostico',$idTemp);
            }
        } else {
            //Ya vencio el tiempo de espera, eliminamos el registro
            // $datos["respuesta"] = $this->deleteRegistry($idRegistro,$folio);
            //Si el proceso terminos con exito borramos el registro temporal
            $datosTem1 = array(
                "idDiagnostico" => $idRegistro,
                "noServicio" => "0",
            );
            $this->mConsultas->update_table_row('diagnostico_sc',$datosTem1,'idDiagnostico',$idTemp);
        }

        $this->index("",$datos);

    }

    //Creamos el registro para el listado de la documentacion de ordenes
    public function registro_documentacion($id_cita = '')
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (!isset($dato)) {
            //Recuperamos la informacion general
            $informacion_gral = $this->mConsultas->orden_completa($id_cita);
            foreach ($informacion_gral as $row){
                //$id_cita = $row->orden_cita;
                $folio_externo = $row->folioIntelisis;
                $fecha_recepcion = $row->fecha_recepcion;
                $vehiculo = $row->vehiculo_modelo;
                $placas = $row->vehiculo_placas;
                $serie = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
                $asesor = $row->asesor;
                $tecnico = $row->tecnico;
                $id_tecnico = $row->id_tecnico;
                $cliente = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            }

            //Verificamos que la informacion se haya cargado
            if (isset($serie)) {
                $registro = date("Y-m-d H:i:s");

                $contededor = array(
                    "id" => NULL,
                    "id_cita" => $id_cita,
                    "folio_externo" => $folio_externo,
                    "fecha_recepcion" => $fecha_recepcion,
                    "vehiculo" => $vehiculo,
                    "placas" => $placas,
                    "serie" => $serie,
                    "asesor" => $asesor,
                    "tecnico" => $tecnico,
                    "id_tecnico" => $id_tecnico,
                    "cliente" => $cliente,
                    "diagnostico" => "1",
                    "oasis" => "",
                    "multipunto" => "0",
                    "firma_jefetaller" => "0",
                    "presupuesto" => "0",
                    "presupuesto_afectado" => "",
                    "presupuesto_origen" => "0",
                    "voz_cliente" => "0",
                    "evidencia_voz_cliente" => "",
                    "servicio_valet" => "0",
                    "fechas_creacion" => $registro,
                );

                //Guardamos el registro
                $guardado = $this->mConsultas->save_register('documentacion_ordenes',$contededor);
            }
        }
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Revisamos si ya se firmaron todos los campos
    public function chekingFirm($idOrden = 0)
    {
        $registro = date("Y-m-d H:i:s");
        //Recuperamos los campos necesarios
        $query = $this->mConsultas->get_result("noServicio",$idOrden,"diagnostico");
        foreach ($query as $row){
            $firmaDiagnostico = $row->firmaDiagnostico;
            $firmaConsumidor1 = $row->firmaConsumidor1;
            $firmaAsesor = $row->firmaAsesor;
            $firmaConsumidor2 = $row->firmaConsumidor2;
        }

        if (isset($firmaAsesor)) {
            //Verificamos si existe el registro en la tabla
            $query2 = $this->mConsultas->get_result("idOrden",$idOrden,"estados_pdf");
            foreach ($query2 as $row){
                $multipunto = $row->multipuntoPDF;
                $idPdf = $row->idRegistro;
            }

            if (isset($multipunto)) {
                //Comprobamos los estados validos (actualizar)
                if (($firmaDiagnostico != "")&&($firmaConsumidor1 != "")&&($firmaAsesor != "")&&($firmaConsumidor2 != "")) {
                    $data = array(
                        "ordenServicioPDF_2" => 1,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                  $data = array(
                      "ordenServicioPDF_2" => 0,
                      "fecha_actualizacion" => $registro,
                  );
                }
                $this->mConsultas->update_table_row('estados_pdf',$data,'idRegistro',$idPdf);
            //Si no existe el registro se cres
            }else {
                //Comprobamos los estados validos (registro)
                if (($firmaDiagnostico != "")&&($firmaConsumidor1 != "")&&($firmaAsesor != "")&&($firmaConsumidor2 != "")) {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 1,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 0,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }
                $this->mConsultas->save_register('estados_pdf',$data);
            }
        }

        return FALSE;
    }

    //Funcion para eliminar un registro temporal que ya expiro
    public function deleteRegistry($idRegistro = 0 ,$folio = '')
    {
        $respuesta = "0";
        $eliminar = $this->mConsultas->delete_row('diagnostico_sc','idDiagnostico',$idRegistro);
        if ($eliminar) {
            $respuesta = "2";
            //Eliminamos el contrato
            $eliminar_2 = $this->mConsultas->delete_row('contrato','identificador',$folio);
        }else {
            $respuesta = "0";
        }
        return $respuesta;
    }

    //Funcion para generar el folio de la venta
    public function newIndex()
    {
        //Recuperamos el indice para el folio
        $fecha = date("Ymd");
        $registro = $this->mConsultas->last_id_table_2();
        $row = $registro->row();
        if (isset($row)){
          $indexFolio = $registro->row(0)->idDiagnostico;
        }else {
          $indexFolio = 0;
        }
        $nvoFolio = $indexFolio + 1;

        return $nvoFolio;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                // 'assets/js/firmaCliente.js',
                'assets/js/buscadorDiag.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Inventarios sin cita"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
