<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadisticas_Indicadores extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mConsultas', 'consulta', TRUE);
		$this->load->model('MIndicadores', 'indicadores', TRUE);
		$this->load->model('Verificacion', '', TRUE);
		date_default_timezone_set(TIMEZONE_SUCURSAL);
		//300 segundos  = 5 minutos
		ini_set('max_execution_time',300);
	}

	public function index()
	{
		//Recuperamos a los tecnicos
        $tecnicos = $this->consulta->get_table("tecnicos");

        foreach ($tecnicos as $row){
          	if ($row->activo == "1") {
            	$datos["idTec"][] = $row->id;
            	$datos["tecnico"][] = $row->nombre;
          	}
        }

        //Recuperamos los tipo de operacion
        $estatus = $this->consulta->get_table("estatus");

        foreach ($estatus as $row){
        	if ($row->activo == "1") {
	        	$datos["id_operacion"][] = $row->id;
	        	$datos["tipo_operacion"][] = $row->nombre;
	        }
        }

        $this->loadAllView($datos,'modulosExternos/Indicadores/VentasMO');
	}

	//Recibimos los parametros para filtrar la informacion
	public function recuperar_totales()
	{
		//Campo que indica que esta cerrada -> id_situacion_intelisis (ordenservicio)
		$tipo_orden = $_POST["tipo_orden"];
		$tipo_operacion = $_POST["tipo_operacion"];
		$fecha_ini = $_POST["fecha_ini"];
		$fecha_fin = $_POST["fecha_fin"];
		$tecnico = $_POST["tecnico"];

		//Identificamos el tipo de busqueda
		if ($tecnico == "") {
			if ($fecha_ini == "") {
				$refacciones_mo = $this->indicadores->total_tipoOrden($tipo_orden,$tipo_operacion);
			}else{
				if ($fecha_fin == "") {
					$refacciones_mo = $this->indicadores->total_tipoOrden_Fecha($tipo_orden,$tipo_operacion,$fecha_ini);
				}else {
					$refacciones_mo = $this->indicadores->total_tipoOrden_Fecha2($tipo_orden,$tipo_operacion,$fecha_ini,$fecha_fin);
				}
			}
		}else{
			if ($fecha_ini == "") {
				$refacciones_mo = $this->indicadores->total_tecnico($tipo_orden,$tipo_operacion,$tecnico);
			}else{
				if ($fecha_fin == "") {
					$refacciones_mo = $this->indicadores->total_tipoOrden_FechaTecnico($tipo_orden,$tipo_operacion,$fecha_ini);
				}else {
					$refacciones_mo = $this->indicadores->total_tipoOrden_FechaTecnico2($tipo_orden,$tipo_operacion,$fecha_ini,$fecha_fin);
				}
			}
		}

		$resultado = "";

		foreach ($refacciones_mo as $row) {
			if (($row->autoriza_cliente == "1")&&($row->activo == "1")) {
				$fecha_sf = new DateTime($row->fecha_recepcion);
				$fecha_impresa[] = $fecha_sf->format('d/m/Y');

				$fecha[] = $row->fecha_recepcion;
				//Si es tipo es garantía, recuperamos las horas indicadas en garantias
				if ($tipo_orden == "2") {
					$horas_mo[] = $row->horas_mo_garantias;
					$costo_mo[] = $row->costo_mo_garantias;
				} else {
					$horas_mo[] = $row->horas_mo;
					$costo_mo[] = $row->costo_mo;
				}
			}
		}

		if (isset($horas_mo)) {	
			$total = (float)$horas_mo[0] * (float)$costo_mo[0]; 
			$horas_total = $horas_mo[0];
			
			$fecha_indicador[] = $fecha_impresa[0];
			$total_horas[] = $horas_mo[0];
			$costo_mo[] = $costo_mo[0];
			$total_venta[] = $total;

			for ($i=1; $i <count($fecha) ; $i++) {

				if ($fecha[$i] != $fecha[$i-1]) {
					$fecha_indicador[] = $fecha_impresa[$i-1];
					$total_horas[] = $horas_total;
					$costo_mo[] = $costo_mo[$i-1];
					$total_venta[] = $total;

					$total = 0;
					$horas_total = 0;

					$total += (float)$horas_mo[$i] * (float)$costo_mo[$i];
					$horas_total += $horas_mo[$i];
				}else{
					$total += (float)$horas_mo[$i] * (float)$costo_mo[$i];
					$horas_total += $horas_mo[$i];
				}
			}
			

			//En caso que sea una sola fecha, actualizamos los totales
			if (count($fecha_indicador) == 1) {
				$total_horas[0] = $horas_total;
				$total_venta[0] = $total;
			} else{
				$fecha_indicador[count($fecha_indicador)-1] = $fecha_impresa[count($fecha)-1];
				$total_horas[count($fecha_indicador)-1] = $horas_total;
				$costo_mo[count($fecha_indicador)-1] = $costo_mo[count($fecha)-1];
				$total_venta[count($fecha_indicador)-1] = $total;
			}

			//Imprimimos la informacion
			for ($i=0; $i <count($fecha_indicador) ; $i++) { 
				$resultado .= $fecha_indicador[$i]."=".$total_horas[$i]."=".$costo_mo[$i]."=".$total_venta[$i]."|";
			}
		}

		echo $resultado;
	}

	//Indicadores de garantia (solo conteo de ordenes)
	public function garantias_conteo($tipo_garantia = '')
	{
		if ($tipo_garantia == "1") {
			$datos["tipo_garantia"] = "Garantía Abiertas";
		}elseif ($tipo_garantia == "2") {
			$datos["tipo_garantia"] = "Garantía Emitidas a Planta";
		}elseif ($tipo_garantia == "3") {
			$datos["tipo_garantia"] = "Garantía Cobradas a Planta";
		}elseif ($tipo_garantia == "4") {
			$datos["tipo_garantia"] = "Garantía con Problemas de Autorización";
		}else {
			$datos["tipo_garantia"] = "Garantía 'ERRROR'";
		}

		$datos["estatus"] = $tipo_garantia;

		//Recuperamos a los tecnicos
        $tecnicos = $this->consulta->get_table("tecnicos");

        foreach ($tecnicos as $row){
          	if ($row->activo == "1") {
            	$datos["idTec"][] = $row->id;
            	$datos["tecnico"][] = $row->nombre;
          	}
        }

        //Recuperamos los esatatus de las garantias
        $estatus = $this->consulta->get_table("cat_estatus_garantia");

        //Recuperar el listado de estatus
        foreach ($estatus as $row){
            $datos["id_estatus"][] = $row->id;
            $datos["estatusList"][] = $row->estatus;
        }

        $this->loadAllView($datos,'modulosExternos/Indicadores/garantias_conteo');
	}

	//Realizamos los filtros de los conteo de ordenes
	public function conteo_garantias()
	{
		$estatus = $_POST["estatus"];
		$fecha_ini = $_POST["fecha_ini"];
		$fecha_fin = $_POST["fecha_fin"];
		$tecnico = $_POST["tecnico"];

		$resultado = "";

		//Identificamos el tipo de busqueda
		if ($tecnico == "") {
			if ($fecha_ini == "") {
				$garantias = $this->indicadores->garantia_estatus($estatus);
			}else{
				if ($fecha_fin == "") {
					$garantias = $this->indicadores->garantia_Fecha($estatus,$fecha_ini);
				}else {
					$garantias = $this->indicadores->garantia_Fecha2($estatus,$fecha_ini,$fecha_fin);
				}
			}
		}else{
			if ($fecha_ini == "") {
				$garantias = $this->indicadores->garantia_tecnico($estatus,$tecnico);
			}else{
				if ($fecha_fin == "") {
					$garantias = $this->indicadores->garantia_FechaTecnico($estatus,$fecha_ini);
				}else {
					$garantias = $this->indicadores->garantia_FechaTecnico2($estatus,$fecha_ini,$fecha_fin);
				}
			}
		}

		foreach ($garantias as $row) {
			$fecha_sf = new DateTime($row->fecha_recepcion);
			$fecha_impresa[] = $fecha_sf->format('d/m/Y');
			$fecha[] = $row->fecha_recepcion;
			$id_cita[] = $row->id_cita;
			//$tecnico[] = $reow->tecnico;
		}

		if (isset($id_cita)) {	
			$conteo = 1;

			$fecha_indicador[] = $fecha_impresa[0];
			$total_ordenes[] = $conteo;
			//$estatus_indicador[] = $costo_mo[0];			

			for ($i=1; $i <count($fecha) ; $i++) {

				if ($fecha[$i] != $fecha[$i-1]) {
					$fecha_indicador[] = $fecha_impresa[$i-1];
					$total_ordenes[] = $conteo;
					//$estatus_indicador[] = $costo_mo[$i-1];

					$conteo = 0;
				}else{
					$conteo++;
				}
			}

			//En caso que sea una sola fecha, actualizamos los totales
			if (count($fecha_indicador) == 1) {
				$total_ordenes[0] = $conteo;
			} else{
				$fecha_indicador[count($fecha_indicador)-1] = $fecha_impresa[count($fecha)-1];
				$total_ordenes[count($fecha_indicador)-1] = $conteo;
			}



			//Imprimimos la informacion
			for ($i=0; $i <count($fecha_indicador) ; $i++) { 
				$resultado .= $fecha_indicador[$i]."=".$total_ordenes[$i]."|";
			}
		}

		echo $resultado;
	}

	//Indicadores de garantia (solo conteo de ordenes)
	public function garantias_monto($tipo_garantia = '')
	{
		if ($tipo_garantia == "1") {
			$datos["tipo_garantia"] = "Garantía Abiertas";
		}elseif ($tipo_garantia == "2") {
			$datos["tipo_garantia"] = "Garantía Emitidas a Planta";
		}elseif ($tipo_garantia == "3") {
			$datos["tipo_garantia"] = "Garantía Cobradas a Planta";
		}elseif ($tipo_garantia == "4") {
			$datos["tipo_garantia"] = "Garantía con Problemas de Autorización";
		}else {
			$datos["tipo_garantia"] = "Garantía 'ERRROR'";
		}

		$datos["estatus"] = $tipo_garantia;

		//Recuperamos a los tecnicos
        $tecnicos = $this->consulta->get_table("tecnicos");

        foreach ($tecnicos as $row){
          	if ($row->activo == "1") {
            	$datos["idTec"][] = $row->id;
            	$datos["tecnico"][] = $row->nombre;
          	}
        }

        //Recuperamos los esatatus de las garantias
        $estatus = $this->consulta->get_table("cat_estatus_garantia");

        //Recuperar el listado de estatus
        foreach ($estatus as $row){
            $datos["id_estatus"][] = $row->id;
            $datos["estatusList"][] = $row->estatus;
        }

        $this->loadAllView($datos,'modulosExternos/Indicadores/garantias_monto');
	}

		//Realizamos los filtros de los conteo de ordenes
	public function monto_garantias()
	{
		$estatus = $_POST["estatus"];
		$fecha_ini = $_POST["fecha_ini"];
		$fecha_fin = $_POST["fecha_fin"];
		$tecnico = $_POST["tecnico"];

		$resultado = "";

		//Identificamos el tipo de busqueda
		if ($tecnico == "") {
			if ($fecha_ini == "") {
				$garantias = $this->indicadores->monto_garantia_estatus($estatus);
			}else{
				if ($fecha_fin == "") {
					$garantias = $this->indicadores->monto_garantia_Fecha($estatus,$fecha_ini);
				}else {
					$garantias = $this->indicadores->monto_garantia_Fecha2($estatus,$fecha_ini,$fecha_fin);
				}
			}
		}else{
			if ($fecha_ini == "") {
				$garantias = $this->indicadores->monto_garantia_tecnico($estatus,$tecnico);
			}else{
				if ($fecha_fin == "") {
					$garantias = $this->indicadores->monto_garantia_FechaTecnico($estatus,$fecha_ini);
				}else {
					$garantias = $this->indicadores->monto_garantia_FechaTecnico2($estatus,$fecha_ini,$fecha_fin);
				}
			}
		}

		foreach ($garantias as $row) {
			$fecha_sf = new DateTime($row->fecha_recepcion);
			$fecha_impresa[] = $fecha_sf->format('d/m/Y');

			$fecha[] = $row->fecha_recepcion;
			if (($row->autoriza_garantia == "1")&&($row->activo == "1")) {
				$costo_mo[] = (float)$row->costo_mo_garantias * (float)$row->horas_mo_garantias;
				$refaccion[] = (float)$row->cantidad * (float)$row->costo_ford;
			}else{
				$costo_mo[] = 0;
				$refaccion[] = 0;
			}
			
		}

		if (isset($fecha_impresa)) {	
			$total = (float)$costo_mo[0] * (float)$refaccion[0]; 
			
			$fecha_indicador[] = $fecha_impresa[0];
			$monto_refacciones[] = $total;

			for ($i=1; $i <count($fecha) ; $i++) {

				if ($fecha[$i] != $fecha[$i-1]) {
					$fecha_indicador[] = $fecha_impresa[$i-1];
					$monto_refacciones[] = $total;

					$total = 0;
					$total += (float)$costo_mo[$i] * (float)$refaccion[$i];
				}else{
					$total += (float)$costo_mo[$i] * (float)$refaccion[$i];
				}
			}
			

			//En caso que sea una sola fecha, actualizamos los totales
			if (count($fecha_indicador) == 1) {
				$monto_refacciones[0] = $total;
			} else{
				$fecha_indicador[count($fecha_indicador)-1] = $fecha_impresa[count($fecha)-1];
				$monto_refacciones[count($fecha_indicador)-1] = $total;
			}

			//Imprimimos la informacion
			for ($i=0; $i <count($fecha_indicador) ; $i++) { 
				$resultado .= $fecha_indicador[$i]."=".$monto_refacciones[$i]."|";
			}
		}

		echo $resultado;
	}


	//Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrosModulos/total_ventasMO.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Indicadores de Venta MO"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}