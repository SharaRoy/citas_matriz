<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConexionFord extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

  	public function index()
  	{
          $date = new DateTime('2000-01-01');
          echo $date->format('Y-m-d H:i:s');
  	}

    //Resibimos el id Cita para recuperar la informacion de la orden
    public function RO_Data_Sample()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];

        try {
            // $idOrden = $_POST["idOrden"];

            $idOrden = "60";
            //Hacemos la consulta
            $query = $this->mConsultas->api_orden($idOrden);

            //Recuperamos la informacion de la consulta
            foreach ($query as $row) {

                $contenedor["BID"] = BIN_SUCURSAL;
                $contenedor["RO_Number"] = $row->id;
                $contenedor["RO_Open_Date"] = $row->created_at;
                $contenedor["RO_Closed_Date"] = "-";
                $contenedor["Odometer_Redading"] = "-";
                $contenedor["Odometer_Indicator"] = "-";
                $contenedor["VIN"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : "-";
                $contenedor["Vehicle_Model"] = ($row->vehiculo_modelo != "") ? $row->vehiculo_modelo : "-";
                $contenedor["Vehicle_Year"] = ($row->vehiculo_anio != "") ? $row->vehiculo_anio : "-";
                $contenedor["Service_Advisor_STARS_ID"] = "-";
                $contenedor["Vehicle_Check-In_Date"] = $row->fecha_recepcion." ".$row->hora_recepcion;
                $contenedor["Vehicle_Check-Out_Date"] = ($row->fecha_entrega_unidad != NULL) ? $row->fecha_entrega_unidad : "-";
                $contenedor["Promised_Date"] = $row->fecha_entrega." ".$row->hora_entrega;
                $contenedor["RO_Invoice_Date"] = "-";
                $contenedor["Signed_Documents"] = "-";
                $contenedor["Job_Card_Created_Date"] = "-";
                $contenedor["Planned_Job_Start"] = "-";
                $contenedor["Planned_Job_Finish"] = "-";
                $contenedor["Actual_Job_Start"] = "-";
                $contenedor["Actual_Job_Finish"] = "-";
                $contenedor["Job_Completion_Date"] = "-";
                $contenedor["Appointment_ID"] = $row->id_Cita_orden;
                $contenedor["Date_Appointment_Initiated"] = $row->fecha_creacion_all;
                $contenedor["Appointment_Type"] = "-";    //Catalogo

            }
            // $contenedor["respuesta"] = "OK";
        } catch (\Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;

        // $this->envioOrdenIntelisis($contenedor,$idOrden);
    }

    //Resibimos el id Cita para recuperar la informacion de la orden
    public function RO_Data_Services()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];

        try {
            // $idOrden = $_POST["idOrden"];

            $idOrden = "60";
            //Hacemos la consulta
            $query = $this->mConsultas->api_orden($idOrden);

            //Recuperamos la informacion de la consulta
            foreach ($query as $row) {

                $contenedor["BID"] = "";
                $contenedor["RO_Number"] = $row->id;
                $contenedor["Sequence_Number"] = "-";
                $contenedor["RO_Type_Code"] = "-";        //Catalogo
                $contenedor["Servicing_Department_Code"] = "-";   //Catalogo

                $items = $this->mConsultas->articulosTitulos($row->id);

                $datos["subtotal_items"] = 0;
                $datos["total_items"] = 0;

                $operaciones = "";
                foreach ($items as $row_items) {
                    $clave = "";
                    if ($row_items->tipo == 3) {
                        $clave .= "GEN";
                    }elseif (($row_items->tipo == 5)||($row_items->tipo == 4)) {
                        $clave .= $row_items->grupo."-";
                        $clave .= $row_items->descripcion;
                    }else {
                        $clave .= $row_items->grupoDes."-";
                        $clave .= $row_items->operacionDes;
                    }

                    $operaciones = $clave.",";                  //Clave del articulo recuperado de intelisis
                }

                $contenedor["Labor_Opcode"] = $operaciones;
                $contenedor["Planned_Start"] = "-";
                $contenedor["Planned_Finish"] = "-";
                $contenedor["Actual_Start"] = "-";
                $contenedor["Actual_Finish"] = "-";
                $contenedor["Pause_Duration"] = "-";
                $contenedor["Pause_Reason"] = "-";
                $contenedor["Billed_Hours"] = "-";
                $contenedor["Line_Status"] = "-";
                $contenedor["Technician"] = "-";

            }
            // $contenedor["respuesta"] = "OK";
        } catch (\Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    //Resibimos el id Cita para recuperar la informacion de la orden
    public function RO_Data_Parts()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];

        try {
            // $idOrden = $_POST["idOrden"];

            $idOrden = "60";
            //Hacemos la consulta
            $query = $this->mConsultas->api_orden($idOrden);

            //Recuperamos la informacion de la consulta
            foreach ($query as $row) {

                $contenedor["BID"] = BIN_SUCURSAL;
                $contenedor["RO_Number"] = $row->id;
                $contenedor["Sequence_Number"] = "-";

                $contenedor["Part_Prefix"] = "-";
                $contenedor["Part_Base"] = "-";
                $contenedor["Part_Suffix"] = "-";
                $contenedor["Part_Quantity"] = "-";
                $contenedor["Part_Description"] = "-";

            }
            // $contenedor["respuesta"] = "OK";
        } catch (\Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }



    // //Enviamos la orden a intelisis
    // public function envioOrdenIntelisis($contenedor = NULL,$idOrden = 0)
    // {
    //     // $url = "https://intelisisws.intelisis-solutions.com/api_plasencia/api/orden";
    //     $url = "plafordmaz.webhop.net/api_plasencia/api/orden";
    //
    //     $ch = curl_init($url);
    //     //Creamos el objeto json para enviar por url los datos del formulario
    //     $jsonDataEncoded = json_encode($contenedor);
    //     //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     //Adjuntamos el json a nuestra petición
    //     curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
    //     //Agregamos los encabezados del contenido
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    //     //Obtenemos la respuesta
    //     $response = curl_exec($ch);
    //
    //     // Se cierra el recurso CURL y se liberan los recursos del sistema
    //     curl_close($ch);
    //     $result = json_decode($response,true);
    //     // echo $response;
    //     if($response) {
    //         //Validamos la respuesta y actualizamos el id venta
    //         if (substr($response,0,11) != '{"Message":') {
    //             //Validamos la respuesta y actualizamos el id factura
    //             if ($result["ok"] != "Error") {
    //                 $dataIntelisis = array(
    //                     "idIntelisis" => $result["Id"],
    //                     "idVentaIntelisis" => $result["Idventa"]
    //                 );
    //
    //                 //Grabamos la informacion en la tabla
    //                 $actualizar = $this->mConsultas->update_table_row('ordenservicio',$dataIntelisis,'id_cita',$idOrden);
    //
    //                 if ($actualizar) {
    //                     $respuesta = "OK";
    //                 }else {
    //                     $respuesta = "NO SE PUDO ACTUALIZAR";
    //                 }
    //             }else {
    //                 $respuesta = $result["okref"];
    //             }
    //         }else {
    //             $respuesta = $response;
    //         }
    //     }else{
    //         $respuesta = "ERROR";
    //     }
    //
    //     echo $respuesta;
    // }

    // //Cargamos los datos de la multipunto
    // public function loadDataMultipunto1()
    // {
    //     //Creamos soporte de tiempo
    //     ini_set('max_execution_time',300);
    //     ini_set('set_time_limit',600);
    //
    //     ob_start("ob_gzhandler");
    //
    //     //Recibimos los datos
    //     $_POST = json_decode(file_get_contents('php://input'), true);
    //
    //     //Creamos contenedor
    //     $contenedor = [];
    //
    //     try {
    //         $idOrden = $_POST["idOrden"];
    //         //Cargamos la información del formulario
    //         $query = $this->mConsultas->multipuntoApi($idOrden);
    //
    //         foreach ($query as $row){
    //             $contenedor["Login"] = array(
    //                 "usuario" => "user",
    //                 "password" => "1234567"
    //             );
    //
    //             if ($row->bateriaEstado == "Aprobado") {
    //                 $bateria = "Verde";
    //             }elseif ($row->bateriaEstado == "Futuro") {
    //                 $bateria = "Amarillo";
    //             }else {
    //                 $bateria = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*1),       // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Baterias",             //Elemento
    //                 "respuesta" => $bateria,               //Indicador de color
    //                 "estatus" =>  ($row->bateriaCambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "",                        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->bateria."%",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->espesoBalataFI == "Aprobado") {
    //                 $balatas = "Verde";
    //             }elseif ($row->espesoBalataFI == "Futuro") {
    //                 $balatas = "Amarillo";
    //             }else {
    //                 $balatas = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*2),       // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Balata",             //Elemento
    //                 "respuesta" => $balatas,               //Indicador de color
    //                 "estatus" =>  ($row->espesorBalataFIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Frente Izquierdo",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorBalataFImm." mm",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->espesorDiscoFI == "Aprobado") {
    //                 $disco = "Verde";
    //             }elseif ($row->espesorDiscoFI == "Futuro") {
    //                 $disco = "Amarillo";
    //             }else {
    //                 $disco = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*3),      // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Disco",                //Elemento
    //                 "respuesta" => $disco,                //Indicador de color
    //                 "estatus" =>  ($row->espesorDiscoFIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Frente Izquierdo",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorDiscoFImm." mm",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->espesoBalataTI == "Aprobado") {
    //                 $balatas = "Verde";
    //             }elseif ($row->espesoBalataTI == "Futuro") {
    //                 $balatas = "Amarillo";
    //             }else {
    //                 $balatas = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*4),       // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Balata",             //Elemento
    //                 "respuesta" => $balatas,               //Indicador de color
    //                 "estatus" =>  ($row->espesorBalataTIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Trasera Izquierda",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorBalataTImm." mm",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->espesorDiscoTI == "Aprobado") {
    //                 $disco = "Verde";
    //             }elseif ($row->espesorDiscoTI == "Futuro") {
    //                 $disco = "Amarillo";
    //             }else {
    //                 $disco = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*5),      // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Disco",                //Elemento
    //                 "respuesta" => $disco,                //Indicador de color
    //                 "estatus" =>  ($row->espesorDiscoTIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Trasera Izquierda",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorDiscoTImm." mm",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->diametroTamborTI == "Aprobado") {
    //                 $disco = "Verde";
    //             }elseif ($row->diametroTamborTI == "Futuro") {
    //                 $disco = "Amarillo";
    //             }else {
    //                 $disco = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*6),      // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Tambor",                //Elemento
    //                 "respuesta" => $disco,                //Indicador de color
    //                 "estatus" =>  ($row->diametroTamborTIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Trasera Izquierda",        //Parte del vehículo del elemento, vacio para unico elemento
    //             );
    //
    //             if ($row->espesoBalataFD == "Aprobado") {
    //                 $balatas = "Verde";
    //             }elseif ($row->espesoBalataFD == "Futuro") {
    //                 $balatas = "Amarillo";
    //             }else {
    //                 $balatas = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*7),       // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Balata",             //Elemento
    //                 "respuesta" => $balatas,               //Indicador de color
    //                 "estatus" =>  ($row->espesorBalataFDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Frente Derecho",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorBalataFDmm." mm",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->espesorDiscoFD == "Aprobado") {
    //                 $disco = "Verde";
    //             }elseif ($row->espesorDiscoFD == "Futuro") {
    //                 $disco = "Amarillo";
    //             }else {
    //                 $disco = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*8),      // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Disco",                //Elemento
    //                 "respuesta" => $disco,                //Indicador de color
    //                 "estatus" =>  ($row->espesorDiscoFDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Frente Derecho",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorDiscoFDmm." mm",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->espesoBalataTD == "Aprobado") {
    //                 $balatas = "Verde";
    //             }elseif ($row->espesoBalataTD == "Futuro") {
    //                 $balatas = "Amarillo";
    //             }else {
    //                 $balatas = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*9),       // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Balata",             //Elemento
    //                 "respuesta" => $balatas,               //Indicador de color
    //                 "estatus" =>  ($row->espesorBalataTDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Trasera Derecha",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorBalataTDmm." mm",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->espesorDiscoTD == "Aprobado") {
    //                 $disco = "Verde";
    //             }elseif ($row->espesorDiscoTD == "Futuro") {
    //                 $disco = "Amarillo";
    //             }else {
    //                 $disco = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*10),      // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Disco",                //Elemento
    //                 "respuesta" => $disco,                //Indicador de color
    //                 "estatus" =>  ($row->espesorDiscoTDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Trasera Derecha",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorDiscoTDmm." mm",   //Valores adicionales del elemento
    //             );
    //
    //             if ($row->diametroTamborTD == "Aprobado") {
    //                 $disco = "Verde";
    //             }elseif ($row->diametroTamborTD == "Futuro") {
    //                 $disco = "Amarillo";
    //             }else {
    //                 $disco = "Rojo";
    //             }
    //
    //             $contenedor["inn"][] = array(
    //                 "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
    //                 "idrevision" => (string)(2024*11),      // ESTE ES UN CONSECUTIVO de 2024
    //                 "tipo" =>  "" ,                       // ? Vacio
    //                 "prioridad" => "",                    // ? Vacio
    //                 "elemento" => "Tambor",                //Elemento
    //                 "respuesta" => $disco,                //Indicador de color
    //                 "estatus" =>  ($row->diametroTamborTDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
    //                 "tecnico" => $row->tecnico,       //Técnico
    //                 "lado" =>  "Trasera Derecha",        //Parte del vehículo del elemento, vacio para unico elemento
    //                 //"datos_extra" => $row->espesorDiscoTDmm." mm",   //Valores adicionales del elemento
    //             );
    //
    //         }
    //
    //         if (isset($contenedor["Login"])) {
    //             $this->envioMultipuntoIntelisis($contenedor,$idOrden);
    //         }else {
    //             // $contenedor["respuesta"] = "NO SE ENCONTRO LA ORDEN";
    //             $contenedor["respuesta"] = "ERROR 01";
    //             // $respuesta = json_encode($contenedor);
    //             echo $contenedor["respuesta"];
    //         }
    //
    //       } catch (\Exception $e) {
    //           // $contenedor["respuesta"] = "ERROR EN EL PROCESO";
    //           $contenedor["respuesta"] = "ERROR 02";
    //           // $respuesta = json_encode($contenedor);
    //           echo $contenedor["respuesta"];
    //       }
    //
    //       // $respuesta = json_encode($contenedor);
    //       // echo $respuesta;
    // }

    // //Enviamos la hoja multipunto a intelisis
    // public function envioMultipuntoIntelisis($contenedor = NULL,$idOrden = 0)
    // {
    //     // $url = "https://intelisisws.intelisis-solutions.com/api_plasencia/api/multi";
    //     $url = "plafordmaz.webhop.net/api_plasencia/api/multi";
    //
    //     $ch = curl_init($url);
    //     //Creamos el objeto json para enviar por url los datos del formulario
    //     $jsonDataEncoded = json_encode($contenedor);
    //
    //     //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //
    //     //Adjuntamos el json a nuestra petición
    //     curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
    //     //Agregamos los encabezados del contenido
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    //
    //     //Obtenemos la respuesta
    //     $response = curl_exec($ch);
    //
    //     // Se cierra el recurso CURL y se liberan los recursos del sistema
    //     curl_close($ch);
    //     $result = json_decode($response,true);
    //     // echo $response;
    //     if($response) {
    //         //Validamos la respuesta y actualizamos el id factura
    //         if ($result["ok"] != "Error") {
    //             $respuesta = "OK";
    //         }else {
    //             $respuesta = $result["okref"];
    //         }
    //     }else{
    //         $respuesta = "ERROR";
    //     }
    //     // echo $response;
    //     if($response) {
    //         //Validamos la respuesta y actualizamos el id venta
    //         if (substr($response,0,11) != '{"Message":') {
    //             //Validamos la respuesta y actualizamos el id factura
    //             if ($result["ok"] != "Error") {
    //                 //Recuperamos el idOrden de la multipunto
    //                 $dataIntelisis = array(
    //                     "envioIntelisis" => 1,
    //                 );
    //
    //                 //Grabamos la informacion en la tabla
    //                 $actualizar = $this->mConsultas->update_table_row('multipunto_general',$dataIntelisis,'orden',$idOrden);
    //
    //                 if ($actualizar) {
    //                     $respuesta = "OK";
    //                 }else {
    //                     $respuesta = "NO SE PUDO ACTUALIZAR";
    //                 }
    //               }else {
    //                   $respuesta = $result["okref"];
    //               }
    //         }else {
    //             $respuesta = $response;
    //         }
    //     }else{
    //         $respuesta = "ERROR";
    //     }
    //
    //     echo $respuesta;
    // }

    //Quitar acentos
    public function quitar_tildes($cadena = "")
    {
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $texto = str_replace($no_permitidas, $permitidas ,$cadena);
        return $texto;
    }


}
