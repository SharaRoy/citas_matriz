<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soporte_login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'consulta', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

  	public function index($destino = "ADMIN",$resp = NULL)
  	{
        //Verificamos si hay alguna sesion iniciada
        if ($this->session->userdata('rolIniciado')) {
            $this->session->sess_destroy();
        }

        $datos["loginOrigen"] = "Soporte";
        $this->loadAllView($datos,'soporte/login','SOPORTE');
  	}

    public function verifica_login()
    {
        $usuariosPermitidos=[             
            //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
            '148',
            //Ángel Salas calderón  ID = 149 Usuario = angel
            '149',
            //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
            '150',
            //Shara Sahori   ID = 151 Usuario = shara
            '151',
        ];

        $usuario = $_POST['usuario'];
        $pass = $_POST['pass'];

        //Enviamos el usuario que va a accesar
        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();

        if (isset($row)) {
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;
            $nombre = $clave->row(0)->adminNombre;

            //Validamos los usuarios autorizados
            if (in_array($idUsuario, $usuariosPermitidos)) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => "SOPORTE",
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    $retorno = "OK";
                //Si las ocntraseñas no coinciden
                }else {
                    //Intentamso comprobar si existe la contraseña sin encriptar
                    if ($pass == $passDB) {
                        //Guardamos el la informacion del usuario en la sesion
                        $dataUser = array(
                            "usuario" => $usuario,
                            "nombreUsuario" => strtoupper($nombre),
                            "idUsuario" => $idUsuario,
                            "rolIniciado" => "SOPORTE",
                        );
                        //Guardamos los valores de sesion
                        $this->session->set_userdata($dataUser);
                        if (!$this->session->userdata('idUsuario')) {
                            $this->session->set_userdata($dataUser);
                        }

                        $retorno = "OK";
                    //No pasa la validacion
                    }else {
                        $retorno = "USUARIO NO PERMITIDO";
                    }
                }
            //Si el usuario no existe
            }else {
                $retorno = "USUARIO NO PERMITIDO";
            }
        //En caso que el usuario no sea un asesor
        }else {
            $retorno = "USUARIO NO PERMITIDO";
        }    
        
        echo $retorno;
    }

    public function verificar()
    {
        $usuariosPermitidos=[             
            //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
            '148',
            //Ángel Salas calderón  ID = 149 Usuario = angel
            '149',
            //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
            '150',
            //Shara Sahori   ID = 151 Usuario = shara
            '151',
        ];

        $usuario = $_POST['inputUsuario'];
        $pass = $_POST['inputPassword'];

        //Enviamos el usuario que va a accesar
        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();

        if (isset($row)) {
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;
            $nombre = $clave->row(0)->adminNombre;

            //Validamos los usuarios autorizados
            if (in_array($idUsuario, $usuariosPermitidos)) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => "SOPORTE",
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //$retorno = "OK";
                    redirect('Panel_soporte', 'refresh');
                //Si las ocntraseñas no coinciden
                }else {
                    //Intentamso comprobar si existe la contraseña sin encriptar
                    if ($pass == $passDB) {
                        //Guardamos el la informacion del usuario en la sesion
                        $dataUser = array(
                            "usuario" => $usuario,
                            "nombreUsuario" => strtoupper($nombre),
                            "idUsuario" => $idUsuario,
                            "rolIniciado" => "SOPORTE",
                        );
                        //Guardamos los valores de sesion
                        $this->session->set_userdata($dataUser);
                        if (!$this->session->userdata('idUsuario')) {
                            $this->session->set_userdata($dataUser);
                        }

                        //$retorno = "OK";
                        redirect('Panel_soporte', 'refresh');
                    //No pasa la validacion
                    }else {
                        $retorno = "USUARIO NO PERMITIDO";
                    }
                }
            //Si el usuario no existe
            }else {
                $retorno = "USUARIO NO PERMITIDO";
            }
        //En caso que el usuario no sea un asesor
        }else {
            $retorno = "USUARIO NO PERMITIDO";
        }    
        
        echo $retorno;
    }

    public function loadAllView($datos = NULL,$vista = "",$titulo = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/soporte/consultas.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Panel Soporte"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
