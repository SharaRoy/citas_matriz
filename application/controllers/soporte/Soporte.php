<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soporte extends CI_Controller { 

    public function __construct()
    {
        parent::__construct();
        //Cargamos los modelos para las consultas
        $this->load->model('mConsultas', 'consulta', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

    public function index($id_cita_url = '',$datos = NULL)
    {
        if (!$this->session->userdata('idUsuario')) {
            redirect('Login_soporte', 'refresh');
        }else{
            $id_cita = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
            $datos["id_cita"] = (($id_cita == "0") ? '' : $id_cita);
            $datos["tipo_vista"] = "Panel";
            $this->loadAllView($datos,'soporte/panel');
        }
    }

    public function historial($id_cita_url = '',$datos = NULL)
    {
        if (!$this->session->userdata('idUsuario')) {
            redirect('Login_soporte', 'refresh');
        }else{
            $datos["id_cita_url"] = $id_cita_url;
            $datos["id_cita"] = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
            $datos["tipo_vista"] = "Lista de Historico";

            $d_orden = $this->consulta->datos_cabecera($datos["id_cita"]);

            if (count($d_orden) > 0) {
                $datos["folio_externo"] = $d_orden["folioIntelisis"];
                $datos["unidad"] = $d_orden["subcategoria"];
                $datos["vehiculo"] = $d_orden["vehiculo_modelo"];
                $datos["placas"] = $d_orden["vehiculo_placas"];
                $datos["serie"] = (($d_orden["vehiculo_numero_serie"] != "") ? $d_orden["vehiculo_numero_serie"] : $d_orden["vehiculo_identificacion"]);
                $datos["asesor"] = $d_orden["asesor"];
                $datos["tecnico"] = $d_orden["tecnico"];
                $datos["cliente"] = $d_orden["datos_nombres"]." ".$d_orden["datos_apellido_paterno"]." ".$d_orden["datos_apellido_materno"];

                $datos["orden_abierta"] = ((($d_orden["id_situacion_intelisis"] != 5) && ($d_orden["id_situacion_intelisis"] != 6)) ? 1 : 0);
            }

            $datos["historico"] = $this->consulta->recuperar_historial($datos["id_cita"],"registro_historial_soporte");

            //Revisamos si hay evidencia cargada en el historial
            for ($i=0; $i < count($datos["historico"]); $i++) { 
                $datos["evidencias"][] = array(
                    "id_historico" => $datos["historico"][$i]["id"],
                    "url" => $this->consulta->evidencias_historica($datos["historico"][$i]["id"])
                );
                $datos["evidencias_inx"][] = $datos["historico"][$i]["id"];
            }

            $this->loadAllView($datos,'soporte/historico_soporte');
        }
    }

    public function cargar_datos(){

    	$id_cita = $_POST["orden"];

    	$contenedor = NULL;
    	$contenedor["informacion"] = 0;

        //Verificamos la documentacion existente y datos generales
        $info_orden = $this->consulta->recuperar_elementos_soporte($id_cita);

        if (count($info_orden) > 0) {
        	$contenedor["id_cita"] = $info_orden["orden_cita"];
            $contenedor["folio_externo"] = $info_orden["folioIntelisis"];
            
            $fecha_recepcion = new DateTime($info_orden["fecha_recepcion"]." ".$info_orden["hora_recepcion"]);
            $contenedor["fecha_recepcion"] = $fecha_recepcion->format('d-m-Y H:i:s');

            $fecha_promesa = new DateTime($info_orden["fecha_entrega"]." ".$info_orden["hora_entrega"]);
            $contenedor["fecha_promesa"] = $fecha_promesa->format('d-m-Y H:i:s');

            $contenedor["vehiculo"] = $info_orden["vehiculo_modelo"];
            $contenedor["placas"] = $info_orden["vehiculo_placas"];
            $contenedor["serie"] = (($info_orden["vehiculo_numero_serie"] != "") ? $info_orden["vehiculo_numero_serie"] : $info_orden["vehiculo_identificacion"]);

            $contenedor["asesor"] = $info_orden["asesor"];
            $contenedor["tecnico"] = $info_orden["tecnico"];
            $contenedor["cliente"] = $info_orden["datos_nombres"]." ".$info_orden["datos_apellido_paterno"]." ".$info_orden["datos_apellido_materno"];

            $contenedor["diagnostico"] = (($info_orden["idDiagnostico"] != "") ? "1" : "0");
            $contenedor["oasis"] = $info_orden["archivoOasis"];
            $contenedor["multipunto"] = (($info_orden["idMultipunto"] != "") ? "1" : "0");
            $contenedor["firma_jefetaller"] = (($info_orden["firmaJefeTaller"] != "") ? "1" : "0");
            
            $contenedor["presupuesto"] = (($info_orden["id_presupuesto"] != "") ? "1" : "0");
            $contenedor["presupuesto_afectado"] = $info_orden["acepta_cliente"];

            if ($contenedor["presupuesto"] == "1") {
                $garantias = $this->consulta->revision_garantias($id_cita);

                $contenedor["presupuesto_datos"] = array(
                    "ref_cgarantias" => (($garantias > 0) ? 1 : 0),
                    "envio_ventanilla" => (($info_orden["envia_ventanilla"] != "0") ? 1 : 0),
                    "envio_jdt" => (($info_orden["firma_jdt"] != "") ? 1 : 0),
                    "envio_garantias" => (($info_orden["firma_garantia"] != "") ? 1 : 0),
                    "envio_asesor" => (($info_orden["firma_asesor"] != "") ? 1 : 0),
                );
            } else {
                $contenedor["presupuesto_datos"] = NULL;
            }
            
            $contenedor["voz_cliente"] = ((($info_orden["idVoz"] != "")||($info_orden["vc_id"] != "")) ? "1" : "0");
            $contenedor["evidencia_voz_cliente"] = (($info_orden["evidencia"] != "") ? $info_orden["evidencia"] : (($info_orden["audio"] != "") ? $info_orden["audio"] : ""));
            $contenedor["servicio_valet"] = (($info_orden["idValet"] != "") ? "1" : "0");

            $contenedor["informacion"] = 1;
            $contenedor["documentacion"] = (($info_orden["documentacion"] != "") ? 1 : 0);
            $contenedor["id_cita_url"] = $this->encrypt($info_orden["orden_cita"]);
        }

        if (!isset($contenedor["documentacion"])) {
        	$contenedor["documentacion"] = 0;
        }

        //Verificamos si ha habido moviminetos de soporte
        $contenedor["historial_soporte"] = $this->consulta->revisar_soporte($id_cita);

    	echo json_encode( $contenedor );
    }

    public function actualizar_documentacion(){
    	//Recuperamos la informacion general
        $informacion_gral = $this->consulta->recuperar_elementos_soporte($_POST["orden"]);

        //foreach ($informacion_gral as $row) {           
            if ($_POST["destino"] == 1) {
            	$contededor["id"] = NULL;
            	$contededor["id_cita"] = $informacion_gral["orden_cita"];
            }
            
            $contededor["folio_externo"] = $informacion_gral["folioIntelisis"];
            $contededor["fecha_recepcion"] = $informacion_gral["fecha_recepcion"];
            $contededor["vehiculo"] = $informacion_gral["vehiculo_modelo"];
            $contededor["placas"] = $informacion_gral["vehiculo_placas"];
            $contededor["serie"] = (($informacion_gral["vehiculo_numero_serie"] != "") ? $informacion_gral["vehiculo_numero_serie"] : $informacion_gral["vehiculo_identificacion"]);
            $contededor["asesor"] = $informacion_gral["asesor"];
            $contededor["tecnico"] = $informacion_gral["tecnico"];
            $contededor["id_tecnico"] = $informacion_gral["id_tecnico"];
            $contededor["cliente"] = $informacion_gral["datos_nombres"]." ".$informacion_gral["datos_apellido_paterno"]." ".$informacion_gral["datos_apellido_materno"];
            $contededor["diagnostico"] = (($informacion_gral["idDiagnostico"] != "") ? "1" : "0");
            $contededor["oasis"] = ((isset($informacion_gral["archivoOasis"])) ? $informacion_gral["archivoOasis"] : "" );//$informacion_gral["archivoOasis"];
            $contededor["multipunto"] = (($informacion_gral["idMultipunto"] != "") ? "1" : "0");
            $contededor["firma_jefetaller"] = (($informacion_gral["firmaJefeTaller"] != "") ? "1" : "0");
            $contededor["presupuesto"] = ((isset($informacion_gral["id_presupuesto"])) ? $informacion_gral["id_presupuesto"] : "");
            $contededor["presupuesto_afectado"] = ((isset($informacion_gral["acepta_cliente"])) ? $informacion_gral["acepta_cliente"] : "");
            $contededor["presupuesto_origen"] = 1;
            $contededor["voz_cliente"] = ((($informacion_gral["idVoz"] != "")||($informacion_gral["vc_id"] != "")) ? "1" : "0");
            $contededor["evidencia_voz_cliente"] = (($informacion_gral["evidencia"] != "") ? $informacion_gral["evidencia"] : (($informacion_gral["audio"] != "") ? $informacion_gral["audio"] : ""));
            $contededor["servicio_valet"] = (($informacion_gral["idValet"] != "") ? "1" : "0");

            if ($_POST["destino"] == 1) {
            	$contededor["fechas_creacion"] = date("Y-m-d H:i:s");
            }
        //}

        if ($_POST["destino"] == 1) {
        	$guardado = $this->consulta->save_register('documentacion_ordenes',$contededor);

        	if ($guardado != 0) {
                $movimiento = (($_POST["destino"] == 1) ? "Se crea el registro de la documentación de manera manual por soporte ya que no se generero de manera automatica" : "Se actualizan los datos de la documentación desde soporte");
                $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Doc. Ordenes");

                $mov_historico = array(
                    "id" => NULL,
                    "id_cita" => $_POST["orden"],
                    "id_login" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : ''),
                    "usuario_login" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('usuario') : ''),
                    "usuario_edita" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('nombreUsuario') : ''),
                    "motivo_edicion" => (($_POST["destino"] == 1) ? "Se crea el registro de la documentación de manera manual por soporte" : "Se actualizan los datos de la documentación desde soporte"),
                    "formulario" => "Documentación de ordenes",
                    "respaldo_edicion" => NULL,
                    "fecha_registro" => date("Y-m-d H:i:s"),
                );

                $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

        		$respuesta = "OK";
        	} else {
        		$respuesta = "NO SE PUDO GUARDAR EL REGISTRO";
        	}
        } else {
        	$actualizar = $this->consulta->update_table_row('documentacion_ordenes',$contededor,'id_cita',$_POST["orden"]);
            if ($actualizar) {
                $movimiento = (($_POST["destino"] == 1) ? "Se crea el registro de la documentación de manera manual por soporte ya que no se generero de manera automatica" : "Se actualizan los datos de la documentación desde soporte");
                $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Doc. Ordenes");

                $mov_historico = array(
                    "id" => NULL,
                    "id_cita" => $_POST["orden"],
                    "id_login" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : ''),
                    "usuario_login" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('usuario') : ''),
                    "usuario_edita" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('nombreUsuario') : ''),
                    "motivo_edicion" => (($_POST["destino"] == 1) ? "Se crea el registro de la documentación de manera manual por soporte" : "Se actualizan los datos de la documentación desde soporte"),
                    "formulario" => "Documentación de ordenes",
                    "respaldo_edicion" => NULL,
                    "fecha_registro" => date("Y-m-d H:i:s"),
                );

                $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

        		$respuesta = "OK";
        	} else {
        		$respuesta = "NO SE PUDO ACTUALIZAR EL REGISTRO";
        	}
        }
        
        echo $respuesta;
    }

// --------------------------------------------------------------------------------------------
    public function verificacion_pm($id_cita_url = "",$tipo_edicion = "", $datos = NULL)
    {
        if (!$this->session->userdata('idUsuario')) {
            redirect('Login_soporte', 'refresh');
        }else{
            $datos["id_cita"] = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url)); 
            $datos["id_cita_url"] = $id_cita_url;
            //1 - Vista Asesor,  2 - Vista Jefe de taller, 3  -  Vista ventanilla, 4  - Vista rapida e historial
            $datos["tipo_vista"] = $tipo_edicion;
            
            //Recuperamos la informacion de los registros
            $query = $this->consulta->get_result_field("id_cita",$datos["id_cita"],"identificador","M","presupuesto_registro");
            foreach ($query as $row){
                $datos["folio_externo"] = $row->folio_externo;
                $datos["ref_garantias"] = $row->ref_garantias;
                //Valores para garantias
                $datos["cliente_paga"] = $row->cliente_paga;
                $datos["ford_paga"] = $row->ford_paga;
                $datos["total_pago_garantia"] = $row->total_pago_garantia;
                $datos["envio_garantia"] = $row->envio_garantia;
                $datos["comentario_garantia"] = $row->comentario_garantia;
                $datos["firma_garantia"] = $row->firma_garantia;
                $fecha_c = new DateTime($row->termina_garantias);
                $datos["fecha_fgarantias"] = $fecha_c->format('d-m-Y H:i:s');

                //Valores globales
                $datos["anticipo"] = $row->anticipo;
                $datos["notaAnticipo"] = $row->nota_anticipo;
                $datos["subtotal"] = $row->subtotal;
                $datos["cancelado"] = $row->cancelado;
                $datos["iva"] = $row->iva;
                $datos["total"] = (((float)$row->subtotal - (float)$row->cancelado) * 1.16)-$row->anticipo;

                //Valores de proceso
                $datos["envia_ventanilla"] = $row->envia_ventanilla;
                $fecha_d = new DateTime($row->termina_ventanilla);
                $datos["fecha_fventanilla"] = $fecha_d->format('d-m-Y H:i:s');
                $datos["ubicacion_proceso"] = $row->ubicacion_proceso;
                $datos["ubicacion_proceso_garantia"] = $row->ubicacion_proceso_garantia;
                $datos["estado_refacciones"] = $row->estado_refacciones;
                $datos["estado_refacciones_garantias"] = $row->estado_refacciones_garantias;

                //Archivos
                $datos["archivos_presupuesto"] = explode("|",$row->archivos_presupuesto);
                $datos["archivos_presupuesto_lineal"] = $row->archivos_presupuesto;

                $datos["archivo_tecnico"] = explode("|",$row->archivo_tecnico);
                $datos["archivo_tecnico_lineal"] = $row->archivo_tecnico;

                $datos["archivo_jdt"] = explode("|",$row->archivo_jdt);
                $datos["archivo_jdt_lineal"] = $row->archivo_jdt;
                
                $datos["firma_asesor"] = $row->firma_asesor;
                $datos["nota_asesor"] = $row->comentario_asesor;
                $fecha_a = new DateTime($row->termina_asesor);
                $datos["fecha_fasesor"] = $fecha_a->format('d-m-Y H:i:s');
                
                $datos["envio_jdt"] = $row->envio_jdt;
                $datos["firma_jdt"] = $row->firma_jdt;
                $datos["comentario_jdt"] = $row->comentario_jdt;
                $fecha_b = new DateTime($row->termina_jdt);
                $datos["fecha_fjdt"] = $fecha_b->format('d-m-Y H:i:s');
                
                $datos["comentario_ventanilla"] = $row->comentario_ventanilla;
                $datos["comentario_tecnico"] = $row->comentario_tecnico;
                
                $datos["serie"] = $row->serie;
                $datos["firma_requisicion"] = (($row->firma_requisicion != "") ? "1" : "0");
                //$datos["firma_requisicion_url"] = $row->firma_requisicion;
                $datos["firma_requisicion_garantias"] = (($row->firma_requisicion_garantias != "") ? "1" : "0");
                //$datos["firma_requisicion_garantias_url"] = $row->firma_requisicion_garantias;
                $datos["correo_adicional"] = $row->correo_adicional;
                $datos["envio_proactivo"] = $row->envio_proactivo;
                $datos["acepta_cliente"] = $row->acepta_cliente;

                $datos["fecha_alta"] = explode(" ",$row->fecha_alta);
                $fragmentos = explode("-",$datos["fecha_alta"][0]);
                $mes_index = (int)$fragmentos[1];
                $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
                //$datos["mes"] = $meses[date("n")-1];
                $datos["fecha_limite"] = $fragmentos[2]." de ".$meses[(($mes_index < 12) ? $mes_index : 0)]." del ".$fragmentos[0];

                $datos["envia_garantia_mod"] = $row->anexa_garantias;
            }

            $query_2 = $this->consulta->get_result_field("id_cita",$datos["id_cita"],"identificador","M","presupuesto_multipunto");

            $datos["registrosControl"] = 0;
            $datos["registrosGarantias"] = 0;

            $datos["pzs_aprobadas"] = 0;
            $datos["pzs_entregadas"] = 0;
            $datos["pzs_recibidas"] = 0;
            $datos["pzs_entregada_ref"] = 0;

            $datos["sin_aprobar_jdt"] = 0;

            foreach ($query_2 as $row){
                if ($row->activo == "1") {
                    $datos["id_refaccion"][] = $row->id;

                    $datos["cantidad"][] = $row->cantidad;
                    $datos["descripcion"][] = $row->descripcion;
                    
                    $datos["prefijo"][] = $row->prefijo;
                    $datos["basico"][] = $row->basico;
                    $datos["sufijo"][] = $row->sufijo;
                    $datos["num_pieza"][] = $row->num_pieza;

                    $datos["existe"][] = $row->existe;
                    $datos["fecha_planta"][] = $row->fecha_planta;
                    $datos["costo_pieza"][] = $row->costo_pieza;
                    $datos["horas_mo"][] = $row->horas_mo;
                    $datos["costo_mo"][] = $row->costo_mo;
                    $datos["total_refacion"][] = $row->total_refacion;
                    $datos["prioridad"][] = $row->prioridad;

                    $datos["renglon"][] = $row->cantidad."_".$row->descripcion."_".$row->num_pieza."_".$row->existe."*".$row->fecha_planta."_".$row->costo_pieza."_".$row->horas_mo."_".$row->costo_mo."_".$row->total_refacion."_".$row->prioridad."_".$row->ref;

                    $datos["pza_garantia"][] = $row->pza_garantia;

                    $datos["costo_ford"][] = $row->costo_ford;
                    $datos["horas_mo_garantias"][] = $row->horas_mo_garantias;
                    $datos["costo_mo_garantias"][] = $row->costo_mo_garantias;

                    $datos["renglonGarantia"][] = $row->costo_ford."_".$row->horas_mo_garantias."_".$row->costo_mo_garantias;

                    $datos["autoriza_garantia"][] = $row->autoriza_garantia;
                    $datos["autoriza_jefeTaller"][] = $row->autoriza_jefeTaller;
                    $datos["autoriza_asesor"][] = $row->autoriza_asesor;
                    $datos["autoriza_cliente"][] = $row->autoriza_cliente;

                    $datos["estado_entrega"][] = $row->estado_entrega;
                    $datos["referencia"][] = $row->ref;

                    if ($row->autoriza_jefeTaller == 0) {
                        $datos["sin_aprobar_jdt"] += 1;
                    }

                    $datos["valida_uso"][] = $row->asc_servicio_req;
                    $datos["req_pza_gerente"][] = $row->nombre_gerente_req;
                    $datos["req_pza_firma"][] = $row->firma_gerente_req;
                    $datos["req_pza_parte"][] = $row->parte_remplazada_req;

                    $datos["alta_garantia"][] = $row->alta_garantia;
                    $datos["procesada_alta_garantia"][] = $row->fecha_precesa_altag;
                    
                    $fecha_2 = new DateTime($row->fecha_firma_greq);
                    $datos["req_pza_fecha"][] = $fecha_2->format('d-m-Y');

                    $fecha_a = new DateTime($row->fecha_entrega);
                    $datos["fecha_entrega"][] = $fecha_a->format('d-m-Y H:i:s');
                    $datos["firma_ventanilla"][] = $row->firma_ventanilla_entrega;

                    //Refacciones (activas) totales del presupuesto
                    $datos["registrosControl"] += 1;
                    //Refacciones (activas) con garantias totales del presupuesto
                    if ($row->pza_garantia == "1") {
                        $datos["registrosGarantias"] += 1;
                    }

                    //Contabilizamos refaccions aprobadas
                    if (($row->autoriza_garantia == "1")||($row->autoriza_cliente =="1")) {
                        $datos["pzs_aprobadas"]  += 1;

                        //Verificamos si es una refaccion o mano de obra
                        if ((($row->costo_pieza == 0)&&($row->num_pieza == ""))||(($row->costo_ford == 0)&&($row->autoriza_garantia == "1"))) {
                            $datos["tipo_refaccion"][] = "MO";
                            $datos["pzs_entregada_ref"] += 1;
                        } else {
                            $datos["tipo_refaccion"][] = "REF";
                            if ($row->estado_entrega == "3") {
                                $datos["pzs_entregada_ref"] += 1;
                            }
                        }
                    }

                    if ($row->estado_entrega == "3") {
                        $datos["pzs_entregadas"] += 1;
                    }

                    if ($row->estado_entrega == "2") {
                        $datos["pzs_recibidas"] += 1;
                    }
                }
            }

            $d_orden = $this->consulta->datos_cabecera($datos["id_cita"]);

            if (count($d_orden) > 0) {
                //$datos["folio_externo"] = $d_orden["folioIntelisis"];
                $datos["unidad"] = $d_orden["subcategoria"];
                $datos["vehiculo"] = $d_orden["vehiculo_modelo"];
                $datos["placas"] = $d_orden["vehiculo_placas"];
                $datos["serie"] = (($d_orden["vehiculo_numero_serie"] != "") ? $d_orden["vehiculo_numero_serie"] : $d_orden["vehiculo_identificacion"]);
                $datos["asesor"] = $d_orden["asesor"];
                $datos["tecnico"] = $d_orden["tecnico"];
            }

            $datos["historial_presupuesto"] = $this->consulta->recuperar_historial($datos["id_cita"],"presupuestos_historial");

            $this->loadAllView($datos,'soporte/soporte_presupuesto');
        }
    }

    public function editar_presupuesto()
    {
        $mensaje = "";

        if (!$this->session->userdata('idUsuario')) {
            $mensaje = "LA SESIÓN HA CADUCADO, FAVOR DE VERIFICAR";
        } else {
            //Recuperamos los datos del presupuesto
            $query = $this->consulta->get_result_field("id_cita",$_POST["orden"],"identificador","M","presupuesto_registro");
            foreach ($query as $row){
                $envio_garantia = $row->envio_garantia;
                $firma_garantia = $row->firma_garantia;
                
                //Valores de proceso
                $envia_ventanilla = $row->envia_ventanilla;
                $ubicacion_proceso = $row->ubicacion_proceso;
                $ubicacion_proceso_garantia = $row->ubicacion_proceso_garantia;
                $estado_refacciones = $row->estado_refacciones;

                $firma_asesor = $row->firma_asesor;            
                $envio_jdt = $row->envio_jdt;
                $firma_jdt = $row->firma_jdt;

                $fecha_fgarantias = $row->termina_garantias;
                $fecha_fventanilla = $row->termina_ventanilla;
                $fecha_fasesor = $row->termina_asesor;
                $fecha_fjdt = $row->termina_jdt;

                $acepta_cliente = $row->acepta_cliente;
            }

            //Identificamos a que fraccion se va a modificar
            if ($_POST["tipo_pre"] == "asesor") {
                $cambios = "Se regresa el presupuesto al asesor, se remueve la firma en la dirección [".$firma_asesor."] que se realizo el [".$fecha_fasesor."]. El presupuesto ".(($acepta_cliente == "") ? "No habia sido afectado" : ($acepta_cliente == "Si") ? "Habia sido aprobado totalmente" : (($acepta_cliente == "Val") ? "Habia sido afectado parcialmente" : "Habia sido rechazado totalmente"));

                $limpieza = array(
                    "firma_asesor" => "",
                    "acepta_cliente" => "",
                    "ubicacion_proceso" => "Espera revisión del asesor" ,
                    "termina_asesor" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s"),
                );

                //Guardamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row_2('presupuesto_registro',$limpieza,"id_cita",$_POST["orden"],"identificador","M");

                if ($actualizar) {
                    $movimiento = "Se regreso el presupuesto al asesor desde soporte por: ".strtoupper($_POST["usuario"]);
                    $this->registrar_movimiento_presupuesto($_POST["orden"],$movimiento,NULL);
                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Presupuesto");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Presupuesto Multipunto",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK";
                } else {
                    $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO";
                }

            } elseif ($_POST["tipo_pre"] == "garantias") {
                $cambios = "Se regresa el presupuesto a garantias, se remueve la firma en la dirección [".$firma_garantia."] que se realizo el [".$fecha_fgarantias."]";

                $limpieza = array(
                    "envio_garantia" => "0",
                    "firma_garantia" => "",
                    "ubicacion_proceso" => "Espera revisión de garantías" ,
                    "termina_garantias" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s"),
                );

                //Guardamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row_2('presupuesto_registro',$limpieza,"id_cita",$_POST["orden"],"identificador","M");

                if ($actualizar) {
                    $movimiento = "Se regreso el presupuesto a garantías desde soporte por: ".strtoupper($_POST["usuario"]);
                    $this->registrar_movimiento_presupuesto($_POST["orden"],$movimiento,NULL);
                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Presupuesto");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Presupuesto Multipunto",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK";
                } else {
                    $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO";
                }
            } elseif ($_POST["tipo_pre"] == "jtaller") {
                $cambios = "Se regresa el presupuesto al jefe de taller, se remueve la firma en la dirección [".$firma_jdt."] que se realizo el [".$fecha_fjdt."]";

                $limpieza = array(
                    "envio_jdt" => "0",
                    "firma_jdt" => "",
                    "ubicacion_proceso" => "Espera revisión del Jefe de Taller" ,
                    "termina_jdt" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s"),
                );

                //Guardamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row_2('presupuesto_registro',$limpieza,"id_cita",$_POST["orden"],"identificador","M");

                if ($actualizar) {
                    $movimiento = "Se regreso el presupuesto a garantías desde soporte por: ".strtoupper($_POST["usuario"]);
                    $this->registrar_movimiento_presupuesto($_POST["orden"],$movimiento,NULL);
                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Presupuesto");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Presupuesto Multipunto",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK";
                } else {
                    $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO";
                }
            } elseif ($_POST["tipo_pre"] == "ventanilla") {
                $cambios = "Se regresa el presupuesto a ventanilla, proceso que termino el : [".$fecha_fventanilla."]";

                $limpieza = array(
                    "envio_jdt" => "0",
                    "firma_jdt" => "",
                    "ubicacion_proceso" => "Espera revisión de Ventanilla" ,
                    "fecha_fventanilla" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s"),
                );

                //Guardamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row_2('presupuesto_registro',$limpieza,"id_cita",$_POST["orden"],"identificador","M");

                if ($actualizar) {
                    $movimiento = "Se regreso el presupuesto a garantías desde soporte por: ".strtoupper($_POST["usuario"]);
                    $this->registrar_movimiento_presupuesto($_POST["orden"],$movimiento,NULL);
                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Presupuesto");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Presupuesto Multipunto",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK";
                } else {
                    $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO";
                }
            }else{
                $mensaje = "OPCIÓN INVALIDA";
            }
        }

        echo $mensaje;
    }

// --------------------------------------------------------------------------------------------
    public function verificacion_hm($id_cita_url = "",$tipo_edicion = "", $datos = NULL)
    {
        if (!$this->session->userdata('idUsuario')) {
            redirect('Login_soporte', 'refresh');
        }else{
            $datos["id_cita"] = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
            $datos["id_cita_url"] = $id_cita_url;
            $datos["tipo_vista"] = $tipo_edicion;

            $query = $this->consulta->get_result("orden",$datos["id_cita"],"multipunto_general");
            foreach ($query as $row){
                $fecha_c = new DateTime($row->fechaCaptura);
                $datos["fechaCaptura"] = $fecha_c->format('d-m-Y');

                $datos["noOrden"] = $row->orden;
                $datos["noSerie"] = $row->noSerie;

                //Recuperamos todas las campañas del vehiculo
                $campanias = $this->consulta->get_result('vin',$row->noSerie,'campanias');
                $datos["campania"] = "[";
                foreach ($campanias as $row_1) {
                    $datos["campania"] .= $row_1->tipo.",";
                }
                $datos["campania"] .= "]";

                $datos["modelo"] = $row->modelo;
                //Filtrado del modelo
                $datos["nvoModelo"] = $row->modelo;

                $datos["torre"] = $row->torre;
                $datos["nombreDueno"] = $row->nombreDueno;
                $datos["email"] = $row->email;
                $datos["emailComp"] = $row->emailCompania;
                $datos["perdidaAceite"] = $row->perdidaAceite;
                $datos["cambioAceite"] = $row->cambioAceite;
                $datos["aceiteMotor"] = $row->aceiteMotor;
                $datos["direccionHidraulica"] = $row->direccionHidraulica;
                $datos["transmision"] = $row->transmision;
                $datos["fluidoFrenos"] = $row->fluidoFrenos;
                $datos["limpiaparabrisas"] = $row->limpiaparabrisas;
                $datos["refrigerante"] = $row->refrigerante;
                $datos["plumasPruebas"] = $row->plumasPruebas;
                $datos["plumas"] = $row->plumas;
                $datos["plumasCambio"] = $row->plumasCambio;
                $datos["lucesClaxon"] = $row->lucesClaxon;
                $datos["lucesClaxonCambio"] = $row->lucesClaxonCambio;
                $datos["grietasParabrisas"] = $row->grietasParabrisas;
                $datos["grietasCambio"] = $row->grietasCambio;

                //Convertimos el porcentaje de la bateria en valores para mostrar
                $datos["bateriaValor"] = (int)$row->bateria;
                if (($datos["bateriaValor"] >= 0)&&($datos["bateriaValor"] < 10)) {
                    $datos["bateria"] = "-80px";
                }elseif (($datos["bateriaValor"] >= 10)&&($datos["bateriaValor"] < 20)) {
                    $datos["bateria"] = "-67px";
                }elseif (($datos["bateriaValor"] >= 20)&&($datos["bateriaValor"] < 30)) {
                    $datos["bateria"] = "-46px";
                }elseif (($datos["bateriaValor"] >= 30)&&($datos["bateriaValor"] < 40)) {
                    $datos["bateria"] = "-29px";
                }elseif (($datos["bateriaValor"] >= 40)&&($datos["bateriaValor"] < 50)) {
                    $datos["bateria"] = "-12px";
                }elseif (($datos["bateriaValor"] >= 50)&&($datos["bateriaValor"] < 60)) {
                    $datos["bateria"] = "5px";
                }elseif (($datos["bateriaValor"] >= 60)&&($datos["bateriaValor"] < 70)) {
                    $datos["bateria"] = "22px";
                }elseif (($datos["bateriaValor"] >= 70)&&($datos["bateriaValor"] < 80)) {
                    $datos["bateria"] = "39px";
                }elseif (($datos["bateriaValor"] >= 80)&&($datos["bateriaValor"] < 90)) {
                    $datos["bateria"] = "56px";
                }else{
                    $datos["bateria"] = "79px";
                }

                $datos["bateriaCambio"] = $row->bateriaCambio;
                $datos["bateriaEstado"] = $row->bateriaEstado;
                $datos["frio"] = $row->frio;
                $datos["frioReal"] = $row->frioReal;
                $datos["id_reg_gral"] = $row->id;
            }

            $query_2 = $this->consulta->get_result("idOrden",$datos["id_reg_gral"],"multipunto_general_2");
            foreach ($query_2 as $row){
                $datos["id_reg_gral2"] = $row->idGral2;

                $datos["sistemaCalefaccion"] = $row->sistemaCalefaccion;
                $datos["sistemaCalefaccionCambio"] = $row->sistemaCalefaccionCambio;
                $datos["sisRefrigeracion"] = $row->sisRefrigeracion;
                $datos["sisRefrigeracionCambio"] = $row->sisRefrigeracionCambio;
                $datos["bandas"] = $row->bandas;
                $datos["bandasCambio"] = $row->bandasCambio;
                $datos["sisFrenos"] = $row->sisFrenos;
                $datos["sisFrenosCambio"] = $row->sisFrenosCambio;
                $datos["suspension"] = $row->suspension;
                $datos["suspensionCambio"] = $row->suspensionCambio;
                $datos["varillajeDireccion"] = $row->varillajeDireccion;
                $datos["varillajeDireccionCambio"] = $row->varillajeDireccionCambio;
                $datos["sisEscape"] = $row->sisEscape;
                $datos["sisEscapeCambio"] = $row->sisEscapeCambio;
                $datos["funEmbrague"] = $row->funEmbrague;
                $datos["funEmbragueCambio"] = $row->funEmbragueCambio;
                $datos["flechaCardan"] = $row->flechaCardan;
                $datos["flechaCardanCambio"] = $row->flechaCardanCambio;
                $datos["defectosImg"] = (file_exists($row->defectosImg)) ? $row->defectosImg : "";
                $datos["medicionesFrenos"] = $row->medicionesFrenos;
                $datos["indicadorAceite"] = $row->indicadorAceite;
                $datos["comentarios"] = $row->comentarios;
            }

            $query_3 = $this->consulta->get_result("idOrden",$datos["id_reg_gral"],"frente_izquierdo");
            foreach ($query_3 as $row){
                $datos["id_reg_fti"] = $row->idFI;

                $datos["pneumaticoFI"] = $row->pneumaticoFI;
                $datos["pneumaticoFImm"] = $row->pneumaticoFImm;
                $datos["pneumaticoFIcambio"] = $row->pneumaticoFIcambio;
                $datos["dNeumaticoFI"] = $row->dNeumaticoFI;
                $datos["dNeumaticoFIcambio"] = $row->dNeumaticoFIcambio;
                $datos["presionInfladoFI"] = $row->presionInfladoFI;
                $datos["presionInfladoFIpsi"] = $row->presionInfladoFIpsi;
                $datos["presionInfladoFIcambio"] = $row->presionInfladoFIcambio;
                $datos["espesoBalataFI"] = $row->espesoBalataFI;
                $datos["espesorBalataFImm"] = $row->espesorBalataFImm;
                $datos["espesorBalataFIcambio"] = $row->espesorBalataFIcambio;
                $datos["espesorDiscoFI"] = $row->espesorDiscoFI;
                $datos["espesorDiscoFImm"] = $row->espesorDiscoFImm;
                $datos["espesorDiscoFIcambio"] = $row->espesorDiscoFIcambio;
            }

            $query_4 = $this->consulta->get_result("idOrden",$datos["id_reg_gral"],"trasero_izquierdo");
            foreach ($query_4 as $row){
                $datos["id_reg_tri"] = $row->idTI;

                $datos["pneumaticoTI"] = $row->pneumaticoTI;
                $datos["pneumaticoTImm"] = $row->pneumaticoTImm;
                $datos["pneumaticoTIcambio"] = $row->pneumaticoTIcambio;
                $datos["dNeumaticoTI"] = $row->dNeumaticoTI;
                $datos["dNeumaticoTIcambio"] = $row->dNeumaticoTIcambio;
                $datos["presionInfladoTI"] = $row->presionInfladoTI;
                $datos["presionInfladoTIpsi"] = $row->presionInfladoTIpsi;
                $datos["presionInfladoTIcambio"] = $row->presionInfladoTIcambio;
                $datos["espesoBalataTI"] = $row->espesoBalataTI;
                $datos["espesorBalataTImm"] = $row->espesorBalataTImm;
                $datos["espesorBalataTIcambio"] = $row->espesorBalataTIcambio;
                $datos["espesorDiscoTI"] = $row->espesorDiscoTI;
                $datos["espesorDiscoTImm"] = $row->espesorDiscoTImm;
                $datos["espesorDiscoTIcambio"] = $row->espesorDiscoTIcambio;
                $datos["diametroTamborTI"] = $row->diametroTamborTI;
                $datos["diametroTamborTImm"] = $row->diametroTamborTImm;
                $datos["diametroTamborTIcambio"] = $row->diametroTamborTIcambio;
            }

            $query_5 = $this->consulta->get_result("idOrden",$datos["id_reg_gral"],"frente_derecho");
            foreach ($query_5 as $row){
                $datos["id_reg_ftd"] = $row->idFD;

                $datos["pneumaticoFD"] = $row->pneumaticoFD;
                $datos["pneumaticoFDmm"] = $row->pneumaticoFDmm;
                $datos["pneumaticoFDcambio"] = $row->pneumaticoFDcambio;
                $datos["dNeumaticoFD"] = $row->dNeumaticoFD;
                $datos["dNeumaticoFDcambio"] = $row->dNeumaticoFDcambio;
                $datos["presionInfladoFD"] = $row->presionInfladoFD;
                $datos["presionInfladoFDpsi"] = $row->presionInfladoFDpsi;
                $datos["presionInfladoFDcambio"] = $row->presionInfladoFDcambio;
                $datos["espesoBalataFD"] = $row->espesoBalataFD;
                $datos["espesorBalataFDmm"] = $row->espesorBalataFDmm;
                $datos["espesorBalataFDcambio"] = $row->espesorBalataFDcambio;
                $datos["espesorDiscoFD"] = $row->espesorDiscoFD;
                $datos["espesorDiscoFDmm"] = $row->espesorDiscoFDmm;
                $datos["espesorDiscoFDcambio"] = $row->espesorDiscoFDcambio;
            }

            $query_6 = $this->consulta->get_result("idOrden",$datos["id_reg_gral"],"trasero_derecho");
            foreach ($query_6 as $row){
                $datos["id_reg_trd"] = $row->idTD;

                $datos["pneumaticoTD"] = $row->pneumaticoTD;
                $datos["pneumaticoTDmm"] = $row->pneumaticoTDmm;
                $datos["pneumaticoTDcambio"] = $row->pneumaticoTDcambio;
                $datos["dNeumaticoTD"] = $row->dNeumaticoTD;
                $datos["dNeumaticoTDcambio"] = $row->dNeumaticoTDcambio;
                $datos["presionInfladoTD"] = $row->presionInfladoTD;
                $datos["presionInfladoTDpsi"] = $row->presionInfladoTDpsi;
                $datos["presionInfladoTDcambio"] = $row->presionInfladoTDcambio;
                $datos["espesoBalataTD"] = $row->espesoBalataTD;
                $datos["espesorBalataTDmm"] = $row->espesorBalataTDmm;
                $datos["espesorBalataTDcambio"] = $row->espesorBalataTDcambio;
                $datos["espesorDiscoTD"] = $row->espesorDiscoTD;
                $datos["espesorDiscoTDmm"] = $row->espesorDiscoTDmm;
                $datos["espesorDiscoTDcambio"] = $row->espesorDiscoTDcambio;
                $datos["diametroTamborTD"] = $row->diametroTamborTD;
                $datos["diametroTamborTDmm"] = $row->diametroTamborTDmm;
                $datos["diametroTamborTDcambio"] = $row->diametroTamborTDcambio;
            }

            $query_7 = $this->consulta->get_result("idOrden",$datos["id_reg_gral"],"multipunto_general_3");
            foreach ($query_7 as $row){
                $datos["id_reg_gral3"] = $row->idGral3;
                $datos["presion"] = $row->presion;
                $datos["presionPSI"] = $row->presionPSI;
                $datos["presionCambio"] = $row->presionCambio;
                $datos["sistema"] = $row->sistema;
                $datos["componente"] = $row->componente;
                $datos["causaRaiz"] = $row->causaRaiz;
                $datos["comentario"] = $row->comentario;
                $datos["archivo"] = explode("|",$row->archivo);
                $datos["archivoTemp"] = $row->archivo;

                $datos["asesor"] = $row->asesor;
                $datos["firmaAsesor"] = $row->firmaAsesor;

                $datos["tecnico"] = $row->tecnico;
                $datos["firmaTecnico"] = $row->firmaTecnico;

                $datos["nombreCliente"] = $row->nombreCliente;
                $datos["firmaCliente"] = $row->firmaCliente;

                $datos["jefeTaller"] = $row->jefeTaller;
                $datos["firmaJefeTaller"] = $row->firmaJefeTaller;
            }

            if ($datos["tipo_vista"] == "3") {
                $datos["multipunto_reg"] = $this->consulta->recuperar_multipuntos($datos["id_cita"],"multipunto_general");
            }

            if (($datos["tipo_vista"] == "1")||($datos["tipo_vista"] == "4")) {
                $this->loadAllView($datos,'soporte/soporte_hmultipunto');
            } else {
                $d_orden = $this->consulta->datos_cabecera($datos["id_cita"]);

                if (count($d_orden) > 0) {
                    $datos["folio_externo"] = $d_orden["folioIntelisis"];
                    $datos["unidad"] = $d_orden["subcategoria"];
                    $datos["vehiculo"] = $d_orden["vehiculo_modelo"];
                    $datos["placas"] = $d_orden["vehiculo_placas"];
                    $datos["serie"] = (($d_orden["vehiculo_numero_serie"] != "") ? $d_orden["vehiculo_numero_serie"] : $d_orden["vehiculo_identificacion"]);
                    //$datos["asesor"] = $d_orden["asesor"];
                    //$datos["tecnico"] = $d_orden["tecnico"];
                }

                $this->loadAllView($datos,'soporte/soporte_hmultipunto_2');
            }   
        }
    }

    public function editar_hojamultipunto()
    {
        $mensaje = "";

        if (!$this->session->userdata('idUsuario')) {
            $mensaje = "LA SESIÓN HA CADUCADO, FAVOR DE VERIFICAR";
        } else {
            if ($_POST["tipo_pre"] == "1") {
                $query = $this->consulta->get_result("idOrden",$_POST['indicador_gral'],"multipunto_general_3");
                foreach ($query as $row){
                    $firma_tecnico = $row->firmaTecnico;
                    $firma_jefetaller = $row->firmaJefeTaller;
                }

                if ($_POST["firma_remv"] == "tecnico") {
                    $cambios = "Se elimina la firma del técnico (sin afectar los indicadores) ubicada en la ruta : [".$firma_tecnico."]";

                    $limpieza = array(
                        "firmaTecnico" => "",
                    );

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row('multipunto_general_3',$limpieza,'idOrden',$_POST['indicador_gral']);

                    if ($actualizar) {
                        $movimiento = "Se elimina la firma del técnico (sin afectar los indicadores) desde soporte por: ".strtoupper($_POST["usuario"]);
                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Hoja Multipunto",
                            "respaldo_edicion" => $cambios,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }
                } else if ($_POST["firma_remv"] == "jefeTaller") {
                    $cambios = "Se elimina la firma del jefe de taller (sin afectar los indicadores) ubicada en la ruta : [".$firma_jefetaller."]";

                    $limpieza = array(
                        "firmaJefeTaller" => "",
                    );

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row('multipunto_general_3',$limpieza,'idOrden',$_POST['indicador_gral']);

                    if ($actualizar) {
                        $movimiento = "Se elimina la firma del jefe de taller (sin afectar los indicadores) desde soporte por el usuario : ".strtoupper($_POST["usuario"]);
                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Hoja Multipunto",
                            "respaldo_edicion" => $cambios,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }
                } else{
                    $mensaje = "NO SE RECONOCIO LA FIRMA QUE SE VA A ELIMINAR=";
                }
                
            } elseif ($_POST["tipo_pre"] == "2") {
                $cambios = "Se reasigno el número de orden para que se creara un nuevo formulario de la orden [".$_POST["orden"]."] -> [".$_POST["orden"]."HME]";

                $limpieza = array(
                    "orden" => $_POST["orden"]."HME",
                );

                //Guardamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row('multipunto_general',$limpieza,'id',$_POST['indicador_gral']);

                if ($actualizar) {
                    $movimiento = "Se reasigno el número de orden para que se creara un nuevo formulario de la orden [".$_POST["orden"]."] -> [".$_POST["orden"]."HME] por el usuario :".strtoupper($_POST["usuario"]);

                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Hoja Multipunto",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK=".$id_retorno;
                } else {
                    $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                }
            
            } elseif ($_POST["tipo_pre"] == "3") {
                $cambios = "Se reasigna la orden porque se dublico y esta generando problemas con los tableros [".$_POST["orden"]."] -> [".$_POST["orden"]."HME]";

                $limpieza = array(
                    "orden" => $_POST["orden"]."HME",
                );

                //Guardamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row('multipunto_general',$limpieza,'id',$_POST['indicador_gral']);

                if ($actualizar) {
                    $movimiento = "Se reasigna la orden porque se dublico y esta generando problemas con los tableros [".$_POST["orden"]."] -> [".$_POST["orden"]."HME] por el usuario :".strtoupper($_POST["usuario"]);

                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Hoja Multipunto",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK=".$id_retorno;
                } else {
                    $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                }
            
            } elseif ($_POST["tipo_pre"] == "4") {
                if ($_POST["bloque_edita"] == "1") {
                    $tabla = 'multipunto_general';
                    $query = $this->consulta->get_result('id',$_POST['indicador_gral'],$tabla);
                    foreach ($query as $row){                    
                        $datos["plumas"] = $row->plumas;
                        $datos["lucesClaxon"] = $row->lucesClaxon;
                        $datos["grietasParabrisas"] = $row->grietasParabrisas;
                        $datos["bateriaEstado"] = $row->bateriaEstado;
                    }

                    if ($_POST["indicador"] == "1") {
                        $bloque_1["plumas"] = (($_POST["indicador_color"] == "Aprobado") ? 'Si' : 'No');
                        $indicador = "plumas";
                        $ant_ind = $datos["plumas"];
                    } elseif ($_POST["indicador"] == "2") {
                        $bloque_1["lucesClaxon"] = $_POST["indicador_color"];
                        $indicador = "lucesClaxon";
                        $ant_ind = $datos["lucesClaxon"];
                    } elseif ($_POST["indicador"] == "3") {
                        $bloque_1["grietasParabrisas"] = $_POST["indicador_color"];
                        $indicador = "grietasParabrisas";
                        $ant_ind = $datos["grietasParabrisas"];
                    } elseif ($_POST["indicador"] == "4") {
                        $bloque_1["bateriaEstado"] = $_POST["indicador_color"];
                        $indicador = "bateriaEstado";
                        $ant_ind = $datos["bateriaEstado"];
                    }

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row($tabla,$bloque_1,'id',$_POST['indicador_gral']);

                    if ($actualizar) {
                        $movimiento = "Se cambio el indicador [".$indicador."] de la tabla [".$tabla."] con el valor de [".$ant_ind."] a [".$_POST["indicador_color"]."] por el usuario :".strtoupper($_POST["usuario"]);

                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Hoja Multipunto",
                            "respaldo_edicion" => $movimiento,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }

                } elseif ($_POST["bloque_edita"] == "2") {
                    $tabla = 'multipunto_general_2';
                    $query = $this->consulta->get_result('idGral2',$_POST['indicador_gral2'],$tabla);
                    foreach ($query as $row){                    
                        $datos["sistemaCalefaccion"] = $row->sistemaCalefaccion;
                        $datos["sisRefrigeracion"] = $row->sisRefrigeracion;
                        $datos["bandas"] = $row->bandas;
                        $datos["sisFrenos"] = $row->sisFrenos;
                        $datos["suspension"] = $row->suspension;
                        $datos["varillajeDireccion"] = $row->varillajeDireccion;
                        $datos["sisEscape"] = $row->sisEscape;
                        $datos["funEmbrague"] = $row->funEmbrague;
                        $datos["flechaCardan"] = $row->flechaCardan;
                    }

                    if ($_POST["indicador"] == "1") {
                        $bloque_2["sistemaCalefaccion"] = $_POST["indicador_color"];
                        $indicador = "sistemaCalefaccion";
                        $ant_ind = $datos["sistemaCalefaccion"];

                    } elseif ($_POST["indicador"] == "2") {
                        $bloque_2["sisRefrigeracion"] = $_POST["indicador_color"];
                        $indicador = "sisRefrigeracion";
                        $ant_ind = $datos["sisRefrigeracion"];

                    } elseif ($_POST["indicador"] == "3") {
                        $bloque_2["bandas"] = $_POST["indicador_color"];
                        $indicador = "bandas";
                        $ant_ind = $datos["bandas"];

                    } elseif ($_POST["indicador"] == "4") {
                        $bloque_2["sisFrenos"] = $_POST["indicador_color"];
                        $indicador = "sisFrenos";
                        $ant_ind = $datos["sisFrenos"];

                    } elseif ($_POST["indicador"] == "5") {
                        $bloque_2["suspension"] = $_POST["indicador_color"];
                        $indicador = "suspension";
                        $ant_ind = $datos["suspension"];

                    } elseif ($_POST["indicador"] == "6") {
                        $bloque_2["varillajeDireccion"] = $_POST["indicador_color"];
                        $indicador = "varillajeDireccion";
                        $ant_ind = $datos["varillajeDireccion"];

                    } elseif ($_POST["indicador"] == "7") {
                        $bloque_2["sisEscape"] = $_POST["indicador_color"];
                        $indicador = "sisEscape";
                        $ant_ind = $datos["sisEscape"];

                    } elseif ($_POST["indicador"] == "8") {
                        $bloque_2["funEmbrague"] = $_POST["indicador_color"];
                        $indicador = "funEmbrague";
                        $ant_ind = $datos["funEmbrague"];

                    } elseif ($_POST["indicador"] == "9") {
                        $bloque_2["flechaCardan"] = $_POST["indicador_color"];
                        $indicador = "flechaCardan";
                        $ant_ind = $datos["flechaCardan"];

                    }

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row($tabla,$bloque_2,'idGral2',$_POST['indicador_gral2']);

                    if ($actualizar) {
                        $movimiento = "Se cambio el indicador [".$indicador."] de la tabla [".$tabla."] con el valor de [".$ant_ind."] a [".$_POST["indicador_color"]."] por el usuario :".strtoupper($_POST["usuario"]);

                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Hoja Multipunto",
                            "respaldo_edicion" => $movimiento,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }

                } elseif ($_POST["bloque_edita"] == "3") {
                    $tabla = 'frente_izquierdo';
                    $query = $this->consulta->get_result('idFI',$_POST['indicador_fi'],$tabla);
                    foreach ($query as $row){                    
                        
                        $datos["pneumaticoFI"] = $row->pneumaticoFI;
                        $datos["dNeumaticoFI"] = $row->dNeumaticoFI;
                        $datos["presionInfladoFI"] = $row->presionInfladoFI;
                        $datos["espesoBalataFI"] = $row->espesoBalataFI;
                        $datos["espesorDiscoFI"] = $row->espesorDiscoFI;
                    }

                    if ($_POST["indicador"] == "1") {
                        $bloque["pneumaticoFI"] = $_POST["indicador_color"];
                        $indicador = "pneumaticoFI";
                        $ant_ind = $datos["pneumaticoFI"];
                        
                    } elseif ($_POST["indicador"] == "2") {
                        $bloque["dNeumaticoFI"] = $_POST["indicador_color"];
                        $indicador = "dNeumaticoFI";
                        $ant_ind = $datos["dNeumaticoFI"];
                        
                    } elseif ($_POST["indicador"] == "3") {
                        $bloque["presionInfladoFI"] = $_POST["indicador_color"];
                        $indicador = "presionInfladoFI";
                        $ant_ind = $datos["presionInfladoFI"];
                        
                    } elseif ($_POST["indicador"] == "4") {
                        $bloque["espesoBalataFI"] = $_POST["indicador_color"];
                        $indicador = "espesoBalataFI";
                        $ant_ind = $datos["espesoBalataFI"];
                        
                    } elseif ($_POST["indicador"] == "5") {
                        $bloque["espesorDiscoFI"] = $_POST["indicador_color"];
                        $indicador = "espesorDiscoFI";
                        $ant_ind = $datos["espesorDiscoFI"];
                        
                    }

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row($tabla,$bloque,'idFI',$_POST['indicador_fi']);

                    if ($actualizar) {
                        $movimiento = "Se cambio el indicador [".$indicador."] de la tabla [".$tabla."] con el valor de [".$ant_ind."] a [".$_POST["indicador_color"]."] por el usuario :".strtoupper($_POST["usuario"]);

                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Hoja Multipunto",
                            "respaldo_edicion" => $movimiento,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }

                } elseif ($_POST["bloque_edita"] == "4") {
                    $tabla = 'trasero_izquierdo';

                    $query = $this->consulta->get_result('idTI',$_POST['indicador_ti'],$tabla);
                    foreach ($query as $row){                    
                        $datos["pneumaticoTI"] = $row->pneumaticoTI;
                        $datos["dNeumaticoTI"] = $row->dNeumaticoTI;
                        $datos["presionInfladoTI"] = $row->presionInfladoTI;
                        $datos["espesoBalataTI"] = $row->espesoBalataTI;
                        $datos["espesorDiscoTI"] = $row->espesorDiscoTI;
                        $datos["diametroTamborTI"] = $row->diametroTamborTI;
                    }

                    if ($_POST["indicador"] == "1") {
                        $bloque["pneumaticoTI"] = $_POST["indicador_color"];
                        $indicador = "pneumaticoTI";
                        $ant_ind = $datos["pneumaticoTI"];
                        
                    } elseif ($_POST["indicador"] == "2") {
                        $bloque["dNeumaticoTI"] = $_POST["indicador_color"];
                        $indicador = "dNeumaticoTI";
                        $ant_ind = $datos["dNeumaticoTI"];
                        
                    } elseif ($_POST["indicador"] == "3") {
                        $bloque["presionInfladoTI"] = $_POST["indicador_color"];
                        $indicador = "presionInfladoTI";
                        $ant_ind = $datos["presionInfladoTI"];
                        
                    } elseif ($_POST["indicador"] == "4") {
                        $bloque["espesoBalataTI"] = $_POST["indicador_color"];
                        $indicador = "espesoBalataTI";
                        $ant_ind = $datos["espesoBalataTI"];
                        
                    } elseif ($_POST["indicador"] == "5") {
                        $bloque["espesorDiscoTI"] = $_POST["indicador_color"];
                        $indicador = "espesorDiscoTI";
                        $ant_ind = $datos["espesorDiscoTI"];
                        
                    } elseif ($_POST["indicador"] == "6") {
                        $bloque["diametroTamborTI"] = $_POST["indicador_color"];
                        $indicador = "diametroTamborTI";
                        $ant_ind = $datos["diametroTamborTI"];
                        
                    }

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row($tabla,$bloque,'idTI',$_POST['indicador_ti']);

                    if ($actualizar) {
                        $movimiento = "Se cambio el indicador [".$indicador."] de la tabla [".$tabla."] con el valor de [".$ant_ind."] a [".$_POST["indicador_color"]."] por el usuario :".strtoupper($_POST["usuario"]);

                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Hoja Multipunto",
                            "respaldo_edicion" => $movimiento,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }
                
                } elseif ($_POST["bloque_edita"] == "5") {
                    $tabla = 'frente_derecho';

                    $query = $this->consulta->get_result('idFD',$_POST['indicador_fd'],$tabla);
                    foreach ($query as $row){                    
                        $datos["pneumaticoFD"] = $row->pneumaticoFD;
                        $datos["dNeumaticoFD"] = $row->dNeumaticoFD;
                        $datos["presionInfladoFD"] = $row->presionInfladoFD;
                        $datos["espesoBalataFD"] = $row->espesoBalataFD;
                        $datos["espesorDiscoFD"] = $row->espesorDiscoFD;
                    }

                    if ($_POST["indicador"] == "1") {
                        $bloque["pneumaticoFD"] = $_POST["indicador_color"];
                        $indicador = "pneumaticoFD";
                        $ant_ind = $datos["pneumaticoFD"];
                        
                    } elseif ($_POST["indicador"] == "2") {
                        $bloque["dNeumaticoFD"] = $_POST["indicador_color"];
                        $indicador = "dNeumaticoFD";
                        $ant_ind = $datos["dNeumaticoFD"];
                        
                    } elseif ($_POST["indicador"] == "3") {
                        $bloque["presionInfladoFD"] = $_POST["indicador_color"];
                        $indicador = "presionInfladoFD";
                        $ant_ind = $datos["presionInfladoFD"];
                        
                    } elseif ($_POST["indicador"] == "4") {
                        $bloque["espesoBalataFD"] = $_POST["indicador_color"];
                        $indicador = "espesoBalataFD";
                        $ant_ind = $datos["espesoBalataFD"];
                        
                    } elseif ($_POST["indicador"] == "5") {
                        $bloque["espesorDiscoFD"] = $_POST["indicador_color"];
                        $indicador = "espesorDiscoFD";
                        $ant_ind = $datos["espesorDiscoFD"];
                        
                    }

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row($tabla,$bloque,'idFD',$_POST['indicador_fd']);

                    if ($actualizar) {
                        $movimiento = "Se cambio el indicador [".$indicador."] de la tabla [".$tabla."] con el valor de [".$ant_ind."] a [".$_POST["indicador_color"]."] por el usuario :".strtoupper($_POST["usuario"]);

                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Hoja Multipunto",
                            "respaldo_edicion" => $movimiento,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }

                } elseif ($_POST["bloque_edita"] == "6") {
                    $tabla = 'trasero_derecho';

                    $query = $this->consulta->get_result('idTD',$_POST['indicador_td'],$tabla);
                    foreach ($query as $row){                    
                        $datos["pneumaticoTD"] = $row->pneumaticoTD;
                        $datos["dNeumaticoTD"] = $row->dNeumaticoTD;
                        $datos["presionInfladoTD"] = $row->presionInfladoTD;
                        $datos["espesoBalataTD"] = $row->espesoBalataTD;
                        $datos["espesorDiscoTD"] = $row->espesorDiscoTD;
                        $datos["diametroTamborTD"] = $row->diametroTamborTD;
                    }

                    if ($_POST["indicador"] == "1") {
                        $bloque["pneumaticoTD"] = $_POST["indicador_color"];
                        $indicador = "pneumaticoTD";
                        $ant_ind = $datos["pneumaticoTD"];
                        
                    } elseif ($_POST["indicador"] == "2") {
                        $bloque["dNeumaticoTD"] = $_POST["indicador_color"];
                        $indicador = "dNeumaticoTD";
                        $ant_ind = $datos["dNeumaticoTD"];
                        
                    } elseif ($_POST["indicador"] == "3") {
                        $bloque["presionInfladoTD"] = $_POST["indicador_color"];
                        $indicador = "presionInfladoTD";
                        $ant_ind = $datos["presionInfladoTD"];
                        
                    } elseif ($_POST["indicador"] == "4") {
                        $bloque["espesoBalataTD"] = $_POST["indicador_color"];
                        $indicador = "espesoBalataTD";
                        $ant_ind = $datos["espesoBalataTD"];
                        
                    } elseif ($_POST["indicador"] == "5") {
                        $bloque["espesorDiscoTD"] = $_POST["indicador_color"];
                        $indicador = "espesorDiscoTD";
                        $ant_ind = $datos["espesorDiscoTD"];
                        
                    } elseif ($_POST["indicador"] == "6") {
                        $bloque["diametroTamborTD"] = $_POST["indicador_color"];
                        $indicador = "diametroTamborTD";
                        $ant_ind = $datos["diametroTamborTD"];
                        
                    }

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row($tabla,$bloque,'idTD',$_POST['indicador_td']);

                    if ($actualizar) {
                        $movimiento = "Se cambio el indicador [".$indicador."] de la tabla [".$tabla."] con el valor de [".$ant_ind."] a [".$_POST["indicador_color"]."] por el usuario :".strtoupper($_POST["usuario"]);

                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Hoja Multipunto",
                            "respaldo_edicion" => $movimiento,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }
                
                }else{
                    $mensaje = "NO SE INDENTIFICO EL BLOQUE A EDITAR=";
                }
                
            } elseif ($_POST["tipo_pre"] == "5") {
                $cambios = "Se reasigna el número de orden por solicitud del asesor [".$_POST["orden"]."] -> [".$_POST["nv_orden"];

                $limpieza = array(
                    "orden" => $_POST["nv_orden"],
                );

                //Guardamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row('multipunto_general',$limpieza,'id',$_POST['indicador_gral']);

                if ($actualizar) {
                    $movimiento = "Se reasigna el número de orden por que se abrio el formulario con el número de orden incorrecto [".$_POST["orden"]."] -> [".$_POST["nv_orden"]." por el usuario :".strtoupper($_POST["usuario"]);

                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Hoja Multipunto");
                    $this->registrar_movimiento_historial($_POST["nv_orden"],$movimiento,"Soporte - Hoja Multipunto");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Hoja Multipunto",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mov_historico["id_cita"] = $_POST["nv_orden"];
                    $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK=".$id_retorno;
                } else {
                    $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                }
            } else{
                $mensaje = "NO SE IDENTIFICO EL PROCESO A REALIZAR=";
            }
        }
        
        echo $mensaje;
    }

// --------------------------------------------------------------------------------------------
    public function verificacion_diag($id_cita_url = "",$tipo_edicion = "", $datos = NULL)
    {
        if (!$this->session->userdata('idUsuario')) {
            redirect('Login_soporte', 'refresh');
        }else{
            $datos["id_cita"] = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
            $datos["id_cita_url"] = $id_cita_url;
            $datos["tipo_vista"] = $tipo_edicion;

            $d_orden = $this->consulta->datos_cabecera($datos["id_cita"]);

            if (count($d_orden) > 0) {
                $datos["folio_externo"] = $d_orden["folioIntelisis"];
                $datos["unidad"] = $d_orden["subcategoria"];
                $datos["vehiculo"] = $d_orden["vehiculo_modelo"];
                $datos["placas"] = $d_orden["vehiculo_placas"];
                $datos["serie"] = (($d_orden["vehiculo_numero_serie"] != "") ? $d_orden["vehiculo_numero_serie"] : $d_orden["vehiculo_identificacion"]);
                $datos["asesor"] = $d_orden["asesor"];
                $datos["tecnico"] = $d_orden["tecnico"];
                $datos["cliente"] = $d_orden["datos_nombres"]." ".$d_orden["datos_apellido_paterno"]." ".$d_orden["datos_apellido_materno"];

                $datos["orden_abierta"] = ((($d_orden["id_situacion_intelisis"] != 5) && ($d_orden["id_situacion_intelisis"] != 6)) ? 1 : 0);
            }

            $id_contrato = $this->consulta->recuperar_index_contrato($datos["id_cita"]);
            $datos["existe_contrato"] = (($id_contrato != 0) ? 1 : 0);

            $this->loadAllView($datos,'soporte/soporte_diagnostico');
        }
    }

    public function editar_diagnostico()
    {
        $mensaje = "";

        if (!$this->session->userdata('idUsuario')) {
            $mensaje = "LA SESIÓN HA CADUCADO, FAVOR DE VERIFICAR=";
        } else {
            $id_diag = $this->consulta->recuperar_index_diag($_POST["orden"]);
            $id_contrato = $this->consulta->recuperar_index_contrato($_POST["orden"]);

            if ($_POST["tipo_pre"] == "1") {
                if ($id_contrato != 0) {
                    $cambios = "Se reasigna el número de orden por solicitud del asesor [".$_POST["orden"]."] -> [".$_POST["nv_orden"];

                    $limpieza = array(
                        "noServicio" => $_POST["nv_orden"],
                        "fecha_actualiza" => date("Y-m-d H:i:s")
                    );

                    //Guardamos el registro de la refaccion
                    $actualizar = $this->consulta->update_table_row('diagnostico',$limpieza,'idDiagnostico',$id_diag);

                    if ($actualizar) {
                        $limpieza2 = array(
                            "identificador" => $_POST["nv_orden"],
                            "folio" => $_POST["nv_orden"],
                        );

                        //Guardamos el registro de la refaccion
                        $actualizar2 = $this->consulta->update_table_row('contrato',$limpieza2,'idContracto',$id_contrato);

                        $cambios .= " Se reasigna el número de identificador del contrato [".$_POST["orden"]."] -> [".$_POST["nv_orden"];

                        $movimiento = "Se reasigna el número de orden por que se abrio el formulario con el número de orden incorrecto [".$_POST["orden"]."] -> [".$_POST["nv_orden"]." por el usuario :".strtoupper($_POST["usuario"]);

                        $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Diagnostico");
                        $this->registrar_movimiento_historial($_POST["nv_orden"],$movimiento,"Soporte - Diagnostico");

                        $mov_historico = array(
                            "id" => NULL,
                            "id_cita" => $_POST["orden"],
                            "id_login" => $_POST["usuario_log"],
                            "usuario_login" => $_POST["n_usuario_log"],
                            "usuario_edita" => $_POST["usuario"],
                            "motivo_edicion" => $_POST["motivo"],
                            "formulario" => "Diagnostico",
                            "respaldo_edicion" => $cambios,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                        );

                        $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mov_historico["id_cita"] = $_POST["nv_orden"];
                        $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                        $mensaje = "OK=".$id_retorno;
                    } else {
                        $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                    }
                } else {
                    $mensaje = "NO SE ENCONTRÓ UN CONTRATO PARA REASIGNAR, EL PROCESO SE ABORTO, VERIFIQUE LA EXISTENCIA DEL CONTRATO O RECUPERARLO EN LA OPCIÓN  'Recuperar contrato y/o firmas' PARA PODER CONTINUAR=";
                }
                
            } elseif ($_POST["tipo_pre"] == "2") {
                $proceso = FALSE;
                //Recuperamos las firmas del diagnostico 
                $datos_base = $this->consulta->recuperar_firmas_orden($_POST["orden"]);
                //Verificamos si existe el contrato, para editar o se tiene que crear un nuevo contrato
                if ($id_contrato != 0) {
                    $cambios = "Se actualizan los datos de nombre y firma del cliente y del asesor en el contrato";
                    $dataContract = array(
                        "identificador"=> trim($_POST["orden"]),
                        "folio" => $_POST["orden"],
                        "firmaCliente" => $datos_base["firmaConsumidor2"],
                        "nombreDistribuidor " => $datos_base["nombreAsesor"],
                        "firmaDistribuidor" => $datos_base["firmaAsesor"],
                        "nombreConsumidor" => $datos_base["nombreConsumidor2"],
                        "firmaConsumidor" => $datos_base["firmaConsumidor2"],
                    );

                    $actualizar = $this->consulta->update_table_row('contrato',$dataContract,'idContracto',$id_contrato);

                    if ($actualizar) {
                        $proceso = TRUE;
                        $movimiento = "Se edita las firmas del contrato ya que se guardo el inventario a medias y se omitio la parte del contrato; por el usuario :".strtoupper($_POST["usuario"]);
                    }
                } else {
                    $cambios = "Se crea todo el contrato completo en base al inventario";

                    $datos_contrato = $this->consulta->recuperar_exp_contrato();
                    $fecha = explode(" ", $datos_base["fechaRegistro"]);

                    $dataContract = array(
                        "idContracto" => NULL, 
                        "identificador"=> trim($_POST["orden"]),
                        "folio" => $_POST["orden"],
                        "fecha" => $fecha[0],
                        "hora" => $fecha[1],
                        "empresa" => SUCURSAL,
                        "pagoConsumidor" => 'Por definir',
                        "condicionalInfo" => '0', 
                        "condicionalPublicidad" => '0',
                        "firmaCliente" => $datos_base["firmaConsumidor2"],
                        "nombreDistribuidor " => $datos_base["nombreAsesor"],
                        "firmaDistribuidor" => $datos_base["firmaAsesor"],
                        "nombreConsumidor" => $datos_base["nombreConsumidor2"],
                        "firmaConsumidor" => $datos_base["firmaConsumidor2"],
                        "aceptaTerminos" => '1', 
                        "noRegistro" => $datos_contrato["noRegistro"],
                        "noExpediente" => $datos_contrato["noExpediente"],
                        "fechDia" => $datos_contrato["fechDia"],
                        "fechMes" => $datos_contrato["fechMes"],
                        "fechAnio" => $datos_contrato["fechAnio"],
                        "terminos_contrato" => '1',
                        "fechaRegistro" => date("Y-m-d H:i:s"),
                    );

                    $contrato = $this->mConsultas->save_register('contrato',$dataContract);
                    if ($contrato != 0) {
                        $proceso = TRUE;
                        $movimiento = "Se crea el contrato ya que se guardo el inventario a medias y se omitio la parte del contrato; por el usuario :".strtoupper($_POST["usuario"]);
                    }
                }
                
                if ($proceso) {
                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Diagnostico");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Diagnostico",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK=".$id_retorno;
                } else{
                    $mensaje = "OCURRIO UN ERROR AL QUERER ACTUALIZAR EL CONTRATO=";
                }

            } elseif ($_POST["tipo_pre"] == "3") {
                $cambios = "Se reasigna la orden en estaus de ignativo para que se llene nuevamente desde cero [".$_POST["orden"]."] -> [DGE".$_POST["orden"]."]";

                $limpieza = array(
                    "noServicio" => "DGE".$_POST["orden"],
                    "fecha_actualiza" => date("Y-m-d H:i:s")
                );

                //Guardamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row('diagnostico',$limpieza,'idDiagnostico',$id_diag);

                if ($actualizar) {
                    $limpieza2 = array(
                        "identificador" => "DGE".$_POST["orden"],
                        "folio" => "DGE".$_POST["orden"],
                    );

                    //Guardamos el registro de la refaccion
                    $actualizar2 = $this->consulta->update_table_row('contrato',$limpieza2,'idContracto',$id_contrato);

                    $movimiento = "Se reasigna la orden porque se dublico y esta generando problemas con los tableros [".$_POST["orden"]."] -> [DGE".$_POST["orden"]."] por el usuario :".strtoupper($_POST["usuario"]);

                    $this->registrar_movimiento_historial($_POST["orden"],$movimiento,"Soporte - Diagnostico");

                    $mov_historico = array(
                        "id" => NULL,
                        "id_cita" => $_POST["orden"],
                        "id_login" => $_POST["usuario_log"],
                        "usuario_login" => $_POST["n_usuario_log"],
                        "usuario_edita" => $_POST["usuario"],
                        "motivo_edicion" => $_POST["motivo"],
                        "formulario" => "Diagnostico",
                        "respaldo_edicion" => $cambios,
                        "fecha_registro" => date("Y-m-d H:i:s"),
                    );

                    $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

                    $mensaje = "OK=".$id_retorno;
                } else {
                    $mensaje = "OCURRIO UN ERROR DURANTE EL PROCESO=";
                }
            } else{
                $mensaje = "NO SE IDENTIFICO EL PROCESO A REALIZAR=";
            }
        }
        
        echo $mensaje;
    }

// --------------------------------------------------------------------------------------------
    public function reenvio_orden()
    {
        try {
            $id_cita = $_POST["id_cita"];

            $data = array('idOrden' => $id_cita );
            $url = base_url()."api/Intelisis/loadDataOrden";

            $ch = curl_init($url);
            //Creamos el objeto json para enviar por url los datos del formulario
            $jsonDataEncoded = json_encode($data);
            //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Adjuntamos el json a nuestra petición
            curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
            //Agregamos los encabezados del contenido
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            //Obtenemos la respuesta
            $response = curl_exec($ch);
            // Se cierra el recurso CURL y se liberan los recursos del sistema
            curl_close($ch);
            
            $mov_historico = array(
                "id" => NULL,
                "id_cita" => $_POST["id_cita"],
                "id_login" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : ''),
                "usuario_login" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('usuario') : ''),
                "usuario_edita" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('nombreUsuario') : ''),
                "motivo_edicion" => "Se reenvia la orden al DMS / Intelisis",
                "formulario" => "Documentación",
                "respaldo_edicion" => $response,
                "fecha_registro" => date("Y-m-d H:i:s"),
            );

            $id_retorno = $this->consulta->save_register('registro_historial_soporte',$mov_historico);

            $mensaje =  "OK";
            
        } catch (Exception $e) {
            $mensaje = "OCURRIO UN PROBLEMA DURANTE EL PROCESO";
        }

        echo $mensaje;
    }

// --------------------------------------------------------------------------------------------
    public function registrar_movimiento_historial($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel Principal";
            }elseif ($this->session->userdata('rolIniciado') == "SOPORTE") {
                $panel = "Panel de Soporte";
            }else {
                $panel = "Soporte";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Soporte";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Soporte";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->consulta->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    public function registrar_movimiento_presupuesto($id_cita='',$movimiento = '',$origen = NULL)
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel Principal";
            }elseif ($this->session->userdata('rolIniciado') == "SOPORTE") {
                $panel = "Panel de Soporte";
            }else {
                $panel = "Soporte";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Soporte";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Soporte";
            $id_usuario =  "";
            $usuario = "";
        }

        $historico = array(
            'id' => NULL,
            'id_cita' => $id_cita,
            'origen_movimiento' => $panel,
            'movimiento' => $movimiento,
            'id_usuario' => $id_usuario,
            'usuario' => $usuario,
            'fecha_registro' => date("Y-m-d H:i:s"),
        );

        $this->consulta->save_register('presupuestos_historial',$historico);

        return TRUE;
    }

    public function cargar_evidencias()
    {
        $respuesta = "";
        try {
            $_FILES['tempFile']['name'] = $_FILES['archivo']['name'];
            $_FILES['tempFile']['type'] = $_FILES['archivo']['type'];
            $_FILES['tempFile']['tmp_name'] = $_FILES['archivo']['tmp_name'];
            $_FILES['tempFile']['error'] = $_FILES['archivo']['error'];
            $_FILES['tempFile']['size'] = $_FILES['archivo']['size'];

            //Url donde se guardara la imagen
            $urlDoc ="assets/evidencia_soporte";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."-Soporte-";
            for($j=0; $j<=7; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                $data['uploadError'] = $this->upload->display_errors();
                // echo $this->upload->display_errors();
                $path = "";
            }else{
                //Si se pudo cargar la imagen la guardamos
                $fileData = $this->upload->data();
                $ext = explode(".",$_FILES['tempFile']['name']);
                $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
            }

            //Confirmamos si se guardo la evidencia y creamos el registro
            if ($path != "") {
                $evidencia = array(
                    "id" => NULL,
                    "id_cita" => $_POST["orden"],
                    "ref_historico" => $_POST["liga_int"],
                    "id_login" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('idUsuario') : ''),
                    "usuario_edita" => (($this->session->userdata('idUsuario')) ? $this->session->userdata('nombreUsuario') : ''),
                    "formulario" => $_POST["soporte"],
                    "url" => $path,
                    "fecha_registro" => date("Y-m-d H:i:s"),
                );

                $index = $this->consulta->save_register('registro_evidencia_soporte',$evidencia);
                if ($_POST["limite"] == 0) {
                    $respuesta = "FIN";
                } else {
                    $respuesta = "OK";
                }
                
            }else{
                $respuesta = "NO SE PUDO SUBIR LA EVIDENCIA, REINTENTAR";
            }
        } catch (Exception $e) {
            $respuesta = "OCURRIO UN ERRO AL SUBIR LA EVIDENCIA";
        }

        echo $respuesta;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array( "include" => NULL);
        $archivosJs["include"][] = 'assets/js/soporte/consultas.js';

        if ($vista == "soporte/soporte_presupuesto") {
            $archivosJs["include"][] = 'assets/js/soporte/soporte_presupuesto.js';
            $titulo = "Soporte Presupuesto";
        } elseif (($vista == "soporte/soporte_hmultipunto")||($vista == "soporte/soporte_hmultipunto_2")) {
            $archivosJs["include"][] = 'assets/js/soporte/soporte_multipunto.js';
            $titulo = "Soporte Multipunto";
        } elseif ($vista == "soporte/soporte_diagnostico") {
            $archivosJs["include"][] = 'assets/js/soporte/soporte_diagnostico.js';
            $titulo = "Soporte Diagnostico";
        } elseif ($vista == "soporte/historico_soporte") {
            //$archivosJs["include"][] = 'assets/js/soporte/soporte_diagnostico.js';
            $titulo = "Soporte Historial";
        } else{
            $titulo = "Panel Soporte";
        }

        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => $titulo
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}