<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Cotizaciones extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($idOrden = 0,$datos = NULL) 
    {
        //Precargamos el catalogo de gpos de las refacciones
        $precarga = $this->mConsultas->filtro_princ('gpo','refacciones','gpo');
        foreach ($precarga as $row) {
            $datos["grupo"][] = $row->gpo;
        }

        if ($this->session->userdata('rolIniciado')) {
            $datos["dirige"] = $this->session->userdata('rolIniciado');
        }else {
            $datos["dirige"] = "";
        }

        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        //Identificamos si la orden se servicio ya ha generado alguna cotizacion
        $revision = $this->mConsultas->get_result("idOrden",$id_cita,"cotizacion_multipunto");
        foreach ($revision as $row) {
            if ($row->identificador == "M") {
                $idCotizacion = $row->idCotizacion;
                $acepta = $row->aceptoTermino;
            }
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idCotizacion)) {
            //Consultamos si ya se entrego la unidad
            $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($id_cita);

            //Verificamos si la cotización ya fue aceptada/rechazada por el cliente
            if ($acepta != "") {
                //Si no existe un destino de vista , se asume que es la vista del cliente
                if (!isset($datos["destinoVista"])) {
                    $datos["destinoVista"] = "Cliente";
                }
                $this->setData($id_cita,$datos);
            }else {
                $this->setData($id_cita,$datos);
            }
        } else {
            $datos['idOrden'] = $id_cita;
            $this->loadAllView($datos,'cotizaciones/cotizacion_alta');
        }
    }

    //Parada comodin para listar cotizaciones que ya estan procesadas y se van a volver a procesar
    public function comodinListaTec($origen = "",$datos = NULL)
    {
        $datos["indicador"] = "ACT";
        $datos["destino"] = $origen;

        $this->loadDataSearch("",$datos);
    }

    //Parada comodin para visualizar la cotizacion y que se siga agregando los parametros
    public function comodinEditaProcesado($idOrden = 0,$datos = NULL)
    {
        $datos["destinoVista"] = "INFINITO";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin cuando la cotizacion lleva piezas de garantias
    public function comodinEditaGarantias($idOrden = '',$datos = NULL)
    {
        $datos["tipoRegistro"] = "Garantias";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin cuando la cotizacion que va a ser revisada por el jefe de taller
    public function comodinEditaJDT($idOrden = '',$datos = NULL)
    {
        $datos["tipoRegistro"] = "JDT";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin para listar cotizaciones cuando la cotizacion sale de refaccionesy para con el jefe de taller
    public function comodinListaJDT($mensaje = "",$datos = NULL)
    {
        $datos["indicador"] = "JDT";
        $datos["mensaje"] = $mensaje;

        $this->loadListCoat("",$datos);
    }

    //Comodin para ventana emergentes
    public function comodinRedireccion($valores = "")
    {
        $tipos = explode("_",$valores);
        
        if (count($tipos)>1) {
            $datos["tipoRegistro"] = $tipos[1];
            $idOrden = (($tipos[0] == "0") ? "0" : $this->decrypt($tipos[0]));
        }else {
            $datos["tipoRegistro"] = "Asesor";
            $idOrden = (($valores == "0") ? "0" : $this->decrypt($valores));
        }

        //Verificamos si es un nuevo presupuesto
        //Consultamos su ya se ha generado alguna cotizacion para ese presupuesto
        $revision = $this->mConsultas->get_result_field("id_cita",$idOrden,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $id_presupuesto = $row->id;
        }

        //Si existe una cotizacion generada
        if (isset($id_presupuesto)) {
            //Redireccionamos a los nuevos presupuestos
            //redirect(base_url().'Presupuesto_Asesor/'.$idOrden);
            redirect(base_url().'Alta_Presupuesto/'.$this->encrypt($idOrden));
        //Verificamos si existe en los presupuestos anteriores
        }else{
            //Verificamos que el numero de orden tenga un presupuesto viejito
            $revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
            foreach ($revision as $row) {
                if ($row->identificador == "M") {
                    $idCotizacion = $row->idCotizacion;
                }
            }
            //Si no existe en los presupuestos viejos
            if (isset($idCotizacion)) {
                $this->index($idOrden,$datos);
            }else{
                //Redireccionamos a los nuevos presupuestos
                redirect(base_url().'Alta_Presupuesto/'.$this->encrypt($idOrden));
            }
        }
    }

    //Entrada para el ventanilla (definir estado de las refacciones)
    public function comodinRefStatus($idOrden = 0)
    {
        $datos["destinoVista"] = "RefStatus";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Redireccion para edicion de superUsuario
    public function comodinEditaSU($idOrden = '')
    {
        $datos["tipoRegistro"] = "Otros";
        $this->index($idOrden,$datos);
    }

    //Redireccionamos para edicion de status del cliente
    public function comodinEditaStatus($idOrden = '')
    {
        $datos["tipoRegistro"] = "StatusCambio";
        $datos["destinoVista"] = "StatusCambio";
        $this->index($idOrden,$datos);
    }

    //Validamos el formulario
    public function validateForm()
    {
        $datos["vista"] = $this->input->post("modoVistaFormulario");
        $datos["idOrden"] = $this->input->post("idOrdenTemp"); 
        $datos["tipoRegistro"] = $this->input->post("tipoRegistro");
        $datos["modoGuardado"] = $this->input->post("modoGuardado");
        $datos["dirige"] = $this->input->post("dirige");
        $datos["destinoMensaje"] = $this->input->post("aceptarCotizacion");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idOrden"]);

        if ($datos["vista"] == "1") {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required|callback_existencia');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }elseif ($datos["vista"] == "3A") {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Firma del encargado de garantias
            $this->form_validation->set_rules('rutaFirmaGarantia', 'Falta firma del encargado de Garantías.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');
        }elseif ($datos["vista"] == "5A") { 
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Firma del encargado de garantias
            $this->form_validation->set_rules('rutaFirmaJDT', 'Falta firma del encargado de Garantías.', 'required');
            //$this->form_validation->set_rules('refAutorizadas', 'Falta no se capturo corectamente los campos de validación.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');
        }
        else {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('registros','No se ha guardado ningun elemento');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar el No. de Orden.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $datos["mensaje"] = "1";
            //Si la vista es de decision
            if ($datos["vista"] == "5") {
                $this->getDataWish($datos);
            }else {
                $this->getDataForm($datos);
            }

        }else{ 
            $datos["mensaje"]="0";
            if ($datos["vista"] == "1") {
                $this->index($datos["idOrden"],$datos);
            }else {
                if (($datos["vista"] == "3")||($datos["vista"] == "5")) {
                    $this->comodinCliente($datos["idOrden"],$datos);
                }elseif ($datos["vista"] == "4") {
                    $this->comodinEditaSU($datos["idOrden"],$datos);
                }elseif ($datos["vista"] == "3A") {
                    $this->comodinEditaGarantias($datos["idOrden"],$datos);
                }elseif ($datos["vista"] == "5A") {
                    $this->comodinEditaJDT($datos["idOrden"],$datos);
                } else {
                    $this->setData($datos["idOrden"],$datos);
                    //$this->index($datos["idOrden"],$datos);
                }
            }

        }
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idOrdenTemp");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == "M") {
                $dato = $row->idCotizacion;
            }
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Validar requisitos minimos P1
    public function registros()
    {
        $return = FALSE;
        if ($this->input->post("registros")) {
            if (count($this->input->post("registros")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Recuperamos la información del formulario
    public function getDataWish($datos = NULL)
    {
        $registro = date("Y-m-d H:i");
        $dataCoti['contenido'] = $this->mergeOfString($this->input->post("registros"),"");
        $dataCoti['subTotal'] = $this->input->post("subTotalMaterial");
        $dataCoti['cancelado'] = $this->input->post("canceladoMaterial");
        $dataCoti['iva'] = $this->input->post("ivaMaterial");
        $dataCoti['total'] = $this->input->post("totalMaterial");

        $dataCoti['aceptoTermino'] = "Val";
        $dataCoti['fech_actualiza'] = $registro;

        $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$datos["idOrden"],'identificador','M');
        if ($actualizar) {
            //Guardamos el movimiento
            $this->loadHistory($datos["idOrden"],"Actualización Cotización: ".$dataCoti['aceptoTermino'],"Cotización");
            $this->smsData($datos["idOrden"],"Afectada");
            $datos["mensaje"]="1";
        }else {
            $datos["mensaje"]="0";
        }

        $this->comodinCliente($datos["idOrden"],$datos);
    }

    //Recuperamos la información del formulario
    public function getDataForm($datos = NULL)
    {
        $registro = date("Y-m-d H:i");
        //Creamos el array para guardado
        if ($datos["vista"] == "1") {
            $dataCoti = array(
                'idCotizacion' => NULL,
                'idOrden' => $this->input->post("idOrdenTemp"),
                'identificador' => "M",
            );
        }

        $dataCoti['contenido'] = $this->mergeOfString($this->input->post("registros"),"");        
        
        //Verificamos que haya piezas con garantias para almacenar en un segunda tabla
        if (($datos["vista"] == "1")||($datos["vista"] == "8")) {
            $pzasCGarantias = $this->validateCheck($this->input->post("pzaGarantia"),"");
            $dataCoti['tablaGarantias'] = $this->mergeOfStringTG($this->input->post("registros"),"",$pzasCGarantias);
        }elseif (($datos["vista"] == "4")||($datos["vista"] == "3A")) {
            if ($this->input->post("tablaPzsGarantias") != "") {
                $dataCoti['tablaGarantias'] = $this->mergeOfString($this->input->post("tablaPzsGarantias"),"");
            } 
        }

        if ($datos["vista"] == "3A") {
            $dataCoti['clientePaga'] = $this->input->post("clientePaga");
            $dataCoti['fordPaga'] = $this->input->post("fordPaga");
            $dataCoti['totalReparacion'] = $this->input->post("totalReparacion");
        }

        //pza con garantias
        if (($datos["vista"] == "1")||($datos["vista"] == "8")) {
            $dataCoti['pzaGarantia'] = $pzasCGarantias;
        } 

        $dataCoti['subTotal'] = $this->input->post("subTotalMaterial");
        $dataCoti['iva'] = $this->input->post("ivaMaterial");
        $dataCoti['total'] = $this->input->post("totalMaterial");

        if ($datos["vista"] == "1") {
            if ($datos["destinoMensaje"] == "Enviar Cotización al Asesor") {
                $dataCoti['aprobacionRefaccion'] = 2;
            }else {
                $dataCoti['aprobacionRefaccion'] = 0;
            }
        }

        $dataCoti['firmaAsesor'] = $this->base64ToImage($this->input->post("rutaFirmaAsesorCostos"),"firmas");
        // $dataCoti['archivos'] = $this->validateDoc("uploadfiles");

        if($this->input->post("tempFile") != "") {
            $archivos_2 = $this->validateDoc("uploadfiles");
            $dataCoti["archivos"] = $this->input->post("tempFile")."".$archivos_2;
        }else {
            $dataCoti["archivos"] = $this->validateDoc("uploadfiles");
        }

        if ($datos["vista"] == "1") {
            $dataCoti['aceptoTermino'] = "";
        }

        if ($this->input->post("notaAsesor")) {
            $dataCoti['notaAsesor'] = $this->input->post("notaAsesor");
        }

        if ($this->input->post("comentariosExtra")) {
            $dataCoti['comentario'] = $this->input->post("comentariosExtra");
        }

        if ($this->input->post("comentarioTecnico")) {
            $dataCoti['comentarioTecnico'] = $this->input->post("comentarioTecnico");
        }

        if($this->input->post("tempFileTecnico") != "") {
            $archivos_3 = $this->validateDoc("uploadfilesTec");
            $dataCoti["archivoTecnico"] = $this->input->post("tempFileTecnico")."".$archivos_3;
        }else {
            $dataCoti["archivoTecnico"] = $this->validateDoc("uploadfilesTec");
        }

        if ($datos["vista"] == "1") {
            $dataCoti['fech_registro'] = $registro;
            $dataCoti['termino_refacciones'] = $registro;
        }

        $dataCoti['fech_actualiza'] = $registro;

        $query = $this->mConsultas->notificacionTecnicoCita($datos["idOrden"]);  
        foreach ($query as $row) {
            $tecnico = $row->tecnico;
            $modelo = $row->vehiculo_modelo;
            $placas = $row->vehiculo_placas;
            $idIntelisis = $row->folioIntelisis;
            $asesor = $row->asesor;
        }

        //Identificamos si se creara el registro o se actualizara
        if ($datos["vista"] == "1") {
            $idReturn = $this->mConsultas->save_register('cotizacion_multipunto',$dataCoti);
            if ($idReturn != 0) {

                //Definimos a quien le llegar el mensaje de la cotizacion (Directo al asesor)
                if ($datos["destinoMensaje"] == "Enviar Cotización al Asesor") {

                    //Brincamos el proceso de la ventanilla
                    $registro_2 = date("d-m-Y");
                    $dataCoti_2['aprobacionRefaccion'] = "2";
                    $dataCoti_2['termino_refacciones'] = $registro;

                    $actualizar_2 = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_2,'idOrden',$dataCoti["idOrden"],'identificador','M');
                    
                    //Si la cotización va directa al asesor, se desvia hacia los jefe de taller
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idOrden"],"Registro (Envio a jefe de taller sin pasar por ventanilla)","Cotización Multipunto");

                    //Grabamos el evento para el panel de gerencia
                    $temp = array(
                        'idUsuario' => '0',
                        'nombre' => '',
                        'dpo' => 'Técnicos',
                        'dpoRecibe' => 'Jefe de Taller',
                        'descripcion' => 'Cotización Multipunto: Se crea cotización y se envia directo al JDT.',
                    );

                    $this->registroEvento($datos["idOrden"],$temp);

                    //$this->envioSMSGarantias($datos["idOrden"]);
                    $mensaje_sms = SUCURSAL.". Nuevo presupuesto (Multipunto) con refacciones por revisar. OR ".$datos["idOrden"]." el día ".$registro_2." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;

                    //Otro (Angel)
                    $this->envio_sms('5535527547',$mensaje_sms);

                    //Jefe de taller
                    //Jesus Gonzales 
                    //$this->envio_sms('3316724093',$mensaje_sms);
                    //Juan Chavez
                    $this->envio_sms('3333017952',$mensaje_sms);
                    //Gerente de servicio
                    //$this->envio_sms('3315273100',$mensaje_sms);

                    $datos["mensaje"] = "1";
                    $this->setData($datos["idOrden"],$datos);
                    
                }else {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idOrden"],"Registro (Envio a refacciones/ventanilla)","Cotización Multipunto");

                    //Grabamos el evento para el panel de gerencia
                    $temp = array(
                        'idUsuario' => '0',
                        'nombre' => 'Ventanilla',
                        'dpo' => 'Ventanilla',
                        'dpoRecibe' => 'Jefe de Taller',
                        'descripcion' => 'Cotización Multipunto: Termina revisión ventanilla, se envía al JDT.',
                    );

                    $this->registroEvento($datos["idOrden"],$temp);

                    //Enviamos el mensaje al area de refacciones
                    $this->sendSMSMult($datos["idOrden"]);
                }

                //$datos["mensaje"] = "1";
                //$this->setData($datos["idOrden"],$datos);
            }else {
                $datos["mensaje"] = "2";

                $this->index($datos["idOrden"],$datos);
            }
        }else {
            $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$datos["idOrden"],'identificador','M');

            if ($actualizar) {
                //Verificamos si se acaba de firmar
                $data = explode(',', $this->input->post("rutaFirmaAsesorCostos"));
                //Comprobamos que se corte bien la cadena
                if ($data[0] == "data:image/png;base64") {
                    $dataCoti_B['termino_asesor'] = $registro;
                    $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_B,'idOrden',$datos["idOrden"],'identificador','M');
                }

                $datos["mensaje"]="1";
            }else {
                $datos["mensaje"]="0";
            }

            //Identificamos desde donde viene la actualización
            //Si viene desde una actualizacion normal
            if ($datos["vista"] == "3") {
                //Guardamos el movimiento
                $this->loadHistory($datos["idOrden"],"Actualización","Cotización Multipunto");

                $this->comodinCliente($datos["idOrden"],$datos);

            //Si la actualizacion viene de garantias
            }elseif ($datos["vista"] == "3A") {
                //Guardamos el movimiento
                $this->loadHistory($datos["idOrden"],"Actualización (Termina revisión garantías)","Cotización Multipunto");

                //Guardamos el momento en que se actualizo la cotizacion (termino de refacciones)
                //$dataCoti_3['envioGarantia'] = "1";
                $dataCoti_3['firmaGarantias'] = $this->base64ToImage($this->input->post("rutaFirmaGarantia"),"firmas");
                $dataCoti_3['comentariosGarantias'] = $this->input->post("notaGarantias");
                $dataCoti_3['pzaAutorizada'] = $this->validateCheck($this->input->post("autorizaPzaGarantia"),"");

                $dataCoti_3['termino_garantia'] = $registro;

                $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_3,'idOrden',$datos["idOrden"],'identificador','M');

                $datos["pzaAutorizada"] = strlen($dataCoti_3['pzaAutorizada']);

                //Se envia el mensaje al asesor
                $this->envioSMS($datos["idOrden"],$datos);

            //Si la actualizacion sale del jefe de taller
            }elseif ($datos["vista"] == "5A") {
                //Actualizamos los valores en la base de datos
                //Guardamos el momento en que se actualizo la cotizacion (termino de jefe de taller)                
                $dataCoti_3['comentario'] = $this->input->post("comentariosExtra");
                $dataCoti_3['firmaJDT'] = $this->base64ToImage($this->input->post("rutaFirmaJDT"),"firmas");
                $dataCoti_3['pzaAprobadas'] = $this->cargacheck("autorizaJDT_",$this->input->post("indiceTablaMateria"));  //$this->input->post("refAutorizadas");
                $dataCoti_3['termino_JDT'] = date("Y-m-d H:i:s");
                $dataCoti_3['enviaJDT'] = "1";

                $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_3,'idOrden',$datos["idOrden"],'identificador','M');

                //Identificamos el diguiente destinatario de la cotizacion (Si no tiene piezas en garantias se envia al asesor)
                //Si no hay piezas marcadas con garantias, se envia el mensaje al asesor
                if ($this->input->post("pzaGarantia") == "") {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idOrden"],"Actualización (Envio al asesor sin pasar por ventanilla y revisado por el jefe de taller)","Cotización Multipunto");

                    //Grabamos el evento para el panel de gerencia
                    $temp = array(
                        'idUsuario' => '0',
                        'nombre' => '',
                        'dpo' => 'Jefe de Taller',
                        'dpoRecibe' => 'Asesores',
                        'descripcion' => 'Cotización Multipunto: Termina revisión JDT, se envía al Asesor.',
                    );

                    $this->registroEvento($datos["idOrden"],$temp);

                    //Enviamos el mensaje al asesor correspondiente
                    $mensaje_sms = SUCURSAL.". Presupuesto Multipunto revisado por el Jefe de Taller con el  OR#".$datos["idOrden"]." el día ".$registro." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;

                    //Mensaje al asesor
                    $celular_sms = $this->mConsultas->telefono_asesor($datos["idOrden"]);
                    $valores = $this->envio_sms($celular_sms,$mensaje_sms);

                    //Otro (Angel)
                    $this->envio_sms('5535527547',$mensaje_sms);

                    //Regresamos a la vista
                    $datos["mensaje"] = "1";

                //De lo contrario, se envia el mansaje a los de garantías
                } else {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idOrden"],"Actualización (Envio a garantías sin pasar por ventanilla y revisado por el jefe de taller)","Cotización Multipunto");

                    //Grabamos el evento para el panel de gerencia
                    $temp = array(
                        'idUsuario' => '0',
                        'nombre' => '',
                        'dpo' => 'Jefe de Taller',
                        'dpoRecibe' => 'Garantías',
                        'descripcion' => 'Cotización Multipunto: Termina revisión JDT, se envía a Garantías.',
                    );

                    $this->registroEvento($datos["idOrden"],$temp);

                    //$this->envioSMSGarantias($datos["idOrden"]);
                    $mensaje_sms = SUCURSAL.". Presupuesto Multipunto revisado por el Jefe de Taller con refacciones por revisar con garantía.  OR #".$datos["idOrden"]." el día ".$registro." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;

                    //Garantias
                    //
                    $this->envio_sms('3316725075',$mensaje_sms);
                    
                    //Otro (Angel)
                    $this->envio_sms('5535527547',$mensaje_sms);
 
                    //Gerente de servicio
                    //$this->envio_sms('3315273100',$mensaje_sms);

                    $datos["mensaje"] = "1";
                    
                }
                //$this->setData($datos["idOrden"],$datos);
                redirect(base_url().'Listar_JefeTaller/'.$datos["mensaje"]);

            //Si la actualizacion es desde la vista de refacciones
            }elseif ($datos["vista"] == "4") {
                if ($datos["modoGuardado"] == "Terminar") {

                    //Guardamos el movimiento
                    $this->loadHistory($datos["idOrden"],"Actualización (Termina trabajo refacciones/ventanilla envia al jefe de taller)","Cotización Multipunto");

                    //Grabamos el evento para el panel de gerencia
                    $temp = array(
                        'idUsuario' => '0',
                        'nombre' => 'Ventanilla',
                        'dpo' => 'Ventanilla',
                        'dpoRecibe' => 'Jefe de Taller',
                        'descripcion' => 'Cotización Multipunto: Termina revisión de Ventanilla, se envía al JDT.',
                    );

                    $this->registroEvento($datos["idOrden"],$temp);
                    
                    //Guardamos el momento en que se actualizo la cotizacion (termino de refacciones)
                    $dataCoti_2['termino_refacciones'] = $registro;
                    $dataCoti_2['aprobacionRefaccion'] = "1";

                    $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_2,'idOrden',$datos["idOrden"],'identificador','M');

                    $query = $this->mConsultas->notificacionTecnicoCita($datos["idOrden"]);  
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $placas = $row->vehiculo_placas;
                        $idIntelisis = $row->folioIntelisis;
                        $asesor = $row->asesor;
                    }

                    //Notificamos al jefe de taller que la cotizacion esta por revisar
                    $mensaje_sms = SUCURSAL.". Presupuesto Multipunto terminado por ventanilla.  No. Orden #".$datos["idOrden"]." el día ".$registro." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;

                    //Jefe de taller
                    //Jesus Gonzales 
                    //$this->envio_sms('3316724093',$mensaje_sms);
                    //Juan Chavez
                    $this->envio_sms('3333017952',$mensaje_sms);
                    //Gerente de servicio
                    //$this->envio_sms('3315273100',$mensaje_sms);

                    //Otro (Angel)
                    $this->envio_sms('5535527547',$mensaje_sms);
                    
                    $temp["mensaje"]="1";
                    $this->comodinLista("OT",$temp);
                    //$this->envioSMS($datos["idOrden"],$datos);
                    
                    // //Verificamos si se enviara mensaje al asesor o a los de garantias
                    // //Si no hay piezas marcadas con garantias, se envia el mensaje al asesor
                    // if ($this->input->post("pzaGarantia") == "") {
                    //     //Guardamos el movimiento
                    //     $this->loadHistory($datos["idOrden"],"Actualización (Termina trabajo refacciones/ventanilla envia a garantías)","Cotización Multipunto");

                    //     $this->envioSMS($datos["idOrden"],$datos);

                    // //De lo contrario, se envia el mansaje a los de garantías
                    // } else {
                    //     //Guardamos el movimiento
                    //     $this->loadHistory($datos["idOrden"],"Actualización (Termina trabajo refacciones/ventanilla envia al asesor)","Cotización Multipunto");

                    //     //$this->envioSMSGarantias($datos["idOrden"]);

                    //     $mensaje_sms = SUCURSAL.". Nuevo presupuesto (Multipunto) terminado por ventanilla con refacciones por revisar con garantía.  No. Orden #".$datos["idOrden"]." el día ".$registro;

                    //     //Garantias
                    //     //
                    //     $this->envio_sms('3316725075',$mensaje_sms);

                    //     //Jefe de taller
                    //     //Jesus Gonzales 
                    //     $this->envio_sms('3316724093',$mensaje_sms);
                    //     //Juan Chavez
                    //     $this->envio_sms('3333017952',$mensaje_sms);
                    //     //Gerente de servicio
                    //     $this->envio_sms('3315273100',$mensaje_sms);

                    //     //Otro (Angel)
                    //     $this->envio_sms('5535527547',$mensaje_sms);

                    //     $datos["mensaje"] = "1";
                    //     redirect(base_url().'Lista_Cotizaciones_Garantias/4');
                        
                    // }
                    // 
                }else {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idOrden"],"Actualización de refacciones/ventanilla","Cotización Multipunto");

                    $temp["mensaje"]="1";
                    $this->comodinLista("OT",$temp);
                }

            //Si viene desde la actualizacion sin parametros (del tecnico)
            }elseif ($datos["vista"] == "8") {
                //Grabamos la actualizacion y hacemos las modificaciones
                $this->loadHistory($datos["idOrden"],"Actualización de presupuesto que ya fue procesada por ventanilla, firmada por el asesor o confirmada por el cliente y se ha modificado","Cotización Multipunto");

                //Grabamos el evento para el panel de gerencia
                $temp = array(
                    'idUsuario' => '0',
                    'nombre' => '',
                    'dpo' => 'Técnicos',
                    'dpoRecibe' => 'Ventanilla',
                    'descripcion' => 'Cotización Multipunto: Se actualiza cotización y se renvía a ventanilla.',
                );

                $this->registroEvento($datos["idOrden"],$temp);
                
                //Limpiamos los campos de confirmacion
                //Decision del cliente
                $dataCoti_2['aceptoTermino'] = "";
                //Firma del asesor
                $dataCoti_2['firmaAsesor'] = "";
                //Revision por ventanilla
                $dataCoti_2['aprobacionRefaccion'] = 0;
                //Revision por garantias (si aplica)
                $dataCoti_2['envioGarantia'] = 0;
                //Firma garantias
                $dataCoti_2['firmaGarantias'] = "";
                //Revision  de entrega de refacciones
                $dataCoti_2['estado'] = 0;
                //Firma jefe de taller
                $dataCoti_2['firmaJDT'] = "";
                //Revision jefe de taller
                $dataCoti_2['enviaJDT'] = 0;

                $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_2,'idOrden',$datos["idOrden"],'identificador','M');

                //Enviamos el mansaje a ventanilla
                $texto = SUCURSAL.". Actualización de presupuesto (Multipunto). OR: ".$datos["idOrden"]." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;

                //Refacciones
                $this->envio_sms('3316721106',$texto);
                $this->envio_sms('3319701092',$texto);
                $this->envio_sms('3317978025',$texto);
                
                //Mensaje adicional Angel
                $this->envio_sms('5535527547',$texto);
                
                //Regresamos a la vista
                //$this->setData($datos["idOrden"],$datos);
                $this->comodinEditaProcesado($datos["idOrden"],$datos);

            //Si es una actualización normal
            }else {
                //Confirmamos que haya sido modificacion de un tecnico
                if (($datos["dirige"] == "TEC")&&($datos["vista"] == "2")) {
                    //Enviamos mensaje a ventanilla para notificar que se afecto la orden
                    $mensaje_sms = SUCURSAL.". Actualización de cotización (Multipunto) el OR #".$dataCoti["idOrden"]." el día ".$registro." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;
                    
                    //Refacciones
                    $this->envio_sms('3316721106',$mensaje_sms);
                    $this->envio_sms('3319701092',$mensaje_sms);
                    $this->envio_sms('3317978025',$mensaje_sms);

                    //Mensaje adicional  Angel
                    $this->envio_sms('5535527547',$texto);
                    
                }
                
                $this->setData($datos["idOrden"],$datos);
            }
        }
    }

    public function cargacheck($campo = "",$limite = 0)
    {
        $respuesta = "";
        for ($i=1; $i <= $limite; $i++) { 
            if ($this->input->post($campo."".$i) == "SI") {
                $respuesta .= $i."_";
            }
        }

        return $respuesta;
    }

    //Enviamos los mensajes a los numeros de refacciones
    public function sendSMSMult($idOrden = 0)
    {
        $registro = date("d-m-Y");
        //Cargamos los numeros de los usuarios
        $numeros = [];

        $query = $this->mConsultas->notificacionTecnicoCita($idOrden); 
        foreach ($query as $row) {
            $tecnico = $row->tecnico;
            $modelo = $row->vehiculo_modelo;
            $idIntelisis = $row->folioIntelisis;
            $asesor = $row->asesor;
            $placas = $row->vehiculo_placas;
        }

        if (isset($tecnico)) {
            //Definimos el mensaje a enviar
            $texto = SUCURSAL.". Nueva solicitud de presupuesto (Multipunto) creada. OR: ".$idOrden." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;
        
            //Refacciones
            $this->envio_sms('3316721106',$texto);
            $this->envio_sms('3319701092',$texto);
            $this->envio_sms('3317978025',$texto);
            
            //Jefe de taller
            //Jesus Gonzales 
            //$this->envio_sms('3316724093',$texto);
            //Juan Chavez
            //$this->envio_sms('3333017952',$texto);
        
            //Numero de angel (pruebas)
            $this->envio_sms('5535527547',$texto);
        }
    
        $datos["mensaje"] = "1";
        $this->setData($idOrden,$datos);
     }

    //Actualizamos el campo correspondiente
    public function envioSMS($idOrden = '', $datos = NULL)
    {
        $registro = date("d-m-Y");
        //Verificamos si ya se envio desde refacciones
        $revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($revision as $row) {
            if ($row->identificador == "M") {
                $idCotizacion = $row->idCotizacion;
                $acepta = (string)$row->envioGarantia;
                //$acepta = (string)$row->aprobacionRefaccion;  
            }
        }

        //Comprobamos que exista el registro
        if (isset($acepta)) {
            //Verificamos que no se haya enviado anteriormente el mensaje
            if ($acepta == "0") {
                $dataCoti['envioGarantia'] = "1";

                $actualizar_2 = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$idOrden,'identificador','M');
                if ($actualizar_2) {
                    $query = $this->mConsultas->notificacionTecnicoCita($idOrden); 
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $idIntelisis = $row->folioIntelisis;
                        $asesor = $row->asesor;
                        $placas = $row->vehiculo_placas;
                    }

                    $mensaje_sms = SUCURSAL.". Nuevo presupuesto (Multipunto) termino revisión de ".(($datos["vista"] == "3A") ? 'Garantías' : 'Ventanilla').", TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR. ".$idOrden." OR Intelisis: ".$idIntelisis." el día ".$registro;
                    
                    //Enviar mensaje al asesor
                    $celular_sms = $this->mConsultas->telefono_asesor($idOrden);
                    $this->sms_general($celular_sms,$mensaje_sms);

                    //Enviamos mensaje al tecnico
                    //$celular_sms_2 = $this->mConsultas->telefono_tecnico($idOrden);
                    //$this->sms_general($celular_sms_2,$mensaje_sms);
                    
                    if ($datos["vista"] != "3A") {
                        //Jefe de taller
                        //Jesus Gonzales 
                        //$this->envio_sms('3316724093',$mensaje_sms);
                        //Juan Chavez
                        //$this->envio_sms('3333017952',$mensaje_sms);
                        //Gerente de servicio
                        //$this->envio_sms('3315273100',$mensaje_sms);
                    }
                
                    //Numero de angel (pruebas)
                    $this->sms_general('5535527547',$mensaje_sms);
            
                    $temp["mensaje"]="1";
                    // $this->comodinEditaSU($datos["idOrden"],$datos);
                    if ($datos["vista"] == "3A") {
                        redirect(base_url().'Listar_Garantias/4');
                    } else {
                        $this->comodinLista("OT",$temp);
                    }
                    
                }else {
                    $temp["mensaje"]="2";
                    // $this->comodinEditaSU($datos["idOrden"],$datos);
                    if ($datos["vista"] == "3A") {
                        redirect(base_url().'Listar_Garantias/4');
                    } else {
                        $this->comodinLista("OT",$temp);
                    }
                    
                }
            } else{
                $temp["mensaje"] = "1";
                // $this->comodinEditaSU($datos["idOrden"],$datos);
                if ($datos["vista"] == "3A") {
                    redirect(base_url().'Listar_Garantias/4');
                } else {
                    $this->comodinLista("OT",$temp);
                }
                
            }
        }else{
            $temp["mensaje"] = "2";
            // $this->comodinEditaSU($datos["idOrden"],$datos);
            if ($datos["vista"] == "3A") {
                redirect(base_url().'Listar_Garantias/4');
            } else {
                $this->comodinLista("OT",$temp);
            }
            
        }    
    }

    //Validar Radio
    public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Validar multiple check
    public function validateCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."_";
                }
            }
        }
        return $respuesta;
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    //Validamos y guardamos los documentos
    public function validateDoc($nombreDoc = NULL)
    {
        $path = "";
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
        for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docMultipunto";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_";
            for($j=0; $j<=7; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }

    //Validar multiple check
    public function mergeOfString($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    //Verificamos que no sean campos duplicados
                    if (strlen($campo[$i]) > 4) {
                        if ($i > 0) {
                            if (($campo[$i] != $campo[$i-1])&&(strlen($campo[$i]) > 17)) {
                                $respuesta .= $campo[$i]."|";
                            }
                        } else {
                            $respuesta .= $campo[$i]."|";
                        }
                    }
                }
            }
        }
        return $respuesta;
    }

    //Validar multiple check
    public function mergeOfStringTG($campo = NULL,$elseValue = "",$renglones = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $renglones_validos = explode("_",$renglones);

            $respuesta = "";
            if (count($campo) >0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    //separamos los campos
                    $columnas = explode("_",$campo[$i]);

                    if (in_array(($i+1),$renglones_validos)) {
                        //Agregamos el inice del renglon en el registro guardado
                        //              Renglon _ cantidad  _   descripcion     _   pieza   _   costo de la pieza _ horas mo   _    costo mo
                        $respuesta .= ($i+1)."_".$columnas[0]."_".$columnas[1]."_".$columnas[6]."_".$columnas[2]."_".$columnas[3]."_".$columnas[4]."|";
                    }
                }
            }
        }
        return $respuesta;
    }

    //Entrada para el PDF
    public function setDataPDF($idOrden = 0)
    {
        $id_cita = (($idOrden == "0") ? "0" : $this->decrypt($idOrden));

        //Confirmamos que exita la cotizacion
        $revision = $this->mConsultas->get_result("idOrden",$id_cita,"cotizacion_multipunto");
        foreach ($revision as $row) {
            if ($row->identificador == "M") {
                $idCotizacion = $row->idCotizacion;
            }
        }

        //Si existe el presupuesto en la tabla anterior, procedemos
        if (isset($idCotizacion)) {
            $datos["destinoVista"] = "PDF";
            $this->setData($id_cita,$datos);
        //De lo contrario, redireccionamos al nuevo proceso
        } else {
            redirect(base_url().'Presupuesto_PDF/'.$idOrden);
        }
    }

    //Entrada para recuperar informacion para el cliente
    public function comodinCliente($idOrden = 0,$datos = NULL)
    {
        $id_cita = (($idOrden == "0") ? "0" : $this->decrypt($idOrden));
        //Consultamos si ya se ha generado alguna cotizacion para ese presupuesto
        $revision = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $id_presupuesto = $row->id;
        }

        //Si existe una cotizacion generada
        if (isset($id_presupuesto)) {
            //Redireccionamos a los nuevos presupuestos
            redirect(base_url().'Presupuesto_Cliente/'.$idOrden);
        //Verificamos si existe en los presupuestos anteriores
        }else{
            $datos["destinoVista"] = "Cliente";
            $this->setData($id_cita,$datos);
        }
    }

    //Parada comodin para listar en refacciones
    public function comodinLista($origen = "",$datos = NULL)
    {
        $datos["indicador"] = "REF";
        $datos["destino"] = $origen;

        $this->loadDataSearch("",$datos);
    }

    //Parada comodin para listar las cotizaciones que ya fueron aceptadas/rechazdas por el cliente
    public function comodinStatusCliente($origen = "",$datos = NULL)
    {
        $datos["indicador"] = "StatusCambio";
        $datos["destino"] = $origen;

        $this->loadDataSearch("",$datos);
    }

    //Cargamos las cotizaciones generadas
    public function loadDataSearch($origen = "",$datos = NULL)
    {
        if (!isset($datos["indicador"])) {
            $datos["indicador"] = "";
        }

        if (!isset($datos["destino"])) {
            $datos["destino"] = $origen;
        }

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }
        $datos["idOrden"] = [];

        if ($datos["indicador"] == "ACT") {
            //Varificamos si se van a cargar todos las cotizaciones o solo las del tecnico
            if ($this->session->userdata('rolIniciado')) {
                $idTecnico = $this->session->userdata('idUsuario');

                //Si es un tecnico en especifico
                if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {

                    //Verificamos si la orden existe 
                    $query = $this->mConsultas->listaCotizacionDatosTecnicos($idTecnico);

                    foreach ($query as $row){
                        $datos["idOrden"][] = $row->idOrden;
                        $datos["id_cita_url"][] = $this->encrypt($row->idOrden);
                        $datos["idIntelisis"][] = $row->folioIntelisis;

                        $datos["placas"][] = $row->vehiculo_placas;
                        $datos["tecnico"][] = $row->tecnico;
                        $datos["modelo"][] = $row->vehiculo_modelo;
                        $datos["asesor"][] = $row->asesor;
        
                        $fecha = explode(" ",$row->fech_registro);
                        $fechaFracc = explode("-",$fecha[0]);
                        $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];

                        $datos["firmaAsesor"][] = ($row->firmaAsesor != "") ? "SI" : "NO";
                        $datos["aceptoTermino"][] = $row->aceptoTermino;
                        $datos["aprobacionRef"][] = $row->aprobacionRefaccion;

                        $datos["envioRef"][] = $row->enviaJDT;
                    }

                //Super usuarios se despliega todas las cotizaciones
                }else{
                    $query = $this->mConsultas->listaCotizacionDatos();
                    foreach ($query as $row){
                        if (!in_array($row->idOrden,$datos["idOrden"])) {
                            $datos["idOrden"][] = $row->idOrden;
                            $datos["id_cita_url"][] = $this->encrypt($row->idOrden);
                            $datos["idIntelisis"][] = $row->folioIntelisis;

                            $datos["placas"][] = $row->vehiculo_placas;
                            $datos["tecnico"][] = $row->tecnico;
                            $datos["modelo"][] = $row->vehiculo_modelo;
                            $datos["asesor"][] = $row->asesor;
                
                            $fecha = explode(" ",$row->fech_registro);
                            $fechaFracc = explode("-",$fecha[0]);
                            $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];

                            $datos["firmaAsesor"][] = ($row->firmaAsesor != "") ? "SI" : "NO";
                            $datos["aceptoTermino"][] = $row->aceptoTermino;
                            $datos["aprobacionRef"][] = $row->aprobacionRefaccion;

                            $datos["envioRef"][] = $row->enviaJDT;

                            //Validamos si tiene garantia
                            //Si tiene piezas con garantias y no se ha enviado, no aparece para el asesor
                            $datos["garantias"][] = ($row->pzaGarantia != "") ? (($row->envioGarantia != "0") ? "1" : "0") : "1";
                        }
                    }
                }
            }

        //Si es un despliege normal
        } else {
            $query = $this->mConsultas->listaCotizacionDatos();
                foreach ($query as $row){
                    if (!in_array($row->idOrden,$datos["idOrden"])) {
                        $datos["placas"][] = $row->vehiculo_placas;
                        $datos["tecnico"][] = $row->tecnico;
                        $datos["modelo"][] = $row->vehiculo_modelo;
                        $datos["asesor"][] = $row->asesor;

                        $datos["idOrden"][] = $row->idOrden;
                        $datos["id_cita_url"][] = $this->encrypt($row->idOrden);
                        $datos["idIntelisis"][] = $row->folioIntelisis;
                    
                        $fecha = explode(" ",$row->fech_registro);
                        $fechaFracc = explode("-",$fecha[0]);
                        $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];

                        $datos["firmaAsesor"][] = ($row->firmaAsesor != "") ? "SI" : "NO";
                        $datos["aceptoTermino"][] = $row->aceptoTermino;
                        $datos["aprobacionRef"][] = $row->aprobacionRefaccion;

                        $datos["envioRef"][] = $row->enviaJDT;

                        $datos["statusRef"][] = $row->estado;

                        //Validamos si tiene garantia
                        //Si tiene piezas con garantias y no se ha enviado, no aparece para el asesor
                        $datos["garantias"][] = ($row->pzaGarantia != "") ? (($row->envioGarantia != "0") ? "1" : "0") : "1";
                    }
                }                 
        }

        //Verificamos de donde vino
        if ($datos["indicador"] == "StatusCambio") {
            $this->loadAllView($datos,'cotizaciones/cambio_status');
        }elseif ($datos["indicador"] == "ACT") {
            $this->loadAllView($datos,'cotizaciones/procesosExtras/buscador_coti_actualiza');
        }else {
            $this->loadAllView($datos,'cotizaciones/buscador');
        }
    }

    //Cargamos las cotizaciones que pasaron por refacciones
    public function loadListCoat($x = 0, $datos = NULL)
    {
        if (!isset($datos["indicador"])) {
            $datos["indicador"] = "";
        }

        if (!isset($datos["destino"])) {
            $datos["destino"] = "JDT";
        }

        //Recuperamos todas las cotizaciones que se han generado en la multipunto
        //$query = $this->mConsultas->get_table_desc("cotizacion_multipunto");
        $query = $this->mConsultas->listaCotizacionDatos();
        $datos["idOrden"] = [];

        foreach ($query as $row){
            //Comprobamos que sea una cotizacion de la multipunto
            //if ($row->identificador == "M") {
                if ($row->aprobacionRefaccion != 0) {
                    if (!in_array($row->idOrden, $datos["idOrden"])) {
                        $datos["idOrden"][] = $row->idOrden;
                        $datos["id_cita_url"][] = $this->encrypt($row->idOrden);
                        $datos["idIntelisis"][] = $row->folioIntelisis;

                        $datos["placas"][] = $row->vehiculo_placas;
                        $datos["tecnico"][] = $row->tecnico;
                        $datos["modelo"][] = $row->vehiculo_modelo;
                        $datos["asesor"][] = $row->asesor;

                        //Fecha en que se creo la cotizacion
                        $fecha = explode(" ",$row->fech_registro);
                        $fechaFracc = explode("-",$fecha[0]);
                        $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];

                        //Fecha en que envia la notificacin¿on el jefe de taller
                        $fecha_2 = explode(" ",$row->termino_JDT);
                        $fechaFracc_2 = explode("-",$fecha_2[0]);
                        $datos["fechaJDT"][] = $fechaFracc_2[2]."/".$fechaFracc_2[1]."/".$fechaFracc_2[0];

                        //Fecha en que fue atendida por refacciones
                        $fecha_3 = explode(" ",$row->termino_refacciones );
                        $fechaFracc_3 = explode("-",$fecha_3[0]);
                        $datos["fechaRefacciones"][] = $fechaFracc_3[2]."/".$fechaFracc_3[1]."/".$fechaFracc_3[0];

                        $datos["firmaAsesor"][] = ($row->firmaAsesor != "") ? "SI" : "NO";
                        $datos["aceptoTermino"][] = $row->aceptoTermino;
                        $datos["enviaJDT"][] = $row->enviaJDT;

                        $datos["tipoCoti"][] = $row->pzaGarantia;
                    }                    
                }
            //}
        }

        $this->loadAllView($datos,'cotizaciones/procesosExtras/lista_coti_jdt');
    }

    //Recuperamos la informacion de la cotizacion
    public function setData($idOrden = 0,$datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["tipoRegistro"])) {
            $datos["tipoRegistro"] = "";
        }

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == "M") {
                $datos["registrosControl"] = $this->createCells(explode("|",$row->contenido));
                $datos["registrosControlRenglon"] = explode("|",$row->contenido);

                $datos["subTotalMaterial"] = $row->subTotal;
                $datos["ivaMaterial"] = $row->iva;
                $datos["anticipo"] = $row->anticipo;
                $datos["notaAnticipo"] = $row->notaAnticipo;
                $datos["canceladoMaterial"] = $row->cancelado;
                $datos["totalMaterial"] = $row->total;
                $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";
                $datos["archivo"] = explode("|",$row->archivos);
                $datos["archivoTxt"] = $row->archivos;
                $datos["comentario"] = $row->comentario;
                $datos["comentarioTecnico"] = $row->comentarioTecnico;
                $datos["archivoTecnico"] = explode("|",$row->archivoTecnico);
                $datos["archivoImgTecnico"] = $row->archivoTecnico;
                $datos["aceptoTermino"] = $row->aceptoTermino;
                $datos["notaAsesor"] = $row->notaAsesor;
                
                $datos["refacciones"] = $row->aprobacionRefaccion;
                $datos["statusRef"] = $row->estado;

                //Validaciones para garantias
                $datos["pzaGarantia"] = explode("_", $row->pzaGarantia);
                $datos["autorizaPzaGarantia"] = explode("_", $row->pzaAutorizada);
                $datos["envioGarantia"] = $row->envioGarantia;
                $datos["pzaGarantia_Og"] = $row->pzaGarantia;
                $datos["notaGarantias"] = $row->comentariosGarantias;
                $datos["firmaGarantias"] = (file_exists($row->firmaGarantias)) ? $row->firmaGarantias : "";

                $datos["tablaPzsGarantias"] = $this->createCells(explode("|",$row->tablaGarantias));
                $datos["tablaPzsGarantias_rg"] = $row->tablaGarantias;  

                $datos["clientePaga"] = $row->clientePaga;
                $datos["fordPaga"] = $row->fordPaga;
                $datos["totalReparacion"] = $row->totalReparacion;

                //Validaciones para jefe de taller
                $datos["pzsAprobadasJDT"] = explode("_",$row->pzaAprobadas);
                $datos["pzsAprobadasJDT_2"] = $row->pzaAprobadas;
                $datos["envioJDT"] = $row->enviaJDT;
                $datos["firmaJDT"] = (file_exists($row->firmaJDT)) ? $row->firmaJDT : "";

            }
        }

        $datos["idOrden"] = $idOrden;
        $datos["tipoOrigen"] = "MULTIPUNTO";

        //Consultamos si ya se entrego la unidad
        //$datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idOrden);

        //Verificamos donde se imprimira la información  
        if ($datos["destinoVista"] == "PDF") {
            $this->generatePDF($datos);
        //VISTA PARA EL CLIENTE
        }elseif (($datos["destinoVista"] == "Cliente")&&($datos["tipoRegistro"] != "Garantias")) {
            $this->loadAllView($datos,'cotizaciones/cliente');
        //PARA VER EL STATUS DE LAS REFACCIONES SOLICIATADAS
        }elseif ($datos["destinoVista"] == "RefStatus") {
            $this->loadAllView($datos,'cotizaciones/estado_refacciones');
        //PARA CAMBIAR ESTATUS DE UNA COTIZACION AFECTADA POR EL CLIENTE
        }elseif ($datos["destinoVista"] == "StatusCambio") {
            $this->loadAllView($datos,'cotizaciones/cliente_status');
        //PARA EDITAR UNA COTIZACION SIN QUE SE CIERRE LA ORDEN
        }elseif ($datos["destinoVista"] == "INFINITO") {
            $this->loadAllView($datos,'cotizaciones/procesosExtras/presupuesto_acumulativo');
        //PROCESOS DE VENTANILLA
        }else {
            //PARA CARGAR LA VISTA DE REFACCIONES
            if ($datos["tipoRegistro"] == "Otros") {
                $this->loadAllView($datos,'cotizaciones/cotizacion_edita_2');
            //PARA CARGAR LA VISTA DE GARANTIAS
            }elseif ($datos["tipoRegistro"] == "Garantias") {
                $this->loadAllView($datos,'cotizaciones/procesosExtras/cotizacion_garantias');
            //PARA CARGAR LA VISTA DE JEFE DE TALLER
            }elseif ($datos["tipoRegistro"] == "JDT") {
                $this->loadAllView($datos,'cotizaciones/procesosExtras/cotizacion_jefeTaller');
            }else {
                $this->loadAllView($datos,'cotizaciones/cotizacion_editar');
            }
        }
    }

    //Recuperamos la informacion para crear el pdf de la requisicion
    public function loadCoat($idOrden = 0,$datos = NULL)
    {
        $id_cita = (($idOrden == "0") ? "0" : $this->decrypt($idOrden));
        //Recuperamos la cotizacion
        $query = $this->mConsultas->get_result("idOrden",$id_cita,"cotizacion_multipunto");
        foreach ($query as $row){
            //Comprobamos que sea una cotizacion de la multipunto
            if ($row->identificador == "M") {
                $registros = $this->createCells(explode("|",$row->contenido));
                $decision = $row->aceptoTermino;
                $fechaAlta = $row->fech_actualiza;
                $idCoti = $row->idCotizacion;
            }
        }

        //Se se recupero con exito la cotizacion
        if (isset($decision)) {
            $datos["requisicion"] = $this->requisicion($idCoti);
            //Recuperamos datos del cliente
            $query = $this->mConsultas->api_orden($id_cita);
            foreach ($query as $row) {
                $datos["ordenT"] = $row->id_Cita_orden."".$row->identificador;
                $datos["nombreCliente"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            }

            //Recuperamos la informacion que se encesita de las piezas
            foreach ($registros as $index => $value) {
                $cantidad[] = $registros[$index][0];
                $numPieza[] = str_replace(" ", "" ,$registros[$index][6]);
                if (count($registros[$index])>9) {
                    $filaDecide[] = $registros[$index][count($registros[$index])-1];
                }
            }

            //Fecha de la pieza
            $datosFecha = explode(" ",$fechaAlta);
            $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
            $dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');

            if (count($datosFecha)>1) {
                $fechats = strtotime($datosFecha[0]); //pasamos a timestamp

                $fecha = explode("-",$datosFecha[0]);
                $datos["dia"] = $fecha[2];
                $datos["anio"] = $fecha[0];

                $mesPrv = (int)$fecha[1];
                $datos["mes"] = $meses[$mesPrv-1];
                $datos["diaSemeana"] = $dias[date('w', $fechats)];

                $hora1 = explode(":",$datosFecha[1]);
                $datos["hora"] = $hora1[0].":".$hora1[1];
                if ((int)$hora1[0] < 13) {
                  $datos["hora"] .= " AM";
                }else {
                  $datos["hora"] .= " PM";
                }
            //Fecha de hoy por soporte
            }else {
                $fechats = strtotime(date("Y-m-d")); //pasamos a timestamp

                $datos["dia"] = date("d");
                $datos["anio"] = date("Y");
                $datos["mes"] = $meses[date("n")-1];
                $datos["hora"] = date("H:i");
                $datos["diaSemeana"] = $dias[date('w', $fechats)];
            }

            $datos["vendedor"] = "M9";

            //Verificamos si fue aceptada completamente
            if ($decision == "Si") {
                //Recuperamos la informacion de la refaccion para enviar
                $contador = 1;
                for ($i=0; $i <count($numPieza) ; $i++) {
                    if ($numPieza[$i] != "") {
                        $inventario = $this->mConsultas->refaccionVerificaClave($numPieza[$i]);
                        foreach ($inventario as $row) {
                            if (($contador > 10)&&($contador < 100)) {
                                $const = "0".$contador;
                                $renglon = $contador;
                            }elseif ($contador < 10) {
                                $const = "00".$contador;
                                $renglon = "0".$contador;
                            }else {
                                $renglon = $const;
                            }

                            $datos["cantidad"][] = $cantidad[$i];
                            $datos["clave"][] = $row->clave_comprimida;
                            $datos["gpo"][] = $row->gpo;
                            $datos["prefijo"][] = $row->prefijo;
                            $datos["basico"][] = $row->basico;
                            $datos["sufijo"][] = $row->sufijo;
                            $datos["descripcion"][] = $row->descripcion;
                            $datos["ubicacion"][] = $row->ubicacion;
                            $datos["renglon"][] = $renglon;
                            $datos["precio_lista"][] = $row->p_lista;
                            $datos["precio"][] = $row->c_repos;

                            $contador++;
                        }
                    }
                }
            //O si fue acepta por partes
            } else {
                //Recuperamos la informacion de la refaccion para enviar
                $contador = 1;
                for ($i=0; $i <count($numPieza) ; $i++) {
                    if (($filaDecide[$i] == "Aceptada") && ($numPieza[$i] != "")) {
                        $inventario = $this->mConsultas->refaccionVerificaClave($numPieza[$i]);
                        foreach ($inventario as $row) {
                            if (($contador > 10)&&($contador < 100)) {
                                $const = "0".$contador;
                                $renglon = $contador;
                            }elseif ($contador < 10) {
                                $const = "00".$contador;
                                $renglon = "0".$contador;
                            }else {
                                $renglon = $const;
                            }

                            $datos["cantidad"][] = $cantidad[$i];
                            $datos["clave"][] = $row->clave_comprimida;
                            $datos["gpo"][] = $row->gpo;
                            $datos["prefijo"][] = $row->prefijo;
                            $datos["sufijo"][] = $row->sufijo;
                            $datos["basico"][] = $row->basico;
                            $datos["descripcion"][] = $row->descripcion;
                            $datos["ubicacion"][] = $row->ubicacion;
                            $datos["precio"][] = $row->c_repos;
                            $datos["precio_lista"][] = $row->p_lista;
                            $datos["renglon"][] = $renglon;

                            $contador++;
                        }
                    }
                }
            }
        }

        $this->generatePDF($datos,"cotizaciones/requisicionPdf");
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Recibir ajax para cotizacion
    public function getAjaxForm()
    {
        //Verificamos que las variables hayan llegado
        if (count($_POST)>0) {
            //Recuperamos los valores
            $idOrden = $_POST["idOrden"];
            $eleccion = $_POST["eleccion"];

            //Comprobamos que no haya valores invalidos
            if (($idOrden != "0") && ($idOrden != "")) {
                //Identificamos si la cotizacion ya ha generado alguna decision
                $revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
                foreach ($revision as $row) {
                    if ($row->identificador == "M") {
                        $idCotizacion = $row->idCotizacion;
                        $acepta = $row->aceptoTermino;
                    }
                }

                //Si no se ha tomado ninguna decision previamente y la cotizacion fue cancelada
                if (($acepta == "")&&($eleccion == "No")) {
                    //Cancelada por el cliente (no se modifica el inventario)
                    // $actualiza = TRUE;
                    $actualiza = "Cancelada";
                //Si no se ha tomado ninguna decision previamente y la cotizacion fue aprobada
                }elseif (($acepta == "")&&($eleccion == "Si")) {
                    //Cotización aprobada por el cliente
                    //Actualizamos los inventarios
                    $actualiza = $this->stockUpdate($idOrden,"1");
                //Si ya se habia tomado una decision y la cotización se cancela
                }elseif (($acepta != "")&&($eleccion == "No")) {
                    //Cotización cancelada por el cliente, posteriormente a haber sido aprobada
                    //Actualizamos los inventarios
                    $actualiza = $this->stockUpdate($idOrden,"0");
                //Si ya se habia tomado una decision y la cotización se cancela
              }elseif (($acepta != "")&&($eleccion == "Si")) {
                    //Cotización aprobada por el cliente, posteriormente a haber sido cancelada
                    //Actualizamos los inventarios
                    $actualiza = $this->stockUpdate($idOrden,"1");
                }

                //Si se actualizo correctamente el inventario
                if ($actualiza) {
                    //Actualizamos el registro
                    $registro = date("Y-m-d H:i:s");
                    $dataCoti = array(
                        "aceptoTermino" => $eleccion,
                        "fech_actualiza" => $registro,
                    );
                    //Guardamos la información
                    $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$idOrden,'identificador','M');
                    if ($actualizar) {
                        echo "OK";
                        $this->loadHistory($idOrden,"Actualización Cotización: ".$eleccion,"Cotización");
                        //Enviamos el mensaje al de refacciones
                        $this->smsData($idOrden,$eleccion);
                    }else {
                        echo "Error 03";
                    }
                }else {
                    echo "Error 03";
                }
            }else {
                echo "Error 02";
            }
        }else {
            echo "Error 01";
        }
    }

    //Actualizamos el inventario de refacciones
    public function stockUpdate($idOrden = '',$modoActualiza = "0")
    {
        //Definimos valor default de retorno
        $retorno = FALSE;
        $existencia = 0;

        //Recuperamos la informacion de la cotizacion
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == "M") {
                $registros = $this->createCells(explode("|",$row->contenido));
            }
        }

        //Comprobamos los registro
        if (isset($registros)) {
            foreach ($registros as $index => $value) {
                $cantidad[] = $registros[$index][0];
                $numPieza[] = $registros[$index][6];
            }

            $registro = date("Y-m-d H:i:s");
            //Creamos ciclo para abarcar todos los capos recuperados
            for ($i=0; $i <count($cantidad) ; $i++) {
                //Buscamos la refacción en el inventario para actualizarlo
                $inventario = $this->mConsultas->get_result("clave_comprimida",$numPieza[$i],"refacciones");
                foreach ($inventario as $row) {
                    $existencia = $row->existencia;
                }

                //Verificamos si la refaccion va de salida o va aingresar al  inventario
                if ($modoActualiza == "1") {
                    //Sacamos del invetario actual
                    $nvoStock = $existencia - $cantidad[$i];
                }else {
                    //Regresamos al inventario
                    $nvoStock = $existencia + $cantidad[$i];
                }

                //Actualizamos el inventario
                $dataStock = array(
                    "existencia" => $nvoStock,
                    "fecha_actualiza" => $registro,
                );
                //Guardamos la información
                $actualizar = $this->mConsultas->update_table_row('refacciones',$dataStock,'clave_comprimida',$numPieza[$i]);

            }

            $retorno = TRUE;
        }

        return $retorno;
    }

    //Armamos el paquete de datos
    public function smsData($idOrden = 0, $estatus = "No")
    {
        $registro = date("d-m-Y");

        if ($estatus == "Si") {
            $decision = "Aceptada";
        }elseif ($estatus == "Afectada") {
            $decision = "Autorizada Parcialmente";
        }else {
            $decision = "Rechazada";
        }

        $data_Coti["termino_cliente"] = date("Y-m-d H:i:s");
        $this->mConsultas->update_table_row_2('cotizacion_multipunto',$data_Coti,'idOrden',$idOrden,'identificador','M');

        //Recuperamos datos extras para la notificación
        $query = $this->mConsultas->notificacionTecnicoCita($idOrden); 
        foreach ($query as $row) {
            $tecnico = $row->tecnico;
            $modelo = $row->vehiculo_modelo;
            $placas = $row->vehiculo_placas;
            $asesor = $row->asesor;
            $idIntelisis = $row->folioIntelisis;
        }

        //Grabamos el evento para el panel de gerencia
        $temp = array(
            'idUsuario' => '0',
            'nombre' => 'Cliente',
            'dpo' => 'Cliente',
            'dpoRecibe' => 'Asesores',
            'descripcion' => 'Cotización Multipunto: Afectada por el cliente.',
        );

        $this->registroEvento($idOrden,$temp);

        $mensaje_sms = SUCURSAL.". La cotización (Multipunto) de la OR ".$idOrden." ha sido '".$decision."' por el usuario el día ".$registro." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;

        //Enviar mensaje al asesor
        $celular_sms = $this->mConsultas->telefono_asesor($idOrden);
        $this->sms_general($celular_sms,$mensaje_sms);

        //Enviamos mensaje al tecnico
        $celular_sms_2 = $this->mConsultas->telefono_tecnico($idOrden);
        $this->sms_general($celular_sms_2,$mensaje_sms);

        //Refacciones
        $this->envio_sms('3316721106',$mensaje_sms);
        $this->envio_sms('3319701092',$mensaje_sms);
        $this->envio_sms('3317978025',$mensaje_sms);

        //Jefe de taller
        //Jesus Gonzales 
        //$this->envio_sms('3316724093',$mensaje_sms);
        //Juan Chavez
        $this->envio_sms('3333017952',$mensaje_sms);
        //Gerente de servicio
        //$this->envio_sms('3315273100',$mensaje_sms);
        
        //Numero de angel (pruebas)
        $this->sms_general('5535527547',$mensaje_sms);
    }

    //Enviamos el mensaje para el asesor
    public function sms_general($celular_sms='',$mensaje_sms=''){
         $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    //Enviamos el mensaje para el asesor
    public function envio_sms($celular_sms = '', $mensaje_sms = '')
    {
        $validar = FALSE;
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);
    
         //Comprobamos que se haya enviado correctamente
         if($server_output) {
             $validar = TRUE;
         }
         // var_dump($server_output);
         // echo "<br>";
         // var_dump($validar);
         // echo "<br><br>";
    
         return $validar;
    }

    //Prueba de envio de notificacion
    public function pruebaCel($celular = ''){
        $validar = FALSE;
        
        //$celular = "5535527547";
        $mensaje = SUCURSAL." :  Prueba envio notificación push en cotizaciones";
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular."&mensaje=".$mensaje."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        var_dump($server_output );
        curl_close ($ch);
    }

    //Peticion ajax para recuperar filtros
    public function filtrosRef()
    {
        //Recuperamos la informacion para filtrar
        $valorFiltro = $_POST['valor_1'];
        $valorFiltro_ant = $_POST['valor_2'];

        $columna = $_POST['dato_1'];
        $columna_ant = $_POST['dato_2'];

        $cadena = "";
        $precarga2 = $this->mConsultas->filtro_Sec($columna,'clave_comprimida',$columna_ant,'refacciones',$valorFiltro,$valorFiltro_ant);
        foreach ($precarga2 as $row) {
            $cadena .= $row->$columna."|";
        }
        echo $cadena;
    }

    //Recuperar final de filtros de refacciones
    public function filtrosRefFinal()
    {
        //Recuperamos la informacion para filtrar
        $valorFiltro = $_POST['dato'];
        $pivote = $_POST['pivote'];

        $cadena = "";
        $final = $this->mConsultas->get_result($pivote,$valorFiltro,"refacciones");
        foreach ($final as $row) {
            $cadena .= $row->descripcion."|";
            $cadena .= $row->fue."|";
            $cadena .= $row->cla."|";
            $cadena .= $row->p_lista."|";
            $cadena .= $row->existencia."|";
            $cadena .= $row->ubicacion."|";
            $cadena .= $row->gpo."|";
            $cadena .= $row->prefijo."|";
            $cadena .= $row->basico."|";
            $cadena .= $row->sufijo."|";
        }
        echo $cadena;
    }

    //Recuperar filtro de refacciones por descripcion
    public function filtrosDescripcion()
    {
        //Recuperamos la informacion para filtrar
        $valorFiltro = $_POST['valor_1'];
        $columna = $_POST['dato_1'];

        $valor_limpio = str_replace(".","",$valorFiltro);
        $cadena = "";

        $precarga2 = $this->mConsultas->filtro_descripcion($columna,$valor_limpio,'refacciones');
        foreach ($precarga2 as $row) {
            $cadena .= "(".$row->clave_comprimida.")  ".$row->descripcion."_";
        }
        echo $cadena;
    }

    //Recuperamos la informacion de la cotizacion (solo aceptadas)
    public function extraQuote($idOrden  = "")
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];

        try {
            // $idOrden = $_POST["idOrden"];

            $contenedor = [];
            //Recuperamos la información de la cotizacion
            $revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
            foreach ($revision as $row) {
                if ($row->identificador == "M") {
                    $idCotizacion = $row->idCotizacion;
                    $acepta = $row->aceptoTermino;
                    $cuerpo = $this->createCells(explode("|",$row->contenido));
                    $subtotal = $row->subTotal;
                    $iva = $row->iva;
                    $cancelado = $row->cancelado;
                    $total = $row->total;
                    $comentario = $row->comentario;
                }
            }

            //Comprobamos que se haya cargado
            if (isset($idCotizacion)) {
                //Verificamos la respuesta afirmativa
                if (($acepta == "Si")||($acepta == "Val")) {
                    $articulos = [];
                    //Comprobamos si la cotizacion se acepto completa o por partes
                    if ($acepta == "Si") {
                        //Recuperamos los elementos de la cotización
                        foreach ($cuerpo as $index => $valor) {
                            $articulos["cantidad"] = $cuerpo[$index][0];
                            $articulos["descripcion"] = $cuerpo[$index][1];
                            $articulos["pieza"] = $cuerpo[$index][6];
                            $articulos["costo"] = $cuerpo[$index][2];

                            $contenedor["cotizacion"][] = $articulos;
                        }
                    }else {
                        //Recuperamos los elementos de la cotización
                        foreach ($cuerpo as $index => $valor) {
                            if ($cuerpo[$index][9] == "Aceptada") {
                                $articulos["cantidad"] = $cuerpo[$index][0];
                                $articulos["descripcion"] = $cuerpo[$index][1];
                                $articulos["pieza"] = $cuerpo[$index][6];
                                $articulos["costo"] = $cuerpo[$index][2];

                                $contenedor["cotizacion"][] = $articulos;
                            }
                        }
                    }

                    $contenedor["respuesta"] = "OK";
                }else {
                    $contenedor["cotizacion"] = array();
                    $contenedor["respuesta"] = "Cotización Rechazada";
                }
            } else {
                $contenedor["cotizacion"] = array();
                $contenedor["respuesta"] = "ERROR";
            }
        } catch (\Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    //Validamos las credenciales del usuario para anticipo refacciones
    public function usuarioAnticipo()
    {
        //Recuperamos la informacion para filtrar
        $usuario = $_POST['user'];
        $pass = $_POST['pass'];

        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();
        if (isset($row)){
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;

            //Verificamos que el usuario sea correcto
            if ($idUsuario == 28) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    $retorno = "OK";
                }else {
                    //Contraseña sin encriptar
                    if ($pass == $passDB ) {
                        $retorno = "OK";
                    } else {
                      $retorno = "CONTRASEÑA INCORRECTA";
                    }
                }
            }else {
                $retorno = "USUARIO SIN PERMISOS";
            }

        }else {
            $retorno = "NO EXISTE EL USUARIO";
        }

        echo $retorno;
    }

    //Guardamos el anticipo en la base de datos
    public function agregarAnticipo() 
    {
        $cantidad = $_POST['cantidad'];
        $comentario = $_POST['comentario'];
        $idCoti = $_POST['idCoti'];
        $total = $_POST['total'];

        //Comprobamos los datos
        if (($cantidad != "") && ($idCoti != "")) {
            $dataCoti['anticipo'] = $cantidad;
            $dataCoti['total'] = $total;
            $dataCoti['notaAnticipo'] = $comentario;

            $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$idCoti,'identificador','M');
            if ($actualizar) {
                $this->loadHistory($idCoti,"Se anexa anticipo a la cotización por: ".$cantidad,"Cotización");
                $retorno = "OK";
            }else {
                $retorno = "ERROR AL GUARDAR LA INFORMACIÓN";
            }
        }else {
            $retorno = "VERIFICAR INFORMACIÓN ENVIADA";
        }

        echo $retorno;
    }

    //Guardamos el anticipo en la base de datos
    public function cambiarEstadoEntregaRef()
    {
        $idCoti = $_POST['idCoti'];
        $estado = $_POST['estado'];

        //Comprobamos los datos
        if (($estado != "") && ($idCoti != "")) {
            $query = $this->mConsultas->notificacionTecnicoCita($idCoti); 
            foreach ($query as $row) {
                $tecnico = $row->tecnico;
                $modelo = $row->vehiculo_modelo;
                $idIntelisis = $row->folioIntelisis;
                $asesor = $row->asesor;
                $placas = $row->vehiculo_placas;
            }

            $dataCoti['estado'] = $estado;

            $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$idCoti,'identificador','M');

            if ($actualizar) {
                $registro = date("Y-m-d H:i:s");
                //Se solicitan las refacciones
                if ($estado == "1") {
                    $this->loadHistory($idCoti,"Se pidieron las piezas para refacciones ","Cotización");

                    //Guardamos el momento de la accion
                    $dataCoti_A['solicita_pieza'] = $registro;
                    $dataCoti_A['fech_actualiza'] = $registro;
                    $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_A,'idOrden',$idCoti,'identificador','M');

                //Se entregan las refacciones
                }elseif ($estado == "2") {
                    $this->loadHistory($idCoti,"Se entregaron las piezas de refacciones ","Cotización");

                    //Guardamos el momento de la accion
                    $dataCoti_A['entrega_pieza'] = $registro;
                    $dataCoti_A['fech_actualiza'] = $registro;
                    $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_A,'idOrden',$idCoti,'identificador','M');

                    //Grabamos el evento para el panel de gerencia
                    $temp = array(
                        'idUsuario' => '0',
                        'nombre' => 'Ventanilla',
                        'dpo' => 'Ventanilla',
                        'dpoRecibe' => 'Técnicos',
                        'descripcion' => 'Cotización Multipunto: Se entregan refacciones de la cotización al técnico.',
                    );

                    $this->registroEvento($idCoti,$temp);

                    //Informamos al asesor que ventanilla ya aentrego las refacciones al tecnico
                    $mensaje = SUCURSAL.". Refacciones autorizadas de la OR ".$idCoti." entregadas al técnico el día: ".date("d-m-Y H:i")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor."."." OR Intelisis: ".$idIntelisis;

                    //Enviar mensaje al asesor
                    $celular_sms = $this->mConsultas->telefono_asesor($idCoti);
                    $this->sms_general($celular_sms,$mensaje);

                    //Enviamos mensaje al tecnico
                    $celular_sms_2 = $this->mConsultas->telefono_tecnico($idCoti);
                    $this->sms_general($celular_sms_2,$mensaje);

                    //Otro (Angel)
                    $this->sms_general('5535527547',$mensaje);
                //Recibir refacciones
                }elseif ($estado == "3") {
                    $this->loadHistory($idCoti,"Se recibieron las piezas de refaccion ","Cotización Multipunto");

                    //Enviamos la notificacion al jefe de taller
                    //Informamos al asesor que ventanilla ya aentrego las refacciones al tecnico
                    $mensaje = SUCURSAL.". Refacciones entregadas a ventanilla de la OR ".$idCoti." el día: ".date("d-m-Y H:i")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR Intelisis: ".$idIntelisis;

                    //Otro (Angel)
                    $this->sms_general('5535527547',$mensaje);

                    //Jefe de taller
                    //Jesus Gonzales 
                    //$this->sms_general('3316724093',$mensaje);
                    //Juan Chavez
                    //$this->sms_general('3333017952',$mensaje);

                    //Guardamos el momento de la accion
                    $dataCoti_A['recibe_pieza'] = $registro;
                    $dataCoti_A['fech_actualiza'] = $registro;
                    $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_A,'idOrden',$idCoti,'identificador','M');
                }else{
                    $this->loadHistory($idCoti,"Movimientos de refacciones ","Cotización");
                }                 
                $retorno = "OK";
            }else {
                $retorno = "ERROR AL GUARDAR LA INFORMACIÓN";
            }
        }else {
            $retorno = "VERIFICAR INFORMACIÓN ENVIADA";
        }

        echo $retorno;
    }

        //Registro de eventos 
    public function registroEvento($idOrden = 0, $datos = NULL)
    {
        //Recuperamos los datos del usuario
        if ($this->session->userdata('rolIniciado')) {
            $envia_id_usuario = $this->session->userdata('idUsuario');
            $envia_nombre_usuario = $datos["nombre"];

            if ($this->session->userdata('rolIniciado') == "TEC") {
                $revision = $this->mConsultas->get_result("id",$envia_id_usuario,"tecnicos");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->nombre);
                }
            } else {
                $revision = $this->Verificacion->get_result("adminId",$envia_id_usuario,"admin");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->adminNombre);
                }
            }
        }else{
            $envia_id_usuario = $datos["idUsuario"];
            $envia_nombre_usuario = $datos["nombre"];
        }

        $envia_departamento = $datos["dpo"];
        $sucursal = BIN_SUCURSAL;
        $envia_fecha_hora = date("Y-m-d H:i:s");
        $recibe_departamento = $datos["dpoRecibe"];
        $id_cita = $idOrden;
        $descripcion = $datos["descripcion"];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/gerencia/index.php/tiempos/insertar_tiempo");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "envia_id_usuario=".$envia_id_usuario."&envia_nombre_usuario=".$envia_nombre_usuario."&envia_departamento=".$envia_departamento."&sucursal=".$sucursal."&envia_fecha_hora=".$envia_fecha_hora."&recibe_departamento=".$recibe_departamento."&id_cita=".$id_cita."&descripcion=".$descripcion."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);
    }

    //Busqueda de cotizaciones
    public function filtroCotizaciones()
    {
        $campo = $_POST['campo'];
        $indicador = $_POST['indicador'];
        
        $cadena = "";

        $precarga = $this->mConsultas->listaCotizacionBusqueda($campo);

        foreach ($precarga as $row) {
            // 0 indicador de la vista
            $cadena .= $indicador."=";
            // 1 idOrden
            $cadena .= $row->idOrden."="; 
            $fecha = explode(" ",$row->fech_registro);
            $fechaFracc = explode("-",$fecha[0]);
            // 2 fechaCaptura
            $cadena .= $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0]."=";
            // 3 modelo
            $cadena .= $row->vehiculo_modelo."=";
            // 4 placas
            $cadena .= $row->vehiculo_placas."=";
            // 5 asesor
            $cadena .= $row->asesor."=";  
            // 6 tecnico
            $cadena .= $row->tecnico."=";
            // 7 aceptoTermino
            $cadena .= $row->aceptoTermino."=";
            // 8 firmaAsesor
            $cadena .= (($row->firmaAsesor != "") ? "SI" : "NO")."=";
            // 9 aprobacionRef
            $cadena .= $row->aprobacionRefaccion."=";
            // 10 identificador
            $cadena .= $row->identificador."=";
            // 11 pzaGarantia
            $cadena .= count(explode("_", $row->pzaGarantia))."=";
            // 12 envioGarantia
            $cadena .= $row->envioGarantia."=";
            // 13 envioRef
            $cadena .= $row->enviaJDT."=";
            // 14 statusRef
            $cadena .= $row->estado."="; 
            // 15 garantias
            $cadena .= (($row->pzaGarantia != "") ? (($row->envioGarantia != "0") ? "1" : "0") : "1")."=";
            //16
            $cadena .= $row->folioIntelisis."=";
            //17
            $cadena .= $this->encrypt($row->idOrden)."="; 
            $cadena .= "|";
        } 

        echo $cadena;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("cotizaciones/plantillaPdf", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_HM.js',
                'assets/js/materiales/presupuestoMulti.js',
                'assets/js/materiales/autosuma.js',
                'assets/js/materiales/anexos.js',
                'assets/js/materiales/reglasVista.js',
                'assets/js/materiales/anexos_2.js',
                'assets/js/materiales/anexos_3.js',
                'assets/js/materiales/filtroCampo.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Cotización"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
