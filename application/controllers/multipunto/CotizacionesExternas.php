<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class CotizacionesExternas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($idOrden = 0,$datos = NULL)
    {
        //Precargamos el catalogo de gpos de las refacciones
        $precarga = $this->mConsultas->filtro_princ('gpo','refacciones','gpo');
        foreach ($precarga as $row) {
            $datos["grupo"][] = $row->gpo;
        }

        if ($this->session->userdata('rolIniciado')) {
            $datos["dirige"] = $this->session->userdata('rolIniciado');
        }else {
            $datos["dirige"] = "";
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        //Identificamos si la orden se servicio ya ha generado alguna cotizacion
        $revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($revision as $row) {
            if ($row->identificador == "E") {
                $idCotizacion = $row->idCotizacion;
                $acepta = $row->aceptoTermino;
            }
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idCotizacion)) {
            //Consultamos si ya se entrego la unidad
            $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idOrden);

            //Verificamos si la cotización ya fue aceptada/rechazada por el cliente
            if ($acepta != "") {
                //Si no existe un destino de vista , se asume que es la vista del cliente
                if (!isset($datos["destinoVista"])) {
                    $datos["destinoVista"] = "Cliente";
                }
                $this->setData($idOrden,$datos);
            }else {
                $this->setData($idOrden,$datos);
            }
        } else {
            $datos['idOrden'] = $idOrden;
            $this->loadAllView($datos,'cotizaciones/externa/cotizacion_alta');
        }

    }

    //Comodin para ventana emergentes
    public function comodinRedireccion($valores = "")
    {
        $tipos = explode("_",$valores);

        if (count($tipos)>1) {
            $datos["tipoRegistro"] = $tipos[1];
            $this->index($tipos[0],$datos);
        }else {
            $datos["tipoRegistro"] = "Asesor";
            $this->index($valores,$datos);
        }
    }

    //Redireccion para edicion de superUsuario
    public function comodinEditaSU($idOrden = '')
    {
        $datos["tipoRegistro"] = "Otros";
        $this->index($idOrden,$datos);
    }

    //Redireccionamos para edicion de status del cliente
    public function comodinEditaStatus($idOrden = '')
    {
        $datos["tipoRegistro"] = "StatusCambio";
        $datos["destinoVista"] = "StatusCambio";
        $this->index($idOrden,$datos);
    }

    //Validamos el formulario
    public function validateForm()
    {
        $datos["vista"] = $this->input->post("modoVistaFormulario");
        $datos["idOrden"] = $this->input->post("idOrdenTemp");
        $datos["tipoRegistro"] = $this->input->post("tipoRegistro");
        $datos["modoGuardado"] = $this->input->post("modoGuardado");
        $datos["dirige"] = $this->input->post("dirige");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idOrden"]);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        if ($datos["vista"] == "1") {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required|callback_existencia');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }else {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('registros','No se ha guardado ningun elemento');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar el No. de Orden.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $datos["mensaje"] = "1";
            //Si la vista es de decision
            if ($datos["vista"] == "5") {
                $this->getDataWish($datos);
            }else {
                $this->getDataForm($datos);
            }

        }else{
            $datos["mensaje"]="0";
            if ($datos["vista"] == "1") {
                $this->index($datos["idOrden"],$datos);
            }else {
                if (($datos["vista"] == "3")||($datos["vista"] == "5")) {
                    $this->comodinCliente($datos["idOrden"],$datos);
                }elseif ($datos["vista"] == "4") {
                    $this->comodinEditaSU($datos["idOrden"],$datos);
                } else {
                    $this->setData($datos["idOrden"],$datos);
                }
            }

        }
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idOrdenTemp");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == "E") {
                $dato = $row->idCotizacion;
            }
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Validar requisitos minimos P1
    public function registros()
    {
        $return = FALSE;
        if ($this->input->post("registros")) {
            if (count($this->input->post("registros")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Recuperamos la información del formulario
    public function getDataWish($datos = NULL)
    {
        $registro = date("Y-m-d H:i");
        $dataCoti['contenido'] = $this->mergeOfString($this->input->post("registros"),"");
        $dataCoti['subTotal'] = $this->input->post("subTotalMaterial");
        $dataCoti['cancelado'] = $this->input->post("canceladoMaterial");
        $dataCoti['iva'] = $this->input->post("ivaMaterial");
        $dataCoti['total'] = $this->input->post("totalMaterial");

        $dataCoti['aceptoTermino'] = "Val";
        $dataCoti['fech_actualiza'] = $registro;

        $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$datos["idOrden"],'identificador','E');
        if ($actualizar) {
            //Guardamos el movimiento
            $this->loadHistory($datos["idOrden"],"Actualización Cotización: ".$dataCoti['aceptoTermino'],"Cotización");
            // $this->smsData($datos["idOrden"],"Afectada");
            $datos["mensaje"]="1";
        }else {
            $datos["mensaje"]="0";
        }

        $this->comodinCliente($datos["idOrden"],$datos);
    }

    //Recuperamos la información del formulario
    public function getDataForm($datos = NULL)
    {
        $registro = date("Y-m-d H:i");
        //Creamos el array para guardado
        if ($datos["vista"] == "1") {
            $dataCoti = array(
                'idCotizacion' => NULL,
                'idOrden' => $this->input->post("idOrdenTemp"),
                'identificador' => "E",
            );
        }

        $dataCoti['contenido'] = $this->mergeOfString($this->input->post("registros"),"");
        $dataCoti['subTotal'] = $this->input->post("subTotalMaterial");
        $dataCoti['iva'] = $this->input->post("ivaMaterial");
        $dataCoti['total'] = $this->input->post("totalMaterial");

        if ($datos["vista"] == "1") {
            $dataCoti['aprobacionRefaccion'] = "0";
        }

        $dataCoti['firmaAsesor'] = $this->base64ToImage($this->input->post("rutaFirmaAsesorCostos"),"firmas");
        // $dataCoti['archivos'] = $this->validateDoc("uploadfiles");

        #if($this->input->post("tempFile") != "") {
        #    $archivos_2 = $this->validateDoc("uploadfiles");
        #    $dataCoti["archivos"] = $this->input->post("tempFile")."".$archivos_2;
        #}else {
        #    $dataCoti["archivos"] = $this->validateDoc("uploadfiles");
        #}
        $dataCoti["archivos"] = "";

        if ($datos["vista"] == "1") {
            $dataCoti['aceptoTermino'] = "";
        }

        if ($this->input->post("comentariosExtra")) {
            $dataCoti['comentario'] = $this->input->post("comentariosExtra");
        }

        if ($this->input->post("comentarioTecnico")) {
            $dataCoti['comentarioTecnico'] = $this->input->post("comentarioTecnico");
        }

        #if($this->input->post("tempFileTecnico") != "") {
        #    $archivos_3 = $this->validateDoc("uploadfilesTec");
        #    $dataCoti["archivoTecnico"] = $this->input->post("tempFileTecnico")."".$archivos_3;
        #}else {
        #    $dataCoti["archivoTecnico"] = $this->validateDoc("uploadfilesTec");
        #}
        $dataCoti["archivoTecnico"] = "";

        if ($datos["vista"] == "1") {
            $dataCoti['fech_registro'] = $registro;
        }

        $dataCoti['fech_actualiza'] = $registro;

        //Identificamos si se creara el registro o se actualizara
        if ($datos["vista"] == "1") {
            $idReturn = $this->mConsultas->save_register('cotizacion_multipunto',$dataCoti);
            if ($idReturn != 0) {
                //Guardamos el movimiento
                $this->loadHistory($datos["idOrden"],"Registro","Cotización");

                //Enviamos el mensaje al area de refacciones
                // $this->sendSMSMult($datos["idOrden"]);

                $datos["mensaje"] = "1";
                $this->setData($datos["idOrden"],$datos);
            }else {
                $datos["mensaje"] = "2";

                $this->index($datos["idOrden"],$datos);
            }
        }else {
            $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$datos["idOrden"],'identificador','E');
            if ($actualizar) {
                //Guardamos el movimiento
                $this->loadHistory($datos["idOrden"],"Actualizacion","Cotización");

                $datos["mensaje"]="1";
            }else {
                $datos["mensaje"]="0";
            }

            if ($datos["vista"] == "3") {
                $this->comodinCliente($datos["idOrden"],$datos);
            }elseif ($datos["vista"] == "4") {
                if ($datos["modoGuardado"] == "Terminar") {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idOrden"],"Actualización (Termina trabajo refacciones)","Cotización");
                    
                    //Guardamos el momento en que se actualizo la cotizacion (termino de refacciones)
                    $dataCoti_2['termino_refacciones'] = $registro;
                    $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti_2,'idOrden',$datos["idOrden"],'identificador','E');

                    //$this->envioSMS($datos["idOrden"],$datos);
                    $temp["mensaje"]="1";
                    $this->comodinLista("OT",$temp);
                }else {
                    $temp["mensaje"]="1";
                    $this->comodinLista("OT",$temp);
                }
            }else {
                $this->setData($datos["idOrden"],$datos);
            }
        }
    }

    //Enviamos los mensajes a los numeros de refacciones
    public function sendSMSMult($idOrden = 0)
    {
        $registro = date("d-m-Y");
        //Cargamos los numeros de los usuarios
        $numeros = [];
    
        //Definimos el mensaje a enviar
        $texto = SUCURSAL.". Nueva solicitud de presupuesto (Adicional) creada. No. Orden: ".$idOrden;
    
        //Refacciones
        $this->envio_sms('3316721106',$texto);
        $this->envio_sms('3319701092',$texto);
    
        //Numero de angel (pruebas)
        $this->envio_sms('5535527547',$texto);
    
        $datos["mensaje"] = "1";
        $this->setData($idOrden,$datos);
    }

    //Actualizamos el campo correspondiente
    public function envioSMS($idOrden = '', $datos = NULL)
    {
        $registro = date("d-m-Y");
        //Verificamos si ya se envio desde refacciones
        $revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($revision as $row) {
            if ($row->identificador == "E") {
                $idCotizacion = $row->idCotizacion;
                $acepta = (string)$row->aprobacionRefaccion;
            }
        }

        //Comprobamos que exista el registro
        if (isset($acepta)) {
            //Verificamos que no se haya enviado anteriormente el mensaje
            if ($acepta == "0") {
                $dataCoti['aprobacionRefaccion'] = "1";

                $actualizar_2 = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$datos["idOrden"],'identificador','E');
                if ($actualizar_2) {
                    $mensaje_sms = SUCURSAL.". Nuevo presupuesto (Adicional) terminado por ventanilla No. Orden #".$idOrden." el día ".$registro;
                    //Enviar mensaje al asesor
                    $celular_sms = $this->mConsultas->telefono_asesor($idOrden);
                    $this->sms_general($celular_sms,$mensaje_sms);

                    //Enviamos mensaje al tecnico
                    $celular_sms_2 = $this->mConsultas->telefono_tecnico($idOrden);
                    $this->sms_general($celular_sms_2,$mensaje_sms);
                
                    //Numero de angel (pruebas)
                    $this->sms_general('5535527547',$mensaje_sms);
            
                     $temp["mensaje"]="1";
                     // $this->comodinEditaSU($datos["idOrden"],$datos);
                     $this->comodinLista("OT",$temp);
                }else {
                    $temp["mensaje"]="2";
                    // $this->comodinEditaSU($datos["idOrden"],$datos);
                    $this->comodinLista("OT",$temp);
                }
            } else{
                $temp["mensaje"] = "1";
                // $this->comodinEditaSU($datos["idOrden"],$datos);
                $this->comodinLista("OT",$temp);
            }
        }else{
            $temp["mensaje"] = "2";
            // $this->comodinEditaSU($datos["idOrden"],$datos);
            $this->comodinLista("OT",$temp);
        }    
    }

    //Enviamos el mensaje para el asesor
    public function envio_sms($celular_sms = '', $mensaje_sms = '')
    {
        $validar = FALSE;
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);
    
         //Comprobamos que se haya enviado correctamente
         if($server_output) {
            $validar = TRUE;
         }
         // var_dump($server_output);
         // echo "<br>";
         // var_dump($validar);
         // echo "<br><br>";
    
         return $validar;
    }

    //Validar Radio
    public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    //Validamos y guardamos los documentos
    public function validateDoc($nombreDoc = NULL)
    {
        $path = "";
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
        for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docMultipunto";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_";
            for($j=0; $j<=7; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }

    //Validar multiple check
    public function mergeOfString($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    if (strlen($campo[$i]) > 4) {
                        $respuesta .= $campo[$i]."|";
                    }
                }
            }
        }
        return $respuesta;
    }

    //Entrada para el PDF
    public function setDataPDF($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $this->setData($idOrden,$datos);
    }

    //Entrada para recuperar informacion para el cliente
    public function comodinCliente($idOrden = 0,$datos = NULL)
    {
        $datos["destinoVista"] = "Cliente";

        //Recuperamos la evidencia de video (si existe)
        // $revicion = $this->mConsultas->get_result("id_cita",$idOrden,"evidencia");
        // foreach ($revicion as $row){
        //     $datos["video"] = $row->video;
        //     $datos["comentario"] = $row->comentario;
        // }
        $this->setData($idOrden,$datos);
    }

    //Parada comodin para listar en refacciones
    public function comodinLista($origen = "",$datos = NULL)
    {
        $datos["indicador"] = "REF";
        $datos["destino"] = $origen;

        $this->loadDataSearch("",$datos);
    }

    //Parada comodin para listar las cotizaciones que ya fueron aceptadas/rechazdas por el cliente
    public function comodinStatusCliente($origen = "",$datos = NULL)
    {
        $datos["indicador"] = "StatusCambio";
        $datos["destino"] = $origen;

        $this->loadDataSearch("",$datos);
    }

    //Cargamos las cotizaciones generadas
    public function loadDataSearch($origen = "",$datos = NULL)
    {
        if (!isset($datos["indicador"])) {
            $datos["indicador"] = "";
        }

        if (!isset($datos["destino"])) {
            $datos["destino"] = $origen;
        }

        //Recuperamos todas las cotizaciones que se han generado en la multipunto
        $query = $this->mConsultas->get_table_desc("cotizacion_multipunto");

        foreach ($query as $row){
            if ($row->identificador == "E") {
                $datos["idOrden"][] = $row->idOrden;
                $fecha = explode(" ",$row->fech_registro);
                $fechaFracc = explode("-",$fecha[0]);
                $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
                $datos["subTotalMaterial"][] = $row->subTotal;
                $datos["ivaMaterial"][] = $row->iva;
                $datos["anticipo"][] = $row->anticipo;
                $datos["totalMaterial"][] = $row->total;
                $datos["canceladoMaterial"][] = $row->cancelado;
                $datos["firmaAsesor"][] = ($row->firmaAsesor != "") ? "SI" : "NO";
                $datos["aceptoTermino"][] = $row->aceptoTermino;
                $datos["aprobacionRef"][] = $row->aprobacionRefaccion;
            }
        }

        //Verificamos de donde vino
        if ($datos["indicador"] == "StatusCambio") {
            $this->loadAllView($datos,'cotizaciones/externa/cambio_status');
        }else {
            $this->loadAllView($datos,'cotizaciones/externa/buscador');
        }

    }

    //Recuperamos la informacion de la cotizacion
    public function setData($idOrden = 0,$datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["tipoRegistro"])) {
            $datos["tipoRegistro"] = "";
        }

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == "E") {
                $datos["registrosControl"] = $this->createCells(explode("|",$row->contenido));
                $datos["registrosControlRenglon"] = explode("|",$row->contenido);

                $datos["subTotalMaterial"] = $row->subTotal;
                $datos["ivaMaterial"] = $row->iva;
                $datos["anticipo"] = $row->anticipo;
                $datos["notaAnticipo"] = $row->notaAnticipo;
                $datos["canceladoMaterial"] = $row->cancelado;
                $datos["totalMaterial"] = $row->total;
                $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";
                $datos["archivo"] = explode("|",$row->archivos);
                $datos["archivoTxt"] = $row->archivos;
                $datos["comentario"] = $row->comentario;
                $datos["comentarioTecnico"] = $row->comentarioTecnico;
                $datos["archivoTecnico"] = explode("|",$row->archivoTecnico);
                $datos["archivoImgTecnico"] = $row->archivoTecnico;
                $datos["aceptoTermino"] = $row->aceptoTermino;
                $datos["refacciones"] = $row->aprobacionRefaccion;
            }
        }

        $datos["idOrden"] = $idOrden;
        $datos["tipoOrigen"] = "ADICIONAL";

        //Consultamos si ya se entrego la unidad
        //$datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idOrden);

        //Verificamos donde se imprimira la información
        if ($datos["destinoVista"] == "PDF") {
            $this->generatePDF($datos);
        }elseif ($datos["destinoVista"] == "Cliente") {
            $this->loadAllView($datos,'cotizaciones/externa/cliente');
        }elseif ($datos["destinoVista"] == "StatusCambio") {
            $this->loadAllView($datos,'cotizaciones/externa/cliente_status');
        }else {
            if ($datos["tipoRegistro"] == "Otros") {
                $this->loadAllView($datos,'cotizaciones/externa/cotizacion_edita_2');
            }else {
                $this->loadAllView($datos,'cotizaciones/externa/cotizacion_editar');
            }
        }
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Recibir ajax para cotizacion
    public function getAjaxForm()
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);
        // ini_set('max_input_time',"20M");

        //Verificamos que las variables hayan llegado
        if (count($_POST)>0) {
            //Recuperamos los valores
            $idOrden = $_POST["idOrden"];
            $eleccion = $_POST["eleccion"];

            //Comprobamos que no haya valores invalidos
            if (($idOrden != "0") && ($idOrden != "")) {
                //Identificamos si la cotizacion ya ha generado alguna decision
                $revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
                foreach ($revision as $row) {
                    if ($row->identificador == "E") {
                        $idCotizacion = $row->idCotizacion;
                        $acepta = $row->aceptoTermino;
                    }
                }

                //Si no se ha tomado ninguna decision previamente y la cotizacion fue cancelada
                if (($acepta == "")&&($eleccion == "No")) {
                    //Cancelada por el cliente (no se modifica el inventario)
                    // $actualiza = TRUE;
                    $actualiza = "Cancelada";
                //Si no se ha tomado ninguna decision previamente y la cotizacion fue aprobada
                }elseif (($acepta == "")&&($eleccion == "Si")) {
                    //Cotización aprobada por el cliente
                    //Actualizamos los inventarios
                    $actualiza = $this->stockUpdate($idOrden,"1");
                //Si ya se habia tomado una decision y la cotización se cancela
                }elseif (($acepta != "")&&($eleccion == "No")) {
                    //Cotización cancelada por el cliente, posteriormente a haber sido aprobada
                    //Actualizamos los inventarios
                    $actualiza = $this->stockUpdate($idOrden,"0");
                //Si ya se habia tomado una decision y la cotización se cancela
              }elseif (($acepta != "")&&($eleccion == "Si")) {
                    //Cotización aprobada por el cliente, posteriormente a haber sido cancelada
                    //Actualizamos los inventarios
                    $actualiza = $this->stockUpdate($idOrden,"1");
                }

                //Si se actualizo correctamente el inventario
                if ($actualiza) {
                    //Actualizamos el registro
                    $registro = date("Y-m-d H:i:s");
                    $dataCoti = array(
                        "aceptoTermino" => $eleccion,
                        "fech_actualiza" => $registro,
                    );
                    //Guardamos la información
                    $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$idOrden,'identificador','E');
                    if ($actualizar) {
                        echo "OK";
                        $this->loadHistory($idOrden,"Actualización Cotización: ".$eleccion,"Cotización");
                        //Enviamos el mensaje al de refacciones
                        // $this->smsData($idOrden,$eleccion);
                    }else {
                        echo "Error 03";
                    }
                }else {
                    echo "Error 03";
                }
            }else {
                echo "Error 02";
            }
        }else {
            echo "Error 01";
        }
    }

    //Actualizamos el inventario de refacciones
    public function stockUpdate($idOrden = '',$modoActualiza = "0")
    {
        //Definimos valor default de retorno
        $retorno = FALSE;
        $existencia = 0;

        //Recuperamos la informacion de la cotizacion
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == "E") {
                $registros = $this->createCells(explode("|",$row->contenido));
            }
        }

        //Comprobamos los registro
        if (isset($registros)) {
            foreach ($registros as $index => $value) {
                $cantidad[] = $registros[$index][0];
                $numPieza[] = $registros[$index][6];
            }

            $registro = date("Y-m-d H:i:s");
            //Creamos ciclo para abarcar todos los capos recuperados
            for ($i=0; $i <count($cantidad) ; $i++) {
                //Buscamos la refacción en el inventario para actualizarlo
                $inventario = $this->mConsultas->get_result("clave_comprimida",$numPieza[$i],"refacciones");
                foreach ($inventario as $row) {
                    $existencia = $row->existencia;
                }

                //Verificamos si la refaccion va de salida o va aingresar al  inventario
                if ($modoActualiza == "1") {
                    //Sacamos del invetario actual
                    $nvoStock = $existencia - $cantidad[$i];
                }else {
                    //Regresamos al inventario
                    $nvoStock = $existencia + $cantidad[$i];
                }

                //Actualizamos el inventario
                $dataStock = array(
                    "existencia" => $nvoStock,
                    "fecha_actualiza" => $registro,
                );
                //Guardamos la información
                $actualizar = $this->mConsultas->update_table_row('refacciones',$dataStock,'clave_comprimida',$numPieza[$i]);

            }

            $retorno = TRUE;
        }

        return $retorno;
    }

    //Armamos el paquete de datos
    public function smsData($idOrden = 0, $estatus = "No")
    {
        $registro = date("d-m-Y");
            if ($estatus == "Si") {
            $decision = "Aceptada";
        }elseif ($estatus == "Afectada") {
            $decision = "Afectada";
        }else {
            $decision = "Rechazada";
        }

        $mensaje_sms = SUCURSAL.". La cotización (Adicional) de la orden #".$idOrden." ha sido ".$decision." por el usuario el día ".$registro;

        //Enviar mensaje al asesor
        $celular_sms = $this->mConsultas->telefono_asesor($idOrden);
        $this->sms_general($celular_sms,$mensaje_sms);

        //Enviamos mensaje al tecnico
        $celular_sms_2 = $this->mConsultas->telefono_tecnico($idOrden);
        $this->sms_general($celular_sms_2,$mensaje_sms);

        //Refacciones
        $this->sms_general('3316721106',$mensaje_sms);
        $this->sms_general('3319701092',$mensaje_sms);
        
        //Numero de angel (pruebas)
        $this->sms_general('5535527547',$mensaje_sms);
    }

    //Enviamos el mensaje para el asesor
    public function sms_general($celular_sms='',$mensaje_sms=''){
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    //Peticion ajax para recuperar filtros
    public function filtrosRef()
    {
        //Recuperamos la informacion para filtrar
        $valorFiltro = $_POST['valor_1'];
        $valorFiltro_ant = $_POST['valor_2'];

        $columna = $_POST['dato_1'];
        $columna_ant = $_POST['dato_2'];

        $cadena = "";
        $precarga2 = $this->mConsultas->filtro_Sec($columna,'clave_comprimida',$columna_ant,'refacciones',$valorFiltro,$valorFiltro_ant);
        foreach ($precarga2 as $row) {
            $cadena .= $row->$columna."|";
        }
        echo $cadena;
    }

    //Recuperar final de filtros de refacciones
    public function filtrosRefFinal()
    {
        //Recuperamos la informacion para filtrar
        $valorFiltro = $_POST['dato'];
        $pivote = $_POST['pivote'];

        $cadena = "";
        $final = $this->mConsultas->get_result($pivote,$valorFiltro,"refacciones");
        foreach ($final as $row) {
            $cadena .= $row->descripcion."|";
            $cadena .= $row->fue."|";
            $cadena .= $row->cla."|";
            $cadena .= $row->p_lista."|";
            $cadena .= $row->existencia."|";
            $cadena .= $row->ubicacion."|";
            $cadena .= $row->gpo."|";
            $cadena .= $row->prefijo."|";
            $cadena .= $row->basico."|";
            $cadena .= $row->sufijo."|";
        }
        echo $cadena;
    }

    //Recuperar filtro de refacciones por descripcion
    public function filtrosDescripcion()
    {
        //Recuperamos la informacion para filtrar
        $valorFiltro = $_POST['valor_1'];
        $columna = $_POST['dato_1'];

        $valor_limpio = str_replace(".","",$valorFiltro);
        $cadena = "";

        $precarga2 = $this->mConsultas->filtro_descripcion($columna,$valor_limpio,'refacciones');
        foreach ($precarga2 as $row) {
            $cadena .= "(".$row->clave_comprimida.")  ".$row->descripcion."_";
        }
        echo $cadena;
    }

    //Recuperamos la informacion de la cotizacion (solo aceptadas)
    public function extraQuote($idOrden  = "")
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];

        try {
            // $idOrden = $_POST["idOrden"];

            $contenedor = [];
            //Recuperamos la información de la cotizacion
            $revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
            foreach ($revision as $row) {
                if ($row->identificador == "E") {
                    $idCotizacion = $row->idCotizacion;
                    $acepta = $row->aceptoTermino;
                    $cuerpo = $this->createCells(explode("|",$row->contenido));
                    $subtotal = $row->subTotal;
                    $iva = $row->iva;
                    $cancelado = $row->cancelado;
                    $total = $row->total;
                    $comentario = $row->comentario;
                }
            }

            //Comprobamos que se haya cargado
            if (isset($idCotizacion)) {
                //Verificamos la respuesta afirmativa
                if (($acepta == "Si")||($acepta == "Val")) {
                    $articulos = [];
                    //Comprobamos si la cotizacion se acepto completa o por partes
                    if ($acepta == "Si") {
                        //Recuperamos los elementos de la cotización
                        foreach ($cuerpo as $index => $valor) {
                            $articulos["cantidad"] = $cuerpo[$index][0];
                            $articulos["descripcion"] = $cuerpo[$index][1];
                            $articulos["pieza"] = $cuerpo[$index][6];
                            $articulos["costo"] = $cuerpo[$index][2];

                            $contenedor["cotizacion"][] = $articulos;
                        }
                    }else {
                        //Recuperamos los elementos de la cotización
                        foreach ($cuerpo as $index => $valor) {
                            if ($cuerpo[$index][9] == "Aceptada") {
                                $articulos["cantidad"] = $cuerpo[$index][0];
                                $articulos["descripcion"] = $cuerpo[$index][1];
                                $articulos["pieza"] = $cuerpo[$index][6];
                                $articulos["costo"] = $cuerpo[$index][2];

                                $contenedor["cotizacion"][] = $articulos;
                            }
                        }
                    }

                    $contenedor["respuesta"] = "OK";
                }else {
                    $contenedor["cotizacion"] = array();
                    $contenedor["respuesta"] = "Cotización Rechazada";
                }
            } else {
                $contenedor["cotizacion"] = array();
                $contenedor["respuesta"] = "ERROR";
            }
        } catch (\Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    //Validamos las credenciales del usuario para anticipo refacciones
    public function usuarioAnticipo()
    {
        //Recuperamos la informacion para filtrar
        $usuario = $_POST['user'];
        $pass = $_POST['pass'];

        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();
        if (isset($row)){
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;

            //Verificamos que el usuario sea correcto
            if ($idUsuario == 28) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    $retorno = "OK";
                }else {
                    //Contraseña sin encriptar
                    if ($pass == $passDB ) {
                        $retorno = "OK";
                    } else {
                      $retorno = "CONTRASEÑA INCORRECTA";
                    }
                }
            }else {
                $retorno = "USUARIO SIN PERMISOS";
            }

        }else {
            $retorno = "NO EXISTE EL USUARIO";
        }

        echo $retorno;
    }

    //Guardamos el anticipo en la base de datos
    public function agregarAnticipo()
    {
        $cantidad = $_POST['cantidad'];
        $comentario = $_POST['comentario'];
        $idCoti = $_POST['idCoti'];
        $total = $_POST['total'];

        //Comprobamos los datos
        if (($cantidad != "") && ($idCoti != "")) {
            $dataCoti['anticipo'] = $cantidad;
            $dataCoti['total'] = $total;
            $dataCoti['notaAnticipo'] = $comentario;

            $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$idCoti,'identificador','E');
            if ($actualizar) {
                $this->loadHistory($idCoti,"Se anexa anticipo a la cotización por: ".$cantidad,"Cotización");
                $retorno = "OK";
            }else {
                $retorno = "ERROR AL GUARDAR LA INFORMACIÓN";
            }
        }else {
            $retorno = "VERIFICAR INFORMACIÓN ENVIADA";
        }

        echo $retorno;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("cotizaciones/plantillaPdf", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                // 'assets/js/Multipunto/multipuntoJs.js',
                'assets/js/firmasMoviles/firmas_HM.js',
                'assets/js/materiales/externo/presupuestoMulti.js',
                'assets/js/materiales/externo/autosuma.js',
                'assets/js/materiales/externo/anexos.js',
                //'assets/js/materiales/reglasVista.js',
                // 'assets/js/superUsuario.js',
                'assets/js/materiales/externo/anexos_2.js',
                'assets/js/materiales/externo/anexos_3.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Cotización"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
