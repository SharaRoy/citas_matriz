<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndicadoresProactivo extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);

        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);
    }

    public function index()
    {
      // code..
    }

    public function lista($valor = "")
    {
        //Cargamos la información del formulario
        $mes_actual = date("m");
        $fecha_busqueda = (($mes_actual <= 1) ? (date("Y")-1) : date("Y"))."-".(($mes_actual <= 1) ? "12" : ($mes_actual -1))."-20";
        $query = $this->mConsultas->indicadores_multipunto($fecha_busqueda);

        foreach ($query as $row){
            $datos["id_cita"][] = $row->id_cita;
            $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
            $datos["id_proactivo"][] = $row->id_proactivo;
            $datos["folio"][] = $row->folio;
            
            $fecha = new DateTime($row->fecha_recepcion);
            $datos["fecha_recepcion"][] = $fecha->format('d-m-Y');

            $datos["cliente"][] = $row->cliente;
            $datos["telefono"][] = $row->telefono;
            $datos["asesor"][] = $row->asesor;
            $datos["tecnico"][] = $row->tecnico;
            //$datos["modelo"][] = $row->modelo;
            //$datos["placas"][] = $row->placas;
            $datos["serie"][] = $row->serie;
            
            $datos["bateria"][] = (($row->bateriaEstado == "V") ? "#c7ecc7" : (($row->bateriaEstado == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["neumaticoFI"][] = (($row->dNeumaticoFI == "V") ? "#c7ecc7" : (($row->dNeumaticoFI == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["balatasFI"][] = (($row->espesoBalataFI == "V") ? "#c7ecc7" : (($row->espesoBalataFI == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["discoFI"][] = (($row->espesorDiscoFI == "V") ? "#c7ecc7" : (($row->espesorDiscoFI == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["neumaticoTI"][] = (($row->dNeumaticoTI == "V") ? "#c7ecc7" : (($row->dNeumaticoTI == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["balatasTI"][] = (($row->espesoBalataTI == "V") ? "#c7ecc7" : (($row->espesoBalataTI == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["discoTI"][] = (($row->espesorDiscoTI == "V") ? "#c7ecc7" : (($row->espesorDiscoTI == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["tamborTI"][] = (($row->diametroTamborTI == "V") ? "#c7ecc7" : (($row->diametroTamborTI == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["neumaticoFD"][] = (($row->dNeumaticoFD == "V") ? "#c7ecc7" : (($row->dNeumaticoFD == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["balatasFD"][] = (($row->espesoBalataFD == "V") ? "#c7ecc7" : (($row->espesoBalataFD == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["discoFD"][] = (($row->espesorDiscoFD == "V") ? "#c7ecc7" : (($row->espesorDiscoFD == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["neumaticoTD"][] = (($row->dNeumaticoTD == "V") ? "#c7ecc7" : (($row->dNeumaticoTD == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["balatasTD"][] = (($row->espesoBalataTD == "V") ? "#c7ecc7" : (($row->espesoBalataTD == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["discoTD"][] = (($row->espesorDiscoTD == "V") ? "#c7ecc7" : (($row->espesorDiscoTD == "A") ? "#f5ff51" : "#f5acaa"));
            $datos["tamborTD"][] = (($row->diametroTamborTD == "V") ? "#c7ecc7" : (($row->diametroTamborTD == "A") ? "#f5ff51" : "#f5acaa"));

            $datos["bateriaCambio"][] = $row->bateriaCambio;
            $datos["dNeumaticoFIcambio"][] = $row->dNeumaticoFIcambio;
            $datos["espesorBalataFIcambio"][] = $row->espesorBalataFIcambio;
            $datos["espesorDiscoFIcambio"][] = $row->espesorDiscoFIcambio;
            $datos["dNeumaticoTIcambio"][] = $row->dNeumaticoTIcambio;
            $datos["espesorBalataTIcambio"][] = $row->espesorBalataTIcambio;
            $datos["espesorDiscoTIcambio"][] = $row->espesorDiscoTIcambio;
            $datos["diametroTamborTIcambio"][] = $row->diametroTamborTIcambio;
            $datos["dNeumaticoFDcambio"][] = $row->dNeumaticoFDcambio;
            $datos["espesorBalataFDcambio"][] = $row->espesorBalataFDcambio;
            $datos["espesorDiscoFDcambio"][] = $row->espesorDiscoFDcambio;
            $datos["dNeumaticoTDcambio"][] = $row->dNeumaticoTDcambio;
            $datos["espesorBalataTDcambio"][] = $row->espesorBalataTDcambio;
            $datos["espesorDiscoTDcambio"][] = $row->espesorDiscoTDcambio;
            $datos["diametroTamborTDcambio"][] = $row->diametroTamborTDcambio;
        }

        $this->loadAllView($datos,'modulosExternos/Proactivo/indicadores_multipunto');
    }

    public function comentarioMagic()
    {
        //Recuperamos los campos del formulario
        $idHistorial = $_POST["idHistorial"];
        $titulo = $_POST["titulo"];
        $hora = $_POST["hora"];
        $fecha = $_POST["fecha"];
        $comentario = $_POST["comentario"];
        $respuesta = "";

        //Guardamos la informacion en el historial
        $contenedor = array(
            'comentario' => $comentario,
            'id_cita' => $idHistorial,
            'fecha_creacion' => date('Y-m-d H:i:s'),
            'id_usuario'=> ($this->session->userdata('id_usuario')) ? $this->session->userdata('id_usuario') : NULL,
            'fecha_notificacion' => $fecha." ".$hora,
        );

        $idComentario = $this->mConsultas->save_register('historial_citas_llamadas_magic',$contenedor);
        if ($idComentario != 0) {
            //Insertar notificación
            if(($fecha != '') && ($fecha != 'cronoHora')){
                $notific['titulo'] = $titulo;
                $notific['texto']= $comentario;
                $notific['id_user'] = ($this->session->userdata('id_usuario')) ? $this->session->userdata('id_usuario') : NULL;
                $notific['estado'] = 1;
                $notific['tipo_notificacion'] = 2;
                $notific['fecha_hora'] = $fecha.' '.$hora;
                //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
                $notific['estadoWeb'] = 1;

                $idNotifi = $this->Verificacion->save_register('noti_user',$notific);
                if ($idNotifi != 0) {
                    $respuesta = "OK";
                }else {
                    $respuesta = "ERROR 02";
                }
            }else {
                $respuesta = "OK";
            }
        }else {
            $respuesta = "ERROR 01";
        }

        echo $respuesta;
    }

    //Hacemos la busqueda de indicadores multipunto
    public function buscador_multipunto($value='')
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $campo = $_POST['campo'];

        //Si no se envio un campo de consulta
        if ($campo == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->indicadores_multipunto_fecha($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->indicadores_multipunto_fecha2($fecha_inicio." 00:00:00",$fecha_fin." 23:59:59");
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->indicadores_multipunto_campo($campo);
            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->indicadores_multipunto_campo_fecha2($campo,$fecha_inicio." 00:00:00",$fecha_fin." 23:59:59");
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->indicadores_multipunto_campo_fecha($campo,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->indicadores_multipunto_campo_fecha($campo,$fecha_fin);
                }
            }
        }
        
        $cadena = "";

        foreach ($precarga as $row) {
            $cadena .= $row->id_cita."=";
            $cadena .= $row->folio."=";

            $fecha = new DateTime($row->fecha_recepcion);
            $cadena .= $fecha->format('d-m-Y')."=";

            $cadena .= $row->serie."=";
            $cadena .= $row->asesor."=";
            $cadena .= $row->tecnico."=";
            $cadena .= $row->cliente."=";
            $cadena .= $row->telefono."=";

            $cadena .= $row->id_proactivo."=";

            $cadena .= $row->bateriaEstado."="; //9
            $cadena .= $row->dNeumaticoFI."=";
            $cadena .= $row->espesoBalataFI."=";
            $cadena .= $row->espesorDiscoFI."=";
            $cadena .= $row->dNeumaticoTI."=";
            $cadena .= $row->espesoBalataTI."=";
            $cadena .= $row->espesorDiscoTI."=";
            $cadena .= $row->diametroTamborTI."=";
            $cadena .= $row->dNeumaticoFD."=";
            $cadena .= $row->espesoBalataFD."=";
            $cadena .= $row->espesorDiscoFD."="; //19
            $cadena .= $row->dNeumaticoTD."=";
            $cadena .= $row->espesoBalataTD."=";
            $cadena .= $row->espesorDiscoTD."=";
            $cadena .= $row->diametroTamborTD."=";

            $cadena .= $row->bateriaCambio."="; //24
            $cadena .= $row->dNeumaticoFIcambio."=";
            $cadena .= $row->espesorBalataFIcambio."=";
            $cadena .= $row->espesorDiscoFIcambio."=";
            $cadena .= $row->dNeumaticoTIcambio."=";
            $cadena .= $row->espesorBalataTIcambio."=";
            $cadena .= $row->espesorDiscoTIcambio."=";
            $cadena .= $row->diametroTamborTIcambio."=";
            $cadena .= $row->dNeumaticoFDcambio."=";
            $cadena .= $row->espesorBalataFDcambio."=";
            $cadena .= $row->espesorDiscoFDcambio."="; //34
            $cadena .= $row->dNeumaticoTDcambio."=";
            $cadena .= $row->espesorBalataTDcambio."=";
            $cadena .= $row->espesorDiscoTDcambio."=";
            $cadena .= $row->diametroTamborTDcambio."=";

            $cadena .= $this->encrypt($row->id_cita)."="; //39

            $cadena .= "|";
        }

        echo $cadena;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrosModulos/indicadores/buscador_multipunto.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Indicadores Proactivo"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}