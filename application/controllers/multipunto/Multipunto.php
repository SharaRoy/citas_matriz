<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

require_once APPPATH . '/third_party/PHPMailer/src/Exception.php';
require_once APPPATH . '/third_party/PHPMailer/src/PHPMailer.php';
require_once APPPATH . '/third_party/PHPMailer/src/SMTP.php';

class Multipunto extends CI_Controller { 

    public function __construct()
    {
        parent::__construct();
        //Cargamos los modelos para las consultas
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        //Cargamos la libreria de email
        //$this->load->library('email');
        date_default_timezone_set(TIMEZONE_SUCURSAL);

        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);
    }

  	public function index($idCita = 0,$datos = NULL)
  	{
        $datos["hoy"] = date("Y-m-d");
        $datos["limiteAno"] = date("Y");

        $id_cita = ((ctype_digit($idCita)) ? $idCita : $this->decrypt($idCita));
        $datos["idCita"] = $id_cita;

        if ($this->session->userdata('rolIniciado')) {
            $datos["dirige"] = $this->session->userdata('rolIniciado');
            $datos["asesor"] = $this->session->userdata('nombreUsuario');
        }else{
            // $datos["dirige"] = "GENERICO";
            $datos["dirige"] = "";
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        //Verificamos si la unidad ha pasado por trabajando
        $datos["unidadTrabajando"] = $this->mConsultas->unidadTrabajando($id_cita);

        //Cargamos el tipo de servicio de la orden
        $datos["tipoServicio"] = $this->mConsultas->tipo_servicio($datos["idCita"]);

  		$this->loadAllView($datos,'multipunto/multipunto_alta');
  	}

    //Recuperamos la información de la vistas
    public function validateView()
    {
        $datos["dirige"] = $this->input->post("dirige");

        $datos["tipoRegistro"] = $this->input->post("cargaFormulario");
        $datos["modalidadFormulario"] = $datos["tipoRegistro"];
        //Dato de busqueda/consulta en base de datos
        $datos["idCita"] = $this->input->post("idCita");
        //Cargamos el tipo de servicio de la orden
        $datos["tipoServicio"] = $this->mConsultas->tipo_servicio($datos["idCita"]);
        //Dato para guardar registro
        $datos["idOrden"] = $this->input->post("idOrden");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idCita"]);

        //Validamos si el formulario es de alta por un asesor o por un tecnico
        //Para el asesor solo su parte es obligatoria
        if ($datos["tipoRegistro"] == "1") {
            //Comprobamos que el no de Orden no exista
            $this->form_validation->set_rules('orden', 'No. de Orden', 'required|callback_existencia|callback_ordenPrevia');
            $this->form_validation->set_rules('fechaCaptura', 'La fecha es requerido', 'required');
            $this->form_validation->set_rules('noSerie', 'El numero de Orden es requerido', 'required');
            $this->form_validation->set_rules('modelo', 'El modelo del vehículo es requerido', 'required');
            $this->form_validation->set_rules('torre', 'La torre es requerido', 'required');
            $this->form_validation->set_rules('nombreCarro', 'El nombre es requerido', 'required');

            $this->form_validation->set_rules('asesorNombre', 'Nombre del asesor', 'required');
            $this->form_validation->set_rules('rutaFirmaAsesor', 'Firma del asesor', 'required');
        //Para el tecnico todo el formulario es obligatorio
        }elseif ($datos["tipoRegistro"] == "1A") {
            //Comprobamos que el no de Orden no exista
            $this->form_validation->set_rules('orden', 'No. de Orden', 'required|callback_existencia|callback_ordenPrevia');

            $this->form_validation->set_rules('fechaCaptura', 'La fecha es requerido', 'required');
            $this->form_validation->set_rules('noSerie', 'El numero de Orden es requerido', 'required');
            $this->form_validation->set_rules('modelo', 'El modelo del vehículo es requerido', 'required');
            $this->form_validation->set_rules('torre', 'La torre es requerido', 'required');
            $this->form_validation->set_rules('nombreCarro', 'El nombre es requerido', 'required');
            //$this->form_validation->set_rules('email', 'Correo electrónico', 'valid_email');

            //Validamos checks
            $this->form_validation->set_rules('perdidaAceite', '', 'callback_checks');
            $this->form_validation->set_rules('aceiteCheck', '', 'callback_checks');
            $this->form_validation->set_rules('direccionCheck', '', 'callback_checks');
            $this->form_validation->set_rules('transmisionCheck', '', 'callback_checks');
            $this->form_validation->set_rules('liquidoFrenosCheck', '', 'callback_checks');
            $this->form_validation->set_rules('liquidoParabriCheck', '', 'callback_checks');
            $this->form_validation->set_rules('refrigeranteCheck', '', 'callback_checks');

            $this->form_validation->set_rules('pruebaparabrisas', '', 'callback_checks');
            $this->form_validation->set_rules('plumas', '', 'callback_checks');
            $this->form_validation->set_rules('luces', '', 'callback_checks');
            $this->form_validation->set_rules('parabrisas', '', 'callback_checks');

            $this->form_validation->set_rules('valorBateria', 'Falta inicar', 'required');
            $this->form_validation->set_rules('bateriaRadio', '', 'callback_checks');
            $this->form_validation->set_rules('ccafabrica', 'Falta indicar', 'required');
            $this->form_validation->set_rules('ccareal', 'Falta indicar', 'required');

            $this->form_validation->set_rules('sisCalefaccion', '', 'callback_checks');
            $this->form_validation->set_rules('radiadorRario', '', 'callback_checks');
            $this->form_validation->set_rules('bandasRadio', '', 'callback_checks');
            $this->form_validation->set_rules('frenosRadio', '', 'callback_checks');
            $this->form_validation->set_rules('amortiguadoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('varillajeRadio', '', 'callback_checks');
            $this->form_validation->set_rules('escapeRadio', '', 'callback_checks');
            $this->form_validation->set_rules('embragueRadio', '', 'callback_checks');
            $this->form_validation->set_rules('cardanRadio', '', 'callback_checks');
            //Frente izquierdo
            $this->form_validation->set_rules('profNeumaticoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('profNeumatico', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('patronDegasteRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionPSIFI', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('balatasRadio', '', 'callback_checks');
            $this->form_validation->set_rules('balatas', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('discoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('discoI', 'Falta Indicar', 'required');
            //Parte trasera izquierda
            $this->form_validation->set_rules('profundidadDibujoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('profundidadDibujo', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('desgasteRadio', '', 'callback_checks');
            $this->form_validation->set_rules('infladoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionPSITI', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('espesorBalataRadio', '', 'callback_checks');
            $this->form_validation->set_rules('espesorBalata', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('espesorDiscoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('espesorDisco', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('diametroRadio', '', 'callback_checks');
            $this->form_validation->set_rules('diametro', 'Falta Indicar', 'required');
            //Frente derecho
            $this->form_validation->set_rules('profNeumaticoDerechoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('profNeumaticoDerecho', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('desgasteDerRadio', '', 'callback_checks');
            $this->form_validation->set_rules('infladoDerRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionPSIFD', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('balatasDerRadio', '', 'callback_checks');
            $this->form_validation->set_rules('balatasDerc', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('discoDercRadio', '', 'callback_checks');
            $this->form_validation->set_rules('discoDerch', 'Falta Indicar', 'required');
            //Parte trasera derecha
            $this->form_validation->set_rules('dibujoDerecRadio', '', 'callback_checks');
            $this->form_validation->set_rules('dibujoDerech', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('patronDesgDerchRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionDerchRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionPSITD', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('balatasEspesorRadio', '', 'callback_checks');
            $this->form_validation->set_rules('balatasEspesor', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('espesorDisco2Radio', '', 'callback_checks');
            $this->form_validation->set_rules('espesorDisco2', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('diametroTamborRadio', '', 'callback_checks');
            $this->form_validation->set_rules('diametroTambor', 'Falta Indicar', 'required');

            $this->form_validation->set_rules('infladoPresionRadio', '', 'callback_checks');
            $this->form_validation->set_rules('infladoPresion', 'Falta Indicar', 'required');

            $this->form_validation->set_rules('system', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('component', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('raiz', 'Falta Indicar', 'required');

            $this->form_validation->set_rules('rutaFirmaTecnico', 'Firma del Técnico', 'required');
            $this->form_validation->set_rules('nombreTecnico', 'Nombre del Técnico', 'required');
        //Fin de las altas
        //Validamos si el formulario es de edicion por un tecnico o por un jefe de servicio
        //Para el tecnico que edita una multipunto ya creada por el asesor
        }elseif ($datos["tipoRegistro"] == "2")  {
            //Comprobamos que el no de Orden no exista
            $this->form_validation->set_rules('orden', 'No. de Orden', 'required');

            $this->form_validation->set_rules('fechaCaptura', 'La fecha es requerido', 'required');
            $this->form_validation->set_rules('noSerie', 'El numero de Orden es requerido', 'required');
            $this->form_validation->set_rules('modelo', 'El modelo del vehículo es requerido', 'required');
            $this->form_validation->set_rules('torre', 'La torre es requerido', 'required');
            $this->form_validation->set_rules('nombreCarro', 'El nombre es requerido', 'required');
            //$this->form_validation->set_rules('email', 'Correo electrónico', 'valid_email');

            //Validamos checks
            $this->form_validation->set_rules('perdidaAceite', '', 'callback_checks');
            $this->form_validation->set_rules('aceiteCheck', '', 'callback_checks');
            $this->form_validation->set_rules('direccionCheck', '', 'callback_checks');
            $this->form_validation->set_rules('transmisionCheck', '', 'callback_checks');
            $this->form_validation->set_rules('liquidoFrenosCheck', '', 'callback_checks');
            $this->form_validation->set_rules('liquidoParabriCheck', '', 'callback_checks');
            $this->form_validation->set_rules('refrigeranteCheck', '', 'callback_checks');

            $this->form_validation->set_rules('pruebaparabrisas', '', 'callback_checks');
            $this->form_validation->set_rules('plumas', '', 'callback_checks');
            $this->form_validation->set_rules('luces', '', 'callback_checks');
            $this->form_validation->set_rules('parabrisas', '', 'callback_checks');

            $this->form_validation->set_rules('valorBateria', 'Falta inicar', 'required');
            $this->form_validation->set_rules('bateriaRadio', '', 'callback_checks');
            $this->form_validation->set_rules('ccafabrica', 'Falta indicar', 'required');
            $this->form_validation->set_rules('ccareal', 'Falta indicar', 'required');

            $this->form_validation->set_rules('rutaFirmaTecnico', 'Firma del Técnico', 'required');
            //$this->form_validation->set_rules('nombreTecnico', 'Nombre del Técnico', 'required');
        //
        }elseif (($datos["tipoRegistro"] == "2B")||($datos["dirige"] == "TEC")) {
            //Comprobamos que el no de Orden no exista
            $this->form_validation->set_rules('orden', 'No. de Orden', 'required');

            //Validamos checks
            $this->form_validation->set_rules('perdidaAceite', '', 'callback_checks');

            $this->form_validation->set_rules('aceiteCheck', '', 'callback_checks');
            $this->form_validation->set_rules('direccionCheck', '', 'callback_checks');
            $this->form_validation->set_rules('transmisionCheck', '', 'callback_checks');
            $this->form_validation->set_rules('liquidoFrenosCheck', '', 'callback_checks');
            $this->form_validation->set_rules('liquidoParabriCheck', '', 'callback_checks');
            $this->form_validation->set_rules('refrigeranteCheck', '', 'callback_checks');

            $this->form_validation->set_rules('pruebaparabrisas', '', 'callback_checks');
            $this->form_validation->set_rules('plumas', '', 'callback_checks');
            $this->form_validation->set_rules('luces', '', 'callback_checks');
            $this->form_validation->set_rules('parabrisas', '', 'callback_checks');

            $this->form_validation->set_rules('valorBateria', 'Falta inicar', 'required');
            $this->form_validation->set_rules('bateriaRadio', '', 'callback_checks');
            $this->form_validation->set_rules('ccafabrica', 'Falta indicar', 'required');
            $this->form_validation->set_rules('ccareal', 'Falta indicar', 'required');

            $this->form_validation->set_rules('sisCalefaccion', '', 'callback_checks');
            $this->form_validation->set_rules('radiadorRario', '', 'callback_checks');
            $this->form_validation->set_rules('bandasRadio', '', 'callback_checks');
            $this->form_validation->set_rules('frenosRadio', '', 'callback_checks');
            $this->form_validation->set_rules('amortiguadoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('varillajeRadio', '', 'callback_checks');
            $this->form_validation->set_rules('escapeRadio', '', 'callback_checks');
            $this->form_validation->set_rules('embragueRadio', '', 'callback_checks');
            $this->form_validation->set_rules('cardanRadio', '', 'callback_checks');
            //Frente izquierdo
            $this->form_validation->set_rules('profNeumaticoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('profNeumatico', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('patronDegasteRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionPSIFI', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('balatasRadio', '', 'callback_checks');
            $this->form_validation->set_rules('balatas', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('discoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('discoI', 'Falta Indicar', 'required');
            //Parte trasera izquierda
            $this->form_validation->set_rules('profundidadDibujoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('profundidadDibujo', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('desgasteRadio', '', 'callback_checks');
            $this->form_validation->set_rules('infladoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionPSITI', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('espesorBalataRadio', '', 'callback_checks');
            $this->form_validation->set_rules('espesorBalata', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('espesorDiscoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('espesorDisco', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('diametroRadio', '', 'callback_checks');
            $this->form_validation->set_rules('diametro', 'Falta Indicar', 'required');
            //Frente derecho
            $this->form_validation->set_rules('profNeumaticoDerechoRadio', '', 'callback_checks');
            $this->form_validation->set_rules('profNeumaticoDerecho', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('desgasteDerRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionPSIFD', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('infladoDerRadio', '', 'callback_checks');
            $this->form_validation->set_rules('balatasDerRadio', '', 'callback_checks');
            $this->form_validation->set_rules('balatasDerc', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('discoDercRadio', '', 'callback_checks');
            $this->form_validation->set_rules('discoDerch', 'Falta Indicar', 'required');
            //Parte trasera derecha
            $this->form_validation->set_rules('dibujoDerecRadio', '', 'callback_checks');
            $this->form_validation->set_rules('dibujoDerech', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('patronDesgDerchRadio', '', 'callback_checks');
            $this->form_validation->set_rules('presionPSITD', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('presionDerchRadio', '', 'callback_checks');
            $this->form_validation->set_rules('balatasEspesorRadio', '', 'callback_checks');
            $this->form_validation->set_rules('balatasEspesor', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('espesorDisco2Radio', '', 'callback_checks');
            $this->form_validation->set_rules('espesorDisco2', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('diametroTamborRadio', '', 'callback_checks');
            $this->form_validation->set_rules('diametroTambor', 'Falta Indicar', 'required');

            $this->form_validation->set_rules('infladoPresionRadio', '', 'callback_checks');
            $this->form_validation->set_rules('infladoPresion', 'Falta Indicar', 'required');

            $this->form_validation->set_rules('system', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('component', 'Falta Indicar', 'required');
            $this->form_validation->set_rules('raiz', 'Falta Indicar', 'required');

            $this->form_validation->set_rules('rutaFirmaTecnico', 'Firma del Técnico', 'required');
            //$this->form_validation->set_rules('nombreTecnico', 'Nombre del Técnico', 'required');
        //Si el que edita es el Asesor
        }elseif ($datos["tipoRegistro"] == "3A") {
            //Comprobamos que el no de Orden no exista
            $this->form_validation->set_rules('orden', 'No. de Orden', 'required');
            $this->form_validation->set_rules('fechaCaptura', 'La fecha es requerido', 'required');
            $this->form_validation->set_rules('noSerie', 'El numero de Orden es requerido', 'required');
            $this->form_validation->set_rules('modelo', 'El modelo del vehículo es requerido', 'required');
            $this->form_validation->set_rules('torre', 'La torre es requerido', 'required');
            $this->form_validation->set_rules('nombreCarro', 'El nombre es requerido', 'required');
            //$this->form_validation->set_rules('email', 'Correo electrónico', 'valid_email');
            //Validamos checks
            $this->form_validation->set_rules('perdidaAceite', '', 'callback_checks');

            $this->form_validation->set_rules('aceiteCheck', '', 'callback_checks');
            $this->form_validation->set_rules('direccionCheck', '', 'callback_checks');
            $this->form_validation->set_rules('transmisionCheck', '', 'callback_checks');
            $this->form_validation->set_rules('liquidoFrenosCheck', '', 'callback_checks');
            $this->form_validation->set_rules('liquidoParabriCheck', '', 'callback_checks');
            $this->form_validation->set_rules('refrigeranteCheck', '', 'callback_checks');

            $this->form_validation->set_rules('pruebaparabrisas', '', 'callback_checks');
            $this->form_validation->set_rules('plumas', '', 'callback_checks');
            $this->form_validation->set_rules('luces', '', 'callback_checks');
            $this->form_validation->set_rules('parabrisas', '', 'callback_checks');

            $this->form_validation->set_rules('valorBateria', 'Falta inicar', 'required');
            $this->form_validation->set_rules('bateriaRadio', '', 'callback_checks');
            $this->form_validation->set_rules('ccafabrica', 'Falta indicar', 'required');
            $this->form_validation->set_rules('ccareal', 'Falta indicar', 'required');

            $this->form_validation->set_rules('asesorNombre', 'Nombre del asesor', 'required');
            $this->form_validation->set_rules('rutaFirmaAsesor', 'Firma del asesor', 'required');
        //Si el que edita es el jefe de taller
        }elseif ($datos["tipoRegistro"] == "3") {
            if ($this->input->post("JefeTaller_firma_2") == "0") {
                $this->form_validation->set_rules('JefeTallerNombre', 'Nombre del jefe de taller', 'required');
                $this->form_validation->set_rules('rutaFirmaJefeTaller', 'Firma del jefe de taller', 'required');
            }else {
                $this->form_validation->set_rules('nombreConsumidor', 'Nombre del cliente', 'required');
                $this->form_validation->set_rules('rutaFirmaConsumidor', 'Firma del cliente', 'required');
            }
        }else{
            $this->form_validation->set_rules('orden', 'No. de Orden', 'required');
        }

        $this->form_validation->set_rules('orden', '', 'required|callback_altaTecnico');

        //Mensajes de validaciones
        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('valid_email','Proporcione un correo electrónico valido');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar.');
        $this->form_validation->set_message('checks','Falta indicar');
        $this->form_validation->set_message('altaTecnico','LA MULTIPUNTO DEBE SER ABIERTA POR EL ASESOR');
        $this->form_validation->set_message('ordenPrevia','La orden de servicio no existe o esta incompleta.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getFormPart1($datos);
        }else{
            $datos["mensaje"]="0";
            if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
                $this->index($datos["idCita"],$datos);
            }else {
                $this->setData($datos["idCita"],$datos);
            }

        }
    }

    public function altaTecnico()
    {
        $respuesta = TRUE;
        $formulario = $this->input->post("cargaFormulario");

        if ($formulario == "1A") {
            $respuesta = FALSE;
        }

        return $respuesta;
    }

    //Validamos si existe un una orden de servicio previa
    public function ordenPrevia()
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("orden");

        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"ordenservicio");
        foreach ($query as $row){
            $validar = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        //Si ya existe la orden parte 1,
        if (isset($validar)) {
            //$respuesta = TRUE;
            //Validamos que exista la orden parte 2
            $query_2 = $this->mConsultas->get_result("noServicio",$id_cita,"diagnostico");
            foreach ($query_2 as $row){
                $dato = $row->idDiagnostico;
            }

            //Interpretamos la respuesta de la consulta
            //Si existe el dato, entonces ya existe el registro
            if (isset($dato)) {
                $respuesta = TRUE;
            //Si no existe el dato, entonces no existe el registro
            }else {
                $respuesta = FALSE;
            }
        //Si no existe el dato, entonces no existe la orden de servicio
        }else {
            $respuesta = FALSE;
        }
        return $respuesta;
    }

    //Validamos los cheks seleccionados
    public function checks($grupo)
    {
        $return = FALSE;
        if ($grupo) {
            $return = TRUE;
        }
        return $return;
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("orden");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("orden",$idOrden,"multipunto_general");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Validar Radio
    public function validateRadioCheck($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Guardar campos generales parte 1
    public function getFormPart1($datos = NULL)
    {
        $datos["indicador_multipunto"] = [];
        $fechaCaptura = $this->input->post("fechaCaptura");
        $orden = $this->input->post("orden");

        //Armamos el array para contener los valores
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $dataPart1 = array(
              "id" => NULL,
              "orden" => $orden,
              "fechaCaptura" => $fechaCaptura,
            );
        }

        $dataPart1["noSerie"] = $this->input->post("noSerie");
        $dataPart1["modelo"] = $this->input->post("modelo");
        $dataPart1["torre"] = $this->input->post("torre");
        $dataPart1["nombreDueno"] = $this->input->post("nombreCarro");
        $dataPart1["email"] = $this->input->post("email");
        $dataPart1["emailCompania"] = $this->input->post("emailComp");

        $dataPart1["perdidaAceite"] = $this->validateRadioCheck($this->input->post("perdidaAceite"),"No");
        $dataPart1["cambioAceite"] = $this->validateRadioCheck($this->input->post("fuidoCambios"),"0");
        $dataPart1["aceiteMotor"] = $this->validateRadioCheck($this->input->post("aceiteCheck"),"S/R");
        $dataPart1["direccionHidraulica"] = $this->validateRadioCheck($this->input->post("direccionCheck"),"S/R");
        $dataPart1["transmision"] = $this->validateRadioCheck($this->input->post("transmisionCheck"),"S/R");
        $dataPart1["fluidoFrenos"] = $this->validateRadioCheck($this->input->post("liquidoFrenosCheck"),"S/R");
        $dataPart1["limpiaparabrisas"] = $this->validateRadioCheck($this->input->post("liquidoParabriCheck"),"S/R");
        $dataPart1["refrigerante"] = $this->validateRadioCheck($this->input->post("refrigeranteCheck"),"S/R");
        $dataPart1["plumasPruebas"] = $this->validateRadioCheck($this->input->post("pruebaparabrisas"),"No");
        $dataPart1["plumas"] = $this->validateRadioCheck($this->input->post("plumas"),"No");
        $dataPart1["plumasCambio"] = $this->validateRadioCheck($this->input->post("plumasCambio"),"0");
        $dataPart1["lucesClaxon"] = $this->validateRadioCheck($this->input->post("luces"),"No");
        $dataPart1["lucesClaxonCambio"] = $this->validateRadioCheck($this->input->post("lucesCambio"),"0");
        $dataPart1["grietasParabrisas"] = $this->validateRadioCheck($this->input->post("parabrisas"),"No");
        $dataPart1["grietasCambio"] = $this->validateRadioCheck($this->input->post("parabrisasCambio"),"0");

        $dataPart1["bateria"] = $this->input->post("valorBateria");
        $dataPart1["bateriaCambio"] = $this->validateRadioCheck($this->input->post("bateriaCambio"),"0");
        $dataPart1["bateriaEstado"] = $this->validateRadioCheck($this->input->post("bateriaRadio"),"S/R");

        $dataPart1["frio"] = $this->input->post("ccafabrica");
        $dataPart1["frioReal"] = $this->input->post("ccareal");

        //Guardamos/Actualizamos el registro
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $idOrden = $this->mConsultas->save_register('multipunto_general',$dataPart1);
            if ($idOrden != 0) {
                $this->getFormPart2($idOrden,$orden,$datos);
            }else {
                $datos["mensaje"] = "2";
                $this->index("0",$datos);
            }
        } else {
            $actualizar = $this->mConsultas->update_table_row('multipunto_general',$dataPart1,'id',$datos["idOrden"]);
            if ($actualizar) {
                
                if ($dataPart1["bateriaEstado"] == "Aprobado") {
                    $datos["indicador_multipunto"][0] = "V";
                }elseif ($dataPart1["bateriaEstado"] == "Futuro") {
                    $datos["indicador_multipunto"][0] = "A";
                }else {
                    $datos["indicador_multipunto"][0] = "R";
                }
                $datos["indicador_multipunto"][1] = $dataPart1["bateriaCambio"];
                 
                $this->getFormPart2($datos["idOrden"],$datos["idCita"],$datos);
            }else {
                $datos["mensaje"]="0";
                // $this->setData($datos["idOrden"],$datos);
                $this->setData($datos["idCita"],$datos);
            }
        }
    }

    //Guardar campos generales parte 2
    public function getFormPart2($idOrden = 0,$idCita = 0,$datos = NULL)
    {
        //Armamos el array para contener los valores
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $dataPart2 = array(
                "idGral2" => NULL,
                "idOrden" => $idOrden,
            );
        }

        $dataPart2["sistemaCalefaccion"] = $this->validateRadioCheck($this->input->post("sisCalefaccion"),"S/R");
        $dataPart2["sistemaCalefaccionCambio"] = $this->validateRadioCheck($this->input->post("sisCalefaccionCambio"),"0");
        $dataPart2["sisRefrigeracion"] = $this->validateRadioCheck($this->input->post("radiadorRario"),"S/R");
        $dataPart2["sisRefrigeracionCambio"] = $this->validateRadioCheck($this->input->post("radiadorCambio"),"0");
        $dataPart2["bandas"] = $this->validateRadioCheck($this->input->post("bandasRadio"),"S/R");
        $dataPart2["bandasCambio"] = $this->validateRadioCheck($this->input->post("bandasCambio"),"0");
        $dataPart2["sisFrenos"] = $this->validateRadioCheck($this->input->post("frenosRadio"),"S/R");
        $dataPart2["sisFrenosCambio"] = $this->validateRadioCheck($this->input->post("frenosCambio"),"0");
        $dataPart2["suspension"] = $this->validateRadioCheck($this->input->post("amortiguadoRadio"),"S/R");
        $dataPart2["suspensionCambio"] = $this->validateRadioCheck($this->input->post("amortiguadorCheck"),"0");
        $dataPart2["varillajeDireccion"] = $this->validateRadioCheck($this->input->post("varillajeRadio"),"S/R");
        $dataPart2["varillajeDireccionCambio"] = $this->validateRadioCheck($this->input->post("varillajeCheck"),"0");
        $dataPart2["sisEscape"] = $this->validateRadioCheck($this->input->post("escapeRadio"),"S/R");
        $dataPart2["sisEscapeCambio"] = $this->validateRadioCheck($this->input->post("escapeCheck"),"0");
        $dataPart2["funEmbrague"] = $this->validateRadioCheck($this->input->post("embragueRadio"),"S/R");
        $dataPart2["funEmbragueCambio"] = $this->validateRadioCheck($this->input->post("embragueCheck"),"0");
        $dataPart2["flechaCardan"] = $this->validateRadioCheck($this->input->post("cardanRadio"),"S/R");
        $dataPart2["flechaCardanCambio"] = $this->validateRadioCheck($this->input->post("cardanCheck"),"0");

        //Verificamos si se guardara la imagen
        $firma_3 = $this->input->post("danosInferior");
        if (substr($firma_3, 0, 22) == "data:image/png;base64,") {
            $dataPart2["defectosImg"] = $this->base64ToImage($firma_3,"diagramas");
        }else {
            $dataPart2["defectosImg"] = $firma_3;
        }

        $dataPart2["medicionesFrenos"] = $this->validateRadioCheck($this->input->post("medicionfrenos"),"0");
        $dataPart2["indicadorAceite"] = $this->validateRadioCheck($this->input->post("reinicioindicador"),"0");
        $dataPart2["comentarios"] = $this->input->post("comentarios");

        //Guardamos/Actualizamos el registro
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $idPart2 = $this->mConsultas->save_register('multipunto_general_2',$dataPart2);
            if ($idPart2 != 0) {
                $this->getFormPart3($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"] = "2";
                $this->index($this->encrypt($idCita),$datos);
            }
        } else {
            $actualizar2 = $this->mConsultas->update_table_row('multipunto_general_2',$dataPart2,'idOrden',$idOrden);
            if ($actualizar2) {
                $this->getFormPart3($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"]="0";
                $this->setData($datos["idCita"],$datos);
            }
        }
    }

    //Guardar campos parte del frente izquierdo
    public function getFormPart3($idOrden = 0,$idCita = 0,$datos = NULL)
    {
        //Armamos el array para contener los valores
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $dataPart3 = array(
                "idFI" => NULL,
                "idOrden" => $idOrden,
            );
        }

        $dataPart3["pneumaticoFI"] = $this->validateRadioCheck($this->input->post("profNeumaticoRadio"),"S/R");
        $dataPart3["pneumaticoFImm"] = $this->input->post("profNeumatico");
        $dataPart3["pneumaticoFIcambio"] = $this->validateRadioCheck($this->input->post("profNeumaticoCheck"),"0");
        $dataPart3["dNeumaticoFI"] = $this->validateRadioCheck($this->input->post("patronDegasteRadio"),"S/R");
        $dataPart3["dNeumaticoFIcambio"] = $this->validateRadioCheck($this->input->post("patronDegasteCheck"),"0");
        $dataPart3["presionInfladoFI"] = $this->validateRadioCheck($this->input->post("presionRadio"),"S/R");
        $dataPart3["presionInfladoFIpsi"] = $this->input->post("presionPSIFI");
        $dataPart3["presionInfladoFIcambio"] = $this->validateRadioCheck($this->input->post("presionCheck"),"0");
        $dataPart3["espesoBalataFI"] = $this->validateRadioCheck($this->input->post("balatasRadio"),"S/R");
        $dataPart3["espesorBalataFImm"] = $this->input->post("balatas");
        $dataPart3["espesorBalataFIcambio"] = $this->validateRadioCheck($this->input->post("balatasCheck"),"0");
        $dataPart3["espesorDiscoFI"] = $this->validateRadioCheck($this->input->post("discoRadio"),"S/R");
        $dataPart3["espesorDiscoFImm"] = $this->input->post("discoI");
        $dataPart3["espesorDiscoFIcambio"] = $this->validateRadioCheck($this->input->post("discoCheck"),"0");

        //Guardamos/Actualizamos el registro
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $idPart3 = $this->mConsultas->save_register('frente_izquierdo',$dataPart3);
            if ($idPart3 != 0) {
                $this->getFormPart4($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"] = "2";
                $this->index($this->encrypt($idCita),$datos);
            }
        } else {
            $actualizar3 = $this->mConsultas->update_table_row('frente_izquierdo',$dataPart3,'idOrden',$idOrden);
            if ($actualizar3) {
                if ($dataPart3["dNeumaticoFI"] == "Aprobado") {
                    $datos["indicador_multipunto"][2] = "V";
                }elseif ($dataPart3["dNeumaticoFI"] == "Futuro") {
                    $datos["indicador_multipunto"][2] = "A";
                }else {
                    $datos["indicador_multipunto"][2] = "R";
                }
                $datos["indicador_multipunto"][3] = $dataPart3["dNeumaticoFIcambio"];

                if ($dataPart3["espesoBalataFI"] == "Aprobado") {
                    $datos["indicador_multipunto"][4] = "V";
                }elseif ($dataPart3["espesoBalataFI"] == "Futuro") {
                    $datos["indicador_multipunto"][4] = "A";
                }else {
                    $datos["indicador_multipunto"][4] = "R";
                }
                $datos["indicador_multipunto"][5] = $dataPart3["espesorBalataFIcambio"];

                if ($dataPart3["espesorDiscoFI"] == "Aprobado") {
                    $datos["indicador_multipunto"][6] = "V";
                }elseif ($dataPart3["espesorDiscoFI"] == "Futuro") {
                    $datos["indicador_multipunto"][6] = "A";
                }else {
                    $datos["indicador_multipunto"][6] = "R";
                }
                $datos["indicador_multipunto"][7] = $dataPart3["espesorDiscoFIcambio"];

                $this->getFormPart4($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"]="0";
                // $this->setData($datos["idOrden"],$datos);
                $this->setData($datos["idCita"],$datos);
            }
        }
    }

    //Guardar campos parte tracera izquierdo
    public function getFormPart4($idOrden = 0,$idCita = 0,$datos = NULL)
    {
        //Armamos el array para contener los valores
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $dataPart4 = array(
                "idTI" => NULL,
                "idOrden" => $idOrden,
            );
        }

        $dataPart4["pneumaticoTI"] = $this->validateRadioCheck($this->input->post("profundidadDibujoRadio"),"S/R");
        $dataPart4["pneumaticoTImm"] = $this->input->post("profundidadDibujo");
        $dataPart4["pneumaticoTIcambio"] = $this->validateRadioCheck($this->input->post("profundidadDibujoCheck"),"0");
        $dataPart4["dNeumaticoTI"] = $this->validateRadioCheck($this->input->post("desgasteRadio"),"S/R");
        $dataPart4["dNeumaticoTIcambio"] = $this->validateRadioCheck($this->input->post("desgasteCheck"),"0");
        $dataPart4["presionInfladoTI"] = $this->validateRadioCheck($this->input->post("infladoRadio"),"S/R");
        $dataPart4["presionInfladoTIpsi"] = $this->input->post("presionPSITI");
        $dataPart4["presionInfladoTIcambio"] = $this->validateRadioCheck($this->input->post("infladoCheck"),"0");
        $dataPart4["espesoBalataTI"] = $this->validateRadioCheck($this->input->post("espesorBalataRadio"),"S/R");
        $dataPart4["espesorBalataTImm"] = $this->input->post("espesorBalata");
        $dataPart4["espesorBalataTIcambio"] = $this->validateRadioCheck($this->input->post("espesorBalataCheck"),"0");
        $dataPart4["espesorDiscoTI"] = $this->validateRadioCheck($this->input->post("espesorDiscoRadio"),"S/R");
        $dataPart4["espesorDiscoTImm"] = $this->input->post("espesorDisco");
        $dataPart4["espesorDiscoTIcambio"] = $this->validateRadioCheck($this->input->post("espesorDiscoCheck"),"0");
        $dataPart4["diametroTamborTI"] = $this->validateRadioCheck($this->input->post("diametroRadio"),"S/R");
        $dataPart4["diametroTamborTImm"] = $this->input->post("diametro");
        $dataPart4["diametroTamborTIcambio"] = $this->validateRadioCheck($this->input->post("diametroCheck"),"0");

        //Guardamos/Actualizamos el registro
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $idPart4 = $this->mConsultas->save_register('trasero_izquierdo',$dataPart4);
            if ($idPart4 != 0) {
                $this->getFormPart5($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"] = "2";
                $this->index($this->encrypt($idCita),$datos);
            }
        } else {
            $actualizar4 = $this->mConsultas->update_table_row('trasero_izquierdo',$dataPart4,'idOrden',$idOrden);
            if ($actualizar4) {
                if ($dataPart4["dNeumaticoTI"] == "Aprobado") {
                    $datos["indicador_multipunto"][8] = "V";
                }elseif ($dataPart4["dNeumaticoTI"] == "Futuro") {
                    $datos["indicador_multipunto"][8] = "A";
                }else {
                    $datos["indicador_multipunto"][8] = "R";
                }
                $datos["indicador_multipunto"][9] = $dataPart4["dNeumaticoTIcambio"];

                if ($dataPart4["espesoBalataTI"] == "Aprobado") {
                    $datos["indicador_multipunto"][10] = "V";
                }elseif ($dataPart4["espesoBalataTI"] == "Futuro") {
                    $datos["indicador_multipunto"][10] = "A";
                }else {
                    $datos["indicador_multipunto"][10] = "R";
                }
                $datos["indicador_multipunto"][11] = $dataPart4["espesorBalataTIcambio"];

                if ($dataPart4["espesorDiscoTI"] == "Aprobado") {
                    $datos["indicador_multipunto"][12] = "V";
                }elseif ($dataPart4["espesorDiscoTI"] == "Futuro") {
                    $datos["indicador_multipunto"][12] = "A";
                }else {
                    $datos["indicador_multipunto"][12] = "R";
                }
                $datos["indicador_multipunto"][13] = $dataPart4["espesorDiscoTIcambio"];

                if ($dataPart4["diametroTamborTI"] == "Aprobado") {
                    $datos["indicador_multipunto"][14] = "V";
                }elseif ($dataPart4["diametroTamborTI"] == "Futuro") {
                    $datos["indicador_multipunto"][14] = "A";
                }else {
                    $datos["indicador_multipunto"][14] = "R";
                }
                $datos["indicador_multipunto"][15] = $dataPart4["diametroTamborTIcambio"];

                $this->getFormPart5($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"]="0";
                // $this->setData($datos["idOrden"],$datos);
                $this->setData($datos["idCita"],$datos);
            }
        }
    }

    //Guardar campos parte del frente derecho
    public function getFormPart5($idOrden = 0,$idCita = 0,$datos = NULL)
    {
        //Armamos el array para contener los valores
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $dataPart5 = array(
                "idFD" => NULL,
                "idOrden" => $idOrden,
            );
        }

        $dataPart5["pneumaticoFD"] = $this->validateRadioCheck($this->input->post("profNeumaticoDerechoRadio"),"S/R");
        $dataPart5["pneumaticoFDmm"] = $this->input->post("profNeumaticoDerecho");
        $dataPart5["pneumaticoFDcambio"] = $this->validateRadioCheck($this->input->post("profNeumaticoDerechoCheck"),"0");
        $dataPart5["dNeumaticoFD"] = $this->validateRadioCheck($this->input->post("desgasteDerRadio"),"S/R");
        $dataPart5["dNeumaticoFDcambio"] = $this->validateRadioCheck($this->input->post("desgasteDerCheck"),"0");
        $dataPart5["presionInfladoFD"] = $this->validateRadioCheck($this->input->post("infladoDerRadio"),"S/R");
        $dataPart5["presionInfladoFDpsi"] = $this->input->post("presionPSIFD");
        $dataPart5["presionInfladoFDcambio"] = $this->validateRadioCheck($this->input->post("infladoDerCheck"),"0");
        $dataPart5["espesoBalataFD"] = $this->validateRadioCheck($this->input->post("balatasDerRadio"),"S/R");
        $dataPart5["espesorBalataFDmm"] = $this->input->post("balatasDerc");
        $dataPart5["espesorBalataFDcambio"] = $this->validateRadioCheck($this->input->post("balatasDercCheck"),"0");
        $dataPart5["espesorDiscoFD"] = $this->validateRadioCheck($this->input->post("discoDercRadio"),"S/R");
        $dataPart5["espesorDiscoFDmm"] = $this->input->post("discoDerch");
        $dataPart5["espesorDiscoFDcambio"] = $this->validateRadioCheck($this->input->post("discoDercCheck"),"0");

        //Guardamos/Actualizamos el registro
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $idPart5 = $this->mConsultas->save_register('frente_derecho',$dataPart5);
            if ($idPart5 != 0) {
                $this->getFormPart6($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"] = "2";
                $this->index($this->encrypt($idCita),$datos);
            }
        } else {
            $actualizar5 = $this->mConsultas->update_table_row('frente_derecho',$dataPart5,'idOrden',$idOrden);
            if ($actualizar5) {

                if ($dataPart5["dNeumaticoFD"] == "Aprobado") {
                    $datos["indicador_multipunto"][16] = "V";
                }elseif ($dataPart5["dNeumaticoFD"] == "Futuro") {
                    $datos["indicador_multipunto"][16] = "A";
                }else {
                    $datos["indicador_multipunto"][16] = "R";
                }
                $datos["indicador_multipunto"][17] = $dataPart5["dNeumaticoFDcambio"];

                if ($dataPart5["espesoBalataFD"] == "Aprobado") {
                    $datos["indicador_multipunto"][18] = "V";
                }elseif ($dataPart5["espesoBalataFD"] == "Futuro") {
                    $datos["indicador_multipunto"][18] = "A";
                }else {
                    $datos["indicador_multipunto"][18] = "R";
                }
                $datos["indicador_multipunto"][19] = $dataPart5["espesorBalataFDcambio"];

                if ($dataPart5["espesorDiscoFD"] == "Aprobado") {
                    $datos["indicador_multipunto"][20] = "V";
                }elseif ($dataPart5["espesorDiscoFD"] == "Futuro") {
                    $datos["indicador_multipunto"][20] = "A";
                }else {
                    $datos["indicador_multipunto"][20] = "R";
                }
                $datos["indicador_multipunto"][21] = $dataPart5["espesorDiscoFDcambio"];

                $this->getFormPart6($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"]="0";
                $this->setData($datos["idCita"],$datos);
            }
        }
    }

    //Guardar campos parte tracera derecha
    public function getFormPart6($idOrden = 0,$idCita = 0,$datos = NULL)
    {
        //Armamos el array para contener los valores
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $dataPart6 = array(
                "idTD" => NULL,
                "idOrden" => $idOrden,
            );
        }

        $dataPart6["pneumaticoTD"] = $this->validateRadioCheck($this->input->post("dibujoDerecRadio"),"S/R");
        $dataPart6["pneumaticoTDmm"] = $this->input->post("dibujoDerech");
        $dataPart6["pneumaticoTDcambio"] = $this->validateRadioCheck($this->input->post("dibujoDerechCheck"),"0");
        $dataPart6["dNeumaticoTD"] = $this->validateRadioCheck($this->input->post("patronDesgDerchRadio"),"S/R");
        $dataPart6["dNeumaticoTDcambio"] = $this->validateRadioCheck($this->input->post("patronDesgDerchCheck"),"0");
        $dataPart6["presionInfladoTD"] = $this->validateRadioCheck($this->input->post("presionDerchRadio"),"S/R");
        $dataPart6["presionInfladoTDpsi"] = $this->input->post("presionPSITD");
        $dataPart6["presionInfladoTDcambio"] = $this->validateRadioCheck($this->input->post("presionDerchCheck"),"0");
        $dataPart6["espesoBalataTD"] = $this->validateRadioCheck($this->input->post("balatasEspesorRadio"),"S/R");
        $dataPart6["espesorBalataTDmm"] = $this->input->post("balatasEspesor");
        $dataPart6["espesorBalataTDcambio"] = $this->validateRadioCheck($this->input->post("balatasEspesorCheck"),"0");
        $dataPart6["espesorDiscoTD"] = $this->validateRadioCheck($this->input->post("espesorDisco2Radio"),"S/R");
        $dataPart6["espesorDiscoTDmm"] = $this->input->post("espesorDisco2");
        $dataPart6["espesorDiscoTDcambio"] = $this->validateRadioCheck($this->input->post("espesorDisco2Check"),"0");
        $dataPart6["diametroTamborTD"] = $this->validateRadioCheck($this->input->post("diametroTamborRadio"),"S/R");
        $dataPart6["diametroTamborTDmm"] = $this->input->post("diametroTambor");
        $dataPart6["diametroTamborTDcambio"] = $this->validateRadioCheck($this->input->post("diametroTamborCheck"),"0");

        //Guardamos/Actualizamos el registro
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $idPart6 = $this->mConsultas->save_register('trasero_derecho',$dataPart6);
            if ($idPart6 != 0) {
                $this->getFormPart7($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"] = "2";
                $this->index($this->encrypt($idCita),$datos);
            }
        } else {
            $actualizar6 = $this->mConsultas->update_table_row('trasero_derecho',$dataPart6,'idOrden',$idOrden);
            if ($actualizar6) {

                if ($dataPart6["dNeumaticoTD"] == "Aprobado") {
                    $datos["indicador_multipunto"][22] = "V";
                }elseif ($dataPart6["dNeumaticoTD"] == "Futuro") {
                    $datos["indicador_multipunto"][22] = "A";
                }else {
                    $datos["indicador_multipunto"][22] = "R";
                }
                $datos["indicador_multipunto"][23] = $dataPart6["dNeumaticoTDcambio"];

                if ($dataPart6["espesoBalataTD"] == "Aprobado") {
                    $datos["indicador_multipunto"][24] = "V";
                }elseif ($dataPart6["espesoBalataTD"] == "Futuro") {
                    $datos["indicador_multipunto"][24] = "A";
                }else {
                    $datos["indicador_multipunto"][24] = "R";
                }
                $datos["indicador_multipunto"][25] = $dataPart6["espesorBalataTDcambio"];

                if ($dataPart6["espesorDiscoTD"] == "Aprobado") {
                    $datos["indicador_multipunto"][26] = "V";
                }elseif ($dataPart6["espesorDiscoTD"] == "Futuro") {
                    $datos["indicador_multipunto"][26] = "A";
                }else {
                    $datos["indicador_multipunto"][26] = "R";
                }
                $datos["indicador_multipunto"][27] = $dataPart6["espesorDiscoTDcambio"];

                if ($dataPart6["diametroTamborTD"] == "Aprobado") {
                    $datos["indicador_multipunto"][28] = "V";
                }elseif ($dataPart6["diametroTamborTD"] == "Futuro") {
                    $datos["indicador_multipunto"][28] = "A";
                }else {
                    $datos["indicador_multipunto"][28] = "R";
                }
                $datos["indicador_multipunto"][29] = $dataPart6["diametroTamborTDcambio"];

                $this->getFormPart7($idOrden,$idCita,$datos);
            }else {
                $datos["mensaje"]="0";
                $this->setData($datos["idCita"],$datos);
            }
        }

    }

    //Guardar campos parte general parte 3
    public function getFormPart7($idOrden = 0,$idCita = 0,$datos = NULL)
    {
        //Armamos el array para contener los valores
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $dataPart7 = array(
                "idGral3" => NULL,
                "idOrden" => $idOrden,
            );
        }

        $dataPart7["presion"] = $this->validateRadioCheck($this->input->post("infladoPresionRadio"),"S/R");
        $dataPart7["presionPSI"] = $this->input->post("infladoPresion");
        $dataPart7["presionCambio"] = $this->validateRadioCheck($this->input->post("infladoPresionCheck"),"0");
        $dataPart7["sistema"] = $this->input->post("system");
        $dataPart7["componente"] = $this->input->post("component");
        $dataPart7["causaRaiz"] = $this->input->post("raiz");

        //Armamos el array para contener los valores
        $dataPart7["asesor"] = $this->input->post("asesorNombre");
        //Verificamos si se guardara la imagen
        $firma_A = $this->input->post("rutaFirmaAsesor");
        if (substr($firma_A, 0, 22) == "data:image/png;base64,") {
            $dataPart7["firmaAsesor"] = $this->base64ToImage($firma_A,"firmas");
        }else {
            if ($firma_A != "") {
                $dataPart7["firmaAsesor"] = $firma_A;
            }
        }       

        $dataPart7["tecnico"] = $this->input->post("nombreTecnico");
        //Verificamos si se guardara la imagen
        $firma_1 = $this->input->post("rutaFirmaTecnico");
        if (substr($firma_1, 0, 22) == "data:image/png;base64,") {
            $dataPart7["firmaTecnico"] = $this->base64ToImage($firma_1,"firmas");
        }else {
            
                $dataPart7["firmaTecnico"] = $firma_1;
            
        }

        if($this->input->post("tempFile") != "") {
            $archivos_2 = $this->validateDoc("uploadfiles");
            $dataPart7["archivo"] = $this->input->post("tempFile")."".$archivos_2;
        }else {
            $dataPart7["archivo"] = $this->validateDoc("uploadfiles");
        }

        $dataPart7["nombreCliente"] = $this->input->post("nombreConsumidor");
        $firma_2 = $this->input->post("rutaFirmaConsumidor");
        if (substr($firma_2, 0, 22) == "data:image/png;base64,") {
            $dataPart7["firmaCliente"] = $this->base64ToImage($firma_2,"firmas");
        }else {
            $dataPart7["firmaCliente"] = $firma_2;
        }

        $dataPart7["jefeTaller"] = "JUAN CHAVEZ";//$this->input->post("JefeTallerNombre");
        $firma_3 = $this->input->post("rutaFirmaJefeTaller");
        if (substr($firma_3, 0, 22) == "data:image/png;base64,") {
            $dataPart7["firmaJefeTaller"] = $this->base64ToImage($firma_3,"firmas");
        }else {
                $dataPart7["firmaJefeTaller"] = $firma_3;
        }

        //Guardamos/Actualizamos el registro
        if (($datos["tipoRegistro"] == "1")||($datos["tipoRegistro"] == "1A")) {
            $idPart7 = $this->mConsultas->save_register('multipunto_general_3',$dataPart7);
            if ($idPart7 != 0) {
                //Actualizamos la información para el historial de ordenes
                $this->actualiza_documentacion($idCita);

                //Si el registro lo genera el asesor
                if (($dataPart7["firmaAsesor"] != "")&&($dataPart7["firmaTecnico"] == "")) {
                    $this->loadHistory($idCita,"Registro","Hoja Multipunto (Firma del Asesor)",$datos["tipoRegistro"]);
                //Si el registro lo genera el tecnico
                }

                if (($dataPart7["firmaTecnico"] != "")&&($dataPart7["firmaAsesor"] == "")) {
                    $this->loadHistory($idCita,"Registro","Hoja Multipunto (Firma del Técnico)",$datos["tipoRegistro"]);
                //Si el registro se genera por algun externo
                }else {
                    $this->loadHistory($idCita,"Registro","Hoja Multipunto",$datos["tipoRegistro"]);
                }

                //Comprobamos si la multipunto se creo por el asesor
                if ($datos["tipoRegistro"] == "1") {
                    //Enviamos la notificación al tecnico
                    $this->notificacionTec($idCita);
                }

                $datos["mensaje"] = "1";
                $this->redirectInsert($datos);
            }else {
                $datos["mensaje"] = "2";
                $this->index($this->encrypt($datos["idCita"]),$datos);
            }
        //En caso que sea actualización
        } else {
            $actualizar6 = $this->mConsultas->update_table_row('multipunto_general_3',$dataPart7,'idOrden',$idOrden);
            if ($actualizar6) {
                //Verificamos si ya se firmoaron todos los campos
                $this->chekingFirm($idOrden,$datos["idCita"]);

                //Si ya firmo el cliente, enviamos el correo
                if (($dataPart7["firmaCliente"] != "")&&(substr($firma_2, 0, 22) == "data:image/png;base64,")) {
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idCita"],"Firma del cliente","Hoja Multipunto",$datos["tipoRegistro"]);
                    //Como no se estaban enviando las multipunto por falta de firma del cliente, se enviara al firmar el jefe de taller
                    $envio = $this->envio_correo($datos["idCita"]);
                    $envio_sms = $this->envio_sms($datos["idCita"]);

                }

                if (($dataPart7["firmaAsesor"] != "")&&(substr($firma_A, 0, 22) == "data:image/png;base64,")) {
                    $this->loadHistory($datos["idCita"],"Actualización de formulario (Firma del Asesor)","Hoja Multipuntos");

                }

                if (($dataPart7["firmaTecnico"] != "")&&(substr($firma_1, 0, 22) == "data:image/png;base64,")) {
                    $this->loadHistory($datos["idCita"],"Actualización de formulario (Firma del Técnico)","Hoja Multipuntos");

                    if((in_array("A", $datos["indicador_multipunto"]))||(in_array("R", $datos["indicador_multipunto"]))){
                        $datos["indicador_multipunto"][30] = $dataPart7["nombreCliente"];
                        $this->crear_indicador_multipunto($datos["idCita"],$datos["indicador_multipunto"]);
                    }

                    //Enviamos la notificacion al jefe de taller para que firme
                    $this->notificacionJDT($datos["idCita"]);

                }

                if (($dataPart7["firmaJefeTaller"] != "")&&(substr($firma_3, 0, 22) == "data:image/png;base64,")) {
                    $this->loadHistory($datos["idCita"],"Actualización de formulario (Firma del Jefe de Taller)","Hoja Multipuntos");

                    //Se envia a intelisis
                    $envio = $this->sendMultipuntoDataApi($datos["idCita"]);

                    //Anviamos la notificacion de de cierre de orden
                    $envioCierre = $this->envioCierreOrden($datos["idCita"]);

                    //Actualizamos la información para el historial de ordenes
                    $this->actualiza_documentacion_firma($datos["idCita"]);
                }else{
                    $this->loadHistory($datos["idCita"],"Actualización de formulario","Hoja Multipuntos");
                }

                $datos["mensaje"]="1";
                $this->redirectUpdate($datos);

            }else {
                $datos["mensaje"]="0";
                // $this->setData($datos["idOrden"],$datos);
                $this->setData($datos["idCita"],$datos);
            }
        }
    }

    //Recibimos indicadores y creamos registro
    public function crear_indicador_multipunto($id_cita = "",$contenedor = NULL)
    {
        //var_dump($contenedor);
        if (count($contenedor)>29) {
            //Recuperamos la informacion general para la tabla
            $query = $this->mConsultas->datos_generales_orden($id_cita); 
            foreach ($query as $row) {

                $folio = $row->folioIntelisis;
                $telefono = "Cel: ".$row->telefono_movil."<br>Tel: ".$row->otro_telefono;
                $asesor = $row->asesor;
                $tecnico = $row->tecnico;
                $modelo = $row->vehiculo_modelo;
                $placas = $row->vehiculo_placas;
                $fecha = $row->fecha_recepcion." ".$row->hora_recepcion;
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                //$proactivo = $this->mConsultas->idHistorialProactivo($serie);
            }

            //Verificamos la carga de informacion
            if (isset($telefono)) {
                $registro = date("Y-m-d H:i:s");

                $query_2 = $this->mConsultas->get_result("id_cita",$id_cita,"indicadores_multipunto");
                foreach ($query_2 as $row){
                    $dato = $row->id;
                }

                if (!isset($dato)) {
                    $registro_indicador = array(
                        "id" => NULL,
                        "id_cita" => $id_cita,
                        "id_proactivo" => "909".$id_cita, //(($proactivo != "0") ? $proactivo : "909".$id_cita),
                        "folio" => $folio,
                        "fecha_recepcion" => $fecha,
                        "cliente" => $contenedor[30],
                        "telefono" => $telefono,
                        "asesor" => $asesor,
                        "tecnico" => $tecnico,
                        "modelo" => $modelo,
                        "placas" => $placas,
                        "serie" => $serie,

                        "bateriaEstado" => $contenedor[0],
                        "bateriaCambio" => $contenedor[1],
                        "dNeumaticoFI" => $contenedor[2],
                        "dNeumaticoFIcambio" => $contenedor[3],
                        "espesoBalataFI" => $contenedor[4],
                        "espesorBalataFIcambio" => $contenedor[5],
                        "espesorDiscoFI" => $contenedor[6],
                        "espesorDiscoFIcambio" => $contenedor[7],
                        "dNeumaticoTI" => $contenedor[8],
                        "dNeumaticoTIcambio" => $contenedor[9],
                        "espesoBalataTI" => $contenedor[10],
                        "espesorBalataTIcambio" => $contenedor[11],
                        "espesorDiscoTI" => $contenedor[12],
                        "espesorDiscoTIcambio" => $contenedor[13],
                        "diametroTamborTI" => $contenedor[14],
                        "diametroTamborTIcambio" => $contenedor[15],
                        "dNeumaticoFD" =>  $contenedor[16],
                        "dNeumaticoFDcambio" =>  $contenedor[17],
                        "espesoBalataFD" =>  $contenedor[18],
                        "espesorBalataFDcambio" =>  $contenedor[19],
                        "espesorDiscoFD" =>  $contenedor[20],
                        "espesorDiscoFDcambio" =>  $contenedor[21],
                        "dNeumaticoTD" =>  $contenedor[22],
                        "dNeumaticoTDcambio" =>  $contenedor[23],
                        "espesoBalataTD" =>  $contenedor[24],
                        "espesorBalataTDcambio" =>  $contenedor[25],
                        "espesorDiscoTD" =>  $contenedor[26],
                        "espesorDiscoTDcambio" =>  $contenedor[27],
                        "diametroTamborTD" => $contenedor[28],
                        "diametroTamborTDcambio" => $contenedor[29],

                        "fecha_alta" => $registro,
                        "fecha_actualiza" => $registro,
                    );

                    $guardar_indicador = $this->mConsultas->save_register('indicadores_multipunto',$registro_indicador);
                }else{
                    $registro_indicador = array(
                        "folio" => $folio,
                        "fecha_recepcion" => $fecha,
                        "cliente" => $contenedor[30],
                        "telefono" => $telefono,
                        "asesor" => $asesor,
                        "tecnico" => $tecnico,
                        "modelo" => $modelo,
                        "placas" => $placas,
                        "serie" => $serie,

                        "bateriaEstado" => $contenedor[0],
                        "bateriaCambio" => $contenedor[1],
                        "dNeumaticoFI" => $contenedor[2],
                        "dNeumaticoFIcambio" => $contenedor[3],
                        "espesoBalataFI" => $contenedor[4],
                        "espesorBalataFIcambio" => $contenedor[5],
                        "espesorDiscoFI" => $contenedor[6],
                        "espesorDiscoFIcambio" => $contenedor[7],
                        "dNeumaticoTI" => $contenedor[8],
                        "dNeumaticoTIcambio" => $contenedor[9],
                        "espesoBalataTI" => $contenedor[10],
                        "espesorBalataTIcambio" => $contenedor[11],
                        "espesorDiscoTI" => $contenedor[12],
                        "espesorDiscoTIcambio" => $contenedor[13],
                        "diametroTamborTI" => $contenedor[14],
                        "diametroTamborTIcambio" => $contenedor[15],
                        "dNeumaticoFD" =>  $contenedor[16],
                        "dNeumaticoFDcambio" =>  $contenedor[17],
                        "espesoBalataFD" =>  $contenedor[18],
                        "espesorBalataFDcambio" =>  $contenedor[19],
                        "espesorDiscoFD" =>  $contenedor[20],
                        "espesorDiscoFDcambio" =>  $contenedor[21],
                        "dNeumaticoTD" =>  $contenedor[22],
                        "dNeumaticoTDcambio" =>  $contenedor[23],
                        "espesoBalataTD" =>  $contenedor[24],
                        "espesorBalataTDcambio" =>  $contenedor[25],
                        "espesorDiscoTD" =>  $contenedor[26],
                        "espesorDiscoTDcambio" =>  $contenedor[27],
                        "diametroTamborTD" => $contenedor[28],
                        "diametroTamborTDcambio" => $contenedor[29],
                        "fecha_actualiza" => $registro,
                    );

                    $actualizar_indicador = $this->mConsultas->update_table_row('indicadores_multipunto',$registro_indicador,'id_cita',$id_cita);
                }
            }
        }
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion($id_cita = '')
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "multipunto" => "1"
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        
        //Si no existe el registro, lo creamos
        }else{
            //Recuperamos la informacion general
            $informacion_gral = $this->mConsultas->precargaConsulta($id_cita);
            foreach ($informacion_gral as $row){
                //$id_cita = $row->orden_cita;
                $folio_externo = $row->folioIntelisis;
                $fecha_recepcion = $row->fecha_recepcion;
                $vehiculo = $row->vehiculo_modelo;
                $placas = $row->vehiculo_placas;
                $serie = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
                $asesor = $row->asesor;
                $tecnico = $row->tecnico;
                $id_tecnico = $row->id_tecnico;
                $cliente = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            }

            $contededor = array(
                "id" => NULL,
                "id_cita" => $id_cita,
                "folio_externo" => $folio_externo,
                "fecha_recepcion" => $fecha_recepcion,
                "vehiculo" => $vehiculo,
                "placas" => $placas,
                "serie" => $serie,
                "asesor" => $asesor,
                "tecnico" => $tecnico,
                "id_tecnico" => $id_tecnico,
                "cliente" => $cliente,
                "diagnostico" => "1",
                "oasis" => "",
                "multipunto" => "1",
                "firma_jefetaller" => "0",
                "presupuesto" => "0",
                "presupuesto_afectado" => "",
                "presupuesto_origen" => "0",
                "voz_cliente" => "0",
                "evidencia_voz_cliente" => "",
                "servicio_valet" => "0",
                "fechas_creacion" => date("Y-m-d H:i:s"),
            );
            //Guardamos el registro
            $guardado = $this->mConsultas->save_register('documentacion_ordenes',$contededor);
        }
    }

    public function actualiza_documentacion_firma($id_cita = '')
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "firma_jefetaller" => "1"
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Notificación para el tecnico de que hay una nueva orden de reparación
    public function notificacionTec($idOrden = ""){
        //Recuperamos la información para la notificación
        $query = $this->mConsultas->notificacionTecnicoCita($idOrden); 
        foreach ($query as $row) {
            $fechaIni = $row->hora_inicio_dia;
            $fechaFin = $row->hora_fin;
            $tipoServicio = $row->tipo_operacion ;
            $estatusResv = $row->nombre ;
            $comentarios = $row->comentarios_servicio ;
            $tecnico = $row->tecnico;
            $modelo = $row->vehiculo_modelo;
            $idIntelisis = $row->folioIntelisis;
            $placas = $row->vehiculo_placas;
            $asesor = $row->asesor;

            $idStatus = $row->id_status_color;
        }

        if (isset($fechaIni)) {  
            //Armamos el mensaje
            $texto = SUCURSAL.". Se ha generado una orden de reparación. TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR. ".$idOrden." OR Intelisis: ".$idIntelisis.". Horario de planeado: ".$fechaIni." a ".$fechaFin.". Tipo de operación: ".$tipoServicio.". Estatus de la reservación: ".$estatusResv.". Comentarios: ".$comentarios.".";

            //Recuperamos el telefono del técnico
            $celular = $this->mConsultas->telefono_tecnico($idOrden);
            $this->sms_general($celular,$texto);

            //Jefe de taller
            //Jesus Gonzales 
            //$this->sms_general('3316724093',$texto);
            //Juan Chavez
            //$this->sms_general('3333017952',$texto);
            
            //Verificamos que sea un trabajo limpio para enviar a ventanilla
            if (($idStatus == "5")||($idStatus == "6")) {
                //Refacciones
                $this->sms_general('3316721106',$texto);
                $this->sms_general('3319701092',$texto);
                $this->sms_general('3317978025',$texto);
            }

            //Numero de angel (pruebas)
            $this->sms_general('5535527547',$texto);

            //Gerente de servicio
            //$this->sms_general('3315273100',$texto);

            //$this->sms_general('3125546366',$texto);

            //echo "OK";
            $estado = 1;
        }else{
            $estado = 0;
        }

        return $estado;        
    }

    //Notificacion Jefe de taller para firmar multipunto
    public function notificacionJDT($idOrden = ""){
        //Recuperamos la información para la notificación
        $query = $this->mConsultas->notificacionTecnicoCita($idOrden); 
        foreach ($query as $row) {
            $tecnico = $row->tecnico;
            $modelo = $row->vehiculo_modelo;
            $idIntelisis = $row->folioIntelisis;
            $asesor = $row->asesor;
            $placas = $row->vehiculo_placas;
        }

        //Armamos el mensaje
        $texto = SUCURSAL.". Edición Hoja Multipuntos.  TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." OR. ".$idOrden." OR MAGIC: ".$idIntelisis.".";

        //Jefe de taller
        //Jesus Gonzales 
        //$this->sms_general('3316724093',$texto);
        //Juan Chavez
        //$this->sms_general('3333017952',$texto);
    
        //Numero de angel (pruebas)
        $this->sms_general('5535527547',$texto);

        //Gerente de servicio
        //$this->sms_general('3315273100',$texto);

        return 1;
    }

    //Notificacion Asesores para firmar multipunto jefe de taller/ cierre de orden
    public function envioCierreOrden($idOrden = ""){
        //Recuperamos la información para la notificación
        $query = $this->mConsultas->notificacionTecnicoCita($idOrden); 
        foreach ($query as $row) {
            $tecnico = $row->tecnico;
            $modelo = $row->vehiculo_modelo;
            $idIntelisis = $row->folioIntelisis;
            $asesor = $row->asesor;
            $placas = $row->vehiculo_placas;
        }

        //Armamos el mensaje
        $texto = SUCURSAL.". La OR. ".$idOrden." OR Intelisis: ".$idIntelisis."  finalizo con la firma del jefe de taller en la Hoja Multipunto.  TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor." .";

        //Enviar mensaje al asesor
        $celular_sms = $this->mConsultas->telefono_asesor($idOrden);
        $this->sms_general($celular_sms,$texto);

        //Numero de angel (pruebas)
        //$this->sms_general('5535527547',$texto);

        //Gerente de servicio
        //$this->sms_general('3315273100',$texto);

        //Cierre de orden
        $this->sms_general('3316723220',$texto);

        //
        $this->sms_general('3323589743',$texto);
        $this->sms_general('3323589743',$texto);

        return 1;
    }

    //Enviamos el mensaje para el asesor
    public function sms_general($celular_sms='',$mensaje_sms=''){ 
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    public function envio_sms($id_cita = '')
    {
        //Verificamos que no se haya enviado el mensaje previamente
        $query_2 = $this->mConsultas->get_result("orden",$id_cita,"multipunto_general");
        foreach ($query_2 as $row){
            $dato = $row->envio_sms;
        }
        
        if (isset($dato)) {
            if ($dato == "0") {
                
                //Recuperamos la informacion a enviar
                $query = $this->mConsultas->precargaConsulta($id_cita);
                foreach ($query as $row){
                    $telefono = $row->otro_telefono ;
                    $telefono_2 = $row->telefono_movil ;
                }

                if (isset($telefono)) {
                    $mensaje = SUCURSAL_SMS.". Ver Hoja Multipuntos ".base_url()."Multipunto_PDF/".$this->encrypt($id_cita);
                    //$this->sms_general_texto("3328351116",$mensaje);
                    $this->sms_general_texto($telefono,$mensaje);
                    //if (($telefono_2 != "")&&($telefono_2 != $telefono)&&($telefono_2 != "9999999999")) {
                    //    $this->sms_general_texto($telefono_2,$mensaje);
                    //}
                    
                    $sms = array(
                        'envio_sms' => 1
                    );
                    $actualizar6 = $this->mConsultas->update_table_row('multipunto_general',$sms,'orden',$id_cita);
                }
            } 
        }

        return FALSE;
    }

    public function sms_general_texto($celular='',$mensaje=''){
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_mensaje");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular."&mensaje=".$mensaje."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);
    }

    //Hacemos la peticion por post para enviar la información a intelisis
    public function sendMultipuntoDataApi($idOrden = 0)
    {
        $data = array('idOrden' => $idOrden );
        $url = base_url()."api/Intelisis/loadDataMultipunto1";

        $ch = curl_init($url);
        //Creamos el objeto json para enviar por url los datos del formulario
        $jsonDataEncoded = json_encode($data);
        //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Adjuntamos el json a nuestra petición
        curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
        //Agregamos los encabezados del contenido
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        //Obtenemos la respuesta
        $response = curl_exec($ch);
        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);
        if($response) {
            //Validamos la respuesta y actualizamos el id factura
            $respuesta = $response;
        }else{
            $respuesta = "ERROR";
        }

        return $respuesta;

    }

    //Revisamos si ya se firmaron todos los campos
    public function chekingFirm($id = 0,$idOrden = 0)
    {
        $registro = date("Y-m-d H:i:s");
        //Recuperamos los campos necesarios
        $query = $this->mConsultas->get_result("idOrden",$id,"multipunto_general_3");
        foreach ($query as $row){
            $firmaAsesor = $row->firmaAsesor;
            $firmaTecnico = $row->firmaTecnico;
            $firmaCliente = $row->firmaCliente;
            $firmaJefeTaller = $row->firmaJefeTaller;
        }

        if (isset($firmaAsesor)) {
            //Verificamos si existe el registro en la tabla
            $query2 = $this->mConsultas->get_result("idOrden",$idOrden,"estados_pdf");
            foreach ($query2 as $row){
                $multipunto = $row->multipuntoPDF;
                $idPdf = $row->idRegistro;
            }

            if (isset($multipunto)) {
                //Comprobamos los estados validos (actualizar)
                if (($firmaAsesor != "")&&($firmaTecnico != "")&&($firmaJefeTaller != "")&&($firmaCliente != "")) {
                    $data = array(
                        "multipuntoPDF" => 1,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                  $data = array(
                      "multipuntoPDF" => 0,
                      "fecha_actualizacion" => $registro,
                  );
                }
                $this->mConsultas->update_table_row('estados_pdf',$data,'idRegistro',$idPdf);
            //Si no existe el registro se cres
            }else {
                //Comprobamos los estados validos (registro)
                if (($firmaAsesor != "")&&($firmaTecnico != "")&&($firmaJefeTaller != "")&&($firmaCliente != "")) {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 1,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 0,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 0,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }
                $this->mConsultas->save_register('estados_pdf',$data);
            }
        }

        return FALSE;
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Redireccion editar
    public function redirectUpdate($datos = NULL)
    {
        if ($datos["dirige"] == "ASE") {
            redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
        }elseif ($datos["dirige"] == "TEC") {
            redirect(base_url().'Panel_Tec/'.$datos["mensaje"]);
        }elseif ($datos["dirige"] == "JDT") {
            redirect(base_url().'Panel_JefeTaller/'.$datos["mensaje"]);
        }else {
            $this->setData($this->encrypt($datos["idCita"]),$datos);
        }
    }

    //Redireccion alta
    public function redirectInsert($datos = NULL)
    {
        if ($datos["dirige"] == "ASE") {
            redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
        }elseif ($datos["dirige"] == "TEC") {
            redirect(base_url().'Panel_Tec/'.$datos["mensaje"]);
        }elseif ($datos["dirige"] == "JDT") {
            redirect(base_url().'Panel_JefeTaller/'.$datos["mensaje"]);
        }else {
            redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
        }
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_HM";
        for($i=0; $i<=6; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    //Validamos y guardamos los documentos
    public function validateDoc($nombreDoc = NULL)
    {
        $path = "";
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);

        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
	       for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docMultipunto";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_HM";
            for($j=0; $j<=6; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }

    //Comprobamos que al final se genere el pdf
    public function setDataPDF($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Comprobamos que al final se genere el pdf
    public function setDataRevision($idOrden = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin para cuando un tecnico vuelve a intentar editar una multipunto
    public function comodin($idOrden = 0)
    {
        $datos["modalidadFormulario"] = "2A";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Edicion del tecnico
    public function comodinEditaTec($idOrden = 0)
    {
        $datos["dirige"] = "TEC";
        $datos["modalidadFormulario"] = "2B";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin para cualquier edicion que venda desde el panel
    public function comodinEditaPrincipal($idOrden = 0)
    {
        $datos["modalidadFormulario"] = "3";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin para que el tecnico registre una multipunto
    public function comodinAlta($idOrden = 0)
    {
        $datos["modalidadFormulario"] = "1A";
        $datos["dirige"] = "TEC";
        $this->index($idOrden,$datos);
    }

    //Parada comodin para que el asesor firme una multipunto llenada por el tecnico
    public function comodinEditaA($idOrden = 0)
    {
        $datos["modalidadFormulario"] = "3A";
        if ($this->session->userdata('rolIniciado')) {
            $datos["dirige"] = $this->session->userdata('rolIniciado');
        }else{
            $datos["dirige"] = "ASE";
        }
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin para cuando el jefe de taller va a firmar la multipunto
    public function comodinEditFinal($idOrden = 0)
    {
        $datos["modalidadFormulario"] = "3";
        $datos["dirige"] = "JDT";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData($idOrden = "0",$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                // $campos = $this->session->userdata('dataUser');
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        if (!isset($datos["modalidadFormulario"])) {
            if ($idOrden == "0") {
                $datos["modalidadFormulario"] = "2";
            }else {
                $datos["modalidadFormulario"] = "3";
            }
        }

        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("orden",$idOrden,"multipunto_general");
        foreach ($query as $row){
            $fecha = explode(" ",$row->fechaCaptura);
            // $fechaFracc = explode("-",$fecha[0]);
            // $datos["fechaCaptura"] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
            $datos["fechaCaptura"] = $fecha[0];

            $datos["noOrden"] = $row->orden;
            $datos["noSerie"] = $row->noSerie;

            //Cargamos el tipo de servicio de la orden
            $datos["tipoServicio"] = $this->mConsultas->tipo_servicio($datos["noOrden"]);

            //Recuperamos todas las campañas del vehiculo
            $campanias = $this->mConsultas->get_result('vin',$row->noSerie,'campanias');
            $datos["campania"] = "[";
            foreach ($campanias as $row_1) {
                $datos["campania"] .= $row_1->tipo.",";
            }
            $datos["campania"] .= "]";

            $datos["modelo"] = $row->modelo;
            //Filtrado del modelo
            $palabras = explode(" ",$row->modelo);
            $datos["nvoModelo"] = "";
            for ($i=0; $i <count($palabras) ; $i++) {
                if (($i % 7) == 0) {
                    $datos["nvoModelo"] .= $palabras[$i];
                    $datos["nvoModelo"] .= "<br>";
                }else {
                    $datos["nvoModelo"] .= $palabras[$i]." ";
                }
            }

            $datos["torre"] = $row->torre;
            $datos["nombreDueno"] = $row->nombreDueno;
            $datos["email"] = $row->email;
            $datos["emailComp"] = $row->emailCompania;
            $datos["perdidaAceite"] = $row->perdidaAceite;
            $datos["cambioAceite"] = $row->cambioAceite;
            $datos["aceiteMotor"] = $row->aceiteMotor;
            $datos["direccionHidraulica"] = $row->direccionHidraulica;
            $datos["transmision"] = $row->transmision;
            $datos["fluidoFrenos"] = $row->fluidoFrenos;
            $datos["limpiaparabrisas"] = $row->limpiaparabrisas;
            $datos["refrigerante"] = $row->refrigerante;
            $datos["plumasPruebas"] = $row->plumasPruebas;
            $datos["plumas"] = $row->plumas;
            $datos["plumasCambio"] = $row->plumasCambio;
            $datos["lucesClaxon"] = $row->lucesClaxon;
            $datos["lucesClaxonCambio"] = $row->lucesClaxonCambio;
            $datos["grietasParabrisas"] = $row->grietasParabrisas;
            $datos["grietasCambio"] = $row->grietasCambio;

            //Convertimos el porcentaje de la bateria en valores para mostrar
            $datos["bateriaValor"] = (int)$row->bateria;
            if (($datos["bateriaValor"] >= 0)&&($datos["bateriaValor"] < 10)) {
                $datos["bateria"] = "-80px";
            }elseif (($datos["bateriaValor"] >= 10)&&($datos["bateriaValor"] < 20)) {
                $datos["bateria"] = "-67px";
            }elseif (($datos["bateriaValor"] >= 20)&&($datos["bateriaValor"] < 30)) {
                $datos["bateria"] = "-46px";
            }elseif (($datos["bateriaValor"] >= 30)&&($datos["bateriaValor"] < 40)) {
                $datos["bateria"] = "-29px";
            }elseif (($datos["bateriaValor"] >= 40)&&($datos["bateriaValor"] < 50)) {
                $datos["bateria"] = "-12px";
            }elseif (($datos["bateriaValor"] >= 50)&&($datos["bateriaValor"] < 60)) {
                $datos["bateria"] = "5px";
            }elseif (($datos["bateriaValor"] >= 60)&&($datos["bateriaValor"] < 70)) {
                $datos["bateria"] = "22px";
            }elseif (($datos["bateriaValor"] >= 70)&&($datos["bateriaValor"] < 80)) {
                $datos["bateria"] = "39px";
            }elseif (($datos["bateriaValor"] >= 80)&&($datos["bateriaValor"] < 90)) {
                $datos["bateria"] = "56px";
            }else{
                $datos["bateria"] = "79px";
            }

            $datos["bateriaCambio"] = $row->bateriaCambio;
            $datos["bateriaEstado"] = $row->bateriaEstado;
            $datos["frio"] = $row->frio;
            $datos["frioReal"] = $row->frioReal;
            $idRegistro = $row->id;
        }

        if (!isset($idRegistro)) {
            $idRegistro = 0;
        }

        //Dato de busqueda/consulta en base de datos
        $datos["idCita"] = $idOrden;
        // $datos["idOrden"] = $idOrden;

        $this->setData2($idRegistro,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData2($idOrden = 0,$datos = NULL)
    {
        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"multipunto_general_2");
        foreach ($query as $row){
            $datos["sistemaCalefaccion"] = $row->sistemaCalefaccion;
            $datos["sistemaCalefaccionCambio"] = $row->sistemaCalefaccionCambio;
            $datos["sisRefrigeracion"] = $row->sisRefrigeracion;
            $datos["sisRefrigeracionCambio"] = $row->sisRefrigeracionCambio;
            $datos["bandas"] = $row->bandas;
            $datos["bandasCambio"] = $row->bandasCambio;
            $datos["sisFrenos"] = $row->sisFrenos;
            $datos["sisFrenosCambio"] = $row->sisFrenosCambio;
            $datos["suspension"] = $row->suspension;
            $datos["suspensionCambio"] = $row->suspensionCambio;
            $datos["varillajeDireccion"] = $row->varillajeDireccion;
            $datos["varillajeDireccionCambio"] = $row->varillajeDireccionCambio;
            $datos["sisEscape"] = $row->sisEscape;
            $datos["sisEscapeCambio"] = $row->sisEscapeCambio;
            $datos["funEmbrague"] = $row->funEmbrague;
            $datos["funEmbragueCambio"] = $row->funEmbragueCambio;
            $datos["flechaCardan"] = $row->flechaCardan;
            $datos["flechaCardanCambio"] = $row->flechaCardanCambio;
            // $datos["defectosInferior"] = $row->defectosInferior;
            $datos["defectosImg"] = (file_exists($row->defectosImg)) ? $row->defectosImg : "";
            // $datos["defectosImgBase"] = $row->defectosImgBase;
            $datos["medicionesFrenos"] = $row->medicionesFrenos;
            $datos["indicadorAceite"] = $row->indicadorAceite;
            $datos["comentarios"] = $row->comentarios;
        }

        $this->setData3($idOrden,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData3($idOrden = 0,$datos = NULL)
    {
        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"frente_izquierdo");
        foreach ($query as $row){
            $datos["pneumaticoFI"] = $row->pneumaticoFI;
            $datos["pneumaticoFImm"] = $row->pneumaticoFImm;
            $datos["pneumaticoFIcambio"] = $row->pneumaticoFIcambio;
            $datos["dNeumaticoFI"] = $row->dNeumaticoFI;
            $datos["dNeumaticoFIcambio"] = $row->dNeumaticoFIcambio;
            $datos["presionInfladoFI"] = $row->presionInfladoFI;
            $datos["presionInfladoFIpsi"] = $row->presionInfladoFIpsi;
            $datos["presionInfladoFIcambio"] = $row->presionInfladoFIcambio;
            $datos["espesoBalataFI"] = $row->espesoBalataFI;
            $datos["espesorBalataFImm"] = $row->espesorBalataFImm;
            $datos["espesorBalataFIcambio"] = $row->espesorBalataFIcambio;
            $datos["espesorDiscoFI"] = $row->espesorDiscoFI;
            $datos["espesorDiscoFImm"] = $row->espesorDiscoFImm;
            $datos["espesorDiscoFIcambio"] = $row->espesorDiscoFIcambio;
        }

        $query_2 = $this->mConsultas->get_result("idOrden",$idOrden,"trasero_izquierdo");
        foreach ($query_2 as $row){
            $datos["pneumaticoTI"] = $row->pneumaticoTI;
            $datos["pneumaticoTImm"] = $row->pneumaticoTImm;
            $datos["pneumaticoTIcambio"] = $row->pneumaticoTIcambio;
            $datos["dNeumaticoTI"] = $row->dNeumaticoTI;
            $datos["dNeumaticoTIcambio"] = $row->dNeumaticoTIcambio;
            $datos["presionInfladoTI"] = $row->presionInfladoTI;
            $datos["presionInfladoTIpsi"] = $row->presionInfladoTIpsi;
            $datos["presionInfladoTIcambio"] = $row->presionInfladoTIcambio;
            $datos["espesoBalataTI"] = $row->espesoBalataTI;
            $datos["espesorBalataTImm"] = $row->espesorBalataTImm;
            $datos["espesorBalataTIcambio"] = $row->espesorBalataTIcambio;
            $datos["espesorDiscoTI"] = $row->espesorDiscoTI;
            $datos["espesorDiscoTImm"] = $row->espesorDiscoTImm;
            $datos["espesorDiscoTIcambio"] = $row->espesorDiscoTIcambio;
            $datos["diametroTamborTI"] = $row->diametroTamborTI;
            $datos["diametroTamborTImm"] = $row->diametroTamborTImm;
            $datos["diametroTamborTIcambio"] = $row->diametroTamborTIcambio;
        }

        $this->setData4($idOrden,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData4($idOrden = 0,$datos = NULL)
    {
        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"frente_derecho");
        foreach ($query as $row){
            $datos["pneumaticoFD"] = $row->pneumaticoFD;
            $datos["pneumaticoFDmm"] = $row->pneumaticoFDmm;
            $datos["pneumaticoFDcambio"] = $row->pneumaticoFDcambio;
            $datos["dNeumaticoFD"] = $row->dNeumaticoFD;
            $datos["dNeumaticoFDcambio"] = $row->dNeumaticoFDcambio;
            $datos["presionInfladoFD"] = $row->presionInfladoFD;
            $datos["presionInfladoFDpsi"] = $row->presionInfladoFDpsi;
            $datos["presionInfladoFDcambio"] = $row->presionInfladoFDcambio;
            $datos["espesoBalataFD"] = $row->espesoBalataFD;
            $datos["espesorBalataFDmm"] = $row->espesorBalataFDmm;
            $datos["espesorBalataFDcambio"] = $row->espesorBalataFDcambio;
            $datos["espesorDiscoFD"] = $row->espesorDiscoFD;
            $datos["espesorDiscoFDmm"] = $row->espesorDiscoFDmm;
            $datos["espesorDiscoFDcambio"] = $row->espesorDiscoFDcambio;
        }

        $query_2 = $this->mConsultas->get_result("idOrden",$idOrden,"trasero_derecho");
        foreach ($query_2 as $row){
            $datos["pneumaticoTD"] = $row->pneumaticoTD;
            $datos["pneumaticoTDmm"] = $row->pneumaticoTDmm;
            $datos["pneumaticoTDcambio"] = $row->pneumaticoTDcambio;
            $datos["dNeumaticoTD"] = $row->dNeumaticoTD;
            $datos["dNeumaticoTDcambio"] = $row->dNeumaticoTDcambio;
            $datos["presionInfladoTD"] = $row->presionInfladoTD;
            $datos["presionInfladoTDpsi"] = $row->presionInfladoTDpsi;
            $datos["presionInfladoTDcambio"] = $row->presionInfladoTDcambio;
            $datos["espesoBalataTD"] = $row->espesoBalataTD;
            $datos["espesorBalataTDmm"] = $row->espesorBalataTDmm;
            $datos["espesorBalataTDcambio"] = $row->espesorBalataTDcambio;
            $datos["espesorDiscoTD"] = $row->espesorDiscoTD;
            $datos["espesorDiscoTDmm"] = $row->espesorDiscoTDmm;
            $datos["espesorDiscoTDcambio"] = $row->espesorDiscoTDcambio;
            $datos["diametroTamborTD"] = $row->diametroTamborTD;
            $datos["diametroTamborTDmm"] = $row->diametroTamborTDmm;
            $datos["diametroTamborTDcambio"] = $row->diametroTamborTDcambio;
        }

        $this->setData5($idOrden,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData5($idOrden = 0,$datos = NULL)
    {
        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"multipunto_general_3");
        foreach ($query as $row){
            $datos["presion"] = $row->presion;
            $datos["presionPSI"] = $row->presionPSI;
            $datos["presionCambio"] = $row->presionCambio;
            $datos["sistema"] = $row->sistema;
            $datos["componente"] = $row->componente;
            $datos["causaRaiz"] = $row->causaRaiz;
            $datos["comentario"] = $row->comentario;
            // $datos["asesor"] = $row->asesor;
            // $datos["tecnico"] = $row->tecnico;
            $datos["archivo"] = explode("|",$row->archivo);
            $datos["archivoTemp"] = $row->archivo;

            $datos["asesor"] = $row->asesor;
            $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";

            $datos["tecnico"] = $row->tecnico;
            $datos["firmaTecnico"] = (file_exists($row->firmaTecnico)) ? $row->firmaTecnico : "";

            $datos["nombreCliente"] = $row->nombreCliente;
            $datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : "";

            $datos["jefeTaller"] = $row->jefeTaller;
            $datos["firmaJefeTaller"] = (file_exists($row->firmaJefeTaller)) ? $row->firmaJefeTaller : "";
        }

        $datos["idOrden"] = $idOrden;

        //Consultamos si ya se entrego la unidad
        $query_cita = $this->mConsultas->get_result("id_cita",$datos["idCita"],"citas");
        foreach ($query_cita as $row2){
             $datos["UnidadEntrega"] = $row2->unidad_entregada;
        }

        //Verificamos si existe el registro en la tabla de estados terminados
        $query2 = $this->mConsultas->get_result("idOrden",$datos["idCita"],"estados_pdf");
        foreach ($query2 as $row){
            $datos["completoFormulario"] = $row->multipuntoPDF;
        }

        //Consultamos si ya se entrego la unidad
        $query_cita = $this->mConsultas->get_result("id_cita",$datos["idCita"],"ordenservicio");
        foreach ($query_cita as $row2){
            $datos["firma_diagnostico"] = $row2->firma_diagnostico;
        }

        //Verificamos si la unidad ha pasado por trabajando
        $datos["unidadTrabajando"] = $this->mConsultas->unidadTrabajando($datos["idCita"]);

        //Verificamos donde se imprimira la información
        if ($datos["destinoVista"] == "PDF") {
            $this->generatePDF($datos);
        }else if($datos["destinoVista"] == "Revisión") {
            $this->loadAllView($datos,'multipunto/plantillaRevision');
        } else {
            $this->loadAllView($datos,'multipunto/multipunto_editar');
        }
    }

    //Metodo donde regresa la precarga (ajax)
    public function precarga()
    {
        $dato = $_POST['serie'];
        $cadena = "";
        $precarga = $this->mConsultas->precargaConsulta($dato);
        foreach ($precarga as $row){
              $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
              $cadena .= $row->vehiculo_modelo."|";
              $cadena .= ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie."|" : $row->vehiculo_identificacion."|";
              $cadena .= $row->numero_interno."-".$row->color."|";
              $cadena .= $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno."|";
              $cadena .= $row->datos_email."|";
              $cadena .= $row->asesor."|";
              $cadena .= $row->nombre_compania."|";
              $cadena .= $row->tecnico."|";
              $cadena .= $row->subcategoria."|";

              //Recuperamos todas las campañas del vehiculo
              $campanias = $this->mConsultas->get_result('vin',$serie,'campanias');
              $cadena .= "[";
              foreach ($campanias as $row_1) {
                  $cadena .= $row_1->tipo.",";
              }
              $cadena .= "]";

              $cadena .= "|";

              $idOrden = $row->orden_cita;

              $dato_2 = $row->id;
              $correo_2 = $row->correo_compania;

        }

        //Si existe la variable de sesion
        if ($this->session->userdata('idUsuario')) {
            // $campos = $this->session->userdata('idUsuario');
            $query = $this->Verificacion->dataAsesor($this->session->userdata('idUsuario'));
            //Verificamos el usuario
            $row = $query->row();
            if (isset($row)){
                $cadena .= $query->row(0)->adminNombre;
            }

            if (isset($correo_2)) {
                $cadena .= "|".$correo_2;
            }
        }else {
            if (isset($correo_2)) {
                $cadena .= "|".$correo_2;
            }
        }

        //Estatus de la orden
        $cadena .= "|".$this->mConsultas->estatusOrden($dato);

        echo $cadena;
    } 

    //Validacion de campos para alta/Edicion (ajax)
    public function searchOrden()
    {
        $dato = $_POST['orden'];
        $cadena = "";
        $query = $this->mConsultas->get_result("orden",$dato,"multipunto_general");
        foreach ($query as $row){
            $validar = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($validar)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "EDITAR_".$this->encrypt($dato);
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "ALTA_".$this->encrypt($dato);
        }
        echo $respuesta;
    }

    //Cargar datos para cotizacion
    public function cargaDatos()
    {
        //Recuperamos el id de la orden
        $idOrden = $_POST['idOrden'];
        $cadena = "";

        $precarga = $this->mConsultas->precargaConsulta($idOrden);
        foreach ($precarga as $row){
            $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
            $cadena .= ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie."_" : $row->vehiculo_identificacion."_";
            // $cadena .= $row->numero_interno."-".$row->color."_";
            $cadena .= $row->vehiculo_anio."_";
            $cadena .= $row->vehiculo_placas."_";
            $cadena .= $row->subcategoria."_";
            $cadena .= $row->tecnico."_";
            $cadena .= $row->asesor."_";

            //Recuperamos todas las campañas del vehiculo
            $campanias = $this->mConsultas->get_result('vin',$serie,'campanias');
            $cadena .= "[";
            foreach ($campanias as $row_1) {
                $cadena .= $row_1->tipo.",";
            }
            $cadena .= "]";

            $cadena .= "_";
        }

        echo $cadena;
    }

    //Verificamos la existencia del registro para recuperar la informacion (ajax)
    public function confirmRegistry()
    {
        //Recuperamos el id de la orden
        $idOrden = $_POST['serie_2'];
        $regreso = "";
        //Hacemos la consulta para recuperar la informacion
        $query = $this->mConsultas->get_result("orden",$idOrden,"multipunto_general");
        foreach ($query as $row){
            $fecha = explode(" ",$row->fechaCaptura);
            $fechaFracc = explode("-",$fecha[0]);
            $regreso .= $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0]."|";
            // $regreso .= $row->orden."|";
            $regreso .= str_replace("|", "", $row->noSerie)."|";
            $regreso .= str_replace("|", "", $row->modelo)."|";
            $regreso .= str_replace("|", "", $row->torre)."|";
            $regreso .= str_replace("|", "", $row->nombreDueno)."|";
            $regreso .= str_replace("|", "", $row->email)."|";
            $regreso .= $row->perdidaAceite."|";
            $regreso .= $row->cambioAceite."|";
            $regreso .= $row->aceiteMotor."|";
            $regreso .= $row->direccionHidraulica."|";
            $regreso .= $row->transmision."|";
            $regreso .= $row->fluidoFrenos."|";
            $regreso .= $row->limpiaparabrisas."|";
            $regreso .= $row->refrigerante."|";
            $regreso .= $row->plumasPruebas."|";
            $regreso .= $row->plumas."|";
            $regreso .= $row->plumasCambio."|";
            $regreso .= $row->lucesClaxon."|";
            $regreso .= $row->lucesClaxonCambio."|";
            $regreso .= $row->grietasParabrisas."|";
            $regreso .= $row->grietasCambio."|";

            //Convertimos el porcentaje de la bateria en valores para mostrar
            $regreso .= $row->bateria."|";
            $regreso .= $row->bateriaCambio."|";
            $regreso .= $row->bateriaEstado."|";
            $regreso .= str_replace("|", "", $row->frio)."|";
            $regreso .= str_replace("|", "", $row->frioReal)."|";
            $regreso .= $row->id."|";

            //Recuperamos todas las campañas del vehiculo
            $campanias = $this->mConsultas->get_result('vin',$row->noSerie,'campanias');
            $regreso .= "[";
            foreach ($campanias as $row_1) {
                $regreso .= str_replace("|", "", $row_1->tipo).",";
            }
            $regreso .= "]";

            $regreso .= "|";
            $idRegistro = $row->id;
        }

        if (isset($idRegistro)) {
            $query2 = $this->mConsultas->get_result("idOrden",$idRegistro,"multipunto_general_3");
            foreach ($query2 as $row){
                $regreso .= str_replace("|", "", $row->asesor)."|";
                $regreso .= str_replace("|", "", $row->firmaAsesor)."|";
                $regreso .= str_replace("|", "", $row->tecnico)."|";
                $regreso .= str_replace("|", "", $row->firmaTecnico)."|";
                $regreso .= str_replace("|", "", $row->firmaJefeTaller)."|";

            }

            $regreso .= $this->encrypt($idOrden)."|"; //33
            echo $regreso;
        }else {
            $regreso .= "ERROR|";
            $regreso .= $this->encrypt($idOrden);
            echo $regreso;
        }
    }

    //Verificar sesion
    public function validateSession()
    {
        $respuesta = FALSE;
        //Si existe la variable de sesion
        if ($this->session->userdata('idUsuario')) {
            $respuesta = TRUE;
        }

        return $respuesta;
    }

    //Enviamos correo de la multipunto
    /*public function envio_correo($idOrden = 0)
    {
        //Definimos la fecha de envio
        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","viernes","Sábado");
        $datosEmail["dia"] = $dias[date("w")];
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $datosEmail["mes"] = $meses[date("n")-1];

        //Recuperamos la informacion a enviar
        $query = $this->mConsultas->precargaConsulta($idOrden);
        foreach ($query as $row){
            $datosEmail["correo1"] = $row->datos_email;
            $datosEmail["correo2"] = $row->correo_compania;
            $datosEmail["nombre"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            $datosEmail["companiaNombre"] = $row->nombre_compania;
            $datosEmail["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
        }

        //Url del archivo
        $datosEmail["url"] = base_url()."Multipunto_PDF/".$this->encrypt($idOrden);
        $datosEmail["formularioEnvio"] = "Hoja Multipuntos";
        //Cargamos la informacion en la plnatilla
        $htmlContent = $this->load->view("plantillaEmail", $datosEmail, TRUE);

        $configGmail = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => EMAIL_SERVER,
            'smtp_pass' => EMAIL_PASS,
            'mailtype' => 'html',
            'smtp_crypto'=> 'tls',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );

        //Establecemos esta configuración
        $this->email->initialize($configGmail);
        //Ponemos la dirección de correo que enviará el email y un nombre
        $this->email->from(EMAIL_SERVER, SUCURSAL);
        //Ponemos la direccion de aquien va dirigido
        $this->email->to($datosEmail["correo1"],$datosEmail["nombre"]);
        $this->email->to($datosEmail["correo2"],$datosEmail["nombre"]); 
        // $this->email->to('shara.valdovinos.1994@gmail.com','Shara');
        //Multiples destinos
        // $this->email->to($correo1, $correo2, $correo3);
        //Con copia a:
        // $this->email->cc($correo);
        //Con copia oculta a:
        // $this->email->bcc($correo);
        //$this->email->bcc('info@planificadorempresarial.mx','Angel');
        $this->email->bcc('contactoplasenciamatriz@gmail.com','Contacto Plasencia Motors Guadalajara');
        //$this->email->bcc('info@sohex.mx','Angel');
        //Colocamos el asunto del correo
        $this->email->subject('Hoja Multipuntos');
        //Cargamos la plantilla del correo
        $this->email->message($htmlContent);
        //Enviar archivo adjunto
        // $this->email->attach('files/attachment.pdf');
        //Enviamos el correo
        if($this->email->send()){
            $envio = 1;
        }else{
            $envio = 2;
        }

        //Actualizamos la tabla de multipunto para notificar = 1 (envio exitoso), notificar = 2 (envio fallido)
        $dataEmail = array(
            "envioEmail" => $envio,
        );

        //Grabamos la información en la tabla
        $actualizar = $this->mConsultas->update_table_row('multipunto_general',$dataEmail,'orden',$idOrden);

        if ($actualizar) {
            $respuesta = "OK";
        }else {
            $respuesta = "NO SE PUDO ACTUALIZAR";
        }

        return $respuesta;
    }*/

    public function envio_correo($id_cita='')
    {
        //Definimos la fecha de envio
        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","viernes","Sábado");
        $datosEmail["dia"] = $dias[date("w")];
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $datosEmail["mes"] = $meses[date("n")-1];

        //Recuperamos la informacion a enviar
        $query = $this->mConsultas->precargaConsulta($id_cita);
        $validar = 0;
        foreach ($query as $row){
            $correo_1 = explode("@",$row->datos_email);
            $correo_1_b = strrpos($correo_1[1], "tiene");
            $correo_1_c = strrpos($correo_1[1], "dejo");

            if ((strlen($correo_1[0]) >= 3) && ( ($correo_1_b === false) && ($correo_1_c === false) ) ) {
                $datosEmail["correo1"] = trim($row->datos_email);
                $validar = 1;
            } 

            $correo_2 = explode("@",$row->correo_compania);
            $correo_2_b = strrpos($correo_2[1], "tiene");
            $correo_2_c = strrpos($correo_2[1], "dejo");

            if ((strlen($correo_2[0]) >= 3) && ( ($correo_2_b === false) && ($correo_2_c === false) ) ) {
                $datosEmail["correo2"] = trim($row->correo_compania);
                $validar = 1;
            }

            $datosEmail["nombre"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            $datosEmail["companiaNombre"] = $row->nombre_compania;
            $datosEmail["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
        }

        if ($validar == 1) {
            //Url del archivo
            $datosEmail["url"] = base_url()."Multipunto_PDF/".$this->encrypt($id_cita);
            $datosEmail["formularioEnvio"] = "Hoja Multipuntos";
            //Cargamos la informacion en la plnatilla
            //$htmlContent = $this->load->view("plantillaEmail", $datosEmail, TRUE);
            $htmlContent = $this->load->view("plantillaEmailSohex", $datosEmail, TRUE);
            
            //La creación de instancias y el paso de 'verdadero' habilita excepciones
            // PHPMailer object
            //$mail = $this->phpmailer_library->load();
            $mail = new PHPMailer\PHPMailer\PHPMailer();
            //Habilitar la salida de depuración detallada
            $mail->SMTPDebug = 0; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages

            try {
                //Enviar usando SMTP
                $mail->isSMTP();
                //Enable SMTP debugging
                //SMTP::DEBUG_OFF = off (for production use)
                //SMTP::DEBUG_CLIENT = client messages
                //SMTP::DEBUG_SERVER = client and server messages
                //$mail->SMTPDebug = SMTP::DEBUG_SERVER;

                //Configure el servidor SMTP para enviar
                $mail->Host = 'smtp.gmail.com';
                //Use `$mail->Host = gethostbyname('smtp.gmail.com');`
                //if your network does not support SMTP over IPv6,
                //though this may cause issues with TLS
                // Establecer el número de puerto SMTP - 587 para TLS autenticado, A.K.A. RFC4409 SMTP
                $mail->Port = 587;
                // Establezca el mecanismo de cifrado para usar - StartTLS o SMTPS
                //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                $mail->SMTPSecure = 'tls';
                //Habilitar la autenticación SMTP
                $mail->SMTPAuth = true;
                // Nombre de usuario para usar para la autenticación SMTP: use la dirección de correo electrónico completa para Gmail
                $mail->Username = EMAIL_SERVER;
                // Contraseña para usar para la autenticación SMTP 
                $mail->Password = EMAIL_PASS;

                //Cuerpo del correo
                $mail->setFrom(EMAIL_SERVER, SUCURSAL);
                //$mail->addReplyTo('no-reply@sohexotificaciones.com', 'Notificacion Automatica');
                
                // Add a recipient
                //$mail->addAddress('desarrollo@sohex.mx','Testeo');
                if (isset($datosEmail["correo1"])) {
                    $mail->addAddress($datosEmail["correo1"],$datosEmail["nombre"]);
                }

                if (isset($datosEmail["correo2"])) {
                    $mail->addAddress($datosEmail["correo2"],$datosEmail["nombre"]); 
                }
                
                // con copia y con copia oculta
                //$mail->addCC('cc@example.com'); 
                $mail->addBCC('contactoplasenciamatriz@gmail.com','Contacto Plasencia Motors Guadalajara');
                $mail->addBCC('desarrollo@sohex.mx','Testeo');
                // Asunto del correo
                $mail->Subject = 'PDF Hoja Multipunto';
                
                // Set email format to HTML
                $mail->isHTML(true);
                
                // Email body content
                $mailContent = $htmlContent;
                $mail->Body = $mailContent;
                
                // Send email
                /*if(!$mail->send()){
                    echo 'El mensaje no se pudo ser enviado.<br>';
                    echo 'Error de envio: ' . $mail->ErrorInfo;
                }else{
                    echo 'Correo enviado correctamente';
                }*/

                //Enviamos el correo
                if($mail->send()){
                    $envio = 1;
                }else{
                    $envio = 2;
                }


            } catch (Exception $e) {
                //echo "El mensaje no se pudo ser enviado. Error de correo: ". $mail->ErrorInfo;
                $envio = 3;
            }
        } else {
            $envio = 4;
        }

        //Actualizamos la tabla de multipunto para notificar = 1 (envio exitoso), notificar = 2 (envio fallido)
        $dataEmail = array(
            "envioEmail" => $envio,
        );

        //Grabamos la información en la tabla
        $actualizar = $this->mConsultas->update_table_row('multipunto_general',$dataEmail,'orden',$id_cita);

        if ($actualizar) {
            $respuesta = "OK";
        }else {
            $respuesta = "NO SE PUDO ACTUALIZAR";
        }

        return $respuesta;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("multipunto/plantillaPdf", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/Multipunto/multipuntoJs.js',
                'assets/js/firmasMoviles/firmas_HM.js',
                'assets/js/Multipunto/anexos_1.js',
                'assets/js/Multipunto/precarga_form.js',
                //'assets/js/Multipunto/reglasForm.js',
                'assets/js/Multipunto/reglasForm_2.js',
                'assets/js/superUsuario.js' 
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Hoja Multipunto"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
