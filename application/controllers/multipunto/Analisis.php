<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Analisis extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);

        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);

        ob_start("ob_gzhandler");
    }

  	public function index($idOrden = 0,$datos = NULL)
  	{
        $datos["hoy"] = date("Y-m-d");
        $datos["limiteAno"] = date("Y");

        $this->countAllValue($datos);
  		  // $this->loadAllView($datos,'multipunto/Analisis/resumenItems');
  	}

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function countAllValue($datos = NULL)
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);

        ob_start("ob_gzhandler");

        //Cargamos la información del formulario
        $query = $this->mConsultas->get_table("multipunto_general");
        foreach ($query as $row){
            $fecha = explode(" ",$row->fechaCaptura);
            $fechaFracc = explode("-",$fecha[0]);
            $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];

            //Inicializamos contadores
            // $aprobado = 0;
            // $futuro = 0;
            // $inmediato = 0;
            // $cambioParte = 0;

            //Comenzamos a analizar los resultados
            //Primera parte, confirmar conteo
            $idRegistros[] = $row->id;
            $datos["idOrden"][] = $row->orden;
        }

        //Validamos la existencia de los registros
        if (isset($idRegistros)) {
            //creamos los contenedores generales
            $datos["aprobado"] = [];
            $datos["futuro"] = [];
            $datos["inmediato"] = [];
            $datos["cambioParte"] = [];

            $datos["idRegistros"] = $idRegistros;

            $this->countAllValue2($datos);
        }else {
            $this->loadAllView($datos,'multipunto/Analisis/resumenItems');
        }
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function countAllValue2($datos = NULL)
    {
        //Recuperamos los valoes de la segunda tabla
        for ($i=0; $i <count($datos["idRegistros"]) ; $i++) {
            $query_2 = $this->mConsultas->get_result("idOrden",$datos["idRegistros"][$i],"multipunto_general_2");
            foreach ($query_2 as $row){
                //Inicializamos contadores
                $aprobado = 0;
                $futuro = 0;
                $inmediato = 0;
                $cambioParte = 0;

                if ($row->sistemaCalefaccion == "Aprobado") {
                    $aprobado++;
                }elseif ($row->sistemaCalefaccion == "Futuro") {
                    $futuro++;
                }elseif ($row->sistemaCalefaccion == "Inmediato") {
                    $inmediato++;
                }

                if ($row->sistemaCalefaccionCambio == "1") {
                    $cambioParte++;
                }

                if ($row->sisRefrigeracion == "Aprobado") {
                    $aprobado++;
                }elseif ($row->sisRefrigeracion == "Futuro") {
                    $futuro++;
                }elseif ($row->sisRefrigeracion == "Inmediato") {
                    $inmediato++;
                }

                if ($row->sisRefrigeracionCambio == "1") {
                    $cambioParte++;
                }

                if ($row->bandas == "Aprobado") {
                    $aprobado++;
                }elseif ($row->bandas == "Futuro") {
                    $futuro++;
                }elseif ($row->bandas == "Inmediato") {
                    $inmediato++;
                }

                if ($row->bandasCambio == "1") {
                    $cambioParte++;
                }

                if ($row->sisFrenos == "Aprobado") {
                    $aprobado++;
                }elseif ($row->sisFrenos == "Futuro") {
                    $futuro++;
                }elseif ($row->sisFrenos == "Inmediato") {
                    $inmediato++;
                }

                if ($row->sisFrenosCambio == "1") {
                    $cambioParte++;
                }

                if ($row->suspension == "Aprobado") {
                    $aprobado++;
                }elseif ($row->suspension == "Futuro") {
                    $futuro++;
                }elseif ($row->suspension == "Inmediato") {
                    $inmediato++;
                }

                if ($row->suspensionCambio == "1") {
                    $cambioParte++;
                }

                if ($row->varillajeDireccion == "Aprobado") {
                    $aprobado++;
                }elseif ($row->varillajeDireccion == "Futuro") {
                    $futuro++;
                }elseif ($row->varillajeDireccion == "Inmediato") {
                    $inmediato++;
                }

                if ($row->varillajeDireccionCambio == "1") {
                    $cambioParte++;
                }

                if ($row->sisEscape == "Aprobado") {
                    $aprobado++;
                }elseif ($row->sisEscape == "Futuro") {
                    $futuro++;
                }elseif ($row->sisEscape == "Inmediato") {
                    $inmediato++;
                }

                if ($row->sisEscapeCambio == "1") {
                    $cambioParte++;
                }

                if ($row->funEmbrague == "Aprobado") {
                    $aprobado++;
                }elseif ($row->funEmbrague == "Futuro") {
                    $futuro++;
                }elseif ($row->funEmbrague == "Inmediato") {
                    $inmediato++;
                }

                if ($row->funEmbragueCambio == "1") {
                    $cambioParte++;
                }

                if ($row->flechaCardan == "Aprobado") {
                    $aprobado++;
                }elseif ($row->flechaCardan == "Futuro") {
                    $futuro++;
                }elseif ($row->flechaCardan == "Inmediato") {
                    $inmediato++;
                }

                if ($row->flechaCardanCambio == "1") {
                    $cambioParte++;
                }
            }

            //Almacenamos los registros
            $datos["aprobado"][$i] = $aprobado;
            $datos["futuro"][$i] = $futuro;
            $datos["inmediato"][$i] = $inmediato;
            $datos["cambioParte"][$i] = $cambioParte;

        }

        $this->countAllValue3($datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function countAllValue3($datos = NULL)
    {

        for ($i=0; $i <count($datos["idRegistros"]) ; $i++) {
            $query_2 = $this->mConsultas->get_result("idOrden",$datos["idRegistros"][$i],"frente_izquierdo");
            foreach ($query_2 as $row){
                //Inicializamos contadores
                $aprobado = 0;
                $futuro = 0;
                $inmediato = 0;
                $cambioParte = 0;

                if ($row->pneumaticoFI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->pneumaticoFI == "Futuro") {
                    $futuro++;
                }elseif ($row->pneumaticoFI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->pneumaticoFIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->dNeumaticoFI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->dNeumaticoFI == "Futuro") {
                    $futuro++;
                }elseif ($row->dNeumaticoFI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->dNeumaticoFIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->presionInfladoFI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->presionInfladoFI == "Futuro") {
                    $futuro++;
                }elseif ($row->presionInfladoFI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->presionInfladoFIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->espesoBalataFI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->espesoBalataFI == "Futuro") {
                    $futuro++;
                }elseif ($row->espesoBalataFI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->espesorBalataFIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->espesorDiscoFI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->espesorDiscoFI == "Futuro") {
                    $futuro++;
                }elseif ($row->espesorDiscoFI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->espesorDiscoFIcambio == "1") {
                    $cambioParte++;
                }

            }

            //Almacenamos los registros
            $datos["aprobado"][$i] += $aprobado;
            $datos["futuro"][$i] += $futuro;
            $datos["inmediato"][$i] += $inmediato;
            $datos["cambioParte"][$i] += $cambioParte;
        }

        for ($i=0; $i <count($datos["idRegistros"]) ; $i++) {
            $query_2 = $this->mConsultas->get_result("idOrden",$datos["idRegistros"][$i],"trasero_izquierdo");
            foreach ($query_2 as $row){
                //Inicializamos contadores
                $aprobado = 0;
                $futuro = 0;
                $inmediato = 0;
                $cambioParte = 0;

                if ($row->pneumaticoTI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->pneumaticoTI == "Futuro") {
                    $futuro++;
                }elseif ($row->pneumaticoTI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->pneumaticoTIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->dNeumaticoTI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->dNeumaticoTI == "Futuro") {
                    $futuro++;
                }elseif ($row->dNeumaticoTI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->dNeumaticoTIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->presionInfladoTI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->presionInfladoTI == "Futuro") {
                    $futuro++;
                }elseif ($row->presionInfladoTI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->presionInfladoTIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->espesoBalataTI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->espesoBalataTI == "Futuro") {
                    $futuro++;
                }elseif ($row->espesoBalataTI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->espesorBalataTIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->espesorDiscoTI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->espesorDiscoTI == "Futuro") {
                    $futuro++;
                }elseif ($row->espesorDiscoTI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->espesorDiscoTIcambio == "1") {
                    $cambioParte++;
                }

                if ($row->diametroTamborTI == "Aprobado") {
                    $aprobado++;
                }elseif ($row->diametroTamborTI == "Futuro") {
                    $futuro++;
                }elseif ($row->diametroTamborTI == "Inmediato") {
                    $inmediato++;
                }

                if ($row->diametroTamborTIcambio == "1") {
                    $cambioParte++;
                }

            }

            //Almacenamos los registros
            $datos["aprobado"][$i] += $aprobado;
            $datos["futuro"][$i] += $futuro;
            $datos["inmediato"][$i] += $inmediato;
            $datos["cambioParte"][$i] += $cambioParte;
        }

        $this->countAllValue4($datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function countAllValue4($datos = NULL)
    {

        for ($i=0; $i <count($datos["idRegistros"]) ; $i++) {
            $query_2 = $this->mConsultas->get_result("idOrden",$datos["idRegistros"][$i],"frente_derecho");
            foreach ($query_2 as $row){
                //Inicializamos contadores
                $aprobado = 0;
                $futuro = 0;
                $inmediato = 0;
                $cambioParte = 0;

                if ($row->pneumaticoFD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->pneumaticoFD == "Futuro") {
                    $futuro++;
                }elseif ($row->pneumaticoFD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->pneumaticoFDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->dNeumaticoFD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->dNeumaticoFD == "Futuro") {
                    $futuro++;
                }elseif ($row->dNeumaticoFD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->dNeumaticoFDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->presionInfladoFD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->presionInfladoFD == "Futuro") {
                    $futuro++;
                }elseif ($row->presionInfladoFD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->presionInfladoFDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->espesoBalataFD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->espesoBalataFD == "Futuro") {
                    $futuro++;
                }elseif ($row->espesoBalataFD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->espesorBalataFDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->espesorDiscoFD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->espesorDiscoFD == "Futuro") {
                    $futuro++;
                }elseif ($row->espesorDiscoFD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->espesorDiscoFDcambio == "1") {
                    $cambioParte++;
                }
            }

            //Almacenamos los registros
            $datos["aprobado"][$i] += $aprobado;
            $datos["futuro"][$i] += $futuro;
            $datos["inmediato"][$i] += $inmediato;
            $datos["cambioParte"][$i] += $cambioParte;
        }

        for ($i=0; $i <count($datos["idRegistros"]) ; $i++) {
            $query_2 = $this->mConsultas->get_result("idOrden",$datos["idRegistros"][$i],"trasero_derecho");
            foreach ($query_2 as $row){
                //Inicializamos contadores
                $aprobado = 0;
                $futuro = 0;
                $inmediato = 0;
                $cambioParte = 0;

                if ($row->pneumaticoTD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->pneumaticoTD == "Futuro") {
                    $futuro++;
                }elseif ($row->pneumaticoTD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->pneumaticoTDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->dNeumaticoTD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->dNeumaticoTD == "Futuro") {
                    $futuro++;
                }elseif ($row->dNeumaticoTD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->dNeumaticoTDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->presionInfladoTD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->presionInfladoTD == "Futuro") {
                    $futuro++;
                }elseif ($row->presionInfladoTD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->presionInfladoTDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->espesoBalataTD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->espesoBalataTD == "Futuro") {
                    $futuro++;
                }elseif ($row->espesoBalataTD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->espesorBalataTDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->espesorDiscoTD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->espesorDiscoTD == "Futuro") {
                    $futuro++;
                }elseif ($row->espesorDiscoTD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->espesorDiscoTDcambio == "1") {
                    $cambioParte++;
                }

                if ($row->diametroTamborTD == "Aprobado") {
                    $aprobado++;
                }elseif ($row->diametroTamborTD == "Futuro") {
                    $futuro++;
                }elseif ($row->diametroTamborTD == "Inmediato") {
                    $inmediato++;
                }

                if ($row->diametroTamborTDcambio == "1") {
                    $cambioParte++;
                }
            }

            //Almacenamos los registros
            $datos["aprobado"][$i] += $aprobado;
            $datos["futuro"][$i] += $futuro;
            $datos["inmediato"][$i] += $inmediato;
            $datos["cambioParte"][$i] += $cambioParte;
        }

        for ($i=0; $i <count($datos["idRegistros"]) ; $i++) {
            $query_2 = $this->mConsultas->get_result("idOrden",$datos["idRegistros"][$i],"multipunto_general_3");
            foreach ($query_2 as $row){
                //Inicializamos contadores
                $aprobado = 0;
                $futuro = 0;
                $inmediato = 0;
                $cambioParte = 0;

                if ($row->presion == "Aprobado") {
                    $aprobado++;
                }elseif ($row->presion == "Futuro") {
                    $futuro++;
                }elseif ($row->presion == "Inmediato") {
                    $inmediato++;
                }

                if ($row->presionCambio == "1") {
                    $cambioParte++;
                }

            }

            //Almacenamos los registros
            $datos["aprobado"][$i] += $aprobado;
            $datos["futuro"][$i] += $futuro;
            $datos["inmediato"][$i] += $inmediato;
            $datos["cambioParte"][$i] += $cambioParte;
        }

        // $respuesta = json_encode($datos);
        // echo $respuesta;
        //Sacamos los totales de cada registro (deberia ser el mismo numero)
        for ($i=0; $i <count($datos["idRegistros"]) ; $i++) {
            $x = $datos["aprobado"][$i] + $datos["futuro"][$i] + $datos["inmediato"][$i];
            $datos["totalItems"][] = $x;
        }

        $this->loadAllView($datos,'multipunto/Analisis/resumenItems');
    }


    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                // 'assets/js/superUsuario.js'
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Analisis Multipunto"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
