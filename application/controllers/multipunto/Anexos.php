<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\SMTP;
//use PHPMailer\PHPMailer\Exception;

require_once APPPATH . '/third_party/PHPMailer/src/Exception.php';
require_once APPPATH . '/third_party/PHPMailer/src/PHPMailer.php';
require_once APPPATH . '/third_party/PHPMailer/src/SMTP.php';

class Anexos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //Cargamos la libreria de email
        //$this->load->library('email');
        //$this->load->library('phpmailer_lib');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
    }

    public function index($algo = "",$datos = NULL)
    {
        if (!isset($datos["mensaje"])) {
            $datos["mensaje"] = "5";
        }

        //Verificamos y cargamos las ordenes de servicio sin cita
        //Tecnico especifico
        if ($this->session->userdata('rolIniciado')) {
          //Solo tecnicos
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {
                $id = $this->session->userdata('idUsuario');
                $registros = $this->mConsultas->dataMultipuntoTec($id); 
            }else {
                $registros = $this->mConsultas->dataMultipunto(); 
            }
            $id = $this->session->userdata('idUsuario');
        } else {
            $registros = NULL;
        }

        if ($registros != NULL) {
            foreach ($registros as $row) {
                $datos["idOrden"][] = $row->orden;
                $datos["id_cita_url"][] = $this->encrypt($row->orden);
                $datos["idIntelisis"][] = $row->folioIntelisis;
                
                $fecha = explode(" ",$row->fechaCaptura);
                $fechaFracc = explode("-",$fecha[0]);
                $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
                $datos["comentario"][] = $row->comentario;
                $datos["sistema"][] = $row->sistema;
                $datos["componente"][] = $row->componente;
                $datos["causaRaiz"][] = $row->causaRaiz;
                $datos["asesor"][] = $row->asesor;
                $datos["tecnico"][] = $row->tecnico;
                $datos["nombreCliente"][] = $row->nombreCliente;
            }
        }

        $this->loadAllView($datos,'multipunto/listaMultipuntos');
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Recuperamos los datos y guardamos el comentario
    public function saveComment()
    {
          $respuesta = "";
          $idCita = $_POST["idOrden"];
          $comentario = $_POST["comentario"];
          $sistema = $_POST["sistema"];
          $componente = $_POST["componente"];
          $causa = $_POST["causa"];

          if ($idCita != "") {
            $query = $this->mConsultas->get_result("orden",$idCita,"multipunto_general");
            foreach ($query as $row){
                $idRegistro = $row->id;
            }

              $datos = array(
                  'comentario' => $comentario,
                  'sistema' => $sistema,
                  'componente' => $componente,
                  'causaRaiz' => $causa,
              );

             $actualiza = $this->mConsultas->update_table_row('multipunto_general_3',$datos,'idOrden',$idRegistro);

             if ($actualiza) {
                  $this->loadHistory($idCita,"Actualización HM comentario","Hoja Multipuntos)");
                  $respuesta = "OK";
             }else {
                  $respuesta = "ERROR";
             }
          }else {
              $respuesta = "ERROR01";
          }

         echo $respuesta;
    }

    public function filtroHM()
    {
        $id_cita = $_POST['campo'];
        $cadena = "";

        //Verificamos y cargamos las ordenes de servicio sin cita
        //Tecnico especifico
        if ($this->session->userdata('rolIniciado')) {
          //Solo tecnicos
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {
                $id = $this->session->userdata('idUsuario');
                $registros = $this->mConsultas->busquedaMultipuntoTec($id,$id_cita);
            }else {
                $registros = $this->mConsultas->busquedaMultipunto($id_cita); 
            }
        } else {
            $registros = NULL;
        }

        if ($registros != NULL) {
            foreach ($registros as $row) {
                $cadena .= $row->orden."=";
                $cadena .= $row->folioIntelisis."=";
                $cadena .= $row->nombreCliente."=";
                $cadena .= $row->asesor."=";
                $cadena .= $row->tecnico."=";
                //$fecha = explode(" ",$row->fechaCaptura);
                //$fechaFracc = explode("-",$fecha[0]);
                //$cadena .= $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
                $cadena .= $row->sistema."=";
                $cadena .= $row->componente."=";
                $cadena .= $row->causaRaiz."=";
                $cadena .= $row->comentario."=";
                $cadena .= $this->encrypt($row->orden)."=";
                $cadena .= "|";
            }
        }

        echo $cadena;
    }

    public function ver_plantilla()
    {
        $this->load->view("plantillaEmailSohex",NULL);
    }

    public function prueba_correo_2($id_cita='')
    {
        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","viernes","Sábado");
        $datosEmail["dia"] = $dias[date("w")];
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $datosEmail["mes"] = $meses[date("n")-1];

        //Url del archivo
        $datosEmail["url"] = base_url()."Multipunto_PDF/".$this->encrypt($id_cita);
        $datosEmail["formularioEnvio"] = "Hoja Multipuntos";
        //Cargamos la informacion en la plnatilla
        $htmlContent = $this->load->view("plantillaEmailSohex", $datosEmail, TRUE);


        //La creación de instancias y el paso de 'verdadero' habilita excepciones
        // PHPMailer object
        //$mail = $this->phpmailer_library->load();
        $mail = new PHPMailer\PHPMailer\PHPMailer();
        $mail->SMTPDebug = 2; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages

        try {
            //Enviar usando SMTP
            $mail->isSMTP();
            //Enable SMTP debugging
            //SMTP::DEBUG_OFF = off (for production use)
            //SMTP::DEBUG_CLIENT = client messages
            //SMTP::DEBUG_SERVER = client and server messages
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;

            //Configure el servidor SMTP para enviar
            $mail->Host = 'smtp.gmail.com';
            //Use `$mail->Host = gethostbyname('smtp.gmail.com');`
            //if your network does not support SMTP over IPv6,
            //though this may cause issues with TLS
            // Establecer el número de puerto SMTP - 587 para TLS autenticado, A.K.A. RFC4409 SMTP
            $mail->Port = 587;
            // Establezca el mecanismo de cifrado para usar - StartTLS o SMTPS
            //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->SMTPSecure = 'tls';
            //Habilitar la autenticación SMTP
            $mail->SMTPAuth = true;
            // Nombre de usuario para usar para la autenticación SMTP: use la dirección de correo electrónico completa para Gmail
            $mail->Username = EMAIL_SERVER;
            // Contraseña para usar para la autenticación SMTP 
            $mail->Password = EMAIL_PASS;

            //Cuerpo del correo
            $mail->setFrom(EMAIL_SERVER, SUCURSAL);
            //$mail->addReplyTo('no-reply@sohexotificaciones.com', 'Notificacion Automatica');
            
            // Add a recipient
            $mail->addAddress('desarrollo@sohex.mx','Testeo');
            
            // con copia y con copia oculta
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('contactoplasenciamatriz@gmail.com','Contacto Plasencia Motors Guadalajara');

            // Asunto del correo
            $mail->Subject = 'Notificación PDF Hoja Multipunto';
            
            // Set email format to HTML
            $mail->isHTML(true);
            
            // Email body content
            $mailContent = $htmlContent;
            $mail->Body = $mailContent;
            
            // Send email
            if(!$mail->send()){
                echo 'El mensaje no se pudo ser enviado.<br>';
                echo 'Error de envio: ' . $mail->ErrorInfo;
            }else{
                echo 'Correo enviado correctamente';
            }
        } catch (Exception $e) {
            echo "El mensaje no se pudo ser enviado. Error de correo: ". $mail->ErrorInfo;
        }
    }

    //Enviamos correo de prueba de la multipunto
    public function pruebaCorreo($idOrden = 0)
    {
        //Definimos la fecha de envio
        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","viernes","Sábado");
        $datosEmail["dia"] = $dias[date("w")];
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $datosEmail["mes"] = $meses[date("n")-1];

        //Url del archivo
        $datosEmail["url"] = base_url()."Multipunto_PDF/".$this->encrypt($idOrden);
        $datosEmail["formularioEnvio"] = "Hoja Multipuntos";
        //Cargamos la informacion en la plnatilla
        $htmlContent = $this->load->view("plantillaEmail", $datosEmail, TRUE);

        //Configuramos el correo QUERÉTARO
        $configGmail = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => EMAIL_SERVER,
            'smtp_pass' => EMAIL_PASS,
            'mailtype' => 'html',
            'smtp_crypto'=> 'tls',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );

        //Establecemos esta configuración
        $this->email->initialize($configGmail);
        //Ponemos la dirección de correo que enviará el email y un nombre
        $this->email->from(EMAIL_SERVER, SUCURSAL);
        //Ponemos la direccion de aquien va dirigido
        $this->email->to('shara.valdovinos.1994@gmail.com','Shara');
        //Multiples destinos
        // $this->email->to($correo1, $correo2, $correo3);
        //Con copia a:
        // $this->email->cc($correo);
        //Con copia oculta a:
        // $this->email->bcc($correo);
        // $this->email->bcc('shara.valdovinos.1994@gmail.com','Shara');
        //Colocamos el asunto del correo
        $this->email->subject('Hoja Multipuntos');
        //Cargamos la plantilla del correo
        $this->email->message($htmlContent);
        //Enviar archivo adjunto
        // $this->email->attach('files/attachment.pdf');
        //Enviamos el correo
        var_dump($this->email->send());
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                // 'assets/js/firmaCliente.js',
                'assets/js/Multipunto/buscadorHM.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Lista Hoja Multipunto"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
