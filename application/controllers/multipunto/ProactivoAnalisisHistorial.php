<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class ProactivoAnalisisHistorial extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);

        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);
    }

    public function index()
    {
      // code...
    }

    //Listado de cotizaciones rechazadas
    public function cotizacionesRechazadas($x = 0, $datos = NULL)
    {
        //Verificamos que existan presupuestos nuevos
        /*$query = $this->mConsultas->presupuesto_proactivo();  
        foreach ($query as $row){
            if ($row->acepta_cliente == "Val") {
                $datos["idOrden"][] = $row->id_cita;
                $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                $datos["idIntelisis"][] = $row->folioIntelisis;

                $datos["asesor"][] = $row->asesor;
                $datos["cliente"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;

                $fecha = explode("-",$row->fecha_recepcion);
                $datos["ultimaFecha"][] = $fecha[2]."/".$fecha[1]."/".$fecha[0];

                $datos["estatus"][] = "2";
                $datos["bandera"][] = "2";

                if ($row->identificador == "M") {
                    $datos["origen"][] = "MULTIPUNTO";
                }else{
                    $datos["origen"][] = "ADICIONAL";
                }

                $datos["telefono"][] = "Tel. 1: ".$row->telefono_movil ."<br>Tel. 2: ".$row->otro_telefono;

                $datos["identificador"][] = $row->identificador;
                $datos["serie"][] = $row->serie;

                $datos["idProactivo"][] = '99'.$row->id_cita; 
            } else{
                $datos["idOrden"][] = $row->id_cita;
                $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                $datos["idIntelisis"][] = $row->folioIntelisis;
                $datos["asesor"][] = $row->asesor;
                $datos["cliente"][] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;

                $fecha = explode("-",$row->fecha_recepcion);
                $datos["ultimaFecha"][] = $fecha[2]."/".$fecha[1]."/".$fecha[0];

                $datos["estatus"][] = "1";
                $datos["bandera"][] = "2";

                if ($row->identificador == "M") { 
                    $datos["origen"][] = "MULTIPUNTO";
                }else{
                    $datos["origen"][] = "ADICIONAL";
                }

                $datos["telefono"][] = "Tel. 1: ".$row->telefono_movil ."<br>Tel. 2: ".$row->otro_telefono;

                $datos["identificador"][] = $row->identificador;
                $datos["serie"][] = $row->serie;

                $datos["idProactivo"][] = '99'.$row->id_cita; 
            }
        }

        $this->loadAllView($datos,'modulosExternos/Proactivo/cotizacionRechazada'); */
        $mes_actual = date("m");
        $fecha_busqueda = (($mes_actual <= 4) ? (date("Y")-1) : date("Y"))."-01-01";

        $datos["presupuestos"] = $this->mConsultas->proactivo_list_rechazado($fecha_busqueda);

        $this->loadAllView($datos,'modulosExternos/Proactivo/presupuesto_rechazado_2');
    }

    //Parada comodin para visualizar la cotizacion multipunto
    public function comodinVista_M($idOrden = 0,$datos = NULL)
    {
        $datos["identificador"] = "M";
        $this->setDataCotizacion($idOrden,$datos);
    }

    //Parada comodin para visualizar la cotizacion adicional
    public function comodinVista_E($idOrden = 0,$datos = NULL)
    {
        $datos["identificador"] = "E";
        $this->setDataCotizacion($idOrden,$datos);
    }

        //Recuperamos la cotizacion para imprimirla
    public function setDataCotizacion($idOrden = 0,$datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["tipoRegistro"])) {
            $datos["tipoRegistro"] = "";
        }

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }

        //Recuperamos el id interno de magic
        $datos["idMagicOrden"] = $this->mConsultas->id_orden_magic($idOrden);
        $datos["idOrden"] = $idOrden;

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == $datos["identificador"]) {
                $datos["registrosControl"] = $this->createCells(explode("|",$row->contenido));
                $datos["registrosControlRenglon"] = explode("|",$row->contenido);

                $datos["subTotalMaterial"] = $row->subTotal;
                $datos["ivaMaterial"] = $row->iva;
                $datos["anticipo"] = $row->anticipo;
                $datos["notaAnticipo"] = $row->notaAnticipo;
                $datos["canceladoMaterial"] = $row->cancelado;
                $datos["totalMaterial"] = $row->total;
                $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";
                $datos["archivo"] = explode("|",$row->archivos);
                $datos["archivoTxt"] = $row->archivos;
                $datos["comentario"] = $row->comentario;
                $datos["comentarioTecnico"] = $row->comentarioTecnico;
                $datos["archivoTecnico"] = explode("|",$row->archivoTecnico);
                $datos["archivoImgTecnico"] = $row->archivoTecnico;
                $datos["aceptoTermino"] = $row->aceptoTermino;
                $datos["notaAsesor"] = $row->notaAsesor;
                
                $datos["refacciones"] = $row->aprobacionRefaccion;
                $datos["statusRef"] = $row->estado;

                //Validaciones para garantias
                $datos["pzaGarantia"] = explode("_", $row->pzaGarantia);
                $datos["autorizaPzaGarantia"] = explode("_", $row->pzaAutorizada);
                $datos["envioGarantia"] = $row->envioGarantia;
                $datos["pzaGarantia_Og"] = $row->pzaGarantia;
                $datos["notaGarantias"] = $row->comentariosGarantias;
                $datos["firmaGarantias"] = (file_exists($row->firmaGarantias)) ? $row->firmaGarantias : "";

                //$datos["tablaPzsGarantias"] = $this->createCells(explode("|",$row->tablaGarantias));
                //$datos["tablaPzsGarantias_rg"] = $row->tablaGarantias;  

                //$datos["clientePaga"] = $row->clientePaga;
                //$datos["fordPaga"] = $row->fordPaga;
                //$datos["totalReparacion"] = $row->totalReparacion;

                //Validaciones para jefe de taller
                $datos["pzsAprobadasJDT"] = explode("_",$row->pzaAprobadas);
                $datos["pzsAprobadasJDT_2"] = $row->pzaAprobadas;
                $datos["envioJDT"] = $row->enviaJDT;
                $datos["firmaJDT"] = (file_exists($row->firmaJDT)) ? $row->firmaJDT : "";

                $datos["envioCorreoProactivo"] = $row->envioCorreoProactivo;
                $datos["correoProactivo"] = $row->correoAdicional;
            }
        }

        $precarga = $this->mConsultas->precargaConsulta($idOrden);
        foreach ($precarga as $row){
            $datos["orden_datos_email"] = $row->datos_email;
            $datos["orden_correo_compania"] = $row->correo_compania;
        }
        $this->loadAllView($datos,"cotizaciones/cliente_proactivo");
    }

    //Proactivo historial por filtro de periodo de tiempo
    public function ajaxHistoriaIndicadores()
    {
        $fecha_1 = $_POST["fecha1"];
        $fecha_2 = $_POST["fecha2"];

        //Identificamos el tipo de filtro (fecha o general)
        if ($fecha_2 == "") {
            //Si se escribio algo en la busqueda general
            $query = $this->mConsultas->multipuntoListaIndicadorFechas($fecha_1);
        }else {
            //Cargamos la información del formulario
            $query = $this->mConsultas->multipuntoListaIndicadorFechas_1($fecha_1,$fecha_2);
        }

        $contenido = "";
        foreach ($query as $row){
             if (($row->bateriaEstado != "Aprobado")||($row->dNeumaticoFI != "Aprobado")||($row->espesoBalataFI != "Aprobado")||($row->espesorDiscoFI != "Aprobado")||($row->dNeumaticoTI != "Aprobado")||($row->espesoBalataTI != "Aprobado")||($row->espesorDiscoTI != "Aprobado")||($row->diametroTamborTI != "Aprobado")||($row->dNeumaticoFD != "Aprobado")||($row->espesoBalataFD != "Aprobado")||($row->espesorDiscoFD != "Aprobado")||($row->dNeumaticoTD != "Aprobado")||($row->espesoBalataTD != "Aprobado")||($row->espesorDiscoTD != "Aprobado")||($row->diametroTamborTD != "Aprobado")) {
            
                // $contenido .= $row->orden."_";
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $contenido .= $serie."_";

                if ($row->fecha_recepcion != NULL) {
                    $fecha = explode("-",$row->fecha_recepcion);
                    $contenido .= $fecha[2]."/".$fecha[1]."/".$fecha[0]."_";
                }else {
                    $contenido .= "_";
                }


                $contenido .= $row->asesor."_";
                $contenido .= $row->nombreCliente."_";
                // $datos["contacto"][] = "Tel. ".$row->datos_telefono."<br>Email:".$row->datos_email;
                $contenido .= $row->datos_telefono."_";

                if ($row->bateriaEstado == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->bateriaEstado == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->espesoBalataFI == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->espesoBalataFI == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->espesorDiscoFI == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->espesorDiscoFI == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->espesoBalataTI == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->espesoBalataTI == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->espesorDiscoTI == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->espesorDiscoTI == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->diametroTamborTI == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->diametroTamborTI == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->espesoBalataFD == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->espesoBalataFD == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->espesorDiscoFD == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->espesorDiscoFD == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->espesoBalataTD == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->espesoBalataTD == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->espesorDiscoTD == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->espesorDiscoTD == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                if ($row->diametroTamborTD == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->diametroTamborTD == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                $contenido .= $row->orden."_";

                //Frente izquierdo
                if ($row->dNeumaticoFI == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->dNeumaticoFI == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                //Parte trasera izquierda
                if ($row->dNeumaticoTI == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->dNeumaticoTI == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                //Frente derecho
                if ($row->dNeumaticoFD == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->dNeumaticoFD == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                //Parte Trasera derecha
                if ($row->dNeumaticoTD == "Aprobado") {
                    $contenido .= "V"."_";
                }elseif ($row->dNeumaticoTD == "Futuro") {
                    $contenido .= "A"."_";
                }else {
                    $contenido .= "R"."_";
                }

                $contenido .= $this->mConsultas->idHistorialProactivo(strtoupper($serie))."_";

                //Piezas cambiadas
                $contenido .= $row->bateriaCambio."_";
                $contenido .= $row->pneumaticoFIcambio."_";
                $contenido .= $row->espesorBalataFIcambio."_";
                $contenido .= $row->espesorDiscoFIcambio."_";
                $contenido .= $row->pneumaticoTIcambio."_";
                $contenido .= $row->espesorBalataTIcambio."_";
                $contenido .= $row->espesorDiscoTIcambio."_";
                $contenido .= $row->diametroTamborTIcambio."_";
                $contenido .= $row->pneumaticoFDcambio."_";
                $contenido .= $row->espesorBalataFDcambio."_";
                $contenido .= $row->espesorDiscoFDcambio."_";
                $contenido .= $row->pneumaticoTDcambio."_";
                $contenido .= $row->espesorBalataTDcambio."_";
                $contenido .= $row->espesorDiscoTDcambio."_";
                $contenido .= $row->diametroTamborTDcambio."_";

                $contenido .= "|";
            }
        }

        echo $contenido;
    }

    public function comentarioMagic()
    {
        //Recuperamos los campos del formulario
        $idHistorial = $_POST["idHistorial"];
        $titulo = $_POST["titulo"];
        $hora = $_POST["hora"];
        $fecha = $_POST["fecha"];
        $comentario = $_POST["comentario"];
        $respuesta = "";

        //Guardamos la informacion en el historial
        $contenedor = array(
            'comentario' => $comentario,
            'id_cita' => $idHistorial,
            'fecha_creacion' => date('Y-m-d H:i:s'),
            'id_usuario'=> ($this->session->userdata('id_usuario')) ? $this->session->userdata('id_usuario') : NULL,
            'fecha_notificacion' => $fecha." ".$hora,
        );

        $idComentario = $this->mConsultas->save_register('historial_citas_llamadas_magic',$contenedor);
        if ($idComentario != 0) {
            //Insertar notificación
            if(($fecha != '') && ($fecha != 'cronoHora')){
                $notific['titulo'] = $titulo;
                $notific['texto']= $comentario;
                $notific['id_user'] = ($this->session->userdata('id_usuario')) ? $this->session->userdata('id_usuario') : NULL;
                $notific['estado'] = 1;
                $notific['tipo_notificacion'] = 2;
                $notific['fecha_hora'] = $fecha.' '.$hora;
                //$notific['url'] = base_url().'index.php/bodyshop/ver_proyecto/'.$this->input->post('idp');
                $notific['estadoWeb'] = 1;

                $idNotifi = $this->Verificacion->save_register('noti_user',$notific);
                if ($idNotifi != 0) {
                    $respuesta = "OK";
                }else {
                    $respuesta = "ERROR 02";
                }
            }else {
                $respuesta = "OK";
            }
        }else {
            $respuesta = "ERROR 01";
        }

        echo $respuesta;
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Cargamos el historial de comnetarios de magic
    public function loadHistoryMagic()
    {
        $idHistorial = $_POST["idHistorial"];

        $cadena = "";

        //Recuperamos la informacion de los comentarios
        $query = $this->mConsultas->get_result("id_cita",$idHistorial,"historial_citas_llamadas_magic");
        foreach ($query as $row){
            $cadena .=  $row->fecha_creacion."_";
            $cadena .=  $row->comentario."_";
            $cadena .=  $row->fecha_notificacion."_";
            $cadena .= "|";
        }

        echo $cadena;
    }

    //Busqueda de fechas para presupuestos no autorizados
    public function filtroPorFechas()
    {
        $fecha_inicio = $_POST["fecha_inicio"];
        $fecha_fin = $_POST["fecha_fin"];
        $campo = $_POST["campo"];

        //Si no se envio un campo de consulta
        if ($campo == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->quotesList_Fecha($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->quotesList_Fechas_1($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->quotesList_campo($campo);
            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->quotesList_fechaCampo($campo,$fecha_inicio,$fecha_fin);
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->quotesList_fechaCampo_1($campo,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->quotesList_fechaCampo_1($campo,$fecha_fin);
                }
            }
        }

        //var_dump ($precarga);
        
        $cadena = "";

        foreach ($precarga as $row) {
            if ($row->aceptoTermino == "Val") {
                $revision = strrpos($row->contenido, "Rechazada");
                if ($revision === TRUE) {
                    // 0 idOrden
                    $cadena .= $row->id_cita."=";
                    // 1 asesor
                    $cadena .= $row->asesor."=";
                    // 2 cliente
                    $cadena .= $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno."=";
                    // 3 ultimaFecha
                    $fecha = explode("-",$row->fecha_recepcion);
                    $cadena .= $fecha[2]."/".$fecha[1]."/".$fecha[0]."=";
                    // 4 estatus
                    $cadena .= "2"."=";

                    // ---5 origine
                    //if ($row->identificador == "M") {
                    //    $cadena .= "MULTIPUNTO";
                    //}else{
                    //    $cadena .= "ADICIONAL";
                    //}

                    // 5 telefono
                    $cadena .= "Tel. 1: ".$row->telefono_movil ."<br>Tel. 2: ".$row->otro_telefono."=";
                    // 6 identificador
                    $cadena .= $row->identificador."=";
                    
                    $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                    // 7 Serie
                    $cadena .= $serie ."=";
                    // 8 idProactivo
                    $cadena .= $cadena .= '99'.$row->id_cita."="; //$this->mConsultas->idHistorialProactivo(strtoupper($serie))."=";
                    // 9 folio intelisis
                    $cadena .= $row->folioIntelisis."=";
                    // 10
                    $cadena .= $this->encrypt($row->id_cita)."=";
                    $cadena .= $serie ."|";
                }
            } else {
                // 0 idOrden
                $cadena .= $row->id_cita."=";
                // 1 asesor
                $cadena .= $row->asesor."=";
                // 2 cliente
                $cadena .= $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno."=";
                // 3 ultimaFecha
                $fecha = explode("-",$row->fecha_recepcion);
                $cadena .= $fecha[2]."/".$fecha[1]."/".$fecha[0]."=";
                // 4 estatus
                $cadena .= "1"."=";

                // ---5 origine
                //if ($row->identificador == "M") {
                //    $cadena .= "MULTIPUNTO";
                //}else{
                //    $cadena .= "ADICIONAL";
                //}

                // 5 telefono
                $cadena .= "Tel. 1: ".$row->telefono_movil ."<br>Tel. 2: ".$row->otro_telefono."=";
                // 6 identificador
                $cadena .= $row->identificador."=";
                
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;            
                // 7 Serie
                $cadena .= $serie ."=";
                // 8 idProactivo
                $cadena .= '99'.$row->id_cita."=";//$this->mConsultas->idHistorialProactivo(strtoupper($serie))."=";
                // 9 folio intelisis
                $cadena .= $row->folioIntelisis."=";
                //10
                $cadena .= $this->encrypt($row->id_cita)."=";
                $cadena .= $serie ."|";
            }
            
        }

        echo $cadena;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrasVistas/buscadorVarios.js',
                'assets/js/otrasVistas/buscadorPresupuesto.js'
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Analisis Indicadores"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
