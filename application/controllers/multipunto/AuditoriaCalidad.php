<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class AuditoriaCalidad extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

  	public function index($idOrden = 0,$datos = NULL)
  	{
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $datos["idOrden"] = $id_cita;

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        $this->loadAllView($datos,'audCalidad/calidad_alta');
  	}

    //Validamos los campos
    public function validateForm()
    {
        $datos["vista"] = $this->input->post("modoVistaFormulario");
        $datos["idOrden"] = $this->input->post("noOrden");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idOrden"]);

        //Validaciones de campos ingresados
        $this->form_validation->set_rules('proxServicio', '(Falta indicar)', 'required');
        $this->form_validation->set_rules('fechaAproxServ', '(Falta indicar)', 'required');

        $this->form_validation->set_rules('unidadRechazada', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('unidadAprobada', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('nombreMecanico', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('nombreAuditor', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('nombreJefeTaller', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('firmaJefeTaller', 'Falta indicar.', 'required');
        $this->form_validation->set_rules('fechaRegistro', 'Falta indicar.', 'required');

        if ($datos["vista"] == "1") {
            $this->form_validation->set_rules('noOrden', 'Falta indicar.', 'required|callback_existencia');
        } else {
            $this->form_validation->set_rules('noOrden', 'Falta indicar.', 'required');
        }

        //Lado Izquierdo del formulario
        //Trabajo de afinacion
        /*$this->form_validation->set_rules('afinacion_1[]', '', 'callback_checks');
        $this->form_validation->set_rules('afinacion_2[]', '', 'callback_checks');
        $this->form_validation->set_rules('afinacion_3[]', '', 'callback_checks');
        $this->form_validation->set_rules('afinacion_4[]', '', 'callback_checks');
        $this->form_validation->set_rules('afinacion_5[]', '', 'callback_checks');
        $this->form_validation->set_rules('afinacion_6[]', '', 'callback_checks');*/

        //Trabajo de frenos
        /*$this->form_validation->set_rules('t_frenos_1[]', '', 'callback_checks');
        $this->form_validation->set_rules('t_frenos_2[]', '', 'callback_checks');
        $this->form_validation->set_rules('t_frenos_3[]', '', 'callback_checks');
        $this->form_validation->set_rules('t_frenos_4[]', '', 'callback_checks');
        $this->form_validation->set_rules('t_frenos_5[]', '', 'callback_checks');*/

        //Trabajo de alineación
        /*$this->form_validation->set_rules('alineacion_1[]', '', 'callback_checks');
        $this->form_validation->set_rules('alineacion_2[]', '', 'callback_checks');
        $this->form_validation->set_rules('alineacion_3[]', '', 'callback_checks');
        $this->form_validation->set_rules('alineacion_4[]', '', 'callback_checks');
        $this->form_validation->set_rules('alineacion_5[]', '', 'callback_checks');
        $this->form_validation->set_rules('alineacion_6[]', '', 'callback_checks');*/

        //Trabajo de AA
        /*$this->form_validation->set_rules('trabajoAA_1[]', '', 'callback_checks');
        $this->form_validation->set_rules('trabajoAA_2[]', '', 'callback_checks');
        $this->form_validation->set_rules('trabajoAA_3[]', '', 'callback_checks');
        $this->form_validation->set_rules('trabajoAA_4[]', '', 'callback_checks');*/

        //Lado Derecho del formulario
        // Revision general
        /*$this->form_validation->set_rules('general_1[]', '', 'callback_checks');
        $this->form_validation->set_rules('general_2[]', '', 'callback_checks');
        $this->form_validation->set_rules('general_3[]', '', 'callback_checks');
        $this->form_validation->set_rules('general_4[]', '', 'callback_checks');
        $this->form_validation->set_rules('general_5[]', '', 'callback_checks');
        $this->form_validation->set_rules('general_6[]', '', 'callback_checks');*/

        //Trabajos transmision automatica
        /*$this->form_validation->set_rules('transmision_auto_1[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_2[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_3[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_4[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_5[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_6[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_7[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_8[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_9[]', '', 'callback_checks');
        $this->form_validation->set_rules('transmision_auto_10[]', '', 'callback_checks');*/

        //Trabajos de lavado
        /*$this->form_validation->set_rules('lavado_1[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_2[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_3[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_4[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_5[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_6[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_7[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_8[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_9[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_10[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_11[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_12[]', '', 'callback_checks');
        $this->form_validation->set_rules('lavado_13[]', '', 'callback_checks');*/

        //Menajes de error
        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('checks','Falta indicar');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar el No. de Orden.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";
            $this->index($datos["idOrden"],$datos);
        }
    }

    //Validamos los cheks seleccionados
    public function checks($grupo = "")
    {
        $return = FALSE;
        if ($grupo) {
            $return = TRUE;
        }
        return $return;
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("noOrden");

        //Comprobamos que no hayan enviado en 0 el campo
        if ($idOrden != "0") {
            //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
            $query = $this->mConsultas->get_result("idOrden",$idOrden,"auditoriacalidad");
            foreach ($query as $row){
                $dato = $row->idRegistro;
            }

            //Interpretamos la respuesta de la consulta
            if (isset($dato)) {
                //Si existe el dato, entonces ya existe el registro
                $respuesta = FALSE;
            }else {
                //Si no existe el dato, entonces no existe el registro
                $respuesta = TRUE;
            }
        }

        return $respuesta;
    }

    //Recuperamos los campos
    public function getDataForm($datos = NULL)
    {
        if ($datos["vista"] == "1") {
            $contenido = array(
                'idRegistro' => NULL,
                'idOrden' => $this->input->post("noOrden"),
            );
        }

        //Trabajo de afinacion
        $contenido["afinacion"] = $this->createString("afinacion",7);
        //Trabajo de frenos
        $contenido["frenos"] = $this->createString("t_frenos",6);
        //Trabajo de alineación
        $contenido["alineacion"] = $this->createString("alineacion",7);
        //Trabajo de AA
        $contenido["trabajoAA"] = $this->createString("trabajoAA",5);
        // Revision general
        $contenido["general"] = $this->createString("general",7);
        //Trabajos transmision automatica
        $contenido["transmision"] = $this->createString("transmision_auto",11);
        //Trabajos de lavado
        $contenido["lavado"] = $this->createString("lavado",14);

        //Datos para el proximo servicio
        $contenido["proxServ"] = $this->input->post("proxServicio");
        $contenido["fechaProxServ"] = $this->input->post("fechaAproxServ");

        //Datos de quien elaboran la auditoria
        $registro = date("Y-m-d H:i");
        $contenido["unidadRechazada"] = $this->input->post("unidadRechazada");
        $contenido["unidadAprobada"] = $this->input->post("unidadAprobada");
        $contenido["nombreMecanico"] = $this->input->post("nombreMecanico");
        $contenido["nombreAuditor"] = $this->input->post("nombreAuditor");
        $contenido["nombreJefeTaller"] = $this->input->post("nombreJefeTaller");
        $contenido["firmaJefeTaller"] = $this->base64ToImage($this->input->post("firmaJefeTaller"),"firmas");
        $contenido["comentario"] = $this->input->post("comentario");
        $contenido["fechaRegistro"] = $this->input->post("fechaRegistro");

        if ($datos["vista"] == "1") {
            $contenido["fechaAlta"] = $registro;
        }

        $contenido["fechaActualiza"] = $registro;

        //Para registro de alta
        if ($datos["vista"] == "1") {
            $registroDB = $this->mConsultas->save_register('auditoriacalidad',$contenido);
            if ($registroDB != 0) {
                $datos["mensaje"] = "1";
            }else {
                $datos["mensaje"] = "2";
            }

            // $this->index($datos["idOrden"],$datos);
            redirect(base_url().'Auditoria_Calidad_Lista/'.$datos["mensaje"]);
        //Para registro que se actualizan
        } else {
            $actualizar = $this->mConsultas->update_table_row('auditoriacalidad',$contenido,'idOrden',$datos["idOrden"]);
            if ($actualizar) {
                $datos["mensaje"]="1";
            }else {
                $datos["mensaje"]="2";
            }

            // $this->index($datos["idOrden"],$datos);
            redirect(base_url().'Auditoria_Calidad_Lista/'.$datos["mensaje"]);
        }
    }

    //Creamos cadenas para guardar registro
    public function createString($campo = "",$limite = 1)
    {
        $cadena = "";
        for ($i=1; $i <$limite ; $i++) {
            $elemento = $this->input->post($campo."_".$i);
            $cadena .= $this->validateRadioCheck($elemento[0],"-")."|";
        }
        return $cadena;
    }

    //Validar Radio
    public function validateRadioCheck($campo = NULL,$elseValue = "")
    {
        if ($campo[0] == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Listamos los registros
    public function comodinLista($x = "",$datos = NULL)
    {
        if (!isset($datos["indicador"])) {
            $datos["indicador"] = "";
        }
        $datos["mensaje"] = $x;

        $mes_actual = date("m");
        if ($mes_actual <= 1) {
            $mes = '09';
        }else{
            if (($mes_actual - 5) < 10) {
                $mes = '0'.($mes_actual - 5);
            }else{
                $mes = $mes_actual - 5;
            }
        }

        $fecha_busqueda = (($mes_actual <= 1) ? (date("Y")-1) : date("Y"))."-".$mes."-01";
        //echo $fecha_busqueda."<br>";
        //$fecha_busqueda = (date("Y")-1)."-".$mes."-01";
        //echo $fecha_busqueda;
        $datos["contenido"] = $this->mConsultas->listado_audotoriacalidad($fecha_busqueda);
        //var_dump($datos);
        $this->loadAllView($datos,'audCalidad/calidad_lista_2');
    }

    //Comodin para pdf
    public function setDataPDF($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Recuperamos la informacion de un registro
    public function setData($idOrden = 0,$datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["tipoRegistro"])) {
            $datos["tipoRegistro"] = "";
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"auditoriacalidad");
        foreach ($query as $row){
            $datos["idOrden"] = $row->idOrden;
            $datos["afinacion"] = explode("|",$row->afinacion);
            $datos["frenos"] = explode("|",$row->frenos);
            $datos["alineacion"] = explode("|",$row->alineacion);
            $datos["trabajoAA"] = explode("|",$row->trabajoAA);
            $datos["general"] = explode("|",$row->general);
            $datos["transmision"] = explode("|",$row->transmision);
            $datos["lavado"] = explode("|",$row->lavado);
            $datos["proxServ"] = $row->proxServ;

            $fechaFracc = explode("-",$row->fechaProxServ);
            $datos["fechaProxServ"] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
            $datos["unidadRechazada"] = $row->unidadRechazada;
            $datos["unidadAprobada"] = $row->unidadAprobada;
            $datos["nombreMecanico"] = $row->nombreMecanico;
            $datos["nombreAuditor"] = $row->nombreAuditor;
            $datos["nombreJefeTaller"] = $row->nombreJefeTaller;
            $datos["comentario"] = $row->comentario;
            $datos["firmaJefeTaller"] = (file_exists($row->firmaJefeTaller)) ? $row->firmaJefeTaller : "";

            $fechaFracc_2 = explode("-",$row->fechaRegistro);
            $datos["fechaRegistro"] = $fechaFracc_2[2]."/".$fechaFracc_2[1]."/".$fechaFracc_2[0];

        }

        $this->generatePDF($datos);
    }

    // Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_AC";
        for($i=0; $i<=6; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    public function buscar_registro()
    {
        $campo = $_POST["campo"];
        //$fecha_ini = $_POST["fecha_inio"];
        //$fecha_fin = $_POST["fecha_fin"];
        
        $busqueda = $this->mConsultas->busqueda_auditoria($campo);

        $contenedor = [];
        foreach ($busqueda as $row) {
            $fecha = new DateTime($row->fechaRegistro.' 00:00:00');
            $fecha_2 = new DateTime($row->fechaProxServ.' 00:00:00');

            $contenedor[] = array(
                "tecnico" => $row->tecnico,
                "vehiculo" => $row->vehiculo,
                "placas" => $row->placas,
                "asesor" => $row->asesor,
                "id_cita" => $row->id_cita,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "serie" => $row->serie,
                "auditor" => $row->nombreAuditor,
                "jefetaller" => $row->nombreJefeTaller,
                "fecha_registro" => $fecha->format('d-m-Y'),
                "fecha_prox" => $fecha_2->format('d-m-Y'),
                "folio_intelisis" => $row->folio_externo,
            );
        }

        echo json_encode( $contenedor );
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("audCalidad/calidad_pdf", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
              'assets/js/calidad/interaccionesCheck.js',
              'assets/js/calidad/firma_calidad.js',
              'assets/js/Multipunto/precarga_form.js',
            )
        );

        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Auditoría de Calidad"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
