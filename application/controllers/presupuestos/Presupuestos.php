<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Presupuestos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL); 
        
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);
    }

    //Recibimos el id de la cita actual y decidimos a donde se va a enviar
    public function index($orden = 0,$datos = NULL)
    {
        //Recuperamos el rol identificado de la operacion
        if ($this->session->userdata('rolIniciado')) {
            $datos["dirige"] = $this->session->userdata('rolIniciado');
        }else {
            $datos["dirige"] = "";
        }

        $id_cita = ((ctype_digit($orden)) ? $orden : $this->decrypt($orden)); 

        //Recuperamos el id interno /folio dms
        //$datos["orden_CDK"] = $this->mConsultas->orden_cdk_query($id_cita);

        //Consultamos su ya se ha generado alguna cotizacion para ese presupuesto
        $revision = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $idCotizacion = $row->id;
            $acepta_cliente = $row->acepta_cliente;
        }

        //Si existe una cotizacion generada
        if (isset($idCotizacion)) {
            //Consultamos si ya se entrego la unidad
            //$datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($id_cita);

            //Si la cotizacion ya fue afectada por el cliente, confirmamos que quien la vera, sera le cliente
            if ($acepta_cliente != "") {
                //Si no existe un destino de vista , se asume que es la vista del cliente
                if (!isset($datos["destinoVista"])) {
                    $datos["destinoVista"] = "Cliente";
                }
                $this->setData($id_cita,$datos);
            }else {
                $this->setData($id_cita,$datos);
            }
        } else {
            //Cargamos datos adicionales
            $precarga = $this->mConsultas->precargaConsulta($id_cita);
            foreach ($precarga as $row){
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $datos["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $datos["folio_externo"] = $row->folioIntelisis; 
            }

            if (!isset($datos["folio_externo"])) {
                $datos["folio_externo"] = $this->mConsultas->id_orden_intelisis($id_cita);
                $datos["serie"] = "";
            } 

            $datos['id_cita'] = $id_cita;
            $this->loadAllView($datos,'presupuestos/presupuesto_alta');
        }
    }

    //Validamos el formulario a guardar
    public function validateFormR2()
    {
        //Destino de la visata
        $datos["vista"] = $this->input->post("modoVistaFormulario");
        //id de la cita asociada
        $datos["id_cita"] = $this->input->post("idOrdenTemp");
        //Dato identificador
        $datos["tipoRegistro"] = $this->input->post("tipoRegistro");
        //
        $datos["modoGuardado"] = $this->input->post("modoGuardado");
        //Perfil que accede a la vista
        $datos["dirige"] = $this->input->post("dirige");
        $datos["folio_externo"] = $this->input->post("folio_externo");
        $datos["destinoMensaje"] = $this->input->post("aceptarCotizacion");
        //Identificamos el tipo de presupuesto que se evalua
        $datos["presupuesto_bandera"] = $this->input->post("destino_vista");

        //Recuperamos el id interno de magic
        //$datos["orden_CDK"] = $this->mConsultas->orden_cdk_query($datos["id_cita"]);
        
        if ($datos["vista"] == "1") {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden es requerido.', 'required|callback_existencia|callback_existeOrden|callback_multipunto');
            //$this->form_validation->set_rules('idOrdenTemp', 'No. Orden es requerido.', 'required|callback_existencia|callback_existeOrden');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        //Validamos los elementos de garantias
        }elseif ($datos["vista"] == "5") {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Firma del encargado de garantias
            $this->form_validation->set_rules('rutaFirmaGarantia', 'Falta firma del encargado de Garantías.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');
            //Comprobamos que se haya seleccionado al menos una refaccion
            //$this->form_validation->set_rules('autoriza_garantia', 'Se debe seleccional al menos una refacción', 'callback_garantias');

        //Validamos los elementos de jefe de taller
        }elseif ($datos["vista"] == "4") { 
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Firma del jefe de taller
            $this->form_validation->set_rules('rutaFirmaJDT', 'Falta firma del encargado de Garantías.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');
            //Comprobamos que se haya seleccionado al menos una refaccion
            $this->form_validation->set_rules('autoriza_jdt', 'Se debe seleccional al menos una refacción', 'callback_jefetaller');

        //Validamos formulario de firma de requisicion
        }elseif ($datos["vista"] == "9") { 
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Firma del tecnico
            $this->form_validation->set_rules('rutaFirmaReq', 'Falta firma el técnico.', 'required');

        //Validamos los elementos del asesor
        }elseif ($datos["vista"] == "2") { 
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Firma del jefe de taller
            //$this->form_validation->set_rules('rutaFirmaAsesorCostos', 'Falta firma del encargado de Garantías.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');
            //Comprobamos que se haya seleccionado al menos una refaccion
            $this->form_validation->set_rules('autoriza_asesor', 'Se debe seleccional al menos una refacción', 'callback_asesor');

        }else {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('registros','No se ha guardado ningun elemento');
        $this->form_validation->set_message('existencia','Ya existe un presupuesto con este No. de Orden.');
        $this->form_validation->set_message('existeOrden','No existe una orden abierta con este No. de Orden.');
        $this->form_validation->set_message('multipunto','No existe una hoja multipunto creada previamente.');

        $this->form_validation->set_message('garantias','Se debe seleccional al menos una refacción');
        $this->form_validation->set_message('jefetaller','Se debe seleccional al menos una refacción');
        $this->form_validation->set_message('asesor','Se debe seleccional al menos una refacción');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $datos["mensaje"] = "1";
            //Si la vista es de decision
            //Alta/Actualiza tecnico
            if ($datos["vista"] == "1") {
                $this->guardarRegistroTec($datos);
            //Actualiza / Termina asesor
            } elseif ($datos["vista"] == "2") {
                $this->actualizaAsesor($datos);
            //Actualiza / Termina ventanilla
            } elseif ($datos["vista"] == "3") {
                $this->actualizaVentanilla($datos);
            //Actualiza / Termina Jefe de taller
            } elseif ($datos["vista"] == "4") {
                $this->actualizaJefeTaller($datos);
            //Actualiza /Termina garantias
            } elseif ($datos["vista"] == "5") {
                $this->actualizaGarantias($datos);
            //Actualiza Tecnico
            } elseif ($datos["vista"] == "8") {
                $this->actualizaRegistroTec($datos);
            //Actualiza Ventanilla / Firma tecnico
            } elseif ($datos["vista"] == "9") {
                $this->actualizaRegistroVentanillaTec($datos);
            }
        //Si no pasa las validaciones
        }else{
            //var_dump(validation_errors());
            $datos["mensaje"]="0";
            if ($datos["vista"] == "1") {
                $this->comodinAltaTec($datos["id_cita"],$datos);
            }else {
                //Asesor
                if ($datos["vista"] == "2") {
                    $this->comodinEditaAsesor($datos["id_cita"],$datos);
                //Ventanilla
                }elseif ($datos["vista"] == "3") {
                    $this->comodinEditaVentanilla($datos["id_cita"],$datos);
                //Jefe de Taller
                }elseif ($datos["vista"] == "4") {
                    $this->comodinEditaJefeTaller($datos["id_cita"],$datos);
                //Garantias
                }elseif ($datos["vista"] == "5") {
                    $this->comodinEditaGarantias($datos["id_cita"],$datos);
                //Tecnico
                }elseif ($datos["vista"] == "8") {
                    $this->comodinEditaTecnico($datos["id_cita"],$datos);
                //Ventanilla - Tecnico
                }elseif ($datos["vista"] == "9") {
                    //Presupuesto tradicional
                    if ($datos["presupuesto_bandera"] == "T") {
                        $this->comodinRequisicion_T($datos["id_cita"],$datos);
                    //Presupuesto garantias
                    }else{
                        $this->comodinRequisicion_G($datos["id_cita"],$datos);
                    }
                }
            }
        }
    }

    //Comprobamos si existe algun presupuesto con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("idOrdenTemp");

        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;

            //Validamos en los presupuesto viejitos
            $query = $this->mConsultas->get_result("idOrden",$id_cita,"cotizacion_multipunto");
            foreach ($query as $row){
                if ($row->identificador == "M") {
                    $dato = $row->idCotizacion;
                }
            }

            //Interpretamos la respuesta de la consulta
            if (isset($dato)) {
                //Si existe el dato, entonces ya existe el registro
                $respuesta = FALSE;
            }else {
                //Si no existe el dato, entonces no existe el registro
                $respuesta = TRUE;
            }
        }
        
        return $respuesta;
    }

    //Comprobamos que exista una orden generada para poder crear la cotizacion
    public function existeOrden()
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("idOrdenTemp");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"ordenservicio");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            $respuesta = TRUE;
        }else {
            $respuesta = FALSE;
        }
        return $respuesta;
    }

    //Validamos si existe un registro con ese numero de orden
    public function multipunto()
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("idOrdenTemp");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("orden",$id_cita,"multipunto_general");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = TRUE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = FALSE;
        }
        return $respuesta;
    }

    //Validar que al menos se haya generado una refaccion
    public function registros()
    {
        $return = FALSE;
        if ($this->input->post("registros")) {
            if (count($this->input->post("registros")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar las refacciones marcadas por garantías
    public function garantias()
    {
        $return = FALSE;
        if ($this->input->post("autoriza_garantia")) {
            if (count($this->input->post("autoriza_garantia")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar las refacciones marcadas por el jefe de taller
    public function jefetaller()
    {
        $return = FALSE;
        if ($this->input->post("autoriza_jdt")) {
            if (count($this->input->post("autoriza_jdt")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar las refacciones marcadas por el asesor
    public function asesor()
    {
        $return = FALSE;
        if ($this->input->post("autoriza_asesor")) {
            if (count($this->input->post("autoriza_asesor")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Guardamos registro por primera vez o actualizacion cel tecnico
    public function guardarRegistroTec($datos = NULL)
    {
        $fecha_registro = date("Y-m-d H:i");
        $renglones = $this->input->post("registros");
        
        if ($this->input->post("pzaGarantia") != NULL) {
            $pzas_garantias = $this->input->post("pzaGarantia");
        }else {
            $pzas_garantias = [];
        }
        $ref_tradicionales = 0;
        $ref_garantias = 0;
        $bandera = FALSE;

        //Guardamos los items
        for ($i=0; $i < count($renglones) ; $i++) { 
            //echo "<br>".$renglones[$i]."<br>";
            //Evitamos guardar basura
            if (strlen($renglones[$i])>19) {
                //Dividimos las celdas
                $campos = explode("_",$renglones[$i]);

                $registro = array(
                    "id" => NULL,
                    "id_cita" => $datos["id_cita"],
                    "identificador" => "M",
                    "cantidad" => $campos[0],
                    "descripcion" => $campos[1],
                    "num_pieza" => $campos[2],
                    "existe" => $campos[3],
                    "fecha_planta" => "0000-00-00",
                    "costo_pieza" => $campos[4],
                    "horas_mo" => $campos[5],
                    "costo_mo" => $campos[6],
                    "total_refacion" => $campos[7],
                    "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                    "costo_ford" => 0,
                    "horas_mo_garantias" => 0,
                    "costo_mo_garantias" => 0,
                    "autoriza_garantia" => 0,
                    "autoriza_jefeTaller" => 1,
                    "autoriza_asesor" => 0,
                    "autoriza_cliente" => 0,
                    "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                    "prioridad" => $campos[8],
                    "estado_entrega" => 0,
                    "activo" => 1,
                    "fecha_registro" => $fecha_registro,
                    "fecha_actualiza" => $fecha_registro
                );

                //Guardamos el registro de la refaccion
                $id_retorno = $this->mConsultas->save_register('presupuesto_multipunto',$registro);
                if ($id_retorno != 0) {
                    //Si se guardo al menos una refaccion guardamos el registro general
                    $bandera = TRUE;
                    //Contabilizamos las refacciones tradicionales y las de garantias
                    if (!in_array(($i+1),$pzas_garantias)) {
                        $ref_tradicionales++;
                    } else {
                        $ref_garantias++;
                    }
                }
                //print_r($registro);
            }
        }

        //Validamos si se guardara un registro pivote
        if ($bandera) {
            //Verificamos los archivos generales
            if($this->input->post("tempFile") != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $this->input->post("tempFile")."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            //Verificamos archivos del tecnico
            if($this->input->post("tempFileTecnico") != "") {
                $archivos_3 = $this->validateDoc("uploadfilesTec");
                $archivos_tec = $this->input->post("tempFileTecnico")."".$archivos_3;
            }else {
                $archivos_tec = $this->validateDoc("uploadfilesTec");
            }

            $destino_presupuesto = $this->input->post("aceptarCotizacion");

            $registro_pivote = array(
                "id" => NULL,
                "id_cita" => $datos["id_cita"],
                "folio_externo" => $this->input->post("folio_externo"),
                "identificador" => "M",
                "cliente_paga" => 0,
                "ref_garantias" => $ref_garantias,
                "ref_tradicional" => $ref_tradicionales,
                "ford_paga" => 0,
                "total_pago_garantia" => 0,
                "envio_garantia" => 0,
                "comentario_garantia" => "",
                "firma_garantia" => "",
                "anticipo" => 0,
                "nota_anticipo" => "",
                "subtotal" => $this->input->post("subTotalMaterial"),
                "cancelado" => 0,
                "iva" => $this->input->post("ivaMaterial"),
                "total" => $this->input->post("totalMaterial"),
                //Nombre del encargado de ventanilla
                "ventanilla" => "",
                "envia_ventanilla" => (($destino_presupuesto == "Enviar Cotización a Ventanilla") ? "0" : "2"),
                "estado_refacciones" => 0,
                "estado_refacciones_garantias" => 0,
                "archivos_presupuesto" => $archivos_gral,
                "archivo_tecnico" => $archivos_tec,
                "archivo_jdt" => "",
                "firma_asesor" => "",
                "comentario_asesor" => "",
                "envio_jdt" => 0,
                "firma_jdt" => "",
                "comentario_jdt" => "",
                "comentario_ventanilla" => "",
                "comentario_tecnico" => $this->input->post("comentarioTecnico"),
                "serie" => $this->input->post("serie"),
                "firma_requisicion" => "",
                "firma_requisicion_garantias" => "",
                "correo_adicional" => "",
                "envio_proactivo" => 0,
                "acepta_cliente" => "",
                //Si el proceso tiene para del jefe de taller
                "ubicacion_proceso" => (($destino_presupuesto == "Enviar Cotización a Ventanilla") ? "Espera revisión de ventanilla" : "Espera revisión del Jefe de Taller"),
                //Si el proceso no tiene parada del jefe de taller
                //"ubicacion_proceso" => (($destino_presupuesto == "Enviar Cotización a Ventanilla") ? "Espera revisión de ventanilla" : "Espera revisión del Asesor"),
                "ubicacion_proceso_garantia" => (($ref_garantias > 0) ? "Espera revisión de ventanilla" : "Sin proceso"),
                "fecha_alta" => $fecha_registro,
                "termina_ventanilla" => $fecha_registro,
                "termina_jdt" => $fecha_registro,
                "termina_garantias" => $fecha_registro,
                "termina_asesor" => $fecha_registro,
                "termina_cliente" => $fecha_registro,
                "termina_requisicion" => $fecha_registro,
                "termina_requisicion_garantias" => $fecha_registro,
                "solicita_pieza" => $fecha_registro,
                "recibe_pieza" => $fecha_registro,
                "entrega_pieza" => $fecha_registro,
                "solicita_pieza_garantia" => $fecha_registro,
                "recibe_pieza_garantia" => $fecha_registro,
                "entrega_pieza_garantia" => $fecha_registro,
                "termina_proactivo" => $fecha_registro,
                "fecha_actualiza" => $fecha_registro,
            );

            //Guardamos el registro de la refaccion
            $id_retorno = $this->mConsultas->save_register('presupuesto_registro',$registro_pivote);
            if ($id_retorno != 0) {
                $datos["mensaje"] = "1";

                //Actualizamos la información para el historial de ordenes
                $this->actualiza_documentacion($datos["id_cita"]);

                //Recuperamos datos extras para la notificación
                $query = $this->mConsultas->notificacionTecnicoCita($datos["id_cita"]); 
                foreach ($query as $row) {
                    $tecnico = $row->tecnico;
                    $modelo = $row->vehiculo_modelo;
                    $placas = $row->vehiculo_placas;
                    $asesor = $row->asesor;
                }

                //Mandamos la notificacion de la cotizacion creada a ventanilla
                if ($destino_presupuesto == "Enviar Cotización a Ventanilla") {
                    $texto = SUCURSAL.". Nueva solicitud de presupuesto multipunto creada. No. Orden: ".$datos["id_cita"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                    $movimiento = "Se crea presupuesto multipunto (Se envia a ventanilla)";
                    
                    //Ventanilla
                    $this->sms_general('3316721106',$texto);
                    $this->sms_general('3319701092',$texto);
                    $this->sms_general('3317978025',$texto);

                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    //$this->sms_general('5535527547',$texto);

                //Mandamos la notificacion de la cotizacion creada al  asesor/jefe de talles 
                }else{
                    //Si el presupuesto tiene piezas con garantias, y no hay jefe de taller, se envia la informacion
                    if ($ref_garantias > 0) {
                        $texto = SUCURSAL.". Nueva solicitud de presupuesto multipunto creada (sin pasar por ventanilla) con refacciones con garantías. No. Orden: ".$datos["id_cita"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        $movimiento = "Se crea presupuesto multipunto con garantías (Se envia al jefe de taller)";

                    }else{
                        $texto = SUCURSAL.". Nueva solicitud de presupuesto multipunto creada (sin pasar por ventanilla). No. Orden: ".$datos["id_cita"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        $movimiento = "Se crea presupuesto multipunto sin garantías (Se envia al jefe de taller)";
                    }

                    //Jefe de taller
                    //Jesus Gonzales 
                    //$this->sms_general('3316724093',$texto); 
                    //Juan Chavez
                    $this->sms_general('3333017952',$texto);
                    //Gerente de servicio
                    //$this->sms_general('3315273100',$texto);
                    
                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    //$this->sms_general('5535527547',$texto);
                    
                    //Grabamos el evento para el panel de gerencia
                    $registro_evento = array(
                        'envia_departamento' => 'Técnicos',
                        'recibe_departamento' => 'Ventanilla',
                        'descripcion' => 'Nuevo presupuesto multipunto creado',
                    );

                    $this->registroEvento($datos["id_cita"],$registro_evento);
                }
                $this->registrar_movimiento($datos["id_cita"],$movimiento,NULL);
                
            }else{
                $datos["mensaje"] = "0";
            }
        }else{
            $datos["mensaje"] = "0";
        }

        //$this->comodinAltaTec($datos["id_cita"],$datos);
        redirect(base_url().'Alta_Presupuesto/'.$this->encrypt($datos["id_cita"]));
    }

    //Envio de presupuesto por ajax (alta)
    public function guardarRegistroTecAjax()
    {
        //Verificamos que no llege la informacion vacia
        if ($_POST["idOrdenTemp"] != "") {
            $fecha_registro = date("Y-m-d H:i");

            //Verificamos que existan los requerimientos para crear el presupuesto
            $hm_previa = $this->validar_HMprevia($_POST["idOrdenTemp"]);
            $presupuesto_previo = $this->validar_existencia($_POST["idOrdenTemp"]);

            if (($hm_previa == 1)&&($presupuesto_previo == 1)) {
                if (isset($_POST["registros"])) {
                    $renglones = $_POST["registros"] ;

                    if (isset($_POST["pzaGarantia"])) {
                        $pzas_garantias = $_POST["pzaGarantia"];
                    }else {
                        $pzas_garantias = [];
                    }

                    $ref_tradicionales = 0;
                    $ref_garantias = 0;
                    $bandera = FALSE;

                    //Guardamos los items
                    for ($i=0; $i < count($renglones) ; $i++) { 
                        //echo "<br>".$renglones[$i]."<br>";
                        //Evitamos guardar basura
                        if (strlen($renglones[$i])>19) {
                            //Dividimos las celdas
                            $campos = explode("_",$renglones[$i]);

                            $registro = array(
                                "id" => NULL,
                                "id_cita" => $_POST["idOrdenTemp"],
                                "identificador" => "M",
                                "ref" => ((isset($campos[9])) ? $campos[9] : NULL),
                                "cantidad" => $campos[0],
                                "descripcion" => $campos[1],
                                "num_pieza" => $campos[2],
                                "existe" => $campos[3],
                                "fecha_planta" => "0000-00-00",
                                "costo_pieza" => $campos[4],
                                "rep_adicional" => ((isset($campos[11])) ? $campos[11] : 0),
                                "horas_mo" => $campos[5],
                                "costo_mo" => $campos[6],
                                "total_refacion" => $campos[7],
                                "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                                "costo_ford" => 0,
                                "horas_mo_garantias" => 0,
                                "costo_mo_garantias" => 0,
                                "autoriza_garantia" => 0,
                                "autoriza_jefeTaller" => 1,
                                "autoriza_asesor" => 0,
                                "autoriza_cliente" => 0,
                                "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                                "prioridad" => ((isset($campos[8])) ? $campos[8] : 2),
                                "activo" => 1,
                                "fecha_registro" => $fecha_registro,
                                "fecha_actualiza" => $fecha_registro
                            );

                            //Guardamos el registro de la refaccion
                            $id_retorno = $this->mConsultas->save_register('presupuesto_multipunto',$registro);
                            if ($id_retorno != 0) {
                                //Si se guardo al menos una refaccion guardamos el registro general
                                $bandera = TRUE;
                                //Contabilizamos las refacciones tradicionales y las de garantias
                                if (!in_array(($i+1),$pzas_garantias)) {
                                    $ref_tradicionales++;
                                } else {
                                    $ref_garantias++;
                                }
                            }
                            //print_r($registro);
                        }
                    }

                    //Validamos si se guardara un registro pivote
                    if ($bandera) {
                        //Verificamos los archivos generales
                        if($_POST["uploadfiles_resp"] != "") {
                            $archivos_2 = $this->validateDoc("uploadfiles");
                            $archivos_gral = $_POST["uploadfiles_resp"]."".$archivos_2;
                        }else {
                            $archivos_gral = $this->validateDoc("uploadfiles");
                        }

                        //Verificamos archivos del tecnico
                        if($_POST["tempFileTecnico"] != "") {
                            $archivos_3 = $this->validateDoc("uploadfilesTec");
                            $archivos_tec = $_POST["tempFileTecnico"]."".$archivos_3;
                        }else {
                            $archivos_tec = $this->validateDoc("uploadfilesTec");
                        }

                        $destino_presupuesto = $_POST["aceptarCotizacion"];

                        $registro_pivote = array(
                            "id" => NULL,
                            "id_cita" => $_POST["idOrdenTemp"],
                            "folio_externo" => $_POST["folio_externo"],
                            "identificador" => "M",
                            "cliente_paga" => 0,
                            "ref_garantias" => $ref_garantias,
                            "ref_tradicional" => $ref_tradicionales,
                            "ford_paga" => 0,
                            "total_pago_garantia" => 0,
                            "envio_garantia" => 0,
                            "comentario_garantia" => "",
                            "firma_garantia" => "",
                            "anticipo" => 0,
                            "nota_anticipo" => "",
                            "subtotal" => $_POST["subTotalMaterial"],
                            "cancelado" => 0,
                            "iva" => $_POST["ivaMaterial"],
                            "total" => $_POST["totalMaterial"],
                            "ventanilla" => "",
                            "envia_ventanilla" => (($destino_presupuesto == "Enviar Cotización a Ventanilla") ? "0" : "2"),
                            "estado_refacciones" => 0,
                            "estado_refacciones_garantias" => 0,
                            "archivos_presupuesto" => $archivos_gral,
                            "archivo_tecnico" => $archivos_tec,
                            "archivo_jdt" => "",
                            "firma_asesor" => "",
                            "comentario_asesor" => "",
                            "envio_jdt" => 0,
                            "firma_jdt" => "",
                            "comentario_jdt" => "",
                            "comentario_ventanilla" => "",
                            "comentario_tecnico" => $_POST["comentarioTecnico"],
                            "serie" => $_POST["serie"],
                            "firma_requisicion" => "",
                            "firma_requisicion_garantias" => "",
                            "correo_adicional" => "",
                            "envio_proactivo" => 0,
                            "acepta_cliente" => "",
                            //Si el proceso tiene para del jefe de taller
                            "ubicacion_proceso" => (($destino_presupuesto == "Enviar Cotización a Ventanilla") ? "Espera revisión de ventanilla" : "Espera revisión del Jefe de Taller"),
                            //Si el proceso no tiene parada del jefe de taller
                            //"ubicacion_proceso" => (($destino_presupuesto == "Enviar Cotización a Ventanilla") ? "Espera revisión de ventanilla" : "Espera revisión del Asesor"),
                            "ubicacion_proceso_garantia" => (($ref_garantias > 0) ? "Espera revisión de ventanilla" : "Sin proceso"),
                            "fecha_alta" => $fecha_registro,
                            "termina_ventanilla" => $fecha_registro,
                            "termina_jdt" => $fecha_registro,
                            "termina_garantias" => $fecha_registro,
                            "termina_asesor" => $fecha_registro,
                            "termina_cliente" => $fecha_registro,
                            "termina_requisicion" => $fecha_registro,
                            "termina_requisicion_garantias" => $fecha_registro,
                            "solicita_pieza" => $fecha_registro,
                            "recibe_pieza" => $fecha_registro,
                            "entrega_pieza" => $fecha_registro,
                            "solicita_pieza_garantia" => $fecha_registro,
                            "recibe_pieza_garantia" => $fecha_registro,
                            "entrega_pieza_garantia" => $fecha_registro,
                            "termina_proactivo" => $fecha_registro,
                            "fecha_actualiza" => $fecha_registro,
                        );

                        //Guardamos el registro de la refaccion
                        $id_retorno = $this->mConsultas->save_register('presupuesto_registro',$registro_pivote);
                        if ($id_retorno != 0) {
                            //Actualizamos la información para el historial de ordenes
                            $this->actualiza_documentacion($_POST["idOrdenTemp"]);

                            $conclusion = "OK";

                            //Recuperamos datos extras para la notificación
                            $query = $this->mConsultas->notificacionTecnicoCita($_POST["idOrdenTemp"]); 
                            foreach ($query as $row) {
                                $tecnico = $row->tecnico;
                                $modelo = $row->vehiculo_modelo;
                                $placas = $row->vehiculo_placas;
                                $asesor = $row->asesor;
                            }

                            //Mandamos la notificacion de la cotizacion creada a ventanilla
                            if ($destino_presupuesto == "Enviar Cotización a Ventanilla") {
                                $texto = SUCURSAL.". Nueva solicitud de presupuesto multipunto creada. No. Orden: ".$_POST["idOrdenTemp"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                                $movimiento = "Se crea presupuesto multipunto (Se envia a ventanilla)";
                                
                                //Ventanilla
                                $this->sms_general('3316721106',$texto);
                                $this->sms_general('3319701092',$texto);
                                $this->sms_general('3317978025',$texto);

                                //Tester
                                //$this->sms_general('3328351116',$texto);
                                //Angel evidencia
                                //$this->sms_general('5535527547',$texto);

                            //Mandamos la notificacion de la cotizacion creada al  asesor/jefe de talles 
                            }else{
                                //Si el presupuesto tiene piezas con garantias, y no hay jefe de taller, se envia la informacion
                                if ($ref_garantias > 0) {
                                    $texto = SUCURSAL.". Nueva solicitud de presupuesto multipunto creada (sin pasar por ventanilla) con refacciones con garantías. No. Orden: ".$_POST["idOrdenTemp"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                                    $movimiento = "Se crea presupuesto multipunto con garantías (Se envia al jefe de taller)";

                                }else{
                                    $texto = SUCURSAL.". Nueva solicitud de presupuesto multipunto creada (sin pasar por ventanilla). No. Orden: ".$_POST["idOrdenTemp"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                                    $movimiento = "Se crea presupuesto multipunto sin garantías (Se envia al jefe de taller)";
                                }

                                //Jefe de taller
                                //Jesus Gonzales 
                                //$this->sms_general('3316724093',$texto); 
                                //Juan Chavez
                                $this->sms_general('3333017952',$texto);
                                //Gerente de servicio
                                //$this->sms_general('3315273100',$texto);
                                
                                //Tester
                                //$this->sms_general('3328351116',$texto);
                                //Angel evidencia
                                //$this->sms_general('5535527547',$texto);
                    
                                
                                //Grabamos el evento para el panel de gerencia
                                $registro_evento = array(
                                    'envia_departamento' => 'Técnicos',
                                    'recibe_departamento' => 'Ventanilla',
                                    'descripcion' => 'Nuevo presupuesto multipunto creado',
                                );

                                $this->registroEvento($_POST["idOrdenTemp"],$registro_evento);
                            }

                            $this->registrar_movimiento($_POST["idOrdenTemp"],$movimiento,NULL);
                            
                        }else{
                            $conclusion = "NO SE GUARDO CORRECTAMENTE EL REGISTRO";
                        }
                    }else{
                        $conclusion = "NO SE GUARDO CORRECTAMENTE EL REGISTRO";
                    }
                }else {
                    $conclusion = "NO SE CARGARON REFACCIONES";
                }
            }else{
                $conclusion = "YA EXISTE UN PRESUPUESTO PARA ESTA ORDEN";
            }
        }
        echo $conclusion;
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion($id_cita = '')
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "presupuesto" => "1",
                "presupuesto_origen" => "1",
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion_2($id_cita = '')
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "presupuesto_afectado" => "",
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion_cliente($id_cita = '',$decision = "")
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "presupuesto_afectado" => $decision,
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Validamos la existenacia de la multipunto
    public function validar_HMprevia($id_cita = '')
    {
        $respuesta = 0;
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("orden",$id_cita,"multipunto_general");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = 1;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = 0;
        }
        return $respuesta;
    }

    //Validamos que no exista refacciones previas
    public function validar_existencia($id_cita = '')
    {
        $respuesta = 0;

        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = 0;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = 1;

            //Validamos en los presupuesto viejitos
            $query = $this->mConsultas->get_result("idOrden",$id_cita,"cotizacion_multipunto");
            foreach ($query as $row){
                if ($row->identificador == "M") {
                    $dato = $row->idCotizacion;
                }
            }

            //Interpretamos la respuesta de la consulta
            if (isset($dato)) {
                //Si existe el dato, entonces ya existe el registro
                $respuesta = 0;
            }else {
                //Si no existe el dato, entonces no existe el registro
                $respuesta = 1;
            }
        }
        
        return $respuesta;
    }

    //Actualizacion de presupuesto
    public function actualizaVentanilla($datos = NULL)
    {
        $fecha_registro = date("Y-m-d H:i");
        $renglones = $this->input->post("registros");
        $renglones_garantias = $this->input->post("registrosGarantias");
        $renglones_id = $this->input->post("id_registros");
        
        if ($this->input->post("pzaGarantia") != NULL) {
            $pzas_garantias = $this->input->post("pzaGarantia");
        }else {
            $pzas_garantias = [];
        }

        $ref_tradicionales = 0;
        $ref_garantias = 0;
        
        $bandera = FALSE;

        $nvo_refaccion = [];
        $refaccion = [];
        $id_dase = [];

        //var_dump($this->input->post());
        //Obtenemos la informacion las refacciones y armamos los paquetes de guardado
        for ($i=0; $i < count($renglones) ; $i++) {
            if (strlen($renglones[$i])>19) {
                //Dividimos las celdas
                $campos_registro = explode("_",$renglones[$i]);
                $campos_garantias = explode("_",$renglones_garantias[$i]);

                $registro_existe_almacen = explode("*",$campos_registro[3]);

                //Identificamos si es una refaccion nueva, con id = 0
                if ($renglones_id[$i] == "0") {
                    $nvo_refaccion[] = array(
                        "id" => NULL,
                        "id_cita" => $datos["id_cita"],
                        "identificador" => "M",
                        //datoas adicionales
                        //"prefijo" => ((isset($campos_registro[9])) ? (($campos_registro[9] != "") ? $campos_registro[9] : ".") : "."),
                        //"sufijo" => ((isset($campos_registro[10])) ? (($campos_registro[10] != "") ? $campos_registro[10] : ".") : "."),
                        //"basico" => ((isset($campos_registro[11])) ? (($campos_registro[11] != "") ? $campos_registro[11] : ".") : "."),

                        "cantidad" => $campos_registro[0],
                        "descripcion" => $campos_registro[1],
                        "num_pieza" => $campos_registro[2], 
                        "existe" => $registro_existe_almacen[0],
                        "fecha_planta" => ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1]: "0000-00-00"),
                        "costo_pieza" => $campos_registro[4],
                        "horas_mo" => $campos_registro[5],
                        "costo_mo" => $campos_registro[6],
                        "total_refacion" => $campos_registro[7],
                        "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                        "costo_ford" => $campos_garantias[0],
                        "horas_mo_garantias" => $campos_garantias[1],
                        "costo_mo_garantias" => $campos_garantias[2],
                        "autoriza_garantia" => 0,
                        "autoriza_jefeTaller" => 1,
                        "autoriza_asesor" => 0,
                        "autoriza_cliente" => 0,
                        "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                        "prioridad" => $campos_registro[8],
                        "estado_entrega" => 0,
                        "activo" => 1,
                        "fecha_registro" => $fecha_registro,
                        "fecha_actualiza" => $fecha_registro
                    );
                //De lo contrario se deduce que es una refaccion que se actualiza
                }else{

                    $info_refaccion = array(
                        //datoas adicionales
                        //"prefijo" => ((isset($campos_registro[9])) ? (($campos_registro[9] != "") ? $campos_registro[9] : ".") : "."),
                        //"sufijo" => ((isset($campos_registro[10])) ? (($campos_registro[10] != "") ? $campos_registro[10] : ".") : "."),
                        //"basico" => ((isset($campos_registro[11])) ? (($campos_registro[11] != "") ? $campos_registro[11] : ".") : "."),

                        "cantidad" => $campos_registro[0],
                        "descripcion" => $campos_registro[1]
                    );

                    if ($campos_registro[2] != "") {
                        $info_refaccion["num_pieza"] = $campos_registro[2];
                    }
                    
                    if ($registro_existe_almacen[0] != "") {
                        $info_refaccion["existe"] = $registro_existe_almacen[0];
                        $info_refaccion["fecha_planta"] = ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1]: "0000-00-00");
                    }

                    if ($campos_registro[4] != "") {
                        $info_refaccion["costo_pieza"] = $campos_registro[4];
                    }
                    
                    $info_refaccion["horas_mo"] = $campos_registro[5];
                    $info_refaccion["costo_mo"] = $campos_registro[6];
                    $info_refaccion["total_refacion"] = $campos_registro[7];
                        //"pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                    if (($campos_garantias[0] != "")&&($campos_garantias[0] != "0")) {
                        $info_refaccion["costo_ford"] = $campos_garantias[0];
                    }

                    if (($campos_garantias[1] != "")&&($campos_garantias[1] != "0")) {
                        $info_refaccion["horas_mo_garantias"] = $campos_garantias[1];
                    }

                    if (($campos_garantias[2] != "")&&($campos_garantias[2] != "0")) {
                        $info_refaccion["costo_mo_garantias"] = $campos_garantias[2];
                    }

                    //$info_refaccion["costo_ford"] = $campos_garantias[0];
                    //$info_refaccion["horas_mo_garantias"] = $campos_garantias[1];
                    //$info_refaccion["costo_mo_garantias"] = $campos_garantias[2];
                        //"prioridad" => $campos_registro[8],
                        //"estado_entrega" => 0,
                    $info_refaccion["fecha_actualiza"] = $fecha_registro;

                    $refaccion[] = $info_refaccion;
                    $id_dase[] = $renglones_id[$i];
                }

                //Contabilizamos las refacciones tradicionales y las de garantias
                if (!in_array(($i+1),$pzas_garantias)) {
                    $ref_tradicionales++;
                } else {
                    $ref_garantias++;
                }
            }
        }

        //Verificamos si se guardo alguna refaccion (nueva o actualizacion)
        if ((count($nvo_refaccion)>0)||(count($refaccion)>0)) {
            //Para las refacciones que se van a dar de alta
            for ($i=0; $i < count($nvo_refaccion); $i++) {
                //Guardamos el registro de la refaccion
                $id_retorno = $this->mConsultas->save_register('presupuesto_multipunto',$nvo_refaccion[$i]);
                if ($id_retorno != 0) {
                    //Si se guardo al menos una refaccion guardamos el registro general
                    $bandera = TRUE;
                }
            }

            //Para las refacciones que se van a actualizar
            for ($i=0; $i < count($refaccion); $i++) { 
                //Actualizamos el registro de la refaccion
                $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$refaccion[$i],'id',$id_dase[$i]);
                if ($actualizar) {
                    //Si se actualizo correctamente
                    $bandera = TRUE;
                }
            }
        }

        //Si la actualizacion y/o registro se llevo con exito, se actualiza el registro general
        //Validamos si se guardara un registro pivote
        if ($bandera) {
            //Verificamos los archivos generales
            if($this->input->post("uploadfiles_resp") != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $this->input->post("uploadfiles_resp")."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            $destino_presupuesto = $this->input->post("aceptarCotizacion");

            $actualiza_pivote = array(
                //"ref_garantias" => $ref_garantias,
                //"ref_tradicional" => $ref_tradicionales,
                "anticipo" => $this->input->post("anticipoMaterial"),
                "nota_anticipo" => $this->input->post("anticipoNota"),
                "subtotal" => $this->input->post("subTotalMaterial"),
                "cancelado" => $this->input->post("canceladoMaterial"),
                "iva" => $this->input->post("ivaMaterial"),
                "total" => $this->input->post("totalMaterial"),
                //"ventanilla" => "",
                "envia_ventanilla" => (($destino_presupuesto == "Enviar Presupuesto") ? "1" : "0"),
                "archivos_presupuesto" => $archivos_gral,
                "comentario_ventanilla" => $this->input->post("comentario_ventanilla"),
                //"serie" => $this->input->post("serie"),
            );

            if ($destino_presupuesto == "Enviar Presupuesto") {
                //$actualiza_pivote["ubicacion_proceso"] = "Espera revisión del asesor";
                $actualiza_pivote["ubicacion_proceso"] = "Espera revisión de Jefe de Taller *";
                //$actualiza_pivote["ubicacion_proceso_garantia"] = (($ref_garantias > 0) ? "Espera revisión garantias": "Proceso truncado");
                $actualiza_pivote["termina_ventanilla"] = $fecha_registro;
            }

            $actualiza_pivote["fecha_actualiza"] = $fecha_registro;

            //Guardamos el registro de la refaccion
            $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$actualiza_pivote,"id_cita",$datos["id_cita"],"identificador","M");
            if ($actualiza_reg != 0) {
                $datos["mensaje"] = "1";

                //Comprobamos si se envio el presupuesto para enviar la notificacion
                if ($destino_presupuesto == "Enviar Presupuesto") {
                    //Recuperamos datos extras para la notificación
                    $query = $this->mConsultas->notificacionTecnicoCita($datos["id_cita"]); 
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $placas = $row->vehiculo_placas;
                        $asesor = $row->asesor;
                    }

                    if (isset($tecnico)) {
                        //Verificamos si el presupuesto tiene garantias
                        if ($ref_garantias > 0) {
                            $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." fue liberado de ventanilla con refacciones con garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        }else{
                            $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." fue liberado de ventanilla el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                        }

                        //Jefe de taller
                        //Jesus Gonzales 
                        //$this->sms_general('3316724093',$texto); 
                        //Juan Chavez
                        $this->sms_general('3333017952',$texto);
                        //Gerente de servicio
                        //$this->sms_general('3315273100',$texto);
                        //Jefe de taller
                        $this->sms_general('3318571171',$texto);
                        
                        //Tester
                        //$this->sms_general('3328351116',$texto);
                        //Angel evidencia
                        //$this->sms_general('5535527547',$texto);
                        
                        //Grabamos el evento para el panel de gerencia
                        $registro_evento = array(
                            'envia_departamento' => 'Ventanilla',
                            'recibe_departamento' => 'Jefe de Taller',
                            'descripcion' => 'Presupuesto multipunto liberado de ventanilla',
                        );

                        $this->registroEvento($datos["id_cita"],$registro_evento);

                        $movimiento = "Presupuesto liberado de ventanilla";
                    }else{
                        $movimiento = "Presupuesto liberado de ventanilla (error)";
                    }
                }else{
                    $movimiento = "Presupuesto actualizado en ventanilla (sin enviar)";
                }

                $this->registrar_movimiento($datos["id_cita"],$movimiento,"Ventanilla");
            }else{
                $datos["mensaje"] = "0";
            }
        }else{
            $datos["mensaje"] = "0";
        }

        $this->comodinEditaVentanilla($datos["id_cita"],$datos);
    }

    //Actualizacion de presupuesto
    public function actualizaJefeTaller($datos = NULL)
    {
        $fecha_registro = date("Y-m-d H:i");
        $renglones = $this->input->post("registros");
        $renglones_id = $this->input->post("id_registros");

        $renglones_jft = [];
        $renglones_jft = $this->input->post("autoriza_jdt");

        $bandera = FALSE;

        $ref_tradicionales = 0;
        $ref_garantias = 0;
        
        $bandera = FALSE;

        $refaccion = [];
        $id_dase = [];

        //var_dump($this->input->post());
        //Obtenemos la informacion las refacciones y armamos los paquetes de guardado
        for ($i=0; $i < count($renglones) ; $i++) {
            if (strlen($renglones[$i])>19) {
                //Dividimos las celdas
                $campos_registro = explode("_",$renglones[$i]);
                //$campos_garantias = explode("_",$renglones_garantias[$i]);

                $registro_existe_almacen = explode("*",$campos_registro[3]);

                if (in_array($renglones_id[$i], $renglones_jft)) {
                    $autoriza_jdt = "1";
                }else{
                    $autoriza_jdt = "0";
                }

                $refaccion[] = array(
                    "cantidad" => $campos_registro[0],
                    "descripcion" => $campos_registro[1],
                    "num_pieza" => $campos_registro[2],
                    "existe" => $registro_existe_almacen[0],
                    "fecha_planta" => ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1]: "0000-00-00"),
                    "costo_pieza" => $campos_registro[4],
                    "horas_mo" => $campos_registro[5],
                    "costo_mo" => $campos_registro[6],
                    "total_refacion" => $campos_registro[7],
                    //"pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                    //"costo_ford" => $campos_garantias[0],
                    //"horas_mo_garantias" => $campos_garantias[1],
                    //"costo_mo_garantias" => $campos_garantias[2],
                    //"autoriza_garantia" => 0,
                    "autoriza_jefeTaller" => $autoriza_jdt,
                    //"autoriza_asesor" => 0,
                    //"autoriza_cliente" => 0,
                    //"tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                    //"prioridad" => $campos_registro[8],
                    //"estado_entrega" => 0
                    //"activo" => 1,
                    "fecha_actualiza" => $fecha_registro
                );

                $id_dase[] = $renglones_id[$i];

                //Contabilizamos las refacciones tradicionales y las de garantias
                //if (!in_array(($i+1),$pzas_garantias)) {
                //    $ref_tradicionales++;
                //} else {
                //    $ref_garantias++;
                //}
            }
        }

        //Verificamos si se guardo alguna refaccion (nueva o actualizacion)
        if (count($refaccion)>0) {
            //Para las refacciones que se van a actualizar
            for ($i=0; $i < count($refaccion); $i++) { 
                //Actualizamos el registro de la refaccion
                $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$refaccion[$i],'id',$id_dase[$i]);
                if ($actualizar) {
                    //Si se actualizo correctamente
                    $bandera = TRUE;
                }
            }
        }

        //var_dump($this->input->post());
        //Obtenemos la informacion las refacciones y armamos los paquetes de guardado
        for ($i=0; $i < count($renglones_id) ; $i++) {
            //Identificamos si la refaccion fue aprovada por el jefe de taller o no
            if (in_array($renglones_id[$i], $renglones_jft)) {
                $autorizacion[] = array(
                    "autoriza_jefeTaller" => 1,
                    "fecha_actualiza" => $fecha_registro
                );
            }else{
                $autorizacion[] = array(
                    "autoriza_jefeTaller" => 0,
                    "fecha_actualiza" => $fecha_registro
                );
            }
        }

        //Confirmamos la cantidd de registros y los actualizamos
        if (count($autorizacion)>0) {
            //Guardamos los registros
            for ($i=0; $i < count($renglones_id); $i++) { 
                //Actualizamos el registro de la refaccion
                $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$autorizacion[$i],'id',$renglones_id[$i]);
                if ($actualizar) {
                    //Si se actualizo correctamente
                    $bandera = TRUE;
                }
            }
        }else{
            $datos["mensaje"] = "0";
        }

        //Verificamos que se actualizaron los registros
        if ($bandera) {
            //Consultamos si el presupuesto lleva garantias
            $query = $this->mConsultas->get_result_field("id_cita",$datos["id_cita"],"identificador","M","presupuesto_multipunto");
            $garantia_valida = 0;
            foreach ($query as $row){
                if (($row->activo == "1")&&($row->pza_garantia == "1")) {
                    $garantia_valida = 1;
                    //break;
                }
            }

            //Verificamos los archivos generales
            if($this->input->post("uploadfiles_resp") != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $this->input->post("uploadfiles_resp")."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            //Verificamos los archivos del jefe de taller
            if($this->input->post("uploadfiles_jdt_resp") != "") {
                $archivos_3 = $this->validateDoc("uploadfiles_jdt");
                $archivos_jdt = $this->input->post("uploadfiles_jdt_resp")."".$archivos_3;
            }else {
                $archivos_jdt = $this->validateDoc("uploadfiles_jdt");
            }

            $actualiza_reg = array(
                "archivos_presupuesto" => $archivos_gral,
                "archivo_jdt" => $archivos_jdt,
                "envio_jdt" => 1,
                "firma_jdt" => $this->base64ToImage($this->input->post("rutaFirmaJDT"),"firmas"),
                "comentario_jdt" => $this->input->post("comentario_jdt"),
                "ubicacion_proceso" => (($garantia_valida == 1) ? "Espera revisión de garantías" : "Espera revisión del asesor") ,
                "termina_jdt" => $fecha_registro,
                "fecha_actualiza" => $fecha_registro,
            );

            //Guardamos el registro de la refaccion
            $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$actualiza_reg,"id_cita",$datos["id_cita"],"identificador","M");
            if ($actualiza_reg != 0) {
                $datos["mensaje"] = "1";

                //Recuperamos datos extras para la notificación
                $query = $this->mConsultas->notificacionTecnicoCita($datos["id_cita"]); 
                foreach ($query as $row) {
                    $tecnico = $row->tecnico;
                    $modelo = $row->vehiculo_modelo;
                    $placas = $row->vehiculo_placas;
                    $asesor = $row->asesor;
                }

                if (isset($tecnico)) {
                    //Verificamos si el presupuesto va para garantias o para el asesor
                    //Si el presupuesto tiene piezas congarantia
                    if ($garantia_valida == 1) {
                        $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." (con garantías) fue liberado por el jefe de taller el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        $movimiento = "Presupuesto liberado por el jefe de taller (con refacciones en garantías)";

                            //Garantias
                            $this->sms_general('3316725075',$texto);
                    } else {
                        $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." fue liberado por el jefe de taller el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        $movimiento = "Presupuesto liberado por el jefe de taller";

                        //Enviar mensaje al asesor (si no hay rol de jefe de taller)
                        $celular_sms = $this->mConsultas->telefono_asesor($datos["id_cita"]);
                        $this->sms_general($celular_sms,$texto);
                    }
                    
                    //Gerente de servicio
                    //$this->sms_general('3315273100',$texto);
                    
                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    //$this->sms_general('5535527547',$texto);
                    
                    //Grabamos el evento para el panel de gerencia
                    $registro_evento = array(
                        'envia_departamento' => 'Jefe de Taller',
                        'recibe_departamento' => 'Asesores',
                        'descripcion' => 'Presupuesto multipunto liberado por el jefe de taller',
                    );

                    $this->registroEvento($datos["id_cita"],$registro_evento);

                    $this->registrar_movimiento($datos["id_cita"],$movimiento,NULL);
                }
            }else{
                $datos["mensaje"] = "2";
            }
        } else {
            $datos["mensaje"] = "2";
        }
        $this->comodinEditaJefeTaller($datos["id_cita"],$datos);
    }

    //Actualizacion de presupuesto
    public function actualizaAsesor($datos = NULL)
    {
        $fecha_registro = date("Y-m-d H:i");
        $renglones = $this->input->post("registros");
        $renglones_id = $this->input->post("id_registros");

        $ref_aprobadas = [];
        $ref_aprobadas = $this->input->post("autoriza_asesor");

        $bandera = FALSE;
        //var_dump($this->input->post());
        
        if ($ref_aprobadas == NULL) {
            $ref_aprobadas = "";
        }

        //Obtenemos la informacion las refacciones y armamos los paquetes de guardado
        for ($i=0; $i < count($renglones_id) ; $i++) {
            if (strlen($renglones[$i])>19) {
                //Dividimos las celdas
                $campos_registro = explode("_",$renglones[$i]);

                $registro_existe_almacen = explode("*",$campos_registro[3]);
                //Identificamos si es una refaccion nueva, con id = 0
                $refaccion[] = array(
                    "cantidad" => $campos_registro[0],
                    "descripcion" => $campos_registro[1],
                    "num_pieza" => $campos_registro[2],
                    "existe" => $registro_existe_almacen[0],
                    "fecha_planta" => ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1]: "0000-00-00"),
                    "costo_pieza" => $campos_registro[4],
                    "horas_mo" => $campos_registro[5],
                    "costo_mo" => $campos_registro[6],
                    "total_refacion" => $campos_registro[7],
                    "autoriza_asesor" => ((in_array($renglones_id[$i], $ref_aprobadas)) ? "1" : "0"),
                    //"prioridad" => $campos_registro[8],
                    "fecha_actualiza" => $fecha_registro,
                );

                $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$refaccion[$i],'id',$renglones_id[$i]);
                if ($actualizar) {
                    //Si se actualizo correctamente
                    $bandera = TRUE;
                }
            }
        }

        //Verificamos que se actualizaron los registros
        if ($bandera) {
            //Verificamos los archivos generales
            if($this->input->post("uploadfiles_resp") != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $this->input->post("uploadfiles_resp")."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            $firma_Asesor = $this->base64ToImage($this->input->post("rutaFirmaAsesorCostos"),"firmas");
            $actualiza_reg = array(
                "subtotal" => $this->input->post("subTotalMaterial"),
                "iva" => $this->input->post("ivaMaterial"),
                "total" => $this->input->post("totalMaterial"),
                "archivos_presupuesto" => $archivos_gral,
                "firma_asesor" => $firma_Asesor,
                "comentario_asesor" => $this->input->post("nota_asesor"),
                "ubicacion_proceso" => (($firma_Asesor != "") ? "Espera a ser afectada por el cliente" : "Revisada por el asesor") ,
                "termina_asesor" => $fecha_registro,
                "fecha_actualiza" => $fecha_registro,
            );

            //Guardamos el registro de la refaccion
            $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$actualiza_reg,"id_cita",$datos["id_cita"],"identificador","M");
            if ($actualiza_reg != 0) {
                $datos["mensaje"] = "1";

                //Recuperamos datos extras para la notificación
                $query = $this->mConsultas->notificacionTecnicoCita($datos["id_cita"]); 
                foreach ($query as $row) {
                    $tecnico = $row->tecnico;
                    $modelo = $row->vehiculo_modelo;
                    $placas = $row->vehiculo_placas;
                    $asesor = $row->asesor;
                }

                if (isset($tecnico)) {
                   $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." fue actualizado por el asesor el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                    //Gerente de servicio
                    //$this->sms_general('3315273100',$texto);
                    
                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    //$this->sms_general('5535527547',$texto);
                    
                    //Grabamos el evento para el panel de gerencia
                    $registro_evento = array(
                        'envia_departamento' => 'Asesores',
                        'recibe_departamento' => 'Cliente',
                        'descripcion' => 'Presupuesto multipunto listo para enviar al cliente',
                    );

                    

                    if ($firma_Asesor != "") {
                        $movimiento= "Presupuesto fue liberado por el asesor";

                        //Enviamos el mensaje al cliente
                        $this->envio_correo_api($datos["id_cita"]);

                        $this->registroEvento($datos["id_cita"],$registro_evento);
                    } else {
                        $movimiento= "Presupusto actualizado por el asesor";
                    }

                    $this->registrar_movimiento($datos["id_cita"],$movimiento,NULL);
                }
            }else{
                $datos["mensaje"] = "2";
            }
        } else {
            $datos["mensaje"] = "0";
        }
        $this->comodinEditaAsesor($datos["id_cita"],$datos);
    }

    //Actualizacion de presupuesto
    public function actualizaGarantias($datos = NULL)
    {
        $fecha_registro = date("Y-m-d H:i");
        $renglones = $this->input->post("registros");
        $renglones_id = $this->input->post("id_registros");

        $renglones_garantias = [];
        $renglones_garantias = $this->input->post("autoriza_garantia");

        $bandera = FALSE;

        //var_dump($this->input->post());
        //Obtenemos la informacion las refacciones y armamos los paquetes de guardado
        if (count($renglones_garantias) > 0) {
            for ($i=0; $i < count($renglones_garantias) ; $i++) {
                $opciones = explode("=",$renglones_garantias[$i]);
                if(count($opciones) > 0){
                    $autorizacion = array(
                        "autoriza_garantia" => $opciones[1],
                    );
                    
                    $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$autorizacion,'id',$opciones[0]);
                    if ($actualizar) {
                        //Si se actualizo correctamente
                        $bandera = TRUE;
                    }
                }
            }
        }

        //Verificamos que se actualizaron los registros
        if ($bandera) {
            //Verificamos los archivos generales
            if($this->input->post("uploadfiles_resp") != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $this->input->post("uploadfiles_resp")."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            $actualiza_reg = array(
                "archivos_presupuesto" => $archivos_gral,
                "cliente_paga" => (float)$this->input->post("cliente_paga"),
                "ford_paga" => (float)$this->input->post("ford_paga"),
                "total_pago_garantia" => (float)$this->input->post("total_pago_garantia"),
                "envio_garantia" => 1,
                "comentario_garantia" => $this->input->post("comentario_garantia"),
                "firma_garantia" => $this->base64ToImage($this->input->post("rutaFirmaGarantia"),"firmas"),
                "ubicacion_proceso_garantia" => "Revisado y enviado a ventanilla",
                "ubicacion_proceso" => "Espera revisión del asesor",
                "termina_garantias" => $fecha_registro,
                "fecha_actualiza" => $fecha_registro,
            );

            //Guardamos el registro de la refaccion
            $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$actualiza_reg,"id_cita",$datos["id_cita"],"identificador","M");
            if ($actualiza_reg != 0) {
                $datos["mensaje"] = "1";

                //Recuperamos datos extras para la notificación
                $query = $this->mConsultas->notificacionTecnicoCita($datos["id_cita"]); 
                foreach ($query as $row) {
                    $tecnico = $row->tecnico;
                    $modelo = $row->vehiculo_modelo;
                    $placas = $row->vehiculo_placas;
                    $asesor = $row->asesor;
                }

                if (isset($tecnico)) {
                    $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." fue liberado por garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                    $movimiento = "Presupuesto liberado por garantías";
                    //Enviar mensaje al asesor
                    $celular_sms = $this->mConsultas->telefono_asesor($datos["id_cita"]);
                    $this->sms_general($celular_sms,$texto);
                    
                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    //$this->sms_general('5535527547',$texto);
                    
                    //Grabamos el evento para el panel de gerencia
                    $registro_evento = array(
                        'envia_departamento' => 'Garantías',
                        'recibe_departamento' => 'Ventanilla',
                        'descripcion' => 'Presupuesto multipunto liberado por garantías, para solicitar piezas',
                    );

                    $this->registroEvento($datos["id_cita"],$registro_evento);
                    $this->registrar_movimiento($datos["id_cita"],$movimiento,"Garantias");
                }
            }else{
                $datos["mensaje"] = "2";
            }
        } else {
            $datos["mensaje"] = "2";
        }

        $this->comodinEditaGarantias($datos["id_cita"],$datos);
    }

    //Actualizacion de presupuesto
    public function actualizaRegistroTec($datos = NULL)
    {
        $fecha_registro = date("Y-m-d H:i:s");
        $renglones = $this->input->post("registros");
        $renglones_garantias = $this->input->post("registrosGarantias");
        $renglones_id = $this->input->post("id_registros");
        
        if ($this->input->post("pzaGarantia") != NULL) {
            $pzas_garantias = $this->input->post("pzaGarantia");
        }else {
            $pzas_garantias = [];
        }

        $ref_tradicionales = 0;
        $ref_garantias = 0;
        
        $bandera = FALSE;

        $nvo_refaccion = [];
        $refaccion = [];
        $id_dase = [];

        //var_dump($this->input->post());
        //Obtenemos la informacion las refacciones y armamos los paquetes de guardado
        for ($i=0; $i < count($renglones) ; $i++) {
            if (strlen($renglones[$i])>19) {
                //Dividimos las celdas
                $campos_registro = explode("_",$renglones[$i]);
                $campos_garantias = explode("_",$renglones_garantias[$i]);

                $registro_existe_almacen = explode("*",$campos_registro[3]);

                //Identificamos si es una refaccion nueva, con id = 0
                if ($renglones_id[$i] == "0") {
                    $nvo_refaccion[] = array(
                        "id" => NULL,
                        "id_cita" => $datos["id_cita"],
                        "identificador" => "M",
                        "ref" => ((isset($campos_registro[9])) ? $campos_registro[9] : NULL),
                        "cantidad" => $campos_registro[0],
                        "cantidad" => $campos_registro[0],
                        "descripcion" => $campos_registro[1],
                        "num_pieza" => $campos_registro[2],
                        "existe" => $registro_existe_almacen[0],
                        "fecha_planta" => ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1]: "0000-00-00"),
                        "costo_pieza" => $campos_registro[4],
                        "horas_mo" => $campos_registro[5],
                        "costo_mo" => $campos_registro[6],
                        "total_refacion" => $campos_registro[7],
                        "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                        "costo_ford" => $campos_garantias[0],
                        "horas_mo_garantias" => $campos_garantias[1],
                        "costo_mo_garantias" => $campos_garantias[2],
                        "autoriza_garantia" => 0,
                        "autoriza_jefeTaller" => 1,
                        "autoriza_asesor" => 0,
                        "autoriza_cliente" => 0,
                        "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                        "prioridad" => ((isset($campos_registro[8])) ? $campos_registro[8] : 2),
                        "estado_entrega" => 0,
                        "activo" => 1,
                        "fecha_registro" => $fecha_registro,
                        "fecha_actualiza" => $fecha_registro
                    );
                //De lo contrario se deduce que es una refaccion que se actualiza
                }else{
                    $refaccion[] = array(
                        "ref" => ((isset($campos_registro[9])) ? $campos_registro[9] : NULL),
                        "cantidad" => $campos_registro[0],
                        "descripcion" => $campos_registro[1],
                        "num_pieza" => $campos_registro[2],
                        "existe" => $registro_existe_almacen[0],
                        "fecha_planta" => ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1]: "0000-00-00"),
                        "costo_pieza" => $campos_registro[4],
                        "horas_mo" => $campos_registro[5],
                        "costo_mo" => $campos_registro[6],
                        "total_refacion" => $campos_registro[7],
                        "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                        "costo_ford" => $campos_garantias[0],
                        "horas_mo_garantias" => $campos_garantias[1],
                        "costo_mo_garantias" => $campos_garantias[2],
                        //"autoriza_garantia" => 0,
                        //"autoriza_jefeTaller" => 1,
                        //"autoriza_asesor" => 0,
                        //"autoriza_cliente" => 0,
                        "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                        "prioridad" => ((isset($campos_registro[8])) ? $campos_registro[8] : 2),
                        //"estado_entrega" => 0
                        "activo" => 1,
                        "fecha_actualiza" => $fecha_registro
                    );

                    $id_dase[] = $renglones_id[$i];
                }

                //Contabilizamos las refacciones tradicionales y las de garantias
                if (!in_array(($i+1),$pzas_garantias)) {
                    $ref_tradicionales++;
                } else {
                    $ref_garantias++;
                }
            }
        }

        //Verificamos si se guardo alguna refaccion (nueva o actualizacion)
        if ((count($nvo_refaccion)>0)||(count($refaccion)>0)) {
            //Para las refacciones que se van a dar de alta
            for ($i=0; $i < count($nvo_refaccion); $i++) {
                //Guardamos el registro de la refaccion
                $id_retorno = $this->mConsultas->save_register('presupuesto_multipunto',$nvo_refaccion[$i]);
                if ($id_retorno != 0) {
                    //Si se guardo al menos una refaccion guardamos el registro general
                    $bandera = TRUE;
                }
            }

            //Para las refacciones que se van a actualizar
            for ($i=0; $i < count($refaccion); $i++) { 
                //Actualizamos el registro de la refaccion
                $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$refaccion[$i],'id',$id_dase[$i]);
                if ($actualizar) {
                    //Si se actualizo correctamente
                    $bandera = TRUE;
                }
            }
        }

        //Si la actualizacion y/o registro se llevo con exito, se actualiza el registro general
        //Validamos si se guardara un registro pivote
        if ($bandera) {
            //Verificamos los archivos generales
            if($this->input->post("uploadfiles_resp") != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $this->input->post("uploadfiles_resp")."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            //Verificamos archivos del tecnico
            if($this->input->post("tempFileTecnico") != "") {
                $archivos_3 = $this->validateDoc("uploadfilesTec");
                $archivos_tec = $this->input->post("tempFileTecnico")."".$archivos_3;
            }else {
                $archivos_tec = $this->validateDoc("uploadfilesTec");
            }

            $reinicia_registro = array(
                //"cliente_paga" => 0,
                "ref_garantias" => $ref_garantias,
                "ref_tradicional" => $ref_tradicionales,
                //"ford_paga" => 0,
                //"total_pago_garantia" => 0,
                "envio_garantia" => 0,
                //"comentario_garantia" => "",
                "firma_garantia" => "",
                //"anticipo" => $this->input->post("anticipoMaterial"),
                //"nota_anticipo" => $this->input->post("anticipoNota"),
                "subtotal" => $this->input->post("subTotalMaterial"),
                "cancelado" => 0,
                "iva" => $this->input->post("ivaMaterial"),
                "total" => $this->input->post("totalMaterial"),
                //"ventanilla" => "",
                "envia_ventanilla" => 0,
                "estado_refacciones" => 0,
                "estado_refacciones_garantias" => 0,
                "archivos_presupuesto" => $archivos_gral,
                "archivo_tecnico" => $archivos_tec,
                //"archivo_jdt" => "",
                "firma_asesor" => "",
                //"comentario_asesor" => "",
                "envio_jdt" => 0,
                "firma_jdt" => "",
                //"comentario_jdt" => "",
                //"comentario_ventanilla" => "",
                "comentario_tecnico" => $this->input->post("comentarioTecnico"),
                "serie" => $this->input->post("serie"),
                "firma_requisicion" => "",
                "firma_requisicion_garantias" => "",
                //"correo_adicional" => "",
                //"envio_proactivo" => 0,
                "acepta_cliente" => "",
                //Si el proceso tiene para del jefe de taller
                "ubicacion_proceso" => "Actualización de presupuesto por el técnico. Espera revisión de Ventanilla.",
                "ubicacion_proceso_garantia" => "Reenviado a ventanilla",

                "termina_ventanilla" => $fecha_registro,
                "termina_jdt" => $fecha_registro,
                "termina_garantias" => $fecha_registro,
                "termina_asesor" => $fecha_registro,
                "termina_cliente" => $fecha_registro,
                "termina_requisicion" => $fecha_registro,
                "termina_requisicion_garantias" => $fecha_registro,
                "solicita_pieza" => $fecha_registro,
                "recibe_pieza" => $fecha_registro,
                "entrega_pieza" => $fecha_registro,
                "solicita_pieza_garantia" => $fecha_registro,
                "recibe_pieza_garantia" => $fecha_registro,
                "entrega_pieza_garantia" => $fecha_registro,
                //"termina_proactivo" => $fecha_registro,
                "fecha_actualiza" => $fecha_registro,
            );

            //Actualizamos el el registro general
            $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$reinicia_registro,"id_cita",$datos["id_cita"],"identificador","M");
            if ($actualiza_reg != 0) {
                $datos["mensaje"] = "1";

                //Recuperamos datos extras para la notificación
                $query = $this->mConsultas->notificacionTecnicoCita($datos["id_cita"]); 
                foreach ($query as $row) {
                    $tecnico = $row->tecnico;
                    $modelo = $row->vehiculo_modelo;
                    $placas = $row->vehiculo_placas;
                    $asesor = $row->asesor;
                }

                if (isset($tecnico)) {
                    //Verificamos si el presupuesto tiene garantias
                    if ($ref_garantias > 0) {
                        $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." modificado por el técnico con refacciones con garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        $movimiento = "Presupuesto modificado por el técnico, reenviado a ventanilla (refacciones con garantías)";

                    }else{
                        $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." modificado por el técnico el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        $movimiento = "Presupuesto modificado por el técnico, reenviado a ventanilla";
                    }

                    //Jefe de taller
                    //Jesus Gonzales 
                    //$this->sms_general('3316724093',$texto);
                    //Juan Chavez
                    //$this->sms_general('3333017952',$texto);
                    //Gerente de servicio
                    //$this->sms_general('3315273100',$texto);
                    
                    //Ventanilla
                    $this->sms_general('3316721106',$texto);
                    $this->sms_general('3319701092',$texto);
                    $this->sms_general('3317978025',$texto);
                    
                    //Enviar mensaje al asesor (si no hay rol de jefe de taller)
                    //$celular_sms = $this->mConsultas->telefono_asesor($datos["id_cita"]);
                    //$this->sms_general($celular_sms,$texto);
                    
                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    //$this->sms_general('5535527547',$texto);
                    
                    //Grabamos el evento para el panel de gerencia
                    $registro_evento = array(
                        'envia_departamento' => 'Técnicos',
                        'recibe_departamento' => 'Ventanilla',
                        'descripcion' => 'Presupuesto multipunto re-editado por el técnico',
                    );

                    $this->registroEvento($datos["id_cita"],$registro_evento);

                    //actualizamos documentacion
                    $this->actualiza_documentacion_2($datos["id_cita"]);
                    $this->registrar_movimiento($datos["id_cita"],$movimiento,NULL);
                    
                }
            }else{
                $datos["mensaje"] = "0";
            }
        }else{
            $datos["mensaje"] = "0";
        }

        $this->comodinEditaTecnico($datos["id_cita"],$datos);
    }

    public function actualizar_tecAjax()
    {
        $fecha_registro = date("Y-m-d H:i:s");
        $renglones = $_POST["registros"];
        $renglones_garantias = $_POST["registrosGarantias"];
        $renglones_id = $_POST["id_registros"];
        
        if (isset($_POST["pzaGarantia"])) {
            $pzas_garantias = $_POST["pzaGarantia"];
        }else {
            $pzas_garantias = [];
        }

        $ref_tradicionales = 0;
        $ref_garantias = 0;
        
        $bandera = FALSE;

        $nvo_refaccion = [];
        $refaccion = [];
        $id_dase = [];

        $valida_edicion = "OK"; //$this->verificar_proceso_garantia($_POST["idOrdenTemp"]);

        if ($valida_edicion == "OK") {
            //Obtenemos la informacion las refacciones y armamos los paquetes de guardado
            for ($i=0; $i < count($renglones) ; $i++) {
                if (strlen($renglones[$i])>19) {
                    //Dividimos las celdas
                    $campos_registro = explode("_",$renglones[$i]);
                    $campos_garantias = explode("_",$renglones_garantias[$i]);

                    $registro_existe_almacen = explode("*",$campos_registro[3]);

                    //Identificamos si es una refaccion nueva, con id = 0
                    if ($renglones_id[$i] == "0") {
                        $nvo_refaccion[] = array(
                            "id" => NULL,
                            "id_cita" => $_POST["idOrdenTemp"],
                            "identificador" => "M",
                            "ref" => ((isset($campos_registro[9])) ? $campos_registro[9] : NULL),
                            "cantidad" => $campos_registro[0],
                            "cantidad" => $campos_registro[0],
                            "descripcion" => $campos_registro[1],
                            "num_pieza" => $campos_registro[2],
                            "existe" => $registro_existe_almacen[0],
                            "fecha_planta" => ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1]: "0000-00-00"),
                            "costo_pieza" => $campos_registro[4],
                            "rep_adicional" => ((isset($campos_registro[11])) ? $campos_registro[11] : 0),
                            "horas_mo" => $campos_registro[5],
                            "costo_mo" => $campos_registro[6],
                            "total_refacion" => $campos_registro[7],
                            "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                            "costo_ford" => $campos_garantias[0],
                            "horas_mo_garantias" => $campos_garantias[1],
                            "costo_mo_garantias" => $campos_garantias[2],
                            "autoriza_garantia" => 0,
                            "autoriza_jefeTaller" => 1,
                            "autoriza_asesor" => 0,
                            "autoriza_cliente" => 0,
                            "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                            "prioridad" => ((isset($campos_registro[8])) ? $campos_registro[8] : 2),
                            "estado_entrega" => 0,
                            "activo" => 1,
                            "fecha_registro" => $fecha_registro,
                            "fecha_actualiza" => $fecha_registro
                        );
                    //De lo contrario se deduce que es una refaccion que se actualiza
                    }else{
                        $refaccion[] = array(
                            "ref" => ((isset($campos_registro[9])) ? $campos_registro[9] : NULL),
                            "cantidad" => $campos_registro[0],
                            "descripcion" => $campos_registro[1],
                            "num_pieza" => $campos_registro[2],
                            "existe" => $registro_existe_almacen[0],
                            "fecha_planta" => ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1]: "0000-00-00"),
                            "costo_pieza" => $campos_registro[4],
                            "rep_adicional" => ((isset($campos_registro[11])) ? $campos_registro[11] : 0),
                            "horas_mo" => $campos_registro[5],
                            "costo_mo" => $campos_registro[6],
                            "total_refacion" => $campos_registro[7],
                            "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                            "costo_ford" => $campos_garantias[0],
                            "horas_mo_garantias" => $campos_garantias[1],
                            "costo_mo_garantias" => $campos_garantias[2],
                            "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                            "prioridad" => ((isset($campos_registro[8])) ? $campos_registro[8] : 2),
                            //"estado_entrega" => 0
                            "activo" => 1,
                            "fecha_actualiza" => $fecha_registro
                        );

                        $id_dase[] = $renglones_id[$i];
                    }

                    //Contabilizamos las refacciones tradicionales y las de garantias
                    if (!in_array(($i+1),$pzas_garantias)) {
                        $ref_tradicionales++;
                    } else {
                        $ref_garantias++;
                    }
                }
            }

            //Verificamos si se guardo alguna refaccion (nueva o actualizacion)
            if ((count($nvo_refaccion)>0)||(count($refaccion)>0)) {
                //Para las refacciones que se van a dar de alta
                for ($i=0; $i < count($nvo_refaccion); $i++) {
                    //Guardamos el registro de la refaccion
                    $id_retorno = $this->mConsultas->save_register('presupuesto_multipunto',$nvo_refaccion[$i]);
                    if ($id_retorno != 0) {
                        //Si se guardo al menos una refaccion guardamos el registro general
                        $bandera = TRUE;
                    }
                }

                //Para las refacciones que se van a actualizar
                for ($i=0; $i < count($refaccion); $i++) { 
                    //Actualizamos el registro de la refaccion
                    $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$refaccion[$i],'id',$id_dase[$i]);
                    if ($actualizar) {
                        //Si se actualizo correctamente
                        $bandera = TRUE;
                    }
                }
            }

            //Si la actualizacion y/o registro se llevo con exito, se actualiza el registro general
            //Validamos si se guardara un registro pivote
            if ($bandera) {
                //Verificamos los archivos generales
                if($_POST["uploadfiles_resp"] != "") {
                    $archivos_2 = $this->validateDoc("uploadfiles");
                    $archivos_gral = $_POST["uploadfiles_resp"]."".$archivos_2;
                }else {
                    $archivos_gral = $this->validateDoc("uploadfiles");
                }

                //Verificamos archivos del tecnico
                if($_POST["tempFileTecnico"] != "") {
                    $archivos_3 = $this->validateDoc("uploadfilesTec");
                    $archivos_tec = $_POST["tempFileTecnico"]."".$archivos_3;
                }else {
                    $archivos_tec = $this->validateDoc("uploadfilesTec");
                }

                $reinicia_registro = array(
                    "ref_garantias" => $ref_garantias,
                    "ref_tradicional" => $ref_tradicionales,
                    "envio_garantia" => 0,
                    "firma_garantia" => "",
                    "subtotal" => $_POST["subTotalMaterial"],
                    "cancelado" => 0,
                    "iva" => $_POST["ivaMaterial"],
                    "total" => $_POST["totalMaterial"],
                    "envia_ventanilla" => 0,
                    "estado_refacciones" => 0,
                    "estado_refacciones_garantias" => 0,
                    "archivos_presupuesto" => $archivos_gral,
                    "archivo_tecnico" => $archivos_tec,
                    "firma_asesor" => "",
                    "envio_jdt" => 0,
                    "firma_jdt" => "",
                    "comentario_tecnico" => $_POST["comentarioTecnico"],
                    //"serie" => $_POST["noSerie"],
                    "firma_requisicion" => "",
                    "firma_requisicion_garantias" => "",
                    "acepta_cliente" => "",
                    //Si el proceso tiene para del jefe de taller
                    "ubicacion_proceso" => "Actualización de presupuesto por el técnico. Espera revisión de Ventanilla.",
                    "ubicacion_proceso_garantia" => "Reenviado a ventanilla",

                    "termina_ventanilla" => $fecha_registro,
                    "termina_jdt" => $fecha_registro,
                    "termina_garantias" => $fecha_registro,
                    "termina_asesor" => $fecha_registro,
                    "termina_cliente" => $fecha_registro,
                    "termina_requisicion" => $fecha_registro,
                    "termina_requisicion_garantias" => $fecha_registro,
                    "solicita_pieza" => $fecha_registro,
                    "recibe_pieza" => $fecha_registro,
                    "entrega_pieza" => $fecha_registro,
                    "solicita_pieza_garantia" => $fecha_registro,
                    "recibe_pieza_garantia" => $fecha_registro,
                    "entrega_pieza_garantia" => $fecha_registro,
                    "fecha_actualiza" => $fecha_registro,
                );

                //Actualizamos el el registro general
                $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$reinicia_registro,"id_cita",$_POST["idOrdenTemp"],"identificador","M");

                if ($actualiza_reg) {
                    //Recuperamos datos extras para la notificación
                    $query = $this->mConsultas->notificacionTecnicoCita($_POST["idOrdenTemp"]); 
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $placas = $row->vehiculo_placas;
                        $asesor = $row->asesor;
                    }

                    if (isset($tecnico)) {
                        //Verificamos si el presupuesto tiene garantias
                        if ($ref_garantias > 0) {
                            $texto = SUCURSAL.". Presupuesto de la Orden: ".$_POST["idOrdenTemp"]." modificado por el técnico con refacciones con garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                            $movimiento = "Presupuesto modificado por el técnico, reenviado a ventanilla (refacciones con garantías)";

                        }else{
                            $texto = SUCURSAL.". Presupuesto de la Orden: ".$_POST["idOrdenTemp"]." modificado por el técnico el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                            $movimiento = "Presupuesto modificado por el técnico, reenviado a ventanilla";
                        }

                        //Gerente de servicio
                        //$this->sms_general('3315273100',$texto);
                        
                        //Ventanilla
                        $this->sms_general('3316721106',$texto);
                        $this->sms_general('3319701092',$texto);
                        $this->sms_general('3317978025',$texto);
                        
                        //Tester
                        //$this->sms_general('3328351116',$texto);
                        //Angel evidencia
                        //$this->sms_general('5535527547',$texto);
                        
                        //Grabamos el evento para el panel de gerencia
                        $registro_evento = array(
                            'envia_departamento' => 'Técnicos',
                            'recibe_departamento' => 'Ventanilla',
                            'descripcion' => 'Presupuesto multipunto re-editado por el técnico',
                        );

                        $this->registroEvento($_POST["idOrdenTemp"],$registro_evento);

                        //actualizamos documentacion
                        $this->actualiza_documentacion_2($_POST["idOrdenTemp"]);
                        $this->registrar_movimiento($_POST["idOrdenTemp"],$movimiento,NULL);
                        
                    }

                    $respuesta = "OK";
                }else{
                    $respuesta = "OCURRIO UN ERROR AL INTENTAR ACTUALIZAR EL PRESUPUESTO";
                }
            }else{
                $respuesta = "NO SE ACTUALIZO NINGUNA REFACCIÓN";
            }
        } else {
            $respuesta = $valida_edicion;
        }

        echo $respuesta;
    }

    public function actualizar_datos_garantia()
    {
        $resultado = "";
        //Verificamos que llege la informacion
        if (isset($_POST["ref_clase"])) {
            $body["costo_ford"] = $_POST["costo_ford"];
            $body["horas_mo_garantias"] = $_POST["horas_mo_g"];
            $body["costo_mo_garantias"] = $_POST["costo_mo_g"];

            $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$body,'id',$_POST["ref_clase"]);
            if ($actualizar) {
                //Si se actualizo correctamente, registramos el movimiento
                $movimiento = "Se actualizan los costos ford de la pieza con garantia (posterior a liberación del panel)";
                $this->registrar_movimiento($_POST["id_cita"],$movimiento,$_POST["panel"]);

                //Mandamos la actualizacion al panle de garantias
                $this->actualizar_formatogarantia($_POST["ref_clase"]);
                
                $resultado = "OK";
            }else{
                $resultado = "ERROR AL INSERTAR";
            }
        } else {
            $resultado = "NO SE RECIBIO LA INFORMACIÓN CORRECTAMENTE";
        }

        echo $resultado;
    }

    public function verificar_proceso_garantia($id_cita='')
    {
        $validacion = "ERROR";
        $ref_garantias = $this->mConsultas->total_ref_garantia($id_cita);
        //Se verifica si el rpesupuesto ya fue afectado por el cliente
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($query as $row){
            $firma_requisicion = (($row->firma_requisicion != "") ? "1" : "0");
            $firma_requisicion_garantias = (($row->firma_requisicion_garantias != "") ? "1" : "0");
            $acepta_cliente = $row->acepta_cliente;
        }

        //Si no ha sido afectado por el clienteo fue rechazado totalmente, se puede editar
        if (($acepta_cliente == "")||($acepta_cliente == "No")) {
            $validacion = "OK";

        //De lo contrario validamos temas de garantias
        } else {
            //Si no tienen refacciones con garantias, se puede editar
            if ($ref_garantias == 0) {
                $validacion = "OK";

            //De lo contrario validamosfirmas de la requisicion
            } else {
                //Si tiene ambas firmas de la requicision, se puede editar
                if (($firma_requisicion == "1")&&($firma_requisicion_garantias == "1")) {
                    $validacion = "OK";

                //Verificamos campo faltante
                } else {
                    //Si tieine la firma del tecnico, y no la de garantias se bloquea
                    if ($firma_requisicion == "0") {
                        $validacion = "OK";
                    } else {
                        if ($firma_requisicion_garantias == "1") {
                            $validacion = "OK";
                        } else {
                            $validacion = "FALTA FIRMA DE GARANTÍAS EN LA REQUISICIÓN PARA PERMITIR EDICION";
                        }
                    }
                    
                    /*if (($firma_requisicion == "0")&&($firma_requisicion_garantias == "0")) {
                        $validacion = "FALTAN AMBAS FIRMAS DE LA REQUISICÓN PARA PERMITIR EDICION";
                    }elseif ($firma_requisicion == "0") {
                        $validacion = "FALTA FIRMA DEL TÉCNICO EN LA REQUISICIÓN PARA PERMITIR EDICION";
                    }else{
                        $validacion = "FALTA FIRMA DE GARANTÍAS EN LA REQUISICIÓN PARA PERMITIR EDICION";
                    }*/
                }
            }
        }

        return $validacion;
    }

    public function actualizar_formatogarantia($id_ref='')
    {
        //Recuperamos los datos de la refaccion para actualizar en el panel de garantias
        $query = $this->mConsultas->get_result_field("id",$id_ref,"identificador","M","presupuesto_multipunto");
        foreach ($query as $row){
            $id_cita = $row->id_cita;
            $num_pieza = $row->num_pieza;
            $costo_ford = $row->costo_ford;
            $horas_mo_garantias = $row->horas_mo_garantias;
            $costo_mo_garantias = $row->costo_mo_garantias;
            $alta_garantia = $row->alta_garantia;
        }

        if (isset($id_cita)) {
            /*$paquete = array(
                'no_orden' => $id_cita,
                'num_pieza' => $num_pieza,
                'precio' => $costo_ford,
                'id_parte' => $id_ref
            );*/

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,API_GARANTIA."index.php/apis/GarantiasRest/actualizar_precios_partes");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                        "no_orden=".$id_cita."&num_pieza=".$num_pieza."&precio=".$costo_ford."&id_parte=".$id_ref);
            // Receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            $err = curl_error($ch);

            curl_close($ch);

            $dataregistro = json_decode($response, TRUE);

            if (isset($dataregistro["data"])) {
                $movimiento = "Se envio la información al panel de garantías, estatus: ".$dataregistro["status"];
                if (isset($dataregistro["message"])) {
                    if (isset($dataregistro["message"]["no_orden"])) {
                        $movimiento .= " Mensaje (no_orden): ".$dataregistro["message"]["no_orden"]." ";
                    }

                    if (isset($dataregistro["message"]["num_pieza"])) {
                        $movimiento .= " Mensaje (num_pieza): ".$dataregistro["message"]["num_pieza"]." ";
                    }

                    if (isset($dataregistro["message"]["precio"])) {
                        $movimiento .= " Mensaje (precio): ".$dataregistro["message"]["precio"]." ";
                    }

                    if (isset($dataregistro["message"]["id_parte"])) {
                        $movimiento .= " Mensaje (id_parte): ".$dataregistro["message"]["id_parte"]." ";
                    }
                }else{
                    $movimiento .= " Mensaje : OK";
                }
            } else {
                $movimiento = "Ocurrio un error en el envio de informacion ";
            }            

        } else {
            $movimiento = "No se encontro la refacción con el id = ".$id_ref.". El precio no se actualizo en el panel de garantias";
        }

        $this->registrar_movimiento($id_cita,$movimiento,"Garantias - Externo");
        return TRUE;
        
    }

    //Actualizacion de presupuesto
    public function actualizaRegistroVentanillaTec($datos = NULL)
    {
        $fecha_registro = date("Y-m-d H:i");
        //$firma = $this->input->post("rutaFirmaReq");
        $formulario = $this->input->post("destino_vista");

        //Identificamos los campos del presupuesto que se modificaran
        if ($formulario == "T") {
            $registro_firma = array(
                "firma_requisicion" => $this->base64ToImage($this->input->post("rutaFirmaReq"),"firmas"),
                "ubicacion_proceso" => "Técnico ha firmado requisición. Refacciones entregadas.",
                "termina_requisicion" => $fecha_registro,
                "fecha_actualiza" => $fecha_registro,
            );
        }elseif ($formulario == "G") {
            $registro_firma = array(
                "firma_requisicion_garantias" => $this->base64ToImage($this->input->post("rutaFirmaReq"),"firmas"),
                "ubicacion_proceso_garantia" => "Técnico ha firmado requisición. Refacciones entregadas.",
                "termina_requisicion_garantias" => $fecha_registro,
                "fecha_actualiza" => $fecha_registro,
            );
        }

        //Guardamos el registro de la refaccion
        $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$registro_firma,"id_cita",$datos["id_cita"],"identificador","M");
        if ($actualiza_reg != 0) {
            $datos["mensaje"] = "1";

            //Recuperamos datos extras para la notificación
            $query = $this->mConsultas->notificacionTecnicoCita($datos["id_cita"]); 
            foreach ($query as $row) {
                $tecnico = $row->tecnico;
                $modelo = $row->vehiculo_modelo;
                $placas = $row->vehiculo_placas;
                $asesor = $row->asesor;
            }

            if (isset($tecnico)) {
                //Verificamos si el presupuesto tiene garantias
                if ($formulario == "G") {
                    $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." se firmo la requisición con garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                    $movimiento = "Presupuesto: se firmo la requisición con garantías";

                    //Garantias
                    //$this->sms_general('',$texto);
                }else{
                    $texto = SUCURSAL.". Presupuesto de la Orden: ".$datos["id_cita"]." se firmo la requisición el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                    $movimiento = "Presupuesto: se firmo la requisición";
                }

                //Jefe de taller (si aplica)
                //$this->sms_general('',$texto);
                
                //Enviar mensaje al asesor (si no hay rol de jefe de taller)
                $celular_sms = $this->mConsultas->telefono_asesor($datos["id_cita"]);
                //$this->sms_general($celular_sms,$texto);
                
                //Tester
                $this->sms_general('3328351116',$texto);
                //Angel evidencia
                ////$this->sms_general('5535527547',$texto);
                
                //Grabamos el evento para el panel de gerencia
                $registro_evento = array(
                    'envia_departamento' => 'Ventanilla',
                    'recibe_departamento' => 'Admin',
                    'descripcion' => 'Requisicion por refacciones firmada',
                );

                $this->registroEvento($datos["id_cita"],$registro_evento);
                $this->registrar_movimiento($datos["id_cita"],$movimiento,"Ventanilla");
            }
        }else{
            $datos["mensaje"] = "0";
        }

        //Presupuesto tradicional
        if ($formulario == "T") {
            $this->comodinRequisicion_T($datos["id_cita"],$datos);
        //Presupuesto garantias
        }else{
            $this->comodinRequisicion_G($datos["id_cita"],$datos);
        }

    }

    public function actualiza_firma_requisicion()
    {
        $fecha_registro = date("Y-m-d H:i:s");
        //$firma = $this->input->post("rutaFirmaReq");
        $formulario = $_POST["destino_vista"];

        //Identificamos los campos del presupuesto que se modificaran
        if ($formulario == "T") {
            $registro_firma = array(
                "firma_requisicion" => $this->base64ToImage($_POST["rutaFirmaReq"],"firmas"),
                "tecnico_nombre" => $_POST["nombre"],
                "ubicacion_proceso" => "Técnico ha firmado requisición. Refacciones entregadas.",
                "termina_requisicion" => date("Y-m-d H:i:s"),
                "fecha_actualiza" => date("Y-m-d H:i:s"),
            );

            $movimiento = "Presupuesto: se firmo la requisición con garantías : ".$_POST["nombre"];

        }elseif ($formulario == "G") {
            if ($_POST["tipo_ruta"] == "1") {
                $registro_firma = array(
                    "ubicacion_proceso_garantia" => "Garantías ha finalizado comentarios para la requisición.",
                    "termina_requisicion_garantias" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s"),
                );

                $movimiento = "Presupuesto: finalizo la inseción de comentarios para la requisición.";

            } else {
                $registro_firma = array(
                    "firma_requisicion_garantias" => $this->base64ToImage($_POST["rutaFirmaReq"],"firmas"),
                    "garantias_nombre" => $_POST["nombre"],
                    "ubicacion_proceso_garantia" => "Garantías ha finalizado recepción de piezas de la requisición.",
                    "fecha_entrega_garantia" => date("Y-m-d H:i:s"),
                    "fecha_actualiza" => date("Y-m-d H:i:s"),
                );

                $movimiento = "Presupuesto: se firmo la recepción de piezas de garantias : ".$_POST["nombre"];
            }
        }

        //Guardamos el registro de la refaccion
        $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$registro_firma,"id_cita",$_POST["id_cita"],"identificador","M");
        if ($actualiza_reg) {
            //Recuperamos datos extras para la notificación
            $query = $this->mConsultas->notificacionTecnicoCita($_POST["id_cita"]); 
            foreach ($query as $row) {
                $tecnico = $row->tecnico;
                $modelo = $row->vehiculo_modelo;
                $placas = $row->vehiculo_placas;
                $asesor = $row->asesor;
            }

            if (isset($tecnico)) {
                //Verificamos si el presupuesto tiene garantias
                if ($formulario == "G") {
                    $texto = SUCURSAL.". Presupuesto de la Orden: ".$_POST["id_cita"]." se firmo la requisición con garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                    //Garantias
                    //$this->sms_general('',$texto);
                }else{
                    $texto = SUCURSAL.". Presupuesto de la Orden: ".$_POST["id_cita"]." se firmo la requisición el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                }

                //Jefe de taller (si aplica)
                //$this->sms_general('',$texto);
                if ($formulario == "T") {
                    //Ventanilla
                    $this->sms_general('3316721106',$texto);
                    $this->sms_general('3319701092',$texto);
                    $this->sms_general('3317978025',$texto);

                    //Enviar mensaje al asesor (si no hay rol de jefe de taller)
                    $celular_sms = $this->mConsultas->telefono_asesor($_POST["id_cita"]);
                    $this->sms_general($celular_sms,$texto);
                }
                //Tester
                //$this->sms_general('3328351116',$texto);
                //Angel evidencia
                //$this->sms_general('5535527547',$texto);
                
                //Grabamos el evento para el panel de gerencia
                $registro_evento = array(
                    'envia_departamento' => 'Ventanilla',
                    'recibe_departamento' => 'Admin',
                    'descripcion' => 'Requisicion por refacciones firmada',
                );

                $this->registroEvento($_POST["id_cita"],$registro_evento);
                $this->registrar_movimiento($_POST["id_cita"],$movimiento,"Ventanilla");

                $conclusion = "OK";

            }else{
                $conclusion = "NO SE PUDO ENVIAR LA NOTIFICACIÓN DEL PROCESO";
            }
        }else{
            $conclusion = "NO SE PUDO ACTUALIZAR LA FIRMA DE LA REQUISICIÓN";
        }

        echo $conclusion;
    }

    public function notas_garantias_refaccion()
    {
        $fecha_registro = date("Y-m-d H:i:s");

        $registro_notas = array(
            "nombre_gerente_req" => $_POST["nombre_firma"],
            "firma_gerente_req" => $this->base64ToImage($_POST["firma_base"],"firmas"),
            "parte_remplazada_req" => $_POST["inspeccion"],
            "asc_servicio_req" => $_POST["asc"],
            "fecha_firma_greq" => $fecha_registro,
        );

        //Guardamos el registro de la refaccion
        $actualiza_reg = $this->mConsultas->update_table_row('presupuesto_multipunto',$registro_notas,"id",$_POST["id_refaccion"]);
        if ($actualiza_reg) {
            $movimiento = "Se agrega comentario de inspeccion de pieza instalada [ID: ".$_POST["id_refaccion"]."] Orden: ".$_POST["id_cita"];
            
            //Grabamos el evento para el panel de gerencia
            $registro_evento = array(
                'envia_departamento' => 'Garantías',
                'recibe_departamento' => 'Garantías',
                'descripcion' => 'Se agrega comentario de inspeccion de pieza instalada',
            );

            $this->registroEvento($_POST["id_cita"],$registro_evento);
            $this->registrar_movimiento($_POST["id_cita"],$movimiento,"Garantias");

            $conclusion = "OK=".$registro_notas["firma_gerente_req"];
        }else{
            $conclusion = "NO SE PUDO ACTUALIZAR LA REQUISICIÓN=";
        }

        echo $conclusion;
    }

    //Validamos y guardamos los documentos
    public function validateDoc($nombreDoc = NULL)
    {
        $path = "";
        //300 segundos  = 5 minutos
        //ini_set('max_execution_time',600);

        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
        for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docMultipunto";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_PM";
            for($j=0; $j<=6; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }

    //Convertir canvas base64 a imagen (firmas)
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_PM";
        for($i=0; $i<=6; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    /**************************************************************************************************************************************
                                        Inicio Seccion de entrada para multiples vistas
    **************************************************************************************************************************************/
    //Parada para cuando se acaba de crear un presupuesto y se debe visualizar
    public function comodinAltaTec($id_cita = "",$datos = NULL)
    {
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        //$id_cita = ((is_numeric($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        //Recuperamos el folio de intelisis
        $datos["folio_externo"] = $this->mConsultas->id_orden_intelisis($id_cita);
        
        //Consultamos su ya existe un presupuesto
        $revision = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $idCotizacion = $row->id;
            $acepta_cliente = $row->acepta_cliente;
        }

        //Si ya existe un presupuesto, solo lo visualizamos
        if (isset($idCotizacion)) {
            $datos["vista"] = "2A";
            $datos["destinoVista"] = "AltaTecnico";
            $this->setData($id_cita,$datos);
        //De lo contratio, si no existe, vamos a la vista de alta
        }else{
            $datos["vista"] = "1";
            $this->index($id_cita,$datos);
        }
    }

    //Parada comodin para listar presupuestos de ventanilla
    public function comodinListaVentanilla($vistaDestino = "",$datos = NULL)
    {
        $datos["destino"] = "Ventanilla";
        
        $mes_actual = date("m");
        if ($mes_actual <= 1) {
            $fecha_busqueda = (date("Y")-1)."-"."12-20";
        } else {
            $fecha_busqueda = date("Y")."-".((($mes_actual - 1) < 10) ? "0" : "").($mes_actual - 1)."-20";
        }

        $datos["presupuestos"] = $this->mConsultas->presupuestosCreados_ventanilla($fecha_busqueda);
        $this->loadAllView($datos,'presupuestos/ventanilla/buscador_ventanilla_2');
        //var_dump($datos);
        //$this->listaCotizacionesVentanilla($vistaDestino,$datos);
    }

    //Parada para indicar que sera una vista de refacciones
    public function comodinEditaVentanilla($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "3";
        $datos["dirige"] = "Ventanilla";
        $datos["destinoVista"] = "Ventanilla";

        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para indicar que sera una vista de refacciones
    public function comodinEntrega_G($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "6";
        $datos["dirige"] = "Entrega_G";
        $datos["destinoVista"] = "EntregaVentanilla";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para indicar que sera una vista de refacciones
    public function comodinEntrega_T($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "6";
        $datos["dirige"] = "Entrega_T";
        $datos["destinoVista"] = "EntregaVentanilla";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para indicar que sera una vista de refacciones / requisicion
    public function comodinRequisicion_G($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "9";
        $datos["dirige"] = "Requisicion_G";
        $datos["destinoVista"] = "RequisicionVentanilla";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para indicar que sera una vista de refacciones
    public function comodinRequisicion_T($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "9";
        $datos["dirige"] = "Requisicion_T";
        $datos["destinoVista"] = "RequisicionVentanilla";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para indicar que sera una vista de refacciones / requisicion
    public function comodinRequisicion_G_PDF($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "10";
        $datos["dirige"] = "Requisicion_G_PDF";
        $datos["destinoVista"] = "RequisicionPDF";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));

        $validacion = 0;
        //Verificamos si ya se firmo la requisicion de garantia
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($query as $row){
            if ($row->firma_requisicion_garantias != "") {
                $validacion = 1;
            }
        }

        if ($validacion == 1) {
            $this->valoresRequisicion($id_cita,$datos);
        }else{
            redirect(base_url().'Requisicion_Firma_G/'.$this->encrypt($id_cita));
        }
    }

    //Parada para indicar que sera una vista de refacciones
    public function comodinRequisicion_T_PDF($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "10";
        $datos["dirige"] = "Requisicion_T_PDF";
        $datos["destinoVista"] = "RequisicionPDF";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->valoresRequisicion($id_cita,$datos);
    }

    public function comodinRequisicion_PDF_Descarga($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "25";
        $datos["dirige"] = "Requisicion_Descarga";
        $datos["destinoVista"] = "RequisicionPDF";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->valoresRequisicion($id_cita,$datos);
    }

    //Parada para indicar que sera una vista de refacciones / requisicion
    public function comodinRecibePza_G($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "26A";
        $datos["dirige"] = "Recibido_G";
        $datos["destinoVista"] = "RequisicionVentanilla";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    public function comodinRecibePza_G_PDF($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "26B";
        $datos["dirige"] = "Recibe_Garantias_PDF";
        $datos["destinoVista"] = "ReciboPDF";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->valoresRequisicion($id_cita,$datos);
    }

    public function comodinRecibePza_G_PDF_Descarga($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "26C";
        $datos["dirige"] = "Recibe_GDescargas";
        $datos["destinoVista"] = "ReciboPDF";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->valoresRequisicion($id_cita,$datos);
    }

    //Parada comodin para listar presupuestos del jefe de taller
    public function comodinListaJefeTaller($vistaDestino = "",$datos = NULL)
    {
        $datos["destino"] = "JDT";
        
        $mes_actual = date("m");
        if ($mes_actual <= 1) {
            $fecha_busqueda = (date("Y")-1)."-"."12-20";
        } else {
            $fecha_busqueda = date("Y")."-".((($mes_actual - 1) < 10) ? "0" : "").($mes_actual - 1)."-20";
        }

        $datos["presupuestos"] = $this->mConsultas->presupuestosJDT_table($fecha_busqueda);
        $this->loadAllView($datos,'presupuestos/jefeTaller/buscador_jefeTaller_2');

        //$this->listaCotizaciones($vistaDestino,$datos);
    }

    //Parada comodin para listar presupuestos del jefe de taller
    public function comodinListaJDTEstatus($vistaDestino = "",$datos = NULL)
    {
        $datos["destino"] = "ESTATUS";
        
        $mes_actual = date("m");
        if ($mes_actual <= 1) {
            $fecha_busqueda = (date("Y")-1)."-"."12-20";
        } else {
            $fecha_busqueda = date("Y")."-".((($mes_actual - 1) < 10) ? "0" : "").($mes_actual - 1)."-20";
        }

        $datos["presupuestos"] = $this->mConsultas->presupuestosJDTStatus_table($fecha_busqueda);
        $this->loadAllView($datos,'presupuestos/jefeTaller/buscador_estatus_2'); 
        //$this->listaCotizaciones($vistaDestino,$datos);
    }

    //Parada para indicar que sera una vista del jefe de taller
    public function comodinEditaJefeTaller($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "4";
        $datos["dirige"] = "JDT";
        $datos["destinoVista"] = "JDT";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para indicar que sera una vista del jefe de taller
    public function comodinEditaJDTEstatus($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "10";
        $datos["dirige"] = "Estatus";
        $datos["destinoVista"] = "Estatus";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin para listar presupuestos de ventanilla
    public function comodinListaAsesor($vistaDestino = "",$datos = NULL)
    {
        $datos["destino"] = "ASESOR";
        
        $mes_actual = date("m");
        if ($mes_actual <= 1) {
            $fecha_busqueda = (date("Y")-1)."-"."12-20";
        } else {
            $fecha_busqueda = date("Y")."-".((($mes_actual - 1) < 10) ? "0" : "").($mes_actual - 1)."-20";
        }

        $datos["presupuestos"] = $this->mConsultas->presupuestosAsesor_table($fecha_busqueda);
        $this->loadAllView($datos,'presupuestos/asesor/buscador_asesor_2'); 

        //$this->listaCotizaciones($vistaDestino,$datos);
    }

    //Parada para entra a vista del asesor / Admin
    public function comodinEditaAsesor($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "2";
        $datos["dirige"] = "ASESOR";
        $datos["destinoVista"] = "ASESOR";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin para listar presupuestos de ventanilla
    public function comodinListaGarantias($vistaDestino = "",$datos = NULL)
    {
        $datos["destino"] = "GARANTIAS";

        $mes_actual = date("m");
        if ($mes_actual <= 1) {
            $fecha_busqueda = (date("Y")-1)."-"."12-20";
        } else {
            $fecha_busqueda = date("Y")."-".((($mes_actual - 1) < 10) ? "0" : "").($mes_actual - 1)."-20";
        }

        $datos["presupuestos"] = $this->mConsultas->presupuestosGarantias_table($fecha_busqueda);
        $this->loadAllView($datos,'presupuestos/garantias/buscador_garantias_2'); 

        //$this->listaCotizaciones($vistaDestino,$datos);
    }

    //Parada para entra a vista del garantias
    public function comodinEditaGarantias($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "5";
        $datos["dirige"] = "GARANTIAS";
        $datos["destinoVista"] = "GARANTIAS";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para entrar a la vista del cliente 
    public function comodinEditaCliente($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "7";
        $datos["dirige"] = "CLIENTE";
        $datos["destinoVista"] = "CLIENTE";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada comodin para listar presupuestos tecnicos
    public function comodinListaTecnico($vistaDestino = "",$datos = NULL)
    {
        $datos["destino"] = "TECNICO";
        
        $mes_actual = date("m");
        if ($mes_actual <= 1) {
            $fecha_busqueda = (date("Y")-1)."-"."12-20";
        } else {
            $fecha_busqueda = date("Y")."-".((($mes_actual - 1) < 10) ? "0" : "").($mes_actual - 1)."-20";
        }

        if ($this->session->userdata('rolIniciado')) {
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {

                $idTecnico = $this->session->userdata('idUsuario'); 
                $datos["presupuestos"] = $this->mConsultas->presupuestosCreados_tec_table($idTecnico,$fecha_busqueda);
            }else{
                $datos["presupuestos"] = $this->mConsultas->presupuestosCreados_table($fecha_busqueda);
            }
        }else{
            $datos["presupuestos"] = $this->mConsultas->presupuestosCreados_table($fecha_busqueda);
        }

        $this->loadAllView($datos,'presupuestos/tecnico/buscador_tecnico_2');

        //$this->listaCotizaciones($vistaDestino,$datos);
    }

    //Parada para entrar a la vista del tecnico 
    public function comodinEditaTecnico($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "8";
        $datos["dirige"] = "TECNICO";
        $datos["destinoVista"] = "TECNICO";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para indicar que sera una vista del cliente
    public function comodinGeneraPdf($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "11";
        $datos["dirige"] = "PDF";
        $datos["destinoVista"] = "PDF";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    //Parada para indicar que sera una vista tipo cliente para garantias
    public function comodinModuloGarantia($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "20";
        $datos["dirige"] = "MGarantias";
        $datos["destinoVista"] = "MGarantias";
        $id_cita = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita,$datos);
    }

    /**************************************************************************************************************************************
                                        Fin Seccion de entrada para multiples vistas
    **************************************************************************************************************************************/

    //Recuperamos la informacion de la cotizacion
    public function setData($id_cita = 0,$datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["tipoRegistro"])) {
            $datos["tipoRegistro"] = "";
        }

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }

        //Recuperamos el id interno de magic
        //$datos["orden_CDK"] = $this->mConsultas->orden_cdk_query($id_cita);

        //Recuperamos la informacion de los registros
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($query as $row){
            $datos["folio_externo"] = $row->folio_externo;
            $datos["ref_garantias"] = $row->ref_garantias;
            //Valores para garantias
            $datos["cliente_paga"] = $row->cliente_paga;
            $datos["ford_paga"] = $row->ford_paga;
            $datos["total_pago_garantia"] = $row->total_pago_garantia;
            $datos["envio_garantia"] = $row->envio_garantia;
            $datos["comentario_garantia"] = $row->comentario_garantia;
            $datos["firma_garantia"] = $row->firma_garantia;
            //Valores globales
            $datos["anticipo"] = $row->anticipo;
            $datos["notaAnticipo"] = $row->nota_anticipo;
            $datos["subtotal"] = $row->subtotal;
            $datos["cancelado"] = $row->cancelado;
            $datos["iva"] = $row->iva;
            $datos["total"] = (((float)$row->subtotal - (float)$row->cancelado) * 1.16)-$row->anticipo;
            //Valores de proceso
            $datos["envia_ventanilla"] = $row->envia_ventanilla;
            $datos["ubicacion_proceso"] = $row->ubicacion_proceso;
            $datos["ubicacion_proceso_garantia"] = $row->ubicacion_proceso_garantia;
            $datos["estado_refacciones"] = $row->estado_refacciones;
            $datos["estado_refacciones_garantias"] = $row->estado_refacciones_garantias;
            //Archivos
            $datos["archivos_presupuesto"] = explode("|",$row->archivos_presupuesto);
            $datos["archivos_presupuesto_lineal"] = $row->archivos_presupuesto;

            $datos["archivo_tecnico"] = explode("|",$row->archivo_tecnico);
            $datos["archivo_tecnico_lineal"] = $row->archivo_tecnico;

            $datos["archivo_jdt"] = explode("|",$row->archivo_jdt);
            $datos["archivo_jdt_lineal"] = $row->archivo_jdt;
            
            $datos["firma_asesor"] = $row->firma_asesor;
            $datos["nota_asesor"] = $row->comentario_asesor;
            
            $datos["envio_jdt"] = $row->envio_jdt;
            $datos["firma_jdt"] = $row->firma_jdt;
            $datos["comentario_jdt"] = $row->comentario_jdt;
            
            $datos["comentario_ventanilla"] = $row->comentario_ventanilla;
            $datos["comentario_tecnico"] = $row->comentario_tecnico;
            
            $datos["serie"] = $row->serie;
            $datos["firma_requisicion"] = (($row->firma_requisicion != "") ? "1" : "0");
            //$datos["firma_requisicion_url"] = $row->firma_requisicion;
            $datos["firma_requisicion_garantias"] = (($row->firma_requisicion_garantias != "") ? "1" : "0");
            //$datos["firma_requisicion_garantias_url"] = $row->firma_requisicion_garantias;
            $datos["correo_adicional"] = $row->correo_adicional;
            $datos["envio_proactivo"] = $row->envio_proactivo;
            $datos["acepta_cliente"] = $row->acepta_cliente;

            $datos["fecha_alta"] = explode(" ",$row->fecha_alta);
            $fragmentos = explode("-",$datos["fecha_alta"][0]);
            $mes_index = (int)$fragmentos[1];
            $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
            //$datos["mes"] = $meses[date("n")-1];
            $datos["fecha_limite"] = $fragmentos[2]." de ".$meses[(($mes_index < 12) ? $mes_index : 0)]." del ".$fragmentos[0];

            $datos["envia_garantia_mod"] = $row->anexa_garantias;
        }

        //Recuperamos las refacciones
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_multipunto");

        $datos["registrosControl"] = 0;
        $datos["registrosGarantias"] = 0;

        $datos["pzs_aprobadas"] = 0;
        $datos["pzs_entregadas"] = 0;
        $datos["pzs_recibidas"] = 0;
        $datos["pzs_entregada_ref"] = 0;

        $datos["sin_aprobar_jdt"] = 0;

        foreach ($query as $row){
            //Confirmamos que la refaccion este activa
            if ($row->activo == "1") {
                //Definimos que valores se cargaran
                if ($datos["destinoVista"] == "ASESOR") {
                    if ($row->autoriza_jefeTaller == 0) {
                        $datos["sin_aprobar_jdt"] += 1;
                    }

                    //Si ya reviso el jefe de taller
                    if ($row->autoriza_jefeTaller == "1") {
                        //Si el presupuesto tiene refacciones con garantias, que ya haya sido liberado de garantias
                        if (((int)$datos["ref_garantias"] > 0) && ( $datos["envio_garantia"] == "1")) {
                            $datos["id_refaccion"][] = $row->id;

                            $datos["cantidad"][] = $row->cantidad;
                            $datos["descripcion"][] = $row->descripcion;
                            $datos["num_pieza"][] = $row->num_pieza;
                            $datos["existe"][] = $row->existe;
                            $datos["fecha_planta"][] = $row->fecha_planta;
                            $datos["costo_pieza"][] = $row->costo_pieza;
                            $datos["horas_mo"][] = $row->horas_mo;
                            $datos["costo_mo"][] = $row->costo_mo;
                            $datos["total_refacion"][] = $row->total_refacion;
                            $datos["prioridad"][] = $row->prioridad;

                            $datos["rep_adicional"][] = $row->rep_adicional;
                            $datos["costo_aseguradora"][] = $row->costo_aseguradora;

                            $datos["renglon"][] = $row->cantidad."_".$row->descripcion."_".$row->num_pieza."_".$row->existe."*".$row->fecha_planta."_".$row->costo_pieza."_".$row->horas_mo."_".$row->costo_mo."_".$row->total_refacion."_".$row->prioridad."_".$row->ref;

                            $datos["pza_garantia"][] = $row->pza_garantia;

                            $datos["costo_ford"][] = $row->costo_ford;
                            $datos["horas_mo_garantias"][] = $row->horas_mo_garantias;
                            $datos["costo_mo_garantias"][] = $row->costo_mo_garantias;

                            $datos["renglonGarantia"][] = $row->costo_ford."_".$row->horas_mo_garantias."_".$row->costo_mo_garantias;

                            $datos["autoriza_garantia"][] = $row->autoriza_garantia;
                            $datos["autoriza_jefeTaller"][] = $row->autoriza_jefeTaller;
                            $datos["autoriza_asesor"][] = $row->autoriza_asesor;
                            $datos["autoriza_cliente"][] = $row->autoriza_cliente;

                            $datos["estado_entrega"][] = $row->estado_entrega;
                            $datos["referencia"][] = $row->ref;

                            //Refacciones (activas) totales del presupuesto
                            $datos["registrosControl"] += 1;
                            //Refacciones (activas) con garantias totales del presupuesto
                            if ($row->pza_garantia == "1") {
                                $datos["registrosGarantias"] += 1;
                            }

                            //Contabilizamos refaccions aprobadas
                            if (($row->autoriza_garantia == "1")||($row->autoriza_cliente =="1")) {
                                $datos["pzs_aprobadas"] += 1;
                            }

                            if ($row->estado_entrega == "3") {
                                $datos["pzs_entregadas"] += 1;
                            }

                            if ($row->estado_entrega == "2") {
                                $datos["pzs_recibidas"] += 1;
                            }

                            //Verificamos si es una refaccion o mano de obra
                            if ((($row->costo_pieza == 0)&&($row->num_pieza == ""))||(($row->costo_ford == 0)&&($row->autoriza_garantia == "1"))) {
                                $datos["tipo_refaccion"][] = "MO";
                            } else {
                                $datos["tipo_refaccion"][] = "REF";
                            }
                            
                        //Si el presupuesto no tiene garantias
                        } else if ($datos["ref_garantias"] == "0") {
                            $datos["id_refaccion"][] = $row->id;

                            $datos["cantidad"][] = $row->cantidad;
                            $datos["descripcion"][] = $row->descripcion;
                            $datos["num_pieza"][] = $row->num_pieza;
                            $datos["existe"][] = $row->existe;
                            $datos["fecha_planta"][] = $row->fecha_planta;
                            $datos["costo_pieza"][] = $row->costo_pieza;
                            $datos["horas_mo"][] = $row->horas_mo;
                            $datos["costo_mo"][] = $row->costo_mo;
                            $datos["total_refacion"][] = $row->total_refacion;
                            $datos["prioridad"][] = $row->prioridad;

                            $datos["rep_adicional"][] = $row->rep_adicional;
                            $datos["costo_aseguradora"][] = $row->costo_aseguradora;

                            $datos["renglon"][] = $row->cantidad."_".$row->descripcion."_".$row->num_pieza."_".$row->existe."*".$row->fecha_planta."_".$row->costo_pieza."_".$row->horas_mo."_".$row->costo_mo."_".$row->total_refacion."_".$row->prioridad."_".$row->ref;

                            $datos["pza_garantia"][] = $row->pza_garantia;

                            $datos["costo_ford"][] = $row->costo_ford;
                            $datos["horas_mo_garantias"][] = $row->horas_mo_garantias;
                            $datos["costo_mo_garantias"][] = $row->costo_mo_garantias;

                            $datos["renglonGarantia"][] = $row->costo_ford."_".$row->horas_mo_garantias."_".$row->costo_mo_garantias;

                            $datos["autoriza_garantia"][] = $row->autoriza_garantia;
                            $datos["autoriza_jefeTaller"][] = $row->autoriza_jefeTaller;
                            $datos["autoriza_asesor"][] = $row->autoriza_asesor;
                            $datos["autoriza_cliente"][] = $row->autoriza_cliente;

                            $datos["estado_entrega"][] = $row->estado_entrega;
                            $datos["referencia"][] = $row->ref;

                            //Refacciones (activas) totales del presupuesto
                            $datos["registrosControl"] += 1;
                            //Refacciones (activas) con garantias totales del presupuesto
                            if ($row->pza_garantia == "1") {
                                $datos["registrosGarantias"] += 1;
                            }

                            //Contabilizamos refaccions aprobadas
                            if (($row->autoriza_garantia == "1")||($row->autoriza_cliente =="1")) {
                                $datos["pzs_aprobadas"] += 1;
                            }

                            if ($row->estado_entrega == "3") {
                                $datos["pzs_entregadas"] += 1;
                            }

                            if ($row->estado_entrega == "2") {
                                $datos["pzs_recibidas"] += 1;
                            }

                            //Verificamos si es una refaccion o mano de obra
                            if ((($row->costo_pieza == 0)&&($row->num_pieza == ""))||(($row->costo_ford == 0)&&($row->autoriza_garantia == "1"))) {
                                $datos["tipo_refaccion"][] = "MO";
                            } else {
                                $datos["tipo_refaccion"][] = "REF";
                            }
                        }
                    }
                }elseif (($datos["destinoVista"] == "CLIENTE")||($datos["destinoVista"] == "FINAL")||($datos["destinoVista"] == "PDF")||($datos["destinoVista"] == "EntregaVentanilla")||($datos["destinoVista"] == "MGarantias")) {
                    if ($row->autoriza_jefeTaller == 0) {
                        $datos["sin_aprobar_jdt"] += 1;
                    }
                    //Si aplica (JEFE DE TALLER Y GARANTIA)
                    if (($row->autoriza_asesor == "1")&&($row->autoriza_jefeTaller == "1")) {
                        $datos["id_refaccion"][] = $row->id;

                        $datos["cantidad"][] = $row->cantidad;
                        $datos["descripcion"][] = $row->descripcion;
                        $datos["num_pieza"][] = $row->num_pieza;
                        $datos["existe"][] = $row->existe;
                        $datos["fecha_planta"][] = $row->fecha_planta;
                        $datos["costo_pieza"][] = $row->costo_pieza;
                        $datos["horas_mo"][] = $row->horas_mo;
                        $datos["costo_mo"][] = $row->costo_mo;
                        $datos["total_refacion"][] = $row->total_refacion;
                        $datos["prioridad"][] = $row->prioridad;

                        $datos["rep_adicional"][] = $row->rep_adicional;
                        $datos["costo_aseguradora"][] = $row->costo_aseguradora;

                        $datos["renglon"][] = $row->cantidad."_".$row->descripcion."_".$row->num_pieza."_".$row->existe."*".$row->fecha_planta."_".$row->costo_pieza."_".$row->horas_mo."_".$row->costo_mo."_".$row->total_refacion."_".$row->prioridad."_".$row->ref;

                        $datos["pza_garantia"][] = $row->pza_garantia;

                        $datos["costo_ford"][] = $row->costo_ford;
                        $datos["horas_mo_garantias"][] = $row->horas_mo_garantias;
                        $datos["costo_mo_garantias"][] = $row->costo_mo_garantias;

                        $datos["renglonGarantia"][] = $row->costo_ford."_".$row->horas_mo_garantias."_".$row->costo_mo_garantias;

                        $datos["autoriza_garantia"][] = $row->autoriza_garantia;
                        $datos["autoriza_jefeTaller"][] = $row->autoriza_jefeTaller;
                        $datos["autoriza_asesor"][] = $row->autoriza_asesor;
                        $datos["autoriza_cliente"][] = $row->autoriza_cliente;

                        $datos["estado_entrega"][] = $row->estado_entrega;
                        $datos["referencia"][] = $row->ref;

                        //Refacciones (activas) totales del presupuesto
                        $datos["registrosControl"] += 1;
                        //Refacciones (activas) con garantias totales del presupuesto
                        if ($row->pza_garantia == "1") {
                            $datos["registrosGarantias"] += 1;
                        }

                        //Contabilizamos refaccions aprobadas
                        if (($row->autoriza_garantia == "1")||($row->autoriza_cliente =="1")) {
                            $datos["pzs_aprobadas"]  += 1;
                        }

                        if ($row->estado_entrega == "3") {
                            $datos["pzs_entregadas"] += 1;
                        }

                        if ($row->estado_entrega == "2") {
                            $datos["pzs_recibidas"] += 1;
                        }

                        //Verificamos si es una refaccion o mano de obra
                        if ((($row->costo_pieza == 0)&&($row->num_pieza == ""))||(($row->costo_ford == 0)&&($row->autoriza_garantia == "1"))) {
                            $datos["tipo_refaccion"][] = "MO";
                        } else {
                            $datos["tipo_refaccion"][] = "REF";
                        }

                        $fecha_a = new DateTime($row->fecha_entrega);
                        $datos["fecha_entrega"][] = $fecha_a->format('d-m-Y H:i:s');
                        $datos["firma_ventanilla"][] = $row->firma_ventanilla_entrega;
                    }
                }elseif ($datos["destinoVista"] == "GARANTIAS") {
                    //Si aplica
                    if (($row->pza_garantia == "1")&&($row->autoriza_jefeTaller == "1")) {
                        $datos["id_refaccion"][] = $row->id;

                        $datos["cantidad"][] = $row->cantidad;
                        $datos["descripcion"][] = $row->descripcion;
                        $datos["num_pieza"][] = $row->num_pieza;
                        $datos["existe"][] = $row->existe;
                        $datos["fecha_planta"][] = $row->fecha_planta;
                        $datos["costo_pieza"][] = $row->costo_pieza;
                        $datos["horas_mo"][] = $row->horas_mo;
                        $datos["costo_mo"][] = $row->costo_mo;
                        $datos["total_refacion"][] = $row->total_refacion;
                        $datos["prioridad"][] = $row->prioridad;

                        $datos["rep_adicional"][] = $row->rep_adicional;
                        $datos["costo_aseguradora"][] = $row->costo_aseguradora;

                        $datos["renglon"][] = $row->cantidad."_".$row->descripcion."_".$row->num_pieza."_".$row->existe."*".$row->fecha_planta."_".$row->costo_pieza."_".$row->horas_mo."_".$row->costo_mo."_".$row->total_refacion."_".$row->prioridad."_".$row->ref;

                        $datos["pza_garantia"][] = $row->pza_garantia;

                        $datos["costo_ford"][] = $row->costo_ford;
                        $datos["horas_mo_garantias"][] = $row->horas_mo_garantias;
                        $datos["costo_mo_garantias"][] = $row->costo_mo_garantias;

                        $datos["renglonGarantia"][] = $row->costo_ford."_".$row->horas_mo_garantias."_".$row->costo_mo_garantias;

                        $datos["autoriza_garantia"][] = $row->autoriza_garantia;
                        $datos["autoriza_jefeTaller"][] = $row->autoriza_jefeTaller;
                        $datos["autoriza_asesor"][] = $row->autoriza_asesor;
                        $datos["autoriza_cliente"][] = $row->autoriza_cliente;

                        $datos["estado_entrega"][] = $row->estado_entrega;
                        $datos["referencia"][] = $row->ref;

                        if ($row->autoriza_jefeTaller == 0) {
                            $datos["sin_aprobar_jdt"] += 1;
                        }

                        //Refacciones (activas) totales del presupuesto
                        $datos["registrosControl"] += 1;
                        //Refacciones (activas) con garantias totales del presupuesto
                        if ($row->pza_garantia == "1") {
                            $datos["registrosGarantias"] += 1;
                        }

                        //Contabilizamos refaccions aprobadas
                        if (($row->autoriza_garantia == "1")||($row->autoriza_cliente =="1")) {
                            $datos["pzs_aprobadas"]  += 1;
                        }

                        if ($row->estado_entrega == "3") {
                            $datos["pzs_entregadas"] += 1;
                        }

                        if ($row->estado_entrega == "2") {
                            $datos["pzs_recibidas"] += 1;
                        }

                        //Verificamos si es una refaccion o mano de obra
                        if ((($row->costo_pieza == 0)&&($row->num_pieza == ""))||(($row->costo_ford == 0)&&($row->autoriza_garantia == "1"))) {
                            $datos["tipo_refaccion"][] = "MO";
                        } else {
                            $datos["tipo_refaccion"][] = "REF";
                        }
                    }
                }else{
                    $datos["id_refaccion"][] = $row->id;

                    $datos["cantidad"][] = $row->cantidad;
                    $datos["descripcion"][] = $row->descripcion;
                    
                    $datos["prefijo"][] = $row->prefijo;
                    $datos["basico"][] = $row->basico;
                    $datos["sufijo"][] = $row->sufijo;
                    $datos["num_pieza"][] = $row->num_pieza;

                    $datos["existe"][] = $row->existe;
                    $datos["fecha_planta"][] = $row->fecha_planta;
                    $datos["costo_pieza"][] = $row->costo_pieza;
                    $datos["horas_mo"][] = $row->horas_mo;
                    $datos["costo_mo"][] = $row->costo_mo;
                    $datos["total_refacion"][] = $row->total_refacion;
                    $datos["prioridad"][] = $row->prioridad;

                    $datos["rep_adicional"][] = $row->rep_adicional;
                    $datos["costo_aseguradora"][] = $row->costo_aseguradora;

                    $datos["renglon"][] = $row->cantidad."_".$row->descripcion."_".$row->num_pieza."_".$row->existe."*".$row->fecha_planta."_".$row->costo_pieza."_".$row->horas_mo."_".$row->costo_mo."_".$row->total_refacion."_".$row->prioridad."_".$row->ref;

                    $datos["pza_garantia"][] = $row->pza_garantia;

                    $datos["costo_ford"][] = $row->costo_ford;
                    $datos["horas_mo_garantias"][] = $row->horas_mo_garantias;
                    $datos["costo_mo_garantias"][] = $row->costo_mo_garantias;

                    $datos["renglonGarantia"][] = $row->costo_ford."_".$row->horas_mo_garantias."_".$row->costo_mo_garantias;

                    $datos["autoriza_garantia"][] = $row->autoriza_garantia;
                    $datos["autoriza_jefeTaller"][] = $row->autoriza_jefeTaller;
                    $datos["autoriza_asesor"][] = $row->autoriza_asesor;
                    $datos["autoriza_cliente"][] = $row->autoriza_cliente;

                    $datos["estado_entrega"][] = $row->estado_entrega;
                    $datos["referencia"][] = $row->ref;

                    if ($row->autoriza_jefeTaller == 0) {
                        $datos["sin_aprobar_jdt"] += 1;
                    }

                    $datos["valida_uso"][] = $row->asc_servicio_req;
                    $datos["req_pza_gerente"][] = $row->nombre_gerente_req;
                    $datos["req_pza_firma"][] = $row->firma_gerente_req;
                    $datos["req_pza_parte"][] = $row->parte_remplazada_req;

                    $datos["alta_garantia"][] = $row->alta_garantia;
                    $datos["procesada_alta_garantia"][] = $row->fecha_precesa_altag;
                    
                    $fecha_2 = new DateTime($row->fecha_firma_greq);
                    $datos["req_pza_fecha"][] = $fecha_2->format('d-m-Y');

                    $fecha_a = new DateTime($row->fecha_entrega);
                    $datos["fecha_entrega"][] = $fecha_a->format('d-m-Y H:i:s');
                    $datos["firma_ventanilla"][] = $row->firma_ventanilla_entrega;

                    //Refacciones (activas) totales del presupuesto
                    $datos["registrosControl"] += 1;
                    //Refacciones (activas) con garantias totales del presupuesto
                    if ($row->pza_garantia == "1") {
                        $datos["registrosGarantias"] += 1;
                    }

                    //Contabilizamos refaccions aprobadas
                    if (($row->autoriza_garantia == "1")||($row->autoriza_cliente =="1")) {
                        $datos["pzs_aprobadas"]  += 1;

                        //Verificamos si es una refaccion o mano de obra
                        if ((($row->costo_pieza == 0)&&($row->num_pieza == ""))||(($row->costo_ford == 0)&&($row->autoriza_garantia == "1"))) {
                            $datos["tipo_refaccion"][] = "MO";
                            $datos["pzs_entregada_ref"] += 1;
                        } else {
                            $datos["tipo_refaccion"][] = "REF";
                            if ($row->estado_entrega == "3") {
                                $datos["pzs_entregada_ref"] += 1;
                            }
                        }
                    }

                    if ($row->estado_entrega == "3") {
                        $datos["pzs_entregadas"] += 1;
                    }

                    if ($row->estado_entrega == "2") {
                        $datos["pzs_recibidas"] += 1;
                    }
                }
            }
        }

        $datos["id_cita"] = $id_cita;
        $datos["tipoOrigen"] = "MULTIPUNTO";

        if ($this->session->userdata('rolIniciado')) {
            $datos["sesion_visal"] = 1;
        }else {
            $datos["sesion_visal"] = 0;
        }

        //Identificamos en que vista se mostrara la informacion
        //Vista del presupuesto para el asesor
        if ($datos["destinoVista"] == "ASESOR") {
            $this->loadAllView($datos,'presupuestos/asesor/presupuesto_edita');

        //Vista del presupuesto original
        }elseif ($datos["destinoVista"] == "AltaTecnico") {
            $this->loadAllView($datos,'presupuestos/presupuesto_sin_editar');

        //Vista del presupuesto para ventanilla
        }elseif ($datos["destinoVista"] == "Ventanilla") {
            if ($datos["envia_garantia_mod"] == "1") {
                $this->loadAllView($datos,'presupuestos/ventanilla/presupuesto_edita_2');
            }else{
                $this->loadAllView($datos,'presupuestos/ventanilla/presupuesto_edita');
            }

        //Vista del presupuesto para el jefe de taller
        }elseif ($datos["destinoVista"] == "JDT") {
            $this->loadAllView($datos,'presupuestos/jefeTaller/presupuesto_edita');

        }elseif ($datos["destinoVista"] == "GARANTIAS") {
            $this->loadAllView($datos,'presupuestos/garantias/presupuesto_edita');

        }elseif ($datos["destinoVista"] == "MGarantias") {
            $this->loadAllView($datos,'presupuestos/garantias/presupuesto_informativo');

        }elseif ($datos["destinoVista"] == "EntregaVentanilla") {
            if ($datos["estado_refacciones"] != "0") {
                //$this->loadAllView($datos,'presupuestos/ventanilla/presupuesto_entrega_2');
                $this->loadAllView($datos,'presupuestos/ventanilla/presupuesto_entrega_3');
            } else {
                $this->loadAllView($datos,'presupuestos/ventanilla/presupuesto_entrega');
            }            
            
        }elseif ($datos["destinoVista"] == "CLIENTE") {
            $this->loadAllView($datos,'presupuestos/cliente/presupuesto_afecta');

        }elseif ($datos["destinoVista"] == "TECNICO") {
            $this->loadAllView($datos,'presupuestos/tecnico/presupuesto_edita');

        }elseif ($datos["destinoVista"] == "RequisicionVentanilla") {
            $precarga = $this->mConsultas->precargaConsulta($id_cita);
            foreach ($precarga as $row){
                $datos["tecnico"] = $row->tecnico;
            }

            $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
            foreach ($query as $row){
                //$datos["firma_requisicion"] = (file_exists($row->firma_requisicion_garantias)) ? $row->firma_requisicion_garantias : ""; 
                //$datos["firma_requisicion_garantias"] = (file_exists($row->firma_requisicion_garantias)) ? $row->firma_requisicion_garantias : "";
                $datos["firma_requisicion_url"] = $row->firma_requisicion;
                $datos["firma_requisicion_garantias_url"] = $row->firma_requisicion_garantias;
                $fecha_1 = new DateTime($row->termina_requisicion);
                $datos["termina_requisicion"] = $fecha_1->format('d-m-Y H:i:s');
                $fecha_2 = new DateTime($row->termina_requisicion_garantias);
                $datos["termina_requisicion_garantias"] = $fecha_2->format('d-m-Y H:i:s');

                $datos["garantias_nombre"] = $row->garantias_nombre;
            }

            if ($datos["dirige"] == "Recibido_G") {
                $this->loadAllView($datos,'presupuestos/ventanilla/presupuesto_recibido_garantias');
            }else{
                $this->loadAllView($datos,'presupuestos/ventanilla/presupuesto_requisicion');
            }

        }elseif ($datos["destinoVista"] == "Estatus") {
            $this->loadAllView($datos,'presupuestos/jefeTaller/presupuesto_edita_estatus');

        }elseif ($datos["destinoVista"] == "PDF") {
            //Cargamos datos adicionales
            $precarga = $this->mConsultas->precargaConsulta($id_cita);
            foreach ($precarga as $row){
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $datos["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $datos["modelo"] = $row->vehiculo_anio;
                $datos["placas"] = $row->vehiculo_placas;
                $datos["categoria"] = $row->subcategoria;
                $datos["tecnico"] = $row->tecnico;
                $datos["asesor"] = $row->asesor;
                $datos["cliente"] = $row->nombre_compania;
            }
            $this->generar_pdf($datos,"presupuestos/cliente/plantilla_pdf");

        }else{
            echo "ERROR DE CARGA";
        }
    }

    //Generamos lista para ventanilla
    public function listaCotizacionesVentanilla($origen = "",$datos = NULL)
    {
        if (!isset($datos["indicador"])) {
            $datos["indicador"] = "";
        }

        if (!isset($datos["destino"])) {
            $datos["destino"] = $origen;
        }

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }

        $contador_renglones = 0;
        $datos["id_cita"] = [];

        $mes_actual = date("m");
        $fecha_busqueda = (($mes_actual <= 1) ? (date("Y")-1) : date("Y"))."-".(($mes_actual <= 1) ? "12" : ($mes_actual -1))."-20";

        //Verificamos que informacion se cargara
        $precarga = $this->mConsultas->presupuestoVentanilla($fecha_busqueda); 

        foreach ($precarga as $row) {
            //Verificamos si ya se libero de ventanilla
            if ($row->envia_ventanilla == "0") {
                //Recuperamos los datos
                $datos["id_cita"][] = $row->id_cita;
                $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                $datos["idIntelisis"][] = $row->folio_externo;
                
                //Identificamos el tipo de presupuesto a mostara
                $datos["tipo"][] = (((int)$row->ref_garantias > 0) ? "CON GARANTÍAS" : "TRADICIONAL");
                $datos["ruta"][] = "1";

                //$datos["envio_garantia"][] = $row->envio_garantia;
                $datos["envia_ventanilla"][] = $row->envia_ventanilla;
                $datos["estado_refacciones"][] = $row->estado_refacciones;
                $datos["estado_refacciones_garantias"][] = $row->estado_refacciones_garantias;
                $datos["firma_asesor"][] = (($row->firma_asesor != "") ? "SI" : "NO");
                $datos["envio_jdt"][] = $row->envio_jdt;
                
                $datos["firma_requisicion"][] = $row->firma_requisicion;
                $datos["firma_requisicion_garantias"][] = $row->firma_requisicion_garantias;
                $datos["ubicacion_proceso"][] = $row->ubicacion_proceso;
                $datos["acepta_cliente"][] = $row->acepta_cliente;

                $datos["serie"][] = $row->serie;
                $datos["tecnico"][] = $row->tecnico;
                $datos["asesor"][] = $row->asesor;
                $datos["modelo"][] = $row->vehiculo_modelo;
                $datos["placas"][] = $row->vehiculo_placas;

                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $datos["color"][] = "background-color:#f5acaa;";
                    } elseif ($row->estado_refacciones == "1") {
                        $datos["color"][] = "background-color:#f5ff51;";
                    } elseif ($row->estado_refacciones == "2") {
                        $datos["color"][] = "background-color:#5bc0de;";
                    }else{
                        $datos["color"][] = "background-color:#c7ecc7;";
                    }
                } else {
                    $datos["color"][] = "background-color:red;color:white;";
                }

                //Campo para peticiones psni
                $datos["peticion_psni"][] = $row->solicita_psni;

                $contador_renglones++;
                
            //Si ya se libero de ventanilla, definimos si seran una o dos presupuestos
            }else{
                //Recuperamos los datos
                $datos["id_cita"][] = $row->id_cita;
                $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                $datos["idIntelisis"][] = $row->folio_externo;
                
                //Identificamos el tipo de presupuesto a mostara
                $datos["tipo"][] = "TRADICIONAL";
                $datos["ruta"][] = "1";

                //$datos["envio_garantia"][] = $row->envio_garantia;
                $datos["envia_ventanilla"][] = $row->envia_ventanilla;
                $datos["estado_refacciones"][] = $row->estado_refacciones;
                //$datos["estado_refacciones_garantias"][] = $row->estado_refacciones_garantias;
                $datos["firma_asesor"][] = (($row->firma_asesor != "") ? "SI" : "NO");
                //$datos["envio_jdt"][] = $row->envio_jdt;
                
                $datos["firma_requisicion"][] = $row->firma_requisicion;
                $datos["firma_requisicion_garantias"][] = $row->firma_requisicion_garantias;
                //$datos["ubicacion_proceso"][] = $row->ubicacion_proceso;
                $datos["acepta_cliente"][] = $row->acepta_cliente;

                $datos["serie"][] = $row->serie;
                $datos["tecnico"][] = $row->tecnico;
                $datos["asesor"][] = $row->asesor;
                $datos["modelo"][] = $row->vehiculo_modelo;
                $datos["placas"][] = $row->vehiculo_placas;

                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $datos["color"][] = "background-color:#f5acaa;";
                    } elseif ($row->estado_refacciones == "1") {
                        $datos["color"][] = "background-color:#f5ff51;";
                    } elseif ($row->estado_refacciones == "2") {
                        $datos["color"][] = "background-color:#5bc0de;";
                    }else{
                        $datos["color"][] = "background-color:#c7ecc7;";
                    }
                } else {
                    $datos["color"][] = "background-color:red;color:white;";
                }

                //Campo para peticiones psni
                $datos["peticion_psni"][] = $row->solicita_psni;

                $contador_renglones++;
            }
        }

        //Identificamos en que vista se mostrara
        if ($datos["destino"] == "Ventanilla") {
            $this->loadAllView($datos,'presupuestos/ventanilla/buscador_ventanilla');
        }
    }

    //Generamos lista para jefe de taller
    public function listaCotizaciones($origen = "",$datos = NULL)
    {
        if (!isset($datos["indicador"])) {
            $datos["indicador"] = "";
        }

        if (!isset($datos["destino"])) {
            $datos["destino"] = $origen;
        }

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }

        $datos["id_cita"] = [];
        $mes_actual = date("m");
        $fecha_busqueda = (($mes_actual <= 1) ? (date("Y")-1) : date("Y"))."-".(($mes_actual <= 1) ? "12" : ($mes_actual -1))."-20";

        //Verificamos que registros se mostraran
        //Cargamos los registros para el jefe de taller
        if ($datos["destino"] == "JDT") {
            $precarga = $this->mConsultas->presupuestosJDT($fecha_busqueda); 

        //Cargamos los registros para el asesor
        }elseif ($datos["destino"] == "ASESOR") {
            //Si existe el rol de jefe de taller (en el presupuesto)
            $precarga = $this->mConsultas->presupuestosAsesor($fecha_busqueda);
        //Cargamos los registros para garantias
        }elseif ($datos["destino"] == "GARANTIAS") {
            $precarga = $this->mConsultas->presupuestosGarantias();

        //Cargamos los registros para tecnicos
        }elseif ($datos["destino"] == "TECNICO") {
            //Validamos si se cargaran los presupuestos de un tecnico enconcreto
            //Si es un tecnico en especifico
            if ($this->session->userdata('rolIniciado')) {
                if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {
                    $idTecnico = $this->session->userdata('idUsuario'); 
                    $precarga = $this->mConsultas->presupuestosCreados_Tec($idTecnico,$fecha_busqueda);

                }else{
                    $precarga = $this->mConsultas->presupuestosCreados($fecha_busqueda);
                }
            }else{
                $precarga = $this->mConsultas->presupuestosCreados($fecha_busqueda);
            }            
            
        //Cargamos los registros que ya fueron afectados por el cliente (jefe de taller)
        }elseif ($datos["destino"] == "ESTATUS") {
            $precarga = $this->mConsultas->presupuestosAfectados();
        }

        $contador_renglones = 0;
        
        foreach ($precarga as $row) {
            //Si es la lista del asesor, realizamos un filtro extra
            if ($datos["destino"] == "ASESOR") {
                //Si el presupuesto tiene garantia, , que haya sido liberado de garantia
                if (((int)$row->ref_garantias > 0) && ( $row->envio_garantia == "1")) {
                    //Recuperamos los datos
                    $datos["id_cita"][] = $row->id_cita;
                    $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                    $datos["idIntelisis"][] = $row->folio_externo;
                    
                    //Identificamos el tipo de presupuesto a mostara
                    $datos["tipo"][] = (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL");

                    $datos["envio_garantia"][] = $row->envio_garantia;
                    $datos["envia_ventanilla"][] = $row->envia_ventanilla;
                    $datos["estado_refacciones"][] = $row->estado_refacciones;
                    $datos["estado_refacciones_garantias"][] = $row->estado_refacciones_garantias;
                    $datos["firma_asesor"][] = (($row->firma_asesor != "") ? "SI" : "NO");
                    $datos["envio_jdt"][] = $row->envio_jdt;
                    
                    $datos["firma_requisicion"][] = $row->firma_requisicion;
                    $datos["firma_requisicion_garantias"][] = $row->firma_requisicion_garantias;
                    $datos["ubicacion_proceso"][] = $row->ubicacion_proceso;
                    $datos["acepta_cliente"][] = $row->acepta_cliente;

                    $datos["serie"][] = $row->serie;
                    $datos["tecnico"][] = $row->tecnico;
                    $datos["asesor"][] = $row->asesor;
                    $datos["modelo"][] = $row->vehiculo_modelo;
                    $datos["placas"][] = $row->vehiculo_placas;

                    if ($row->acepta_cliente != "No") {
                        if ($row->estado_refacciones == "0") {
                            $datos["color"][] = "background-color:#f5acaa;";
                        } elseif ($row->estado_refacciones == "1") {
                            $datos["color"][] = "background-color:#f5ff51;";
                        } elseif ($row->estado_refacciones == "2") {
                            $datos["color"][] = "background-color:#5bc0de;";
                        }else{
                            $datos["color"][] = "background-color:#c7ecc7;";
                        }
                    } else {
                        $datos["color"][] = "background-color:red;color:white;";
                    }

                    //Campo para peticiones psni
                    $datos["peticion_psni"][] = $row->solicita_psni;

                    $contador_renglones++;
                //Si no tiene garantias, se imprime
                } else if ($row->ref_garantias == "0") {
                    //Recuperamos los datos
                    $datos["id_cita"][] = $row->id_cita;
                    $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                    $datos["idIntelisis"][] = $row->folio_externo;
                    
                    //Identificamos el tipo de presupuesto a mostara
                    $datos["tipo"][] = (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL");

                    $datos["envio_garantia"][] = $row->envio_garantia;
                    $datos["envia_ventanilla"][] = $row->envia_ventanilla;
                    $datos["estado_refacciones"][] = $row->estado_refacciones;
                    $datos["estado_refacciones_garantias"][] = $row->estado_refacciones_garantias;
                    $datos["firma_asesor"][] = (($row->firma_asesor != "") ? "SI" : "NO");
                    $datos["envio_jdt"][] = $row->envio_jdt;
                    
                    $datos["firma_requisicion"][] = $row->firma_requisicion;
                    $datos["firma_requisicion_garantias"][] = $row->firma_requisicion_garantias;
                    $datos["ubicacion_proceso"][] = $row->ubicacion_proceso;
                    $datos["acepta_cliente"][] = $row->acepta_cliente;

                    $datos["serie"][] = $row->serie;
                    $datos["tecnico"][] = $row->tecnico;
                    $datos["asesor"][] = $row->asesor;
                    $datos["modelo"][] = $row->vehiculo_modelo;
                    $datos["placas"][] = $row->vehiculo_placas;

                    if ($row->acepta_cliente != "No") {
                        if ($row->estado_refacciones == "0") {
                            $datos["color"][] = "background-color:#f5acaa;";
                        } elseif ($row->estado_refacciones == "1") {
                            $datos["color"][] = "background-color:#f5ff51;";
                        } elseif ($row->estado_refacciones == "2") {
                            $datos["color"][] = "background-color:#5bc0de;";
                        }else{
                            $datos["color"][] = "background-color:#c7ecc7;";
                        }
                    } else {
                        $datos["color"][] = "background-color:red;color:white;";
                    }

                    //Campo para peticiones psni
                    $datos["peticion_psni"][] = $row->solicita_psni;

                    $contador_renglones++;
                }
            //De lo contrario, bajamos todo lo recuperado
            } else {
                //Recuperamos los datos
                $datos["id_cita"][] = $row->id_cita;
                $datos["id_cita_url"][] = $this->encrypt($row->id_cita);
                $datos["idIntelisis"][] = $row->folio_externo;
                
                //Identificamos el tipo de presupuesto a mostara
                $datos["tipo"][] = (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL");

                $datos["envio_garantia"][] = $row->envio_garantia;
                $datos["envia_ventanilla"][] = $row->envia_ventanilla;
                $datos["estado_refacciones"][] = $row->estado_refacciones;
                $datos["estado_refacciones_garantias"][] = $row->estado_refacciones_garantias;
                $datos["firma_asesor"][] = (($row->firma_asesor != "") ? "SI" : "NO");
                $datos["envio_jdt"][] = $row->envio_jdt;
                
                $datos["firma_requisicion"][] = $row->firma_requisicion;
                $datos["firma_requisicion_garantias"][] = $row->firma_requisicion_garantias;
                $datos["ubicacion_proceso"][] = $row->ubicacion_proceso;
                $datos["acepta_cliente"][] = $row->acepta_cliente;

                $datos["serie"][] = $row->serie;
                $datos["tecnico"][] = $row->tecnico;
                $datos["asesor"][] = $row->asesor;
                $datos["modelo"][] = $row->vehiculo_modelo;
                $datos["placas"][] = $row->vehiculo_placas;

                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $datos["color"][] = "background-color:#f5acaa;";
                    } elseif ($row->estado_refacciones == "1") {
                        $datos["color"][] = "background-color:#f5ff51;";
                    } elseif ($row->estado_refacciones == "2") {
                        $datos["color"][] = "background-color:#5bc0de;";
                    }else{
                        $datos["color"][] = "background-color:#c7ecc7;";
                    }
                } else {
                    $datos["color"][] = "background-color:red;color:white;";
                }

                //Campo para peticiones psni
                $datos["peticion_psni"][] = $row->solicita_psni;

                $contador_renglones++;
            }   
        }

        //Verificamos en que lista se cargaran los datos
        //Cargamos los registros para el jefe de taller
        if ($datos["destino"] == "JDT") {
            $this->loadAllView($datos,'presupuestos/jefeTaller/buscador_jefeTaller');
        //Cargamos los registros para el Asesor
        }elseif ($datos["destino"] == "ASESOR") {
            $this->loadAllView($datos,'presupuestos/asesor/buscador_asesor');
        //Cargamos los registros para garantias
        }elseif ($datos["destino"] == "GARANTIAS") {
            $this->loadAllView($datos,'presupuestos/garantias/buscador_garantias');
        //Cargamos los registros para tecnicos
        }elseif ($datos["destino"] == "TECNICO") {
            $this->loadAllView($datos,'presupuestos/tecnico/buscador_tecnico');
        //Cargamos los registros que ya fueron afectados por el cliente (jefe de taller)
        }elseif ($datos["destino"] == "ESTATUS") {
            $this->loadAllView($datos,'presupuestos/jefeTaller/buscador_estatus');
        }
    }

    //Cargamos los datos para valorar la requisicion
    public function valoresRequisicion($id_cita = 0,$datos = NULL)
    {
        $datos["id_cita"] = $id_cita;

        //Recuperamos la informacion general del presupuesto
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($query as $row){
            $datos["id"] = $row->id;
            $datos["firma_requisicion"] = (file_exists($row->firma_requisicion)) ? $row->firma_requisicion : ""; 
            $datos["firma_requisicion_garantias"] = (file_exists($row->firma_requisicion_garantias)) ? $row->firma_requisicion_garantias : "";
            
            $datos["firma_requisicion_url"] = $row->firma_requisicion;
            $datos["firma_requisicion_garantias_url"] = $row->firma_requisicion_garantias;

            $fecha_1 = new DateTime($row->termina_requisicion);
            $datos["termina_requisicion"] = $fecha_1->format('d-m-Y H:i:s');
            $fecha_2 = new DateTime($row->termina_requisicion_garantias);
            $datos["termina_requisicion_garantias"] = $fecha_2->format('d-m-Y H:i:s');

            if ($row->tecnico_nombre != NULL) {
                $datos["tecnico_nombre"] = $row->tecnico_nombre;
            }
            $datos["garantias_nombre"] = $row->garantias_nombre;

            $datos["fecha_alta"] = $row->fecha_alta;
        }

        $datos["requisicion"] = $this->requisicion($datos["id"]);
        $datos["vendedor"] = "M9";

        //Recuperamos datos del cliente
        $query = $this->mConsultas->api_orden($datos["id_cita"]);
        foreach ($query as $row) {
            $datos["ordenT"] = $row->identificador."-".$row->id_Cita_orden;
            $datos["tecnico"] = $row->tecnico;
            $datos["nombreCliente"] = ((strlen($row->nombre_compania) > 2) ? $row->nombre_compania : $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno);

            $datos["orden_foliointelsis"] = $row->folioIntelisis;
            //$datos["orden_domicilio"] = strtoupper($row->calle." ".$row->noexterior." ".$row->nointerior.", ".$row->colonia);
            //$datos["orden_municipio"] = strtoupper($row->municipio);
            //$datos["orden_cp"] = strtoupper($row->cp);
            //$datos["orden_estado"] = strtoupper($row->estado);
            //$datos["orden_telefono_movil"] = $row->telefono_movil;
            //$datos["orden_otro_telefono"] = $row->otro_telefono;

            $datos["orden_vehiculo_placas"] = $row->vehiculo_placas;
            $datos["orden_vehiculo_identificacion"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
            $datos["orden_vehiculo_marca"] =  $row->subcategoria; 
            //$datos["destino"] =  $row->almacen; 
        }

        //Fecha de la pieza
        $datosFecha = explode(" ",$datos["fecha_alta"]);
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');

        if (count($datosFecha)>1) {
            $fechats = strtotime($datosFecha[0]); //pasamos a timestamp

            $fecha = explode("-",$datosFecha[0]);
            $datos["dia"] = $fecha[2];
            $datos["anio"] = $fecha[0];

            $mesPrv = (int)$fecha[1];
            $datos["mes"] = $meses[$mesPrv-1];
            $datos["diaSemeana"] = $dias[date('w', $fechats)];

            $hora1 = explode(":",$datosFecha[1]);
            $datos["hora"] = $hora1[0].":".$hora1[1];
            if ((int)$hora1[0] < 13) {
              $datos["hora"] .= " AM";
            }else {
              $datos["hora"] .= " PM";
            }
        //Fecha de hoy por soporte
        }else {
            $fechats = strtotime(date("Y-m-d")); //pasamos a timestamp

            $datos["dia"] = date("d");
            $datos["anio"] = date("Y");
            $datos["mes"] = $meses[date("n")-1];
            $datos["hora"] = date("H:i");
            $datos["diaSemeana"] = $dias[date('w', $fechats)];
        }

        //Limpiamos los registros que se imprimiran
        //Recuperamos las refacciones 
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_multipunto");

        $datos["registrosControl"] = 0;
        $datos["registrosGarantias"] = 0;

        $contador = 1;

        $datos["subtotal"] = 0;
        $datos["iva"] = 0;
        $datos["cta_total"] = 0;

        foreach ($query as $row){
            //Cargamos refacciones tradicionales
            if (($datos["dirige"] == "Requisicion_T_PDF")||($datos["dirige"] == "Requisicion_Descarga")) {
                if (($row->pza_garantia == "0")&&($row->autoriza_cliente == "1")&&($row->activo == "1")) {
                    $costo_pz = (float)(($row->pza_garantia == "1") ? $row->costo_ford : $row->costo_pieza);

                    if (($contador > 10)&&($contador < 100)) {
                        $const = "0".$contador;
                        $datos["renglon"][] = $contador;
                    }elseif ($contador < 10) {
                        $const = "00".$contador;
                        $datos["renglon"][] = "0".$contador;
                    }else {
                        $datos["renglon"][] = $const;
                    }

                    $datos["cantidad"][] = $row->cantidad;
                    $datos["descripcion"][] = $row->descripcion;
                    $datos["clave"][] = $row->num_pieza;
                    $datos["precio_lista"][] = $costo_pz;
                    $datos["precio"][] = $costo_pz;
                    $datos["importe"][] = $costo_pz * (float)$row->cantidad;
                    $datos["total"][] = ($costo_pz * (float)$row->cantidad) * 1.16;

                    $datos["subtotal"] += $costo_pz * (float)$row->cantidad;
                    $datos["iva"] = $datos["subtotal"] * 0.16;
                    $datos["cta_total"] += ($costo_pz * (float)$row->cantidad) * 1.16;

                    $datos["leyenda"] = "PÚBLICO";

                    $datos["estado_entrega"][] = $row->estado_entrega;
                    $fecha_a = new DateTime($row->fecha_entrega);
                    $datos["fecha_entrega"][] = $fecha_a->format('d-m-Y H:i:s');
                    $datos["firma_ventanilla"][] = $row->firma_ventanilla_entrega;

                    $datos["valida_uso"][] = $row->asc_servicio_req;
                    $datos["req_pza_gerente"][] = $row->nombre_gerente_req;
                    $datos["req_pza_firma"][] = $row->firma_gerente_req;
                    $datos["req_pza_parte"][] = $row->parte_remplazada_req;
                    
                    $fecha_2 = new DateTime($row->fecha_firma_greq);
                    $datos["req_pza_fecha"][] = $fecha_2->format('d-m-Y');

                    $contador++;
                } elseif (($row->pza_garantia == "1")&&($row->autoriza_garantia == "1")&&($row->activo == "1")) {
                    $costo_pz = (float)(($row->pza_garantia == "1") ? $row->costo_ford : $row->costo_pieza);

                    if (($contador > 10)&&($contador < 100)) {
                        $const = "0".$contador;
                        $datos["renglon"][] = $contador;
                    }elseif ($contador < 10) {
                        $const = "00".$contador;
                        $datos["renglon"][] = "0".$contador;
                    }else {
                        $datos["renglon"][] = $const;
                    }

                    $datos["cantidad"][] = $row->cantidad;
                    $datos["descripcion"][] = $row->descripcion;
                    $datos["clave"][] = $row->num_pieza;
                    
                    $datos["precio_lista"][] = $costo_pz;
                    $datos["precio"][] = $costo_pz;
                    
                    $datos["importe"][] = $costo_pz * (float)$row->cantidad;
                    $datos["total"][] = ($costo_pz * (float)$row->cantidad) * 1.16;

                    $datos["subtotal"] += $costo_pz * (float)$row->cantidad;
                    $datos["iva"] = $datos["subtotal"] * 0.16;
                    $datos["cta_total"] += ($costo_pz * (float)$row->cantidad) * 1.16;

                    $datos["estado_entrega"][] = $row->estado_entrega;
                    $fecha_a = new DateTime($row->fecha_entrega);
                    $datos["fecha_entrega"][] = $fecha_a->format('d-m-Y H:i:s');
                    $datos["firma_ventanilla"][] = $row->firma_ventanilla_entrega;

                    $datos["valida_uso"][] = $row->asc_servicio_req;
                    $datos["req_pza_gerente"][] = $row->nombre_gerente_req;
                    $datos["req_pza_firma"][] = $row->firma_gerente_req;
                    $datos["req_pza_parte"][] = $row->parte_remplazada_req;
                    
                    $fecha_2 = new DateTime($row->fecha_firma_greq);
                    $datos["req_pza_fecha"][] = $fecha_2->format('d-m-Y');

                    $contador++;

                    $datos["leyenda"] = "GARANTÍA";
                }

            //Cargamos refacciones con garantias
            } else {
                if (($row->pza_garantia == "1")&&($row->autoriza_garantia == "1")&&($row->activo == "1")) {
                    $costo_pz = (float)(($row->pza_garantia == "1") ? $row->costo_ford : $row->costo_pieza);
                    
                    if (($contador > 10)&&($contador < 100)) {
                        $const = "0".$contador;
                        $datos["renglon"][] = $contador;
                    }elseif ($contador < 10) {
                        $const = "00".$contador;
                        $datos["renglon"][] = "0".$contador;
                    }else {
                        $datos["renglon"][] = $const;
                    }

                    $datos["cantidad"][] = $row->cantidad;
                    $datos["descripcion"][] = $row->descripcion;
                    $datos["clave"][] = $row->num_pieza;
                    $datos["precio_lista"][] = $costo_pz;
                    $datos["precio"][] = $costo_pz;
                    
                    $datos["importe"][] = $costo_pz * (float)$row->cantidad;
                    $datos["total"][] = ($costo_pz * (float)$row->cantidad) * 1.16;

                    $datos["subtotal"] += $costo_pz * (float)$row->cantidad;
                    $datos["iva"] = $datos["subtotal"] * 0.16;
                    $datos["cta_total"] += ($costo_pz * (float)$row->cantidad) * 1.16;

                    $datos["estado_entrega"][] = $row->estado_entrega;
                    $fecha_a = new DateTime($row->fecha_entrega);
                    $datos["fecha_entrega"][] = $fecha_a->format('d-m-Y H:i:s');
                    $datos["firma_ventanilla"][] = $row->firma_ventanilla_entrega;

                    $datos["valida_uso"][] = $row->asc_servicio_req;
                    $datos["req_pza_gerente"][] = $row->nombre_gerente_req;
                    $datos["req_pza_firma"][] = $row->firma_gerente_req;
                    $datos["req_pza_parte"][] = $row->parte_remplazada_req;
                    
                    $fecha_2 = new DateTime($row->fecha_firma_greq);
                    $datos["req_pza_fecha"][] = $fecha_2->format('d-m-Y');

                    $contador++;
                }
            }
        }

        //Cargamos refacciones tradicionales
        if ($datos["dirige"] == "Requisicion_T_PDF") {
            $datos["leyenda"] = "PÚBLICO";
        } else {
            $datos["leyenda"] = "GARANTÍAS";
        }

        if ($datos["dirige"] == "Requisicion_Descarga") {
            $datos["nombre_archivo"] = 'Requisicion-'.$datos["ordenT"]."_".date("Ymd-His").'.pdf';

            if (isset($datos["cantidad"])) {
                //Guardamos el registro
                $contenido_temp = array(
                    "id" => NULL,
                    "tipo_archivo" => 1,
                    "id_cita" => $datos["id_cita"],
                    "url_archivo" => 'assets/pdf_requisicion1/'.$datos["nombre_archivo"],
                    "fecha_creacion" => date("Y-m-d H:i:s"),
                );

                $check = $this->mConsultas->save_register('historial_requisicion',$contenido_temp);
            }

            $this->generar_pdf($datos,"presupuestos/ventanilla/requisicion_pdf_3");

        }elseif ($datos["dirige"] == "Recibe_GDescargas") {
            $datos["nombre_archivo"] = 'Recepcion_piezas-'.$datos["ordenT"]."_".date("Ymd-His").'.pdf';

            if (isset($datos["cantidad"])) {
                //Guardamos el registro
                $contenido_temp = array(
                    "id" => NULL,
                    "tipo_archivo" => 2,
                    "id_cita" => $datos["id_cita"],
                    "url_archivo" => 'assets/pdf_requisicion1/'.$datos["nombre_archivo"],
                    "fecha_creacion" => date("Y-m-d H:i:s"),
                );

                $check = $this->mConsultas->save_register('historial_requisicion',$contenido_temp);
            }

            $this->generar_pdf($datos,"presupuestos/ventanilla/recepcion_pdf_2");
        }elseif ($datos["dirige"] == "Recibe_Garantias_PDF") {
            $this->generar_pdf($datos,"presupuestos/ventanilla/recepcion_pdf_1");
        }else{
            $this->generar_pdf($datos,"presupuestos/ventanilla/requisicion_pdf_2"); 
        }
    }

    //Generamos folio para la requisicion
    public function requisicion($indice = 0)
    {
        $retorno = "T000000";

        if ($indice < 10) {
            $retorno = "T00000".$indice;
        }elseif (($indice > 9)&&($indice < 100)) {
            $retorno = "T0000".$indice;
        }elseif (($indice > 9)&&($indice < 100)) {
            $retorno = "T000".$indice;
        }elseif (($indice > 99)&&($indice < 1000)) {
            $retorno = "T00".$indice;
        }elseif (($indice > 999)&&($indice < 10000)) {
            $retorno = "T0".$indice;
        }else{
            $retorno = "T".$indice;
        }

        return $retorno;
    }

    //Envio de notificaciones
    public function sms_general($celular_sms='',$mensaje_sms=''){
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    //Registro de eventos 
    public function registroEvento($id_cita = 0, $datos = NULL)
    {
        //Recuperamos los datos del usuario
        if ($this->session->userdata('rolIniciado')) {
            $envia_id_usuario = $this->session->userdata('idUsuario');
            $envia_nombre_usuario = $this->session->userdata('nombreUsuario');

            if ($this->session->userdata('rolIniciado') == "TEC") {
                $revision = $this->mConsultas->get_result("id",$envia_id_usuario,"tecnicos");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->nombre);
                }
            } else {
                $revision = $this->Verificacion->get_result("adminId",$envia_id_usuario,"admin");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->adminNombre);
                }
            }
        }else{
            $envia_id_usuario = "0";//$datos["idUsuario"];
            $envia_nombre_usuario = "Usuario";//$datos["nombre"];
        }

        //$envia_departamento = $datos["dpo"];
        $sucursal = BIN_SUCURSAL;
        $envia_fecha_hora = date("Y-m-d H:i:s");
        //$recibe_departamento = $datos["dpoRecibe"];
        $id_cita = $id_cita;
        //$descripcion = $datos["descripcion"];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/gerencia/index.php/tiempos/insertar_tiempo");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "envia_id_usuario=".$envia_id_usuario."&envia_nombre_usuario=".$envia_nombre_usuario."&envia_departamento=".$datos["envia_departamento"]."&sucursal=".$sucursal."&envia_fecha_hora=".$envia_fecha_hora."&recibe_departamento=".$datos["recibe_departamento"]."&id_cita=".$id_cita."&descripcion=".$datos["descripcion"]."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);
    }

    //Peticiones por ajax para procesos
    //Actualizar es estado de entrega de las refacciones
    public function cambioEstatusRefacciones()
    {
        //Verificamos que las variables hayan llegado
        if (count($_POST)>0) {
            //Recuperamos los valores
            $id_cita = $_POST["id_cita"];
            $estatus = $_POST["estatus"];
            $presupuesto = $_POST["presupuesto"];

            $fecha_registro = date("Y-m-d H:i:s");

            //Identificamos el campo que se actualizara
            //Presupuesto tradicional
            if ($presupuesto == "T") {
                //Armamos los datos que se actualizaran
                $refaccion["estado_refacciones"] = $estatus;

                if ($estatus == "1") {
                    $refaccion["ubicacion_proceso"] = "Refacciones solicitadas en ventanilla";
                    $refaccion["solicita_pieza"] = $fecha_registro;
                    $movimiento = "Refacciones solicitadas en ventanilla";
                }elseif ($estatus == "2") {
                    $refaccion["ubicacion_proceso"] = "Refacciones recibidas en ventanilla";
                    $refaccion["recibe_pieza"] = $fecha_registro;
                    $movimiento = "Refacciones recibidas en ventanilla";
                }else{
                    $refaccion["ubicacion_proceso"] = "Refacciones entregadas al técnico";
                    $refaccion["entrega_pieza"] = $fecha_registro;
                    $movimiento = "Refacciones entregadas al técnico";
                }            
            //Presupuesto de garantías
            }else{
                $refaccion["estado_refacciones_garantias"] = $estatus;

                if ($estatus == "1") {
                    $refaccion["ubicacion_proceso_garantia"] = "Refacciones solicitadas en ventanilla";
                    $refaccion["solicita_pieza_garantia"] = $fecha_registro;
                    $movimiento = "Refacciones solicitadas en ventanilla (garantías)";
                }elseif ($estatus == "2") {
                    $refaccion["ubicacion_proceso_garantia"] = "Refacciones recibidas en ventanilla";
                    $refaccion["recibe_pieza_garantia"] = $fecha_registro;
                    $movimiento = "Refacciones recibidas en ventanilla (garantías)";
                }else{
                    $refaccion["ubicacion_proceso_garantia"] = "Refacciones entregadas al técnico";
                    $refaccion["entrega_pieza_garantia"] = $fecha_registro;
                    $movimiento = "Refacciones entregadas al técnico (garantías)";
                }
            }

            $refaccion["fecha_actualiza"] = $fecha_registro;

            //Actualizamos el registro
            $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$refaccion,"id_cita",$id_cita,"identificador","M");
            if ($actualiza_reg) {
                $this->registrar_movimiento($id_cita,$movimiento,"Ventanilla");

                //Actualizamos la refacciones individuales
                $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_multipunto");
                foreach ($query as $row){
                    //Si el estado actual es menor al que se modifica, entonces se actualiza
                    if ((int)$row->estado_entrega <= (int)$estatus ) {
                        $renglon["estado_entrega"] = $estatus;
                    
                        //Actualizamos el registro
                        $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$renglon,'id',$row->id);
                        if ($actualizar) {
                            //Si se actualizo correctamente
                            $bandera = TRUE;
                        }
                    }
                }

                //Comprobamos si son refacciones entregadas, enviamos la notificacion
                if ($estatus == "3") {
                    //Recuperamos datos extras para la notificación
                    $query = $this->mConsultas->notificacionTecnicoCita($id_cita); 
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $placas = $row->vehiculo_placas;
                        $asesor = $row->asesor;
                    }

                    if (isset($tecnico)) {
                        
                        //Identificamos los destinatarios y mensaje de la notificacion
                        if ($presupuesto == "T") {
                            $texto = SUCURSAL.". Refacciones (sin garantías) autorizadas del presupuesto: ".$id_cita." fue entregado al técnico el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                            $descripcion = 'Presupuesto multipunto (tradicional): refacciones entregadas al técnico ';

                        }else{
                            $texto = SUCURSAL.". Refacciones (con garantías) autorizadas del presupuesto: ".$id_cita." fue entregado al técnico el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                            $descripcion = 'Presupuesto multipunto (con garantías): refacciones entregadas al técnico ';
                            
                            //Garantias
                            //$this->sms_general('',$texto);
                        }
                        
                        //Enviar mensaje al asesor (si no hay rol de jefe de taller y tampoco garantias)
                        $celular_sms = $this->mConsultas->telefono_asesor($id_cita);
                        $this->sms_general($celular_sms,$texto);
                        
                        //Enviamos mensaje al tecnico
                        //$celular_sms_2 = $this->mConsultas->telefono_tecnico($id_cita);
                        //$this->sms_general($celular_sms_2,$texto);

                        //Tester
                        //$this->sms_general('3328351116',$texto);
                        //Angel evidencia
                        //$this->sms_general('5535527547',$texto);
                        
                        //Grabamos el evento para el panel de gerencia
                        $registro_evento = array(
                            'envia_departamento' => 'Ventanilla',
                            'recibe_departamento' => 'Asesores',
                            'descripcion' => $descripcion,
                        );

                        $this->registroEvento($id_cita,$registro_evento);

                        $resultado = "OK";
                    }else{
                        $resultado = "No se envio la notificación";
                    }
                
                //De lo contrario solo terminamos enviando el resultado
                }else{
                    $resultado = "OK";
                }               
            }else{
                $resultado = "Error al actualizar el registro";
            }
        }else{
            $resultado = "Campo incompletos";
        }

        echo $resultado;
    }

    //Actualizar presupuesto afectados por el cliente
    public function presupuestoAfectado()
    {
        //Verificamos que las variables hayan llegado
        if (count($_POST)>0) {
            //Recuperamos los valores
            $renglones = explode("=",$_POST["renglones"]);
            $cancelado = (float)$_POST["cancelado"];
            $subtotal = (float)$_POST["subtotal"];
            $desicion = $_POST["desicion"];
            $id_cita = $_POST["id_cita"];

            $bandera = FALSE;
            $fecha_registro = date("Y-m-d H:i:s");

            //Recuperamos la informacion para los renglones
            for ($i=0; $i < count($renglones); $i++) { 
                $registro = explode("-",$renglones[$i]);
                if (count($registro)>1) {
                    $refaccion["autoriza_cliente"] = $registro[1];
                    //$id_refaccion[] = $registro[0];
                    
                    //Actualizamos el registro
                    $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$refaccion,'id',$registro[0]);
                    if ($actualizar) {
                        //Si se actualizo correctamente
                        $bandera = TRUE;
                   }
                }
            }

            //Verificamos que la actualizacion se haya completado con exito
            if ($bandera) {
                $nvo_subtotal = $subtotal - $cancelado;
                $iva = (($nvo_subtotal > 0) ? ($nvo_subtotal * 0.16) : 0);
                $total = (($nvo_subtotal > 0) ? ($nvo_subtotal * 1.16) : 0);
                $presupuesto_gral = array(
                    //"subtotal" => $subtotal,
                    "cancelado" => $cancelado,
                    "iva" => $iva,
                    "total" => $total,
                    "acepta_cliente" => $desicion,
                    "ubicacion_proceso" => "Afectada por el cliente",
                    "termina_cliente" => $fecha_registro,
                    "fecha_actualiza" => $fecha_registro,
                );

                //Guardamos el registro de la refaccion
                $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$presupuesto_gral,"id_cita",$id_cita,"identificador","M");
                if ($actualiza_reg) {
                    $this->actualiza_documentacion_cliente($id_cita,$desicion);

                    //Recuperamos datos extras para la notificación
                    $query = $this->mConsultas->notificacionTecnicoCita($id_cita); 
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $placas = $row->vehiculo_placas;
                        $asesor = $row->asesor;
                    }

                    if (isset($tecnico)) {
                        //Interpretamos la decision del cliente
                        if ($desicion == "Si") {
                            $desicion_txt = "'Autorizado Totalmente'";
                        }elseif ($desicion == "No") {
                            $desicion_txt = "'Rechazado Totalmente'";
                        }else{
                            $desicion_txt = "'Autorizado Parcialmente'";
                        }

                        //Identificamos si lo afecta el cliente o el asesor
                        if ($this->session->userdata('rolIniciado')) {
                            $quien = "Asesor";
                        }else{
                            $quien = "Cliente";
                        }

                        $movimiento = "Presupuesto afectado por el cliente. [".$desicion_txt."]. Por: ".$quien;

                        $texto = SUCURSAL.". Presupuesto de la Orden: ".$id_cita." fue ".$desicion_txt." por el ".$quien." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        //Jefe de taller
                        //Jesus Gonzales 
                        //$this->sms_general('3316724093',$texto);
                        //Juan Chavez
                        $this->sms_general('3333017952',$texto);
                        //Gerente de servicio
                        //$this->sms_general('3315273100',$texto);
                        
                        //Ventanilla
                        $this->sms_general('3316721106',$texto);
                        $this->sms_general('3319701092',$texto);
                        $this->sms_general('3317978025',$texto);
                        
                        //Enviar mensaje al asesor (si no hay rol de jefe de taller)
                        $celular_sms = $this->mConsultas->telefono_asesor($id_cita);
                        $this->sms_general($celular_sms,$texto);
                        
                        //Enviamos mensaje al tecnico
                        $celular_sms_2 = $this->mConsultas->telefono_tecnico($id_cita);
                        $this->sms_general($celular_sms_2,$texto);
                        
                        //Tester
                        //$this->sms_general('3328351116',$texto);
                        //Angel evidencia
                        //$this->sms_general('5535527547',$texto);
                    }

                    //Grabamos el evento para el panel de gerencia
                    $registro_evento = array(
                        'envia_departamento' => 'Cliente',
                        'recibe_departamento' => 'Asesores',
                        'descripcion' => 'Presupuesto afectado: '.$desicion_txt,
                    );

                    $this->registroEvento($id_cita,$registro_evento);
                    $this->registrar_movimiento($id_cita,$movimiento,NULL);

                    //Si el presupuesto es aprobado, revisamos si las piezas estan en existencia
                    if (($desicion == "Si")||($desicion == "Val")) {
                        $texto_2 = "Se crea registro psni";
                        $this->sms_general('3328351116',$texto_2);

                        $pedidos = $this->crear_registro_npsi($id_cita);

                        /*if (count($pedidos)>0) {
                            $texto_3 = "Se hace pedido de piezas al dms";
                            //$this->sms_general('3328351116',$texto_3);
                            $this->pedido_piezas_dms($pedidos,$id_cita);
                        }*/
                    }

                    $respuesta = "OK";
                }
            }else{
                $respuesta = "No se actualizo correctamente la información. Intentar nuevamnete mas tarde.";
            }
        }else{
            $respuesta = "No llego la información. Intente nuevamente.";
        }

        echo $respuesta;
    }

    //"Elimiar" una refaccion
    public function refaccionesBaja()
    {
        //Verificamos que las variables hayan llegado
        if (count($_POST)>0) {
            //Recuperamos los valores
            $id_cita = $_POST["id_cita"];
            $id_refaccion = $_POST["id_refaccion"];

            $fecha_registro = date("Y-m-d H:i:s");
            
            //Verificamos que haya llegado la informacion
            if (($id_cita != "")&&($id_refaccion != "")) {
                //Armanos el paquete para modificar
                $refaccion_baja = array(
                    "activo" => 0,
                    "fecha_actualiza" => $fecha_registro,
                );

                //Actualizamos el registro de la refaccion
                $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$refaccion_baja,'id',$id_refaccion);
                if ($actualizar) {
                    $this->registrar_movimiento($id_cita,"Se dio de baja la refacción con id: ".$id_refaccion);
                    $respuesta = "OK";
                }else{
                    $respuesta = "Error al intentar actualizar el registro";
                }
            }else{
                $respuesta = "Error información incompleta";
            }
        }else{
            $respuesta = "Error al intentar recuperar la información de la refacción";
        }

        echo $respuesta;
    }

    //Generamos pdf
    public function generar_pdf($datos = NULL,$vista = '')
    {
        $this->load->view($vista, $datos);
    }

    public function registrar_movimiento($id_cita='',$movimiento = '',$origen = NULL)
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = (($origen != NULL) ? $origen : "Presupuestos");
            $id_usuario =  "";
            $usuario = "";
        }

        $historico = array(
            'id' => NULL,
            'id_cita' => $id_cita,
            'origen_movimiento' => $panel,
            'movimiento' => $movimiento,
            'id_usuario' => $id_usuario,
            'usuario' => $usuario,
            'fecha_registro' => $registro,
        );

        $this->mConsultas->save_register('presupuestos_historial',$historico);

        return TRUE;
    }

    //// ----------------------------------------------------------------------------
    ///                 Nuevas implementaciones
    //// ----------------------------------------------------------------------------
    //En caso de el presupuesto aprobado, no haya refacciones en existencia
    public function crear_registro_npsi($id_cita = '')
    {
        $id = "";
        $cantidad = "";
        $descripcion = "";
        $clave_pieza = "";
        $pedido = [];
        $refacciones_p = [];
        $refacciones_g = [];

        //Recuperamos las refacciones del presupuesto
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_multipunto");
        foreach ($query as $row){
            if (($row->autoriza_asesor == "1")&&($row->pza_garantia == "0")&&($row->autoriza_cliente == "1")&&($row->activo == "1")) {
                if ($row->existe == "PLANTA") {
                    $refacciones_p[] = array(
                        "id" => $row->id,
                        "cantidad" => $row->cantidad,
                        "descripcion" => trim($row->descripcion, " \t\n\r\x0B"),
                        "clave_pieza" => $row->num_pieza,
                        "existencia" => $row->existe ,
                        "fecha_tentativa" => $row->fecha_planta,
                    );
                }elseif ($row->existe == "NO") {
                    $refacciones_p[] = array(
                        "id" => $row->id,
                        "cantidad" => $row->cantidad,
                        "descripcion" => trim($row->descripcion, " \t\n\r\x0B"),
                        "clave_pieza" => $row->num_pieza,
                        "existencia" => $row->existe ,
                        "fecha_tentativa" => $row->fecha_planta,
                    );
                }

                if (($row->existe == "PLANTA")||($row->existe == "NO")) {
                    $pedido[] = array(
                        "id" => $row->id,
                        "numero_parte" => $row->num_pieza,
                        "cantidad" => $row->cantidad,
                        "tipo" => $row->existe
                    );
                }
                    
            } elseif (($row->autoriza_asesor == "1")&&($row->autoriza_garantia == "1")&&($row->activo == "1")) {
                if ($row->existe == "PLANTA") {
                    $refacciones_g[] = array(
                        "id" => $row->id,
                        "cantidad" => $row->cantidad,
                        "descripcion" => trim($row->descripcion, " \t\n\r\x0B"),
                        "clave_pieza" => $row->num_pieza,
                        "existencia" => $row->existe ,
                        "fecha_tentativa" => $row->fecha_planta,
                    );
                }elseif ($row->existe == "NO") {
                    $refacciones_g[] = array(
                        "id" => $row->id,
                        "cantidad" => $row->cantidad,
                        "descripcion" => trim($row->descripcion, " \t\n\r\x0B"),
                        "clave_pieza" => $row->num_pieza,
                        "existencia" => $row->existe ,
                        "fecha_tentativa" => $row->fecha_planta,
                    );
                }

                if (($row->existe == "PLANTA")||($row->existe == "NO")) {
                    $pedido[] = array(
                        "id" => $row->id,
                        "numero_parte" => $row->num_pieza,
                        "cantidad" => $row->cantidad,
                        "tipo" => $row->existe
                    );
                }
            }
        }

        //Verificamos si hay refacciones publicas para psni
        if (count($refacciones_p)>0) {
            $this->guardar_psni_publico($refacciones_p,$id_cita);
        }

        //Verificamos si hay refacciones con garantias para psni
        if (count($refacciones_g)>0) {
            $this->guardar_psni_garantia($refacciones_g,$id_cita);
        }

        return $pedido;
        //$jsonDataEncoded = json_encode($pedido);
        //echo $jsonDataEncoded;
    }

    public function guardar_psni_publico($refacciones = NULL, $id_cita = "")
    {
        //Verificamos que no se dupliquen los registros
        //$query = $this->mConsultas->get_result("id_cita",$id_cita,"refacciones_cliente");
        //foreach ($query as $row){
            //$dato = $row->id;
        //}
        
         //Interpretamos la respuesta de la consulta
        //if (!isset($dato)) {
            $registro = date("Y-m-d H:i:s");

            $precarga = $this->mConsultas->precargaConsulta($id_cita);

            foreach ($precarga as $row){
                $contenedor = array(
                    "id" => NULL,
                    "fecha_folio" => date('Y-m-d'),
                    "id_cita" => $id_cita,
                    "estatus" => "PENDIENTE",
                    "notaEstatus" => "Estatus predeterminado",
                    "unidad_taller" => (($row->nombre_compania != "") ? $row->nombre_compania : $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno),
                    "telefono_principal" => $row->telefono_movil,
                    "telefono_secundario" => $row->otro_telefono,
                    "folio" => $this->generar_folio_psni(),
                    "asesor" => $row->asesor,
                    "modeloVehiculo" => (($row->subcategoria != "") ? $row->subcategoria : $row->vehiculo_modelo),
                    "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),

                    "cantidad" => "", //$cantidad,
                    "descripcionCorta" => "", //$descripcion,
                    "noParte" => "", //$clave_pieza,
                    "id_refaccion" => "", //$id,

                    "fecha_pedido" => date('Y-m-d'),
                    "noRemision" => "",
                    "costo" => "0",
                    "noUnidadImov" => "",
                    "fecha_promesa" => $row->fecha_entrega,
                    "fecha_recibe" => $row->fecha_recepcion,
                    "fecha_taller" =>  date('Y-m-d'),
                    "revisadaPor" => "",
                    "fecha_instalacion" => "",
                    "observaciones" => ((isset($fecha_tentativa)) ? "Fecha tentativa: ".$fecha_tentativa : ""),
                    "costoDist" => "0",
                    "costoTotal" => "0",
                    "tecnico" => $row->tecnico,
                    "notas" => "Refacciones autorizadas no disponibles",
                    "fecha_BO" => "0000-00-00", 
                    "backorder" => "0",
                    "archivos" => "",
                    "tiempo_inst_tecnico" => "/",
                    "tiempo_inst_cleinte" => "/",
                    "fecha_creacion" => $registro,
                    "fecha_actualizacion" => $registro,
                );

                $registroDB = $this->mConsultas->save_register('refacciones_cliente',$contenedor);

                //Si se guarda el registro, creamos el historico en los presupuestos
                if ($registroDB != 0) {
                    //Guardamos la refacciones
                    for ($i=0; $i < count($refacciones) ; $i++) { 
                        if ($refacciones[$i]["descripcion"] != "") {
                            $paquete = array(
                                "id" => NULL,
                                "id_cita" => $id_cita,
                                "id_registro" => $registroDB,
                                "id_refaccion" => $refacciones[$i]["id"],
                                "tipo" => "1",          //1 - público   2 - garantías
                                "cantidad" => $refacciones[$i]["cantidad"],
                                "descripcion" => $refacciones[$i]["descripcion"],
                                "clave_pieza" => $refacciones[$i]["clave_pieza"],
                                "existencia" => $refacciones[$i]["existencia"],
                                "activo" => "1",
                                "pedido" => "0",
                                "fecha_pedido" => $registro,
                                "fecha_alta" => $registro,
                                "fecha_edicion" => $registro,
                            );

                            $ref_p = $this->mConsultas->save_register('refaciones_psni',$paquete); 
                        }
                    }

                    //Recuperamos el usuario que realiza el movimiento
                    if ($this->session->userdata('rolIniciado')) {
                        $usuario = $this->session->userdata('nombreUsuario');
                    }elseif ($this->session->userdata('id_usuario')) {
                        $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
                    }else{
                        $usuario = "Cliente/Asesor";
                    }

                    //Creamos el registro del estaus
                    $contenedor_estatus = array(
                        "id" => NULL,
                        "id_cita" => $contenedor["id_cita"],
                        "id_registro" => $registroDB,
                        "estatus_anterior" => "",
                        "estatus_actual" => $contenedor["estatus"],
                        "fecha_inicio" => $registro,
                        "fecha_fin" => $registro,
                        "usuario_afecta" => $usuario,
                        "motivo" => $contenedor["notaEstatus"],
                        "cambio_num" => 1,
                    );

                    $registro_estatus = $this->mConsultas->save_register('estatus_cliente_historico',$contenedor_estatus);

                    $registro_evento = array(
                        'envia_departamento' => 'Cliente/Asesor',
                        'recibe_departamento' => 'Ventanilla',
                        'descripcion' => 'Registro automatico para PSNI',
                    );

                    $this->registroEvento($id_cita,$registro_evento);
                    $this->registrar_movimiento($id_cita,"Presupuesto: Se crea registro automatico para PSNI público","Cliente/Asesor");
                } 
            }
        //}
    }

    public function guardar_psni_garantia($refacciones = NULL, $id_cita = "")
    {
        //Verificamos que no se dupliquen los registros
        //$query = $this->mConsultas->get_result("id_cita",$id_cita,"refacciones_garantias");
        //foreach ($query as $row){
            //$dato = $row->id;
        //}
        
         //Interpretamos la respuesta de la consulta
        //if (!isset($dato)) {
            $registro = date("Y-m-d H:i:s");

            $precarga = $this->mConsultas->precargaConsulta($id_cita);

            foreach ($precarga as $row){
                $contenedor = array(
                    "id" => NULL,
                    "fecha_folio" => date('Y-m-d'),
                    "id_cita" => $id_cita,
                    "estatus" => "PENDIENTE",
                    "tel_cliente" => $row->telefono_movil,
                    "tel_secundario" => $row->otro_telefono,
                    "tecnico" => $row->tecnico,
                    "folio" => $this->generar_folio_psni_g(),
                    "asesor" => $row->asesor,
                    "modelo" => (($row->subcategoria != "") ? $row->subcategoria : $row->vehiculo_modelo),
                    "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                    "cliente" => (($row->nombre_compania != "") ? $row->nombre_compania : $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno),

                    "cantidad" => "",
                    "descripcion_corta" => "",
                    "nParte" => "",

                    "fecha_pedido" => date('Y-m-d'),
                    "no_rem" => "",
                    "fecha_BO" => date('Y-m-d'),
                    "nounidadInmov" => "",
                    "fecha_promesa" => $row->fecha_entrega,
                    "fecha_recibe" => $row->fecha_recepcion,
                    "fecha_aviso_taller" => date('Y-m-d'),
                    "ref_revisado_por" => "",
                    "observaciones_pieza" => "",
                    "costo_dist" => "",
                    "costo_total" => "",
                    "notasEstatus" => "Estatus predeterminado",
                    "backorder" => "0",
                    "tiempo_inst_tecnico" => "/",
                    "tiempo_inst_cleinte" => "/",
                    "fecha_creacion" => date('Y-m-d'),
                    "fecha_actualizacion" => date('Y-m-d'),
                );

                $registroDB = $this->mConsultas->save_register('refacciones_garantias',$contenedor);

                //Si se guarda el registro, creamos el historico en los presupuestos
                if ($registroDB != 0) {
                    //Guardamos la refacciones
                    for ($i=0; $i < count($refacciones) ; $i++) { 
                        if ($refacciones[$i]["descripcion"] != "") {
                            $paquete = array(
                                "id" => NULL,
                                "id_cita" => $id_cita,
                                "id_registro" => $registroDB,
                                "id_refaccion" => $refacciones[$i]["id"],
                                "tipo" => "2",          //1 - público   2 - garantías
                                "cantidad" => $refacciones[$i]["cantidad"],
                                "descripcion" => $refacciones[$i]["descripcion"],
                                "clave_pieza" => $refacciones[$i]["clave_pieza"],
                                "existencia" => $refacciones[$i]["existencia"],
                                "activo" => "1",
                                "pedido" => "0",
                                "fecha_pedido" => $registro,
                                "fecha_alta" => $registro,
                                "fecha_edicion" => $registro,
                            );

                            $ref_p = $this->mConsultas->save_register('refaciones_psni',$paquete);
                        }
                    }

                    //Recuperamos el usuario que realiza el movimiento
                    if ($this->session->userdata('rolIniciado')) {
                        $usuario = $this->session->userdata('nombreUsuario');
                    }elseif ($this->session->userdata('id_usuario')) {
                        $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
                    }else{
                        $usuario = "Cliente/Asesor";
                    }

                    //Creamos el registro del estaus
                    $contenedor_estatus = array(
                        "id" => NULL,
                        "id_cita" => $contenedor["id_cita"],
                        "id_registro" => $registroDB,
                        "estatus_anterior" => "",
                        "estatus_actual" => $contenedor["estatus"],
                        "fecha_inicio" => $registro,
                        "fecha_fin" => $registro,
                        "usuario_afecta" => $usuario,
                        "motivo" => $contenedor["notasEstatus"],
                        "cambio_num" => 1,
                    );

                    $registro_estatus = $this->mConsultas->save_register('estatus_refacciones_historico',$contenedor_estatus);

                    $registro_evento = array(
                        'envia_departamento' => 'Cliente/Asesor',
                        'recibe_departamento' => 'Ventanilla',
                        'descripcion' => 'Registro automatico para PSNI garantías',
                    );

                    $this->registroEvento($id_cita,$registro_evento);
                    $this->registrar_movimiento($id_cita,"Presupuesto: Se crea registro automatico para PSNI garantías","Cliente/Asesor");
                } 
            }
        //}
    }

    public function generar_folio_psni()
    {
        //Recuperamos el indice para el folio
        $registro = $this->mConsultas->last_id_refacciones("refacciones_cliente");

        $nvoFolio = $registro + 1;
        if ($nvoFolio < 10) {
            $folio = "0000".$nvoFolio;
        } elseif(($nvoFolio > 9) && ($nvoFolio < 100)) {
            $folio = "000".$nvoFolio;
        } elseif(($nvoFolio > 99) && ($nvoFolio < 1000)) {
            $folio = "00".$nvoFolio;
        } elseif(($nvoFolio > 999) && ($nvoFolio < 1000)) {
            $folio = "0".$nvoFolio;
        }else {
            $folio = $nvoFolio;
        }
        return $folio;
    }

    public function generar_folio_psni_g()
    {
        //Recuperamos el indice para el folio
        $registro = $this->mConsultas->last_id_refacciones("refacciones_garantias");

        $nvoFolio = $registro + 1;
        if ($nvoFolio < 10) {
            $folio = "0000".$nvoFolio;
        } elseif(($nvoFolio > 9) && ($nvoFolio < 100)) {
            $folio = "000".$nvoFolio;
        } elseif(($nvoFolio > 99) && ($nvoFolio < 1000)) {
            $folio = "00".$nvoFolio;
        } elseif(($nvoFolio > 999) && ($nvoFolio < 1000)) {
            $folio = "0".$nvoFolio;
        }else {
            $folio = $nvoFolio;
        }
        return $folio;
    }

    public function envio_correo_api($id_cita='')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, BASE_CITAS."Api/enviar_cotizacion_multipunto/".$id_cita);
        // Receive server response ...
        //curl_setopt($ch, CURLOPT_POSTFIELDS,"");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        /*curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Cache-Control": "no-cache",
                "Postman-Token": "6900f9a3-bbd5-4558-af94-c9ac3830948f"
            ));
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_HEADER, 0);*/

        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        return TRUE;
    }

    public function grabar_desgloce_piezas()
    {
        try {
            $sufijo = $_POST["sufijo"];
            $prefijo = $_POST["prefijo"];
            $basico = $_POST["basico"];
            $n_clave = $_POST["clave"];
            $id_ref = $_POST["refaccion"];

            if ($basico != "") {
                $embalaje = array(
                    "prefijo" => $prefijo,
                    "sufijo" => $sufijo,
                    "basico" => $basico,
                    "num_pieza" => $n_clave,
                );

                $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$embalaje,'id',$id_ref);
                if ($actualizar) {
                    echo "OK";
                } else {
                    echo "NO OK";
                }
                
            }else{
                echo "VACIO";
            }
        } catch (Exception $e) {
            echo "ERROR";
        }
    }

    public function entrega_refaccion_individual()
    {
        $aprobadas = $_POST["refaccion_aprobadas"];
        $entregadas = $_POST["refaccion_entregadas"];
        $id_refaccion = $_POST["refaccion"];

        $entregadas++;

        //Guardamos la refaccion
        $fecha_registro = date("Y-m-d H:i:s");

        $renglon["estado_entrega"] = 3;
        $renglon["firma_ventanilla_entrega"] = $this->base64ToImage($_POST["rutaFirmaVentanilla"],"firmas");
        $renglon["fecha_firma_entrega"] = date("Y-m-d H:i:s");
        $renglon["fecha_entrega"] = $fecha_registro;

        $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$renglon,'id',$id_refaccion);

        $movimiento = "Refacción ".$entregadas. " de ".$aprobadas." entregada al técnico";

        $refaccion["ubicacion_proceso"] = (($entregadas >= $aprobadas)) ? "Refacciones entregadas al técnico" : "Refacciones parcialmente entregadas al técnico";

        if ($entregadas >= $aprobadas) {
            $refaccion["estado_refacciones"] = 3;
            $refaccion["entrega_pieza"] = $fecha_registro;
        }

        $refaccion["fecha_actualiza"] = $fecha_registro;

        $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$refaccion,"id_cita",$_POST["id_cita"],"identificador","M");
        
        $this->registrar_movimiento($_POST["id_cita"],$movimiento,"Ventanilla");

        //Verificamos si ya se entregaron todas las refacciones
        if ($entregadas >= $aprobadas) {
            //Recuperamos datos extras para la notificación
            $query = $this->mConsultas->notificacionTecnicoCita($_POST["id_cita"]); 
            foreach ($query as $row) {
                $tecnico = $row->tecnico;
                $modelo = $row->vehiculo_modelo;
                $placas = $row->vehiculo_placas;
                $asesor = $row->asesor;
            }

            if (isset($tecnico)) {
                
                $texto = SUCURSAL.". Refacciones autorizadas del presupuesto: ".$_POST["id_cita"]." fue entregado al técnico el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                $descripcion = 'Presupuesto multipunto: refacciones entregadas al técnico ';
                
                //Enviar mensaje al asesor (si no hay rol de jefe de taller y tampoco garantias)
                $celular_sms = $this->mConsultas->telefono_asesor($_POST["id_cita"]);
                $this->sms_general($celular_sms,$texto);

                //Tester
                $this->sms_general('3328351116',$texto);
                //Angel evidencia
                //$this->sms_general('5535527547',$texto);
                
                //Grabamos el evento para el panel de gerencia
                $registro_evento = array(
                    'envia_departamento' => 'Ventanilla',
                    'recibe_departamento' => 'Asesores',
                    'descripcion' => $descripcion,
                );

                $this->registroEvento($_POST["id_cita"],$registro_evento);

                $resultado = "OK";
            }else{
                $resultado = "NO SE PUDO ENVIAR LA NOTIFICACIÓN";
            }
        }else{
            $resultado = "OK";
        }

        echo $resultado;
        
    }

    public function solicituar_refaccion_individual()
    {
        $aprobadas = $_POST["refaccion_aprobadas"];
        $recibidas = $_POST["refaccion_recibida"];
        $id_refaccion = $_POST["refaccion"];

        $recibidas++;

        //Guardamos la refaccion
        $fecha_registro = date("Y-m-d H:i:s");

        $renglon["estado_entrega"] = 2;

        $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$renglon,'id',$id_refaccion);

        $movimiento = "Refacción ".$recibidas. " de ".$aprobadas." recibidas en ventanilla";

        $refaccion["ubicacion_proceso"] = (($recibidas >= $aprobadas)) ? "Refacciones recibidas en ventanilla" : "Refacciones parcialmente recibidas en ventanilla";

        if ($recibidas >= $aprobadas) {
            $refaccion["estado_refacciones"] = 2;
            $refaccion["recibe_pieza"] = $fecha_registro;
        }

        $refaccion["fecha_actualiza"] = $fecha_registro;

        $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$refaccion,"id_cita",$_POST["id_cita"],"identificador","M");
        
        $this->registrar_movimiento($_POST["id_cita"],$movimiento,"Ventanilla");

        //Verificamos si ya se entregaron todas las refacciones
        $resultado = "OK";  

        echo $resultado;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
    
    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_HM.js',

                'assets/js/presupuestos/presupuestoMulti.js',
                'assets/js/presupuestos/autosuma.js',
                'assets/js/presupuestos/reglasVista_2.js',
                'assets/js/presupuestos/buscarRefacciones.js',
                //'assets/js/presupuestos/busquedas_filtros.js',
                'assets/js/presupuestos/busquedas_filtros_temp.js',
                'assets/js/presupuestos/envio_alta.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Presupuesto Multipunto"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}