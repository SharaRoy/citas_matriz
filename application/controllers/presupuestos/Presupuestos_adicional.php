<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Presupuestos_adicional extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    //Recibimos el id de la cita actual y decidimos a donde se va a enviar
    public function index($id_cita = 0,$datos = NULL)
    {
        //Recuperamos el folio de intelisis
        $datos["folio_externo"] = $this->mConsultas->id_orden_intelisis($id_cita);
        
        //Consultamos su ya se ha generado alguna cotizacion para ese presupuesto
        $revision = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","E","presupuesto_registro");
        foreach ($revision as $row) {
            $idCotizacion = $row->id;
            $acepta_cliente = $row->acepta_cliente;
        }

        //Si existe una cotizacion generada, solo se muestra la vista informativa
        if (isset($idCotizacion)) {
            //Consultamos si ya se entrego la unidad
            //$datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($id_cita);

            //Verificamos si ya se afecto por el cliente
            if ($acepta_cliente != "") {
                $this->comodinCliente($id_cita,$datos);
            } else {
                $this->comodinNoEdita($id_cita,$datos);
            }
            
        //De lo contrario se abre la vista de alta
        } else {
            $datos['id_cita'] = $id_cita;
            $this->loadAllView($datos,'presupuestos/adicional/presupuesto_alta');
        }
    }

/******************** Entradas comodines para redireccionar *************************/
    //Parada para indicar que sera una vista de alta
    public function comodinCliente($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "2";
        $datos["dirige"] = "Cliente";
        $datos["destinoVista"] = "Cliente";
        $id_cita_d = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita_d,$datos);
    }

    //Parada para indicar que sera una vista del cliente
    public function comodinNoEdita($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "3";
        $datos["dirige"] = "NoEdita";
        $datos["destinoVista"] = "NoEdita";
        $id_cita_d = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita_d,$datos);
    }

    //Parada para indicar que sera una vista del cliente
    public function comodinGeneraPdf($id_cita = "",$datos = NULL)
    {
        $datos["vista"] = "4";
        $datos["dirige"] = "PDF";
        $datos["destinoVista"] = "PDF";
        $id_cita_d = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita_d,$datos);
    }
/******************** Entradas comodines para redireccionar *************************/

    //Validamos el formulario a guardar
    public function validateFormR2()
    {
        //Destino de la visata
        $datos["vista"] = $this->input->post("modoVistaFormulario");
        //id de la cita asociada
        $datos["id_cita"] = $this->input->post("idOrdenTemp");
        //Dato identificador
        $datos["tipoRegistro"] = $this->input->post("tipoRegistro");
        //
        $datos["modoGuardado"] = $this->input->post("modoGuardado");
        //Perfil que accede a la vista
        $datos["dirige"] = $this->input->post("dirige");
        $datos["folio_externo"] = $this->input->post("folio_externo");
        $datos["destinoMensaje"] = $this->input->post("aceptarCotizacion");
        //Identificamos el tipo de presupuesto que se evalua
        $datos["presupuesto_bandera"] = $this->input->post("destino_vista");

        //Recuperamos el id interno de magic
        //$datos["orden_CDK"] = $this->mConsultas->orden_cdk_query($datos["id_cita"]);
        
        if ($datos["vista"] == "1") {
            //$this->form_validation->set_rules('idOrdenTemp', 'No. Orden es requerido.', 'required|callback_existencia|callback_existeOrden|callback_multipunto');
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden es requerido.', 'required|callback_existencia|callback_existeOrden');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }else {
            $this->form_validation->set_rules('idOrdenTemp', 'No. Orden de servicio requerido.', 'required');
            //Comprobamos que se hayan guardado al menos un elemento
            $this->form_validation->set_rules('registros', 'Campo requerido.', 'callback_registros');

        }

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('registros','No se ha guardado ningun elemento');
        $this->form_validation->set_message('existencia','Ya existe un presupuesto con este No. de Orden.');
        $this->form_validation->set_message('existeOrden','No existe una orden abierta con este No. de Orden.');
        $this->form_validation->set_message('multipunto','No existe una hoja multipunto creada previamente.');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $datos["mensaje"] = "1";
            //Si la vista es de decision
            //Alta/Actualiza asesor
            if ($datos["vista"] == "1") {
                $this->guardarRegistro($datos);
            }
        //Si no pasa las validaciones
        }else{
            //var_dump(validation_errors());
            $datos["mensaje"]="0";
            //Alta/Actualiza asesor
            if ($datos["vista"] == "1") {
                $this->index($datos["id_cita"],$datos);
            }
        }
    }

    //Comprobamos si existe algun presupuesto con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("idOrdenTemp");

        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;

            //Validamos en los presupuesto viejitos
            $query = $this->mConsultas->get_result("idOrden",$id_cita,"cotizacion_multipunto");
            foreach ($query as $row){
                if ($row->identificador == "M") {
                    $dato = $row->idCotizacion;
                }
            }

            //Interpretamos la respuesta de la consulta
            if (isset($dato)) {
                //Si existe el dato, entonces ya existe el registro
                $respuesta = FALSE;
            }else {
                //Si no existe el dato, entonces no existe el registro
                $respuesta = TRUE;
            }
        }
        
        return $respuesta;
    }

    //Comprobamos que exista una orden generada para poder crear la cotizacion
    public function existeOrden()
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("idOrdenTemp");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"ordenservicio");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            $respuesta = TRUE;
        }else {
            $respuesta = FALSE;
        }
        return $respuesta;
    }

    //Validamos si existe un registro con ese numero de orden
    public function multipunto()
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("idOrdenTemp");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("orden",$id_cita,"multipunto_general");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = TRUE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = FALSE;
        }
        return $respuesta;
    }

    //Validar requisitos minimos P1
    public function registros()
    {
        $return = FALSE;
        if ($this->input->post("registros")) {
            if (count($this->input->post("registros")) >0){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Guardamos registro por primera vez o actualizacion cel tecnico
    public function guardarRegistro($datos = NULL)
    {
        $fecha_registro = date("Y-m-d H:i");
        $renglones = $this->input->post("registros");
        
        if ($this->input->post("pzaGarantia") != NULL) {
            $pzas_garantias = $this->input->post("pzaGarantia");
        }else {
            $pzas_garantias = [];
        }

        $ref_tradicionales = 0;
        $ref_garantias = 0;
        $bandera = FALSE;

        //Guardamos los items
        for ($i=0; $i < count($renglones) ; $i++) { 
            //echo "<br>".$renglones[$i]."<br>";
            //Evitamos guardar basura
            if (strlen($renglones[$i])>19) {
                //Dividimos las celdas
                $campos = explode("_",$renglones[$i]);

                $registro = array(
                    "id" => NULL,
                    "id_cita" => $datos["id_cita"],
                    "identificador" => "E",
                    "cantidad" => $campos[0],
                    "descripcion" => $campos[1],
                    "num_pieza" => $campos[2],
                    "existe" => $campos[3],
                    "fecha_planta" => "0000-00-00",
                    "costo_pieza" => $campos[4],
                    "horas_mo" => $campos[5],
                    "costo_mo" => $campos[6],
                    "total_refacion" => $campos[7],
                    "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                    "costo_ford" => 0,
                    "horas_mo_garantias" => 0,
                    "costo_mo_garantias" => 0,
                    "autoriza_garantia" => 0,
                    "autoriza_jefeTaller" => 1,
                    "autoriza_asesor" => 1,
                    "autoriza_cliente" => 0,
                    "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                    "prioridad" => $campos[8],
                    "activo" => 1,
                    "fecha_registro" => $fecha_registro,
                    "fecha_actualiza" => $fecha_registro
                );

                //Guardamos el registro de la refaccion
                $id_retorno = $this->mConsultas->save_register('presupuesto_multipunto',$registro);
                if ($id_retorno != 0) {
                    //Si se guardo al menos una refaccion guardamos el registro general
                    $bandera = TRUE;
                    //Contabilizamos las refacciones tradicionales y las de garantias
                    if (!in_array(($i+1),$pzas_garantias)) {
                        $ref_tradicionales++;
                    } else {
                        $ref_garantias++;
                    }
                }
            }
        }

        //Validamos si se guardara un registro pivote
        if ($bandera) {
            //Verificamos los archivos generales
            if($this->input->post("tempFile") != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $this->input->post("tempFile")."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            $destino_presupuesto = $this->input->post("aceptarCotizacion");

            $registro_pivote = array(
                "id" => NULL,
                "id_cita" => $datos["id_cita"],
                "folio_externo" => $this->input->post("folio_externo"),
                "identificador" => "E",
                "cliente_paga" => 0,
                "ref_garantias" => $ref_garantias,
                "ref_tradicional" => $ref_tradicionales,
                "ford_paga" => 0,
                "total_pago_garantia" => 0,
                "envio_garantia" => 0,
                "comentario_garantia" => "",
                "firma_garantia" => "",
                "anticipo" => 0,
                "nota_anticipo" => "",
                "subtotal" => $this->input->post("subTotalMaterial"),
                "cancelado" => 0,
                "iva" => $this->input->post("ivaMaterial"),
                "total" => $this->input->post("totalMaterial"),
                "ventanilla" => "",
                "envia_ventanilla" => (($destino_presupuesto == "Enviar Cotización a Ventanilla") ? "0" : "2"),
                "estado_refacciones" => 0,
                "estado_refacciones_garantias" => 0,
                "archivos_presupuesto" => $archivos_gral,
                "archivo_tecnico" => "",
                "archivo_jdt" => "",
                "firma_asesor" => $this->base64ToImage($this->input->post("rutaFirmaAsesorCostos"),"firmas"),
                "comentario_asesor" => $this->input->post("nota_asesor"),
                "envio_jdt" => 0,
                "firma_jdt" => "",
                "comentario_jdt" => "",
                "comentario_ventanilla" => "",
                "comentario_tecnico" => "",
                "serie" => $this->input->post("noSerie"),
                "firma_requisicion" => "",
                "firma_requisicion_garantias" => "",
                "correo_adicional" => "",
                "envio_proactivo" => 0,
                "acepta_cliente" => "",
                //Si el proceso tiene para del jefe de taller
                "ubicacion_proceso" => "En espera de ser afectada por el cliente",
                "ubicacion_proceso_garantia" => (($ref_garantias > 0) ? "Espera revisión de ventanilla" : "Sin proceso"),
                "fecha_alta" => $fecha_registro,
                "termina_ventanilla" => $fecha_registro,
                "termina_jdt" => $fecha_registro,
                "termina_garantias" => $fecha_registro,
                "termina_asesor" => $fecha_registro,
                "termina_cliente" => $fecha_registro,
                "termina_requisicion" => $fecha_registro,
                "termina_requisicion_garantias" => $fecha_registro,
                "solicita_pieza" => $fecha_registro,
                "recibe_pieza" => $fecha_registro,
                "entrega_pieza" => $fecha_registro,
                "solicita_pieza_garantia" => $fecha_registro,
                "recibe_pieza_garantia" => $fecha_registro,
                "entrega_pieza_garantia" => $fecha_registro,
                "termina_proactivo" => $fecha_registro,
                "fecha_actualiza" => $fecha_registro,
            );

            //Guardamos el registro de la refaccion
            $id_retorno = $this->mConsultas->save_register('presupuesto_registro',$registro_pivote);
            if ($id_retorno != 0) {
                $datos["mensaje"] = "1";

                //Recuperamos datos extras para la notificación
                $query = $this->mConsultas->notificacionTecnicoCita($datos["id_cita"]); 
                foreach ($query as $row) {
                    $tecnico = $row->tecnico;
                    $modelo = $row->vehiculo_modelo;
                    $placas = $row->vehiculo_placas;
                    $asesor = $row->asesor;
                }

                //Mandamos la notificacion de la cotizacion creada a ventanilla
                if ($destino_presupuesto == "Enviar Cotización a Ventanilla") {
                    $texto = SUCURSAL.". Nueva solicitud de presupuesto multipunto creada. No. Orden: ".$datos["id_cita"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                    //Refacciones
                    //$this->sms_general('',$texto);
                    
                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    $this->sms_general('5535527547',$texto);

                //Mandamos la notificacion de la cotizacion creada al  asesor/jefe de talles 
                }else{
                    $texto = SUCURSAL.". Nueva solicitud de presupuesto adicional creada por el asesor con refacciones con garantías. No. Orden: ".$datos["id_cita"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                    //Jefe de taller (si aplica)
                    //$this->sms_general('',$texto);
                    
                    //Garantias (si aplica y no hay jefe de taller)
                    //$this->sms_general('',$texto);
                    
                    //Enviar mensaje al asesor (si no hay rol de jefe de taller y tampoco garantias)
                    //$celular_sms = $this->mConsultas->telefono_asesor($datos["id_cita"]);
                    //$this->sms_general($celular_sms,$texto);

                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    $this->sms_general('5535527547',$texto);
                }

                //Grabamos el evento para el panel de gerencia
                $registro_evento = array(
                    'envia_departamento' => 'Asesores',
                    'recibe_departamento' => 'Clientes',
                    'descripcion' => 'Nueva solicitud de presupuesto adicional creada.',
                );

                $this->registroEvento($datos["id_cita"],$registro_evento);
            }else{
                $datos["mensaje"] = "0";
            }
        }else{
            $datos["mensaje"] = "0";
        }

        $this->index($datos["id_cita"],$datos);
    }

    //Recuperamos la informacion de la cotizacion
    public function setData($id_cita = 0,$datos = NULL)
    {
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["tipoRegistro"])) {
            $datos["tipoRegistro"] = "";
        }

        if (!isset($datos["dirige"])) {
            if ($this->session->userdata('rolIniciado')) {
                $datos["dirige"] = $this->session->userdata('rolIniciado');
            }else {
                $datos["dirige"] = "";
            }
        }

        //Recuperamos el id interno de magic
        //$datos["orden_CDK"] = $this->mConsultas->orden_cdk_query($id_cita);

        //Recuperamos la informacion de los registros
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","E","presupuesto_registro");
        foreach ($query as $row){
            $datos["folio_externo"] = $row->folio_externo;
            $datos["ref_garantias"] = $row->ref_garantias;
            //Valores para garantias
            $datos["cliente_paga"] = $row->cliente_paga;
            $datos["ford_paga"] = $row->ford_paga;
            $datos["total_pago_garantia"] = $row->total_pago_garantia;
            $datos["envio_garantia"] = $row->envio_garantia;
            $datos["comentario_garantia"] = $row->comentario_garantia;
            $datos["firma_garantia"] = $row->firma_garantia;
            //Valores globales
            $datos["anticipo"] = $row->anticipo;
            $datos["notaAnticipo"] = $row->nota_anticipo;
            $datos["subtotal"] = $row->subtotal;
            $datos["cancelado"] = $row->cancelado;
            $datos["iva"] = $row->iva;
            $datos["total"] = (((float)$row->subtotal - (float)$row->cancelado) * 1.16)-$row->anticipo;
            //Valores de proceso
            $datos["envia_ventanilla"] = $row->envia_ventanilla;
            $datos["ubicacion_proceso"] = $row->ubicacion_proceso;
            $datos["ubicacion_proceso_garantia"] = $row->ubicacion_proceso_garantia;
            $datos["estado_refacciones"] = $row->estado_refacciones;
            $datos["estado_refacciones_garantias"] = $row->estado_refacciones_garantias;
            //Archivos
            $datos["archivos_presupuesto"] = explode("|",$row->archivos_presupuesto);
            $datos["archivos_presupuesto_lineal"] = $row->archivos_presupuesto;

            $datos["archivo_tecnico"] = explode("|",$row->archivo_tecnico);
            $datos["archivo_tecnico_lineal"] = $row->archivo_tecnico;

            $datos["archivo_jdt"] = explode("|",$row->archivo_jdt);
            $datos["archivo_jdt_lineal"] = $row->archivo_jdt;
            
            $datos["firma_asesor"] = $row->firma_asesor;
            $datos["nota_asesor"] = $row->comentario_asesor;
            
            $datos["envio_jdt"] = $row->envio_jdt;
            $datos["firma_jdt"] = $row->firma_jdt;
            $datos["comentario_jdt"] = $row->comentario_jdt;
            
            $datos["comentario_ventanilla"] = $row->comentario_ventanilla;
            $datos["comentario_tecnico"] = $row->comentario_tecnico;
            
            $datos["serie"] = $row->serie;
            $datos["firma_requisicion"] = $row->firma_requisicion;
            $datos["firma_requisicion_garantias"] = $row->firma_requisicion_garantias;
            $datos["correo_adicional"] = $row->correo_adicional;
            $datos["envio_proactivo"] = $row->envio_proactivo;
            $datos["acepta_cliente"] = $row->acepta_cliente;

            $datos["fecha_alta"] = explode(" ",$row->fecha_alta);
            $fragmentos = explode("-",$datos["fecha_alta"][0]);
            $mes_index = (int)$fragmentos[1];
            $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
            //$datos["mes"] = $meses[date("n")-1];
            $datos["fecha_limite"] = $fragmentos[2]." de ".$meses[(($mes_index < 12) ? $mes_index : 0)]." del ".$fragmentos[0];
        }

        //Recuperamos las refacciones
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","E","presupuesto_multipunto");

        $datos["registrosControl"] = 0;
        $datos["registrosGarantias"] = 0;

        foreach ($query as $row){
            //Confirmamos que la refaccion este activa
            if ($row->activo == "1") {
                $datos["id_refaccion"][] = $row->id;

                $datos["cantidad"][] = $row->cantidad;
                $datos["descripcion"][] = $row->descripcion;
                $datos["num_pieza"][] = $row->num_pieza;
                $datos["existe"][] = $row->existe;
                $datos["fecha_planta"][] = $row->fecha_planta;
                $datos["costo_pieza"][] = $row->costo_pieza;
                $datos["horas_mo"][] = $row->horas_mo;
                $datos["costo_mo"][] = $row->costo_mo;
                $datos["total_refacion"][] = $row->total_refacion;
                $datos["prioridad"][] = $row->prioridad;

                $datos["renglon"][] = $row->cantidad."_".$row->descripcion."_".$row->num_pieza."_".$row->existe."*".$row->fecha_planta."_".$row->costo_pieza."_".$row->horas_mo."_".$row->costo_mo."_".$row->total_refacion."_".$row->prioridad;

                $datos["pza_garantia"][] = $row->pza_garantia;

                $datos["costo_ford"][] = $row->costo_ford;
                $datos["horas_mo_garantias"][] = $row->horas_mo_garantias;
                $datos["costo_mo_garantias"][] = $row->costo_mo_garantias;

                $datos["renglonGarantia"][] = $row->costo_ford."_".$row->horas_mo_garantias."_".$row->costo_mo_garantias;

                $datos["autoriza_garantia"][] = $row->autoriza_garantia;
                $datos["autoriza_jefeTaller"][] = $row->autoriza_jefeTaller;
                $datos["autoriza_asesor"][] = $row->autoriza_asesor;
                $datos["autoriza_cliente"][] = $row->autoriza_cliente;

                //Refacciones (activas) totales del presupuesto
                $datos["registrosControl"] += 1;
                //Refacciones (activas) con garantias totales del presupuesto
                if ($row->pza_garantia == "1") {
                    $datos["registrosGarantias"] += 1;
                }
                
            }
        }

        $datos["id_cita"] = $id_cita;
        $datos["tipoOrigen"] = "ADICIONAL";

        //Identificamos en que vista se mostrara la informacion
        //Vista del presupuesto para el asesor
        if ($datos["destinoVista"] == "NoEdita") {
            $this->loadAllView($datos,'presupuestos/adicional/presupuesto_sin_editar');
        //Vista del presupuesto original
        }elseif ($datos["destinoVista"] == "Cliente") {
            $this->loadAllView($datos,'presupuestos/adicional/presupuesto_cliente');
        //Vista para ver el pdf
        }elseif ($datos["destinoVista"] == "PDF") {
            //Cargamos datos adicionales
            $precarga = $this->mConsultas->precargaConsulta($id_cita);
            foreach ($precarga as $row){
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $datos["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $datos["modelo"] = $row->vehiculo_anio;
                $datos["placas"] = $row->vehiculo_placas;
                $datos["categoria"] = $row->subcategoria;
                $datos["tecnico"] = $row->tecnico;
                $datos["asesor"] = $row->asesor;
            }
            $this->generar_pdf($datos,"presupuestos/cliente/plantilla_pdf");
        }
    }

    //Validamos y guardamos los documentos
    public function validateDoc($nombreDoc = NULL)
    {
        $path = "";
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);

        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
        for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docMultipunto";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_PMA";
            for($j=0; $j<=5; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }

    //Convertir canvas base64 a imagen (firmas)
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_PMA";
        for($i=0; $i<=5; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    //Actualizar presupuesto afectados por el cliente
    public function presupuestoAfectado()
    {
        //Verificamos que las variables hayan llegado
        if (count($_POST)>0) {
            //Recuperamos los valores
            $renglones = explode("=",$_POST["renglones"]);
            $cancelado = (float)$_POST["cancelado"];
            $subtotal = (float)$_POST["subtotal"];
            $desicion = $_POST["desicion"];
            $id_cita = $_POST["id_cita"];

            $bandera = FALSE;
            $fecha_registro = date("Y-m-d H:i:s");

            //Recuperamos la informacion para los renglones
            for ($i=0; $i < count($renglones); $i++) { 
                $registro = explode("-",$renglones[$i]);
                if (count($registro)>1) {
                    $refaccion["autoriza_cliente"] = $registro[1];
                    //$id_refaccion[] = $registro[0];
                    
                    //Actualizamos el registro
                    $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$refaccion,'id',$registro[0]);
                    if ($actualizar) {
                        //Si se actualizo correctamente
                        $bandera = TRUE;
                   }
                }
            }

            //Verificamos que la actualizacion se haya completado con exito
            if ($bandera) {
                //Calculamos los nuevos totales
                //$nvo_subtotal =  $subtotal - $cancelado;
                //$iva = $nvo_subtotal * 0.16;
                //$total = $nvo_subtotal * 1.16;

                $presupuesto_gral = array(
                    //"subtotal" => $nvo_subtotal,
                    "cancelado" => $cancelado,
                    //"iva" => $iva,
                    //"total" => $total,
                    "acepta_cliente" => $desicion,
                    "ubicacion_proceso" => "Afectada por el cliente",
                    "termina_cliente" => $fecha_registro,
                    "fecha_actualiza" => $fecha_registro,
                );

                //Guardamos el registro de la refaccion
                $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$presupuesto_gral,"id_cita",$id_cita,"identificador","E");
                if ($actualiza_reg) {
                    //Recuperamos datos extras para la notificación
                    $query = $this->mConsultas->notificacionTecnicoCita($id_cita); 
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $placas = $row->vehiculo_placas;
                        $asesor = $row->asesor;
                    }

                    if (isset($tecnico)) {
                        //Interpretamos la decision del cliente
                        if ($desicion == "Si") {
                            $desicion_txt = "'Autorizado Totalmente'";
                        }elseif ($desicion == "No") {
                            $desicion_txt = "'Rechazado Totalmente'";
                        }else{
                            $desicion_txt = "'Autorizado Parcialmente'";
                        }

                        //Identificamos si lo afecta el cliente o el asesor
                        if ($this->session->userdata('rolIniciado')) {
                            $quien = "Asesor";
                        }else{
                            $quien = "Cliente";
                        }

                        $texto = SUCURSAL.". Presupuesto adicional de la Orden: ".$id_cita." fue ".$desicion_txt." por el ".$quien." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        //Jefe de taller (si aplica)
                        //$this->sms_general('',$texto);
                        
                        //Ventanilla
                        //$this->sms_general('',$texto);
                        //$this->sms_general('',$texto);

                        //Enviar mensaje al asesor (si no hay rol de jefe de taller)
                        $celular_sms = $this->mConsultas->telefono_asesor($id_cita);
                        $this->sms_general($celular_sms,$texto);
                        
                        //Enviamos mensaje al tecnico
                        $celular_sms_2 = $this->mConsultas->telefono_tecnico($id_cita);
                        $this->sms_general($celular_sms_2,$texto);
                        
                        //Tester
                        //$this->sms_general('3328351116',$texto);
                        //Angel evidencia
                        $this->sms_general('5535527547',$texto);
                    }

                    //Grabamos el evento para el panel de gerencia
                    $registro_evento = array(
                        'envia_departamento' => 'Cliente',
                        'recibe_departamento' => 'Asesores',
                        'descripcion' => 'Presupuesto afectado: '.$desicion_txt,
                    );

                    $this->registroEvento($datos["id_cita"],$registro_evento);

                    $respuesta = "OK";
                }
            }else{
                $respuesta = "No se actualizo correctamente la información. Intentar nuevamnete mas tarde.";
            }
        }else{
            $respuesta = "No llego la información. Intente nuevamente.";
        }

        echo $respuesta;
    }

    //Envio de notificaciones
    public function sms_general($celular_sms='',$mensaje_sms=''){
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    //Registro de eventos 
    public function registroEvento($id_cita = 0, $datos = NULL)
    {
        //Recuperamos los datos del usuario
        if ($this->session->userdata('rolIniciado')) {
            $envia_id_usuario = $this->session->userdata('idUsuario');
            //$envia_nombre_usuario = $datos["nombre"];

            if ($this->session->userdata('rolIniciado') == "TEC") {
                $revision = $this->mConsultas->get_result("id",$envia_id_usuario,"tecnicos");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->nombre);
                }
            } else {
                $revision = $this->Verificacion->get_result("adminId",$envia_id_usuario,"admin");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->adminNombre);
                }
            }
        }else{
            $envia_id_usuario = "0";//$datos["idUsuario"];
            $envia_nombre_usuario = "Usuario";//$datos["nombre"];
        }

        //$envia_departamento = $datos["dpo"];
        $sucursal = BIN_SUCURSAL;
        $envia_fecha_hora = date("Y-m-d H:i:s");
        //$recibe_departamento = $datos["dpoRecibe"];
        $id_cita = $id_cita;
        //$descripcion = $datos["descripcion"];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/gerencia/index.php/tiempos/insertar_tiempo");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "envia_id_usuario=".$envia_id_usuario."&envia_nombre_usuario=".$envia_nombre_usuario."&envia_departamento=".$datos["envia_departamento"]."&sucursal=".$sucursal."&envia_fecha_hora=".$envia_fecha_hora."&recibe_departamento=".$datos["recibe_departamento"]."&id_cita=".$id_cita."&descripcion=".$datos["descripcion"]."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);
    }

    //Generamos pdf
    public function generar_pdf($datos = NULL,$vista = '')
    {
        $this->load->view($vista, $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_HM.js',
                'assets/js/presupuestos/operaciones_adicionales.js',
                //'assets/js/presupuestos/presupuestoMulti.js',
                //'assets/js/presupuestos/autosuma.js',
                'assets/js/presupuestos/reglasVista_2.js',
                'assets/js/presupuestos/buscarRefacciones.js',
                'assets/js/presupuestos/busquedas_filtros.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Presupuesto Multipunto"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}