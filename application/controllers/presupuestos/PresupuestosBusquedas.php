<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

require_once APPPATH . "/third_party/PHPExcel.php";

class PresupuestosBusquedas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

        $this->load->library('excel');
    }

    //Busqueda de presupuestos por estatus de ventanilla
    public function filtroCotizacionesVentanilla()
    {
    	$campo = $_POST['campo'];
        $estado_refaccion = $_POST['estado_refaccion'];
        //$condicion_2 = $_POST['condicion_2'];
        
        if ($estado_refaccion == "") {
            $precarga = $this->mConsultas->listaPresupuestoCampo($campo); 

            $precarga_old = $this->mConsultas->listaCotizacionBusqueda($campo);
        }else{
            if ($campo != "") {
                $precarga = $this->mConsultas->listaPresupuestoEstatusCampo($campo,$estado_refaccion);

                $edo_refaccion = (($estado_refaccion == "2") ? "3" : (($estado_refaccion == "3") ? "2" : $estado_refaccion));
                $precarga_old = $this->mConsultas->listaCotizacionCampoEstatus($campo,$edo_refaccion);

            } elseif ($campo == "") {
                $precarga = $this->mConsultas->listaCotizacionEstatus($estado_refaccion);

                $edo_refaccion = (($estado_refaccion == "2") ? "3" : (($estado_refaccion == "3") ? "2" : $estado_refaccion));
                $precarga_old = $this->mConsultas->listaCotizacionEstatus($edo_refaccion);

            }else {
                $precarga = $this->mConsultas->listaPresupuestoCampo($campo);

                $precarga_old = $this->mConsultas->listaCotizacionBusqueda($campo);
            }
        }

        $cadena = "";
        $renglones = 0;
        foreach ($precarga as $row) {
        	//Verificamos si ya se libero de ventanilla
            if ($row->envia_ventanilla == "0") {
                //Recuperamos los datos
                $cadena .= $row->id_cita."=";
                
                //Identificamos el tipo de presupuesto a mostara
                $cadena .=(((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
                $cadena .= "1"."=";

                $cadena .= $row->envio_garantia."="; // 3
                $cadena .= $row->envia_ventanilla."="; // 4
                $cadena .= $row->estado_refacciones."="; // 5
                $cadena .= $row->estado_refacciones_garantias."="; // 6
                $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
                $cadena .= $row->envio_jdt."="; // 8
                
                $cadena .= $row->firma_requisicion."="; // 9
                $cadena .= $row->firma_requisicion_garantias."="; // 10
                $cadena .= $row->ubicacion_proceso."="; // 11
                $cadena .= $row->acepta_cliente."="; // 12

                $cadena .= $row->serie."=";  //13
                $cadena .= $row->tecnico."=";  //14
                $cadena .= $row->asesor."=";  //15
                $cadena .= $row->vehiculo_modelo."=";  //16
                $cadena .= $row->vehiculo_placas."=";  //17
                
                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $cadena .= "background-color:#f5acaa;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    } elseif ($row->estado_refacciones == "1") {
                        $cadena .= "background-color:#f5ff51;"."=";  // 18
                        $cadena .= "SOLICIATADAS"."=";  // 19
                    } elseif ($row->estado_refacciones == "2") {
                        $cadena .= "background-color:#5bc0de;"."=";  // 18
                        $cadena .= "RECIBIDAS"."=";  // 19
                    }else{
                        $cadena .= "background-color:#c7ecc7;"."=";  // 18
                        $cadena .= "ENTREGADAS"."=";  // 19
                    }
                } else {
                    $cadena .= "background-color:red;color:white;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                }

                $cadena .= $this->encrypt($row->id_cita)."="; //20
                
            //Si ya se libero de ventanilla, definimos si seran una o dos presupuestos
            }else{
                if ((int)$row->ref_tradicional > 0) {
                    //Recuperamos los datos
                    $cadena .=  $row->id_cita."=";
                    
                    //Identificamos el tipo de presupuesto a mostara
                    $cadena .=  "TRADICIONAL"."=";
                    $cadena .=  "1"."=";

                    $cadena .= $row->envio_garantia."="; // 3
	                $cadena .= $row->envia_ventanilla."="; // 4
	                $cadena .= $row->estado_refacciones."="; // 5
	                $cadena .= $row->estado_refacciones_garantias."="; // 6
	                $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
	                $cadena .= $row->envio_jdt."="; // 8
	                
	                $cadena .= $row->firma_requisicion."="; // 9
	                $cadena .= $row->firma_requisicion_garantias."="; // 10
	                $cadena .= $row->ubicacion_proceso."="; // 11
	                $cadena .= $row->acepta_cliente."="; // 12

	                $cadena .= $row->serie."=";  //13
	                $cadena .= $row->tecnico."=";  //14
	                $cadena .= $row->asesor."=";  //15
	                $cadena .= $row->vehiculo_modelo."=";  //16
	                $cadena .= $row->vehiculo_placas."=";  //17
	                
	                if ($row->acepta_cliente != "No") {
	                    if ($row->estado_refacciones == "0") {
	                        $cadena .= "background-color:#f5acaa;"."=";  // 18
	                        $cadena .= "SIN SOLICITAR"."=";  // 19
	                    } elseif ($row->estado_refacciones == "1") {
	                        $cadena .= "background-color:#f5ff51;"."=";  // 18
	                        $cadena .= "SOLICIATADAS"."=";  // 19
	                    } elseif ($row->estado_refacciones == "2") {
	                        $cadena .= "background-color:#5bc0de;"."=";  // 18
	                        $cadena .= "RECIBIDAS"."=";  // 19
	                    }else{
	                        $cadena .= "background-color:#c7ecc7;"."=";  // 18
	                        $cadena .= "ENTREGADAS"."=";  // 19
	                    }
	                } else {
	                    $cadena .= "background-color:red;color:white;"."=";  // 18
	                    $cadena .= "SIN SOLICITAR"."=";  // 19
	                }

                    $cadena .= $this->encrypt($row->id_cita)."="; //20
                }

                //Si hay refacciones tradicionales creamos el registro (solo si se libero de garantias)
                if (((int)$row->ref_garantias > 0) && ($row->envio_garantia == "1")) {
                    //Recuperamos los datos
                    $cadena .=  $row->id_cita."=";
                    
                    //Identificamos el tipo de presupuesto a mostara
                    $cadena .=  "GARANTÍA"."=";
                    $cadena .=  "2"."=";

                    $cadena .= $row->envio_garantia."="; // 3
	                $cadena .= $row->envia_ventanilla."="; // 4
	                $cadena .= $row->estado_refacciones."="; // 5
	                $cadena .= $row->estado_refacciones_garantias."="; // 6
	                $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
	                $cadena .= $row->envio_jdt."="; // 8
	                
	                $cadena .= $row->firma_requisicion."="; // 9
	                $cadena .= $row->firma_requisicion_garantias."="; // 10
	                $cadena .= $row->ubicacion_proceso."="; // 11
	                $cadena .= $row->acepta_cliente."="; // 12

	                $cadena .= $row->serie."=";  //13
	                $cadena .= $row->tecnico."=";  //14
	                $cadena .= $row->asesor."=";  //15
	                $cadena .= $row->vehiculo_modelo."=";  //16
	                $cadena .= $row->vehiculo_placas."=";  //17
	                
                    if ($row->estado_refacciones_garantias == "0") {
                        $cadena .= "background-color:#f5acaa;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    } elseif ($row->estado_refacciones_garantias == "1") {
                        $cadena .= "background-color:#f5ff51;"."=";  // 18
                        $cadena .= "SOLICIATADAS"."=";  // 19
                    } elseif ($row->estado_refacciones_garantias == "2") {
                        $cadena .= "background-color:#5bc0de;"."=";  // 18
                        $cadena .= "RECIBIDAS"."=";  // 19
                    }else{
                        $cadena .= "background-color:#c7ecc7;"."=";  // 18
                        $cadena .= "ENTREGADAS"."=";  // 19
                    }

                    $cadena .= $this->encrypt($row->id_cita)."="; //20
                }
            }

			$cadena .= "|";
            $renglones++;
        }

        echo $cadena;
    }

    //Busqueda de presupuestos global
    public function filtroCotizacionesGeneral()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $campo = $_POST['campo'];

        //Si no se envio un campo de consulta
        if ($campo == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->busquedaPresupuestoFecha($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->busquedaPresupuestoFechas_1($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->busquedaPresupuestoCampo($campo);
            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha($campo,$fecha_inicio,$fecha_fin);
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_1($campo,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_1($campo,$fecha_fin);
                }
            }
        }

        $cadena = "";

        foreach ($precarga as $row) {
            //Recuperamos los datos
            $cadena .= $row->id_cita."=";
            
            //Identificamos el tipo de presupuesto a mostara
            $cadena .= (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
            $cadena .= (((int)$row->ref_tradicional > 0) ? "1" : "0")."=";

            $cadena .= $row->envio_garantia."="; // 3
            $cadena .= $row->envia_ventanilla."="; // 4
            $cadena .= $row->estado_refacciones."="; // 5
            $cadena .= $row->estado_refacciones_garantias."="; // 6
            $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
            $cadena .= $row->envio_jdt."="; // 8
            
            $cadena .= $row->firma_requisicion."="; // 9
            $cadena .= $row->firma_requisicion_garantias."="; // 10
            $cadena .= $row->ubicacion_proceso."="; // 11
            $cadena .= $row->acepta_cliente."="; // 12

            $cadena .= $row->serie."=";  //13
            $cadena .= $row->tecnico."=";  //14
            $cadena .= $row->asesor."=";  //15
            $cadena .= $row->vehiculo_modelo."=";  //16
            $cadena .= $row->vehiculo_placas."=";  //17
            
            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $cadena .= "background-color:#f5acaa;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                } elseif ($row->estado_refacciones == "1") {
                    $cadena .= "background-color:#f5ff51;"."=";  // 18
                    $cadena .= "SOLICIATADAS"."=";  // 19
                } elseif ($row->estado_refacciones == "2") {
                    $cadena .= "background-color:#5bc0de;"."=";  // 18
                    $cadena .= "RECIBIDAS"."=";  // 19
                }else{
                    $cadena .= "background-color:#c7ecc7;"."=";  // 18
                    $cadena .= "ENTREGADAS"."=";  // 19
                }
            } else {
                $cadena .= "background-color:red;color:white;"."=";  // 18
                $cadena .= "SIN SOLICITAR"."=";  // 19
            }

            $cadena .= (((int)$row->ref_garantias > 0) ? "1" : "0")."="; //20

            $cadena .= $this->encrypt($row->id_cita)."="; //21

            $cadena .= "|";
        }

        echo $cadena;
    }

    //Busqueda de presupuestos del tecnico
    public function filtroCotizacionesTec()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $campo = $_POST['campo'];

        //Si no se envio un campo de consulta
        if ($campo == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->busquedaPresupuestoFecha_Tec($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->busquedaPresupuestoFechas_1_Tec($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->busquedaPresupuestoCampo_Tec($campo); 
            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_Tec($campo,$fecha_inicio,$fecha_fin);
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_1_Tec($campo,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_1_Tec($campo,$fecha_fin);
                }
            }
        }

        //Verificamos el id del tecnico
        if ($this->session->userdata('rolIniciado')) {
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {
                $idTecnico = $this->session->userdata('idUsuario'); 
            }else{
                $idTecnico = "";
            }
        }else{
            $idTecnico = "";
        } 

        $cadena = "";

        foreach ($precarga as $row) {
            //Verificamos si es un usuario general cargamos todos los datos
            if ($idTecnico == "") {
                //Recuperamos los datos
                $cadena .= $row->id_cita."=";
                
                //Identificamos el tipo de presupuesto a mostara
                $cadena .= (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
                $cadena .= (((int)$row->ref_tradicional > 0) ? "1" : "0")."=";

                $cadena .= $row->envio_garantia."="; // 3
                $cadena .= $row->envia_ventanilla."="; // 4
                $cadena .= $row->estado_refacciones."="; // 5
                $cadena .= $row->estado_refacciones_garantias."="; // 6
                $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
                $cadena .= $row->envio_jdt."="; // 8
                
                $cadena .= $row->firma_requisicion."="; // 9
                $cadena .= $row->firma_requisicion_garantias."="; // 10
                $cadena .= $row->ubicacion_proceso."="; // 11
                $cadena .= $row->acepta_cliente."="; // 12

                $cadena .= $row->serie."=";  //13
                $cadena .= $row->tecnico."=";  //14
                $cadena .= $row->asesor."=";  //15
                $cadena .= $row->vehiculo_modelo."=";  //16
                $cadena .= $row->vehiculo_placas."=";  //17
                
                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $cadena .= "background-color:#f5acaa;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    } elseif ($row->estado_refacciones == "1") {
                        $cadena .= "background-color:#f5ff51;"."=";  // 18
                        $cadena .= "SOLICIATADAS"."=";  // 19
                    } elseif ($row->estado_refacciones == "2") {
                        $cadena .= "background-color:#5bc0de;"."=";  // 18
                        $cadena .= "RECIBIDAS"."=";  // 19
                    }else{
                        $cadena .= "background-color:#c7ecc7;"."=";  // 18
                        $cadena .= "ENTREGADAS"."=";  // 19
                    }
                } else {
                    $cadena .= "background-color:red;color:white;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                }

                $cadena .= (((int)$row->ref_garantias > 0) ? "1" : "0")."="; //20

                $cadena .= $this->encrypt($row->id_cita)."="; //21

                $cadena .= "|";
            //De lo contrario solo cargamos los datos del tecnico
            }else{
                //Verificamos que el id del tecnico coincida con los de los resutados recuperados
                if ($idTecnico == $row->id_tecnico) {
                    //Recuperamos los datos
                    $cadena .= $row->id_cita."=";
                    
                    //Identificamos el tipo de presupuesto a mostara
                    $cadena .= (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
                    $cadena .= (((int)$row->ref_tradicional > 0) ? "1" : "0")."=";

                    $cadena .= $row->envio_garantia."="; // 3
                    $cadena .= $row->envia_ventanilla."="; // 4
                    $cadena .= $row->estado_refacciones."="; // 5
                    $cadena .= $row->estado_refacciones_garantias."="; // 6
                    $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
                    $cadena .= $row->envio_jdt."="; // 8
                    
                    $cadena .= $row->firma_requisicion."="; // 9
                    $cadena .= $row->firma_requisicion_garantias."="; // 10
                    $cadena .= $row->ubicacion_proceso."="; // 11
                    $cadena .= $row->acepta_cliente."="; // 12

                    $cadena .= $row->serie."=";  //13
                    $cadena .= $row->tecnico."=";  //14
                    $cadena .= $row->asesor."=";  //15
                    $cadena .= $row->vehiculo_modelo."=";  //16
                    $cadena .= $row->vehiculo_placas."=";  //17
                    
                    if ($row->acepta_cliente != "No") {
                        if ($row->estado_refacciones == "0") {
                            $cadena .= "background-color:#f5acaa;"."=";  // 18
                            $cadena .= "SIN SOLICITAR"."=";  // 19
                        } elseif ($row->estado_refacciones == "1") {
                            $cadena .= "background-color:#f5ff51;"."=";  // 18
                            $cadena .= "SOLICIATADAS"."=";  // 19
                        } elseif ($row->estado_refacciones == "2") {
                            $cadena .= "background-color:#5bc0de;"."=";  // 18
                            $cadena .= "RECIBIDAS"."=";  // 19
                        }else{
                            $cadena .= "background-color:#c7ecc7;"."=";  // 18
                            $cadena .= "ENTREGADAS"."=";  // 19
                        }
                    } else {
                        $cadena .= "background-color:red;color:white;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    }

                    $cadena .= (((int)$row->ref_garantias > 0) ? "1" : "0")."="; //20

                    $cadena .= $this->encrypt($row->id_cita)."="; //21

                    $cadena .= "|";
                }
            }
        }
        echo $cadena;
    }

    //Busquedas personalizadas para adaptacion y unificadion
    //Busqueda de presupuestos por estatus de ventanilla
    public function filtroCotizacionesVentanilla_Temp()
    {
        $campo = $_POST['campo'];
        $estado_refaccion = $_POST['estado_refaccion'];
        //$condicion_2 = $_POST['condicion_2'];
        
        if ($estado_refaccion == "") {
            $precarga = $this->mConsultas->listaPresupuestoCampo($campo); 

            $precarga_old = $this->mConsultas->listaCotizacionBusqueda($campo);
        }else{
            if ($campo != "") {
                $precarga = $this->mConsultas->listaPresupuestoEstatusCampo($campo,$estado_refaccion);

                $edo_refaccion = (($estado_refaccion == "2") ? "3" : (($estado_refaccion == "3") ? "2" : $estado_refaccion));
                $precarga_old = $this->mConsultas->listaCotizacionCampoEstatus($campo,$edo_refaccion);

            } elseif ($campo == "") {
                $precarga = $this->mConsultas->listaPresupuestoEstatus($estado_refaccion);

                $edo_refaccion = (($estado_refaccion == "2") ? "3" : (($estado_refaccion == "3") ? "2" : $estado_refaccion));
                $precarga_old = $this->mConsultas->listaCotizacionEstatus($edo_refaccion);

            }else {
                $precarga = $this->mConsultas->listaPresupuestoCampo($campo);

                $precarga_old = $this->mConsultas->listaCotizacionBusqueda($campo);
            }
        }

        $cadena = "";
        $renglon = 0;
        foreach ($precarga as $row) {
            //Verificamos si ya se libero de ventanilla
            if ($row->envia_ventanilla == "0") {
                //Recuperamos los datos
                $cadena .= $row->id_cita."=";
                
                //Identificamos el tipo de presupuesto a mostara
                $cadena .=(((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
                $cadena .= "1"."=";

                $cadena .= $row->envio_garantia."="; // 3
                $cadena .= $row->envia_ventanilla."="; // 4
                $cadena .= $row->estado_refacciones."="; // 5
                $cadena .= $row->estado_refacciones_garantias."="; // 6
                $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
                $cadena .= $row->envio_jdt."="; // 8
                
                $cadena .= $row->firma_requisicion."="; // 9
                $cadena .= $row->firma_requisicion_garantias."="; // 10
                $cadena .= $row->ubicacion_proceso."="; // 11
                $cadena .= $row->acepta_cliente."="; // 12

                $cadena .= $row->serie."=";  //13
                $cadena .= $row->tecnico."=";  //14
                $cadena .= $row->asesor."=";  //15
                $cadena .= $row->vehiculo_modelo."=";  //16
                $cadena .= $row->vehiculo_placas."=";  //17
                
                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $cadena .= "background-color:#f5acaa;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    } elseif ($row->estado_refacciones == "1") {
                        $cadena .= "background-color:#f5ff51;"."=";  // 18
                        $cadena .= "SOLICIATADAS"."=";  // 19
                    } elseif ($row->estado_refacciones == "2") {
                        $cadena .= "background-color:#5bc0de;"."=";  // 18
                        $cadena .= "RECIBIDAS"."=";  // 19
                    }else{
                        $cadena .= "background-color:#c7ecc7;"."=";  // 18
                        $cadena .= "ENTREGADAS"."=";  // 19
                    }
                } else {
                    $cadena .= "background-color:red;color:white;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                }

                $cadena .= $row->folio_externo."="; //20
                $cadena .= $row->solicita_psni."="; //21

                $cadena .= $this->encrypt($row->id_cita)."="; //22
                
            //Si ya se libero de ventanilla, definimos si seran una o dos presupuestos
            }else{
                //if ((int)$row->ref_tradicional > 0) {
                    //Recuperamos los datos
                    $cadena .=  $row->id_cita."=";
                    
                    //Identificamos el tipo de presupuesto a mostara
                    $cadena .=  "TRADICIONAL"."=";
                    $cadena .=  "1"."=";

                    $cadena .= $row->envio_garantia."="; // 3
                    $cadena .= $row->envia_ventanilla."="; // 4
                    $cadena .= $row->estado_refacciones."="; // 5
                    $cadena .= $row->estado_refacciones_garantias."="; // 6
                    $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
                    $cadena .= $row->envio_jdt."="; // 8
                    
                    $cadena .= $row->firma_requisicion."="; // 9
                    $cadena .= $row->firma_requisicion_garantias."="; // 10
                    $cadena .= $row->ubicacion_proceso."="; // 11
                    $cadena .= $row->acepta_cliente."="; // 12

                    $cadena .= $row->serie."=";  //13
                    $cadena .= $row->tecnico."=";  //14
                    $cadena .= $row->asesor."=";  //15
                    $cadena .= $row->vehiculo_modelo."=";  //16
                    $cadena .= $row->vehiculo_placas."=";  //17
                    
                    if ($row->acepta_cliente != "No") {
                        if ($row->estado_refacciones == "0") {
                            $cadena .= "background-color:#f5acaa;"."=";  // 18
                            $cadena .= "SIN SOLICITAR"."=";  // 19
                        } elseif ($row->estado_refacciones == "1") {
                            $cadena .= "background-color:#f5ff51;"."=";  // 18
                            $cadena .= "SOLICIATADAS"."=";  // 19
                        } elseif ($row->estado_refacciones == "2") {
                            $cadena .= "background-color:#5bc0de;"."=";  // 18
                            $cadena .= "RECIBIDAS"."=";  // 19
                        }else{
                            $cadena .= "background-color:#c7ecc7;"."=";  // 18
                            $cadena .= "ENTREGADAS"."=";  // 19
                        }
                    } else {
                        $cadena .= "background-color:red;color:white;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    }
                    $cadena .= $row->folio_externo."="; //20
                    $cadena .= $row->solicita_psni."="; //21
                    $cadena .= $this->encrypt($row->id_cita)."="; //22
                //}
            }

            $cadena .= "|";
            $renglon++;
        }

        if ($renglon < 100) {
            foreach ($precarga_old as $row) {
                //Recuperamos los datos
                $cadena .= $row->idOrden."=";
                
                //Identificamos el tipo de presupuesto a mostara
                $cadena .= "TRADICIONAL *"."=";
                $cadena .= "3"."=";

                $cadena .= $row->envioGarantia."="; // 3
                $cadena .= $row->aprobacionRefaccion."="; // 4
                $cadena .= $row->estado."="; // 5
                $cadena .= "0"."="; // 6
                $cadena .= (($row->firmaAsesor != "") ? "SI" : "NO")."="; // 7
                $cadena .= $row->enviaJDT."="; // 8
                
                $cadena .= ""."="; // 9
                $cadena .= ""."="; // 10
                $cadena .= ""."="; // 11
                $cadena .= $row->aceptoTermino."="; // 12

                $cadena .= ""."=";  //13
                $cadena .= $row->tecnico."=";  //14
                $cadena .= $row->asesor."=";  //15
                $cadena .= $row->vehiculo_modelo."=";  //16
                $cadena .= $row->vehiculo_placas."=";  //17
                
                if ($row->aceptoTermino != "No") {
                    if ($row->estado == "0") {
                        $cadena .= "background-color:#f5acaa;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    } elseif ($row->estado == "1") {
                        $cadena .= "background-color:#f5ff51;"."=";  // 18
                        $cadena .= "SOLICIATADAS"."=";  // 19
                    } elseif ($row->estado == "3") {
                        $cadena .= "background-color:#5bc0de;"."=";  // 18
                        $cadena .= "RECIBIDAS"."=";  // 19
                    }else{
                        $cadena .= "background-color:#c7ecc7;"."=";  // 18
                        $cadena .= "ENTREGADAS"."=";  // 19
                    }
                } else {
                    $cadena .= "background-color:red;color:white;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                }

                $cadena .= $row->folioIntelisis."="; //20
                $cadena .= $row->solicita_psni."="; //21

                $cadena .= $this->encrypt($row->idOrden)."="; //22

                $cadena .= "|";
            } 
        }

        echo $cadena;
    }

    //Busqueda de presupuestos global
    public function filtroCotizacionesGeneral_Temp()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $campo = $_POST['campo'];

        //Si no se envio un campo de consulta
        if ($campo == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->busquedaPresupuestoFecha($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->busquedaPresupuestoFechas_1($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) { 
                $precarga = $this->mConsultas->busquedaPresupuestoCampo($campo);
            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha($campo,$fecha_inicio,$fecha_fin);
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_1($campo,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_1($campo,$fecha_fin);
                }
            }
        }

        $cadena = "";

        $id_cita = [];
        foreach ($precarga as $row) {
            $id_cita[] = $row->id_cita;
            //Recuperamos los datos
            $cadena .= $row->id_cita."=";
            
            //Identificamos el tipo de presupuesto a mostara
            $cadena .= (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
            $cadena .= (((int)$row->ref_tradicional > 0) ? "1" : "0")."=";

            $cadena .= $row->envio_garantia."="; // 3
            $cadena .= $row->envia_ventanilla."="; // 4
            $cadena .= $row->estado_refacciones."="; // 5
            $cadena .= $row->estado_refacciones_garantias."="; // 6
            $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
            $cadena .= $row->envio_jdt."="; // 8
            
            $cadena .= $row->firma_requisicion."="; // 9
            $cadena .= $row->firma_requisicion_garantias."="; // 10
            $cadena .= $row->ubicacion_proceso."="; // 11
            $cadena .= $row->acepta_cliente."="; // 12

            $cadena .= $row->serie."=";  //13
            $cadena .= $row->tecnico."=";  //14
            $cadena .= $row->asesor."=";  //15
            $cadena .= $row->vehiculo_modelo."=";  //16
            $cadena .= $row->vehiculo_placas."=";  //17
            
            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $cadena .= "background-color:#f5acaa;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                } elseif ($row->estado_refacciones == "1") {
                    $cadena .= "background-color:#f5ff51;"."=";  // 18
                    $cadena .= "SOLICIATADAS"."=";  // 19
                } elseif ($row->estado_refacciones == "2") {
                    $cadena .= "background-color:#5bc0de;"."=";  // 18
                    $cadena .= "RECIBIDAS"."=";  // 19
                }else{
                    $cadena .= "background-color:#c7ecc7;"."=";  // 18
                    $cadena .= "ENTREGADAS"."=";  // 19
                }
            } else {
                $cadena .= "background-color:red;color:white;"."=";  // 18
                $cadena .= "SIN SOLICITAR"."=";  // 19
            }

            $cadena .= (((int)$row->ref_garantias > 0) ? "1" : "0")."="; //20
            $cadena .= "="; //21
            $cadena .= $row->folio_externo."="; //22
            $cadena .= $row->solicita_psni."="; //23

            $cadena .= $this->encrypt($row->id_cita)."="; //24

            $cadena .= "|";
        }

        $precarga = $this->mConsultas->listaCotizacionBusqueda($campo);

        foreach ($precarga as $row) {
            if (!in_array($row->idOrden,$id_cita)) {
                //Recuperamos los datos
                $cadena .= $row->idOrden."=";
                
                //Identificamos el tipo de presupuesto a mostara
                $cadena .= "TRADICIONAL *"."=";
                $cadena .= "3"."=";

                $cadena .= $row->envioGarantia."="; // 3
                $cadena .= $row->aprobacionRefaccion."="; // 4
                $cadena .= $row->estado."="; // 5
                $cadena .= "0"."="; // 6
                $cadena .= (($row->firmaAsesor != "") ? "SI" : "NO")."="; // 7
                $cadena .= $row->enviaJDT."="; // 8
                
                $cadena .= ""."="; // 9
                $cadena .= ""."="; // 10
                $cadena .= ""."="; // 11
                $cadena .= $row->aceptoTermino."="; // 12

                $cadena .= ""."=";  //13
                $cadena .= $row->tecnico."=";  //14
                $cadena .= $row->asesor."=";  //15
                $cadena .= $row->vehiculo_modelo."=";  //16
                $cadena .= $row->vehiculo_placas."=";  //17
                
                if ($row->aceptoTermino != "No") {
                    if ($row->estado == "0") {
                        $cadena .= "background-color:#f5acaa;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    } elseif ($row->estado == "1") {
                        $cadena .= "background-color:#f5ff51;"."=";  // 18
                        $cadena .= "SOLICIATADAS"."=";  // 19
                    } elseif ($row->estado == "3") {
                        $cadena .= "background-color:#5bc0de;"."=";  // 18
                        $cadena .= "RECIBIDAS"."=";  // 19
                    }else{
                        $cadena .= "background-color:#c7ecc7;"."=";  // 18
                        $cadena .= "ENTREGADAS"."=";  // 19
                    }
                } else {
                    $cadena .= "background-color:red;color:white;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                }

                $cadena .= "3"."="; //20
                $cadena .= (($row->pzaGarantia != "") ? "1" : "0")."="; //21

                $cadena .= $row->folioIntelisis."="; //22
                $cadena .= $row->solicita_psni."="; //23

                $cadena .= $this->encrypt($row->idOrden)."="; //24

                $cadena .= "|";
            }
        }

        echo $cadena;
    }

    //Busqueda de presupuestos del tecnico
    public function filtroCotizacionesTec_Temp()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $campo = $_POST['campo'];

        //Si no se envio un campo de consulta
        if ($campo == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->busquedaPresupuestoFecha_Tec($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->busquedaPresupuestoFechas_1_Tec($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->busquedaPresupuestoCampo_Tec($campo);
            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_Tec($campo,$fecha_inicio,$fecha_fin);
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_1_Tec($campo,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->busquedaPresupuestoCampo_Fecha_1_Tec($campo,$fecha_fin);
                }
            }
        }

        //Verificamos el id del tecnico
        if ($this->session->userdata('rolIniciado')) {
            if (($this->session->userdata('rolIniciado') == "TEC")&&(!in_array($this->session->userdata('usuario'), $this->config->item('usuarios_admin'))) ) {
                $idTecnico = $this->session->userdata('idUsuario'); 
            }else{
                $idTecnico = "";
            }
        }else{
            $idTecnico = "";
        } 

        $cadena = "";

        foreach ($precarga as $row) {
            //Verificamos si es un usuario general cargamos todos los datos
            if ($idTecnico == "") {
                //Recuperamos los datos
                $cadena .= $row->id_cita."=";
                
                //Identificamos el tipo de presupuesto a mostara
                $cadena .= (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
                $cadena .= (((int)$row->ref_tradicional > 0) ? "1" : "0")."=";

                $cadena .= $row->envio_garantia."="; // 3
                $cadena .= $row->envia_ventanilla."="; // 4
                $cadena .= $row->estado_refacciones."="; // 5
                $cadena .= $row->estado_refacciones_garantias."="; // 6
                $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
                $cadena .= $row->envio_jdt."="; // 8
                
                $cadena .= $row->firma_requisicion."="; // 9
                $cadena .= $row->firma_requisicion_garantias."="; // 10
                $cadena .= $row->ubicacion_proceso."="; // 11
                $cadena .= $row->acepta_cliente."="; // 12

                $cadena .= $row->serie."=";  //13
                $cadena .= $row->tecnico."=";  //14
                $cadena .= $row->asesor."=";  //15
                $cadena .= $row->vehiculo_modelo."=";  //16
                $cadena .= $row->vehiculo_placas."=";  //17
                
                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $cadena .= "background-color:#f5acaa;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    } elseif ($row->estado_refacciones == "1") {
                        $cadena .= "background-color:#f5ff51;"."=";  // 18
                        $cadena .= "SOLICIATADAS"."=";  // 19
                    } elseif ($row->estado_refacciones == "2") {
                        $cadena .= "background-color:#5bc0de;"."=";  // 18
                        $cadena .= "RECIBIDAS"."=";  // 19
                    }else{
                        $cadena .= "background-color:#c7ecc7;"."=";  // 18
                        $cadena .= "ENTREGADAS"."=";  // 19
                    }
                } else {
                    $cadena .= "background-color:red;color:white;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                }

                $cadena .= (((int)$row->ref_garantias > 0) ? "1" : "0")."="; //20

                $cadena .= $row->folio_externo."="; //21
                $cadena .= $row->solicita_psni."="; //22

                $cadena .= $this->encrypt($row->id_cita)."="; //23

                $cadena .= "|";

            //De lo contrario solo cargamos los datos del tecnico
            }else{
                //Verificamos que el id del tecnico coincida con los de los resutados recuperados
                if ($idTecnico == $row->id_tecnico) {
                    //Recuperamos los datos
                    $cadena .= $row->id_cita."=";
                    
                    //Identificamos el tipo de presupuesto a mostara
                    $cadena .= (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
                    $cadena .= (((int)$row->ref_tradicional > 0) ? "1" : "0")."=";

                    $cadena .= $row->envio_garantia."="; // 3
                    $cadena .= $row->envia_ventanilla."="; // 4
                    $cadena .= $row->estado_refacciones."="; // 5
                    $cadena .= $row->estado_refacciones_garantias."="; // 6
                    $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
                    $cadena .= $row->envio_jdt."="; // 8
                    
                    $cadena .= $row->firma_requisicion."="; // 9
                    $cadena .= $row->firma_requisicion_garantias."="; // 10
                    $cadena .= $row->ubicacion_proceso."="; // 11
                    $cadena .= $row->acepta_cliente."="; // 12

                    $cadena .= $row->serie."=";  //13
                    $cadena .= $row->tecnico."=";  //14
                    $cadena .= $row->asesor."=";  //15
                    $cadena .= $row->vehiculo_modelo."=";  //16
                    $cadena .= $row->vehiculo_placas."=";  //17
                    
                    if ($row->acepta_cliente != "No") {
                        if ($row->estado_refacciones == "0") {
                            $cadena .= "background-color:#f5acaa;"."=";  // 18
                            $cadena .= "SIN SOLICITAR"."=";  // 19
                        } elseif ($row->estado_refacciones == "1") {
                            $cadena .= "background-color:#f5ff51;"."=";  // 18
                            $cadena .= "SOLICIATADAS"."=";  // 19
                        } elseif ($row->estado_refacciones == "2") {
                            $cadena .= "background-color:#5bc0de;"."=";  // 18
                            $cadena .= "RECIBIDAS"."=";  // 19
                        }else{
                            $cadena .= "background-color:#c7ecc7;"."=";  // 18
                            $cadena .= "ENTREGADAS"."=";  // 19
                        }
                    } else {
                        $cadena .= "background-color:red;color:white;"."=";  // 18
                        $cadena .= "SIN SOLICITAR"."=";  // 19
                    }

                    $cadena .= (((int)$row->ref_garantias > 0) ? "1" : "0")."="; //20

                    $cadena .= $row->folio_externo."="; //21
                    $cadena .= $row->solicita_psni."="; //22

                    $cadena .= $this->encrypt($row->id_cita)."="; //23

                    $cadena .= "|";
                }
            }
        }

        $precarga = $this->mConsultas->listaCotizacionBusqueda($campo);

        foreach ($precarga as $row) {
            //Recuperamos los datos
            $cadena .= $row->idOrden."=";
            
            //Identificamos el tipo de presupuesto a mostara
            $cadena .= "TRADICIONAL *"."=";
            $cadena .= "3"."=";

            $cadena .= $row->envioGarantia."="; // 3
            $cadena .= $row->aprobacionRefaccion."="; // 4
            $cadena .= $row->estado."="; // 5
            $cadena .= "0"."="; // 6
            $cadena .= (($row->firmaAsesor != "") ? "SI" : "NO")."="; // 7
            $cadena .= $row->enviaJDT."="; // 8
            
            $cadena .= ""."="; // 9
            $cadena .= ""."="; // 10
            $cadena .= ""."="; // 11
            $cadena .= $row->aceptoTermino."="; // 12

            $cadena .= ""."=";  //13
            $cadena .= $row->tecnico."=";  //14
            $cadena .= $row->asesor."=";  //15
            $cadena .= $row->vehiculo_modelo."=";  //16
            $cadena .= $row->vehiculo_placas."=";  //17
            
            if ($row->aceptoTermino != "No") {
                if ($row->estado == "0") {
                    $cadena .= "background-color:#f5acaa;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                } elseif ($row->estado == "1") {
                    $cadena .= "background-color:#f5ff51;"."=";  // 18
                    $cadena .= "SOLICIATADAS"."=";  // 19
                } elseif ($row->estado == "3") {
                    $cadena .= "background-color:#5bc0de;"."=";  // 18
                    $cadena .= "RECIBIDAS"."=";  // 19
                }else{
                    $cadena .= "background-color:#c7ecc7;"."=";  // 18
                    $cadena .= "ENTREGADAS"."=";  // 19
                }
            } else {
                $cadena .= "background-color:red;color:white;"."=";  // 18
                $cadena .= "SIN SOLICITAR"."=";  // 19
            }

            $cadena .= "1"."="; //20

            $cadena .= $row->folioIntelisis."="; // 21
            $cadena .= $row->solicita_psni."="; //22
            $cadena .= $this->encrypt($row->idOrden)."="; //23

            $cadena .= "|";
        }

        echo $cadena;
    }

    public function filtroPresupuestosVentanilla_Estatus()
    {
        $estado_refaccion = $_POST['estado_refaccion'];
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];

        //Si no se envio un campo de consulta
        if ($estado_refaccion == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->PresupuestoFechaVentanilla($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->PresupuestoFechas_1Ventanilla($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->listaPresupuestoEstatus($estado_refaccion);

                //$edo_refaccion = (($estado_refaccion == "2") ? "3" : (($estado_refaccion == "3") ? "2" : $estado_refaccion));
                //$precarga_old = $this->mConsultas->listaCotizacionEstatus($edo_refaccion);

            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->PresupuestoFechaVentanilla_Edo_1($estado_refaccion,$fecha_inicio,$fecha_fin);
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->PresupuestoFechasVentanilla_Edo($estado_refaccion,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->PresupuestoFechasVentanilla_Edo($estado_refaccion,$fecha_fin);
                }
            }
        }

        $cadena = "";
        $renglon = 0;
        foreach ($precarga as $row) {
            $cadena .= $row->id_cita."=";
                
            //Identificamos el tipo de presupuesto a mostara
            $cadena .=(((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL")."=";
            $cadena .= "1"."=";

            $cadena .= $row->envio_garantia."="; // 3
            $cadena .= $row->envia_ventanilla."="; // 4
            $cadena .= $row->estado_refacciones."="; // 5
            $cadena .= $row->estado_refacciones_garantias."="; // 6
            $cadena .= (($row->firma_asesor != "") ? "SI" : "NO")."="; // 7
            $cadena .= $row->envio_jdt."="; // 8
            
            $cadena .= $row->firma_requisicion."="; // 9
            $cadena .= $row->firma_requisicion_garantias."="; // 10
            $cadena .= $row->ubicacion_proceso."="; // 11
            $cadena .= $row->acepta_cliente."="; // 12

            $cadena .= $row->serie."=";  //13
            $cadena .= $row->tecnico."=";  //14
            $cadena .= $row->asesor."=";  //15
            $cadena .= $row->vehiculo_modelo."=";  //16
            $cadena .= $row->vehiculo_placas."=";  //17
            
            if (($row->acepta_cliente != "No")&&($row->acepta_cliente != "")) {
                if ($row->estado_refacciones == "0") {
                    $cadena .= "background-color:#f5acaa;"."=";  // 18
                    $cadena .= "SIN SOLICITAR"."=";  // 19
                } elseif ($row->estado_refacciones == "1") {
                    $cadena .= "background-color:#f5ff51;"."=";  // 18
                    $cadena .= "SOLICIATADAS"."=";  // 19
                } elseif ($row->estado_refacciones == "2") {
                    $cadena .= "background-color:#5bc0de;"."=";  // 18
                    $cadena .= "RECIBIDAS"."=";  // 19
                }else{
                    $cadena .= "background-color:#c7ecc7;"."=";  // 18
                    $cadena .= "ENTREGADAS"."=";  // 19
                }
            } else {
                $cadena .= "background-color:red;color:white;"."=";  // 18
                $cadena .= "SIN SOLICITAR"."=";  // 19
            }

            $cadena .= $row->folio_externo."="; //20
            $cadena .= $row->solicita_psni."="; //21

            $cadena .= $this->encrypt($row->id_cita)."="; //22

            $cadena .= "|";
            $renglon++;
        }

        echo $cadena;
    }

    public function descargar_filtro($campos = "..",$datos = NULL)
    {
        $valores = explode(".", $campos);

        $estado_refaccion = $valores[0];
        $fecha_inicio = $valores[1];
        $fecha_fin = $valores[2];

        $renglon = 0;
        
        //Si no se envio un campo de consulta
        if ($estado_refaccion == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->PresupuestoFechaVentanilla($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->PresupuestoFechas_1Ventanilla($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->listaPresupuestoEstatus($estado_refaccion);

                //$edo_refaccion = (($estado_refaccion == "2") ? "3" : (($estado_refaccion == "3") ? "2" : $estado_refaccion));
                //$precarga_old = $this->mConsultas->listaCotizacionEstatus($edo_refaccion);

            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->PresupuestoFechaVentanilla_Edo_1($estado_refaccion,$fecha_inicio,$fecha_fin);
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->PresupuestoFechasVentanilla_Edo($estado_refaccion,$fecha_inicio);
                } else {
                    $precarga = $this->mConsultas->PresupuestoFechasVentanilla_Edo($estado_refaccion,$fecha_fin);
                }
            }
        }

        $datos["contenido"] = [];
        foreach ($precarga as $row) {
            if ($row->estado_refacciones == "0") {
                $color = "f5acaa";
                $edo_refaccion = "SIN SOLICITAR";
            } elseif ($row->estado_refacciones == "1") {
                $color = "f5ff51";
                $edo_refaccion = "SOLICIATADAS";
            } elseif ($row->estado_refacciones == "2") {
                $color = "5bc0de";
                $edo_refaccion = "RECIBIDAS";
            }else{
                $color = "c7ecc7";
                $edo_refaccion = "ENTREGADAS";
            }

            if ($row->nombre_compania != "") {
                $cliente = $row->nombre_compania;
            }else{
                $cliente = $row->datos_nombres ." ".$row->datos_apellido_paterno ." ".$row->datos_apellido_materno;
            }

            $datos["contenido"][] = array(
                'id_cita' => $row->id_cita,
                'folio' => $row->folio_externo,
                'acepta_cliente' => (($row->acepta_cliente == "Si") ? "Totalmente" : "Pacialmente"),
                'serie' => $row->serie,
                'cliente' => $this->quitar_tildes($cliente),
                'tecnico' => $this->quitar_tildes($row->tecnico),
                'asesor' => $this->quitar_tildes($row->asesor),
                'vehiculo_modelo' => $row->vehiculo_modelo,
                'vehiculo_placas' => $row->vehiculo_placas,
                'color' => $color,
                'status_refaccion' => $edo_refaccion,
            );
        }

        //Comprobamos que hayan resultados 
        if (count($datos["contenido"])>0) {
            //Recuperamos las refacciones individuales de la refaccion
            for ($i=0; $i < count($datos["contenido"]) ; $i++) { 
                $refacciones = $this->mConsultas->refacciones_ventanilla($datos["contenido"][$i]["id_cita"]);
                if ($refacciones != NULL) {
                    foreach ($refacciones as $row) {
                        if ($row->estado_entrega == "0") {
                            $color = "f5acaa";
                            $edo_refaccion = "SIN SOLICITAR";
                        } elseif ($row->estado_entrega == "1") {
                            $color = "f5ff51";
                            $edo_refaccion = "SOLICIATADAS";
                        } elseif ($row->estado_entrega == "2") {
                            $color = "5bc0de";
                            $edo_refaccion = "RECIBIDAS";
                        } elseif ($row->estado_entrega == "3") {
                            $color = "c7ecc7";
                            $edo_refaccion = "ENTREGADAS";
                        }

                        $datos["contenido"][$i]["refacciones"][] = array(
                            'id_cita' => $row->id_cita,
                            'cantidad' => $row->cantidad,
                            'descripcion' => $this->quitar_tildes($row->descripcion),
                            'num_pieza' => $row->num_pieza,
                            'color' => $color,
                            'status_refaccion' => $edo_refaccion,
                        );
                    }
                }else{
                    $datos["contenido"][$i]["refacciones"] = [];
                }
            }

            // create file name
            $fileName = 'reporte-'.date("YmdHi").'.xlsx';  
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            // set Header
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'No Orden');
            $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Folio');
            $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Cliente');
            $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Asesor');
            $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Tecnico');
            $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Vehiculo');
            $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Serie');
            $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Placas');
            $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Estatus. Presupuesto');
            // set Row
            $indice = 2;
            for ($i=0; $i < count($datos["contenido"]) ; $i++) { 
                if (count($datos["contenido"][$i]["refacciones"]) > 0) {
                    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$indice, $datos["contenido"][$i]['id_cita']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$indice, $datos["contenido"][$i]['folio']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$indice, $datos["contenido"][$i]['cliente']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$indice, $datos["contenido"][$i]['tecnico']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$indice, $datos["contenido"][$i]['asesor']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$indice, $datos["contenido"][$i]['vehiculo_modelo']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('G'.$indice, $datos["contenido"][$i]['serie']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('H'.$indice, $datos["contenido"][$i]['vehiculo_placas']);
                    $objPHPExcel->getActiveSheet()->SetCellValue('I'.$indice, $datos["contenido"][$i]['status_refaccion']);

                    $indice++;
                    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$indice, 'Cantidad');
                    $objPHPExcel->getActiveSheet()->SetCellValue('C'.$indice, 'Descripcion');
                    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$indice, 'No. Pieza');
                    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$indice, 'Estatus Individual');

                    $indice++;
                    for ($j=0; $j < count($datos["contenido"][$i]["refacciones"]); $j++) { 
                        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$indice, $datos["contenido"][$i]["refacciones"][$j]['cantidad']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$indice, $datos["contenido"][$i]["refacciones"][$j]['descripcion']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$indice, $datos["contenido"][$i]["refacciones"][$j]['num_pieza']);
                        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$indice, $datos["contenido"][$i]["refacciones"][$j]['status_refaccion']);
                        $indice++;
                    }
                }               
            } 
            
            $filename = "reporte_". date("Y-m-d-H-i-s").".csv"; //xlsx
            header('Content-Type: application/vnd.ms-excel'); 
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            header('Cache-Control: max-age=0'); 

            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');
             
            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');  
            //$objWriter->setOffice2003Compatibility(true);
            $objWriter->save('php://output'); 

        }
    }

    //Quitar acentos
    public function quitar_tildes($cadena = "")
    {
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E");
        $texto = str_replace($no_permitidas, $permitidas ,$cadena);
        return $texto;
    }

/*********************************************************************************************************************/
/*********************************************************************************************************************/
/*********************************************************************************************************************/

    public function buscador_asesor() 
    {
        $campo = $_POST["campo"];
        $fecha_ini = $_POST["fecha_inio"];
        $fecha_fin = $_POST["fecha_fin"];

        if (($fecha_ini == "")&&($fecha_fin == "")) {
            $busqueda = $this->mConsultas->presupuestosAsesor_table_campo($campo);
        }else{
            if (($fecha_ini != "")&&($fecha_fin != "")) {
                $busqueda = $this->mConsultas->presupuestosAsesor_tabl_fecha_2($fecha_ini,$fecha_fin,$campo);
            }else {
                if ($fecha_ini != "") {
                    $busqueda = $this->mConsultas->presupuestosAsesor_tabl_fecha($fecha_ini,$campo);
                }else{
                    $busqueda = $this->mConsultas->presupuestosAsesor_tabl_fecha($fecha_fin,$campo);
                }
            }
        }

        $contenedor = [];
        foreach ($busqueda as $row) {
            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $fondo = "background-color:#f5acaa;";
                    $color = "SIN SOLICITAR";
                } elseif ($row->estado_refacciones == "1") {
                    $fondo = "background-color:#f5ff51;";
                    $color = "SOLICIATADAS";
                } elseif ($row->estado_refacciones == "2") {
                    $fondo = "background-color:#5bc0de;";
                    $color = "RECIBIDAS";
                }else{
                    $fondo = "background-color:#c7ecc7;";
                    $color = "ENTREGADAS";
                }
            } else {
                $fondo = "background-color:red;color:white;";
                $color = "RECHAZADA";
            }

            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

            $contenedor[] = array(
                "tecnico" => $row->tecnico,
                "vehiculo" => $row->vehiculo,
                "placas" => $row->placas,
                "asesor" => $row->asesor,
                "id_cita" => $row->id_cita,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "ref_garantias" => $row->ref_garantias,
                "acepta_cliente" => $row->acepta_cliente,
                "envio_jdt" => $row->envio_jdt,
                "serie" => $row->serie,
                "estado_refacciones" => $row->estado_refacciones,
                "color_bg" => $fondo,
                "edo_entrega" => $color,
                "firma_asesor" => $row->firma_asesor,
                "fecha_recepcion" => $row->fecha_recepcion,
                "fecha_formato" => $fecha->format('d-m-Y'),
                "folio_intelisis" => $row->folio_externo,
            );
        }

        //var_dump($busqueda);
        echo json_encode( $contenedor );
    }

    public function buscador_jefetaller()
    {
        $campo = $_POST["campo"];
        $fecha_ini = $_POST["fecha_inio"];
        $fecha_fin = $_POST["fecha_fin"];

        if (($fecha_ini == "")&&($fecha_fin == "")) {
            $busqueda = $this->mConsultas->presupuestosjdt_table_campo($campo);
        }else{
            if (($fecha_ini != "")&&($fecha_fin != "")) {
                $busqueda = $this->mConsultas->presupuestosjdt_tabl_fecha_2($fecha_ini,$fecha_fin,$campo);
            }else {
                if ($fecha_ini != "") {
                    $busqueda = $this->mConsultas->presupuestosjdt_tabl_fecha($fecha_ini,$campo);
                }else{
                    $busqueda = $this->mConsultas->presupuestosjdt_tabl_fecha($fecha_fin,$campo);
                }
            }
        }

        $contenedor = [];
        foreach ($busqueda as $row) {
            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $fondo = "background-color:#f5acaa;";
                    $color = "SIN SOLICITAR";
                } elseif ($row->estado_refacciones == "1") {
                    $fondo = "background-color:#f5ff51;";
                    $color = "SOLICIATADAS";
                } elseif ($row->estado_refacciones == "2") {
                    $fondo = "background-color:#5bc0de;";
                    $color = "RECIBIDAS";
                }else{
                    $fondo = "background-color:#c7ecc7;";
                    $color = "ENTREGADAS";
                }
            } else {
                $fondo = "background-color:red;color:white;";
                $color = "RECHAZADA";
            }

            $contenedor[] = array(
                "tecnico" => $row->tecnico,
                "vehiculo" => $row->vehiculo,
                "placas" => $row->placas,
                "asesor" => $row->asesor,
                "id_cita" => $row->id_cita,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "ref_garantias" => $row->ref_garantias,
                "acepta_cliente" => $row->acepta_cliente,
                "envio_jdt" => $row->envio_jdt,
                "serie" => $row->serie,
                "estado_refacciones" => $row->estado_refacciones,
                "color_bg" => $fondo,
                "edo_entrega" => $color,
                "firma_asesor" => $row->firma_asesor,
                "fecha_recepcion" => $row->fecha_recepcion,
                "fecha_formato" => $fecha->format('d-m-Y'),
                "tipo_presupuesto" => (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL"),
                "folio_intelisis" => $row->folio_externo,
            );
        }

        //var_dump($busqueda);
        echo json_encode( $contenedor );
    }

    public function buscador_garantias() 
    {
        $campo = $_POST["campo"];
        $fecha_ini = $_POST["fecha_inio"];
        $fecha_fin = $_POST["fecha_fin"];

        if (($fecha_ini == "")&&($fecha_fin == "")) {
            $busqueda = $this->mConsultas->presupuestosGarantias_table_campo($campo);
        }else{
            if (($fecha_ini != "")&&($fecha_fin != "")) {
                $busqueda = $this->mConsultas->presupuestosGarantias_tabl_fecha_2($fecha_ini,$fecha_fin,$campo);
            }else {
                if ($fecha_ini != "") {
                    $busqueda = $this->mConsultas->presupuestosGarantias_tabl_fecha($fecha_ini,$campo);
                }else{
                    $busqueda = $this->mConsultas->presupuestosGarantias_tabl_fecha($fecha_fin,$campo);
                }
            }
        }

        $contenedor = [];
        foreach ($busqueda as $row) {
            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $fondo = "background-color:#f5acaa;";
                    $color = "SIN SOLICITAR";
                } elseif ($row->estado_refacciones == "1") {
                    $fondo = "background-color:#f5ff51;";
                    $color = "SOLICIATADAS";
                } elseif ($row->estado_refacciones == "2") {
                    $fondo = "background-color:#5bc0de;";
                    $color = "RECIBIDAS";
                }else{
                    $fondo = "background-color:#c7ecc7;";
                    $color = "ENTREGADAS";
                }
            } else {
                $fondo = "background-color:red;color:white;";
                $color = "RECHAZADA";
            }

            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

            $contenedor[] = array(
                "tecnico" => $row->tecnico,
                "vehiculo" => $row->vehiculo,
                "placas" => $row->placas,
                "asesor" => $row->asesor,
                "id_cita" => $row->id_cita,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "ref_garantias" => $row->ref_garantias,
                "acepta_cliente" => $row->acepta_cliente,
                "envio_garantia" => $row->envio_garantia,
                "serie" => $row->serie,
                "estado_refacciones" => $row->estado_refacciones,
                "color_bg" => $fondo,
                "edo_entrega" => $color,
                "firma_asesor" => $row->firma_asesor,
                "fecha_recepcion" => $row->fecha_recepcion,
                "fecha_formato" => $fecha->format('d-m-Y'),
                "folio_intelisis" => $row->folio_externo,
                "firma_requisicion" => (($row->firma_requisicion != "") ? "1" : "0"),
                "firma_requisicion_garantias" => (($row->firma_requisicion_garantias != "") ? "1" : "0"),
            );
        }

        //var_dump($busqueda);
        echo json_encode( $contenedor );
    }

    public function buscador_tecnicos()
    {
        $campo = $_POST["campo"];
        $fecha_ini = $_POST["fecha_inio"];
        $fecha_fin = $_POST["fecha_fin"];

        if (($fecha_ini == "")&&($fecha_fin == "")) {
            $busqueda = $this->mConsultas->presupuestosTec_table_campo($campo);
        }else{
            if (($fecha_ini != "")&&($fecha_fin != "")) {
                $busqueda = $this->mConsultas->presupuestosTec_tabl_fecha_2($fecha_ini,$fecha_fin,$campo);
            }else {
                if ($fecha_ini != "") {
                    $busqueda = $this->mConsultas->presupuestosTec_tabl_fecha($fecha_ini,$campo);
                }else{
                    $busqueda = $this->mConsultas->presupuestosTec_tabl_fecha($fecha_fin,$campo);
                }
            }
        }

        $contenedor = [];
        foreach ($busqueda as $row) {
            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

            $contenedor[] = array(
                "tecnico" => $row->tecnico,
                "vehiculo" => $row->vehiculo,
                "placas" => $row->placas,
                "asesor" => $row->asesor,
                "id_cita" => $row->id_cita,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "acepta_cliente" => $row->acepta_cliente,
                "ubicacion" => $row->ubicacion_proceso,
                "serie" => $row->serie,
                "estado_refacciones" => $row->estado_refacciones,
                "fecha_recepcion" => $row->fecha_recepcion,
                "fecha_formato" => $fecha->format('d-m-Y'),
                "folio_intelisis" => $row->folio_externo,
                "requisicion" => $row->firma_requisicion,
            );
        }

        //var_dump($busqueda);
        echo json_encode( $contenedor );
    }

    public function buscador_ventanilla()
    {
        $campo = $_POST["campo"];
        $fecha_ini = $_POST["fecha_inio"];
        $fecha_fin = $_POST["fecha_fin"];

        if (($fecha_ini == "")&&($fecha_fin == "")) {
            $busqueda = $this->mConsultas->presupuestosVentanilla_table_campo($campo);
        }else{
            if (($fecha_ini != "")&&($fecha_fin != "")) {
                $busqueda = $this->mConsultas->presupuestosVentanilla_tabl_fecha_2($fecha_ini,$fecha_fin,$campo);
            }else {
                if ($fecha_ini != "") {
                    $busqueda = $this->mConsultas->presupuestosVentanilla_tabl_fecha($fecha_ini,$campo);
                }else{
                    $busqueda = $this->mConsultas->presupuestosVentanilla_tabl_fecha($fecha_fin,$campo);
                }
            }
        }

        $contenedor = [];
        foreach ($busqueda as $row) {
            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $fondo = "background-color:#f5acaa;";
                    $color = "SIN SOLICITAR";
                } elseif ($row->estado_refacciones == "1") {
                    $fondo = "background-color:#f5ff51;";
                    $color = "SOLICIATADAS";
                } elseif ($row->estado_refacciones == "2") {
                    $fondo = "background-color:#5bc0de;";
                    $color = "RECIBIDAS";
                }else{
                    $fondo = "background-color:#c7ecc7;";
                    $color = "ENTREGADAS";
                }
            } else {
                $fondo = "background-color:red;color:white;";
                $color = "RECHAZADA";
            }

            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

            $contenedor[] = array(
                "tecnico" => $row->tecnico,
                "vehiculo" => $row->vehiculo,
                "placas" => $row->placas,
                "asesor" => $row->asesor,
                "id_cita" => $row->id_cita,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "acepta_cliente" => $row->acepta_cliente,
                "envia_ventanilla" => $row->envia_ventanilla,
                "serie" => $row->serie,
                "estado_refacciones" => $row->estado_refacciones,
                "color_bg" => $fondo,
                "edo_entrega" => $color,
                "fecha_recepcion" => $row->fecha_recepcion,
                "fecha_formato" => $fecha->format('d-m-Y'),
                "tipo_presupuesto" => (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL"),
                "requisicion" => $row->firma_requisicion,
                "psni" => $row->solicita_psni,
                "folio_intelisis" => $row->folio_externo,
            );
        }

        //var_dump($busqueda);
        echo json_encode( $contenedor );
    }

    public function buscador_ventanilla_estatus()
    {
        $campo = $_POST["campo"];
        $fecha_ini = $_POST["fecha_inio"];
        $fecha_fin = $_POST["fecha_fin"];
        $status_ref = $_POST["estado"];

        if (($fecha_ini == "")&&($fecha_fin == "")) {
            $busqueda = $this->mConsultas->presupuestosVentanilla_table_campo_status($campo,$status_ref);
        }else{
            if (($fecha_ini != "")&&($fecha_fin != "")) {
                $busqueda = $this->mConsultas->presupuestosVentanilla_tabl_fecha_2_status($fecha_ini,$fecha_fin,$campo,$status_ref);
            }else {
                if ($fecha_ini != "") {
                    $busqueda = $this->mConsultas->presupuestosVentanilla_tabl_fecha_status($fecha_ini,$campo,$status_ref);
                }else{
                    $busqueda = $this->mConsultas->presupuestosVentanilla_tabl_fecha_status($fecha_fin,$campo,$status_ref);
                }
            }
        }

        $contenedor = [];
        foreach ($busqueda as $row) {
            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $fondo = "background-color:#f5acaa;";
                    $color = "SIN SOLICITAR";
                } elseif ($row->estado_refacciones == "1") {
                    $fondo = "background-color:#f5ff51;";
                    $color = "SOLICIATADAS";
                } elseif ($row->estado_refacciones == "2") {
                    $fondo = "background-color:#5bc0de;";
                    $color = "RECIBIDAS";
                }else{
                    $fondo = "background-color:#c7ecc7;";
                    $color = "ENTREGADAS";
                }
            } else {
                $fondo = "background-color:red;color:white;";
                $color = "RECHAZADA";
            }

            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

            $contenedor[] = array(
                "tecnico" => $row->tecnico,
                "vehiculo" => $row->vehiculo,
                "placas" => $row->placas,
                "asesor" => $row->asesor,
                "id_cita" => $row->id_cita,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "acepta_cliente" => $row->acepta_cliente,
                "envia_ventanilla" => $row->envia_ventanilla,
                "serie" => $row->serie,
                "estado_refacciones" => $row->estado_refacciones,
                "color_bg" => $fondo,
                "edo_entrega" => $color,
                "fecha_recepcion" => $row->fecha_recepcion,
                "fecha_formato" => $fecha->format('d-m-Y'),
                "tipo_presupuesto" => (((int)$row->ref_garantias > 0) ? "CON GARANTÍA" : "TRADICIONAL"),
                "requisicion" => $row->firma_requisicion,
                "psni" => $row->solicita_psni,
                "folio_intelisis" => $row->folio_externo,
            );
        }

        //var_dump($busqueda);
        echo json_encode( $contenedor );
    }

    public function buscador_status_cliente() 
    {
        $campo = $_POST["campo"];
        $fecha_ini = $_POST["fecha_inio"];
        $fecha_fin = $_POST["fecha_fin"];


        if ($campo == "") {
            if ($fecha_fin == "") {
                $busqueda = $this->mConsultas->presupuestosJDTStatus_table_f($fecha_ini);
            } elseif ($fecha_ini == "") {
                $busqueda = $this->mConsultas->presupuestosJDTStatus_table_f($fecha_fin);
            } else {
                $busqueda = $this->mConsultas->presupuestosJDTStatus_tabl_fecha($fecha_ini,$fecha_fin);
            }
            
        } else {
            if (($fecha_ini != "")&&($fecha_fin != "")) {
                $busqueda = $this->mConsultas->presupuestosJDTStatus_tabl_fecha_2($fecha_ini,$fecha_fin,$campo);
            } elseif (($fecha_ini == "")&&($fecha_fin == "")) {
                $busqueda = $this->mConsultas->presupuestosJDTStatus_table_campo($campo);
            }else {
                if ($fecha_ini != "") {
                    $busqueda = $this->mConsultas->presupuestosJDTStatus_tabl_fechacampo($fecha_ini,$campo);
                }else{
                    $busqueda = $this->mConsultas->presupuestosJDTStatus_tabl_fechacampo($fecha_fin,$campo);
                }
            }
        }
        

        $contenedor = [];
        foreach ($busqueda as $row) {
            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $fondo = "background-color:#f5acaa;";
                    $color = "SIN SOLICITAR";
                } elseif ($row->estado_refacciones == "1") {
                    $fondo = "background-color:#f5ff51;";
                    $color = "SOLICIATADAS";
                } elseif ($row->estado_refacciones == "2") {
                    $fondo = "background-color:#5bc0de;";
                    $color = "RECIBIDAS";
                }else{
                    $fondo = "background-color:#c7ecc7;";
                    $color = "ENTREGADAS";
                }
            } else {
                $fondo = "background-color:red;color:white;";
                $color = "RECHAZADA";
            }

            $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

            $contenedor[] = array(
                "tecnico" => $row->tecnico,
                "vehiculo" => $row->vehiculo,
                "placas" => $row->placas,
                "asesor" => $row->asesor,
                "id_cita" => $row->id_cita,
                "id_cita_url" => $this->encrypt($row->id_cita),
                "ref_garantias" => $row->ref_garantias,
                "acepta_cliente" => $row->acepta_cliente,
                "envio_jdt" => $row->envio_jdt,
                "serie" => $row->serie,
                "estado_refacciones" => $row->estado_refacciones,
                "color_bg" => $fondo,
                "edo_entrega" => $color,
                "firma_asesor" => $row->firma_asesor,
                "fecha_recepcion" => $row->fecha_recepcion,
                "fecha_formato" => $fecha->format('d-m-Y'),
                "folio_intelisis" => $row->folio_externo,
            );
        }

        //var_dump($busqueda);
        echo json_encode( $contenedor );
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

}