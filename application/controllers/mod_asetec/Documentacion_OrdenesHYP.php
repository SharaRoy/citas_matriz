<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Documentacion_OrdenesHYP extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

    public function listStatusOrderHYP()
    {
        if (!$this->session->userdata('idUsuario')) {
            redirect('Login_AseTecnico', 'refresh');
        }else{
            if ($this->session->userdata('rolIniciado')) {
                $datos["pOrigen"] = $this->session->userdata('rolIniciado');
                //Rosendo internas
                if ($this->session->userdata('idUsuario') == "33") {
                    $tipo_ordenes = "4";
                    $asesor = "ROSENDO MENDOZA OLVERA"; //"13";
                //JESUS HYP
                } elseif ($this->session->userdata('idUsuario') == "34") {
                    $tipo_ordenes = "3";
                    $asesor = "JESUS RESENDIZ RANGEL"; //"14";
                }else{
                    $tipo_ordenes = "";
                    $asesor = "";
                }
            } else{
                $tipo_ordenes = "";
                $asesor = "";
            }

            $fecha_inicio = (date("Y")-1)."-".date("m-d");
            if ($tipo_ordenes == "") {
                $datos["contenido"] = $this->mConsultas->ordenesEstatusHYP_Gral($fecha_inicio);
            }else{
                $datos["contenido"] = $this->mConsultas->ordenesEstatusHYP($fecha_inicio,$asesor);
            }

            if ($this->session->userdata('rolIniciado')) {
                $datos["pOrigen"] = $this->session->userdata('rolIniciado');
            }
            
            $this->loadAllView($datos,'mod_asetec/documentacion/lista_estatus_tablero');
        }
    }

    //Cargamos la nueva forma de listar la documentacion
    public function documentacion_tb_asesortec()
    {
        if (!$this->session->userdata('idUsuario')) {
            redirect('Login_AseTecnico', 'refresh');
        }else{
            $datos["destinoVista"] = "ASETEC";
            $datos["tipo_vista"] = "LISTA";

            if ($this->session->userdata('rolIniciado')) {
                $datos["pOrigen"] = $this->session->userdata('rolIniciado');
                //Rosendo internas
                if ($this->session->userdata('idUsuario') == "33") {
                    $tipo_ordenes = "4";
                    $asesor = "ROSENDO MENDOZA OLVERA"; //"13";
                //JESUS HYP
                } elseif ($this->session->userdata('idUsuario') == "34") {
                    $tipo_ordenes = "3";
                    $asesor = "JESUS RESENDIZ RANGEL"; //"14";
                }else{
                    $tipo_ordenes = "";
                    $asesor = "";
                }
            } else{
                $tipo_ordenes = "";
                $asesor = "";
            }

            $fecha_inicio = (date("Y")-1)."-".date("m-d");
            if ($tipo_ordenes == "") {
                $datos["registros"] = $this->mConsultas->doc_tb_ordenes_body($fecha_inicio);
            }else{
                $datos["registros"] = $this->mConsultas->doc_tb_ordenes_body2($fecha_inicio,$asesor);
            }

            $this->loadAllView($datos,'mod_asetec/documentacion/documentos_asetec');
        }   
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        $archivosJs = array(
            "include" => array(
            
            )
        );
        
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Documentación de Ordenes HBody"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}