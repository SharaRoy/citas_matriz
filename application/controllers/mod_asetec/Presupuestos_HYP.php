<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class Presupuestos_HYP extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'consulta', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL); 
    }

    public function comodinAltaHYP($id_cita_url = "",$id_user = '',$datos = NULL)
    {
        //if (!$this->session->userdata('idUsuario')) {
            //redirect('Login_AseTecnico', 'refresh');
        //} else {
            $id_cita = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
            $datos["dirige"] = "ASETEC";
            $datos["usuariosPermitidos"] = [
                //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
                '148',
                //Ángel Salas calderón  ID = 149 Usuario = angel
                '149',
                //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
                '150',
                //Shara Sahori   ID = 151 Usuario = shara
                '151',
            ];

            //Recuperamos el tipo de orden para ver si es valida
            $datos["tipos_orden_permitido"] = [3,4];
            $tipo_orden = $this->consulta->recuperar_id_orden($id_cita);

            if (isset($tipo_orden[1])) {
                $datos["tipo_orden"] = $tipo_orden[1];

                if (in_array($id_user, $datos["usuariosPermitidos"])) {
                    //borramos la sesion para reescribirla
                    if ($this->session->userdata('rolIniciado')) {
                        $this->session->sess_destroy();
                    }

                    $infor_user = $this->Verificacion->dataAsesor($id_user);

                    $row = $infor_user->row();
                    if (isset($row)){
                        $idUsuario = $infor_user->row(0)->adminId;
                        $nombre = $infor_user->row(0)->adminNombre;
                        $usuario = $infor_user->row(0)->adminUsername;
                    }

                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                        "nombreUsuario" => strtoupper($nombre),
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);

                    //Confirmamos que se hayan guardado los datos
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    $datos["usuario"] = $this->session->userdata('idUsuario');
                } else {
                    $datos["usuario"] = "";
                }
            } else {
                $datos["tipo_orden"] = "0";
                $datos["usuario"] = "";
            }

            $datos['id_cita'] = $id_cita;
            $datos['id_cita_url'] = $this->encrypt($id_cita);

            //Consultamos su ya existe un presupuesto
            $revision = $this->consulta->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
            foreach ($revision as $row) {
                $idCotizacion = $row->id;
                $acepta_cliente = $row->acepta_cliente;
                $identificador = $row->identificador;

                if ($identificador == "M") {
                    if ($this->session->userdata('rolIniciado')) {
                        try {
                            $this->session->sess_destroy();   
                        } catch (Exception $e) {
                            
                        }
                    }
                }
            }

            //Si ya existe un presupuesto, solo lo visualizamos
            if (isset($idCotizacion)) {
                redirect('Alta_Presupuesto/'.$datos['id_cita_url'], 'refresh');
                    
            //De lo contratio, si no existe, vamos a la vista de alta
            }else{
                $datos["vista"] = "6R";
                
                //Recuperamos los datos de cabecera
                $informacion_gral = $this->consulta->orden_completa($id_cita);
                foreach ($informacion_gral as $row){
                    $datos["vehiculo"] = $row->vehiculo_modelo;
                    $datos["placas"] = $row->vehiculo_placas;
                    $datos["modelo"] = $row->vehiculo_anio;
                    $datos["serie"] = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
                    $datos["asesor"] = $row->asesor;
                    $datos["folio"] = $row->folioIntelisis;
                    $datos["tecnico"] = $row->tecnico;
                    $datos["cliente"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
                }
                //Recuperamos el tipo de orden
                $this->loadAllView($datos,'mod_asetec/presupuesto/presupuesto_alta_hyp');
            }
        //}    
    }

    //Parada comodin para listar presupuestos de del panel de interana y hyp (asesor)
    public function comodinListaAsestec($vistaDestino = "",$datos = NULL)
    {
        $datos["destino"] = "ASESOR";

        $fecha_busqueda = (date("Y") - 2)."-".date("m-d");

        if ($this->session->userdata('rolIniciado')) {
            //Rosendo internas
            if ($this->session->userdata('idUsuario') == "33") {
                $asesor = "ROSENDO MENDOZA OLVERA"; //"13";
                //$tipo_ordenes = "4";
                $datos["presupuestos"] = $this->consulta->presupuestosAsesTec_tableHyP($fecha_busqueda,4,$asesor);
            //JESUS HYP
            } elseif ($this->session->userdata('idUsuario') == "34") {
                $asesor = "JESUS RESENDIZ RANGEL"; //"14";
                //$tipo_ordenes = "3";
                $datos["presupuestos"] = $this->consulta->presupuestosAsesTec_tableHyP($fecha_busqueda,3,$asesor);
            }else{
                $datos["presupuestos"] = $this->consulta->presupuestosCAsesTec_table($fecha_busqueda);
            }

            $this->loadAllView($datos,'mod_asetec/presupuesto/buscador_asesorbody'); 
        } else{
            redirect('Panel_AsesorBody', 'refresh');
        }
    }

    //Parada comodin para listar presupuestos de del panel de interana y hyp (tecnico)
    public function comodinListaAsestecCreadas($vistaDestino = "",$datos = NULL)
    {
        $datos["destino"] = "TECNICO";

        $fecha_busqueda = (date("Y") - 2)."-".date("m-d");

        if ($this->session->userdata('rolIniciado')) {
            //Rosendo internas
            if ($this->session->userdata('idUsuario') == "33") {
                //$tipo_ordenes = "4";
                $asesor = "ROSENDO MENDOZA OLVERA"; //"13";
                $datos["presupuestos"] = $this->consulta->presupuestosCAsesTec_tableHyP($fecha_busqueda,4,$asesor);
            //JESUS HYP
            } elseif ($this->session->userdata('idUsuario') == "34") {
                //$tipo_ordenes = "3";
                $asesor = "JESUS RESENDIZ RANGEL"; //"14";
                $datos["presupuestos"] = $this->consulta->presupuestosCAsesTec_tableHyP($fecha_busqueda,3,$asesor);
            }else{
                $datos["presupuestos"] = $this->consulta->presupuestosCAsesTec_table($fecha_busqueda);
            }

            $this->loadAllView($datos,'mod_asetec/presupuesto/buscador_tecnicobody'); 
        } else{
            redirect('Panel_AsesorBody', 'refresh');
        }
    }

    public function guardarRegistroTecAjax_hyp()
    {
        //Verificamos que no llege la informacion vacia
        if ($_POST["idOrdenTemp"] != "") {
            $fecha_registro = date("Y-m-d H:i");

            if (isset($_POST["registros"])) {
                $tipo_orden = $this->consulta->recuperar_id_orden($_POST["idOrdenTemp"]);
                $renglones = $_POST["registros"] ;

                if (isset($_POST["pzaGarantia"])) {
                    $pzas_garantias = $_POST["pzaGarantia"];
                }else {
                    $pzas_garantias = [];
                }

                $ref_tradicionales = 0;
                $ref_garantias = 0;
                $bandera = FALSE;

                //Guardamos los items
                for ($i=0; $i < count($renglones) ; $i++) { 
                    //echo "<br>".$renglones[$i]."<br>";
                    //Evitamos guardar basura
                    if (strlen($renglones[$i])>19) {
                        //Dividimos las celdas
                        $campos = explode("_",$renglones[$i]);

                        $registro = array(
                            "id" => NULL,
                            "id_cita" => $_POST["idOrdenTemp"],
                            "identificador" => "MHYP",
                            "ref" => ((isset($campos[9])) ? $campos[9] : NULL),
                            "id_tipo_orden" => $tipo_orden[1],
                            "cantidad" => $campos[0],
                            "descripcion" => $campos[1],
                            "num_pieza" => $campos[2],
                            "existe" => $campos[3],
                            "fecha_planta" => "0000-00-00",
                            "costo_pieza" => $campos[4],
                            "costo_aseguradora" => ((isset($campos[9])) ? $campos[9] : 0),
                            "horas_mo" => $campos[5],
                            "costo_mo" => $campos[6],
                            "total_refacion" => $campos[7],
                            "pza_garantia" => ((in_array(($i+1),$pzas_garantias)) ? 1 : 0),
                            "costo_ford" => 0,
                            "horas_mo_garantias" => 0,
                            "costo_mo_garantias" => 0,
                            "autoriza_garantia" => 0,
                            "autoriza_jefeTaller" => 1,
                            "autoriza_asesor" => 0,
                            "autoriza_cliente" => 0,
                            "tipo_precio" => ((in_array(($i+1),$pzas_garantias)) ? "G" : "P"),
                            "prioridad" => ((isset($campos[8])) ? $campos[8] : 2),
                            "activo" => 1,
                            "fecha_registro" => date("Y-m-d H:i:s"),
                            "fecha_actualiza" => date("Y-m-d H:i:s")
                        );

                        //Confirmamos que no se haya duplicado
                        $existe_refaccion = $this->consulta->valida_refaccion_multipunto($_POST["idOrdenTemp"],$campos[1],$campos[0]);
                        if ($existe_refaccion == "0") {
                            if ($campos[1] != "") {
                                //Guardamos el registro de la refaccion
                                $id_retorno = $this->consulta->save_register('presupuesto_multipunto',$registro);
                                if ($id_retorno != 0) {
                                    //Si se guardo al menos una refaccion guardamos el registro general
                                    $bandera = TRUE;
                                    //Contabilizamos las refacciones tradicionales y las de garantias
                                    if (!in_array(($i+1),$pzas_garantias)) {
                                        $ref_tradicionales++;
                                    } else {
                                        $ref_garantias++;
                                    }
                                }
                            }
                        }
                        //print_r($registro);
                    }
                }

                //Validamos si se guardara un registro pivote
                if ($bandera) {
                    //Verificamos los archivos generales
                    if($_POST["uploadfiles_resp"] != "") {
                        $archivos_2 = $this->validateDoc("uploadfiles");
                        $archivos_gral = $_POST["uploadfiles_resp"]."".$archivos_2;
                    }else {
                        $archivos_gral = $this->validateDoc("uploadfiles");
                    }

                    //Verificamos archivos del tecnico
                    if($_POST["tempFileTecnico"] != "") {
                        $archivos_3 = $this->validateDoc("uploadfilesTec");
                        $archivos_tec = $_POST["tempFileTecnico"]."".$archivos_3;
                    }else {
                        $archivos_tec = $this->validateDoc("uploadfilesTec");
                    }

                    $destino_presupuesto = $_POST["aceptarCotizacion"];

                    $registro_pivote = array(
                        "id" => NULL,
                        "id_cita" => $_POST["idOrdenTemp"],
                        "folio_externo" => $_POST["folio_externo"],
                        "identificador" => "MHYP",
                        "id_tipo_orden" => $tipo_orden[1],
                        "cliente_paga" => 0,
                        "ref_garantias" => $ref_garantias,
                        "ref_tradicional" => $ref_tradicionales,
                        "ford_paga" => 0,
                        "total_pago_garantia" => 0,
                        "envio_garantia" => 0,
                        "comentario_garantia" => "",
                        "firma_garantia" => "",
                        "anticipo" => 0,
                        "nota_anticipo" => "",
                        "subtotal" => $_POST["subTotalMaterial"],
                        "cancelado" => 0,
                        "iva" => $_POST["ivaMaterial"],
                        "total" => $_POST["totalMaterial"],
                        "ventanilla" => "",
                        "envia_ventanilla" => (($destino_presupuesto != "Enviar Cotización al Asesor") ? "0" : "2"),
                        "estado_refacciones" => 0,
                        "estado_refacciones_garantias" => 0,
                        "archivos_presupuesto" =>$archivos_gral,
                        "archivo_tecnico" => $archivos_tec,
                        "archivo_jdt" => "",
                        "firma_asesor" => "",
                        "comentario_asesor" => "",
                        "envio_jdt" => 0,
                        "firma_jdt" => "",
                        "comentario_jdt" => "",
                        "comentario_ventanilla" => "",
                        "comentario_tecnico" => $_POST["comentarioTecnico"],
                        "serie" => $_POST["noSerie"],
                        "firma_requisicion" => "",
                        "firma_requisicion_garantias" => "",
                        "correo_adicional" => "",
                        "envio_proactivo" => 0,
                        "acepta_cliente" => "",
                        //Si el proceso tiene para del jefe de taller
                        "ubicacion_proceso" => (($destino_presupuesto != "Enviar Cotización al Asesor") ? "Espera revisión de ventanilla" : "Espera revision del Asesor"),
                        //Si el proceso no tiene parada del jefe de taller
                        //"ubicacion_proceso" => (($destino_presupuesto == "Enviar Cotización a Ventanilla") ? "Espera revisión de ventanilla" : "Espera revisión del Asesor"),
                        "ubicacion_proceso_garantia" => (($ref_garantias > 0) ? "Espera revisión de ventanilla" : "Sin proceso"),
                        "fecha_alta" => $fecha_registro,
                        "termina_ventanilla" => $fecha_registro,
                        "termina_jdt" => $fecha_registro,
                        "termina_garantias" => $fecha_registro,
                        "termina_asesor" => $fecha_registro,
                        "termina_cliente" => $fecha_registro,
                        "termina_requisicion" => $fecha_registro,
                        "termina_requisicion_garantias" => $fecha_registro,
                        "solicita_pieza" => $fecha_registro,
                        "recibe_pieza" => $fecha_registro,
                        "entrega_pieza" => $fecha_registro,
                        "solicita_pieza_garantia" => $fecha_registro,
                        "recibe_pieza_garantia" => $fecha_registro,
                        "entrega_pieza_garantia" => $fecha_registro,
                        "termina_proactivo" => $fecha_registro,
                        "fecha_actualiza" => $fecha_registro,
                    );

                    $existe_registro = $this->consulta->valida_refaccion_registro($_POST["idOrdenTemp"]);

                    if ($existe_registro == 0) {
                        //Guardamos el registro de la refaccion
                        $id_retorno = $this->consulta->save_register('presupuesto_registro',$registro_pivote);
                        if ($id_retorno != 0) {
                            //Actualizamos la información para el historial de ordenes
                            $this->creamos_documentacion_sinformatos($_POST["idOrdenTemp"]);

                            //Recuperamos datos extras para la notificación
                            $query = $this->consulta->notificacionTecnicoCita($_POST["idOrdenTemp"]); 
                            foreach ($query as $row) {
                                $tecnico = $row->tecnico;
                                $modelo = $row->vehiculo_modelo;
                                $placas = $row->vehiculo_placas;
                                $asesor = $row->asesor;
                            }

                            //Mandamos la notificacion de la cotizacion creada a ventanilla
                            //if ($destino_presupuesto == "Enviar Cotización a Ventanilla") {
                            if ($destino_presupuesto != "Enviar Cotización al Asesor") {
                                $texto = SUCURSAL.". Nueva solicitud de presupuesto por orden interna/bodyshop creada. No. Orden: ".$_POST["idOrdenTemp"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                                
                                //Ventanilla
                                $this->sms_general('3316721106',$texto);
                                $this->sms_general('3319701092',$texto);
                                $this->sms_general('3317978025',$texto);

                                //Tester
                                //$this->sms_general('3328351116',$texto);
                                //Angel evidencia
                                //$this->sms_general('5535527547',$texto);

                                $movimiento = "Se crea presupuesto por orden interna/bodyshop (Se envia a ventanilla)";

                            //Mandamos la notificacion de la cotizacion creada al  asesor/jefe de talles 
                            }else{
                                //Si el presupuesto tiene piezas con garantias, y no hay jefe de taller, se envia la informacion
                                if ($ref_garantias > 0) {
                                    $texto = SUCURSAL.". Nueva solicitud de presupuesto por orden interna/bodyshop creada (sin pasar por ventanilla) con refacciones con garantías. No. Orden: ".$_POST["idOrdenTemp"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                                    $movimiento = "Se crea presupuesto por orden interna/bodyshop con garantías (Se envia al jefe de taller)";

                                    $recibe = 'Jefe de Taller';
                                }else{
                                    $texto = SUCURSAL.". Nueva solicitud de presupuesto por orden interna/bodyshop creada (sin pasar por ventanilla). No. Orden: ".$_POST["idOrdenTemp"]." el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                                    $movimiento = "Se crea presupuesto por orden interna/bodyshop sin garantías (Se envia al jefe de taller)";

                                    $recibe = 'Ventanilla';
                                }

                                //Jefe de taller
                                //Jesus Gonzales 
                                //$this->sms_general('3316724093',$texto); 
                                //Juan Chavez
                                $this->sms_general('3333017952',$texto);
                                //Gerente de servicio
                                //$this->sms_general('3315273100',$texto);
                                
                                //Tester
                                //$this->sms_general('3328351116',$texto);
                                //Angel evidencia
                                //$this->sms_general('5535527547',$texto)
                                
                                //Grabamos el evento para el panel de gerencia
                                $registro_evento = array(
                                    'envia_departamento' => 'Asesor Tecnico',
                                    'recibe_departamento' => $recibe,
                                    'descripcion' => 'Nuevo presupuesto por orden interna/bodyshop creado',
                                );

                                $this->registroEvento($_POST["idOrdenTemp"],$registro_evento);
                            }

                            $this->registrar_movimiento($_POST["idOrdenTemp"],$movimiento,NULL);

                            $conclusion = "OK=".$this->encrypt($_POST["idOrdenTemp"]);
                        }else{
                            $conclusion = "NO SE GUARDO CORRECTAMENTE EL REGISTRO 01=".$this->encrypt($_POST["idOrdenTemp"]);
                        }
                    } else {
                        $conclusion = "NO SE GUARDO CORRECTAMENTE EL REGISTRO**=".$this->encrypt($_POST["idOrdenTemp"]);
                    }    
                }else{
                    $conclusion = "NO SE GUARDO CORRECTAMENTE EL REGISTRO 02=".$this->encrypt($_POST["idOrdenTemp"]);
                }
            }else {
                $conclusion = "NO SE CARGARON REFACCIONES=".$this->encrypt($_POST["idOrdenTemp"]);
            }
        }else{
            $conclusion = "PRESUPUESTO VACIO=".$this->encrypt($_POST["idOrdenTemp"]);
        }
        echo $conclusion;
    }

    public function creamos_documentacion_sinformatos($id_cita = '')
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->consulta->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "presupuesto" => "1",
                "presupuesto_origen" => "1",
            );

            //Guardamos el registro
            $actualiza = $this->consulta->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }else{
            $campos = $this->consulta->recuperar_documentaciones_cita($id_cita);
            
            foreach ($campos as $row) {
                $documentacion_nvo = array(
                    "id" => NULL,
                    "id_cita" => $row->orden_cita,
                    "folio_externo" => $row->folioIntelisis,
                    "tipo_vita" => 2,
                    "fecha_recepcion" => $row->fecha_recepcion,
                    "vehiculo" => $row->vehiculo_modelo,
                    "placas" => $row->vehiculo_placas,
                    "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                    "asesor" => $row->asesor,
                    "tecnico" => $row->tecnico,
                    "id_tecnico" => $row->id_tecnico,
                    "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                    "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                    "oasis" => "",
                    "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                    "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                    "presupuesto" => 1,
                    "presupuesto_afectado" => "",
                    "presupuesto_origen" => 1,
                    "voz_cliente" => (($row->idVoz != "") ? "1" : "0"),
                    "evidencia_voz_cliente" => ((file_exists($row->evidencia)) ? $row->evidencia : ""),
                    "servicio_valet" => "0",
                    "fechas_creacion" => date("Y-m-d H:s:i"),
                );

                $this->consulta->save_register('documentacion_ordenes',$documentacion_nvo);
            }
        }
    }

    public function registrar_movimiento($id_cita='',$movimiento = '',$origen = NULL)
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Técnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }elseif ($this->session->userdata('rolIniciado') == "ASETEC") {
                $panel = "Panel Asesor Técnico";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = (($origen != NULL) ? $origen : "Presupuestos");
            $id_usuario =  "";
            $usuario = "";
        }

        $historico = array(
            'id' => NULL,
            'id_cita' => $id_cita,
            'origen_movimiento' => $panel,
            'movimiento' => $movimiento,
            'id_usuario' => $id_usuario,
            'usuario' => $usuario,
            'fecha_registro' => $registro,
        );

        $this->consulta->save_register('presupuestos_historial',$historico);

        return TRUE;
    }

    public function registroEvento($id_cita = 0, $datos = NULL)
    {
        //Recuperamos los datos del usuario
        if ($this->session->userdata('rolIniciado')) {
            $envia_id_usuario = $this->session->userdata('idUsuario');
            $envia_nombre_usuario = $this->session->userdata('nombreUsuario');

            if ($this->session->userdata('rolIniciado') == "TEC") {
                $revision = $this->consulta->get_result("id",$envia_id_usuario,"tecnicos");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->nombre);
                }
            } else {
                $revision = $this->Verificacion->get_result("adminId",$envia_id_usuario,"admin");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->adminNombre);
                }
            }
        }else{
            $envia_id_usuario = "0";//$datos["idUsuario"];
            $envia_nombre_usuario = "Usuario";//$datos["nombre"];
        }

        //$envia_departamento = $datos["dpo"];
        $sucursal = BIN_SUCURSAL;
        $envia_fecha_hora = date("Y-m-d H:i:s");
        //$recibe_departamento = $datos["dpoRecibe"];
        $id_cita = $id_cita;
        //$descripcion = $datos["descripcion"];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/gerencia/index.php/tiempos/insertar_tiempo");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "envia_id_usuario=".$envia_id_usuario."&envia_nombre_usuario=".$envia_nombre_usuario."&envia_departamento=".$datos["envia_departamento"]."&sucursal=".$sucursal."&envia_fecha_hora=".$envia_fecha_hora."&recibe_departamento=".$datos["recibe_departamento"]."&id_cita=".$id_cita."&descripcion=".$datos["descripcion"]."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);
    }

    public function revisar_datos()
    {
        $contenedor = [];

        $id_cita = $_POST["id_cita"];
        $informacion_gral = $this->consulta->orden_completa($id_cita);
        foreach ($informacion_gral as $row){
            $contenedor["vehiculo"] = $row->vehiculo_modelo;
            $contenedor["placas"] = $row->vehiculo_placas;
            $contenedor["modelo"] = $row->vehiculo_anio;
            $contenedor["serie"] = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
            $contenedor["asesor"] = $row->asesor;
            $contenedor["folio"] = $row->folioIntelisis;
            $contenedor["tecnico"] = $row->tecnico;
            $contenedor["cliente"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
        }

        //Revisamos si existe el presupuesto
        $revision = $this->consulta->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $contenedor["pr_id"] = $row->id;
            $contenedor["pr_acepta_cliente"] = $row->acepta_cliente;
            $contenedor["pr_identificador"] = $row->identificador;
        }

        $contenedor["id_cita_url"] = $this->encrypt($id_cita);

        if (!isset($contenedor["pr_id"])) {
            $contenedor["pr_id"] = 0;
        }

        echo json_encode( $contenedor );
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/presupuestos/presupuestoMulti.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Presupuesto Multipunto Body"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}