<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VC_DiagnosticoHYP extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'consulta', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

	public function lista_asestec()
    {
    	if (!$this->session->userdata('idUsuario')) {
            redirect('Login_AseTecnico', 'refresh');
        } else {
    		$fecha_busqueda = (date("Y") - 2)."-".date("m-d");

	        if ($this->session->userdata('rolIniciado')) {
	            //Rosendo internas
	            if ($this->session->userdata('idUsuario') == "33") {
	                //$tipo_ordenes = "4";
	                $datos["listado"] = $this->consulta->voz_cliente_athyp($fecha_busqueda,4);
	            //JESUS HYP
	            } elseif ($this->session->userdata('idUsuario') == "34") {
	                //$tipo_ordenes = "3";
	                $datos["listado"] = $this->consulta->voz_cliente_athyp($fecha_busqueda,3);
	            }else{
	                $datos["listado"] = $this->consulta->voz_cliente_at($fecha_busqueda);
	            }
	        } else{
	            $datos["listado"] = $this->consulta->voz_cliente_at($fecha_busqueda);
	        }

	        $this->loadAllView($datos,'mod_asetec/voz_cliente/lista_asesor_tecnico'); 
    	}   
    }

    public function loadAllView($datos = NULL,$vista = "")
    {
        $archivosJs = array(
            "include" => array(
            	'assets/js/vozCliente/nvo_formulario/buscador_vc.js',
            )
        );
        
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Voz Cliente HBody"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}