<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->database();
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

  	public function index($destino = "ADMIN",$resp = NULL)
  	{
        $datos["mensaje"] = $resp;
        //Cargamos la vista principal dependiendo la ruta
        if ($destino == "PT") {
            //Pantalla principal para tecnico (solo la multipunto)
            $this->loadAllView($datos,'principales/principal_tec');
        } elseif ($destino == "PA") {
            //Pantalla principal para asesor (todos los formularios de edicion)
            $this->loadAllView($datos,'principales/principal_ases');
        } else {
            //Pantalla principal para pruebas
            $this->loadAllView($datos,'principales/principal_2');
        }


  	}

    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", '', TRUE),
            "titulo" => "Principal"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

    public function consulta($i = 0)
    {
        $precarga = $this->mConsultas->filtro_princ('gpo','refacciones');
        echo "<br>Refacciones<br>";
        var_dump($precarga);

        echo "<br><br>";
        $precarga2 = $this->mConsultas->filtro_Sec('prefijo','clave_comprimida','refacciones','MC');
        echo "<br>Refacciones<br>";
        var_dump($precarga2);
        // $precarga = $this->mConsultas->get_result('id_cita',$i,'citas');
        // echo "<br>Citas:<br>";
        // var_dump($precarga);
        // $precarga2 = $this->mConsultas->get_result('id_cita',$i,'ordenservicio');
        // foreach ($precarga2 as $row){
        //     $idOrden = $row->id;
        //     $tipoOrden = $row->id_tipo_orden;
        // }
        // echo "<br><br>OrdenServicio: (id = <u>".$idOrden."</u>)<br><br>";
        // var_dump($precarga2);
        // $precarga3 = $this->mConsultas->joint_table_car($i);
        // echo "<br><br>Consulta:<br><br>";
        // var_dump($precarga3);
        // echo "<br>------------------------------------------------------<br>";
        //
        // echo "<br>------------------------------------------------------<br>";
        // $precarga_4 = $this->mConsultas->get_result('idorden',$idOrden,'articulos_orden');
        // echo "<br><br>articulos_orden:<br><br>";
        // var_dump($precarga_4);
        //
        // $precarga_5 = $this->mConsultas->citas($idOrden);
        // echo "<br><br>Citas - orden de servicio - tipo de pago:<br><br>";
        // var_dump($precarga_5);
        // foreach ($precarga_5 as $row){
        //     $idColor = $row->id_color;
        // }
        //
        // echo "<br><br>Error aqui <br>";
        // $precarga_6 = $this->mConsultas->getTipoOrden($tipoOrden);
        // echo "<br><br>Tipo de orden:<br><br>";
        // var_dump($precarga_6);
        // $precarga_7 = $this->mConsultas->tecnicos($idOrden);
        // // $extraPrec = $this->mConsultas->get_table('tecnicos_citas');
        // echo "<br><br>Tecnicos:<br><br>";
        // var_dump($precarga_7);
        // // echo "<br><br>Tecnicos _ table:<br><br>";
        // // var_dump($extraPrec);
        // echo "<br><br>Error aca";
        // $precarga_8 = $this->mConsultas->getColorCitaAuto($tipoOrden);
        // echo "<br><br>color:<br><br>";
        // var_dump($precarga_8);
        // $precarga_9 = $this->mConsultas->getAnticipoOrden($idOrden);
        // echo "<br><br>anticipo:<br><br>";
        // var_dump($precarga_9);
        //
        // $precarga_10 = $this->mConsultas->EstatusCotizacionUser($idOrden);
        // echo "<br><br>confirmada:<br><br>";
        // var_dump($precarga_10);
        // echo "<br><br><br>Actualizacion";
        //
        //
        // $algomas = $this->mConsultas->get_result('idPivote',$idOrden,'voz_cliente');
        // echo "<br><br>voz cleinte:<br><br>";
        // var_dump($algomas);
        //
        // echo "<br>------------------------------------------------------<br>";


    }


}
