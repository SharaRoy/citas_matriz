<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DuplicarRegistros extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
        // $this->load->view('welcome_message');
    }

    //Recibimos actualización de refacciones
    public function duplicar_Registro()
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);

        $_POST = json_decode(file_get_contents('php://input'), true);

        $contenedor = [];
        //Evitamos posibles fallos al momento de ejecutar las consultas
        try {
            $proceso = "";
            $registro = date("Y-m-d H:i:s");
            //Recibimos los datos
            $idOrden = $_POST["id_cita"];
            $idOrdenNvo = $_POST["id_cita_nuevo"];
            
            //Orden de servico parte 2 (Diagnostico)
            $orden = $this->diagnostico($idOrden,$idOrdenNvo,$registro);
            //Multipunto
            $multipunto = $this->multipunto($idOrden,$idOrdenNvo,$registro);
            //Voz Cliente
            $vozCliente = $this->vozCliente($idOrden,$idOrdenNvo,$registro);
            //Auditoria de calidad
            //$auditoria = $this->auditoria($idOrden,$idOrdenNvo,$registro);
            //Cotización
            //$cotizacion = $this->cotizacion($idOrden,$idOrdenNvo,$registro);
            
            $proceso = $orden."-".$multipunto."-".$vozCliente."-";

            if ($orden == "1") {
                //Si el diagnostico se guardo, creamos el registro para la documentacion
                $this->registro_documentacion($idOrdenNvo);
            }

        } catch (\Exception $e) {
            $proceso = "ERROR AL PROCESAR 02";
        } 

        echo $proceso;

    }

    //Buscamos y dublicamos la orden de servicio
    public function diagnostico($idOrden = '',$nvoIdOrden = '',$registro = '')
    {
        $dataForm = array();
        $query = $this->mConsultas->get_result("noServicio",$idOrden,"diagnostico");
        foreach ($query as $row){
            $dataForm["idDiagnostico"] = NULL;
            $dataForm["noServicio"] = $nvoIdOrden;
            $dataForm["vehiculo"] = $row->vehiculo;
            $dataForm["dias"] = $row->dias;
            $dataForm["terminos"] = $row->terminos;

            $dataForm["fecha_diagnostico"] = $row->fecha_diagnostico;
            $dataForm["fecha_aceptacion"] = $row->fecha_aceptacion;
            $dataForm["firmaDiagnostico"] = $row->firmaDiagnostico;
            $dataForm["firmaConsumidor1"] = $row->firmaConsumidor1;
            $dataForm["danos"] = $row->danos;
            $dataForm["danosImg"] = $row->danosImg;

            $dataForm["interiores"] = $row->interiores;
            $dataForm["cajuela"] = $row->cajuela;
            $dataForm["exteriores"] = $row->exteriores;
            $dataForm["documentacion"] = $row->documentacion;
            $dataForm["articulos"] = $row->articulos;
            $dataForm["cualesArticulos"] = $row->cualesArticulos;
            $dataForm["reporte"] = $row->reporte;
            $dataForm["costo"] = $row->costo;
            $dataForm["diaValue"] = $row->diaValue;
            $dataForm["mesValue"] = $row->mesValue;
            $dataForm["anioValue"] = $row->anioValue;

            $dataForm["firmaAsesor"] = $row->firmaAsesor;
            $dataForm["firmaConsumidor2"] = $row->firmaConsumidor2;
            $dataForm["nivelGasolina"] = $row->nivelGasolina;

            $dataForm["nombreDiagnostico"] = $row->nombreDiagnostico;
            $dataForm["nombreConsumido1"] = $row->nombreConsumido1;
            $dataForm["nombreAsesor"] = $row->nombreAsesor;
            $dataForm["nombreConsumidor2"] = $row->nombreConsumidor2;
            $dataForm["archivoOasis"] = $row->archivoOasis;
            $dataForm["fecha_actualiza"] = date("Y-m-d H:i:s");
            $dataForm["inicio_formulario"] = $registro;
            $dataForm["existePrev"] = $row->existePrev;
            
        }

        if (isset($dataForm["existePrev"])) {
            $servicio = $this->mConsultas->save_register('diagnostico',$dataForm);
            if ($servicio != 0) {
                //Guardamos el movimiento
                $this->loadHistory($idOrden,"Duplicando OR ".$idOrden." en OR ".$nvoIdOrden,"Orden de Servicio (Diagnostico)");

                $query = $this->mConsultas->get_result("identificador",$idOrden,"contrato");
                foreach ($query as $row){
                    $dataContract = array(
                        "idContracto" => NULL,
                        "identificador"=> $nvoIdOrden,
                        "folio" => $row->folio,
                        "fecha" => $row->fecha,
                        "hora" => $row->hora,
                        "empresa" => $row->empresa,
                        "pagoConsumidor" => $row->pagoConsumidor,
                        "condicionalInfo" => $row->condicionalInfo,
                        "condicionalPublicidad" => $row->condicionalPublicidad,
                        "firmaCliente" => $row->firmaCliente,
                        "nombreDistribuidor " =>$row->nombreDistribuidor,
                        "firmaDistribuidor" => $row->firmaDistribuidor,
                        "nombreConsumidor" => $row->nombreConsumidor,
                        "firmaConsumidor" => $row->firmaConsumidor,
                        "aceptaTerminos" => $row->aceptaTerminos,
                        "noRegistro" => $row->noRegistro,
                        "noExpediente" => $row->noExpediente,
                        "fechDia" => $row->fechDia,
                        "fechMes" => $row->fechMes,
                        "fechAnio" => $row->fechAnio,
                        "fechaRegistro" => $registro,
                    );
                }

                $contrato = $this->mConsultas->save_register('contrato',$dataContract);
                if ($contrato != 0) {
                    $contededor["firma_diagnostico"] = "1";
                    //Guardamos el registro
                    $actualiza = $this->mConsultas->update_table_row('ordenservicio',$contededor,'id_cita',$nvoIdOrden);
                    $retorno = "1";
                }else {
                    $retorno = "2";
                }
            }else {
                $retorno = "2";
            }
        } else {
            $retorno = "0";
        }
        
        return  $retorno;
    }

    //Buscamos y dublicamos la hoja multipunto
    public function multipunto($idOrden = '',$nvoIdOrden = '',$registro = '')
    {
        $query = $this->mConsultas->get_result("orden",$idOrden,"multipunto_general");
        foreach ($query as $row){
            $idHM = $row->id;

            $dataPart1["id"] = NULL;
            $dataPart1["orden"] = $nvoIdOrden;
            $dataPart1["fechaCaptura"] = $row->fechaCaptura;

            $dataPart1["noSerie"] = $row->noSerie;
            $dataPart1["modelo"] = $row->modelo;
            $dataPart1["torre"] = $row->torre;
            $dataPart1["nombreDueno"] = $row->nombreDueno;
            $dataPart1["email"] = $row->email;
            $dataPart1["emailCompania"] = $row->emailCompania;

            $dataPart1["perdidaAceite"] = $row->perdidaAceite;
            $dataPart1["cambioAceite"] = $row->cambioAceite;
            $dataPart1["aceiteMotor"] = $row->aceiteMotor;
            $dataPart1["direccionHidraulica"] = $row->direccionHidraulica;
            $dataPart1["transmision"] = $row->transmision;
            $dataPart1["fluidoFrenos"] = $row->fluidoFrenos;
            $dataPart1["limpiaparabrisas"] = $row->limpiaparabrisas;
            $dataPart1["refrigerante"] = $row->refrigerante;
            $dataPart1["plumasPruebas"] = $row->plumasPruebas;
            $dataPart1["plumas"] = $row->plumas;
            $dataPart1["plumasCambio"] = $row->plumasCambio;
            $dataPart1["lucesClaxon"] = $row->lucesClaxon;
            $dataPart1["lucesClaxonCambio"] = $row->lucesClaxonCambio;
            $dataPart1["grietasParabrisas"] = $row->grietasParabrisas;
            $dataPart1["grietasCambio"] = $row->grietasCambio;

            $dataPart1["bateria"] = $row->bateria;
            $dataPart1["bateriaCambio"] = $row->bateriaCambio;
            $dataPart1["bateriaEstado"] = $row->bateriaEstado;

            $dataPart1["frio"] = $row->frio;
            $dataPart1["frioReal"] = $row->frioReal;
        }

        if (isset($idHM)) {
            $idRegistroP1 = $this->mConsultas->save_register('multipunto_general',$dataPart1);
            if (($idRegistroP1 != 0)&&($idRegistroP1 != "")) {
                //Multipunto parte 2
                $query2 = $this->mConsultas->get_result("idOrden",$idHM,"multipunto_general_2");
                foreach ($query2 as $row2){
                    $dataPart2["idGral2"] = NULL;
                    $dataPart2["idOrden"] = $idRegistroP1;

                    $dataPart2["sistemaCalefaccion"] = $row2->sistemaCalefaccion;
                    $dataPart2["sistemaCalefaccionCambio"] = $row2->sistemaCalefaccionCambio;
                    $dataPart2["sisRefrigeracion"] = $row2->sisRefrigeracion;
                    $dataPart2["sisRefrigeracionCambio"] = $row2->sisRefrigeracionCambio;
                    $dataPart2["bandas"] = $row2->bandas;
                    $dataPart2["bandasCambio"] = $row2->bandasCambio;
                    $dataPart2["sisFrenos"] = $row2->sisFrenos;
                    $dataPart2["sisFrenosCambio"] = $row2->sisFrenosCambio;
                    $dataPart2["suspension"] = $row2->suspension;
                    $dataPart2["suspensionCambio"] = $row2->suspensionCambio;
                    $dataPart2["varillajeDireccion"] = $row2->varillajeDireccion;
                    $dataPart2["varillajeDireccionCambio"] = $row2->varillajeDireccionCambio;
                    $dataPart2["sisEscape"] = $row2->sisEscape;
                    $dataPart2["sisEscapeCambio"] = $row2->sisEscapeCambio;
                    $dataPart2["funEmbrague"] = $row2->funEmbrague;
                    $dataPart2["funEmbragueCambio"] = $row2->funEmbragueCambio;
                    $dataPart2["flechaCardan"] = $row2->flechaCardan;
                    $dataPart2["flechaCardanCambio"] = $row2->flechaCardanCambio;

                    $dataPart2["defectosImg"] = $row2->defectosImg;
                    $dataPart2["medicionesFrenos"] = $row2->medicionesFrenos;
                    $dataPart2["indicadorAceite"] = $row2->indicadorAceite;
                    $dataPart2["comentarios"] = $row2->comentarios;
                }

                $idRegistroP2 = $this->mConsultas->save_register('multipunto_general_2',$dataPart2);

                //Multipunto parte 3
                $query3 = $this->mConsultas->get_result("idOrden",$idHM,"frente_izquierdo");
                foreach ($query3 as $row3){
                    $dataPart3["idFI"] = NULL;
                    $dataPart3["idOrden"] = $idRegistroP1;

                    $dataPart3["pneumaticoFI"] = $row3->pneumaticoFI;
                    $dataPart3["pneumaticoFImm"] = $row3->pneumaticoFImm;
                    $dataPart3["pneumaticoFIcambio"] = $row3->pneumaticoFIcambio;
                    $dataPart3["dNeumaticoFI"] = $row3->dNeumaticoFI;
                    $dataPart3["dNeumaticoFIcambio"] = $row3->dNeumaticoFIcambio;
                    $dataPart3["presionInfladoFI"] = $row3->presionInfladoFI;
                    $dataPart3["presionInfladoFIpsi"] = $row3->presionInfladoFIpsi;
                    $dataPart3["presionInfladoFIcambio"] = $row3->presionInfladoFIcambio;
                    $dataPart3["espesoBalataFI"] = $row3->espesoBalataFI;
                    $dataPart3["espesorBalataFImm"] = $row3->espesorBalataFImm;
                    $dataPart3["espesorBalataFIcambio"] = $row3->espesorBalataFIcambio;
                    $dataPart3["espesorDiscoFI"] = $row3->espesorDiscoFI;
                    $dataPart3["espesorDiscoFImm"] = $row3->espesorDiscoFImm;
                    $dataPart3["espesorDiscoFIcambio"] = $row3->espesorDiscoFIcambio;
                }

                $idRegistroP3 = $this->mConsultas->save_register('frente_izquierdo',$dataPart3);

                //Multipunto parte 4
                $query4 = $this->mConsultas->get_result("idOrden",$idHM,"trasero_izquierdo");
                foreach ($query4 as $row4){
                    $dataPart4["idTI"] = NULL;
                    $dataPart4["idOrden"] = $idRegistroP1;

                    $dataPart4["pneumaticoTI"] = $row4->pneumaticoTI;
                    $dataPart4["pneumaticoTImm"] = $row4->pneumaticoTImm;
                    $dataPart4["pneumaticoTIcambio"] = $row4->pneumaticoTIcambio;
                    $dataPart4["dNeumaticoTI"] = $row4->dNeumaticoTI;
                    $dataPart4["dNeumaticoTIcambio"] = $row4->dNeumaticoTIcambio;
                    $dataPart4["presionInfladoTI"] = $row4->presionInfladoTI;
                    $dataPart4["presionInfladoTIpsi"] = $row4->presionInfladoTIpsi;
                    $dataPart4["presionInfladoTIcambio"] = $row4->presionInfladoTIcambio;
                    $dataPart4["espesoBalataTI"] = $row4->espesoBalataTI;
                    $dataPart4["espesorBalataTImm"] = $row4->espesorBalataTImm;
                    $dataPart4["espesorBalataTIcambio"] = $row4->espesorBalataTIcambio;
                    $dataPart4["espesorDiscoTI"] = $row4->espesorDiscoTI;
                    $dataPart4["espesorDiscoTImm"] = $row4->espesorDiscoTImm;
                    $dataPart4["espesorDiscoTIcambio"] = $row4->espesorDiscoTIcambio;
                    $dataPart4["diametroTamborTI"] = $row4->diametroTamborTI;
                    $dataPart4["diametroTamborTImm"] = $row4->diametroTamborTImm;
                    $dataPart4["diametroTamborTIcambio"] = $row4->diametroTamborTIcambio;
                }

                $idRegistroP4 = $this->mConsultas->save_register('trasero_izquierdo',$dataPart4);

                //Multipunto parte 5
                $query5 = $this->mConsultas->get_result("idOrden",$idHM,"frente_derecho");
                foreach ($query5 as $row5){
                    $dataPart5["idFD"] = NULL;
                    $dataPart5["idOrden"] = $idRegistroP1;

                    $dataPart5["pneumaticoFD"] = $row5->pneumaticoFD;
                    $dataPart5["pneumaticoFDmm"] = $row5->pneumaticoFDmm;
                    $dataPart5["pneumaticoFDcambio"] = $row5->pneumaticoFDcambio;
                    $dataPart5["dNeumaticoFD"] = $row5->dNeumaticoFD;
                    $dataPart5["dNeumaticoFDcambio"] = $row5->dNeumaticoFDcambio;
                    $dataPart5["presionInfladoFD"] = $row5->presionInfladoFD;
                    $dataPart5["presionInfladoFDpsi"] = $row5->presionInfladoFDpsi;
                    $dataPart5["presionInfladoFDcambio"] = $row5->presionInfladoFDcambio;
                    $dataPart5["espesoBalataFD"] = $row5->espesoBalataFD;
                    $dataPart5["espesorBalataFDmm"] = $row5->espesorBalataFDmm;
                    $dataPart5["espesorBalataFDcambio"] = $row5->espesorBalataFDcambio;
                    $dataPart5["espesorDiscoFD"] = $row5->espesorDiscoFD;
                    $dataPart5["espesorDiscoFDmm"] = $row5->espesorDiscoFDmm;
                    $dataPart5["espesorDiscoFDcambio"] = $row5->espesorDiscoFDcambio;
                }

                $idRegistroP5 = $this->mConsultas->save_register('frente_derecho',$dataPart5);

                //Multipunto parte 6
                $query6 = $this->mConsultas->get_result("idOrden",$idHM,"trasero_derecho");
                foreach ($query6 as $row6){
                    $dataPart6["idTD"] = NULL;
                    $dataPart6["idOrden"] = $idRegistroP1;

                    $dataPart6["pneumaticoTD"] = $row6->pneumaticoTD;
                    $dataPart6["pneumaticoTDmm"] = $row6->pneumaticoTDmm;
                    $dataPart6["pneumaticoTDcambio"] = $row6->pneumaticoTDcambio;
                    $dataPart6["dNeumaticoTD"] = $row6->dNeumaticoTD;
                    $dataPart6["dNeumaticoTDcambio"] = $row6->dNeumaticoTDcambio;
                    $dataPart6["presionInfladoTD"] = $row6->presionInfladoTD;
                    $dataPart6["presionInfladoTDpsi"] = $row6->presionInfladoTDpsi;
                    $dataPart6["presionInfladoTDcambio"] = $row6->presionInfladoTDcambio;
                    $dataPart6["espesoBalataTD"] = $row6->espesoBalataTD;
                    $dataPart6["espesorBalataTDmm"] = $row6->espesorBalataTDmm;
                    $dataPart6["espesorBalataTDcambio"] = $row6->espesorBalataTDcambio;
                    $dataPart6["espesorDiscoTD"] = $row6->espesorDiscoTD;
                    $dataPart6["espesorDiscoTDmm"] = $row6->espesorDiscoTDmm;
                    $dataPart6["espesorDiscoTDcambio"] = $row6->espesorDiscoTDcambio;
                    $dataPart6["diametroTamborTD"] = $row6->diametroTamborTD;
                    $dataPart6["diametroTamborTDmm"] = $row6->diametroTamborTDmm;
                    $dataPart6["diametroTamborTDcambio"] = $row6->diametroTamborTDcambio;
                }

                $idRegistroP6 = $this->mConsultas->save_register('trasero_derecho',$dataPart6);

                //Multipunto parte 7
                $query7 = $this->mConsultas->get_result("idOrden",$idHM,"multipunto_general_3");
                foreach ($query7 as $row7){
                    $dataPart7["idGral3"] = NULL;
                    $dataPart7["idOrden"] = $idRegistroP1;

                    $dataPart7["presion"] = $row7->presion;
                    $dataPart7["presionPSI"] = $row7->presionPSI;
                    $dataPart7["presionCambio"] = $row7->presionCambio;
                    $dataPart7["sistema"] = $row7->sistema;
                    $dataPart7["componente"] = $row7->componente;
                    $dataPart7["causaRaiz"] = $row7->causaRaiz;

                    $dataPart7["asesor"] = $row7->asesor;
                    $dataPart7["firmaAsesor"] = $row7->firmaAsesor;
                    $dataPart7["tecnico"] = $row7->tecnico;
                    $dataPart7["firmaTecnico"] = $row7->firmaTecnico;
                    $dataPart7["archivo"] = $row7->archivo;
                    $dataPart7["nombreCliente"] = $row7->nombreCliente;
                    $dataPart7["firmaCliente"] = $row7->firmaCliente;
                    $dataPart7["jefeTaller"] = $row7->jefeTaller;
                    $dataPart7["firmaJefeTaller"] = $row7->firmaJefeTaller;
                }

                $idRegistroP7 = $this->mConsultas->save_register('multipunto_general_3',$dataPart7);

                $retorno = "1";
            }else {
                $retorno = "0";
            }
        }else {
            $retorno = "0";
        }

        return $retorno;
    }

    //Buscamos y dublicamos la voz cliente
    public function vozCliente($idOrden = '',$nvoIdOrden = '',$registro = '')
    {
        $query = $this->mConsultas->get_result("idPivote",$idOrden,"voz_cliente");
        foreach ($query as $row){
            $dataForm_1["id"] = NULL;
            $dataForm_1["id_cita"] = $nvoIdOrden;
            $dataForm_1["n_diagnosticos"] = "1";
            $dataForm_1["audio"] = $row->evidencia;
            $dataForm_1["nombre_asesor"] = $row->nombreAsesor;
            $dataForm_1["firma_asesor"] = $row->firmaAsesor;
            $dataForm_1["nombre_cliente"] = $row->nombreCliente;
            $dataForm_1["firma_cliente"] = $row->firmaCliente;

            $dataForm_1["fecha_registro"] = $registro;
            $dataForm_1["fecha_actualiza"] = $registro;

            $vc_registro = $this->mConsultas->save_register('vc_registro',$dataForm_1);

            $dataForm["id"] = NULL;
            $dataForm["id_cita"] = $nvoIdOrden;
            $dataForm["definicion_falla"] = $row->definicionFalla;
            $dataForm["indicadores_falla"] = $row->sentidosFalla;
            $dataForm["diagrama_falla"] = $row->imgFallas;
            $dataForm["presenta_falla"] = $row->presentaFalla;
            $dataForm["percibe_falla"] = $row->percibeFalla;
            $dataForm["temperatura"] = $row->temperatura;
            $dataForm["humedad"] = $row->humedad;
            $dataForm["viento"] = $row->viento;
            $dataForm["velocidad"] = $row->velocidad;
            $dataForm["transmision_p1"] = $row->cambioTransmision;
            $dataForm["transmision_p2"] = $row->transmision;
            $dataForm["transmision"] = $row->cambioOperativa;
            $dataForm["rmp"] = $row->rmp;
            $dataForm["carga"] = $row->carga;
            $dataForm["carga_remolque"] = $row->remolque;
            $dataForm["pasajeros"] = $row->pasajeros;
            $dataForm["pasajero_cajuela"] = $row->cajuela;
            $dataForm["estructura"] = $row->estructura;
            $dataForm["camino"] = $row->camino;
            $dataForm["pendiente"] = $row->pendiente;
            $dataForm["superficie"] = $row->superficie;
            $dataForm["diag_sistema"] = $row->sistema;
            $dataForm["diag_componente"] = $row->componente;
            $dataForm["diag_causa_raiz"] = $row->causaRaiz;
            $dataForm["comentarios"] = "";

            $dataForm["fecha_registro"] = $registro;
            $dataForm["fecha_actualiza"] = $registro;

            $diagnostico = $this->mConsultas->save_register('vc_diagnostico',$dataForm);
        }

        if (isset($diagnostico)) {
            $contededor["firma_vc"] = "1";
            $actualiza = $this->mConsultas->update_table_row('ordenservicio',$contededor,'id_cita',$nvoIdOrden);

            $retorno = "1";
        }else{
            $retorno = $this->vozCliente_2($idOrden,$nvoIdOrden,$registro);
        }
        
        return $retorno;
    }

    public function vozCliente_2($idOrden = '',$nvoIdOrden = '',$registro = '')
    {
        $registro = date("Y-m-d H:i:s");

        $query_2 = $this->mConsultas->get_result("id_cita",$idOrden,"vc_diagnostico");
        foreach ($query_2 as $row2){
            $dataForm = NULL;

            $dataForm["id"] = NULL;
            $dataForm["id_cita"] = $nvoIdOrden;
            $dataForm["definicion_falla"] = $row2->definicion_falla;
            $dataForm["indicadores_falla"] = $row2->indicadores_falla;
            $dataForm["diagrama_falla"] = $row2->diagrama_falla;
            $dataForm["presenta_falla"] = $row2->presenta_falla;
            $dataForm["percibe_falla"] = $row2->percibe_falla;
            $dataForm["temperatura"] = $row2->temperatura;
            $dataForm["humedad"] = $row2->humedad;
            $dataForm["viento"] = $row2->viento;
            $dataForm["velocidad"] = $row2->velocidad;
            $dataForm["transmision_p1"] = $row2->transmision_p1;
            $dataForm["transmision_p2"] = $row2->transmision_p2;
            $dataForm["transmision"] = $row2->transmision;
            $dataForm["rmp"] = $row2->rmp;
            $dataForm["carga"] = $row2->carga;
            $dataForm["carga_remolque"] = $row2->carga_remolque;
            $dataForm["pasajeros"] = $row2->pasajeros;
            $dataForm["pasajero_cajuela"] = $row2->pasajero_cajuela;
            $dataForm["estructura"] = $row2->estructura;
            $dataForm["camino"] = $row2->camino;
            $dataForm["pendiente"] = $row2->pendiente;
            $dataForm["superficie"] = $row2->superficie;
            $dataForm["diag_sistema"] = $row2->diag_sistema;
            $dataForm["diag_componente"] = $row2->diag_componente;
            $dataForm["diag_causa_raiz"] = $row2->diag_causa_raiz;
            $dataForm["comentarios"] = $row2->comentarios;

            $dataForm["fecha_registro"] = $registro;
            $dataForm["fecha_actualiza"] = $registro;
            if ($dataForm != NULL) {
                $diagnostico = $this->mConsultas->save_register('vc_diagnostico',$dataForm);
            }
        }

        if (isset($diagnostico)) {
            $contededor["firma_vc"] = "1";
            $actualiza = $this->mConsultas->update_table_row('ordenservicio',$contededor,'id_cita',$nvoIdOrden);

            if ($diagnostico != 0) {
                $vc_registro = 0;
                $query = $this->mConsultas->get_result("id_cita",$idOrden,"vc_registro");
                foreach ($query as $row){
                    $dataForm_1["id"] = NULL;
                    $dataForm_1["id_cita"] = $nvoIdOrden;
                    $dataForm_1["n_diagnosticos"] = $row->n_diagnosticos;
                    $dataForm_1["audio"] = $row->audio;
                    $dataForm_1["nombre_asesor"] = $row->nombre_asesor;
                    $dataForm_1["firma_asesor"] = $row->firma_asesor;
                    $dataForm_1["nombre_cliente"] = $row->nombre_cliente;
                    $dataForm_1["firma_cliente"] = $row->firma_cliente;

                    $dataForm_1["fecha_registro"] = $registro;
                    $dataForm_1["fecha_actualiza"] = $registro;

                    $vc_registro = $this->mConsultas->save_register('vc_registro',$dataForm_1);
                }

                if ($vc_registro != 0) {
                    $this->loadHistory($nvoIdOrden,"Asociación​ de T-".$idOrden." a G-".$nvoIdOrden,"Voz Cliente NF");
                    $retorno = "1";
                }else{
                    $retorno = "3";
                }
            }else{
                $retorno = "2";
            }
        }else{
            $retorno = "0";
        }
            
        return $retorno;
    }

    //Creamos el registro para el listado de la documentacion de ordenes
    public function registro_documentacion($id_cita = '') 
    {
        //Recuperamos la informacion general
        $informacion_gral = $this->mConsultas->recuperar_documentaciones_cita($id_cita);
        $registro = date("Y-m-d H:i:s");

        foreach ($informacion_gral as $row) {
            //Verificamos si hay algun presupuesto
            if ($row->id_presupuesto != "") {
                $presupuesto = $row->id_presupuesto;
                $afecta_cliente = $row->acepta_cliente;
                $origen = "1";
            
            //De lo contrario, no hay ningun presupuesto hecho
            } else {
                $presupuesto = "";
                $afecta_cliente = "";
                $origen = "0";
            }
            
            $contededor = array(
                "id" => NULL,
                "id_cita" => $row->orden_cita,
                "folio_externo" => $row->folioIntelisis,
                "fecha_recepcion" => $row->fecha_recepcion,
                "vehiculo" => $row->vehiculo_modelo,
                "placas" => $row->vehiculo_placas,
                "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                "asesor" => $row->asesor,
                "tecnico" => $row->tecnico,
                "id_tecnico" => $row->id_tecnico,
                "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                "oasis" => $row->archivoOasis,
                "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                "presupuesto" => "0", //(($presupuesto != "") ? "1" : "0"),
                "presupuesto_afectado" => "", //$afecta_cliente,
                "presupuesto_origen" => $origen,
                "voz_cliente" => ((($row->idVoz != "")||($row->vc_id != "")) ? "1" : "0"),
                "evidencia_voz_cliente" => (($row->evidencia != "") ? $row->evidencia : (($row->audio != "") ? $row->audio : "")),
                "servicio_valet" => (($row->idValet != "") ? "1" : "0"),
                "fechas_creacion" => $registro,
            );

            //$contededor;
        }

        //Guardamos el registro
        $guardado = $this->mConsultas->save_register('documentacion_ordenes',$contededor);
    }

    //Recuperamos la serie para recuperar las ordenes anteriores
    public function ordenes_serie()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];

        //Evitamos posibles fallos al momento de ejecutar las consultas
        try {
            $serie = $_POST["serie"];

            //Consultamos los presupuestos nuevos
            //Recuperamos el numero de orden asociados a la serie
            $precarga_presupuesto = $this->mConsultas->presupuesto_por_serie($serie);

            foreach ($precarga_presupuesto as $row) {
                $contenedor[] = array(
                    'Contenido' => $this->recuperar_refacciones_presupuesto($row->id_cita)
                );
            }

            //Recuperamos el numero de orden asociados a la serie
            $precargaCitas = $this->mConsultas->cotizaciones_por_serie($serie);

            //$id_citas = [];
            foreach ($precargaCitas as $row) {
                //$id_citas[] =  $row->id_cita;
                $refaccion = $this->separacion_contenido($row->id_cita);
                if (count($refaccion)>0) {
                    $contenedor[] = array(
                        //'Cita' => $row->id_cita,
                        'Contenido' => $this->separacion_contenido($row->id_cita)
                    );
                }
                
                //$contenedor["Reparaciones"] = $this->separacion_contenido($row->id_cita);
            }

            //for ($i=0; $i < count($id_citas) ; $i++) { 
                //Consutamos las cotizaciones
            //}
            
        } catch (Exception $e) {
            
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    //Recuperamos los items del presupuesto
    public function recuperar_refacciones_presupuesto($id_cita = '')
    {
        $contenido = [];

        //Recuperamos las refacciones
        $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_multipunto");
        foreach ($query as $row){
            if (($row->autoriza_asesor == "1")&&($row->autoriza_cliente == "0")) {
                $contenido[] = $row->cantidad."||".trim($row->descripcion, " \t\n\r\x0B")."||".$row->num_pieza."||".
                                $row->costo_pieza."||".$row->horas_mo."||".$row->costo_mo."||".round($row->total_refacion,2);
            } 
        }

        return $contenido;
    }

    //Recuperar las refacciones rechazadas de cada cita
    public function separacion_contenido($idOrden = '')
    {
        $contenido = [];

        //Revisamos si existen refacciones para enviar
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
        foreach ($query as $row){
            if ($row->identificador == "M") {
                $registros = $this->createCells(explode("|",$row->contenido));
                $decision = $row->aceptoTermino;
                $fechaAlta = $row->fech_actualiza;
                $idCoti = $row->idCotizacion;
            }
        }

        //Verificamos que exista la cotizacion de la orden
        if (isset($decision)) {
            //Verificamos que ya se haya tomado la decison
            if ($decision != "") {
                //Verificamos que la cotizacion haya sido aceptada
                if ($decision == "No") {
                    //Recuperamos la informacion que se encesita de las piezas
                    foreach ($registros as $index => $value) {
                        $cantidad[] = $registros[$index][0];
                        $numPieza[] = str_replace(" ", "", $registros[$index][6]);
                        $precio[] = $registros[$index][2];
                        $descripcion[] = $registros[$index][1];
                        $horas[] = $registros[$index][3];
                        $mo[] = $registros[$index][4];
                    }

                    //Recuperamos la informacion de la refaccion para enviar
                    $total = 0;
                    for ($i=0; $i <count($numPieza) ; $i++) {
                        if ($descripcion[$i] != "") {
                            $conteo1 = (float)$cantidad[$i] * (float)$precio[$i]; 
                            $conteo2 = (float)$horas[$i] * (float)$mo[$i];
                            $total = $conteo1+$conteo2;

                            $contenido[] = $cantidad[$i]."||".trim($descripcion[$i], " \t\n\r\x0B")."||".$numPieza[$i]."||".
                                $precio[$i]."||".$horas[$i]."||".$mo[$i]."||".round($total,2);
                        }
                    }

                //O si la cotizacion se acepto por partes
                }elseif ($decision == "Val") {
                    //Recuperamos la informacion que se encesita de las piezas
                    foreach ($registros as $index => $value) {
                        if (count($registros[$index])>9) {
                            $filaDecide[] = $registros[$index][count($registros[$index])-1];
                            $cantidad[] = $registros[$index][0];
                            $numPieza[] = $registros[$index][6];
                            $precio[] = $registros[$index][2];
                            $descripcion[] = $registros[$index][1];
                            $horas[] = $registros[$index][3];
                            $mo[] = $registros[$index][4];
                        }
                    }

                    //Recuperamos la informacion de la refaccion para enviar
                    $contador = 1;
                    for ($i=0; $i <count($numPieza) ; $i++) {
                        if (($filaDecide[$i] == "Rechazada") && ($descripcion[$i] != "")) {
                            if ($descripcion[$i] != "") {
                                $conteo1 = (float)$cantidad[$i] * (float)$precio[$i]; 
                                $conteo2 = (float)$horas[$i] * (float)$mo[$i];
                                $total = $conteo1+$conteo2;
                                
                                $contenido[] = $cantidad[$i]."||".trim($descripcion[$i], " \t\n\r\x0B")."||".$numPieza[$i]."||".
                                    $precio[$i]."||".$horas[$i]."||".$mo[$i]."||".round($total,2);
                            }
                        }
                    }

                //De lo contrario no se crea ningun archivo
                } else {
                    //$this->envioCorreo("OS_".$idOrden,"",$idOrden);
                }
            //De lo contrario no se crea ningun archivo
            } else {
                //$this->envioCorreo("OS_".$idOrden,"",$idOrden);
            }
        //De lo contrario no se crea ningun archivo
        }else {
            //$this->envioCorreo("OS_".$idOrden,"",$idOrden);
        }

        return $contenido;
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }


}
