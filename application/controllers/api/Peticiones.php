<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peticiones extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set('America/Mazatlan');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
    }

    public function index()
    {
          // $this->load->view('welcome_message');
    }

    //Resibimos el id Cita para recuperar la informacion de la orden
    public function loadDataOrden_estructura()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];
        $idOrden = $_POST["idOrden"];

        try {            

            // $idOrden = "6858";
            //Hacemos la consulta
            $query = $this->mConsultas->api_orden($idOrden);

            //var_dump($idOrden);
            //var_dump($query);

            //Recuperamos la informacion de la consulta
            //Recuperamos la informacion de la consulta
            foreach ($query as $row) {
                $contenedor["Login"] = array(
                    "usuario" => "user",
                    "password" => "123456"
                );

                $contenedor["ID"] = $row->id;
                $contenedor["Mov"] = $row->id_Cita_orden;
                //Formato YYYY-MM-DD
                //Datos de la orden
                $contenedor["FechaEmision"] = $row->fecha_recepcion;
                $contenedor["HoraEmision"] = $row->hora_recepcion;
                $contenedor["FechaPromesa"] = $row->fecha_entrega;
                $contenedor["HoraPromesa"] = $row->hora_entrega;

                if ($row->id_concepto != "") {
                    $query_temp = $this->mConsultas->get_result("id",$row->id_concepto,"cat_concepto");
                    foreach ($query_temp as $row_1){
                        $dato = $row_1->concepto;
                    }
                    //Comprobamos que se haya hecho la consulta
                    if (isset($dato)) {
                        $contenedor["Concepto"] = (string)$this->quitar_tildes($dato);
                    }else {
                        $contenedor["Concepto"] = "";
                    }
                }else {
                    $contenedor["Concepto"] = "";
                }

                if ($row->id_uen != "") {
                    $query_temp_2 = $this->mConsultas->get_result("id",$row->id_uen,"cat_uen");
                    foreach ($query_temp_2 as $row_2){
                        $dato_2 = $row_2->uen;
                    }
                    //Comprobamos que se haya hecho la consulta
                    if (isset($dato_2)) {
                        $fraccion = explode(" ",$dato_2);
                        $fraccion_2 = str_replace(array("(",")"),"",$fraccion[0]);
                        $contenedor["UEN"] = $fraccion_2;
                    }else {
                        $contenedor["UEN"] = "";
                    }
                }else {
                    $contenedor["UEN"] = "";
                }

                if ($row->id_moneda != "") {
                    $query_temp_3 = $this->mConsultas->get_result("id",$row->id_moneda,"cat_moneda");
                    foreach ($query_temp_3 as $row_3){
                        $dato_3 = $row_3->moneda;
                    }
                    //Comprobamos que se haya hecho la consulta
                    if (isset($dato_3)) {
                        $contenedor["Moneda"] = $dato_3;
                    }else {
                        $contenedor["Moneda"] = "Pesos";
                    }
                }else {
                    $contenedor["Moneda"] = "Pesos";
                }

                $contenedor["TipoCambio"] = "1";             //Pendiente***

                //Nombre del usuario (id_usuario -> tabla citas)
                //$contenedor["Usuario"] = $row->id_usuario;
                
                //Identificamos al usuario a enviar
                //Asesor
                if ($row->asesor == "JORGE MUÑOZ SERRANO") {
                    $contenedor["Usuario"] = strtoupper("Jmunoz");
                //Asesor
                }elseif ($row->asesor == "WENDY FLORES CORONA") {
                    $contenedor["Usuario"] = strtoupper("Wflores");
                //Asesor
                }elseif ($row->asesor == "ALEJANDRA MEZA") {
                    $contenedor["Usuario"] = strtoupper("Ameza");
                //Asesor
                }elseif ($row->asesor == "GABRIELA BENELI ROCHA CONTRERAS") {
                    $contenedor["Usuario"] = strtoupper("Gbeneli");
                //Asesor
                }elseif ($row->asesor == "ELENA BARBOSA") {
                    $contenedor["Usuario"] = strtoupper("MBARBOSA");
                //Asesor
                }elseif ($row->asesor == "NAYELI RENTERÍA") {
                    $contenedor["Usuario"] = strtoupper("NRENTERIA");
                //Asesor
                }elseif ($row->asesor == "DEISSER BETINA CASTRO ROMERO") {
                    $contenedor["Usuario"] = strtoupper("DCASTRO");
                //Asesor
                }elseif ($row->asesor == "JOSE LUIS MANZO RAMIREZ") {
                    $contenedor["Usuario"] = strtoupper("JMANZO");
                //Otro Usuario 
                //}elseif ($row->id_usuario == "") {
                    //$contenedor["Usuario"] = strtoupper("");
                }elseif ($row->asesor == "NESTOR GERARDO OCHOA SALDAÑA") {
                    $contenedor["Usuario"] = strtoupper("nochoa");
                }else{
                    $contenedor["Usuario"] = "";
                }

                $limpieza_1 = $this->quitar_tildes($row->comentarios_servicio);
                $limpieza_2 = $this->quitar_saltos($limpieza_1);
                $contenedor["Observaciones"] =  trim($limpieza_2," \t\n\r");

                //Identificamos el tipo de rfc
                if (strlen($row->rfc) == 12) {
                    $tipoRegimen = "MORAL";
                    $nombre = $row->nombre_compania;
                    $pNombre = $row->datos_nombres;
                    $pAp = $row->datos_apellido_paterno;
                    $pAm = $row->datos_apellido_materno;
                //En caso que sea persona FÍSICA
                }else {
                    $tipoRegimen = "FISICA";
                    $nombre = $row->datos_nombres."".$row->datos_apellido_paterno."".$row->datos_apellido_materno;
                    $pNombre = $row->datos_nombres;
                    $pAp = $row->datos_apellido_paterno;
                    $pAm = $row->datos_apellido_materno;
                }

                //Verificamos el correo
                if ($row->datos_email == "") {
                    $correo = "notiene@correo.com";
                }else {
                    $correo = $row->datos_email;
                }

                //Comprobamos si es un cliente nuevo
                if ($row->cliente_nuevo == 0) {
                    // Si no es un cliente nuevo recuperamos el numero del cliente ( tabla ordenServicio)
                    if ($row->num_Cliente_orden != "") {
                        $cliente = $row->num_Cliente_orden;
                    }else {
                        //Si el campo del numero del cleinte esta vacio, buscamos en la otra tabla
                        $cliente = ($row->num_Cliente_cita != NULL) ? $row->num_Cliente_cita : "";
                        //Si aun continua vacio, asumimos que es nuevo
                        if ($cliente == "") {
                            $cliente = "NA";
                        }
                    }
                }else {
                    //Si es un cleinte nuevo enviamos NA
                    $cliente = "NA";
                }

                $contenedor["Cliente"] = array (
                    "cliente" => $cliente,
                    "Nombre" =>  trim($nombre," \t\n\r"),
                    "FiscalRegimen" => $tipoRegimen,
                    "Telefono" => $row->otro_telefono,
                    "Celular" => $row->telefono_movil,
                    "Direccion" => $row->calle.", ".$row->colonia.", ".$row->municipio,
                    "DireccionNumExt" => ((strtoupper($row->noexterior) != "SN")&&($row->noexterior != "")) ? $row->noexterior : "0",
                    "DireccionNumInt" => ((strtoupper($row->nointerior) != "SN")&&($row->nointerior != "")) ? $row->nointerior : "0",
                    "PersonalNombres" => $pNombre,
                    "PersonalApellidoPaterno" => $pAp,
                    "PersonalApellidoMaterno" => $pAm,
                    "Estado" => $row->estado,
                    "CP" => $row->cp,
                    "Pais" => "MEXICO",
                    "RFC" => $row->rfc,
                    "Observaciones" => "SIN COMENTARIOS",
                    "Email" => $correo,
                    "Colonia" => $row->colonia,
                    "Poblacion" => $row->municipio,
                    "Delegacion" => $row->municipio,
                );
                $contenedor["Almacen"] = (($row->almacen != NULL) ? $row->almacen : "");
                $contenedor["Agente"] = (($row->agente != NULL) ? $row->agente : "");

                if ($row->id_tipo_pago != "") {
                    $query_temp_4 = $this->mConsultas->get_result("id",$row->id_tipo_pago,"cat_tipo_pago");
                    foreach ($query_temp_4 as $row_4){
                        $dato_4 = $row_4->tipo_pago;
                    }
                    //Comprobamos que se haya hecho la consulta
                    if (isset($dato_4)) {
                        $contenedor["Condicion"] = $dato_4;
                    }else {
                        $contenedor["Condicion"] = "Contado";
                    }
                }else {
                    $contenedor["Condicion"] = "Contado";
                }

                //Recuperamos los articulos de la orden para sacar totales
                $orden_confirmada = $this->mConsultas->EstatusCotizacionUser($row->id);
                $items_Art = $this->mConsultas->articulosTitulos($row->id);

                $total = 0 ;
                foreach ($items_Art as $row_Art_2b) {
                    if ($orden_confirmada == "1") {
                        $total += $row_Art_2b->total;
                    } else {
                        //Si la cotizacion no esta confirmada, mientras no sea tipo 2
                        if($row_Art_2b->tipo != 2){
                            $total += $row_Art_2b->total;
                        }
                    }
                }

                $subtotal = ($total > 0) ? $total/1.16 : 0;
                $iva = ($total > 0) ? $subtotal*0.16 : 0;

                //Datos para la factura
                $contenedor["Importe"] = (string)round($subtotal,2);
                $contenedor["Impuestos"] = (string)round($iva,2);

                //$contenedor["ServicioArticulo"] = "";   //Pendiente
                $contenedor["ServicioArticulo"] = $this->quitar_tildes($row->vehiculo_modelo);

                //Datos del vehiculo
                $contenedor["ServicioSerie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                //$contenedor["ServicioDescripcion"] = $this->quitar_tildes($row->vehiculo_modelo);
                
                //Recuperamos el color
                if ($row->id_color != "") {
                    $queryColor  = $this->mConsultas->get_result('id',$row->id_color,'cat_colores');
                    foreach ($queryColor as $row_A){
                        $dato_A = $row_A->color;
                    }
                    //Comprobamos que se haya hecho la consulta
                    if (isset($dato_A)) {
                        $color = $this->quitar_tildes($dato_A);
                    }else {
                        $color = "";
                    }
                }else {
                    $color = "";
                }

                $contenedor["ServicioDescripcion"] = $color;

                $contenedor["ServicioPlacas"] = $row->vehiculo_placas;
                $contenedor["ServicioKms"] = $row->km;

                if ($row->id_tipo_orden != "") {
                    $query_temp_6 = $this->mConsultas->get_result("id",$row->id_tipo_orden,"cat_tipo_orden");
                    foreach ($query_temp_6 as $row_6){
                        $dato_6 = $row_6->tipo_orden;
                    }
                    //Comprobamos que se haya hecho la consulta
                    if (isset($dato_6)) {
                        $tipo_Orden = $this->quitar_tildes($dato_6);
                    }else {
                        $tipo_Orden = "";
                    }
                }else {
                    $tipo_Orden = "";
                }

                $contenedor["ServicioTipoOrden"] = $tipo_Orden;
                $contenedor["Sucursal"] = (($row->sucursal != NULL) ? $row->sucursal : "1");
                $contenedor["Referencia"] = (($row->servicio == NULL) ? "" : $row->servicio);

                if ($row->id_tipo_precio != "") {
                    $query_temp_7 = $this->mConsultas->get_result("id",$row->id_tipo_precio,"cat_tipo_precio");
                    foreach ($query_temp_7 as $row_7){
                        $dato_7 = $row_7->tipo_precio;
                    }
                    //Comprobamos que se haya hecho la consulta
                    if (isset($dato_7)) {
                        $fraccion = str_replace(array("(",")"),"",$dato_7);
                        $contenedor["ListaPreciosEsp"] = $this->quitar_tildes($fraccion);
                    }else {
                        $contenedor["ListaPreciosEsp"] = "";
                    }
                }else {
                    $contenedor["ListaPreciosEsp"] = "";
                }

                //Hacemos la consulta para los datos de la multipunto
                // $contenedor["inn"] = $this->loadDataMultipunto1($idOrden);
                $contenedor["inn"] = [];

                //Un for del contenido de las coincidencias de la tabla articulos_orden
                $items = $this->mConsultas->articulosTitulos($row->id);

                $datos["subtotal_items"] = 0;
                $datos["total_items"] = 0;

                $articulos = [];
                $contador = 1;
                foreach ($items as $row_items) {
                    // if ($orden_confirmada == "1") {
                        $coleccion["ID"] = $row_items->id;
                        $coleccion["RenglonID"] = (string)($contador * 2048);            //Multiplos de 2048
                        $coleccion["RenglonTipo"] = "N";
                        $coleccion["Cantidad"] = $row_items->cantidad;
                        $coleccion["Almacen"] = (($row->almacen != NULL) ? $row->almacen : "");
                        $coleccion["Tipo"] = "Servicio";             //Servicio ->Mano de obra  Normal -> Refaccion

                        $clave = "";
                        if ($row_items->tipo == 3) {
                            $clave .= "GEN";
                            $clave .= "";
                        }elseif (($row_items->tipo == 5)||($row_items->tipo == 4)) {
                            $clave .= $row_items->grupo."-";

                            $longitud_clave = strlen($clave);
                            $longitud_desc = strlen($row_items->descripcion);
                            $descipcion_clave = explode(" ",$row_items->descripcion); //$this->quitar_tildes($row_items->descripcion);

                            $clave .= (($longitud_desc < 17) ? $longitud_desc :  $descipcion_clave[0] );    //   substr($descipcion_clave, 0, (20-$longitud_clave)) ); 
                        }else {
                            $clave .= $row_items->grupoDes."-";
                            //$clave .= $row_items->operacionDes;
                            $longitud_clave = strlen($clave);
                            $longitud_desc = strlen($row_items->operacionDes);

                            $clave .= (($longitud_desc < 17) ? $longitud_desc : substr($row_items->operacionDes, 0, (19-$longitud_clave)) );
                        }

                        $coleccion["Articulo"] = (string)$this->quitar_tildes($clave);                   //Clave del articulo recuperado de intelisis

                        //Por modificaciones de FORD, para los genericos se envia solo el total
                        if ($row_items->tipo == 3) {
                            //Sacamos la diferencia de la MO y el precio
                            $mo = (float)$row_items->total_horas * (float)$row_items->costo_hora;
                            $mo_iva = $mo * 1.16;
                            $precio_unitario = $row_items->total - $mo_iva;

                            $coleccion["Precio"] = (($precio_unitario > 0) ? $precio_unitario/1.16 : 0);
                            //(string)round((($row_items->total > 0) ? $row_items->total/1.16 : 0),2);
                            //
                            $coleccion["Horas"] = $row_items->total_horas;
                            $coleccion["CostoHoras"] = (string)round((($row_items->costo_hora > 0) ? $row_items->costo_hora/1.16 : 0),2); 
                        }else{
                            $coleccion["Precio"] = (string)round((($row_items->precio_unitario > 0) ? $row_items->precio_unitario/1.16 : 0),2); 
                            $coleccion["Horas"] = $row_items->total_horas;
                            $coleccion["CostoHoras"] = (string)round((($row_items->costo_hora > 0) ? $row_items->costo_hora/1.16 : 0),2); 
                        }

                        $coleccion["DescripcionExtra"] = $row_items->descripcion;
                        $coleccion["Agente"] = (($row->agente != NULL) ? $row->agente : "");
                        $coleccion["Sucursal"] = (($row->sucursal != NULL) ? $row->sucursal : "1"); 

                        //Recopilamos la informacion del articulo
                        $articulos[] = $coleccion;
                        $contador++;
                    // }
                }

                //Comprobamos que se hayan cargado los articulos
                if (count($articulos) == 0) {
                    //Si no se cargaron los articulos repetimos la operacion
                    $items = $this->mConsultas->articulosTitulos($row->id);

                    $datos["subtotal_items"] = 0;
                    $datos["total_items"] = 0;

                    $articulos = [];
                    $contador = 1;
                    foreach ($items as $row_items) {
                        $coleccion["ID"] = $row_items->id;
                        $coleccion["RenglonID"] = (string)($contador * 2048);            //Multiplos de 2048
                        $coleccion["RenglonTipo"] = "N";
                        $coleccion["Cantidad"] = $row_items->cantidad;
                        $coleccion["Almacen"] = (($row->almacen != NULL) ? $row->almacen : "");
                        $coleccion["Tipo"] = "Servicio";             //Servicio ->Mano de obra  Normal -> Refaccion

                        $clave = "";
                        if ($row_items->tipo == 3) {
                            $clave .= "GEN";
                            $clave .= "";
                        }elseif (($row_items->tipo == 5)||($row_items->tipo == 4)) {
                            $clave .= $row_items->grupo."-";

                            $longitud_clave = strlen($clave);
                            $longitud_desc = strlen($row_items->descripcion);

                            $clave .= (($longitud_desc < 17) ? $longitud_desc : substr($row_items->descripcion, 0, (20-$longitud_clave)) ); 
                        }else {
                            $clave .= $row_items->grupoDes."-";
                            //$clave .= $row_items->operacionDes;
                            $longitud_clave = strlen($clave);
                            $longitud_desc = strlen($row_items->operacionDes);

                            $clave .= (($longitud_desc < 17) ? $longitud_desc : substr($row_items->operacionDes, 0, (20-$longitud_clave)) );
                        }

                        $coleccion["Articulo"] = $clave;                   //Clave del articulo recuperado de intelisis

                        //Por modificaciones de FORD, para los genericos se envia solo el total
                        if ($row_items->tipo == 3) {
                            //Sacamos la diferencia de la MO y el precio
                            $mo = (float)$row_items->total_horas * (float)$row_items->costo_hora;
                            $mo_iva = $mo * 1.16;
                            $precio_unitario = $row_items->total - $mo_iva;

                            $coleccion["Precio"] = (($precio_unitario > 0) ? $precio_unitario/1.16 : 0);
                            //(string)round((($row_items->total > 0) ? $row_items->total/1.16 : 0),2);
                            //
                            $coleccion["Horas"] = $row_items->total_horas;
                            $coleccion["CostoHoras"] = (string)round((($row_items->costo_hora > 0) ? $row_items->costo_hora/1.16 : 0),2); 
                        }else{
                            $coleccion["Precio"] = (string)round((($row_items->precio_unitario > 0) ? $row_items->precio_unitario/1.16 : 0),2); 
                            $coleccion["Horas"] = $row_items->total_horas;
                            $coleccion["CostoHoras"] = (string)round((($row_items->costo_hora > 0) ? $row_items->costo_hora/1.16 : 0),2); 
                        }

                        $coleccion["DescripcionExtra"] = $row_items->descripcion;
                        $coleccion["Agente"] = (($row->agente != NULL) ? $row->agente : "");
                        $coleccion["Sucursal"] = $row->sucursal;

                        //Recopilamos la informacion del articulo
                        $articulos[] = $coleccion;
                        $contador++;
                    }
                }

                $contenedor["Detalle"] = $articulos;

                //Campos nuevos
                if ($row->id_tipo_operacion != "") {
                    $query_temp_6A = $this->mConsultas->get_result("id",$row->id_tipo_operacion,"cat_tipo_operacion");
                    foreach ($query_temp_6A as $row_6A){
                        $dato_6A = $row_6A->tipo_operacion;
                    }
                    //Comprobamos que se haya hecho la consulta
                    if (isset($dato_6A)) {
                        $tipo_operacion = $this->quitar_tildes($dato_6A);
                    }else {
                        $tipo_operacion = "";
                    }
                }else {
                    $tipo_operacion = "";
                }

                $contenedor["ServicioTipoOperacion"] = $tipo_operacion;
                $contenedor["Torre"] = $row->numero_interno;
                $tecnico = explode(".",$row->tecnico);

                /*$tecnico = explode(".",$row->tecnico);
                if (count($tecnico)>1) {
                    $contenedor["Tecnico"] = str_replace("", "" ,$tecnico[0]);
                }else {
                    $contenedor["Tecnico"] = "";
                }*/
                
                $contenedor["Tecnico"] = $row->num_tec;
                
                // Campo nuevo
                $contenedor["Anio"] = $row->vehiculo_anio;
                // Identificador de la sucursal
                $contenedor["Empresa"] = 'G';
            }
            // $contenedor["respuesta"] = "OK";
        } catch (\Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $respuesta = json_encode($contenedor);
        echo $respuesta;
        var_dump($contenedor);
        // $this->envioOrdenIntelisis($contenedor,$idOrden);
    }

    //Cargamos los datos de la multipunto
    public function loadDataMultipunto_estructura()
    {
        //Creamos soporte de tiempo
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);

        ob_start("ob_gzhandler");

        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = [];

        try {
            $idOrden = $_POST["idOrden"];
            //Cargamos la información del formulario
            $query = $this->mConsultas->multipuntoApi($idOrden); 

            foreach ($query as $row){
                $contenedor["Login"] = array(
                    "usuario" => "user",
                    "password" => "1234567"
                );

                $tecnico = explode(".",$row->tecnico);
                $claveTec = str_replace("", "" ,$tecnico[0]);

                if ($row->bateriaEstado == "Aprobado") {
                    $bateria = "Verde";
                }elseif ($row->bateriaEstado == "Futuro") {
                    $bateria = "Amarillo";
                }else {
                    $bateria = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*1),       // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Baterias",             //Elemento
                    "respuesta" => $bateria,               //Indicador de color
                    "estatus" =>  ($row->bateriaCambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "",                        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->bateria."%",   //Valores adicionales del elemento
                );

                if ($row->espesoBalataFI == "Aprobado") {
                    $balatas = "Verde";
                }elseif ($row->espesoBalataFI == "Futuro") {
                    $balatas = "Amarillo";
                }else {
                    $balatas = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*2),       // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Balata",             //Elemento
                    "respuesta" => $balatas,               //Indicador de color
                    "estatus" =>  ($row->espesorBalataFIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Frente Izquierdo",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorBalataFImm." mm",   //Valores adicionales del elemento
                );

                if ($row->espesorDiscoFI == "Aprobado") {
                    $disco = "Verde";
                }elseif ($row->espesorDiscoFI == "Futuro") {
                    $disco = "Amarillo";
                }else {
                    $disco = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*3),      // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Disco",                //Elemento
                    "respuesta" => $disco,                //Indicador de color
                    "estatus" =>  ($row->espesorDiscoFIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Frente Izquierdo",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorDiscoFImm." mm",   //Valores adicionales del elemento
                );

                if ($row->espesoBalataTI == "Aprobado") {
                    $balatas = "Verde";
                }elseif ($row->espesoBalataTI == "Futuro") {
                    $balatas = "Amarillo";
                }else {
                    $balatas = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*4),       // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Balata",             //Elemento
                    "respuesta" => $balatas,               //Indicador de color
                    "estatus" =>  ($row->espesorBalataTIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Trasera Izquierda",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorBalataTImm." mm",   //Valores adicionales del elemento
                );

                if ($row->espesorDiscoTI == "Aprobado") {
                    $disco = "Verde";
                }elseif ($row->espesorDiscoTI == "Futuro") {
                    $disco = "Amarillo";
                }else {
                    $disco = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*5),      // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Disco",                //Elemento
                    "respuesta" => $disco,                //Indicador de color
                    "estatus" =>  ($row->espesorDiscoTIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Trasera Izquierda",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorDiscoTImm." mm",   //Valores adicionales del elemento
                );

                if ($row->diametroTamborTI == "Aprobado") {
                    $tambor = "Verde";
                }elseif ($row->diametroTamborTI == "Futuro") {
                    $tambor = "Amarillo";
                }else {
                    $tambor = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*6),      // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Tambor",                //Elemento
                    "respuesta" => $tambor,                //Indicador de color
                    "estatus" =>  ($row->diametroTamborTIcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Trasera Izquierda",        //Parte del vehículo del elemento, vacio para unico elemento
                );

                if ($row->espesoBalataFD == "Aprobado") {
                    $balatas = "Verde";
                }elseif ($row->espesoBalataFD == "Futuro") {
                    $balatas = "Amarillo";
                }else {
                    $balatas = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*7),       // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Balata",             //Elemento
                    "respuesta" => $balatas,               //Indicador de color
                    "estatus" =>  ($row->espesorBalataFDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Frente Derecho",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorBalataFDmm." mm",   //Valores adicionales del elemento
                );

                if ($row->espesorDiscoFD == "Aprobado") {
                    $disco = "Verde";
                }elseif ($row->espesorDiscoFD == "Futuro") {
                    $disco = "Amarillo";
                }else {
                    $disco = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*8),      // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Disco",                //Elemento
                    "respuesta" => $disco,                //Indicador de color
                    "estatus" =>  ($row->espesorDiscoFDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Frente Derecho",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorDiscoFDmm." mm",   //Valores adicionales del elemento
                );

                if ($row->espesoBalataTD == "Aprobado") {
                    $balatas = "Verde";
                }elseif ($row->espesoBalataTD == "Futuro") {
                    $balatas = "Amarillo";
                }else {
                    $balatas = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis,         //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*9),       // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Balata",             //Elemento
                    "respuesta" => $balatas,               //Indicador de color
                    "estatus" =>  ($row->espesorBalataTDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Trasera Derecha",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorBalataTDmm." mm",   //Valores adicionales del elemento
                );

                if ($row->espesorDiscoTD == "Aprobado") {
                    $disco = "Verde";
                }elseif ($row->espesorDiscoTD == "Futuro") {
                    $disco = "Amarillo";
                }else {
                    $disco = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*10),      // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Disco",                //Elemento
                    "respuesta" => $disco,                //Indicador de color
                    "estatus" =>  ($row->espesorDiscoTDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Trasera Derecha",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorDiscoTDmm." mm",   //Valores adicionales del elemento
                );

                if ($row->diametroTamborTD == "Aprobado") {
                    $tambor = "Verde";
                }elseif ($row->diametroTamborTD == "Futuro") {
                    $tambor = "Amarillo";
                }else {
                    $tambor = "Rojo";
                }

                $contenedor["inn"][] = array(
                    "idventa" => $row->idVentaIntelisis, //ESTE ES NUESTRO NUMERO DE ORDEN INTERNO
                    "idrevision" => (string)(2024*11),      // ESTE ES UN CONSECUTIVO de 2024
                    "tipo" =>  "" ,                       // ? Vacio
                    "prioridad" => "",                    // ? Vacio
                    "elemento" => "Tambor",                //Elemento
                    "respuesta" => $tambor,                //Indicador de color
                    "estatus" =>  ($row->diametroTamborTDcambio == 0) ? "No cambiado" : "Cambiado",     //Estatus Cambiado (si se cambio la pieza) / No cambiado
                    "tecnico" => $row->num_tec, //$claveTec,       //Técnico
                    "lado" =>  "Trasera Derecha",        //Parte del vehículo del elemento, vacio para unico elemento
                    //"datos_extra" => $row->espesorDiscoTDmm." mm",   //Valores adicionales del elemento
                );
                // Identificador de la sucursal
                $contenedor["Empresa"] = 'G';

            }

            if (!isset($contenedor["Login"])) {
              $contenedor["respuesta"] = "NO SE ENCONTRO LA ORDEN";
            }
          } catch (\Exception $e) {
              $contenedor["respuesta"] = "ERROR EN EL PROCESO";
              // $respuesta = json_encode($contenedor);
              // echo $respuesta;
          }

          $comprimido[] = $contenedor;
          $respuesta = json_encode($contenedor);
          echo $respuesta;
    }

    //Quitar acentos
    public function quitar_tildes($cadena = "")
    {
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹","�");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E","?");
        $texto = str_replace($no_permitidas, $permitidas ,$cadena);
        return $texto;
    }

    //Quitar saltos de linea y tabulaciones
    public function quitar_saltos($cadena = "")
    {
        $no_permitidas= array ("\n","\r");
        $texto = str_replace($no_permitidas, "" ,$cadena);
        return $texto;
    }


}
