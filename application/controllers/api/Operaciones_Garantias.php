<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operaciones_Garantias extends CI_Controller { 

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'consulta', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL); 
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
    }

    public function index()
    {
          
    }


    //// -----------------------------------------------------------------------------
    ///                 Orden de servicio para garantias
    //// -----------------------------------------------------------------------------
    //Comprobamos que al final se genera la vista de revision
    public function setDataRevisionFirmas($id_cita_url = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $datos["tipo_form"] = "3";
        $datos["destino"] = "Garantias";
        $id_cita = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
        $this->setData($id_cita,$datos);
    }

    public function setDataRevisionTecnico($id_cita_url = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $datos["tipo_form"] = "3";
        $datos["destino"] = "Tecnico";
        $id_cita = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
        $this->setData($id_cita,$datos);
    }

    public function setDataRevisionJefeTaller($id_cita_url = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $datos["tipo_form"] = "3";
        $datos["destino"] = "JefeTaller";
        $id_cita = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
        $this->setData($id_cita,$datos);
    }

    public function setDataFirmasPDF($id_cita_url = 0)
    {
        $datos["destinoVista"] = "PDF";
        $datos["tipo_form"] = "4";
        $datos["destino"] = "Garantias";
        $id_cita = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));
        $this->setData($id_cita,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData($idServicio = 0,$datos = NULL)
    {
        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
            $datos["tipo_form"] = "2";
        }

        if (!isset($datos["destino"])) {
            $datos["destino"] = "Garantia";
        }

        //Cargamos la información del formulario
        $query = $this->consulta->get_result("noServicio",$idServicio,"diagnostico");
        foreach ($query as $row){
            $fecha = explode(" ",$row->fecha_diagnostico);
            $datos["fecha_diagnostico"] = $fecha[0];
            $fecha_2 = explode(" ",$row->fecha_aceptacion);
            $datos["fecha_aceptacion"] = $fecha_2[0];

            $datos["id_cita"] = $row->noServicio;
            $datos["tipo_servicio"] = $row->vehiculo;
            $datos["dias"] = $row->dias;
            $datos["terminos"] = $row->terminos;
            $datos["firmaDiagnostico"] = (file_exists($row->firmaDiagnostico)) ? $row->firmaDiagnostico : "";
            $datos["firmaConsumidor1"] = (file_exists($row->firmaConsumidor1)) ? $row->firmaConsumidor1 : "";
            $datos["danos"] = $row->danos;
            $datos["danosImg"] = (file_exists($row->danosImg)) ? $row->danosImg : "";

            $datos["interiores"] = explode("_",$row->interiores);
            $datos["cajuela"] = explode("_",$row->cajuela);
            $datos["exteriores"] = explode("_",$row->exteriores);
            $datos["documentacion"] = explode("_",$row->documentacion);

            $datos["articulos"] = $row->articulos;
            $datos["cualesArticulos"] = $row->cualesArticulos;
            $datos["reporte"] = $row->reporte;
            $datos["costo"] = $row->costo;
            $datos["diaValue"] = $row->diaValue;
            $datos["mesValue"] = $row->mesValue;
            $datos["anioValue"] = $row->anioValue;
            $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";
            $datos["firmaConsumidor2"] = (file_exists($row->firmaConsumidor2)) ? $row->firmaConsumidor2 : "";
            $datos["nivelGasolina"] = $row->nivelGasolina;
            $datos["nivelGasolinaDiv"] = explode("/",$row->nivelGasolina);
            $datos["nombreDiagnostico"] = $row->nombreDiagnostico;
            $datos["nombreConsumido1"] = $row->nombreConsumido1;
            $datos["nombreAsesor"] = $row->nombreAsesor;
            $datos["nombreConsumidor2"] = $row->nombreConsumidor2;

            //$datos["autorizacion_ausencia"] = $row->autorizacion_ausencia;
            //$datos["motivo_ausencia"] = $row->motivo_ausencia;

            $datos["idServicio"] = $row->idDiagnostico;
        }

        $datos["limiteAno"] = date("Y");

        //Consultamos si ya se entrego la unidad
        $datos["UnidadEntrega"] = $this->consulta->Unidad_entregada($idServicio);

        $this->loadDataContrac($idServicio,$datos);
    }

    //Recuperamos la informacion del contrato
    public function loadDataContrac($idServicio = 0,$datos = NULL)
    {
        //Cargamos la información del formulario
        $query = $this->consulta->get_result("identificador",$idServicio,"contrato"); 
        foreach ($query as $row){
            $fecha = new DateTime($row->fecha);
            $datos["fecha"] = $fecha->format('d/m/Y');
            $datos["fecha_f"] = $row->fecha;

            $datos["folio"] = $row->folio;
            $datos["hora"] = $row->hora;
            $datos["empresa"] = $row->empresa;
            $datos["pagoConsumidor"] = $row->pagoConsumidor;
            $datos["condicionalInfo"] = $row->condicionalInfo;
            $datos["condicionalPublicidad"] = $row->condicionalPublicidad;
            $datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : "";
            $datos["nombreDistribuidor"] = $row->nombreDistribuidor;
            $datos["firmaDistribuidor"] = (file_exists($row->firmaDistribuidor)) ? $row->firmaDistribuidor : "";
            $datos["nombreConsumidor"] = $row->nombreConsumidor;
            $datos["firmaConsumidor"] = (file_exists($row->firmaConsumidor)) ? $row->firmaConsumidor : "";
            $datos["aceptaTerminos"] = $row->aceptaTerminos;
            $datos["noRegistro"] = $row->noRegistro;
            $datos["noExpediente"] = $row->noExpediente;
            $datos["fechDia"] = $row->fechDia;
            $datos["fechMes"] = $row->fechMes;
            $datos["fechAnio"] = $row->fechAnio;
            $datos["checkImg"] = base_url().'assets/imgs/Check.png';
            $datos["checkImgA"] = base_url().'assets/imgs/check2.png';
            $datos["terminos_contrato"] = $row->terminos_contrato;
        }

        $this->loadDataPart1($idServicio,$datos);
    }

    //Recuperamos la parte de pita
    public function loadDataPart1($idServicio = 0,$datos = NULL)
    {
        //Recuperamos el id de la orden de servicio
        $precarga = $this->consulta->get_result('id_cita',$idServicio,'ordenservicio'); 
        foreach ($precarga as $row){
            $idOrden = $row->id;
            $iva = $row->iva/100;
            $datos["orden_id_tipo_orden"] = $row->id_tipo_orden;
            $datos["idIntelisis"] = $row->folioIntelisis;
            $idCategoria = $row->idcategoria;
        }

        $datos["totalRenglones"] = 0;
        $datos["total_items"] = 0;
        $datos["subtotal_items"] = 0;
        $datos["iva_items"] = 0;
        $datos["presupuesto_items"] = 0;
        $datos["orden_anticipo"] = 0;

        if (isset($idOrden)) {
            //if ($datos["orden_id_tipo_orden"] == "2") { 
                //Recuperamos el id de la categoria
                $Categoria = $this->consulta->get_result('id',$idCategoria,'categorias');
                foreach ($Categoria as $row){
                    $datos["orden_categoria"] = $row->categoria;
                }

                //Recuperamos la información de la cita
                $infoCita = $this->consulta->citas($idOrden);
                foreach ($infoCita as $row) {
                    $datos["orden_id_cita"] = $row->id_cita;
                    //Recuperamos todas las campañas del vehiculo
                    $campanias = $this->consulta->get_result('vin',$row->vehiculo_numero_serie,'campanias');
                    $datos["nombreCampania"] = "[";
                    foreach ($campanias as $row_1) {
                        $datos["nombreCampania"] .= $row_1->tipo.",";
                    }

                    $datos["nombreCampania"] .= "]";

                    $datos["orden_tecnico"] = $row->tecnico;

                    $datos["orden_campania"] = $row->campania;
                    $datos["orden_asesor"] = $row->asesor;
                    $datos["orden_telefono_asesor"] = $row->telefono_asesor;
                    $datos["orden_extension_asesor"] = $row->extension_asesor;
                    //torre
                    $datos["orden_numero_interno"] = $row->numero_interno."-".$row->color;
                    $datos["orden_fecha_recepcion"] = $row->fecha_recepcion;
                    $datos["orden_hora_recepcion"] = $row->hora_recepcion;
                    $datos["orden_fecha_entrega"] = $row->fecha_entrega;
                    $datos["orden_hora_entrega"] = $row->hora_entrega;
                    $datos["orden_datos_nombres"] = $row->datos_nombres;
                    $datos["orden_datos_apellido_paterno"] = $row->datos_apellido_paterno;
                    $datos["orden_datos_apellido_materno"] = $row->datos_apellido_materno;
                    $datos["orden_nombre_compania"] = $row->nombre_compania;
                    $datos["orden_nombre_contacto_compania"] = $row->nombre_contacto_compania;
                    $datos["orden_ap_contacto"] = $row->ap_contacto;
                    $datos["orden_am_contacto"] = $row->am_contacto;

                    $datos["orden_datos_email"] = $row->datos_email;
                    $datos["orden_rfc"] = $row->rfc;
                    $datos["orden_correo_compania"] = $row->correo_compania;

                    $datos["orden_domicilio"] = $row->calle." ".$row->noexterior." ".$row->nointerior.", ".$row->colonia;
                    $datos["orden_municipio"] = $row->municipio;
                    $datos["orden_cp"] = $row->cp;
                    $datos["orden_estado"] = $row->estado;
                    $datos["orden_telefono_movil"] = $row->telefono_movil;
                    $datos["orden_otro_telefono"] = $row->otro_telefono;
                    $datos["orden_vehiculo_placas"] = $row->vehiculo_placas;
                    // $datos["orden_vehiculo_identificacion"] = $row->vehiculo_numero_serie;
                    $datos["orden_vehiculo_identificacion"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : "Sin serie";
                    $datos["orden_vehiculo_kilometraje"] = $row->km;
                    $datos["orden_vehiculo_marca"] =  $row->vehiculo_marca; 
                    $datos["orden_vehiculo_anio"] = $row->vehiculo_anio;

                    //Otras variables
                    $datos["orden_anticipo"] += (float)$row->anticipo;
                    $datos["orden_confirmada"] = $row->cotizacion_confirmada;
                    $idColor = $row->id_color;

                    $datos["orden_asociada"] = $row->orden_asociada;

                }

                if (isset($datos["orden_confirmada"])) {
                    //Recuperamos el estatus de la cotización
                    $datos["orden_confirmada"] = $this->consulta->EstatusCotizacionUser($idOrden);
                    $items = $this->consulta->articulosTitulos($idOrden); 

                    foreach ($items as $row) {
                        if ($datos["orden_confirmada"] == "1") {
                            $datos["item_descripcion"][] = $row->descripcion;

                            if ($row->tipo == 3) {
                                $datos["gp_items"][] = "GEN.";
                                $datos["op_items"][] = "";

                                $datos["item_precio_unitario"][] = number_format(($row->total/1.16),2);
                            }elseif ($row->tipo == 5) {
                                $datos["gp_items"][] = $row->grupo;
                                $datos["op_items"][] = $row->descripcion;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }elseif ($row->tipo == 4) {
                                $datos["gp_items"][] = $row->grupo;
                                $datos["op_items"][] = $row->tipo_operacion;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }else {
                                $datos["gp_items"][] = $row->grupoDes;
                                $datos["op_items"][] = $row->operacionDes;

                                $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            }

                            //$datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                            $datos["item_cantidad"][] = number_format($row->cantidad,2);
                            $datos["item_descuento"][] = number_format($row->descuento,2);
                            $datos["item_operacion"][] = ($row->operacion!='') ? 'Si': 'No';
                            $datos["item_total"][] = number_format($row->total,2);

                            //Nvo procedimiento
                            $datos["total_items"] += $row->total;
                            $datos["subtotal_items"] = $datos["total_items"] / 1.16;
                            $datos["iva_items"] = $datos["total_items"] - $datos["subtotal_items"];
                            // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];
                        } else {
                            //Si la cotizacion no esta confirmada, solo lo tipo 1
                            if(($row->tipo == 1)||($row->tipo == 3)||($row->tipo == 5)||($row->tipo == 4)){
                                $datos["item_descripcion"][] = $row->descripcion;

                                if ($row->tipo == 3) {
                                    $datos["gp_items"][] = "GEN.";
                                    $datos["op_items"][] = "";

                                    $datos["item_precio_unitario"][] = number_format(($row->total/1.16),2);
                                }elseif ($row->tipo == 5) {
                                    $datos["gp_items"][] = $row->grupo;
                                    $datos["op_items"][] = $row->descripcion;

                                    $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                                }elseif ($row->tipo == 4) {
                                    $datos["gp_items"][] = $row->grupo;
                                    $datos["op_items"][] = $row->tipo_operacion;

                                    $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                                }else {
                                    $datos["gp_items"][] = $row->grupoDes;
                                    $datos["op_items"][] = $row->operacionDes;

                                    $datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                                }

                                //$datos["item_precio_unitario"][] = number_format($row->precio_unitario,2);
                                $datos["item_cantidad"][] = number_format($row->cantidad,2);
                                $datos["item_descuento"][] = number_format($row->descuento,2);
                                $datos["item_operacion"][] = ($row->operacion!='') ? 'Si': 'No';
                                $datos["item_total"][] = number_format($row->total,2);

                                //Nvo procedimiento
                                $datos["total_items"] += $row->total;
                                $datos["subtotal_items"] = $datos["total_items"] / 1.16;
                                $datos["iva_items"] = $datos["total_items"] - $datos["subtotal_items"];
                                // $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];
                            }
                        }
                    }

                    if (isset($datos["item_total"])) {
                        $datos["totalRenglones"] = count($datos["item_total"]);
                    }

                    $datos["Coti_1"] = $datos["totalRenglones"];

                    //Comprobamos que se cargaran los datos
                    if (isset($datos["orden_id_cita"])) {
                        //Recuperamos el color
                        $queryColor  = $this->consulta->get_result('id',$idColor,'cat_colores');
                        foreach ($queryColor as $row) {
                            $datos["color"] = $row->color;
                        }

                        // $datos["color"] = $this->consulta->getColorCitaAuto($datos["orden_id_tipo_orden"]);

                        //Recuperamos los tecnicos
                        $datos["tecnicos"] = $this->consulta->tecnicos($datos["orden_id_cita"]);

                        //Recuperamos el tipo de orden
                        $datos["orden_tipo_orden"] = $this->consulta->getTipoOrden($datos["orden_id_tipo_orden"]);
                        $datos["orden_identificador"] = $this->consulta->getTipoOrdenIdentificacion($datos["orden_id_tipo_orden"]);

                        //Recuperamos el dato del anticipo
                        // $datos["orden_anticipo"] = $this->consulta->getAnticipoOrden($idOrden);
                    }

                    //Verificamos si es uan orden publica o una garantia
                    if ($datos["orden_id_tipo_orden"] == "2") {
                        $id_cita_presupuesto = $datos["orden_asociada"];
                    } else {
                        $id_cita_presupuesto = $datos["orden_id_cita"];
                    }
                    
                
                    $revision = $this->consulta->get_result_field("id_cita",$id_cita_presupuesto,"identificador","M","presupuesto_registro");
                    foreach ($revision as $row) {
                        $idCotizacion = $row->id;
                        $acepta_cliente = $row->acepta_cliente;
                        $datos["anticipo_HM"] = (float)$row->anticipo;
                        $datos["firma_requisicion"] = $row->firma_requisicion;
                        $datos["firma_requisicion_garantias"] = $row->firma_requisicion_garantias;
                    }

                    if (isset($acepta_cliente)) {
                        $datos["subtotal_HM"] = 0;
                        $datos["presupuesto_HM"] = 0;
                        $datos["iva_HM"] = 0;
                        $datos["pieza_HM"] = [];
                        
                        if (($acepta_cliente == "Si")||($acepta_cliente == "Val")) {
                            //Recuperamos las refacciones
                            $query_presupuesto = $this->consulta->get_result_field("id_cita",$id_cita_presupuesto,"identificador","M","presupuesto_multipunto");
                            foreach ($query_presupuesto as $row_p){
                                if ($row_p->activo == "1") {
                                    //Solo las refacciones aceptadas poe el cliente
                                    if (($row_p->autoriza_cliente == "1")&&($row_p->autoriza_jefeTaller == "1")&&($row_p->autoriza_asesor == "1")&&($row_p->pza_garantia  == "0")) {
                                        //Columnas de la tabla
                                        /*$datos["gp_op_HM"][] = "Refacción";
                                        $datos["gp_tipo"][] = "P";

                                        $datos["id_refaccion"][] = $row_p->id;

                                        $datos["pieza_HM"][] = $row_p->num_pieza;
                                        $datos["descripcion_HM"][] = $row_p->descripcion;
                                        $datos["mo_hm"][] = ($row_p->horas_mo * $row_p->costo_mo);

                                        $datos["precio_HM"][] = $row_p->costo_pieza;
                                        $datos["cantidad_HM"][] = $row_p->cantidad;

                                        $precarga_C = ((float)$row_p->cantidad*(float)$row_p->costo_pieza) + ((float)$row_p->horas_mo*(float)$row_p->costo_mo);

                                        $datos["total_fila_HM"][] = $precarga_C;
                                        //Subtotal sin iva
                                        $datos["subtotal_HM"] += $precarga_C;
                                        //Obetenemos el iva
                                        $datos["iva_HM"] = $datos["subtotal_HM"] * 0.16;
                                        //Sacamos el acumulativo final (subtotal + iva)
                                        $datos["presupuesto_HM"] = $datos["subtotal_HM"] * 1.16;

                                        //$datos["presupuesto_HM"] += $precarga_C;
                                        //$datos["iva_HM"] = $datos["presupuesto_HM"] * 0.16;
                                        //$datos["subtotal_HM"] = $datos["presupuesto_HM"] - $datos["iva_HM"];
                                        
                                        $datos["firma_gerente"][] = $row_p->firma_gerente_garantia;
                                        $datos["firma_jdt"][] = $row_p->firma_jefetaller_garantia;

                                        if (($row_p->firma_gerente_garantia != "")&&($row_p->firma_jefetaller_garantia != "")) {
                                            $datos["ref_finalizada"][] = "1";
                                        } else {
                                            $datos["ref_finalizada"][] = "0";
                                        }*/
                                        
                                    }elseif (($row_p->pza_garantia  == "1")&&($row_p->autoriza_garantia == "1")) {
                                        //Columnas de la tabla
                                        $datos["gp_op_HM"][] = "Refacción";
                                        $datos["gp_tipo"][] = "G";

                                        $datos["id_refaccion"][] = $row_p->id;

                                        $datos["pieza_HM"][] = $row_p->num_pieza;
                                        $datos["descripcion_HM"][] = $row_p->descripcion;
                                        $datos["mo_hm"][] = ($row_p->horas_mo_garantias * $row_p->costo_mo_garantias);

                                        $datos["precio_HM"][] = $row_p->costo_ford;
                                        $datos["cantidad_HM"][] = $row_p->cantidad;

                                        $precarga_C = ((float)$row_p->cantidad*(float)$row_p->costo_ford) + ((float)$row_p->horas_mo_garantias*(float)$row_p->costo_mo_garantias);

                                        $datos["total_fila_HM"][] = $precarga_C;
                                        //Subtotal sin iva
                                        $datos["subtotal_HM"] += $precarga_C;
                                        //Obetenemos el iva
                                        $datos["iva_HM"] = $datos["subtotal_HM"] * 0.16;
                                        //Sacamos el acumulativo final (subtotal + iva)
                                        $datos["presupuesto_HM"] = $datos["subtotal_HM"] * 1.16;

                                        //Firmas para garantias
                                        $datos["firma_gerente"][] = $row_p->firma_gerente_garantia;
                                        $datos["firma_jdt"][] = $row_p->firma_jefetaller_garantia;

                                        if (($row_p->firma_gerente_garantia != "")&&($row_p->firma_jefetaller_garantia != "")) {
                                            $datos["ref_finalizada"][] = "1";
                                        } else {
                                            $datos["ref_finalizada"][] = "0";
                                        }
                                    }
                                }
                            }

                            //Totales
                            $datos["subtotal_items"] = $datos["subtotal_items"] + $datos["subtotal_HM"];
                            $datos["iva_items"] = $datos["iva_items"] + $datos["iva_HM"];
                            $datos["total_items"] = $datos["total_items"] + $datos["presupuesto_HM"];
                            $datos["orden_anticipo"] += $datos["anticipo_HM"];
                            $datos["presupuesto_items"] = $datos["total_items"] - $datos["orden_anticipo"];

                            $datos["totalRenglones"] = $datos["totalRenglones"] + count($datos["pieza_HM"]);
                        }            
                    }
                }

            //Si la orden no es garantia, redireccionamos a la vista normal
            //}else{
                //redirect('OrdenServicio_Revision/'.$this->encrypt($idServicio), 'refresh');
            //}  
        }

        if ($datos["destinoVista"] == "PDF") {
            //$this->loadAllView($datos,'ordenServicio/plantillaPdf_garantias_2');
            $this->load->view("ordenServicio/plantillaPdf_garantias_2", $datos); 
        } else {
            $this->loadAllView($datos,'ordenServicio/plantillaRevision_Garantia');
        }
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Ediciones adicionales de garantias en presupuestos ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function firma_gerente()
    {
        $id_cita = $_POST["orden"];
        $id_refaccion = $_POST["renglon"];
        $nombre_firma = $_POST["encargado"];
        $firma = $this->base64ToImage($_POST["firma"],"firmas");

        $actualizacion = array(
            "firma_gerente_garantia" => $firma,
            "nombre_gerente" => $nombre_firma,
            "fecha_firma_gerente" => date("Y-m-d H:i:s")
        );

        $actualizar = $this->consulta->update_table_row('presupuesto_multipunto',$actualizacion,'id',$id_refaccion);
        if ($actualizar) {
            $id_cita_publica = $this->consulta->id_cita_refaccion($id_refaccion);
            $movimiento = "Se firma (gerente de servicio) en orden de servicio, refaccion para garantias, id refaccion : ".$id_refaccion. " firmado por: ".$nombre_firma." De la orden de garantía # ".$id_cita;
            //$this->registrar_movimiento($id_cita,$movimiento,"Garantias");
            $this->registrar_movimiento($id_cita_publica,$movimiento,"Garantias");
            $respuesta = "OK";
        } else {
            $respuesta = "NO SE PUDO ACTUALIZAR LA FIRMA";
        }
        echo $respuesta;
    }

    public function firma_jefetaller()
    {
        $id_cita = $_POST["orden"];
        $id_refaccion = $_POST["renglon"];
        $nombre_firma = $_POST["encargado"];
        $firma = $this->base64ToImage($_POST["firma"],"firmas");

        $actualizacion = array(
            "firma_jefetaller_garantia" => $firma,
            "nombre_jefetaller" => $nombre_firma,
            "fecha_firma_jefetaller" => date("Y-m-d H:i:s")
        );

        $actualizar = $this->consulta->update_table_row('presupuesto_multipunto',$actualizacion,'id',$id_refaccion);
        if ($actualizar) {
            $id_cita_publica = $this->consulta->id_cita_refaccion($id_refaccion);
            $movimiento = "Se firma (jefe de taller) en orden de servicio, refaccion para garantias, id refaccion : ".$id_refaccion. " firmado por: ".$nombre_firma." De la orden de garantía # ".$id_cita;
            //$this->registrar_movimiento($id_cita,$movimiento,"Garantias");
            $this->registrar_movimiento($id_cita_publica,$movimiento,"Garantias");
            $respuesta = "OK";
        } else {
            $respuesta = "NO SE PUDO ACTUALIZAR LA FIRMA";
        }
        echo $respuesta;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        }elseif ($data[0] == "data:image/jpeg;base64") {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".jpeg";

                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    public function adicionar_refaccion()
    {
        //Recuperamos las refacciones agregadas en garantias
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,API_GARANTIA."apis/partes_operacines?numero_orden=".$_GET["orden_garantia"]); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $response = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        $dataregistro = json_decode($response, TRUE);

        $registro = [];

        $reg_refaccion = FALSE;
        $valida_edicion_2 = "";

        //Verificamos el tipo de orden
        $tipo_orden = $this->consulta->tipo_orden($_GET["orden_garantia"]);

        if ($tipo_orden == "2") {
            $orden_publica = $this->consulta->garantia_asociada($_GET["orden_garantia"]);
            $valida_edicion = $this->verificar_proceso_garantia($orden_publica);
        }else{
            $orden_publica = $_GET["orden_garantia"];
            $valida_edicion = $this->verificar_proceso_garantia($_GET["orden_garantia"]);
        }

        //$valida_edicion = $this->verificar_proceso_garantia($_GET["orden_garantia"]);
        
        //Si no se encuentra el presupuesto con el id cita proporcionado ..
        /*if ($valida_edicion == "ERROR") {
            $orden_garantia = $this->consulta->publica_asociada($_GET["orden_garantia"]);
            $valida_edicion_2 = $this->verificar_proceso_garantia($orden_garantia);
            if ($valida_edicion_2 == "ERROR") {
                $orden_garantia = $this->consulta->garantia_asociada($_GET["orden_garantia"]);
                $valida_edicion_2 = $this->verificar_proceso_garantia($orden_garantia);
            }
        }else{
            $valida_edicion_2 = "";
        }*/

        if (($valida_edicion == "OK")||($valida_edicion_2 == "OK")) {
            if (isset($dataregistro["data"])) {
                for ($i=0; $i < count($dataregistro["data"]); $i++) {
                    if ($dataregistro["data"][$i]["numero_orden_asociada"] != "") {
                        $orden_presupuesto = $dataregistro["data"][$i]["numero_orden_asociada"];
                        $orden_origen = $dataregistro["data"][$i]["numero_orden_origen"];

                        //Verificamos que las refacciones no sean del mismo presupuesto
                        if ($orden_presupuesto != $orden_origen) {
                            $reg_refaccion = $this->consulta->buscar_ref_adicioanl($orden_presupuesto,$dataregistro["data"][$i]["partes_operacion_id"]);
                        } else {
                            $reg_refaccion = FALSE;
                        }

                    //} elseif ($dataregistro["data"][$i]["numero_orden_origen"] != "") {
                        //$orden_presupuesto = $dataregistro["data"][$i]["numero_orden_origen"];
                        //$reg_refaccion = $this->consulta->buscar_ref_adicioanl($orden_presupuesto,$dataregistro["data"][$i]["partes_operacion_id"]);

                    } else{
                        $orden_presupuesto = $dataregistro["data"][$i]["numero_orden"];
                        $orden_origen = $dataregistro["data"][$i]["numero_orden_origen"];
                        
                        //Verificamos que las refacciones no sean del mismo presupuesto
                        if ($orden_presupuesto != $orden_origen) {
                            $reg_refaccion = $this->consulta->buscar_ref_adicioanl($orden_presupuesto,$dataregistro["data"][$i]["partes_operacion_id"]);
                        } else {
                            $reg_refaccion = FALSE;
                        }
                    }

                    //Si no existe la refaccion, la creamos
                    if (!$reg_refaccion) {
                        $presupuesto = $this->consulta->existencia_presupuesto($orden_presupuesto);

                        //Si existe el presupuesto, solo agregamos la refaccion
                        if ($presupuesto) {
                            //Agregamos la refaccion
                            if ($dataregistro["data"][$i]["nombre"] != "") {
                                $registro["data"][] = $this->guardar_refaccion_presupuesto($dataregistro["data"][$i],$orden_presupuesto);
                            }
                        //De lo contrario, creamos el presupuesto completo
                        } else {
                            if ($dataregistro["data"][$i]["nombre"] != "") {
                                $registro["data"][] = $this->crear_presupuesto($dataregistro["data"][$i],$orden_presupuesto);
                            }
                        }
                        
                    //Si existe no hacemos nada
                    } else {
                        $registro["data"][] = array(
                            "partes_operacion_id" => $dataregistro["data"][$i]["partes_operacion_id"],
                            "id_refaccion_presupuesto" => NULL,
                            "mensaje" => "YA EXISTE LA REFACCION EN EL PRESUPUESTO"
                        );
                    }
                }
            }else{
                $registro["data"][] = array(
                    "partes_operacion_id" => $dataregistro["data"][$i]["partes_operacion_id"],
                    "id_refaccion_presupuesto" => NULL,
                    "mensaje" => "NO SE RECIBIO LA INFORMACIÓN CORRECTA"
                );
            }
        } else {
            $registro["data"][] = array(
                "partes_operacion_id" => ((isset($orden_publica)) ? $orden_publica : $_GET["orden_garantia"]),
                "id_refaccion_presupuesto" => NULL,
                "mensaje" => (($valida_edicion != "ERROR") ? $valida_edicion : $valida_edicion_2)
            );
        }

        echo json_encode($registro);
    }

    public function guardar_refaccion_presupuesto($data = NULL,$id_cita = "")
    {
        //Revisamos que no se dupliquen
        $query = $this->consulta->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_multipunto");
        foreach ($query as $row){
            $refaccion_descripcion[] = strtoupper(trim($row->descripcion));
            $id_refacciones[] = $row->id;
        }

        $descripcion = strtoupper(trim($data["nombre"]));

        if (!in_array($descripcion, $refaccion_descripcion)) {
            //Guardamos la refaccion 
            $registro = array(
                "id" => NULL,
                "id_cita" => $id_cita,
                "id_garantia" => $data["partes_operacion_id"],
                "identificador" => "M",
                "alta_garantia" => 1,
                "ref" => (($data["reporte_numero"] != "") ? $data["reporte_numero"] : NULL),
                "cantidad" => (($data["cantidad"] != "") ? $data["cantidad"] : 0),
                "descripcion" => $data["nombre"],
                "prefijo" => $data["prefijo"],
                "sufijo" => $data["sufijo"],
                "basico" => $data["basico"],
                "num_pieza" => $data["prefijo"]."".$data["basico"]."".$data["sufijo"],
                "existe" => "",
                "fecha_planta" => "0000-00-00",
                "costo_pieza" => 0,
                "horas_mo" => 0,
                "costo_mo" => 910,
                "total_refacion" => 0, //$data["importe_partes"],
                "pza_garantia" => 1,
                "costo_ford" => $data["precio_cu"],
                "horas_mo_garantias" => $data["tiempo_tarifa"],
                "costo_mo_garantias" => 0,
                "autoriza_garantia" => 1,
                "autoriza_jefeTaller" => 1,
                "autoriza_asesor" => 1,
                "autoriza_cliente" => 1,
                "tipo_precio" => "G",
                "prioridad" => 2,
                "estado_entrega" => 0,
                "activo" => 1,
                "fecha_registro" => date("Y-m-d H:i:s"),
                "fecha_registro_garantia" => date("Y-m-d H:i:s"),
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            //Guardamos el registro de la refaccion
            $id_retorno = $this->consulta->save_register('presupuesto_multipunto',$registro);

            if ($id_retorno != 0) {
                //Actualizamos la informacion general del presupuesto
                $query = $this->consulta->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
                foreach ($query as $row){
                    $tradicionales = $row->ref_tradicional;
                    $garantias = (int)$row->ref_garantias;

                    $comentarios = $row->comentario_garantia;

                    if ($row->acepta_cliente != "") {
                        if ($row->acepta_cliente == "No") {
                            $afecta_cliente = "Val";
                        } else {
                            $afecta_cliente = $row->acepta_cliente;
                        }
                        
                    }else{
                        $afecta_cliente = "";
                    }
                }

                if (isset($garantias)) {
                    $garantias++;
                    $comentarios_adicioanles = $comentarios. '<br>' . 'Se agrega refacción desde el módulo de garantías: '.$data["nombre"].' Observaciones del cliente: '.(($data["queja_cliente"] != NULL) ? $data["queja_cliente"] : "Sin Observaciones");

                    $actualizacion = array(
                        "anexa_garantias" => 1,
                        "ref_garantias" => $garantias,
                        "ref_tradicional" => $tradicionales,
                        "envia_ventanilla" => 3,
                        "estado_refacciones" => 0,
                        "estado_refacciones_garantias" => 0,
                        "firma_requisicion" => "",
                        "firma_requisicion_garantias" => "",
                        "comentario_garantia" => $comentarios_adicioanles,
                        "acepta_cliente" => $afecta_cliente,
                        "ubicacion_proceso" => "Se agrego una refacción desde garantías. Espera autorización de ventanilla.",
                        "ubicacion_proceso_garantia" => "Se agregarón refacciones",
                        "fecha_actualiza" => date("Y-m-d H:i:s"),
                    );

                    $actualizar = $this->consulta->update_table_row_2('presupuesto_registro',$actualizacion,"id_cita",$id_cita,"identificador","M");

                    //Recuperamos datos extras para la notificación
                    $query = $this->consulta->notificacionTecnicoCita($id_cita); 
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $placas = $row->vehiculo_placas;
                        $asesor = $row->asesor;
                    }

                    if (isset($tecnico)) {
                        $texto = SUCURSAL.".@@@URGENTE@@@  Presupuesto de la Orden: ".$id_cita." se agrega una refacción desde el módulo de garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                        //Gerente de servicio
                        //$this->sms_general('3315273100',$texto);
                        
                        //Ventanilla
                        $this->sms_general('3316721106',$texto);
                        $this->sms_general('3319701092',$texto);
                        $this->sms_general('3317978025',$texto);
                        
                        //Tester
                        $this->sms_general('3328351116',$texto);
                        //Angel evidencia
                        //$this->sms_general('5535527547',$texto);
                    }

                    $movimiento = "Presupuesto: Se agrega una refacción desde el módulo de garantías";
                    $this->registrar_movimiento($id_cita,$movimiento,"Garantías");
                }

                $refaccion = array(
                    "partes_operacion_id" => $data["partes_operacion_id"],
                    "id_refaccion_presupuesto" => $id_retorno,
                    "mensaje" => "OK"
                );

            }else{
                $refaccion = array(
                    "partes_operacion_id" => $data["partes_operacion_id"],
                    "id_refaccion_presupuesto" => NULL,
                    "mensaje" => "OCURRIO UN ERROR AL GUARDAR LA INFORMACIÓN"
                );
            }
        }else{
            $id = array_search($descripcion, $refaccion_descripcion);
            $id_ref = $id_refacciones[$id];
            
            $actualizacion = array(
                "id_garantia" => $data["partes_operacion_id"],
                "alta_garantia" => 2,
                "ref" => (($data["reporte_numero"] != "") ? $data["reporte_numero"] : NULL),
                "costo_ford" => $data["precio_cu"],
                "horas_mo_garantias" => $data["tiempo_tarifa"],
                "costo_mo_garantias" => 0,
                "autoriza_garantia" => 1,
                "activo" => 1,
                "fecha_actualiza" => date("Y-m-d H:i:s")
            );

            $actualizar = $this->consulta->update_table_row('presupuesto_multipunto',$actualizacion,'id',$id_ref);

            $refaccion = array(
                "partes_operacion_id" => $data["partes_operacion_id"],
                "id_refaccion_presupuesto" => NULL,
                "mensaje" => "PIEZA YA REGISTRADA"
            );
        }

        return $refaccion;
    }

    public function crear_presupuesto($data = NULL,$id_cita = "")
    {
        //Guardamos la refaccion 
        $registro = array(
            "id" => NULL,
            "id_cita" => $id_cita,
            "id_garantia" => $data["partes_operacion_id"],
            "identificador" => "M",
            "alta_garantia" => 1,
            "ref" => (($data["reporte_numero"] != "") ? $data["reporte_numero"] : NULL),
            "cantidad" => (($data["cantidad"] != "") ? $data["cantidad"] : 0),
            "descripcion" => $data["nombre"],
            "prefijo" => $data["prefijo"],
            "sufijo" => $data["sufijo"],
            "basico" => $data["basico"],
            "num_pieza" => $data["prefijo"]."".$data["basico"]."".$data["sufijo"],
            "existe" => "",
            "fecha_planta" => "0000-00-00",
            "costo_pieza" => 0,
            "horas_mo" => 0,
            "costo_mo" => 910,
            "total_refacion" => 0, //$data["importe_partes"],
            "pza_garantia" => 1,
            "costo_ford" => $data["precio_cu"],
            "horas_mo_garantias" => $data["tiempo_tarifa"],
            "costo_mo_garantias" => 0,
            "autoriza_garantia" => 1,
            "autoriza_jefeTaller" => 1,
            "autoriza_asesor" => 1,
            "autoriza_cliente" => 1,
            "tipo_precio" => "G",
            "prioridad" => 2,
            "estado_entrega" => 0,
            "activo" => 1,
            "fecha_registro" => date("Y-m-d H:i:s"),
            "fecha_registro_garantia" => date("Y-m-d H:i:s"),
            "fecha_actualiza" => date("Y-m-d H:i:s")
        );

        //Guardamos el registro de la refaccion
        $id_retorno = $this->consulta->save_register('presupuesto_multipunto',$registro);

        if ($id_retorno != 0) {
            $query = $this->consulta->datos_generales_orden($id_cita); 
            foreach ($query as $row) {

                $folio = $row->folioIntelisis;
                $asesor = $row->asesor;
                $tecnico = $row->tecnico;
                $modelo = $row->vehiculo_modelo;
                $placas = $row->vehiculo_placas;
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                //$proactivo = $this->consulta->idHistorialProactivo($serie);
            }

            $comentarios_adicioanles = 'Se crea presupuesto con una refacción desde el módulo de garantías: '.$data["nombre"].' Observaciones del cliente: '.(($data["queja_cliente"] != NULL) ? $data["queja_cliente"] : "Sin Observaciones");

            $valida_estatus = 0;
            $ev_garantias = 0;
            $ev_jdt = 0;
            //Recuperamos firmas para comentar
            $query = $this->consulta->get_result_field("id_cita",$data["numero_orden_origen"],"identificador","M","presupuesto_registro");
            foreach ($query as $row){
                $firma_garantia = $row->firma_garantia;
                $firma_asesor = $row->firma_asesor;
                $firma_jdt = $row->firma_jdt;
                $archivos_presupuesto = $row->archivos_presupuesto;
                $archivo_tecnico = $row->archivo_tecnico;
                $archivo_jdt = $row->archivo_jdt;
                $comentario_jdt = $row->comentario_jdt;
                $comentario_ventanilla = $row->comentario_ventanilla;
                $comentario_tecnico = $row->comentario_tecnico;

                if ($firma_jdt != "") {
                    $ev_jdt = 1;
                    if ($firma_garantia != "") {
                        $ev_garantias = 1;
                        if ($firma_asesor != "") {
                            $estatus = "Espera solicitud de piezas";
                            $valida_estatus = 1;
                        } else {
                            $estatus = "Espera revisión del asesor";
                        }
                    }else{
                        $estatus = "Espera revisión de garantías";
                    }
                }else{
                    $estatus = "Espera revisión del jefe de taller";
                }
            }

            //Guardamos el cuerpo principal del presupuesto
            $registro_pivote = array(
                "id" => NULL,
                "id_cita" => $id_cita,
                "folio_externo" => $folio,
                "identificador" => "M",
                "anexa_garantias" => 1,
                "cliente_paga" => 0,
                "ref_garantias" => 1,
                "ref_tradicional" => 0,
                "ford_paga" => 0,
                "total_pago_garantia" => 0,
                "envio_garantia" => $ev_garantias,
                "comentario_garantia" => $comentarios_adicioanles,
                "firma_garantia" => ((isset($firma_garantia)) ? $firma_garantia : ""),
                "anticipo" => 0,
                "nota_anticipo" => "",
                "subtotal" => 0,
                "cancelado" => 0,
                "iva" => 0,
                "total" => 0,
                "ventanilla" => "",
                "envia_ventanilla" => 1,
                "estado_refacciones" => 0,
                "estado_refacciones_garantias" => 0,
                "archivos_presupuesto" => ((isset($archivos_presupuesto)) ? $archivos_presupuesto : ""),
                "archivo_tecnico" => ((isset($archivo_tecnico)) ? $archivo_tecnico : ""),
                "archivo_jdt" => ((isset($archivo_jdt)) ? $archivo_jdt : ""),
                "firma_asesor" => ((isset($firma_asesor)) ? $firma_asesor : ""),
                "comentario_asesor" => "",
                "envio_jdt" => $ev_jdt,
                "firma_jdt" => ((isset($firma_jdt)) ? $firma_jdt : ""),
                "comentario_jdt" => ((isset($comentario_jdt)) ? $comentario_jdt : ""),
                "comentario_ventanilla" => ((isset($comentario_ventanilla)) ? $comentario_ventanilla : ""),
                "comentario_tecnico" => ((isset($comentario_tecnico)) ? $comentario_tecnico : ""),
                "serie" => $serie,
                "firma_requisicion" => "",
                "firma_requisicion_garantias" => "",
                "correo_adicional" => "",
                "envio_proactivo" => 0,
                "acepta_cliente" => (($valida_estatus == 1) ? "Si" : ""),
                "ubicacion_proceso" => ((isset($estatus)) ? $estatus : "Se crea presupuesto desde garantías."),
                "ubicacion_proceso_garantia" => "Se agregarón refacciones",
                "fecha_alta" => date("Y-m-d H:i:s"),
                "termina_ventanilla" => date("Y-m-d H:i:s"),
                "termina_jdt" => date("Y-m-d H:i:s"),
                "termina_garantias" => date("Y-m-d H:i:s"),
                "termina_asesor" => date("Y-m-d H:i:s"),
                "termina_cliente" => date("Y-m-d H:i:s"),
                "termina_requisicion" => date("Y-m-d H:i:s"),
                "termina_requisicion_garantias" => date("Y-m-d H:i:s"),
                "solicita_pieza" => date("Y-m-d H:i:s"),
                "recibe_pieza" => date("Y-m-d H:i:s"),
                "entrega_pieza" => date("Y-m-d H:i:s"),
                "solicita_pieza_garantia" => date("Y-m-d H:i:s"),
                "recibe_pieza_garantia" => date("Y-m-d H:i:s"),
                "entrega_pieza_garantia" => date("Y-m-d H:i:s"),
                "termina_proactivo" => date("Y-m-d H:i:s"),
                "fecha_actualiza" => date("Y-m-d H:i:s"),
            );

            //Guardamos el registro de la refaccion
            $id_retorno = $this->consulta->save_register('presupuesto_registro',$registro_pivote);
            /*
            $actualizacion = array(
                "anexa_garantias" => 1,
                "ref_garantias" => 1,
                "ref_tradicional" => 0,
                "envia_ventanilla" => 3,
                "estado_refacciones" => 0,
                "estado_refacciones_garantias" => 0,
                "firma_requisicion" => "",
                "firma_requisicion_garantias" => "",
                "comentario_garantia" => '<br>' . 'Se agrega refacción desde el módulo de garantías: '.$data["nombre"],
                "acepta_cliente" => "",
                "ubicacion_proceso" => "Se agrego una refacción desde garantías. Espera autorización de ventanilla.",
                "ubicacion_proceso_garantia" => "Se agregarón refacciones",
                "fecha_actualiza" => date("Y-m-d H:i:s"),
            );

            $actualizar = $this->consulta->update_table_row_2('presupuesto_registro',$actualizacion,"id_cita",$id_cita,"identificador","M");
            */
            if (isset($tecnico)) {
                $texto = SUCURSAL.".@@@URGENTE@@@ Presupuesto de la Orden: ".$id_cita.". Se crea presupuesto desde el módulo de garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";

                //Gerente de servicio
                //$this->sms_general('3315273100',$texto);
                
                //Ventanilla
                $this->sms_general('3316721106',$texto);
                $this->sms_general('3319701092',$texto);
                $this->sms_general('3317978025',$texto);

                if ($ev_jdt == 1) {
                    if ($ev_garantias == 1) {
                        if ($valida_estatus == 0) {
                            $texto_2 = SUCURSAL.".@@@URGENTE@@@  Se crea presupuesto de la Orden: ".$id_cita." desde el módulo de garantías con faltante de la firma del encargado de garantías; el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                            //Assor
                            $celular_sms = $this->consulta->telefono_asesor($id_cita);
                            $this->sms_general($celular_sms,$texto_2);
                        }
                    }else{
                        $texto_2 = SUCURSAL.".@@@URGENTE@@@  Se crea presupuesto de la Orden: ".$id_cita." desde el módulo de garantías con faltante de la firma del encargado de garantías; el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                        //Garantias
                        $this->sms_general('3316725075',$texto_2);
                    }
                }else{
                    $texto_2 = SUCURSAL.".@@@URGENTE@@@  Se crea presupuesto de la Orden: ".$id_cita." desde el módulo de garantías con faltante de la firma de jefe de taller; el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                    //Juan Chavez
                    $this->sms_general('3333017952',$texto_2);
                }

                if (isset($texto_2)) {
                    //Gerente de servicio
                    //$this->sms_general('3315273100',$texto_2);
                    //Tester
                    $this->sms_general('3328351116',$texto_2);
                }
                
                //Tester
                $this->sms_general('3328351116',$texto);
                //Angel evidencia
                //$this->sms_general('5535527547',$texto);
            }

            $this->actualiza_documentacion($id_cita,$registro_pivote["acepta_cliente"]);

            $movimiento = "Presupuesto: Se agrega una refacción desde el módulo de garantías";
            $this->registrar_movimiento($id_cita,$movimiento,"Garantías");

            $refaccion = array(
                "partes_operacion_id" => $data["partes_operacion_id"],
                "id_refaccion_presupuesto" => $id_retorno,
                "mensaje" => "OK. NUEVO PRESUPUESTO"
            );

        }else{
            $refaccion = array(
                "partes_operacion_id" => $data["partes_operacion_id"],
                "id_refaccion_presupuesto" => NULL,
                "mensaje" => "OCURRIO UN ERROR AL GUARDAR LA INFORMACIÓN. NUEVO PRESUPUESTO"
            );
        }

        return $refaccion;
    }

    public function baja_refaccion()
    {
        //Recibimos los datos
        //$_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 

        try {
            $id_refaccion = $_GET["id_refaccion"];

            $id_cita = $this->consulta->id_cita_refaccion($id_refaccion);

            $valida_edicion = $this->verificar_proceso_garantia($id_cita);

            if ($valida_edicion == "OK") {
                //Armanos el paquete para modificar
                $refaccion_baja = array(
                    "activo" => 0,
                    "fecha_actualiza" => date("Y-m-d H:i:s"),
                );

                //Actualizamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row('presupuesto_multipunto',$refaccion_baja,'id',$id_refaccion);
                if ($actualizar) {
                    $query = $this->consulta->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
                    foreach ($query as $row){
                        $tradicionales = $row->ref_tradicional;
                        $garantias = (int)$row->ref_garantias;

                        $comentarios = $row->comentario_garantia;
                    }

                    if (isset($garantias)) {
                        $garantias--;

                        $actualizacion = array(
                            "ref_garantias" => $garantias,
                            "ref_tradicional" => $tradicionales,
                            "firma_requisicion" => "",
                            "firma_requisicion_garantias" => "",
                            "fecha_actualiza" => date("Y-m-d H:i:s"),
                        );

                        $actualizar = $this->consulta->update_table_row_2('presupuesto_registro',$actualizacion,"id_cita",$id_cita,"identificador","M");

                        $this->registrar_movimiento($id_cita,"Se dio de baja la refacción con id: ".$id_refaccion.". Desde el módulo de garantías");
                    }else{
                        $contenedor["respuesta"] = "ERROR INTERNO AL ACTUALIZAR EL REGISTRO";
                    }

                    $contenedor["respuesta"] = "OK";
                }else{
                    $contenedor["respuesta"] = "ERROR AL ACTUALIZAR EL REGISTRO";
                }
            }else{
                $contenedor["respuesta"] = $valida_edicion;
            }   

        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function verificar_presupuesto(){
        $contenedor = []; 

        try {
            $valida_edicion = $this->verificar_proceso_garantia($_GET["id_cita"]);
            $contenedor["respuesta"] = $valida_edicion;

        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function verificar_proceso_garantia($id_cita='')
    {
        $validacion = "ERROR";
        $ref_garantias = $this->consulta->total_ref_garantia($id_cita);
        //Se verifica si el rpesupuesto ya fue afectado por el cliente
        $query = $this->consulta->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($query as $row){
            $firma_requisicion = (($row->firma_requisicion != "") ? "1" : "0");
            $firma_requisicion_garantias = (($row->firma_requisicion_garantias != "") ? "1" : "0");
            $acepta_cliente = $row->acepta_cliente;
        }

        if (isset($acepta_cliente)) {
            //Si no ha sido afectado por el clienteo fue rechazado totalmente, se puede editar
            if (($acepta_cliente == "")||($acepta_cliente == "No")) {
                $validacion = "OK";

            //De lo contrario validamos temas de garantias
            } else {
                //Si no tienen refacciones con garantias, se puede editar
                if ($ref_garantias == 0) {
                    $validacion = "OK";

                //De lo contrario validamosfirmas de la requisicion
                } else {
                    //Si tiene ambas firmas de la requicision, se puede editar
                    if (($firma_requisicion == "1")&&($firma_requisicion_garantias == "1")) {
                        $validacion = "OK";

                    //Verificamos campo faltante
                    } else {
                        //Si tieine la firma del tecnico, y no la de garantias se bloquea
                        if ($firma_requisicion == "0") {
                            $validacion = "OK";
                        } else {
                            if ($firma_requisicion_garantias == "1") {
                                $validacion = "OK";
                            } else {
                                $validacion = "FALTA FIRMA DE GARANTÍAS EN LA REQUISICIÓN PARA PERMITIR EDICION";
                            }
                        }
                        /*if (($firma_requisicion == "0")&&($firma_requisicion_garantias == "0")) {
                            $validacion = "FALTAN AMBAS FIRMAS DE LA REQUISICÓN PARA PERMITIR EDICION";
                        }elseif ($firma_requisicion == "0") {
                            $validacion = "FALTA FIRMA DEL TÉCNICO EN LA REQUISICIÓN PARA PERMITIR EDICION";
                        }else{
                            $validacion = "FALTA FIRMA DE GARANTÍAS EN LA REQUISICIÓN PARA PERMITIR EDICION";
                        }*/
                    }
                }
            }
        }else{
            $validacion = "OK";
        }

        return $validacion;
    }

    public function actualiza_documentacion($id_cita = '',$afecta = '')
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->consulta->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "presupuesto" => "1",
                "presupuesto_origen" => "1",
                "presupuesto_afectado" => $afecta,
            );

            //Guardamos el registro
            $actualiza = $this->consulta->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Envio de notificaciones
    public function sms_general($celular_sms='',$mensaje_sms=''){
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    public function actualiza_ref_ventanilla()
    {
        $fecha_registro = date("Y-m-d H:i");
        $reg_alta_garantia = $_POST["refacciones_adicionales"];
        $renglones = $_POST["registros"];
        $renglones_garantias = $_POST["registrosGarantias"];
        $renglones_id = $_POST["id_registros"];
        
        if ($_POST["pzaGarantia"] != NULL) {
            $pzas_garantias = $_POST["pzaGarantia"];
        }else {
            $pzas_garantias = [];
        }

        $ref_tradicionales = 0;
        $ref_garantias = 0;
        
        $bandera = FALSE;

        $nvo_refaccion = [];
        $refaccion = [];
        $id_dase = [];
        $mixto = FALSE;

        //Obtenemos la informacion las refacciones y armamos los paquetes de guardado
        for ($i=0; $i < count($renglones) ; $i++) {
            if (strlen($renglones[$i])>19) {

                //Dividimos las celdas
                $campos_registro = explode("_",$renglones[$i]);
                $campos_garantias = explode("_",$renglones_garantias[$i]);

                $registro_existe_almacen = explode("*",$campos_registro[3]);

                $info_refaccion = array(
                    "alta_garantia" => (((int)$reg_alta_garantia[$i] == 1) ? 2 : NULL),
                    "cantidad" => $campos_registro[0],
                    "descripcion" => $campos_registro[1]
                );

                if ($info_refaccion["num_pieza"] === NULL) {
                    $mixto = TRUE;
                }

                if ($campos_registro[2] != "") {
                    $info_refaccion["num_pieza"] = $campos_registro[2];
                }
                
                if ($registro_existe_almacen[0] != "") {
                    $info_refaccion["existe"] = $registro_existe_almacen[0];
                    $info_refaccion["fecha_planta"] = ((count($registro_existe_almacen)>1) ? $registro_existe_almacen[1] : "0000-00-00");
                }

                if ($campos_registro[4] != "") {
                    $info_refaccion["costo_pieza"] = $campos_registro[4];
                }
                
                $info_refaccion["horas_mo"] = $campos_registro[5];
                $info_refaccion["costo_mo"] = $campos_registro[6];
                $info_refaccion["total_refacion"] = $campos_registro[7];
                $info_refaccion["costo_ford"] = $campos_garantias[0];
                $info_refaccion["horas_mo_garantias"] = $campos_garantias[1];
                $info_refaccion["costo_mo_garantias"] = $campos_garantias[2];
                
                $info_refaccion["fecha_precesa_altag"] = (((int)$reg_alta_garantia[$i] == 1) ? $fecha_registro : NULL);
                $info_refaccion["fecha_actualiza"] = $fecha_registro;

                $refaccion = $info_refaccion;
                $id_dase[] = $renglones_id[$i];

                //Actualizamos el registro de la refaccion
                $actualizar = $this->consulta->update_table_row('presupuesto_multipunto',$refaccion,'id',$renglones_id[$i]);
                if ($actualizar) {
                    //Si se actualizo correctamente
                    $bandera = TRUE;
                }

                //Contabilizamos las refacciones tradicionales y las de garantias
                if (!in_array(($i+1),$pzas_garantias)) {
                    $ref_tradicionales++;
                } else {
                    $ref_garantias++;
                }
            }
        }

        //Si la actualizacion y/o registro se llevo con exito, se actualiza el registro general
        //Validamos si se guardara un registro pivote
        if ($bandera) {
            //Verificamos los archivos generales
            if($_POST["uploadfiles_resp"] != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $_POST["uploadfiles_resp"]."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            $actualiza_pivote = array(
                //"anexa_garantias" => 2,
                "ref_garantias" => $ref_garantias,
                "ref_tradicional" => $ref_tradicionales,
                "anticipo" => $_POST["anticipoMaterial"],
                "nota_anticipo" => $_POST["anticipoNota"],
                "subtotal" => $_POST["subTotalMaterial"],
                "cancelado" => $_POST["canceladoMaterial"],
                "iva" => $_POST["ivaMaterial"],
                "total" => $_POST["totalMaterial"],
                //"ventanilla" => "",
                "envia_ventanilla" => 1,
                "archivos_presupuesto" => $archivos_gral,
                "comentario_ventanilla" => $_POST["comentario_ventanilla"],
                //"serie" => $_POST["serie"],
            );

            $actualiza_pivote["ubicacion_proceso"] = "En espera de solicitud de refacción con garantía";
            $actualiza_pivote["ubicacion_proceso_garantia"] = "Refacción adicional procesada";

            $actualiza_pivote["fecha_actualiza"] = $fecha_registro;

            //Guardamos el registro de la refaccion
            $actualiza_reg = $this->consulta->update_table_row_2('presupuesto_registro',$actualiza_pivote,"id_cita",$_POST["idOrdenTemp"],"identificador","M");
            if ($actualiza_reg != 0) {
                //Recuperamos datos extras para la notificación
                $query = $this->consulta->notificacionTecnicoCita($_POST["idOrdenTemp"]); 
                foreach ($query as $row) {
                    $tecnico = $row->tecnico;
                    $modelo = $row->vehiculo_modelo;
                    $placas = $row->vehiculo_placas;
                    $asesor = $row->asesor;
                }

                if (isset($tecnico)) {
                     $texto = SUCURSAL.".@@@URGENTE@@@  Presupuesto de la Orden: ".$_POST["idOrdenTemp"]." fue liberado de ventanilla para procesar refacciones adicionales de garantías el día ".date("d-m-Y")." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                    //Enviamos mensaje al tecnico
                    $celular_sms_2 = $this->consulta->telefono_tecnico($_POST["idOrdenTemp"]);
                    $this->sms_general($celular_sms_2,$texto);

                    if ($mixto) {
                        //Juan Chavez
                        $this->sms_general('3333017952',$texto);
                    }
                    
                    //Tester
                    $this->sms_general('3328351116',$texto);
                    
                    //Angel evidencia
                    //$this->sms_general('5535527547',$texto);

                    $movimiento = "Presupuesto liberado de ventanilla refacción adicional del módulo de garantías";
                }else{
                    $movimiento = "Presupuesto liberado de ventanilla refacción adicional del módulo de garantías (Sin notificación";
                }

                $this->registrar_movimiento($_POST["idOrdenTemp"],$movimiento,"Ventanilla");

                $respuesta = "OK";
            }else{
                $respuesta = "Ocurrio un error enla actualización del registro";
            }
        }else{
            $respuesta = "No se pudo actualizar la información, favor de intentar nuevamente";
        }

        echo $respuesta;
    }

    public function validateDoc($nombreDoc = NULL)
    {
        $path = "";
        //300 segundos  = 5 minutos
        //ini_set('max_execution_time',600);

        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
        for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docMultipunto";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_PM";
            for($j=0; $j<=6; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function ordenes_tb_poliza($entrada = "",$datos = NULL)
    {
        //$fecha_busqueda = (date("Y")-1)."-".date("m-d");
        $mes_temp = (int)date("m");
        $mes = (($mes_temp < 5) ? (8 + $mes_temp) : ($mes_temp - 4));
        $anio = (($mes_temp < 5) ? date("Y")-1 : date("Y"));
        $mes = (($mes < 10) ? "0" : "").$mes;
        $fecha_busqueda = $anio."-".$mes."-".date("d");
        
        $datos["ordenes"] = $this->consulta->listado_ordenes_doc($fecha_busqueda);


        $this->loadAllView($datos,'ordenServicio/listas/documentos_poliza_garantias');
    }

    public function vista_archivo_poliza($id_cita='' , $datos = NULL)
    {
        $datos["origen_apertura"] = "1";
        $this->carga_archivo_poliza($id_cita,$datos);
    }    

    public function carga_archivo_poliza($id_cita_url='' , $datos = NULL)
    {
        $datos["id_cita"] = ((ctype_digit($id_cita_url)) ? $id_cita_url : $this->decrypt($id_cita_url));

        $datos["archivos"] = $this->consulta->get_result_field("id_cita",$datos["id_cita"],"activo","1","archivos_poliza_garantias");
        $datos["origen_apertura"] = ((isset($datos["origen_apertura"])) ? $datos["origen_apertura"] : "");
        //Datos de cabecera
        $cabecera = $this->consulta->notificacionTecnicoCita($datos["id_cita"]);
        foreach ($cabecera as $row) {
            $datos["cabecera"] = array(
                "id_cita" => $row->id_cita,
                "folio_externo" => $row->folioIntelisis,
                "serie" => $row->vehiculo_identificacion,
                "placas" => $row->vehiculo_placas,
                "modelo" => $row->vehiculo_modelo,
                "anio" => $row->vehiculo_anio,
                "tecnico" => $row->tecnico,
                "asesor" => $row->asesor,
            );
        }

        $this->loadAllView($datos,'ordenServicio/listas/cargar_poliza_garantias');
    }

    public function subir_polizas()
    {
        $validar_carga = 0;
        $contenedor_archivos = [];

        if ($this->session->userdata('rolIniciado')) {
            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $id_usuario =  "0";
            $usuario = "Sesión Caducada";
        }

        //Guardamos los archivos en la carpeta del servidor
        for ($i = 0; $i < count($_FILES["archivo"]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES["archivo"]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES["archivo"]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES["archivo"]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES["archivo"]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES["archivo"]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/poliza_garantias";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = "Poliza_".$_POST["id_cita"]."-".date("YmdHi");
            for($j=0; $j<=1; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                $data['uploadError'] = $this->upload->display_errors();
                // echo $this->upload->display_errors();
                $validar_carga = 0;
            }else{
                //Si se pudo cargar la imagen la guardamos
                $fileData = $this->upload->data();
                $ext = explode(".",$_FILES['tempFile']['name']);
                $contenedor_archivos[] = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
            }
        }

        $conteo = 0;
        //Verificamos que se hayan guardado los archivos
        if (count($contenedor_archivos) > 0) {
            $validar_carga = 1;
            //Guardamos los registros en la base
            for ($i=0; $i < count($contenedor_archivos) ; $i++) { 
                if ($contenedor_archivos[$i] != '') {
                    $contenido_temp = array(
                        "id" => NULL,
                        "id_cita" => $_POST['id_cita'],
                        "url_archivo" => $contenedor_archivos[$i],
                        "id_usuario" => $id_usuario,
                        "usuario" => $usuario,
                        "fecha_creacion" => date("Y-m-d H:i:s"),
                    );

                    $check = $this->consulta->save_register('archivos_poliza_garantias',$contenido_temp);
                    if ($check != 0) {
                        $conteo++;
                    }
                }
            }
        }

        //Registramos movimientos y mandamos respuesta
        if ($conteo > 0) {
            //Actualizamos la documentacion
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "archivo_poliza" => "1",
            );

            //Guardamos el registro
            $actualiza = $this->consulta->update_table_row('documentacion_ordenes',$contededor,'id_cita',$_POST["id_cita"]);

            $movimiento = "Se cargan (".$conteo."/".count($contenedor_archivos).") archivos de poliza en la orden, para cuestion de garantías";
            $this->loadHistory($_POST["id_cita"],$movimiento,"Externo");

            $resultado = "OK";
        }else{
            $resultado = "ERROR";
        }

        echo $resultado;
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function registrar_movimiento($id_cita='',$movimiento = '',$origen = NULL)
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Garantias";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = (($origen != NULL) ? $origen : "");
            $id_usuario =  "";
            $usuario = "";
        }

        $historico = array(
            'id' => NULL,
            'id_cita' => $id_cita,
            'origen_movimiento' => $panel,
            'movimiento' => $movimiento,
            'id_usuario' => $id_usuario,
            'usuario' => $usuario,
            'fecha_registro' => $registro,
        );

        $this->consulta->save_register('presupuestos_historial',$historico);

        return TRUE;
    }

    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->consulta->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

        //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_ogarantia.js',
                'assets/js/otrosModulos/adicional_garantias/envio_firmas.js'
                //'assets/js/reglasBloqueo.js',
                //'assets/js/superUsuario.js'
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Proceso Garantias"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}