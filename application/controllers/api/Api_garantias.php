<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_garantias extends CI_Controller { 

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'consulta', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL); 
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
    }

    public function index()
    {
          
    }

    //// -----------------------------------------------------------------------------
    ///                 Otros datos para garantia
    //// -----------------------------------------------------------------------------
    public function datos_garantia()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 

        try {
            $id_cita = $_POST["id_cita"];

            $valida = $this->consulta->verificar_orden_garantia($id_cita);

            if ($valida) {
                $datos = $this->consulta->info_garantia($id_cita);

                foreach ($datos as $row){
                    $contenedor["sucursal_bid"] = BIN_SUCURSAL;
                    $contenedor["verificacion_oasis"] = (($row->oasis != NULL) ? (($row->oasis != "") ? '1' : '0') : '0');

                    $contenedor["identificacion_asesor"] = $row->aidStars;
                    $contenedor["nombre_asesor"] = $row->asesor;
                    $contenedor["telefono_asesor"] = $row->telefono_asesor;
                    $contenedor["identificacion_tecnico"] = $row->tidStars;
                    $contenedor["nombre_tecnico"] = $row->tecnico;

                    $contenedor["hora_recepcion"] = $row->hora_recepcion;
                    $contenedor["fecha_recepcion"] = $row->fecha_recepcion;
                    $contenedor["hora_promesa"] = $row->hora_entrega;
                    $contenedor["fecha_promesa"] = $row->fecha_entrega;

                    $contenedor["numero_torre"] = $row->notorre;
                    $contenedor["color_torre"] = $row->color;
                    $contenedor["torre"] = $row->notorre."-".$row->color;

                    $contenedor["auto_placas"] = $row->vehiculo_placas;
                    $contenedor["auto_serie"] = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
                    $contenedor["auto_anio"] = $row->vehiculo_anio;
                    $contenedor["auto_modelo"] = $row->vehiculo_modelo;
                    $contenedor["auto_kilometraje"] = $row->km;
                    $contenedor["auto_tipo"] = $row->categoria;

                    $contenedor["cliente_nombre"] = $row->datos_nombres;
                    $contenedor["cliente_ap"] = $row->datos_apellido_paterno;
                    $contenedor["cliente_am"] = $row->datos_apellido_materno;
                    $contenedor["cliente_empresa"] = $row->nombre_compania;
                    $contenedor["cliente_tel1"] = $row->telefono_movil;
                    $contenedor["cliente_tel2"] = $row->otro_telefono;
                    $contenedor["cliente_tel3"] = $row->datos_telefono;
                    $contenedor["cliente_calle"] = $row->calle;
                    $contenedor["cliente_nointerior"] = (($row->nointerior != "") ? $row->nointerior : 'SN');
                    $contenedor["cliente_noexterior"] = (($row->noexterior != "") ? $row->noexterior : 'SN');
                    $contenedor["cliente_colonia"] = $row->colonia;
                    $contenedor["cliente_municipio"] = $row->municipio;
                    $contenedor["cliente_estado"] = $row->estado;
                    $contenedor["cliente_cp"] = $row->cp;

                    $contenedor["cita_asociada"] = (($row->orden_asociada != NULL) ? $row->orden_asociada : "SIN ASOCIAR"); 
                    $contenedor["cita_asociada_url"] = (($row->orden_asociada != NULL) ? $this->encrypt($row->orden_asociada) : "SIN ASOCIAR"); 

                    $contenedor["folio_intelisis_garantia"] = $row->folioIntelisis;
                    $contenedor["folio_intelisis_publico"] = (($row->orden_asociada != NULL) ? $this->consulta->id_orden_intelisis($row->orden_asociada) : "");
                    $contenedor["inicio_vigencia"] = $row->inicio_vigencia;

                    $contenedor["tel_ventanilla_1"] = "3316721106";
                    $contenedor["tel_ventanilla_2"] = "3319701092";
                    $contenedor["tel_ventanilla_3"] = "3317978025";

                    $contenedor["tel_jefe_taller"] = "3333017952";
                    $contenedor["tel_gerente"] = ""; // 3315273100
                }

                if (isset($contenedor["cliente_cp"])) {
                    $refacciones = $this->listar_refacciones($contenedor["cita_asociada"]);
                    $contenedor["refaccion"] = $refacciones;
                }

                $contenedor["respuesta"] = "OK";
            } else {
                $contenedor["respuesta"] = "ESTE NÚMERO DE ORDEN NO ES UNA GARANTÍA";
            }
        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function datos_garantia_f1863()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 

        try {
            $id_cita = $_POST["id_cita"];

            $datos = $this->consulta->info_garantia($id_cita);

            foreach ($datos as $row){
                $contenedor["sucursal_bid"] = BIN_SUCURSAL;
                $contenedor["verificacion_oasis"] = (($row->oasis != NULL) ? (($row->oasis != "") ? '1' : '0') : '0');

                $contenedor["identificacion_tecnico"] = $row->tidStars;
                $contenedor["nombre_tecnico"] = $row->tecnico;

                $contenedor["auto_placas"] = $row->vehiculo_placas;
                $contenedor["auto_serie"] = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
                $contenedor["auto_anio"] = $row->vehiculo_anio;
                $contenedor["auto_modelo"] = $row->vehiculo_modelo;
                $contenedor["auto_kilometraje"] = $row->km;

                $contenedor["folio_intelisis_garantia"] = $row->folioIntelisis;
                $contenedor["folio_intelisis_publico"] = (($row->orden_asociada != NULL) ? $this->consulta->id_orden_intelisis($row->orden_asociada) : "");

                $contenedor["tel_ventanilla_1"] = "3316721106";
                $contenedor["tel_ventanilla_2"] = "3319701092";
                $contenedor["tel_ventanilla_3"] = "3317978025";

                $contenedor["tel_jefe_taller"] = "3333017952";
                $contenedor["tel_gerente"] = ""; //3315273100
            }

            $contenedor["respuesta"] = "OK";
            $contenedor["id_cita"] = $id_cita;
        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function listar_refacciones($id_cita = '')
    {
        $refacciones = [];
        $presupuestos_garantias = $this->consulta->ref_garantias($id_cita);

        foreach ($presupuestos_garantias as $row){
            if ($row->descripcion != "") {
                $costo_pz = (float)(($row->pza_garantia == "1") ? $row->costo_ford : $row->costo_pieza);
                $importe = (float)$row->cantidad * $costo_pz;
                $importe_mo = (float)$row->costo_mo_garantias * (float)$row->horas_mo_garantias;

                $refacciones[] = array (
                    "prefijo" => (($row->prefijo != ".") ? $row->prefijo : ""),
                    "basico" => (($row->basico != ".") ? $row->basico : ""),
                    "sufijo" => (($row->sufijo != ".") ? $row->sufijo : ""),
                    "num_pieza" => $row->num_pieza,
                    "nombre" => $row->descripcion,
                    "cantidad" => $row->cantidad,
                    "precio_u" => $costo_pz,
                    "importe" => number_format($importe,4),
                    "costo_mo" => $row->costo_mo_garantias,
                    "horas_mo" => $row->horas_mo_garantias,
                    "importe_mo" => number_format($importe_mo,4),
                    "reparacion" => $row->ref,
                    "id_interno" => $row->id,
                );
            }   
        }

        return $refacciones;
    }

    public function diagnostico_orden()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 
        try {
            $id_cita = $_POST["id_cita"];

            $diag_vc = $this->consulta->get_result("idPivote",$id_cita,"voz_cliente");
            foreach ($diag_vc as $row){
                $falla = $row->definicionFalla;
            }

            if (isset($falla)) {
                $renglones = explode("DIAG",strtoupper($falla));
                $contenedor["fallas"] = [];
                $diag = 1;

                for ($i = 0; $i < count($renglones); $i++) { 
                    if ($renglones[$i] != "") {
                        $contenedor["fallas"][] = " DIAG.- ".$renglones[$i];
                        $diag++;
                    }                    
                }
            } else {
                $query = $this->consulta->get_result("id_cita",$id_cita,"vc_diagnostico");
                foreach ($query as $row) {
                    $contenedor["fallas"][] = " DIAG.- ".$row->definicion_falla;
                    $falla_2 = 1;
                }

                if (!isset($falla_2)) {
                    $contenedor["respuesta"] = "NO SE ENCONTRO VOZ CLIENTE CON ESTE NÚMERO DE ORDEN";
                }
            }
            
        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function recuperar_orgarantia()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 

        try {
            $id_cita = $_POST["id_cita"];
            
            $asociada = $this->consulta->publica_asociada($id_cita);

            $contenedor["orden_garantia"] = (($asociada != NULL) ? $asociada : NULL);
            //$contenedor["folio_asociado"] = (($asociada != NULL) ? $asociada[1] : NULL);

        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function firma_requisicion()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 

        try {
            $id_cita = $_POST["id_cita"];
            
            $termino = $this->consulta->firmas_req_firmas($id_cita);

            $contenedor["firma_reguisicion_garantias"] = $termino[0];
            $contenedor["firma_reguisicion_tecnico"] = $termino[1];


        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    //// -----------------------------------------------------------------------------
    ///                 Presupuesto para garantias
    //// -----------------------------------------------------------------------------
    public function presupuestos_garantias()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 
        try {
            $presupuestos = $this->consulta->cotizaciones_garantias();

            $contenedor["base_documentacion"] = base_url();
            $contenedor["base_orden"] = "OrdenServicio_RGarantia/"; //"OrdenServicio_Revision/";
            $contenedor["base_presupuesto"] = "Presupuesto_Garantia/";
            $contenedor["base_requisicion"] = "Requisicion_T/";
            $contenedor["base_multipunto"] = "Multipunto_Revision/";
            $contenedor["base_polizas"] = "Ver_Polizas/";

            foreach ($presupuestos as $row){
                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $fondo = "#f5acaa";
                        $color = "#17202a";
                        $estado_refacciones = "SIN SOLICITAR";
                    } elseif ($row->estado_refacciones == "1") {
                        $fondo = "#f5ff51";
                        $color = "#17202a";
                        $estado_refacciones = "SOLICIATADAS";
                    } elseif ($row->estado_refacciones == "2") {
                        $fondo = "#5bc0de";
                        $color = "#17202a";
                        $estado_refacciones = "RECIBIDAS";
                    }else{
                        $fondo = "#c7cec7";
                        $color = "#17202a";
                        $estado_refacciones = "ENTREGADAS";
                    }
                } else {
                    $fondo = "#FF0000";
                    $color = "#fdfefe";
                    $estado_refacciones = "SIN SOLICITAR";
                }

                if ($row->acepta_cliente != "") {
                    if ($row->acepta_cliente == "Si") {
                        $acepta_presupuesto = "CONFIRMADA";
                        $color_acepta = "#015b01";
                    } elseif ($row->acepta_cliente == "Val") {
                        $acepta_presupuesto = "DETALLADA";
                        $color_acepta = "#f98901";
                    } else {
                        $acepta_presupuesto = "RECHAZADA";
                        $color_acepta = "#f90606";
                    }
                    
                } else {
                    $acepta_presupuesto = "SIN CONFIRMAR";
                    $color_acepta = "#17202a";
                }

                //$archivos_polizas = (($row->archivo_poliza != NULL) ? $row->archivo_poliza : "0");
                //$url_poliza = $this->encrypt($row->id_cita);
                
                $contenedor["presupuestos"][] = array(
                    "id_cita" => $row->identificador."-".$row->id_cita,
                    "id_cita_url" => $this->encrypt($row->id_cita),
                    "folio_externo" => $row->folio_externo,
                    "revision_presupuesto" => $row->envio_garantia,
                    "estado_refacciones" => $estado_refacciones,
                    "estado_refacciones_fondo" => $fondo,
                    "estado_refacciones_letra" => $color,
                    "presupuesto_afectado" => $acepta_presupuesto,
                    "presupuesto_afectado_colortxt" => $color_acepta,
                    "cliente" => $row->cliente,
                    "serie" => $row->serie,
                    "tecnico" => $row->tecnico,
                    "asesor" => $row->asesor,
                    "modelo" => $row->vehiculo,
                    "placas" => $row->placas,
                    "oasis" => str_replace("|", "", $row->oasis),
                    "requisicion" => (($row->firma_requisicion != "") ? 1 : 0),
                    "doc_multipunto" => $row->multipunto,
                    "archivos_polizas" => (($row->archivo_poliza != NULL) ? "1" : "0") ,
                );
            }
            
        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function presupuestos_garantias_busqueda()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 
        try {
            $fecha_ini = $_POST["fecha_inicio"];
            $fecha_fin = $_POST["fecha_fin"];
            $campo = $_POST["campo"];

            //Si no se envio un campo de consulta
            if ($campo == "") {
                if (($fecha_fin == "")&&($fecha_ini != "")) {
                    $presupuestos = $this->consulta->cotizaciones_garantias_fecha($fecha_ini);
                } elseif (($fecha_fin != "")&&($fecha_ini == "")) {
                    $presupuestos = $this->consulta->cotizaciones_garantias_fecha($fecha_fin);
                } else {
                    $presupuestos = $this->consulta->cotizaciones_garantias_fecha1($fecha_ini,$fecha_fin);
                }
            //Si se envio algun campode busqueda
            } else {
                //Si no se envia ninguna fecha
                if (($fecha_fin == "")&&($fecha_ini == "")) {
                    $presupuestos = $this->consulta->cotizaciones_garantias_campo($campo); 
                //Si se envian ambas fechas
                }elseif (($fecha_fin != "")&&($fecha_ini != "")) {
                    $presupuestos = $this->consulta->cotizaciones_garantias_campofecha1($campo,$fecha_ini,$fecha_fin);
                //Si solo se envia una fecha
                } else {
                    if ($fecha_fin == "") {
                        $presupuestos = $this->consulta->cotizaciones_garantias_campofecha($campo,$fecha_ini);
                    } else {
                        $presupuestos = $this->consulta->cotizaciones_garantias_campofecha($campo,$fecha_fin);
                    }
                }
            }

            $contenedor["base_documentacion"] = base_url();
            $contenedor["base_orden"] = "OrdenServicio_RGarantia/"; //"OrdenServicio_Revision/";
            $contenedor["base_presupuesto"] = "Presupuesto_Garantia/";
            $contenedor["base_requisicion"] = "Requisicion_T/";
            $contenedor["base_multipunto"] = "Multipunto_Revision/";
            $contenedor["base_polizas"] = "Ver_Polizas/";

            foreach ($presupuestos as $row){
                if ((int)$row->ref_garantias > 0) {
                    if ($row->acepta_cliente != "No") {
                        if ($row->estado_refacciones == "0") {
                            $fondo = "#f5acaa";
                            $color = "#17202a";
                            $estado_refacciones = "SIN SOLICITAR";
                        } elseif ($row->estado_refacciones == "1") {
                            $fondo = "#f5ff51";
                            $color = "#17202a";
                            $estado_refacciones = "SOLICIATADAS";
                        } elseif ($row->estado_refacciones == "2") {
                            $fondo = "#5bc0de";
                            $color = "#17202a";
                            $estado_refacciones = "RECIBIDAS";
                        }else{
                            $fondo = "#c7cec7";
                            $color = "#17202a";
                            $estado_refacciones = "ENTREGADAS";
                        }
                    } else {
                        $fondo = "#FF0000";
                        $color = "#fdfefe";
                        $estado_refacciones = "SIN SOLICITAR";
                    }

                    if ($row->acepta_cliente != "") {
                        if ($row->acepta_cliente == "Si") {
                            $acepta_presupuesto = "CONFIRMADA";
                            $color_acepta = "#015b01";
                        } elseif ($row->acepta_cliente == "Val") {
                            $acepta_presupuesto = "DETALLADA";
                            $color_acepta = "#f98901";
                        } else {
                            $acepta_presupuesto = "RECHAZADA";
                            $color_acepta = "#f90606";
                        }
                        
                    } else {
                        $acepta_presupuesto = "SIN CONFIRMAR";
                        $color_acepta = "#17202a";
                    }

                    //$archivos_polizas = (($row->archivo_poliza != NULL) ? $row->archivo_poliza : "0");
                    //$url_poliza = $this->encrypt($row->id_cita);
                    
                    $contenedor["presupuestos"][] = array(
                        "id_cita" => $row->identificador."-".$row->id_cita,
                        "id_cita_url" => $this->encrypt($row->id_cita),
                        "folio_externo" => $row->folio_externo,
                        "revision_presupuesto" => $row->envio_garantia,
                        "estado_refacciones" => $estado_refacciones,
                        "estado_refacciones_fondo" => $fondo,
                        "estado_refacciones_letra" => $color,
                        "presupuesto_afectado" => $acepta_presupuesto,
                        "presupuesto_afectado_colortxt" => $color_acepta,
                        "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                        "serie" => (($row->serie != "") ? $row->serie : $row->vehiculo_identificacion),
                        "tecnico" => $row->tecnico,
                        "asesor" => $row->asesor,
                        "modelo" => $row->vehiculo_modelo,
                        "placas" => $row->vehiculo_placas,
                        "oasis" => str_replace("|", "", $row->oasis),
                        "requisicion" => (($row->firma_requisicion != "") ? 1 : 0),
                        "doc_multipunto" => $row->multipunto,
                        "archivos_polizas" => (($row->archivo_poliza != NULL) ? "1" : "0") ,
                    );
                }
            }
            
        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function presupuesto_refacciones()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 
        try {
            $id_cita = $_POST["id_cita"];

            $refacciones_cgarantia = $this->consulta->presupuesto_piezas($id_cita);

            foreach ($refacciones_cgarantia as $row){
                if ($row->descripcion != "") {
                    if (($row->activo == "1")&&($row->pza_garantia == "1")) {
                        $costo_pz = (($row->pza_garantia == "1") ? $row->costo_ford : $row->costo_pieza);
                        $importe = (float)$row->cantidad * (float)$costo_pz;
                        $importe_mo = (float)$row->horas_mo_garantias * (float)$row->costo_mo_garantias;
                        
                        $contenedor[] = array (
                            "prefijo" => (($row->prefijo != ".") ? $row->prefijo : ""),
                            "basico" => (($row->basico != ".") ? $row->basico : ""),
                            "sufijo" => (($row->sufijo != ".") ? $row->sufijo : ""),
                            "num_pieza" => $row->num_pieza,
                            "nombre" => $row->descripcion,
                            "cantidad" => $row->cantidad,
                            "precio_u" => $costo_pz,
                            "importe" => number_format($importe,4),
                            "costo_mo" => $row->costo_mo_garantias,
                            "horas_mo" => $row->horas_mo_garantias,
                            "importe_mo" => number_format($importe_mo,4),
                            "ref_garantia" => (($row->pza_garantia == "1") ? "SI" : "NO"),
                            "autoriza_garantia" => (($row->autoriza_garantia == "1") ? "SI" : "NO"),
                            "libera_garantias" => (($row->envio_garantia == "1") ? "SI" : "NO"),
                            "reparacion" => $row->ref,
                        );
                    }   
                }
            }

            //$contenedor["respuesta"] = "OK";
            
        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }


    //// -----------------------------------------------------------------------------
    ///                 Documentacion para garantias
    //// -----------------------------------------------------------------------------
    public function documentacion_garantias()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 
        $conte_id_cita = [];

        try {
            $mes_actual = date("m");

            $fecha_busqueda = (($mes_actual <= 1) ? (date("Y")-1) : date("Y"))."-".(($mes_actual <= 1) ? "12" : ($mes_actual - 1))."-01";
            $documentacion = $this->consulta->garantias_doc("2021-05-01");  //"2021-05-01"
            
            $contenedor["base_documentacion"] = base_url();
            $contenedor["base_orden"] = "OrdenServicio_RGarantia/";
            $contenedor["base_multipunto"] = "Multipunto_Revision/";
            $contenedor["base_vozcliente"] = "VCliente_Revision/";
            $contenedor["base_presupuesto"] = "Presupuesto_FinalGarantias/";          //Presupuesto_Cliente
            $contenedor["base_requisicion"] = "Requisicion_G/";
            $contenedor["base_recibir_garantia"] = "Recibir_Req_G/";
            $contenedor["base_polizas"] = "Ver_Polizas/";
            $contenedor["base_pdf_orden"] = "OrdenServicio_PDF/";
            $contenedor["base_pdf_multipunto"] = "Multipunto_PDF/";
            $contenedor["base_pdf_vozcliente"] = "VCliente_PDF/";
            $contenedor["base_pdf_presupuesto"] = "Presupuesto_PDF/";
            $contenedor["base_pdf_orden_firmapz"] = "OrdenServicio_RGPDF/";

            foreach ($documentacion as $row){ 
                $relacion_folios = [];
                $doc_polizas = NULL;
                $requisicion = 0;

                //if (!in_array($row->id_cita, $conte_id_cita)) {

                    if (($row->orden_asociada != NULL)&&($row->folio_asociado != "")) {
                        $relacion_folios = $this->consulta->folio_garantia($row->folio_asociado,$row->id_cita);
                    }

                    if ($row->orden_asociada != NULL) {
                        $validacion_req = $this->consulta->revisar_requisicion($row->orden_asociada); 
                        $requisicion = (($validacion_req == "1") ? $this->encrypt($row->orden_asociada) : "");
                        $respuesta_historial = $this->recuperar_pdf($row->orden_asociada);
                        $historico_pdf = $respuesta_historial["requi"];
                        $historico_pdf_1 = $respuesta_historial["recibido"];

                        $archivos_pre = $this->consulta->recuperar_evidencia_hm($row->orden_asociada);

                        //Recuperamos las polizas
                        $doc_polizas = $this->archivos_polizas($row->orden_asociada);

                    }else{
                        $requisicion = (($row->firma_requisicion != "") ? $this->encrypt($row->id_cita) : "");
                        $respuesta_historial = $this->recuperar_pdf($row->id_cita);
                        $historico_pdf = $respuesta_historial["requi"];
                        $historico_pdf_1 = $respuesta_historial["recibido"];

                        $archivos_pre = $this->consulta->recuperar_evidencia_hm($row->id_cita);

                        //Recuperamos las polizas
                        $doc_polizas = $this->archivos_polizas($row->id_cita);
                    }
                    
                    $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');

                    $oasis = str_replace("|", "", $row->oasis);

                    $or_identificador = $this->consulta->tipo_orde_asociada($row->orden_asociada);

                    if ($row->presupuesto == "0") {
                        if ($row->orden_asociada != NULL) {
                            $revision_pre = $this->consulta->revisar_presupuesto($row->orden_asociada);
                            //$archivos_pre = $this->consulta->recuperar_evidencia_presupuesto($row->orden_asociada);
                            //$archivos_pre["archivos"] .= "|".$this->consulta->recuperar_evidencia_hm($row->orden_asociada);
                            $presupuesto = (($revision_pre["id"] != "0") ? "1" : "0");
                            $presupuesto_afecta = $revision_pre["acepta_cliente"];
                            $presupuesto_url = (($revision_pre["id"] != "0") ? $this->encrypt($row->orden_asociada) : "");
                        }else{
                            $presupuesto = "0";
                            $presupuesto_afecta = "";
                            $presupuesto_url = "";
                            $archivos_pre = array("archivos" => "");
                        }
                        
                    }else{
                        //$archivos_pre = $this->consulta->recuperar_evidencia_presupuesto($row->id_cita);
                        //$archivos_pre["archivos"] .= "|".$this->consulta->recuperar_evidencia_hm($row->id_cita);
                        $presupuesto = $row->presupuesto;
                        $presupuesto_afecta = $row->presupuesto_afectado;
                        $presupuesto_url = $this->encrypt($row->id_cita);
                    }

                    $archivos_1 = explode("|",$archivos_pre["archivos"]);

                    $evidencias = [];
                    for ($i=0; $i < count($archivos_1); $i++) { 
                        if ($archivos_1[$i] != "") {
                            $evidencias[] = $archivos_1[$i];
                        }
                    }

                    //Si no se encontro cargada la poliza
                    if ($row->archivo_poliza == NULL) {
                        $archivos_polizas = $this->consulta->existencia_archivos_polizas($row->orden_asociada);
                        $url_poliza = $this->encrypt($row->orden_asociada);
                    }else{
                        $archivos_polizas = (($row->archivo_poliza != NULL) ? $row->archivo_poliza : "0");
                        $url_poliza = $this->encrypt($row->id_cita);
                    }
                    
                    $contenedor["documentacion"][] = array(
                        "fecha_recepcion" => $fecha->format('d-m-Y'), 
                        "id_cita" => $row->id_cita,
                        "id_cita_url" => $this->encrypt($row->id_cita),
                        "id_cita_identificador" => $row->identificador."-".$row->id_cita,
                        "folio_externo" => $row->folio_externo,
                        "auto_placas" => $row->placas,
                        "auto_modelo" => $row->vehiculo,
                        "auto_serie" => (($row->serie != "") ? $row->serie : $row->vehiculo_identificacion),
                        "asesor" => $row->asesor,
                        "tecnico" => $row->tecnico,
                        "cliente" => $row->cliente,
                        "doc_diagnostico" => (($row->diagnostico != NULL) ? "1" : "0"),
                        "doc_presupuesto" => $presupuesto,
                        "presupuesto_afecta" => $presupuesto_afecta,
                        "presupuesto_url" => $presupuesto_url,
                        "vc_udio" => $row->evidencia_voz_cliente ,
                        "doc_voz_cliente" => $row->voz_cliente ,
                        "doc_multipunto" => $row->multipunto ,
                        "firma_jefetaller" => $row->firma_jefetaller ,
                        "oasis" => $oasis,
                        "unidad_taller" => (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : ''),
                        "tipo_orden" => "Garantía",  //trim($tipo_orden_1[0]),
                        "orden_asociada" => (($row->orden_asociada != NULL) ? $or_identificador."-".$row->orden_asociada : "SIN ASOCIAR"),
                        "orden_asociada_url" => (($row->orden_asociada != NULL) ? $this->encrypt($row->orden_asociada) : NULL),
                        "folio_garantia" => $row->folio_asociado,
                        "relacion_folios" => $relacion_folios,
                        "requisicion" => $requisicion,
                        "folio_intelisis_garantia" => $row->folioIntelisis,
                        "folio_intelisis_publico" => (($row->orden_asociada != NULL) ? $this->consulta->id_orden_intelisis($row->orden_asociada) : ""),
                        "archivos_polizas" => (($archivos_polizas != 0) ? $url_poliza : "") ,
                        "polizas" => $doc_polizas,
                        "pdf_requisicion" => $historico_pdf,
                        "pdf_recepcion_piezas" => $historico_pdf_1,
                        "evidencias_presupuesto" => $evidencias,
                    );

                    //$conte_id_cita[] = $row->id_cita;
                //}
            }

        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function documentacion_garantias_busqueda()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 
        $conte_id_cita = [];

        try {
            $fecha_ini = $_POST["fecha_inicio"];
            $fecha_fin = $_POST["fecha_fin"];
            $campo = $_POST["campo"];

            //Si no se envio un campo de consulta
            if ($campo == "") {
                if (($fecha_fin == "")&&($fecha_ini != "")) {
                    $documentacion = $this->consulta->garantias_doc_fecha($fecha_ini);
                } elseif (($fecha_fin != "")&&($fecha_ini == "")) {
                    $documentacion = $this->consulta->garantias_doc_fecha($fecha_fin);
                } else {
                    $documentacion = $this->consulta->garantias_doc_fecha2($fecha_ini,$fecha_fin);
                }
            //Si se envio algun campode busqueda
            } else {
                //Si no se envia ninguna fecha
                if (($fecha_fin == "")&&($fecha_ini == "")) {
                    $documentacion = $this->consulta->garantias_doc_campo($campo); 
                //Si se envian ambas fechas
                }elseif (($fecha_fin != "")&&($fecha_ini != "")) {
                    $documentacion = $this->consulta->garantias_doc_campo_fecha1($campo,$fecha_ini,$fecha_fin);
                //Si solo se envia una fecha
                } else {
                    if ($fecha_fin == "") {
                        $documentacion = $this->consulta->garantias_doc_campo_fecha($campo,$fecha_ini);
                    } else {
                        $documentacion = $this->consulta->garantias_doc_campo_fecha($campo,$fecha_fin);
                    }
                }
            }

            $contenedor["base_documentacion"] = base_url();
            $contenedor["base_orden"] = "OrdenServicio_RGarantia/"; //"OrdenServicio_Revision/";
            $contenedor["base_multipunto"] = "Multipunto_Revision/";
            $contenedor["base_vozcliente"] = "VCliente_Revision/";
            $contenedor["base_presupuesto"] = "Presupuesto_FinalGarantias/";          //Presupuesto_Cliente
            $contenedor["base_requisicion"] = "Requisicion_G/";
            $contenedor["base_recibir_garantia"] = "Recibir_Req_G/";
            $contenedor["base_polizas"] = "Ver_Polizas/";
            $contenedor["base_pdf_orden"] = "OrdenServicio_PDF/";
            $contenedor["base_pdf_multipunto"] = "Multipunto_PDF/";
            $contenedor["base_pdf_vozcliente"] = "VCliente_PDF/";
            $contenedor["base_pdf_presupuesto"] = "Presupuesto_PDF/";
            $contenedor["base_pdf_orden_firmapz"] = "OrdenServicio_RGPDF/";

            foreach ($documentacion as $row){
                $relacion_folios = [];
                $requisicion = 0;
                $doc_polizas = NULL;

                if (!in_array($row->id_cita, $conte_id_cita)) {
                    if (($row->orden_asociada != NULL)&&($row->folio_asociado != "")) {
                        $relacion_folios = $this->consulta->folio_garantia($row->folio_asociado,$row->id_cita);
                    }

                    if ($row->orden_asociada != NULL) {
                        $validacion_req = $this->consulta->revisar_requisicion($row->orden_asociada);
                        $requisicion = (($validacion_req == "1") ? $this->encrypt($row->orden_asociada) : "");
                        $respuesta_historial = $this->recuperar_pdf($row->orden_asociada);
                        $historico_pdf = $respuesta_historial["requi"];
                        $historico_pdf_1 = $respuesta_historial["recibido"];

                        $archivos_pre = $this->consulta->recuperar_evidencia_hm($row->orden_asociada);

                        $doc_polizas = $this->archivos_polizas($row->orden_asociada);
                        
                    }else{
                        $requisicion = (($row->firma_requisicion != "") ? $this->encrypt($row->id_cita) : "");
                        $respuesta_historial = $this->recuperar_pdf($row->id_cita);
                        $historico_pdf = $respuesta_historial["requi"];
                        $historico_pdf_1 = $respuesta_historial["recibido"];

                        $archivos_pre = $this->consulta->recuperar_evidencia_hm($row->id_cita);

                        $doc_polizas = $this->archivos_polizas($row->id_cita);
                    }

                    if ($row->presupuesto == "0") {
                        if ($row->orden_asociada != NULL) {
                            $revision_pre = $this->consulta->revisar_presupuesto($row->orden_asociada);
                            //$archivos_pre = $this->consulta->recuperar_evidencia_presupuesto($row->orden_asociada);
                            //$archivos_pre = $this->consulta->recuperar_evidencia_hm($row->orden_asociada);
                            $presupuesto = (($revision_pre["id"] != "0") ? "1" : "0");
                            $presupuesto_afecta = $revision_pre["acepta_cliente"];
                            $presupuesto_url = (($revision_pre["id"] != "0") ? $this->encrypt($row->orden_asociada) : "");
                        }else{
                            $presupuesto = "0";
                            $presupuesto_afecta = "";
                            $presupuesto_url = "";
                            $archivos_pre = array("archivos" => "");
                        }
                        
                    }else{
                        $presupuesto = $row->presupuesto;
                        $presupuesto_afecta = $row->presupuesto_afectado;
                        $presupuesto_url = $this->encrypt($row->id_cita);
                        //$archivos_pre = $this->consulta->recuperar_evidencia_presupuesto($row->id_cita);
                        //$archivos_pre = $this->consulta->recuperar_evidencia_hm($row->id_cita);
                    }

                    $archivos_1 = explode("|",$archivos_pre["archivos"]);

                    $evidencias = [];
                    for ($i=0; $i < count($archivos_1); $i++) { 
                        if ($archivos_1[$i] != "") {
                            $evidencias[] = $archivos_1[$i];
                        }
                    }
                    
                    $fecha = new DateTime($row->fecha_recepcion.' 00:00:00');
                    /*$oasis_dt = explode("|", $row->oasis);

                    if (count($oasis_dt)>0) {
                        $oasis = $oasis_dt[0];
                    } else {
                        $oasis = "";
                    }*/

                    $oasis = str_replace("|", "", $row->oasis);

                    //$tipo_orden = $this->consulta->getTipoOrden($row->id_tipo_orden);
                    //$tipo_orden_1 = explode("(",$tipo_orden);

                    $or_identificador = $this->consulta->tipo_orde_asociada($row->orden_asociada);

                    //Si no se encontro cargada la poliza
                    if ($row->archivo_poliza == NULL) {
                        $archivos_polizas = $this->consulta->existencia_archivos_polizas($row->orden_asociada);
                        $url_poliza = $this->encrypt($row->orden_asociada);
                    }else{
                        $archivos_polizas = (($row->archivo_poliza != NULL) ? $row->archivo_poliza : "0");
                        $url_poliza = $this->encrypt($row->id_cita);
                    }
                    
                    $contenedor["documentacion"][] = array(
                        "fecha_recepcion" => $fecha->format('d-m-Y'),
                        "id_cita" => $row->id_cita,
                        "id_cita_url" => $this->encrypt($row->id_cita),
                        "id_cita_identificador" => $row->identificador."-".$row->id_cita,
                        "folio_externo" => $row->folio_externo,
                        "auto_placas" => $row->placas,
                        "auto_modelo" => $row->vehiculo,
                        "auto_serie" => (($row->serie != "") ? $row->serie : $row->vehiculo_identificacion),
                        "asesor" => $row->asesor,
                        "tecnico" => $row->tecnico,
                        "cliente" => $row->cliente,
                        "doc_diagnostico" => (($row->diagnostico != NULL) ? "1" : "0"),
                        "doc_presupuesto" => $presupuesto,
                        "presupuesto_afecta" => $presupuesto_afecta,
                        "presupuesto_url" => $presupuesto_url,
                        "vc_udio" => $row->evidencia_voz_cliente, //((file_exists($row->evidencia_voz_cliente)) ? $row->evidencia_voz_cliente : "") ,
                        "doc_voz_cliente" => $row->voz_cliente ,
                        "doc_multipunto" => $row->multipunto ,
                        "firma_jefetaller" => $row->firma_jefetaller ,
                        "oasis" => $oasis,
                        "unidad_taller" => (($row->id_lavador_cambio_estatus != "") ? 'TERMINADO' : ''),
                        "tipo_orden" => "Garantía",  //trim($tipo_orden_1[0]),
                        "orden_asociada" => (($row->orden_asociada != NULL) ? $or_identificador."-".$row->orden_asociada : "SIN ASOCIAR"),
                        "orden_asociada_url" => (($row->orden_asociada != NULL) ? $this->encrypt($row->orden_asociada) : NULL),
                        "folio_garantia" => $row->folio_asociado,
                        "relacion_folios" => $relacion_folios,
                        "requisicion" => $requisicion,
                        "archivos_polizas" => (($archivos_polizas != 0) ? $url_poliza : ""),
                        "polizas" => $doc_polizas,
                        "pdf_requisicion" => $historico_pdf,
                        "pdf_recepcion_piezas" => $historico_pdf_1,
                        "evidencias_presupuesto" => $evidencias,
                    );

                    $conte_id_cita[] = $row->id_cita;
                }
            }
        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    public function recuperar_pdf($id_cita = '')
    {
        $retorno["requi"] = NULL;
        $retorno["recibido"] = NULL;

        $query = $this->consulta->get_result("id_cita",$id_cita,"historial_requisicion");
        foreach ($query as $row){
            if ($row->tipo_archivo == 1) {
                $retorno["requi"][] = $row->url_archivo;
            } else {
                $retorno["recibido"][] = $row->url_archivo;
            }
            //$retorno[] = $row->url_archivo;
        }

        return $retorno;
    }

    public function archivos_polizas($id_cita = '')
    {
        $retorno = NULL;

        $query = $this->consulta->get_result_field("id_cita",$id_cita,"activo","1","archivos_poliza_garantias");
        foreach ($query as $row){
            $retorno[] = $row->url_archivo;
        }

        return $retorno;
    }

    public function registro_documentacion()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenedor = []; 

        try {
            $id_cita = $_POST["id_cita"];
            
            //Recuperamos la informacion general
            $informacion_gral = $this->consulta->recuperar_documentaciones_cita($id_cita);
            
            $revision = $this->consulta->get_result("id_cita",$id_cita,"documentacion_ordenes");
            foreach ($revision as $row2) {
                $verificacion = $row2->id;
            }

            foreach ($informacion_gral as $row) {
                if (($row->id_tipo_orden == "3")||($row->id_tipo_orden == "4")) {
                    $tipo_vista = 2;
                }else{
                    $tipo_vista = 1;
                }
                //Si existe el registro lo creamos
                if (isset($verificacion)) {
                    $actualizacion = array(
                        "folio_externo" => $row->folioIntelisis,
                        "fecha_recepcion" => $row->fecha_recepcion,
                        "vehiculo" => $row->vehiculo_modelo,
                        "placas" => $row->vehiculo_placas,
                        "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                        "asesor" => $row->asesor,
                        "tecnico" => $row->tecnico,
                        "id_tecnico" => $row->id_tecnico,
                        "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                        "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                        "oasis" => (($row->archivoOasis != "") ? $row->archivoOasis : ""),
                        "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                        "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                        "presupuesto" => "0",
                        "presupuesto_afectado" => "",
                        "presupuesto_origen" => 1,
                        "voz_cliente" => ((($row->idVoz != "")||($row->vc_id != "")) ? "1" : "0"),
                        "evidencia_voz_cliente" => (($row->evidencia !="") ? $row->evidencia : (($row->audio != "") ? $row->audio : "")),
                        "servicio_valet" => (($row->idValet != "") ? "1" : "0"),
                    );
                }else{
                    $registro = array(
                        "id" => NULL,
                        "id_cita" => $row->orden_cita,
                        "folio_externo" => $row->folioIntelisis,
                        "tipo_vita" => $tipo_vista,
                        //"tipo_vita" => 3,
                        "fecha_recepcion" => $row->fecha_recepcion,
                        "vehiculo" => $row->vehiculo_modelo,
                        "placas" => $row->vehiculo_placas,
                        "serie" => (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion),
                        "asesor" => $row->asesor,
                        "tecnico" => $row->tecnico,
                        "id_tecnico" => $row->id_tecnico,
                        "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                        "diagnostico" => (($row->idDiagnostico != "") ? "1" : "0"),
                        "oasis" => (($row->archivoOasis != "") ? $row->archivoOasis : ""),
                        "multipunto" => (($row->idMultipunto != "") ? "1" : "0"),
                        "firma_jefetaller" => (($row->firmaJefeTaller != "") ? "1" : "0"),
                        "presupuesto" => "0",
                        "presupuesto_afectado" => "",
                        "presupuesto_origen" => 1,
                        "voz_cliente" => ((($row->idVoz != "")||($row->vc_id != "")) ? "1" : "0"),
                        "evidencia_voz_cliente" => (($row->evidencia != "") ? $row->evidencia : (($row->audio != "") ? $row->audio : "")),
                        "servicio_valet" => (($row->idValet != "") ? "1" : "0"),
                        "fechas_creacion" => date("Y-m-d H:i:s"),
                    );
                }
            }

            if (isset($registro)) {
                $guardado = $this->consulta->save_register('documentacion_ordenes',$registro);
                $contenedor["respuesta"] = (($guardado != 0) ? "OK" : "ERROR");
            } else {
                $guardado = $this->consulta->update_table_row('documentacion_ordenes',$actualizacion,'id_cita',$id_cita);
                $contenedor["respuesta"] = (($guardado) ? "OK" : "ERROR");
            }
        } catch (Exception $e) {
            $contenedor["respuesta"] = "ERROR EN EL PROCESO";
        }

        $response = json_encode($contenedor);
        echo $response;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
}