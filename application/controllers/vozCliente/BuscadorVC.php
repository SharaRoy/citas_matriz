<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BuscadorVC extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index($algo = "",$datos = NULL)
    {
        if (!isset($datos["mensaje"])) {
            $datos["mensaje"] = "5";
        }
        // Recuperamos la hora y fecha actual
        $limiteHora = date("H:i:s");
        $horaAct = strtotime($limiteHora);

        $hoy = date("Y-m-d");

        //Verificamos y cargamos las ordenes de servicio sin cita
        $registros = $this->mConsultas->get_table('voz_cliente_temp');
        foreach ($registros as $row) {
            if ($row->idPivote != "0") {
                //Recuperamos la hora y fecha del registro
                $fecha = explode(" ",$row->fecha_registro);
                $fechaCreacion = $fecha[0];
                $horaCreacion = $fecha[1];

                //Verificamos que no se haya excedido el tiempo limite de espera
                // if (strcmp($hoy, $fechaCreacion) === 0) {
                    //Verificamos que el cambio este en tiempo y forma
                    // $horaReg = strtotime($horaCreacion);
                    // $diferencia = $horaAct-$horaReg;
                    // if (($diferencia >= 0 )&&($diferencia <= 1800)) {
                        //El registro aun esta dentro de tiempo y forma
                        $datos['idRegistro'][] = $row->idRegistro;
                        $datos["folio"][] = $row->idPivote;

                        $corte_b = explode("-",$fecha[0]);
                        $datos["limite"][] = $corte_b[2]."-".$corte_b[1]."-".$corte_b[0];
                        $datos["limiteHoraReg"][] = $fecha[1];
                        $datos["falla"][] = $row->definicionFalla;
                        $datos["nombreConsumido1"][] = $row->nombreCliente;
                        $datos["nombreAsesor"][] = $row->nombreAsesor;
                        $datos["audio"][] = (file_exists($row->evidencia)) ? $row->evidencia : "";
                    // }else {
                        //Ya vencio el tiempo de espera, eliminamos el registro
                        // $eliminar = $this->deleteRegistry($row->idRegistro);
                    // }
                // }else {
                    //Ya vencio el tiempo de espera, eliminamos el registro
                    // $eliminar = $this->deleteRegistry($row->idRegistro);
                // }
            }

        }

        $this->loadAllView($datos,'vozCliente/buscador_vcTemp');
    }

    //Recibimos los valones necesarios para actualizar las tablas
    public function validateForm()
    {
        $this->form_validation->set_rules('idOrden', 'El Número de orden es requerido.', 'required');
        $this->form_validation->set_message('required','%s');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->transferData();
        }else{
            $datos["mensaje"]="0";
            $this->index("",$datos);
        }
    }

    //Actualizamos los campos de las tablas
    public function transferData()
    {
        $idTemp = $this->newIndex();
        $limiteHora = date("H:i:s");
        $hoy = date("d-m-Y");

        $horaRegistro = $this->input->post("horaRegistro");
        $folio = $this->input->post("folioTem");
        $fechaReg = $this->input->post("fechReg");

        $idRegistro = $this->input->post("idRegistro");
        $idOrden = $this->input->post("idOrden");

        //Verificamos que el cambio este en tiempo y forma
        $horaReg = strtotime($horaRegistro);
        $horaAct = strtotime($limiteHora);

        //Verificamos que el regsitro sea de ese dia
        // if (strcmp($hoy, $fechaReg) === 0) {
            //Verificamos que no hayan pasado mas de 30 min desde el registro
            // $diferencia = $horaAct-$horaReg;
            // if (($diferencia >= 0 )&&($diferencia <= 1800)) {
                //Aun queda tiempo, actualizamos los campos de la tabla antes de tranferirlos
                $datosTem = array(
                    "idRegistro" => $idTemp,
                    "idPivote" => $idOrden,
                );
                $actualizar = $this->mConsultas->update_table_row('voz_cliente_temp',$datosTem,'idRegistro',$idRegistro);
                if ($actualizar) {
                    //Si se actualizo correctamente el registro, tranferimos la informacion
                    $transpasa = $this->mConsultas->merge_tableVC($idTemp);
                    if ($transpasa) {
                        //Actualizamos la información para el historial de ordenes
                        $evidencia =  $this->mConsultas->audio_evi($datosTem["idPivote"]);
                        $this->actualiza_documentacion($datosTem["idPivote"],$evidencia);

                        //Si el proceso terminos con exito borramos el registro temporal
                        $datosTem1 = array(
                            "idRegistro" => $idRegistro,
                            "idPivote" => "0",
                        );

                        $this->mConsultas->update_table_row('voz_cliente_temp',$datosTem1,'idRegistro',$idTemp);

                        $this->loadHistory($idTemp,"Actualización VC temporal ".$folio."->".$idTemp,"Voz Cliente temporal)");
                        // $this->mConsultas->delete_row('voz_cliente_temp','idRegistro',$idTemp);
                        $datos["mensaje"]="1";
                        //Verificamos si ya se firmaron todos los campos
                        $this->chekingFirm($idOrden);
                    }else {
                        $datos["mensaje"]="0";
                    }
                }else {
                    $datos["mensaje"]="2";
                }
            // }else {
                //Ya vencio el tiempo de espera, eliminamos el registro
                // $datos["respuesta"] = $this->deleteRegistry($idRegistro,$folio);
            // }
        // } else {
            //Ya vencio el tiempo de espera, eliminamos el registro
            // $datos["respuesta"] = $this->deleteRegistry($idRegistro,$folio);
        // }

        $this->index("",$datos);
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion($id_cita = '',$evidencia = "")
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "voz_cliente" => "1",
                "evidencia_voz_cliente" => $evidencia,
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Revisamos si ya se firmaron todos los campos
    public function chekingFirm($idOrden = 0)
    {
        $registro = date("Y-m-d H:i:s");
        //Recuperamos los campos necesarios
        $query = $this->mConsultas->get_result("idPivote",$idOrden,"voz_cliente");
        foreach ($query as $row){
            $firmaAsesor = $row->firmaAsesor;
            $firmaConsumidor1 = $row->firmaCliente;
        }

        if (isset($firmaAsesor)) {
            //Verificamos si existe el registro en la tabla
            $query2 = $this->mConsultas->get_result("idOrden",$idOrden,"estados_pdf");
            foreach ($query2 as $row){
                $multipunto = $row->multipuntoPDF;
                $idPdf = $row->idRegistro;
            }

            if (isset($multipunto)) {
                //Comprobamos los estados validos (actualizar)
                if (($firmaAsesor != "")&&($firmaConsumidor1 != "")) {
                    $data = array(
                        "vozClientePDF" => 1,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                  $data = array(
                      "vozClientePDF" => 0,
                      "fecha_actualizacion" => $registro,
                  );
                }
                $this->mConsultas->update_table_row('estados_pdf',$data,'idRegistro',$idPdf);
            //Si no existe el registro se cres
            }else {
                //Comprobamos los estados validos (registro)
                if (($firmaConsumidor1 != "")&&($firmaAsesor != "")) {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 1,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 0,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }
                $this->mConsultas->save_register('estados_pdf',$data);
            }
        }

        return FALSE;
    }

    //Funcion para eliminar un registro temporal que ya expiro
    public function deleteRegistry($idRegistro = 0)
    {
        $respuesta = "0";
        $eliminar = $this->mConsultas->delete_row('voz_cliente_temp','idegistro',$idRegistro);
        if ($eliminar) {
            $respuesta = "1";
        }else {
            $respuesta = "0";
        }
        return $respuesta;
    }

    //Funcion para generar el folio de la venta
    public function newIndex()
    {
        //Recuperamos el indice para el folio
        $fecha = date("Ymd");
        $registro = $this->mConsultas->last_id_tableVC_2();
        $row = $registro->row();
        if (isset($row)){
          $indexFolio = $registro->row(0)->idRegistro;
        }else {
          $indexFolio = 0;
        }

        $nvoFolio = $indexFolio + 1;

        return $nvoFolio;
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }


    //Recuperamos los registros d ela voz cliente para listarlos
    public function listaVC($x = '',$datos = NULL)
    {
        //Recuperamos todas las cotizaciones que se han generado en la multipunto
        $query = $this->mConsultas->get_table_desc_VC();

        foreach ($query as $row){
            $datos["idRegistro"][] = $row->idRegistro;
            $datos["idIntelisis"][] = $this->mConsultas->id_orden_intelisis($row->idPivote);
            $datos["idOrden"][] = $row->idPivote;
            $datos["id_cita_url"][] = $this->encrypt($row->idPivote);
            $fecha = explode(" ",$row->fecha_registro);
            $fechaFracc = explode("-",$fecha[0]);
            $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
            $datos["definicionFalla"][] = $row->definicionFalla;
            $datos["nombreCliente"][] = $row->nombreCliente;
            $datos["nombreAsesor"][] = $row->nombreAsesor;
            $datos["evidencia"][] = $row->evidencia;
            $datos["componente"][] = $row->componente;
            $datos["sistema"][] = $row->sistema;
            $datos["causaRaiz"][] = $row->causaRaiz;
            $datos["comentario"][] = $row->comentario;
        }

        $this->loadAllView($datos,'vozCliente/listaVC_comentarios');
    }

    //Recuperamos los datos y guardamos el comentario
    public function saveComment()
    {
          $respuesta = "";
          $idCita = $_POST["idOrden"];
          $comentario = $_POST["comentario"];
          $sistema = $_POST["sistema"];
          $componente = $_POST["componente"];
          $causa = $_POST["causa"];

          if (($idCita != "")&&($comentario != "")) {
              $datos = array(
                  'sistema' => $sistema,
                  'componente' => $componente,
                  'causaRaiz' => $causa,
                  'comentario' => $comentario,
              );

             $actualiza = $this->mConsultas->update_table_row('voz_cliente',$datos,'idPivote',$idCita);

             if ($actualiza) {
                  $this->loadHistory($idCita,"Actualización VC comentario","Voz Cliente)");
                  $respuesta = "OK";
             }else {
                  $respuesta = "ERROR";
             }
          }else {
              $respuesta = "ERROR01";
          }

         echo $respuesta;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }


    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                // 'assets/js/firmaCliente.js',
                'assets/js/vozCliente/buscadorVC.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Voz cliente sin orden previa"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }


}
