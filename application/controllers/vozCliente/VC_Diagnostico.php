<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VC_Diagnostico extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'consulta', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('upload_max_filesize',5000);
    }

    public function index($id_cita = 0,$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");
        $datos["idPivote"] = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        if ($this->session->userdata('rolIniciado')) {
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
            $datos["asesor"] = $this->session->userdata('nombreUsuario');
        }

        $datos["idIntelisis"] = ""; //$this->consulta->id_orden_intelisis($id_cita);

        $validar_destino = $this->validar_registro($datos["idPivote"]);

        if ($validar_destino == 3) {
            redirect(base_url().'VCliente2_Revision/'.$this->encrypt($datos["idPivote"]));
        } elseif ($validar_destino == 2) {
            $this->setDataRevision($id_cita);
        }else {
            $this->loadAllView($datos,'vozCliente/nva_estructura/formulario');
        }
    }

    public function add_diagnostico()
    {
        //Revisamos que no exista ese numero de ordenen un formato anterior
        $validar_registro = $this->validar_registro($_POST["idPivote"]);

        if ($validar_registro == 2) {

            //Guardamos el diagnóstico
            $registro = date("Y-m-d H:i:s");
            
            $dataForm["id"] = NULL;
            $dataForm["id_cita"] = $_POST["idPivote"];
            $dataForm["definicion_falla"] = $_POST["falla"];
            $dataForm["indicadores_falla"] = $this->validateCheck($_POST["sentidos"],"S/R");
            $dataForm["diagrama_falla"] = $this->base64ToImage($_POST["fallasImg"],"assets/imgs/diagramas","png");
            $dataForm["presenta_falla"] = $this->validateCheck($_POST["fallasCheck"],"S/R");
            $dataForm["percibe_falla"] = $this->validateCheck($_POST["fallaPercibe"],"S/R");

            //Condiciones ambientales
            if (isset($_POST["temperatura_check"])) {
                $dataForm["temperatura"] = "NO APLICA";
            }else{
                $dataForm["temperatura"] = ((isset($_POST["tempAmbiente"])) ? $_POST["tempAmbiente"] : "");
            }

            if (isset($_POST["humedad_check"])) {
                $dataForm["humedad"] = "NO APLICA";
            }else{
                $dataForm["humedad"] = ((isset($_POST["humedad"])) ? $_POST["humedad"] : "");
            }

            if (isset($_POST["viento_check"])) {
                $dataForm["viento"] = "NO APLICA";
            }else{
                $dataForm["viento"] = ((isset($_POST["viento"])) ? $_POST["viento"] : "");
            }

            if (isset($_POST["frenado_check"])) {
                $dataForm["velocidad"] = "NO APLICA";
            }else{
                $dataForm["velocidad"] = ((isset($_POST["frenoMedidor"])) ? $_POST["frenoMedidor"] : "");
            }
            
            //Condiciones operativas
            if (isset($_POST["transmision_1_check"])) {
                $dataForm["transmision_p1"] = "NO APLICA";
                $dataForm["transmision_p2"] = "NO APLICA";
                $dataForm["transmision"] = "0";
            }else{
                $dataForm["transmision_p1"] = $_POST["cambio"];
                $dataForm["transmision_p2"] = ((isset($_POST["transmision"])) ? $this->validateRadio($_POST["transmision"],"S/R") : "S/R");
                $dataForm["transmision"] = $_POST["cambio_A"];
            }

            if (isset($_POST["rpm_check"])) {
                $dataForm["rmp"] = "NO APLICA";
            }else{
                $dataForm["rmp"] = ((isset($_POST["rpm"])) ? $_POST["rpm"] : "");
            }

            if (isset($_POST["carga_check"])) {
                $dataForm["carga"] = "NO APLICA";
                $dataForm["carga_remolque"] = "0";
            }else{
                $dataForm["carga"] = ((isset($_POST["carga"])) ? $_POST["carga"] : "");
                $dataForm["carga_remolque"] = ((isset($_POST["remolque"])) ? $this->validateRadio($_POST["remolque"],"0") : "0");
            }

            if (isset($_POST["pasajeros_check"])) {
                $dataForm["pasajeros"] = "NO APLICA";
                $dataForm["pasajero_cajuela"] = "NO APLICA";
            }else{
                $dataForm["pasajeros"] = ((isset($_POST["pasajeros"])) ? $_POST["pasajeros"] : "NO APLICA");
                $dataForm["pasajero_cajuela"] = ((isset($_POST["cajuela_2"])) ? $_POST["cajuela_2"] : "NO APLICA");
            }
            
            //Condicones del camino
            if (isset($_POST["estructura_check"])) {
                $dataForm["estructura"] = "NO APLICA";
            }else{
                $dataForm["estructura"] = ((isset($_POST["estructura"])) ? $_POST["estructura"] : "NO APLICA");
            }

            if (isset($_POST["camino_check"])) {
                $dataForm["camino"] = "NO APLICA";
            }else{
                $dataForm["camino"] = ((isset($_POST["camino"])) ? $_POST["camino"] : "NO APLICA");
            }

            if (isset($_POST["pendiente_check"])) {
                $dataForm["pendiente"] = "NO APLICA";
            }else{
                $dataForm["pendiente"] = ((isset($_POST["pendiente"])) ? $_POST["pendiente"] : "NO APLICA");
            }

            if (isset($_POST["superficie_check"])) {
                $dataForm["superficie"] = "NO APLICA";
            }else{
                $dataForm["superficie"] = ((isset($_POST["superficie"])) ? $_POST["superficie"] : "NO APLICA");
            }
            
            //Diagnostico
            $dataForm["diag_sistema"] = ((isset($_POST["sistema"])) ? $_POST["sistema"] : "");
            $dataForm["diag_componente"] = ((isset($_POST["componente"])) ? $_POST["componente"] : "");
            $dataForm["diag_causa_raiz"] = ((isset($_POST["causa"])) ? $_POST["causa"] : "");
            $dataForm["comentarios"] = ((isset($_POST["comentario_diagnostico"])) ? $_POST["comentario_diagnostico"] : "Sin comentarios");

            $dataForm["fecha_registro"] = $registro;
            $dataForm["fecha_actualiza"] = $registro;

            $diagnostico = $this->consulta->save_register('vc_diagnostico',$dataForm);

            if ($diagnostico != 0) {

                $diagnosticos_reg = $this->consulta->total_diag_vc($_POST["idPivote"]);
                $this->loadHistory($_POST["idPivote"],"Registro diagnóstico #".$diagnosticos_reg." después de cerrar el formulario (sin evidencia de audio)","Voz Cliente");

                //Actualizamos el dato en la tabla principal
                $dataForm_2["n_diagnosticos"] = $diagnosticos_reg;
                $dataForm_2["fecha_actualiza"] = date("Y-m-d H:i:s");

                $actualiza = $this->consulta->update_table_row('vc_registro',$dataForm_2,'id_cita',$_POST["idPivote"]);
                if ($actualiza) {
                    $this->loadHistory($_POST["idPivote"],"Se actualiza el total de diagnóticos asignados a la voz cliente","Voz Cliente");
                }

                $respuesta = "OK=".$diagnosticos_reg;

            }else{
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO CORRECTAMENTE"."=";
            }
                
        } else {
            $respuesta = "NO EXISTE UNA VOZ CLIENTE CON EL NÚMERO DE ORDEN : ".$_POST["idPivote"]."=";
        }
        echo $respuesta;
    }

    public function guardar_formulario_diagnostico()
    {
        //Revisamos que no exista ese numero de ordenen un formato anterior
        $validar_registro = $this->validar_registro($_POST["idPivote"]);

        if ($validar_registro == 1) {
            //Validamos que se haya abierto una orden de servicio
            $query = $this->consulta->get_result("id_cita",$_POST["idPivote"],"ordenservicio");
            foreach ($query as $row){
                $validar = $row->id;
            }

            if (isset($validar)) {
                //Verificamos si se ha llenado el diagnostico
                $query2 = $this->consulta->get_result("noServicio",$_POST["idPivote"],"diagnostico");
                foreach ($query2 as $row2){
                    $validar2 = $row2->idDiagnostico;
                }
                
                if (isset($validar2)) {
                    //Guardamos el diagnóstico
                    $registro = date("Y-m-d H:i:s");
                    
                    $dataForm["id"] = NULL;
                    $dataForm["id_cita"] = $_POST["idPivote"];
                    $dataForm["definicion_falla"] = $_POST["falla"];
                    $dataForm["indicadores_falla"] = $this->validateCheck($_POST["sentidos"],"S/R");
                    $dataForm["diagrama_falla"] = $this->base64ToImage($_POST["fallasImg"],"assets/imgs/diagramas","png");
                    $dataForm["presenta_falla"] = $this->validateCheck($_POST["fallasCheck"],"S/R");
                    $dataForm["percibe_falla"] = $this->validateCheck($_POST["fallaPercibe"],"S/R");

                    //Condiciones ambientales
                    if (isset($_POST["temperatura_check"])) {
                        $dataForm["temperatura"] = "NO APLICA";
                    }else{
                        $dataForm["temperatura"] = ((isset($_POST["tempAmbiente"])) ? $_POST["tempAmbiente"] : "NO APLICA");
                    }

                    if (isset($_POST["humedad_check"])) {
                        $dataForm["humedad"] = "NO APLICA";
                    }else{
                        $dataForm["humedad"] = ((isset($_POST["humedad"])) ? $_POST["humedad"] : "NO APLICA");
                    }

                    if (isset($_POST["viento_check"])) {
                        $dataForm["viento"] = "NO APLICA";
                    }else{
                        $dataForm["viento"] = ((isset($_POST["viento"])) ? $_POST["viento"] : "NO APLICA");
                    }

                    if (isset($_POST["frenado_check"])) {
                        $dataForm["velocidad"] = "NO APLICA";
                    }else{
                        $dataForm["velocidad"] = ((isset($_POST["frenoMedidor"])) ? $_POST["frenoMedidor"] : "NO APLICA");
                    }
                    
                    //Condiciones operativas
                    if (isset($_POST["transmision_1_check"])) {
                        $dataForm["transmision_p1"] = "NO APLICA";
                        $dataForm["transmision_p2"] = "NO APLICA";
                        $dataForm["transmision"] = "0";
                    }else{
                        $dataForm["transmision_p1"] = $_POST["cambio"];
                        $dataForm["transmision_p2"] = ((isset($_POST["transmision"])) ? $this->validateRadio($_POST["transmision"],"S/R") : "S/R");
                        $dataForm["transmision"] = $_POST["cambio_A"];
                    }

                    if (isset($_POST["rpm_check"])) {
                        $dataForm["rmp"] = "NO APLICA";
                    }else{
                        $dataForm["rmp"] = ((isset($_POST["rpm"])) ? $_POST["rpm"] : "NO APLICA");
                    }

                    if (isset($_POST["carga_check"])) {
                        $dataForm["carga"] = "NO APLICA";
                        $dataForm["carga_remolque"] = "0";
                    }else{
                        $dataForm["carga"] = ((isset($_POST["carga"])) ? $_POST["carga"] : "");
                        $dataForm["carga_remolque"] = ((isset($_POST["remolque"])) ? $this->validateRadio($_POST["remolque"],"0") : "0");
                    }

                    if (isset($_POST["pasajeros_check"])) {
                        $dataForm["pasajeros"] = "NO APLICA";
                        $dataForm["pasajero_cajuela"] = "NO APLICA";
                    }else{
                        $dataForm["pasajeros"] = ((isset($_POST["pasajeros"])) ? $_POST["pasajeros"] : "NO APLICA");
                        $dataForm["pasajero_cajuela"] = ((isset($_POST["cajuela_2"])) ? $_POST["cajuela_2"] : "NO APLICA");
                    }
                    
                    //Condicones del camino
                    if (isset($_POST["estructura_check"])) {
                        $dataForm["estructura"] = "NO APLICA";
                    }else{
                        $dataForm["estructura"] = ((isset($_POST["estructura"])) ? $_POST["estructura"] : "NO APLICA");
                    }

                    if (isset($_POST["camino_check"])) {
                        $dataForm["camino"] = "NO APLICA";
                    }else{
                        $dataForm["camino"] = ((isset($_POST["camino"])) ? $_POST["camino"] : "NO APLICA");
                    }

                    if (isset($_POST["pendiente_check"])) {
                        $dataForm["pendiente"] = "NO APLICA";
                    }else{
                        $dataForm["pendiente"] = ((isset($_POST["pendiente"])) ? $_POST["pendiente"] : "NO APLICA");
                    }

                    if (isset($_POST["superficie_check"])) {
                        $dataForm["superficie"] = "NO APLICA";
                    }else{
                        $dataForm["superficie"] = ((isset($_POST["superficie"])) ? $_POST["superficie"] : "NO APLICA");
                    }
                    
                    //Diagnostico
                    $dataForm["diag_sistema"] = ((isset($_POST["sistema"])) ? $_POST["sistema"] : "");
                    $dataForm["diag_componente"] = ((isset($_POST["componente"])) ? $_POST["componente"] : "");
                    $dataForm["diag_causa_raiz"] = ((isset($_POST["causa"])) ? $_POST["causa"] : "");
                    $dataForm["comentarios"] = ((isset($_POST["comentario_diagnostico"])) ? $_POST["comentario_diagnostico"] : "Sin comentarios");

                    $dataForm["fecha_registro"] = $registro;
                    $dataForm["fecha_actualiza"] = $registro;

                    $diagnostico = $this->consulta->save_register('vc_diagnostico',$dataForm);
                    if ($diagnostico != 0) {
                        $diagnosticos_reg = $this->consulta->total_diag_vc($_POST["idPivote"]);
                        $this->loadHistory($dataForm["id_cita"],"Registro diagnóstico #".$diagnosticos_reg." Voz Cliente","Voz Cliente");
                        $respuesta = "OK=".$diagnosticos_reg;
                    }else{
                        $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO CORRECTAMENTE"."=";
                    }
                }else {
                    $respuesta = "NO SE HA ABIERTO UN INVENTARIO CON ESTE NÚMERO DE ORDEN: ".$_POST["idPivote"]."=";
                }
            }else {
                $respuesta = "NO SE HA ABIERTO UNA ORDEN DE SERVICIO CON ESTE NÚMERO DE ORDEN: ".$_POST["idPivote"]."=";
            }
        } else {
            if ($validar_registro == 2) {
                $respuesta = "YA EXISTE UNA VOZ CLIENTE CON EL NÚMERO DE ORDEN : ".$_POST["idPivote"]."=";
            } else {
                $respuesta = "YA EXISTE UNA VOZ CLIENTE (FORMATO ANTERIOR) CON ESTE NÚMERO DE ORDEN: ".$_POST["idPivote"]."=";
            }
        }
        echo $respuesta;
    }

    public function validar_registro($id_cita='')
    {
        $respuesta = 1;
        $query = $this->consulta->get_result("idPivote",$id_cita,"voz_cliente");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        if (isset($dato)) {
            $respuesta = 3;
        }else{
            $query = $this->consulta->get_result("id_cita",$id_cita,"vc_registro");
            foreach ($query as $row){
                $dato2 = $row->id;
            }

            if (isset($dato2)) {
                $respuesta = 2;
            }else{
                $respuesta = 1;
            }
        }

        return $respuesta;
    }

    public function guardar_formulario()
    {
        $registro = date("Y-m-d H:i:s");

        $query = $this->consulta->get_result("id_cita",$_POST["id_cita"],"vc_registro");
        foreach ($query as $row){
            $dato = $row->id;
        }

        if (!isset($dato)) {
            $diagnosticos_reg = $this->consulta->total_diag_vc($_POST["id_cita"]); 

            $dataForm["id"] = NULL;
            $dataForm["id_cita"] = $_POST["id_cita"];
            $dataForm["n_diagnosticos"] = $diagnosticos_reg;
            $dataForm["nombre_asesor"] = $_POST["nombreAsesor"];
            $dataForm["firma_asesor"] = $this->base64ToImage($_POST["rutaFirmaAsesor"],"assets/imgs/firmas","png");
            $dataForm["nombre_cliente"] = $_POST["consumidor1Nombre"];
            $dataForm["firma_cliente"] = $this->base64ToImage($_POST["rutaFirmaAcepta"],"assets/imgs/firmas","png");
            $dataForm["fecha_registro"] = $registro;
            $dataForm["fecha_actualiza"] = $registro;

            $vc_registro = $this->consulta->save_register('vc_registro',$dataForm);
            if ($vc_registro != 0) {
                //Actualizamos la información para el historial de ordenes
                $this->actualiza_documentacion($dataForm["id_cita"],"");

                //Actualizamos  los campos de la orden y guardamos el movimiento
                if ($dataForm["firma_cliente"] != "") {
                    $contededor["firma_vc"] = "1";
                    $actualiza = $this->consulta->update_table_row('ordenservicio',$contededor,'id_cita',$dataForm["id_cita"]);

                    $this->loadHistory($dataForm["id_cita"],"Registro completo","Voz Cliente");
                } else {
                    $this->loadHistory($dataForm["id_cita"],"Registro sin firma del cliente (autorizacion por ausencia)","Voz Cliente");
                }

                //Guardamos el movimiento
                $this->loadHistory($dataForm["id_cita"],"Alta formulario","Voz Cliente");

                //Verificamos quien creo el formulario
                if ($this->session->userdata('rolIniciado')) {
                    //Si quien inicio sesion fue un jefe de taller
                    if ($this->session->userdata('rolIniciado') == "JDT") {
                        $respuesta = "OK=".'Panel_JefeTaller';
                    }else {
                        $respuesta = "OK=".'Panel_Asesor';
                    }
                }else{
                    //Asumimos que sue un asesor quien inicio sesion
                    $respuesta = "OK=".'VCliente_Revision/'.$this->encrypt($dataForm["id_cita"]);
                }
                
            }else{
                $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO=";
            }
        }else {
            $respuesta = "YA EXISTE UNA VOZ CLIENTE CON ESTE NO. ORDEN=";
        }
        
        echo $respuesta;
    }

    public function guardar_formulario_p2()
    {
        $registro = date("Y-m-d H:i:s");

        $cadena = $this->descomprimir();
        $dataForm["audio"] = $this->base64ToImage($cadena ,"assets/audios","ogg");

        if ($_POST["desicion_cliente"] == "1") {
            $afirma_audio = 'El cliente acepta que se grabe el audio del diagnóstico. <br>'. (($dataForm["audio"] != "") ? 'Se grabo el audio' : 'No se grabo el audio');
        }else{
            $afirma_audio = 'El cliente NO acepta que se grabe el audio del diagnóstico. <br>'. (($dataForm["audio"] != "") ? 'Se grabo el audio' : 'No se grabo el audio');
        }

        $dataForm["acepta_audio"] = $afirma_audio;
        
        $actualiza2 = $this->consulta->update_table_row('vc_registro',$dataForm,'id_cita',$_POST["id_cita"]);
        if ($actualiza2) {
            //Actualizamos la información para el historial de ordenes
            $this->actualiza_documentacion($_POST["id_cita"],$dataForm["audio"]);

            //Guardamos el movimiento
            if ($_POST["acepta_audio"] == "1") {
                if ($dataForm["audio"] != "") {
                    $this->loadHistory($_POST["id_cita"],"Actualizar formulario (guardar audio)","Voz Cliente");
                }else{
                    $this->loadHistory($_POST["id_cita"],"Actualizar formulario (no se pudo guardar el audio)","Voz Cliente");
                }
            }else{
                $this->loadHistory($_POST["id_cita"],"Actualizar formulario (el cliente no acepto grabar el audio)","Voz Cliente");
            }

            //Verificamos quien creo el formulario
            if ($this->session->userdata('rolIniciado')) {
                //Si quien inicio sesion fue un jefe de taller
                if ($this->session->userdata('rolIniciado') == "JDT") {
                    $respuesta = "OK=".'Panel_JefeTaller';
                }else {
                    $respuesta = "OK=".'Panel_Asesor';
                }
            }else{
                //Asumimos que sue un asesor quien inicio sesion
                $respuesta = "OK=".'VCliente_Revision/'.$this->encrypt($_POST["id_cita"]);
            }
            
        }else{
            $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO=";
        }
        
        echo $respuesta;
    }    

    //Unimos las partes del audio en texto
    public function descomprimir()
    {
        $respuesta = "";

        for ($i=1; $i <10 ; $i++) {
            if ($_POST["blockAudio_".$i] != "") {
                $respuesta .= trim($_POST["blockAudio_".$i]);
            }
        }
        return $respuesta;
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion($id_cita = '',$evidencia = "")
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->consulta->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "voz_cliente" => "1",
                "evidencia_voz_cliente" => $evidencia,
            );

            //Guardamos el registro
            $actualiza = $this->consulta->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Comprobamos que al final se genera la vista de revision
    public function setDataFormulario($id_cita = 0)
    {
        $datos["destinoVista"] = "Formulario";
        $datos["id_cita_url"] = $id_cita;
        $id_cita_de = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita_de,$datos);
    }

    public function setDataRevision($id_cita = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $datos["id_cita_url"] = $id_cita;
        $id_cita_de = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita_de,$datos);
    }

    public function setDataPDF($id_cita = 0)
    {
        $datos["destinoVista"] = "PDF";
        $datos["id_cita_url"] = $id_cita;
        $id_cita_de = ((ctype_digit($id_cita)) ? $id_cita : $this->decrypt($id_cita));
        $this->setData($id_cita_de,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData($id_cita = 0,$datos = NULL)
    {
        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "PDF";
        }

        //Recuperamos el folio de intelisis
        $datos["orden_intelisis"] = ""; //$this->consulta->id_orden_intelisis($id_cita);
        $datos["id_cita"] = $id_cita;

        if ($datos["destinoVista"] == "Formulario") {
            $validar_destino = 2;
        }else{
            $validar_destino = $this->validar_registro($id_cita);
        }

        if ($validar_destino == 2) {
            //Cargamos la información del formulario
            $query = $this->consulta->get_result("id_cita",$id_cita,"vc_diagnostico");
            foreach ($query as $row) {

                $datos["definicionFalla"][] = explode(" ",$row->definicion_falla);
                $datos["definicionFalla_txt"][] = $row->definicion_falla;
                $datos["imgFallas"][] = (file_exists($row->diagrama_falla)) ? $row->diagrama_falla : ""; 

                //Recuperamos los valores que debemos tratar para imprimir
                $datos["sentidosFalla"][] = explode("|", $row->indicadores_falla);
                $datos["presentaFalla"][] = explode("|", $row->presenta_falla);
                $datos["percibeFalla"][] = explode("|",$row->percibe_falla);

                //Interpretacion para indicadores
                if ($row->temperatura != "NO APLICA") {
                    $temperatura = (int)$row->temperatura;
                    if (($temperatura > 0)&&($row->temperatura != "")) {
                        if ($temperatura == 0) {
                            $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_1.png';
                        }elseif (($temperatura > 0)&&($temperatura < 11)) {
                            $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_2.png';
                        }elseif (($temperatura > 10)&&($temperatura < 21)) {
                            $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_3.png';
                        }elseif (($temperatura > 20)&&($temperatura < 31)) {
                            $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_4.png';
                        }elseif (($temperatura > 30)&&($temperatura < 41)) {
                            $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_5.png';
                        }elseif (($temperatura > 40)&&($temperatura < 51)) {
                            $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_6.png';
                        }elseif (($temperatura > 50)&&($temperatura < 61)) {
                            $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_7.png';
                        }else{
                            $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_8.png';
                        }                      
                    }else {
                        //$datos["logTempBarra"][] = 'assets/imgs/metrix2.png'; 
                        $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/metrix2_1.png';
                    }
                } else {
                    $datos["logTempBarra"][] = 'assets/imgs/reducidos/condiciones/nc/metrix2_nc.jpg';
                }   

                $humedad = $row->humedad;
                if ($humedad == "Seco") {
                    $datos["logHumBarra"][] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_1.png';
                }elseif ($humedad == "Húmedo") {
                    $datos["logHumBarra"][] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_2.png';
                }elseif ($humedad == "Mojado") {
                    $datos["logHumBarra"][] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_3.png';
                }elseif ($humedad == "Lluvia") {
                    $datos["logHumBarra"][] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_4.png';
                }elseif ($humedad == "Hielo") {
                    $datos["logHumBarra"][] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_5.png';
                }else{
                    if ($humedad == "NO APLICA") {
                        $datos["logHumBarra"][] = 'assets/imgs/reducidos/condiciones/nc/metrix_humedad_nc.jpg';
                    } else {
                        $datos["logHumBarra"][] = 'assets/imgs/metrix_humedad2.png'; 
                    }
                }

                $viento = $row->viento;
                if ($viento == "Ligero") {
                    $datos["logVientoBarra"][] = 'assets/imgs/reducidos/condiciones/metrix_viento2_1.png';
                }elseif ($viento == "Medio") {
                    $datos["logVientoBarra"][] = 'assets/imgs/reducidos/condiciones/metrix_viento2_2.png';
                }elseif ($viento == "Fuerte") {
                    $datos["logVientoBarra"][] = 'assets/imgs/reducidos/condiciones/metrix_viento2_3.png';
                }else{
                    if ($viento == "NO APLICA") {
                        $datos["logVientoBarra"][] = 'assets/imgs/reducidos/condiciones/nc/metrix_viento_nc.jpg';
                    } else {
                        $datos["logVientoBarra"][] = 'assets/imgs/metrix_viento2.png'; 
                    }
                }

                if ($row->velocidad != "NO APLICA") {
                    $velocidad = (int)$row->velocidad;
                    if (($velocidad > 0)&&($row->velocidad != "")) {
                        if ($velocidad < 21) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_1.png';
                        }elseif (($velocidad > 20)&&($velocidad < 41)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_2.png';
                        }elseif (($velocidad > 40)&&($velocidad < 61)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_3.png';
                        }elseif (($velocidad > 60)&&($velocidad < 81)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_4.png';
                        }elseif (($velocidad > 80)&&($velocidad < 101)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_5.png';
                        }elseif (($velocidad > 100)&&($velocidad < 121)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_6.png';
                        }elseif (($velocidad > 120)&&($velocidad < 141)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_7.png';
                        }elseif (($velocidad > 140)&&($velocidad < 161)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_8.png';
                        }elseif (($velocidad > 160)&&($velocidad < 181)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_9.png';
                        }elseif (($velocidad > 180)&&($velocidad < 201)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_10.png';
                        }elseif (($velocidad > 200)&&($velocidad < 241)) {
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_11.png';
                        }else{
                            $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/velocidad3_13.png';
                        }                      
                    }else {
                        $datos["logVelBarra"][] = 'assets/imgs/velocidad3.png'; 
                    }
                } else {
                    $datos["logVelBarra"][] = 'assets/imgs/reducidos/condiciones/nc/velocidad3_nc.jpg';
                }
                
                $datos["ind_transmision"][] = $row->transmision_p2;
                if ($row->transmision_p2 == "2") {
                    $datos["transmision_2"][] = 'assets/imgs/reducidos/condiciones/cambio3_a_1.png';
                }elseif ($row->transmision_p2 == "4") {
                    $datos["transmision_2"][] = 'assets/imgs/reducidos/condiciones/cambio3_a_2.png';
                }else{
                    $datos["transmision_2"][] = 'assets/imgs/reducidos/condiciones/cambio3_a.png';
                }

                $camino_1 = $row->transmision_p1;
                if ($camino_1 == "R") {
                  $datos["logCam1Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_b_1.png';
                }elseif ($camino_1 == "1") {
                  $datos["logCam1Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_b_2.png';
                }elseif ($camino_1 == "2") {
                  $datos["logCam1Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_b_3.png';
                }elseif ($camino_1 == "3") {
                  $datos["logCam1Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_b_4.png';
                }elseif ($camino_1 == "4") {
                  $datos["logCam1Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_b_5.png';
                }elseif ($camino_1 == "5") {
                  $datos["logCam1Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_b_6.png';
                }elseif ($camino_1 == "6") {
                  $datos["logCam1Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_b_7.png';
                }else{
                    $datos["logCam1Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_b.png';
                }

                $operativa = $row->transmision;
                if ($operativa == "P") {
                  $datos["logCam2Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_c_1.png';
                }elseif ($operativa == "R") {
                  $datos["logCam2Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_c_2.png';
                }elseif ($operativa == "N") {
                  $datos["logCam2Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_c_3.png';
                }elseif ($operativa == "D") {
                  $datos["logCam2Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_c_4.png';
                }elseif ($operativa == "L") {
                  $datos["logCam2Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_c_5.png';
                }else{
                    if ($operativa == "NO APLICA") {
                        $datos["logCam2Barra"][] = 'assets/imgs/reducidos/condiciones/nc/cambio3_c_nc.jpg';
                    } else {
                        $datos["logCam2Barra"][] = 'assets/imgs/reducidos/condiciones/cambio3_c.png';
                    }
                }

                $rmp = $row->rmp;
                if ($rmp == "1") {
                  $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/rpm3_1.png';
                }elseif ($rmp == "2") {
                  $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/rpm3_2.png';
                }elseif ($rmp == "3") {
                  $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/rpm3_3.png';
                }elseif ($rmp == "4") {
                  $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/rpm3_4.png';
                }elseif ($rmp == "5") {
                  $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/rpm3_5.png';
                }elseif ($rmp == "6") {
                  $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/rpm3_6.png';
                }elseif ($rmp == "7") {
                  $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/rpm3_7.png';
                }elseif ($rmp == "8") {
                  $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/rpm3_8.png';
                }else{
                    if ($rmp == "NO APLICA") {
                        $datos["logRMPBarra"][] = 'assets/imgs/reducidos/condiciones/nc/rpm3_nc.jpg';
                    } else {
                        $datos["logRMPBarra"][] = 'assets/imgs/rpm3.png';
                    }
                }

                if ($row->carga != "NO APLICA") {
                    $carga = (int)$row->carga;
                    if (($carga > 0)&&($row->carga != "")) {
                        if ($carga < 26) {
                            $datos["logCargaBarra"][] = 'assets/imgs/reducidos/condiciones/carga3_1.png';
                        }elseif (($carga > 25)&&($carga < 51)) {
                            $datos["logCargaBarra"][] = 'assets/imgs/reducidos/condiciones/carga3_2.png';
                        }elseif (($carga > 50)&&($carga < 76)) {
                            $datos["logCargaBarra"][] = 'assets/imgs/reducidos/condiciones/carga3_3.png';
                        }else{
                            $datos["logCargaBarra"][] = 'assets/imgs/reducidos/condiciones/carga3_4.png';
                        }
                    }else {
                        $datos["logCargaBarra"][] = 'assets/imgs/carga3.png'; 
                    }
                } else {
                    $datos["logCargaBarra"][] = 'assets/imgs/reducidos/condiciones/nc/carga3_nc.jpg'; 
                }
                    

                $datos["remolque"][] = (($row->carga_remolque != "NO APLICA") ? $row->carga_remolque : 0);

                if ($row->pasajeros != "NO APLICA") {
                    $pasajeros = $row->pasajeros;
                    if ($pasajeros == "1") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_1.png';
                    }elseif ($pasajeros == "2") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_2.png';
                    }elseif ($pasajeros == "3") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_3.png';
                    }elseif ($pasajeros == "4") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_4.png';
                    }elseif ($pasajeros == "5") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_5.png';
                    }elseif ($pasajeros == "6") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_6.png';
                    }elseif ($pasajeros == "7") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_7.png';
                    }elseif ($pasajeros == "8") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_8.png';
                    }elseif ($pasajeros == "9") {
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_9.png';
                    }else{
                        $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a.png';
                    }
                } else {
                    $datos["logPasajeBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_a.png';
                }

                $datos["ind_pasajero"][] = $row->pasajero_cajuela;
                if ($row->pasajero_cajuela != "NO APLICA") {
                    $cajuela_temp = (int)$row->pasajero_cajuela;
                    if (($cajuela_temp > 0)&&($row->pasajero_cajuela != "")) {
                        if ($cajuela_temp < 26) {
                            $datos["logCajuelaBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_b_1.png';
                        }elseif (($cajuela_temp > 25)&&($cajuela_temp < 51)) {
                            $datos["logCajuelaBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_b_2.png';
                        }elseif (($cajuela_temp > 50)&&($cajuela_temp < 76)) {
                            $datos["logCajuelaBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_b_3.png';
                        }else{
                            $datos["logCajuelaBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_b_4.png';
                        }
                    }else {
                        $datos["logCajuelaBarra"][] = 'assets/imgs/reducidos/condiciones/pasajeros3_b.png'; 
                    }
                } else {
                    $datos["logCajuelaBarra"][] = 'assets/imgs/reducidos/condiciones/nc/pasajeros3_nc.jpg'; 
                }

                $estructura = $row->estructura;
                if ($estructura == "Plano") {
                    $datos["logEstrBarra"][] = 'assets/imgs/reducidos/condiciones/estructura3_1.png';
                }elseif ($estructura == "Vado") {
                    $datos["logEstrBarra"][] = 'assets/imgs/reducidos/condiciones/estructura3_2.png';
                }elseif ($estructura == "Tope") {
                    $datos["logEstrBarra"][] = 'assets/imgs/reducidos/condiciones/estructura3_3.png';
                }elseif ($estructura == "Baches") {
                    $datos["logEstrBarra"][] = 'assets/imgs/reducidos/condiciones/estructura3_4.png';
                }elseif ($estructura == "Vibradores") {
                    $datos["logEstrBarra"][] = 'assets/imgs/reducidos/condiciones/estructura3_5.png';
                }else{
                    if ($estructura == "NO APLICA") {
                        $datos["logEstrBarra"][] = 'assets/imgs/reducidos/condiciones/nc/estructura3_nc.jpg';
                    } else {
                        $datos["logEstrBarra"][] = 'assets/imgs/estructura3.png';
                    }
                }

                $camino = $row->camino;
                if ($camino == "Recto") {
                    $datos["logCamBarra"][] = 'assets/imgs/reducidos/condiciones/camino3_1.png';
                }elseif ($camino == "Curva ligera") {
                    $datos["logCamBarra"][] = 'assets/imgs/reducidos/condiciones/camino3_2.png';
                }elseif ($camino == "Curva cerrada") {
                    $datos["logCamBarra"][] = 'assets/imgs/reducidos/condiciones/camino3_3.png';
                }elseif ($camino == "Sinuoso") {
                    $datos["logCamBarra"][] = 'assets/imgs/reducidos/condiciones/camino3_4.png';
                }else{
                    if ($camino == "NO APLICA") {
                        $datos["logCamBarra"][] = 'assets/imgs/reducidos/condiciones/nc/camino3_nc.jpg';
                    } else {
                        $datos["logCamBarra"][] = 'assets/imgs/camino3.png';
                    }
                }

                $pendiente = $row->pendiente;
                if ($pendiente == "Recto") {
                    $datos["logPenBarra"][] = 'assets/imgs/reducidos/condiciones/pendiente3_1.png';
                }elseif ($pendiente == "Pendiente ligera 10°") {
                    $datos["logPenBarra"][] = 'assets/imgs/reducidos/condiciones/pendiente3_2.png';
                }elseif ($pendiente == "Pendiente media") {
                    $datos["logPenBarra"][] = 'assets/imgs/reducidos/condiciones/pendiente3_3.png';
                }elseif ($pendiente == "Montaña") {
                    $datos["logPenBarra"][] = 'assets/imgs/reducidos/condiciones/pendiente3_4.png';
                }else{
                    if ($pendiente == "NO APLICA") {
                        $datos["logPenBarra"][] = 'assets/imgs/reducidos/condiciones/nc/pendiente3_nc.jpg';
                    } else {
                        $datos["logPenBarra"][] = 'assets/imgs/pendiente3.png';
                    }
                }

                $superficie = $row->superficie;
                if ($superficie == "Pavimento") {
                    $datos["logSupeBarra"][] = 'assets/imgs/reducidos/condiciones/superficie3_1.png';
                }elseif ($superficie == "Terracería") {
                    $datos["logSupeBarra"][] = 'assets/imgs/reducidos/condiciones/superficie3_2.png';
                }elseif ($superficie == "Empedrado") {
                    $datos["logSupeBarra"][] = 'assets/imgs/reducidos/condiciones/superficie3_3.png';
                }elseif ($superficie == "Adoquín") {
                    $datos["logSupeBarra"][] = 'assets/imgs/reducidos/condiciones/superficie3_4.png';
                }elseif ($superficie == "Fango") {
                    $datos["logSupeBarra"][] = 'assets/imgs/reducidos/condiciones/superficie3_5.png';
                }else{
                    if ($superficie == "NO APLICA") {
                        $datos["logSupeBarra"][] = 'assets/imgs/reducidos/condiciones/nc/superficie3_nc.jpg';
                    } else {
                        $datos["logSupeBarra"][] = 'assets/imgs/superficie3.png';
                    }
                }

                $datos["sistema"][] = $row->diag_sistema;
                $datos["componente"][] = $row->diag_componente;
                $datos["causaRaiz"][] = $row->diag_causa_raiz;
                $datos["comentarios"][] = $row->comentarios;
            }

            $query2 = $this->consulta->get_result("id_cita",$id_cita,"vc_registro");
            foreach ($query2 as $row) {
                $datos["audio"] = $row->audio;
                $datos["audio_eleccion"] = $row->acepta_audio;
                $datos["nombreAsesor"] = trim($row->nombre_asesor);
                $datos["firmaAsesor"] = (file_exists($row->firma_asesor)) ? $row->firma_asesor : "";
                $datos["nombreCliente"] = trim($row->nombre_cliente);
                $datos["firmaCliente"] = (file_exists($row->firma_cliente)) ? $row->firma_cliente : "";
            }
            
            
            //Generamos el PDF con los datos o la vista rapida
            if ($datos["destinoVista"] == "PDF") {
                $this->generatePDF($datos);

            }elseif ($datos["destinoVista"] == "Formulario") {
                $datos["total_diag_vc"] = $this->consulta->total_diag_vc($id_cita);

                $this->loadAllView($datos,'vozCliente/nva_estructura/add_diagnostico');

            }else{
                $this->loadAllView($datos,'vozCliente/nva_estructura/vista_rapida');

            }
        } elseif ($validar_destino == 2) {
            if ($datos["destinoVista"] == "PDF") {
                redirect(base_url().'VCliente2_PDF/'.$this->encrypt($id_cita));

            }else{
                redirect(base_url().'VCliente2_Revision/'.$this->encrypt($id_cita));

            }
        }else{
            redirect(base_url().'VCliente_Alta/'.$this->encrypt($id_cita));

        }
    }  

    public function listado($indicador = NULL,$datos = NULL)
    {
        $mes_actual = date("m");
        $fecha_busqueda = (($mes_actual <= 1) ? (date("Y")-1) : date("Y"))."-".(($mes_actual <= 1) ? "12" : ($mes_actual -1))."-20";
        $query = $this->consulta->lista_vc($fecha_busqueda); 

        foreach ($query as $row){
            if (isset($row->id_vc)) {
                $datos["diagnostico"][] = "Diagnóstico(s) Guardado(s): ".$row->n_diagnosticos;
                $datos["evidencia"][] = $row->audio;
                $datos["tipo_registro"][] = "2";
            } else {
                $datos["diagnostico"][] = $row->definicionFalla;
                $datos["evidencia"][] = $row->evidencia;
                $datos["tipo_registro"][] = "1";
            }
            
            $datos["id_cita"][] = $row->id_cita;
            $datos["tecnico"][] = $row->tecnico;
            $datos["asesor"][] = $row->asesor;
            $datos["cliente"][] = $row->cliente;

            $fecha = new DateTime($row->fecha_recepcion.' '.$row->hora_recepcion);
            $datos["fecha_recepcion"][] = $fecha->format('d-m-Y');  

            $datos["id_cita_url"][] = $this->encrypt($row->id_cita);  
        }

        $this->loadAllView($datos,'vozCliente/nva_estructura/listado_vozcliente');
    }

    public function recuperar_informacion()
    {
        $id_cita = $_POST["orden"];
        $respuesta = "";
        $query = $this->consulta->get_result("idPivote",$id_cita,"voz_cliente");
        foreach ($query as $row) {
            $respuesta .= trim($row->nombreAsesor)."=";
            $respuesta .=  trim($row->nombreCliente)."=";
            $respuesta .= $row->definicionFalla."=";
            $respuesta .= (($row->comentario != "") ? $row->comentario : "Sin información")."=";
            $respuesta .= (($row->sistema != "") ? $row->sistema : "Sin información")."=";
            $respuesta .= (($row->componente != "") ? $row->componente : "Sin información")."=";
            $respuesta .= (($row->causaRaiz != "") ? $row->causaRaiz : "Sin información")."=";
            $respuesta .= $row->evidencia."=";
        }

        echo $respuesta;
    }

    public function lista_diagnosticos()
    {
        $id_cita = $_POST["orden"];
        $respuesta = "";

        $query = $this->consulta->get_result("id_cita",$id_cita,"vc_registro");
        foreach ($query as $row) {
            $respuesta .= $row->nombre_cliente."=";
            $respuesta .= $row->nombre_asesor."=";
            $respuesta .= $row->audio."=";
            $respuesta .= "/-/";
        }

        $query = $this->consulta->get_result("id_cita",$id_cita,"vc_diagnostico");
        foreach ($query as $row) {
            $respuesta .= $row->id."=";
            $falla = substr($row->definicion_falla,0,40)."...";
            $respuesta .= str_replace("="," ",$row->definicion_falla)."=";
            $respuesta .= "|";
        }

        echo $respuesta;
    }

    public function recuperar_informacion_2()
    {
        $registro = $_POST["id_registro"];
        $respuesta = "";
        $query = $this->consulta->get_result("id",$registro,"vc_diagnostico");
        foreach ($query as $row) {
            $respuesta .= (($row->definicion_falla != "") ? $row->definicion_falla : "Sin información")."=";
            $respuesta .= (($row->diag_sistema != "") ? $row->diag_sistema : "Sin información")."=";
            $respuesta .= (($row->diag_componente != "") ? $row->diag_componente : "Sin información")."=";
            $respuesta .= (($row->diag_causa_raiz != "") ? $row->diag_causa_raiz : "Sin información")."=";
            $respuesta .= (($row->comentarios != "") ? $row->comentarios : "Sin información")."=";
        }

        echo $respuesta;
    }

    public function guardar_comentario()
    {
        $respuesta = "";

        //Recuperamos los valores para comparar y evaluar la edicion
        $query = $this->consulta->get_result("id",$_POST["registro"],"vc_diagnostico");
        foreach ($query as $row) {
            $diag_sistema = $row->diag_sistema;
            $diag_componente = $row->diag_componente;
            $diag_causa_raiz = $row->diag_causa_raiz;
            $comentarios = $row->comentarios;
        }

        $evaluacion = 0;
        $movimiento = "";
        if (strtoupper($diag_sistema) != strtoupper($_POST["sistema"])) {
            $contenido["diag_sistema"] = $_POST["sistema"];
            $movimiento .= " [Sistema: ".$diag_sistema." => ".$_POST["sistema"]."] ";
            $evaluacion = 1;
        }

        if (strtoupper($diag_componente) != strtoupper($_POST["componente"])) {
            $contenido["diag_componente"] = $_POST["componente"];
            $movimiento .= " [Componente: ".$diag_componente." => ".$_POST["componente"]."] ";
            $evaluacion = 1;
        }

        if (strtoupper($diag_causa_raiz) != strtoupper($_POST["causa"])) {
            $contenido["diag_causa_raiz"] = $_POST["causa"];
            $movimiento .= " [Causa Raíz: ".$diag_causa_raiz." => ".$_POST["causa"]."] ";
            $evaluacion = 1;
        }

        if (strtoupper($comentarios) != strtoupper($_POST["comentario"])) {
            $contenido["comentarios"] = $_POST["comentario"];
            $movimiento .= " [Comentario: ".$comentarios." => ".$_POST["comentario"]."] ";
            $evaluacion = 1;
        }

        if ($evaluacion == 1) {
            //Guardamos los cambios y regristramos el movimiento
            $actualiza = $this->consulta->update_table_row('vc_diagnostico',$contenido,'id',$_POST["registro"]);
            if ($actualiza) {
                $movimiento_final = "Se actualizan comentarios del diagnóstico [".$_POST["registro"]."], de la orden : ".$_POST["id_cita"]. " ".$movimiento;
                $this->loadHistory($_POST["id_cita"],$movimiento_final,"Voz Cliente (NF)");

                $respuesta = "OK";
            }else {
                $respuesta = "OCURRIO UN PROBLEMA AL ACTUAIZAR LA INFORMACIÓN";
            }
        } else {
            $respuesta = "NO SE ENCONTRO NINGUN CAMBIO A REALIZAR";
        }
        
        echo $respuesta;
    }

    public function filtrovoz_cliente()
    {
        $cadena = "";
        //$id_cita = $_POST["idCita"];
        $campo = $_POST["campo"];

        if ($campo != "") {
            $query = $this->consulta->busqueda_vc($campo); 
            foreach ($query as $row){
                $cadena .= $row->id_cita."=";       //0
                $cadena .= $row->tecnico."=";       //1
                $cadena .= $row->asesor."=";        //2
                $cadena .= $row->cliente."=";       //3

                $fecha = new DateTime($row->fecha_recepcion.' '.$row->hora_recepcion);
                $cadena .= $fecha->format('d-m-Y')."=";     //4

                if (isset($row->id_vc)) {
                    $cadena .= "Diagnóstico(s) Guardado(s) : ".$row->n_diagnosticos."=";        //5
                    $cadena .= $row->audio."=";         //6
                    $cadena .= "2"."=";         //7
                } else {
                    $cadena .= $row->definicionFalla."=";       //5
                    $cadena .= $row->evidencia."=";         //6
                    $cadena .= "1"."=";         //7
                }

                $cadena .= $row->folio_externo."=";         //8
                $cadena .= $this->encrypt($row->id_cita)."=";   //9

                $cadena .= "|";
            }
        }else{
            $cadena = "ERROR";
        }

        echo $cadena;
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->consulta->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "",$ext = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_VC";
        for($i=0; $i<=5; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($direcctorio)){
            mkdir($direcctorio, 0647, true);
        }

        //Creamos la ruta donde se guardara la firma y su extensión
        if (strlen($imgBase64)>1) {
            $base = $direcctorio.'/'.$urlNombre.".".$ext;
            $data = explode(',', $imgBase64);
            $Base64Img = base64_decode($data[1]);
            file_put_contents($base, $Base64Img);
        }else {
            $base = "";
        }

        return $base;
    }

    //Validar multiple check
    public function validateCheck($campo = NULL,$elseValue = "")
    {
        $respuesta = "";
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            if (count($campo) > 0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."|";
                }
            }
        }
        return $respuesta;
    }

    //Validar Radio
    public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Consultamos pdf de la voz por medio de ajax
    public function existenciaVerifica()
    {
        $respuesta = "FALSE";
        $idOrden = $_POST['serie'];
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->consulta->get_result("idPivote",$idOrden,"voz_cliente");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "OK=".$this->encrypt($idOrden);
        }else {
            $query = $this->consulta->get_result("id_cita",$idOrden,"vc_registro");
            foreach ($query as $row){
                $dato2 = $row->id;
            }

            if (isset($dato2)) {
                $respuesta = "OK1=".$this->encrypt($idOrden);
            }else{
                $respuesta = "NO EXISTE=0";
            }
        }

        echo  $respuesta;
    }

    //Consultamos la existencia de la vc y sus requerimientos para el llenado
    public function revision_datos()
    {
        $respuesta = "";
        $id_cita = $_POST['orden'];

        $revision_existencia = $this->validar_registro($id_cita);

        if ($revision_existencia == 3) {
            $respuesta = "OK=".$this->encrypt($id_cita);

        } elseif ($revision_existencia == 2) {
            $respuesta = "OK1=".$this->encrypt($id_cita);
        } else {
            $respuesta = "OK2=";
            $consulta = $this->consulta->datos_generales_orden($id_cita);
            foreach ($consulta as $row){
                $respuesta .= $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno."=";
                $respuesta .= $row->asesor."=";
                //$respuesta .= $row->nombre_compania."=";
            }

            $query = $this->consulta->get_result("id_cita",$id_cita,"documentacion_ordenes");
            foreach ($query as $row){
                $validar = $row->id;
            }

            $respuesta .= ((isset($validar)) ? "" : "NO SE HA ABIERTO UN INVENTARIO CON ESTE NO. DE ORDEN");
        }

        echo  $respuesta;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("vozCliente/nva_estructura/plantillaPdf", $datos);
        //$this->load->helper('dompdf');  
        //$vista = $this->load->view("vozCliente/nva_estructura/plantillaPdf", $datos, TRUE);
        //pdf_create_vertical($vista, 'Voz Cliente #'.$datos["id_cita"]); 
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_VC.js',                
                'assets/js/vozCliente/nvo_formulario/buscador_vc.js',
                'assets/js/vozCliente/nvo_formulario/grabado_audio.js',
                'assets/js/vozCliente/nvo_formulario/envio_formularios.js',
                'assets/js/vozCliente/nvo_formulario/indicadores_slider.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Voz Cliente"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
