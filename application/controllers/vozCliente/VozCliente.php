<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class VozCliente extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        //Cargamos la libreria de email
        $this->load->library('email');
    }

    public function index($idCliente = 0,$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");

        $id_cita = ((ctype_digit($idCliente)) ? $idCliente : $this->decrypt($idCliente));

        $datos["idPivote"] = $id_cita;

        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
            $datos["asesor"] = $this->session->userdata('nombreUsuario');
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        $this->loadAllView($datos,'vozCliente/vozCliente_alta');
    }

    //Validamos campor obligarotios
    public function validateForm()
    {

        $datos["tipoRegistro"] = $this->input->post("cargaFormulario");
        $datos["idServicio"] = $this->input->post("idServicio");
        $datos["idPivote"] = $this->input->post("idPivote");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idPivote"]);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        //if ($datos["tipoRegistro"] == "1") {
            $this->form_validation->set_rules('idPivote', 'La orden de servicio es requerida.', 'required|callback_existencia|callback_ordenPrevia');

            $this->form_validation->set_rules('falla', 'Definicion de falla requerida.', 'required');
            //Validación las firmas
            $this->form_validation->set_rules('rutaFirmaAsesor', 'Firma requerida.', 'required');
            $this->form_validation->set_rules('rutaFirmaAcepta', 'Firma requerida.', 'required');
            //$this->form_validation->set_rules('rutaFirmaAcepta', 'Firma requerida.', 'callback_validarFirma');

            //Validación de nombres de quienes firman
            $this->form_validation->set_rules('nombreAsesor', 'Nombre del asesor requerido', 'required');
            $this->form_validation->set_rules('consumidor1Nombre', 'Nombre del cliente requerido', 'required');

            $this->form_validation->set_rules('falla', 'Campo requerido', 'required');

            //Archivo de audio
            // $this->form_validation->set_rules('evidencia', 'Firma requerida.', 'required');
        //}

        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('validarFirma','La firma es requeridda.');
        $this->form_validation->set_message('existencia','El No. de Orden ya existe. Favor de verificar.');
        $this->form_validation->set_message('ordenPrevia','No existe una orden de servicio previa con este No. de Orden.');

        //Si pasa las validaciones enviamos la información al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";

            if ($this->session->userdata('rolIniciado')) {
                // $campos = $this->session->userdata('dataUser');
                $datos["pOrigen"] = $this->session->userdata('rolIniciado');
            }

            $this->index($datos["idPivote"],$datos);
        }
    }

    //Validamos si existe un una orden de servicio previa
    public function ordenPrevia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idPivote");

        //Verificamos si es un formulario sin cita
        if ($idOrden == "0") {
            $respuesta = TRUE;
        
        //De caso contrario validamos que exista una orden previa para guardar
        }else{
            //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
            $query = $this->mConsultas->get_result("id_cita",$idOrden,"ordenservicio");
            foreach ($query as $row){
                $validar = $row->id;
            }

            //Interpretamos la respuesta de la consulta
            if (isset($validar)) {
                //Si existe el dato, entonces ya existe la orden se servicio
                $respuesta = TRUE;
            }else {
                //Si no existe el dato, entonces no existe la orden de servicio
                $respuesta = FALSE;
            }
        }
        
        return $respuesta;
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idPivote");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idPivote",$idOrden,"voz_cliente");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Validamos si existe un registro con ese numero de orden
    public function validarFirma()
    {
        $respuesta = FALSE;
        $id_cita = $this->input->post("idPivote");
        $firma = $this->input->post("rutaFirmaAcepta");

        $query_cita = $this->mConsultas->get_result("id_cita",$id_cita,"ordenservicio");
        foreach ($query_cita as $row2){
            $autorizacion = $row2->autorizacion_ausencia;
            $firma = $row2->firma_diagnostico;
        }

        //si en la orden se coloco un "autorizado por ausencia"
        if ($autorizacion == "1") {
            //La firma puede pasar sin ser obligatoria
            $respuesta = TRUE;

        //si en la orden NO se coloco como "autorizado por ausencia"
        }else {
            //La firma es obligatoria
            if ($firma != "") {
                $respuesta = FALSE;
            } else {
                $respuesta = TRUE;
            }
        }
        return $respuesta;
    }

    //Recuperamos la informacion del formulario
    public function getDataForm($datos = NULL)
    {
        $registro = date("Y-m-d H:i:s");
        $valoresAdicionales = "";
        $valoresAdicionales .= $this->input->post("presupuestoIni")."_";
        $valoresAdicionales .= $this->input->post("adicionales")."_";
        $valoresAdicionales .= $this->input->post("totalAutorizado");

        //Si es un nuevo registro
        if ($datos["tipoRegistro"] == "1") {
            $dataForm = array(
                "idRegistro" => NULL,
                "idPivote" => $this->input->post("idPivote"),
            );
        }

        $dataForm["definicionFalla"] = $this->input->post("falla");
        $dataForm["sentidosFalla"] = $this->validateCheck($this->input->post("sentidos"),"S/R");

        //Si es un nuevo registro
        if ($datos["tipoRegistro"] == "1") {
            $dataForm["imgFallas"] = $this->base64ToImage($this->input->post("fallasImg"),"assets/imgs/diagramas","png");
        }

        $dataForm["presentaFalla"] = $this->validateCheck($this->input->post("fallasCheck"),"S/R");
        $dataForm["percibeFalla"] = $this->validateCheck($this->input->post("fallaPercibe"),"S/R");
        $dataForm["temperatura"] = $this->input->post("tempAmbiente");
        $dataForm["humedad"] = $this->input->post("humedad");
        $dataForm["viento"] = $this->input->post("viento");
        $dataForm["velocidad"] = $this->input->post("frenoMedidor");
        $dataForm["cambioTransmision"] = $this->input->post("cambio");
        $dataForm["transmision"] = $this->validateRadio($this->input->post("transmision"),"S/R");
        $dataForm["cambioOperativa"] = $this->input->post("cambio_A");
        $dataForm["rmp"] = $this->input->post("rpm");
        $dataForm["carga"] = $this->input->post("carga");
        $dataForm["remolque"] = $this->validateRadio($this->input->post("remolque"),"0");
        $dataForm["pasajeros"] = $this->input->post("pasajeros");
        $dataForm["cajuela"] = $this->input->post("cajuela_2");
        $dataForm["estructura"] = $this->input->post("estructura");
        $dataForm["camino"] = $this->input->post("camino");
        $dataForm["pendiente"] = $this->input->post("pendiente");
        $dataForm["superficie"] = $this->input->post("superficie");
        $dataForm["sistema"] = $this->input->post("sistema");
        $dataForm["componente"] = $this->input->post("componente");
        $dataForm["causaRaiz"] = $this->input->post("causa");
        $dataForm["trabajoRealizado"] = $this->validateCheck($this->input->post("valores"),"S/R");
        $dataForm["subtoal"] = $this->input->post("subTotalGralValue");
        $dataForm["iva"] = $this->input->post("ivaGralValue");
        $dataForm["total"] = $this->input->post("totalGralValue");
        $dataForm["presupuestos"] = $valoresAdicionales;
        $dataForm["notificaciones"] = $this->validateCheck($this->input->post("avisosValues"),"S/R");

        //Si es un nuevo registro
        if ($datos["tipoRegistro"] == "1") {
            //Aqui se guarda la pista de audio
            // $cadena = $this->descomprimir($this->input->post("blockAudio"));
            $cadena = $this->descomprimir();
            $dataForm["evidencia"] = $this->base64ToImage($cadena,"assets/audios","ogg");
            $dataForm["nombreAsesor"] = $this->input->post("nombreAsesor");
            $dataForm["firmaAsesor"] = $this->base64ToImage($this->input->post("rutaFirmaAsesor"),"assets/imgs/firmas","png");
            $dataForm["nombreCliente"] = $this->input->post("consumidor1Nombre");
            $dataForm["firmaCliente"] = $this->base64ToImage($this->input->post("rutaFirmaAcepta"),"assets/imgs/firmas","png");
            $dataForm["fecha_registro"] = $registro;
        }

        //Guardamosel formulario
        if ($datos["tipoRegistro"] == "1") {
            //Identificamos si el registro tiene orden o es temporal
            if ($dataForm["idPivote"] != "0") {
                //Si el pivote es diferente a 0, entonces es una formulario con cita/orden
                $vozCliente = $this->mConsultas->save_register('voz_cliente',$dataForm);
                if ($vozCliente != 0) {
                    //Verificamos si ya se firmoaron todos los campos
                    $this->chekingFirm($datos["idPivote"]);

                    //Actualizamos  los campos de la orden y guardamos el movimiento
                    if ($dataForm["firmaCliente"] != "") {
                        $contededor["firma_vc"] = "1";
                        $actualiza = $this->mConsultas->update_table_row('ordenservicio',$contededor,'id_cita',$datos["idPivote"]);

                        $this->loadHistory($datos["idPivote"],"Registro completo","Voz Cliente");
                    } else {
                        $this->loadHistory($datos["idPivote"],"Registro sin firma del cliente (autorizacion por ausencia)","Voz Cliente");
                        //Enviamos el correo al cliente
                        //$this->envio_correo($datos["idPivote"]);
                    }

                    //Actualizamos la información para el historial de ordenes
                    $this->actualiza_documentacion($dataForm["idPivote"],$dataForm["evidencia"]);
                    $datos["mensaje"] = "1";

                    //Verificamos quien creo el formulario
                    if ($this->session->userdata('rolIniciado')) {
                        // $campos = $this->session->userdata('dataUser');

                        //Si quien inicio sesion fue un jefe de taller
                        if ($this->session->userdata('rolIniciado') == "JDT") {
                            redirect(base_url().'Panel_JefeTaller/'.$datos["mensaje"]);
                        }else {
                            redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
                        }
                    }else{
                        //Asumimos que sue un asesor quien inicio sesion
                        redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
                    }
                }else {
                    $datos["mensaje"] = "2";
                    $this->index($datos["idPivote"],$datos);
                }
            //De lo contrario, y el pivote es igual a 0 , entonces es un formulario sin cita
            }else {
                //Cambiamos el pivote por un folio autogenerado
                $dataForm["idPivote"] = $this->generateFolio();
                $vozCliente = $this->mConsultas->save_register('voz_cliente_temp',$dataForm);
                if ($vozCliente != 0) {
                    
                    //Guardamos el movimiento
                    $this->loadHistory($datos["idPivote"],"Registro temporal","Voz Cliente Temporal");
                    $datos["mensaje"] = "1";
                    //Verificamos quien creo el formulario
                    if ($this->session->userdata('rolIniciado')) {
                        //Si quien inicio sesion fue un jefe de taller
                        if ($this->session->userdata('rolIniciado') == "JDT") {
                            redirect(base_url().'Panel_JefeTaller/'.$datos["mensaje"]);
                        }else {
                            redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
                        }
                    }else{
                        //Asumimos que sue un asesor quien inicio sesion
                        redirect(base_url().'Panel_Asesor/'.$datos["mensaje"]);
                    }
                }else {
                    $datos["mensaje"] = "2";
                    $this->index($datos["idPivote"],$datos);
                }
            }
        //Actualizamos el formulario (no aplica)
        } else {
            $actualizar = $this->mConsultas->update_table_row('voz_cliente',$dataForm,'idRegistro',$datos["idServicio"]);
            if ($actualizar) {
                //Guardamos el movimiento
                $this->loadHistory($datos["idPivote"],"Actualización","Voz Cliente");

                //Verificamos si ya se firmoaron todos los campos
                $this->chekingFirm($datos["idPivote"]);

                $datos["mensaje"]="1";
                $this->setData($datos["idPivote"],$datos);
            }else {
                $datos["mensaje"]="2";
                $this->setData($datos["idPivote"],$datos);
            }

        }
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion($id_cita = '',$evidencia = "")
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "voz_cliente" => "1",
                "evidencia_voz_cliente" => $evidencia,
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    public function guardar_firma()
    {
        $datos["registro"] = date("Y-m-d H:i:s");
        $val_comment = 0;

        $dataForm_2["firmaCliente"] = $this->base64ToImage($_POST["rutaFirmaAcepta"],"assets/imgs/diagramas","png");
        //Verificamos si hay comentarios del cliente
        if ($_POST["comentario"] != "") {
            //Recuperamos los comentarios anteriores
            $revision = $this->mConsultas->get_result("idPivote",$_POST["id_cita"],"voz_cliente");
            foreach ($revision as $row) {
                $comentario = $row->comentario;
            }

            $val_comment = 1;

            $comentario .= "\t\n";
            $comentario .= "Comentario del cliente : ".$_POST["comentario"];

            $dataForm_2["comentario"] = $comentario;
        } 
        $dataForm_2["fecha_firmaCliente"] = $datos["registro"];

        $actualizar = $this->mConsultas->update_table_row('voz_cliente',$dataForm_2,'idPivote',$_POST["id_cita"]);
        if ($actualizar) {
            //Guardamos el movimiento
            if ($val_comment == 1) {
                $desc_txt = "Actualizacion firma del cliente (retraso por ausencia) ";
                $desc_txt .= "Comentario del cliente : ".$_POST["comentario"];
            } else {
                $desc_txt = "Actualizacion firma del cliente (retraso por ausencia) ";
            }
            
            $this->loadHistory($_POST["id_cita"],$desc_txt,"Voz Cliente");

            $contededor["firma_vc"] = "1";
            $actualiza = $this->mConsultas->update_table_row('ordenservicio',$contededor,'id_cita',$_POST["id_cita"]);

            $respuesta = "OK=".$this->encrypt($_POST["id_cita"]);
        } else {
            $respuesta = "La ocurrio un problema al intentar actualizar el formulario=".$this->encrypt($_POST["id_cita"]);
        }
        
        echo $respuesta;
    }

    //Funcion para generar el folio de la venta
    public function generateFolio()
    {
        //Recuperamos el indice para el folio
        $fecha = date("Ymd");
        $registro = $this->mConsultas->last_id_tableVC();
        $row = $registro->row();
        if (isset($row)){
          $indexFolio = $registro->row(0)->idRegistro;
        }else {
          $indexFolio = 0;
        }

        $nvoFolio = $indexFolio + 1;
        if ($nvoFolio < 10) {
            $folio = "00".$nvoFolio."-".$fecha;
        } elseif(($nvoFolio > 9) && ($nvoFolio < 100)) {
            $folio = "0".$nvoFolio."-".$fecha;
        }else {
            $folio = $nvoFolio."-".$fecha;
        }
        return $folio;
    }

    //Revisamos si ya se firmaron todos los campos
    public function chekingFirm($idOrden = 0)
    {
        $registro = date("Y-m-d H:i:s");
        //Recuperamos los campos necesarios
        $query = $this->mConsultas->get_result("idPivote",$idOrden,"voz_cliente");
        foreach ($query as $row) {
            $firmaAsesor = $row->firmaAsesor;
            $firmaCliente = $row->firmaCliente;
        }

        if (isset($firmaAsesor)) {
            //Verificamos si existe el registro en la tabla
            $query2 = $this->mConsultas->get_result("idOrden",$idOrden,"estados_pdf");
            foreach ($query2 as $row){
                $multipunto = $row->multipuntoPDF;
                $idPdf = $row->idRegistro;
            }

            if (isset($multipunto)) {
                //Comprobamos los estados validos (actualizar)
                if (($firmaAsesor != "")&&($firmaCliente != "")) {
                    $data = array(
                        "vozClientePDF" => 1,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                  $data = array(
                      "vozClientePDF" => 0,
                      "fecha_actualizacion" => $registro,
                  );
                }
                $this->mConsultas->update_table_row('estados_pdf',$data,'idRegistro',$idPdf);
            //Si no existe el registro se cres
            }else {
                //Comprobamos los estados validos (registro)
                if (($firmaAsesor != "")&&($firmaCliente != "")) {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 0,
                        "vozClientePDF" => 1,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }else {
                    $data = array(
                        "idRegistro" => NULL,
                        "idOrden" => $idOrden,
                        "multipuntoPDF" => 0,
                        "ordenServicioPDF_1" => 0,
                        "ordenServicioPDF_2" => 0,
                        "vozClientePDF" => 0,
                        "fechaRegistro" => $registro,
                        "fecha_actualizacion" => $registro,
                    );
                }
                $this->mConsultas->save_register('estados_pdf',$data);
            }
        }

        return FALSE;
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Unimos las partes del audio en texto
    public function descomprimir()
    {
        $respuesta = "";

        for ($i=1; $i <9 ; $i++) {
            if ($this->input->post("blockAudio_".$i) != "") {
                $respuesta .= trim($this->input->post("blockAudio_".$i));
            }
        }
        return $respuesta;
    }

    //Entrada para firma individual del cliente
    public function setDataClienteFirma($idOrden = 0)
    {
        $datos["destinoVista"] = "FIRMA";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setDataPDF($id_cita,$datos);
    }

    //Comprobamos que al final se genera la vista de revision
    public function setDataRevision($idOrden = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setDataPDF($id_cita,$datos);
    }

    //Comprobamos que al final se genera la vista de revision
    public function setDataPDF_R($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setDataPDF($id_cita,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setDataPDF($id_cita = 0,$datos = NULL)
    {
        if ($this->session->userdata('rolIniciado')) {
            // $campos = $this->session->userdata('dataUser');
            $datos["pOrigen"] = $this->session->userdata('rolIniciado');
        }

        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "PDF";
        }

        //Recuperamos el folio de intelisis
        $datos["orden_intelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        //Cargamos la información del formulario
        $query = $this->mConsultas->get_result("idPivote",$id_cita,"voz_cliente");
        foreach ($query as $row) {
            $datos["definicionFalla"] = explode(" ",$row->definicionFalla);
            $datos["definicionFalla_txt"] = $row->definicionFalla;
            $datos["imgFallas"] = (file_exists($row->imgFallas)) ? $row->imgFallas : ""; //$row->imgFallas;

            //Recuperamos los valores que debemos tratar para imprimir
            $datos["sentidosFalla"] = explode("|", $row->sentidosFalla);
            $datos["presentaFalla"] = explode("|", $row->presentaFalla);
            $datos["percibeFalla"] = explode("|",$row->percibeFalla);
            $datos["presupuestos"] = explode("_",$row->presupuestos);

            //Valores para PDF
            if ($datos["destinoVista"] == "PDF") {
                $temperatura = (int)$row->temperatura;
                if (($temperatura > 0)&&($row->temperatura != "")) {
                    if ($temperatura == 0) {
                        $datos["logTempBarra"] = 'assets/imgs/reducidos/condiciones/metrix2_1.png';
                    }elseif (($temperatura > 0)&&($temperatura < 11)) {
                        $datos["logTempBarra"] = 'assets/imgs/reducidos/condiciones/metrix2_2.png';
                    }elseif (($temperatura > 10)&&($temperatura < 21)) {
                        $datos["logTempBarra"] = 'assets/imgs/reducidos/condiciones/metrix2_3.png';
                    }elseif (($temperatura > 20)&&($temperatura < 31)) {
                        $datos["logTempBarra"] = 'assets/imgs/reducidos/condiciones/metrix2_4.png';
                    }elseif (($temperatura > 30)&&($temperatura < 41)) {
                        $datos["logTempBarra"] = 'assets/imgs/reducidos/condiciones/metrix2_5.png';
                    }elseif (($temperatura > 40)&&($temperatura < 51)) {
                        $datos["logTempBarra"] = 'assets/imgs/reducidos/condiciones/metrix2_6.png';
                    }elseif (($temperatura > 50)&&($temperatura < 61)) {
                        $datos["logTempBarra"] = 'assets/imgs/reducidos/condiciones/metrix2_7.png';
                    }else{
                        $datos["logTempBarra"] = 'assets/imgs/reducidos/condiciones/metrix2_8.png';
                    }                      
                }else {
                    $datos["logTempBarra"] = 'assets/imgs/metrix2.png'; 
                }

                $humedad = $row->humedad;
                if ($humedad == "Seco") {
                    $datos["logHumBarra"] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_1.png';
                }elseif ($humedad == "Húmedo") {
                    $datos["logHumBarra"] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_2.png';
                }elseif ($humedad == "Mojado") {
                    $datos["logHumBarra"] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_3.png';
                }elseif ($humedad == "Lluvia") {
                    $datos["logHumBarra"] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_4.png';
                }elseif ($humedad == "Hielo") {
                    $datos["logHumBarra"] = 'assets/imgs/reducidos/condiciones/metrix_humedad2_5.png';
                }else{
                    $datos["logHumBarra"] = 'assets/imgs/metrix_humedad2.png'; 
                }

                $viento = $row->viento;
                if ($viento == "Ligero") {
                    $datos["logVientoBarra"] = 'assets/imgs/reducidos/condiciones/metrix_viento2_1.png';
                }elseif ($viento == "Medio") {
                    $datos["logVientoBarra"] = 'assets/imgs/reducidos/condiciones/metrix_viento2_2.png';
                }elseif ($viento == "Fuerte") {
                    $datos["logVientoBarra"] = 'assets/imgs/reducidos/condiciones/metrix_viento2_3.png';
                }else{
                    $datos["logVientoBarra"] = 'assets/imgs/metrix_viento2.png';
                }

                $velocidad = (int)$row->velocidad;
                if (($velocidad > 0)&&($row->velocidad != "")) {
                    if ($velocidad < 21) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_1.png';
                    }elseif (($velocidad > 20)&&($velocidad < 41)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_2.png';
                    }elseif (($velocidad > 40)&&($velocidad < 61)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_3.png';
                    }elseif (($velocidad > 60)&&($velocidad < 81)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_4.png';
                    }elseif (($velocidad > 80)&&($velocidad < 101)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_5.png';
                    }elseif (($velocidad > 100)&&($velocidad < 121)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_6.png';
                    }elseif (($velocidad > 120)&&($velocidad < 141)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_7.png';
                    }elseif (($velocidad > 140)&&($velocidad < 161)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_8.png';
                    }elseif (($velocidad > 160)&&($velocidad < 181)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_9.png';
                    }elseif (($velocidad > 180)&&($velocidad < 201)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_10.png';
                    }elseif (($velocidad > 200)&&($velocidad < 241)) {
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_11.png';
                    }else{
                        $datos["logVelBarra"] = 'assets/imgs/reducidos/condiciones/velocidad3_13.png';
                    }                      
                }else {
                    $datos["logVelBarra"] = 'assets/imgs/velocidad3.png'; 
                }

                if ($row->transmision == "2") {
                    $datos["transmision_2"] = 'assets/imgs/reducidos/condiciones/cambio3_a_1.png';
                }elseif ($row->transmision == "4") {
                    $datos["transmision_2"] = 'assets/imgs/reducidos/condiciones/cambio3_a_2.png';
                }else{
                    $datos["transmision_2"] = 'assets/imgs/reducidos/condiciones/cambio3_a.png';
                }

                $camino_1 = $row->cambioTransmision;
                if ($camino_1 == "R") {
                  $datos["logCam1Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_b_1.png';
                }elseif ($camino_1 == "1") {
                  $datos["logCam1Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_b_2.png';
                }elseif ($camino_1 == "2") {
                  $datos["logCam1Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_b_3.png';
                }elseif ($camino_1 == "3") {
                  $datos["logCam1Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_b_4.png';
                }elseif ($camino_1 == "4") {
                  $datos["logCam1Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_b_5.png';
                }elseif ($camino_1 == "5") {
                  $datos["logCam1Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_b_6.png';
                }elseif ($camino_1 == "6") {
                  $datos["logCam1Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_b_7.png';
                }else{
                    $datos["logCam1Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_b.png';
                }

                $operativa = $row->cambioOperativa;
                if ($operativa == "P") {
                  $datos["logCam2Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_c_1.png';
                }elseif ($operativa == "R") {
                  $datos["logCam2Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_c_2.png';
                }elseif ($operativa == "N") {
                  $datos["logCam2Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_c_3.png';
                }elseif ($operativa == "D") {
                  $datos["logCam2Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_c_4.png';
                }elseif ($operativa == "L") {
                  $datos["logCam2Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_c_5.png';
                }else{
                    $datos["logCam2Barra"] = 'assets/imgs/reducidos/condiciones/cambio3_c.png';
                }

                $rmp = (int)$row->rmp;
                if ($rmp == "1") {
                  $datos["logRMPBarra"] = 'assets/imgs/reducidos/condiciones/rpm3_1.png';
                }elseif ($rmp == "2") {
                  $datos["logRMPBarra"] = 'assets/imgs/reducidos/condiciones/rpm3_2.png';
                }elseif ($rmp == "3") {
                  $datos["logRMPBarra"] = 'assets/imgs/reducidos/condiciones/rpm3_3.png';
                }elseif ($rmp == "4") {
                  $datos["logRMPBarra"] = 'assets/imgs/reducidos/condiciones/rpm3_4.png';
                }elseif ($rmp == "5") {
                  $datos["logRMPBarra"] = 'assets/imgs/reducidos/condiciones/rpm3_5.png';
                }elseif ($rmp == "6") {
                  $datos["logRMPBarra"] = 'assets/imgs/reducidos/condiciones/rpm3_6.png';
                }elseif ($rmp == "7") {
                  $datos["logRMPBarra"] = 'assets/imgs/reducidos/condiciones/rpm3_7.png';
                }elseif ($rmp == "8") {
                  $datos["logRMPBarra"] = 'assets/imgs/reducidos/condiciones/rpm3_8.png';
                }else{
                    $datos["logRMPBarra"] = 'assets/imgs/rpm3.png';
                }

                $carga = (int)$row->carga;
                if (($carga > 0)&&($row->carga != "")) {
                    if ($carga < 26) {
                        $datos["logCargaBarra"] = 'assets/imgs/reducidos/condiciones/carga3_1.png';
                    }elseif (($carga > 25)&&($carga < 51)) {
                        $datos["logCargaBarra"] = 'assets/imgs/reducidos/condiciones/carga3_2.png';
                    }elseif (($carga > 50)&&($carga < 76)) {
                        $datos["logCargaBarra"] = 'assets/imgs/reducidos/condiciones/carga3_3.png';
                    }else{
                        $datos["logCargaBarra"] = 'assets/imgs/reducidos/condiciones/carga3_4.png';
                    }
                }else {
                    $datos["logCargaBarra"] = 'assets/imgs/carga3.png'; 
                }

                $datos["remolque"] = $row->remolque;

                $pasajeros = (int)$row->pasajeros;
                if ($pasajeros == "1") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_1.png';
                }elseif ($pasajeros == "2") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_2.png';
                }elseif ($pasajeros == "3") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_3.png';
                }elseif ($pasajeros == "4") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_4.png';
                }elseif ($pasajeros == "5") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_5.png';
                }elseif ($pasajeros == "6") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_6.png';
                }elseif ($pasajeros == "7") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_7.png';
                }elseif ($pasajeros == "8") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_8.png';
                }elseif ($pasajeros == "9") {
                  $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a_9.png';
                }else{
                    $datos["logPasajeBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_a.png';
                }

                $cajuela_temp = (int)$row->cajuela;
                if (($cajuela_temp > 0)&&($row->cajuela != "")) {
                    if ($cajuela_temp < 26) {
                        $datos["logCajuelaBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_b_1.png';
                    }elseif (($cajuela_temp > 25)&&($cajuela_temp < 51)) {
                        $datos["logCajuelaBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_b_2.png';
                    }elseif (($cajuela_temp > 50)&&($cajuela_temp < 76)) {
                        $datos["logCajuelaBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_b_3.png';
                    }else{
                        $datos["logCajuelaBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_b_4.png';
                    }
                }else {
                    $datos["logCajuelaBarra"] = 'assets/imgs/reducidos/condiciones/pasajeros3_b.png'; 
                }

                $estructura = $row->estructura;
                if ($estructura == "Plano") {
                    $datos["logEstrBarra"] = 'assets/imgs/reducidos/condiciones/estructura3_1.png';
                }elseif ($estructura == "Vado") {
                    $datos["logEstrBarra"] = 'assets/imgs/reducidos/condiciones/estructura3_2.png';
                }elseif ($estructura == "Tope") {
                    $datos["logEstrBarra"] = 'assets/imgs/reducidos/condiciones/estructura3_3.png';
                }elseif ($estructura == "Baches") {
                    $datos["logEstrBarra"] = 'assets/imgs/reducidos/condiciones/estructura3_4.png';
                }elseif ($estructura == "Vibradores") {
                    $datos["logEstrBarra"] = 'assets/imgs/reducidos/condiciones/estructura3_5.png';
                }else{
                    $datos["logEstrBarra"] = 'assets/imgs/estructura3.png';
                }

                $camino = $row->camino;
                if ($camino == "Recto") {
                    $datos["logCamBarra"] = 'assets/imgs/reducidos/condiciones/camino3_1.png';
                }elseif ($camino == "Curva ligera") {
                    $datos["logCamBarra"] = 'assets/imgs/reducidos/condiciones/camino3_2.png';
                }elseif ($camino == "Curva cerrada") {
                    $datos["logCamBarra"] = 'assets/imgs/reducidos/condiciones/camino3_3.png';
                }elseif ($camino == "Sinuoso") {
                    $datos["logCamBarra"] = 'assets/imgs/reducidos/condiciones/camino3_4.png';
                }else{
                    $datos["logCamBarra"] = 'assets/imgs/camino3.png';
                }

                $pendiente = $row->pendiente;
                if ($pendiente == "Recto") {
                    $datos["logPenBarra"] = 'assets/imgs/reducidos/condiciones/pendiente3_1.png';
                }elseif ($pendiente == "Pendiente ligera 10°") {
                    $datos["logPenBarra"] = 'assets/imgs/reducidos/condiciones/pendiente3_2.png';
                }elseif ($pendiente == "Pendiente media") {
                    $datos["logPenBarra"] = 'assets/imgs/reducidos/condiciones/pendiente3_3.png';
                }elseif ($pendiente == "Montaña") {
                    $datos["logPenBarra"] = 'assets/imgs/reducidos/condiciones/pendiente3_4.png';
                }else{
                    $datos["logPenBarra"] = 'assets/imgs/pendiente3.png';
                }

                $superficie = $row->superficie;
                if ($superficie == "Pavimento") {
                    $datos["logSupeBarra"] = 'assets/imgs/reducidos/condiciones/superficie3_1.png';
                }elseif ($superficie == "Terracería") {
                    $datos["logSupeBarra"] = 'assets/imgs/reducidos/condiciones/superficie3_2.png';
                }elseif ($superficie == "Empedrado") {
                    $datos["logSupeBarra"] = 'assets/imgs/reducidos/condiciones/superficie3_3.png';
                }elseif ($superficie == "Adoquín") {
                    $datos["logSupeBarra"] = 'assets/imgs/reducidos/condiciones/superficie3_4.png';
                }elseif ($superficie == "Fango") {
                    $datos["logSupeBarra"] = 'assets/imgs/reducidos/condiciones/superficie3_5.png';
                }else{
                    $datos["logSupeBarra"] = 'assets/imgs/superficie3.png';
                }

            //Valores para vista rapida
            }else{
                $datos["logTempBarra"] = (int)$row->temperatura;

                $datos["humedad"] = $row->humedad;
                if ($datos["humedad"] == "Seco") {
                    $datos["logHumBarra"] = 20;
                }elseif ($datos["humedad"] == "Húmedo") {
                    $datos["logHumBarra"] = 40;
                }elseif ($datos["humedad"] == "Mojado") {
                    $datos["logHumBarra"] = 60;
                }elseif ($datos["humedad"] == "Lluvia") {
                    $datos["logHumBarra"] = 80;
                }elseif ($datos["humedad"] == "Hielo") {
                    $datos["logHumBarra"] = 100;
                }else {
                    $datos["logHumBarra"] = 0;
                }

                $datos["viento"] = $row->viento;
                if ($datos["viento"] == "Ligero") {
                    $datos["logVientoBarra"] = 40;
                }elseif ($datos["viento"] == "Medio") {
                    $datos["logVientoBarra"] = 80;
                }elseif ($datos["viento"] == "Fuerte") {
                    $datos["logVientoBarra"] = 120;
                }else {
                    $datos["logVientoBarra"] = 0;
                }

                $datos["logVelBarra"] = (int)$row->velocidad;
                $datos["transmision"] = (int)$row->transmision;

                $datos["cambio_A"] = $row->cambioTransmision;
                if ($datos["cambio_A"] == "R") {
                    $datos["logCam1Barra"] = 10;
                }elseif ($datos["cambio_A"] == "1") {
                    $datos["logCam1Barra"] = 20;
                }elseif ($datos["cambio_A"] == "2") {
                    $datos["logCam1Barra"] = 30;
                }elseif ($datos["cambio_A"] == "3") {
                    $datos["logCam1Barra"] = 40;
                }elseif ($datos["cambio_A"] == "4") {
                    $datos["logCam1Barra"] = 50;
                }elseif ($datos["cambio_A"] == "5") {
                    $datos["logCam1Barra"] = 60;
                }elseif ($datos["cambio_A"] == "6") {
                    $datos["logCam1Barra"] = 70;
                }else {
                    $datos["logCam1Barra"] = 0;
                }

                $datos["operativa"] = $row->cambioOperativa;
                if ($datos["operativa"] == "P") {
                    $datos["logCam2Barra"] = 20;
                }elseif ($datos["operativa"] == "R") {
                    $datos["logCam2Barra"] = 40;
                }elseif ($datos["operativa"] == "N") {
                    $datos["logCam2Barra"] = 60;
                }elseif ($datos["operativa"] == "D") {
                    $datos["logCam2Barra"] = 80;
                }elseif ($datos["operativa"] == "L") {
                    $datos["logCam2Barra"] = 100;
                }else {
                    $datos["logCam2Barra"] = 0;
                }

                $datos["rmp"] = (int)$row->rmp;
                $temp_3 = ($datos["rmp"]-1)*10;
                $datos["logRMPBarra"] = (string)round($temp_3);

                $datos["logCargaBarra"] = $row->carga;;
                $datos["remolque"] = $row->remolque;

                $datos["pasajeros"] = (int)$row->pasajeros;
                $temp_5 = ($datos["pasajeros"]-1)*10;
                $datos["logPasajeBarra"] = (string)round($temp_5);
                $datos["logCajuelaBarra"] = (string)$row->cajuela;

                $datos["estructura"] = $row->estructura;
                if ($datos["estructura"] == "Plano") {
                    $datos["logEstrBarra"] = 20;
                }elseif ($datos["estructura"] == "Vado") {
                    $datos["logEstrBarra"] = 40;
                }elseif ($datos["estructura"] == "Tope") {
                    $datos["logEstrBarra"] = 60;
                }elseif ($datos["estructura"] == "Baches") {
                    $datos["logEstrBarra"] = 80;
                }elseif ($datos["estructura"] == "Vibradores") {
                    $datos["logEstrBarra"] = 100;
                }else {
                    $datos["logEstrBarra"] = 0;
                }

                $datos["camino"] = $row->camino;
                if ($datos["camino"] == "Recto") {
                    $datos["logCamBarra"] = 20;
                }elseif ($datos["camino"] == "Curva ligera") {
                    $datos["logCamBarra"] = 40;
                }elseif ($datos["camino"] == "Curva cerrada") {
                    $datos["logCamBarra"] = 60;
                }elseif ($datos["camino"] == "Sinuoso") {
                    $datos["logCamBarra"] = 80;
                }else {
                    $datos["logCamBarra"] = 0;
                }

                $datos["pendiente"] = $row->pendiente;
                if ($datos["pendiente"] == "Recto") {
                    $datos["logPenBarra"] = 20;
                }elseif ($datos["pendiente"] == "Pendiente ligera 10°") {
                    $datos["logPenBarra"] = 40;
                }elseif ($datos["pendiente"] == "Pendiente media") {
                    $datos["logPenBarra"] = 60;
                }elseif ($datos["pendiente"] == "Montaña") {
                    $datos["logPenBarra"] = 80;
                }else {
                    $datos["logPenBarra"] = 0;
                }

                $datos["superficie"] = $row->superficie;
                if ($datos["superficie"] == "Pavimento") {
                    $datos["logSupeBarra"] = 20;
                }elseif ($datos["superficie"] == "Terracería") {
                    $datos["logSupeBarra"] = 40;
                }elseif ($datos["superficie"] == "Empedrado") {
                    $datos["logSupeBarra"] = 80;
                }elseif ($datos["superficie"] == "Adoquín") {
                    $datos["logSupeBarra"] = 100;
                }elseif ($datos["superficie"] == "Fango") {
                    $datos["logSupeBarra"] = 120;
                }else {
                    $datos["logSupeBarra"] = 0;
                }
            }

            $datos["sistema"] = $row->sistema;
            $datos["componente"] = $row->componente;
            $datos["causaRaiz"] = $row->causaRaiz;

            $datos["subtoal"] = $row->subtoal;
            $datos["iva"] = $row->iva;
            $datos["total"] = $row->total;

            $datos["nombreAsesor"] = trim($row->nombreAsesor);
            $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : "";
            $datos["nombreCliente"] = trim($row->nombreCliente);
            $datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : "";
            $datos["transmision"] = $row->transmision;

            //Tratamos los datos que requieren conversión
            $trabajoRealizado = explode("|",$row->trabajoRealizado);
            $datos["trabajos"] = $this->filtros($trabajoRealizado);
            $datos["comentario"] = $row->comentario;

            $notificaciones = explode("|",$row->notificaciones);
            $datos["avisos"] = $this->filtros($notificaciones);

            $datos["evidencia"] = $row->evidencia;
        }

        //Generamos el PDF con los datos o la vista rapida
        if ($datos["destinoVista"] == "PDF") {
            $this->generatePDF($datos);
        }elseif ($datos["destinoVista"] == "FIRMA") {
            //Cargamos datos adicionales
            $precarga = $this->mConsultas->precargaConsulta($id_cita);
            foreach ($precarga as $row){
                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $datos["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $datos["modelo"] = $row->vehiculo_anio;
                $datos["placas"] = $row->vehiculo_placas;
                $datos["categoria"] = $row->vehiculo_modelo;
                $datos["tecnico"] = $row->tecnico;
                $datos["asesor"] = $row->asesor;
            }

            $this->loadAllView($datos,'vozCliente/plantillaCliente');
        } else{
            $this->loadAllView($datos,'vozCliente/plantillaRevision');
        }
    }

    //Recuperamos los valores de las tablas
    public function filtros($filas = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($filas) {
            if (count($filas)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($filas) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$filas[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "",$ext = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_VC";
        for($i=0; $i<=6; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($direcctorio)){
            mkdir($direcctorio, 0647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if (substr($imgBase64,0,6) != "assets") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base = $direcctorio.'/'.$urlNombre.".".$ext;
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }
        }

        return $base;
    }

    //Validar multiple check
    public function validateCheck($campo = NULL,$elseValue = "")
    {
        $respuesta = "";
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            for ($i=0; $i <= count($campo) ; $i++) {
                if (isset($campo[$i])) {
                    $respuesta .= $campo[$i]."|";
                }
            }
        }
        return $respuesta;
    }

    //Validar Radio
    public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Consultamos pdf de la voz por medio de ajax
    public function existenciaVerifica()
    {
        $respuesta = "FALSE";
        $idOrden = $_POST['serie'];
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idPivote",$idOrden,"voz_cliente");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "OK=".$this->encrypt($idOrden);
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = "NO EXISTE=".$this->encrypt($idOrden);
        }

        echo  $respuesta;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("vozCliente/plantillaPdf", $datos);
    }

    public function envio_correo($idOrden = 0)
    {
        //Definimos la fecha de envio
        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","viernes","Sábado");
        $datosEmail["dia"] = $dias[date("w")];
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $datosEmail["mes"] = $meses[date("n")-1];

        //Recuperamos la informacion a enviar
        $query = $this->mConsultas->precargaConsulta($idOrden);
        foreach ($query as $row){
            $datosEmail["correo1"] = $row->datos_email;
            $datosEmail["correo2"] = $row->correo_compania;
            $datosEmail["nombre"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            $datosEmail["companiaNombre"] = $row->nombre_compania;
            $datosEmail["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
        }

        //Url del archivo
        $datosEmail["url"] = base_url()."VozCliente_Firmas/".$this->encrypt($idOrden);
        $datosEmail["formularioEnvio"] = "Voz Cliente";
        //Cargamos la informacion en la plnatilla
        $htmlContent = $this->load->view("plantillaEmailAusencia", $datosEmail, TRUE);

        $configGmail = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => EMAIL_SERVER,
            'smtp_pass' => EMAIL_PASS,
            'mailtype' => 'html',
            'smtp_crypto'=> 'tls',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );

        //Establecemos esta configuración
        $this->email->initialize($configGmail);
        //Ponemos la dirección de correo que enviará el email y un nombre
        $this->email->from(EMAIL_SERVER, SUCURSAL);
        //Ponemos la direccion de aquien va dirigido
        //$this->email->to($datosEmail["correo1"],$datosEmail["nombre"]);
        //$this->email->to($datosEmail["correo2"],$datosEmail["nombre"]);
        $this->email->to('desarrollo@sohex.mx','Tester');

        //Multiples destinos
        // $this->email->to($correo1, $correo2, $correo3);
        //Con copia a:
        // $this->email->cc($correo);
        //Con copia oculta a:
        // $this->email->bcc($correo);
        //$this->email->bcc('info@planificadorempresarial.mx','Angel');
        $this->email->bcc('info@sohex.mx','Angel');
        //Colocamos el asunto del correo
        $this->email->subject('Diagnóstico Autorizado por Ausencia ()');
        //Cargamos la plantilla del correo
        $this->email->message($htmlContent);
        //Enviar archivo adjunto
        // $this->email->attach('files/attachment.pdf');
        //Enviamos el correo
        if($this->email->send()){
            $envio = 1;
        }else{
            $envio = 2;
        }

        return $envio;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/vozCliente/audioPrueba.js',
                'assets/js/vozCliente/servicio.js',
                'assets/js/vozCliente/medidoresVC.js',
                'assets/js/firmasMoviles/firmas_VC.js',
                'assets/js/Multipunto/precarga_form.js',
                'assets/js/otrosModulos/hibrido/interaccionesMedidor.js',
                'assets/js/vozCliente/alternativa.js'
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Voz Cliente"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
