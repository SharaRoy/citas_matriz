<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class FormatoPrevio extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

  	public function index($idOrden = 0,$datos = NULL)
  	{
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$id_cita,"previa");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $this->setData($id_cita,$datos);
        }else {
            //Si no existe el dato, entonces no existe el registro
            $this->loadAllView($datos,'previa/previa_alta');
        }
  	}

    //Validamos los campos
    public function validateForm()
    {
        $datos["vista"] = $this->input->post("modoVistaFormulario");
        $datos["idOrden"] = $this->input->post("txtOrden");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idOrden"]);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        if ($datos["vista"] == "1") {
            $this->form_validation->set_rules('txtFolio', 'Folio requerido.', 'required');
            $this->form_validation->set_rules('txtOrden', 'No. Orden de servicio requerido.', 'required|callback_existencia');
            $this->form_validation->set_rules('rutaFirmaTecnico', 'Firma del técnico requerido.', 'required');
            $this->form_validation->set_rules('nombreTecnico', 'Nombre del técnico requerido.', 'required');
            $this->form_validation->set_rules('txtFecha', 'La Fecha es un campo requerido.', 'required');
        }

        $this->form_validation->set_rules('txtVin', 'VIN requerido.', 'required');
        $this->form_validation->set_rules('txtUnidad', 'Unidad requerido.', 'required');
        $this->form_validation->set_rules('txtCat', 'CAT requerido.', 'required');
        $this->form_validation->set_rules('txtColor', 'Color requerido.', 'required');
        // $this->form_validation->set_rules('txtCliente', 'Nombre del cliente requerido.', 'required');

        $this->form_validation->set_rules('txtDistribuidor', 'Distribuidor requerido.', 'required');
        $this->form_validation->set_rules('txtDireccion', 'Dirección requerido.', 'required');
        $this->form_validation->set_rules('txtCiudad', 'Municipio/Ciudad requerido.', 'required');
        $this->form_validation->set_rules('txtEstado', 'Estado requerido.', 'required');
        $this->form_validation->set_rules('txtCp', 'Código postal requerido.', 'required');


        $this->form_validation->set_message('required','%s');

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";
            $this->index($datos["idOrden"],$datos);
        }
    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("txtOrden");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("idOrden",$idOrden,"previa");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Recuperamos los campos
    public function getDataForm($datos = NULL)
    {
        if ($datos["vista"] == "1") {
            $contenido = array(
                'idRegistro' => NULL,
                'idOrden' => $this->input->post("txtOrden"),
                'folio' => $this->input->post("txtFolio"),
            );
        }

        $contenido["vin"] = $this->input->post("txtVin");
        $contenido["cat"] = $this->input->post("txtCat");
        $contenido["unidad"] = $this->input->post("txtUnidad");
        $contenido["color"] = $this->input->post("txtColor");

        $revision = "";
        $revision .= $this->validateRadioCheck($this->input->post("txtA1"),"0")."_";
        $revision .= $this->validateRadioCheck($this->input->post("txtA3"),"0")."_";
        $revision .= $this->validateRadioCheck($this->input->post("txtA4"),"0")."_";
        $revision .= $this->validateRadioCheck($this->input->post("txtA8"),"0")."_";

        //Lado Izquierdo
        //REVISIONES
        $contenido["revisiones"] = $revision;
        $contenido["voltaje"] = $this->input->post("txtInputA1");

        //VERIFIQUE Y PONER A ESPECIFICACIÓN
        $especificacion = "";
        $especificacion .= $this->validateRadioCheck($this->input->post("txtB1"),"0")."_";
        $especificacion .= $this->validateRadioCheck($this->input->post("txtB3"),"0")."_";
        $especificacion .= $this->validateRadioCheck($this->input->post("txtB4"),"0")."_";
        $contenido["especificaciones"] = $especificacion;

        //REVISAR OPERACIÓN
        $operaciones = "";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF2"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF3"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF6"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF7"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF12"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF15"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF16"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF20"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF21"),"0")."_";
        $operaciones .= $this->validateRadioCheck($this->input->post("txtF24"),"0")."_";
        $contenido["operaciones"] = $operaciones;

        //AJUSTE Y VERIFICACIONES
        $ajustes = "";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ1"),"0")."_";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ2"),"0")."_";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ4"),"0")."_";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ5"),"0")."_";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ6"),"0")."_";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ7"),"0")."_";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ8"),"0")."_";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ9"),"0")."_";
        $ajustes .= $this->validateRadioCheck($this->input->post("txtJ11"),"0")."_";
        $contenido["ajustes"] = $ajustes;

        //PRUEBA DE CAMINO
        $pruebas = "";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH1"),"0")."_";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH2"),"0")."_";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH3"),"0")."_";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH5"),"0")."_";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH6"),"0")."_";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH8"),"0")."_";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH10"),"0")."_";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH12"),"0")."_";
        $pruebas .= $this->validateRadioCheck($this->input->post("txtH14"),"0")."_";
        $contenido["pruebas"] = $pruebas;

        //Codigos y configuraciones
        $config = "";
        $config .= $this->validateRadioCheck($this->input->post("txtCod1"),"0")."_";
        $config .= $this->validateRadioCheck($this->input->post("txtCod2"),"0")."_";
        $config .= $this->validateRadioCheck($this->input->post("txtCod3"),"0")."_";
        $config .= $this->validateRadioCheck($this->input->post("txtCod4"),"0")."_";
        $config .= $this->validateRadioCheck($this->input->post("txtCod5"),"0")."_";
        $config .= $this->validateRadioCheck($this->input->post("txtCod6"),"0")."_";
        $config .= $this->validateRadioCheck($this->input->post("txtCod7"),"0")."_";
        $contenido["configuracion"] = $config;

        //Lado derecho
        $apariencia = "";
        $apariencia .= $this->validateRadioCheck($this->input->post("txtAP1"),"0")."_";
        $apariencia .= $this->validateRadioCheck($this->input->post("txtAP2"),"0")."_";
        $contenido["apariencia"] = $apariencia;

        //Formulario
        $contenido["distribuidor"] = $this->input->post("txtDistribuidor");
        $contenido["direccion"] = $this->input->post("txtDireccion");
        $contenido["municipio"] = $this->input->post("txtCiudad");
        $contenido["estados"] = $this->input->post("txtEstado");
        $contenido["cp"] = $this->input->post("txtCp");
        $contenido["numero"] = $this->input->post("txtNumero");
        $contenido["codigo"] = $this->input->post("txtRadio");
        $contenido["apertura"] = $this->input->post("txtPuerta");
        $contenido["llantas"] = $this->input->post("txtDot");
        $contenido["reserva"] = $this->input->post("txtReserva");

        //Tecnico Oasis
        $oasis = "";
        $oasis .= $this->validateRadioCheck($this->input->post("txtOasis1"),"0")."_";
        $oasis .= $this->validateRadioCheck($this->input->post("txtOasis2"),"0")."_";
        $oasis .= $this->validateRadioCheck($this->input->post("txtOasis3"),"0")."_";
        $contenido["oasis"] = $oasis;

        $registro = date("Y-m-d H:i");

        if ($datos["vista"] == "1") {
            $contenido["firmaTecnico"] = $this->base64ToImage($this->input->post("rutaFirmaTecnico"),"firmas");
            $contenido["nombreTecnico"] = $this->input->post("nombreTecnico");
            $contenido["nombrecliente"] = "";
            $contenido["fechaFirma"] = $this->input->post("txtFecha");
            $contenido["fechaRegistro"] = $registro;
        }

        $contenido["nombrecliente"] = $registro;

        //Para registro de alta
        if ($datos["vista"] == "1") {
            $registroDB = $this->mConsultas->save_register('previa',$contenido);
            if ($registroDB != 0) {
                $datos["mensaje"] = "1";
            }else {
                $datos["mensaje"] = "2";
            }

            $this->index($datos["idOrden"],$datos);
        //Para registro que se actualizan
        } else {
            $actualizar = $this->mConsultas->update_table_row('previa',$contenido,'idOrden',$datos["idOrden"]);
            if ($actualizar) {
                $datos["mensaje"]="1";
            }else {
                $datos["mensaje"]="2";
            }

            $this->index($datos["idOrden"],$datos);
        }
    }

    //Validar Radio
    public function validateRadioCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Listamos los registros
    public function listData($x = "",$datos = NULL)
    {
        if (!isset($datos["indicador"])) {
            $datos["indicador"] = "";
        }

        //Recuperamos todas las cotizaciones que se han generado en la multipunto
        $query = $this->mConsultas->get_table_list("previa","fechaRegistro");

        foreach ($query as $row){
            $datos["idOrden"][] = $row->idOrden;
            $datos["id_cita_url"][] = $this->encrypt($row->idOrden);
            $datos["folio"][] = $row->folio;
            $fechaFracc = explode("-",$row->fechaFirma);
            $datos["fechaCaptura"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
            $datos["distribuidor"][] = $row->distribuidor;
            $datos["color"][] = $row->color;
            $datos["tecnico"][] = $row->nombreTecnico;
            $datos["vin"][] = $row->vin;
            $datos["cat"][] = $row->cat;
            $datos["unidad"][] = $row->unidad;
        }

        //Verificamos de donde vino
        $this->loadAllView($datos,'previa/previa_lista');
    }

    //Comodin para pdf
    public function setDataPDF($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $this->setData($id_cita,$datos);
    }

    //Recuperamos la informacion de un registro
    public function setData($idOrden = 0,$datos = NULL)
    {
        $idOrden = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        if (!isset($datos["tipoRegistro"])) {
            $datos["tipoRegistro"] = "";
        }

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);

        $query = $this->mConsultas->get_result("idOrden",$idOrden,"previa");
        foreach ($query as $row){
            $datos["idOrden"] = $row->idOrden;
            $datos["folio"] = $row->folio;
            $datos["vin"] = $row->vin;
            $datos["cat"] = $row->cat;
            $datos["unidad"] = $row->unidad;
            $datos["color"] = $row->color;
            $datos["revisiones"] = explode("_",$row->revisiones);
            $datos["voltaje"] = $row->voltaje;
            $datos["especificacion"] = explode("_",$row->especificaciones);
            $datos["operacion"] = explode("_",$row->operaciones);
            $datos["ajuste"] = explode("_",$row->ajustes);
            $datos["prueba"] = explode("_",$row->pruebas);
            $datos["cod_Confg"] = explode("_",$row->configuracion);
            $datos["apariencia"] = explode("_",$row->apariencia);
            $datos["distribuidor"] = $row->distribuidor;
            $datos["direccion"] = $row->direccion;
            $datos["municipio"] = $row->municipio;
            $datos["estados"] = $row->estados;
            $datos["cp"] = $row->cp;
            $datos["numero"] = $row->numero;
            $datos["codigo"] = $row->codigo;
            $datos["apertura"] = $row->apertura;
            $datos["llantas"] = $row->llantas;
            $datos["reserva"] = $row->reserva;
            $datos["oasis"] = explode("_",$row->oasis);
            $datos["firmaTecnico"] = (file_exists($row->firmaTecnico)) ? $row->firmaTecnico : ""; //$row->firmaTecnico;
            $datos["tecnico"] = $row->nombreTecnico;
            $datos["fechaCaptura"] = $row->fechaFirma;
            $fechaFracc = explode("-",$row->fechaFirma);
            $datos["fecha"] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];

        }

        //Verificamos donde se imprimira la información
        if ($datos["destinoVista"] == "PDF") {
            $this->generatePDF($datos);
        } else {
            $this->loadAllView($datos,'previa/previa_edita');
        }
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "")
    {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_PV";
        for($i=0; $i<=6; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 0647, true);
        }
        
        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("previa/previa_pdf", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        return $data;
    }

    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
              // 'assets/js/garantias/firmas_SA.js',
              'assets/js/firmasMoviles/firmas_Previas.js',
              "assets/js/otrosFormularios/form_1_qro.js",
              "assets/js/otrosFormularios/bloqueos.js",
              'assets/js/superUsuario.js'
            )
        );

        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Previa Entrega"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
