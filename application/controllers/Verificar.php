<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verificar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('MConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }


  	public function index($formato = NULL,$idReg = 0)
  	{
        $datos['idRegistro'] = $idReg;
        $datos['destino'] = $formato;
  		$this->loadAllView($datos,'verificar');
  	}

    //Recibimos la informacion del formulario
    public function edit()
    {
        //Validamos campos
        $this->form_validation->set_rules('inputUsuario', '', 'required');
        $this->form_validation->set_rules('inputPassword', '', 'required');

        $this->form_validation->set_message('required','Campo requerido');

        //Conservamos los valores por si ocurre un error
        $datos['idRegistro'] = $this->input->post("idRegistro");
        $datos['destino'] = $this->input->post("destino");
        //Identifiacamos desde donde intentan accesar  ASE ->Asesores , TEC->Tecnicos, JDT ->Jefe de taller, EDIT -> desde el panel
        $datos['loginOrigen'] = $this->input->post("loginOrigen");

        //Si pasa las validaciones enviamos la informacion al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->validateUser($datos);
        }else{
            $datos["mensaje"]="0";
            //Identificamos a que vista va a retornar el error
            $this->loadAllView($datos,'verificar');
        }
    }

    //Validamos los datos proporcionados por el usuario
    public function validateUser($datos = NULL)
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);

        //Buscamos la informacion del usuario
        $usuario = $this->input->post("inputUsuario");
        $pass = $this->input->post("inputPassword");

        //Identificamos el panel al cual se quiere accesar para aplicar las reglas de validacion
        //Asesores
        if ($datos['loginOrigen'] == "ASE") {
            $datos["dirige"] = "ASE";
            $validacion = $this->validarAsesor($usuario,$pass);
            if ($validacion == "1") {
                $this->redirectEdit($datos);
            }else {
                $datos["mensaje"]="2";
                $this->loadAllView($datos,'verificar');
            }

        //Técnicos
        }elseif ($datos['loginOrigen'] == "TEC") {
            $datos["dirige"] = "TEC";
            $validacion = $this->validarTecnico($usuario,$pass);
            if ($validacion == "1") {
                $this->redirectEdit($datos);
            }else {
                $datos["mensaje"]="2";
                $this->loadAllView($datos,'verificar');
            }

        //Jefe de Taller
        }elseif ($datos['loginOrigen'] == "JDT") {
            $datos["dirige"] = "JDT";
            $validacion = $this->validarJefeTaller($usuario,$pass);
            if ($validacion == "1") {
                $this->redirectEdit($datos);
            }else {
                $datos["mensaje"]="2";
                $this->loadAllView($datos,'verificar');
            }

        //Acceso desde el planificador a ventanilla
        }elseif ($datos['loginOrigen'] == "OT") {
            $datos["dirige"] = "OT";
            $validacion = $this->validarOtroPanel($usuario,$pass);
            if ($validacion == "1") {
                $this->redirectEdit($datos);
            }else {
                $datos["mensaje"]="2";
                $this->loadAllView($datos,'verificar');
            }

        //Acceso desde el planificador y cierre de orden
        }elseif ($datos['loginOrigen'] == "PCO") {
            $datos["dirige"] = "PCO";
            $validacion = $this->validarVistaCierreOrden($usuario,$pass);
            if ($validacion == "1") {
                $this->redirectEdit($datos);
            }else {
                $datos["mensaje"]="2";
                $this->loadAllView($datos,'verificar');
            }
        
        //Acceso desde el planificador a asesor tecnico, mezclado
        }elseif ($datos['loginOrigen'] == "ASETEC") {
            $datos["dirige"] = "ASETEC";
            $validacion = $this->validarPanelAseTec($usuario,$pass);
            if ($validacion == "1") {
                $this->redirectEdit($datos);
            }else {
                $datos["mensaje"]="2";
                $this->loadAllView($datos,'verificar');
            }
        
        //Acceso desde el planificador para edicion
        }else {
            $datos["dirige"] = "ADMIN";
            $this->redirectEdit($datos);
        }
    }

    //Validamos los tecnico
    public function validarTecnico($usuario = "", $pass = "")
    {
        $datos["dirige"] = "TEC";
        $usuariosPermitidos=[
            //Jefe de taller
            //JOSE DE JESUS GONZALEZ FREGOSO ID = 79 Usuario = taller13
            'taller13',
            //JUAN CARLOS CHAVEZ LAUREANO ID = 58 Usuario = juan
            'juan',
            //Accesos especiales
            //FORD PLASENCIA ID=1 Usuario = fordplasematriz
            //'fordplasematriz',
            //Angel Salas ID = 13 Usuario = Angel Salas
            'Angel Salas',
            //Gregorio Jalomo Larios ID = 12 Usuario = jalomo
            //'jalomo',
            //Cierre de orden
            //Maria Elvia Garcia Tomas   ID = 68 Usuario = egarcia1
            'egarcia1',
            //Asesor
            //WENDY VERENICE FLORES CORONA ID = 57 Ususario = wflores
            'wflores',
             
            //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
            'pita',
            //Ángel Salas calderón  ID = 149 Usuario = angel
            'angel',
            //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
            'jalomo',
            //Shara Sahori   ID = 151 Usuario = shara
            'shara',
        ];
        
        //Validacion especial para Angel y jefe de taller
        if (in_array($usuario, $usuariosPermitidos)) {
            $clave = $this->Verificacion->login($usuario);
            //Verificamos el usuario
            $row = $clave->row();
            if (isset($row)){
                $passDB = $clave->row(0)->adminPassword;
                $idUsuario = $clave->row(0)->adminId;
                $nombre = $clave->row(0)->adminNombre;

                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);

                    //Confirmamos que se hayan guardado los datos
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                    //setcookie("panel_origen", "Tecnico", time()+25200);
                    //set_cookie('panel_origen','Tecnico','25200'); 

                    $retorno = "1";
                //Intentamso comprobar si existe la contraseña sin encriptar
                }elseif ($pass == $passDB) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                    //setcookie("panel_origen", "Tecnico", time()+25200); 
                    //set_cookie('panel_origen','Tecnico','25200'); 

                    $retorno = "1";
                //No pasa la validacion
                }else {
                    $retorno = "2";
                }
            //Si el usuario no existe
            }else {
                $retorno = "2";
            }
        //Si no son los accesos especiales, se consultan los tecnicos
        }else{
            //$query = $this->MConsultas->get_result("nombreUsuario",$usuario,"tecnicos");
            $query = $this->MConsultas->get_result_field("nombreUsuario",$usuario,"activo","1","tecnicos");
            foreach ($query as $row) {
                if ($row->activo == "1") {
                    $passDB = $row->password;
                    $idUsuario = $row->id;
                    $nombre = $row->nombre;
                }
            }

            //Verificamos que exista el dato
            if (isset($passDB)) {
                $datos["dirige"] = "TEC";
                //Comprobamos la contraseña encriptada
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);

                    //Confirmamos que se hayan guardado los datos
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                    //setcookie("panel_origen", "Tecnico", time()+25200); 
                    //set_cookie('panel_origen','Tecnico','25200'); 

                    $retorno = "1";
                //Intentamso comprobar si existe la contraseña sin encriptar
                }elseif ($pass == $passDB) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                    //setcookie("panel_origen", "Tecnico", time()+25200); 
                    //set_cookie('panel_origen','Tecnico','25200'); 

                    $retorno = "1";
                //No pasa la validacion
                }else {
                    $retorno = "2";
                }
            //Si el usuario no existe
            }else {
                $retorno = "2";
            }
        }

        return $retorno;
    }

    //Validamos a los asesores
    public function validarAsesor($usuario = "", $pass = "")
    {
        $datos["dirige"] = "ASE";
        $usuariosPermitidos=[
            //Asesores
            //WENDY VERENICE FLORES CORONA ID = 57 Ususario = wflores
            '57',
            //GABRIELA ALMA BENELI ROCHA CONTRERAS ID = 60 Ususario = BENELIROCHA
            '60',
            //ALEJANDRA DAFSTALI MEZA URZUA  ID = 51 Ususario = ALEJANDRA MEZA
            '51',
            //ROBERTO AVENDAÑO LOMELI ID = 62
            // '62',
            //EDITH BERENICE ZAMBRANO MONTERO ID = 107 Usuario = EDITH
            '107',
            //Nayeli
            '138',
            //Elena ID = 128 Usuario = ELENA BARBOSA
            '128',
            //DEISSER BETINA CASTRO ROMERO   ID = 141  Ususario = dcastro
            '141',
            //Jefe de taller
            //JOSE DE JESUS GONZALEZ FREGOSO ID = 79 Ususario = taller13
            '79',
            //JUAN CARLOS CHAVEZ LAUREANO ID = 58 Ususario = juan
            '58',
            //Accesos especiales
            //FORD PLASENCIA ID=1 Ususario = fordplasematriz
            '1',
            //Angel Salas ID = 13 Ususario = Angel Salas
            '13',
            //Gregorio Jalomo Larios ID = 12 Usuario = jalomo
            //'12',
             
            //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
            '148',
            //Ángel Salas calderón  ID = 149 Usuario = angel
            '149',
            //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
            '150',
            //Shara Sahori   ID = 151 Usuario = shara
            '151',
        ];

        //Enviamos el usuario que va a accesar
        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();

        if (isset($row)) {
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;
            $nombre = $clave->row(0)->adminNombre;

            //Validamos los usuarios autorizados
            if (in_array($idUsuario, $usuariosPermitidos)) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                    //setcookie("panel_origen", "Asesor", time()+25200);
                    //set_cookie('panel_origen','Asesor','25200'); 

                    $retorno = "1";
                //Si las ocntraseñas no coinciden
                }else {
                    //Intentamso comprobar si existe la contraseña sin encriptar
                    if ($pass == $passDB) {
                        //Guardamos el la informacion del usuario en la sesion
                        $dataUser = array(
                            "usuario" => $usuario,
                            "nombreUsuario" => strtoupper($nombre),
                            "idUsuario" => $idUsuario,
                            "rolIniciado" => $datos["dirige"],
                        );
                        //Guardamos los valores de sesion
                        $this->session->set_userdata($dataUser);
                        if (!$this->session->userdata('idUsuario')) {
                            $this->session->set_userdata($dataUser);
                        }

                        //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                        //setcookie("panel_origen", "Asesor", time()+25200);
                        //set_cookie('panel_origen','Asesor','25200'); 

                        $retorno = "1";
                    //No pasa la validacion
                    }else {
                        $retorno = "2";
                    }
                }
            //Si el usuario no existe
            }else {
                $retorno = "2";
            }
        //En caso que el usuario no sea un asesor
        }else {
            $retorno = "2";
        }

        return $retorno;
    }

    // Validamos el usuario de jefe de taller
    public function validarJefeTaller($usuario = "", $pass = "")
    {
        $datos["dirige"] = "JDT";
        $usuariosPermitidos=[
            //Asesor
            //WENDY VERENICE FLORES CORONA ID = 57 Ususario = wflores
            '57',
            //Jefe de taller
            //JOSE DE JESUS GONZALEZ FREGOSO ID = 79 Usuario = taller13
            '79',
            //JUAN CARLOS CHAVEZ LAUREANO ID = 58 Usuario = juan
            '58',
            //Accesos especiales
            //FORD PLASENCIA ID = 1 Usuario = fordplasematriz
            '1',
            //Angel Salas ID = 13 Usuario = Angel Salas
            '13',
            //Gregorio Jalomo Larios ID = 12 Usuario = jalomo
            //'12',
             
            //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
            '148',
            //Ángel Salas calderón  ID = 149 Usuario = angel
            '149',
            //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
            '150',
            //Shara Sahori   ID = 151 Usuario = shara
            '151',
        ];

        //Enviamos el usuario que va a accesar
        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();

        if (isset($row)) {
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;
            $nombre = $clave->row(0)->adminNombre;

            //Validamos los usuarios autorizados
            if (in_array($idUsuario, $usuariosPermitidos)) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                    //setcookie("panel_origen", "JefeTaller", time()+25200);
                    //set_cookie('panel_origen','JefeTaller','25200'); 

                    $retorno = "1";
                //Si las ocntraseñas no coinciden
                }else {
                    //Intentamso comprobar si existe la contraseña sin encriptar
                    if ($pass == $passDB) {
                        //Guardamos el la informacion del usuario en la sesion
                        $dataUser = array(
                            "usuario" => $usuario,
                            "nombreUsuario" => strtoupper($nombre),
                            "idUsuario" => $idUsuario,
                            "rolIniciado" => $datos["dirige"],
                        );
                        //Guardamos los valores de sesion
                        $this->session->set_userdata($dataUser);
                        if (!$this->session->userdata('idUsuario')) {
                            $this->session->set_userdata($dataUser);
                        }

                        //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                        //setcookie("panel_origen", "JefeTaller", time()+25200);
                        //set_cookie('panel_origen','JefeTaller','25200'); 

                        $retorno = "1";
                    //No pasa la validacion
                    }else {
                        $retorno = "2";
                    }
                }
            //Si el usuario no existe
            }else {
                $retorno = "2";
            }
        //En caso que el usuario no sea un asesor
        }else {
            $retorno = "2";
        }

        return $retorno;
    }

    //Validamos otros usuarios
    public function validarOtroPanel($usuario = "", $pass = "")
    {
        $datos["dirige"] = "OT";

        $usuariosPermitidos=[             
            //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
            '148',
            //Ángel Salas calderón  ID = 149 Usuario = angel
            '149',
            //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
            '150',
            //Shara Sahori   ID = 151 Usuario = shara
            '151',
        ];

        //Enviamos el usuario que va a accesar
        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();

        if (isset($row)) {
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;
            $nombre = $clave->row(0)->adminNombre;

            //Validamos los usuarios autorizados
            if (in_array($idUsuario, $usuariosPermitidos)) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                    //setcookie("panel_origen", "JefeTaller", time()+25200);
                    //set_cookie('panel_origen','JefeTaller','25200'); 

                    $retorno = "1";
                //Si las ocntraseñas no coinciden
                }else {
                    //Intentamso comprobar si existe la contraseña sin encriptar
                    if ($pass == $passDB) {
                        //Guardamos el la informacion del usuario en la sesion
                        $dataUser = array(
                            "usuario" => $usuario,
                            "nombreUsuario" => strtoupper($nombre),
                            "idUsuario" => $idUsuario,
                            "rolIniciado" => $datos["dirige"],
                        );
                        //Guardamos los valores de sesion
                        $this->session->set_userdata($dataUser);
                        if (!$this->session->userdata('idUsuario')) {
                            $this->session->set_userdata($dataUser);
                        }

                        //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                        //setcookie("panel_origen", "JefeTaller", time()+25200);
                        //set_cookie('panel_origen','JefeTaller','25200'); 

                        $retorno = "1";
                    //No pasa la validacion
                    }else {
                        $retorno = "2";
                    }
                }
            //Si el usuario no existe
            }else {
                $retorno = "2";
            }
        //En caso que el usuario no sea un asesor
        }else {
            if (($usuario == "Ventanilla")&&($pass == "Venta52Mostrador")) {
                $dataUser = array(
                    "usuario" => $usuario,
                    "nombreUsuario" => "VENTANILLA",
                    "idUsuario" => '0',
                    "rolIniciado" => $datos["dirige"],
                );

                //Guardamos los valores de sesion
                $this->session->set_userdata($dataUser);

                //Confirmamos que se hayan guardado los datos
                if (!$this->session->userdata('idUsuario')) {
                    $this->session->set_userdata($dataUser);
                }

                $retorno = "1";
            } else {
                $retorno = "2";
            }
        }

        return $retorno;
    }

    //Validamos otros usuarios
    public function validarVistaCierreOrden($usuario = "", $pass = "")
    {
        $datos["dirige"] = "PCO";
        $usuariosPermitidos=[
            //JOSE ALBERTO MUÑOZ SERRANO ID = 53 Usuario = jorge2730
            '53',
            //Tania cardenas ID = 145 Usuario = tcardenas@fordplasencia.com
            '145',
            //Jefe de Taller
            //JUAN CARLOS CHAVEZ LAUREANO ID = 58 Usuario = juan
            '58',
            //Accesos especiales
            //FORD PLASENCIA ID = 1 Usuario = fordplasematriz
            '1',
            //Angel Salas ID = 13 Usuario = Angel Salas
            '13',
            //Cierre de orden
            //Maria Elvia Garcia Tomas   ID = 68 Usuario = egarcia1
            '68',
            //Diosselim Neptali Galindo Jimenez   ID = 137  Usuario = Diosselim Galindo
            '137',
            //Asesor
            //WENDY VERENICE FLORES CORONA ID = 57 Ususario = wflores
            '57',
            //Otro usuario
            //Erendira Rayas  ID = 20  Usuario = Negocios_Matriz
            '20',
            //Alaiza Medina  ID = 152  Usuario = Amedina
            '152',
            //Gregorio Jalomo Larios ID = 12 Usuario = jalomo
            //'12',
             
            //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
            '148',
            //Ángel Salas calderón  ID = 149 Usuario = angel
            '149',
            //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
            '150',
            //Shara Sahori   ID = 151 Usuario = shara
            '151',
        ];

        //Enviamos el usuario que va a accesar
        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();

        if (isset($row)) {
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;
            $nombre = $clave->row(0)->adminNombre;

            //Validamos los usuarios autorizados
            if (in_array($idUsuario, $usuariosPermitidos)) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "nombreUsuario" => strtoupper($nombre),
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                    //setcookie("panel_origen", "cierre_orden", time()+25200);
                    //set_cookie('panel_origen','cierre_orden','25200'); 

                    $retorno = "1";
                //Si las ocntraseñas no coinciden
                }else {
                    //Intentamso comprobar si existe la contraseña sin encriptar
                    if ($pass == $passDB) {
                        //Guardamos el la informacion del usuario en la sesion
                        $dataUser = array(
                            "usuario" => $usuario,
                            "nombreUsuario" => strtoupper($nombre),
                            "idUsuario" => $idUsuario,
                            "rolIniciado" => $datos["dirige"],
                        );
                        //Guardamos los valores de sesion
                        $this->session->set_userdata($dataUser);
                        if (!$this->session->userdata('idUsuario')) {
                            $this->session->set_userdata($dataUser);
                        }

                        //Si se inicio sesion correctamente, creamos un identificador del hilo del origen
                        //setcookie("panel_origen", "cierre_orden", time()+25200);
                        //set_cookie('panel_origen','cierre_orden','25200'); 

                        $retorno = "1";
                    //No pasa la validacion
                    }else {
                        $retorno = "2";
                    }
                }
            //Si el usuario no existe
            }else {
                $retorno = "2";
            }
        //En caso que el usuario no sea un asesor
        }else {
            $retorno = "2";
        }

        return $retorno;
    }

    //Validamos a los usuarios con privilegios de asesores y tecnicos
    public function validarPanelAseTec($usuario = "", $pass = "")
    {
        $datos["dirige"] = "ASETEC";
        $usuariosPermitidos=[
            //Luis Alberto Pita Vázquez  ID = 148 Usuario = angel
            '148',
            //Ángel Salas calderón  ID = 149 Usuario = angel
            '149',
            //Gregorio Jalomo Larios  ID = 150 Usuario = jalomo
            '150',
            //Shara Sahori   ID = 151 Usuario = shara
            '151',
        ];

        //Enviamos el usuario que va a accesar
        $clave = $this->Verificacion->login($usuario);
        //Verificamos el usuario
        $row = $clave->row();

        if (isset($row)) {
            $passDB = $clave->row(0)->adminPassword;
            $idUsuario = $clave->row(0)->adminId;
            $nombre = $clave->row(0)->adminNombre;

            //Validamos los usuarios autorizados
            if (in_array($idUsuario, $usuariosPermitidos)) {
                //Comprobamos que la contraseña encriptada coincidan
                if ((md5(sha1($pass))) == $passDB ) {
                    //Guardamos el la informacion del usuario en la sesion
                    $dataUser = array(
                        "usuario" => $usuario,
                        "idUsuario" => $idUsuario,
                        "rolIniciado" => $datos["dirige"],
                        "nombreUsuario" => strtoupper($nombre),
                    );

                    //Guardamos los valores de sesion
                    $this->session->set_userdata($dataUser);
                    if (!$this->session->userdata('idUsuario')) {
                        $this->session->set_userdata($dataUser);
                    }

                    $retorno = "1";
                //Si las ocntraseñas no coinciden
                }else {
                    //Intentamso comprobar si existe la contraseña sin encriptar
                    if ($pass == $passDB) {
                        //Guardamos el la informacion del usuario en la sesion
                        $dataUser = array(
                            "usuario" => $usuario,
                            "idUsuario" => $idUsuario,
                            "rolIniciado" => $datos["dirige"],
                            "nombreUsuario" => strtoupper($nombre),
                        );
                        //Guardamos los valores de sesion
                        $this->session->set_userdata($dataUser);
                        if (!$this->session->userdata('idUsuario')) {
                            $this->session->set_userdata($dataUser);
                        }

                        $retorno = "1";
                    //No pasa la validacion
                    }else {
                        $retorno = "2";
                    }
                }
            //Si el usuario no existe
            }else {
                $retorno = "2";
            }
        //En caso que el usuario no sea un asesor
        }else {
            $retorno = "2";
        }

        return $retorno;
    }

    //Identificamos la ventana destino para edicion
    public function redirectEdit($datos = NULL)
    {
        //Identificamos a que vista se va a redireccionar para aabrir el panel
        //Asesores
        if ($datos['loginOrigen'] == "ASE") {
            redirect('Panel_Asesor/5', 'refresh');

        //Técnico
        }elseif ($datos['loginOrigen'] == "TEC") {
            redirect('Panel_Tec/5', 'refresh');

        //Jefe de taller
        }elseif ($datos['loginOrigen'] == "JDT") {
            redirect('Panel_JefeTaller/5', 'refresh');

        //Planificador - Cierre de orden 
        }elseif ($datos['loginOrigen'] == "PCO") {
            //redirect('OrdenServicio_Lista/PCO', 'refresh');
            redirect('Documentacion_CierreOrden/4', 'refresh');

        //Paner Mixto Assor - Tecnico
        }elseif ($datos['loginOrigen'] == "ASETEC") {
            redirect('Panel_AsesorTecnico/5', 'refresh');
        
        //Otros
        }elseif ($datos['loginOrigen'] == "OT") {
            redirect('Panel/5', 'refresh');
        }else {
            if ($datos['destino'] == "HM") {
                redirect('Multipunto_Principal/'.$datos['idRegistro'], 'refresh');
            } elseif ($datos['destino'] == "OS") {
                redirect('OrdenServicio_Verifica/'.$datos['idRegistro'], 'refresh');
            } elseif ($datos['destino'] == "OS_CM") {
                redirect('CtrolMaterial_Verifica/'.$datos['idRegistro'], 'refresh');
            } else {
                $datos["mensaje"]="0";
                $this->loadAllView($datos,'verificar');
            }
        }
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", "", TRUE),
            "titulo" => "Verificar identidad"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}
