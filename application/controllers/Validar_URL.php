<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Validar_URL extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->database();
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

  	public function index($panel_destino = "ADMIN",$resp = NULL)
  	{
        
  	}

    //Validar sesion en panel del tecnico
    public function panel_tecnico($indicador = '4',$datos = NULL)
    {
        //Validamos que exista una sesion iniciada
        if ($this->session->userdata('rolIniciado')) {
            redirect('Menu_Tecnico/4', 'refresh');
        
        //Si la sesion no esta iniciada o expiro, regregsamos al login del tecnico
        }else{
            redirect('Principal_Tec/4', 'refresh');
        }
    }

    //Validar sesion en panel del tecnico
    public function panel_asesor($indicador = '4',$datos = NULL)
    {
        //var_dump($this->session->userdata());
        //Validamos que exista una sesion iniciada
        if ($this->session->userdata('rolIniciado')) {
            redirect('Menu_Asesor/4', 'refresh');
        
        //Si la sesion no esta iniciada o expiro, regregsamos al login del tecnico
        }else{
            redirect('Principal_Asesor/4', 'refresh');
        }
    }

    //Validar sesion en panel del tecnico
    public function panel_jefeTaller($indicador = '4',$datos = NULL)
    {
        //var_dump($this->session->userdata());
        //Validamos que exista una sesion iniciada
        if ($this->session->userdata('rolIniciado')) {
            redirect('Menu_JefeTaller/4', 'refresh');
        
        //Si la sesion no esta iniciada o expiro, regregsamos al login del tecnico
        }else{
            redirect('Panel_JefeTaller/4', 'refresh');
        }
    }

    //Validar Alta multipunto
    public function alta_multipunto($id_cita = '0',$datos = NULL)
    {
        //Validamos que exista una sesion iniciada
        if ($this->session->userdata('rolIniciado')) {
            redirect('HM_ALTA/'.$id_cita, 'refresh');
        
        //Si la sesion no esta iniciada o expiro, regregsamos al login
        }else{
            //Nos aseguramos que aun no se haya redireccionado
            if (get_cookie('panel_origen')) {
                //Verificamos el rol de origen
                //Asesores
                if (get_cookie('panel_origen') == "Tecnico") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para tecnico
                    redirect('Principal_Tec/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "Asesor") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para asesor 
                    redirect('Principal_Asesor/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "JefeTaller") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para jefe de taller 
                    redirect('Principal_JefeTaller/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "cierre_orden") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para cierre de orden
                    redirect('Principal_CierreOrden/4', 'refresh');
                }elseif (get_cookie('panel_origen') == "Ventanilla") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Ventanilla
                    redirect('Panel/4', 'refresh');
                }
            }
        }
    }

    //Validar Alta multipunto
    public function edita_asesor_multipunto($id_cita = '0',$datos = NULL)
    {
        //Validamos que exista una sesion iniciada
        if ($this->session->userdata('rolIniciado')) {
            redirect('HM_Edita_Asesor/'.$id_cita, 'refresh');
        
        //Si la sesion no esta iniciada o expiro, regregsamos al login
        }else{
            //Nos aseguramos que aun no se haya redireccionado
            if (get_cookie('panel_origen')) {
                //Verificamos el rol de origen
                //Asesores
                if (get_cookie('panel_origen') == "Tecnico") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para tecnico
                    redirect('Principal_Tec/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "Asesor") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para asesor 
                    redirect('Principal_Asesor/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "JefeTaller") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para jefe de taller 
                    redirect('Principal_JefeTaller/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "cierre_orden") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para cierre de orden
                    redirect('Principal_CierreOrden/4', 'refresh');
                }elseif (get_cookie('panel_origen') == "Ventanilla") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Ventanilla
                    redirect('Panel/4', 'refresh');
                }
            }
        }
    }

    //Validar redireccionameiento edicion multipunto
    public function n_edita_multipunto($id_cita = '0',$datos = NULL)
    {
        //Validamos que exista una sesion iniciada
        if ($this->session->userdata('rolIniciado')) {
            redirect('HM_N_Edita/'.$id_cita, 'refresh');
        
        //Si la sesion no esta iniciada o expiro, regregsamos al login
        }else{
            //Nos aseguramos que aun no se haya redireccionado
            if (get_cookie('panel_origen')) {
                //Verificamos el rol de origen
                //Asesores
                if (get_cookie('panel_origen') == "Tecnico") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para tecnico
                    redirect('Principal_Tec/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "Asesor") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para asesor 
                    redirect('Principal_Asesor/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "JefeTaller") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para jefe de taller 
                    redirect('Principal_JefeTaller/4', 'refresh');
                } elseif (get_cookie('panel_origen') == "cierre_orden") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Pantalla login para cierre de orden
                    redirect('Principal_CierreOrden/4', 'refresh');
                }elseif (get_cookie('panel_origen') == "Ventanilla") {
                    //Eliminamos la cookie
                    delete_cookie('panel_origen');
                    //Ventanilla
                    redirect('Panel/4', 'refresh');
                }
            }
        }
    }

    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", '', TRUE),
            "titulo" => "Principal"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

    public function consulta($i = 0)
    {

    }


}
