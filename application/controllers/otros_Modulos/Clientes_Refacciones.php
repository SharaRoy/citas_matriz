<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_Refacciones extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

    //Inicio para validar si existe un regsitro o no
  	public function index($id = "0",$datos = NULL)
  	{
        //Verificamos si existe el registro
        $revision = $this->mConsultas->get_result("id",$id,"refacciones_cliente");
        foreach ($revision as $row) {
            $idHG = $row->id_cita;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idHG)) {
            //Verificamos si la cotización ya fue aceptada/rechazada por el cliente
            $this->setData($id,$datos);
        } else {
            $datos['folio'] = $this->generateFolio('refacciones_cliente');
            //$datos['orden'] = ($idOrden != "0") ? $idOrden : '';
            $datos['idRegistro'] = '0';
            $datos['asesores'] = $this->mConsultas->get_result("activo","1","operadores");
            $datos["tiempo_int"] = $this->cat_horas();
            $this->loadAllView($datos,'otros_Modulos/refacciones_clientes/rc_alta');
        }
  	}

    //Funcion para generar el folio de la venta
    public function generateFolio($tabla = "")
    {
        //Recuperamos el indice para el folio
        $fecha = date("Ymd");
        $registro = $this->mConsultas->last_id_refacciones($tabla);

        $nvoFolio = $registro + 1;
        if ($nvoFolio < 10) {
            $folio = "0000".$nvoFolio;
        } elseif(($nvoFolio > 9) && ($nvoFolio < 100)) {
            $folio = "000".$nvoFolio;
        } elseif(($nvoFolio > 99) && ($nvoFolio < 1000)) {
            $folio = "00".$nvoFolio;
        } elseif(($nvoFolio > 999) && ($nvoFolio < 1000)) {
            $folio = "0".$nvoFolio;
        }else {
            $folio = $nvoFolio;
        }
        return $folio;
    }

    public function cat_horas(){
        $contenedor = [];

        for ($i=0; $i < 13 ; $i= $i+1) {
            
            if($i < 1){
                $contenedor[] = "MEDIA";
            }else{
                $contenedor[] = $i;
                $contenedor[] = $i." Y MEDIA";
            }
        }

        return $contenedor;
    }

    //Validamos el formulario
    /*public function validateForm()
    {
        $datos["tipoRegistro"] = $this->input->post("cargaFormulario");
        $datos["orden"] = $this->input->post("orden");
        $datos["idRegistro"] = $this->input->post("registro");

        //Recuperamos el id orden_cdk
        //$datos["orden_CDK"] = $this->mConsultas->id_orden_intelisis($datos["orden"]);

        //Consultamos si ya se entrego la unidad
        $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($datos["orden"]);

        if ($datos["tipoRegistro"] == "1") {
            $this->form_validation->set_rules('fecha_folio', 'Campo requerido', 'required');
            $this->form_validation->set_rules('orden', 'Campo requerido', 'required|callback_existencia');
            $this->form_validation->set_rules('folio', 'Campo requerido', 'required');

            $this->form_validation->set_rules('asesor', 'Campo requerido', 'required');
            $this->form_validation->set_rules('serie', 'Campo requerido', 'required');
            //$this->form_validation->set_rules('estadoRefaccion', 'Campo requerido', 'required');

            $this->form_validation->set_rules('unidadTaller', 'Campo requerido', 'required');
            $this->form_validation->set_rules('modeloVehiculo', 'Campo requerido', 'required');

        } else {
            $this->form_validation->set_rules('orden', 'Campo requerido', 'required');
        }
        
        $this->form_validation->set_rules('estadoProceso', 'Campo requerido', 'required');
        
        $this->form_validation->set_rules('cantidad_0', 'Campo requerido', 'required');
        //$this->form_validation->set_rules('numParte_0', 'Campo requerido', 'required');
        $this->form_validation->set_rules('descripcionCorta_0', 'Campo requerido', 'required');

        //$this->form_validation->set_rules('fecha_pedido', 'Campo requerido', 'required');

        //Mensajes de error
        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('valid_email','Proporcione un correo electrónico valido');

        //Si pasa las validaciones enviamos la información al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";
            //var_dump(form_error());
            //var_dump($this->form_validation->run());
            $this->index($datos["idRegistro"],$datos);
        }
    }*/

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("order");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("orden",$idOrden,"refacciones_cliente");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Recibimos el formulario y guardamos
    public function guardar_formulario()
    {
        $tipo_formulario = $_POST["cargaFormulario"];
        $registro = date("Y-m-d H:i:s");
        $confirmacion = FALSE;

        //Si el registro se va a guardar por primera vez
        if ($tipo_formulario == "1") {
            //Verificamos que exista la orden de apertura
            $validar = $this->validar_orden();
            if ($validar) {
                //Rectificamso que no se haya guardado un registro con ese numero de cita
                $reevaluacion = $this->mConsultas->get_result("id_cita",$_POST["orden"],"refacciones_cliente");
                foreach ($reevaluacion as $row) {
                    $valida_edo = $row->id;
                }

                //Si existe el registro
                if (!isset($valida_edo)) {
                    $limite = $_POST["maxRenglon"];

                    //Verificamos los archivos generales
                    if($_POST["uploadfiles_resp"] != "") {
                        $archivos_2 = $this->validateDoc("uploadfiles");
                        $archivos_gral = $_POST["uploadfiles_resp"]."".$archivos_2;
                    }else {
                        $archivos_gral = $this->validateDoc("uploadfiles");
                    }

                    $contenedor = array(
                        "id" => NULL,
                        "fecha_folio" => $_POST["fecha_folio"],
                        "id_cita" => $_POST["orden"],
                        "estatus" => $_POST["estadoProceso"],
                        "notaEstatus" => $_POST["notaEstatus"],
                        "unidad_taller" => $_POST["unidadTaller"],
                        "telefono_principal" => $_POST["telefonoCliente"],
                        "telefono_secundario" => $_POST["telefonoCliente_2"],
                        "folio" => $_POST["folio"],
                        "asesor" => $_POST["asesor"],
                        "modeloVehiculo" => $_POST["modeloVehiculo"],
                        "serie" => $_POST["serie"],

                        "cantidad" => $this->mergeOfString2("cantidad_","",$limite),
                        "descripcionCorta" => $this->mergeOfString2("descripcionCorta_","",$limite),
                        "noParte" => $this->mergeOfString2("numParte_","",$limite),
                        "id_refaccion" => $this->mergeOfString2("id_refaccion_","",$limite),

                        "fecha_pedido" => $_POST["fecha_pedido"],
                        "noRemision" => $_POST["noRemision"],
                        "costo" => $_POST["costo"],
                        "noUnidadImov" => $_POST["unidadInmo"],
                        "fecha_promesa" => $_POST["fecha_promesa"],
                        "fecha_recibe" => $_POST["fecha_recibe"],
                        "fecha_taller" => $_POST["fecha_taller"],
                        "revisadaPor" => $_POST["revisadaPor"],
                        "fecha_instalacion" => $_POST["fechaInstalacion"],
                        "observaciones" => $_POST["observacionesPza"],
                        "costoDist" => $_POST["costoDist"],
                        "costoTotal" => $_POST["costoTotal"],
                        "tecnico" => $_POST["tecnico"],
                        "notas" => $_POST["notas"],
                        "fecha_BO" => $_POST["fechaBO"],
                        "backorder" => "0",
                        "archivos" => $archivos_gral,
                        "tiempo_inst_tecnico" => $_POST["tiempo_tecnico"]."/".$_POST["unidad_tecnico"],
                        "tiempo_inst_cleinte" => $_POST["tiempo_cliente"]."/".$_POST["unidad_cliente"],
                        "fecha_creacion" => $registro,
                        "fecha_actualizacion" => $registro,
                    );

                    $registroDB = $this->mConsultas->save_register('refacciones_cliente',$contenedor);

                    if ($registroDB != 0) {
                        for ($i=0; $i <= $limite ; $i++) { 
                            if (isset($_POST["cantidad_".$i])) {
                                if ($_POST["descripcionCorta_".$i] != "") {
                                    $paquete = array(
                                        "id" => NULL,
                                        "id_cita" => $_POST["orden"],
                                        "id_registro" => $registroDB,
                                        "id_refaccion" => $_POST["id_refaccion_".$i],
                                        "tipo" => "1",          //1 - público   2 - garantías
                                        "cantidad" => $_POST["cantidad_".$i],
                                        "descripcion" => $_POST["descripcionCorta_".$i],
                                        "clave_pieza" => $_POST["numParte_".$i],
                                        "existencia" => "NO",
                                        "activo" => "1",
                                        "pedido" => "0",
                                        "fecha_pedido" => $registro,
                                        "fecha_alta" => $registro,
                                        "fecha_edicion" => $registro,
                                    );

                                    $ref_p = $this->mConsultas->save_register('refaciones_psni',$paquete);
                                }
                            }
                        }

                        //Recuperamos el usuario que realiza el movimiento
                        if ($this->session->userdata('rolIniciado')) {
                            $usuario = $this->session->userdata('nombreUsuario');
                        }elseif ($this->session->userdata('id_usuario')) {
                            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
                        }else{
                            $usuario = "Ventanilla";
                        }

                        //Creamos el registro del estaus
                        $contenedor_estatus = array(
                            "id" => NULL,
                            "id_cita" => $contenedor["id_cita"],
                            "estatus_anterior" => "",
                            "estatus_actual" => $contenedor["estatus"],
                            "fecha_inicio" => $registro,
                            "fecha_fin" => $registro,
                            "usuario_afecta" => $usuario,
                            "motivo" => $contenedor["notaEstatus"],
                            "cambio_num" => 1,
                        );

                        $registro_estatus = $this->mConsultas->save_register('estatus_cliente_historico',$contenedor_estatus);

                        //Actualizamos en los presupuesto multipunto el registro
                        $this->actualizar_presupuesto($contenedor["id_refaccion"],$contenedor["id_cita"]);
                        
                        $respuesta = "OK_".$_POST["orden"];
                    } else {
                        $respuesta = "ERROR AL GUARDAR EL FORMULARIO_NS";
                    }
                    
                }else{
                   $respuesta = "YA EXISTE UN REGISTRO CON ESE No DE ORDEN_NS";
                }
            }else{
               $respuesta = "NO SE ENCONTRO UNA ORDEN ABIERTA_NS"; 
            }            
        } else {
            $limite = $_POST["maxRenglon"];

            if($_POST["uploadfiles_resp"] != "") {
                $archivos_2 = $this->validateDoc("uploadfiles");
                $archivos_gral = $_POST["uploadfiles_resp"]."".$archivos_2;
            }else {
                $archivos_gral = $this->validateDoc("uploadfiles");
            }

            $contenedor = array(
                "estatus" => $_POST["estadoProceso"],
                "notaEstatus" => $_POST["notaEstatus"],
                "unidad_taller" => $_POST["unidadTaller"],
                "folio" => $_POST["folio"],
                "asesor" => $_POST["asesor"],
                "modeloVehiculo" => $_POST["modeloVehiculo"],
                "serie" => $_POST["serie"],

                "cantidad" => $this->mergeOfString2("cantidad_","",$limite),
                "descripcionCorta" => $this->mergeOfString2("descripcionCorta_","",$limite),
                "noParte" => $this->mergeOfString2("numParte_","",$limite),
                "id_refaccion" => $this->mergeOfString2("id_refaccion_","",$limite),

                "fecha_pedido" => $_POST["fecha_pedido"],
                "noRemision" => $_POST["noRemision"],
                "costo" => $_POST["costo"],
                "noUnidadImov" => $_POST["unidadInmo"],
                //"fecha_promesa" => $_POST["fecha_promesa"],
                //"fecha_recibe" => $_POST["fecha_recibe"],
                "fecha_taller" => $_POST["fecha_taller"],
                "revisadaPor" => $_POST["revisadaPor"],
                "fecha_instalacion" => $_POST["fechaInstalacion"],
                "observaciones" => $_POST["observacionesPza"],
                "costoDist" => $_POST["costoDist"],
                "costoTotal" => $_POST["costoTotal"],
                "tecnico" => $_POST["tecnico"],
                "notas" => $_POST["notas"],
                "archivos" => $archivos_gral,
                "tiempo_inst_tecnico" => $_POST["tiempo_tecnico"]."/".$_POST["unidad_tecnico"],
                "tiempo_inst_cleinte" => $_POST["tiempo_cliente"]."/".$_POST["unidad_cliente"],
                "fecha_BO" => $_POST["fechaBO"],
                "fecha_actualizacion" => $registro,
            );

            $actualizar = $this->mConsultas->update_table_row('refacciones_cliente',$contenedor,'id',$_POST["registro"]);

            if ($actualizar) {
                //Actualizamos las refacciones
                for ($i=1; $i <= $limite ; $i++) { 
                    //Evitamos errores al llamar un campo
                    if (isset($_POST["descripcionCorta_".$i])) {
                        //Evitamos los renglones vacios
                        if ($_POST["descripcionCorta_".$i] != "") {
                            //Verificamos si es un renglon nuevo o una actualizacion
                            if ($_POST["id_index_".$i] == "0") {

                                $paquete = array(
                                    "id" => NULL,
                                    "id_cita" =>$_POST["orden"],
                                    "id_registro" => $_POST["registro"],
                                    "id_refaccion" => ((isset($_POST["id_refaccion_".$i])) ? $_POST["id_refaccion_".$i] : NULL),
                                    "tipo" => "1",          //1 - público   2 - garantías
                                    "cantidad" => $_POST["cantidad_".$i],
                                    "descripcion" => $_POST["descripcionCorta_".$i],
                                    "clave_pieza" => $_POST["numParte_".$i],
                                    "existencia" => "NO",
                                    "activo" => "1",
                                    "pedido" => "0",
                                    "fecha_pedido" => $registro,
                                    "fecha_alta" => $registro,
                                    "fecha_edicion" => $registro,
                                );

                                $ref_p = $this->mConsultas->save_register('refaciones_psni',$paquete);
                            }else{
                                $paquete = array(
                                    //"id_refaccion" => ((isset($_POST["id_refaccion_".$i])) ? $_POST["id_refaccion_".$i] : NULL),
                                    //"tipo" => "1",          //1 - público   2 - garantías
                                    "cantidad" => $_POST["cantidad_".$i],
                                    "descripcion" => $_POST["descripcionCorta_".$i],
                                    "clave_pieza" => $_POST["numParte_".$i],
                                    "activo" => (($_POST["id_refaccion_".$i] != "1A") ? "1" : "0"),
                                    "fecha_edicion" => $registro
                                );

                                $this->mConsultas->update_table_row('refaciones_psni',$paquete,'id',$_POST["id_index_".$i]);
                            }
                        }
                    }
                }

                //Recuperamos el usuario que realiza el movimiento
                if ($this->session->userdata('rolIniciado')) {
                    $usuario = $this->session->userdata('nombreUsuario');
                }elseif ($this->session->userdata('id_usuario')) {
                    $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
                }else{
                    $usuario = "Ventanilla";
                }

                //Recuperamos los datos del estatus anterior
                $estatus_revision = $this->mConsultas->ultimo_estatus('estatus_cliente_historico',$_POST["orden"]);
                $estatus_revision[2] = (int)$estatus_revision[2] + 1;
                
                //Actualizamos el cambio de estatus
                $contenedor_estatus = array(
                    "id" => NULL,
                    "id_cita" => $_POST["orden"],
                    "estatus_anterior" => $estatus_revision[0],
                    "estatus_actual" => $contenedor["estatus"],
                    "fecha_inicio" => $estatus_revision[1],
                    "fecha_fin" => $registro,
                    "usuario_afecta" => $usuario,
                    "motivo" => $contenedor["notaEstatus"],
                    "cambio_num" => $estatus_revision[2],
                );

                $registro_estatus = $this->mConsultas->save_register('estatus_cliente_historico',$contenedor_estatus);

                //Si se actualizo correctamente
                $respuesta = "OK_".$_POST["orden"];
            } else {
                $respuesta = "ERROR AL ACTUALIZAR EL FORMULARIO_NS";
            }
        }
        
        echo $respuesta;       
    }

    public function validar_orden()
    {
        $respuesta = FALSE;
        $id_cita = $_POST["orden"];
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"ordenservicio");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = TRUE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = FALSE;
        }

        return $respuesta;
    }

    //Recuperamos la informacion del formulario
    /*public function getDataForm($datos = NULL)
    {
        //Verificamos los archivos generales
        if($this->input->post("uploadfiles_resp") != "") {
            $archivos_2 = $this->validateDoc("uploadfiles");
            $archivos_gral = $this->input->post("uploadfiles_resp")."".$archivos_2;
        }else {
            $archivos_gral = $this->validateDoc("uploadfiles");
        }

        $registro = date("Y-m-d H:i");
        //Armamos el array para contener los valores
        if ($datos["tipoRegistro"] == "1") {
            $dataForm = array(
              "id" => NULL,
              "fecha_folio" => $this->input->post("fecha_folio")
            );
        }

        $dataForm["id_cita"] = $this->input->post("orden");
        $dataForm["estatus"] = $this->input->post("estadoProceso");
        $dataForm["notaEstatus"] = $this->input->post("notaEstatus");
        $dataForm["unidad_taller"] = $this->input->post("unidadTaller");
        $dataForm["telefono_principal"] = $this->input->post("telefonoCliente");
        $dataForm["telefono_secundario"] = $this->input->post("telefonoCliente_2");
        $dataForm["folio"] = $this->input->post("folio");
        $dataForm["asesor"] = $this->input->post("asesor");
        $dataForm["modeloVehiculo"] = $this->input->post("modeloVehiculo");
        $dataForm["serie"] = $this->input->post("serie");

        $limite = $this->input->post("maxRenglon");

        $dataForm["cantidad"] = $this->mergeOfString("cantidad_","",$limite);
        $dataForm["descripcionCorta"] = $this->mergeOfString("descripcionCorta_","",$limite);
        $dataForm["noParte"] = $this->mergeOfString("numParte_","",$limite);
        $dataForm["id_refaccion"] = $this->mergeOfString("id_refaccion_","",$limite);
        
        $dataForm["fecha_pedido"] = $this->input->post("fecha_pedido");
        $dataForm["noRemision"] = $this->input->post("noRemision");
        $dataForm["costo"] = $this->input->post("costo");
        $dataForm["noUnidadImov"] = $this->input->post("unidadInmo");
        $dataForm["fecha_promesa"] = $this->input->post("fecha_promesa");
        $dataForm["fecha_recibe"] = $this->input->post("fecha_recibe");
        $dataForm["fecha_taller"] = $this->input->post("fecha_taller");
        $dataForm["revisadaPor"] = $this->input->post("revisadaPor");
        $dataForm["fecha_instalacion"] = $this->input->post("fechaInstalacion");
        $dataForm["observaciones"] = $this->input->post("observacionesPza");
        $dataForm["costoDist"] = $this->input->post("costoDist");
        $dataForm["costoTotal"] = $this->input->post("costoTotal");
        $dataForm["tecnico"] = $this->input->post("tecnico");
        $dataForm["notas"] = $this->input->post("notas");
        $dataForm["fecha_BO"] = $this->input->post("fechaBO");
        $dataForm["backorder"] = '0';
        $dataForm["archivos"] = $archivos_gral;

        if ($datos["tipoRegistro"] == "1") {
            $dataForm["fecha_creacion"] = $registro;
        }
        
        $dataForm["fecha_actualizacion"] = $registro;

        //Validamos si es un registro de alta o una actualizacion
        //Registro de alta
        if ($datos["tipoRegistro"] == "1") {
            $registroDB = $this->mConsultas->save_register('refacciones_cliente',$dataForm);
            if ($registroDB != 0) {
                //Recuperamos el usuario que realiza el movimiento
                if ($this->session->userdata('rolIniciado')) {
                    $usuario = $this->session->userdata('nombreUsuario');
                }elseif ($this->session->userdata('id_usuario')) {
                    $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
                }else{
                    $usuario = "Ventanilla";
                }

                //Creamos el registro del estaus
                $contenedor_estatus = array(
                    "id" => NULL,
                    "id_cita" => $dataForm["orden"],
                    "estatus_anterior" => "",
                    "estatus_actual" => $dataForm["estatus"],
                    "fecha_inicio" => $registro,
                    "fecha_fin" => $registro,
                    "usuario_afecta" => $usuario,
                    "motivo" => $dataForm["notaEstatus"],
                    "cambio_num" => 1,
                );

                $registro_estatus = $this->mConsultas->save_register('estatus_cliente_historico',$contenedor_estatus);

                $datos["mensaje"] = "1";
                redirect(base_url().'ClienteRF_Lista/'.$datos["mensaje"]);
            }else {
                $datos["mensaje"] = "2";
                $this->index($datos["idRegistro"],$datos);
            }

            // $this->index($datos["idOrden"],$datos);
        //Registro de actualizacion
        }else {
            $actualizar = $this->mConsultas->update_table_row('refacciones_cliente',$dataForm,'id',$datos["idRegistro"]);

            //Recuperamos el usuario que realiza el movimiento
            if ($this->session->userdata('rolIniciado')) {
                $usuario = $this->session->userdata('nombreUsuario');
            }elseif ($this->session->userdata('id_usuario')) {
                $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
            }else{
                $usuario = "Ventanilla";
            }

            //Recuperamos los datos del estatus anterior
            $estatus_revision = $this->mConsultas->ultimo_estatus('estatus_cliente_historico',$datos["orden"]);
            $estatus_revision[2] = (int)$estatus_revision[2] + 1;
            
            //Actualizamos el cambio de estatus
            $contenedor_estatus = array(
                "id" => NULL,
                "id_cita" => $datos["orden"],
                "estatus_anterior" => $estatus_revision[0],
                "estatus_actual" => $dataForm["estatus"],
                "fecha_inicio" => $estatus_revision[1],
                "fecha_fin" => $registro,
                "usuario_afecta" => $usuario,
                "motivo" => $dataForm["notaEstatus"],
                "cambio_num" => $estatus_revision[2],
            );

            $registro_estatus = $this->mConsultas->save_register('estatus_cliente_historico',$contenedor_estatus);
            
            if ($actualizar) {
                $datos["mensaje"] = "1";
                //redirect(base_url().'ClienteRF_Lista/'.$datos["mensaje"]);
                $this->index($datos["idRegistro"],$datos);
            }else {
                $datos["mensaje"] = "0";
                $this->index($datos["idRegistro"],$datos);
            }
            //$this->index($datos["idRegistro"],$datos);
        }
    }*/

    //Validar multiple renglones del campo
    public function mergeOfString2($campo = "",$elseValue = "",$limite = 0)
    {   
        if ($campo == "") {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            //Separamos los index de los id de tipo de usuario
            for ($i=0; $i < $limite ; $i++) {
                if (isset($_POST[$campo."".$i])) {
                    if ($_POST[$campo."".$i] != "") {
                        $respuesta .= $_POST[$campo."".$i]."_";
                    }
                }
            }
        }
        return $respuesta;
    }

    //Validar multiple renglones del campo
    public function mergeOfString($campo = "",$elseValue = "",$limite = 0)
    {   
        if ($campo == "") {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            //Separamos los index de los id de tipo de usuario
            for ($i=0; $i < $limite ; $i++) {
                if($this->input->post($campo."".$i)){
                    if ($this->input->post($campo."".$i) != "") {
                        $respuesta .= $this->input->post($campo."".$i)."_";
                    }
                }
            }
        }
        return $respuesta;
    }

    //Validamos y guardamos los documentos
    public function validateDoc($nombreDoc = NULL)
    {
        $path = "";
        //300 segundos  = 5 minutos
        //ini_set('max_execution_time',600);

        if ($nombreDoc != NULL) {
            //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
            for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
                $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
                $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
                $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
                $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
                $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

                //Url donde se guardara la imagen
                $urlDoc = "assets/imgs/docOasis";
                //Generamos un nombre random
                $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $urlNombre = date("YmdHi")."_RC";
                for($j=0; $j<=6; $j++ ){
                   $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
                }

                //Validamos que exista la carpeta destino   base_url()
                if(!file_exists($urlDoc)){
                   mkdir($urlDoc, 0647, true);
                }

                //Configuramos las propiedades permitidas para la imagen
                $config['upload_path'] = $urlDoc;
                $config['file_name'] = $urlNombre;
                $config['allowed_types'] = "*";

                //Cargamos la libreria y la inicializamos
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('tempFile')) {
                     $data['uploadError'] = $this->upload->display_errors();
                     // echo $this->upload->display_errors();
                     $path .= "";
                }else{
                     //Si se pudo cargar la imagen la guardamos
                     $fileData = $this->upload->data();
                     $ext = explode(".",$_FILES['tempFile']['name']);
                     $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                     $path .= "|";
                }
            }
        }

        return $path;
    }

    //Recuperamos los ids que se afectaran
    public function actualizar_presupuesto($ids = "",$id_cita = "")
    {
        $desglose = explode("_",$ids);
        //Verificamos que existan valores
        if (count($desglose)>0) {
            $registro = date("Y-m-d H:i:s");

            //Identificamos que tipo de presupuesto es 
            $revision = $this->mConsultas->get_result("idOrden",$id_cita,"cotizacion_multipunto");
            foreach ($revision as $row) {
                if ($row->identificador == "M") {
                    $idCotizacion = $row->idCotizacion;
                }
            }

            if (isset($idCotizacion)) {
                $dataCoti['aceptoTermino'] = "Val";
                $dataCoti['fecha_psni'] = $registro;
                $dataCoti['solicita_psni'] = "1";
                $dataCoti['fech_actualiza'] = $registro;

                $actualizar = $this->mConsultas->update_table_row_2('cotizacion_multipunto',$dataCoti,'idOrden',$id_cita,'identificador','M');

                if ($actualizar) {
                    //Recuperamos datos extras para la notificación
                    $query = $this->mConsultas->notificacionTecnicoCita($id_cita); 
                    foreach ($query as $row) {
                        $tecnico = $row->tecnico;
                        $modelo = $row->vehiculo_modelo;
                        $placas = $row->vehiculo_placas;
                        $asesor = $row->asesor;
                    }

                    $texto = SUCURSAL.". Solicitud de refacción por PSNI de la Orden: ".$id_cita." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                        
                    //Ventanilla
                    $this->sms_general('3316721106',$texto);
                    $this->sms_general('3319701092',$texto);
                    $this->sms_general('3317978025',$texto);
                    
                    //Tester
                    //$this->sms_general('3328351116',$texto);
                    //Angel evidencia
                    $this->sms_general('5535527547',$texto);
                    
                    //Grabamos el evento para el panel de gerencia
                    $registro_evento = array(
                        'envia_departamento' => 'Ventanilla/Asesor',
                        'recibe_departamento' => 'Ventanilla',
                        'descripcion' => 'Solicitud de refacción por PSNI',
                    );

                    $this->registroEvento($id_cita,$registro_evento);
                }
            } else {
                $bandera = FALSE;
                //Actualizamos las refacciones
                for ($i=0; $i < count($desglose); $i++) {
                    //Validamos que las refacciones tengan un id valido
                    if ($desglose[$i] != "0") {
                        $actualizacion = array(
                            "autoriza_cliente" => "1",
                            "solicita-psni" => "1",
                            "fecha_psni" => $registro,
                            "fecha_actualiza" => $registro,
                        );

                        $actualizar = $this->mConsultas->update_table_row('presupuesto_multipunto',$actualizacion,'id',$desglose[$i]);
                        if ($actualizar) {
                            //Si se actualizo correctamente
                            $bandera = TRUE;
                        }
                    }
                }

                //Si se actualizo al menos una refaccion, hactualizamos el presupuesto general
                if ($bandera) {
                    $recuperado = 0;
                    $cancelado = 0;
                    $iva = 0;
                    //Recuperamos las piezas para sacar nuevos totales
                    $query = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_multipunto");
                    foreach ($query as $row){
                        //Confirmamos que la refaccion este activa
                        if ($row->activo == "1") {
                            //Si el id pertenecem para recuperar el total
                            if (in_array($row->id,$desglose)) {
                                $costo_mo = ((float)$row->cantidad * (float)$row->costo_pieza);
                                $cosot_ref = ((float)$row->horas_mo * (float)$row->costo_mo);

                                $recuperado += ($costo_mo + $cosot_ref);
                                $iva = $recuperado * 0.16;
                            }else{
                                //Mientras que no sea garantia, se acumula en el monto cancelado
                                if ($row->pza_garantia == "0") {
                                    $costo_mo = ((float)$row->cantidad * (float)$row->costo_pieza);
                                    $cosot_ref = ((float)$row->horas_mo * (float)$row->costo_mo);

                                    $cancelado += ($costo_mo + $cosot_ref);
                                }
                            } 
                        }
                    }

                    $actualiza_presupuesto = array(
                        "cancelado" => $cancelado,
                        "iva" => $iva,
                        "total" => ($recuperado + $iva),
                        "acepta_cliente" => "Val",
                        "solicita_psni" => "1",
                        "solicitud_psni" => $registro,
                        //"fecha_actualiza" => $registro,
                    );
                    //Actualizamos el registro
                    $actualiza_reg = $this->mConsultas->update_table_row_2('presupuesto_registro',$actualiza_presupuesto,"id_cita",$id_cita,"identificador","M");

                    //Si se actualizo correctamente, enviamos la notificacion
                    if ($actualiza_reg) {
                        //Recuperamos datos extras para la notificación
                        $query = $this->mConsultas->notificacionTecnicoCita($id_cita); 
                        foreach ($query as $row) {
                            $tecnico = $row->tecnico;
                            $modelo = $row->vehiculo_modelo;
                            $placas = $row->vehiculo_placas;
                            $asesor = $row->asesor;
                        }

                        $texto = SUCURSAL.". Solicitud de refacción por PSNI de la Orden: ".$id_cita." TÉCNICO: ".$tecnico."  VEHÍCULO: ".$modelo." PLACAS: ".$placas." ASESOR: ".$asesor.".";
                            
                        //Ventanilla
                        $this->sms_general('3316721106',$texto);
                        $this->sms_general('3319701092',$texto);
                        $this->sms_general('3317978025',$texto);
                        
                        //Tester
                        //$this->sms_general('3328351116',$texto);
                        //Angel evidencia
                        $this->sms_general('5535527547',$texto);
                        
                        //Grabamos el evento para el panel de gerencia
                        $registro_evento = array(
                            'envia_departamento' => 'Ventanilla/Asesor',
                            'recibe_departamento' => 'Ventanilla',
                            'descripcion' => 'Solicitud de refacción por PSNI',
                        );

                        $this->registroEvento($id_cita,$registro_evento);
                    }
                }
            }     
        }
    }

    //Registro de eventos 
    public function registroEvento($id_cita = 0, $datos = NULL)
    {
        //Recuperamos los datos del usuario
        if ($this->session->userdata('rolIniciado')) {
            $envia_id_usuario = $this->session->userdata('idUsuario');
            //$envia_nombre_usuario = $datos["nombre"];

            if ($this->session->userdata('rolIniciado') == "TEC") {
                $revision = $this->mConsultas->get_result("id",$envia_id_usuario,"tecnicos");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->nombre);
                }
            } else {
                $revision = $this->Verificacion->get_result("adminId",$envia_id_usuario,"admin");
                foreach ($revision as $row) {
                    $envia_nombre_usuario = strtoupper($row->adminNombre);
                }
            }
        }else{
            $envia_id_usuario = "0";//$datos["idUsuario"];
            $envia_nombre_usuario = "Ventanilla";//$datos["nombre"];
        }

        //$envia_departamento = $datos["dpo"];
        $sucursal = BIN_SUCURSAL;
        $envia_fecha_hora = date("Y-m-d H:i:s");
        //$recibe_departamento = $datos["dpoRecibe"];
        $id_cita = $id_cita;
        //$descripcion = $datos["descripcion"];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/gerencia/index.php/tiempos/insertar_tiempo");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "envia_id_usuario=".$envia_id_usuario."&envia_nombre_usuario=".$envia_nombre_usuario."&envia_departamento=".$datos["envia_departamento"]."&sucursal=".$sucursal."&envia_fecha_hora=".$envia_fecha_hora."&recibe_departamento=".$datos["recibe_departamento"]."&id_cita=".$id_cita."&descripcion=".$datos["descripcion"]."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);
    }

    //Envio de notificaciones
    public function sms_general($celular_sms='',$mensaje_sms=''){
        $sucursal = BIN_SUCURSAL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=".$sucursal."");
        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        //var_dump($server_output );
        curl_close ($ch);

        //echo "OK";
    }

    //Recuperamos todos los registro
    public function comodinLista($respuesta = 4, $datos = NULL)
    {
        //Verificamos si existe el registro
        //$query = $this->mConsultas->get_table_list("refacciones_cliente","id");
        $query = $this->mConsultas->lista_clietes_refacciones();
        foreach ($query as $row) {
            if ($row->baja == "0") {
                $fecha_recibe = new DateTime($row->fecha_recibe);
                $datos["fechaRecibe"][] = $fecha_recibe->format('d/m/Y');
                
                $datos["id"][] = $row->id ;
                $datos["orden"][] = $row->id_cita ;
                $datos["estatus"][] = $row->estatus ;
                $datos["tecnico"][] = $row->tecnico ;
                $datos["asesor"][] = $row->asesor ;
                $datos["folio"][] = $row->folio ;
                //$datos["descripcionCorta"][] = $row->descripcionCorta ;                        
                $datos["unidad_taller"][] = $row->unidad_taller ;
                $datos["telefono"][] = "Tel 1. ".$row->telefono_principal ."<br>Tel 2. ".$row->telefono_secundario;
                //$datos["modelo"][] = $row->modeloVehiculo ; 
                $datos["serie"][] = $row->serie ;
                
                $consultaSeri = "907".$row->id_cita; //($row->serie != '') ? $this->mConsultas->idHistorialProactivo(strtoupper($row->serie)) : '0';
                $datos["idProactivo"][] = $consultaSeri;

                $datos["estatus"][] = $row->estatus ;

                //Clasificamos el color a mostrar por el estaus
                if (strtoupper($row->estatus) == "DISP INSTALAR") {
                    $datos["estatusColor"][] = "#92D050";
                    $datos["colorLetra"][] = "#FFFFFF";
                }elseif (strtoupper($row->estatus) == "NO CONTESTA") {
                    $datos["estatusColor"][] = "#FF0000";
                    $datos["colorLetra"][] = "#000000";
                }elseif (strtoupper($row->estatus) == "RESERVADO") {
                    $datos["estatusColor"][] = "#7030A0";
                    $datos["colorLetra"][] = "#FFFFFF";
                }elseif (strtoupper($row->estatus) == "NO IDENTIFICADO") {
                    $datos["estatusColor"][] = "#FFFF00";
                    $datos["colorLetra"][] = "#000000";
                }elseif (strtoupper($row->estatus) == "POR RESERVAR") {
                    $datos["estatusColor"][] = "#00B0F0";
                    $datos["colorLetra"][] = "#FFFFFF";
                }else{  //PENDIENTE
                    $datos["estatusColor"][] = "#ffffff";
                    $datos["colorLetra"][] = "#000000";
                }

                $datos["estado_refacciones"][] = $row->estado_refacciones;
                $datos["acepta_cliente"][] = $row->acepta_cliente;
                
                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $datos["color"][] = "background-color:#f5acaa;";
                    } elseif ($row->estado_refacciones == "1") {
                        $datos["color"][] = "background-color:#f5ff51;";
                    } elseif ($row->estado_refacciones == "2") {
                        $datos["color"][] = "background-color:#5bc0de;";
                    }else{
                        $datos["color"][] = "background-color:#c7ecc7;";
                    }
                } else {
                    $datos["color"][] = "background-color:red;color:white;";
                }
            }
        }

        $datos["mensaje"] = $respuesta;

        $this->loadAllView($datos,'otros_Modulos/refacciones_clientes/lista_refacciones_clientes');
    }

    //Recuperamos los datos del registro
    public function setData($idOrden = 0, $datos = NULL)
    {
        //Verificamos si existe el registro
        $revision = $this->mConsultas->get_result("id",$idOrden,"refacciones_cliente");
        foreach ($revision as $row) {
            $datos["idRegistro"] = $row->id;

            $datos["orden"] = $row->id_cita ;
            $datos["estadoProceso"] = strtoupper( $row->estatus );
            $datos["notaEstatus"] = $row->notaEstatus ;
            $datos["unidadTaller"] = $row->unidad_taller ;
            $datos["folio"] = $row->folio ;
            $datos["asesor"] = $row->asesor ;
            $datos["modeloVehiculo"] = $row->modeloVehiculo ;
            $datos["serie"] = $row->serie ;

            $datos["cantidad"] = explode("_",$row->cantidad) ;
            $datos["descripcionCorta"] = explode("_",$row->descripcionCorta) ;
            $datos["numParte"] = explode("_",$row->noParte) ;
            $datos["id_refaccion"] = explode("_",$row->id_refaccion) ;
            
            $datos["fecha_pedido"] = $row->fecha_pedido ;
            $datos["noRemision"] = $row->noRemision ;
            $datos["costo"] = $row->costo ;
            $datos["unidadInmo"] = $row->noUnidadImov ;
            
            $datos["fecha_promesa"] = $row->fecha_promesa;
            $datos["fecha_recibe"] = $row->fecha_recibe ; 
            $datos["fecha_taller"] = $row->fecha_taller ;
            $datos["fecha_folio"] = $row->fecha_folio ;
            $datos["fechaBO"] = $row->fecha_BO ;

            $datos["revisadaPor"] = $row->revisadaPor ;
            $datos["fechaInstalacion"] = $row->fecha_instalacion ;
            $datos["observacionesPza"] = $row->observaciones ;
            $datos["costoDist"] = $row->costoDist ;

            $datos["costoTotal"] = $row->costoTotal ;
            $datos["tecnico"] = $row->tecnico ;
            $datos["notas"] = $row->notas ;

            $datos["archivos_txt"] = $row->archivos;
            $datos["archivos"] = explode("|",$row->archivos);

            $datos["t_tec"] = explode("/",$row->tiempo_inst_tecnico) ;
            $datos["t_cl"] = explode("/",$row->tiempo_inst_cleinte) ;

        }

        if (isset($datos["orden"])) {
            //Cargamos refacciones
            $revision = $this->mConsultas->get_result_field("id_registro",$datos["idRegistro"],"activo","1","refaciones_psni");
            foreach ($revision as $row) {
                if($row->tipo == "1"){
                    $datos["refacciones"][] = array(
                        "id_refaccion" => $row->id,
                        "cantidad" => $row->cantidad,
                        "descripcion" => $row->descripcion,
                        "clave_pieza" => $row->clave_pieza
                    );
                }
            }
        }

        //Recuperamos el id orden_cdk
        //$datos["orden_CDK"] = $this->mConsultas->id_orden_intelisis($datos["orden"]);
        
        $datos['asesores'] = $this->mConsultas->get_result("activo","1","operadores");
        $datos["tiempo_int"] = $this->cat_horas();
        //Consultamos si ya se entrego la unidad
        $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($datos["orden"]);

        $this->loadAllView($datos,'otros_Modulos/refacciones_clientes/rc_edita');
    }

    //Recuperamos los campos para prellenar un formulario 
    public function loadDataForm()
    {
        $dato = $_POST['idOrden'];
        $cadena = "";
        $precarga = $this->mConsultas->precargaConsulta($dato);
        foreach ($precarga as $row){
            $cadena .= $row->tecnico."|";
            $cadena .= $row->asesor."|";
            $cadena .= $row->subcategoria."|";
            $cadena .= $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno."|";
            $cadena .= $row->telefono_movil."|";
            $cadena .= (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion)."|";
            $cadena .= $row->fecha_entrega."|";
            $cadena .= $row->fecha_recepcion."|";
            $cadena .= $row->otro_telefono."|";
        }

        echo $cadena;
    }

    public function cargar_datos_presupuesto()
    {
        $id_cita = $_POST['id_cita'];
        $cadena = "";

        $resultado = $this->mConsultas->refacciones_rechazadas_cm($id_cita);

        foreach ($resultado as $row){ 
            //Verificamos que sea un presupuesto con refacciones rechazadas
            if ($row->aceptoTermino == "No") {
                //Creamos los renglones
                $renglones = $this->createCells(explode("|",$row->contenido));

                foreach ($renglones as $index => $value) {
                    if ($renglones[$index][1] != "") {
                        if ((count($renglones[$index])>6)&&($renglones[$index][1] != "")) {
                            $cadena .= "0"."=";
                            $cadena .= $renglones[$index][0]."=";
                            $cadena .= $renglones[$index][1]."=";
                            $cadena .= $renglones[$index][6]."=";

                            $cadena .= "|";
                        }
                    }
                }
            } elseif ($row->aceptoTermino == "Val") {
                //Creamos los renglones
                $renglones = $this->createCells(explode("|",$row->contenido));

                foreach ($renglones as $index => $value) {
                    if (count($renglones[$index]) >9) {
                        if (($renglones[$index][9] != "Aceptada")&&($renglones[$index][9] != "")) {
                            $cadena .= "0"."=";
                            $cadena .= $renglones[$index][0]."=";
                            $cadena .= $renglones[$index][1]."=";
                            $cadena .= $renglones[$index][6]."=";

                            $cadena .= "|";
                        } 
                        
                    } 
                }
            }
        }

        if (strlen($cadena) < 6) {
            $resultado_2 = $this->mConsultas->refacciones_rechazadas_pm($id_cita);

            foreach ($resultado_2 as $row){ 
                //Verificamos que sea un presupuesto con refacciones rechazadas
                if (($row->acepta_cliente == "No") || ($row->acepta_cliente == "Val")) {
                    //Imprimimos las refacciones que NO tienen garantia y que no fueron aprovadas por el cliente
                    if (($row->pza_garantia == "0") && ($row->autoriza_cliente == "0")) {
                        $cadena .= $row->id."=";
                        $cadena .= $row->cantidad."=";
                        $cadena .= $row->descripcion."=";
                        $cadena .= $row->num_pieza."=";

                        $cadena .= "|";
                    }
                } 
            }
        } 
        echo $cadena;
    }

    //Separamos las filas recibidas de la tabla por celdas
    public function createCells($renglones = NULL)
    {
        $filtrado = [];
        //Si existe una cadena para tratar
        if ($renglones) {
            if (count($renglones)>0) {
                //Recorremos los renglones almacenados
                for ($i=0; $i <count($renglones) ; $i++) {
                    //Separamos los campos de cada renglon
                    $temporal = explode("_",$renglones[$i]);

                    //Comprobamos que se haya hecho una partición
                    if (count($temporal)>3) {
                        //Guardamos los campos en la variable que se retornara
                        $filtrado[] = $temporal;
                    }
                }
            }
        }
        return  $filtrado;
    }

    //Recuperamos las fechas para relaizar los filtros
    public function filtroPorFechas()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];

        if ($fecha_fin == "") {
            $precarga = $this->mConsultas->busquedaClientesRefFecha_1($fecha_inicio);
        } else {
            $precarga = $this->mConsultas->busquedaClientesRefFecha($fecha_inicio,$fecha_fin);
        }
        
        $cadena = "";
        
        foreach ($precarga as $row){
            $cadena .= $row->id_cita."=" ;              //0
            $cadena .= $row->folio."=" ;                //1
            $cadena .= $row->serie."=" ;                //2
            $cadena .= $row->unidad_taller."=" ;        //3
            $cadena .= $row->asesor."=" ;               //4
            $cadena .= $row->tecnico."=" ;              //5
            
            $cadena .= $row->modeloVehiculo."=" ;       //6

            $fecha_recibe = new DateTime($row->fecha_recibe);
            $cadena .= $fecha_recibe->format('d/m/Y')."=";      //7

            $cadena .= $row->id."=" ;                   //8

            $serie = ($row->serie != "") ? $row->serie : "-";
            //$this->mConsultas->idHistorialProactivo(strtoupper($serie))."=";
            $cadena .= "907".$row->id_cita."=";         //9

            //Clasificamos el color a mostrar por el estaus
            if (strtoupper($row->estatus) == "DISP INSTALAR") {
                $cadena .= "#92D050"."=";       //10
                $cadena .= "#FFFFFF"."=";       //11
            }elseif (strtoupper($row->estatus) == "NO CONTESTA") {
                $cadena .= "#FF0000"."=";       //10
                $cadena .= "#000000"."=";       //11
            }elseif (strtoupper($row->estatus) == "RESERVADO") {
                $cadena .= "#7030A0"."=";       //10
                $cadena .= "#FFFFFF"."=";       //11
            }elseif (strtoupper($row->estatus) == "NO IDENTIFICADO") {
                $cadena .= "#FFFF00"."=";       //10
                $cadena .= "#000000"."=";       //11
            }elseif (strtoupper($row->estatus) == "POR RESERVAR") {
                $cadena .= "#00B0F0"."=";       //10
                $cadena .= "#FFFFFF"."=";       //11
            }else{  //PENDIENTE
                $cadena .= "#ffffff"."=";       //10
                $cadena .= "#000000"."=";       //11
            }

            $cadena .= "Tel 1. ".$row->telefono_principal ."<br>Tel 2. ".$row->telefono_secundario."=" ;        //12

            if ($row->acepta_cliente != "No") {
                if ($row->estado_refacciones == "0") {
                    $cadena .= "background-color:#f5acaa;"."=";  // 13
                    $cadena .= "SIN SOLICITAR"."=";  // 14
                } elseif ($row->estado_refacciones == "1") {
                    $cadena .= "background-color:#f5ff51;"."=";  // 13
                    $cadena .= "SOLICIATADAS"."=";  // 14
                } elseif ($row->estado_refacciones == "2") {
                    $cadena .= "background-color:#5bc0de;"."=";  // 13
                    $cadena .= "RECIBIDAS"."=";  // 14
                }else{
                    $cadena .= "background-color:#c7ecc7;"."=";  // 13
                    $cadena .= "ENTREGADAS"."=";  // 14
                }
            } else {
                $cadena .= "background-color:red;color:white;"."=";  // 13
                $cadena .= "SIN SOLICITAR"."=";  // 14
            }

            $cadena .= "|";
        }


        echo $cadena;
    }

    //Recuperamos la informacion del registro
    public function cargaInfo()
    {
        $idRegistro = $_POST['idRegistro'];
        $cadena = "";

        $revision = $this->mConsultas->get_result("id",$idRegistro,"refacciones_cliente");

        foreach ($revision as $row) {            
            $cadena .= $row->id_cita."|" ;
            $cadena .= $row->folio."|" ;
            $cadena .= strtoupper($row->estatus)."|";

            //Clasificamos el color a mostrar por el estaus
            if (strtoupper($row->estatus) == "DISP INSTALAR") {
                $cadena .= "#92D050|";
                $cadena .= "#FFFFFF|";
            }elseif (strtoupper($row->estatus) == "NO CONTESTA") {
                $cadena .= "#FF0000|";
                $cadena .= "#000000|";
            }elseif (strtoupper($row->estatus) == "RESERVADO") {
                $cadena .= "#7030A0|";
                $cadena .= "#FFFFFF|";
            }elseif (strtoupper($row->estatus) == "NO IDENTIFICADO") {
                $cadena .= "#FFFF00|";
                $cadena .= "#000000|";
            }elseif (strtoupper($row->estatus) == "POR RESERVAR") {
                $cadena .= "#00B0F0|";
                $cadena .= "#FFFFFF|";
            }else{  //PENDIENTE
                $cadena .= "#ffffff|";
                $cadena .= "#000000|";
            }
            
            $cadena .= $row->cantidad."|" ;
            $cadena .= $row->descripcionCorta."|" ;
        }

        echo $cadena;
    }

    //Guardamos el id que se da de baja
    public function guardarBaja()
    {
        $registro = date("Y-m-d H:i:s");

        $idRegistro = $_POST['idRegistro'];
        $motivo = $_POST['motivo'];

        $contenedor = array(
            'baja' => 1,
            'motivoBaja' => $motivo,
            'fecha_baja' => $registro,
        );

        $actualizar = $this->mConsultas->update_table_row('refacciones_cliente',$contenedor,'id',$idRegistro);

        if ($actualizar) {
            $result = "OK";
        }else{
            $result = "ERROR";
        }

        echo $result;
    }

    //Recuperamos el historial de cambios realizados a la orden
    public function historial_cambios()
    {
        $id_cita = $_POST['id_cita'];

        $cadena = "";

        $revision = $this->mConsultas->get_result("id_cita",$id_cita,"estatus_cliente_historico");

        foreach ($revision as $row) {
            $cadena .= $row->cambio_num."=";

            $fecha = explode(" ",$row->fecha_fin);
            $fecha_reg = explode("-",$fecha[0]);
            $cadena .= $fecha_reg[2]."/".$fecha_reg[1]."/".$fecha_reg[0]." ".$fecha[1]."=";

            $cadena .= $row->estatus_actual."=";
            $cadena .= $row->motivo."=";

            //Clasificamos el color a mostrar por el estaus
            if (strtoupper($row->estatus_actual) == "DISP INSTALAR") {
                $cadena .= "#92D050"."=";
                $cadena .= "#FFFFFF"."=";
            }elseif (strtoupper($row->estatus_actual) == "NO CONTESTA") {
                $cadena .= "#FF0000"."=";
                $cadena .= "#000000"."=";
            }elseif (strtoupper($row->estatus_actual) == "RESERVADO") {
                $cadena .= "#7030A0"."=";
                $cadena .= "#FFFFFF"."=";
            }elseif (strtoupper($row->estatus_actual) == "NO IDENTIFICADO") {
                $cadena .= "#FFFF00"."=";
                $cadena .= "#000000"."=";
            }elseif (strtoupper($row->estatus_actual) == "POR RESERVAR") {
                $cadena .= "#00B0F0"."=";
                $cadena .= "#FFFFFF"."=";
            }else{  //PENDIENTE
                $cadena .= "#ffffff"."=";
                $cadena .= "#000000"."=";
            }

            $cadena .= "|";
        }

        echo $cadena;
    }

    public function recuperar_spni_cliente()
    {
        //Recibimos los datos
        $_POST = json_decode(file_get_contents('php://input'), true);

        //Creamos contenedor
        $contenido = [];
        $contenedor = [];

        //Evitamos posibles fallos al momento de ejecutar las consultas
        try {
            $serie = $_POST["serie"];
            $id_cita_serie = $this->mConsultas->id_cita_spni_clientes($serie);

            $revision = $this->mConsultas->get_result("id_cita",$id_cita_serie,"refacciones_cliente");
            foreach ($revision as $row) {
                $cantidad = explode("_",$row->cantidad) ;
                $descripcionCorta = explode("_",$row->descripcionCorta) ;
                $numParte = explode("_",$row->noParte) ;
                //$id_refaccion = explode("_",$row->id_refaccion) ;
            }
            if (isset($cantidad)) {
                for ($i=0; $i < count($cantidad) ; $i++) { 
                    $contenido[] = $cantidad[$i].'||'.$descripcionCorta[$i].'||'.$numParte[$i];
                }
            }
        } catch (Exception $e) {
            
        }

        $contenedor[] = array(
            'Contenido' => $contenido
        );

        $respuesta = json_encode($contenedor);
        echo $respuesta;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {        
        $this->load->view("", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrasVistas/buscadorVarios.js',
                'assets/js/otrosModulos/nspi/busqueda.js',
                'assets/js/otrosModulos/nspi/lista_delete.js',
                'assets/js/otrosModulos/nspi/envio_historico_clientes.js',
                'assets/js/superUsuario.js'
            )
        );

        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Historial Cliente - Refacciones"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}