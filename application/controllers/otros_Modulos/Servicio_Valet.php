<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicio_Valet extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'original', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        $this->load->model('MGraficas', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);
    }

    public function index($placas = "",$datos = NULL)
    {
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $dias = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];

        $mes_d = date("m");
        $datos["mes_solicitud"] = $meses[$mes_d - 1];

        $this->loadAllView($datos,'otros_Modulos/servicio_valet/alta');
    }

    //Recibimos la informacion para guardarla
    public function recepcion_formulario()
    {
        $tipo_formulario = $_POST["tipo_formulario"];
        $registro = date("Y-m-d H:i:s");

        $firma_cliente = ((isset($_POST["rutaFirmaConsumidor"])) ? $_POST["rutaFirmaConsumidor"] : "");

        $archivos = $this->validateDoc("archivos");

        //Si el registro se va a guardar por primera vez
        if ($tipo_formulario == "1") {
            //Si es un registro nuevo, Verificamos que no exista previamente el registro
            //$revision = $this->original->get_result("id_cita",$_POST["id_cita"],"servicio_valet");

            //foreach ($revision as $row) {
                //$valida = $row->id;
            //}

            //Si no existe el registro, lo guardamos
            //if (!isset($valida)) {
                $contenedor = array(
                    "id" => NULL,
                    "id_cita" => ((isset($_POST["id_cita"])) ? $_POST["id_cita"] : ""),
                    "fecha_solicitud" => ((isset($_POST["fecha"])) ? $_POST["fecha"] : date("Y-m-d")),
                    "placas" => ((isset($_POST["placas"])) ? $_POST["placas"] : ""),
                    "modelo" => ((isset($_POST["modelo"])) ? $_POST["modelo"] : ""),
                    "serie" => ((isset($_POST["serie"])) ? $_POST["serie"] : ""),
                    "color" => ((isset($_POST["color"])) ? $_POST["color"] : ""),
                    "marca" => ((isset($_POST["marca"])) ? $_POST["marca"] : ""),

                    "aseguradora" => ((isset($_POST["aseguradora"])) ? $_POST["aseguradora"] : ""),
                    "num_poliza" => ((isset($_POST["poliza"])) ? $_POST["poliza"] : ""),
                    "contratante" => ((isset($_POST["contratante"])) ? $_POST["contratante"] : ""),
                    "fecha_vencimiento" => ((isset($_POST["fecha_vencimiento"])) ? $_POST["fecha_vencimiento"] : ""),

                    "correo" => ((isset($_POST["correo"])) ? $_POST["correo"] : ""),
                    "telefono" => ((isset($_POST["whatsp"])) ? $_POST["whatsp"] : ""),

                    "cliente" => ((isset($_POST["nombre_cliente"])) ? $_POST["nombre_cliente"] : ""),
                    "firma_cliente" => $this->base64ToImage($firma_cliente,"firmas"),
                    "archivos" => $archivos,
                    "fecha_alta" => $registro,
                    "fecha_actualizacion" => $registro,
                );

                //Guardamos los datos
                $id_retorno = $this->original->save_register('servicio_valet',$contenedor);
                if ($id_retorno != 0) {
                    //Actualizamos la información para el historial de ordenes
                    $this->actualiza_documentacion($contenedor["id_cita"]);

                    $respuesta = "OK_".$_POST["id_cita"];
                } else {
                    $respuesta = "ERROR_NS";
                }
            //Si existe, mandamos un mensaje
            //} else {
                //$respuesta = "ESTE NÚMERO DE ORDEN YA TIENE UN FORMULARIO INICIADO_NS";
            //}
        
        //De lo contrario, es una actualizavion
        }else{
            $contenedor = array(
                "placas" => ((isset($_POST["placas"])) ? $_POST["placas"] : ""),
                "modelo" => ((isset($_POST["modelo"])) ? $_POST["modelo"] : ""),
                "serie" => ((isset($_POST["serie"])) ? $_POST["serie"] : ""),
                "color" => ((isset($_POST["color"])) ? $_POST["color"] : ""),
                "marca" => ((isset($_POST["marca"])) ? $_POST["marca"] : ""),

                "aseguradora" => ((isset($_POST["aseguradora"])) ? $_POST["aseguradora"] : ""),
                "num_poliza" => ((isset($_POST["poliza"])) ? $_POST["poliza"] : ""),
                "contratante" => ((isset($_POST["contratante"])) ? $_POST["contratante"] : ""),
                "fecha_vencimiento" => ((isset($_POST["fecha_vencimiento"])) ? $_POST["fecha_vencimiento"] : ""),

                "correo" => ((isset($_POST["correo"])) ? $_POST["correo"] : ""),
                "telefono" => ((isset($_POST["whatsp"])) ? $_POST["whatsp"] : ""),

                "cliente" => ((isset($_POST["nombre_cliente"])) ? $_POST["nombre_cliente"] : ""),
                "firma_cliente" => $this->base64ToImage($firma_cliente,"firmas"),
                "fecha_actualizacion" => $registro,
            );
            
            //Actualizamos el registro de la refaccion
            $actualizar = $this->original->update_table_row('servicio_valet',$contenedor,'id_cita',$_POST["id_cita"]);
            if ($actualizar) {
                //Si se actualizo correctamente
                $respuesta = "OK_".$_POST["id_cita"];
            } else {
                $respuesta = "ERROR_NS";
            }
        }
        
        echo $respuesta;
    }

    //Actualizamos el registro para el listado de la documentacion de ordenes
    public function actualiza_documentacion($id_cita = '')
    {
        //Verificamos que no exista ese numero de orden registrosdo para alta
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"documentacion_ordenes");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Verificamos que la informacion se haya cargado
            $contededor = array(
                "servicio_valet" => "1"
            );

            //Guardamos el registro
            $actualiza = $this->mConsultas->update_table_row('documentacion_ordenes',$contededor,'id_cita',$id_cita);
        }
    }

    //Comodin para abrir pdf informacion del transpaso
    public function comodinPdf($id_cita = '')
    {
        $datos["destino"] = "PDF";
        $datos["tipo"] = "VISUAL";
        $this->leer_registro($this->decrypt($id_cita),$datos);
    }

    //Recuperamos los datos del formulario
    public function leer_registro($id_cita = '',$datos = NULL)
    {
        if (!isset($datos["destino"])) {
            $datos["destino"] = "";
        }

        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $dias = ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];

        $datos["id_cita"] = $id_cita;

        $revision = $this->original->get_result("id_cita",$id_cita,"servicio_valet");

        foreach ($revision as $row) {
            $fecha = explode("-",$row->fecha_solicitud);
            $datos["mes_solicitud"] = $meses[(int)$fecha[1] - 1];
            $datos["dia_solicitud"] = $fecha[2];
            $datos["anio_solicitud"] = $fecha[0];
            //$datos["fecha_solicitud"] = $row->fecha_solicitud;
            
            $fecha  = new DateTime($row->fecha_vencimiento);
            $datos["fecha_vencimiento"] = $fecha->format('d/m/Y');
            //$datos["fecha_vencimiento"] = $row->fecha_vencimiento;
            
            $datos["placas"] = $row->placas;
            $datos["modelo"] = $row->modelo;
            $datos["serie"] = $row->serie;
            $datos["color"] = $row->color;
            $datos["marca"] = $row->marca;
            $datos["aseguradora"] = $row->aseguradora;
            $datos["poliza"] = $row->num_poliza;
            $datos["contratante"] = $row->contratante;
            $datos["correo"] = $row->correo;
            $datos["whatsp"] = $row->telefono;
            $datos["nombre_cliente"] = $row->cliente;
            $datos["firma_cliente"] = $row->firma_cliente;
        }

        if ($datos["destino"] == "PDF") {
            $this->load->view("otros_Modulos/servicio_valet/plantillaPDF",$datos);
        } else {
            #$this->loadAllView($datos,'otros_Modulos/servicio_valet/editar');
            echo "Error";
        }
    }

    //Cargamos las ordenes por asignar
    public function porAsignar($x = '',$datos = NULL)
    {
        $revision = $this->original->valet_por_asignar("0");
        foreach ($revision as $row) {
            $datos["idRegistro"][] = $row->id;
            $datos["cliente"][] = $row->cliente;

            $fecha  = new DateTime($row->fecha_solicitud);
            $datos["fecha_solicitud"][] = $fecha->format('d/m/Y');

            $datos["placas"][] = $row->placas;
            $datos["serie"][] = $row->serie;
            $datos["modelo"][] = $row->modelo;
        }

        $this->loadAllView($datos,'otros_Modulos/servicio_valet/lista_asignar');
    }

    public function cargarLista($x = '',$datos = NULL)
    {
        $revision = $this->original->valet_asignardas("0");
        foreach ($revision as $row) {
            $datos["idRegistro"][] = $row->id;
            $datos["cliente"][] = $row->cliente;
            $datos["id_cita"][] = $row->id_cita;
            $datos["id_cita_url"][] = $this->encrypt($row->id_cita);

            $fecha  = new DateTime($row->fecha_solicitud);
            $datos["fecha_solicitud"][] = $fecha->format('d/m/Y');

            $datos["placas"][] = $row->placas;
            $datos["serie"][] = $row->serie;
            $datos["modelo"][] = $row->modelo;

            $datos["evidencias"][] = explode("|",$row->archivos);
        }

        $this->loadAllView($datos,'otros_Modulos/servicio_valet/lista');
    }

    //Validamos la existencia del registro
    public function revision_registro()
    {
        $id_cita = $_POST["cita_ingresada"];
        $respuesta = 0;

        //Verificamos si ya existe un registro
        $revision = $this->original->get_result("id_cita",$id_cita,"servicio_valet");
        foreach ($revision as $row) {
            $valida = $row->id;
        }

        //Si existe el registro
        if (isset($valida)) {
            $respuesta = $this->encrypt($id_cita);
        } else {
            $respuesta = 0;
        }

        echo $respuesta;
    }

    //Cargamos la informcaion en el formulario
    public function carga_datos()
    {
        //Recuperamos el id de la orden
        $serie = $_POST['serie']; 
        $placas = $_POST['placas'];

        $cadena = "";

        //Primero buscar informacion de la tabla ordenes de servicio y si no esta buscar en magic
        //Buscamos informacion del cliente en nuestro historial de ordenes de servicio
        $consulta_1 = $this->original->buscar_orden($serie,$placas); 
        //echo $consulta_1;
        //Si encuentra un numero de orden, jalamos la informacion
        if ($consulta_1 != "0") {
            $precarga = $this->original->precargaConsulta($consulta_1);
            foreach ($precarga as $row){
                $cadena .= str_replace("|", "*",$row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno)."=";
                $cadena .= $row->vehiculo_marca."=";
                $cadena .= $row->vehiculo_modelo."=";

                $serie = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
                $cadena .= $serie."=";
                $cadena .= $row->vehiculo_placas."=";

                //Recuperamos el color
                $queryColor  = $this->original->get_result('id',$row->id_color,'cat_colores');
                foreach ($queryColor as $row_2) {
                  $color = $row_2->color;
                }

                $cadena .= ((isset($color)) ? $color : "")."=";

                $cadena .= $row->datos_email."=";
                $cadena .= $row->correo_compania."=";
                $cadena .= $row->telefono_movil."=";
                $cadena .= $row->otro_telefono."=";
                //$cadena .= $row->vehiculo_anio."=";
                //$cadena .= $row->subcategoria."=";
                //$cadena .= $row->tecnico."=";
                //$cadena .= $row->asesor."=";
            }
        //De lo contrario buscamos en magic
        }else{
            $consulta_2 = $this->original->buscar_magic($serie,$placas);

            if ($consulta_2 != "0") {
                $precarga = $this->original->get_result("id",$consulta_2,"magic");

                foreach ($precarga as $row){
                    $cadena .= str_replace("|", "*",$row->nombre." ".$row->ap." ".$row->am)."=";
                    $cadena .= $row->marca."=";
                    $cadena .= $row->modelo."=";

                    $cadena .= $row->serie_larga."=";
                    $cadena .= $row->placas."=";

                    $cadena .= "=";

                    $cadena .= $row->correo."=";
                    $cadena .= "=";
                    $cadena .= $row->tel_principal."=";
                    $cadena .= $row->tel_secundario."=";
                    //$cadena .= $row->vehiculo_anio."=";
                    //$cadena .= $row->subcategoria."=";
                    //$cadena .= $row->tecnico."=";
                    //$cadena .= $row->asesor."=";
                }
            }else{
                $cadena = "No se encontro información para cargar";
            }
        }

        echo $cadena;
    }

    //Buscamos un registro, sin asignar
    public function buscar_registro()
    {
        $campo = $_POST['campo']; 
        $respuesta = "";

        $consulta = $this->original->buscar_valetsorden($campo);

        foreach ($consulta as $row){
            //Pra confirmar que solo aparezcan servicios sin asignar
            if ($row->id_cita == "0") {
                $respuesta .= $row->id."=";
                $respuesta .= $row->cliente."=";

                $fecha  = new DateTime($row->fecha_solicitud);
                $respuesta .= $fecha->format('d/m/Y')."=";
                $respuesta .= $row->placas."=";
                $respuesta .= $row->serie."=";
                $respuesta .= $row->modelo."=";

                $respuesta .= "|";
            }
        }

        echo $respuesta;
    }

    //Buscamos un registro, sin asignar
    public function buscar_registro_2()
    {
        $campo = $_POST['campo']; 
        $respuesta = "";

        $consulta = $this->original->buscar_valet($campo);

        foreach ($consulta as $row){
            //Pra confirmar que solo aparezcan servicios asignadas
            if ($row->id_cita != "0") {
                $respuesta .= $row->id."=";
                $respuesta .= $row->cliente."=";

                $fecha  = new DateTime($row->fecha_solicitud);
                $respuesta .= $fecha->format('d/m/Y')."=";
                $respuesta .= $row->placas."=";
                $respuesta .= $row->serie."=";
                $respuesta .= $row->modelo."=";

                $respuesta .= $row->id_cita."=";
                $respuesta .= $row->archivos."=";

                $respuesta .= $this->encrypt($row->id_cita)."=";

                $respuesta .= "~";
            }
        }

        echo $respuesta;
    }

    //Buscamos un registro, sin asignar
    public function consulta_datos()
    {
        $registro = $_POST['registro']; 
        $respuesta = "";

        $revision = $this->original->get_result("id",$registro,"servicio_valet");

        foreach ($revision as $row){
            //Pra confirmar que solo aparezcan servicios sin asignar
            if ($row->id_cita == "0") {
                $respuesta .= $row->cliente."=";
                $fecha  = new DateTime($row->fecha_solicitud);
                $respuesta .= $fecha->format('d/m/Y')."=";
                $respuesta .= $row->modelo."=";
                $respuesta .= $row->placas."=";
                $respuesta .= $row->serie."=";
            }
        }

        echo $respuesta;
    }

    //Validamos y confirmamos la asignacion del registro
    public function asignar_orden()
    {
        $id_registro = $_POST["registro"];
        $id_cita = $_POST["id_cita"];

        $respuesta = "";

        //Verificamos si ya existe un registro
        $revision = $this->original->get_result("id_cita",$id_cita,"servicio_valet");
        foreach ($revision as $row) {
            $valida = $row->id;
        }

        //Si existe el registro, esa orden ya fue asignada
        if (isset($valida)) {
            $respuesta = "ERROR_YA EXISTE UN REGISTRO ASIGNADO A ESE NÚMERO DE ORDEN";
        //de lo contrario, podemos asignarla
        } else {
            $registro = date("Y-m-d H:i:s");

            $contenedor = array(
                "id_cita" => $id_cita,
                "fecha_actualizacion" => $registro,
            );
            //Actualizamos el registro de la refaccion
            $actualizar = $this->original->update_table_row('servicio_valet',$contenedor,'id',$id_registro);
            if ($actualizar) {
                $respuesta = "OK_REGISTRO ASIGNADO CORRECTAMENTE";
            } else {
                $respuesta = "ERROR_NO SE PUDO ASIGNAR EL REGISTRO";
            }
        }

        echo $respuesta;
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_SV";
        for($i=0; $i<=6; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        $urlDoc = 'assets/imgs/'.$direcctorio;
        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($urlDoc)){
            mkdir($urlDoc, 647, true);
        }

        $data = explode(',', $imgBase64);
        //Comprobamos que se corte bien la cadena
        if ($data[0] == "data:image/png;base64") {
            //Creamos la ruta donde se guardara la firma y su extensión
            if (strlen($imgBase64)>1) {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            if (substr($imgBase64,0,6) == "assets" ) {
                $base = $imgBase64;
            } else {
                $base = "";
            }

        }

        return $base;
    }

    //Validamos y guardamos los documentos
    public function validateDoc($nombreDoc = NULL)
    {
        $path = "";
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);

        //Como el elemento es un arreglos utilizamos foreach para extraer todos los valores
           for ($i = 0; $i < count($_FILES[$nombreDoc]['tmp_name']); $i++) {
            $_FILES['tempFile']['name'] = $_FILES[$nombreDoc]['name'][$i];
            $_FILES['tempFile']['type'] = $_FILES[$nombreDoc]['type'][$i];
            $_FILES['tempFile']['tmp_name'] = $_FILES[$nombreDoc]['tmp_name'][$i];
            $_FILES['tempFile']['error'] = $_FILES[$nombreDoc]['error'][$i];
            $_FILES['tempFile']['size'] = $_FILES[$nombreDoc]['size'][$i];

            //Url donde se guardara la imagen
            $urlDoc ="assets/imgs/docValet";
            //Generamos un nombre random
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_";
            for($j=0; $j<=7; $j++ ){
               $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
               mkdir($urlDoc, 0647, true);
            }

            //Configuramos las propiedades permitidas para la imagen
            $config['upload_path'] = $urlDoc;
            $config['file_name'] = $urlNombre;
            $config['allowed_types'] = "*";

            //Cargamos la libreria y la inicializamos
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('tempFile')) {
                 $data['uploadError'] = $this->upload->display_errors();
                 // echo $this->upload->display_errors();
                 $path .= "";
            }else{
                 //Si se pudo cargar la imagen la guardamos
                 $fileData = $this->upload->data();
                 $ext = explode(".",$_FILES['tempFile']['name']);
                 $path .= $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
                 $path .= "|";
            }
        }

        return $path;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_HM.js',
                'assets/js/otrosModulos/consulta/consulta.js',
                'assets/js/otrosModulos/consulta/enviar_formulario.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Servicio Valet"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}