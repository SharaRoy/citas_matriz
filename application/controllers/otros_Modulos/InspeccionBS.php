<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Spipu\Html2Pdf\Html2Pdf;

class InspeccionBS extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);
    }

    public function index($idOrden = 0,$datos = NULL)
    {
        $datos["hoy"] = date("Y-m-d");
        $datos["hora"] = date("H:i");
        
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $datos["idOrden"] = $id_cita;
        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($id_cita);

        //Verificamos si existe el registro en la BD
        $query = $this->Verificacion->get_result("proyectoId",$id_cita,"diagnostico_bodyshop");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $this->setData($id_cita,$datos);
        //Si no existe el dato, entonces no existe el registro
        }else {
            //Recuperamos el proximo id disponible de la tabla BodyShop
            $datos["idOrden"] = $this->Verificacion->getLastId() + 1;

            // Recuperamos el nombre del asesor logeado
            if ($this->session->userdata('idUsuario')) {
                //Verificamos que el usuario logeado sea un asesor
                $asesor = $this->session->userdata('usuario');
                if (($asesor == "LUISESTRADA")||($asesor == "pacheco")) {
                    $datos["asesor"] = mb_strtoupper($this->session->userdata('nombreUsuario'));
                }else {
                    $datos["asesor"] = "";
                }
            }else {
                $datos["asesor"] = "";
            }

            //Cargamos los colores del catalogo
            $query2 = $this->Verificacion->get_table("colores");
            foreach ($query2 as $row){
                $datos["idColor"][] = $row->id;
                $datos["colorLista"][] = $row->color;
            }

            $this->loadAllView($datos,'otros_Modulos/inspeccion_bodyshop/inspeccion_alta');
        }

    }

    //Validamos campor obligarotios
    public function validateView()
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);
        // ini_set('max_input_time',"20M");

        $datos["tipoRegistro"] = $this->input->post("cargaFormulario");
        $datos["idOrden"] = $this->input->post("idOrden");

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($datos["idOrden"]);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        if ($datos["tipoRegistro"] == "1") {
            $this->form_validation->set_rules('idOrden', 'La orden de servicio es requerida.', 'required|callback_existencia');
            //Firmas
            $this->form_validation->set_rules('rutaFirmaAsesor', 'Firma requerida', 'required');
            $this->form_validation->set_rules('nombreAsesor', 'Nombre requerido', 'required');
            $this->form_validation->set_rules('rutaFirmaAcepta', 'Firma requerida', 'required');
            $this->form_validation->set_rules('consumidor1Nombre', 'Nombre requerido', 'required');
        }else{
            $this->form_validation->set_rules('idOrden', 'La orden de servicio es requerida.', 'required');
        }

        //Datos del cliente y de los vehiculo
        $this->form_validation->set_rules('placas', 'Campo requerido', 'required');
        $this->form_validation->set_rules('kilometraje', 'Campo requerido', 'required');
        $this->form_validation->set_rules('idOrden', 'Campo requerido', 'required');
        $this->form_validation->set_rules('serieVehiculo', 'Campo requerido', 'required');
        $this->form_validation->set_rules('colorVehiculo', 'Campo requerido', 'required');
        $this->form_validation->set_rules('torreVehiculo', 'Campo requerido', 'required');
        $this->form_validation->set_rules('tipoMotor', 'Campo requerido', 'required');
        $this->form_validation->set_rules('tipoTrans', 'Campo requerido', 'required');
        $this->form_validation->set_rules('nombreCliente', 'Campo requerido', 'required');
        $this->form_validation->set_rules('fechaRegistro', 'Campo requerido', 'required');
        $this->form_validation->set_rules('correoCliente', '', 'valid_email');
        // $this->form_validation->set_rules('telefonoCliente', 'Campo requerido', 'required');
        $this->form_validation->set_rules('vin', 'Campo requerido', 'required');
        //Revision por cheks
        // $this->form_validation->set_rules('interiores', '<br>Las Características interiores son requeridas.', 'callback_interiores');
        // $this->form_validation->set_rules('cajuela', '<br>Falta opciones.', 'callback_cajuela');
        // $this->form_validation->set_rules('cofre', '<br>Falta opciones.', 'callback_cofre');
        // $this->form_validation->set_rules('inferior', '<br>Falta opciones.', 'callback_inferior');
        $this->form_validation->set_rules('articulos', 'Falta indicar.', 'required');
        // $this->form_validation->set_rules('exteriores', 'Falta indicar.', 'required');
        //Niveles de gasolina
        $this->form_validation->set_rules('nivelGasolina_1', 'Campo requerido', 'required');
        $this->form_validation->set_rules('nivelGasolina_2', 'Campo requerido', 'required');
        //daños en carroceria
        // $this->form_validation->set_rules('danos', 'Campo requerido', 'required');
        // $this->form_validation->set_rules('marcasRadio', 'Campo requerido', 'required');
        //$this->form_validation->set_rules('danosMarcas', 'Campo requerido', 'required'); //validar con el input validaClick_1
        //Sistema de frenos
        // $this->form_validation->set_rules('sisFrenos', 'Campo requerido', 'callback_frenos');
        //Otros
        // $this->form_validation->set_rules('observaciones', 'Campo requerido', 'required');
        $this->form_validation->set_rules('articulos', 'Campo requerido', 'required');
        // $this->form_validation->set_rules('cualesArticulos', 'Campo requerido', 'required');
        // Parte de voz cliente, parte 1
        // $this->form_validation->set_rules('falla', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('fallasImg', 'Campo requerido', 'required'); //validar con el input validaClick_2
        // $this->form_validation->set_rules('sentidos', 'Campo requerido', 'required');
        // $this->form_validation->set_rules('fallasCheck', 'Campo requerido', 'required');
        // $this->form_validation->set_rules('fallaPercibe', 'Campo requerido', 'required');
        //Medidores de condición
       // $this->form_validation->set_rules('tempAmbiente', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('humedad', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('viento', 'Campo requerido', 'required');
       //
       // $this->form_validation->set_rules('frenoMedidor', 'Campo requerido', 'required');
       // // $this->form_validation->set_rules('transmision', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('cambio', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('cambio_A', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('rpm', 'Campo requerido', 'required');
       // // $this->form_validation->set_rules('remolque', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('carga', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('pasajeros', 'Campo requerido', 'required');
       // // $this->form_validation->set_rules('cajuela_2', 'Campo requerido', 'required');
       //
       // $this->form_validation->set_rules('estructura', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('camino', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('pendiente', 'Campo requerido', 'required');
       // $this->form_validation->set_rules('superficie', 'Campo requerido', 'required');
       //  // $this->form_validation->set_rules('observaciones2', 'Campo requerido', 'required');

        //Mensajes de error
        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('inferior','Faltan características.');
        $this->form_validation->set_message('frenos','Faltan características.');
        $this->form_validation->set_message('interiores','Faltan características.');
        $this->form_validation->set_message('cofre','Faltan características del cofre.');
        $this->form_validation->set_message('cajuela','Faltan características de cajuela.');
        $this->form_validation->set_message('valid_email','Proporcione un correo electrónico valido');
        $this->form_validation->set_message('existencia','<br>El No. de Orden ya existe. Favor de verificar.');

        //Si pasa las validaciones enviamos la información al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";
            // var_dump(form_error());
            // var_dump($this->form_validation->run());
            $this->index($datos["idOrden"],$datos);
        }

    }

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("idOrden");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->Verificacion->get_result("proyectoId",$idOrden,"diagnostico_bodyshop");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    //Validar requisitos minimos P1
    public function interiores()
    {
        $return = FALSE;
        if ($this->input->post("interiores")) {
            if (count($this->input->post("interiores")) >10){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar requisitos minimos para checks de cajuela
    public function cajuela()
    {
        $return = FALSE;
        if ($this->input->post("cajuela")) {
            if (count($this->input->post("cajuela")) >3){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar requisitos minimos para cheks de cofre
    public function cofre()
    {
        $return = FALSE;
        if ($this->input->post("cofre")) {
            if (count($this->input->post("cofre")) >3){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar requisitos minimos para cheks de parte inferior
    public function inferior()
    {
        $return = FALSE;
        if ($this->input->post("inferior")) {
            if (count($this->input->post("inferior")) >3){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Validar requisitos minimos para cheks del sistema de frenos
    public function frenos()
    {
        $return = FALSE;
        if ($this->input->post("sisFrenos")) {
            if (count($this->input->post("sisFrenos")) >3){
                $return = TRUE;
            }
        }
        return $return;
    }

    //Recuperamos la informacion del formulario
    public function getDataForm($datos = NULL)
    {

        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);
        // ini_set('max_input_time',"20M");

        $registro = date("Y-m-d H:i:s");
        //Si es un nuevo registro
        if ($datos["tipoRegistro"] == "1") {
            $dataForm = array(
                "idRegistro" => NULL,
                "proyectoId" => $this->input->post("idOrden"),
            );
        }

        //Datos de cabecera
        $dataForm["nombreCliente"] = $this->input->post("nombreCliente");
        $dataForm["correo"] = $this->input->post("correoCliente");
        $dataForm["contacto"] = $this->input->post("contacto");
        $dataForm["telCelular"] = $this->input->post("celular");
        $dataForm["telefono"] = $this->input->post("telefonoCliente");
        $dataForm["vehiculo"] = $this->input->post("vin");
        $dataForm["placas"] = $this->input->post("placas");
        $dataForm["kilometraje"] = $this->input->post("kilometraje");
        $dataForm["serie"] = $this->input->post("serieVehiculo");

        $idColor = $this->input->post("colorVehiculo");
        $dataForm["color"] = $idColor;

        $dataForm["torre"] = $this->input->post("torreVehiculo");
        $dataForm["tipoMotor"] = $this->input->post("tipoMotor");
        $dataForm["tipoTransmision"] = $this->validateRadio($this->input->post("tipoTrans"),"S/R");
        //Valores de tablas (indicadores) Parte - Diagnostico
        $dataForm["interiores"] = $this->validateCheck($this->input->post("interiores"),"S/R");
        $dataForm["cajuela"] = $this->validateCheck($this->input->post("cajuela"),"S/R");
        $dataForm["exteriores"] = $this->validateCheck($this->input->post("exteriores"),"S/R");
        $dataForm["cofre"] = $this->validateCheck($this->input->post("cofre"),"S/R");
        $dataForm["inferior"] = $this->validateCheck($this->input->post("inferior"),"S/R");
        $dataForm["sisFrenos"] = $this->validateCheck($this->input->post("sisFrenos"),"S/R");
        //Si es un nuevo registro
        if ($datos["tipoRegistro"] == "1") {
            $dataForm["nivelGasolina"] = $this->input->post("nivelGasolina_1")."/".$this->input->post("nivelGasolina_2");
            $dataForm["imgDanios"] = $this->base64ToImage($this->input->post("danosMarcas"),"assets/imgs/diagramas","png");
            $dataForm["danios"] = $this->validateRadio($this->input->post("danos"),"0");
        }
        //Otros datos
        $dataForm["observacion_1"] = $this->input->post("observaciones");
        $dataForm["articulos"] = $this->validateRadio($this->input->post("articulos"),"0");
        $dataForm["cualesArticulos"] = $this->input->post("cualesArticulos");

        //Parte - Voz Cliente
        $dataForm["definicionFalla"] = $this->input->post("falla");
        $dataForm["sentidosFalla"] = $this->validateCheck($this->input->post("sentidos"),"S/R");
        //Si es un nuevo registro
        if ($datos["tipoRegistro"] == "1") {
            $dataForm["imgFallas"] = $this->base64ToImage($this->input->post("fallasImg"),"assets/imgs/diagramas","png");
        }
        //Indicadores de fallas (cheks)
        $dataForm["presentaFalla"] = $this->validateCheck($this->input->post("fallasCheck"),"S/R");
        $dataForm["percibeFalla"] = $this->validateCheck($this->input->post("fallaPercibe"),"S/R");
        //Medidores de condiciones (progressbar)
        $dataForm["temperatura"] = $this->input->post("tempAmbiente");
        $dataForm["humedad"] = $this->input->post("humedad");
        $dataForm["viento"] = $this->input->post("viento");
        $dataForm["velocidad"] = $this->input->post("frenoMedidor");
        $dataForm["cambioTransmision"] = $this->input->post("cambio");
        $dataForm["transmision"] = $this->validateRadio($this->input->post("transmision"),"S/R");
        $dataForm["cambioOperativa"] = $this->input->post("cambio_A");
        $dataForm["rmp"] = $this->input->post("rpm");
        $dataForm["carga"] = $this->input->post("carga");
        $dataForm["remolque"] = $this->validateRadio($this->input->post("remolque"),"0");
        $dataForm["pasajeros"] = $this->input->post("pasajeros");
        $dataForm["cajuela_C"] = $this->input->post("cajuela_2");
        $dataForm["estructura"] = $this->input->post("estructura");
        $dataForm["camino"] = $this->input->post("camino");
        $dataForm["pendiente"] = $this->input->post("pendiente");
        $dataForm["superficie"] = $this->input->post("superficie");

        $dataForm["observacion_2"] = $this->input->post("observaciones2");

        //Si es un nuevo registro (firmas)
        if ($datos["tipoRegistro"] == "1") {
            //Aqui se guarda la pista de audio
            $cadena = $this->descomprimir();
            $dataForm["evidencia_audio"] = $this->base64ToImage($cadena,"assets/audios","ogg");
        }
        $dataForm["evidencia_video"] = $this->validateDoc("videos");
        if ($datos["tipoRegistro"] == "1") {
            $dataForm["nombreAsesor"] = $this->input->post("nombreAsesor");
            $dataForm["firmaAsesor"] = $this->base64ToImage($this->input->post("rutaFirmaAsesor"),"assets/imgs/firmas","png");
            $dataForm["firmaCliente"] = $this->base64ToImage($this->input->post("rutaFirmaAcepta"),"assets/imgs/firmas","png");
            $dataForm["fechaCreacion"] = $registro;
        }

        $dataForm["fechaActualiza"] = $registro;

        // ob_start("ob_gzhandler");

        //Guardamos/Actualizamos el registro
        if ($datos["tipoRegistro"] == "1") {
            try {
                //Fraccionamos el nombre
                $fraccion = explode(" ",$dataForm["nombreCliente"]);

                if (count($fraccion) == 3) {
                    $nombre = $fraccion[0];
                    $ap = $fraccion[1];
                    $am = $fraccion[2];
                }elseif (count($fraccion) == 2) {
                    $nombre = $fraccion[0];
                    $ap = $fraccion[1];
                    $am = "";
                }else {
                    $ap = $fraccion[count($fraccion)-2];
                    $am = $fraccion[count($fraccion)-1];
                    $nombre = "";
                    for ($i=0; $i <count($fraccion)-2 ; $i++) {
                        $nombre .= $fraccion[$i]." ";
                    }
                }

                //Manualmente ingresamos los id de los asesores
                if ($this->session->userdata('idUsuario')) {
                    $valida = $this->session->userdata('usuario');
                    if ($valida == "LUISESTRADA") {
                        //Asesor LUIS ESTRADA   (ID DE LA TABLA OPERADORES)
                        $asesor = 2;
                    }else {
                        //Asesor JORGE PACHECO   (ID DE LA TABLA OPERADORES)
                        $asesor = 3;
                    }
                }else {
                    //Asesor default (sin asesor default)
                    $asesor = 0;
                }

                //Guardamos la información en la base de datos de bodyshop
                $dataBodyshop = array(
                    "proyectoId" => $dataForm["proyectoId"],
                    "proyectoFechaCreacion" => $registro,
                    "vehiculo_placas" => $dataForm["placas"],
                    "id_color" => $idColor,
                    "vehiculo_numero_serie" => $dataForm["serie"],
                    "asesor" => $asesor,
                    "datos_email" => $dataForm["correo"],
                    "datos_nombres" => $nombre,
                    "datos_apellido_paterno" => $ap,
                    "datos_apellido_materno" => $am,
                    "datos_telefono" => $dataForm["telefono"],
                );

                $altaBodyShop = $this->Verificacion->save_register('bodyshop',$dataBodyshop);

                if ($altaBodyShop != 0) {
                    //Actualizamos el id que se relacionara
                    $dataForm["proyectoId"] = $altaBodyShop;
                    $alta = $this->Verificacion->save_register('diagnostico_bodyshop',$dataForm);

                    $datos["mensaje"] = "1";
                    // redirect(base_url().'IV_BodyShop_Lista/'.$datos["mensaje"]);
                }else {
                    $datos["mensaje"] = "2";
                    // $this->index($datos["idOrden"],$datos);
                }
                redirect(base_url().'IV_BodyShop_Lista/'.$datos["mensaje"]);
            } catch (\Exception $e) {
                $datos["mensaje"] = "2";
                $this->index($datos["idOrden"],$datos);
            }
        } else {
            $actualizar = $this->Verificacion->update_table_row('diagnostico_bodyshop',$dataForm,'proyectoId',$datos["idOrden"]);
            if ($actualizar) {
                //Guardamos el movimiento
                // $this->loadHistory($datos["idOrden"],"Actualización","Diagnostico Hibrido BodyShop");

                $datos["mensaje"]="1";
            }else {
                $datos["mensaje"]="2";
            }
            $this->index($datos["idOrden"],$datos);
        }
    }

    //Parada comodin para visualizar los registros si n poder hacer cambios
    public function listDataReading($y = 0)
    {
        $datos["modalidad"] = "Lista";
        $this->listData($y,$datos);
    }

    //Recuperamos los elementos de la tabla para listarlos
    public function listData($x = 4,$datos = NULL)
    {
        $datos["mensaje"] = $x;

        $query = $this->Verificacion->get_table("diagnostico_bodyshop");
        foreach ($query as $row){
            $datos["idRegistro"][] = $row->idRegistro;
            $datos["idOrden"][] = $row->proyectoId;

            $datos["asesor"][] = $row->nombreAsesor;
            $datos["placas"][] = $row->placas;
            $datos["vin"][] = $row->serie;
            $datos["telefono"][] = $row->telefono;
            $datos["nombreProp"][] = $row->nombreCliente;

            $fecha = explode(" ",$row->fechaCreacion);
            $fechaFracc = explode("-",$fecha[0]);
            $datos["fecha"][] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
        }

        $this->loadAllView($datos,'otros_Modulos/inspeccion_bodyshop/inspeccion_lista');
    }

    public function panelCerrarSesion($x = "",$datos = NULL)
    {
        if ($this->session->userdata('rolIniciado')) {
            $this->session->sess_destroy();
        }

        redirect('IV_BodyShop_Login/NV', 'refresh');
    }

    //Funcion para mostrar el panel principal para asesores
    public function panelAcceso($y = "",$datos = NULL)
    {
        //Validamos si existe la sesion activa
        if ($this->session->userdata('rolIniciado')) {
            $datos["sesionActiva"] = "1";
        }else {
            $datos["sesionActiva"] = "0";
        }

        $this->loadAllView($datos,'principales/otros/recepcionBS');
    }

    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->mConsultas->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    //Unimos las partes del audio en texto
    public function descomprimir()
    {
        $respuesta = "";
        for ($i=1; $i <9 ; $i++) {
            if ($this->input->post("blockAudio_".$i) != "") {
                $respuesta .= trim($this->input->post("blockAudio_".$i));
            }
        }
        // if ($cadena != NULL) {
        //     $respuesta = "";
        //     if (count($cadena) >0){
        //         //Separamos los index de los id de tipo de usuario
        //         for ($i=0; $i < count($cadena) ; $i++) {
        //             $respuesta .= trim($cadena[$i]);
        //         }
        //     }
        // }
        // echo $respuesta;
        return $respuesta;
    }

    //Validamos y guardamos los documentos
    public function validateDoc($direcctorio = "")
    {
        $path = "";
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);

        //Revisamos que la imagen se envio
        if ($_FILES["archivo"]['name'] != "") {
           $urlDoc = $direcctorio;
           //Generamos un nombre random
           $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
           $urlNombre = date("YmdHi")."_";
           for($j=0; $j<=8; $j++ ){
              $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
           }

            //Validamos que exista la carpeta destino   base_url()
            if(!file_exists($urlDoc)){
                mkdir($urlDoc, 0647, true);
            }

           //Configuramos las propiedades permitidas para la imagen
           $config['upload_path'] = $urlDoc;
           $config['file_name'] = $urlNombre;
           $config['allowed_types'] = "*";

           //Cargamos la libreria y la inicializamos
           $this->load->library('upload', $config);
           $this->upload->initialize($config);
           if (!$this->upload->do_upload('archivo')) {
                $data['uploadError'] = $this->upload->display_errors();
                // echo $this->upload->display_errors();
                $path .= "";
           }else{
                //Si se pudo cargar la imagen la guardamos
                $fileData = $this->upload->data();
                $ext = explode(".",$_FILES['archivo']['name']);
                $path = $urlDoc."/".$urlNombre.".".$ext[count($ext)-1];
           }
        }else{
            $path = "";
        }

        return $path;
    }

    //Comprobamos que al final se genere el pdf
    public function setDataPDF($idOrden = 0)
    {
        $datos["destinoVista"] = "PDF";
        $this->setData($idOrden,$datos);
    }

    //Recuperar informacion para cargar en la vista (edicion/PDF)
    public function setData($idOrden = 0,$datos = NULL)
    {
        $datos["idOrden"] = $idOrden;

        //Recuperamos el id interno de magic
        $datos["idIntelisis"] = $this->mConsultas->id_orden_intelisis($idOrden);
        if($datos["idIntelisis"] == ""){
            $datos["idIntelisis"] = "SIN FOLIO";
        }

        //Cargamos los colores del catalogo
        $query2 = $this->Verificacion->get_table("colores");
        foreach ($query2 as $row){
            $datos["idColor"][] = $row->id;
            $datos["colorLista"][] = $row->color;
        }

        //Verificamos si se imprimira en formulario o en pdf
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        //Cargamos la información del formulario
        $query = $this->Verificacion->get_result("proyectoId",$idOrden,"diagnostico_bodyshop");
        foreach ($query as $row) {
            //Valores generales
            $fecha = explode(" ",$row->fechaCreacion);

            //Recuperamos valores para formulario (Exclusivo)
            if ($datos["destinoVista"] == "Formulario") {
                $datos["fecha"] = $fecha[0];
                $datos["nivelGasolina"] = explode("/",$row->nivelGasolina);
                $datos["evidencia_audio"] = $row->evidencia_audio;
                $datos["evidencia_video"] = $row->evidencia_video;
            }else {
                $fechaFracc = explode("-",$fecha[0]);
                $datos["fecha"] = $fechaFracc[2]."/".$fechaFracc[1]."/".$fechaFracc[0];
                $datos["nivelGasolina"] = $row->nivelGasolina;
            }

            $datos["nombreCliente"] = $row->nombreCliente;
            $datos["correoCliente"] = $row->correo;
            $datos["telefonoCliente"] = $row->telefono;
            $datos["nombreContacto"] = $row->contacto;
            $datos["celular"] = $row->telCelular;
            $datos["vehiculo"] = $row->vehiculo;
            $datos["placas"] = $row->placas;
            $datos["kilometraje"] = $row->kilometraje;
            $datos["serie"] = $row->serie;
            $datos["color"] = $this->Verificacion->getColor($row->color);
            $datos["colorID"] = $row->color;
            $datos["torre"] = $row->torre;
            $datos["tipoMotor"] = $row->tipoMotor;
            $datos["tipoTransmision"] = $row->tipoTransmision;

            //Valores para diagnostico (OS)
            $datos["interiores"] = explode("|",$row->interiores);
            $datos["cajuela"] = explode("|",$row->cajuela);
            $datos["exteriores"] = explode("|",$row->exteriores);
            $datos["cofre"] = explode("|",$row->cofre);
            $datos["inferior"] = explode("|",$row->inferior);
            $datos["sisFrenos"] = explode("|",$row->sisFrenos);

            $datos["articulos"] = $row->articulos;
            $datos["danios"] = $row->danios;
            $datos["imgDanios"] = (file_exists($row->imgDanios)) ? $row->imgDanios : ""; //$row->imgDanios;
            $datos["cualesArticulos"] = $row->cualesArticulos;
            $datos["observaciones1"] = $row->observacion_1;

            // Valores para parte de voz cliente
            $datos["definicionFalla"] = $row->definicionFalla;
            $datos["imgFallas"] = (file_exists($row->imgFallas)) ? $row->imgFallas : ""; //$row->imgFallas;
            $datos["observaciones2"] = $row->observacion_2;

            //Recuperamos los valores que debemos tratar para imprimir
            $datos["sentidosFalla"] = explode("|", $row->sentidosFalla);
            $datos["presentaFalla"] = explode("|", $row->presentaFalla);
            $datos["percibeFalla"] = explode("|",$row->percibeFalla);

            //Recuperamos valores para formulario (Exclusivo)
            if ($datos["destinoVista"] == "Formulario") {
                $datos["logTempBarra"] = (int)$row->temperatura;

                $datos["humedad"] = $row->humedad;
                if ($datos["humedad"] == "Seco") {
                    $datos["logHumBarra"] = 20;
                }elseif ($datos["humedad"] == "Húmedo") {
                    $datos["logHumBarra"] = 40;
                }elseif ($datos["humedad"] == "Mojado") {
                    $datos["logHumBarra"] = 60;
                }elseif ($datos["humedad"] == "Lluvia") {
                    $datos["logHumBarra"] = 80;
                }elseif ($datos["humedad"] == "Hielo") {
                    $datos["logHumBarra"] = 100;
                }else {
                    $datos["logHumBarra"] = 0;
                }

                $datos["viento"] = $row->viento;
                if ($datos["viento"] == "Ligero") {
                    $datos["logVientoBarra"] = 40;
                }elseif ($datos["viento"] == "Medio") {
                    $datos["logVientoBarra"] = 80;
                }elseif ($datos["viento"] == "Fuerte") {
                    $datos["logVientoBarra"] = 120;
                }else {
                    $datos["logVientoBarra"] = 0;
                }

                $datos["logVelBarra"] = (int)$row->velocidad;
                $datos["transmision"] = (int)$row->transmision;

                $datos["cambio_A"] = $row->cambioTransmision;
                if ($datos["cambio_A"] == "R") {
                    $datos["logCam1Barra"] = 10;
                }elseif ($datos["cambio_A"] == "1") {
                    $datos["logCam1Barra"] = 20;
                }elseif ($datos["cambio_A"] == "2") {
                    $datos["logCam1Barra"] = 30;
                }elseif ($datos["cambio_A"] == "3") {
                    $datos["logCam1Barra"] = 40;
                }elseif ($datos["cambio_A"] == "4") {
                    $datos["logCam1Barra"] = 50;
                }elseif ($datos["cambio_A"] == "5") {
                    $datos["logCam1Barra"] = 60;
                }elseif ($datos["cambio_A"] == "6") {
                    $datos["logCam1Barra"] = 70;
                }else {
                    $datos["logCam1Barra"] = 0;
                }

                $datos["operativa"] = $row->cambioOperativa;
                if ($datos["operativa"] == "P") {
                    $datos["logCam2Barra"] = 20;
                }elseif ($datos["operativa"] == "R") {
                    $datos["logCam2Barra"] = 40;
                }elseif ($datos["operativa"] == "N") {
                    $datos["logCam2Barra"] = 60;
                }elseif ($datos["operativa"] == "D") {
                    $datos["logCam2Barra"] = 80;
                }elseif ($datos["operativa"] == "L") {
                    $datos["logCam2Barra"] = 100;
                }else {
                    $datos["logCam2Barra"] = 0;
                }

                $datos["rmp"] = (int)$row->rmp;
                $temp_3 = ($datos["rmp"]-1)*10;
                $datos["logRMPBarra"] = (string)round($temp_3);

                $datos["logCargaBarra"] = $row->carga;;
                $datos["remolque"] = $row->remolque;

                $datos["pasajeros"] = (int)$row->pasajeros;
                $temp_5 = ($datos["pasajeros"]-1)*10;
                $datos["logPasajeBarra"] = (string)round($temp_5);
                $datos["logCajuelaBarra"] = (string)$row->cajuela_C;

                $datos["estructura"] = $row->estructura;
                if ($datos["estructura"] == "Plano") {
                    $datos["logEstrBarra"] = 20;
                }elseif ($datos["estructura"] == "Vado") {
                    $datos["logEstrBarra"] = 40;
                }elseif ($datos["estructura"] == "Tope") {
                    $datos["logEstrBarra"] = 60;
                }elseif ($datos["estructura"] == "Baches") {
                    $datos["logEstrBarra"] = 80;
                }elseif ($datos["estructura"] == "Vibradores") {
                    $datos["logEstrBarra"] = 100;
                }else {
                    $datos["logEstrBarra"] = 0;
                }

                $datos["camino"] = $row->camino;
                if ($datos["camino"] == "Recto") {
                    $datos["logCamBarra"] = 20;
                }elseif ($datos["camino"] == "Curva ligera") {
                    $datos["logCamBarra"] = 40;
                }elseif ($datos["camino"] == "Curva cerrada") {
                    $datos["logCamBarra"] = 60;
                }elseif ($datos["camino"] == "Sinuoso") {
                    $datos["logCamBarra"] = 80;
                }else {
                    $datos["logCamBarra"] = 0;
                }

                $datos["pendiente"] = $row->pendiente;
                if ($datos["pendiente"] == "Recto") {
                    $datos["logPenBarra"] = 20;
                }elseif ($datos["pendiente"] == "Pendiente ligera 10°") {
                    $datos["logPenBarra"] = 40;
                }elseif ($datos["pendiente"] == "Pendiente media") {
                    $datos["logPenBarra"] = 60;
                }elseif ($datos["pendiente"] == "Montaña") {
                    $datos["logPenBarra"] = 80;
                }else {
                    $datos["logPenBarra"] = 0;
                }

                $datos["superficie"] = $row->superficie;
                if ($datos["superficie"] == "Pavimento") {
                    $datos["logSupeBarra"] = 20;
                }elseif ($datos["superficie"] == "Terracería") {
                    $datos["logSupeBarra"] = 40;
                }elseif ($datos["superficie"] == "Empedrado") {
                    $datos["logSupeBarra"] = 80;
                }elseif ($datos["superficie"] == "Adoquín") {
                    $datos["logSupeBarra"] = 100;
                }elseif ($datos["superficie"] == "Fango") {
                    $datos["logSupeBarra"] = 120;
                }else {
                    $datos["logSupeBarra"] = 0;
                }
            //Campos para el pdf
            }else {
                $temperatura = (int)$row->temperatura;
                if ($temperatura > 1) {
                    $temp_1 = ($temperatura*345)/60;
                    $datos["logTempBarra"] = (string)round($temp_1)+10;
                }else {
                    $datos["logTempBarra"] = "0";
                }

                $humedad = $row->humedad;
                if ($humedad == "") {
                    $datos["logHumBarra"] = "0";
                }elseif ($humedad == "Seco") {
                    $datos["logHumBarra"] = "69";
                }elseif ($humedad == "Húmedo") {
                    $datos["logHumBarra"] = "138";
                }elseif ($humedad == "Mojado") {
                    $datos["logHumBarra"] = "207";
                }elseif ($humedad == "Lluvia") {
                    $datos["logHumBarra"] = "276";
                }elseif ($humedad == "Hielo") {
                    $datos["logHumBarra"] = "345";
                }else {
                    $datos["logHumBarra"] = "0";
                }

                $viento = $row->viento;
                if ($viento == "") {
                    $datos["logVientoBarra"] = "0";
                }elseif ($viento == "Ligero") {
                    $datos["logVientoBarra"] = "115";
                }elseif ($viento == "Medio") {
                    $datos["logVientoBarra"] = "230";
                }elseif ($viento == "Fuerte") {
                    $datos["logVientoBarra"] = "345";
                }else {
                    $datos["logVientoBarra"] = "0";
                }

                $velocidad = (int)$row->velocidad;
                if ($velocidad > 1) {
                    $temp_2 = ($velocidad*345)/260;
                    $datos["logVelBarra"] = (string)round($temp_2)+10;
                } else {
                    $datos["logVelBarra"] = "0";
                }

                $datos["transmision"] = $row->transmision;

                $camino_1 = $row->cambioTransmision;
                if ($camino_1 == "") {
                    $datos["logCam1Barra"] = "0";
                }elseif ($camino_1 == "R") {
                    $datos["logCam1Barra"] = "25";
                }elseif ($camino_1 == "1") {
                    $datos["logCam1Barra"] = "50";
                }elseif ($camino_1 == "2") {
                    $datos["logCam1Barra"] = "75";
                }elseif ($camino_1 == "3") {
                    $datos["logCam1Barra"] = "100";
                }elseif ($camino_1 == "4") {
                    $datos["logCam1Barra"] = "125";
                }elseif ($camino_1 == "5") {
                    $datos["logCam1Barra"] = "150";
                }elseif ($camino_1 == "6") {
                    $datos["logCam1Barra"] = "175";
                }else {
                    $datos["logCam1Barra"] = "0";
                }

                $operativa = $row->cambioOperativa;
                if ($operativa == "") {
                    $datos["logCam2Barra"] = "0";
                }elseif ($operativa == "P") {
                    $datos["logCam2Barra"] = "20";
                }elseif ($operativa == "R") {
                    $datos["logCam2Barra"] = "40";
                }elseif ($operativa == "N") {
                    $datos["logCam2Barra"] = "60";
                }elseif ($operativa == "D") {
                    $datos["logCam2Barra"] = "80";
                }elseif ($operativa == "L") {
                    $datos["logCam2Barra"] = "100";
                }else {
                    $datos["logCam2Barra"] = "0";
                }

                $rmp = (int)$row->rmp;
                if ($rmp > 1) {
                    $temp_3 = ($rmp*345)/8;
                    $datos["logRMPBarra"] = (string)round($temp_3);
                }else {
                    $datos["logRMPBarra"] = "0";
                }

                $carga = (int)$row->carga;
                if ($carga > 1) {
                    $temp_4 = ($carga*213)/100;
                    $datos["logCargaBarra"] = (string)round($temp_4);
                } else {
                    $datos["logCargaBarra"] = "0";
                }

                $datos["remolque"] = $row->remolque;

                $pasajeros = (int)$row->pasajeros;
                if ($pasajeros > 0) {
                    $temp_5 = ($pasajeros*213)/9;
                    $datos["logPasajeBarra"] = (string)round($temp_5);
                } else {
                    $datos["logPasajeBarra"] = "0";
                }

                $datos["logCajuelaBarra"] = (string)$row->cajuela_C;

                $estructura = $row->estructura;
                if ($estructura == "") {
                    $datos["logEstrBarra"] = "0";
                }elseif ($estructura == "Plano") {
                    $datos["logEstrBarra"] = "69";
                }elseif ($estructura == "Vado") {
                    $datos["logEstrBarra"] = "138";
                }elseif ($estructura == "Tope") {
                    $datos["logEstrBarra"] = "207";
                }elseif ($estructura == "Baches") {
                    $datos["logEstrBarra"] = "276";
                }elseif ($estructura == "Vibradores") {
                    $datos["logEstrBarra"] = "345";
                }else {
                    $datos["logEstrBarra"] = "0";
                }

                $camino = $row->camino;
                if ($camino == "") {
                    $datos["logCamBarra"] = "0";
                }elseif ($camino == "Recto") {
                    $datos["logCamBarra"] = "86";
                }elseif ($camino == "Curva ligera") {
                    $datos["logCamBarra"] = "172";
                }elseif ($camino == "Curva cerrada") {
                    $datos["logCamBarra"] = "258";
                }elseif ($camino == "Sinuoso") {
                    $datos["logCamBarra"] = "345";
                }else {
                    $datos["logCamBarra"] = "0";
                }

                $pendiente = $row->pendiente;
                if ($pendiente == "") {
                    $datos["logPenBarra"] = "0";
                }elseif ($pendiente == "Recto") {
                    $datos["logPenBarra"] = "86";
                }elseif ($pendiente == "Pendiente ligera 10°") {
                    $datos["logPenBarra"] = "172";
                }elseif ($pendiente == "Pendiente media") {
                    $datos["logPenBarra"] = "258";
                }elseif ($pendiente == "Montaña") {
                    $datos["logPenBarra"] = "345";
                }else {
                    $datos["logPenBarra"] = "0";
                }

                $superficie = $row->superficie;
                if ($superficie == "") {
                    $datos["logSupeBarra"] = "0";
                }elseif ($superficie == "Pavimento") {
                    $datos["logSupeBarra"] = "69";
                }elseif ($superficie == "Terracería") {
                    $datos["logSupeBarra"] = "138";
                }elseif ($superficie == "Empedrado") {
                    $datos["logSupeBarra"] = "207";
                }elseif ($superficie == "Adoquín") {
                    $datos["logSupeBarra"] = "276";
                }elseif ($superficie == "Fango") {
                    $datos["logSupeBarra"] = "345";
                }else {
                    $datos["logSupeBarra"] = "0";
                }
            }

            $datos["nombreAsesor"] = trim($row->nombreAsesor);
            $datos["firmaAsesor"] = (file_exists($row->firmaAsesor)) ? $row->firmaAsesor : ""; //$row->firmaAsesor;
            // $datos["nombreCliente"] = trim($row->nombreCliente);
            $datos["firmaCliente"] = (file_exists($row->firmaCliente)) ? $row->firmaCliente : ""; //$row->firmaCliente;
            // $datos["transmision"] = $row->transmision;

        }
        // print_r($datos);
        //Verificamos donde se imprimira la información
        if ($datos["destinoVista"] == "PDF") {
            $this->generatePDF($datos);
        } else {
            $this->loadAllView($datos,'otros_Modulos/inspeccion_bodyshop/inspeccion_edita');
        }

    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {
        $this->load->view("otros_Modulos/inspeccion_bodyshop/plantillaPdf", $datos);
        /*//Orientación  de la hoja P->Vertical   L->Horizontal
        $html2pdf = new Html2Pdf('P', 'A4', 'en',TRUE,'UTF-8',array(3,3,3,3));
        try {
            $html = $this->load->view("otros_Modulos/inspeccion_bodyshop/plantillaPdf", $datos, TRUE);
            $html2pdf->setDefaultFont('Helvetica');     //Helvetica
            $html2pdf->WriteHTML($html);
            $html2pdf->setTestTdInOnePage(FALSE);
            $html2pdf->Output();
        } catch (Html2PdfException $e) {
            $html2pdf->clean();
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }*/
    }

    //Convertir canvas base64 a imagen
    function base64ToImage($imgBase64 = "",$direcctorio = "",$ext = "") {
        //Generamos un nombre random para la imagen
        $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $urlNombre = date("YmdHi")."_";
        for($i=0; $i<=7; $i++ ){
            $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
        }

        //Validamos que exista la carpeta destino   base_url()
        if(!file_exists($directorio)){
            mkdir($directorio, 0647, true);
        }

        //Creamos la ruta donde se guardara la firma y su extensión
        if (strlen($imgBase64)>1) {
            $base = $direcctorio.'/'.$urlNombre.".".$ext;
            $data = explode(',', $imgBase64);
            $Base64Img = base64_decode($data[1]);
            file_put_contents($base, $Base64Img);
        }else {
            $base = "";
        }

        return $base;
    }

    //Validar multiple check
    public function validateCheck($campo = NULL,$elseValue = "")
    {
        $respuesta = "";
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            if (count($campo) >1){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."|";
                }
            }
        }
        return $respuesta;
    }

    //Validar Radio
    public function validateRadio($campo = NULL,$elseValue = 0)
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = $campo;
        }
        return $respuesta;
    }

    //Consultamos pdf de la voz por medio de ajax
    public function datosVerifica()
    {
        $respuesta = "FALSE";
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
        ini_set('set_time_limit',600);
        // ini_set('max_input_time',"20M");

        //Recuperamos el numero de orden a consultar
        $idProyecto = $_POST['orden'];
        //Verificamos si existe un registro con ese numero de orden

        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->Verificacion->get_result("proyectoId",$idProyecto,"diagnostico_bodyshop");
        foreach ($query as $row){
            $dato = $row->idRegistro;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = "EXISTE";
        }else {
            //Si no existe el registro, recuperamos la informacion de la orden
            $cadena = "";
            $precarga = $this->Verificacion->get_result('proyectoId',$idProyecto,'bodyshop');
            foreach ($precarga as $row){
                $cadena .= $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno."|";
                $cadena .= $row->datos_email."|";
                $cadena .= $row->datos_telefono."|";
                $cadena .= $row->vehiculo_placas."|";
                $cadena .= $row->vehiculo_numero_serie."|";

                //Cargamos el asesor logeado, basado en las variables de sesion
                if ($this->session->userdata('idUsuario')) {
                    //Verificamos que el usuario logeado sea un asesor
                    $asesor = $this->session->userdata('usuario');
                    if (($asesor == "LUISESTRADA")||($asesor == "pacheco")) {
                        $cadena .= mb_strtoupper($this->session->userdata('nombreUsuario'))."|";
                    }else {
                        $cadena .= $this->Verificacion->nombreAsesor($row->asesor)."|";
                    }
                }else {
                    $cadena .= $this->Verificacion->nombreAsesor($row->asesor)."|";
                }
                // $idColor = $row->id_color;
                $cadena .= $row->id_color;
            }

            //Comprobamos los valores
            if ($cadena != "") {
                $respuesta = $cadena;
            }else {
                $respuesta = "S/I";
            }
        }

        echo  $respuesta;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/firmasMoviles/firmas_DiagnosticoH.js',
                'assets/js/otrosModulos/hibrido/interaccionBotones.js',
                'assets/js/otrosModulos/hibrido/interaccionesMedidor.js',
                'assets/js/otrosModulos/hibrido/interaccionesDiagramas.js',
                'assets/js/otrosModulos/hibrido/interaccionCheck.js',
                'assets/js/otrosModulos/hibrido/peticiones.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Inspección Visual"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
