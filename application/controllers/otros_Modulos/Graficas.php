<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Graficas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        $this->load->model('MGraficas', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        //ini_set('max_execution_time',1800);
        //ini_set('set_time_limit',1800);
    }

    public function index($idOrden = "",$datos = NULL)
    {
        $contador_renglones = 0;

        $datos["presupuestos"] = $this->mConsultas->presupuesto_graficas();

        //$this->loadAllView($datos,'otros_Modulos/analisis_tiempos/lista_ordenes');
        $this->loadAllView($datos,'otros_Modulos/analisis_tiempos/lista_presupuestos');
    }

    public function cargaGrafica($idOrden = 0, $datos = NULL) 
    {
        $id_cita = ((ctype_digit($idOrden)) ? $idOrden : $this->decrypt($idOrden));
        $datos["idOrden"] = $id_cita;

        //Verificamos si es un presupuesto de formato nuevo
        $revision = $this->mConsultas->get_result_field("id_cita",$datos["idOrden"],"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $datos["envio_ventanilla"] = $row->envia_ventanilla;
            $datos["termina_ventanilla"] = new DateTime($row->termina_ventanilla);

            $datos["envio_jefeTaller"] = $row->envio_jdt;
            $datos["termina_JDT"] = new DateTime($row->termina_jdt);

            $datos["garantia"] = (((int)$row->ref_garantias > 0) ? "Si" : "");
            $datos["envio_garantia"] = $row->envio_garantia;
            $datos["termino_garantias"] = new DateTime($row->termina_garantias);

            $datos["termino_asesor"] = new DateTime($row->termina_asesor);
            $datos["registro"] = new DateTime($row->fecha_alta);

            $datos["termino_cliente"] = new DateTime($row->termina_cliente);
            $datos["ultima_actualizacion"] = new DateTime($row->fecha_actualiza);

            $datos["solicita_pieza"] = new DateTime($row->solicita_pieza);
            $datos["recibe_pieza"] = new DateTime($row->recibe_pieza);
            $datos["entrega_pieza"] = new DateTime($row->entrega_pieza);

            $datos["confirmacion"] = $row->acepta_cliente;
            $datos["entrega_refaccion"] = $row->estado_refacciones;
            $datos["firma_asesor"] = $row->firma_asesor;

            $datos["encargado_ventanilla"] = $row->ventanilla;

            //Tiempos transcurridos
            
            //Del tecnico a ventanilla
            //Si el presupuesto no paso por ventanilla
            if ($row->envia_ventanilla == "2") {
                //No hay tiempo que contabilizar
                $datos["paso_1"] = "0";
            //Si ya salio de ventanilla , recuperamos el tiempo
            }elseif ($row->envia_ventanilla == "1") {
                //Si paso por ventanilla comparamos el tiempo transcurrido desde que se creo hasta que se termino
                $datos["paso_1"] = $this->dateDiffMinutes($row->fecha_alta,$row->termina_ventanilla);
            //De lo contrario sigue en ventanilla, lo marcamos como retraso
            }else{
                //De lo contrario se asume que sige detenida en ventanilla
                $tiempo_trancurrido = $this->dateDiffMinutes($row->fecha_alta,date("Y-m-d H:i:s"));
                $datos["paso_1"] = "RETRASO: ".$tiempo_trancurrido;
            }

            //De ventanilla al jefe de taller
            if ($row->envia_ventanilla == "2") {
                //Medimos el tiempo desde que se creo hasta que se firmo
                //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                if ($row->envio_jdt == "1") {
                    $datos["paso_7"] = $this->dateDiffMinutes($row->fecha_alta,$row->termina_jdt);
                //De lo contrario se asume que la operacion esta retrasada
                } else {
                    $tiempo_trancurrido_7 = $this->dateDiffMinutes($row->fecha_alta,date("Y-m-d H:i:s"));
                    $datos["paso_7"] = "RETRASO: ".$tiempo_trancurrido_7;
                }
            //De lo contrario, medimos el tiempo desde que se libero de ventanilla hasta que se firmo
            } else {
                //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                if ($row->envio_jdt == "1") {
                    $datos["paso_7"] = $this->dateDiffMinutes($row->termina_ventanilla,$row->termina_jdt);
                //De lo contrario se asume que la operacion esta retrasada
                } else {
                    $tiempo_trancurrido_7 = $this->dateDiffMinutes($row->termina_ventanilla,date("Y-m-d H:i:s"));
                    $datos["paso_7"] = "RETRASO: ".$tiempo_trancurrido_7;
                }
            }
            
            //Del jefe de taller a garantias
            if ((int)$row->ref_garantias > 0 ) {
                if ($row->envio_garantia == "1") {
                    $datos["paso_8"] = $this->dateDiffMinutes($row->termina_jdt,$row->termina_garantias);
                }else{
                    $datos["paso_8"] = $this->dateDiffMinutes($row->termina_jdt,date("Y-m-d H:i:s"));
                }
            }else{
                $datos["paso_8"] = "NO PROCEDE";
            }            
            
            //Si el presupuesto no tiene garantia
            if ((int)$row->ref_garantias > 0 ) {
                //Medimos el tiempo desde que se creo hasta que se firmo
                //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                if ($row->firma_asesor != "") {
                    $datos["paso_2"] = $this->dateDiffMinutes($row->termina_garantias,$row->termina_asesor);
                //De lo contrario se asume que la operacion esta retrasada
                } else {
                    $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termina_garantias,date("Y-m-d H:i:s"));
                    $datos["paso_2"] = "RETRASO: ".$tiempo_trancurrido_2;
                }
            //De lo contrario, medimos el tiempo desde que se libero de ventanilla hasta que se firmo
            } else {
                //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                if ($row->firma_asesor != "") {
                    $datos["paso_2"] = $this->dateDiffMinutes($row->termina_jdt,$row->termina_asesor);
                //De lo contrario se asume que la operacion esta retrasada
                } else {
                    $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termina_jdt,date("Y-m-d H:i:s"));
                    $datos["paso_2"] = "RETRASO: ".$tiempo_trancurrido_2;
                }
            }

            //Del asesor al cliente
            if ($row->acepta_cliente != "") {
                $datos["paso_3"] = $this->dateDiffMinutes($row->termina_asesor,$row->termina_cliente);
            } else {
                $tiempo_trancurrido_3 = $this->dateDiffMinutes($row->termina_asesor,date("Y-m-d H:i:s"));
                $datos["paso_3"] = "RETRASO: ".$tiempo_trancurrido_3;
            }

            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $datos["paso_4"] = "PROCESO DETENIDO";
            } else {
                //Entrega refacciones parte 1 (solicitar)
                if (($row->solicita_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones > 0)) {
                    $datos["paso_4"] = $this->dateDiffMinutes($row->termina_cliente,$row->solicita_pieza);
                } else {
                    $tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $datos["paso_4"] = "RETRASO: ".$tiempo_trancurrido_4;
                }
            }
            
            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $datos["paso_5"] = "PROCESO DETENIDO";
            } else {
                //Entrega refacciones parte 2 (recibir)
                if(($row->recibe_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones > 1)&&(strtotime($row->recibe_pieza) > strtotime($row->solicita_pieza)) ){
                    $datos["paso_5"] = $this->dateDiffMinutes($row->solicita_pieza,$row->recibe_pieza);
                } else {
                    $tiempo_trancurrido_5 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $datos["paso_5"] = "RETRASO: ".$tiempo_trancurrido_5;
                }
            }
            
            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $datos["paso_6"] = "PROCESO DETENIDO";
            } else {
                //Entrega refacciones parte 3 (entregar)
                if(($row->entrega_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones  == 3)&&(strtotime($row->entrega_pieza) > strtotime($row->recibe_pieza)) ){
                    $datos["paso_6"] = $this->dateDiffMinutes($row->recibe_pieza,$row->entrega_pieza);
                } else {
                    $tiempo_trancurrido_6 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $datos["paso_6"] = "RETRASO: ".$tiempo_trancurrido_6;
                }
            }
        }

        //Si no existe el registro en los presupuestos nuevos, buscamos en los viejos
        if (!isset($datos["envio_ventanilla"])) {
            //Recuperamos la informacion para mostrar en tabla
            //$query = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
            $revision = $this->mConsultas->get_result_field("idOrden",$datos["idOrden"],"identificador","M","cotizacion_multipunto");
            foreach ($revision as $row){
                $datos["envio_ventanilla"] = $row->aprobacionRefaccion;
                $datos["termina_ventanilla"] = new DateTime($row->termino_refacciones);

                $datos["envio_jefeTaller"] = $row->enviaJDT;
                $datos["termina_JDT"] = new DateTime($row->termino_JDT);

                $datos["garantia"] = $row->pzaGarantia;
                $datos["envio_garantia"] = $row->envioGarantia;
                $datos["termino_garantias"] = new DateTime($row->termino_garantia);

                $datos["termino_asesor"] = new DateTime($row->termino_asesor);
                $datos["registro"] = new DateTime($row->fech_registro);

                $datos["termino_cliente"] = new DateTime($row->termino_cliente);
                $datos["ultima_actualizacion"] = new DateTime($row->fech_actualiza);

                $datos["solicita_pieza"] = new DateTime($row->solicita_pieza);
                $datos["recibe_pieza"] = new DateTime($row->recibe_pieza);
                $datos["entrega_pieza"] = new DateTime($row->entrega_pieza);

                $datos["confirmacion"] = $row->aceptoTermino;
                $datos["entrega_refaccion"] = $row->estado;
                $datos["firma_asesor"] = $row->firmaAsesor;

                $datos["encargado_ventanilla"] = $row->ventanilla;

                //Del tecnico a ventanilla
                //Si el presupuesto no paso por ventanilla
                if ($row->aprobacionRefaccion == "2") {
                    //No hay tiempo que contabilizar
                    $datos["paso_1"] = "0";
                //Si ya salio de ventanilla , recuperamos el tiempo
                }elseif ($row->aprobacionRefaccion == "1") {
                    //Si paso por ventanilla comparamos el tiempo transcurrido desde que se creo hasta que se termino
                    $datos["paso_1"] = $this->dateDiffMinutes($row->fech_registro,$row->termino_refacciones);
                //De lo contrario sigue en ventanilla, lo marcamos como retraso
                }else{
                    //De lo contrario se asume que sige detenida en ventanilla
                    $tiempo_trancurrido = $this->dateDiffMinutes($row->fech_registro,date("Y-m-d H:i:s"));
                    $datos["paso_1"] = "RETRASO: ".$tiempo_trancurrido;
                }

                //De ventanilla al jefe de taller
                if ($row->aprobacionRefaccion == "2") {
                    //Medimos el tiempo desde que se creo hasta que se firmo
                    //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                    if ($row->enviaJDT == "1") {
                        $datos["paso_7"] = $this->dateDiffMinutes($row->fecha_alta,$row->termino_JDT);
                    //De lo contrario se asume que la operacion esta retrasada
                    } else {
                        $tiempo_trancurrido_7 = $this->dateDiffMinutes($row->fecha_alta,date("Y-m-d H:i:s"));
                        $datos["paso_7"] = "RETRASO: ".$tiempo_trancurrido_7;
                    }
                //De lo contrario, medimos el tiempo desde que se libero de ventanilla hasta que se firmo
                } else {
                    //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                    if ($row->enviaJDT == "1") {
                        $datos["paso_7"] = $this->dateDiffMinutes($row->termino_refacciones,$row->termina_jdt);
                    //De lo contrario se asume que la operacion esta retrasada
                    } else {
                        $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termino_refacciones,date("Y-m-d H:i:s"));
                        $datos["paso_7"] = "RETRASO: ".$tiempo_trancurrido_7;
                    }
                }
                
                //Del jefe de taller a garantias
                if ($row->pzaGarantia != "" ) {
                    if ($row->envioGarantia == "1") {
                        $datos["paso_8"] = $this->dateDiffMinutes($row->termina_jdt,$row->termino_garantia);
                    }else{
                        $datos["paso_8"] = $this->dateDiffMinutes($row->termina_jdt,date("Y-m-d H:i:s"));
                    }
                }else{
                    $datos["paso_8"] = "NO PROCEDE";
                }
                
                //De ventanilla al asesor  (del JDT/Garantias -> Asesor)
                //Si tiene garantias, se valida que se haya liberado de garantias
                if ($row->pzaGarantia != "" ) {
                    //Medimos el tiempo desde que se creo hasta que se firmo
                    //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                    if ($row->firma_asesor != "") {
                        $datos["paso_2"] = $this->dateDiffMinutes($row->termino_garantia,$row->termino_asesor);
                    //De lo contrario se asume que la operacion esta retrasada
                    } else {
                        $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termino_garantia,date("Y-m-d H:i:s"));
                        $datos["paso_2"] = "RETRASO: ".$tiempo_trancurrido_2;
                    }
                    
                //De lo contrario, medimos el tiempo desde que se libero del jefe de taller
                } else {
                    //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                    if ($row->firmaAsesor != "") {
                        $datos["paso_2"] = $this->dateDiffMinutes($row->termina_jdt,$row->termino_asesor);
                    //De lo contrario se asume que la operacion esta retrasada
                    } else {
                        $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termina_jdt,date("Y-m-d H:i:s"));
                        $datos["paso_2"] = "RETRASO: ".$tiempo_trancurrido_2;
                    }
                }

                //Del asesor al cliente
                if ($row->aceptoTermino != "") {
                    $datos["paso_3"] = $this->dateDiffMinutes($row->termino_asesor,$row->termino_cliente);
                } else {
                    $tiempo_trancurrido_3 = $this->dateDiffMinutes($row->termino_asesor,date("Y-m-d H:i:s"));
                    $datos["paso_3"] = "RETRASO: ".$tiempo_trancurrido_3;
                }

                //Si el presupuesto fue cancelado, este evento se suspenden
                if ($row->aceptoTermino == "No") {
                    $datos["paso_4"] = "PROCESO DETENIDO";
                } else {
                    //Entrega refacciones parte 1 (solicitar)
                    if (($row->solicita_pieza != $row->fech_registro)&&((int)$row->estado > 0)) {
                        $datos["paso_4"] = $this->dateDiffMinutes($row->termino_cliente,$row->solicita_pieza);
                    } else {
                        $tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termino_cliente,date("Y-m-d H:i:s"));
                        $datos["paso_4"] = "RETRASO: ".$tiempo_trancurrido_4;
                    }
                }
                
                //Si el presupuesto fue cancelado, este evento se suspenden
                if ($row->aceptoTermino == "No") {
                    $datos["paso_5"] = "PROCESO DETENIDO";
                } else {
                    //Entrega refacciones parte 2 (recibir)
                    if(($row->recibe_pieza != $row->fech_registro)&&((int)$row->estado > 1)&&(strtotime($row->recibe_pieza) > strtotime($row->solicita_pieza)) ){
                        $datos["paso_5"] = $this->dateDiffMinutes($row->solicita_pieza,$row->recibe_pieza);
                    } else {
                        $tiempo_trancurrido_5 = $this->dateDiffMinutes($row->termino_cliente,date("Y-m-d H:i:s"));
                        $datos["paso_5"] = "RETRASO: ".$tiempo_trancurrido_5;
                    }
                }
                
                //Si el presupuesto fue cancelado, este evento se suspenden
                if ($row->aceptoTermino == "No") {
                    $datos["paso_6"] = "PROCESO DETENIDO";
                } else {
                    //Entrega refacciones parte 3 (entregar)
                    if(($row->entrega_pieza != $row->fech_registro)&&((int)$row->estado  == 2)&&(strtotime($row->entrega_pieza) > strtotime($row->recibe_pieza)) ){
                        $datos["paso_6"] = $this->dateDiffMinutes($row->recibe_pieza,$row->entrega_pieza);
                    } else {
                        $tiempo_trancurrido_6 = $this->dateDiffMinutes($row->termino_cliente,date("Y-m-d H:i:s"));
                        $datos["paso_6"] = "RETRASO: ".$tiempo_trancurrido_6;
                    }
                }
            }
        }

        //Recuperamos la información para la notificación
        $precarga = $this->mConsultas->precargaConsulta($id_cita);
        foreach ($precarga as $row){
            $datos["serie"] = (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion);
            $datos["modelo"] = $row->vehiculo_modelo;
            $datos["placas"] = $row->vehiculo_placas;
            $datos["unidad"] = $row->subcategoria;
            $datos["tecnico"] = $row->tecnico;
            $datos["asesor"] = $row->asesor;
            $datos["idIntelisis"] = $row->folioIntelisis;
            $datos["cliente"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
        }

        $this->loadAllView($datos,'otros_Modulos/analisis_tiempos/vista_analisis');
    }

    function dateDiffMinutes($fecha_inicio = '',$fecha_fin = '')
    {
        if($fecha_inicio=='0000-00-00 00:00:00' || $fecha_fin == '0000-00-00 00:00:00'){
            return 0;
        }

        //NUEVA VERSIÓN
        // ESTABLECE LOS MINUTOS POR DÍA DESDE EL HORARIO DE INICIO Y FINALIZACIÓN
        $start_time = '07:30:00';
        $end_time = '19:00:00';

        $start_ts = strtotime($start_time);
        $end_ts = strtotime($end_time);
        $minutes_per_day = (int)( ($end_ts - $start_ts) / 60 )+1;

        // ESTABLECER LAS VACACIONES
        $holidays = array
        (
            //'Feb 04', // MLK Day
        );

        // CONVERTIR VACACIONES A FECHAS ISO
        foreach ($holidays as $x => $holiday)
        {
            $holidays[$x] = date('Y-m-d', strtotime($holiday));
        }

        $fecha_sol = $fecha_inicio;
        $fecha_menor = $fecha_fin;

        //var_dump($fecha_sol,$fecha_menor);die();
        // CHECK FOR VALID DATES
        $start = strtotime($fecha_sol);
        $end = strtotime($fecha_menor);
        $start_p = date('Y-m-d H:i:s', $start);
        $end_p = date('Y-m-d H:i:s', $end);

        // MAKE AN ARRAY OF DATES
        $workdays = array();
        $workminutes = array();
        // ITERATE OVER THE DAYS
        $start = $start - 60;
        while ($start < $end)
        {
            $start = $start + 60;
            // ELIMINATE WEEKENDS - SAT AND SUN
            $weekday = date('D', $start);
            //echo $weekday;die();
            //if (substr($weekday,0,1) == 'S') continue;
            // ELIMINATE HOURS BEFORE BUSINESS HOURS
            $daytime = date('H:i:s', $start);

            if(($daytime < date('H:i:s',$start_ts))) continue;
                // ELIMINATE HOURS PAST BUSINESS HOURS
                $daytime = date('H:i:s', $start);
            if(($daytime > date('H:i:s',$end_ts))) continue;
                // ELIMINATE HOLIDAYS
                $iso_date = date('Y-m-d', $start);
            if (in_array($iso_date, $holidays)) continue;
                $workminutes[] = $iso_date;
                // END ITERATOR
        }

        $number_of_workminutes = (count($workminutes));
        $number_of_minutes = number_format($minutes_per_day);
        $horas_habiles = number_format($number_of_workminutes/60 ,2);

        if($number_of_workminutes>0){
            $number_of_minutes = $number_of_workminutes-1;
        }
        return $number_of_workminutes;
    }

    //Cargamos la informacion junto con la pagina a travez de ajax
    public function cargaDatosGrafica($idOrden = 0)
    {
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);
        // ini_set('max_input_time',"20M");

        //Recuperamos el id de la orden
        $idOrden = $_POST['idOrden'];
        $regreso = "";

        //Verificamos si es un presupuesto de formato nuevo
        $revision = $this->mConsultas->get_result_field("id_cita",$idOrden,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $valida = $row->id;

            //Fecha de creacion
            $regreso .= "Cotización Creada|";
            $regreso .= $row->fecha_alta."|";
            
            //Fecha de termino en ventanilla
            //if($row->aprobacionRefaccion == "1"){
            if($row->envia_ventanilla != "0"){
                $regreso .= "Termina Ventanilla|";
                $regreso .= $row->termina_ventanilla."|";
            }else{
                $regreso .= "NA|";
                $regreso .= "_|";
            }

            //Revision JDT (si aplica)
            if (isset($row->envio_jdt)) {
                //Fecha de termino en revision de jefe de taller
                if($row->envio_jdt == "1"){
                    $regreso .= "Termina JDT|";
                    $regreso .= $row->termina_jdt."|";
                }else{
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }
            } else {
                $regreso .= "NA|";
                $regreso .= "_|";
            }

            //Revision Garantias (si aplica)
            if (isset($row->envio_garantia)) {
                //Fecha de termino en revision de garantias
                if ((int)$row->ref_garantias > 0) {
                    if($row->envio_garantia == "1"){
                        $regreso .= "Termina Garantías|";
                        $regreso .= $row->termina_garantias."|";
                    }else{
                        $regreso .= "NA|";
                        $regreso .= "_|";
                    }
                } else {
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }
            } else {
                $regreso .= "NA|";
                $regreso .= "_|";
            }

            //Fecha de firma del asesor
            //if($row->termino_asesor != $row->fech_registro){
            if($row->firma_asesor != ""){
                $regreso .= "Firma Asesor|";
                $regreso .= $row->termina_asesor."|";
            }else{
                $regreso .= "NA|";
                $regreso .= "_|";
            }

            //Fecha en que el cliente/asesor aceptan/rechazan la cotizacion
            //if($row->termino_cliente != $row->fech_registro){
            if($row->acepta_cliente != ""){
                $regreso .= "Afecta el Cliente|";
                $regreso .= $row->termina_cliente."|";
            }else{
                $regreso .= "NA|";
                $regreso .= "_|";
            }

            if(($row->acepta_cliente != "")&&( ((int)$row->estado_refacciones > 0)||((int)$row->estado_refacciones == 1)) ){
                $regreso .= "Refaciones Solicitadas|";
                $regreso .= $row->solicita_pieza."|";
            }else{
                $regreso .= "NA|";
                $regreso .= "_|";
            }

            if(($row->acepta_cliente != "")&&( ((int)$row->estado_refacciones > 1)||((int)$row->estado_refacciones == 2)) ){
                $regreso .= "Refacciones Recibidas|";
                $regreso .= $row->recibe_pieza."|";
            }else{
                $regreso .= "NA|";
                $regreso .= "_|";
            }

            if(($row->acepta_cliente != "")&&( ((int)$row->estado_refacciones > 2)||((int)$row->estado_refacciones == 3)) ){
                $regreso .= "Refacciones Entregadas|";
                $regreso .= $row->entrega_pieza."|";
            }else{
                $regreso .= "NA|";
                $regreso .= "_|";
            }

            //Ultima actualizacion
            $regreso .= "Ultima Actualización|";
            $regreso .= $row->fecha_actualiza;
        }

        //Si no se encontro el registro, se busca en los prespupuestos viejitos
        if (!isset($valida)) {
            //$revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
            $revision = $this->mConsultas->get_result_field("idOrden",$idOrden,"identificador","M","cotizacion_multipunto");
            foreach ($revision as $row) {
                //Fecha de creacion
                $regreso .= "Cotización Creada|";
                $regreso .= $row->fech_registro."|";
                
                //Fecha de termino en ventanilla
                //if($row->aprobacionRefaccion == "1"){
                if($row->aprobacionRefaccion != "0"){
                    $regreso .= "Termina Ventanilla|";
                    $regreso .= $row->termino_refacciones."|";
                }else{
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }

                //Revision JDT (si aplica)
                if (isset($row->enviaJDT)) {
                    //Fecha de termino en revision de jefe de taller
                    if($row->enviaJDT == "1"){
                        $regreso .= "Termina JDT|";
                        $regreso .= $row->termino_JDT."|";
                    }else{
                        $regreso .= "NA|";
                        $regreso .= "_|";
                    }
                } else {
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }

                //Revision Garantias (si aplica)
                if (isset($row->envioGarantia)) {
                    //Fecha de termino en revision de garantias
                    if ($row->pzaGarantia != "") {
                        if($row->envioGarantia == "1"){
                            $regreso .= "Termina Garantías|";
                            $regreso .= $row->termino_garantia."|";
                        }else{
                            $regreso .= "NA|";
                            $regreso .= "_|";
                        }
                    } else {
                        $regreso .= "NA|";
                        $regreso .= "_|";
                    }
                } else {
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }

                //Fecha de firma del asesor
                //if($row->termino_asesor != $row->fech_registro){
                if($row->firmaAsesor != ""){
                    $regreso .= "Firma Asesor|";
                    $regreso .= $row->termino_asesor."|";
                }else{
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }

                //Fecha en que el cliente/asesor aceptan/rechazan la cotizacion
                //if($row->termino_cliente != $row->fech_registro){
                if($row->aceptoTermino != ""){
                    $regreso .= "Afecta el Cliente|";
                    $regreso .= $row->termino_cliente."|";
                }else{
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }

                if(($row->solicita_pieza != $row->fech_registro)&&((int)$row->estado > 0)){
                    $regreso .= "Refaciones Solicitadas|";
                    $regreso .= $row->solicita_pieza."|";
                }else{
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }

                if(($row->recibe_pieza != $row->fech_registro)&&((int)$row->estado > 1)&&(strtotime($row->recibe_pieza) > strtotime($row->solicita_pieza)) ){
                    $regreso .= "Refacciones Recibidas|";
                    $regreso .= $row->recibe_pieza."|";
                }else{
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }

                if(($row->entrega_pieza != $row->fech_registro)&&($row->estado != "0")&&(strtotime($row->entrega_pieza) > strtotime($row->recibe_pieza)) ){
                    $regreso .= "Refacciones Entregadas|";
                    $regreso .= $row->entrega_pieza."|";
                }else{
                    $regreso .= "NA|";
                    $regreso .= "_|";
                }

                //Ultima actualizacion
                $regreso .= "Ultima Actualización|";
                $regreso .= $row->fech_actualiza;
            }
        }

        echo $regreso;
    }

    //Cargamos la informacion de tiempos retradados
    public function cargaDatosGraficaRetrasados()
    {
        //Recuperamos el id de la orden
        $idOrden = $_POST['idOrden'];
        $regreso = "";

        $revision = $this->mConsultas->get_result_field("id_cita",$idOrden,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $validar = $row->id;

            //Del tecnico a ventanilla
            if ($row->envia_ventanilla == "2") {
                $regreso .= "Termina Ventanilla|";
                $regreso .= "0|";
            }elseif ($row->envia_ventanilla == "1") {
                //$tiempo_trancurrido = $this->dateDiffMinutes($row->fecha_alta,$row->termina_ventanilla);
                $regreso .= "Termina Ventanilla|";
                //$regreso .= $tiempo_trancurrido;
                $regreso .= "0|";
            }else{
                $tiempo_trancurrido = $this->dateDiffMinutes($row->fecha_alta,date("Y-m-d H:i:s"));
                $regreso .= "Termina Ventanilla|";
                $regreso .= $tiempo_trancurrido."|";
            }

            //Revision JDT (si aplica)
            if (isset($row->envio_jdt)) {
                //Fecha de termino en revision de jefe de taller
                if($row->envio_jdt == "1"){
                    //$tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termina_ventanilla,$row->termina_jdt);
                    $regreso .= "Termina JDT|";
                    //$regreso .= $tiempo_trancurrido_2."|";
                    $regreso .= "0|";
                }else{
                    $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termina_ventanilla,date("Y-m-d H:i:s"));
                    $regreso .= "Termina JDT|";
                    $regreso .= $tiempo_trancurrido_2."|";
                }
            } else {
                $regreso .= "NA|";
                $regreso .= "0|";
            }

            //Revision Garantias (si aplica)
            if (isset($row->ref_garantias)) {
                //Fecha de termino en revision de garantias
                if ((int)$row->ref_garantias > 0) {
                    if($row->envio_garantia == "1"){
                        //$tiempo_trancurrido_3 = $this->dateDiffMinutes($row->termina_jdt,$row->termino_garantia);
                        $regreso .= "Termina Garantías|";
                        //$regreso .= $tiempo_trancurrido_3."|";
                        $regreso .= "0|";
                    }else{
                        $tiempo_trancurrido_3 = $this->dateDiffMinutes($row->termina_jdt,date("Y-m-d H:i:s"));
                        $regreso .= "Termina Garantías|";
                        $regreso .= $tiempo_trancurrido_3."|";
                    }
                } else {
                    $regreso .= "NA|";
                    $regreso .= "0|";
                }
            } else {
                $regreso .= "NA|";
                $regreso .= "0|";
            }

            //Fecha de firma del asesor
            if ((int)$row->ref_garantias > 0) {
                if($row->firma_asesor != ""){
                    //$tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termino_garantia,$row->termina_asesor));
                    $regreso .= "Firma Asesor|";
                    //$regreso .= $tiempo_trancurrido_4."|";
                    $regreso .= "0|";
                }else{
                    $tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termino_garantia,date("Y-m-d H:i:s"));
                    $regreso .= "Firma Asesor|";
                    $regreso .= $tiempo_trancurrido_4."|";
                }
            } else {
                if($row->firma_asesor != ""){
                    //$tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termina_jdt,$row->termina_asesor));
                    $regreso .= "Firma Asesor|";
                    //$regreso .= $tiempo_trancurrido_4."|";
                    $regreso .= "0|";
                }else{
                    $tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termina_jdt,date("Y-m-d H:i:s"));
                    $regreso .= "Firma Asesor|";
                    $regreso .= $tiempo_trancurrido_4."|";
                }
            }
            
            //Fecha en que el cliente/asesor aceptan/rechazan la cotizacion
            //if($row->termina_cliente != $row->fecha_alta){
            if($row->acepta_cliente != ""){
                //$tiempo_trancurrido_5 = $this->dateDiffMinutes($row->termina_asesor,$row->termina_cliente));
                $regreso .= "Afecta el Cliente|";
                //$regreso .= $tiempo_trancurrido_5."|";
                $regreso .= "0|";
            }else{
                $tiempo_trancurrido_5 = $this->dateDiffMinutes($row->termina_asesor,date("Y-m-d H:i:s"));
                $regreso .= "Afecta el Cliente|";
                $regreso .= $tiempo_trancurrido_5."|";
            }

            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $regreso .= "Refaciones Solicitadas|";
                $regreso .= "0|";
            } else {
                //Entrega refacciones parte 1 (solicitar)
                if (($row->solicita_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones > 0)||((int)$row->estado_refacciones == 1) ){
                    //$tiempo_trancurrido_6 = $this->dateDiffMinutes($row->termina_cliente,$row->solicita_pieza);
                    $regreso .= "Refaciones Solicitadas|";
                    //$regreso .= $tiempo_trancurrido_6."|";
                    $regreso .= "0|";
                } else {
                    $tiempo_trancurrido_6 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $regreso .= "Refaciones Solicitadas|";
                    $regreso .= $tiempo_trancurrido_6."|";
                }
            }

            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $regreso .= "Refaciones Recibidas|";
                $regreso .= "0|";
            } else {
                //Entrega refacciones parte 1 (solicitar)
                if(($row->recibe_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones > 1)||((int)$row->estado_refacciones == 2) ){
                    //$tiempo_trancurrido_7 = $this->dateDiffMinutes($row->solicita_pieza,$row->recibe_pieza);
                    $regreso .= "Refacciones Recibidas|";
                    //$regreso .= $tiempo_trancurrido_7."|";
                    $regreso .= "0|";
                } else {
                    $tiempo_trancurrido_7 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $regreso .= "Refacciones Recibidas|";
                    $regreso .= $tiempo_trancurrido_7."|";
                }
            }

            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $regreso .= "Refacciones Entregadas|";
                $regreso .= "0|";
            } else {
                //Entrega refacciones parte 1 (solicitar)
                if(($row->entrega_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones > 2)||((int)$row->estado_refacciones == 3) ){
                    //$tiempo_trancurrido_8 = $this->dateDiffMinutes($row->recibe_pieza,$row->entrega_pieza);
                    $regreso .= "Refacciones Entregadas|";
                    //$regreso .= $tiempo_trancurrido_8."|";
                    $regreso .= "0|";
                } else {
                    $tiempo_trancurrido_8 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $regreso .= "Refacciones Entregadas|";
                    $regreso .= $tiempo_trancurrido_8."|";
                }
            }
        }

        if (!isset($validar)) {
            $revision = $this->mConsultas->get_result_field("idOrden",$idOrden,"identificador","M","cotizacion_multipunto");
            //$revision = $this->mConsultas->get_result("idOrden",$idOrden,"cotizacion_multipunto");
            foreach ($revision as $row) {
                //Del tecnico a ventanilla
                if ($row->aprobacionRefaccion == "2") {
                    $regreso .= "Termina Ventanilla|";
                    $regreso .= "0|";
                }elseif ($row->aprobacionRefaccion == "1") {
                    //$tiempo_trancurrido = $this->dateDiffMinutes($row->fech_registro,$row->termino_refacciones);
                    $regreso .= "Termina Ventanilla|";
                    //$regreso .= $tiempo_trancurrido;
                    $regreso .= "0|";
                }else{
                    $tiempo_trancurrido = $this->dateDiffMinutes($row->fech_registro,date("Y-m-d H:i:s"));
                    $regreso .= "Termina Ventanilla|";
                    $regreso .= $tiempo_trancurrido."|";
                }

                //Revision JDT (si aplica)
                if (isset($row->enviaJDT)) {
                    //Fecha de termino en revision de jefe de taller
                    if($row->enviaJDT == "1"){
                        //$tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termino_refacciones,$row->termino_JDT);
                        $regreso .= "Termina JDT|";
                        //$regreso .= $tiempo_trancurrido_2."|";
                        $regreso .= "0|";
                    }else{
                        $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termino_refacciones,date("Y-m-d H:i:s"));
                        $regreso .= "Termina JDT|";
                        $regreso .= $tiempo_trancurrido_2."|";
                    }
                } else {
                    $regreso .= "NA|";
                    $regreso .= "0|";
                }

                //Revision Garantias (si aplica)
                if (isset($row->envioGarantia)) {
                    //Fecha de termino en revision de garantias
                    if ($row->pzaGarantia != "") {
                        if($row->envioGarantia == "1"){
                            //$tiempo_trancurrido_3 = $this->dateDiffMinutes($row->termino_JDT,$row->termino_garantia);
                            $regreso .= "Termina Garantías|";
                            //$regreso .= $tiempo_trancurrido_3."|";
                            $regreso .= "0|";
                        }else{
                            $tiempo_trancurrido_3 = $this->dateDiffMinutes($row->termino_JDT,date("Y-m-d H:i:s"));
                            $regreso .= "Termina Garantías|";
                            $regreso .= $tiempo_trancurrido_3."|";
                        }
                    } else {
                        $regreso .= "NA|";
                        $regreso .= "0|";
                    }
                } else {
                    $regreso .= "NA|";
                    $regreso .= "0|";
                }

                //Fecha de firma del asesor
                if ($row->pzaGarantia != "") {
                    if($row->firmaAsesor != ""){
                        //$tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termino_JDT,$row->termino_asesor));
                        $regreso .= "Firma Asesor|";
                        //$regreso .= $tiempo_trancurrido_4."|";
                        $regreso .= "0|";
                    }else{
                        $tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termino_garantia,date("Y-m-d H:i:s"));
                        $regreso .= "Firma Asesor|";
                        $regreso .= $tiempo_trancurrido_4."|";
                    }
                } else {
                    if($row->firmaAsesor != ""){
                        //$tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termino_JDT,$row->termino_asesor));
                        $regreso .= "Firma Asesor|";
                        //$regreso .= $tiempo_trancurrido_4."|";
                        $regreso .= "0|";
                    }else{
                        $tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termino_JDT,date("Y-m-d H:i:s"));
                        $regreso .= "Firma Asesor|";
                        $regreso .= $tiempo_trancurrido_4."|";
                    }
                }
                
                //Fecha en que el cliente/asesor aceptan/rechazan la cotizacion
                //if($row->termino_cliente != $row->fech_registro){
                if($row->aceptoTermino != ""){
                    //$tiempo_trancurrido_5 = $this->dateDiffMinutes($row->termino_asesor,$row->termino_cliente));
                    $regreso .= "Afecta el Cliente|";
                    //$regreso .= $tiempo_trancurrido_5."|";
                    $regreso .= "0|";
                }else{
                    $tiempo_trancurrido_5 = $this->dateDiffMinutes($row->termino_asesor,date("Y-m-d H:i:s"));
                    $regreso .= "Afecta el Cliente|";
                    $regreso .= $tiempo_trancurrido_5."|";
                }

                //Si el presupuesto fue cancelado, este evento se suspenden
                if ($row->aceptoTermino == "No") {
                    $regreso .= "Refaciones Solicitadas|";
                    $regreso .= "0|";
                } else {
                    //Entrega refacciones parte 1 (solicitar)
                    if (($row->solicita_pieza != $row->fech_registro)&&((int)$row->estado > 0)) {
                        //$tiempo_trancurrido_6 = $this->dateDiffMinutes($row->termino_cliente,$row->solicita_pieza);
                        $regreso .= "Refaciones Solicitadas|";
                        //$regreso .= $tiempo_trancurrido_6."|";
                        $regreso .= "0|";
                    } else {
                        $tiempo_trancurrido_6 = $this->dateDiffMinutes($row->termino_cliente,date("Y-m-d H:i:s"));
                        $regreso .= "Refaciones Solicitadas|";
                        $regreso .= $tiempo_trancurrido_6."|";
                    }
                }

                //Si el presupuesto fue cancelado, este evento se suspenden
                if ($row->aceptoTermino == "No") {
                    $regreso .= "Refaciones Recibidas|";
                    $regreso .= "0|";
                } else {
                    //Entrega refacciones parte 1 (solicitar)
                    if(($row->recibe_pieza != $row->fech_registro)&&((int)$row->estado > 1)&&(strtotime($row->recibe_pieza) > strtotime($row->solicita_pieza)) ){
                        //$tiempo_trancurrido_7 = $this->dateDiffMinutes($row->solicita_pieza,$row->recibe_pieza);
                        $regreso .= "Refacciones Recibidas|";
                        //$regreso .= $tiempo_trancurrido_7."|";
                        $regreso .= "0|";
                    } else {
                        $tiempo_trancurrido_7 = $this->dateDiffMinutes($row->termino_cliente,date("Y-m-d H:i:s"));
                        $regreso .= "Refacciones Recibidas|";
                        $regreso .= $tiempo_trancurrido_7."|";
                    }
                }

                //Si el presupuesto fue cancelado, este evento se suspenden
                if ($row->aceptoTermino == "No") {
                    $regreso .= "Refacciones Entregadas|";
                    $regreso .= "0|";
                } else {
                    //Entrega refacciones parte 1 (solicitar)
                    if(($row->entrega_pieza != $row->fech_registro)&&((int)$row->estado  == 2)&&(strtotime($row->entrega_pieza) > strtotime($row->recibe_pieza)) ){
                        //$tiempo_trancurrido_8 = $this->dateDiffMinutes($row->recibe_pieza,$row->entrega_pieza);
                        $regreso .= "Refacciones Entregadas|";
                        //$regreso .= $tiempo_trancurrido_8."|";
                        $regreso .= "0|";
                    } else {
                        $tiempo_trancurrido_8 = $this->dateDiffMinutes($row->termino_cliente,date("Y-m-d H:i:s"));
                        $regreso .= "Refacciones Entregadas|";
                        $regreso .= $tiempo_trancurrido_8."|";
                    }
                }
            }
        }

        echo $regreso;
    }

    //Buscamos los presupuestos relacionados
    public function busquedaGrafica()
    {
        //Recuperamos el id de la orden
        $campo = $_POST['campo'];
        $regreso = "";

        //Buscamos en los nuevos presupuestos
        $precarga = $this->mConsultas->listaPresupuestoCampo($campo); 
        foreach ($precarga as $row) {
            if (isset($row->id_cita)) {
                // ORDEN            0
                $regreso .= $row->id_cita."=";
                // ORDEN INTELISIS  1
                $regreso .= $row->folio_externo."=";
                // ASESOR           2
                $regreso .= $row->asesor."=";
                // TÉCNICO          3
                $regreso .= $row->tecnico."=";
                // MODELO           4
                $regreso .= $row->vehiculo_modelo."=";
                // PLACAS           5
                $regreso .= $row->vehiculo_placas."=";
                // PROCESO          6
                $regreso .=  strtoupper($row->ubicacion_proceso)."=";
                // ULTIMA ACTUALIZACIÓN     7
                $actualiza = new DateTime($row->fecha_actualiza);
                $regreso .= $actualiza->format('d/m/Y')."=";
                // ESTADO/CLIENTE           8
                $regreso .= $row->acepta_cliente."=";
                // ESTADO/REFACCIÓN         9
                $regreso .= $row->estado_refacciones."=";

                //COLORES DE REFACCIONES
                if ($row->acepta_cliente != "No") {
                    if ($row->estado_refacciones == "0") {
                        $regreso .= "background-color:#f5acaa;"."=";  // 10
                        $regreso .= "SIN SOLICITAR"."=";  // 11
                    } elseif ($row->estado_refacciones == "1") {
                        $regreso .= "background-color:#f5ff51;"."=";  // 10
                        $regreso .= "SOLICIATADAS"."=";  // 11
                    } elseif ($row->estado_refacciones == "2") {
                        $regreso .= "background-color:#5bc0de;"."=";  // 10
                        $regreso .= "RECIBIDAS"."=";  // 11
                    }else{
                        $regreso .= "background-color:#c7ecc7;"."=";  // 10
                        $regreso .= "ENTREGADAS"."=";  // 11
                    }
                } else {
                    $regreso .= "background-color:red;color:white;"."=";  // 10
                    $regreso .= "SIN SOLICITAR"."=";  // 11
                }

                //12
                $regreso .= $this->encrypt($row->id_cita)."=";

                $regreso .= "|";
            } 
        }

        //Buscamos en los presupuestos viejitos
        /*$resultados = $this->mConsultas->listaCotizacionBusqueda($campo);

        foreach ($resultados as $row) {
            // ORDEN            0
            $regreso .= $row->idOrden."=";
            // ORDEN INTELISIS  1
            $regreso .= $row->folioIntelisis."=";
            // ASESOR           2
            $regreso .= $row->asesor."=";
            // TÉCNICO          3
            $regreso .= $row->tecnico."=";
            // MODELO           4
            $regreso .= $row->vehiculo_modelo."=";
            // PLACAS           5
            $regreso .= $row->vehiculo_placas."=";
            // PROCESO          6
            //Identificamos la parte del proceso
            //Si aun no pasa por ventanilla (ni se envio al asesor)
            if ($row->aprobacionRefaccion == "0") {
                $regreso .= "ESPERA REVISIÓN DE VENTANILLA"."=";
            //si ya salio de ventanilla y aun no firma el asesor (y el cliente no ha afectado)
            }elseif (($row->aprobacionRefaccion != "0")&&($row->firmaAsesor == "")&&($row->aceptoTermino == "")&&($row->estado == "0")) {
                $regreso .= "ESPERA REVISIÓN DEL ASESOR"."=";
            //si ya salio de ventanilla ya  firmo el asesor (y el cliente no ha afectado)
            }elseif (($row->aprobacionRefaccion != "0")&&($row->firmaAsesor != "")&&($row->aceptoTermino == "")&&($row->estado == "0")) {
                $regreso .= "ESPERA SER AFECTADA POR EL CLIENTE"."=";
            //si ya salio de ventanilla ya  firmo el asesor y el cliente ya afecto el presupuesto
            }elseif (($row->aprobacionRefaccion != "0")&&($row->firmaAsesor != "")&&($row->aceptoTermino != "")&&($row->estado == "0")) {
                if ($row->aceptoTermino == "No") {
                    $regreso .= "PRESUPUESTO RECHAZADO"."=";
                }else{
                    $regreso .= "ESPERA ENTREGA DE REFACCIONES"."=";
                }
            //si ya salio de ventanilla ya  firmo el asesor y el cliente ya afecto el presupuesto
            }elseif (($row->aprobacionRefaccion != "0")&&($row->firmaAsesor != "")&&($row->aceptoTermino != "")&&($row->estado == "2")) {
                $regreso .= "REFACCIONES ENTREGADAS AL TÉCNICO"."=";
            }else{
                if (($row->aceptoTermino != "")&&($row->estado == "0")) {
                    $regreso .= "AFECTADA POR EL CLIENTE"."=";
                }else{
                    $regreso .= "PRESUPUESTO  EN PROCESO"."=";
                }
            }

            // ULTIMA ACTUALIZACIÓN     7
            $actualiza = new DateTime($row->fech_actualiza);
            $regreso .= $actualiza->format('d/m/Y')."=";
            // ESTADO/CLIENTE           8
            $regreso .= $row->aceptoTermino."=";
            // ESTADO/REFACCIÓN         9
            $regreso .= $row->estado."=";

            //COLORES DE REFACCIONES
            if ($row->aceptoTermino != "No") {
                if ($row->estado == "0") {
                    $regreso .= "background-color:#f5acaa;"."=";  // 10
                    $regreso .= "SIN SOLICITAR"."=";  // 11
                } elseif ($row->estado == "1") {
                    $regreso .= "background-color:#f5ff51;"."=";  // 10
                    $regreso .= "SOLICIATADAS"."=";  // 11
                } elseif ($row->estado == "3") {
                    $regreso .= "background-color:#5bc0de;"."=";  // 10
                    $regreso .= "RECIBIDAS"."=";  // 11
                }else{
                    $regreso .= "background-color:#c7ecc7;"."=";  // 10
                    $regreso .= "ENTREGADAS"."=";  // 11
                }
            } else {
                $regreso .= "background-color:red;color:white;"."=";  // 10
                $regreso .= "SIN SOLICITAR"."=";  // 11
            }

            //12
            $regreso .= $this->encrypt($row->idOrden)."=";

            $regreso .= "|";
        }*/

        echo $regreso;
    }

    public function reporte_tiempos_presupuestos($idOrden = '', $datos = NULL)
    {
        $presupuestos = $this->mConsultas->presupuesto_graficas_garantias();

        foreach ($presupuestos as $row) {
            $fecha = new DateTime($row->fecha_actualiza);

            if($row->acepta_cliente != ""){
                if ($row->acepta_cliente == "Si") {
                    $style = "color: darkgreen; font-weight: bold;background-color: lightgreen;";
                    $mensaje = "CONFIRMADA";
                }elseif ($row->acepta_cliente == "Val") {
                    $style = "color: darkorange; font-weight: bold;background-color: antiquewhite;";
                    $mensaje = "DETALLADA";
                }else{
                    $style = "color: red ; font-weight: bold;background-color: antiquewhite;";
                    $mensaje = "RECHAZADA";
                }
            }else{
                $style = "color: black; font-weight: bold;background-color: gainsboro;";
                $mensaje = "SIN CONFIRMAR";
            }

            $datos["reporte"][] = array(
                "id_cita" => $row->id_cita,
                "folio_externo" => $row->folio_externo,
                "asesor" => $row->asesor,
                "tecnico" => $row->tecnico,
                "modelo" => $row->subcategoria,
                "placas" => $row->vehiculo_placas,
                "serie" => $row->vehiculo_identificacion,
                "cliente" => $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno,
                "ubicacion_proceso" => $row->ubicacion_proceso,
                "fecha_actualiza" => $fecha->format('d/m/Y'),
                "afecta_cliente" => $style,
                "txtafecta_cliente" => $mensaje,
                "historial" => $this->historial_movimientos($row->id_cita),
            );
        }
        //var_dump($datos["reporte"]);
        $this->loadAllView($datos,'otros_Modulos/analisis_tiempos/reporte_garantias');
    }

    public function historial_movimientos($id_cita = ''){
        //Verificamos si es un presupuesto de formato nuevo
        $revision = $this->mConsultas->get_result_field("id_cita",$id_cita,"identificador","M","presupuesto_registro");
        foreach ($revision as $row) {
            $data["envio_ventanilla"] = $row->envia_ventanilla;
            $data["termina_ventanilla"] = new DateTime($row->termina_ventanilla);

            $data["envio_jefeTaller"] = $row->envio_jdt;
            $data["termina_JDT"] = new DateTime($row->termina_jdt);

            $data["garantia"] = (((int)$row->ref_garantias > 0) ? "Si" : "");
            $data["envio_garantia"] = $row->envio_garantia;
            $data["termino_garantias"] = new DateTime($row->termina_garantias);

            $data["termino_asesor"] = new DateTime($row->termina_asesor);
            $data["registro"] = new DateTime($row->fecha_alta);

            $data["termino_cliente"] = new DateTime($row->termina_cliente);
            $data["ultima_actualizacion"] = new DateTime($row->fecha_actualiza);

            $data["solicita_pieza"] = new DateTime($row->solicita_pieza);
            $data["recibe_pieza"] = new DateTime($row->recibe_pieza);
            $data["entrega_pieza"] = new DateTime($row->entrega_pieza);

            $data["confirmacion"] = $row->acepta_cliente;
            $data["entrega_refaccion"] = $row->estado_refacciones;
            $data["firma_asesor"] = $row->firma_asesor;

            $data["encargado_ventanilla"] = $row->ventanilla;

            //Tiempos transcurridos
            
            //Del tecnico a ventanilla
            //Si el presupuesto no paso por ventanilla
            if ($row->envia_ventanilla == "2") {
                //No hay tiempo que contabilizar
                $data["paso_1"] = "0";
            //Si ya salio de ventanilla , recuperamos el tiempo
            }elseif ($row->envia_ventanilla == "1") {
                //Si paso por ventanilla comparamos el tiempo transcurrido desde que se creo hasta que se termino
                $data["paso_1"] = $this->dateDiffMinutes($row->fecha_alta,$row->termina_ventanilla);
            //De lo contrario sigue en ventanilla, lo marcamos como retraso
            }else{
                //De lo contrario se asume que sige detenida en ventanilla
                $tiempo_trancurrido = $this->dateDiffMinutes($row->fecha_alta,date("Y-m-d H:i:s"));
                $data["paso_1"] = "RETRASO: ".$tiempo_trancurrido;
            }

            //De ventanilla al jefe de taller
            if ($row->envia_ventanilla == "2") {
                //Medimos el tiempo desde que se creo hasta que se firmo
                //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                if ($row->envio_jdt == "1") {
                    $data["paso_7"] = $this->dateDiffMinutes($row->fecha_alta,$row->termina_jdt);
                //De lo contrario se asume que la operacion esta retrasada
                } else {
                    $tiempo_trancurrido_7 = $this->dateDiffMinutes($row->fecha_alta,date("Y-m-d H:i:s"));
                    $data["paso_7"] = "RETRASO: ".$tiempo_trancurrido_7;
                }
            //De lo contrario, medimos el tiempo desde que se libero de ventanilla hasta que se firmo
            } else {
                //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                if ($row->envio_jdt == "1") {
                    $data["paso_7"] = $this->dateDiffMinutes($row->termina_ventanilla,$row->termina_jdt);
                //De lo contrario se asume que la operacion esta retrasada
                } else {
                    $tiempo_trancurrido_7 = $this->dateDiffMinutes($row->termina_ventanilla,date("Y-m-d H:i:s"));
                    $data["paso_7"] = "RETRASO: ".$tiempo_trancurrido_7;
                }
            }
            
            //Del jefe de taller a garantias
            if ((int)$row->ref_garantias > 0 ) {
                if ($row->envio_garantia == "1") {
                    $data["paso_8"] = $this->dateDiffMinutes($row->termina_jdt,$row->termina_garantias);
                }else{
                    $data["paso_8"] = $this->dateDiffMinutes($row->termina_jdt,date("Y-m-d H:i:s"));
                }
            }else{
                $data["paso_8"] = "NO PROCEDE";
            }            
            
            //Si el presupuesto no tiene garantia
            if ((int)$row->ref_garantias > 0 ) {
                //Medimos el tiempo desde que se creo hasta que se firmo
                //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                if ($row->firma_asesor != "") {
                    //$data["paso_2"] = $this->dateDiffMinutes($row->termina_garantias,$row->termina_asesor);
                    $data["paso_2"] = $this->dateDiffMinutes($row->termina_jdt,$row->termina_asesor);
                //De lo contrario se asume que la operacion esta retrasada
                } else {
                    $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termina_garantias,date("Y-m-d H:i:s"));
                    $data["paso_2"] = "RETRASO: ".$tiempo_trancurrido_2;
                }
            //De lo contrario, medimos el tiempo desde que se libero de ventanilla hasta que se firmo
            } else {
                //Si el asesor ya firmo el presupuesto, validamos el tiempo trancurrido
                if ($row->firma_asesor != "") {
                    $data["paso_2"] = $this->dateDiffMinutes($row->termina_jdt,$row->termina_asesor);
                //De lo contrario se asume que la operacion esta retrasada
                } else {
                    $tiempo_trancurrido_2 = $this->dateDiffMinutes($row->termina_jdt,date("Y-m-d H:i:s"));
                    $data["paso_2"] = "RETRASO: ".$tiempo_trancurrido_2;
                }
            }

            //Del asesor al cliente
            if ($row->acepta_cliente != "") {
                $data["paso_3"] = $this->dateDiffMinutes($row->termina_asesor,$row->termina_cliente);
            } else {
                $tiempo_trancurrido_3 = $this->dateDiffMinutes($row->termina_asesor,date("Y-m-d H:i:s"));
                $data["paso_3"] = "RETRASO: ".$tiempo_trancurrido_3;
            }

            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $data["paso_4"] = "PROCESO DETENIDO";
            } else {
                //Entrega refacciones parte 1 (solicitar)
                if (($row->solicita_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones > 0)) {
                    $data["paso_4"] = $this->dateDiffMinutes($row->termina_cliente,$row->solicita_pieza);
                } else {
                    $tiempo_trancurrido_4 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $data["paso_4"] = "RETRASO: ".$tiempo_trancurrido_4;
                }
            }
            
            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $data["paso_5"] = "PROCESO DETENIDO";
            } else {
                //Entrega refacciones parte 2 (recibir)
                if(($row->recibe_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones > 1)&&(strtotime($row->recibe_pieza) > strtotime($row->solicita_pieza)) ){
                    $data["paso_5"] = $this->dateDiffMinutes($row->solicita_pieza,$row->recibe_pieza);
                } else {
                    $tiempo_trancurrido_5 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $data["paso_5"] = "RETRASO: ".$tiempo_trancurrido_5;
                }
            }
            
            //Si el presupuesto fue cancelado, este evento se suspenden
            if ($row->acepta_cliente == "No") {
                $data["paso_6"] = "PROCESO DETENIDO";
            } else {
                //Entrega refacciones parte 3 (entregar)
                if(($row->entrega_pieza != $row->fecha_alta)&&((int)$row->estado_refacciones  == 3)&&(strtotime($row->entrega_pieza) > strtotime($row->recibe_pieza)) ){
                    $data["paso_6"] = $this->dateDiffMinutes($row->recibe_pieza,$row->entrega_pieza);
                } else {
                    $tiempo_trancurrido_6 = $this->dateDiffMinutes($row->termina_cliente,date("Y-m-d H:i:s"));
                    $data["paso_6"] = "RETRASO: ".$tiempo_trancurrido_6;
                }
            }
        }

        return $data;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }
    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrosModulos/graficas/cargaGraficas.js',
                'assets/js/otrosModulos/graficas/busquedaGrafica.js',
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Analisis tiempos"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
