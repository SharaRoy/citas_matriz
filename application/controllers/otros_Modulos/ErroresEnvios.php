<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ErroresEnvios extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',600);
        ini_set('set_time_limit',600);
    }

    public function index($x = "",$datos = NULL)
    {
        //Recuperamos las ordenes que tienen errores de entio
        $revision = $this->mConsultas->get_result_not("");
        foreach ($revision as $row) {
            $datos["idOrden"][] = $row->id_cita;
            $datos["mensaje"][] = ($row->errorEnvio != "") ? $row->errorEnvio : "ORDEN NO ENVIADA";
            $datos["asesor"][] = $row->asesor;
            $datos["cliente"][] = $nombre = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            
            $fecha1 = explode("-",$row->fecha_recepcion);
            $datos["fechaAlta"][] = $fecha1[2]."/".$fecha1[1]."/".$fecha1[0]." ".$row->hora_recepcion;
            
            $fecha2 = explode("-",$row->fecha_entrega);
            $datos["fechaPromesa"][] = $fecha2[2]."/".$fecha2[1]."/".$fecha2[0]." ".$row->hora_entrega; 
        }

        $this->loadAllView($datos,'otrosFormularios/otrasVistas/listaMensajes');
    }

    //Hacemos la peticion por post para enviar la información a intelisis
    public function reenvioDeOrden($idOrden = 0)
    {
        $idOrden = $_POST["idOrden"];

        $data = array('idOrden' => $idOrden );
        $url = base_url()."api/Intelisis/loadDataOrden";

        $ch = curl_init($url);
        //Creamos el objeto json para enviar por url los datos del formulario
        $jsonDataEncoded = json_encode($data);
        //Para que la peticion no imprima el resultado como una cadena, y podamos manipularlo
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Adjuntamos el json a nuestra petición
        curl_setopt($ch, CURLOPT_POSTFIELDS,$jsonDataEncoded);
        //Agregamos los encabezados del contenido
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        //Obtenemos la respuesta
        $response = curl_exec($ch);
        // Se cierra el recurso CURL y se liberan los recursos del sistema
        curl_close($ch);
        
        echo $response;
    }

    //Busqueda por fechas
    public function filtro_ordenes()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $campo = $_POST['campo'];

        $bandera = '0';

        //Si no se envio un campo de consulta
        if ($campo == "") {
            if ($fecha_fin == "") {
                $precarga = $this->mConsultas->get_result_not_fecha($fecha_inicio);
            } else {
                $precarga = $this->mConsultas->get_result_not_fecha_1($fecha_inicio,$fecha_fin);
            }
        //Si se envio algun campode busqueda
        } else {
            //Si no se envia ninguna fecha
            if (($fecha_fin == "")&&($fecha_inicio == "")) {
                $precarga = $this->mConsultas->get_result_not_cita($campo);
                
            //Si se envian ambas fechas
            }elseif (($fecha_fin != "")&&($fecha_inicio != "")) {
                $precarga = $this->mConsultas->get_result_not_fecha_1($fecha_inicio,$fecha_fin);
                $bandera = '1';
            //Si solo se envia una fecha
            } else {
                if ($fecha_fin == "") {
                    $precarga = $this->mConsultas->get_result_not_fecha($fecha_inicio);
                    $bandera = '1';
                } else {
                    $precarga = $this->mConsultas->get_result_not_fecha($fecha_fin);
                    $bandera = '1';
                }
            }
        }
        
        $cadena = "";

        foreach ($precarga as $row){
            if ($bandera == '1') {
                if ($row->id_cita == $campo) {
                    // 0
                    $cadena .= $row->id_cita ."=";
                    // 1
                    $cadena .= (($row->errorEnvio != "") ? $row->errorEnvio : "ORDEN NO ENVIADA" )."=";
                    // 2
                    $cadena .= $row->asesor ."=";
                    // 3
                    $cadena .= $nombre = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno ."=";
                    
                    $fecha1 = explode("-",$row->fecha_recepcion);
                    // 4
                    $cadena .= $fecha1[2]."/".$fecha1[1]."/".$fecha1[0]." ".$row->hora_recepcion ."=";
                    
                    $fecha2 = explode("-",$row->fecha_entrega);
                    // 5
                    $cadena .= $fecha2[2]."/".$fecha2[1]."/".$fecha2[0]." ".$row->hora_entrega ."=";
                    $cadena .= "|";
                }
            } else {
                // 0
                $cadena .= $row->id_cita ."=";
                // 1
                $cadena .= (($row->errorEnvio != "") ? $row->errorEnvio : "ORDEN NO ENVIADA" )."=";
                // 2
                $cadena .= $row->asesor ."=";
                // 3
                $cadena .= $nombre = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno ."=";
                
                $fecha1 = explode("-",$row->fecha_recepcion);
                // 4
                $cadena .= $fecha1[2]."/".$fecha1[1]."/".$fecha1[0]." ".$row->hora_recepcion ."=";
                
                $fecha2 = explode("-",$row->fecha_entrega);
                // 5
                $cadena .= $fecha2[2]."/".$fecha2[1]."/".$fecha2[0]." ".$row->hora_entrega ."=";
                $cadena .= "|";
            }
        }

        echo $cadena;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrosFormularios/erroresEnvio.js',
                ''
            )
        );
        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Errores de envio"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }

}
