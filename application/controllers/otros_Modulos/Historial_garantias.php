<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historial_garantias extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', '', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

    //Inicio para validar si existe un regsitro o no
  	public function index($id = 0,$datos = NULL)
  	{
        //Recuperamos el id orden_cdk
        //$datos["orden_CDK"] = $this->mConsultas->id_orden_intelisis($idOrden);

        //Consultamos si ya se entrego la unidad
        //$datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($idOrden);

        //Verificamos si existe el registro
        $revision = $this->mConsultas->get_result("id",$id,"refacciones_garantias");
        foreach ($revision as $row) {
            $idHG = $row->id_cita;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($idHG)) {
            //Verificamos si la cotización ya fue aceptada/rechazada por el cliente
            $this->setData($id,$datos);
        } else {
            $datos['asesores'] = $this->mConsultas->get_result("activo","1","operadores");
            $datos["tiempo_int"] = $this->cat_horas();
            $datos['idRegistro'] = '0';
            $this->loadAllView($datos,'otros_Modulos/historial_garantia/hg_alta');
        }
  	}

    //Funcion para generar el folio de la venta
    public function generateFolio($tabla = "")
    {
        //Recuperamos el indice para el folio
        //$fecha = date("Ymd");
        $registro = $this->mConsultas->last_id_refacciones($tabla);

        $nvoFolio = $registro + 1;
        if ($nvoFolio < 10) {
            $folio = "0000".$nvoFolio;
        } elseif(($nvoFolio > 9) && ($nvoFolio < 100)) {
            $folio = "000".$nvoFolio;
        } elseif(($nvoFolio > 99) && ($nvoFolio < 1000)) {
            $folio = "00".$nvoFolio;
        } elseif(($nvoFolio > 999) && ($nvoFolio < 1000)) {
            $folio = "0".$nvoFolio;
        }else {
            $folio = $nvoFolio;
        }
        return $folio;
    }

    public function cat_horas(){
        $contenedor = [];

        for ($i=0; $i < 13 ; $i= $i+1) {
            
            if($i < 1){
                $contenedor[] = "MEDIA";
            }else{
                $contenedor[] = $i;
                $contenedor[] = $i." Y MEDIA";
            }
        }

        return $contenedor;
    }

    //Validamos el formulario
    /*public function validateForm()
    {
        $datos["tipoRegistro"] = $this->input->post("cargaFormulario");
        $datos["orden"] = $this->input->post("orden");
        $datos["idRegistro"] = $this->input->post("registro");

        //Recuperamos el id orden_cdk
        //$datos["orden_CDK"] = $this->mConsultas->id_orden_intelisis($$datos["orden"]);

        //Consultamos si ya se entrego la unidad
        $datos["UnidadEntrega"] = $this->mConsultas->Unidad_entregada($$datos["orden"]);

        if ($datos["tipoRegistro"] == "1") {
            $this->form_validation->set_rules('fecha_folio', 'Campo requerido', 'required');
            $this->form_validation->set_rules('orden', 'Campo requerido', 'required|callback_existencia');
            
            $this->form_validation->set_rules('serie', 'Campo requerido', 'required');

            $this->form_validation->set_rules('tecnico', 'Campo requerido', 'required');
            $this->form_validation->set_rules('asesor', 'Campo requerido', 'required');

            $this->form_validation->set_rules('telefonoCliente', 'Campo requerido', 'required');
            $this->form_validation->set_rules('modeloVehiculo', 'Campo requerido', 'required');
            $this->form_validation->set_rules('cliente', 'Campo requerido', 'required');
        }else{
            $this->form_validation->set_rules('orden', 'Campo requerido', 'required');
        } 
        
        $this->form_validation->set_rules('folio', 'Campo requerido', 'required');
        $this->form_validation->set_rules('estatuaOrden', 'Campo requerido', 'required');
        $this->form_validation->set_rules('cantidad_0', 'Campo requerido', 'required');
        //$this->form_validation->set_rules('numParte_0', 'Campo requerido', 'required');
        $this->form_validation->set_rules('descripcionCorta_0', 'Campo requerido', 'required');
        
        //$this->form_validation->set_rules('fecha_pedido', 'Campo requerido', 'required');

        //Mensajes de error
        $this->form_validation->set_message('required','%s');
        $this->form_validation->set_message('valid_email','Proporcione un correo electrónico valido');

        //Si pasa las validaciones enviamos la información al servidor
        if($this->form_validation->run()!=false){
            //Recuperamos la informacion del formulario
            $this->getDataForm($datos);
        }else{
            $datos["mensaje"]="0";
            // var_dump(form_error());
            // var_dump($this->form_validation->run());
            $this->index($datos["idRegistro"],$datos);
        }
    }*/

    //Validamos si existe un registro con ese numero de orden
    public function existencia()
    {
        $respuesta = FALSE;
        $idOrden = $this->input->post("orden");
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("id_cita",$idOrden,"refacciones_garantias");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = FALSE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = TRUE;
        }
        return $respuesta;
    }

    public function validar_orden()
    {
        $respuesta = FALSE;
        $id_cita = $_POST["orden"];
        //Consultamos en la tabla para verificar que esa orden de servico no haya sido guardada previamente
        $query = $this->mConsultas->get_result("id_cita",$id_cita,"ordenservicio");
        foreach ($query as $row){
            $dato = $row->id;
        }

        //Interpretamos la respuesta de la consulta
        if (isset($dato)) {
            //Si existe el dato, entonces ya existe el registro
            $respuesta = TRUE;
        }else {
            //Si no existe el dato, entonces no existe el registro
            $respuesta = FALSE;
        }

        return $respuesta;
    }

    //Recibimos el formulario y guardamos
    public function guardar_formulario()
    {
        $tipo_formulario = $_POST["cargaFormulario"];
        $registro = date("Y-m-d H:i:s");
        $confirmacion = FALSE;

        //Si el registro se va a guardar por primera vez
        if ($tipo_formulario == "1") {
            //Verificamos que exista la orden de apertura
            $validar = $this->validar_orden();
            if ($validar) {
                //Rectificamso que no se haya guardado un registro con ese numero de cita
                $reevaluacion = $this->mConsultas->get_result("id_cita",$_POST["orden"],"refacciones_garantias");
                foreach ($reevaluacion as $row) {
                    $valida_edo = $row->id;
                }

                //Si existe el registro
                if (!isset($valida_edo)) {
                    $limite = $_POST["maxRenglon"];

                    $contenedor = array(
                        "id" => NULL,
                        "fecha_folio" => $_POST["fecha_folio"],
                        "id_cita" => $_POST["orden"],
                        "estatus" => $_POST["estatuaOrden"],
                        "tel_cliente" => $_POST["telefonoCliente"],
                        "tel_secundario" => $_POST["telefonoCliente_2"],
                        "tecnico" => $_POST["tecnico"],
                        "folio" => $_POST["folio"],
                        "asesor" => $_POST["asesor"],
                        "modelo" => $_POST["modeloVehiculo"],
                        "serie" => $_POST["serie"],
                        "cliente" => $_POST["cliente"],

                        "cantidad" => $this->mergeOfString2("cantidad_","",$limite),
                        "descripcion_corta" => $this->mergeOfString2("descripcionCorta_","",$limite),
                        "nParte" => $this->mergeOfString2("numParte_","",$limite),

                        "fecha_pedido" => $_POST["fecha_pedido"],
                        "no_rem" => $_POST["noRemision"],
                        "fecha_BO" => $_POST["fechaBO"],
                        "nounidadInmov" => $_POST["unidadInmo"],
                        "fecha_promesa" => $_POST["fecha_promesa"],
                        "fecha_recibe" => $_POST["fecha_recibe"],
                        "fecha_aviso_taller" => $_POST["fecha_taller"],
                        "ref_revisado_por" => $_POST["revisadaPor"],
                        "observaciones_pieza" => $_POST["observacionesPza"],
                        "costo_dist" => $_POST["costoDist"],
                        "costo_total" => $_POST["costoTotal"],
                        "notasEstatus" => $_POST["notaEstatus"],
                        "backorder" => "0",
                        "tiempo_inst_tecnico" => $_POST["tiempo_tecnico"]."/".$_POST["unidad_tecnico"],
                        "tiempo_inst_cleinte" => $_POST["tiempo_cliente"]."/".$_POST["unidad_cliente"],
                        "fecha_creacion" => $registro,
                        "fecha_actualizacion" => $registro,
                    );

                    $registroDB = $this->mConsultas->save_register('refacciones_garantias',$contenedor);

                    if ($registroDB != 0) {
                        $limite++;
                        for ($i=0; $i <= $limite ; $i++) { 
                            if (isset($_POST["descripcionCorta_".$i])) {
                                if ($_POST["descripcionCorta_".$i] != "") {
                                    $paquete = array(
                                        "id" => NULL,
                                        "id_cita" =>$_POST["orden"],
                                        "id_registro" => $registroDB,
                                        "id_refaccion" => ((isset($_POST["id_refaccion_".$i])) ? $_POST["id_refaccion_".$i] : NULL),
                                        "tipo" => "2",          //1 - público   2 - garantías
                                        "cantidad" => $_POST["cantidad_".$i],
                                        "descripcion" => $_POST["descripcionCorta_".$i],
                                        "clave_pieza" => $_POST["numParte_".$i],
                                        "existencia" => "NO",
                                        "activo" => "1",
                                        "pedido" => "0",
                                        "fecha_pedido" => $registro,
                                        "fecha_alta" => $registro,
                                        "fecha_edicion" => $registro,
                                    );

                                    $ref_p = $this->mConsultas->save_register('refaciones_psni',$paquete);
                                }
                            }
                        }

                        //Recuperamos el usuario que realiza el movimiento
                        if ($this->session->userdata('rolIniciado')) {
                            $usuario = $this->session->userdata('nombreUsuario');
                        }elseif ($this->session->userdata('id_usuario')) {
                            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
                        }else{
                            $usuario = "Ventanilla";
                        }

                        //Creamos el registro del estaus
                        $contenedor_estatus = array(
                            "id" => NULL,
                            "id_cita" => $contenedor["id_cita"],
                            "estatus_anterior" => "",
                            "estatus_actual" => $contenedor["estatus"],
                            "fecha_inicio" => $registro,
                            "fecha_fin" => $registro,
                            "usuario_afecta" => $usuario,
                            "motivo" => $contenedor["notasEstatus"],
                            "cambio_num" => 1,
                        );

                        $registro_estatus = $this->mConsultas->save_register('estatus_refacciones_historico',$contenedor_estatus);
                        
                        $respuesta = "OK_".$_POST["orden"];
                    } else {
                        $respuesta = "ERROR AL GUARDAR EL FORMULARIO_NS";
                    }
                    
                }else{
                   $respuesta = "YA EXISTE UN REGISTRO CON ESE No DE ORDEN_NS";
                }
            }else{
               $respuesta = "NO SE ENCONTRO UNA ORDEN ABIERTA_NS"; 
            }            
        } else {
            $limite = $_POST["maxRenglon"];

            $contenedor = array(
                //"id" => NULL,
                //"fecha_folio" => $_POST["fecha_folio"],
                //"id_cita" => $_POST["orden"],
                "estatus" => $_POST["estatuaOrden"],
                "tel_cliente" => $_POST["telefonoCliente"],
                "tecnico" => $_POST["tecnico"],
                "folio" => $_POST["folio"],
                "asesor" => $_POST["asesor"],
                "modelo" => $_POST["modeloVehiculo"],
                "serie" => $_POST["serie"],
                "cliente" => $_POST["cliente"],

                "cantidad" => $this->mergeOfString("cantidad_","",$limite),
                "descripcion_corta" => $this->mergeOfString("descripcionCorta_","",$limite),
                "nParte" => $this->mergeOfString("numParte_","",$limite),

                "fecha_pedido" => $_POST["fecha_pedido"],
                "no_rem" => $_POST["noRemision"],
                "fecha_BO" => $_POST["fechaBO"],
                "nounidadInmov" => $_POST["unidadInmo"],
                "fecha_promesa" => $_POST["fecha_promesa"],
                "fecha_recibe" => $_POST["fecha_recibe"],
                "fecha_aviso_taller" => $_POST["fecha_taller"],
                "ref_revisado_por" => $_POST["revisadaPor"],
                "observaciones_pieza" => $_POST["observacionesPza"],
                "costo_dist" => $_POST["costoDist"],
                "costo_total" => $_POST["costoTotal"],
                "notasEstatus" => $_POST["notaEstatus"],
                //"backorder" => "0",
                "tiempo_inst_tecnico" => $_POST["tiempo_tecnico"]."/".$_POST["unidad_tecnico"],
                "tiempo_inst_cleinte" => $_POST["tiempo_cliente"]."/".$_POST["unidad_cliente"],
                //"fecha_creacion" => $registro,
                "fecha_actualizacion" => $registro,
            );

            $actualizar = $this->mConsultas->update_table_row('refacciones_garantias',$contenedor,'id',$_POST["registro"]);

            if ($actualizar) {
                $limite++;
                //Actualizamos las refacciones
                for ($i=0; $i <= $limite ; $i++) { 
                    //Evitamos errores al llamar un campo
                    if (isset($_POST["id_index_".$i])) {
                        //Evitamos los renglones vacios
                        if ($_POST["descripcionCorta_".$i] != "") {
                            //Verificamos si es un renglon nuevo o una actualizacion
                            if ($_POST["id_index_".$i] == "0") {
                                $paquete = array(
                                    "id" => NULL,
                                    "id_cita" =>$_POST["orden"],
                                    "id_registro" => $_POST["registro"],
                                    "id_refaccion" => ((isset($_POST["id_refaccion_".$i])) ? $_POST["id_refaccion_".$i] : NULL),
                                    "tipo" => "2",          //1 - público   2 - garantías
                                    "cantidad" => $_POST["cantidad_".$i],
                                    "descripcion" => $_POST["descripcionCorta_".$i],
                                    "clave_pieza" => $_POST["numParte_".$i],
                                    "existencia" => "NO",
                                    "activo" => "1",
                                    "pedido" => "0",
                                    "fecha_pedido" => $registro,
                                    "fecha_alta" => $registro,
                                    "fecha_edicion" => $registro,
                                );

                                $ref_p = $this->mConsultas->save_register('refaciones_psni',$paquete);
                            }else{
                                $paquete = array(
                                    //"id_refaccion" => ((isset($_POST["id_refaccion_".$i])) ? $_POST["id_refaccion_".$i] : NULL),
                                    //"tipo" => "2",          //1 - público   2 - garantías
                                    "cantidad" => $_POST["cantidad_".$i],
                                    "descripcion" => $_POST["descripcionCorta_".$i],
                                    "clave_pieza" => $_POST["numParte_".$i],
                                    "activo" => (($_POST["id_refaccion_".$i] != "1A") ? "1" : "0"),
                                    "fecha_edicion" => $registro
                                );

                                $this->mConsultas->update_table_row('refaciones_psni',$paquete,'id',$_POST["id_index_".$i]);
                            }
                        }
                    }
                }

                //Recuperamos el usuario que realiza el movimiento
                if ($this->session->userdata('rolIniciado')) {
                    $usuario = $this->session->userdata('nombreUsuario');
                }elseif ($this->session->userdata('id_usuario')) {
                    $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
                }else{
                    $usuario = "Ventanilla";
                }

                //Recuperamos los datos del estatus anterior
                $estatus_revision = $this->mConsultas->ultimo_estatus('estatus_refacciones_historico',$_POST["orden"]);
                $estatus_revision[2] = (int)$estatus_revision[2] + 1;
                
                //Actualizamos el cambio de estatus
                $contenedor_estatus = array(
                    "id" => NULL,
                    "id_cita" => $_POST["orden"],
                    "estatus_anterior" => $estatus_revision[0],
                    "estatus_actual" => $contenedor["estatus"],
                    "fecha_inicio" => $estatus_revision[1],
                    "fecha_fin" => $registro,
                    "usuario_afecta" => $usuario,
                    "motivo" => $contenedor["notasEstatus"],
                    "cambio_num" => $estatus_revision[2],
                );

                $registro_estatus = $this->mConsultas->save_register('estatus_refacciones_historico',$contenedor_estatus);

                //Si se actualizo correctamente
                $respuesta = "OK_".$_POST["orden"];
            } else {
                $respuesta = "ERROR AL ACTUALIZAR EL FORMULARIO_NS";
            }
        }
        
        echo $respuesta;       
    }

    //Recuperamos la informacion del formulario
    /*public function getDataForm($datos = NULL)
    {
        $registro = date("Y-m-d H:i");
        //Armamos el array para contener los valores
        if ($datos["tipoRegistro"] == "1") {
            $dataForm = array(
              "id" => NULL,
              "fecha_folio" => $this->input->post("fecha_folio")
            );
        }

        //$dataForm["fecha_folio"] = $this->input->post("fecha_folio");
        $dataForm["id_cita"] = $this->input->post("orden");
        $dataForm["estatus"] = $this->input->post("estatuaOrden");
        $dataForm["tel_cliente"] = $this->input->post("telefonoCliente");
        $dataForm["tel_secundario"] = $this->input->post("telefonoCliente_2");
        $dataForm["tecnico"] = $this->input->post("tecnico"); 
        $dataForm["folio"] = $this->input->post("folio");
        $dataForm["asesor"] = $this->input->post("asesor");
        $dataForm["modelo"] = $this->input->post("modeloVehiculo");
        $dataForm["serie"] = $this->input->post("serie");
        $dataForm["cliente"] = $this->input->post("cliente");

        $limite = $this->input->post("maxRenglon");

        $dataForm["cantidad"] = $this->mergeOfString2("cantidad_","",$limite);
        $dataForm["descripcion_corta"] = $this->mergeOfString2("descripcionCorta_","",$limite);
        $dataForm["nParte"] = $this->mergeOfString2("numParte_","",$limite);
        
        $dataForm["fecha_pedido"] = $this->input->post("fecha_pedido");
        $dataForm["no_rem"] = $this->input->post("noRemision");
        $dataForm["fecha_BO"] = $this->input->post("fechaBO");
        $dataForm["nounidadInmov"] = $this->input->post("unidadInmo");
        $dataForm["fecha_promesa"] = $this->input->post("fecha_promesa");
        $dataForm["fecha_recibe"] = $this->input->post("fecha_recibe");
        $dataForm["fecha_aviso_taller"] = $this->input->post("fecha_taller");
        $dataForm["ref_revisado_por"] = $this->input->post("revisadaPor");
        $dataForm["observaciones_pieza"] = $this->input->post("observacionesPza");
        $dataForm["costo_dist"] = $this->input->post("costoDist");
        $dataForm["costo_total"] = $this->input->post("costoTotal");
        $dataForm["notasEstatus"] = $this->input->post("notaEstatus");

        $dataForm["backorder"] = '0';

        if ($datos["tipoRegistro"] == "1") {
          $dataForm["fecha_creacion"] = $registro;
        }
        
        $dataForm["fecha_actualizacion"] = $registro;

        //var_dump($dataForm);

        //Validamos si es un registro de alta o una actualizacion
        //Registro de alta
        if ($datos["tipoRegistro"] == "1") {
            $registroDB = $this->mConsultas->save_register('refacciones_garantias',$dataForm);

            if ($registroDB != 0) {

                //Creamos el registro del estaus
                $contenedor_estatus = array(
                    "id" => NULL,
                    "id_cita" => $dataForm["orden"],
                    "estatus_anterior" => "",
                    "estatus_actual" => $dataForm["estatus"],
                    "fecha_inicio" => $registro,
                    "fecha_fin" => $registro,
                    "usuario_afecta" => "",
                    "motivo" => $dataForm["notasEstatus"],
                    "cambio_num" => 1,
                );

                $registro_estatus = $this->mConsultas->save_register('estatus_refacciones_historico',$contenedor_estatus);

                $datos["mensaje"] = "1";
                redirect(base_url().'HG_Lista/'.$datos["mensaje"]);
            }else {
                $datos["mensaje"] = "2";
                $this->index($datos["idRegistro"],$datos);
            }

            // $this->index($datos["idOrden"],$datos);
            
        //Registro de actualizacion
        }else {
            $actualizar = $this->mConsultas->update_table_row('refacciones_garantias',$dataForm,'id',$datos["idRegistro"]);

            if ($actualizar) {
                //Recuperamos el usuario que realiza el movimiento
                if ($this->session->userdata('rolIniciado')) {
                    $usuario = $this->session->userdata('nombreUsuario');
                }elseif ($this->session->userdata('id_usuario')) {
                    $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
                }else{
                    $usuario = "Ventanilla";
                }

                //Recuperamos los datos del estatus anterior
                $estatus_revision = $this->mConsultas->ultimo_estatus('estatus_refacciones_historico',$datos["orden"]);
                $estatus_revision[2] = (int)$estatus_revision[2] + 1;
                
                //Actualizamos el cambio de estatus
                $contenedor_estatus = array(
                    "id" => NULL,
                    "id_cita" => $datos["orden"],
                    "estatus_anterior" => $estatus_revision[0],
                    "estatus_actual" => $dataForm["estatus"],
                    "fecha_inicio" => $estatus_revision[1],
                    "fecha_fin" => $registro,
                    "usuario_afecta" => $usuario,
                    "motivo" => $dataForm["notasEstatus"],
                    "cambio_num" => $estatus_revision[2],
                );

                $registro_estatus = $this->mConsultas->save_register('estatus_refacciones_historico',$contenedor_estatus);

                $datos["mensaje"] = "1";
                redirect(base_url().'HG_Lista/'.$datos["mensaje"]);
                //$this->index($datos["idRegistro"],$datos);
            }else {
                $datos["mensaje"] = "0";
                $this->index($datos["idRegistro"],$datos);
            }
            //$this->index($datos["idRegistro"],$datos);
        }
    }*/

    //Validar multiple renglones del campo
    public function mergeOfString($campo = "",$elseValue = "",$limite = 0)
    {   
        if ($campo == "") {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            //Separamos los index de los id de tipo de usuario
            for ($i=0; $i < $limite ; $i++) {
                if (isset($_POST[$campo."".$i])) {
                    if ($_POST[$campo."".$i] != "") {
                        $respuesta .= $_POST[$campo."".$i]."=";
                    }
                }
            }
        }
        return $respuesta;
    }

    //Validar multiple renglones del campo
    public function mergeOfString2($campo = "",$elseValue = "",$limite = 0)
    {   
        if ($campo == "") {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            //Separamos los index de los id de tipo de usuario
            for ($i=0; $i < $limite ; $i++) {
                if($this->input->post($campo."".$i)){
                    if ($this->input->post($campo."".$i) != "") {
                        $respuesta .= $this->input->post($campo."".$i)."_";
                    }
                }
            }
        }
        return $respuesta;
    }

    //Recuperamos todos los registro
    public function comodinLista($respuesta = 4, $datos = NULL)
    {
        //Verificamos si existe el registro
        $query = $this->mConsultas->get_table_list("refacciones_garantias","id");
        foreach ($query as $row) {
            if ($row->baja == "0") {
                $fecha_promesa = new DateTime($row->fecha_promesa);
                $datos["fecha_promesa"][] = $fecha_promesa->format('d/m/Y');

                $fecha_recibe = new DateTime($row->fecha_recibe);
                $datos["fecha_recibe"][] = $fecha_recibe->format('d/m/Y');
                
                $datos["id"][] = $row->id ;
                $datos["orden"][] = $row->id_cita ;            
                $datos["tecnico"][] = $row->tecnico ;
                $datos["folio"][] = $row->folio ;
                $datos["asesor"][] = $row->asesor ;                        
                $datos["cliente"][] = $row->cliente ;
                $datos["modelo"][] = $row->modelo ;
                $datos["tel_cliente"][] = "Tel 1. ".$row->tel_cliente ."<br>Tel 2. ".$row->tel_secundario ;

                $datos["serie"][] = $row->serie ;

                $consultaSeri = "908".$row->id_cita; //($row->serie != '') ? $this->mConsultas->idHistorialProactivo(strtoupper($row->serie)) : '0';
                $datos["idProactivo"][] = $consultaSeri;

                $datos["estatus"][] = $row->estatus ;

                //Clasificamos el color a mostrar por el estaus
                if (strtoupper($row->estatus) == "DISP INSTALAR") {
                    $datos["estatusColor"][] = "#92D050";
                    $datos["colorLetra"][] = "#FFFFFF";
                }elseif (strtoupper($row->estatus) == "NO CONTESTA") {
                    $datos["estatusColor"][] = "#FF0000";
                    $datos["colorLetra"][] = "#000000";
                }elseif (strtoupper($row->estatus) == "RESERVADO") {
                    $datos["estatusColor"][] = "#7030A0";
                    $datos["colorLetra"][] = "#FFFFFF";
                }elseif (strtoupper($row->estatus) == "NO IDENTIFICADO") {
                    $datos["estatusColor"][] = "#FFFF00";
                    $datos["colorLetra"][] = "#000000";
                }elseif (strtoupper($row->estatus) == "POR RESERVAR") {
                    $datos["estatusColor"][] = "#00B0F0";
                    $datos["colorLetra"][] = "#FFFFFF";
                }else{  //PENDIENTE
                    $datos["estatusColor"][] = "#ffffff";
                    $datos["colorLetra"][] = "#000000";
                }
            }            
        }

        $datos["mensaje"] = $respuesta;

        $this->loadAllView($datos,'otros_Modulos/historial_garantia/lista_historial');
    }

    //Recuperamos los datos del registro
    public function setData($idOrden = 0, $datos = NULL)
    {
        //Verificamos si existe el registro
        $revision = $this->mConsultas->get_result("id",$idOrden,"refacciones_garantias");
        foreach ($revision as $row) {
            $datos["idRegistro"] = $row->id;
            
            $datos["orden"] = $row->id_cita ;

            $datos["estatuaOrden"] = strtoupper($row->estatus);
            $datos["notaEstatus"] = strtoupper($row->notasEstatus);
            
            $datos["telefonoCliente"] = $row->tel_cliente ;
            $datos["tecnico"] = $row->tecnico ;
            $datos["folio"] = $row->folio ;
            $datos["asesor"] = $row->asesor ;
            $datos["modeloVehiculo"] = $row->modelo ;
            $datos["serie"] = strtoupper($row->serie);
            $datos["cliente"] = $row->cliente ;

            $datos["cantidad"] = explode("=",$row->cantidad) ;
            $datos["descripcionCorta"] = explode("=",$row->descripcion_corta) ;
            $datos["numParte"] = explode("=",$row->nParte) ;

            $datos["noRemision"] = $row->no_rem ;
            $datos["fechaBO"] = $row->fecha_BO ;
            $datos["unidadInmo"] = $row->nounidadInmov ;
            $datos["revisadaPor"] = $row->ref_revisado_por ;
            $datos["observacionesPza"] = $row->observaciones_pieza ;
            $datos["costoDist"] = $row->costo_dist ;
            $datos["costoTotal"] = $row->costo_total;

            $datos["fecha_folio"] = $row->fecha_folio ;
            $datos["fecha_pedido"] = $row->fecha_pedido ;
            $datos["fecha_promesa"] = $row->fecha_promesa ;
            $datos["fecha_recibe"] = $row->fecha_recibe ;
            $datos["fecha_taller"] = $row->fecha_aviso_taller ;
        
            $datos["t_tec"] = explode("/",$row->tiempo_inst_tecnico) ;
            $datos["t_cl"] = explode("/",$row->tiempo_inst_cleinte) ;

        }

        if (isset($datos["orden"])) {
            //Cargamos refacciones
            $revision = $this->mConsultas->get_result_field("id_registro",$datos["idRegistro"],"activo","1","refaciones_psni");
            foreach ($revision as $row) {
                if($row->tipo == "2"){
                    $datos["refacciones"][] = array(
                        "id_refaccion" => $row->id,
                        "cantidad" => $row->cantidad,
                        "descripcion" => $row->descripcion,
                        "clave_pieza" => $row->clave_pieza
                    );
                }
            }
        }

        $datos['asesores'] = $this->mConsultas->get_result("activo","1","operadores");
        $datos["tiempo_int"] = $this->cat_horas();

        $this->loadAllView($datos,'otros_Modulos/historial_garantia/hg_edita');
    }

    //Recuperamos las fechas para relaizar los filtros
    public function filtroPorFechas()
    {
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];

        if ($fecha_fin == "") {
            $precarga = $this->mConsultas->busquedaGarantiasRefFecha_1($fecha_inicio);
        } else {
            $precarga = $this->mConsultas->busquedaGarantiasRefFecha($fecha_inicio,$fecha_fin);
        }
        
        $cadena = "";
        
        foreach ($precarga as $row){
            $cadena .= $row->id_cita."="; 
            $cadena .= $row->folio."=";
            $cadena .= $row->cliente."=";
            $cadena .= $row->asesor."=";
            $cadena .= $row->tecnico."=";
            $cadena .= "Tel 1. ".$row->tel_cliente ."<br>Tel 2. ".$row->tel_secundario."=";

            $fecha_recibe = new DateTime($row->fecha_recibe);
            $cadena .= $fecha_recibe->format('d/m/Y')."=";

            $fecha_promesa = new DateTime($row->fecha_promesa);
            $cadena .= $fecha_promesa->format('d/m/Y')."=";

            $cadena .= $row->id."=" ;

            $serie = ($row->serie != "") ? $row->serie : "*";
            $cadena .= "908".$row->id_cita."="; //$this->mConsultas->idHistorialProactivo(strtoupper($serie))."=";

            //Clasificamos el color a mostrar por el estaus
            if (strtoupper($row->estatus) == "DISP INSTALAR") {
                $cadena .= "#92D050"."=";
                $cadena .= "#FFFFFF"."=";
            }elseif (strtoupper($row->estatus) == "NO CONTESTA") {
                $cadena .= "#FF0000"."=";
                $cadena .= "#000000"."=";
            }elseif (strtoupper($row->estatus) == "RESERVADO") {
                $cadena .= "#7030A0"."=";
                $cadena .= "#FFFFFF"."=";
            }elseif (strtoupper($row->estatus) == "NO IDENTIFICADO") {
                $cadena .= "#FFFF00"."=";
                $cadena .= "#000000"."=";
            }elseif (strtoupper($row->estatus) == "POR RESERVAR") {
                $cadena .= "#00B0F0"."=";
                $cadena .= "#FFFFFF"."=";
            }else{  //PENDIENTE
                $cadena .= "#ffffff"."=";
                $cadena .= "#000000"."=";
            }
            $cadena .= $row->serie."=";
            $cadena .= "|";
        }


        echo $cadena;
    }

    //Recuperamos la informacion del registro
    public function cargaInfo()
    {
        $idRegistro = $_POST['idRegistro'];
        $cadena = "";

        $revision = $this->mConsultas->get_result("id",$idRegistro,"refacciones_garantias");

        foreach ($revision as $row) {            
            $cadena .= $row->id_cita."|" ;
            $cadena .= $row->folio."|" ;
            $cadena .= strtoupper($row->estatus)."|";

            //Clasificamos el color a mostrar por el estaus
            if (strtoupper($row->estatus) == "DISP INSTALAR") {
                $cadena .= "#92D050|";
                $cadena .= "#FFFFFF|";
            }elseif (strtoupper($row->estatus) == "NO CONTESTA") {
                $cadena .= "#FF0000|";
                $cadena .= "#000000|";
            }elseif (strtoupper($row->estatus) == "RESERVADO") {
                $cadena .= "#7030A0|";
                $cadena .= "#FFFFFF|";
            }elseif (strtoupper($row->estatus) == "NO IDENTIFICADO") {
                $cadena .= "#FFFF00|";
                $cadena .= "#000000|";
            }elseif (strtoupper($row->estatus) == "POR RESERVAR") {
                $cadena .= "#00B0F0|";
                $cadena .= "#FFFFFF|";
            }else{  //PENDIENTE
                $cadena .= "#ffffff|";
                $cadena .= "#000000|";
            }
            
            $cadena .= $row->cantidad."|" ;
            $cadena .= $row->descripcion_corta."|" ;
        }

        echo $cadena;
    }

    //Guardamos el id que se da de baja
    public function guardarBaja()
    {
        $registro = date("Y-m-d H:i:s");

        $idRegistro = $_POST['idRegistro'];
        $motivo = $_POST['motivo'];

        $contenedor = array(
            'baja' => 1,
            'motivoBaja' => $motivo,
            'fecha_baja' => $registro,
        );

        $actualizar = $this->mConsultas->update_table_row('refacciones_garantias',$contenedor,'id',$idRegistro);

        if ($actualizar) {
            $result = "OK";
        }else{
            $result = "ERROR";
        }

        echo $result;
    }

    //Recuperamos el historial de cambios realizados a la orden
    public function historial_cambios()
    {
        $id_cita = $_POST['id_cita'];

        $cadena = "";

        $revision = $this->mConsultas->get_result("id_cita",$id_cita,"estatus_refacciones_historico");

        foreach ($revision as $row) {
            $cadena .= $row->cambio_num."=";

            $fecha = explode(" ",$row->fecha_fin);
            $fecha_reg = explode("-",$fecha[0]);
            $cadena .= $fecha_reg[2]."/".$fecha_reg[1]."/".$fecha_reg[0]." ".$fecha[1]."=";

            $cadena .= $row->estatus_actual."=";
            $cadena .= $row->motivo."=";

            //Clasificamos el color a mostrar por el estaus
            if (strtoupper($row->estatus_actual) == "DISP INSTALAR") {
                $cadena .= "#92D050"."=";
                $cadena .= "#FFFFFF"."=";
            }elseif (strtoupper($row->estatus_actual) == "NO CONTESTA") {
                $cadena .= "#FF0000"."=";
                $cadena .= "#000000"."=";
            }elseif (strtoupper($row->estatus_actual) == "RESERVADO") {
                $cadena .= "#7030A0"."=";
                $cadena .= "#FFFFFF"."=";
            }elseif (strtoupper($row->estatus_actual) == "NO IDENTIFICADO") {
                $cadena .= "#FFFF00"."=";
                $cadena .= "#000000"."=";
            }elseif (strtoupper($row->estatus_actual) == "POR RESERVAR") {
                $cadena .= "#00B0F0"."=";
                $cadena .= "#FFFFFF"."=";
            }else{  //PENDIENTE
                $cadena .= "#ffffff"."=";
                $cadena .= "#000000"."=";
            }
            
            $cadena .= "|";
        }

        echo $cadena;
    }

    //Generamos pdf
    public function generatePDF($datos = NULL)
    {        
        $this->load->view("", $datos);
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrosModulos/nspi/busqueda.js',
                'assets/js/otrosModulos/nspi/lista_delete.js',
                'assets/js/otrosModulos/nspi/envio_historico_garantia.js',
                'assets/js/superUsuario.js'
            )
        );

        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Historial Garantías"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}