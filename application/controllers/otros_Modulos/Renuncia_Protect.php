<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Renuncia_Protect extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mConsultas', 'consulta', TRUE);
        $this->load->model('Verificacion', '', TRUE);
        //Cargamos la libreria de email
        $this->load->library('email');
        date_default_timezone_set(TIMEZONE_SUCURSAL);
    }

    //Inicio para validar si existe un regsitro o no
    public function index($orden = "0",$datos = NULL)
    {
        $id_limpio = ((ctype_digit($orden)) ? $orden : $this->decrypt($orden));
        //Verificamos si existe el registro
        $revision = $this->consulta->get_result("id_cita",$id_limpio,"carta_renuncia");
        foreach ($revision as $row) {
            $verificar = $row->id;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($verificar)) {
            //Verificamos si la cotización ya fue aceptada/rechazada por el cliente
            $this->cargar_registro($id_limpio,$datos);
        } else {
            $datos["motivos_ausencia"] = $this->consulta->get_table("cat_autorizacion_ausencia");
            $this->loadAllView($datos,'otros_Modulos/carta_renuncia/formulario');
        }
    }

    public function guardar_formulario()
    {
        $registro = date("Y-m-d H:i:s");
        $valida_form = 0;

        if (isset($_POST["envio_formulario"])) {
            if ($_POST["envio_formulario"] == "1") {
                $valida_form = $this->confirmar_orden($_POST["orden"]);
            } else {
                $valida_form = 1;
            }
            $envio_formulario = $_POST["envio_formulario"];
        } else {
            $valida_form = 1;
        }
        
            

        if ($valida_form == 1) {
            if ($envio_formulario == "1") {
                $contenido["id"] = NULL;
                $contenido["id_cita"] = $_POST["orden"];
                $contenido["folio"] = $_POST["folio_externo"];
            }

            $contenido["serie"] = strtoupper($_POST["serie"]);
            $contenido["modelo"] = strtoupper($_POST["modelo"]);
            $contenido["placas"] = strtoupper($_POST["placas"]);
            $contenido["unidad"] = strtoupper($_POST["uen"]);
            $contenido["tecnico"] = strtoupper($_POST["tecnico"]);

            $contenido["beneficios"] = $this->validateCheck($_POST["beneficios"],"");

            $contenido["firma_asesor"] = $this->base64ToImage($_POST["ruta_firma_asesor"],"firmas");
            $contenido["nombre_asesor"] = strtoupper($_POST["nombre_asesor"]);
            $contenido["fecha_asesor"] = $_POST["fecha_fasesor"];

            $contenido["firma_cliente"] = $this->base64ToImage($_POST["ruta_firma_cliente"],"firmas");
            $contenido["nombre_cliente"] = strtoupper($_POST["nombre_cliente"]);
            $contenido["fecha_cliente"] = $_POST["fecha_fcliente"];

            if ($envio_formulario == "1") {
                $contenido["fecha_alta"] = $registro;
            }

            $contenido["fecha_actualiza"] = $registro;

            if ($envio_formulario == "1") {
                $contenido["autorizacion_ausencia"] = ((isset($_POST["autorizacion_ausencia"])) ? "1" : "0");
                $contenido["motivo_ausencia"] = ((isset($_POST["motivo"])) ? $_POST["motivo"] : "0");
            }

            if ($envio_formulario == "1") {
                $insertar = $this->consulta->save_register('carta_renuncia',$contenido);
                if ($insertar != 0) {

                    //Si se lleno el diagnostico por ausencia, se actualiza en la orden de servicio
                    if (isset($_POST["autorizacion_ausencia"])) {
                        $contededor["autorizacion_ausencia"] = "1";
                        //Guardamos el registro
                        $actualiza = $this->consulta->update_table_row('ordenservicio',$contededor,'id_cita',$_POST["orden"]);

                        //Guardamos el movimiento
                        $this->loadHistory($_POST["orden"],"Registro de Carta Renuncia Beneficios Ford Protect. Registro con autorizacion por ausensia","Carta Renuncia");

                        //->$this->envio_correo($_POST["orden"]);
                    //De lo contrario, actualizamos la firma
                    }else{
                        $contededor["firma_protect"] = "1";
                        //Guardamos el registro
                        $actualiza = $this->consulta->update_table_row('ordenservicio',$contededor,'id_cita',$_POST["orden"]);
                        //Guardamos el movimiento
                        $this->loadHistory($_POST["orden"],"Registro de Carta Renuncia Beneficios Ford Protect","Carta Renuncia");
                    }

                    //$respuesta = "OK_".$insertar;
                    $respuesta = "OK_".$this->encrypt($_POST["orden"]);
                } else {
                    $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_0";
                }
                
            }else{
                $actualizar = $this->consulta->update_table_row('carta_renuncia',$contenido,'id_cita',$_POST["orden"]);
                if ($actualizar) {
                    $this->loadHistory($_POST["orden"],"Actualización de Carta Renuncia Beneficios Ford Protecj","Carta Renuncia");
                    $respuesta = "OK_".$this->encrypt($_POST["orden"]);
                } else {
                    $respuesta = "NO SE PUDO GUARDAR EL FORMULARIO_".$this->encrypt($_POST["orden"]);
                }
            }
        }else{
            if ($valida_form == 2) {
                $respuesta = "NO SE ENCONTRO UNA ORDEN DE SERVICIO ABIERTA_0";
            }else{
                $respuesta = "YA EXISTE UN REGISTRO PARA ESTE NÚMERO DE ORDEN_0";
            }
        }
        
        echo $respuesta;
    }

    public function guardar_firma()
    {
        $contenido["firma_cliente"] = $this->base64ToImage($_POST["ruta_firma_cliente"],"firmas");
        $contenido["fecha_cliente"] = date("Y-m-d");
        $contenido["fecha_actualiza"] = date("Y-m-d H:i:s");

        $actualizar = $this->consulta->update_table_row('carta_renuncia',$contenido,'id_cita',$_POST["id_cita"]);
        if ($actualizar) {
            $contededor["firma_protect"] = "1";
            //Guardamos el registro
            $actualiza = $this->consulta->update_table_row('ordenservicio',$contededor,'id_cita',$_POST["id_cita"]);
            $this->loadHistory($_POST["id_cita"],"Actualización de Carta Renuncia Beneficios Ford Protecj ( Firma del cliente)","Carta Renuncia");

            $respuesta = "OK=".$this->encrypt($_POST["id_cita"]);
        } else {
            $respuesta = "La ocurrio un problema al intentar actualizar el formulario=".$this->encrypt($_POST["id_cita"]);
        }
        
        echo $respuesta;
    }

    public function confirmar_orden($id_cita='')
    {
        //Revisamos que exista una orden abierta
        $query = $this->consulta->get_result("id_cita",$id_cita,"ordenservicio");
        foreach ($query as $row){
            $validar = $row->id;
        }

        //Si ya existe la orden parte 1, revisamos si no existe un formato ya registrado con esa orden
        if (isset($validar)) {
            $query = $this->consulta->get_result("id_cita",$id_cita,"carta_renuncia");
            foreach ($query as $row){
                $validar_2 = $row->id;
            }

            //Si ya existe la orden parte 1, revisamos si no existe un formato ya registrado con esa orden
            if (!isset($validar_2)) {
                $respuesta = 1;
            } else {
                $respuesta = 3;
            }
            
        } else {
            $respuesta = 2;
        }

        return $respuesta;
    }

    function base64ToImage($imgBase64 = "",$direcctorio = "") {
        //Verificamos si se guarda la imagen o si se envio las url
        if (substr($imgBase64,0,7) != "assets/") {
            //Generamos un nombre random para la imagen
            $str = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $urlNombre = date("YmdHi")."_CR";
            for($i=0; $i<=6; $i++ ){
                $urlNombre .= substr($str, rand(0,strlen($str)-1) ,1 );
            }

            $urlDoc = 'assets/imgs/'.$direcctorio;

            $data = explode(',', $imgBase64);
            //Creamos la ruta donde se guardara la firma y su extensión
            if ($data[0] == "data:image/png;base64") {
                $base ='assets/imgs/'.$direcctorio.'/'.$urlNombre.".png";
                $Base64Img = base64_decode($data[1]);
                file_put_contents($base, $Base64Img);
            }else {
                $base = "";
            }
        } else {
            $base = $imgBase64;
        }

        return $base;
    }

    public function validateCheck($campo = NULL,$elseValue = "")
    {
        if ($campo == NULL) {
            $respuesta = $elseValue;
        }else {
            $respuesta = "";
            if (count($campo) >0){
                //Separamos los index de los id de tipo de usuario
                for ($i=0; $i < count($campo) ; $i++) {
                    $respuesta .= $campo[$i]."_";
                }
            }
        }
        return $respuesta;
    }

    public function comodinLista($respuesta = 4, $datos = NULL)
    {
        $query = $this->consulta->get_table_list("carta_renuncia","id");
        foreach ($query as $row) {
            $datos["id_cita"][] = $row->id_cita ;
            $datos["folio"][] = $row->folio ;
            $datos["envio"][] = $row->envio ;
            //$datos["reenvio"][] = $row->reenvio ;
            $datos["serie"][] = $row->serie;
            $datos["placas"][] = $row->placas;
            $datos["unidad"][] = $row->unidad;
            $datos["cliente"][] = $row->nombre_cliente ;
            $datos["asesor"][] = $row->nombre_asesor ;                        
            $datos["tecnico"][] = $row->tecnico ;
            $fecha_recibe = new DateTime($row->fecha_alta);
            $datos["fecha_registro"][] = $fecha_recibe->format('d/m/Y');

            $datos["id_cita_url"][] = $this->encrypt($row->id_cita) ;

            if ($row->reenvio == "0") {
                if ($row->envio == "0") {
                    $datos["cl_envio"][] = "#fffff";
                } else if ($row->envio == "1") {
                    $datos["cl_envio"][] = "#68ce68";
                }else{
                    $datos["cl_envio"][] = "#f5acaa";
                }
            } else {
                if ($row->reenvio == "1") {
                    $datos["cl_envio"][] = "#68ce68";
                }else{
                    $datos["cl_envio"][] = "#f5acaa";
                }
            }
            
        }

        $datos["mensaje"] = $respuesta;

        $this->loadAllView($datos,'otros_Modulos/carta_renuncia/lista');
    }

    public function setDataClienteFirma($identificador = 0)
    {
        $datos["destinoVista"] = "FIRMA";
        $id_limpio = ((ctype_digit($identificador)) ? $identificador : $this->decrypt($identificador));
        $this->cargar_registro($id_limpio,$datos);
    }

    public function setDataRevision($identificador = 0)
    {
        $datos["destinoVista"] = "Revisión";
        $id_limpio = ((ctype_digit($identificador)) ? $identificador : $this->decrypt($identificador));
        $this->cargar_registro($id_limpio,$datos);
    }

    public function setDataPDF($identificador = 0)
    {
        $datos["destinoVista"] = "PDF";
        $id_limpio = ((ctype_digit($identificador)) ? $identificador : $this->decrypt($identificador));
        $this->cargar_registro($id_limpio,$datos);
    }

    public function cargar_registro($id_cita='',$datos=NULL)
    {
        if (!isset($datos["destinoVista"])) {
            $datos["destinoVista"] = "Formulario";
        }

        $revision = $this->consulta->get_result("id_cita",$id_cita,"carta_renuncia");
        foreach ($revision as $row) {
            $datos["id"] = $row->id;
            $datos["id_orden"] = $row->id_cita;
            $datos["folio"] = $row->folio;
            $datos["serie"] = $row->serie;
            $datos["modelo"] = $row->modelo;
            $datos["placas"] = $row->placas;
            $datos["unidad"] = $row->unidad;
            $datos["tecnico"] = $row->tecnico;
            $datos["beneficios"] = explode("_",$row->beneficios);
            $datos["firma_asesor"] = ((file_exists($row->firma_asesor)) ? $row->firma_asesor : "");
            $datos["nombre_asesor"] = $row->nombre_asesor;
            $datos["fecha_asesor"] = $row->fecha_asesor;
            $datos["firma_cliente"] = ((file_exists($row->firma_cliente)) ? $row->firma_cliente : "");
            $datos["nombre_cliente"] = $row->nombre_cliente;
            $datos["fecha_cliente"] = $row->fecha_cliente;

            $fecha_asesor = new DateTime($row->fecha_asesor.' 00:00:00');
            $datos["fecha_asesor_visual"] = $fecha_asesor->format('d-m-Y');
            $fecha_cliente = new DateTime($row->fecha_cliente.' 00:00:00');
            $datos["fecha_cliente_visual"] = $fecha_cliente->format('d-m-Y');

            $datos["fecha_asesor"] = $row->fecha_asesor;
            $datos["fecha_cliente"] = $row->fecha_cliente;
            
        }

        if ($datos["destinoVista"] == "PDF") {
            //$this->generatePDF($datos);
            $this->load->view("otros_Modulos/carta_renuncia/plantillaPDF2", $datos);
        }elseif ($datos["destinoVista"] == "FIRMA") { 
            $this->loadAllView($datos,'otros_Modulos/carta_renuncia/plantillaCliente');
        }else{
            $this->loadAllView($datos,'otros_Modulos/carta_renuncia/formulario');
        }
    }

    /*public function generatePDF($datos = NULL)
    {
        $this->load->view("otros_Modulos/carta_renuncia/plantillaPDF2", $datos);
    }*/

    public function consulta_datos()
    {
        //Recuperamos el id de la orden
        $id_cita = $_POST['orden'];
        $cadena = "";

        //verificamos si existe el reistro
        $revision = $this->consulta->get_result("id_cita",$id_cita,"carta_renuncia");
        foreach ($revision as $row) {
            $verificar = $row->id;
        }

        //Si existe el registro cargamos los datos, de lo contrario ponemos el formulario en blanco
        if (isset($verificar)) {
            $cadena .= "EDITAR=".$this->encrypt($id_cita)."=";
        }else{
            $cadena .= "ALTA=";

            $precarga = $this->consulta->precargaConsulta($id_cita);
            foreach ($precarga as $row){
                $cadena .= (($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion)."=";
                $cadena .= "="; //$row->folioIntelisis."=";
                $cadena .= $row->vehiculo_anio."=";
                $cadena .= $row->vehiculo_placas."=";
                $cadena .= $row->vehiculo_modelo."=";
                $cadena .= $row->tecnico."=";
                $cadena .= (($row->nombre_compania != "") ? $row->nombre_compania : $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno)."=";
                $cadena .= $row->asesor."=";

                $orden = $row->id;
            }

            //Revisar si se lleno la carta renuncia con datos
            $revision = $this->consulta->get_result("noServicio",$id_cita,"diagnostico");
            foreach ($revision as $row) {
                $cadena .= $row->autorizacion_ausencia."=";
                $cadena .= $row->motivo_ausencia."=";
                $indicador = $row->idDiagnostico;
            }

            if (!isset($indicador)) {
                $cadena .= "0=0=";
            }

            //Validamos si existe la orden previa
            if (isset($orden)) {
                if ($orden != NULL) {
                    $cadena .= "1";
                }else{
                   $cadena .= "0";
                }
            }else{
                $cadena .= "2";
            }
        }

        echo $cadena;
    } 

    public function buscar_cartarenuncia()
    {
        $campo = $_POST['campo'];
        $cadena = "";

        $consulta = $this->consulta->buscar_cartarenuncia($campo);

        foreach ($consulta as $row){
            // 0
            $cadena .= $row->id_cita."=";
            $cadena .= $row->folio."=";
            $cadena .= $row->serie."=";
            $cadena .= $row->placas."=";
            $cadena .= $row->unidad."=";
            //5
            $cadena .= $row->nombre_cliente."=";
            $cadena .= $row->nombre_asesor."=";
            $cadena .= $row->tecnico."=";

            $fecha_recibe = new DateTime($row->fecha_alta);
            $cadena .= $fecha_recibe->format('d/m/Y')."=";
            //9
            $cadena .= $this->encrypt($row->id_cita) ."=";
            $cadena .= $row->modelo."=";
            $cadena .= $row->envio."=";

            if ($row->reenvio == "0") {
                if ($row->envio == "0") {
                    $cadena .= "#ffffff=";
                } else if ($row->envio == "1") {
                    $cadena .= "#68ce68=";
                }else{
                    $cadena .= "#f5acaa=";
                }
            } else {
                if ($row->reenvio == "1") {
                    $cadena .= "#68ce68=";
                }else{
                    $cadena .= "#f5acaa=";
                }
            }

            $cadena .= "|";
        }

        echo $cadena;
    }

    public function envio_email()
    {
        $id_cita = $_POST['orden'];
        $tipo_envio = $_POST['envio'];
        $cadena = "";

        //Definimos la fecha de envio
        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","viernes","Sábado");
        $datosEmail["dia"] = $dias[date("w")];
        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $datosEmail["mes"] = $meses[date("n")-1];
        $datosEmail["id_cita"] = $id_cita;

        //Recuperamos la informacion a enviar
        $query = $this->consulta->precargaConsulta($id_cita);
        foreach ($query as $row){
            $datosEmail["correo1"] = $row->datos_email;
            $datosEmail["unidad"] = $row->vehiculo_modelo;
            $datosEmail["correo2"] = $row->correo_compania;
            $datosEmail["nombre"] = $row->datos_nombres." ".$row->datos_apellido_paterno." ".$row->datos_apellido_materno;
            $datosEmail["companiaNombre"] = $row->nombre_compania;
            $datosEmail["serie"] = ($row->vehiculo_numero_serie != "") ? $row->vehiculo_numero_serie : $row->vehiculo_identificacion;
        }

        //Url del archivo
        $datosEmail["url"] = base_url()."CartaRenuncia_PDF/".$this->encrypt($id_cita);
        //Cargamos la informacion en la plnatilla
        $htmlContent = $this->load->view("otros_Modulos/carta_renuncia/plantillaEmail", $datosEmail, TRUE);

        $configGmail = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => EMAIL_SERVER,
            'smtp_pass' => EMAIL_PASS,
            'mailtype' => 'html',
            'smtp_crypto'=> 'tls',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        );

        //Establecemos esta configuración
        $this->email->initialize($configGmail);
        //Ponemos la dirección de correo que enviará el email y un nombre
        $this->email->from(EMAIL_SERVER, SUCURSAL);
        //Ponemos la direccion de aquien va dirigido
        $this->email->to($datosEmail["correo1"],$datosEmail["nombre"]);
        $this->email->to($datosEmail["correo2"],$datosEmail["nombre"]); 
        //$this->email->to('desarrollo@sohex.mx','Tester');
        //Multiples destinos
        // $this->email->to($correo1, $correo2, $correo3);
        //Con copia a:
        // $this->email->cc($correo);
        //Con copia oculta a:
        // $this->email->bcc($correo);
        //$this->email->bcc('info@planificadorempresarial.mx','Angel');
            //$this->email->bcc('contactoplasenciamatriz@gmail.com','Contacto Plasencia Motors Guadalajara');
        //$this->email->bcc('info@sohex.mx','Angel');
        //Colocamos el asunto del correo
        $this->email->subject('Formato de renuncia de beneficio');
        //Cargamos la plantilla del correo
        $this->email->message($htmlContent);
        //Enviar archivo adjunto
        // $this->email->attach('files/attachment.pdf');
        //Enviamos el correo
        if($this->email->send()){
            $envio = 1;
        }else{
            $envio = 2;
        }
        //echo $this->email->print_debugger();
        //die();

        $registro = date("Y-m-d H:i:s");
        $envios_email = $datosEmail["correo1"]."|".$datosEmail["correo2"];
        //Actualizamos la tabla de multipunto para notificar = 1 (envio exitoso), notificar = 2 (envio fallido)
        if ($tipo_envio == "1") {
            $dataEmail = array(
                "correo" => $envios_email,
                "envio" => $envio,
                "fecha_envio" => $registro,
                "fecha_actualiza" => $registro
            );
        }else{
            $dataEmail = array(
                "correo" => $envios_email,
                "reenvio" => $envio,
                "fecha_reenvio" => $registro,
                "fecha_actualiza" => $registro
            );
        }

        //Grabamos la información en la tabla
        $actualizar = $this->consulta->update_table_row('carta_renuncia',$dataEmail,'id_cita',$id_cita);

        if ($actualizar) {
            if ($envio == 1) {
                $this->loadHistory($id_cita,"Envio de correo de Carta Renuncia Beneficios Ford Protecj","Carta Renuncia");
                $respuesta = "OK|".$envios_email;
            } else {
                $respuesta = "NO SE PUDO ENVIAR EL CORREO|".$envios_email;
                $this->loadHistory($id_cita,"Envio fallido de correo de Carta Renuncia Beneficios Ford Protecj","Carta Renuncia");
            }
        }else {
            $respuesta = "NO SE PUDO ACTUALIZAR|".$envios_email;
        }

        echo $respuesta;
    }

    //Creamos el registro del historial
    public function loadHistory($id_cita = 0, $descripcion = "",$formulario = "")
    {
        $registro = date("Y-m-d H:i:s");

        if ($this->session->userdata('rolIniciado')) {
            if ($this->session->userdata('rolIniciado') == "ASE") {
                $panel = "Asesores";
            }elseif ($this->session->userdata('rolIniciado') == "TEC") {
                $panel = "Tecnicos";
            }elseif ($this->session->userdata('rolIniciado') == "JDT") {
                $panel = "Jefe de Taller";
            }elseif ($this->session->userdata('rolIniciado') == "ADMIN") {
                $panel = "Panel principal";
            }else {
                $panel = "Sesión expirada";
            }

            $id_usuario = $this->session->userdata('idUsuario');
            $usuario = $this->session->userdata('nombreUsuario');

        } elseif ($this->session->userdata('id_usuario')) {
            $panel = "Panel Servicios";
            $id_usuario = $this->session->userdata('id_usuario');
            $usuario = $this->Verificacion->recuperar_usuario($this->session->userdata('id_usuario'));
        }else{
            $panel = "Sesión expirada";
            $id_usuario =  "";
            $usuario = "";
        }

        $dataHistory = array(
            'id' => NULL,
            'idOrden' => $id_cita,
            'panel' => $panel,
            'formulario' => $formulario,
            'tipoMovimiento' => $descripcion,
            'idUsuario' => $id_usuario,
            'nombreUsuario' => $usuario,
            'fechaRegistro' => $registro,
            'fechaModificacion' => $registro,
        );

        $this->consulta->save_register('registro_actividad',$dataHistory);

        return TRUE;
    }

    function encrypt($data){
        $id = (double)$data*CONST_ENCRYPT;
        $url_id = base64_encode($id);
        $url = str_replace("=", "" ,$url_id);
        return $url;
        //return $data;
    }

    function decrypt($data){
        $url_id = base64_decode($data);
        $id = (double)$url_id/CONST_ENCRYPT;
        return $id;
        //return $data;
    }

    //Cargamos las vistas
    public function loadAllView($datos = NULL,$vista = "")
    {
        //Cargamos los archivos js necesarios para la vista
        $archivosJs = array(
            "include" => array(
                'assets/js/otrosModulos/protect/envio_formulario.js',
                'assets/js/otrosModulos/protect/consulta.js',
                'assets/js/firmasMoviles/firmas_carta_renuncia.js',
                //'assets/js/superUsuario.js'
            )
        );

        //Cargamos las vistas para implementarlas
        $coleccion = array(
            "header" => $this->load->view("layout/header", '', TRUE),
            "contenido" => $this->load->view($vista, $datos, TRUE),
            "footer" => $this->load->view("layout/footer", $archivosJs, TRUE),
            "titulo" => "Carta Renuncia Protect"
        );

        //Cargamos la estructura principal para cargar el Panel
        $this->load->view("layout/main",$coleccion);
    }
}